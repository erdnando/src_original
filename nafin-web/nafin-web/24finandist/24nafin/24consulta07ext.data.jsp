<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/24finandist/24secsession_extjs.jspf" %>
		<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
	// variables de despliegue
	String tipoCreditoOperar =	"";
	String responsabilidadPagoInteres =	"";
	String tipoCobranza = "";
	String esquemaPagoInteres = "";
	
	String diasMinimosDocCarga = "";
	String diasMaximosDocCarga = "";
	String tipoConversion = "";
	String opPantallaControl	= "";
	String proxVenc	= "";
	String diasFechaVenc = "";
	
	String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
	String tipoDato = "", valor = "", consulta = "", infoRegresar =""; 
	Vector vecParametros1 = new Vector(); 	
	Vector vecParametros2 = new Vector();
	HashMap datosEsquema = new HashMap();
	HashMap datosParametro = new HashMap();
	
	JSONObject jsonObj = new JSONObject();
	JSONArray registrosEsquema = new JSONArray();
	JSONArray registrosParametro = new JSONArray();
	
	ParametrosDist  beanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
	
	if(informacion.equals("esquemasOperacion")) {//-- Obtengo los datos del primer grid (Seccion 1)
		
		vecParametros1 = beanParametros.getParamGral();
		tipoCreditoOperar = (vecParametros1.get(0).toString().trim()==null)?"":vecParametros1.get(0).toString().trim();
	   responsabilidadPagoInteres = (vecParametros1.get(1).toString().trim()==null)?"":vecParametros1.get(1).toString().trim();
	   tipoCobranza = (vecParametros1.get(2).toString().trim()==null)?"":vecParametros1.get(2).toString().trim();
	   esquemaPagoInteres = (vecParametros1.get(3).toString().trim()==null)?"":vecParametros1.get(3).toString().trim();
		tipoDato = "C";
	
		//------------ Defino el texto que llevará la columna valores ------------
		if(tipoCreditoOperar.equals("D")){
			valor = "Descuento y/o Facturaje";
		} else if(tipoCreditoOperar.equals("C")){
			valor = "Crédito en cuenta corriente";
		} else if(tipoCreditoOperar.equals("A")){
			valor = "Descuento crédito en cuenta corriente";
		} else{
			valor = "";
		}	
		if(tipoCobranza.equals("D")){
			tipoCobranza = "Delegada";
		} else if(tipoCobranza.equals("N")){
			tipoCobranza = "No delegada";
		}
		if(responsabilidadPagoInteres.equals("E")){
			responsabilidadPagoInteres = "EPO";
		} else if(responsabilidadPagoInteres.equals("D")){
			responsabilidadPagoInteres ="Distribuidor";
		}	
		if(esquemaPagoInteres.equals("1")){
			esquemaPagoInteres = "Anticipado";
		} else if(esquemaPagoInteres.equals("2")){
			esquemaPagoInteres ="Al vencimiento";
		}	
		//------------------------------------------------
		
		datosEsquema = new HashMap();
		datosEsquema.put("NOMBRE_PARAMETRO", "Tipo de crédito a operar");
		datosEsquema.put("TIPO_DATO", tipoDato);
		datosEsquema.put("VALORES", valor);
		registrosEsquema.add(datosEsquema);
		
		//------------  Valido si se muestran las columnas 2 y 3  ------------
		if(tipoCreditoOperar.equals("A") || tipoCreditoOperar.equals("D")){
			datosEsquema = new HashMap();
			datosEsquema.put("NOMBRE_PARAMETRO", "Responsabilidad de pago de interés (Sólo si opera DM)");
			datosEsquema.put("TIPO_DATO", tipoDato);
			datosEsquema.put("VALORES", responsabilidadPagoInteres);
			registrosEsquema.add(datosEsquema);
			
			datosEsquema = new HashMap();
			datosEsquema.put("NOMBRE_PARAMETRO", "Tipo de cobranza (Sólo si opera DM)");
			datosEsquema.put("TIPO_DATO", tipoDato);
			datosEsquema.put("VALORES", tipoCobranza);
			registrosEsquema.add(datosEsquema);
		}
		//------------------------------------------------
		
		datosEsquema = new HashMap();
		datosEsquema.put("NOMBRE_PARAMETRO", "Esquema de pago de intereses");
		datosEsquema.put("TIPO_DATO", tipoDato);
		datosEsquema.put("VALORES", esquemaPagoInteres);
		registrosEsquema.add(datosEsquema);
		
		consulta =  "{\"success\": true, \"total\": \"" + registrosEsquema.size() + "\", \"registros\": " + registrosEsquema.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		infoRegresar = jsonObj.toString();  
		
	}  else if(informacion.equals("parametros")) {//--> Obtengo los datos del segundo grid (Seccion 2)
		
		vecParametros2 = beanParametros.getParamGrala();
		diasMinimosDocCarga = (vecParametros2.get(0).toString().trim()==null)?"":vecParametros2.get(0).toString().trim();
	   diasMaximosDocCarga = (vecParametros2.get(1).toString().trim()==null)?"":vecParametros2.get(1).toString().trim();
	   tipoConversion = (vecParametros2.get(2).toString().trim()==null)?"":vecParametros2.get(2).toString().trim();
	   opPantallaControl	= (vecParametros2.get(3).toString().trim()==null)?"":vecParametros2.get(3).toString().trim();
	   proxVenc	= (vecParametros2.get(5).toString().trim()==null)?"":vecParametros2.get(5).toString().trim();
	   diasFechaVenc = (vecParametros2.get(6).toString().trim()==null)?"":vecParametros2.get(6).toString().trim();
		tipoDato = "C";
		
		//------------ Defino el texto que llevará la columna valores ------------
		if(tipoConversion.equals("P")){
			tipoConversion = "Peso - Dolar";
		} else if(tipoConversion.equals("")){
			tipoConversion = "Sin conversión";
		} else{
			tipoConversion = "Sin conversión";
		}
		if(opPantallaControl.equals("S")){
			opPantallaControl = "Si mostrar";
		} else if(opPantallaControl.equals("N")){
			opPantallaControl = "No mostrar";
		}
		//-----------------------------------------------
		
		datosParametro = new HashMap();
		datosParametro.put("NOMBRE_PARAMETRO", "Plazo mínimo del financiamiento");
		datosParametro.put("TIPO_DATO", "N");
		datosParametro.put("VALORES", diasMinimosDocCarga);
		registrosParametro.add(datosParametro);
	
		datosParametro = new HashMap();
		datosParametro.put("NOMBRE_PARAMETRO", "Plazo máximo del financiamiento");
		datosParametro.put("TIPO_DATO", "N");
		datosParametro.put("VALORES", diasMaximosDocCarga);
		registrosParametro.add(datosParametro);
		
		datosParametro = new HashMap();
		datosParametro.put("NOMBRE_PARAMETRO", "Días máximos para ampliar la fecha de vencimiento");
		datosParametro.put("TIPO_DATO", "N");
		datosParametro.put("VALORES", diasFechaVenc);
		registrosParametro.add(datosParametro);
	
		datosParametro = new HashMap();
		datosParametro.put("NOMBRE_PARAMETRO", "Tipo de converisón");
		datosParametro.put("TIPO_DATO", tipoDato);
		datosParametro.put("VALORES", tipoConversion);
		registrosParametro.add(datosParametro);
		
		datosParametro = new HashMap();
		datosParametro.put("NOMBRE_PARAMETRO", "Operar con pantalla de cifras de control");
		datosParametro.put("TIPO_DATO", tipoDato);
		datosParametro.put("VALORES", opPantallaControl);
		registrosParametro.add(datosParametro);
	
		datosParametro = new HashMap();
		datosParametro.put("NOMBRE_PARAMETRO", "Aviso de próximos vencimientos en:");
		datosParametro.put("TIPO_DATO", "N");
		datosParametro.put("VALORES", proxVenc + " días hábiles");
		registrosParametro.add(datosParametro);
		
		consulta =  "{\"success\": true, \"total\": \"" + registrosParametro.size() + "\", \"registros\": " + registrosParametro.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		infoRegresar = jsonObj.toString();  
		
	}
%>
<%=infoRegresar%>