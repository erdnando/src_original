Ext.onReady(function() {


	function procesaGuarda_Modifica_Eliminar(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
			
				consultaData.load({	});	
				Ext.getCmp('forma').hide();
			
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesaValoresTasas(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
				
				Ext.getCmp('lblMoneda').update(jsonData.lblMoneda);	
				Ext.getCmp('lblPlazo').update(jsonData.lblPlazo);	
				Ext.getCmp('lblTasaAplicar').update(jsonData.lblTasaAplicar);	
				Ext.getCmp('lblFechaUltima').update(jsonData.lblFechaUltima);	
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	

	
	// Catalogo de Relaci�n Mat. 
	var procesarCatRelMatData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cbRelMat1 = Ext.getCmp('cbRelMat1');
			var hidcbRelMat = Ext.getCmp('hidcbRelMat');				
			if(hidcbRelMat.getValue()!=''){
				cbRelMat1.setValue(hidcbRelMat.getValue());
			}
		}
	}  
  
	var catRelMatData = new Ext.data.JsonStore({
		id: 'catRelMatData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma2.data.jsp',
		baseParams: {
			informacion: 'catRelMatData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatRelMatData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	// Catalogo de Tasas 
	var procesarCatTipoTasaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cbTipoTasa1 = Ext.getCmp('cbTipoTasa1');
			var hidcbTipoTasa = Ext.getCmp('hidcbTipoTasa');				
			if(hidcbTipoTasa.getValue()!=''){
				cbTipoTasa1.setValue(hidcbTipoTasa.getValue());
			}
		}
	}  
  
	var catTipoTasaData = new Ext.data.JsonStore({
		id: 'catTipoTasaData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma2.data.jsp',
		baseParams: {
			informacion: 'catTipoTasaData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatTipoTasaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	
	// ***********************Pantalla de  Alta y  Modificar   de tasas *********************+
	
		var elementosForma  =  [
			{				
				xtype				: 'combo',
				id          	: 'cbTipoTasa1',
				name				: 'cbTipoTasa',
				hiddenName 		: 'cbTipoTasa',
				fieldLabel  	: 'Tipo Tasa',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				store				: catTipoTasaData,
				tpl : NE.util.templateMensajeCargaCombo,
				listeners: {
					select: {
						fn: function(combo) {					
							Ext.Ajax.request({
								url: '24forma2.data.jsp',
								params: {
									informacion: "SelecCatTipoTasa",
									cbTipoTasa:combo.getValue(),
									pAccion:Ext.getCmp('pAccion').getValue()
								},
								callback: procesaValoresTasas
							});				
						}
					}
				}			
		},
		{
			xtype: 'displayfield',
			id: 'lblTipoTasa',
			style: 'text-align:left;',
			fieldLabel: 'Tipo Tasa',
			text: '-',
			hidden: true
		},	
		{
			xtype: 'displayfield',
			id: 'lblMoneda',
			style: 'text-align:left;',
			fieldLabel: 'Moneda',
			text: '-'				
		},
		{
			xtype: 'displayfield',
			id: 'lblPlazo',
			style: 'text-align:left;',
			fieldLabel: 'Plazo',
			text: '-'				
		},	
		{
			xtype: 'combo',	
			name: 'cbRelMat',	
			id: 'cbRelMat1',	
			fieldLabel: 'Relaci�n Mat.', 
			mode: 'local',	
			hiddenName : 'cbRelMat',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catRelMatData,	
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 200,
			value	: '+'	
		},
		{
					xtype: 'textfield',
					fieldLabel: 'Puntos Adicionales',	
					name: 'pPuntos',
					id: 'pPuntos1',
					allowBlank: true,
					maxLength: 7,
					width: 200,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},			
		{
			xtype: 'displayfield',
			id: 'lblTasaAplicar',
			style: 'text-align:left;',
			fieldLabel: 'Tasa a Aplicar',
			text: '-'				
		},	
		{
			xtype: 'displayfield',
			id: 'lblFechaUltima',
			style: 'text-align:left;',
			fieldLabel: 'Fecha �ltima actualizaci�n',
			text: '-'				
		},	
		
		{ 	xtype: 'textfield',  hidden: true, id: 'hidcbTipoTasa', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'hidcbRelMat', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'pAccion', 	value: '' }		
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 400,		
		monitorValid: true,		
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [	
			{
				text: 'Guardar',
				id: 'Guardar',
				iconCls: 'icoGuardar',
				handler: function() {
					
					var cbTipoTasa1 = Ext.getCmp('cbTipoTasa1');
					if (Ext.isEmpty(cbTipoTasa1.getValue()) ){
						cbTipoTasa1.markInvalid('Este campo es obigatorio');
						return true;
					}	
					var pPuntos1 = Ext.getCmp('pPuntos1');
					if (Ext.isEmpty(pPuntos1.getValue()) ){
						pPuntos1.markInvalid('Este campo es obigatorio');
						return true;
					}	
					
					Ext.Ajax.request({
						url: '24forma2.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{  
							informacion: "Guarda_Modifica"						
						})
						,callback: procesaGuarda_Modifica_Eliminar
					});	
					
				}
			},
			{
				text: 'Cancelar',
				id: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {
				
					var mensaje = "";
					if(Ext.getCmp('pAccion').getValue()=='A') {
					mensaje =  "Desea cancelar la captura de la nueva tasa.";
					}else  if(Ext.getCmp('pAccion').getValue()=='M') {
						mensaje = "Desea cancelar la modificaci�n de valores de la tasa seleccionada.";
					}
				
					Ext.Msg.show({
						title: "Confirmar",
						msg: mensaje,
						buttons: Ext.Msg.OKCANCEL,
						fn: function peticionAjax(btn){
							if (btn == 'ok') {
								window.location = '24forma2ext.jsp';	
							}
						},
						closable:false,
						icon: Ext.MessageBox.QUESTION
					});			
				}
			}
		]
	});
	
	
	
	//funci�n boton Eliminar
	
	var procesarEliminar = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var cbTipoTasa = registro.get('CLAVE_TASA');		
		
		Ext.Msg.show({
			title: "Confirmar",
			msg: "�Este proceso eliminara la tasa �Desea continuar?", 
			buttons: Ext.Msg.OKCANCEL,
			fn: function peticionAjax(btn){
				if (btn == 'ok') {
					Ext.Ajax.request({
						url: '24forma2.data.jsp',
						params: {  
							informacion: "Eliminar",
							cbTipoTasa:cbTipoTasa
						}
						,callback: procesaGuarda_Modifica_Eliminar
					});
				}
			},
			closable:false,
			icon: Ext.MessageBox.QUESTION
		});							
	}
	
	
	//funci�n boton Modificar
	
	var procesoModificar = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		
		Ext.getCmp('forma').show();
		Ext.getCmp('gridConsulta').hide();
		
		Ext.getCmp('pAccion').setValue('M');
		Ext.getCmp('pPuntos1').setValue(registro.get('PUNTOS_ADICIONALES'));
		Ext.getCmp('hidcbTipoTasa').setValue(registro.get('CLAVE_TASA'));
		Ext.getCmp('hidcbRelMat').setValue(registro.get('REL_MAT'));
		Ext.getCmp('lblTipoTasa').setValue(registro.get('TIPO_TASA'));
		Ext.getCmp('lblTipoTasa').show();
		Ext.getCmp('cbTipoTasa1').hide();

		Ext.getCmp('lblMoneda').update(registro.get('MONEDA'));	
		Ext.getCmp('lblPlazo').update(registro.get('PLAZO'));	
		Ext.getCmp('lblTasaAplicar').update(registro.get('TASA_APLICAR'));	
		Ext.getCmp('lblFechaUltima').update(registro.get('FECHA'));	
				

				
		catTipoTasaData.load() ;
		catRelMatData.load();		
	}
	
	//funci�n boton Agregar
	function procesoAgregar() {
	
		Ext.getCmp('forma').show();
		Ext.getCmp('gridConsulta').hide();
		catTipoTasaData.load();
		catRelMatData.load();
		Ext.getCmp('pAccion').setValue('A');
		
		Ext.getCmp('pPuntos1').setValue('');
		Ext.getCmp('hidcbTipoTasa').setValue('');
		Ext.getCmp('hidcbRelMat').setValue('');
		Ext.getCmp('lblTipoTasa').setValue('');
		Ext.getCmp('lblTipoTasa').hide();
		Ext.getCmp('cbTipoTasa1').show();
		Ext.getCmp('cbTipoTasa1').setValue('');
		
		Ext.getCmp('lblMoneda').update('');	
		Ext.getCmp('lblPlazo').update('');	
		Ext.getCmp('lblTasaAplicar').update('');	
		Ext.getCmp('lblFechaUltima').update('');	
		
		
		
	}
	
 //*******************Consulta *************************************
 
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
	
		var gridConsulta = Ext.getCmp('gridConsulta');				
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsulta.getColumnModel();
			
			if(store.getTotalCount() > 0) {			
				el.unmask();				
			} else {		
				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24forma2.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [
			{	name: 'MONEDA'},
			{	name: 'TIPO_TASA'},
			{	name: 'PLAZO'},
			{	name: 'VALOR'},
			{	name: 'REL_MAT'},
			{	name: 'PUNTOS_ADICIONALES'},
			{	name: 'TASA_APLICAR'},
			{	name: 'CLAVE_TASA'},
			{	name: 'FECHA'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});	
		
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Tasas',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Moneda ',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Tipo Tasa',
				tooltip: 'Tipo Tasa',
				dataIndex: 'TIPO_TASA',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'			
			},
			{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'VALOR',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'			
			},
			{
				header: 'Rel. Mat.',
				tooltip: 'Rel. Mat.',
				dataIndex: 'REL_MAT',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'			
			},
			{
				header: 'Puntos Adicionales',
				tooltip: 'Puntos Adicionales',
				dataIndex: 'PUNTOS_ADICIONALES',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'			
			},
			{
				header: 'Tasa a Aplicar',
				tooltip: 'Tasa a Aplicar',
				dataIndex: 'TASA_APLICAR',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'			
			},
				{
				xtype: 'actioncolumn',
				header: 'Acciones',
				tooltip: 'Acciones',				
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Modificar';
							return 'modificar';
						}
						,handler: procesoModificar
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[1].tooltip = 'Eliminar';
							return 'borrar';
						}
						,handler: procesarEliminar
					}					
				]				
			}
		
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,		
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Agregar',					
					tooltip:	'Agregar',
					iconCls: 'icoAgregar',
					id: 'btnAgregar',
				   handler: procesoAgregar						
				}
			]
		}
	});


 //*******************Criterios de Busqueda *************************************
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),			
			gridConsulta,
			fp, 
			NE.util.getEspaciador(20)			
		]
	});
	
	
	Ext.getCmp('forma').hide();
	consultaData.load({	});
	
	
});
