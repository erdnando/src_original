<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String lsCveTasa = request.getParameter("cbTipoTasa") == null?"":(String)request.getParameter("cbTipoTasa");
String pAccion = request.getParameter("pAccion") == null?"":(String)request.getParameter("pAccion");
String pRelMat = request.getParameter("cbRelMat") == null?"":(String)request.getParameter("cbRelMat");
String pPuntos = request.getParameter("pPuntos") == null?"":(String)request.getParameter("pPuntos");


String  infoRegresar ="", consulta ="",  lsCadena = "", lsMoneda="", lsTipoTasa="",  lsPlazo="", lsValor="", 
lsRelMat="", lsPuntos="", lsTasaPiso="", lsFecha="";
HashMap datos = new HashMap();
JSONArray registros = new JSONArray(); 
JSONObject jsonObj = new JSONObject();

CapturaTasas  BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class); 

	
if(informacion.equals("Consultar") )  {
	
	Vector lovTasas = BeanTasas.ovgetTasas(4);
	
	for (int i=0; i<lovTasas.size(); i++) {
		Vector lovDatosTasa = (Vector)lovTasas.get(i);
		lsMoneda = lovDatosTasa.get(0).toString();
		lsTipoTasa = lovDatosTasa.get(1).toString();
		lsCveTasa = lovDatosTasa.get(2).toString();
		lsPlazo = lovDatosTasa.get(3).toString();
		lsValor = lovDatosTasa.get(4).toString();
		lsRelMat = lovDatosTasa.get(5).toString();
		lsPuntos = lovDatosTasa.get(6).toString();
		lsTasaPiso = lovDatosTasa.get(7).toString();
		lsFecha = lovDatosTasa.get(8).toString();		
	
		datos = new HashMap();
		datos.put("MONEDA", lsMoneda);
		datos.put("TIPO_TASA", lsTipoTasa);
		datos.put("PLAZO", lsPlazo);
		datos.put("VALOR", Comunes.formatoDecimal(Double.parseDouble(lsValor),2));
		datos.put("REL_MAT", lsRelMat);
		datos.put("PUNTOS_ADICIONALES", Comunes.formatoDecimal(Double.parseDouble(lsPuntos),2));		
		datos.put("TASA_APLICAR", Comunes.formatoDecimal(Double.parseDouble(lsTasaPiso),2));	
		datos.put("CLAVE_TASA", lsCveTasa);	
		datos.put("FECHA", lsFecha);	
		registros.add(datos);
	
	} //for
	
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();  	
	
}else   if(informacion.equals("catTipoTasaData") )  {
		
	registros = new JSONArray();
	
	Vector lovTasasD = BeanTasas.ovgetTasasDisponibles(4);
	for (int i=0; i<lovTasasD.size(); i++) {
		Vector lovDatos = (Vector)lovTasasD.get(i);
		String lsClaveTasa = lovDatos.get(0).toString();
		String lsDescripcionTasa = lovDatos.get(1).toString();
		
		datos = new HashMap();
		datos.put("clave", lsClaveTasa);
		datos.put("descripcion", lsDescripcionTasa);
		registros.add(datos);
	} 	
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);		
	infoRegresar = jsonObj.toString();
	
}else   if(informacion.equals("SelecCatTipoTasa") )  {
	
	String pMoneda ="",pPlazo  ="", pFecha ="", pTasaApp ="";

	Vector lovInformacionTasa = BeanTasas.ovgetDatosTasaDis(pAccion,4,lsCveTasa);	
	if(lovInformacionTasa.size() != 0){
		pMoneda = lovInformacionTasa.get(0).toString();
		pPlazo = lovInformacionTasa.get(1).toString();
		pFecha = lovInformacionTasa.get(2).toString();
		pTasaApp = lovInformacionTasa.get(3).toString();					
	}		

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("lblMoneda", pMoneda);
	jsonObj.put("lblPlazo", pPlazo);
	jsonObj.put("lblTasaAplicar", pTasaApp);
	jsonObj.put("lblFechaUltima", pFecha);
	infoRegresar = jsonObj.toString();


}else   if(informacion.equals("catRelMatData") )  {
		
	registros = new JSONArray();
	datos = new HashMap(); 	datos.put("clave", "+"); datos.put("descripcion", "+"); 	registros.add(datos);
	datos = new HashMap(); 	datos.put("clave", "-"); datos.put("descripcion", "-"); 	registros.add(datos);
	datos = new HashMap(); 	datos.put("clave", "*"); datos.put("descripcion", "*"); 	registros.add(datos);
	datos = new HashMap(); 	datos.put("clave", "/"); datos.put("descripcion", "/"); 	registros.add(datos);
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();


}else   if(informacion.equals("Guarda_Modifica")  )  {
	
	if(pAccion.equals("A")  )  {  pAccion= "GA"; }
	if(pAccion.equals("M")  )  {  pAccion= "GM"; }	
	
	try {
		
		boolean bOkInserta = BeanTasas.binsertaTasaDis(pAccion,4,lsCveTasa, pRelMat, pPuntos); // SQL para insertar nuevo registro.
		jsonObj.put("success", new Boolean(true));
	
	} catch(NafinException ne) {
		log.error ("Error al Eliminar las Tasas "+ne.getMsgError());
	
	}	
	
	infoRegresar = jsonObj.toString();
	
}else   if(informacion.equals("Eliminar")  )  {
	
	try {
		
		boolean bOkInserta = BeanTasas.eliminarTasasDis(lsCveTasa);	 // SQL para eliminar registro.
		jsonObj.put("success", new Boolean(true));
		
	} catch(NafinException ne) {
		log.error ("Error al Eliminar las Tasas "+ne.getMsgError());
	
	}	
	
	infoRegresar = jsonObj.toString();
			
}	
		
%>

<%=infoRegresar%>