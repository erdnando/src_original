<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.distribuidores.*,
	com.netro.exception.*,
	netropology.utilerias.*,	 
	com.netro.model.catalogos.*,	
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion   = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String estatus       = request.getParameter("status")      == null?"":(String)request.getParameter("status");
int opcionEstatus    = 0;
double totalDoctosMN = 0, totalMontoMN   = 0, totalMontoValuadoMN = 0, totalMontoCreditoMN = 0, totalMontoDescuentoMN = 0, totalMontoInteresesMN = 0;
double totalDoctosUS = 0, totalMontoUS   = 0, totalMontoValuadoUS = 0, totalMontoCreditoUS = 0, totalMontoDescuentoUS = 0, totalMontoInteresesUS = 0;
double montoValuado  = 0, montoDescontar = 0, montoCapitalInt     = 0;

ConsCambioEstatusNafin paginador = new ConsCambioEstatusNafin();
CatalogoSimple catalogo = new CatalogoSimple();

String infoRegresar="", consulta="", totales="", mensaje="", tituloGrid="", registrosStr="", nombreArchivo="";
JSONObject jsonObj  = new JSONObject();
JSONArray registros = new JSONArray(); 

HashMap datos = new HashMap();

//	Se establece el título para el grid que se mostrará
if(estatus.equals("2")){
	tituloGrid = "Seleccionado PyME a negociable";
} else if(estatus.equals("4")){
	tituloGrid = "Negociable a baja";
} else if(estatus.equals("21")){
	tituloGrid = "Modificaciones";
} else if(estatus.equals("22")){
	tituloGrid = "Negociable a vencido sin operar";
} else if(estatus.equals("24")){
	tituloGrid = "No negociable a baja";
}else if(estatus.equals("23")){
	tituloGrid = "En Proceso de Autorización IF a Negociable";
}else if(estatus.equals("34")){
	tituloGrid = "En Proceso de Autorización IF a Seleccionado Pyme";
}

if (informacion.equals("catalogoEstatus")) {

	catalogo.setTabla("comcat_cambio_estatus");
   catalogo.setDistinc(false);
   catalogo.setCampoClave("ic_cambio_estatus");
   catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setValoresCondicionIn("23,34,2, 4, 21, 24", Integer.class); //22: Negociable a vencido sin operar
	infoRegresar = catalogo.getJSONElementos();  
	
} else if (informacion.equals("consultaData_2") || informacion.equals("consultaData_21") || informacion.equals("consultaData_4_22_24") || informacion.equals("consultaTotal")|| informacion.equals("consultaData_22")) {
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	paginador.setOpcionEstatus(estatus);
	paginador.setTituloTabla(tituloGrid);
	Registros listaRegistros = new Registros();
	listaRegistros = queryHelper.doSearch();
	while(listaRegistros.next()) { //Actualizo los campos necesarios, y agrego las columnas faltantes
		if((listaRegistros.getString("IC_MONEDA")).equals("1")){
			listaRegistros.setObject("TIPO_CAMBIO", "1");
		}
		if((listaRegistros.getString("CG_VENTACARTERA")).equals("S")){
			listaRegistros.setObject("MODO_PLAZO", "");
		}
		//---- montoValuado, montoDescontar y montoCapitalInt (INICIO)---------		
		try{
			montoValuado = Double.parseDouble(listaRegistros.getString("FN_MONTO"))*Double.parseDouble(listaRegistros.getString("TIPO_CAMBIO"));	
			montoDescontar = Double.parseDouble(listaRegistros.getString("FN_MONTO"))*Double.parseDouble(listaRegistros.getString("FN_PORC_DESCUENTO"))/100;	
			listaRegistros.setObject("MONTO_VALUADO", ""+montoValuado);
			listaRegistros.setObject("MONTO_DESCONTAR", ""+montoDescontar);
			if(estatus.equals("2") || estatus.equals("23") || estatus.equals("34") ){
				montoCapitalInt = Double.parseDouble(listaRegistros.getString("MONTO_CREDITO"))+Double.parseDouble(listaRegistros.getString("MONTO_TASA_INT"));
				listaRegistros.setObject("MONTO_CAPITAL_INT", ""+montoCapitalInt);
			}
		}catch(Exception e){
			log.warn("Error al calcular el montoValuado, montoDescontar y montoCapitalInt: " + e);
		}
		//-------------------- Calculo los totales --------------------
		if((listaRegistros.getString("IC_MONEDA")).equals("1")){
			totalDoctosMN++;
			totalMontoMN += Double.parseDouble(listaRegistros.getString("FN_MONTO"));
			totalMontoValuadoMN += montoValuado; 
			if(estatus.equals("2") || estatus.equals("23") || estatus.equals("34")){
				totalMontoCreditoMN += Double.parseDouble(listaRegistros.getString("MONTO_CREDITO"));
				totalMontoDescuentoMN += montoDescontar;
				totalMontoInteresesMN += Double.parseDouble(listaRegistros.getString("MONTO_TASA_INT"));
			}
		}else{
			totalDoctosUS++;
			totalMontoUS += Double.parseDouble(listaRegistros.getString("FN_MONTO"));
			totalMontoValuadoUS += montoValuado; 
			if(estatus.equals("2") || estatus.equals("23") || estatus.equals("34")){
				totalMontoCreditoUS += Double.parseDouble(listaRegistros.getString("MONTO_CREDITO"));
				totalMontoDescuentoUS += montoDescontar;
				totalMontoInteresesUS += Double.parseDouble(listaRegistros.getString("MONTO_TASA_INT"));
			}
		}
	}
	
	if (informacion.equals("consultaData_2") || informacion.equals("consultaData_21") || informacion.equals("consultaData_4_22_24") ||  informacion.equals("consultaData_22")){ 	//Llena los grids
		
		consulta = listaRegistros.getJSONData();
		
		registrosStr = "{\"success\": true, \"tituloGrid\": \"" + tituloGrid + "\", \"registros\": " + consulta+"}";
		jsonObj = JSONObject.fromObject(registrosStr);	
		infoRegresar = jsonObj.toString();  		
	} else if(informacion.equals("consultaTotal")){
		StringBuffer consultaTotales = new StringBuffer();
		if(estatus.equals("2") || estatus.equals("23") || estatus.equals("34")){
			consultaTotales.append("[");
			if( totalDoctosMN > 0 ){
				consultaTotales.append("{\"TOTALES\":\"Total MN\",\"TOTAL_DOCTOS\":\""+totalDoctosMN+"\",\"TOTAL_MONTO\":\""+
										 totalMontoMN+"\",\"TOTAL_MONTO_VALUADO\":\""+totalMontoValuadoMN+"\",\"TOTAL_MONTO_CREDITO\":\""+
										 totalMontoCreditoMN+"\",\"TOTAL_MONTO_DESCUENTO\":\""+totalMontoDescuentoMN+"\",\"TOTAL_MONTO_INTERESES\":\""+
										 totalMontoInteresesMN+"\"}");
			}	
			if(totalDoctosMN > 0 && totalDoctosUS > 0 ){
				consultaTotales.append(",");
			}
			if( totalDoctosUS > 0 ){
				consultaTotales.append("{\"TOTALES\":\"Total USD\",\"TOTAL_DOCTOS\":\""+totalDoctosUS+"\",\"TOTAL_MONTO\":\""+
										 totalMontoUS+"\",\"TOTAL_MONTO_VALUADO\":\""+totalMontoValuadoUS+"\",\"TOTAL_MONTO_CREDITO\":\""+
										 totalMontoCreditoUS+"\",\"TOTAL_MONTO_DESCUENTO\":\""+totalMontoDescuentoUS+"\",\"TOTAL_MONTO_INTERESES\":\""+
										 totalMontoInteresesUS+"\"}");	
			}
			consultaTotales.append("]");
		} else{
		consultaTotales.append("[");
			if( totalDoctosMN > 0 ){
				consultaTotales.append("{\"TOTALES\":\"Total MN\",\"TOTAL_DOCTOS\":\""+totalDoctosMN+"\",\"TOTAL_MONTO\":\""+totalMontoMN+
										  "\",\"TOTAL_MONTO_VALUADO\":\""+totalMontoValuadoMN+"\"}");
			} 
			if(totalDoctosMN > 0 && totalDoctosUS > 0 ){
				consultaTotales.append(",");
			}
			if( totalDoctosUS > 0 ){
				consultaTotales.append("{\"TOTALES\":\"Total USD\",\"TOTAL_DOCTOS\":\""+totalDoctosUS+"\",\"TOTAL_MONTO\":\""+totalMontoUS+
								        "\",\"TOTAL_MONTO_VALUADO\":\""+totalMontoValuadoUS+"\"}");
			}	
			consultaTotales.append("]");
		}
		registrosStr = "{\"success\": true, \"tituloGrid\": \"" + tituloGrid + "\", \"registros\": " + consultaTotales.toString()+"}";
		jsonObj = JSONObject.fromObject(registrosStr);	
		infoRegresar = jsonObj.toString(); 								
	}
} else if(informacion.equals("imprimir") || informacion.equals("generarArchivo")){ 	//Genera el archivo PDF y CSV

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	paginador.setOpcionEstatus(estatus);
	paginador.setTituloTabla(tituloGrid);
	if(informacion.equals("imprimir")){
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}	
	} else if(informacion.equals("generarArchivo")){
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}	
	}	
	infoRegresar = jsonObj.toString();	
}
%>
<%=infoRegresar%>
