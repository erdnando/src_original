Ext.onReady(function() {

	
	
	var folioSolicitud =  [];
	var ic_estatus =  [];
	var huboCambios =  [];
	var elementos =  0;
	var causasRechazo =  [];
	
	var limpia =  function() {  
		folioSolicitud = [];
		ic_estatus = [];
		huboCambios = [];
		causasRechazo = [];
		elementos=0;
	}
	
	//concliye
	function transmiteCaptura(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
			
				var fp = Ext.getCmp('forma');
				fp.el.unmask();
				
				Ext.MessageBox.alert('Mensaje',jsonData.mensaje,
				function(){
					window.location = '24forma6ext.jsp';
				});
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
   
	
	//**********Funcion de Validaciones  al seleccionar los registros 
	function procesoTerminaCap() {	
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		var columnModelGrid = gridConsulta.getColumnModel();
		
		limpia(); //limpia las variables 
		var numRegistro = -1;
		var errorValidacion = false;
					
		store.each(function(record) {		
			numRegistro = store.indexOf(record);
			
			folioSolicitud.push(record.data['FOLIO_SOLICITUD']);			
			ic_estatus.push(record.data['IC_ESTATUS']);
			if(record.data['IC_ESTATUS']!=0){
				huboCambios.push('S');
			}else  {
				huboCambios.push('N');
			}
			if(record.data['IC_ESTATUS']==6){			
				if(record.data['CAUSA_RECHAZO'] ==''){	
					 errorValidacion = true;
					Ext.MessageBox.alert('Mensaje Validaci�n','Indique la(s) causa(s) de rechazo"',
						function(){
							gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('CAUSA_RECHAZO'));
						}
					);
					return false ;				
				}
			}	
			causasRechazo.push(record.data['CAUSA_RECHAZO']);
			elementos++;			
			
		});
		
		if (errorValidacion) {
			return;
			
		}else  {				
			fp.el.mask('Procesando...', 'x-mask-loading');	
			Ext.getCmp('btnTerminarCap').disable();	
			Ext.Ajax.request({
				url: '24forma6.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: "TerminaCaptura",
					folioSolicitud:folioSolicitud,
					ic_estatus:ic_estatus,
					huboCambios:huboCambios,
					causasRechazo:causasRechazo,
					elementos:elementos					
				}),
				callback: transmiteCaptura
			});		
		
		}
			
	}
	
	
	//Combos del Criterio de Busqueda 
	
	var catalogoEstatusAsignar = new Ext.data.JsonStore({
		id: 'catalogoEstatusAsignar',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '24forma6.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatusAsignar'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	
	//para la creacion del combo en el grid
	var comboEstatusAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catalogoEstatusAsignar
	});

	
	Ext.util.Format.comboRenderer = function(comboEstatusAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var valor = registro.data['IC_ESTATUS'];					
			if(valor !=''){
				var record = comboEstatusAsignar.findRecord(comboEstatusAsignar.valueField, value);
				return record ? record.get(comboEstatusAsignar.displayField) : comboEstatusAsignar.valueNotFoundText;
			} 
			if(valor !=''){
				return valor;
			}         
		}
	}	
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsulta.getColumnModel();
						
			if(store.getTotalCount() > 0) {			
				el.unmask();				
				Ext.getCmp('btnTerminarCap').enable();	
			} else {			
				Ext.getCmp('btnTerminarCap').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24forma6.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [
			{	name: 'PRODUCTO'},	
			{	name: 'FOLIO_SOLICITUD'},	
			{	name: 'NOMBRE_IF'},	
			{	name: 'NOMBRE_PYME'},	
			{	name: 'NOMBRE_EPO'},	
			{	name: 'TIPO_SOLICITUD'},	
			{	name: 'FECHA_SOLICITUD'},
			{	name: 'FECHA_AUTORIZACION_IF'},
			{	name: 'MONTO_AUTORIZADO'},	
			{	name: 'SALDO_DISPONIBLE'},	
			{	name: 'BANCO_SERVICIO'},	
			{	name: 'DISTRIBUIDOR_CUENTA'},	
			{	name: 'IF_CUENTA'},	
			{	name: 'IC_ESTATUS'},	
			{	name: 'CAUSA_RECHAZO'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});	
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		hidden: true,	
		clicksToEdit: 1,			
		columns: [			
			{
				header: 'Producto',
				tooltip: 'Producto',
				dataIndex: 'PRODUCTO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Folio Solicitud',
				tooltip: 'Folio Solicitud',
				dataIndex: 'FOLIO_SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'IF Relacionada',
				tooltip: 'IF Relacionada',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Nombre del PYME',
				tooltip: 'Nombre del PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'EPO Relacionada',
				tooltip: 'EPO Relacionada',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Tipo Solicitud',
				tooltip: 'Tipo Solicitud',
				dataIndex: 'TIPO_SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'			
			},
			{
				header: 'Fecha Solicitud',
				tooltip: 'Fecha Solicitud',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'			
			},
			{
				header: 'Fecha Autorizaci�n IF',
				tooltip: 'Fecha Autorizaci�n IF',
				dataIndex: 'FECHA_AUTORIZACION_IF',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'			
			},
			{
				header: 'Monto Autorizado',
				tooltip: 'Monto Autorizado',
				dataIndex: 'MONTO_AUTORIZADO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
				header: 'Saldo Disponible',
				tooltip: 'Saldo Disponible',
				dataIndex: 'SALDO_DISPONIBLE',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
				header: 'Banco de Servicio',
				tooltip: 'Banco de Servicio',
				dataIndex: 'BANCO_SERVICIO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Distribuidor - Cuenta',
				tooltip: 'Distribuidor - Cuenta',
				dataIndex: 'DISTRIBUIDOR_CUENTA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'IF - Cuenta',
				tooltip: 'IF - Cuenta',
				dataIndex: 'IF_CUENTA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},			
			{
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'IC_ESTATUS',
				sortable: true,
				resizable: true,
				width: 200,					
				align: 'left',				
				editor:comboEstatusAsignar,
				renderer: Ext.util.Format.comboRenderer(comboEstatusAsignar)				
			},			
			{
				header : 'Ingresar causas de rechazo', 
				tooltip: 'Ingresar causas de rechazo',
				dataIndex : 'CAUSA_RECHAZO', 
				fixed:true,
				sortable : false,	
				width : 300,
				hiddeable: false,
				editor:{
					xtype:'textarea',
					id: 'txtArea',
					maxLength: 260,
					enableKeyEvents: true,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 254){
								field.setValue((field.getValue()).substring(0,999));
								Ext.Msg.alert('Observaciones','La causas de rechazo no debe sobrepasar los 254 caracteres.');								
								field.focus();
							}
						}
					}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,	
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Terminar Captura',					
					tooltip:	'Terminar Captura',
					iconCls: 'icoAceptar',
					id: 'btnTerminarCap',
					handler: procesoTerminaCap						
				}				
			]
		}				
	});
			
	
	
	//***************Criterios de busqueda  ****************************
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma6.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma6.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoPYME = new Ext.data.JsonStore({
		id: 'catalogoPYME',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma6.data.jsp',
		baseParams: {
			informacion: 'catalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var elementosForma  =[	
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO Relacionada',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {
					
						catalogoIF.load({
							params: {
								ic_epo:combo.getValue()
							}				
						});					
					}
				}
			}			
		},
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'ic_if1',
			hiddenName : 'ic_if',
			fieldLabel: 'IF Relacionada',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catalogoIF,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {
					
						var ic_epo = Ext.getCmp("ic_epo1").getValue();
					
						catalogoPYME.load({
							params: {
								ic_if:combo.getValue(),
								ic_epo:ic_epo
							}				
						});					
					}
				}
			}				
		},
		{
			xtype: 'combo',
			name: 'ic_pyme',
			id: 'ic_pyme1',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Nombre PYME',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catalogoPYME,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'N�m de solicitud',
			name: 'ic_linea',
			id: 'ic_linea1',
			allowBlank: true,
			maxLength: 10,
			width: 200,
			msgTarget: 'side',
			margins: '0 20 0 0'			
		}
	];
		
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		monitorValid: true,
		title: 'Autorizaci�n de L�neas de Cr�dito Distribuidor',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		buttons: [		
			{
				text: 'Consultar',
				id: 'btnConsultar',				
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{ 
						
						})
					});
					
				}
				
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',				
				iconCls: 'icoLimpiar',
				handler: function(boton, evento) {
					window.location = '24forma6ext.jsp';				
				}
				
			}		
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	
	catalogoEPO.load();
	catalogoEstatusAsignar.load();
	
	
});
