Ext.onReady(function() {

	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);

	/**
	Se ejecuta cada vez que se oprime el bot�n consultar. Oculta los grids para 
	que se ejecuten desde cero, y guarda el valor del combo tipo de cretido en un
	campo oculto para que en la recarga y paginaci�n utilice este campo oculto y 
	evite confusiones si se cambia el valor del combo. La consulta de totales se 
	ejecuta una vez cada vez que se oprime el bot�n "Consultar" y no cada vez que
	se recarga el grid principal
	*/
	function ocultaGrid(){
		Ext.getCmp("gridConsulta").hide();
		Ext.getCmp("gridConsultaTotales").hide();		
		Ext.getCmp('tipo_cred_respaldo1').setValue(Ext.getCmp('tipo_credito1').getValue());
	}
	
	//----------------------Descargar Archivos--------------------------
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnImprimir').enable();
		Ext.getCmp('btnImprimir').setIconClass('icoPdf');
		Ext.getCmp('btnGenerarCSV').enable();
		Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
	}

	var procesarCatalogoTipoCredito = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var tipo_credito = Ext.getCmp('tipo_credito1');
				Ext.getCmp('tipo_credito1').setValue("");
				if(tipo_credito.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					tipo_credito.setValue("");			
				}	
			} else {
			}
		}	else{
		}
  };

	//---------------Proceso para el combo Tipo de Solicitud ---------------
	var procesarCatalogoTipoSolicitud = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var tipo_solicitud = Ext.getCmp('tipo_solicitud1');
			Ext.getCmp('tipo_solicitud1').setValue("");
			if(tipo_solicitud.getValue()==""){
				var newRecord = new recordType({
					clave:"",
					descripcion:"Seleccione..."
				});							
				store.insert(0,newRecord);
				store.commitChanges();
				tipo_solicitud.setValue("");			
			}						
		}
  };

	//--------------Proceso para el combo box EPO---------------
	var procesarCatalogoEPO = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var epo = Ext.getCmp('epo1');
				Ext.getCmp('epo1').setValue("");
				if(epo.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					epo.setValue("");			
				}	
			} 
		}
  };
 
	//--------------Proceso para el combo box IF---------------
	var procesarCatalogoIF = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var ic_if = Ext.getCmp('ic_if1');
				Ext.getCmp('ic_if1').setValue("");
				if(ic_if.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					ic_if.setValue("");			
				}	
			} 
		}
  };
  
	//--------------Proceso para el combo box Cliente---------------
	var procesarCatalogoCliente = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var cliente = Ext.getCmp('cliente1');
				Ext.getCmp('cliente1').setValue("");
				if(cliente.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					cliente.setValue("");			
				}	
			}
		}
  };
  
	//--------------Proceso para el combo box Estatus---------------  
  	var procesarCatalogoEstatus = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var estatus = Ext.getCmp('estatus1');
				Ext.getCmp('estatus1').setValue("");
				if(estatus.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					estatus.setValue("");			
				}	
			} else {
			}
		}	else{
		}
  };
  
	//-------------------- Se crea el store para el combo Tipo de Credito --------------------  
	var catalogoTipoCredito = new Ext.data.JsonStore({
		id: 'catalogoTipoCredito',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'catalogoTipoCredito'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
		load: procesarCatalogoTipoCredito,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	//-------------------- Se crea el store para el combo Tipo de Solicitud --------------------  
	var catalogoTipoSolicitud = new Ext.data.JsonStore({
		id: 'catalogoTipoSolicitud',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'catalogoTipoSolicitud'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
		load: procesarCatalogoTipoSolicitud,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});	
		
	//--------------- Se crea el store para el combo box "EPO"----------------
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
			load: procesarCatalogoEPO,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
	//--------------- Se crea el store para el combo box "IF"----------------
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
			load: procesarCatalogoIF,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});	
	
	//--------------- Se crea el store para el combo box Cliente----------------
	var catalogoCliente = new Ext.data.JsonStore({
		id: 'catalogoCliente',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'catalogoCliente'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
			load: procesarCatalogoCliente,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});	
  
	//-------------------- Se crea el store para el combo Tipo de Estatus --------------------  
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
		load: procesarCatalogoEstatus,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	//------------Generaci�n de la Consula -------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();				
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {		
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0) {
				gridConsulta.show();			
				el.unmask();
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarCSV').enable();
				consultaTotales.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'consultarTotales',
						start:0,
						limit:15
					})
				});				
			} else {		
				gridConsulta.show();
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarCSV').disable();				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'consultar'		
		},
		fields: [
			{	name: 'FOLIO_SOLICITUD'}, 					//FOLIO SOLICITUD	
			{	name: 'TIPO_CREDITO'},						//TIPO CREDITO 
			{	name: 'TIPO_SOLICITUD'}, 					//TIPO SOLICITUD	
			{	name: 'FOLIO_SULICITUD_RELACIONADA'}, 	//FOLIO DE SULICITUD RELACIONADA	
			{	name: 'NUM_EPO'}, 							//NUM EPO	
			{	name: 'EPO'}, 									//EPO	
			{	name: 'NUM_IF'}, 								//NUMERO IF
			{	name: 'IF'}, 									//IF
			{	name: 'NUM_DISTRIBUIDOR'}, 				//NUMERO DISTRIBUIDOR
			{	name: 'DISTRIBUIDOR'}, 						//DISTRIBUIDOR
			{	name: 'FECHA_SOLICITUD'}, 					//FECHA_SOLICITUD	
			{	name: 'FECHA_AUTORIZACION'}, 				//FECHA_AUTORIZACION	
			{	name: 'MONEDA'}, 								//MONEDA	
			{	name: 'ESTATUS'}, 							//ESTATUS	
			{	name: 'CAUSA_RECHAZO'}, 					//CAUSAS_RECHAZO	
			{	name: 'PLAZO'}, 								//PLAZO
			{	name: 'FECHA_VENCIMIENTO'}, 				//FECHA_VENCIMIENTO
			{	name: 'FINANCIERA'}, 						//CLIENTE BANCO...
			{	name: 'EPO_CLIENTE_NUM_CUENTA'}, 		//EPO_CLIENTE	
			{	name: 'IF_NUM_CUENTA'}, 					//IF_CUENTA
			{	name: 'MONTO_AUTORIZADO'}, 				//MONTO_AUTORIZADO
			{	name: 'SALDO_DISPONIBLE'} 					//SALDO_DISPONIBLE	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
				Ext.apply(options.params, {
					//tipo_credito: Ext.getCmp("tipo_credito1").getValue()
					tipo_credito: Ext.getCmp("tipo_cred_respaldo1").getValue()
				});
			}},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store:        consultaData,
		id:           'gridConsulta',
		margins:      '20 0 0 0',
		style:        'margin:0 auto;',
		title:        'Consulta',	
		clicksToEdit: 1,
		hidden:       true,
		columns: [{
			header:    'Folio de solicitud',
			tooltip:   'Folio de solicitud',
			dataIndex: 'FOLIO_SOLICITUD',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Tipo de cr�dito',
			tooltip:   'Tipo de credito',
			dataIndex: 'TIPO_CREDITO',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Tipo de solicitud',
			tooltip:   'Tipo de solicitud',
			dataIndex: 'TIPO_SOLICITUD',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Folio de solicitud relacionada',
			tooltip:   'Folio de solicitud relacionada',
			dataIndex: 'FOLIO_SULICITUD_RELACIONADA',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'N�mero de EPO',
			tooltip:   'Numero de EPO',
			dataIndex: 'NUM_EPO',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'EPO',
			tooltip:   'EPO',
			dataIndex: 'EPO',
			sortable:  true,
			resizable: true,
			align:     'left'
		},{
			header:    'N�mero de IF',
			tooltip:   'Numero de IF',
			dataIndex: 'NUM_IF',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'IF',
			tooltip:   'IF',
			dataIndex: 'IF',
			sortable:  true,
			resizable: true,
			align:     'left'
		},{
			header:    'N�mero de distribuidor',
			tooltip:   'Numero de distribuidor',
			dataIndex: 'NUM_DISTRIBUIDOR',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Distribuidor',
			tooltip:   'Distribuidor',
			dataIndex: 'DISTRIBUIDOR',
			sortable:  true,
			resizable: true,
			align:     'left'
		},{
			header:    'Fecha de solicitud',
			tooltip:   'Fecha de solicitud',
			dataIndex: 'FECHA_SOLICITUD',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Fecha de autorizaci�n',
			tooltip:   'Fecha de autorizacion',
			dataIndex: 'FECHA_AUTORIZACION',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Moneda',
			tooltip:   'Moneda',
			dataIndex: 'MONEDA',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Estatus',
			tooltip:   'Estatus',
			dataIndex: 'ESTATUS',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Causas del rechazo',
			tooltip:   'Causas del rechazo',
			dataIndex: 'CAUSA_RECHAZO',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Plazo',
			tooltip:   'Plazo',
			dataIndex: 'PLAZO',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Fecha de vencimiento',
			tooltip:   'Fecha de vencimiento',
			dataIndex: 'FECHA_VENCIMIENTO',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'Cliente - Banco Servicio',
			tooltip:   'Cliente - Banco Servicio',
			dataIndex: 'FINANCIERA',
			sortable:  true,
			resizable: true,
			align:     'left'
		},{
			header:    'EPO/Cliente - Num. cuenta',
			tooltip:   'EPO/Cliente - Num. cuenta',
			dataIndex: 'EPO_CLIENTE_NUM_CUENTA',
			sortable:  true,
			resizable: true,
			align:     'center'
		},{
			header:    'IF - Num. cuenta',
			tooltip:   'IF - Num. cuenta',
			dataIndex: 'IF_NUM_CUENTA',
			sortable:  true,
			resizable: true,
			align:     'left'
		},{
			header:    'Monto autorizado',
			tooltip:   'Monto autorizado',
			dataIndex: 'MONTO_AUTORIZADO',
			sortable:  true,
			resizable: true,
			align:     'right',
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header:    'Saldo disponible',
			tooltip:   'Saldo disponible',
			dataIndex: 'SALDO_DISPONIBLE',
			sortable:  true,
			resizable: true,
			align:     'right',
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		}],
		displayInfo:  true,
		emptyMsg:     'No hay registros.',
		loadMask:     true,
		stripeRows:   true,
		height:       400,
		width:        900,
		align:        'center',
		frame:        false,
		bbar: {
			xtype:       'paging',
			pageSize:    15,
			buttonAlign: 'left',
			id:          'barraPaginacion',
			displayInfo: true,
			store:       consultaData,
			displayMsg:  '{0} - {1} de {2}',
			emptyMsg:    'No hay registros.',
			items: [
				'->','-',
				{
					xtype:   'button',
					text:    'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id:      'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta02ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'generarArchivo'
							}),
							callback: descargaArchivo
						});
					}
				},
				'-',
				{
					xtype:   'button',
					text:    'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id:      'btnImprimir',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta02ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'imprimirTodo',
								start: 0,
								limit: 0
							}),
							callback: descargaArchivo
						});
					}
				}
/*
	Fodea 011-2015
	La opci�n de generar el PDF por paginaci�n queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el m�todo por si se requiere nuevamente 
	Fecha: 23/06/2015
	BY: Agust�n Bautista Ruiz
*/
/*
				{
					xtype:   'button',
					text:    'Imprimir',
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id:      'btnImprimir',
					handler: function(boton, evento) {
						var barraPaginacionA = Ext.getCmp("barraPaginacion");
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta02ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'imprimir',
								start: barraPaginacionA.cursor,
								limit: barraPaginacionA.pageSize
							}),
							callback: descargaArchivo
						});
					}
				}
*/
			]
		}
	});

	//------------Generaci�n de la Consula de totales-------------------------------
	var procesarConsultaTotales = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsultaTotales = Ext.getCmp('gridConsultaTotales');	
		var el = gridConsultaTotales.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsultaTotales.isVisible()) {
				gridConsultaTotales.show();
			}								
			if(store.getTotalCount() > 0) {					
				el.unmask();				
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'consultarTotales'			
		},
		hidden: true,
		fields: [					
			{	name: 'CD_NOMBRE'},
			{	name: 'TOTAL'}, 
			{	name: 'MONTO_TOTAL'}, 
			{	name: 'SALDO_TOTAL'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotales,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotales(null, null, null);					
				}
			}
		}					
	});
	
	var gridConsultaTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotales,
		id: 'gridConsultaTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Totales',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Tipo de Moneda',
				tooltip: 'Tipo de Moneda',
				dataIndex: 'CD_NOMBRE',
				sortable: true,
				width: 235,			
				resizable: true,					
				align: 'center'
			},
			{
				header: 'Total',
				tooltip: 'Total',
				dataIndex: 'TOTAL',
				sortable: true,
				width:  220,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Monto autorizado total',
				tooltip: 'Monto autorizado total',
				dataIndex: 'MONTO_TOTAL',
				sortable: true,
				width: 220,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Saldo disponible total',
				tooltip: 'Saldo disponible total',
				dataIndex: 'SALDO_TOTAL',
				sortable: true,
				width:  220,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],		
		loadMask: true,
		stripeRows: true,
		autoHeight: true,//150,
		width: 900,
		align: 'center',
		frame: false
	});	
				
	//-------------------- Se crean los elementos de la forma(combo y p�neles)  --------------------
	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'tipo_credito',	
			id: 'tipo_credito1',	
			fieldLabel: '&nbsp;&nbsp;Tipo de cr�dito', 
			mode: 'local',	
			hiddenName : 'tipo_credito',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars: 1,	
			allowBlank: false,
			store: catalogoTipoCredito,	
			displayField: 'descripcion',	
			valueField: 'clave',
			width: 331,		
			listeners: {
				select: {
					fn: function(combo) {
						if(combo.getValue()!=''){
							catalogoEPO.load({
								params: {
									tipo_credito:combo.getValue()
								}				
							});
							catalogoEstatus.load({
								params: {
									tipo_credito:combo.getValue()
								}				
							});
							Ext.getCmp("epo1").show();
							Ext.getCmp("ic_if1").hide();
							Ext.getCmp("cliente1").hide();
						}else  {
							Ext.getCmp("epo1").hide();
							Ext.getCmp("ic_if1").hide();
							Ext.getCmp("cliente1").hide();
						}
					}
				}
			}	
		},{
			xtype: 'combo',	
			name: 'tipo_solicitud',	
			id: 'tipo_solicitud1',	
			fieldLabel: '&nbsp;&nbsp;Tipo de solicitud', 
			mode: 'local',	
			hiddenName : 'tipo_solicitud',
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catalogoTipoSolicitud,	
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 331,			
			listeners: {
				select: {
					fn: function(combo) { }
				}
			}	
		},{
			xtype: 'combo',
			name: 'epo',
			id: 'epo1',
			hiddenName : 'epo',
			fieldLabel: '&nbsp;&nbsp;EPO',
			mode: 'local', 
			autoLoad: false,
			hidden:true,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccione ...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catalogoEPO,
			width: 331,		
			listeners: {
				select: {
					fn: function(combo) {
						if(combo.getValue()!= '' ){
							catalogoIF.load({
								params: {
									epo:combo.getValue()
								}				
							});	
							Ext.getCmp("ic_if1").show();
							if(Ext.getCmp("tipo_credito1").getValue() == 'C'){
								Ext.getCmp("cliente1").show();
							}	else{
								Ext.getCmp("cliente1").hide();
							}
						} else{
							Ext.getCmp("ic_if1").hide();
							Ext.getCmp("cliente1").hide();
						}
					}
				}
			}	
		},{
			xtype: 'combo',	
			name: 'ic_if',	
			id: 'ic_if1',	
			fieldLabel: '&nbsp;&nbsp;IF', 
			mode: 'local',	
			hiddenName: 'ic_if',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction: 'all',	
			typeAhead: true,
			minChars: 1,	
			store: catalogoIF,	
			hidden: true,
			displayField: 'descripcion',	
			valueField: 'clave',
			width: 331,			
			listeners: {
				select: {
					fn: function(combo) {
						if(combo.getValue()!= '' ){
							if(Ext.getCmp("tipo_credito1").getValue() == 'C'){
								catalogoCliente.load({
									params: {
										ic_if:combo.getValue(),
										epo:Ext.getCmp('epo1').getValue()
									}				
								});
							}
						} else {
							if(Ext.getCmp("tipo_credito1").getValue() == 'C'){
								Ext.getCmp("cliente1").setValue("");
							}
						}
					}
				}
			}	
		},{
			xtype: 'combo',	
			name: 'cliente',	
			id: 'cliente1',	
			fieldLabel: '&nbsp;&nbsp;Cliente', 
			mode: 'local',	
			hiddenName: 'cliente',	
			emptyText: 'Seleccione ... ',
			forceSelection: true,	
			triggerAction: 'all',	
			typeAhead: true,
			minChars : 1,	
			store: catalogoCliente,	
			hidden: true,
			displayField: 'descripcion',	
			valueField: 'clave',
			width: 331,			
			listeners: {
			}	
		},{
			xtype: 'combo',	
			name: 'estatus',	
			id: 'estatus1',	
			fieldLabel: '&nbsp;&nbsp;Estatus', 
			mode: 'local',	
			hiddenName : 'estatus',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catalogoEstatus,	
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 331,			
			listeners: {
			}	
		},{	
			width: 150,	
			fieldLabel: 'Tipo cred_respaldo',									
			hidden: true,			
			id: 'tipo_cred_respaldo1',	
			name: 'tipo_cred_respaldo'					
		},{
			xtype: 'compositefield',
			fieldLabel: '&nbsp;&nbsp;Fecha de autorizaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_autorizacion_de',
					id: 'fecha_autorizacion_de',
					hiddenName : 'fecha_autorizacion_de',	
					value: new Date(),
					allowBlank: true,
					startDay: 0,
					width: 145,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'fecha_autorizacion_a',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 16
				},
				{
					xtype: 'datefield',
					name: 'fecha_autorizacion_a',
					id: 'fecha_autorizacion_a',
					hiddenName : 'fecha_autorizacion_a',
					value: new Date(),	
					allowBlank: true,
					startDay: 1,
					width: 145,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fecha_autorizacion_de',
					margins: '0 20 0 0'  
				}
			]
		}	
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame	: true,
		width: 500,		
		autoHeight	: true,
		title: 'Criterios de b�squeda',				
		layout : 'form',
		style: 'margin:0 auto;',
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		monitorValid: true,		
		items: elementosForma,
		buttons: [		
			{
				text: 'Consultar',
				id: 'btnConsultar',				
				iconCls: 'icoBuscar',
				formBind: true,	
				handler: function(boton, evento) {
				if(Ext.getCmp("tipo_credito1").getValue() == ''){
					//Ext.Msg.alert('Error...', 'El campo Tipo de cr�dito es obligatorio.');
					Ext.getCmp('tipo_credito1').markInvalid('El campo es obligatorio');
				} else {
					if((Ext.getCmp("fecha_autorizacion_de").getValue() == '' && Ext.getCmp("fecha_autorizacion_a").getValue() != '') ||
						(Ext.getCmp("fecha_autorizacion_de").getValue() != '' && Ext.getCmp("fecha_autorizacion_a").getValue() == '')){
						Ext.Msg.alert('Error...', 'Ambos campos de fecha deben estar llenos o vacios.');
					}	else {
							ocultaGrid();
							fp.el.mask('Enviando...', 'x-mask-loading');			
							consultaData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar',
									informacion: 'consultar',
									start:0,
									limit:15
								})
							});
						}
					}//fin del else
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',				
				iconCls: 'icoLimpiar',
				handler: function(boton, evento) {
					window.location = '24consulta02ext.jsp';
				}
			}		
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridConsultaTotales
		]
	});

	catalogoTipoCredito.load();
	catalogoTipoSolicitud.load();
});