<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
	<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion 		= request.getParameter("informacion") 			== null?"" :(String)request.getParameter("informacion");
String ic_if 				= request.getParameter("ic_if") 					== null?"1":(String)request.getParameter("ic_if");
String cg_tipo_sol 		= request.getParameter("cg_tipo_sol") 			== null?"1":(String)request.getParameter("cg_tipo_sol");
String fn_limite_puntos = request.getParameter("fn_limite_puntos") 	== null?"1":(String)request.getParameter("fn_limite_puntos");
String cg_opera_tarjeta = request.getParameter("cg_opera_tarjeta") 	== null?"" :(String)request.getParameter("cg_opera_tarjeta");
String cg_linea_c = request.getParameter("cg_linea_c") 	== null?"" :(String)request.getParameter("cg_linea_c");
String datosGrid 			= request.getParameter("datos") 					== null?"" :(String)request.getParameter("datos");
String notifica_doctos       = request.getParameter("notifica_doctos")        == null?"N":(String)request.getParameter("notifica_doctos");
String email_notifica_doctos = request.getParameter("email_notifica_doctos")  == null?"" :(String)request.getParameter("email_notifica_doctos");
String infoRegresar 		= "",   mensaje ="";

JSONObject jsonObj   = new JSONObject();
Vector vecParametros = new Vector(); 	

ParametrosDist  BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
	
if ( informacion.equals("catalogoIF") ) {
	
	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");   
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	
	
} if ( informacion.equals("catalogoMoneda") ) {
	
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre"); 
	cat.setValoresCondicionIn("1,54", Integer.class);
	cat.setOrden("CLAVE"); 
	List elementos = cat.getListaElementos();
	JSONArray jsonArr = new JSONArray();
	Iterator it = elementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}

	infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	

	
} else if ( informacion.equals("ConsultaValores") ) {

	vecParametros 		= BeanParametros.getParamIF(ic_if);
	ic_if 				= (String)vecParametros.get(0);
	cg_tipo_sol 		= (String)vecParametros.get(1);
	fn_limite_puntos	= (String)vecParametros.get(2);
	cg_opera_tarjeta	= (String)vecParametros.get(3);
	cg_opera_tarjeta = cg_opera_tarjeta.trim();
	cg_linea_c       =(String)vecParametros.get(4);	
	notifica_doctos       =(String)vecParametros.get(5);
	email_notifica_doctos =(String)vecParametros.get(6);
	 
	
	jsonObj.put("success", 				new Boolean(true)	);	
	jsonObj.put("cg_opera_tarjeta", 	cg_opera_tarjeta	);
	jsonObj.put("cg_tipo_sol", 		cg_tipo_sol			);	
	jsonObj.put("fn_limite_puntos", 	fn_limite_puntos	);
	jsonObj.put("accion", 				"C"					);
	jsonObj.put("cg_linea_c", 	cg_linea_c.trim()		);
	jsonObj.put("notifica_doctos",       notifica_doctos.trim()      );
	jsonObj.put("email_notifica_doctos", email_notifica_doctos.trim());
	
	infoRegresar = jsonObj.toString();	
	
} else if( informacion.equals("consultaBinsIF") ){
	
	Registros registros 					= new Registros();
	JSONObject resultado 				= new JSONObject();
	OperaTarjetaIf paginador 			= new OperaTarjetaIf();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String cadena = "[{\"BIN\":\"\",\"IC_IF\":\"\",\"MONEDA\":\"\",\"DESCRIPCION\":\"\",\"CODIGO_BIN\":\"\",\"COMISION_APLICABLE\":\"\",\"ESTATUS\":\"ACTIVO\"}]";
	
	if( !ic_if.equals("") ) {
	
		paginador.setIcIf(Integer.parseInt(ic_if));
		registros = queryHelper.doSearch();
		if(registros.getNumeroRegistros() > 0){
			resultado.put("success", 	new Boolean(true));
			resultado.put("total", 		""+registros.getNumeroRegistros());
			resultado.put("registros", registros.getJSONData());
		} else {
			resultado.put("success", 	new Boolean(true));
			resultado.put("total", 	 	"1");
			resultado.put("registros", cadena);
		}
		
	} else {
	
		resultado.put("success", 	new Boolean(false));
		resultado.put("total", 	 	"0");
		resultado.put("registros", "");	
		resultado.put("mensaje", 	"Error al obtener los parámetros del IF. Los parámetros de búsqueda no pueden ser nulos");
	}
	
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("Valida_Grid") ){

	JSONArray arrayRegNuevo = JSONArray.fromObject(datosGrid);
	JSONObject resultado		= new JSONObject();
	List regExistente			= new ArrayList();
	List regNuevo 				= new ArrayList();
	StringBuffer binsRepet  = new StringBuffer();
	boolean valiado  			= false;
	int filasNuevas 	 		= 0;
	int camposVacios 			= 0;
	int tamanoCampo1 			= 0;
	int tamanoCampo2 			= 0;
	int gridVacio 	  			= 0;
	String msg       			= "";
	
	if(arrayRegNuevo.size() > 0){
		for(int i=0; i<arrayRegNuevo.size(); i++){
			JSONObject auxiliar = arrayRegNuevo.getJSONObject(i);
			if( auxiliar.getString("IC_IF").equals("") ){ // Es un renglon nuevo
				filasNuevas ++;
				if( auxiliar.getString("DESCRIPCION").equals("") || 
					 auxiliar.getString("CODIGO_BIN").equals("")  || 
					 auxiliar.getString("ESTATUS").equals("") ){
					 camposVacios ++;
				}
				if( auxiliar.getString("DESCRIPCION").length() >30 ){
					tamanoCampo1 ++;
				}
				if( auxiliar.getString("CODIGO_BIN").length() >6 ){
					tamanoCampo2 ++;
				}
				if( !auxiliar.getString("CODIGO_BIN").equals("") ){
					regNuevo.add(auxiliar.getString("CODIGO_BIN"));
				}
				
			} else { // Es un renglon existente
				regExistente.add(auxiliar.getString("CODIGO_BIN"));
			}
		}
	} else {
		gridVacio ++;
	}
	
	/***** Verifico que no se repitan los BINS nuevos con los existentes *****/
	if( regNuevo.size() > 0 && regExistente.size() > 0 ){
		for( int i = 0; i < regExistente.size(); i++ ){
			for( int j = 0; j < regNuevo.size(); j++ ){
				if( regExistente.get(i).equals(regNuevo.get(j)) ){
					binsRepet.append(regNuevo.get(j)+".<br>");
				}
			}
		}
	}

	String nombreTmp = "";
	/***** Verifico que no se repitan los BINS nuevos *****/
	if( regNuevo.size() > 0 ){
		for( int i = 0; i < regNuevo.size(); i++ ){
			nombreTmp = (String)regNuevo.get(i);
			if( regNuevo.size() >= i ){
				for( int j = i+1; j < regNuevo.size(); j++ ){
					if( nombreTmp.equals(regNuevo.get(j)) ){
						binsRepet.append(regNuevo.get(j)+".<br>");
					}
				}
			}
		}
	}

	/***** Verifico que no se repitan los BINS existentes *****/
	if( regExistente.size() > 0 ){
		for( int i = 0; i < regExistente.size(); i++ ){
			nombreTmp = (String)regExistente.get(i);
			if( regExistente.size() >= i ){
				for( int j = i+1; j < regExistente.size(); j++ ){
					if( nombreTmp.equals(regExistente.get(j)) ){
						binsRepet.append(regExistente.get(j)+".<br>");
					}
				}
			}
		}
	}

	/***** Envío la respuesta *****/
	if( gridVacio > 0 ){
		msg = "Debe ingresar al menos un Producto del IF en el Grid.";	
	} else if ( camposVacios > 0 ){
		msg = "Se encontraron inconsistencias con "+ camposVacios +" BIN(s) agregado(s). <br>"+
							"El campo DESCRIPCION es obligatorio.</br>"+
							"El campo CODIGO DEL PRODUCTO es obligatorio.</br>"+
							"El campo ESTATUS es obligatorio.";
	} else if( tamanoCampo1 > 1 ){
		msg = "El tamaño máximo para el campo DESCRIPCION es de 30 caracteres ";	
	} else if( tamanoCampo2 > 1 ){
		msg = "El tamaño máximo para el campo CODIGO_DEL_PRODUCTO es de 6 caracteres ";	
	} else if( !binsRepet.toString().equals("") ){
		msg = "El Código del Producto que se desea Registrar o Actualizar ya existe favor de Verificarlo: <br>" + binsRepet.toString();
	} else {
	 // Vaidación correcta
	 valiado = true;
	}
	resultado.put("success",	new Boolean(valiado)	);
	resultado.put("datosGrid",	arrayRegNuevo			);
	resultado.put("msg",			msg						);
	infoRegresar = resultado.toString();
	
} else if ( informacion.equals("Guardar_Datos") ){

	String ic_epos[]			= request.getParameterValues("ic_epo");
	String ic_epo_vals[]		= request.getParameterValues("ic_epo_val");
	String operaTarjeta 		= "N";
	int icNafin					= 0;
	boolean banderaGrid		= true;
	
	if( cg_opera_tarjeta.equals("true")){
		try{
			JSONArray arrayRegNuevo = JSONArray.fromObject(datosGrid);
			operaTarjeta = "S";
			OperaTarjetaIf operaTarjetaIf = new OperaTarjetaIf();
			operaTarjetaIf.setIcIf(Integer.parseInt(ic_if));
			operaTarjetaIf.setArrayRegNuevo(arrayRegNuevo);
			operaTarjetaIf.setClaveUsuario(iNoUsuario);
			operaTarjetaIf.setNombreUsuario(strNombreUsuario);
			icNafin = operaTarjetaIf.getIcNafinElectronico();
			if(icNafin>0){
				operaTarjetaIf.setNafinElectronico(icNafin);
				banderaGrid = operaTarjetaIf.insertaBinsIf();
			} else{
				log.info("Error al obtener el ic_nafin_electronico, se cancela el proceso");
				banderaGrid	= false;
			}
		} catch(Exception e){
			banderaGrid	= false;
			log.warn("Error en Guardar_Datos: " + e);
		}
	} else {
		banderaGrid	= true;
	}
	
	try{	
		if( banderaGrid	== true ){
			boolean bOkUpdate = BeanParametros.updateParametros(ic_if, cg_tipo_sol, ic_epos, ic_epo_vals, fn_limite_puntos, operaTarjeta, cg_linea_c.trim(), notifica_doctos.trim(), email_notifica_doctos.trim());
			if(bOkUpdate){
				mensaje = "Los Parametros se actualizaron satisfactoriamente";	
			}
		}else {
			mensaje = "Ocurrió un error al guardar los datos Bins del IF. <br> Los Parámetros no se actualizaron.";	
		}	
	}catch(NafinException ne){
		mensaje="ERROR "+ne.getMsgError();
		log.error("ERROR "+mensaje);		
	}catch(Exception e){
		mensaje="ERROR "+e.getMessage();
		log.error("ERROR "+ mensaje);
	}
	
	jsonObj.put("success",	new Boolean(true)	);	
	jsonObj.put("mensaje",	mensaje				);	
	jsonObj.put("ic_if", 	ic_if					);		
	jsonObj.put("accion",	"G"					);
	
	infoRegresar = jsonObj.toString();
	
}

%>

<%=infoRegresar%>