<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_epo = request.getParameter("ic_epo") == null?"":(String)request.getParameter("ic_epo");
String ic_if = request.getParameter("ic_if") == null?"":(String)request.getParameter("ic_if");
String ic_moneda = (request.getParameter("ic_moneda") == null)?"":(String)request.getParameter("ic_moneda");

String pRelMat 		= (request.getParameter("pRelMat") == null) ? "+" : request.getParameter("pRelMat");
String pPuntos 		= (request.getParameter("pPuntos") == null) ? "0" : request.getParameter("pPuntos");
String clavesTasa 		= (request.getParameter("clavesTasa") == null) ? "" : request.getParameter("clavesTasa");
String pAccion 		= (request.getParameter("pAccion") == null) ? "" : request.getParameter("pAccion");
String combo 		= (request.getParameter("combo") == null) ? "" : request.getParameter("combo");
if(ic_moneda.equals("") )  { ic_moneda = "1"; };

String Claves_a="",Valores="",Puntos="", lsMoneda="", lsTipoTasa="", lsCveTasa="", lsPlazo="", lsValor="",
lsRelMat="", lsPuntos="", lsTasaPiso="", lsFecha = "", existe ="N";	
double tasaAplicar = 0;
Vector lovTasas = null;


String infoRegresar ="", consulta ="", mensaje ="";
JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

CapturaTasas  BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class); 

if (informacion.equals("catalogoEPO") ) {
	
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setTipoCredito("D"); 
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("catalogoIF") ) {

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveEpo(ic_epo); 
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("validaDatosMoneda") ) {
	
	lovTasas = BeanTasas.ovgetTasasxEpo(4,ic_epo,ic_if,ic_moneda,"");
	int liRegistros  =lovTasas.size();
	
	jsonObj.put("success", new Boolean(true));		
	jsonObj.put("liRegistros",String.valueOf(liRegistros));	
	jsonObj.put("combo",combo);	
	infoRegresar = jsonObj.toString(); 		

} else if (informacion.equals("validaPuntos") ) {
	String maxpuntos = "";	
	int  totalLineas = BeanTasas.getTotalLineas("4",ic_epo,ic_if,"54");
	
	lovTasas = BeanTasas.ovgetTasasxEpo(4,ic_epo,ic_if,ic_moneda,"");
	int  liRegistros = lovTasas.size();
	
	try {
		maxpuntos = BeanTasas.getPuntosMaximosxIf("4",ic_if);
	} catch(NafinException ne) {
		mensaje = ne.getMsgError();
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("maxpuntos",maxpuntos);	
	jsonObj.put("mensaje",mensaje);	
	jsonObj.put("totalLineas",String.valueOf(totalLineas));	
	jsonObj.put("liRegistros",String.valueOf(liRegistros));	
	jsonObj.put("ic_if",ic_if);	
	jsonObj.put("combo",combo);		
	infoRegresar = jsonObj.toString(); 	

} else if (informacion.equals("catalogoMoneda") ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	


} else if (informacion.equals("Consultar") ) {
	
	lovTasas = BeanTasas.ovgetTasasxEpo(4,ic_epo,ic_if,ic_moneda,"");	
	if(lovTasas.size()==0) {
		lovTasas = BeanTasas.getTasasDispxEpo("4",ic_epo,ic_if,ic_moneda);
		existe = "S";
	}	
	for (int i=0; i<lovTasas.size(); i++) {
		Vector lovDatosTasa = (Vector)lovTasas.get(i);
		lsCveTasa	= lovDatosTasa.get(2).toString();
		lsMoneda	= lovDatosTasa.get(0).toString();
		lsTipoTasa	= lovDatosTasa.get(1).toString();
		lsPlazo 	= lovDatosTasa.get(3).toString();
		lsValor 	= lovDatosTasa.get(4).toString();
		if (existe.equals("N")) {
			lsFecha 	= lovDatosTasa.get(8).toString();
		}	
		
		if (existe.equals("S")) {		
			if ("+".equals(pRelMat)) {
				tasaAplicar = Double.parseDouble(lsValor) + Double.parseDouble(pPuntos);
			}else	if ("-".equals(pRelMat)) {
					tasaAplicar = Double.parseDouble(lsValor) - Double.parseDouble(pPuntos);
			}else if ("*".equals(pRelMat)) {
						tasaAplicar = Double.parseDouble(lsValor) * Double.parseDouble(pPuntos);
			}else  {
				tasaAplicar = Double.parseDouble(lsValor) / Double.parseDouble(pPuntos);
			}
			lsRelMat = pRelMat;
			lsPuntos = pPuntos;
			lsTasaPiso = ""+tasaAplicar;
			
		} else{
			lsRelMat = lovDatosTasa.get(5).toString();
			lsPuntos = lovDatosTasa.get(6).toString();
			lsTasaPiso = lovDatosTasa.get(7).toString();
		}
		if (i==0) {
			Claves_a = lsCveTasa;
			Valores = lsValor;
		}else {
			Claves_a += ","+lsCveTasa;
			Valores += ","+lsValor;			
		}
			
		datos = new HashMap();
		datos.put("IC_MONEDA",ic_moneda);
		datos.put("MONEDA",lsMoneda);
		datos.put("TIPO_TASA",lsTipoTasa);
		datos.put("PLAZO",lsPlazo);
		datos.put("VALOR",Comunes.formatoDecimal(Double.parseDouble(lsValor),2));
		datos.put("REL_MAT",lsRelMat);
		datos.put("PUNTOS_ADICIONALES",lsPuntos);
		datos.put("TASA_APLICAR",lsTasaPiso);			
		datos.put("FECHA_ACTUALIZACION",lsFecha);			
		registros.add(datos);
		
		
	}//for
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("existe",existe);	
	jsonObj.put("clavesTasa",Claves_a);	
	jsonObj.put("pAccion",pAccion);		
	infoRegresar = jsonObj.toString(); 		

} else if (informacion.equals("Agregar_Modificar_Tasas")  ) {

	boolean OK = false;		
	try {
	
		OK = BeanTasas.setTasasxEpo(ic_epo,ic_if,pAccion,4,clavesTasa,pRelMat,pPuntos);
	
	} catch(NafinException ne) {
		mensaje = ne.getMsgError();
	
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", mensaje);	
	infoRegresar = jsonObj.toString(); 			

} 

%>
<%=infoRegresar%>