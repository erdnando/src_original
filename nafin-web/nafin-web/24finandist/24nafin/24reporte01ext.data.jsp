<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	com.netro.pdf.*,
	net.sf.json.JSONArray,	
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="/appComun.jspf" %>
<%//PARAMETROS DE LA PAGINA ACTUAL
	String	claveEstatus=	(request.getParameter("catalogoEstatus")==null)?"":request.getParameter("catalogoEstatus");
	String 	claveDocto=(request.getParameter("claveDocumento")==null)?"":request.getParameter("claveDocumento");
	String 	estatusSolic			= "";//recibira el valor del  claveEstatus.substring(1,ic_estatus.length());
	String	estatusDocto	= "";//recibira el valor del  claveEstatus.substring(0,1);
	
	int  start= 0, limit =0;
	
	String nombreArchivo="";
%>


<%
	if(!"".equals(claveEstatus)){
		estatusDocto = claveEstatus.substring(1,claveEstatus.length());
		estatusSolic		 = claveEstatus.substring(0,1);
	}
	
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String operacion = (request.getParameter("operacion") == null) ? ""   : request.getParameter("operacion");
	
	
	String infoRegresar = "", consulta ="";
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray(); 
	JSONObject jsonObj = new JSONObject();
	
	RepDoctoSolicxEstatusDistBean ejb=new RepDoctoSolicxEstatusDistBean();	
	
	ConsDoctosSolicXEstatusDist paginador = new ConsDoctosSolicXEstatusDist(ejb);
		paginador.setEstatusSolic(estatusSolic);
		paginador.setEstatusDocto((estatusDocto));
		paginador.setClaveDocto(claveDocto);

if (informacion.equals("CatalogoEstatus") ) {
	/*
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_docto");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_docto");
	cat.setValoresCondicionIn("3,11,22,20,24", Integer.class);
	//cat.setOrden("1");
	List lista = cat.getListaElementos();
	
	CatalogoSimple cat_b = new CatalogoSimple();
	cat_b.setCampoClave("ic_estatus_solic");
	cat_b.setCampoDescripcion("cd_descripcion");
	cat_b.setTabla("comcat_estatus_solic");
	cat_b.setValoresCondicionIn("1,2,3,4", Integer.class);
	//cat.setOrden("1");
	List lista_b = cat_b.getListaElementos();

	JSONArray jsonArr = new JSONArray();

	Iterator it = lista.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			String clave = ec.getClave();
			ec.setClave("D"+clave);
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}

	Iterator it_b = lista_b.iterator();
	while(it_b.hasNext()) {
		Object obj = it_b.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			String clave = ec.getClave();
			ec.setClave("C"+clave);
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}

	infoRegresar = "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

	
	*/
	

	List catEstatus= paginador.getEstatusData();
	jsonObj.put("registros", catEstatus);	
   infoRegresar = jsonObj.toString();	
}else  if (informacion.equals("Consultar")  ){//||  informacion.equals("ResumenTotales")  ) {
	int i=Integer.parseInt(estatusDocto);
	if(!estatusDocto.equals("") && !estatusSolic.equals("")){
		ejb.setEstatusDocto(Integer.parseInt(estatusDocto));
		ejb.setEstatusSolic(estatusSolic);
	}
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	if (operacion.equals("Generar") ){
		try {
			Registros reg	=	queryHelper.doSearch();
			//if(reg.getJSONData()!=null){
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}	
	} else if (operacion.equals("ArchivoPDF")) {
		try {
			try {
				nombreArchivo = queryHelper.getCreateCustomFile(request,  strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}				
	} else if (operacion.equals("ArchivoCSV")) {
		try {
			 nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}		
	}
	infoRegresar=jsonObj.toString();
}
if(informacion.equals("ConsultaDetalles")){
			Registros regDet	=	paginador.getDetallesDocumentQuery();
			consulta	=	"{\"success\": true, \"total\": \""	+	regDet.getNumeroRegistros() + "\", \"registros\": " + regDet.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar=jsonObj.toString();	
}
if ( informacion.equals("ResumenTotales")  ) {

			Registros reg	=	paginador.getTotalesPorEstatus();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar=jsonObj.toString();			
}

//javax.swing.JOptionPane.showMessageDialog(null,"informacion: "+informacion+"\nOperacion "+operacion+"\nInforRegresa: "+infoRegresar);
%>
<%=infoRegresar%>

