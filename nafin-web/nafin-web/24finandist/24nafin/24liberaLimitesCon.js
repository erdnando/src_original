Ext.onReady(function(){
	
	var documentos = [];
	var valores = []; 
	
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: false,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: 'Captura',			
				id: 'btnCaptura',					
				handler: function() {
					window.location = '/nafin/24finandist/24pki/24nafin/24liberaLimites.jsp';
				}
			},		
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',		id: 'btnConsulta',					

				text: '<h1><b>Consulta</1></b>',			
						handler: function() {
					window.location = '/nafin/24finandist/24nafin/24liberaLimitesCon.jsp';
				}
			}	
		]
	});
	
	//-----------------------   HANDLER'S		--------------------------------
	
	var procesarConsultaRegistros = function(store, registros, opts){
		var jsonData = store.reader.jsonData;
		var titulo = jsonData.titulo;
		var forma = Ext.getCmp('forma');
		forma.el.unmask();

		if (registros != null) {

			var grid  = Ext.getCmp('grid');

			if (!grid.isVisible()) {
				grid.show();
			}

			//Ext.getCmp('barraPaginacion').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				if(titulo=='C')
					grid.setTitle('Liberaci�n de l�mites por Distribuidor');
				else if(titulo=='D')
					grid.setTitle('Liberaci�n de l�mites por EPO');
				Ext.getCmp('btnXls').enable();
				el.unmask();
			} else {
				Ext.getCmp('btnXls').disable();
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}
	}
	
	function procesarSuccessFailureArchivoCSV(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			boton = Ext.getCmp('btnXls');
			boton.setIconClass('icoXls');
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	//-----------------------  MAQUINA DE EDOS.		---------------------------
	
	var accionConsulta = function(estadoSiguiente, respuesta){

		 if(  estadoSiguiente == "CONSULTAR" 											){
			bandera = true;
			var fechaMin = Ext.getCmp('fechaIni');
			var fechaMax = Ext.getCmp('fechaFin');
			if(fechaMin.getValue()!=""){
				var fechaDe = Ext.util.Format.date(fechaMin.getValue(),'d/m/Y');
				var fechaA = Ext.util.Format.date(fechaMax.getValue(),'d/m/Y');
				if(!isdate(fechaDe)){
					fechaMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}else if(!isdate(fechaA)){
					fechaMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}
			}
				
			if(bandera){
				var forma = Ext.getCmp("forma");
				forma.el.mask('Buscando...','x-mask-loading');
	
				// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
				Ext.getCmp("grid").hide(); 
				Ext.StoreMgr.key('registrosConsultados').load({
										params:	Ext.apply(fp.getForm().getValues(),
												{
													informacion: 'consultaDatos',
													pantalla: 'Con'
												}
											)
				});
			}
		} else if(	estadoSiguiente == "LIMPIAR"){
				Ext.getCmp('forma').getForm().reset();
				grid.hide();
		}
	}
	
	//-----------------------   STORE'S		---------------------------------
	
	var catalogoEpo = new Ext.data.JsonStore({
		id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '/nafin/24finandist/24pki/24nafin/24liberaLimites.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '/nafin/24finandist/24pki/24nafin/24liberaLimites.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var catalogoDist = new Ext.data.JsonStore({
		id: 'catalogoDist',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '/nafin/24finandist/24pki/24nafin/24liberaLimites.data.jsp',
		baseParams: {
			informacion: 'catalogoDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	//store para grid de estatus servicio
	var consultaData = new Ext.data.JsonStore({
		id: 'registrosConsultados',
		root: 'registros',
		url : '/nafin/24finandist/24pki/24nafin/24liberaLimites.data.jsp',
		baseParams: {
			informacion: 'consultaDatos'
		},
		fields: [
					{name: 'INT_FINAN'},
					{name: 'DISTRIBUIDOR'},
					{name: 'NO_DOCTO'},
					{name: 'TASA_FON'},
					{name: 'NO_PRESTAMO'},
					{name: 'MONEDA'},
					{name: 'MONTO_DESC'},
					{name: 'FEC_VEN'},
					{name: 'FEC_PRE'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaRegistros,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});
	
	//----------------------	COMPONENTES		---------------------------------
	
	//Grid de consulta
	var grid = new Ext.grid.GridPanel({
		id: 'grid',
		store: consultaData,
		height: 290,
		width: 920,
		hidden: true,
		//loadMask: true,
		margins: '20 0 0 0',
		align: 'center',
		loadMask: 	true,
		style: 'margin:0 auto',
		title: 'Liberaci�n de limites por Distribuidor',
		columns:[
			{
				header: 'Intermediario Financiero',
				tooltip: 'Nombre del IF',
				width: 200,
				dataIndex: 'INT_FINAN',
				sortable: true,
				resizable: true,
				align: 'left'
			},
			{
				header: 'Distribuidor',
				tooltip: 'Nombre del Distribuidor',
				width: 180,
				dataIndex: 'DISTRIBUIDOR',
				sortable: true,
				resizable: true,
				align: 'left'
			},			
			{
				header: 'N�mero de documento',
				tooltip: 'N�mero del documento',
				width: 128,
				dataIndex: 'NO_DOCTO',
				sortable: true,
				resizable: false,
				align: 'center'
			},{
				header: 'Tasa de Fondeo',
				tooltip: 'Tasa de fondeo',
				width: 100,
				dataIndex: 'TASA_FON',
				sortable: false,
				resizable: true,
				align: 'center',
				renderer:function(value,metadata,registro){    
					if(value =='0') {
						return '%';
					}else {
						return  Ext.util.Format.number(value, '0.0000%');
					}
				}
			},{
				header: 'N�mero de Prestamo',
				tooltip: 'N�mero de prestamo',
				width: 115,
				dataIndex: 'NO_PRESTAMO',
				sortable: false,
				resizable: true,
				align: 'center',
				renderer:function(value,metadata,registro){   
					if(value =='0') {
						return '';
					}else {
						return value;
					}
				}
			},{
				header: 'Moneda',
				tooltip: 'Tipo de moneda',
				width: 120,
				dataIndex: 'MONEDA',
				sortable: false,
				resizable: true,
				align: 'center'
			},{
				header: 'Monto a Descontar',
				tooltip: 'Monto a descontar',
				width: 120,
				dataIndex: 'MONTO_DESC',
				sortable: true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Fecha de Vencimiento del Cr�dito',
				tooltip: 'Fecha del vencimiento del cr�dito',
				width: 205,
				dataIndex: 'FEC_VEN',
				sortable: false,
				resizable: true,
				align: 'center'
			},{
				header: 'Fecha de Prepago',
				tooltip: 'Fecha de �Prepago',
				width: 165,
				dataIndex: 'FEC_PRE',
				sortable: false,
				resizable: true,
				align: 'center'
			}
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnXls',
					iconCls: 'icoXls',
					handler: function(boton, evento){
									boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url : '/nafin/24finandist/24pki/24nafin/24liberaLimites.data.jsp',
											params:	Ext.apply(fp.getForm().getValues(),
														{
															informacion:'ArchivoCSV',
															pantalla: 'Con'
														}
											),
											callback: procesarSuccessFailureArchivoCSV
										});
					}
				},
				'-'
			]
		}

	});
	
	var elementosForma  = [		
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la EPO',
			id: '_ic_epo',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,
			width: 200,
			store: catalogoEpo,
			listeners: {
					select: {
						fn: function(combo) {
							Ext.getCmp('_ic_nae_dis').reset();
							Ext.getCmp('_ic_if').reset();
							catalogoIF.load({
								params: {
									ic_epo: combo.getValue()
								}
							});
							
							catalogoDist.load({
								params: {
									ic_epo: combo.getValue()
								}
							});
						}
					}
			}			
		},{
			xtype: 'combo',
			fieldLabel: 'Distribuidor',
			id: '_ic_nae_dis',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_nae_dis',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,
			width: 200,
			store: catalogoDist
			},
			{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			id: '_ic_if',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_if',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,
			width: 200,
			store: catalogoIF
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento del Cr�dito',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaIni',
					id: 'fechaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaFin',
					margins: '0 20 0 0',
					formBind: true
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaFin',
					id: 'fechaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fechaIni',
					margins: '0 20 0 0',
					formBind: true
				}
			]
		},
		{ 	xtype: 'displayfield', hidden:true, id: 'strTipoUsuario', 	value: '' },
		{ 	xtype: 'displayfield', hidden:true, id: 'ic_pyme', 	value: '' }	
	];	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,
		title: 'Consulta Liberaci�n de L�mites',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,	  		
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
				handler: function(boton, evento) {
									var fechaIni = Ext.getCmp("fechaIni");
									var fechaFin = Ext.getCmp("fechaFin");
									if(Ext.isEmpty(fechaIni.getValue()) || Ext.isEmpty(fechaFin.getValue())){
										if(Ext.isEmpty(fechaIni.getValue())){
											fechaIni.markInvalid('Debe capturar ambas fechas');
											fechaIni.focus();
											return;
										}
										if(Ext.isEmpty(fechaFin.getValue())){
											fechaFin.markInvalid('Debe capturar ambas fechas');
											fechaFin.focus();
											return;
										}
										
									}
									
									if(!Ext.isEmpty(fechaIni.getValue()) || !Ext.isEmpty(fechaFin.getValue())){									
										
										var fechaIni_ = Ext.util.Format.date(fechaIni.getValue(),'d/m/Y');
										var fechaFin_ = Ext.util.Format.date(fechaFin.getValue(),'d/m/Y');
										
										
										if(datecomp(fechaIni_,fechaFin_)==1) {
											fechaIni.markInvalid("La fecha de este campo debe de ser anterior a."+fechaIni_);
											fechaIni.focus();
											return;
										}
						
									}
									
									var epo = Ext.getCmp('_ic_epo');
									var nae = Ext.getCmp('_ic_nae_dis');
									if(epo.getValue() == "")
										epo.markInvalid('Campo Obligatorio');
									else if(nae.getValue() == "")
										nae.markInvalid('Campo Obligatorio');
									else 
										accionConsulta("CONSULTAR", null)
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
								accionConsulta("LIMPIAR", null)
				}				
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	});
	
	catalogoEpo.load();
	
});