<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="/appComun.jspf" %>
<%//PARAMETROS

String numDocFinal         = (request.getParameter("numCredito")             == null) ? ""  : request.getParameter("numCredito");
String tipoCobroInteres    = (request.getParameter("catalogoTipoCobroInter") == null) ? ""  : request.getParameter("catalogoTipoCobroInter");
String tipoCredito         = (request.getParameter("catalogoTipoCredito")    == null) ? ""  : request.getParameter("catalogoTipoCredito");
String claveIF             = (request.getParameter("catalogoIF")             == null) ? ""  : request.getParameter("catalogoIF");
String moneda              = (request.getParameter("catalogoTipoMoneda")     == null) ? ""  : request.getParameter("catalogoTipoMoneda");
String estatus             = (request.getParameter("catalogoTipoEstatus")    == null) ? "5" : request.getParameter("catalogoTipoEstatus");
String montoMin            = (request.getParameter("montoMin")               == null) ? "0" :  (request.getParameter("montoMin").equals("")) ? "0" : request.getParameter("montoMin");
String montoMax            = (request.getParameter("montoMax")               == null) ? "0" :  (request.getParameter("montoMax").equals("")) ? "0" : request.getParameter("montoMax");
String fechaPagoMin        = (request.getParameter("fechaPagoMin")           == null) ? ""  : request.getParameter("fechaPagoMin");
String fechaPagoMax        = (request.getParameter("fechaPagoMax")           == null) ? ""  : request.getParameter("fechaPagoMax");
String fechaVencimientoMin = (request.getParameter("fechaVencimientoMin")    == null) ? ""  : request.getParameter("fechaVencimientoMin");
String fechaVencimientoMax = (request.getParameter("fechaVencimientoMax")    == null) ? ""  : request.getParameter("fechaVencimientoMax");
String informacion         = (request.getParameter("informacion")            == null) ? ""  : request.getParameter("informacion");
String operacion           = (request.getParameter("operacion")              == null) ? ""  : request.getParameter("operacion");

int start= 0, limit =0;
%>
<%
String infoRegresar = "", consulta ="";
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
JSONObject jsonObj = new JSONObject();
%>
<%

ConsPagosCredNafinDist paginador = new ConsPagosCredNafinDist();
if(informacion.equals("catalogoTipoInter")) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_TIPO_COBRO_INTERES");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("COMCAT_TIPO_COBRO_INTERES");
	catalogo.setValoresCondicionIn("1,2", Integer.class);
	infoRegresar = catalogo.getJSONElementos();

}else if( informacion.equals("catalogoIF") ) {
	List reg = paginador.getDatosCatalogoIFDist();
	/*CatalogoIF catalogo = new CatalogoIF();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setOrden("cg_razon_social");
	*/
	jsonObj.put("registros", reg);
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("catalogoMoneda")) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("CD_NOMBRE");
	catalogo.setTabla("comcat_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);

	infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("catalogoEstatus")) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_ESTATUS_SOLIC");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("COMCAT_ESTATUS_SOLIC");
	catalogo.setValoresCondicionIn("5,11", Integer.class);

	infoRegresar = catalogo.getJSONElementos();

}else if (informacion.equals("catalogoTC")){
	registros = new JSONArray();
	datos = new HashMap();
		datos.put("clave", "C");
		datos.put("descripcion", "Crédito en Cuenta Corriente");
		registros.add(datos);
	datos = new HashMap();
		datos.put("clave", "D");
		datos.put("descripcion", "Descuento y/o Factoraje");
		registros.add(datos);

	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

	jsonObj = JSONObject.fromObject(consulta);

	infoRegresar = jsonObj.toString();

}else if(informacion.equals("Consultar") || informacion.equals("ConsultarTotales") ||
			informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")) {
	
		paginador.setNumDocFinal(numDocFinal);
		paginador.setTipoCobroInter(tipoCobroInteres);
		paginador.setTipoCredito(tipoCredito);
		paginador.setClaveIF(claveIF);
		paginador.setMoneda(moneda);
		paginador.setEstatus(estatus);
		paginador.setMontoMin(montoMin);
		paginador.setMontoMax(montoMax);
		paginador.setFechaPagoInicio(fechaPagoMin);
		paginador.setFechaPagoFin(fechaPagoMax);
		paginador.setFechaVencInicio(fechaVencimientoMin);	
		paginador.setFechaVencFin(fechaVencimientoMax);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
		if(informacion.equals("Consultar")){
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));

			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		}
		if(informacion.equals("Consultar") ){
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
					consulta = queryHelper.getJSONPageResultSet(request,start,limit);
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
				//Thread.sleep(5000);
				jsonObj = JSONObject.fromObject(consulta);
			
		} else if(informacion.equals("ConsultarTotales")) {
			consulta = queryHelper.getJSONResultCount(request);
			jsonObj = JSONObject.fromObject(consulta);
			//Thread.sleep(2000);
		} else if(informacion.equals("ArchivoPDF") ){
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		} else if(informacion.equals("ArchivoCSV") ) {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}
/*
	Fodea 011-2015
	La opción de generar el PDF por paginación queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el método por si se requiere nuevamente 
	Fecha: 25/06/2015
	BY: Agustín Bautista Ruiz
*/
/*
 else if(informacion.equals("ArchivoPDF") ){
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
*/
	infoRegresar = jsonObj.toString();

}
%>
<%=infoRegresar%>

