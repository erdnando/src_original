<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.distribuidores.*,
		com.netro.exception.*,
		com.netro.seguridadbean.*,
		javax.naming.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>

<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
JSONObject jsonObj = new JSONObject();

if (informacion.equals("catalogoEpo")) {
	
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("consultaParametros")) {
	String cveEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
	String cveSeccion = (request.getParameter("cboSeccion")!=null)?request.getParameter("cboSeccion"):"";
	
	ParametrosDist parametrosDist = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
	List lstParams = parametrosDist.getParametrosEpo(cveEpo, cveSeccion);
	JSONArray jsonArray = new JSONArray();
	jsonArray = JSONArray.fromObject(lstParams);
	infoRegresar = "{\"success\": true, \"total\": "+ jsonArray.size()+" , \"registros\": " + jsonArray.toString() + " }";

}

%>

<%=infoRegresar%>