Ext.onReady(function() {

	//---------------------------------- HANDLERS -------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		Ext.getCmp('btnArchivoPDF').setIconClass('icoPdf');
		Ext.getCmp('btnArchivoCSV').setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsDetalle = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridDetalle = Ext.getCmp('gridDetalle');
		var el = gridDetalle.getGridEl();
		if (arrRegistros != null) {
			if (!gridDetalle.isVisible()) {
				gridDetalle.show();
			}
			var jsonData = store.reader.jsonData;
			var cm = gridDetalle.getColumnModel();
			gridDetalle.setTitle('<div align="center"><b> </div>');
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n cambio para este documento', 'x-mask');
			}
		}
	}
	var verDetalleCambios= function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var ventana = Ext.getCmp('VerDetalle');
		consDetalles.load({
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'consDetalles',
				tipoConsulta:'Detalle',
				ic_documento:registro.get('IC_DOCUMENTO')
			})
		});
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				width: 710,
				id:'VerDetalle',
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'destroy',
				closable:true,
				autoDestroy:true,
				items: [
					gridDetalle
				],
				title: 'Cambios del documento',
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',
					buttons: ['->',
						{
							xtype: 'button',
							text: 'Salir ',
							iconCls: 'icoLimpiar',
							id: 'btnSalir_1',
							handler: function(){
								Ext.getCmp('VerDetalle').destroy();
							}
						}
					]
				}
			}).show();
		}
	}

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Documento', colspan: 19, align: 'center'},
					{header: 'Datos del Financiamiento', colspan: 11, align: 'center'}
				]
			]
	});

	//------Consulta  procesarConsultaData_1   ------------------------
	var procesarConsultaData_1 = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta_1 = Ext.getCmp('gridConsulta_1');
		var el = gridConsulta_1.getGridEl();
		if (arrRegistros != null) {
			if (!gridConsulta_1.isVisible()) {
				gridConsulta_1.show();
			}
			var jsonData = store.reader.jsonData;
			var cm = gridConsulta_1.getColumnModel();
			if(jsonData.tipo_credito=='D'){
				gridConsulta_1.setTitle('<div align="center"><b>Datos Documento Inicial </div>');
			} else{
				gridConsulta_1.setTitle('<div align="center"><b>Documento </div>');
			}
			if(store.getTotalCount() > 0){
				Ext.getCmp('gridTotales_1').show();
				el.unmask();
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnGenerarArchivoCSV').enable();
				consTotales_1.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'consTotales_1'
					})
				});
			} else{
				Ext.getCmp('gridTotales_1').hide();
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('btnGenerarArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	function mostrarArchivoCSV(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivoCSV').enable();
		Ext.getCmp('btnGenerarArchivoCSV').setIconClass('icoXls');
	}

	function mostrarArchivoPDF(opts, success, response){
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

	//-------------------------------- STORES ----------------------------------
	var consTotales_1 = new Ext.data.JsonStore({
		root: 'registros',
		url: '24consulta4Ext.data.jsp',
		baseParams: {
			informacion:  'consTotales_1',
			tipoConsulta: '1'
		},
		fields: [
			{ name: 'MONEDA'           },
			{ name: 'SUM(NUMREGISTROS)'},
			{ name: 'SUM(MONTO)'       }
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});

	var consultaData_1 = new Ext.data.JsonStore({
		root: 'registros',
		url:  '24consulta4Ext.data.jsp',
		baseParams: {
			informacion:  'Consultar_1',
			tipoConsulta: '1'
		},
		fields: [
			{name: 'DF_FECHA_EMISION'   },
			{name: 'NOMBRE_DIST'        },
			{name: 'CC_ACUSE'           },
			{name: 'IG_NUMERO_DOCTO'    },
			{name: 'DF_CARGA'           },
			{name: 'DF_FECHA_VENC'      },
			{name: 'IG_PLAZO_DOCTO'     },
			{name: 'MONEDA'             },
			{name: 'FN_MONTO'           },
			{name: 'MONEDA_LINEA'       },
			{name: 'TIPO_CONVERSION'    },
			{name: 'PLAZO_DESC_DIAS'    },
			{name: 'TIPO_CAMBIO'        },
			{name: 'IG_PLAZO_DESCUENTO' },
			{name: 'FN_PORC_DESCUENTO'  },
			{name: 'MODO_PLAZO'         },
			{name: 'ESTATUS'            },
			{name: 'IC_MONEDA'          },
			{name: 'IC_DOCUMENTO'       },
			{name: 'NOMBRE_EPO'         },
			{name: 'TIPO_COBRANZA'      },
			{name: 'NOMBRE_IF'          },
			{name: 'MONEDA_LINEA'       },
			{name: 'FECHA_CAMBIO'       },
			{name: 'TIPO_CAMBIO_ESTATUS'},
			{name: 'CAUSA'              },
			{name: 'CG_VENTACARTERA'    }
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData_1,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData_1(null, null, null);
				}
			}
		}
	});

	var consDetalles = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta4Ext.data.jsp',
		baseParams: {
			informacion: 'consDetalles',
			tipoConsulta:'Detalle'
		},
		fields: [
			{name: 'FECHA_CAMBIO'          },
			{name: 'CAMBIO_ESTATUS'        },
			{name: 'F_EMISION_ANTERIOR'    },
			{name: 'F_EMISION_NUEVA'       },
			{name: 'MONTO_ANTERIOR'        },
			{name: 'MONTO_NUEVO'           },
			{name: 'F_VENCIMIENTO_ANTERIOR'},
			{name: 'F_VENCIMIENTO_NUEVO'   },
			{name: 'MODALIDAD_ANTERIOR'    },
			{name: 'MODALIDAD_NUEVO'       }
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsDetalle,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsDetalle(null, null, null);
				}
			}
		}
	});

	var catEPOData = new Ext.data.JsonStore({
		id: 'catEPOData',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '24consulta4Ext.data.jsp',
		baseParams: {
			informacion: 'catEPOData'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catPymeData = new Ext.data.JsonStore({
		id: 'catPymeData',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '24consulta4Ext.data.jsp',
		baseParams: {
			informacion: 'catPymeData'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catIFData = new Ext.data.JsonStore({
		id: 'catIFData',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '24consulta4Ext.data.jsp',
		baseParams: {
			informacion: 'catIFData'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catEstatusData = new Ext.data.JsonStore({
		id: 'catEstatusData',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '24consulta4Ext.data.jsp',
		baseParams: {
			informacion: 'catEstatusData'
		},
		totalProperty: 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catTipoCreditoData = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data: [
			['D','Descuento y/o Factoraje'],
			['C','Cr�dito en Cuenta Corriente']
		]
	});

	var catCobroInteresData = new Ext.data.JsonStore({
		id: 'catCobroInteresData',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '24consulta4Ext.data.jsp',
		baseParams: {
			informacion: 'catCobroInteresData'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catMonedaData = new Ext.data.JsonStore({
		id: 'catMonedaData',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '24consulta4Ext.data.jsp',
		baseParams: {
			informacion: 'catMonedaData'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catModalidadData = new Ext.data.JsonStore({
		id: 'catModalidadData',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '24consulta4Ext.data.jsp',
		baseParams: {
			informacion: 'catModalidadData'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	//-------------------------------- COMPONENTES -----------------------------------
		var gridDetalle = [{
		id:              'gridDetalle',
		xtype:           'grid',
		title:           '<div align="center"><b> Procesado .... </div> ',
		store:           consDetalles,
		style:           'margin:0 auto;',
		columns:[{
				header:    'Fecha del Cambio',
				tooltip:   'Fecha del Cambio',
				dataIndex: 'FECHA_CAMBIO',
				sortable:  true,
				width:     150,
				align:     'center'
		},{
				header:    'Cambio Estatus',
				tooltip:   'Cambio Estatus ',
				dataIndex: 'CAMBIO_ESTATUS',
				sortable:  true,
				width:     150,
				align:     'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
		},{
				header:    'Fecha Emisi�n Anterior',
				tooltip:   'Fecha Emisi�n Anterior',
				dataIndex: 'F_EMISION_ANTERIOR',
				sortable:  true,
				width:     150,
				align:     'center'
		},{
				header:    'Fecha Emision Nueva',
				tooltip:   'Fecha Emision Nueva',
				dataIndex: 'F_EMISION_NUEVA',
				sortable:  true,
				width:     150,
				align:     'center'
		},{
				header:    'Monto Anterior',
				tooltip:   'Monto Anterior',
				dataIndex: 'MONTO_ANTERIOR',
				sortable:  true,
				width:     150,
				align:     'right',				
				renderer:  function(value,metadata,registro){
					if(registro.data['MONTO_ANTERIOR'] ==0) {
						return '';
					}else {
						return Ext.util.Format.number(value, '$0,0.00');
					}
				}
		},{
				header:    'Monto Nuevo',
				tooltip:   'Monto Nuevo',
				dataIndex: 'MONTO_NUEVO',
				sortable:  true,
				width:     150,
				align:     'right',
				renderer:  function(value,metadata,registro){
				if(registro.data['MONTO_NUEVO'] ==0) {
						return '';
					}else {
						return Ext.util.Format.number(value, '$0,0.00');
					}
				}			
		},{
				header:    'Fecha Vencimiento Anterior',
				tooltip:   'Fecha Vencimiento Anterior',
				dataIndex: 'F_VENCIMIENTO_ANTERIOR',
				sortable:  true,
				width:     150,
				align:     'center'
		},{
				header:    'Fecha Vencimiento Nuevo',
				tooltip:   'Fecha Vencimiento Nuevo',
				dataIndex: 'F_VENCIMIENTO_NUEVO',
				sortable:  true,
				width:     150,
				align:     'center'
		},{
				header:    'Modalidad de Plazo Anterior',
				tooltip:   'Modalidad de Plazo Anterior',
				dataIndex: 'MODALIDAD_ANTERIOR',
				sortable:  true,
				width:     150,
				align:     'center'
		},{
				header:    'Modalidad de Plazo Nuevo',
				tooltip:   'Modalidad de Plazo Nuevo',
				dataIndex: 'MODALIDAD_NUEVO',
				sortable:  true,
				width:     150,
				align:     'center'
		}],
		stripeRows:      true,
		loadMask:        true,
		height:          200,
		width:           680,
		style:           'margin:0 auto;',
		frame:           true
	}];

	var gridTotales_1 = new Ext.grid.EditorGridPanel({	
		store: consTotales_1,
		id: 'gridTotales_1',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '<div align="center"><b> </div>',
		clicksToEdit: 1,
		hidden: true,		
		columns: [
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Total Documentos',
				tooltip: 'Total Documentos',
				dataIndex: 'SUM(NUMREGISTROS)',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'SUM(MONTO)',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 100,
		width: 900,
		align: 'center',
		frame: false		
	});

	var gridConsulta_1 = new Ext.grid.EditorGridPanel({
		store: consultaData_1,
		id: 'gridConsulta_1',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title: 'Documento',
		clicksToEdit: 1,
		hidden: true,
		columns: [
			{
				header:    'EPO',
				tooltip:   'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'left',
				renderer:  function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return  value;
				}
			},
			{
				header:    'Distribuidor',
				tooltip:   'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'left',
				renderer:  function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header:    'N�mero de documento inicial',
				tooltip:   'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'center'
			},
			{
				header:    'Num. acuse',
				tooltip:   'Num. acuse',
				dataIndex: 'CC_ACUSE',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'center'
			},
			{
				header:    'Fecha de emisi�n',
				tooltip:   'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'center'
			},
			{
				header:    'Fecha vencimiento',
				tooltip:   'Fecha vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'center'
			},
			{
				header:    'Fecha de publicaci�n',
				tooltip:   'Fecha de publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'center'
			},
			{
				header:    'Plazo docto.',
				tooltip:   'Plazo docto.',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'center'
			},
			{
				header:    'Moneda.',
				tooltip:   'Moneda',
				dataIndex: 'MONEDA',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'center'
			},
			{
				header:    'Monto',
				tooltip:   'Monto',
				dataIndex: 'FN_MONTO',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'right',
				renderer:  Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header:    'Plazo para descuento en dias.',
				tooltip:   'Plazo para descuento en dias',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'center',
				renderer:  function(value,metadata,registro){ 
					if(registro.data['IG_PLAZO_DESCUENTO']==0){
							return ' ';
						}else{
						 return value;
						}
					}
			},
			{
				header:    '% de descuento',
				tooltip:   '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'center',
				renderer:  Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header:    'Monto % de descuento',
				tooltip:   'Monto % de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'right',
				renderer:  function(value,metadata,registro){
						var auxMonto = registro.data['FN_MONTO'];
						var auxPorDescantar = registro.data['FN_PORC_DESCUENTO'];
						var auxmontodescantar = (auxMonto*auxPorDescantar)/100;
						return Ext.util.Format.number(auxmontodescantar, '$0,0.00');
				}
			},
			{
				header:    'Tipo conv.',
				tooltip:   'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'center',
				renderer:  function(value,metadata,registro){
					var icMoneda = Ext.getCmp('cmbMoneda');
					if((icMoneda==registro.data['MONEDA_LINEA'])||(registro.data['TIPO_CAMBIO']==1)) {
						return '';
					}else {
						return value;
					}
				}
			},
			{
				header:    'Tipo cambio',
				tooltip:   'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'right',
				renderer:  function(value,metadata,registro){
					var icMoneda = Ext.getCmp('cmbMoneda');
					if((icMoneda==registro.data['MONEDA_LINEA'])||(registro.data['TIPO_CAMBIO']==1)) {
						return '';
					}else {
						return Ext.util.Format.number(value, '$0,0.00');
					}
				}
			},
			{
				header:    'Monto valuado en Pesos',
				tooltip:   'Monto valuado en Pesos',
				dataIndex: 'MONEDA_LINEA',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'right',
				renderer:  function(value,metadata,registro){ 
					var monto= registro.data['FN_MONTO'];
					var tipoCambio= registro.data['TIPO_CAMBIO'];
					var icMoneda = Ext.getCmp('cmbMoneda');
					var montoValu = monto*tipoCambio;
					if((icMoneda==registro.data['MONEDA_LINEA'])||(registro.data['TIPO_CAMBIO']==1)) {
						return '';
					}else {
						return  Ext.util.Format.number(montoValu, '$0,0.00');
					}
				}
			},
			{
				header:    'Modalidad de plazo',
				tooltip:   'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'left',
				renderer:  function(value,metadata,registro){
					if(registro.data['CG_VENTACARTERA'] =='S') {
						return '';
					}else {
						return value;
					}
				}
			},
			{
				header:    'Tipo de cobranza',
				tooltip:   'Tipo de cobranza',
				dataIndex: 'TIPO_COBRANZA',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'left'
			},
			{
				header:    'Estatus',
				tooltip:   'Estatus',
				dataIndex: 'ESTATUS',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'left'
			},
			{
				xtype:     'actioncolumn',
				header:    'Cambios del docto',
				tooltip:   'Cambios del docto',
				width:     130,
				align:     'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoBuscar';
						}
						,handler: verDetalleCambios
					}
				]
			},
			{
				header:    'IF',
				tooltip:   'IF',
				dataIndex: 'NOMBRE_IF',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'left',
				renderer:  function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header:    'Tipo de cambio de estatus',
				tooltip:   'Tipo de cambio de estatus',
				dataIndex: 'TIPO_CAMBIO_ESTATUS',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'left'
			},
			{
				header:    'Fecha de cambio de estatus',
				tooltip:   'Fecha de cambio de estatus',
				dataIndex: 'FECHA_CAMBIO',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'left'
			},
			{
				header:    'Causa',
				tooltip:   'Causa',
				dataIndex: 'CAUSA',
				sortable:  true,
				width:     150,
				resizable: true,
				align:     'left'
			}
		],
		displayInfo:     true,
		emptyMsg:        'No hay registros.',
		loadMask:        true,
		stripeRows:      true,
		height:          400,
		width:           900,
		align:           'center',
		frame:           false,
		bbar: {
			xtype:        'paging',
			pageSize:     15,
			buttonAlign:  'left',
			id:           'barraPaginacion1',
			displayInfo:  true,
			store:        consultaData_1,
			displayMsg:   '{0} - {1} de {2}',
			emptyMsg:     'No hay registros.',
			items: [
				'->',
				{
					xtype:  'button',
					text:   'Generar Archivo',
					id:     'btnGenerarArchivoCSV',
					tooltip:'Imprime los registros en formato CSV.',
					iconCls:'icoXls',
					handler:function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url:'24consulta4Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
								informacion:'GenerarArchivoCSV',
								tipo:'CSV'
							}),
							callback: mostrarArchivoCSV
						});
					}
				},
				{
					xtype:  'button',
					text:   'Generar Todo',
					tooltip:'Imprime los registros en formato PDF.',
					iconCls:'icoPdf',
					id:     'btnGenerarPDF',
					hidden: true,
					handler:function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url:'24consulta4Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'PDF_Todo',
								start: 0,
								limit: 0,
								tipo:'PDF'
							}),
							callback: mostrarArchivoPDF
						});
					}
				}
/*
	Fodea 011-2015
	La opci�n de generar el PDF por paginaci�n queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el m�todo por si se requiere nuevamente 
	Fecha: 24/06/2015
	BY: Agust�n Bautista Ruiz
*/
/*
				{
					xtype:  'button',
					text:   'Imprimir',
					iconCls:'icoPdf',
					id:     'btnGenerarPDF',
					hidden: true,
					handler:function(boton, evento) {
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion1");
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url:'24consulta4Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'PDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize,
								tipo:'PDF'
							}),
							callback: mostrarArchivoPDF
						});
					}
				}
*/
			]
		}
	});

	var  elementosForma  = [
	{
			xtype: 'compositefield',
			fieldLabel: 'EPO',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'epo',
					id: 'cmbEpo',
					fieldLabel: 'EPO',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'epo',
					emptyText: 'Seleccione EPO...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catEPOData,
					width: 270,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners: {
						select:{ 
							fn:function (combo) {
								Ext.getCmp('ic_pyme1').setValue('');
								catPymeData.load({
									params: {
										epo:combo.getValue()
									}				
								});							
							}
						}
					}					
				},
				{
					xtype: 'displayfield',
					value: 'Modalidad de plazo',
					width: 100
				},
				{
					xtype: 'combo',	
					name: 'modalidadPlazo',	
					id: 'cmbModalidadPlazo',	
					fieldLabel: '', 
					mode: 'local',	
					hiddenName : 'modalidadPlazo',	
					emptyText: 'Seleccione modalidad de plazo ',
					forceSelection : true,	
					triggerAction : 'all',	
					typeAhead: true,
					minChars : 1,	
					store : catModalidadData,	
					displayField : 'descripcion',	
					valueField : 'clave',
					tpl : NE.util.templateMensajeCargaCombo,
					width: 270
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Distribuidor',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'ic_pyme',
					id: 'ic_pyme1',
					fieldLabel: 'Distribuidor',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_pyme',
					emptyText: 'Seleccione Distribuidor...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catPymeData,
					width: 270,
					tpl : NE.util.templateMensajeCargaCombo
				},
				{
					xtype: 'displayfield',
					value: 'Tipo de cambio de estatus',
					width: 100
				},
				{
					xtype: 'combo',	
					name: 'tipoEstatus',	
					id: 'cmbTipoEstatus',	
					fieldLabel: '', 
					mode: 'local',	
					hiddenName : 'tipoEstatus',	
					emptyText: 'Seleccione tipo de cambio de estatus ',
					forceSelection : true,	
					triggerAction : 'all',	
					typeAhead: true,
					minChars : 1,	
					store : catEstatusData,	
					displayField : 'descripcion',	
					valueField : 'clave',
					tpl : NE.util.templateMensajeCargaCombo,
					width: 270
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de cr�dito',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',	
					name: 'tipo_credito',	
					id: 'cmbCredito',	
					fieldLabel: '', 
					mode: 'local',	
					hiddenName : 'tipo_credito',	
					emptyText: 'Seleccione tipo de cr�dito ... ',
					forceSelection : true,	
					triggerAction : 'all',	
					typeAhead: true,
					minChars : 1,	
					store : catTipoCreditoData,	
					displayField : 'descripcion',	
					valueField : 'clave',
					width: 270
				},
				{
					xtype: 'displayfield',
					value: 'Fecha de cambio de estatus',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'fecha_seleccion_de',
					id: 'fecha_seleccion_de',
					allowBlank: true,
					startDay: 0,
					width: 100,
					value: new Date(),
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'fecha_seleccion_a',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',   
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_seleccion_a',
					id: 'fecha_seleccion_a',
					allowBlank: true,
					startDay: 1,
					width: 100,
					value: new Date(),
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fecha_seleccion_de',
					margins: '0 20 0 0'  
				}		
				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de documento inicial',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					fieldLabel: 'N�mero de documento inicial',
					name: 'documento',
					id: 'cmbDocumento',
					allowBlank: true,
					maxLength: 15,
					width: 100,
					msgTarget: 'side',				
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: '  ',
					width: 150
				},
				{
					xtype: 'displayfield',
					value: ' IF ',
					width: 100
				},
				{
					xtype: 'combo',
					name: 'if',
					id: 'cmbIf',
					fieldLabel: '',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'if',
					emptyText: 'Seleccione IF...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catIFData,
					width: 270,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha vencimiento docto',
			combineErrors: false,
			msgTarget: 'side',
			items: [			
				{
					xtype: 'datefield',
					name: 'fecha_vto_de',
					id: 'fecha_vto_de',
					fieldLabel: 'Fecha vencimiento docto',	
					allowBlank: true,
					startDay: 0,
					width: 105,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_vto_a',
					margins: '0 20 0 0' ,
					minValue: '01/01/1901'				
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_vto_a',
					id: 'fecha_vto_a',
					allowBlank: true,
					startDay: 1,
					width: 105,
					msgTarget: 'side',
					vtype: 'rangofecha',
					minValue: '01/01/1901',
					campoInicioFecha: 'fecha_vto_de',
					margins: '0 20 0 0'					
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de publicaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [			
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_de',
					id: 'fecha_publicacion_de',
					fieldLabel: 'Fecha de publicaci�n',	
					allowBlank: true,
					startDay: 0,
					width: 105,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_publicacion_a',
					margins: '0 20 0 0' ,
					minValue: '01/01/1901'				
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_a',
					id: 'fecha_publicacion_a',
					allowBlank: true,
					startDay: 1,
					width: 105,
					msgTarget: 'side',
					vtype: 'rangofecha',
					minValue: '01/01/1901',
					campoInicioFecha: 'fecha_publicacion_de',
					margins: '0 20 0 0'					
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'cmbMoneda',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione Moneda...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catMonedaData,
					width: 270,
					tpl : NE.util.templateMensajeCargaCombo
				}
				
			]
			
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Monto documento',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'fn_monto_de',
					id: 'fn_monto_de',
					fieldLabel: 'Monto documento',					
					allowBlank: true,
					startDay: 0,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 100,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					vtype:	'rangoBigamount',
					xtype: 'bigamount'
				},
				{
					xtype: 'displayfield',
					value: ' a',					 
					width: 20
				},				
				{
					xtype			: 'textfield',
					name			: 'fn_monto_a',
					id				: 'fn_monto_a',
					fieldLabel	: 'Monto entre',				
					allowBlank	: true,
					startDay		: 0,
					maxLength	: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width			: 100,
					msgTarget	: 'side',						
					margins		: '0 20 0 0',
					vtype			:	'rangoBigamount',
					xtype			: 'bigamount'
				}	
				
			]
		},
		{
			xtype			: 'checkbox',
			id				: 'monto_con_descuento1',
			name			: 'monto_con_descuento',
			hiddenName	: 'convenio',
			fieldLabel	: 'Monto % de descuento',
			tabIndex		: 29,
			enable 		: true
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 900,
		monitorValid: true,
		title: '<center>Criterios de B�squeda<center>',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 100,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {
					var fecha_seleccion_de  = Ext.getCmp('fecha_seleccion_de');
					var fecha_seleccion_a  = Ext.getCmp('fecha_seleccion_a');
					if(!Ext.isEmpty(fecha_seleccion_de.getValue()) || !Ext.isEmpty(fecha_seleccion_a.getValue())){
						if(Ext.isEmpty(fecha_seleccion_de.getValue())){
							fecha_seleccion_de.markInvalid('Debe capturar ambas fechas de cambio de estatus o dejarlas en blanco');
							fecha_seleccion_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_seleccion_a.getValue())){
							fecha_seleccion_a.markInvalid('Debe capturar ambas fechas de cambio de estatus o dejarlas en blanco');
							fecha_seleccion_a.focus();
							return;
						}
					}
					var fecha_vto_de  = Ext.getCmp('fecha_vto_de');
					var fecha_vto_a  = Ext.getCmp('fecha_vto_a');
					if(!Ext.isEmpty(fecha_vto_de.getValue()) || !Ext.isEmpty(fecha_vto_a.getValue())){
						if(Ext.isEmpty(fecha_vto_de.getValue())){
							fecha_vto_de.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fecha_vto_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_vto_a.getValue())){
							fecha_vto_a.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fecha_vto_a.focus();
							return;
						}
					}
					var fecha_publicacion_de  = Ext.getCmp('fecha_publicacion_de');
					var fecha_publicacion_a  = Ext.getCmp('fecha_publicacion_a');
					if(!Ext.isEmpty(fecha_publicacion_de.getValue()) || !Ext.isEmpty(fecha_publicacion_a.getValue())){
						if(Ext.isEmpty(fecha_publicacion_de.getValue())){
							fecha_publicacion_de.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');
							fecha_publicacion_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_publicacion_a.getValue())){
							fecha_publicacion_a.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');
							fecha_publicacion_a.focus();
							return;
						}
					}
					var fn_monto_de  = Ext.getCmp('fn_monto_de');
					var fn_monto_a  = Ext.getCmp('fn_monto_a');
					if(!Ext.isEmpty(fn_monto_de.getValue()) || !Ext.isEmpty(fn_monto_a.getValue())){
						if(Ext.isEmpty(fn_monto_de.getValue())){
							fn_monto_de.markInvalid('Debe capturar ambos montos o dejarlas en blanco');
							fn_monto_de.focus();
							return;
						}
						if(Ext.isEmpty(fn_monto_a.getValue())){
							fn_monto_a.markInvalid('Debe capturar ambos montos o dejarlas en blanco');
							fn_monto_a.focus();
							return;
						}
					}
					if(!Ext.isEmpty(fn_monto_de.getValue()) && !Ext.isEmpty(fn_monto_a.getValue())){
						if(parseFloat(fn_monto_de.getValue())>parseFloat(fn_monto_a.getValue())){					
							fn_monto_de.markInvalid('El monto  a no puede ser mayor que el monto de');
							fn_monto_de.focus();
							return;
						}
					}
					fp.el.mask('Procesando...', 'x-mask-loading');	
					consultaData_1.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar_1',
							tipoConsulta:'1',
							start:0,
							limit:15						
						})
					});		
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24consulta4Ext.jsp';					
				}
			}
		]
	
	});
	//-------------------------------- PRINCIPAL -----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta_1,
			gridTotales_1,
			NE.util.getEspaciador(20)
		]
	});
	catEPOData.load();
	catEstatusData.load();
	catCobroInteresData.load();
	catMonedaData.load();
	catModalidadData.load();
	catIFData.load();
});