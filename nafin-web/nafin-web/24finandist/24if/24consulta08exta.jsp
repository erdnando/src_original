<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj	=	new JSONObject();
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String	ic_epo		=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String	ic_pyme		=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String	fecha_ini	=	(request.getParameter("Txtfchini")==null)?"":request.getParameter("Txtfchini");
String	fecha_fin	=	(request.getParameter("Txtffin")==null)?"":request.getParameter("Txtffin");
String	tipoConsulta	=	(request.getParameter("tipoConsulta")==null)?"":request.getParameter("tipoConsulta");
String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");

String epo				= "";
String distribuidor	= "";
String numdocto		= "";
String acuse			= "";
String fchemision		= "";
String fchpublica		= "";
String fchvence		= "";
String plazodocto		= "";
String ic_moneda		= "";
String moneda			= "";
double monto			= 0;
String tipoconversion= "";
String tipocambio		= "";
double mntovaluado	= 0;
String plzodescto		= "";
String porcdescto		= "";
double mntodescuento	= 0;
String estatus			= "";
String numcredito		= "";
String descif			= "";
String referencia		= "";
String plazocred		= "";
String fchvenccred	= "";
String tipocobroint	= "";
String modoPlazo		= "";
double montoint		= 0;
String valortaza		= "";
double mntocredito	= 0;
String fchopera		= "";
String monedaLinea	= "";
int numreg = 0;
String tipoPago = "";

try {
	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;

	if (informacion.equals("ArchivoCSV")) {
		Registros reg = new Registros();
		AvNotifIfDist notif = new AvNotifIfDist();
		notif.setIc_if(iNoCliente);
		notif.setIc_epo(ic_epo);
		notif.setIc_pyme(ic_pyme);
		notif.setFechaInicial(fecha_ini);
		notif.setFechaFinal(fecha_fin);
		notif.setTipoConsulta(tipoConsulta);
		notif.setStrPerfil(strPerfil);
		notif.setNumOrden(numOrden);
      if(strPerfil.equals("IF FACT RECURSO")){
         notif.setTipoCredito("F");
      }else{
         notif.setTipoCredito("");
      }

		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(notif);
		reg = queryHelper.doSearch();
		/*******************
		Cabecera del archivo
		********************/
		contenidoArchivo.append(
					"EPO,Distribuidor,Numero de documento inicial,Num. acuse carga,Fecha de emisión,Fecha de publicación"+
					",Fecha vencimiento,Fecha de operación,Plazo docto.,Moneda,Monto,Plazo para descuento en dias,% de descuento"+
					",Monto % de descuento,Tipo conv.,Tipo cambio,Monto valuado,Modalidad de plazo,Estatus,Numero de documento final"+
					",Monto,Plazo,Fecha de vencimiento,Referencia tasa de interés,Valor tasa de interés,Monto tasa de interés,Tipo de cobro interés"	);
		/***************************
		 Generacion del archivo
		/***************************/
		while(reg.next()){
			ic_moneda		= (reg.getString("ic_moneda")==null)?"":reg.getString("ic_moneda");	
			distribuidor	= (reg.getString("DISTRIBUIDOR")==null)?"":reg.getString("DISTRIBUIDOR");
			numdocto			= (reg.getString("ig_numero_docto")==null)?"":reg.getString("ig_numero_docto");
			acuse				= (reg.getString("cc_acuse")==null)?"":reg.getString("cc_acuse");
			fchemision		= (reg.getString("df_fecha_emision")==null)?"":reg.getString("df_fecha_emision");
			fchpublica		= (reg.getString("df_fecha_publicacion")==null)?"":reg.getString("df_fecha_publicacion");		
			fchvence			= (reg.getString("df_fecha_venc")==null)?"":reg.getString("df_fecha_venc");
			plazodocto		= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
			moneda			= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
			monto				= Double.parseDouble(reg.getString("FN_MONTO"));
			tipoconversion	= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
			tipocambio		= (reg.getString("TIPO_CAMBIO")==null)?"":reg.getString("TIPO_CAMBIO");
			plzodescto		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
			porcdescto		= (reg.getString("fn_porc_descuento")==null)?"":reg.getString("fn_porc_descuento");
			mntodescuento	= Double.parseDouble(reg.getString("MONTO_DESCUENTO"));
			mntovaluado		= (monto-mntodescuento)*Double.parseDouble(tipocambio);
			estatus			= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
			if(!strPerfil.equals("IF FACT RECURSO")){
            modoPlazo		= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
         }
			monedaLinea		= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
			epo				= (reg.getString("EPO")==null)?"":reg.getString("EPO");
			/**********PARA CREDITO**************/
			numcredito		=(reg.getString("ic_documento")==null)?"":reg.getString("ic_documento");
			numcredito		=(reg.getString("ic_documento")==null)?"":reg.getString("ic_documento");
			descif			= (reg.getString("DIF")==null)?"":reg.getString("DIF");
			referencia		= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
			plazocred		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
			fchvenccred		= (reg.getString("DF_FECHA_VENC_CREDITO")==null)?"":reg.getString("DF_FECHA_VENC_CREDITO");
			tipocobroint	= (reg.getString("tipo_cobro_interes")==null)?"":reg.getString("tipo_cobro_interes");
			montoint			= Double.parseDouble(reg.getString("MONTO_INTERES"));
			valortaza		= (reg.getString("VALOR_TASA")==null)?"":reg.getString("VALOR_TASA");
			mntocredito		= Double.parseDouble(reg.getString("MONTO_CREDITO"));
			fchopera			= (reg.getString("df_fecha_hora")==null)?"":reg.getString("df_fecha_hora");
			// INICIO: FODEA 09-2015
			tipoPago = (reg.getString("IG_TIPO_PAGO")==null)?"":reg.getString("IG_TIPO_PAGO");
			if(tipoPago.equals("2")){
				plazocred = plazocred + " (M)";
			}
			// FIN: FODEA 09-2015
			numreg++;

			contenidoArchivo.append("\n" + epo.replace(',',' ')+","+distribuidor.replace(',', ' ') + "," + numdocto + "," + acuse + ","+ fchemision+","+fchpublica+","+fchvence+","+fchopera+","+ plazodocto + "," + moneda +
						", "+ Comunes.formatoDecimal(monto,2,false) + ", " + plzodescto + ","+ porcdescto + "," + Comunes.formatoDecimal(mntodescuento,2,false) + "," + ((ic_moneda.equals(monedaLinea))?"":tipoconversion) + 
						", " + ((ic_moneda.equals(monedaLinea))?"":tipocambio) + ", " + ((ic_moneda.equals(monedaLinea))?"":Comunes.formatoDecimal(mntovaluado,2,false))+ ","+modoPlazo +"," + estatus + 
						", "+ numcredito + ", "	+ Comunes.formatoDecimal(mntocredito,2,false) + ", " + plazocred + "," + fchvenccred + ","	+ referencia.replace(',', ' ') + 
						", "+ Comunes.formatoDecimal(valortaza,2,false) + ", "+ Comunes.formatoDecimal(montoint,2,false)  + "," + tipocobroint);
		}
		if (numreg == 0)	{
			contenidoArchivo.append("\nNo se Encontró Ningún Registro");
		}
	}
	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>