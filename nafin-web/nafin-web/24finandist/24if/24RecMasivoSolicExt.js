Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   

	//------------------------------ EXTRA - Function �S ----------------------------------------

	function procesarProcesar(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				grid.hide();
				Ext.getCmp('regArchivo').body.update('<div class="formas" align="center">'+infoR.regArchivo+'</div>');
				Ext.getCmp('regProcesados').body.update('<div class="formas" align="center">'+infoR.regProcesados+'</div>');
				Ext.getCmp('noProceso').body.update('<div class="formas" align="center">'+infoR.noProceso+'</div>');
				Ext.getCmp('nombreArchivo_b').body.update('<div class="formas" align="center">'+infoR.nombreArchivo_b+'</div>');
				Ext.getCmp('fechaCarga').body.update('<div class="formas" align="center">'+infoR.fechaCarga+'</div>');
				fpAcuse.show();
				Ext.getCmp('btnGenerarPdf').setHandler(
					function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24RecMasivoPdfExt.jsp',
							params:	{regArchivo:infoR.regArchivo,
										regProcesados:infoR.regProcesados,
										noProceso:infoR.noProceso,
										nombreArchivo:infoR.nombreArchivo_b,
										fechaCarga:infoR.fechaCarga
										},
							callback: procesarSuccessFailureGenerarPdf
						});
					}
				);
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function mostrarArchivoTXT(opts, success, response) {
		//Ext.getCmp('btnGenerarArchivoCSV').enable();
		//var btnImprimirCSV = Ext.getCmp('btnGenerarArchivoCSV');
		//btnImprimirCSV.setIconClass('icoXls');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPdf =  function(opts, success, response) {
		var btnGenerarPdf = Ext.getCmp('btnGenerarPdf');
		btnGenerarPdf.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPdf = Ext.getCmp('btnBajarPdf');
			btnBajarPdf.show();
			btnBajarPdf.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPdf.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPdf.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarErrores =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerar');
		btnGenerar.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajar');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	


	var gridLayoutData = new Ext.data.JsonStore({
		fields:	[{name: 'NOMBRE'},
					{name: 'DESCRIPCION'},
					{name: 'TIPO_CAMPO'},
					{name: 'LONGITUD'},
					{name: 'OBLIGATORIO'}],
		data:		[
					{'NOMBRE':'No. Documentos Final','DESCRIPCION':'N�mero de Documento Final con estatus Seleccionado Pyme (3)<br> � En Proceso de Autorizaci�n IF(24)','TIPO_CAMPO':'Num�rico','LONGITUD':'10','OBLIGATORIO':'S'},
					{'NOMBRE':'Estatus','DESCRIPCION':'Los documentos Seleccionada Pyme se pueden actualizar a:<br> Negociable(2) <br> Los documentos en Proceso de Autorizaci�n IF se pueden <br> actualizar a Negociable(2) � Seleccionada Pyme(3).','TIPO_CAMPO':'Num�rico','LONGITUD':'2','OBLIGATORIO':'S'},
					{'NOMBRE':'Causas','DESCRIPCION':'Causas por las que se cambia el estatus del documento','TIPO_CAMPO':'Alfanum�rico','LONGITUD':'255','OBLIGATORIO':'S'}
		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData = new Ext.data.JsonStore({
		fields:	[{name: 'TEXTO'},	{name: 'VALOR'}	],
		data:		[
					{'TEXTO':'N�mero de Registros del Archivo:','VALOR':''},
					{'TEXTO':'N�mero de Registros sin Errores:','VALOR':''},
					{'TEXTO':'N�mero de Registros con Errores:','VALOR':''}
		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var elementosAcuse = [
		{
			xtype: 'panel',layout:'table',	width:350,	border:true,	layoutConfig:{ columns: 2 }, style: 'margin:0 auto;',
			defaults: {frame:false, border: true,width:218, height: 30,bodyStyle:'padding:6px'},
			items:[
				{	frame:true,	html:'<div class="formas" align="left">N�mero de Registros del Archivo:</div>'	},
				{	height: 37,width:143,	id:'regArchivo',	html:'&nbsp;'	},
				{	frame:true,	html:'<div class="formas" align="left">N�mero de Registros Procesados:</div>'	},
				{	height: 37,width:143,	id:'regProcesados',	html:'&nbsp;'	}
			]
		},{
			xtype:'displayfield', value:''
		},{
			xtype: 'panel',layout:'table',	width:650,	border:true,	layoutConfig:{ columns: 3 }, style: 'margin:0 auto;',
			defaults: {frame:false, border: true,width:150, height: 35,bodyStyle:'padding:4px'},
			items:[
				{	frame:true,html:'<div class="formas" align="center">N�mero de Proceso</div>'	},
				{	width:300,frame:true,html:'<div class="formas" align="center">Nombre Archivo</div>'	},
				{	width:200,frame:true,html:'<div class="formas" align="center">Fecha Carga de Archivo</div>'	},
				{	id:'noProceso',	html:'&nbsp;'	},
				{	width:300,id:'nombreArchivo_b',	html:'&nbsp;'	},
				{	width:200,id:'fechaCarga',	html:'&nbsp;'}
			]
		}
	];

	var fpAcuse=new Ext.FormPanel({
		title:'<div align="center">Resumen proceso Cambio de Estatus Masivo</div>',
		style: 'margin:0 auto;',	height:'auto',	width:700, bodyStyle:'padding:10px',
		border:true,	frame:false,	items:elementosAcuse,	hidden:true,
		bbar:{
			items: [
				'->',
				{
					xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5
				},{
					text: 'Generar PDF',	id:'btnGenerarPdf',	iconCls: 'icoPdf'
				},{
					text: 'Bajar PDF',	id:'btnBajarPdf',	hidden:true
				},{
					xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5
				},{
					text: 'Salir',	iconCls: 'icoLimpiar',	handler:function() {window.location = '24RecMasivoSolicExt.jsp';}
				}
			]
		}
	});

	var grid = new Ext.grid.GridPanel({
		title:'<div align="center">Resumen de validaci�n</div>',
		id:'grid',store: consultaData,	columnLines:true, hidden:true,
		stripeRows:true,	loadMask:true,	height:150,	width:380,	style:'margin:0 auto;',	frame:false,
		columns: [
			{
				header:'',	dataIndex:'TEXTO',	width:255,	align:'left',	resizable:false,	sortable:false, hideable:false, menuDisabled:true
			},{
				header:'',	dataIndex:'VALOR',	width:120,	align:'center',resizable:false,	sortable : false,	hideable:false,	menuDisabled:true,
				renderer:function(value, metaData, record, rowIndex, colIndex, store) {
								switch(rowIndex) {
									case 1:
										value = '<div style="color:#66FF00"><b>'+value+'</b></div>';
										break;
									case 2:
										value = '<div style="color:#CC0000"><b>'+value+'</b></div>';
										break;
									default:
										value = '<div><b>'+value+'</b></div>';
								}
								return value;
							}
			}
		],
		bbar: {
			id:'barraGrid', height:50,
			items: [
				'->',{
					xtype:'button',	text:'Procesar',	id:'btnProcesar',	iconCls:'icoAceptar', scale:'large', hidden:true
				},{
					xtype:'button',	text:'Generar Archivo<br>con Errores',	id:'btnGenerar',	iconCls:'icoXls', scale:'large', hidden:true
				},{
					xtype:'button',	text:'Bajar Archivo<br>con Errores',	id:'btnBajar',	iconCls:'', scale:'large', hidden:true
				},{
					xtype:'button',	text:'Cancelar',	iconCls:'icoRechazar', scale:'large',
					handler: function(boton, evento) {
									window.location = '24RecMasivoSolicExt.jsp';
								}
				}
			]
		}
	});
	var procesaCargaMasivaValidacion = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var action = 	Ext.util.JSON.decode(response.responseText);
			// Procesar respuesta
			var infoRegresar = action.infoRegresar;
													var jsonData = consultaData.data.items;
													Ext.each(jsonData, function(item,index,arrItem){
															switch(index) {
																case 1:
																	item.data.VALOR = infoRegresar.sinErrores;
																	break;
																case 2:
																	item.data.VALOR = infoRegresar.conErrores;
																	if(infoRegresar.conErrores == "0" || Ext.isEmpty(infoRegresar.conErrores) ){
																		Ext.getCmp('btnProcesar').setVisible(true);
																		Ext.getCmp('btnProcesar').setHandler(
																			function(boton, evento) {
																				pnl.el.mask('Enviando...', 'x-mask-loading');
																				Ext.Ajax.request({
																					url: '24RecMasivoSolic.data.jsp',
																					params: {informacion:"Procesar",	
																								aprocesar:infoRegresar.aprocesar, 
																								totalRegistros:infoRegresar.totalRegistros,
																								nombreArchivo:infoRegresar.nombreArchivo
																								},
																					callback: procesarProcesar
																				});
																			}
																		);
																	}
																	if(infoRegresar.conErrores != "0"){
																		Ext.getCmp('btnGenerar').setVisible(true);
																		Ext.getCmp('btnGenerar').setHandler(
																			function(boton, evento) {
																				boton.disable();
																				boton.setIconClass('loading-indicator');
																				Ext.Ajax.request({
																					url: '24RecMasivoErrorExt.jsp',
																					params: {detalleErrores:infoRegresar.detalleErrores},
																					callback: procesarSuccessFailureGenerarErrores
																				});
																			}
																		);
																	}
																	break;
																default:
																	item.data.VALOR = infoRegresar.totalRegistros;
															}
													});
													fp.el.unmask();
													fp.hide();
													grid.show();
													grid.getView().refresh();
			
		}else{
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	var procesaCargaMasiva = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaMasiva(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaMasiva(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	var cargaMasiva = function(estado, respuesta ){
	if(			estado == "SUBIR_ARCHIVO"					){
			
		fp.getForm().submit({
			url: '24RecMasivoSolicExt.data.jsp',									
			waitMsg: 'Enviando datos...',
			waitTitle:'Procesando registros',
			success: function(form, action) {								
				procesaCargaMasiva(null,  true,  action.response );
			}
			,failure: NE.util.mostrarSubmitError
		})	
			
	} else if(	estado == "VALIDA_CARACTERES_CONTROL"				){
			
			var nombreArchivo	= respuesta.nombreArchivo;
			Ext.getCmp('nombArchivo').setValue(nombreArchivo);
			Ext.Ajax.request({
				url: 		'24RecMasivoSolicExtControl.data.jsp',
				params: 	{
					informacion:				'validaCaracteresControl',
					nombreArchivo: 	nombreArchivo
				},
				callback: 				procesaCargaMasiva
			});
			
		} else if(	estado == "MOSTRAR_AVANCE_THREAD"				){
			Ext.Ajax.request({
				url: 		'24RecMasivoSolicExtControl.data.jsp',
				params: 	{
					informacion:				'avanceThreadCaracteres'
				},
				callback: 				procesaCargaMasiva
			});
		
		}else if(	estado == "HAY_CARACTERES_CONTROL"				){
				var mensaje	= respuesta.mns;
				var nombreArchivoCaractEsp	= respuesta.nombreArchivoCaractEsp;
				Ext.getCmp('nombArchivoDetalle').setValue(nombreArchivoCaractEsp);
				fp.hide();
				caracteresEspeciales.show();
				caracteresEspeciales.setMensaje(mensaje);
		}else if(	estado == "ERROR_THREAD_CARACTERES"				){
			var mensaje	= respuesta.mns;
			Ext.Msg.alert("Error", mensaje );
			return;
		}else if(	estado == "VALIDA_CARGA_DOCTO"				){
			
			Ext.Ajax.request({
				url: 		'24RecMasivoSolicExtValida.data.jsp',
				params: 	{
					informacion:				'validaArchivoErrores',
					archivo: Ext.getCmp('nombArchivo').getValue()						
				},
				callback: 				procesaCargaMasivaValidacion
			});
		}else if(	estado == "DESCARGAR_ARCHIVO"				){
		
			var archivo = Ext.getCmp('nombArchivoDetalle').getValue();
			Ext.Ajax.request({
				url: 		'24RecMasivoSolicExtControl.data.jsp',
				params: 	{
					informacion:				'descargaArchivoDetalle',
					archivo : archivo
				},
				callback: 				mostrarArchivoTXT
			});
		}else if(	estado == "MENSAJE_CODIFICACION"								){
			new NE.cespcial.AvisoCodificacionArchivo({codificacion:respuesta.codificacionArchivo}).show();
 
		}
		return;
	}
	var elementosForma = [
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			defaults: {
				bodyStyle:'padding:6px'
			},
			items:	[
				{
					xtype: 'button',	columnWidth: .05,	autoWidth: true,	autoHeight: true,	iconCls: 'icoAyuda',
					handler: function() {
						if (!Ext.getCmp('gridLayout').isVisible()){
							Ext.getCmp('gridLayout').show();
						}
					}
				},{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 105,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'fileuploadfield',
							id: 'form_file',
							anchor: '95%',
							allowBlank: false,
							blankText:	'Debe seleccionar una ruta de archivo.',
							emptyText: 'Seleccione un archivo',
							fieldLabel: 'Cargar Archivo de',
							name: 'txtarchivo',
							regex:	/^.+\.([tT][xX][tT])$/,
							regexText:'El archivo debe tener extensi�n .txt',
							buttonCfg: {iconCls: 'upload-icon'},
							buttonText: ''
						}
					],
					buttons: [
						{
							text: 'Aceptar',	iconCls: 'correcto',
							handler: function(boton, evento) {
								var cargaArchivo = Ext.getCmp('form_file');
								if (!cargaArchivo.isValid()){
									cargaArchivo.focus();
									return;
								}
								cargaMasiva("SUBIR_ARCHIVO", null);
	
							}
						},{
							text: 'Cancelar',	iconCls: 'icoRechazar',
							handler: function() {
											fp.getForm().reset();
											if (Ext.getCmp('gridLayout').isVisible()){
												Ext.getCmp('gridLayout').hide();
											}
										}
						}
					]
				},{
					xtype: 'grid',	id: 'gridLayout', columnWidth: 1,
					title:'<div align="center">Layout de Cambio de Estatus Masivo</div>',
					store: gridLayoutData,	height: 200,	columnLines:true,	width: 730,	anchor: '100%',	frame: false,	hidden: true, style:'margin:0 auto;',
					stripeRows: true,
					columns: [
						new Ext.grid.RowNumberer(),
						{header: 'Nombre del Campo',tooltip: 'Nombre del Campo',	dataIndex: 'NOMBRE',	sortable : false, width : 125, align: 'left',resizable: false, hideable:false},
						{header: 'Descripci�n',tooltip: 'Descripci�n',	dataIndex: 'DESCRIPCION',	sortable : false, width : 335, align: 'left',resizable: true},
						{header: 'Tipo de Campo',tooltip: 'Tipo de Campo',	dataIndex: 'TIPO_CAMPO',	sortable : false, width : 90, align: 'left',resizable: false},
						{header: 'Longitud',tooltip: 'Longitud',	dataIndex: 'LONGITUD',	sortable : false, width : 70, align: 'center',resizable: false},
						{header: 'Obligatorio',tooltip: 'Obligatorio',	dataIndex: 'OBLIGATORIO',	sortable : false, width : 70, align: 'center',resizable: false,
							renderer: function(value, metaData, record, rowIndex, colIndex, store) {
											if(value=='S') {
												return '<img src="/nafin/14seguridad/Seguridad/gif/l_p.gif" width="10" height="10" border="0" alt="">';
											} else {
												return '<img src="/nafin/00utils/gif/rechazar.png" width="10" height="10" border="0" alt="">';
											}
							}
						}
					],
					tools: [
						{
							id: 'close',
							handler: function(evento, toolEl, panel, tc) {
								panel.hide();
							}
						}
					]
				},
				{  
				  xtype:	'hidden',  
				  name:	'nombArchivo',
				  id: 	'nombArchivo'
				},
				{  
				  xtype:	'hidden',  
				  name:	'nombArchivoDetalle',
				  id: 	'nombArchivoDetalle'
				}	
			]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		fileUpload: true,
		frame: true,
		width: 750,
		title: 'Cambio de Estatus Masivo',
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 4px',
		style: 'margin: 0 auto',
		items: elementosForma
	});
	var caracteresEspeciales = new NE.cespcial.CaracteresEspeciales(
		{
		hidden: true
		}
	);
	
	caracteresEspeciales.setHandlerAceptar(function(){
		caracteresEspeciales.hide(),
		fp.show();
	});
	
	caracteresEspeciales.setHandlerDescargarArchivo(function(){
		 cargaMasiva("DESCARGAR_ARCHIVO", null);
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,NE.util.getEspaciador(20),grid,fpAcuse,
			caracteresEspeciales
		]
	});

});