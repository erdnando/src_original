Ext.onReady(function() {
	
	//var dt = Ext.util.Format.date(Date(),'d/m/Y');

	var procesaCambiaEstatus = function (opts, success, response) {
		Ext.getCmp('btnGuardar').setIconClass('icoGuardar');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				Ext.Msg.show({
					title:	'',
					msg:		'Actualizaciones llevadas a cabo con �xito',
					buttons:	Ext.Msg.OK,
					fn: leeRespuesta,
					closable:false,
					icon: Ext.MessageBox.INFO
				});
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function leeRespuesta(){
		window.location = '24forma02ext.jsp';
	}
	var estatusDoctoOriginal=[];//Variable que almacena los estatus originales que trae la consulta
	function procesaConsulta(opts, success, response) {
		pnl.el.unmask();
		consultaData.loadData('');
			estatusDoctoOriginal.length=0;///////Vaciamos la variable
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			for(var i = 0; i < infoR.regs.length; i++){
				estatusDoctoOriginal.push(infoR.regs[i].FLAG_COMBO);
			}
			if (!grid.isVisible()){
				grid.show();
			}
			if (infoR.regs != undefined && infoR.regs.length > 0){
				consultaData.loadData(infoR.regs);
				grid.getGridEl().unmask();
				if (infoR.totales != undefined && infoR.totales.length > 0 ){
					Ext.each(infoR.totales, function(item,index){
							resumenTotalesData.loadData('');
							var reg = Ext.data.Record.create(['NOMMONEDA', 'TOTAL_REGISTROS','TOTAL_MONTO_DOCUMENTOS']);
							if (parseFloat(item.numRegistros) > 0){
								resumenTotalesData.add(	new reg({	NOMMONEDA:'Solicitudes encontradas',TOTAL_REGISTROS:item.numRegistros ,	TOTAL_MONTO_DOCUMENTOS:''}));
							}
							if (parseFloat(item.numRegistrosMN)!=0) {
								resumenTotalesData.add(	new reg({	NOMMONEDA:'Total de Solicitudes en Moneda Nacional',TOTAL_REGISTROS:item.numRegistrosMN ,	TOTAL_MONTO_DOCUMENTOS:item.montoTotalMN}));
							}
							if (parseFloat(item.numRegistrosDL)!=0)	{
								resumenTotalesData.add(	new reg({	NOMMONEDA:'Total de Solicitudes en D�lares',TOTAL_REGISTROS:item.numRegistrosDL ,	TOTAL_MONTO_DOCUMENTOS:item.montoTotalDL}));
							}
					});
				}
				Ext.getCmp('btnTotales').enable();
				Ext.getCmp('btnGuardar').enable();
			}else{
				grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnGuardar').disable();
				Ext.getCmp('btnTotales').disable();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var tipoCreditoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],					
		data : [	['D','Modalidad 1 (Riesgo Empresa de Primer Orden )'],	['C','Modalidad 2 (Riesgo Distribuidor) ']	]		 
	});

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDistribuidor'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoTipoCobroData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTipoCobroDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesarCatalogoRechazar = function(store, arrRegistros, opts) {
		var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
		store.insert(	0,new reg({	clave: "0",	descripcion: 'No asignar estatus',	loadMsg: null	})	);
	}

	var catalogoRechazarData= new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02ext.data.jsp',
		baseParams: {informacion: 'CatalogoRechazar'},
		totalProperty : 'total',
		autoLoad: true,//MOD +(false) -(true)
		listeners: {	load:procesarCatalogoRechazar,	exception:NE.util.mostrarDataProxyError,	beforeload:NE.util.initMensajeCargaCombo	}
	});

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name:'IC_DOCUMENTO'},
			{name:'IC_MONEDA'},
			{name:'TIPO_CONVERSION'},
			{name:'TIPO_CAMBIO'},
			{name:'TIPO_CREDITO'},
			{name:'FECHA_SOLICITUD'},
			{name:'NOMBRE_EPO'},
			{name:'NOMBRE_PYME'},
			{name:'MONEDA'},
			{name:'MONTO'},
			{name:'REFERENCIA_TASA'},
			{name:'TASA_INTERES'},
			{name:'PLAZO'},
			{name:'FECHA_VENCIMIENTO'},
			{name:'MONTO_INTERES'},
			{name:'TIPO_INTERES'},
			{name:'FLAG_CAUSA'},
			{name:'FLAG_COMBO'},
			{name:'IC_ESTATUS_DOCTO'}
		],
		data:	[{	'IC_DOCUMENTO':'',	'IC_MONEDA':'',	'TIPO_CONVERSION':'',	'TIPO_CAMBIO':'',	'TIPO_CREDITO':'',	'FECHA_SOLICITUD':'',	'NOMBRE_EPO':'',	'NOMBRE_PYME':'',	
					'MONEDA':'',	'MONTO':'',	'REFERENCIA_TASA':'',	'TASA_INTERES':'',	'PLAZO':'',	'FECHA_VENCIMIENTO':'',	'MONTO_INTERES':'','TIPO_INTERES':'','FLAG_CAUSA':'','FLAG_COMBO':''
				}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		fields: [{name: 'NOMMONEDA'},	{name: 'TOTAL_REGISTROS'},	{name: 'TOTAL_MONTO_DOCUMENTOS'}],
		data:	[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_DOCUMENTOS':''}],
		totalProperty : 'total',
		autoLoad: true,
		listeners: {	exception: NE.util.mostrarDataProxyError}
	});

	var gridTotales = {
		xtype: 'grid',	store: resumenTotalesData,	id: 'gridTotales',	hidden:	true, frame: false, width: 940,	height: 120,	title: 'Totales',
		columns: [
			{header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 450, resizable:false,menuDisabled:true},
			{header: 'Registros',dataIndex: 'TOTAL_REGISTROS',	width: 240,	align: 'right',resizable:false,menuDisabled:true},
			{header: 'Monto Documentos',dataIndex: 'TOTAL_MONTO_DOCUMENTOS',width: 240,	align: 'right',resizable:false,menuDisabled:true}
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	};
	///////////////////////////////////////
	var comboEstatus = new Ext.form.ComboBox({
		triggerAction : 'all',
		displayField : 'descripcion',
		valueField : 'clave',
		idgen: {
			type: 'sequential',
			prefix: 'id_',
			seed: 1000,
			id: 'seqgen01'
		},
		store : catalogoRechazarData,
		
		listener:function(combo,e){
			
		}
	});
	Ext.util.Format.comboRenderer = function(comboEstatusAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var valor = registro.data['FLAG_COMBO'];					
			if(valor !=''){
				var record = comboEstatusAsignar.findRecord(comboEstatusAsignar.valueField, value);
				return record ? record.get(comboEstatusAsignar.displayField) : comboEstatusAsignar.valueNotFoundText;
			} 
			if(valor !=''){
				return valor;
			}
        
		}
	}	
	////////////////////////////////////
	var grid = new Ext.grid.EditorGridPanel({
		id:'grid',store: consultaData,	columnLines:true, clicksToEdit:1, hidden:true,
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
	
		columns: [
			{
				header:'N�m. Documento Final',	tooltip:'N�m. Documento Final',	dataIndex:'IC_DOCUMENTO',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Epo',	tooltip:'Epo',	dataIndex:'NOMBRE_EPO',	sortable:true,	resizable:true,	width:130,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header: 'Distribuidor',	tooltip: 'Distribuidor',	dataIndex:'NOMBRE_PYME',	sortable:true,	resizable:true,	width:200,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header : 'Tipo de Cr�dito', tooltip: 'Tipo de Cr�dito',	dataIndex : 'TIPO_CREDITO',	sortable:true, resizable:true,	width : 180, align: 'center'
			},{
				header : 'Fecha solicitud', tooltip: 'Fecha solicitud',	dataIndex : 'FECHA_SOLICITUD',
				sortable : true, width : 100, align: 'center'
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header : 'Monto', tooltip: 'Monto',	dataIndex : 'MONTO',
				sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, hideable: false
			},{
				header:'Referencia Tasa de inter�s',	tooltip:'Referencia Tasa de inter�s',	dataIndex:'REFERENCIA_TASA',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Tasa inter�s',	tooltip:'Tasa inter�s',	dataIndex:'TASA_INTERES',	sortable:true,	resizable:true,	width:130,	align:'center',renderer: Ext.util.Format.numberRenderer('0.00')
			},{
				header:'Plazo',	tooltip:'Plazo',	dataIndex:'PLAZO',	sortable:true,	resizable:true,	width:80,	align:'center'
			},{
				header : 'Fecha de vencimiento', tooltip: 'Fecha de vencimiento',	dataIndex : 'FECHA_VENCIMIENTO',
				sortable : true, width : 100, align: 'center', hidden: false
			},{
				header : 'Monto inter�s', tooltip: 'Monto inter�s',	dataIndex : 'MONTO_INTERES',
				sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, hideable: false
			},{
				header:'Tipo de cobro inter�s',	tooltip:'Tipo de cobro inter�s',	dataIndex:'TIPO_INTERES',	sortable:true,	resizable:true,	width:130,	align:'center'
			},			
			{
				header: 'Rechazar(cambiar estatus)',	tooltip: 'Rechazar(cambiar estatus)',	dataIndex: 'FLAG_COMBO',	sortable: true,	resizable: true,	width: 160,	align: 'center',
				editor: {
							xtype : 'combo', id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection : true,	triggerAction : 'all',fixed:true,
							typeAhead: true,	minChars : 1,store:catalogoRechazarData,lazyInit: false, lazyRender: true, emptyText:'Seleccione...',
							listeners:{ focus:{ 
												fn: function (comboField,record,index) { 
													comboField.doQuery(comboField.allQuery, true); 
													comboField.expand(); 
													
													catalogoRechazarData.load({
															params: {
																estatus: comboField.value
															}
													});
												}
											},
											select:{ fn:function (comboField, record, index) {
																comboField.fireEvent('blur'); //Ext.getCmp('grid').getView().refresh();
															}
													 }
							}
						},
				renderer:Ext.util.Format.comboRenderer(comboEstatus)/*function(value,metadata,registro, rowIndex, colIndex){
									//var record = combo.findRecord(combo.valueField, value);
									//return record ? record.get(combo.displayField) : combo.valueNotFoundText;
									if(!Ext.isEmpty(value)){
										var dato = catalogoRechazarData.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoRechazarData.getAt(dato);
											value = reg.get('descripcion');
										}
									}
                        return NE.util.colorCampoEdit(value,metadata,registro);
							}*/
			},{
				header : 'Causa', tooltip: 'Causa',
				dataIndex : 'FLAG_CAUSA', fixed:true,
				sortable : false,	width : 250,	hiddeable: false,
				editor:{
					xtype:	'textarea',
					id: 'txtArea',
					maxLength: 260,
					enableKeyEvents: true,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 260){
								field.setValue((field.getValue()).substring(0,259));
								Ext.Msg.alert('Observaciones','La causa del cambio de estatus no debe sobrepasar los 260 caracteres.');
								field.focus();
							}
						}
					}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			}
		],
		bbar: {
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						var totalesCmp = Ext.getCmp('gridTotales');
						if (!totalesCmp.isVisible()) {
							totalesCmp.show();
							totalesCmp.el.dom.scrollIntoView();
						}
					}
				},{xtype: 'tbspacer', width: 5},'-',{
					xtype: 'tbspacer', width: 5
				},
				{
					text:'Guardar Cambios', id:'btnGuardar',	iconCls:'icoGuardar',
					handler: processResult
				}
				
				
				/*{
					text:'Guardar Cambios', id:'btnGuardar',	iconCls:'icoGuardar',
					handler: function(boton){
									var flag = false;
									var jsonData = consultaData.data.items;
									Ext.each(jsonData, function(item,index,arrItem){
									alert(item.data.FLAG_COMBO);
										if (!Ext.isEmpty(item.data.FLAG_COMBO) && item.data.FLAG_COMBO != '0'){											
											flag = true;
											return false;
										}	
									});								
									
									if (!flag){
										Ext.Msg.alert(boton.text,'Para poder cambiar el estatus debe seleccionar por lo menos uno');
										return;
									}
									Ext.Msg.show({
										title:boton.text,
										msg: 'Este proceso cambiara el estatus de los documentos seleccionados.&nbsp;<br>�Desea continuar?',
										buttons: Ext.Msg.OKCANCEL,
										fn: processResult,
										animEl: 'elId',
										icon: Ext.MessageBox.QUESTION
									});
									boton.setIconClass('loading-indicator');
								}
				}
				*/,{
					xtype: 'tbspacer', width: 5
				}
			]
		}
	});

	/*
	Ext.getCmp('grid').on('afteredit', function(e){
		 if (e.field == 'FLAG_COMBO') {
			var gridEl = Ext.getCmp('grid').getGridEl();
			var col = grid.getColumnModel().findColumnIndex('FLAG_CAUSA');
			var rowEl = Ext.getCmp('grid').getView().getCell( e.row, col);
			rowEl.scrollIntoView(gridEl,false);
			Ext.getCmp('grid').startEditing(e.row, col);
		 }
	});
	*/

	var icDocumento = [];
	var causa = [];
	var estatusdocto=[];
	
	var cancelar =  function() {  
		icDocumento = [];
		causa = [];
		estatusdocto=[];
	}
	
	function processResult(){
	
		var grid = Ext.getCmp('grid');
		var store = grid.getStore();		
		var columnModelGrid = grid.getColumnModel();
		var totales =0;
		var numRegistro = 0;
		var totalesC =0; 
		
		cancelar(); //limpia las variables 
		
		store.each(function(record) {
			numRegistro = store.indexOf(record);
			
			if(record.data['IC_ESTATUS_DOCTO']!=record.data['FLAG_COMBO']  ){
				if(record.data['FLAG_CAUSA'] ==''){					
					Ext.MessageBox.alert('Error de validaci�n','Debe de capturar la causa de rechazo',
						function(){
							grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('FLAG_CAUSA'));							
							return;
						}
					);	
					totalesC++;	
				}
				totales++;
			}
			estatusdocto.push(record.data['FLAG_COMBO'] );
			icDocumento.push( record.data['IC_DOCUMENTO']  );
			causa.push(record.data['FLAG_CAUSA']);
						
		});
		
		if (totales==0 ){
			Ext.Msg.alert('Mensaje','Para poder cambiar el estatus debe seleccionar por lo menos uno');
			return;
		}
		if (totalesC==0 ){		
			Ext.Ajax.request({
				url: '24forma02ext.data.jsp',
				params: Ext.apply({
					informacion: 'CambiaEstatus',
					ic_documento: icDocumento,
					estatusdocto:	estatusdocto,
					causa: causa	
				}),
				callback: procesaCambiaEstatus
			});	
			
		}
			
	}
	
	var dt = new Date();

	var elementosForma = [
		{
			xtype: 'panel',
			layout:'column',
				items:[{
					xtype: 'container',
					id:	'panelIzq',
					columnWidth:.5,
					defaults: {	msgTarget: 'side',	anchor: '-20'	},
					layout: 'form',
					items: [
						{
							xtype: 'combo',
							name: 'tipoLinCre',
							fieldLabel: 'Tipo de Cr�dito',
							emptyText: 'Seleccionar Credito',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'tipoLinCre',
							forceSelection : false,
							triggerAction : 'all',
							allowBlank: false,
							lazyRender:true,
							typeAhead: true,
							minChars : 1,
							store: tipoCreditoData,
							listeners:{
								'select':function(cbo){
												Ext.getCmp('_distrib').hide();
												var comboEpo = Ext.getCmp('_epo');
												comboEpo.setValue('');
												comboEpo.store.removeAll();
												comboEpo.store.reload({	params: {tipo_credito: cbo.getValue()}	});
											}
							}
						},{
							xtype: 'combo',
							id:	'_epo',
							name: 'epo',
							hiddenName : 'epo',
							fieldLabel: 'EPO',
							emptyText: 'Seleccione una EPO. . .',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoEpoDistData,
							tpl : NE.util.templateMensajeCargaCombo,
							listeners:{
								'select':function(cbo){
												if(!Ext.isEmpty(cbo.getValue())){
														var comboPyme = Ext.getCmp('_distrib');
														comboPyme.setValue('');
														comboPyme.store.removeAll();
														comboPyme.store.reload({	params: {epo: cbo.getValue()}	});
														comboPyme.show();
												}
											}
							}
						},{
							xtype: 'combo',
							id:	'_distrib',
							name: 'distrib',
							hiddenName : 'distrib',
							fieldLabel: 'Distribuidor',
							emptyText: 'Seleccione un distribuidor. . .',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							hidden:true,
							minChars : 1,
							store: catalogoPymeDistData
						},{
							xtype: 'numberfield',
							name: 'numCred',
							id: 'numCred',
							fieldLabel: 'N�mero de Documento Final',
							anchor:'50%',
							maxLength: 9
						},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de solicitud',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaOper1',
									id: '_fechaOper1',
									allowBlank: true,
									value: dt.format('d/m/Y'),
									startDay: 0,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: '_fechaOper2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaOper2',
									id: '_fechaOper2',
									allowBlank: true,
									value: dt.format('d/m/Y'),
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: '_fechaOper1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},{
							xtype: 'compositefield',
							fieldLabel: 'Monto',
							msgTarget: 'side',
							combineErrors: false, anchor: '98%',
							items: [
								{
									xtype: 'numberfield',
									name: 'monto1',
									id: '_monto1',
									allowBlank: true,
									maxLength: 9,
									width: 110,
									msgTarget: 'side',
									vtype: 'rangoValor',
									campoFinValor: '_monto2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'a',
									width: 15
								},
								{
									xtype: 'numberfield',
									name: 'monto2',
									id: '_monto2',
									allowBlank: true,
									maxLength: 9,
									width: 110,
									msgTarget: 'side',
									vtype: 'rangoValor',
									campoInicioValor: '_monto1',
									margins: '0 20 0 0'	  //necesario para mostrar el icono de error
								}
							]
						}
					]
				},{
					xtype: 'container',
					id:	'panelDer',
					columnWidth:.5,
					layout: 'form',
					defaults: {	msgTarget: 'side',	anchor: '-20'	},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de vencimiento',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaVenc1',
									id: '_fechaVenc1',
									allowBlank: true,
									startDay: 0,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: '_fechaVenc2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaVenc2',
									id: '_fechaVenc2',
									allowBlank: true,
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: '_fechaVenc1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},{
							xtype: 'combo',
							name: 'tipoCoInt',
							id:	'_tipoCoInt',
							fieldLabel: 'Tipo de cobro de inter�s',
							emptyText: 'Seleccione Tipo de cobro',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'tipoCoInt',
							forceSelection : false,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoTipoCobroData,
							tpl : NE.util.templateMensajeCargaCombo
						}
					]
				}]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 940,
		style: ' margin:0 auto;',
		title:	'<div align="center">Criterios de B�squeda</div>',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					var totalesCmp = Ext.getCmp('gridTotales');
					if (totalesCmp.isVisible()) {
						totalesCmp.hide();
					}
					if(!verificaPanelIzq()) {
						return;
					}
					if(!verificaPanelDer()) {
						return;
					}
					var fechaOper1 = Ext.getCmp('_fechaOper1');
					var fechaOper2 = Ext.getCmp('_fechaOper2');
					var fechaVenceMin = Ext.getCmp('_fechaVenc1');
					var fechaVenceMax = Ext.getCmp('_fechaVenc2');
					var izq_montoMin = Ext.getCmp('_monto1');
					var izq_montoMax = Ext.getCmp('_monto2');
					if (!Ext.isEmpty(fechaOper1.getValue()) || !Ext.isEmpty(fechaOper2.getValue()) ) {
						if(Ext.isEmpty(fechaOper1.getValue()))	{
							fechaOper1.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
							fechaOper1.focus();
							return;
						}else if (Ext.isEmpty(fechaOper2.getValue())){
							fechaOper2.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
							fechaOper2.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaVenceMin.getValue()) || !Ext.isEmpty(fechaVenceMax.getValue()) ) {
						if(Ext.isEmpty(fechaVenceMin.getValue()))	{
							fechaVenceMin.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMin.focus();
							return;
						}else if (Ext.isEmpty(fechaVenceMax.getValue())){
							fechaVenceMax.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(izq_montoMin.getValue()) || !Ext.isEmpty(izq_montoMax.getValue()) ) {
						if(Ext.isEmpty(izq_montoMin.getValue()))	{
							izq_montoMin.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							izq_montoMin.focus();
							return;
						}else if (Ext.isEmpty(izq_montoMax.getValue())){
							izq_montoMax.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							izq_montoMax.focus();
							return;
						}
					}
					pnl.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24forma02ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{informacion: "Consulta"}),
						callback: procesaConsulta
					});
				} //fin handler
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24forma02ext.jsp';
				}
			}
		]
	});

	function verificaPanelIzq(){
		var myPanel = Ext.getCmp('panelIzq');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

	function verificaPanelDer(){
		var myPanel = Ext.getCmp('panelDer');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid,	gridTotales,	NE.util.getEspaciador(10)
		]
	});

	//catalogoEpoDistData.load();
	catalogoTipoCobroData.load();

});