	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,  
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONObject,
	net.sf.json.JSONArray"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/24finandist/24secsession.jspf" %>

<%
	JSONObject jsonObj  = new JSONObject();
	String infoRegresar = "";
	String informacion  =(request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String ic_epo       =(request.getParameter("HicEpo")      != null) ? request.getParameter("HicEpo")      :"";
	String ctCredito    =(request.getParameter("HctCredito")  != null) ? request.getParameter("HctCredito")  :"";
	String icDist       =(request.getParameter("HicDist")     != null) ? request.getParameter("HicDist")     :"";
	String numCred      =(request.getParameter("numCred")     != null) ? request.getParameter("numCred")     :"";
	String fechaOper1   =(request.getParameter("fechaOper1")  != null) ? request.getParameter("fechaOper1")  :"";
	String fechaOper2   =(request.getParameter("fechaOper2")  != null) ? request.getParameter("fechaOper2")  :"";
	String monto1       =(request.getParameter("monto1")      != null) ? request.getParameter("monto1")      :"";
	String monto2       =(request.getParameter("monto2")      != null) ? request.getParameter("monto2")      :"";
	String cbMoneda     =(request.getParameter("HcbMoneda")   != null) ? request.getParameter("HcbMoneda")   :"";
	String fechaVenc1   =(request.getParameter("fechaVenc1")  != null) ? request.getParameter("fechaVenc1")  :"";
	String fechaVenc2   =(request.getParameter("fechaVenc2")  != null) ? request.getParameter("fechaVenc2")  :"";
	String tipoCoInt    =(request.getParameter("HtipoCoInt")  != null) ? request.getParameter("HtipoCoInt")  :"";
	String estatus      =(request.getParameter("Hestatus")    != null) ? request.getParameter("Hestatus")    :"";
	String operacion    =(request.getParameter("operacion")   != null) ? request.getParameter("operacion")   :"";
	String tipo_pago    =(request.getParameter("tipo_pago")   != null) ? request.getParameter("tipo_pago")   :""; //FODEA 09-2015

	int start=0,limit=0;
	JSONObject resultado = new JSONObject();

	if(informacion.equals("catologoEpo")){

		CatalogoEPODistribuidores catalogo = new CatalogoEPODistribuidores();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveIf(iNoCliente);
		infoRegresar=catalogo.getJSONElementos();
	}else if(informacion.equals("catologoDist")){
	
		CatalogoPymeDistribuidores catalogo = new CatalogoPymeDistribuidores();
		catalogo.setCampoClave("cp.ic_pyme");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveEpo(ic_epo);
		infoRegresar=catalogo.getJSONElementos();
	}else if (informacion.equals("catologoMonedaDist")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();	
	}else if (informacion.equals("CatalogoTipoCobroDist")){

		String rs_epo_tipo_cobro_int	=	"1,2";
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_tipo_cobro_interes");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_tipo_cobro_interes");
		cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoEstatus")){
		
		String rs_epo_tipo_cobro_int	=	"1,2,3,5,10,11,4,14";
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_estatus_solic");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_estatus_solic");
		cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
		cat.setOrden("1");
		
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta")||informacion.equals("ArchivoPaginaPDF")||informacion.equals("ArchivoPDF")||informacion.equals("ArchivoCSV")||informacion.equals("ArchivoVar")||informacion.equals("ArchivoFijo")||informacion.equals("ArchivoTCPDF")||informacion.equals("ArchivoTCCSV")){
		InfSolicIfDist paginador=new InfSolicIfDist();
		paginador.setIcEpo(ic_epo);
		if("".equals(ctCredito)){
			if(strPerfil.equals("IF FACT RECURSO")){
				paginador.setCtCredito("F");
			}else{
				paginador.setCtCredito(ctCredito);
			}
		}else{
			paginador.setCtCredito(ctCredito);
		}
		paginador.setIcDist(icDist);
		paginador.setNumCred(numCred);
		paginador.setFechaOper1(fechaOper1);
		paginador.setFechaOper2(fechaOper2);
		paginador.setMonto1(monto1);
		paginador.setMonto2(monto2);
		paginador.setCbMoneda(cbMoneda);
		paginador.setFechaVenc1(fechaVenc1);
		paginador.setFechaVenc2(fechaVenc2);
		paginador.setTipoCoInt(tipoCoInt);
		paginador.setEstatus(estatus);
		paginador.setSesIf(iNoCliente);
		paginador.setTipoPago(tipo_pago);

		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
			try{
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));	
			}catch(Exception e){
					System.out.println("Error en parametros");
			}
		if (operacion.equals("Generar")) {	//Nueva consulta
				cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		if (informacion.equals("Consulta")){
		String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
		resultado = JSONObject.fromObject(consultar);
		infoRegresar=resultado.toString();
		}else	 if(informacion.equals("ArchivoPaginaPDF")){
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				String nombreArchivo = cqhelper.getCreatePageCustomFile(request, start, limit,strDirectorioTemp, "PDF");
				jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
		else if(informacion.equals("ArchivoPDF")){
			jsonObj = new JSONObject();
			try{
				String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			}catch(Throwable e){
				jsonObj.put("success",new Boolean(false));
				throw new AppException("Error al generar el archivo PDF");
			}
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("ArchivoCSV")){
			String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("ArchivoVar")){
			try{
				String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"VAR");
				jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF");
			}
		}else if(informacion.equals("ArchivoFijo")){
			try{
				String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"FIJO");
				jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF");
			}
		}else if(informacion.equals("ArchivoTCPDF")){
			try{
				String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"TC-PDF");
				jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo TCPDF");
			}
		}else if(informacion.equals("ArchivoTCCSV")){
			try{
				String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"TC-CSV");
				jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo TCCSV");
			}
		}
		
} else if (informacion.equals("Totales")){
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesión
		infoRegresar = queryHelper.getJSONResultCount(request);
} 


	%>

<%=infoRegresar%>


