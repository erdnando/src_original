<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.distribuidores.*,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("Procesar"))	{

	String datosPro[]	= request.getParameterValues("aprocesar");
	String totalRegistros	=	(request.getParameter("totalRegistros")!=null)?request.getParameter("totalRegistros"):"";
	String nombreArchivo	=	(request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";

	StringBuffer datosProcesar = new StringBuffer();

	if(datosPro.length > 0){
		for(int i= 0; i<datosPro.length;  i++){
			datosProcesar.append(datosPro[i]+"|");
		}
	}

	JSONObject jsonObj = new JSONObject();
	List datosAcuse = new ArrayList();
	List procesar = new ArrayList();
	List aprocesar = new ArrayList();

	try{

		CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

			//esto es para los datos a procesar
		if(!datosProcesar.equals("")){
				List datos = new VectorTokenizer(datosProcesar.toString(), "\n").getValuesVector();		
				for(int i = 0; i < datos.size(); i++) 	{
					if(datos.get(i)!=null){						
							List undato = new VectorTokenizer((String)datos.get(i), "|").getValuesVector();								
							for(int e = 0; e < undato.size(); e++) 	{										
								String undato1   = (undato.size()>= 1)?undato.get(e)==null?"":((String)undato.get(e)).trim():"";				 
								
								//aqui empiezo de nuevo
								List datos2 = new VectorTokenizer(undato1, "\n").getValuesVector();	
								
								for(int x = 0; x < datos2.size(); x++) 	{
									if(datos2.get(x)!=null){			
									List undato4 = new VectorTokenizer((String)datos2.get(x), ",").getValuesVector();		
									procesar = new ArrayList();
									for(int y = 0; y < undato4.size(); y++) 	{										
										String undatov   = (undato4.size()>= 1)?undato4.get(y)==null?"":((String)undato4.get(y)).trim():"";				 
										procesar.add(undatov);										
									}									
								}							
							aprocesar.add(procesar);
							}								
					}							
				}
			}
		}

		//metodo para cambiar el estatus y regresa los datos capturados en la tabla  de DIS_RECHAZO_SOLIC
		datosAcuse = cargaDocto.procesarRechazosMasivo( aprocesar,  nombreArchivo,  totalRegistros);

		String regArchivo = (String)datosAcuse.get(0);
		String regProcesados = (String)datosAcuse.get(1);
		String noProceso = (String)datosAcuse.get(2);
		String nombreArchivo_b = (String)datosAcuse.get(3);
		String fechaCarga = (String)datosAcuse.get(4);

		jsonObj.put("regArchivo", regArchivo);
		jsonObj.put("regProcesados", regProcesados);
		jsonObj.put("noProceso", noProceso);
		jsonObj.put("nombreArchivo_b", nombreArchivo_b);
		jsonObj.put("fechaCarga", fechaCarga);
		jsonObj.put("success", new Boolean(true));

	} catch(Exception e) {
		throw new AppException("Ocurrio un error durante el proceso . . . ", e);
	}

	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>