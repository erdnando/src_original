Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

	var form = {Maxpuntos:null, Cadena:null}

	function procesaConfirmaTasas(opts, success, response) {
		pnl.el.unmask();
		Ext.getCmp('btnConfirmaTasas').setIconClass('autorizar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			window.location = '24forma03ext.jsp';
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaAgregaTasas(opts, success, response) {
		pnl.el.unmask();
		consultaData.loadData('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (!grid.isVisible()){
				grid.show();
			}
			if (infoR.regs != undefined && infoR.regs.length > 0){
				consultaData.loadData(infoR.regs);
				grid.getGridEl().unmask();
				Ext.getCmp('barraGrid').setVisible(true);
			}else{
				grid.getGridEl().mask('No hay tasas disponibles para esta EPO', 'x-mask');
				Ext.getCmp('barraGrid').setVisible(false);
			}
			if(infoR.lsCadena != undefined){
				form.Cadena = infoR.lsCadena;
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaConsultaTasas(opts, success, response) {
		pnl.el.unmask();
		consultaData.loadData('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.liRegistros != undefined && infoR.liRegistros === '0'){
				Ext.getCmp('_pRelMat').show();
				Ext.getCmp('_pPuntos').reset();
				Ext.getCmp('_pPuntos').show();
				Ext.getCmp('btnAgregar').show();
				Ext.getCmp('btnLimpiar').show();
				if (infoR.alerta != undefined){
					Ext.Msg.alert('',infoR.alerta);
				}
			}else{
				if (!grid.isVisible()){
					grid.show();
				}
				if (infoR.regs != undefined && infoR.regs.length > 0){
					consultaData.loadData(infoR.regs);
					grid.getGridEl().unmask();
				}else{
					grid.getGridEl().mask('No hay tasas disponibles para esta EPO', 'x-mask');
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaComboConsulta(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (parseFloat(infoR.TotalLineas)==0 ){
				Ext.getCmp('_ic_moneda').setValue("1");
			}else{
				Ext.getCmp('_ic_moneda').show();
			}
			Ext.Ajax.request({
				url: '24forma03ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{informacion: "ConsultaTasas"}),
				callback: procesaConsultaTasas
			});
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if ( Ext.util.JSON.decode(response.responseText).Maxpuntos != undefined){ form.Maxpuntos = Ext.util.JSON.decode(response.responseText).Maxpuntos;}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var catalogoRelacionMatData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['+','+'],
			['-','-'],
			['*','*'],
			['/','/']
		 ]
	});

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma03ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesarCatalogoMoneda = function(store, arrRegistros, opts) {
		var dato = catalogoMonedaData.findExact("clave", "1");
		if (dato != -1 ) {
			Ext.getCmp('_ic_moneda').setValue('1');
		}
	}

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma03ext.data.jsp',
		baseParams: {	informacion: 'CatalogoMonedaDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesarCatalogoMoneda,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name:'MONEDA'},
			{name:'TASA_INTERES'},
			{name:'PLAZO'},
			{name:'VALOR'},
			{name:'REL_MAT'},
			{name:'PUNTOS_ADD'},
			{name:'TASA_APLICAR'}
		],
		data:	[{	'MONEDA':'',	'TASA_INTERES':'',	'PLAZO':'',	'VALOR':'',	'REL_MAT':'',	'PUNTOS_ADD':'',	'TASA_APLICAR':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var grid = new Ext.grid.GridPanel({
		id:'grid',store: consultaData,	columnLines:true, hidden:true, viewConfig: {forceFit: true},
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
		columns: [
			{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:true,	width:150,	align:'center'
			},{
				header:'Tipo Tasa',	tooltip:'Tipo Tasa',	dataIndex:'TASA_INTERES',	sortable:true,	width:200,	align:'center'
			},{
				header:'Plazo',	tooltip:'Plazo',	dataIndex:'PLAZO',	sortable:true,	width:120,	align:'center'
			},{
				header:'Valor',	tooltip:'Valor',	dataIndex:'VALOR',	sortable:true,	width:130,	align:'right',renderer: Ext.util.Format.numberRenderer('0.00')
			},{
				header:'Rel. Mat.',	tooltip:'Rel. Mat.',	dataIndex:'REL_MAT',	sortable:true,	width:130,	align:'center'
			},{
				header:'Puntos Adicionales',	tooltip:'Puntos Adicionales',	dataIndex:'PUNTOS_ADD',	sortable:true,	width:130,	align:'center',renderer: Ext.util.Format.numberRenderer('0.00')
			},{
				header:'Tasa a Aplicar',	tooltip:'Tasa a Aplicar',	dataIndex:'TASA_APLICAR',	sortable:true,	width:130,	align:'right',renderer: Ext.util.Format.numberRenderer('0.00')
			}
		],
		bbar: {
			id:'barraGrid',
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',
					text: 'Confirmar Tasas',
					id: 'btnConfirmaTasas',
					iconCls:'autorizar',
					handler: function(boton, evento) {
									Ext.Msg.show({
										title:boton.text,
										msg: '�Esta seguro de confirmar tasas?',
										buttons: Ext.Msg.OKCANCEL,
										fn: processConfirma,
										animEl: 'elId',
										icon: Ext.MessageBox.QUESTION
									});
									boton.setIconClass('loading-indicator');
								}
				},{
					xtype: 'tbspacer', width: 5
				}
			]
		}
	});

	function processConfirma(res){
		if (res == 'ok' || res == 'yes'){
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '24forma03ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),
							{informacion: "ConfirmaTasas",
							lsCadena: form.Cadena}),
				callback: procesaConfirmaTasas
			});
		}else{
			Ext.getCmp('btnConfirmaTasas').setIconClass('autorizar');
		}
	}

	var elementosForma = [
		{
			xtype: 'combo',
			id:	'_ic_epo',
			name: 'ic_epo',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEpoDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:	{fn:function(combo){
									if (	!Ext.isEmpty(combo.getValue())	) {
										Ext.getCmp('_ic_moneda').hide();
										Ext.getCmp('_pRelMat').hide();
										Ext.getCmp('_pPuntos').hide();
										Ext.getCmp('btnAgregar').hide();
										Ext.getCmp('btnLimpiar').hide();
										grid.hide();
										Ext.getCmp('barraGrid').setVisible(false);
										pnl.el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: '24forma03ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{informacion: "comboConsulta"}),
											callback: procesaComboConsulta
										});
									}
							}
				}
			}
		},{
			xtype: 'combo',
			name: 'ic_moneda',
			id:	'_ic_moneda',
			hiddenName : 'ic_moneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccionar . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo,	hidden:true,
			listeners:{
				select:	{fn:function(combo){
									if (	!Ext.isEmpty(combo.getValue())	) {
										Ext.getCmp('_pRelMat').hide();
										Ext.getCmp('_pPuntos').hide();
										Ext.getCmp('btnAgregar').hide();
										Ext.getCmp('btnLimpiar').hide();
										grid.hide();
										pnl.el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: '24forma03ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{informacion: "ConsultaTasas"}),
											callback: procesaConsultaTasas
										});
									}
								}
				}
			}
		},{
			xtype: 'combo',
			name: 'pRelMat',
			id:	'_pRelMat',
			hiddenName : 'pRelMat',
			fieldLabel: 'Relaci�n Mat.',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			editable:false,
			lazyRender:true,
			typeAhead: true,
			minChars:1,
			value:	'+',
			anchor:	'32%',
			store: catalogoRelacionMatData,
			hidden:true
		},{
			xtype: 'numberfield',
			name: 'pPuntos',
			id: 	'_pPuntos',
			fieldLabel: 'Puntos Adicionales',
			allowBlank:false,	maxLength: 7,	anchor:'30%',	hidden:true,
			listeners:{
				blur:	{fn:function(field){
									if (	!Ext.isEmpty(field.getValue())	) {
										validaFlotante(field,form.Maxpuntos);
									}
							}
				}
			}
		}
	];

	function validaFlotante(field,	Maxpuntos){
		var valid = true;
		if(Maxpuntos < field.getValue() ){
			field.markInvalid('El n�mero rebasa los puntos autorizados');
			field.focus();
			valid = false;
		}
		return valid;
	}

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title:'Tasas por EPO',
		width: 500,
		frame: true,
		labelWidth:110,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Agregar',
				id: 'btnAgregar',
				iconCls: 'aceptar',
				hidden:true,
				handler: function(boton, evento) {
								if (	!validaFlotante(Ext.getCmp('_pPuntos'),form.Maxpuntos)	){
									return;
								};
								if(!verificaPanel()) {
									return;
								}
								grid.hide();
								pnl.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '24forma03ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{informacion: "AgregaTasas"}),
									callback: procesaAgregaTasas
								});
				} //fin handler
			},{
				text: 'Limpiar',
				id:	'btnLimpiar',
				iconCls: 'icoLimpiar',
				hidden:true,
				handler: function() {
					window.location = '24forma03ext.jsp';
				}
			}
		]
	});

	function verificaPanel(){
		var myPanel = Ext.getCmp('forma');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid,	NE.util.getEspaciador(10)
		]
	});

	catalogoMonedaData.load();
	catalogoEpoDistData.load();

	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24forma03ext.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});

});