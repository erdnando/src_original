<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj	=	new JSONObject();
//String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String	ic_epo		=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String	ic_pyme		=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String	fecha_ini	=	(request.getParameter("Txtfchini")==null)?"":request.getParameter("Txtfchini");
String	fecha_fin	=	(request.getParameter("Txtffin")==null)?"":request.getParameter("Txtffin");
String 	tipoArchivo	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String	tipoConsulta	=	(request.getParameter("tipoConsulta")==null)?"":request.getParameter("tipoConsulta");
String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");

String fechaHoy		= "";
String HoraActual		= "";
String extArchivo		= "";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
  HoraActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}
try {
	StringBuffer contenidoArchivoGeneral	=	new StringBuffer();
	StringBuffer contenidoArchivoFijo		=	new StringBuffer();
	StringBuffer contenidoArchivoVariable	=	new StringBuffer();

	String numdocto ="", noDistribuidor="", nombredis="", claveEpo ="", nombreEpo ="", 
	fechaOperacion = "" ,claveMoneda="", moneda= "", plazoCredito="",fechaVencimiento="",
	claveTasa="",valorTasa="", tipocobroint ="", monedaLinea ="", tipoconversion ="", 
	tipocambio="", referencia ="";
	double montoInteres		= 0;
	double totalDoctosMN 	= 0;
	double totalMontoMN 	 	= 0;
	double totalMontoIntMN	= 0;
	double totalDoctosUSD	= 0;
	double totalMontoUSD 	= 0;
	double montoCredito     = 0;
	double totalMontoIntUSD = 0;

	if(tipoArchivo.equals("Variable")){
		contenidoArchivoVariable.append("H"+","+fechaHoy+","+HoraActual+","+"Consulta Solicitudes"+"\n");
		contenidoArchivoVariable.append(
				"D"+","+"No. Documento final,"+
				"No. Distribuidor,"+
				"Nombre Distribuidor,"+
				"Clave Epo,"+
				"Nombre EPO,"+
				"Fecha de operación,"+
				"Clave de moneda,"+
				"Moneda,"+
				"Monto del crédito,"+
				"Plazo crédito,"+
				"Fecha vencimiento crédito,"+
				"Monto Interes,"+
				"Clave Tasa interes,"+
				"Tasa interes,"+
				"Tipo de conversión");
		contenidoArchivoVariable.append("\n");
	}

	if(tipoArchivo.equals("Fijo")){
		contenidoArchivoFijo.append("H"+","+fechaHoy+","+HoraActual+","+"Documentos por Autorizar"+"\n");
		contenidoArchivoFijo.append("\n");
	}

	AvNotifIfDist notif = new AvNotifIfDist();
	notif.setIc_if(iNoCliente);
	notif.setIc_epo(ic_epo);
	notif.setIc_pyme(ic_pyme);
	notif.setFechaInicial(fecha_ini);
	notif.setFechaFinal(fecha_fin);
	notif.setTipoConsulta(tipoConsulta);
	notif.setStrPerfil(strPerfil);
	notif.setNumOrden(numOrden);
   if(strPerfil.equals("IF FACT RECURSO")){
      notif.setTipoCredito("F");
   }else{
      notif.setTipoCredito("");
   }
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(notif);
	Registros reg = queryHelper.doSearch();
	
	while (reg.next()) {
		numdocto			= (reg.getString("ig_numero_docto")==null)?"":reg.getString("ig_numero_docto");
		noDistribuidor	= (reg.getString("ic_pyme")==null)?"":reg.getString("ic_pyme"); //fff
		nombredis		= (reg.getString("DISTRIBUIDOR")==null)?"":reg.getString("DISTRIBUIDOR");
		claveEpo			= (reg.getString("ic_epo")==null)?"":reg.getString("ic_epo"); //ssss
		nombreEpo		= (reg.getString("EPO")==null)?"":reg.getString("EPO");
		fechaOperacion	= (reg.getString("df_fecha_hora")==null)?"":reg.getString("df_fecha_hora");
		claveMoneda		= (reg.getString("ic_moneda")==null)?"":reg.getString("ic_moneda");		
		moneda			= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
		montoCredito	= Double.parseDouble( (reg.getString("FN_MONTO")==null)?"0":reg.getString("FN_MONTO") );
		plazoCredito	= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
		fechaVencimiento= (reg.getString("df_fecha_venc")==null)?"":reg.getString("df_fecha_venc");
		montoInteres	= Double.parseDouble( (reg.getString("MONTO_INTERES")==null)?"0":reg.getString("MONTO_INTERES") );
		claveTasa		= (reg.getString("IC_TASA")==null)?"":reg.getString("IC_TASA"); //dddd
		valorTasa		= (reg.getString("VALOR_TASA")==null)?"":reg.getString("VALOR_TASA");
		tipocobroint	= (reg.getString("tipo_cobro_interes")==null)?"":reg.getString("tipo_cobro_interes");
		monedaLinea		= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
		tipoconversion	= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
		tipocambio		= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
		tipoconversion	= (reg.getString("cg_tipo_conversion")==null)?"":reg.getString("cg_tipo_conversion");
		tipocambio		= (reg.getString("TIPO_CAMBIO")==null)?"":reg.getString("TIPO_CAMBIO");
		referencia		= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
		// INICIO: FODEA 09-2015
		String tipoPago = (reg.getString("IG_TIPO_PAGO")==null)?"":reg.getString("IG_TIPO_PAGO");
		if(tipoPago.equals("2")){
			plazoCredito = plazoCredito + " (M)";
		}
			// FIN: FODEA 09-2015
		if(tipoArchivo.equals("Variable")){
			contenidoArchivoVariable.append("D"+",");
			contenidoArchivoVariable.append(numdocto.replace(',',' ')+",");
			contenidoArchivoVariable.append(noDistribuidor.replace(',',' ')+",");
			contenidoArchivoVariable.append(nombredis.replace(',',' ')+",");
			contenidoArchivoVariable.append(claveEpo.replace(',',' ')+",");
			contenidoArchivoVariable.append(nombreEpo.replace(',',' ')+",");
			contenidoArchivoVariable.append(fechaOperacion.replace(',',' ')+",");
			contenidoArchivoVariable.append(claveMoneda.replace(',',' ')+",");
			contenidoArchivoVariable.append(moneda.replace(',',' ')+",");
			contenidoArchivoVariable.append(Comunes.formatoDecimal(montoCredito,2).replace(',',' ')+",");
			contenidoArchivoVariable.append(plazoCredito.replace(',',' ')+",");
			contenidoArchivoVariable.append(fechaVencimiento.replace(',',' ')+",");
			contenidoArchivoVariable.append(Comunes.formatoDecimal(montoInteres,2).replace(',',' ')+",");
			contenidoArchivoVariable.append(claveTasa.replace(',',' ')+",");
			contenidoArchivoVariable.append(valorTasa.replace(',',' ')+",");	
			contenidoArchivoVariable.append(tipoconversion.replace(',',' ')+",");					
			contenidoArchivoVariable.append("\n");
		}
		if(tipoArchivo.equals("Fijo")){
			contenidoArchivoFijo.append( Comunes.formatoFijo("D",5," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(numdocto.replace(',',' '),15," ","A"));		 
			contenidoArchivoFijo.append( Comunes.formatoFijo(nombredis.replace(',',' '),100," ","A"));		 
			contenidoArchivoFijo.append( Comunes.formatoFijo(claveEpo.replace(',',' '),5," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(nombreEpo.replace(',',' '),100," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(fechaOperacion.replace(',',' '),20," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(claveMoneda.replace(',',' '),5," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(moneda.replace(',',' '),30," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(Comunes.formatoDecimal(montoCredito,2).replace(',',' '),20," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(plazoCredito.replace(',',' '),20," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(fechaVencimiento.replace(',',' '),15," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(Comunes.formatoDecimal(montoInteres,2).replace(',',' '),15," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(claveTasa.replace(',',' '),10," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(Comunes.formatoDecimal(valorTasa,0).replace(',',' '),15," ","A"));
			//contenidoArchivoFijo.append( Comunes.formatoFijo(numeroCuenta.replace(',',' '),25," ",""));	   //esto queda en duda ya que hay dos tipos de cuenta Epo e IF
			contenidoArchivoFijo.append("\n");  
		}

		if("1".equals(claveMoneda)){
			totalDoctosMN 		++;
			totalMontoMN 		+= montoCredito;		
			totalMontoIntMN		+= montoInteres;
		}else if("54".equals(claveMoneda)){
			totalDoctosUSD			++;
			totalMontoUSD 			+= montoCredito;			
			totalMontoIntUSD		+= montoInteres;
		}
	}

	if(tipoArchivo.equals("Fijo")){
		if(totalDoctosMN>0){
			contenidoArchivoFijo.append("T"+"0"+"0");
			contenidoArchivoFijo.append(totalDoctosMN+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0");
			contenidoArchivoFijo.append(Comunes.formatoDecimal(totalMontoMN,2).replace(',',' ')+"0"+"0"+"0"+"0");
			contenidoArchivoFijo.append(Comunes.formatoDecimal(totalMontoIntMN,2).replace(',',' ')+"0"+"0");
			contenidoArchivoFijo.append("\n");
		}

		if(totalDoctosUSD>0){
			contenidoArchivoFijo.append("T"+"0"+"0");
			contenidoArchivoFijo.append(totalDoctosUSD+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0");
			contenidoArchivoFijo.append(Comunes.formatoDecimal(totalMontoUSD,2).replace(',',' ')+"0"+"0"+"0"+"0");
			contenidoArchivoFijo.append(Comunes.formatoDecimal(totalMontoIntUSD,2).replace(',',' ')+"0"+"0");
			contenidoArchivoFijo.append("\n");
		}
	}

	if(tipoArchivo.equals("Variable")){
		contenidoArchivoGeneral = contenidoArchivoVariable;
		extArchivo= ".csv";
	}
	
	if(tipoArchivo.equals("Fijo")){
		contenidoArchivoGeneral = contenidoArchivoFijo;
		extArchivo= ".txt";
	}

	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = null;

	if(!archivo.make(contenidoArchivoGeneral.toString(), strDirectorioTemp, extArchivo)) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>