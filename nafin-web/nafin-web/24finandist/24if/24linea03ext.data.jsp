<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.distribuidores.*, com.netro.anticipos.*,
		net.sf.json.JSONArray,	net.sf.json.JSONObject, com.netro.pdf.*"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("valoresIniciales"))	{

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

	JSONObject jsonObj = new JSONObject();
	boolean diaHabilUno = false;
	diaHabilUno = lineaCredito.getDiaHabil();
	if(diaHabilUno) {
		jsonObj.put("diaHabilUno", new Boolean(diaHabilUno));
	}

	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoEPODist") ) {

	String tipo_credito = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveIf(iNoCliente);
	if (tipo_credito.equals("F")){
		cat.setTipoCredito("F");
	}
	/*if (tipo_credito.equals("D")){
		cat.setTipoCredito("D");
	}else if (tipo_credito.equals("C")){
		cat.setTipoCredito("C");
	}else if (tipo_credito.equals("C")){
		cat.setTipoCredito("F");
	}*/
	//cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();

}if (informacion.equals("CatalogoDistribuidor")){

	String ic_epo = (request.getParameter("epo")==null)?"":request.getParameter("epo");
	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		if(!strPerfil.equals("IF FACT RECURSO")){
			cat.setTipoCredito("C");
		}
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}else{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("CatalogoEstatusDist")){

	String tipo_credito = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
	if(!"".equals(tipo_credito)) {
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_estatus_linea");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_estatus_linea");
		if (tipo_credito.equals("D")){
			cat.setValoresCondicionIn("3,11", Integer.class);	
		}else{
			if (tipo_credito.equals("C")){
				cat.setValoresCondicionIn("1,3,6,7,8,9,10,12", Integer.class);
			}else{
				cat.setValoresCondicionIn("1,2,3,4,5,6,11", Integer.class);
			}
		}
		infoRegresar = cat.getJSONElementos();
	}else{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("CatalogoEstatusAsignar_A")){	//Aplica para los estatus(ic_estatus_linea) con valor 7 o 9

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

	Vector vecColumnas = null;
	Vector vecFilas	= lineaCredito.getEstatusXAsignar("I");
	
	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	String value	=	"",	desc	=	"";
	for (int i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector)vecFilas.get(i);
		value 		= (String)vecColumnas.get(0);
		desc			= (String)vecColumnas.get(1);
		if("12".equals(value)|| "8".equals(value) ){
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(value);
			elementoCatalogo.setDescripcion(desc);
			coleccionElementos.add(elementoCatalogo);
		}
	}
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar = "{\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString()+"}";

}else if (informacion.equals("CatalogoEstatusAsignar_B")){	//Aplica para los estatus(ic_estatus_linea) con valor 12

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);
	Vector vecColumnas = null;
	Vector vecFilas	= lineaCredito.getEstatusXAsignar("I");
	
	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	String value	=	"",	desc	=	"";
	for (int i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector)vecFilas.get(i);
		value 		= (String)vecColumnas.get(0);
		desc			= (String)vecColumnas.get(1);
		if("7".equals(value) || "8".equals(value) ){
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(value);
			elementoCatalogo.setDescripcion(desc);
			coleccionElementos.add(elementoCatalogo);
		}
	}
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar = "{\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString()+"}";

}else if (informacion.equals("Consulta")	||	informacion.equals("ArchivoCSV")	||	informacion.equals("ArchivoPDF")){

	int start = 0;
	int limit = 0;

	String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
	String	tipo_solic			=	(request.getParameter("tipo_solic")==null)?"":request.getParameter("tipo_solic");
	String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String	ic_estatus_linea	=	(request.getParameter("ic_estatus_linea")==null)?"":request.getParameter("ic_estatus_linea");
	String	dc_fecha_solic_de	=	(request.getParameter("dc_fecha_solic_de")==null)?"":request.getParameter("dc_fecha_solic_de");
	String	dc_fecha_solic_a	=	(request.getParameter("dc_fecha_solic_a")==null)?"":request.getParameter("dc_fecha_solic_a");
	String	dc_fecha_auto_de	=	(request.getParameter("dc_fecha_auto_de")==null)?"":request.getParameter("dc_fecha_auto_de");
	String	dc_fecha_auto_a	=	(request.getParameter("dc_fecha_auto_a")==null)?"":request.getParameter("dc_fecha_auto_a");

	MonitorLineasIfDist monLineas = new MonitorLineasIfDist();
	monLineas.setIc_if(iNoCliente);
	monLineas.setTipoCredito(tipo_credito);
	monLineas.setTipoSolic(tipo_solic);
	monLineas.setIc_epo(ic_epo);
	monLineas.setIc_pyme(ic_pyme);
	monLineas.setIc_estatus(ic_estatus_linea);
	monLineas.setFechaSolic_de(dc_fecha_solic_de);
	monLineas.setFechaSolic_a(dc_fecha_solic_a);
	monLineas.setFechaAuto_de(dc_fecha_auto_de);
	monLineas.setFechaAuto_a(dc_fecha_auto_a);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(monLineas);

	if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

	}else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ArchivoPDF")) {
/*
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
*/
		JSONObject jsonObj = new JSONObject();
		try {
			//String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();
	}

}else if (informacion.equals("ResumenTotales")) {		//Datos para el Resumen de Totales
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(new MonitorLineasIfDist());
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

}else if (informacion.equals("Confirmar")){

	JSONObject jsonObj = new JSONObject();
	
	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

	String folios[]			= request.getParameterValues("folioSolic");
	String estatuses[]		= request.getParameterValues("estatus");
	String tiposCredito[]	= request.getParameterValues("tipoCredito");
	String montoAut[]			= request.getParameterValues("montoAut");
	String saldoTot[]			= request.getParameterValues("saldoTot");
	String fechaVence[]		= request.getParameterValues("fechaVence");
	String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	
	String folioSolic		= "";
	String tipoCredito	= "";
	String tipoSol 		= "";
	String numEpo			= "";
	String epo 				= "";
	String numPyme 		= "";
	String pyme 			= "";
	String folioSolicRel	= "";
	String fechaSol 		= "";
	String fechaAuto		= "";
	String moneda 			= "";
	double mtoSol 			= 0;
	String estatus 		= "";
	String causas 			= "";
	String plazo 			= "";
	String fechaVenc 		= "";
	String bancoServ 		= "";
	String bcoEpo 			= "";
	String bcoIf 			= "";
	double montoAuto 		= 0;
	String icMoneda		= "";
	String ctaBancaria	= "";
	String usuarioMod		= "";
	
	int 	totalLineasMN	= 0;
	int 	totalLineasUSD	= 0;
	double	totalMtoAutMN	= 0;
	double	totalMtoAutUSD	= 0;
	String msg="";
	int i = 0;
	Vector vecColumnas = null;
	Vector vecFilas = null;
	if(!strPerfil.equals("IF FACT RECURSO")){
		vecFilas = lineaCredito.cambiaEstatusLinea(folios,estatuses,tiposCredito);
	}else{
		vecFilas = lineaCredito.cambiaEstatusLinea(folios,estatuses,tiposCredito,montoAut,saldoTot,fechaVence);
	}

	try {
		List newReg = new ArrayList();
		List totalReg = new ArrayList();

		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual_a.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
		String anioActual   = fechaActual_a.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		int nCols=0;
		if(!strPerfil.equals("IF FACT RECURSO")){
			nCols = 11;
		}else{
			nCols = 13;
		}
		pdfDoc.setTable(nCols, 100);
		pdfDoc.setCell("Núm. EPO","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Folio de solicitud","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo de crédito","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto autorizado","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Saldo disponible","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha autorización","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Estatus Nuevo","celda01",ComunesPDF.CENTER);
		if(!strPerfil.equals("IF FACT RECURSO")){
			pdfDoc.setCell("Fecha de cambio de estatus","celda01",ComunesPDF.CENTER);
		}else{
			pdfDoc.setCell("Cuenta Bancaria","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Usuario última modificación","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha y hora última modificación","celda01",ComunesPDF.CENTER);
		}

		for(i=0;i<vecFilas.size();i++){
			vecColumnas 	= (Vector)vecFilas.get(i);
			HashMap hash = new HashMap();
			folioSolic		= (String)vecColumnas.get(0);
			tipoCredito		= (String)vecColumnas.get(1);
			tipoSol 			= (String)vecColumnas.get(2);
			numEpo			= (String)vecColumnas.get(3);
			epo 				= (String)vecColumnas.get(4);
			numPyme 			= (String)vecColumnas.get(5);
			pyme 				= (String)vecColumnas.get(6);
			folioSolicRel	= (String)vecColumnas.get(7);
			fechaSol 		= (String)vecColumnas.get(8);
			fechaAuto		= (String)vecColumnas.get(9);
			moneda 			= (String)vecColumnas.get(10);
			estatus 			= (String)vecColumnas.get(11);
			causas 			= (String)vecColumnas.get(12);
			plazo 			= (String)vecColumnas.get(13);
			fechaVenc 		= (String)vecColumnas.get(14);
			bancoServ 		= (String)vecColumnas.get(15);
			bcoEpo 			= (String)vecColumnas.get(16);
			bcoIf 			= (String)vecColumnas.get(17);
			montoAuto 		= Double.parseDouble(vecColumnas.get(18).toString());
			mtoSol 			= Double.parseDouble(vecColumnas.get(19).toString());
			icMoneda			= (String)vecColumnas.get(20);
			if(strPerfil.equals("IF FACT RECURSO")){
				ctaBancaria = (String)vecColumnas.get(23);
				usuarioMod  = (String)vecColumnas.get(24);
				fechaHoy		= (String)vecColumnas.get(25);
			}

			if("1".equals(icMoneda)){
				totalLineasMN++;
				totalMtoAutMN += montoAuto;
			}else if("54".equals(icMoneda)){
				totalLineasUSD++;
				totalMtoAutUSD += montoAuto;
			}
			hash.put("FOLIO",folioSolic);
			hash.put("TIPOCREDITO",tipoCredito);
			hash.put("TIPO_SOL",tipoSol);
			hash.put("NUMEPO",numEpo);
			hash.put("EPO",epo);
			hash.put("NUMPYME",numPyme);
			hash.put("PYME",pyme);
			hash.put("FOLIOSOL_RELAC",folioSolicRel);
			hash.put("FECHA_SOL",fechaSol);
			hash.put("FECHA_AUTO",fechaAuto);
			hash.put("MONEDA",moneda);
			hash.put("ESTATUS",estatus);
			hash.put("CAUSAS",causas);
			hash.put("PLAZO",plazo);
			hash.put("FECHA_VENC",fechaVenc);
			hash.put("BANCO_SERV",bancoServ);
			hash.put("BANCO_EPO",bcoEpo);
			hash.put("BANCO_IF",bcoIf);
			hash.put("MONTO_AUTO",Double.toString(montoAuto));
			hash.put("MONTO_SOL",Double.toString(mtoSol));
			hash.put("IC_MONEDA",icMoneda);
			hash.put("FECHA_HOY",fechaHoy);
			if(strPerfil.equals("IF FACT RECURSO")){
				hash.put("IG_CUENTA_BANCARIA",ctaBancaria);
				hash.put("CG_USUARIO_CAMBIO",usuarioMod);
			}

			newReg.add(hash);
			pdfDoc.setCell(numEpo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(folioSolic,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoAuto,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(mtoSol,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaAuto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVenc,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
			if(strPerfil.equals("IF FACT RECURSO")){
				pdfDoc.setCell(ctaBancaria,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(usuarioMod,"formas",ComunesPDF.CENTER);
			}
			pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.CENTER);
		}
		if(totalLineasMN> 0){
			pdfDoc.setCell("Total M.N.","celda01",ComunesPDF.CENTER,2);
			pdfDoc.setCell(Integer.toString(totalLineasMN),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMtoAutMN,2),"formas",ComunesPDF.CENTER);
			if(strPerfil.equals("IF FACT RECURSO")){
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,8);
			}else{
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
			}
		}
		if(totalLineasUSD> 0){
			pdfDoc.setCell("Total Dólares","celda01",ComunesPDF.CENTER,2);
			pdfDoc.setCell(Integer.toString(totalLineasUSD),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMtoAutUSD,2),"formas",ComunesPDF.CENTER);
			if(strPerfil.equals("IF FACT RECURSO")){
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,8);
			}else{
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
			}
		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		
		jsonObj.put("regs", newReg);
		HashMap hashTot = new HashMap();
		hashTot.put("numRegistrosMN",Integer.toString(totalLineasMN));
		hashTot.put("numRegistrosDL",Integer.toString(totalLineasUSD));
		hashTot.put("montoTotalMN","$ "+Comunes.formatoDecimal(totalMtoAutMN,2));
		hashTot.put("montoTotalDL","$ "+Comunes.formatoDecimal(totalMtoAutUSD,2));
		totalReg.add(hashTot);
		jsonObj.put("totales", totalReg);
		msg="ok";
	} catch (Exception e) {
		msg = "Error al obtener la información actualizada"+e;
	} finally {
		//Termina proceso de imprimir
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ConsultaInhabil")	||	informacion.equals("ArchivoCSV_inhabil")	||	informacion.equals("ArchivoPDF_inhabil")){

	MonitorLineasIfDist_inhabil monLineasIn = new MonitorLineasIfDist_inhabil();
	monLineasIn.setIc_if(iNoCliente);
	if(strPerfil.equals("IF FACT RECURSO")){
		monLineasIn.setTipoCredito("F");
	}else{
		monLineasIn.setTipoCredito("");
	}

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(monLineasIn);
	Registros reg = queryHelper.doSearch();
	
	if (informacion.equals("ConsultaInhabil")) {
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			reg = queryHelper.doSearch();
			infoRegresar =  "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData() + "}";

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

	}else if (informacion.equals("ArchivoCSV_inhabil")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ArchivoPDF_inhabil")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}
}

%>
<%=infoRegresar%>
