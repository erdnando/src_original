<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.sql.*, java.math.*,
	com.netro.distribuidores.*,com.netro.exception.*, javax.naming.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj	=	new JSONObject();
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String	ic_epo		=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String	ic_pyme		=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String	fecha_ini	=	(request.getParameter("Txtfchini")==null)?"":request.getParameter("Txtfchini");
String	fecha_fin	=	(request.getParameter("Txtffin")==null)?"":request.getParameter("Txtffin");
String	tipoConsulta	=	(request.getParameter("tipoConsulta")==null)?"":request.getParameter("tipoConsulta");
String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");

String epo				= "";
String distribuidor		= "";
String numdocto			= "";
String acuse			= "";
String fchemision		= "";
String fchpublica		= "";
String fchvence			= "";
String plazodocto		= "";
String ic_moneda		= "";
String moneda			= "";
double monto			= 0;
String tipoconversion	= "";
String tipocambio		= "";
double mntovaluado		= 0;
String plzodescto		= "";
String porcdescto		= "";
double mntodescuento	= 0;
String estatus			= "";
String numcredito		= "";
String descif			= "";
String referencia		= "";
String plazocred		= "";
String fchvenccred		= "";
String tipocobroint		= "";
String modoPlazo		= "";
double montoint			= 0;
String valortaza		= "";
double mntocredito		= 0;
String fchopera			= "";
String monedaLinea		= "";
int    totalDoctosMN	= 0;
double imontoTot		= 0;
double imontoValTot		= 0;
double imontodescTot	= 0;
double imontocredTot	= 0;
double imontotasaTot	= 0;

int totalDoctosUSD		= 0;
double imontoTotDls		= 0;
double imontoValTotDls	= 0;
double imontodescTotDls	= 0;
double imontocredTotDls	= 0;
double imontotasaTotDls	= 0;

try {
	if(informacion.equals("ArchivoXpaginaPDF")  || informacion.equals("ArchivoTotalPDF")	){
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
		int start = 0;
		int limit = 0;
		int numreg = 0;
		int numCols = 19;
		Registros reg = new Registros();

//Inicia creacion de archivo*-*-*-*-*-*-*-*-*-*-*-*-
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);

		AvNotifIfDist notif = new AvNotifIfDist();
		notif.setIc_if(iNoCliente);
		notif.setIc_epo(ic_epo);
		notif.setIc_pyme(ic_pyme);
		notif.setFechaInicial(fecha_ini);
		notif.setFechaFinal(fecha_fin);
		notif.setTipoConsulta(tipoConsulta);
		notif.setStrPerfil(strPerfil);
		notif.setNumOrden(numOrden);
      if(strPerfil.equals("IF FACT RECURSO")){
         notif.setTipoCredito("F");
      }else{
         notif.setTipoCredito("");
      }

		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(notif);

		if (informacion.equals("ArchivoXpaginaPDF"))	{	//-*-*-*-*-*-*-	Obtiene los registros por pagina del primer grid
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			reg = queryHelper.getPageResultSet(request,start,limit);
		}else{
			reg = queryHelper.doSearch();
		}
		/***************************/	/* Generacion del archivo *//***************************/
		while (reg.next()) {
			ic_moneda		= (reg.getString("ic_moneda")==null)?"":reg.getString("ic_moneda");	
			distribuidor	= (reg.getString("DISTRIBUIDOR")==null)?"":reg.getString("DISTRIBUIDOR");
			numdocto			= (reg.getString("ig_numero_docto")==null)?"":reg.getString("ig_numero_docto");
			acuse				= (reg.getString("cc_acuse")==null)?"":reg.getString("cc_acuse");
			fchemision		= (reg.getString("df_fecha_emision")==null)?"":reg.getString("df_fecha_emision");
			fchpublica		= (reg.getString("df_fecha_publicacion")==null)?"":reg.getString("df_fecha_publicacion");		
			fchvence			= (reg.getString("df_fecha_venc")==null)?"":reg.getString("df_fecha_venc");
			plazodocto		= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
			moneda			= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
			monto				= Double.parseDouble(reg.getString("FN_MONTO"));
			tipoconversion	= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
			tipocambio		= (reg.getString("TIPO_CAMBIO")==null)?"":reg.getString("TIPO_CAMBIO");
			plzodescto		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
			porcdescto		= (reg.getString("fn_porc_descuento")==null)?"":reg.getString("fn_porc_descuento");
			mntodescuento	= Double.parseDouble(reg.getString("MONTO_DESCUENTO"));
			mntovaluado		= (monto-mntodescuento)*Double.parseDouble(tipocambio);
			estatus			= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
         if(!strPerfil.equals("IF FACT RECURSO")){
            modoPlazo		= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
         }
			monedaLinea		= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
			epo				= (reg.getString("EPO")==null)?"":reg.getString("EPO");
			/**********PARA CREDITO**************/
			numcredito		=(reg.getString("ic_documento")==null)?"":reg.getString("ic_documento");
			numcredito		=(reg.getString("ic_documento")==null)?"":reg.getString("ic_documento");
			descif			= (reg.getString("DIF")==null)?"":reg.getString("DIF");
			referencia		= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
			plazocred		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
			fchvenccred		= (reg.getString("DF_FECHA_VENC_CREDITO")==null)?"":reg.getString("DF_FECHA_VENC_CREDITO");
			tipocobroint	= (reg.getString("tipo_cobro_interes")==null)?"":reg.getString("tipo_cobro_interes");
			montoint			= Double.parseDouble(reg.getString("MONTO_INTERES"));
			valortaza		= (reg.getString("VALOR_TASA")==null)?"":reg.getString("VALOR_TASA");
			mntocredito		= Double.parseDouble(reg.getString("MONTO_CREDITO"));
			fchopera			= (reg.getString("df_fecha_hora")==null)?"":reg.getString("df_fecha_hora");
			if(ic_moneda.equals("1")){
				totalDoctosMN++;
				imontoTot+= monto; 
				imontoValTot+= mntovaluado;
				imontodescTot+= mntodescuento;
				imontocredTot+= mntocredito; 
				imontotasaTot+= montoint; 		
			}
			if(ic_moneda.equals("54")){
				totalDoctosUSD++;
				imontoTotDls+= monto;
				if(ic_moneda.equals(monedaLinea))
					imontoValTotDls+= mntovaluado;
				imontodescTotDls+= mntodescuento;
				imontocredTotDls+= mntocredito; 
				imontotasaTotDls+= montoint;
			}
			if(numreg == 0){
				float anchos[] = new float[numCols];
				for(int x=0;x<numCols;x++) {
					if(x==0)
						anchos[x] = .35f;
					else
						anchos[x] = 1f;
						
					if(x==11||x==3)
						anchos[x] = 1.45f;
					if(x==5||x==6||x==7||x==8)
						anchos[x] = 1.15f;
				}
				pdfDoc.setTable(numCols,100,anchos);
				pdfDoc.setCell("Datos del Documento Inicial","celda01rep",ComunesPDF.CENTER,numCols);
				pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Dist.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Docto. Inicial","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Emisión","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Oper.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo docto.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo Dscto Días","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("% Dscto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto % Dscto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo conv.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo cambio","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto valuado","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Modo Plazo","celda01rep",ComunesPDF.CENTER);
				
				pdfDoc.setCell("Datos Documento Final","celda01rep",ComunesPDF.CENTER,numCols);
				pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Docto Final","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Ref. Tasa Interés","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor Tasa Interés","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Interés","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Cobro Interés","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,numCols-10);
			}
		/*************************/ /* Contenido del archivo */ /**************************/
			pdfDoc.setCell("A","formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(epo,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(distribuidor,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(numdocto,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(acuse,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchemision,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchpublica,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchvence,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchopera,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(plazodocto,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(plzodescto,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(porcdescto,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(mntodescuento,2),"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell((ic_moneda.equals(monedaLinea))?"":tipoconversion,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell((ic_moneda.equals(monedaLinea))?"":tipocambio,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell((ic_moneda.equals(monedaLinea))?"":"$ "+Comunes.formatoDecimal(mntovaluado,2),"formasrep",ComunesPDF.CENTER);
         if(!strPerfil.equals("IF FACT RECURSO")){
            pdfDoc.setCell(modoPlazo,"formasrep",ComunesPDF.CENTER);
         }

			pdfDoc.setCell("B","formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(estatus,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(numcredito,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(mntocredito,2),"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(plazocred,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchvenccred,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(referencia,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(valortaza,2)+" %","formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(montoint,2),"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(tipocobroint,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,numCols-10);
			numreg++;
		} //fin del while

		/* Se queda pendiente esta parte ya que en los archivos originales no se muestran los totales.....
		int i = 0;
		Registros vecTotales = new Registros();
		if(numreg>0)
			vecTotales = queryHelper.getResultCount(request);

			while(vecTotales.next()){
				String moneda_ic	=	(vecTotales.getString("MONEDA")==null)?"":vecTotales.getString("MONEDA");
				String nommoneda	=	(vecTotales.getString("NOMMONEDA")==null)?"":vecTotales.getString("NOMMONEDA");
				String totReg		=	(vecTotales.getString(2)==null)?"":vecTotales.getString(2);
				String totMonto	=	(vecTotales.getString(3)==null)?"":vecTotales.getString(3);
				String montoDesc	=	(vecTotales.getString(4)==null)?"":vecTotales.getString(4);
				String montoCred	=	(vecTotales.getString(5)==null)?"":vecTotales.getString(5);
				String montoInte	=	(vecTotales.getString(5)==null)?"":vecTotales.getString(6);
				pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("TOTAL "+nommoneda,"celda01rep",ComunesPDF.CENTER,3);
				pdfDoc.setCell(totReg,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 6);
				pdfDoc.setCell(Comunes.formatoMN(totMonto),"formasrep",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 1);
				pdfDoc.setCell(Comunes.formatoMN(montoDesc),"formasrep",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 3);
				
				pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 2);
				pdfDoc.setCell(Comunes.formatoMN(montoCred),"formasrep",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 3);
				pdfDoc.setCell(Comunes.formatoMN(montoInte),"formasrep",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,numCols-10);
			}//End-while
		*/
		if (numreg == 0)	{
			pdfDoc.addText(" ","formas",ComunesPDF.LEFT);
			pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
		}else	{
			pdfDoc.addTable();
		}
		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>