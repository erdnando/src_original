	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,  
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONObject,
	net.sf.json.JSONArray"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/15cadenas/015secsession.jspf" %>

<%
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String informacion         =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String ic_epo					=(request.getParameter("HicEpo")    != null) ?   request.getParameter("HicEpo") :"";
	String ctCredito				=(request.getParameter("HcdCredito")    != null) ?   request.getParameter("HcdCredito") :"";
	String icDist					=(request.getParameter("HicDist")    != null) ?   request.getParameter("HicDist") :"";
	String numDoc					=(request.getParameter("numCred")    != null) ?   request.getParameter("numCred") :"";
	String fechaOper1				=(request.getParameter("fechaOper1")    != null) ?   request.getParameter("fechaOper1") :"";
	String fechaOper2				=(request.getParameter("fechaOper2")    != null) ?   request.getParameter("fechaOper2") :"";
	String monto1					=(request.getParameter("monto1")    != null) ?   request.getParameter("monto1") :"";
	String monto2					=(request.getParameter("monto2")    != null) ?   request.getParameter("monto2") :"";
	String rechazo					=(request.getParameter("Hrechazo")    != null) ?   request.getParameter("Hrechazo") :"";
	String fechaVenc1				=(request.getParameter("fechaVenc1")    != null) ?   request.getParameter("fechaVenc1") :"";
	String fechaVenc2				=(request.getParameter("fechaVenc2")    != null) ?   request.getParameter("fechaVenc2") :"";
	String tipoCoInt				=(request.getParameter("HtipoCoInt")    != null) ?   request.getParameter("HtipoCoInt") :"";
	String operacion        	=(request.getParameter("operacion")    != null) ?   request.getParameter("operacion") :"";
	String fechaRe1				=(request.getParameter("fechaRe1")    != null) ?   request.getParameter("fechaRe1") :"";
	String fechaRe2				=(request.getParameter("fechaRe2")    != null) ?   request.getParameter("fechaRe2") :"";

	JSONObject resultado 	      = new JSONObject();
	 
	
	if(informacion.equals("catologoEpo")){
	
		CatalogoEPODistribuidores catalogo = new CatalogoEPODistribuidores();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveIf(iNoCliente);
		catalogo.setTipoCredito(ctCredito);
		infoRegresar=catalogo.getJSONElementos();
	}else if(informacion.equals("catologoDist")){
	
		CatalogoPymeDistribuidores catalogo = new CatalogoPymeDistribuidores();
		catalogo.setCampoClave("cp.ic_pyme");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveEpo(ic_epo);
		infoRegresar=catalogo.getJSONElementos();
	}else  if (informacion.equals("CatalogoTipoCobroDist")){

		String rs_epo_tipo_cobro_int	=	"1,2";
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_tipo_cobro_interes");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_tipo_cobro_interes");
		cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
		infoRegresar = cat.getJSONElementos();

}else  if (informacion.equals("CatalogoRechazo")){

		String rs_rechazo	=	"23,34,2";
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_cambio_estatus");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_cambio_estatus");
		cat.setValoresCondicionIn(rs_rechazo, Integer.class);
		infoRegresar = cat.getJSONElementos();
}else if(informacion.equals("Consulta")||informacion.equals("ArchivoPaginaPDF")||informacion.equals("ArchivoCSV")){
		int start=0,limit=0;
		ConsNoOperIfDist paginador=new ConsNoOperIfDist();
		paginador.setINoCliente(iNoCliente);
		paginador.setCtCredito(ctCredito);
		paginador.setIc_epo(ic_epo);
		paginador.setIcDist(icDist);
		paginador.setNumDoc(numDoc);
		paginador.setFechaOper1(fechaOper1);
		paginador.setFechaOper2(fechaOper2);
		paginador.setMonto1(monto1);
		paginador.setMonto2(monto2);
		paginador.setRechazo(rechazo);
		paginador.setFechaVenc1(fechaVenc1);
		paginador.setFechaVenc2(fechaVenc2);
		paginador.setTipoCoInt(tipoCoInt);
		paginador.setFechaRe1(fechaRe1);
		paginador.setFechaRe2(fechaRe2);
				
		
		
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
	
		if (informacion.equals("Consulta")){
			try{
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));
			}catch(Exception e){
					System.out.println("Error en parametros");
			}
			if (operacion.equals("Generar")) {	//Nueva consulta
				cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				
		   }
			
			Registros registros = cqhelper.getPageResultSet(request,start,limit);
			String consulta2	=	"{\"success\": true, \"total\": \"" + cqhelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
			resultado = JSONObject.fromObject(consulta2);
			infoRegresar = resultado.toString();
		}else	 if(informacion.equals("ArchivoPaginaPDF")){
			jsonObj = new JSONObject();
			try{
				//start = Integer.parseInt(request.getParameter("start"));
				//limit = Integer.parseInt(request.getParameter("limit"));
				//String nombreArchivo = cqhelper.getCreatePageCustomFile(request, start, limit,strDirectorioTemp, "PDF");
				String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			}catch(Throwable e){
				jsonObj.put("success", new Boolean(false));
				throw new AppException("Error al generar el archivo PDF", e);
			}
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("ArchivoCSV")){
			String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}

}else if (informacion.equals("Totales")){
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesión
		infoRegresar = queryHelper.getJSONResultCount(request);
} 

%>

<%=infoRegresar%>