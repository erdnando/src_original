<%@ page
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		com.netro.distribuidores.*,
		netropology.utilerias.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>

<%
	String infoRegresar	=	"";
	JSONObject jsonObj = new JSONObject();
	String nombreArchivo 	= (request.getParameter("archivo")	== null)?"":request.getParameter("archivo");	
	try{
		java.io.File 					file 					= 	new java.io.File(strDirectorioTemp+nombreArchivo);
		
                CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
		
		String archivo6 = leerArchivo(new FileInputStream(file));
		
		int totalReg =0,	noLinea= 0,	indice = 0,	sinErrores = 0,	conErrores = 0;
		String indice1="",	totalReg2 ="",	documento="", estatus="", causa="";
		List camposLineaArchivo = new ArrayList();
		List procesar = new ArrayList();
		List detallerrores = new ArrayList();
		List respuesta  = new ArrayList();
		List error = new ArrayList();
		List proce = new ArrayList();
		double porcentaje = 0;
		
		if(!archivo6.equals("")){

				List LineaArchivo = new VectorTokenizer(archivo6, "\n").getValuesVector();
				totalReg = LineaArchivo.size()-1; //total de registros
				totalReg2 = String.valueOf(totalReg);
							
				for(int i = 0; i < LineaArchivo.size()-1; i++) 	{
					//lee linea por linea y la procesa
					if(LineaArchivo.get(i)!=null){
						camposLineaArchivo = new ArrayList();
						camposLineaArchivo = new VectorTokenizer((String)LineaArchivo.get(i), "|").getValuesVector();
						
						documento=""; estatus=""; causa=""; 								
						documento   = (camposLineaArchivo.size()>= 1)?camposLineaArchivo.get(0)==null?"":((String)camposLineaArchivo.get(0)).trim():"";
						estatus   = (camposLineaArchivo.size()>= 2)?camposLineaArchivo.get(1)==null?"":((String)camposLineaArchivo.get(1)).trim():"";
						causa   = (camposLineaArchivo.size()>= 3)?camposLineaArchivo.get(2)==null?"":((String)camposLineaArchivo.get(2)).trim():"";
							
						noLinea = i+1;
						indice ++;
						indice1 =  String.valueOf(indice);
							
							//metodo q valida los datos capturados en la linea del archivo
						respuesta  = new ArrayList();
						respuesta =  cargaDocto.ValidaLineaRechazo( documento, estatus, causa, noLinea) ;

						//saca el porcentaje de la pantalla
						porcentaje =   (Double.parseDouble((String)indice1) / Double.parseDouble((String)totalReg2) )* 100;
						request.setAttribute("porcentaje",Double.toString(porcentaje));

						error = new ArrayList();
						proce = new ArrayList();

						if(respuesta.size()>0){

							for(int n =0; n<respuesta.size()-1; n++){

								List error1 = (List)respuesta.get(0);	
								List proce1 = (List)respuesta.get(1);

								if(error1.size()>0 ){
										error = (List)respuesta.get(0);
								}
								if(proce1.size()>0){
									proce = (List)respuesta.get(1);
								}
							}//for(int n =0; n<respuesta.size()-1; n++){

							if(error.size()>0){
								detallerrores.add(error);
							}
							if(proce.size()>0){
								procesar.add(proce);
							}
						}//if(respuesta.size()>0)
					}//if(LineaArchivo.get(i)!=null){
				}	//for(int i = 0; i < LineaArchivo.size()-1; i++)
			}

			sinErrores = totalReg-detallerrores.size();  // numero de documentos sin errores
			conErrores  = detallerrores.size();  // numero de documentos con errores
			String SnombreArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf("\\")+1, nombreArchivo.length());

			//regresa los valores
			jsonObj.put("nombreArchivo",nombreArchivo);
			jsonObj.put("totalRegistros",Integer.toString(totalReg));
			jsonObj.put("sinErrores",Integer.toString(sinErrores));
			jsonObj.put("conErrores",Integer.toString(conErrores));
			jsonObj.put("aprocesar",procesar);
			jsonObj.put("detalleErrores",detallerrores);
			jsonObj.put("SnombreArchivo",SnombreArchivo);
	}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
	}
	System.out.println("************** datos "+jsonObj.toString());

%>
<%!
	private String leerArchivo(InputStream archivo) throws AppException{
		InputStreamReader isR = null;
		BufferedReader bis    = null;
		String lineaArchivo   = "";
		StringBuffer archivoS = new StringBuffer();
		try{
			isR = new InputStreamReader(archivo, "ISO-8859-1");
			bis = new BufferedReader(isR);
			lineaArchivo = bis.readLine();
			while(lineaArchivo!=null){
			  archivoS.append(lineaArchivo+"\n");
			  lineaArchivo = bis.readLine();  
			}
		}catch(Exception e){
			throw new AppException("Error Inesperado", e);
		}
		return archivoS.toString();
	}
%>



{"success": true,"infoRegresar":<%=jsonObj.toString()%>}
