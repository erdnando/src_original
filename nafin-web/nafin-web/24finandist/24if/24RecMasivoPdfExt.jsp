<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	com.netro.exception.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String regArchivo		=	(request.getParameter("regArchivo")!=null)?request.getParameter("regArchivo"):"";
String regProcesados	=	(request.getParameter("regProcesados")!=null)?request.getParameter("regProcesados"):"";
String noProceso		=	(request.getParameter("noProceso")!=null)?request.getParameter("noProceso"):"";
String nombreArchivo_b=	(request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";
String fechaCarga		=	(request.getParameter("fechaCarga")!=null)?request.getParameter("fechaCarga"):"";

JSONObject jsonObj = new JSONObject();

try {
	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

	pdfDoc.addText("Resumen proceso Cambio de Estatus Masivo","formas",ComunesPDF.CENTER);
	pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
	
	pdfDoc.setTable(2, 50);
	pdfDoc.setCell("Número de Registros del Archivo:","celda01",ComunesPDF.LEFT);
	pdfDoc.setCell(regArchivo,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Registros Procesados:","celda01",ComunesPDF.LEFT);
	pdfDoc.setCell(regProcesados,"formas",ComunesPDF.CENTER);
	pdfDoc.addTable();
	
	pdfDoc.setTable(3, 50);
	pdfDoc.setCell("Número de Proceso","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Nombre Archivo","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Carga de Archivo","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(noProceso,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell(nombreArchivo_b,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell(fechaCarga,"formas",ComunesPDF.CENTER);
	pdfDoc.addTable();

	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>