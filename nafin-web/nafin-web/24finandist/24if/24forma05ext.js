Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

	var strPerfil = Ext.get('_strPerfil').getValue();
	var form = {Maxpuntos:null, Cadena:null, relMat:null, puntos:null}
	var edita=false;

	function procesaModificarPuntos(opts, success, response) {		
		var fp = Ext.getCmp('forma');		
				
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			Ext.Msg.alert("Aviso","El registro se guardo con �xito",
				function(){
				//	window.location = '24forma05ext.jsp';
				});			
				
			fp.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function procesaConfirmaTasas(opts, success, response) {
		pnl.el.unmask();
		Ext.getCmp('btnConfirmaTasas').setIconClass('autorizar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			window.location = '24forma05ext.jsp';
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaGuardarTasas(opts, success, response) {
		pnl.el.unmask();
		Ext.getCmp('btnGuardar').setIconClass('icoGuardar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert("Aviso","El registro se guardo con �xito",
			function(){
				window.location = '24forma05ext.jsp';
			});
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaRemoverTasas(opts, success, response) {
		pnl.el.unmask();
		Ext.getCmp('btnRemover').setIconClass('borrar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert("Aviso","El registro fue eliminado",
			function(){
				window.location = '24forma05ext.jsp';
			});
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function editarRegistro(camposEditar){
		grid.hide();
		//Ext.getCmp('ic_moneda').show();
		Ext.getCmp('ic_moneda').setValue(camposEditar.IC_MONEDA);
		var dato = Ext.getCmp('in_PlazoDias').getStore().findExact("clave", camposEditar.CLAVE_PLAZO);
		if (dato != -1 ) {
			Ext.getCmp('in_PlazoDias').show();
			Ext.getCmp('in_PlazoDias').setValue(camposEditar.CLAVE_PLAZO);
			Ext.getCmp('in_PlazoDias').setReadOnly(true);
		}
		Ext.getCmp('pRelMat').setValue(camposEditar.REL_MAT);
		Ext.getCmp('pRelMat').show();
		Ext.getCmp('pPuntos').show();
		Ext.getCmp('pPuntos').setValue(camposEditar.PUNTOS_ADD);
		Ext.getCmp('btnGuardar').show();
		Ext.getCmp('btnCancelar').show();
		edita = true;
		Ext.getCmp('claveTasa').setValue(camposEditar.CLAVE_TASA);
	}

	function addRegistroFactoraje(){
		grid.hide();
		var store = grid.getStore();
		var cadenaPlazos = "";
		var x = 0;
		store.each(function(record) {
			if (x > 0 ) {cadenaPlazos += ",";}
			cadenaPlazos += record.data['CLAVE_PLAZO'];
			x++;
		});

		var comboPlazo = Ext.getCmp('in_PlazoDias');
		comboPlazo.setValue('');
		comboPlazo.store.removeAll();
		comboPlazo.store.reload({
			params: {
				_ic_epo: Ext.getCmp('ic_epo').getValue(),
				cadPlazos:	cadenaPlazos
			}
		});
		Ext.getCmp('in_PlazoDias').show();
		Ext.getCmp('pRelMat').show();
		Ext.getCmp('pPuntos').show();
		Ext.getCmp('btnAgregar').show();
		Ext.getCmp('btnCancelar').show();
		store.removeAll();
	}

	function procesaAgregaTasas(opts, success, response) {
		pnl.el.unmask();
		if (strPerfil != "IF FACT RECURSO"){
			consultaData.removeAll();
		}
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (!grid.isVisible()){
				grid.show();
			}
			if (infoR.regs != undefined && infoR.regs.length > 0){
				if (strPerfil === "IF FACT RECURSO"){
					consultaData.loadData(infoR.regs, true);
					var cm = grid.getColumnModel();
					cm.setHidden(cm.findColumnIndex('FLAG_CHECK'),true);
					cm.setHidden(cm.findColumnIndex('CG_USUARIO_CAMBIO'),false);
					cm.setHidden(cm.findColumnIndex('FECHA_CAMBIO'),false);
					Ext.getCmp('btnEditar').setVisible(false);
					Ext.getCmp('btnRemover').setVisible(false);
					Ext.getCmp('btnAgregarFactoraje').setVisible(false);
					Ext.getCmp('in_PlazoDias').setValue('');
					Ext.getCmp('pPuntos').setValue('');
					Ext.getCmp('pPuntos').clearInvalid();
				}else{
					consultaData.loadData(infoR.regs);
				}
				grid.getGridEl().unmask();
				Ext.getCmp('barraGrid').setVisible(true);
				Ext.getCmp('btnConfirmaTasas').setVisible(true);
			}else{
				grid.getGridEl().mask('No hay tasas disponibles para esta EPO', 'x-mask');
				Ext.getCmp('barraGrid').setVisible(false);
				Ext.getCmp('btnConfirmaTasas').setVisible(false);
			}
			if(infoR.lsCadena != undefined){
				form.Cadena = infoR.lsCadena;
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaConsultaTasas(opts, success, response) {
		pnl.el.unmask();
		consultaData.removeAll();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.liRegistros != undefined && infoR.liRegistros === '0'){
				Ext.getCmp('in_PlazoDias').show();
				Ext.getCmp('pRelMat').show();
				Ext.getCmp('pPuntos').reset();
				Ext.getCmp('pPuntos').show();
				Ext.getCmp('btnAgregar').show();
				Ext.getCmp('btnLimpiar').show();
				if (infoR.alerta != undefined){
					Ext.Msg.alert('',infoR.alerta);
				}
			}else{
				if (!grid.isVisible()){
					grid.show();
				}
				if (infoR.regs != undefined && infoR.regs.length > 0){
					if (strPerfil === "IF FACT RECURSO"){
						var cm = grid.getColumnModel();
						cm.setHidden(cm.findColumnIndex('FLAG_CHECK'),false);
						cm.setHidden(cm.findColumnIndex('CG_USUARIO_CAMBIO'),false);
						cm.setHidden(cm.findColumnIndex('FECHA_CAMBIO'),false);
						Ext.getCmp('barraGrid').setVisible(true);
						Ext.getCmp('btnEditar').setVisible(true);
						Ext.getCmp('btnRemover').setVisible(true);
						Ext.getCmp('btnAgregarFactoraje').setVisible(true);
					}
					consultaData.loadData(infoR.regs);
					grid.getGridEl().unmask();
					Ext.getCmp('btnGuardarG').show();
					Ext.getCmp('barraGrid').setVisible(true);
				}else{
					grid.getGridEl().mask('No hay tasas disponibles para esta EPO', 'x-mask');
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaComboConsulta(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (parseFloat(infoR.TotalLineas)==0 ){
				Ext.getCmp('ic_moneda').setValue("1");
			}else{
				Ext.getCmp('ic_moneda').show();
			}
			if(!edita){
				Ext.Ajax.request({
					url: '24forma05ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{informacion: "ConsultaTasas"}),
					callback: procesaConsultaTasas
				});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if ( Ext.util.JSON.decode(response.responseText).Maxpuntos != undefined){ form.Maxpuntos = Ext.util.JSON.decode(response.responseText).Maxpuntos;}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var catalogoRelacionMatData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['+','+'],
			['-','-']
		 ]
	});

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma05ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma05ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDistribuidor'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesarCatalogoMoneda = function(store, arrRegistros, opts) {
		var dato = catalogoMonedaData.findExact('clave', '1');
		if (dato != -1 ) {
			Ext.getCmp('ic_moneda').setValue('1');
		}
	}

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma05ext.data.jsp',
		baseParams: {	informacion: 'CatalogoMonedaDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesarCatalogoMoneda,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesarCatalogoPlazo = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('in_PlazoDias').valueNotFoundText = '';
		}else{
			Ext.getCmp('in_PlazoDias').valueNotFoundText = 'Plazos no disponibles para operar';
		}
		Ext.getCmp('in_PlazoDias').reset();
	}

	var catalogoPlazoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma05ext.data.jsp',
		baseParams: {	informacion: 'CatalogoPlazo'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load: procesarCatalogoPlazo,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name:'IC_MONEDA'},
			{name:'CLAVE_TASA'},
			{name:'MONEDA'},
			{name:'TASA_INTERES'},
			{name:'CLAVE_PLAZO'},
			{name:'PLAZO'},
			{name:'VALOR'},
			{name:'REL_MAT'},
			{name:'PUNTOS_ADD'},
			{name:'TASA_APLICAR'},
			{name:'CG_USUARIO_CAMBIO'},
			{name:'FECHA_CAMBIO',type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name:'FLAG_CHECK'},
			{name:'LIMITES_PUNTOS_TASA'},
			{name:'PUNTOS_ADD_ANT'},
			{name:'TASA_APLICAR_ANT'}
		],
		data:	[{'CLAVE_TASA':'',	'IC_MONEDA':'','FLAG_CHECK':'','MONEDA':'',	'TASA_INTERES':'',	'CLAVE_PLAZO':'',	'PLAZO':'',	'VALOR':'',	'REL_MAT':'',	'PUNTOS_ADD':'',	'TASA_APLICAR':'', 'CG_USUARIO_CAMBIO':'', 'FECHA_CAMBIO':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var grid = new Ext.grid.EditorGridPanel({
		id:'grid',
			title: 'Consulta',	
		store: consultaData,	columnLines:true, hidden:true, viewConfig: {markDirty: false},//viewConfig: {forceFit: true},
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
		columns: [					
			{
				xtype:'checkcolumn',	header:'Seleccionar', tooltip:'Seleccionar', dataIndex:'FLAG_CHECK',	sortable:false,	width:80,	align:'center', hideable:false, hidden:true
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:true,	width:150,	align:'center'
			},{
				header:'Tipo Tasa',	tooltip:'Tipo Tasa',	dataIndex:'TASA_INTERES',	sortable:true,	width:200,	align:'center'
			},{
				header:'Plazo de la Tasa Base aplicable',	tooltip:'Plazo de la Tasa Base aplicable',	dataIndex:'PLAZO',	sortable:true,	width:200,	align:'center'
			},{
				header:'Valor',	tooltip:'Valor',	dataIndex:'VALOR',	sortable:true,	width:130,	align:'right',renderer: Ext.util.Format.numberRenderer('0.00')
			},{
				header:'Rel. Mat.',	tooltip:'Rel. Mat.',	dataIndex:'REL_MAT',	sortable:true,	width:130,	align:'center'
			},{
				header:'Puntos Adicionales',	
				tooltip:'Puntos Adicionales',	
				dataIndex:'PUNTOS_ADD',	
				sortable:true,	width:130,	
				align:'center',
					editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: false
				},
				renderer:function(value,metadata,registro){
					return 	NE.util.colorCampoEdit(value,metadata,registro);
				}				
			},{
				header:'Tasa a Aplicar',	tooltip:'Tasa a Aplicar',	dataIndex:'TASA_APLICAR',	sortable:true,	width:130,	align:'right',renderer: Ext.util.Format.numberRenderer('0.00')
			},{
				header:'Usuario �ltima modificaci�n',	tooltip:'Usuario �ltima modificaci�n',	dataIndex:'CG_USUARIO_CAMBIO',	sortable:false,	width:200,	align:'center', hidden:true, hideable:false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Fecha �ltima modificaci�n', tooltip:'Fecha �ltima modificaci�n',	dataIndex:'FECHA_CAMBIO',sortable:true, width:120, align:'center', hidden:true,	hideable:false,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(strPerfil != "IF FACT RECURSO"){
									return Ext.util.Format.date(value,'d/m/Y');
								}else{
									return Ext.util.Format.date(value,'d/m/Y H:i:s');
								}
				}
			}
		],
		listeners: {	
			afteredit : function(e){
				var record = e.record;
				var grid = e.grid; 
				var campo= e.field;
		
				var limiPuntos= 	record.data['LIMITES_PUNTOS_TASA'];
				var puntosAdd= 	record.data['PUNTOS_ADD'];	
				var rel_mat= 	record.data['REL_MAT'];				
				var valor= 	record.data['VALOR'];	
				var tasaA=0;				
				if(rel_mat=='+'){   tasaA = parseFloat(valor)+ parseFloat(puntosAdd) ;   }
				if(rel_mat=='-'){   tasaA = parseFloat(valor)- parseFloat(puntosAdd);   }
				if(rel_mat=='*'){   tasaA = parseFloat(valor)* parseFloat(puntosAdd);   }
				if(rel_mat=='/'){   tasaA = parseFloat(valor)/ parseFloat(puntosAdd);   }
				
				
				if(campo == 'PUNTOS_ADD') {
					if(puntosAdd>limiPuntos){
						Ext.Msg.alert("Aviso","El valor no puede ser mayor a: "+limiPuntos );
						record.data['PUNTOS_ADD']= record.data['PUNTOS_ADD_ANT'] ;
						record.commit();				
						return;
					}else {
						record.data['TASA_APLICAR']= tasaA;
						record.commit();
					}
				}
			}
		},
		bbar: {
			id:'barraGrid',			
			items: [
				{
					xtype: 'button',	text:'Editar registro',	id:'btnEditar',	iconCls:'modificar', hidden:true,
					handler: function(boton, evento) {
									var jsonData = consultaData.data.items;
									var count=0;
									var camposEditar;
									Ext.each(jsonData, function(item,index,arrItem){
										if (item.data.FLAG_CHECK){
											count++;
											if (count > 1){
												return false;
											}else{
												camposEditar = item.data;
											}
										}
									});
									if (count==0){
										Ext.Msg.alert(boton.text,'Para poder editar el registro debe seleccionar por lo menos uno');
										return;
									}else if(count > 1 ){
										Ext.Msg.alert(boton.text,'Seleccione solo un registro para editar');
										return;
									}
									editarRegistro(camposEditar);
								}
				},{
					xtype: 'button',	text:'Remover registro',	id:'btnRemover',	iconCls:'borrar', hidden:true,
					handler: function(boton, evento) {
									var jsonData = consultaData.data.items;
									var count=0;
									var cveTasas="";
									Ext.each(jsonData, function(item,index,arrItem){
										if (item.data.FLAG_CHECK){
											if(count > 0){
												cveTasas += ",";
											}
											cveTasas += item.data.CLAVE_TASA;
											count++;
										}
									});
									if (count==0){
										Ext.Msg.alert(boton.text,'Para poder remover el registro debe seleccionar por lo menos uno');
										return;
									}
									Ext.getCmp('claveTasa').setValue(cveTasas);
									Ext.Msg.show({
										title:boton.text,
										msg: 'El registro ser� eliminado, <br> �Desea usted continuar?',
										buttons: Ext.Msg.OKCANCEL,
										fn: processRemover,
										animEl: 'elId',
										icon: Ext.MessageBox.WARNING
									});
								}
				},{
					xtype: 'button',	text:'Agregar registro',	id:'btnAgregarFactoraje',	iconCls:'aceptar', hidden:true,
					handler: function(boton, evento) {
									addRegistroFactoraje();
					}
				},
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',	text:'Confirmar Tasas',	id:'btnConfirmaTasas',	iconCls:'autorizar', hidden:true,
					handler: function(boton, evento) {
									if (strPerfil === "IF FACT RECURSO"){
										var jsonData = consultaData.data.items;
										var count=0;
										var cveTasas="";
										var relaMate="";
										var puntosAd="";
										Ext.each(jsonData, function(item,index,arrItem){
											if(count > 0){
												cveTasas += ",";
												relaMate += ",";
												puntosAd += ",";
											}
											cveTasas += item.data.CLAVE_TASA;
											relaMate += item.data.REL_MAT;
											puntosAd += item.data.PUNTOS_ADD;
											count++;
										});
										form.Cadena = cveTasas;
										form.relMat = relaMate;
										form.puntos = puntosAd;
									}
									Ext.Msg.show({
										title:boton.text,
										msg: '�Esta seguro de confirmar tasas?',
										buttons: Ext.Msg.OKCANCEL,
										fn: (strPerfil === "IF FACT RECURSO")?processConfirmaFactoraje:processConfirma,
										animEl: 'elId',
										icon: Ext.MessageBox.QUESTION
									});
									boton.setIconClass('loading-indicator');
								}
				}
				,{
					xtype: 'tbspacer', width: 5
				},
				{
					xtype: 'button',	text:'Guardar',	id:'btnGuardarG',	iconCls:'aceptar', hidden:false,
					handler: function(boton, evento) {
						
						grid = Ext.getCmp('grid');	
						var store = grid.getStore();	
						var cveTasa = [];
						var pRelMat= [];
						var pPuntos  = [];
						var fecha_cambio = [];
						var lsTipoTasa = [];
						var lsPlazo = [];
						var lsTasaPiso = [];
						var lsTasaPisoA = [];
						var lsPuntosA = [];
						var moneda = [];
						var limiPuntos = [];
						
						store.each(function(record) {
							cveTasa.push(record.data['CLAVE_TASA']);
							pRelMat.push(record.data['REL_MAT']);							
							pPuntos.push(record.data['PUNTOS_ADD']);
							fecha_cambio.push(Ext.util.Format.date(record.data['FECHA_CAMBIO'],'d/m/Y'));
							lsTipoTasa.push( record.data['TASA_INTERES']);
							lsPlazo.push(record.data['PLAZO']);
							moneda.push( record.data['IC_MONEDA']);								
							lsPuntosA.push( record.data['PUNTOS_ADD_ANT']);
							lsTasaPiso.push( record.data['TASA_APLICAR']);
							lsTasaPisoA.push(record.data['TASA_APLICAR_ANT']);
							limiPuntos.push(record.data['LIMITES_PUNTOS_TASA']);													
						});
						
						if(pPuntos !='') {
							Ext.Ajax.request({
								url: '24forma05ext.data.jsp',
								params:{
									informacion: "ModificarPuntos",
									cveTasa:cveTasa,
									pRelMat:pRelMat,
									pPuntos:pPuntos,
									ic_pyme:		Ext.getCmp('ic_pyme').getValue(),
									ic_epo:		Ext.getCmp('ic_epo').getValue(),
									fecha_cambio:fecha_cambio,
									lsTipoTasa:lsTipoTasa,
									lsPlazo:lsPlazo,
									lsTasaPiso:lsTasaPiso,								
									moneda:moneda,
									lsPuntosA:lsPuntosA,
									lsTasaPisoA:lsTasaPisoA								
								},								
								callback: procesaModificarPuntos
							});
							
						}
					}
				}
			]
		}
	});

	function processRemover(res){
		if (res == 'ok' || res == 'yes'){
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '24forma05ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),
							{informacion: "RemoverTasas"}),
				callback: procesaRemoverTasas
			});
		}else{
			Ext.getCmp('btnRemover').setIconClass('borrar');
		}
	}

	function processConfirmaFactoraje(res){
		if (res == 'ok' || res == 'yes'){
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '24forma05ext.data.jsp',
				params: {
					informacion:	'ConfirmaTasas',
					lsCadena:		form.Cadena,
					_ic_pyme:		Ext.getCmp('ic_pyme').getValue(),
					_pRelMat:		form.relMat,
					pPuntos:			form.puntos
				},
				callback: procesaConfirmaTasas
			});
		}else{
			Ext.getCmp('btnConfirmaTasas').setIconClass('autorizar');
		}
	}

	function processConfirma(res){
		if (res == 'ok' || res == 'yes'){
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '24forma05ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),
							{informacion: "ConfirmaTasas",
							lsCadena: form.Cadena}),
				callback: procesaConfirmaTasas
			});
		}else{
			Ext.getCmp('btnConfirmaTasas').setIconClass('autorizar');
		}
	}

	var elementosForma = [
		{
			xtype: 'combo',
			id:	'ic_epo',
			name: '_ic_epo',
			hiddenName : '_ic_epo',
			fieldLabel: 'EPO',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEpoDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:	{fn:function(combo){
									if (	!Ext.isEmpty(combo.getValue())	) {
										if(!edita){
											Ext.getCmp('ic_moneda').hide();
											Ext.getCmp('in_PlazoDias').hide();
											Ext.getCmp('pRelMat').hide();
											Ext.getCmp('pPuntos').hide();
											Ext.getCmp('btnAgregar').hide();
											Ext.getCmp('btnLimpiar').hide();
											grid.hide();
											Ext.getCmp('barraGrid').setVisible(false);
										}
										//cambios de otro combo . . . 
										var comboPyme = Ext.getCmp('ic_pyme');
										var comboPlazo = Ext.getCmp('in_PlazoDias');
										comboPyme.setValue('');
										comboPyme.store.removeAll();
										comboPyme.store.reload({	params: {_ic_epo: combo.getValue() }	});
										comboPyme.setVisible(true);
										comboPlazo.setValue('');
										comboPlazo.store.removeAll();
										comboPlazo.store.reload({	params: {_ic_epo: combo.getValue() }	});
									}
							}
				}
			}
		},{
			xtype: 'combo',
			id:	'ic_pyme',
			name: '_ic_pyme',
			hiddenName : '_ic_pyme',
			fieldLabel: 'PYME',
			emptyText: 'Seleccione distribuidor. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			minChars : 1,
			store: catalogoPymeDistData,
			listeners:{
				select:	{fn:function(combo){
									if (	!Ext.isEmpty(combo.getValue())	) {
										if(!edita){
											Ext.getCmp('ic_moneda').hide();
											Ext.getCmp('in_PlazoDias').hide();
											Ext.getCmp('pRelMat').hide();
											Ext.getCmp('pPuntos').hide();
											Ext.getCmp('btnAgregar').hide();
											Ext.getCmp('btnLimpiar').hide();
											grid.hide();
											Ext.getCmp('barraGrid').setVisible(false);
										}
										pnl.el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: '24forma05ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{informacion: "comboConsulta"}),
											callback: procesaComboConsulta
										});
									}
							}
				}
			}
		},{
			xtype: 'combo',
			id:	'ic_moneda',
			name: '_ic_moneda',
			hiddenName : '_ic_moneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccionar . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo,	hidden:true,
			listeners:{
				select:	{fn:function(combo){
									if (	!Ext.isEmpty(combo.getValue())	) {
										if(!edita){
											Ext.getCmp('in_PlazoDias').hide();
											Ext.getCmp('pRelMat').hide();
											Ext.getCmp('pPuntos').hide();
											Ext.getCmp('btnAgregar').hide();
											Ext.getCmp('btnLimpiar').hide();
											grid.hide();
											pnl.el.mask('Enviando...', 'x-mask-loading');
											Ext.Ajax.request({
												url: '24forma05ext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{informacion: "ConsultaTasas"}),
												callback: procesaConsultaTasas
											});
										}
									}
								}
				}
			}
		},{
			xtype: 'combo',
			id:	'in_PlazoDias',
			name: '_in_PlazoDias',
			hiddenName : '_in_PlazoDias',
			fieldLabel: 'Plazo de la Tasa Base aplicable',
			emptyText: 'Seleccione . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1, 
			//anchor:'35%',
			store: catalogoPlazoData,
			tpl : NE.util.templateMensajeCargaCombo,	hidden:true,
			listeners:{
				select:	{fn:function(combo){
									if (strPerfil != "IF FACT RECURSO"){
										if (	!validaFlotante(Ext.getCmp('pPuntos'),form.Maxpuntos)	){
											return;
										};
										if(!edita){
											if(!verificaPanel()) {
												return;
											}
											grid.hide();
											pnl.el.mask('Enviando...', 'x-mask-loading');
											Ext.Ajax.request({
												url: '24forma05ext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{informacion: "AgregaTasas"}),
												callback: procesaAgregaTasas
											});
										}
									}
								}
				}
			}			
		},{
			xtype: 'combo',
			id:	'pRelMat',
			name: '_pRelMat',
			hiddenName : '_pRelMat',
			fieldLabel: 'Relaci�n Mat.',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			editable:false,
			lazyRender:true,
			typeAhead: true,
			minChars:1,
			value:	'+',
			anchor:	'27%',
			store: catalogoRelacionMatData,
			hidden:true
		},{
			xtype: 'numberfield',
			name: 'pPuntos',
			id: 	'pPuntos',
			fieldLabel: 'Puntos Adicionales',
			allowBlank:false,	maxLength: 7,	anchor:'32%',	hidden:true,
			listeners:{
				blur:	{fn:function(field){
									if (	!Ext.isEmpty(field.getValue())	) {
										validaFlotante(field,form.Maxpuntos);
									}
							}
				}
			}
		},{
			xtype:'hidden', id:'claveTasa', name:'_claveTasa', value:''
		}
	];

	function validaFlotante(field,	Maxpuntos){
		var valid = true;
		if(Maxpuntos < field.getValue() ){
			field.markInvalid('El n�mero rebasa los puntos autorizados');
			field.focus();
			valid = false;
		}
		return valid;
	}

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title:'Tasas por PYME',
		width: 600,
		frame: true,
		labelWidth:110,
		labelAlign:'right',
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Guardar',	id:'btnGuardar',	iconCls:'icoGuardar',	hidden:true,
				handler: function(boton, evento) {
								if (	!validaFlotante(Ext.getCmp('pPuntos'),form.Maxpuntos)	){
									return;
								};
								if(!verificaPanel()) {
									return;
								}
								pnl.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '24forma05ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{informacion: "GuardarTasas"}),
									callback: procesaGuardarTasas
								});
				} //fin handler
			},{
				text: 'Cancelar',	id:'btnCancelar',	iconCls:'icoRechazar',	hidden:true,	handler: function(boton, evento) {window.location = '24forma05ext.jsp';}
			},{
				text: 'Agregar',	id:'btnAgregar',	iconCls:'aceptar',	hidden:true,
				handler: function(boton, evento) {
								if (	!validaFlotante(Ext.getCmp('pPuntos'),form.Maxpuntos)	){
									return;
								};
								if(!verificaPanel()) {
									return;
								}
								if (strPerfil === "IF FACT RECURSO"){
									var stor = grid.getStore();
									var value = Ext.getCmp('in_PlazoDias').getValue();
									var listaPlazos = stor.query('CLAVE_PLAZO', value);
	
									if(listaPlazos.length >= 1){
										Ext.Msg.alert('Aviso','El plazo ' + value + ' ya esta agregado');
										return;
									}
								}else{
									grid.hide();
								}

								pnl.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '24forma05ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{informacion: "AgregaTasas"}),
									callback: procesaAgregaTasas
								});
				} //fin handler
			},{
				text: 'Limpiar',
				id:	'btnLimpiar',
				iconCls: 'icoLimpiar',
				hidden:true,
				handler: function() {
					window.location = '24forma05ext.jsp';
				}
			}
		]
	});

	function verificaPanel(){
		var myPanel = Ext.getCmp('forma');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid,	NE.util.getEspaciador(10)
		]
	});

	catalogoMonedaData.load();
	catalogoEpoDistData.load();

	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24forma05ext.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});

});