<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.*,		
		com.netro.distribuidores.*,
		java.text.SimpleDateFormat,
		java.util.Date,	
		com.netro.anticipos.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";
String usuario 	= iNoUsuario+"-"+strNombreUsuario;
		
ParametrosDist  BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 

CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

		
JSONObject jsonObj = new JSONObject();

if (informacion.equals("valoresIniciales"))	{
		
	String Maxpuntos = BeanTasas.getPuntosMaximosxIf("4",iNoCliente);
	jsonObj.put("Maxpuntos", Maxpuntos);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("CatalogoEPODist") ) {

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveIf(iNoCliente);
	if(strPerfil.equals("IF FACT RECURSO")){
		cat.setTipoCredito("F");
	}else{
		cat.setTipoCredito("C");
	}
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoDistribuidor")){

	String ic_epo = (request.getParameter("_ic_epo")==null)?"":request.getParameter("_ic_epo");

	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		cat.setIntermediario(iNoCliente);
		if(!strPerfil.equals("IF FACT RECURSO")){
			cat.setTipoCredito("C");
		}
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}

}else if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoPlazo")){

	String ic_epo		= (request.getParameter("_ic_epo")==null)?"":request.getParameter("_ic_epo");
	String cadPlazos	= (request.getParameter("cadPlazos")==null)?"":request.getParameter("cadPlazos");
	if(!"".equals(ic_epo)){
		List coleccionElementos = new ArrayList();
	
		String icPlazo = "",inPlazoDias = "";
		Vector vecColumnas = null;
		Vector vecFilas = null;

		if(!strPerfil.equals("IF FACT RECURSO")){
			vecFilas = BeanTasas.getComboPlazo(4,ic_epo);
		}else{
			vecFilas = BeanTasas.getComboPlazoFactoraje(4,ic_epo,cadPlazos);
		}

		for(int i=0;i<vecFilas.size();i++) {
			vecColumnas = (Vector)vecFilas.get(i);
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
	
			icPlazo = (String)vecColumnas.get(1);
			inPlazoDias = (String)vecColumnas.get(1);
			elementoCatalogo.setClave(icPlazo);
			elementoCatalogo.setDescripcion(inPlazoDias);
			coleccionElementos.add(elementoCatalogo);
		}
		Iterator it = coleccionElementos.iterator();
		JSONArray jsonArr = new JSONArray();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}

}else if (informacion.equals("comboConsulta")){

	String ic_epo = (request.getParameter("_ic_epo")!=null)?request.getParameter("_ic_epo"):"";
	String ic_pyme = (request.getParameter("_ic_pyme")!=null)?request.getParameter("_ic_pyme"):"";

	int TotalLineas=0;
	TotalLineas = BeanTasas.getTotalLineas("4",ic_epo,iNoCliente,ic_pyme,"54");
	jsonObj.put("TotalLineas", Integer.toString(TotalLineas));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ConsultaTasas")){

	String ic_epo = (request.getParameter("_ic_epo")!=null)?request.getParameter("_ic_epo"):"";
	String ic_pyme = (request.getParameter("_ic_pyme")!=null)?request.getParameter("_ic_pyme"):"";
	String ic_moneda	= (request.getParameter("_ic_moneda")==null)?"1":request.getParameter("_ic_moneda");
	String pRelMat = (request.getParameter("_pRelMat")!=null)?request.getParameter("_pRelMat"):"";
	String pPuntos = (request.getParameter("pPuntos")!=null)?request.getParameter("pPuntos"):"";
	String existen = (request.getParameter("existen")!=null)?request.getParameter("existen"):"";
	Vector lovTasas = null;
	
	int liRegistros=0;
	if (!"".equals(ic_moneda)){
		lovTasas = BeanTasas.ovgetTasasxPyme(4,ic_pyme,iNoCliente,ic_moneda);
		liRegistros = lovTasas.size();
		if (liRegistros==0){
			if (ic_moneda.equals("54") && liRegistros>0) {
				jsonObj.put("alerta","Favor de capturar las tasas en dólares");
			}
			jsonObj.put("liRegistros", Integer.toString(liRegistros));
		}else{		
			
			List vecParametros = BeanParametros.getParamIF(iNoCliente); //F05-2014
			String fn_limite_puntos = (String)vecParametros.get(2);  //F05-2014
			
			List regs = new ArrayList();
			String lsMoneda="", lsTipoTasa="", lsCveTasa="", lsPlazo="", lsValor="",	in_plazo_dias="";
			String lsRelMat="", lsPuntos="", lsTasaPiso="", fecha_cambio="", usuarioMod="";
			double tasaAplicar = 0;
			for (int i=0; i<liRegistros; i++) {
				HashMap hash = new HashMap();
				Vector lovDatosTasa = (Vector)lovTasas.get(i);
				lsCveTasa	= lovDatosTasa.get(2).toString();
				lsMoneda	= lovDatosTasa.get(0).toString();
				lsTipoTasa	= lovDatosTasa.get(1).toString();
				lsPlazo = lovDatosTasa.get(3).toString();
				lsValor = lovDatosTasa.get(4).toString();
				lsRelMat = lovDatosTasa.get(5).toString();
				lsPuntos = lovDatosTasa.get(6).toString();
				lsTasaPiso = lovDatosTasa.get(7).toString();
				in_plazo_dias = lovDatosTasa.get(10).toString();
				fecha_cambio = lovDatosTasa.get(11).toString();
				usuarioMod = lovDatosTasa.get(12).toString();
				hash.put("IC_MONEDA",ic_moneda);
				hash.put("CLAVE_TASA",lsCveTasa);
				hash.put("MONEDA",lsMoneda);
				hash.put("TASA_INTERES",lsTipoTasa);
				hash.put("CLAVE_PLAZO",in_plazo_dias);
				hash.put("PLAZO",lsPlazo);
				hash.put("VALOR",lsValor);
				hash.put("REL_MAT",lsRelMat);
				hash.put("PUNTOS_ADD",lsPuntos);
				hash.put("TASA_APLICAR",lsTasaPiso);			
				hash.put("FECHA_CAMBIO",fecha_cambio);
				hash.put("CG_USUARIO_CAMBIO",usuarioMod);
				hash.put("FLAG_CHECK","");
				hash.put("LIMITES_PUNTOS_TASA",fn_limite_puntos);
				hash.put("PUNTOS_ADD_ANT",lsPuntos);
				hash.put("TASA_APLICAR_ANT",lsTasaPiso);
				regs.add(hash);
			}//for
			jsonObj.put("regs", regs);
		}
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("AgregaTasas")){

	String ic_epo = (request.getParameter("_ic_epo")!=null)?request.getParameter("_ic_epo"):"";
	String ic_pyme = (request.getParameter("_ic_pyme")!=null)?request.getParameter("_ic_pyme"):"";
	String ic_moneda	= (request.getParameter("_ic_moneda")==null)?"1":request.getParameter("_ic_moneda");
	String in_PlazoDias = (request.getParameter("_in_PlazoDias")!=null)?request.getParameter("_in_PlazoDias"):"";
	String pRelMat = (request.getParameter("_pRelMat")!=null)?request.getParameter("_pRelMat"):"";
	String pPuntos = (request.getParameter("pPuntos")!=null)?request.getParameter("pPuntos"):"";
	if (in_PlazoDias.equals("")){
		in_PlazoDias = "null";	
	}
	Vector lovTasas = null;
	
	int liRegistros=0;
	if (!"".equals(ic_moneda)){
		List regs = new ArrayList();
		String Cadena="";
		String lsMoneda="", lsTipoTasa="", lsCveTasa="", lsPlazo="", lsValor="";
		String lsRelMat="", lsPuntos="", lsTasaPiso="";
		double tasaAplicar = 0;
		if(!strPerfil.equals("IF FACT RECURSO")){
			lovTasas = BeanTasas.ovgetTasasxPyme(4,ic_pyme,iNoCliente,ic_moneda);
			liRegistros = lovTasas.size();
		}
		boolean existen = false;
		if (liRegistros == 0){
			if(!strPerfil.equals("IF FACT RECURSO")){
				lovTasas = BeanTasas.getTasasDispxPyme("4",ic_pyme,iNoCliente,ic_moneda,in_PlazoDias);
			}else{
				lovTasas = BeanTasas.getTasasDispxPyme("4",ic_pyme,iNoCliente,ic_moneda,in_PlazoDias,true);
			}

			liRegistros = lovTasas.size();
			existen = true;
		}
		for (int i=0; i<liRegistros; i++) {
			HashMap hash = new HashMap();
			Vector lovDatosTasa = (Vector)lovTasas.get(i);
			lsCveTasa	= lovDatosTasa.get(2).toString();
			lsMoneda	= lovDatosTasa.get(0).toString();
			lsTipoTasa	= lovDatosTasa.get(1).toString();
			lsPlazo = lovDatosTasa.get(3).toString();
			lsValor = lovDatosTasa.get(4).toString();
			if (i==0){
				Cadena = lsCveTasa;
			}else{
				Cadena += ","+lsCveTasa;
			}
			if (existen) {
				if ("+".equals(pRelMat))
					tasaAplicar = Double.parseDouble(lsValor) + Double.parseDouble(pPuntos);
				else
					if ("-".equals(pRelMat))
						tasaAplicar = Double.parseDouble(lsValor) - Double.parseDouble(pPuntos);
					else
						if ("*".equals(pRelMat))
							tasaAplicar = Double.parseDouble(lsValor) * Double.parseDouble(pPuntos);
						else 
							tasaAplicar = Double.parseDouble(lsValor) / Double.parseDouble(pPuntos);
				lsRelMat = pRelMat;
				lsPuntos = pPuntos;
				lsTasaPiso = ""+tasaAplicar;
			}else{
				lsRelMat = lovDatosTasa.get(5).toString();
				lsPuntos = lovDatosTasa.get(6).toString();
				lsTasaPiso = lovDatosTasa.get(7).toString();
			}
			
			hash.put("CLAVE_TASA",lsCveTasa);
			hash.put("MONEDA",lsMoneda);
			hash.put("TASA_INTERES",lsTipoTasa);
			hash.put("CLAVE_PLAZO",in_PlazoDias);
			hash.put("PLAZO",lsPlazo);
			hash.put("VALOR",lsValor);
			hash.put("REL_MAT",lsRelMat);
			hash.put("PUNTOS_ADD",lsPuntos);
			hash.put("TASA_APLICAR",lsTasaPiso);
			regs.add(hash);
		}//for
		jsonObj.put("lsCadena", Cadena);
		jsonObj.put("regs", regs);
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ConfirmaTasas")){
	
	String ic_pyme	=	(request.getParameter("_ic_pyme")!=null)?request.getParameter("_ic_pyme"):"";
	String lsCadena=	(request.getParameter("lsCadena")==null)?"1":request.getParameter("lsCadena");
	String pRelMat	=	(request.getParameter("_pRelMat")!=null)?request.getParameter("_pRelMat"):"";
	String pPuntos	=	(request.getParameter("pPuntos")!=null)?request.getParameter("pPuntos"):"";
	
	if(strPerfil.equals("IF FACT RECURSO")){
		boolean OK = BeanTasas.setTasasxPyme(ic_pyme,iNoCliente,"GA",4,lsCadena,pRelMat,pPuntos, usuario);
	}else{
		boolean OK = BeanTasas.setTasasxPyme(ic_pyme,iNoCliente,"GA",4,lsCadena,pRelMat,pPuntos);
	}

	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("GuardarTasas")){
	
	if(strPerfil.equals("IF FACT RECURSO")){
		String ic_pyme		=	(request.getParameter("_ic_pyme")!=null)?request.getParameter("_ic_pyme"):"";
		String claveTasa	=	(request.getParameter("_claveTasa")==null)?"0":request.getParameter("_claveTasa");
		String pRelMat		=	(request.getParameter("_pRelMat")!=null)?request.getParameter("_pRelMat"):"";
		String pPuntos		=	(request.getParameter("pPuntos")!=null)?request.getParameter("pPuntos"):"";	

		boolean OK = BeanTasas.setTasasxPyme(ic_pyme,iNoCliente,"GM",4,claveTasa,pRelMat,pPuntos, usuario);

		jsonObj.put("success", new Boolean(true));

	}else{
		jsonObj.put("success", new Boolean(false));
	}
	
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("RemoverTasas")){

	

	if(strPerfil.equals("IF FACT RECURSO")){

		String ic_pyme		=	(request.getParameter("_ic_pyme")!=null)?request.getParameter("_ic_pyme"):"";
		String cveTasa		=	(request.getParameter("_claveTasa")==null)?"0":request.getParameter("_claveTasa");
		String claveTasa[]=	cveTasa.split(",");
		String pRelMat		=	(request.getParameter("_pRelMat")!=null)?request.getParameter("_pRelMat"):"";
		String pPuntos		=	(request.getParameter("pPuntos")!=null)?request.getParameter("pPuntos"):"";
	
		boolean OK = BeanTasas.eliminaTasaDisxPyme(ic_pyme, iNoCliente, 4, claveTasa, usuario);

		jsonObj.put("success", new Boolean(true));

	}else{
		jsonObj.put("success", new Boolean(false));
	}

	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ModificarPuntos")){
	
	String ic_pyme		=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String cveTasa_1[]		= request.getParameterValues("cveTasa");
	String pRelMat_1[]		= request.getParameterValues("pRelMat");
	String pPuntos_1[]		= request.getParameterValues("pPuntos");
	String fecha_cambio_1[]		= request.getParameterValues("fecha_cambio");
	String ic_epo		=	(request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):""; 
	String lsTipoTasa_1[]		= request.getParameterValues("lsTipoTasa");
	String lsPlazo_1[]		= request.getParameterValues("lsPlazo");
	String lsTasaPiso_1[]		= request.getParameterValues("lsTasaPiso");
	String lsPuntosA_1[]		= request.getParameterValues("lsPuntosA");	
	String lsTasaPisoA_1[]		= request.getParameterValues("lsTasaPisoA");
	String moneda_1[]		= request.getParameterValues("moneda");

	
	
	String  cveTasa = "", pRelMat = "", pPuntos  = "", fecha_cambio = "", lsTipoTasa = "", 
	lsPlazo = "", lsTasaPiso = "", lsPuntosA = "", lsTasaPisoA = "", 	moneda = "";
	int total = 0;
	SimpleDateFormat fecha_hoy = new SimpleDateFormat("dd/MM/yyyy");
	String fecha_hoy_1 = fecha_hoy.format(new java.util.Date());
		
		System.out.println("moneda_1.length ---->"+moneda_1.length );  
		
	for(int i=0;i<moneda_1.length;i++){	
	
	System.out.println("i ---->"+i ); 
	
		cveTasa = cveTasa_1[i];
		pRelMat = pRelMat_1[i];
		pPuntos  = pPuntos_1[i];
		fecha_cambio = fecha_cambio_1[i];		
		lsTipoTasa = lsTipoTasa_1[i];
		lsPlazo = lsPlazo_1[i]; 
		lsTasaPiso = lsTasaPiso_1[i];
		lsPuntosA = lsPuntosA_1[i];
		lsTasaPisoA = lsTasaPisoA_1[i];
		moneda = moneda_1[i];
		
		String Anteriores="Fecha y hora =" +fecha_cambio+  ", Epo = " +ic_epo+ ", Tipo de tasa  = " +lsTipoTasa+
					 ", Plazo  = " +lsPlazo+ 	 ", Moneda  = " +moneda+ ", Tasa  a  aplicar  = " +lsTasaPisoA+
					 ", Punto  adicional  = " +lsPuntosA;
					 
		String Actuales="Fecha y hora =" +fecha_hoy_1+  ", Epo = " +ic_epo+  ", Tipo de tasa  = " +lsTipoTasa+
					 ", Plazo  = " +lsPlazo+  ", Moneda  = " +moneda+  ", Tasa  a  aplicar  = " +lsTasaPiso+
					 ", Punto  adicional  = " +pPuntos;			 				 
		
		System.out.println("Anteriores  "+Anteriores);
		System.out.println("Actuales  "+Actuales);	
		
		boolean OK = BeanTasas.binsertaTasaDisxPyme(ic_pyme,iNoCliente,"GM", 4,  cveTasa, pRelMat,  pPuntos,  usuario);		
		if(OK) {			
		  BeanTasas.setBitacoraTasasPyme(iNoCliente,  strLogin,  strNombreUsuario,  Anteriores,  Actuales  ); 
		  total++;
		}
	}
	
	if(total==moneda_1.length)  {
		jsonObj.put("success", new Boolean(true));
		
	}else {
		jsonObj.put("success", new Boolean(false));
	}
		
	infoRegresar = jsonObj.toString();
	
}

%>
<%=infoRegresar%>