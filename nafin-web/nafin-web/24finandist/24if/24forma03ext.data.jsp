<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.anticipos.*,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("valoresIniciales"))	{

	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);
	
	JSONObject jsonObj = new JSONObject();
	String Maxpuntos = BeanTasas.getPuntosMaximosxIf("4",iNoCliente);
	jsonObj.put("Maxpuntos", Maxpuntos);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("CatalogoEPODist") ) {

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveIf(iNoCliente);
	cat.setTipoCredito("D");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("comboConsulta")){

	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	JSONObject jsonObj = new JSONObject();

	int TotalLineas=0;
	TotalLineas = BeanTasas.getTotalLineas("4",ic_epo,iNoCliente,"54");
	jsonObj.put("TotalLineas", Integer.toString(TotalLineas));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ConsultaTasas")){

	String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	String ic_moneda	= (request.getParameter("ic_moneda")==null)?"1":request.getParameter("ic_moneda");
	String pRelMat = (request.getParameter("pRelMat")!=null)?request.getParameter("pRelMat"):"";
	String pPuntos = (request.getParameter("pPuntos")!=null)?request.getParameter("pPuntos"):"";
	String existen = (request.getParameter("existen")!=null)?request.getParameter("existen"):"";
	Vector lovTasas = null;

	JSONObject jsonObj = new JSONObject();
	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	int liRegistros=0;
	if (!"".equals(ic_moneda)){
		lovTasas = BeanTasas.ovgetTasasxEpo(4,ic_epo,iNoCliente,ic_moneda,"");
		liRegistros = lovTasas.size();
		if (liRegistros==0){
			if (ic_moneda.equals("54") && liRegistros>0) {
				jsonObj.put("alerta","Favor de capturar las tasas en dólares");
			}
			jsonObj.put("liRegistros", Integer.toString(liRegistros));
		}else{
			List regs = new ArrayList();
			String lsMoneda="", lsTipoTasa="", lsCveTasa="", lsPlazo="", lsValor="";
			String lsRelMat="", lsPuntos="", lsTasaPiso="";
			double tasaAplicar = 0;
			for (int i=0; i<liRegistros; i++) {
				HashMap hash = new HashMap();
				Vector lovDatosTasa = (Vector)lovTasas.get(i);
				lsCveTasa	= lovDatosTasa.get(2).toString();
				lsMoneda	= lovDatosTasa.get(0).toString();
				lsTipoTasa	= lovDatosTasa.get(1).toString();
				lsPlazo = lovDatosTasa.get(3).toString();
				lsValor = lovDatosTasa.get(4).toString();
				lsRelMat = lovDatosTasa.get(5).toString();
				lsPuntos = lovDatosTasa.get(6).toString();
				lsTasaPiso = lovDatosTasa.get(7).toString();
				hash.put("MONEDA",lsMoneda);
				hash.put("TASA_INTERES",lsTipoTasa);
				hash.put("PLAZO",lsPlazo);
				hash.put("VALOR",lsValor);
				hash.put("REL_MAT",lsRelMat);
				hash.put("PUNTOS_ADD",lsPuntos);
				hash.put("TASA_APLICAR",lsTasaPiso);
				regs.add(hash);
			}//for
			jsonObj.put("regs", regs);
		}
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("AgregaTasas")){

	String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	String ic_moneda	= (request.getParameter("ic_moneda")==null)?"1":request.getParameter("ic_moneda");
	String pRelMat = (request.getParameter("pRelMat")!=null)?request.getParameter("pRelMat"):"";
	String pPuntos = (request.getParameter("pPuntos")!=null)?request.getParameter("pPuntos"):"";
	Vector lovTasas = null;

	JSONObject jsonObj = new JSONObject();
	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	int liRegistros=0;
	if (!"".equals(ic_moneda)){
		List regs = new ArrayList();
		String Cadena="";
		String lsMoneda="", lsTipoTasa="", lsCveTasa="", lsPlazo="", lsValor="";
		String lsRelMat="", lsPuntos="", lsTasaPiso="";
		double tasaAplicar = 0;
		lovTasas = BeanTasas.ovgetTasasxEpo(4,ic_epo,iNoCliente,ic_moneda,"");
		liRegistros = lovTasas.size();
		boolean existen = false;
		if (liRegistros == 0){
			lovTasas = BeanTasas.getTasasDispxEpo("4",ic_epo,iNoCliente,ic_moneda);
			liRegistros = lovTasas.size();
			existen = true;
		}
		for (int i=0; i<liRegistros; i++) {
			HashMap hash = new HashMap();
			Vector lovDatosTasa = (Vector)lovTasas.get(i);
			lsCveTasa	= lovDatosTasa.get(2).toString();
			lsMoneda	= lovDatosTasa.get(0).toString();
			lsTipoTasa	= lovDatosTasa.get(1).toString();
			lsPlazo = lovDatosTasa.get(3).toString();
			lsValor = lovDatosTasa.get(4).toString();
			if (i==0){
				Cadena = lsCveTasa;
			}else{
				Cadena += ","+lsCveTasa;
			}
			if (existen) {
				if ("+".equals(pRelMat))
					tasaAplicar = Double.parseDouble(lsValor) + Double.parseDouble(pPuntos);
				else
					if ("-".equals(pRelMat))
						tasaAplicar = Double.parseDouble(lsValor) - Double.parseDouble(pPuntos);
					else
						if ("*".equals(pRelMat))
							tasaAplicar = Double.parseDouble(lsValor) * Double.parseDouble(pPuntos);
						else 
							tasaAplicar = Double.parseDouble(lsValor) / Double.parseDouble(pPuntos);
				lsRelMat = pRelMat;
				lsPuntos = pPuntos;
				lsTasaPiso = ""+tasaAplicar;
			}else{
				lsRelMat = lovDatosTasa.get(5).toString();
				lsPuntos = lovDatosTasa.get(6).toString();
				lsTasaPiso = lovDatosTasa.get(7).toString();
			}
			hash.put("MONEDA",lsMoneda);
			hash.put("TASA_INTERES",lsTipoTasa);
			hash.put("PLAZO",lsPlazo);
			hash.put("VALOR",lsValor);
			hash.put("REL_MAT",lsRelMat);
			hash.put("PUNTOS_ADD",lsPuntos);
			hash.put("TASA_APLICAR",lsTasaPiso);
			regs.add(hash);
		}//for
		jsonObj.put("lsCadena", Cadena);
		jsonObj.put("regs", regs);
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ConfirmaTasas")){

	JSONObject jsonObj = new JSONObject();
	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	String ic_epo	=	(request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	String lsCadena=	(request.getParameter("lsCadena")==null)?"1":request.getParameter("lsCadena");
	String pRelMat	=	(request.getParameter("pRelMat")!=null)?request.getParameter("pRelMat"):"";
	String pPuntos	=	(request.getParameter("pPuntos")!=null)?request.getParameter("pPuntos"):"";

	boolean OK = BeanTasas.setTasasxEpo(ic_epo,iNoCliente,"GA",4,lsCadena,pRelMat,pPuntos);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}


%>
<%=infoRegresar%>