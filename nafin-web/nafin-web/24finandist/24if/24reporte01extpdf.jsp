<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.pdf.*,net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
String ic_estatus_docto	=	"";
String tipoSolic			=	"";
String ic_estatus	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
if(!"".equals(ic_estatus)){
	ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
	tipoSolic		 = ic_estatus.substring(0,1);
}

try {

	if(iNoCliente != null && !iNoCliente.equals("") && !"".equals(ic_estatus) ) {

		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);

		//variables de despliegue
		String epo					= "";
		String distribuidor		= "";
		String numDocto			= "";
		String numAcuseCarga		= "";
		String fechaEmision		= "";
		String fechaPublicacion	= "";
		String fechaVencimiento	= "";
		String plazoDocto			= "";
		String moneda				= "";
		String icMoneda			= "";
		double monto				= 0;
		String tipoConversion	= "";
		double tipoCambio			= 0;
		double montoValuado		= 0;
		String plazoDescuento	= "";
		String porcDescuento		= "";
		double montoDescontar	= 0;
		String modoPlazo			= "";
		String estatus 			= "";
		String icDocumento		= "";
		String intermediario		= "";
		String tipoCredito		= "";
		String fechaOperacion	= "";
		double montoCredito		= 0;
		String plazoCredito		= "";
		String fechaSeleccion	= "";
		String fechaVencCredito	= "";
		String fechaOperIF		= "";
		String referenciaTasaInt= "";
		double valorTasaInt		= 0;
		double montoTasaInt		= 0;
		String tipoCobranza		= "";
		String tipoCobroInt		= "";
		String numeroPago	 		= "";
		String causa				= "";
		String fechaCambio		= "";
		String monedaLinea		= "";
		//totales
		int		totalDoctosMN		= 0;
		int 		totalDoctosUSD		= 0;
		double	totalMontoMN		= 0;
		double	totalMontoUSD		= 0;
		double	totalMontoValuadoMN	= 0;
		double	totalMontoValuadoUSD	= 0;
		double	totalMontoCreditoMN	= 0;
		double	totalMontoCreditoUSD	= 0;

		int i = 0;

		RepCreditosEstatusIf reporte = new RepCreditosEstatusIf();
		reporte.setClaveIf(iNoCliente);
		reporte.setClaveEstatus(ic_estatus_docto);
		reporte.setTipoSolic(tipoSolic);
		reporte.setQrysentencia();
		Registros reg = reporte.executeQuery();
		/***************
		*	Cabeceras	*
		****************/
		int numCols=0;
		if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			numCols=11;
		}else{
			numCols=10;
		}
		pdfDoc.setTable(numCols, 100);

		if(	("3".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	){
			pdfDoc.setCell("Estatus: Seleccionado Pyme","celda01",ComunesPDF.LEFT,numCols);
		}
		if(	("1".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	){
			pdfDoc.setCell("Estatus: Seleccionado IF/Operado","formasmenB",ComunesPDF.LEFT,numCols);
		}
		if(	("2".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	){
			pdfDoc.setCell("Estatus: En Proceso Nafin","formasmenB",ComunesPDF.LEFT,numCols);
		}
		if(	("3".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	){
			pdfDoc.setCell("Estatus: Operado","formasmenB",ComunesPDF.LEFT,numCols);
		}
		if(	("4".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	){
			pdfDoc.setCell("Estatus: Rechazada Nafin","formasmenB",ComunesPDF.LEFT,numCols);
		}
		if(	("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	){
			pdfDoc.setCell("Estatus: Operado Pagado","formasmenB",ComunesPDF.LEFT,numCols);
		}
		if(	("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	){
			pdfDoc.setCell("Estatus: Pendiente de pago IF","formasmenB",ComunesPDF.LEFT,numCols);
		}
		if(	("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	){
			pdfDoc.setCell("Estatus: Rechazado IF","formasmenB",ComunesPDF.LEFT,numCols);
		}

		pdfDoc.setCell("A","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("EPO","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Distribuidor","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de acuse de carga","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de documento inicial","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de emisión","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de publicación","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo docto.","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER);
		if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
		}
		pdfDoc.setCell("B","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo para descuento en días","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("% de descuento","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto % de descuento","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo conv.","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo cambio","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto valuado","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Modalidad de plazo","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de documento final","formasmenB",ComunesPDF.CENTER);
		if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
		}
		pdfDoc.setCell("C","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de operación distribuidor","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Referencia tasa de interés","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Valor tasa de interés","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto de intereses","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto total de capital e interés","formasmenB",ComunesPDF.CENTER);

		if(	("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic)) || ("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	){
			pdfDoc.setCell("Núm. de pago de interés relacionado","formasmenB",ComunesPDF.CENTER);
		}else if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			pdfDoc.setCell("Fecha de rechazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Causa","formasmenB",ComunesPDF.CENTER);
		}else{
			pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
		}

		while(reg.next()){
			epo			 		= (reg.getString("NOMBRE_EPO")==null)?"":reg.getString("NOMBRE_EPO");
			distribuidor 		= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
			numDocto				= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
			numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
			fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
			fechaPublicacion	= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
			fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
			plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
			moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
			monto 				= Double.parseDouble(reg.getString("FN_MONTO"));
			tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
			tipoCambio			= Double.parseDouble(reg.getString("TIPO_CAMBIO"));
			plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
			porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
			montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
			montoValuado		= (monto-montoDescontar)*tipoCambio;
			modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
			estatus 				= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
			icMoneda				= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
			icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
			tipoCobranza		= (reg.getString("TIPO_COBRANZA")==null)?"":reg.getString("TIPO_COBRANZA");
			icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
	//		intermediario		= (reg.getString("NOMBRE_IF")==null)?"":reg.getString("NOMBRE_IF");
			tipoCredito			= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
			montoCredito		= Double.parseDouble(reg.getString("MONTO_CREDITO"));
			plazoCredito		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
			tipoCobroInt		= (reg.getString("TIPO_COBRO_INT")==null)?"":reg.getString("TIPO_COBRO_INT");
			referenciaTasaInt	= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
			valorTasaInt		= Double.parseDouble(reg.getString("VALOR_TASA_INT"));
			montoTasaInt		= Double.parseDouble(reg.getString("MONTO_TASA_INT"));
			fechaSeleccion		= (reg.getString("FECHA_SELECCION")==null)?"":reg.getString("FECHA_SELECCION");
			fechaVencCredito	= (reg.getString("FECHA_VENC_CREDITO")==null)?"":reg.getString("FECHA_VENC_CREDITO");
			monedaLinea			= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");

			if(	("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	|| ("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){		//documento negociable
				numeroPago			= (reg.getString("CG_NUMERO_PAGO")==null)?"":reg.getString("CG_NUMERO_PAGO");
			}
			if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
				causa			=	(reg.getString("CT_CAMBIO_MOTIVO")==null)?"":reg.getString("CT_CAMBIO_MOTIVO");
				fechaCambio	=	(reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
			}
			if("1".equals(icMoneda)){
				totalDoctosMN ++;
				totalMontoMN += monto;
				totalMontoValuadoMN += montoValuado;
				totalMontoCreditoMN += montoCredito;
			}else{
				totalDoctosUSD ++;
				totalMontoUSD += monto;
				if(!icMoneda.equals(monedaLinea))
					totalMontoValuadoUSD += montoValuado;
				totalMontoCreditoUSD += montoCredito;
			}
			/***************
			*	Contenido	*
			****************/

			pdfDoc.setCell("A","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			}
			pdfDoc.setCell("B","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(porcDescuento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell((icMoneda.equals(monedaLinea))?"":tipoConversion,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell((icMoneda.equals(monedaLinea))?"":"$ "+Comunes.formatoDecimal(tipoCambio,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell((icMoneda.equals(monedaLinea))?"":"$ "+Comunes.formatoDecimal(montoValuado,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
			if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			}
			pdfDoc.setCell("C","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoCredito,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaSeleccion,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(referenciaTasaInt,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(valorTasaInt+"%","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoTasaInt,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoCredito+montoTasaInt,2),"formas",ComunesPDF.CENTER);

			if(	("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic)) || ("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	){
				pdfDoc.setCell(numeroPago,"formas",ComunesPDF.CENTER);
			}else if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
				pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
			}else{
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			}
			i++;
		}
		if (i==0){
			pdfDoc.setCell("No se encontró ningún registro","formas",ComunesPDF.CENTER);
		}
		if(totalDoctosMN>0){
			pdfDoc.setCell("Total de Documentos M.N.","formasmenB",ComunesPDF.RIGHT,3);
			pdfDoc.setCell(Integer.toString(totalDoctosMN),"formas",ComunesPDF.CENTER,2);
			if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
			}else{
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
			}
			pdfDoc.setCell("Total Monto M.N.","formasmenB",ComunesPDF.RIGHT,3);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.CENTER,2);
			pdfDoc.setCell("Total Monto del Credito M.N.","formasmenB",ComunesPDF.RIGHT,3);
			if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.CENTER,3);
			}else{
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.CENTER,2);
			}
		}
		if(totalDoctosUSD>0){
			pdfDoc.setCell("Total de Documentos Dólares","formasmenB",ComunesPDF.RIGHT,3);
			pdfDoc.setCell(Integer.toString(totalDoctosUSD),"formas",ComunesPDF.CENTER,2);
			pdfDoc.setCell("Total Monto Valuado Dólares","formasmenB",ComunesPDF.RIGHT,3);
			
			if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.CENTER,3);
			}else{
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.CENTER,2);
			}
			pdfDoc.setCell("Total Monto Dólares","formasmenB",ComunesPDF.RIGHT,3);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.CENTER,2);
			pdfDoc.setCell("Total Monto del Crédito Dólares","formasmenB",ComunesPDF.RIGHT,3);
			
			if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.CENTER,3);
			}else{
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.CENTER,2);
			}
		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>