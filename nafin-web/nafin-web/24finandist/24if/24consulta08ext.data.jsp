<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.distribuidores.*,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String operaContrato    = "";
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";
double montoComision = 0;
double montoDepositar = 0;

if (informacion.equals("valoresIniciales"))	{

	String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	boolean cadEpo = true;
	/*if(application.getInitParameter("EpoCemex").equals(ic_epo)){
		//cadEpoCemex
		cadEpo = false;
	}else{
		cadEpo = true;
	}*/
	jsonObj.put("cadEpo", new Boolean(cadEpo));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoEPODist") ) {
        
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveIf(iNoCliente);
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoDistribuidor")){
  String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
  	
	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cp.cg_razon_social");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
                /*JSONObject jsonObj = new JSONObject();
                jsonObj.put("operaContrato",operaContrato);
                infoRegresar = jsonObj.toString();*/
	}

}else if (informacion.equals("getLeyenda")){
  String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
 
	
	if(!"".equals(ic_epo)){
		 ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
  operaContrato = BeanParametros.obtieneOperaSinCesion(ic_epo); //PARAMETRO NUEVO JAGE 14012017
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("operaContrato",operaContrato);
                infoRegresar = jsonObj.toString();
	}



}else if (informacion.equals("Consulta")	||	informacion.equals("ArchivoCSV")	||	informacion.equals("ArchivoPDF")||	informacion.equals("ArchivoReporteTcPDF")||	informacion.equals("ArchivoReporteTcCSV")){

	int start = 0;
	int limit = 0;
	
	String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String fecha_ini = (request.getParameter("Txtfchini")==null)?"":request.getParameter("Txtfchini");
	String fecha_fin = (request.getParameter("Txtffin")==null)?"":request.getParameter("Txtffin");
	String tipoConsulta = (request.getParameter("tipoConsulta")==null)?"":request.getParameter("tipoConsulta");
	String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");
	String tipoCon = "";
	AvNotifIfDist notif = new AvNotifIfDist();
	notif.setIc_if(iNoCliente);
	notif.setIc_epo(ic_epo);
	notif.setIc_pyme(ic_pyme);
	notif.setFechaInicial(fecha_ini);
	notif.setFechaFinal(fecha_fin);
	notif.setTipoConsulta(tipoConsulta);
	notif.setStrPerfil(strPerfil);
	notif.setNumOrden(numOrden);
	if(strPerfil.equals("IF FACT RECURSO")){
		notif.setTipoCredito("F");
		tipoCon="F";
	}else{
		notif.setTipoCredito("");
		tipoCon="";
	}
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(notif);
	JSONObject 	resultado	= new JSONObject();
	if (informacion.equals("Consulta")) {		
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}
			Registros 	reg = queryHelper.getPageResultSet(request,start,limit);
			if(!tipoCon.equals("F")){
				while(reg.next()){  
					String montoCredito = reg.getString("MONTO_CREDITO")==null?"0":reg.getString("MONTO_CREDITO");
					String comApli = reg.getString("COMISION_APLICABLE")==null?"0":reg.getString("COMISION_APLICABLE");
					montoComision = (Double.parseDouble(montoCredito))*(Double.parseDouble(comApli))/100;
					montoDepositar =(Double.parseDouble(montoCredito))-montoComision;
					reg.setObject("MONTO_COMISION",String.valueOf(montoComision));
					reg.setObject("MONTO_DEPOSITAR",String.valueOf(montoDepositar));
				}
			}
			String consulta	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + reg.getJSONData()+"}";
			resultado = JSONObject.fromObject(consulta);
			infoRegresar = resultado.toString(); 	
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

	}else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ArchivoPDF")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}else if (informacion.equals("ArchivoReporteTcPDF")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}else if(informacion.equals("ArchivoReporteTcCSV")){
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}

} else if(informacion.equals("ArchivoTotalPDF")){

	String ic_epo       = (request.getParameter("ic_epo")      ==null)?"":request.getParameter("ic_epo");
	String ic_pyme      = (request.getParameter("ic_pyme")     ==null)?"":request.getParameter("ic_pyme");
	String fecha_ini    = (request.getParameter("Txtfchini")   ==null)?"":request.getParameter("Txtfchini");
	String fecha_fin    = (request.getParameter("Txtffin")     ==null)?"":request.getParameter("Txtffin");
	String tipoConsulta = (request.getParameter("tipoConsulta")==null)?"":request.getParameter("tipoConsulta");
	String numOrden     = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");

	JSONObject jsonObj = new JSONObject();
	AvNotifIfDistribuidores paginador = new AvNotifIfDistribuidores();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_if(iNoCliente);
	paginador.setFecha_ini(fecha_ini);
	paginador.setFecha_fin(fecha_fin);
	paginador.setNumOrden(numOrden);
	paginador.setTipoConsulta(tipoConsulta);
	paginador.setPerfil(strPerfil);
	if(strPerfil.equals("IF FACT RECURSO")){
		paginador.setTipoCredito("F");
	}else{
		paginador.setTipoCredito("");
	}
	try{
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>
