Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

	var form = {plazo_min:null, plazo_epo:null, CuentaCorriente:null}

	function procesaAceptar(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if ( Ext.util.JSON.decode(response.responseText).msg != undefined){
				Ext.Msg.alert('',Ext.util.JSON.decode(response.responseText).msg,
				function(){
					window.location = '24paramPymeExt.jsp';
				});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaParametrizacion(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if ( Ext.util.JSON.decode(response.responseText).plazo_min != undefined){ form.plazo_min = Ext.util.JSON.decode(response.responseText).plazo_min;}
			if ( Ext.util.JSON.decode(response.responseText).plazo_epo != undefined){ form.plazo_epo = Ext.util.JSON.decode(response.responseText).plazo_epo;}
			if ( Ext.util.JSON.decode(response.responseText).CuentaCorriente != undefined){ form.CuentaCorriente = Ext.util.JSON.decode(response.responseText).CuentaCorriente;}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24paramPyme.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesarCatalogoPymeDistData = function(store, arrRegistros, opts) {
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg', 'plazo']);
			store.insert(	0,new reg({	clave: "0",	descripcion: 'Seleccionar Todos',	loadMsg: null, plazo:""	})	);
	}

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg', 'plazo'],
		url : '24paramPyme.data.jsp',
		baseParams: {	informacion: 'CatalogoDistribuidor'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesarCatalogoPymeDistData,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	
	var consultaData = new Ext.data.JsonStore({
		fields: [{name:'clave'},{name:'descripcion'},{name:'plazo'}],
		data:	[{	'clave':'',	'descripcion':'',	'plazo':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var grid = new Ext.grid.EditorGridPanel({
		id:'grid',store: consultaData,	columnLines:true, clicksToEdit:1,	hidden:true, viewConfig: {forceFit: true},
		stripeRows:true,	loadMask:true,	height:400,	width:500,	style:'margin:0 auto;',	frame:false,
		columns: [
			{
				header:'PYME',	tooltip:'PYME',	dataIndex:'descripcion',	sortable:true,	width:350,	align:'left'
			},{
				header:'Plazo',	tooltip:'Plazo',	dataIndex:'plazo',	sortable:true,	width:150,	align:'center',
				editor:{
					xtype:'textfield',name:	'plazo',id: 0,maxLength: 9,regex: /^\d*$/
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			}
		],
		bbar: {
			id:'barraGrid',
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',
					text: 'Aceptar',
					id: 'btnAceptar',
					iconCls:'icoAceptar',
					handler: function(boton, evento) {
									var flag=false;
									var jsonData = consultaData.data.items;
									Ext.each(jsonData, function(item,index,arrItem){
										if (	Ext.isEmpty(item.data.plazo)	){
											Ext.Msg.alert(boton.text,
												"El valor del Plazo es requerido",
												function() {
													var gridEl = Ext.getCmp('grid').getGridEl();
													var col = Ext.getCmp('grid').getColumnModel().findColumnIndex('plazo');
													Ext.getCmp('grid').startEditing(index, col);
												}
											);
											flag=true;
											return false;
										}else{
											if( !Ext.isEmpty(form.plazo_min) ){
												if(new Number(item.data.plazo) < new Number(form.plazo_min) ){
													Ext.Msg.alert(boton.text,
														"El Plazo Minimo de financiamiento por PYME es "+form.plazo_min+" dias",
														function() {
															var gridEl = Ext.getCmp('grid').getGridEl();
															var col = Ext.getCmp('grid').getColumnModel().findColumnIndex('plazo');
															Ext.getCmp('grid').startEditing(index, col);
														}
													);
													flag=true;
													return false;
												}
											}
										}
									});
									if(flag){return;}
									var plazoEpo = parseFloat(form.plazo_epo);
									if (form.CuentaCorriente === "C") {
										var jsonData = consultaData.data.items;
										Ext.each(jsonData, function(item,index,arrItem){
											if (	!Ext.isEmpty(item.data.plazo)	){
												var plazo =  parseFloat(item.data.plazo);
												if (plazo > plazoEpo){
													Ext.Msg.alert("",
														"El plazo capturado ("+plazo+") debe ser menor o igual al plazo m�ximo de financiamiento de la EPO ("+plazoEpo+")",
														function() {
															var gridEl = Ext.getCmp('grid').getGridEl();
															var col = Ext.getCmp('grid').getColumnModel().findColumnIndex('plazo');
															Ext.getCmp('grid').startEditing(index, col);
														}
													);
													flag=true;
													return false;
												}
											}
										});
									}
									if(flag){return;}

									var plazos = [];
									var ic_pymes = [];
									
									Ext.each(jsonData, function(item,index,arrItem){
										if (	!Ext.isEmpty(item.data.plazo)	){
											plazos.push(item.data.plazo);
											ic_pymes.push(item.data.clave);
										}
									});
									pnl.el.mask('Enviando...', 'x-mask-loading');
									Ext.Ajax.request({
										url: '24paramPyme.data.jsp',
										params: {informacion: 'Aceptar', ic_epo: Ext.getCmp('_ic_epo').getValue(), plazo:plazos, ic_pymes:ic_pymes},
										callback: procesaAceptar
									});
								}
				},{
					xtype: 'tbspacer', width: 5
				}
			]
		}
	});

	var elementosForma = [
		{
			xtype: 'combo',
			id:	'_ic_epo',
			name: 'ic_epo',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEpoDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:	{fn:function(combo){
									grid.hide();
									form.plazo_min=null;
									form.plazo_epo=null;
									if (	!Ext.isEmpty(combo.getValue())	) {
										var comboPyme = Ext.getCmp('_ic_pyme');
										comboPyme.setValue('');
										comboPyme.store.removeAll();
										comboPyme.store.reload({	params: {ic_epo: combo.getValue() }	});
										comboPyme.setVisible(true);
										pnl.el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: '24paramPyme.data.jsp',
											params: {informacion: 'Parametrizacion', ic_epo: combo.getValue()},
											callback: procesaParametrizacion
										});
									}
							}
				}
			}
		},{
			xtype: 'combo',
			id:	'_ic_pyme',
			name: 'ic_pyme',
			hiddenName : 'ic_pyme',
			fieldLabel: 'PYME',
			emptyText: 'Seleccione distribuidor. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			minChars : 1,
			store: catalogoPymeDistData,
			listeners:{
				select:	{fn:function(combo){
									if (	!Ext.isEmpty(combo.getValue())	) {
										grid.hide();
										var ds = combo.store;
										var newData = [];
										consultaData.loadData('');
										pnl.el.mask('Enviando...', 'x-mask-loading');
										var jsonData = ds.data.items;
										if ( combo.getValue() === "0" ){
											Ext.each(jsonData, function(item,index,arrItem){
												if(index > 0){
													newData.push(item.data);
												}
											});
										}else{
											Ext.each(jsonData, function(item,index,arrItem){
												if (item.data.clave === combo.getValue()){
													newData.push(item.data);
													return false;
												}
											});
										}
										if (newData.length > 0) {
											consultaData.loadData(newData);
											grid.show();
										}
										pnl.el.unmask();
									}
							}
				}
			}
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title:'Parametros por PYME',
		width: 500,
		frame: true,
		labelWidth:50,
		labelAlign:'right',
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: false
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid,	NE.util.getEspaciador(10)
		]
	});

	catalogoEpoDistData.load();

});