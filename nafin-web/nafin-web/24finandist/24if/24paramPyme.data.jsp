<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("CatalogoEPODist") ) {

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveIf(iNoCliente);
	cat.setTipoCredito("C");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoDistribuidor")){

	JSONObject jsonObj = new JSONObject();
	String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	ParametrosDist parametrosDist = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

	if(!"".equals(ic_epo)){

		List colPlazoPyme = new ArrayList();
		List regPlazoPyme = new ArrayList();
		List regs = new ArrayList();
		colPlazoPyme = parametrosDist.getPlazosPymeIfEpo(ic_epo, iNoCliente);
		
		if(colPlazoPyme.size()>0){
			for(int x=0;x<colPlazoPyme.size();x++){
				HashMap hash = new HashMap();
				regPlazoPyme = (List)colPlazoPyme.get(x);
				String clavePyme = (String)regPlazoPyme.get(0);
				String plazo = ((String)regPlazoPyme.get(1))==null?"":(String)regPlazoPyme.get(1);
				String desc = (String)regPlazoPyme.get(2);
				hash.put("clave",clavePyme);
				hash.put("descripcion",desc);
				hash.put("plazo",plazo);
				regs.add(hash);
			}
			JSONArray jsonArr = new JSONArray();
			jsonArr = JSONArray.fromObject(regs);
			infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
		}else{
			infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
		}
	}else{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("Parametrizacion")){

	Vector paramEpo = new Vector();
	String plazo_min	="",	plazo_epo = "", CuentaCorriente =""; //Fodea 014-2010
	String ic_epo 		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");

	ParametrosDist parametrosDist = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

	JSONObject jsonObj = new JSONObject();
	
	if(!"".equals(ic_epo)){
		paramEpo = parametrosDist.getParamEpoa(ic_epo);
		if(paramEpo!=null && paramEpo.size()>0){
			plazo_min = (String)paramEpo.get(0);
		}
		CuentaCorriente= parametrosDist.obtieneTipoCredito(ic_epo);
		plazo_epo= parametrosDist.plazoPorEpo(ic_epo);
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("plazo_epo", plazo_epo);
	jsonObj.put("plazo_min", plazo_min);
	jsonObj.put("CuentaCorriente", CuentaCorriente);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Aceptar")){

	JSONObject jsonObj = new JSONObject();

	ParametrosDist parametrosDist = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
	String ic_epo 		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String plazos[] 	= request.getParameterValues("plazo");
	String ic_pymes[] = request.getParameterValues("ic_pymes");

	if(!ic_epo.equals("") && plazos!=null && plazos.length>0 && ic_pymes!=null && ic_pymes.length>0){
		try{
			parametrosDist.setPlazosPyme(ic_epo, ic_pymes, plazos);
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("msg", "Asignacion de Plazo(s) llevada a cabo con exito");
		} catch(Exception e) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", e);
		}	
	}
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>