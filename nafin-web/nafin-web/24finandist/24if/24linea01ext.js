Ext.onReady(function() { 

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------
	
	var auxLink;
	var limpiar=false;
	var mensajeVal="";
	var formularios=new Array();
	var minVence;
	var form;
	var tSolicI=0;

	function makeTimeoutFunc(param) { 
		return function() {
			if (tSolicI == 1){
				Ext.Msg.alert("Mensaje de p�gina web","Ahora que ha capturado la L�nea de Cr�dito Inicial, &nbsp;<br>debe capturar las Tasas por EPO correspondientes<br>Presione el boton Salir para continuar.");
			}else if(tSolicI > 1){
				Ext.Msg.alert("Mensaje de p�gina web","Ahora que ha capturado las L�neas de Cr�dito Iniciales, \ndebe capturar las Tasas por EPO correspondientes\nPresione el boton Salir para continuar.");
			}
		} 
	} 

	function timeMsg(){
		setTimeout(makeTimeoutFunc(), 3000);
	}

	function Limpiar() {
		Ext.getCmp('ic_epo').reset();
		Ext.getCmp('_tipoSol').reset();
		Ext.getCmp('_ic_moneda').reset();
		Ext.getCmp('mtoAuto').reset();
		Ext.getCmp('vencimiento').reset();
		//Ext.getCmp('vencimiento').setReadOnly(false);
		Ext.getCmp('nCtaEpo').reset();
		//Ext.getCmp('nCtaEpo').setReadOnly(false);
		Ext.getCmp('nCtaEpo').hide();
		Ext.getCmp('nCtaIf').reset();
		Ext.getCmp('nCtaIf').hide();
		Ext.getCmp('disDetalle').hide();
		Ext.getCmp('disMsgDetalle').hide();
		Ext.getCmp('disMsgDetalleNo').hide();
		Ext.getCmp('_tipoInt').hide();
		Ext.getCmp('_tipoInt').reset();
		Ext.getCmp('_tipoInt').allowBlank = true;
		Ext.getCmp('_folioSol').hide();
		Ext.getCmp('_folioSol').reset();
		Ext.getCmp('btnAgregar').hide();
		Ext.getCmp('btnLimpiar').hide();		
	}

	function recargar(aux)	{
		if ( Ext.isEmpty(Ext.getCmp('ic_epo').getValue()) && aux==1)
			Limpiar();
		if ( Ext.isEmpty(Ext.getCmp('ic_epo').getValue()) && aux == 2){
			Ext.getCmp('_tipoSol').reset();
			Ext.getCmp('ic_epo').markInvalid("Seleccione primero una EPO");
			return;
		}
	}

	function verificaPanel(myPanel){
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype === 'panel')	{
				verificaPanel(panelItem);
			}else{
				if (!panelItem.isValid() && panelItem.isVisible())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}

	function validaFecha(objeto,minimo){
		if(!Ext.isEmpty(objeto.getValue())){
			if(!revisaFecha(objeto))
				return false;
			else if(minimo!=""){
				var comp = datecomp(minimo,objeto.value);
				if(comp==1||comp==0){
					Ext.Msg.alert("","La fecha de vencimiento debe ser mayor a "+minimo);
					objeto.selectText();
					return false;
				}
			}
			return true;	
		}else{
			return true;
		}	
	}
	
	function validaFechaAmRed(objeto){
	
		var hoy = new Date();
		var dia = hoy.getDate();
		var mes = hoy.getMonth() + 1;  
		if(navigator.appName =='Microsoft Internet Explorer'){
			var anio = hoy.getYear();
		}else{
			var anio = hoy.getYear() + 1900;
		}		
		var fechahoy = dia+"/"+mes+"/"+anio;
		
	
		if(!Ext.isEmpty(objeto.getValue())){
			if(!revisaFecha(objeto)) {
				return false;
			} else {
				var comp = datecomp(fechahoy,objeto.value);
				if(comp==1 || comp==0){
					Ext.Msg.alert("","La fecha de vencimiento debe ser mayor al dia actual ");
					objeto.selectText();
					Ext.getCmp('vencimiento').setValue(''); 
					return false;
				}
			}
			return true;	
		}else{
			return true;
		}	
	}
	
	function showHide(){
		Ext.getCmp('mtoAuto').reset();
		Ext.getCmp('vencimiento').reset();
		//Ext.getCmp('vencimiento').setReadOnly(false);
		Ext.getCmp('nCtaEpo').reset();
		//Ext.getCmp('nCtaEpo').setReadOnly(false);
		Ext.getCmp('nCtaEpo').hide();
		Ext.getCmp('nCtaIf').reset();
		Ext.getCmp('nCtaIf').hide();
		Ext.getCmp('disMsgDetalle').hide();
		Ext.getCmp('disMsgDetalleNo').hide();
		if (auxLink === "A") {
			if (auxLink === "A" && ( Ext.isEmpty(Ext.getCmp('_tipoSol').getValue()) ||  Ext.getCmp('_tipoSol').getValue() === "I") ) {
				Ext.getCmp('nCtaEpo').show();
			}else{
				if ( !Ext.isEmpty(Ext.getCmp('_folioSol').getValue()) ) {
					fp.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24linea01ext.data.jsp',
						params: {
									ic_epo:Ext.getCmp('ic_epo').getValue(),
									solic:Ext.getCmp('_tipoSol').getValue(),
									peticion:"epo",
									informacion:'detalleCuenta',
									folio:Ext.getCmp('_folioSol').getValue(),
									ic_moneda:Ext.getCmp('_ic_moneda').getValue()
									
						},
						callback: procesaDetalleCuenta
					});
				}else{
					Ext.getCmp('disMsgDetalle').show();
				}
			}
		}else{
			if (	auxLink === "I" && (Ext.isEmpty(Ext.getCmp('_tipoSol').getValue()) ||  Ext.getCmp('_tipoSol').getValue() === "I")	){
				Ext.getCmp('nCtaIf').show();
			}else{
				if ( !Ext.isEmpty(Ext.getCmp('_folioSol').getValue()) ) {
					fp.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24linea01ext.data.jsp',
						params: {
									ic_epo:Ext.getCmp('ic_epo').getValue(),
									solic:Ext.getCmp('_tipoSol').getValue(),
									peticion:"if",
									informacion:'detalleCuenta',
									folio:Ext.getCmp('_folioSol').getValue(),
									ic_moneda:Ext.getCmp('_ic_moneda').getValue()
						},
						callback: procesaDetalleCuenta
					});
				}else{
					Ext.getCmp('disMsgDetalle').show();
				}
			}
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdf');
			toolbar.setWidth(toolbar.getWidth()+90);
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			toolbar.setWidth(toolbar.getWidth()+90);
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarConfirmar(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			fp.hide();
			//Ext.getCmp('gridTotales').tools['close'].hide();
			Ext.getCmp('barraGrid').hide();
			fpCoins.show();
			Ext.getCmp('disAcuse').body.update(infoR._acuse);
			Ext.getCmp('disFechaCarga').body.update(infoR.fechaCarga);
			Ext.getCmp('disHoraCarga').body.update(infoR.horaCarga);
			Ext.getCmp('disUsuario').body.update('<div class="formas" >'+infoR.usuario+'<div>');
			Ext.getCmp('btnGenerarArchivo').setHandler( function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url : '24linea01csv_ext.jsp',
					params: Ext.apply(opts.params,{
									_acuse: infoR._acuse,
									fechaHoy:infoR.fechaCarga,
									horaActual:infoR.horaCarga,
									usuario:infoR.usuario
								}),
					callback: procesarSuccessFailureGenerarArchivo
				});
			});
			Ext.getCmp('btnGenerarPDF').setHandler( function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url : '24linea01pdf_ext.jsp',
					params: Ext.apply(opts.params,{
									_acuse: infoR._acuse,
									fechaHoy:infoR.fechaCarga,
									horaActual:infoR.horaCarga,
									usuario:infoR.usuario
								}),
					callback: procesarSuccessFailureGenerarPDF
				});
			});
			var cm = grid.getColumnModel();
			cm.setHidden(cm.findColumnIndex('IFBANCO'),false);
			
			fpAcuse.show();
			toolbar.show();
			timeMsg();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaCapturaLinea(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var cm = grid.getColumnModel();
			cm.setHidden(cm.findColumnIndex('IFBANCO'),true);
			var newReg = Ext.data.Record.create(['NAE','EPO','CLAVE_EPO','TIPO_SOL','IC_SOL','FOLIO_SOL','MONEDA','MONTO_AUTO','TIPO_COBRO','CLAVE_COBINT'
															,'FECHA_VTO','AFILIADOS','CUENTA_EPO','CUENTA_IF','IC_MONEDA','IFBANCO','PLAZO']);
			consultaData.add(new newReg({	'NAE':infoR.noNafinE,
													'EPO':infoR.ic_epo,
													'CLAVE_EPO':form.epoDM,
													'TIPO_SOL':infoR.solic,
													'IC_SOL':form.tipoSol,
													'FOLIO_SOL':form.folioSol,
													'MONEDA':infoR.nomMoneda,
													'MONTO_AUTO':form.mtoAuto,
													'TIPO_COBRO':infoR.cobint,
													'CLAVE_COBINT':form.tipoInt,
													'FECHA_VTO':form.vencimiento,
													'AFILIADOS':infoR.cliAfil,
													'CUENTA_EPO':form.nCtaEpo,
													'CUENTA_IF':form.nCtaIf,
													'IC_MONEDA':form.ic_moneda,
													'IFBANCO':infoR.ifBanco,
													'PLAZO':infoR.plaz	}) );
			var sumMontoMN=0;
			var sumMontoDL=0;
			var countMN = 0;
			var countDL = 0;
			consultaData.each(function(registro){
				if (registro.get('IC_MONEDA') === "1"){
					countMN++;
					sumMontoMN += parseFloat(registro.get('MONTO_AUTO'));
				}else if(registro.get('IC_MONEDA') === "54"){
					countDL++;
					sumMontoDL += parseFloat(registro.get('MONTO_AUTO'));
				}
			});
			if (countMN > 0){
				var regMN = totalesData.getAt(0);
				regMN.set('NOMMONEDA','Moneda Nacional');
				regMN.set('TOTAL_REGISTROS',countMN);
				regMN.set('TOTAL_MONTO_AUTORIZADO',sumMontoMN);

				if(countDL > 0){
					var regDL = totalesData.getAt(1);
					regDL.set('NOMMONEDA','Total Dolares');
					regDL.set('TOTAL_REGISTROS',countDL);
					regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
				}
			}else if(countDL > 0){
				var regDL = totalesData.getAt(0);
				regDL.set('NOMMONEDA','Total Dolares');
				regDL.set('TOTAL_REGISTROS',countDL);
				regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
			}
			Ext.getCmp('totMn').body.update('<div align="left">'+countMN+'</div>');
			Ext.getCmp('montoMn').body.update('<div align="left">'+Ext.util.Format.number(sumMontoMN, '$ 0,0.00')+'</div>');
			Ext.getCmp('totDl').body.update('<div align="left">'+countDL+'</div>');
			Ext.getCmp('montoDl').body.update('<div align="left">'+Ext.util.Format.number(sumMontoDL, '$ 0,0.00')+'</div>');
			grid.show();
			var totalesCmp = Ext.getCmp('gridTotales');
			if (!totalesCmp.isVisible()) {
				totalesCmp.show();
			}
			formularios[formularios.length]=fp.getForm().getValues();

			var jsonData = consultaData.data.items;
			var noNafinEs_aux	=[];
			var eposDM_aux		=[];
			var ic_epos_aux	=[];
			var solics_aux		=[];
			var tipoSol_aux	=[];
			var folios_aux		=[];
			var monedas_aux	=[];
			var nomMoneda		=[];
			var montoAutos_aux=[];
			var plazs_aux		=[];
			var cobints_aux	=[];
			var tipoCobro_aux	=[];
			var vencimientos_aux=[];
			var cliAfils_aux	=[];
			var nCtaEpos_aux	=[];
			var ifBancos_aux	=[];
			var nCtaIfs_aux	=[];
			Ext.each(jsonData, function(item,index,arrItem){
				noNafinEs_aux.push(item.data.NAE);
				eposDM_aux.push(item.data.CLAVE_EPO);
				ic_epos_aux.push(item.data.EPO);
				solics_aux.push(item.data.IC_SOL);
				tipoSol_aux.push(item.data.TIPO_SOL);
				folios_aux.push(item.data.FOLIO_SOL);
				monedas_aux.push(item.data.IC_MONEDA);
				nomMoneda.push(item.data.MONEDA);
				montoAutos_aux.push(item.data.MONTO_AUTO);
				plazs_aux.push(item.data.PLAZO);
				cobints_aux.push(item.data.CLAVE_COBINT);
				tipoCobro_aux.push(item.data.TIPO_COBRO);
				vencimientos_aux.push(item.data.FECHA_VTO);
				cliAfils_aux.push(item.data.AFILIADOS);
				nCtaEpos_aux.push(item.data.CUENTA_EPO);
				ifBancos_aux.push(item.data.IFBANCO);
				nCtaIfs_aux.push(item.data.CUENTA_IF);
				if( item.data.IC_SOL == "I" ){
					tSolicI++;
				}
			});
			if (tSolicI >= 1){
				Ext.getCmp('btnSalir').setHandler( function(boton, evento) {
					boton.setIconClass('loading-indicator');
					window.location = '24forma03ext.jsp';
				});
			}else{
				Ext.getCmp('btnSalir').setHandler( function(boton, evento) {
					boton.setIconClass('loading-indicator');
					location.reload();
				});
			}

			Ext.getCmp('btnConfirmar').setHandler( function(boton, evento) {
					pnl.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24linea01ext.data.jsp',
						params:	{	informacion: "Confirma",
										totDocs:			countMN,
										totMtoAuto:		sumMontoMN,
										totDocsDol:		countDL,
										totMtoAutoDol:	sumMontoDL,
										noNafinEs_aux:	noNafinEs_aux,
										eposDM_aux:		eposDM_aux,
										ic_epos_aux:	ic_epos_aux,
										solics_aux:		solics_aux,
										tipoSol_aux:	tipoSol_aux,
										folios_aux:		folios_aux,
										monedas_aux:	monedas_aux,
										nomMoneda:		nomMoneda,
										montoAutos_aux:montoAutos_aux,
										plazs_aux:		plazs_aux,
										cobints_aux:	cobints_aux,
										tipoCobro_aux:	tipoCobro_aux,
										vencimientos_aux:vencimientos_aux,
										cliAfils_aux:	cliAfils_aux,
										nCtaEpos_aux:	nCtaEpos_aux,
										ifBancos_aux:	ifBancos_aux,
										nCtaIfs_aux:	nCtaIfs_aux
									},
						callback: procesarConfirmar
					});
			});

			Limpiar();
			fp.el.unmask();
			return;
		} else {
			NE.util.mostrarConnError(response,opts);
			fp.el.unmask();
		}
	}

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			auxLink = infoR.auxLink;
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaDetalleCuenta(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.auxCad != undefined){
				if (auxLink === "A" && !(Ext.getCmp('_tipoSol').getValue() === "I")) {
					Ext.getCmp('nCtaEpo').setValue(infoR.auxCad);
					//Ext.getCmp('nCtaEpo').setReadOnly(true);
					Ext.getCmp('nCtaEpo').show();
				}
				if (auxLink === "I" && !(Ext.getCmp('_tipoSol').getValue() === "I")) {
					Ext.getCmp('nCtaIf').setValue(infoR.auxCad);
					Ext.getCmp('nCtaIf').show();
					Ext.getCmp('ifBanco').setValue(infoR.auxCad2);
				}
			}else{
				Ext.getCmp('disMsgDetalleNo').show();
			}			
			if(Ext.getCmp('_tipoSol').getValue() != "I"){
				Ext.getCmp('mtoAuto').setMinValue(parseFloat(infoR.minimo)+1);
				Ext.getCmp('mtoAuto').minText = "El nuevo monto no puede ser menor $ "+roundOff(infoR.minimo,2);
				if (infoR.auxLink != undefined){
					Ext.getCmp('mtoAuto').setValue(infoR.auxLink);
				}
			}
			if (opts.params.solic === "R"){
				Ext.getCmp('vencimiento').setReadOnly(false);
				minVence = infoR.minVence;
			}else if (opts.params.solic === "A"){
			//	Ext.getCmp('vencimiento').setReadOnly(true);
				Ext.getCmp('vencimiento').setValue(infoR.vencimiento);
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaValidaLinea(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.limpiar){
				Ext.Msg.alert('',infoR.mensajeVal);
				Limpiar();
				return;
			}else{
				fp.el.mask('Enviando...', 'x-mask-loading');
				Ext.Ajax.request({
					url: '24linea01ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{informacion: "capturaLinea"}),
					callback: procesaCapturaLinea
				});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea01ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var tipoSolData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['I','INICIAL'],
			['A','AMPLIACI�N/REDUCCI�N '],
			['R','RENOVACI�N']
		 ]
	});

	var procesarTipoFolio = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('_folioSol').valueNotFoundText = '';
		}else{
			Ext.getCmp('_folioSol').valueNotFoundText = 'No existen folios relacionados';
		}
		Ext.getCmp('_folioSol').reset();
	}

	var catalogoFolioSolData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea01ext.data.jsp',
		baseParams: {	informacion: 'TipoFolio'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load: procesarTipoFolio,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea01ext.data.jsp',
		baseParams: {	informacion: 'CatalogoMonedaDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoTipoCobroData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea01ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTipoCobroDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var totalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO_AUTORIZADO'}
		],
		data:[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_AUTORIZADO':''},
				{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_AUTORIZADO':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name:'NAE'},
			{name:'CLAVE_EPO'},
			{name:'EPO'},
			{name:'TIPO_SOL'},
			{name:'IC_SOL'},
			{name:'FOLIO_SOL'},
			{name:'IC_MONEDA'},
			{name:'MONEDA'},
			{name:'MONTO_AUTO'},
			{name:'TIPO_COBRO'},
			{name:'CLAVE_COBINT'},
			{name:'FECHA_VTO'},
			{name:'AFILIADOS'},
			{name:'CUENTA_EPO'},
			{name:'CUENTA_IF'},
			{name:'IFBANCO'},
			{name:'PLAZO'}
		],
		totalProperty : 'total',
		data:	[{	'NAE':'','CLAVE_EPO':'','EPO':'','TIPO_SOL':'','IC_SOL':'','FOLIO_SOL':'','IC_MONEDA':'','MONEDA':'','MONTO_AUTO':'',
					'TIPO_COBRO':'','CLAVE_COBINT':'','FECHA_VTO':'','AFILIADOS':'','CUENTA_EPO':'','CUENTA_IF':'','IFBANCO':'','PLAZO':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridTotales = {
		xtype:'grid',	store:totalesData,	id:'gridTotales',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:940,	height:100,	title:'Totales', hidden:true,	
		columns: [
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 400,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),menuDisabled:true
			},{
				header: 'Monto Autorizado',	dataIndex: 'TOTAL_MONTO_AUTORIZADO',	width:400,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		],
		/*tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],*/
		frame: false
	};

	var grid = new Ext.grid.GridPanel({
		id:'grid',store: consultaData,	columnLines:true, hidden:true,
		stripeRows:true,	loadMask:true,	width:938,	height:250,	style:'margin:0 auto;',	frame:false,
		columns: [
			{
				header:'N�mero Nafin-Electr�nico',	tooltip:'N�mero Nafin-Electr�nico',	dataIndex:'NAE',	sortable:true,	width:140,	align:'center', hideable:false
			},{
				header:'EPO',	tooltip:'EPO',	dataIndex:'EPO',	sortable:false,	width:150,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Tipo de solicitud',	tooltip:'Tipo de solicitud',	dataIndex:'TIPO_SOL',	sortable:false,	width:100,	align:'center'
			},{
				header:'Folio solicitud relacionada',	tooltip:'Folio solicitud relacionada',	dataIndex:'FOLIO_SOL',	sortable:false,	width:140,	align:'center'
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:false,	width:150,	align:'center'
			},{
				header:'Monto autorizado',	tooltip:'Monto autorizado',	dataIndex:'MONTO_AUTO',	sortable:false,	width:120,	align:'right',renderer: Ext.util.Format.numberRenderer('$ 0,000.00')
			},{
				header:'Tipo de cobro de intereses',	tooltip:'Tipo de cobro de intereses',	dataIndex:'TIPO_COBRO',	sortable:false,	width:150,	align:'center'
			},{
				header:'Fecha de vencimiento',	tooltip:'Fecha de vencimiento',	dataIndex:'FECHA_VTO',	sortable:false,	width:120,	align:'center'
			},{
				header:'N�m. clientes afiliados a la EPO',	tooltip:'N�m. clientes afiliados a la EPO',	dataIndex:'AFILIADOS',	sortable:false,	width:160,	align:'center'
			},{
				header:'No. Cuenta EPO',	tooltip:'No. Cuenta EPO',	dataIndex:'CUENTA_EPO',	sortable:false,	width:110,	align:'center'
			},{
				header:'IF Banco de Servicio',	tooltip:'IF Banco de Servicio',	dataIndex:'IFBANCO',	sortable:false,	width:110,	align:'center', hidden:true, hieable:false
			},{
				header:'No. Cuenta IF',	tooltip:'No. Cuenta IF',	dataIndex:'CUENTA_IF',	sortable:false,	width:110,	align:'center'
			}
		],
		bbar: {
			id:'barraGrid',
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',	text: 'Confirmar',	id: 'btnConfirmar',	iconCls:'autorizar'
				},{
					xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5
				},{
					xtype: 'button',	text: 'Cancelar',	id: 'btnCancelar',	iconCls:'icoRechazar',	handler: function(boton, evento) {location.reload();}
				}
			]
		}
	});

	var toolbar = new Ext.Toolbar({
		id:'barBotones',
		style: 'margin:0 auto;',
		autoScroll : true,
		hidden:false,
		width: 310,
		height: 50,
		 items: [
			'->','-',
			{
				text:'Generar Archivo',	id:'btnGenerarArchivo',	iconCls:'icoXls', scale:'medium'
			},{
				text:'Bajar Archivo',	id:'btnBajarArchivo',	hidden:true, scale:'medium'
			},'-',{
				xtype: 'tbspacer', width: 2
			},{
				text:'Generar Pdf',	id:'btnGenerarPDF',	iconCls:'icoPdf',scale:'medium'
			},{
				text:'Bajar Pdf',	id:'btnBajarPdf',	hidden:true, scale:'medium'
			},'-',{
				xtype: 'tbspacer', width: 2
			},{
				text:'Salir',id:'btnSalir',iconCls:'icoLimpiar',scale:'medium'
			},'-',{
				xtype: 'tbspacer', width: 2
			}
		]
	});

	var elementosCoins = [
		{
			xtype: 'panel',layout:'table',	width:820,	border:true,	layoutConfig:{ columns: 4 },
			defaults: {frame:false, border:true,width:205, height: 35,bodyStyle:'padding:8px'},
			items:[
				{	width:410,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Moneda Nacional</div>'	},
				{	width:410,	colspan:2,	border:false,	frame:true,	html:'<div align="center">D�lares</div>'	},
				{	border:false,	frame:true,	html:'<div class="formas" align="center">N�mero de solicitudes capturadas</div>'	},
				{	border:false,	frame:true,	html:'<div class="formas" align="center">Monto de solicitudes capturadas</div>'	},
				{	border:false,	frame:true,	html:'<div class="formas" align="center">N�mero de solicitudes capturadas</div>'	},
				{	border:false,	frame:true,	html:'<div class="formas" align="center">Monto de solicitudes capturadas</div>'	},
				{	id:'totMn',		html:'&nbsp;'	},
				{	id:'montoMn',	html:'&nbsp;'	},
				{	id:'totDl',		html:'&nbsp;'	},
				{	id:'montoDl',	html:'&nbsp;'	},
				{	height: 25,	colspan:4,	width:820,	html:'<div class="formas" align="center">Leyenda legal</div>'	}
			]
		}
	];

	var fpCoins=new Ext.FormPanel({	style: 'margin:0 auto;',	height:'auto',	width:820,	border:true,	frame:false,	items:elementosCoins,	hidden:true 	});

	var elementosAcuse = [
		{
			xtype: 'panel',layout:'table',	width:699,	border:false,	layoutConfig:{ columns: 2 },	style: 'margin:0 auto;',
			defaults: {frame:false, border: true,width:170	, height: 35,	bodyStyle:'padding:6px'},
			items:[
				{	width:698,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Datos de cifras de control</div>'	},
				{	html:'<div class="formas" align="left">N�m. acuse</div>'	},
				{	width:535,id:'disAcuse',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Fecha</div>'	},
				{	width:535,id:'disFechaCarga',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Hora</div>'	},
				{	width:535,id:'disHoraCarga',	html:'&nbsp;'},
				{	html:'<div class="formas" align="left">Nombre y n�mero de usuario</div>'	},
				{	width:535,id:'disUsuario',html: '&nbsp;'	}
			]
		}
	];

	var fpAcuse=new Ext.FormPanel({	style: 'margin:0 auto;',	height:'auto',	width:700,	border:true,	frame:false,	items:elementosAcuse,	hidden:true 	});

	var elementosForma = [
		{
			xtype: 'combo',
			id:	'ic_epo',
			name: 'epoDM',
			hiddenName : 'epoDM',
			fieldLabel: 'EPO',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			allowBlank: false,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEpoDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:function(combo){
							var comboTipo = Ext.getCmp('_tipoInt');
							comboTipo.setValue('');
							comboTipo.store.removeAll();
							comboTipo.store.reload({	params: {epoDM: combo.getValue() }	});
							comboTipo.setVisible(true);
							Ext.getCmp('_folioSol').setValue('');
							Ext.getCmp('disDetalle').show();
							Ext.getCmp('btnAgregar').show();
							Ext.getCmp('btnLimpiar').show();
							var cmbFol = Ext.getCmp('_folioSol');
							if (!Ext.isEmpty(Ext.getCmp('_tipoSol').getValue())){
								if (	Ext.getCmp('_tipoSol').getValue() != "I" && !Ext.isEmpty(Ext.getCmp('_ic_moneda').getValue())	) {
									cmbFol.setValue('');
									cmbFol.store.removeAll();
									cmbFol.store.reload({	params: {epoDM: Ext.getCmp('ic_epo').getValue(),
																				solic:Ext.getCmp('_tipoSol').getValue(), moneda:Ext.getCmp('_ic_moneda').getValue()}	});
									cmbFol.setVisible(true);
								}else{
									cmbFol.setVisible(false);
								}
							}
							showHide();
				},
				change:function(combo, ewVal, oldVal) {recargar(1);}
			}
		},{
			xtype:'panel', layout:'table',	width:690,	border:false, fram:true,	layoutConfig:{ columns: 2 }, anchor:'100%',
			defaults: {width:313, autoHeight: true},
			items: [
				{
					xtype:'panel', layout:'form',	labelWidth:120,defaults: {	msgTarget: 'side',	anchor: '-20'	},
					items:[
						{
							xtype: 'combo',
							id:	'_tipoSol',
							name: 'tipoSol',
							hiddenName : 'tipoSol',
							emptyText: 'Seleccionar. . .',
							fieldLabel: 'Tipo de solicitud',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							forceSelection : false,
							triggerAction : 'all',
							allowBlank: false,
							lazyRender:true,
							typeAhead: true,
							minChars : 1,
							store: tipoSolData,
							listeners:{
								select:	{fn:function(combo){
												if(combo.getValue() === "I"){	
													Ext.getCmp('_tipoInt').allowBlank = false;
												}else{
													Ext.getCmp('_tipoInt').allowBlank = true;
												}
												if (!Ext.isEmpty(combo.getValue()) && !Ext.isEmpty(Ext.getCmp('ic_epo').getValue())){
														var cmbFol = Ext.getCmp('_folioSol');
														if (	combo.getValue() != "I" && !Ext.isEmpty(Ext.getCmp('_ic_moneda').getValue())	) {
															cmbFol.setValue('');
															cmbFol.store.removeAll();
															cmbFol.store.reload({	params: {epoDM: Ext.getCmp('ic_epo').getValue(),
																										solic:combo.getValue(), moneda:Ext.getCmp('_ic_moneda').getValue()}	});
															cmbFol.setVisible(true);
														}else{
															minVence="";
															Ext.getCmp('mtoAuto').setMinValue(0);
															cmbFol.setVisible(false);
														}
													showHide();
												}
											}
								},
								change:function(combo, ewVal, oldVal) {recargar(2);}
							}
						}
					]
				},{
					xtype:'panel', layout:'form',	defaults: {	msgTarget: 'side',	anchor: '-20'	},width:355, labelWidth:120,
					items:[
						{
							xtype: 'combo',
							id:'_folioSol',
							name: 'folioSol',
							hiddenName:'folioSol',
							fieldLabel:'Folio de solicitud relacionada',
							emptyText: 'Seleccione una solicitud relacionada',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							allowBlank: false,
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoFolioSolData,
							tpl : NE.util.templateMensajeCargaCombo,
							hidden:true,
							listeners:{
								change:function(combo, ewVal, oldVal) {								
									recargar(2);									
								},
								select:function(combo) {									
									showHide();
								}
								
							}
						}
					]
				}
			]
		},{
			xtype: 'combo',
			name: 'ic_moneda',
			id:	'_ic_moneda',
			hiddenName : 'ic_moneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccionar . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			allowBlank: false,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo, anchor:'49%',
			listeners:{
				select:	{fn:function(combo){
								Ext.getCmp('mtoAuto').reset();
								if (!Ext.isEmpty(Ext.getCmp('ic_epo').getValue()) && !Ext.isEmpty(Ext.getCmp('_tipoSol').getValue()) ){
									var cmbFol = Ext.getCmp('_folioSol');
									if (	Ext.getCmp('_tipoSol').getValue() != "I" && !Ext.isEmpty(combo.getValue())	) {
										cmbFol.setValue('');
										cmbFol.store.removeAll();
										cmbFol.store.reload({	params: {epoDM: Ext.getCmp('ic_epo').getValue(),
																					moneda:combo.getValue(), solic:Ext.getCmp('_tipoSol').getValue()}	});
										cmbFol.setVisible(true);
									}else{
										cmbFol.setVisible(false);
									}
								}
							}
				},
				change:function(combo, ewVal, oldVal) {recargar(2);}
			}
		},{
			xtype: 'numberfield',	name: 'mtoAuto',	id: 	'mtoAuto',	fieldLabel: 'Monto autorizado',	allowBlank:false,	maxLength: 10,	anchor:'38%', minValue:0, decimalPrecision:9
		},{
			xtype: 'datefield',
			fieldLabel: 'Fecha de vencimiento',	name: 'vencimiento',	id: 'vencimiento',	allowBlank: false,	startDay: 0,	anchor:'34%',
			listeners:{
				change:function(combo, ewVal, oldVal) {
							if(Ext.getCmp('_tipoSol').getValue()==="R" ){							
								validaFecha(combo,minVence);
							}else if( Ext.getCmp('_tipoSol').getValue()==="A" ){	 
								validaFechaAmRed(combo);
							}
				}
			}
		},{
			xtype: 'combo',
			name: 'tipoInt',
			id:	'_tipoInt',
			hiddenName : 'tipoInt',
			fieldLabel: 'Tipo de cobro de inter�s',
			emptyText: 'Seleccione un tipo',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1, anchor:'49%',
			store: catalogoTipoCobroData, hidden:true,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype:'displayfield', id:'disDetalle',value:'<div class="titulos" align="center">Detalle de cuentas</div>', hidden:true
		},{
			xtype:'displayfield', id:'disMsgDetalle',value:'<div class="formas" align="center">Seleccione el folio de solicitud relacionada para mostrar la informaci�n</div>', hidden:true
		},{
			xtype:'displayfield', id:'disMsgDetalleNo',value:'<div class="formas" align="center">No existen Detalles</div>', hidden:true
		},{
			xtype: 'textfield',
			name: 'nCtaEpo',
			id: 	'nCtaEpo',
			vtype:'alphanum',
			fieldLabel: 'EPO - Num. cuenta',
			allowBlank:false,	maxLength: 9,	anchor:'40%',	hidden:true
		},{
			xtype: 'textfield',
			name: 'nCtaIf',
			id: 	'nCtaIf',
			vtype:'alphanum',
			fieldLabel: 'IF - Num. cuenta',
			allowBlank:false,	maxLength: 11,	anchor:'40%',	hidden:true
		},{
			xtype: 'hidden',	name: 'ifBanco',	id: 	'ifBanco',	maxLength: 9
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title:'<div align="center">Datos de Captura</div>',
		width: 690,
		frame: true,
		collapsible: true,
		titleCollapse: true,
		labelWidth:120,
		labelAlign:'right',
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Agregar',	id: 'btnAgregar',	iconCls: 'aceptar',	hidden:true, formBind:true,
				handler: function(boton, evento) {
								if(!verificaPanel(Ext.getCmp('forma'))) {
									return;
								}
								form = fp.getForm().getValues();
								var folio = Ext.getCmp('_folioSol');
								if (folio.isVisible() && Ext.isEmpty(folio.getValue())){
									folio.markInvalid('El campo no puede estar vacio');
									return;
								}
								var dt = new Date();
								var res = datecomp(form.vencimiento,dt.format('d/m/Y'));
								if(res==2 || res == 0 || res == -1){
									Ext.getCmp('vencimiento').markInvalid("La fecha de vencimiento no es valida.");
									return;	
								}
								
								var tipoInt = Ext.getCmp('_tipoInt');
								if (tipoInt.isVisible() && Ext.isEmpty(tipoInt.getValue())){
									tipoInt.markInvalid('El campo es obligatorio');
									return;
								}
								
								var ic_epo = form.epoDM;
								var solic = form.tipoSol;
								var moneda = form.ic_moneda;
								var bandera=false;
								limpiar = false;
								mensajeVal="";

								if(formularios.length > 0) {
									for(var i=0;i<formularios.length;i++){
										if (ic_epo === formularios[i].epoDM && solic === "I" && solic === formularios[i].tipoSol && moneda === formularios[i].ic_moneda){
											bandera=true;
											limpiar=true;
											mensajeVal = "Ya se capturo una linea inicial para esta EPO y Moneda";
											break;
										}else{
											if (ic_epo === formularios[i].epoDM && solic === "R" && solic === formularios[i].tipoSol){
												bandera=true;
												limpiar=true;
												mensajeVal = "Ya se capturo una renovacion para esta EPO y Moneda";
												break;
											}
										}
									}
								}
								if ((solic === "I" || solic === "R") && !bandera){
										fp.el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: '24linea01ext.data.jsp',
											params: {
														moneda:moneda,
														epoDM:ic_epo,
														vencimiento:form.vencimiento,
														solic:solic,
														informacion:"validaLinea"
											},
											callback: procesaValidaLinea
										});
								}else{
									if(limpiar){
										Ext.Msg.alert("",mensajeVal);
										Limpiar();
										return;
									}
									fp.el.mask('Enviando...', 'x-mask-loading');
									Ext.Ajax.request({
										url: '24linea01ext.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{informacion: "capturaLinea"}),
										callback: procesaCapturaLinea
									});
								}
				} //fin handler
			},{
				text: 'Limpiar',	id:'btnLimpiar',	iconCls:'icoLimpiar',	hidden:true,
				handler: function() {
					window.location = '24linea01ext.jsp';											
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,	fpCoins,	NE.util.getEspaciador(10),
			grid,	gridTotales,	NE.util.getEspaciador(10),	fpAcuse,
			NE.util.getEspaciador(10),	toolbar
		]
	});

	consultaData.loadData('');
	catalogoEpoDistData.load();
	catalogoMonedaData.load();
	toolbar.hide();
	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24linea01ext.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});

});