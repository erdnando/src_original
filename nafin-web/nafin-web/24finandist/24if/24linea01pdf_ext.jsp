<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	com.netro.exception.*, 
	netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

JSONObject jsonObj = new JSONObject();

String _acuse		= (request.getParameter("_acuse")==null)?"":request.getParameter("_acuse");
String fechaHoy	= (request.getParameter("fechaHoy")==null)?"":request.getParameter("fechaHoy");
String horaCarga	= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");
String usuario		= (request.getParameter("usuario")==null)?"":request.getParameter("usuario");

String totMtoAuto	  	= (request.getParameter("totMtoAuto")==null)?"":request.getParameter("totMtoAuto");
String totDocs	  		= (request.getParameter("totDocs")==null)?"":request.getParameter("totDocs");
String totMtoAutoDol = (request.getParameter("totMtoAutoDol")==null)?"":request.getParameter("totMtoAutoDol");
String totDocsDol	  	= (request.getParameter("totDocsDol")==null)?"":request.getParameter("totDocsDol");

String anoNafinE[] 	= request.getParameterValues("noNafinEs_aux");
String ic_epos[] 		= request.getParameterValues("ic_epos_aux");
String asolic[] 		= request.getParameterValues("tipoSol_aux");
String afolio[] 		= request.getParameterValues("folios_aux");
String amoneda[] 		= request.getParameterValues("nomMoneda");
//String amonto[] 	= request.getParameterValues("montos_aux");
String amontoAuto[] 	= request.getParameterValues("montoAutos_aux");
String aplaz[] 		= request.getParameterValues("plazs_aux");
//String acobint[] 		= request.getParameterValues("cobints_aux");
String tipoCobro[] 	= request.getParameterValues("tipoCobro_aux");
String avencimiento[]= request.getParameterValues("vencimientos_aux");
String acliAfil[] 	= request.getParameterValues("cliAfils_aux");
String anCtaEpo[] 	= request.getParameterValues("nCtaEpos_aux");
String aifBanco[] 	= request.getParameterValues("ifBancos_aux");
String anCtaIf[] 		= request.getParameterValues("nCtaIfs_aux");

try {

	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

	pdfDoc.setTable(4, 50);
	pdfDoc.setCell("Moneda Nacional","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Dólares","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de solicitudes capturadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto de Solicitudes capturadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de solicitudes capturadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto de Solicitudes capturadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(totDocs,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAuto,2),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell(totDocsDol,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAutoDol,2),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Leyenda legal","formas",ComunesPDF.CENTER,4);
	pdfDoc.addTable();

	pdfDoc.setTable(12, 100);
	pdfDoc.setCell("Número Nafin-Electrónico","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo de solicitud","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Folio solicitud relacionada","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto autorizado","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo de cobro de intereses","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Núm. clientes afiliados a la EPO","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("No. Cuenta EPO","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("IF Banco de Servicio","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("No. Cuenta IF","celda01",ComunesPDF.CENTER);
	if (ic_epos.length > 0){
		for(int i=0;i<ic_epos.length;i++)	{
			String banco = aifBanco[i];
			pdfDoc.setCell(anoNafinE[i],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(ic_epos[i],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(asolic[i],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(afolio[i],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(amoneda[i],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(amontoAuto[i],2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(tipoCobro[i],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(avencimiento[i],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(acliAfil[i],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(anCtaEpo[i],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(banco,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(anCtaIf[i],"formas",ComunesPDF.CENTER);
		}
	}
	if (!("0".equals(totDocs))){
		pdfDoc.setCell("Totales MN","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(totDocs,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAuto,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
	}
	if (!("0".equals(totDocsDol))){
		pdfDoc.setCell("Totales Dolares","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(totDocsDol,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAutoDol,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
	}
	pdfDoc.addTable();
	
	pdfDoc.setTable(2, 50);
	pdfDoc.setCell("Datos de cifras de control","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de Acuse","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(_acuse,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Fecha","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Hora","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(horaCarga,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Nombre y número de usuario","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(usuario,"formas",ComunesPDF.LEFT);
	pdfDoc.addTable();

	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>