Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

	var form = {
		lstEpo:null,
		lstPyme:null,
		txtNumCredito:null,
		txtFchOper1:null,
		txtFchOper2:null,
		txtMonto1:null,
		txtMonto2:null,
		txtFchVencimiento1:null,
		txtFchVencimiento2:null,
		lstEstatus:null,
		lstTipoCobroInte:null,
		lstTipoCredito:null
	}

	function leeRespuesta(){
		window.location = '24forma04ext.jsp';
	}

	function procesaGuardaCambios(opts, success, response) {
		pnl.el.unmask();
		Ext.getCmp('btnGuardar').setIconClass('icoGuardar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				Ext.Msg.show({
					title:	'',
					msg:		'Datos actualizados con �xito',
					buttons:	Ext.Msg.OK,
					fn: leeRespuesta,
					closable:false,
					icon: Ext.MessageBox.INFO
				});
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store,arrRegistros,opts) {
		pnl.el.unmask();
		if (arrRegistros!=null){
				if(!grid.isVisible()){
					grid.show();
				}
				var btnTotales = Ext.getCmp('btnTotales');
				var btnGuardar = Ext.getCmp('btnGuardar');
				var el = grid.getGridEl();

				if(store.getTotalCount()>0){
					btnTotales.enable();
					btnGuardar.enable();
					el.unmask();
				} else {
					btnTotales.disable();
					btnGuardar.disable();
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
		}
	}

	var procesarResumenTotalesData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var gridTotales = Ext.getCmp('gridTotales');
			var elTotal = gridTotales.getGridEl();
			if (!gridTotales.isVisible()) {
				gridTotales.show();
				gridTotales.el.dom.scrollIntoView();
			}
			if(store.getTotalCount() > 0) {
				elTotal.unmask();
			}else {
				Ext.getCmp('btnTotales').disable();
			}
		}
	}

	var tipoCreditoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [	['D','Modalidad 1 (Riesgo Empresa de Primer Orden )'],	['C','Modalidad 2 (Riesgo Distribuidor) ']	]	
	});

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDistribuidor'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesarCatalogoTipoCobro = function(store, arrRegistros, opts) {
		if (store.getCount() > 0) {
			var r = Ext.getCmp('_lstTipoCobroInte').findRecord("clave", "2");
			if(r){
				Ext.getCmp('_lstTipoCobroInte').setValue('2');
			}
		}
	}

	var catalogoTipoCobroData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTipoCobroDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesarCatalogoTipoCobro,exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoPorAsignarData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion']/*,
		 data : [
			//['0','Selecciona Estatus'],
			['11','Operada Pagada'],
			['22','Pendiente de pago IF']
		 ]*/
	});

	var procesarCatalogoEstatus = function(store, arrRegistros, opts) {
		if (store.getCount() > 0) {
			var r = Ext.getCmp('_lstEstatus').findRecord("clave", "11");
			if(r){
				Ext.getCmp('_lstEstatus').setValue('11');
			}
		}
	}

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEstatusDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesarCatalogoEstatus,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '24forma04ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name:'NUM_CREDITO'},
			{name:'NOMBRE_EPO'},
			{name:'NOMBRE_PYME'},
			{name:'TIPO_MONEDA'},
			{name:'MONTO'},
			{name:'TIPO_CONVERSION'},
			{name:'TIPO_CAMBIO'},
			{name:'MONTO_VALUADO'},
			{name:'REF_TASA'},
			{name:'TASA_INTERES'},
			{name:'PLAZO'},
			{name:'FCH_VENCIMIENTO'},
			{name:'MONTO_INTERES'},
			{name:'TIPO_COBRO_INTE'},
			{name:'ESTATUS_SOLI'},
			{name:'FLAG_COMBO'},
			{name:'FLAG_PAGO_REF'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
				beforeLoad:	{fn: function(store, options){
								Ext.apply(options.params, {
										lstEpo:form.lstEpo,
										lstPyme:form.lstPyme,
										txtNumCredito:form.txtNumCredito,
										txtFchOper1:form.txtFchOper1,
										txtFchOper2:form.txtFchOper2,
										txtMonto1:form.txtMonto1,
										txtMonto2:form.txtMonto2,
										txtFchVencimiento1:form.txtFchVencimiento1,
										txtFchVencimiento2:form.txtFchVencimiento2,
										lstEstatus:form.lstEstatus,
										lstTipoCobroInte:form.lstTipoCobroInte,
										lstTipoCredito:form.lstTipoCredito
								})}},
				load: procesarConsultaData,
				exception: {
							fn: function(proxy,type,action,optionsRequest,response,args){
									NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
									procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
							}
				}
		}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma04ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'MONEDA'},
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO'},
			{name: 'TOTAL_MONTO_VALUADO'},
			{name: 'TOTAL_MONTO_INTERES'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarResumenTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarResumenTotalesData(null, null, null);
				}
			}
		}
	});

	var gridTotales = {
		xtype: 'grid',	store: resumenTotalesData,	id: 'gridTotales',	hidden:	true, frame: false, width: 940,	height: 120,	title: 'Totales',
		columns: [
			{header: 'Totales',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 200, resizable:false,menuDisabled:true},
			{header: 'Registros',dataIndex: 'TOTAL_REGISTROS',	width: 240,	align: 'right',resizable:false,menuDisabled:true},
			{header: 'Monto',dataIndex: 'TOTAL_MONTO',width: 240,	align: 'right',resizable:false,menuDisabled:true, renderer: Ext.util.Format.numberRenderer('$ 0,0.00') },
			{header: 'Monto Interes',dataIndex: 'TOTAL_MONTO_INTERES',width: 240,	align: 'right',resizable:false,menuDisabled:true, renderer: Ext.util.Format.numberRenderer('$ 0,0.00')}
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	};

	var grid = new Ext.grid.EditorGridPanel({
		id:'grid',store: consultaData,	columnLines:true, clicksToEdit:1, hidden:true,
		stripeRows:true,	loadMask:true,	height:415,	width:940,	style:'margin:0 auto;',	frame:false,
		columns: [
			{
				header:'N�m. Documento Final',	tooltip:'N�m. Documento Final',	dataIndex:'NUM_CREDITO',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Epo',	tooltip:'Epo',	dataIndex:'NOMBRE_EPO',	sortable:true,	resizable:true,	width:130,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header: 'Distribuidor',	tooltip: 'Distribuidor',	dataIndex:'NOMBRE_PYME',	sortable:true,	resizable:true,	width:200,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'TIPO_MONEDA',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header : 'Monto', tooltip: 'Monto',	dataIndex:'MONTO',	sortable:true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), hidden: false, hideable: false
			},{
				header:'Referencia Tasa de inter�s',	tooltip:'Referencia Tasa de inter�s',	dataIndex:'REF_TASA',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Tasa inter�s',	tooltip:'Tasa inter�s',	dataIndex:'TASA_INTERES',	sortable:true,	resizable:true,	width:130,	align:'center',renderer: Ext.util.Format.numberRenderer('0.00 %')
			},{
				header:'Plazo',	tooltip:'Plazo',	dataIndex:'PLAZO',	sortable:true,	resizable:true,	width:80,	align:'center'
			},{
				header : 'Fecha de vencimiento', tooltip: 'Fecha de vencimiento',	dataIndex : 'FCH_VENCIMIENTO', sortable : true, width : 100, align: 'center', hidden: false
			},{
				header : 'Monto inter�s', tooltip: 'Monto inter�s',	dataIndex : 'MONTO_INTERES',
				sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), hidden: false, hideable: false
			},{
				header:'Tipo de cobro inter�s',	tooltip:'Tipo de cobro inter�s',	dataIndex:'TIPO_COBRO_INTE',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Estatus actual',	tooltip:'Estatus actual',	dataIndex:'ESTATUS_SOLI',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header: 'Estatus por asignar',	tooltip: 'Estatus por asignar',	dataIndex: 'FLAG_COMBO',	sortable: true,	resizable: true,	width: 160,	align: 'center',
				editor: {xtype : 'combo', id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection : true,	triggerAction : 'all',
							typeAhead: true,	minChars : 1,store:catalogoPorAsignarData,lazyInit: false, lazyRender: true, emptyText:'Seleccione...', value:'0',
							listeners:{ focus:{ fn: function (comboField) { comboField.doQuery(comboField.allQuery, true); comboField.expand(); }},
											select:{ fn:function (comboField, record, index) {
																comboField.fireEvent('blur');
															}
													 }
							}},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
									if(!Ext.isEmpty(value)){
										var dato = catalogoPorAsignarData.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoPorAsignarData.getAt(dato);
											value = reg.get('descripcion');
										}
									}else{
											value = 'Selecciona Estatus';
									}
                        return NE.util.colorCampoEdit(value,metadata,registro);
							}
			},{
				header : 'Num. pago ref.', tooltip: 'Num. pago ref.',
				dataIndex : 'FLAG_PAGO_REF', fixed:true,
				sortable : false,	width : 150,	hiddeable: false,
				editor:{
					xtype:	'textfield',
					id: 'txtNumRegPago',
					maxLength: 9
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			},{
				header:'Detalle de pagos de inter�s',	tooltip:'Detalle de pagos de inter�s',	dataIndex:'',	sortable:true,	width:150,	align:'center'
			}
		],
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						resumenTotalesData.load();
					}
				},{xtype: 'tbspacer', width: 5},'-',{
					xtype: 'tbspacer', width: 5
				},{
					text:'Guardar Cambios', id:'btnGuardar',	iconCls:'icoGuardar',
					handler: function(boton){
									var flag = false;
									var jsonData = consultaData.data.items;
									Ext.each(jsonData, function(item,index,arrItem){
										if (!Ext.isEmpty(item.data.FLAG_COMBO) && item.data.FLAG_COMBO != '0'){
											flag = true;
											return false;
										}
									});
									if (!flag){
										Ext.Msg.alert(boton.text,'Debe seleccionar por lo menos uno para continuar');
										var gridEl = Ext.getCmp('grid').getGridEl();
										var col = Ext.getCmp('grid').getColumnModel().findColumnIndex('FLAG_PAGO_REF')
										var rowEl = Ext.getCmp('grid').getView().getCell( 0, col);
										rowEl.scrollIntoView(gridEl,false);
										return;
									}
									Ext.Msg.show({
										title:boton.text,
										msg: '�Desea guardar los cambios?',
										buttons: Ext.Msg.OKCANCEL,
										fn: processResult,
										animEl: 'elId',
										icon: Ext.MessageBox.QUESTION
									});
									boton.setIconClass('loading-indicator');
								}
				},{
					xtype: 'tbspacer', width: 5
				}
			]
		}
	});

	Ext.getCmp('grid').on('afteredit', function(e){
		 if (e.field == 'FLAG_COMBO') {
			var gridEl = Ext.getCmp('grid').getGridEl();
			var col = grid.getColumnModel().findColumnIndex('FLAG_PAGO_REF');
			var rowEl = Ext.getCmp('grid').getView().getCell( e.row, col);
			rowEl.scrollIntoView(gridEl,false);
			Ext.getCmp('grid').startEditing(e.row, col);
		 }
	});

	function processResult(res){
		if (res == 'ok' || res == 'yes'){
			var jsonData = consultaData.data.items;
			var numCredito = [];
			var comboStatus = [];
			var pagoRef=[];
			var simulaVector = [];
			Ext.each(jsonData, function(item,index,arrItem){
				if (!Ext.isEmpty(item.data.FLAG_COMBO) && item.data.FLAG_COMBO != '0'){
						numCredito.push(item.data.NUM_CREDITO);
						comboStatus.push(item.data.FLAG_COMBO);
						pagoRef.push(item.data.FLAG_PAGO_REF);
				}
			});
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '24forma04ext.data.jsp',
				params: Ext.apply({
					informacion: 'GuardaCambios',
					hddNumCredito: numCredito,
					lstEstatusXAsig:	comboStatus,
					txtNumRegPago: pagoRef }),
				callback: procesaGuardaCambios
			});
		}else{
			Ext.getCmp('btnGuardar').setIconClass('icoGuardar');
		}
	}

	var elementosForma = [
		{
			xtype: 'compositefield',  
			fieldLabel: 'Tipo de Cr�dito',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					id:	'_lstTipoCredito',
					name: 'lstTipoCredito',
					hiddenName : 'lstTipoCredito',
					fieldLabel: 'Tipo de Cr�dito',
					emptyText: 'Seleccionar Credito',
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					forceSelection : true,
					triggerAction : 'all',
					allowBlank: false,
					lazyRender:true,
					typeAhead: true,
					minChars : 1,
					store: tipoCreditoData,							
					width: 300,
					value:'D'
				},
				{
					xtype: 'displayfield',
					value: ' Fecha de Vencimiento:',
					width: 150
				},
				{
					xtype: 'datefield',
					name: 'txtFchVencimiento1',
					id: '_txtFchVencimiento1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: '_txtFchVencimiento2',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 24
				},
				{
					xtype: 'datefield',
					name: 'txtFchVencimiento2',
					id: '_txtFchVencimiento2',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: '_txtFchVencimiento1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}				
			]
		},
		{
			xtype: 'compositefield',  
			fieldLabel: 'EPO',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					id:	'_lstEpo',
					name: 'lstEpo',
					hiddenName : 'lstEpo',
					fieldLabel: 'EPO',
					emptyText: 'Seleccione EPO. . .',
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					width: 300,	
					store: catalogoEpoDistData,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners:{
						'select':function(cbo){
							var comboPyme = Ext.getCmp('_lstPyme');
							comboPyme.setValue('');
							comboPyme.store.removeAll();
							comboPyme.store.reload({	params: {ic_epo: cbo.getValue(), tipoCredito: Ext.getCmp('_lstTipoCredito').getValue() }	});
						}
					}
				},
				{
					xtype: 'displayfield',
					value: 'Tipo de cobro de inter�s:',
					width: 150
				},
				{
					xtype: 'combo',
					name: 'lstTipoCobroInte',
					id:	'_lstTipoCobroInte',
					hiddenName : 'lstTipoCobroInte',
					fieldLabel: 'Tipo de cobro de inter�s',
					emptyText: 'Seleccione Tipo de cobro',
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoTipoCobroData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',  
			fieldLabel: 'Distribuidor',
			combineErrors: false,
			msgTarget: 'side',
			items: [	
				{
					xtype: 'combo',
					id:	'_lstPyme',
					name: 'lstPyme',
					hiddenName : 'lstPyme',
					fieldLabel: 'Distribuidor',
					emptyText: 'Seleccione distribuidor. . .',
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					hidden:false,
					minChars : 1,
					store: catalogoPymeDistData,
					width: 300							
				},
				{
					xtype: 'displayfield',
					value: 'Estatus:',
					width: 150
				},
				{
					xtype: 'combo',
					name: 'lstEstatus',
					id:	'_lstEstatus',
					hiddenName : 'lstEstatus',
					fieldLabel: 'Estatus',
					emptyText: 'Seleccione',
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					forceSelection : true,
					triggerAction : 'all',
					allowBlank:false,
					editable:false,
					typeAhead:true,
					minChars : 1,
					store: catalogoEstatusData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',  
			fieldLabel: 'N�m. Documento Final',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					name: 'txtNumCredito',
					id: '_txtNumCredito',
					fieldLabel: 'N�m. Documento Final',
					anchor:'50%',
					maxLength: 9
				}						
			]
		},		
		{
			xtype: 'compositefield',  
			fieldLabel: 'Fecha de Operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [	
					{
						xtype: 'datefield',
						name: 'txtFchOper1',
						id: '_txtFchOper1',
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoFinFecha: '_txtFchOper2',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'al',
						width: 24
					},
					{
						xtype: 'datefield',
						name: 'txtFchOper2',
						id: '_txtFchOper2',
						allowBlank: true,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoInicioFecha: '_txtFchOper1',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
			]
		},
		{
			xtype: 'compositefield',  
			fieldLabel: 'Monto ',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					name: 'txtMonto1',
					id: '_txtMonto1',
					allowBlank: true,
					maxLength: 9,
					width: 110,
					msgTarget: 'side',
					vtype: 'rangoValor',
					campoFinValor: '_txtMonto2',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'numberfield',
					name: 'txtMonto2',
					id: '_txtMonto2',
					allowBlank: true,
					maxLength: 9,
					width: 110,
					msgTarget: 'side',
					vtype: 'rangoValor',
					campoInicioValor: 'txtMonto1',
					margins: '0 20 0 0'	  //necesario para mostrar el icono de error
				}								
			]
		}	
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 940,
		style: ' margin:0 auto;',
		title:	'<div align="center">Criterios de B�squeda</div>',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					var totalesCmp = Ext.getCmp('gridTotales');
					if (totalesCmp.isVisible()) {
						totalesCmp.hide();
					}
					
					var fechaOper1 = Ext.getCmp('_txtFchOper1');
					var fechaOper2 = Ext.getCmp('_txtFchOper2');
					var fechaVenceMin = Ext.getCmp('_txtFchVencimiento1');
					var fechaVenceMax = Ext.getCmp('_txtFchVencimiento2');
					var izq_montoMin = Ext.getCmp('_txtMonto1');
					var izq_montoMax = Ext.getCmp('_txtMonto2');
					if (!Ext.isEmpty(fechaOper1.getValue()) || !Ext.isEmpty(fechaOper2.getValue()) ) {
						if(Ext.isEmpty(fechaOper1.getValue()))	{
							fechaOper1.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
							fechaOper1.focus();
							return;
						}else if (Ext.isEmpty(fechaOper2.getValue())){
							fechaOper2.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
							fechaOper2.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaVenceMin.getValue()) || !Ext.isEmpty(fechaVenceMax.getValue()) ) {
						if(Ext.isEmpty(fechaVenceMin.getValue()))	{
							fechaVenceMin.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMin.focus();
							return;
						}else if (Ext.isEmpty(fechaVenceMax.getValue())){
							fechaVenceMax.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(izq_montoMin.getValue()) || !Ext.isEmpty(izq_montoMax.getValue()) ) {
						if(Ext.isEmpty(izq_montoMin.getValue()))	{
							izq_montoMin.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							izq_montoMin.focus();
							return;
						}else if (Ext.isEmpty(izq_montoMax.getValue())){
							izq_montoMax.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							izq_montoMax.focus();
							return;
						}
					}
					form.lstEpo = Ext.getCmp('_lstEpo').getValue();
					form.lstPyme = Ext.getCmp('_lstPyme').getValue();
					form.txtNumCredito = Ext.getCmp('_txtNumCredito').getValue();
					form.txtFchOper1 = Ext.util.Format.date(Ext.getCmp('_txtFchOper1').getValue(),'d/m/Y');
					form.txtFchOper2 = Ext.util.Format.date(Ext.getCmp('_txtFchOper2').getValue(),'d/m/Y');
					form.txtMonto1 = Ext.getCmp('_txtMonto1').getValue();
					form.txtMonto2 = Ext.getCmp('_txtMonto2').getValue();
					form.txtFchVencimiento1 = Ext.util.Format.date(Ext.getCmp('_txtFchVencimiento1').getValue(),'d/m/Y');
					form.txtFchVencimiento2 = Ext.util.Format.date(Ext.getCmp('_txtFchVencimiento2').getValue(),'d/m/Y');
					form.lstEstatus = Ext.getCmp('_lstEstatus').getValue();
					form.lstTipoCobroInte = Ext.getCmp('_lstTipoCobroInte').getValue();
					form.lstTipoCredito = Ext.getCmp('_lstTipoCredito').getValue();


					if(!Ext.isEmpty(fechaOper1.getValue()) && !Ext.isEmpty(fechaOper1.getValue())){							
						var fechaIni_ = Ext.util.Format.date(fechaOper1.getValue(),'d/m/Y');
						var fechaFin_ = Ext.util.Format.date(fechaOper2.getValue(),'d/m/Y');
									
						if(datecomp(fechaIni_,fechaFin_)==1) {
							fechaOper1.markInvalid("La fecha de este campo debe de ser anterior a."+fechaIni_);
							fechaOper1.focus();
							//boolValida = false;
							return;
						}
					}
					
					if(!Ext.isEmpty(fechaVenceMin.getValue()) && !Ext.isEmpty(fechaVenceMax.getValue())){							
						var fechaIni_ = Ext.util.Format.date(fechaVenceMin.getValue(),'d/m/Y');
						var fechaFin_ = Ext.util.Format.date(fechaVenceMax.getValue(),'d/m/Y');
									
						if(datecomp(fechaIni_,fechaFin_)==1) {
							fechaVenceMin.markInvalid("La fecha de este campo debe de ser anterior a."+fechaIni_);
							fechaVenceMin.focus();
							//boolValida = false;
							return;
						}
					}
								

					catalogoPorAsignarData.removeAll();
					var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
					if(form.lstEstatus === '11'){
						catalogoPorAsignarData.insert(	0,new reg({	clave: "22",	descripcion: 'Pendiente de pago IF',	loadMsg: null	})	);
					}else{
						catalogoPorAsignarData.insert(	0,new reg({	clave: "11",	descripcion: 'Operada Pagada',	loadMsg: null	})	);
					}

					pnl.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
							//params: Ext.apply(fp.getForm().getValues(),{
						params: Ext.apply({
							operacion: 'Generar',
							start: 0,
							limit: 15
						})
					});

				} //fin handler
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					leeRespuesta();
				}
			}
		]
	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid,	gridTotales,	NE.util.getEspaciador(10)
		]
	});

	catalogoEpoDistData.load();
	catalogoTipoCobroData.load();
	catalogoEstatusData.load();

});