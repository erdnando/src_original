Ext.onReady(function(){
//--------------Handlers--------------------------------
var botonesBlo = function (){
	Ext.getCmp('btnGenerarArchivo').disable();
	Ext.getCmp('btnGenerarPDF').disable();
	Ext.getCmp('btnBajarArchivo').hide();
	Ext.getCmp('btnBajarPDF').hide();
}

var botonesDes = function (){
	Ext.getCmp('btnGenerarArchivo').enable();
	Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');//icoXls
	Ext.getCmp('btnGenerarPDF').enable();
	Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');//icoXls
	Ext.getCmp('btnBajarArchivo').hide();
	Ext.getCmp('btnBajarPDF').hide();
	
}

var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarArchivo');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarArchivo');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}


//-------------STORES-----------------------------------
var catalogoCredito = new Ext.data.ArrayStore({
        fields: [
            'myId',
            'displayText'
        ],        
		  data : [	['D','Modalidad 1 (Riesgo Empresa de Primer Orden )'],	['C','Modalidad 2 (Riesgo Distribuidor) ']	]	
    });
	
var catalogoTasa = new Ext.data.ArrayStore({
        fields: [
            'myId',
            'displayText'
        ],
        data: [['B', 'Base'], ['P', 'Preferencial'],['N','Negociada']]
    });
	
	 
	 
	 
var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catologoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var procesarConsultaDataC = function(store,arrRegistros,opts){
		
			if(arrRegistros!=null){
			var el = gridC.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
				botonesDes();
				gridC.setTitle("<center>Esquema de tasas para operaciones con Modalidad 2 (Riesgo Distribuidor)</center>");
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	var procesarConsultaDataD = function(store,arrRegistros,opts){
		
			if(arrRegistros!=null){
			var el = gridD.getGridEl();
			if(store.getTotalCount()>0){
			botonesDes();
				gridD.setTitle("<center>"+Ext.getCmp('icEpo').lastSelectionText+"</center>");
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					
				}
			}
		}

var catalogoDist = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoDist'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError
		}
  });

var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
var consultaD = new Ext.data.JsonStore({
	root: 'registros',
	url: '24consulta03ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'CREDITO'},
				{name: 'PYME'},
				{name: 'REFERENCIA'},
				{name: 'PLAZO'},
				{name: 'TIPO_TASA'},
				{name: 'VALOR'},
				{name: 'FECHA'},
				{name: 'VALOR_TASA'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
	
		load: procesarConsultaDataD,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaDataD(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var consultaC = new Ext.data.JsonStore({
	root: 'registros',
	url: '24consulta03ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'TIPO_CREDITO'},
				{name: 'DISTRIBUIDOR'},
				{name: 'REFERENCIA'},
				{name: 'TIPO_TASA'},
				{name: 'PLAZO'},
				{name: 'VALOR'},
				{name: 'FECHA'},
				{name: 'VALOR_TASA'}

	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		
		load: procesarConsultaDataC,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaDataC(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

//------------COMPONENTES--------------------------------
 
var barra= 
								 [
									'->','-',
									{
										xtype: 'button',
										text: 'Generar Archivo',
										id: 'btnGenerarArchivo',
										iconCls: 'icoXls',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
												url: '24consulta03ext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'ArchivoCSV',
													tipoTasa: Ext.getCmp('ctTasa').lastSelectionText,
													moneda: Ext.getCmp('cbMoneda').lastSelectionText
												}),
												callback: procesarSuccessFailureGenerarArchivo
											});
										}
									},
									{
										xtype: 'button',
										text: 'Bajar Archivo',
										id: 'btnBajarArchivo',
										hidden: true
									},
									'-',{
										xtype: 'button',
										text: 'Generar PDF',
										iconCls: 'icoPdf',
										id: 'btnGenerarPDF',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
												url: '24consulta03ext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'ArchivoPDF',
													tipoTasa: Ext.getCmp('ctTasa').lastSelectionText,
													moneda: Ext.getCmp('cbMoneda').lastSelectionText
												}),
												callback: procesarSuccessFailureGenerarPDF
											});
										}
									},{
										xtype: 'button',
										text: 'Bajar PDF',
										id: 'btnBajarPDF',
										hidden: true
									}]
								
						


var elementosForma = [
	{
		xtype: 'combo',
		editable:false,
		fieldLabel: 'Tipo de Cr�dito',
		displayField: 'displayText',
		valueField: 'myId',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		name:'HctCredito',
		allowBlank: false,
		store: catalogoCredito,
		id: 'ctCredito',
		mode: 'local',
		hiddenName: 'HctCredito',
		hidden: false,
		emptyText: 'Seleccionar tipo de Cr�dito',
		listeners: {
					select: {
						fn: function(combo) {
							Ext.getCmp('icEpo').setValue();
							var cEpo = combo.getValue();
							var cDist = Ext.getCmp('icEpo');
							cDist.setValue('');
							cDist.store.removeAll();
				
							cDist.store.load({
									params: {
										HctCredito: combo.getValue()
									}
							});
						
						}
					}
			}
	},{
		xtype: 'combo',
		fieldLabel: 'Nombre de la EPO',
		emptyText: 'Seleccionar una EPO',
		displayField: 'descripcion',
		allowBlank: false,
		valueField: 'clave',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		store: catalogoEpo,
		tpl: NE.util.templateMensajeCargaCombo,
		name:'HicEpo',
		id: 'icEpo',
		mode: 'local',
		hiddenName: 'HicEpo',
		forceSelection: true,
		listeners: {
					select: {
						fn: function(combo) {
							Ext.getCmp('icDist').setValue();
							var cEpo = combo.getValue();
							var cDist = Ext.getCmp('icDist');
							cDist.setValue('');
							cDist.store.removeAll();
				
							cDist.store.load({
									params: {
										HicEpo: combo.getValue()
									}
							});
						
						}
					}
			}
	},{
		xtype: 'combo',
		editable:false,
		fieldLabel: 'Tipo de Tasa',
		displayField: 'displayText',
		valueField: 'myId',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		allowBlank: false,
		name:'HctTasa',
		store: catalogoTasa,
		id: 'ctTasa',
		mode: 'local',
		hiddenName: 'HctTasa',
		hidden: false,
		emptyText: 'Seleccionar tipo de Tasa'
	},{
		xtype: 'combo',
		fieldLabel: 'Moneda',
		displayField: 'descripcion',
		valueField: 'clave',
		triggerAction: 'all',
		typeAhead: true,
		allowBlank: false,
		minChars: 1,
		name:'HcbMoneda',
		id: 'cbMoneda',
		mode: 'local',
		forceSelection : true,
		allowBlank: false,
		hiddenName: 'HcbMoneda',
		hidden: false,
		emptyText: 'Seleccionar Moneda',
		store: catalogoMoneda,
		tpl: NE.util.templateMensajeCargaCombo
	},{
		//distribuidor
		xtype: 'combo',
		width: 230,
		emptyText: 'Seleccionar un Distribuidor',
		fieldLabel: 'Nombre del Distribuidor',
		displayField: 'descripcion',
		valueField: 'clave',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		store: catalogoDist,
		tpl: NE.util.templateMensajeCargaCombo,
		name:'HicDist',
		id: 'icDist',
		mode: 'local',
		hiddenName: 'HicDist',
		allowBlank: false,
		forceSelection: true
	}
]
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						tb.show();
						botonesBlo();
						//console.dir(Ext.getCmp('ctCredito').getValue());
						if(Ext.getCmp('ctCredito').getValue()=='D'){
						gridC.hide();
						gridD.show();
						
						consultaD.load({ params: Ext.apply(fp.getForm().getValues(),{
								})
								});
						
						}else{
							
							gridD.hide();
							gridC.show();
							consultaC.load({ params: Ext.apply(fp.getForm().getValues(),{
								})
								});
						}

					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});
	var gridD = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				loadMask: true,	
				height: 400,
				width: 940,
				store: consultaD,
				style: 'margin:0 auto;',
				columns:[
					{
						
						header:'Tipo de Cr�dito',
						tooltip: 'Tipo de Cr�dito',
						id: 'gridTipoCredito',
						sortable: true,
						dataIndex: 'CREDITO',
						width: 130,
						align: 'center',
						renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(Ext.getCmp('ctCredito').lastSelectionText) + '"';
								return Ext.getCmp('ctCredito').lastSelectionText;
								}
						},{
						
						header:'Distribuidor',
						tooltip: 'Distribuidor',
						sortable: true,
						dataIndex: 'PYME',
						width: 140,
						align: 'center',
						renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
								}
						},{
						
						header:'Referencia',
						tooltip: 'Referencia',
						sortable: true,
						dataIndex: 'REFERENCIA',
						width: 110,
						align: 'center',
						renderer: function(value, metadata, record, rowindex, colindex, store) {
								return Ext.getCmp('ctTasa').lastSelectionText;
								
								}
						},{
						header:'Plazo',
						tooltip: 'Plazo',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 110,
						align: 'center'
						},{
						header:'Tipo Tasa',
						tooltip: 'Tipo Tasa',
						sortable: true,
						dataIndex: 'TIPO_TASA',
						width: 130,
						align: 'center',
						renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
								}
						},{
						header:'Valor',
						tooltip: 'Valor',
						sortable: true,
						dataIndex: 'VALOR',
						width: 130,
						align: 'center'
						
						},{
						header:'Fecha',
						tooltip: 'Fecha',
						sortable: true,
						dataIndex: 'FECHA',
						width: 130,
						align: 'center'
						},{
						header:'Valor Tasa',
						tooltip: 'Valor Tasa',
						sortable: true,
						dataIndex: 'VALOR_TASA',
						width: 130,
						align: 'center'
						}
				]
			});
var gridC = new Ext.grid.GridPanel({
				id: 'gridC',
				height: 400,
				width: 940,
				hidden: true,
				header:true,
				loadMask: true,	
				store: consultaC,
				style: 'margin:0 auto;',
				columns:[
					{
						
						header:'Tipo de Cr�dito',
						tooltip: 'Tipo de Cr�dito',
						sortable: true,
						dataIndex: 'TIPO_CREDITO',
						width: 130,
						align: 'center'
								
						},{
						
						header:'Distribuidor',
						tooltip: 'Distribuidor',
						sortable: true,
						dataIndex: 'DISTRIBUIDOR',
						width: 130,
						align: 'center'
						},{
						
						header:'Referencia',
						tooltip: 'Referencia',
						sortable: true,
						dataIndex: 'REFERENCIA',
						width: 110,
						align: 'center',
						renderer: function(value, metadata, record, rowindex, colindex, store) {
								//return Ext.getCmp('ctTasa').lastSelectionText;
								return (value=='P')?'Preferencial':'Negociada';
								}
						},{
						header:'Plazo',
						tooltip: 'Plazo',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 110,
						align: 'center'
						},{
						header:'Tipo Tasa',
						tooltip: 'Tipo Tasa',
						sortable: true,
						dataIndex: 'TIPO_TASA',
						width: 110,
						align: 'center'
						},{
						header:'Valor',
						tooltip: 'Valor',
						sortable: true,
						dataIndex: 'VALOR',
						width: 110,
						align: 'center'
						},{
						header:'Fecha',
						tooltip: 'Fecha',
						sortable: true,
						dataIndex: 'FECHA',
						width: 130,
						align: 'center'
						},{
						header:'Valor Tasa',
						tooltip: 'Valor Tasa',
						sortable: true,
						dataIndex: 'VALOR_TASA',
						width: 140,
						align: 'center'
						}
				]
				});
				
				
var tb = new Ext.Toolbar({
    renderTo: document.body,
    width: 940,
    height: 30,
	 hidden: true,
	 header:true,
	 style: 'margin:0 auto;',
	 items: barra
	 });

//------------Principal--------------------------

var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [ 
		fp,
	   NE.util.getEspaciador(30),
		gridC,
		gridD,
		tb
		
		 
		 ]
  });
	catalogoMoneda.load();


});