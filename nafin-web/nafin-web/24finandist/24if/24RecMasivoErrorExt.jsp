<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	netropology.utilerias.*,
	com.netro.exception.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
//String informacion= (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

String detalleErrores[]	= request.getParameterValues("detalleErrores");

try {

	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;

	contenidoArchivo.append("No. de Linea"+",");
	contenidoArchivo.append("No. de Documento Final"+",");
	contenidoArchivo.append("Causa del Error "+"\n");
	
	if(detalleErrores.length > 0){
		for(int i= 0; i<detalleErrores.length;  i++){
			contenidoArchivo.append(detalleErrores[i]+"\n");
		}
	}
	
	
	if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivo = archivo.nombre;
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>