<%@ page
	contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileUploadException,
		org.apache.commons.logging.Log,
		com.netro.parametrosgrales.*,
		com.netro.distribuidores.*,
		netropology.utilerias.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	private final int MAX_PLANTILLA_FILE_SIZE = 2097152; // 50 MB   
%>
<%
	ParametrosRequest req = null;
	JSONObject jsonObj = new JSONObject();
	String itemArchivo = "",  rutaArchivo ="",   error_tam =""; 
	String PATH_FILE	=	strDirectorioTemp;
	String nombreArchivo= "";
	String estadoSiguiente = "";
	String rutaArchivoTemporal="";
	
	boolean		success		= true;
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
	
	if (ServletFileUpload.isMultipartContent(request)) {
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
	
		try {
		
		// Definir el tamaño máximo que pueden tener los archivos
		upload.setSizeMax(MAX_PLANTILLA_FILE_SIZE);
		// Parsear request
		
	} catch(Throwable t) {
			
		success		= false;
		log.error("CargaPlantilla.subirArchivo(Exception): Cargar en memoria o disco el contenido del archivo");
		t.printStackTrace();
		if( t instanceof SizeLimitExceededException  ) {
			throw new AppException("El Archivo es muy Grande, excede el límite que es de " + ( MAX_PLANTILLA_FILE_SIZE / 1048576 ) + " MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
		try {	 
			req = new ParametrosRequest(upload.parseRequest(request));
			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName();
			InputStream archivo = fItem.getInputStream();
			rutaArchivo = PATH_FILE+itemArchivo;
			int tamanio			= (int)fItem.getSize();
			
			nombreArchivo= Comunes.cadenaAleatoria(16)+ ".txt";
			rutaArchivoTemporal = PATH_FILE + "/" + nombreArchivo;
			fItem.write(new File(rutaArchivoTemporal));
			
		} catch(Throwable e) {
				
				success		= false;
				log.error("CargaPlantilla.subirArchivo(Exception): Guardar Archivo en Disco");
				e.printStackTrace();
				throw new AppException("Ocurrió un error al guardar el archivo");
				
		}
		
	}
	if(!error_tam.equals("")){
		nombreArchivo="";
	}
	ParametrosGrales paramGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
	boolean continua = true;
	String codificacionArchivo ="";
	//boolean resul_cod
	if(paramGrales.validaCodificacionArchivoHabilitado()){
		String[]  	CODIFICACION	= {""};
		CodificacionArchivo  valida_codificacion= new CodificacionArchivo();
		valida_codificacion.setProcessTxt(true);
			//codificaArchivo.setProcessZip(true);
		if(valida_codificacion.esCharsetNoSoportado(rutaArchivoTemporal,CODIFICACION)){
				codificacionArchivo = CODIFICACION[0];
				continua = false;
			
		}
	}
	if(continua==true){
		estadoSiguiente = "VALIDA_CARACTERES_CONTROL";
	}else{
		estadoSiguiente = "MENSAJE_CODIFICACION";
	}

%>



{
	"success": true,
	"nombreArchivo":	'<%=nombreArchivo%>',
	"estadoSiguiente":	'<%=estadoSiguiente%>'
}