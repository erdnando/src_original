Ext.onReady(function(){

	var strPerfil = Ext.get('_strPerfil').getValue();


function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	

function procesarConsultaTotal(store,arrRegistros,opts) {
			
			if(arrRegistros!=null){
			var el = gridTotales.getGridEl();
			if(store.getTotalCount()>0){
				if(arrRegistros==''){
					consultaTotales.load();
				}
				gridTotales.el.unmask();
			}else{		
					gridTotales.el.mask('No se cargaron los totales', 'x-mask');
				}
			}
		}
var accionBotones= function(){
	//Ext.getCmp('btnBajarPDF').hide();
	Ext.getCmp('btnBajarArchivoTodas').hide();
	//Ext.getCmp('btnBajarPDFpag').hide();
	Ext.getCmp('btnBajarVar').hide();
	Ext.getCmp('btnBajarFijo').hide();
	//Ext.getCmp('btnGenerarPDFpag').enable();
	Ext.getCmp('btnGenerarArchivoTodas').enable();
	Ext.getCmp('btnGenerarVar').enable();
	Ext.getCmp('btnGenerarFijo').enable();
	Ext.getCmp('btnGenerarPDF').enable();
	Ext.getCmp('btnTotales').enable();
}
var accionBotonesDisabled= function(){
	
	Ext.getCmp('btnTotales').disable();
	//Ext.getCmp('btnGenerarPDFpag').disable();
	Ext.getCmp('btnGenerarArchivoTodas').disable();
	Ext.getCmp('btnGenerarPDF').disable();
	Ext.getCmp('btnGenerarVar').disable();
	Ext.getCmp('btnGenerarFijo').disable();
}
var accioneBotonesDisabledPorEstatus = function(estatus){
	if(estatus !=''){
		if(estatus == '14'){
			//Ext.getCmp('btnGenerarPDFpag').disable();
			Ext.getCmp('btnGenerarArchivoTodas').disable();
			Ext.getCmp('btnGenerarPDF').disable();
			Ext.getCmp('btnGenerarVar').disable();
			Ext.getCmp('btnGenerarFijo').disable();
			
			Ext.getCmp('btnTCPDF').enable();//
			Ext.getCmp('TCCSV').enable();////
		}else{
			//Ext.getCmp('btnGenerarPDFpag').enable();
			Ext.getCmp('btnGenerarArchivoTodas').enable();
			Ext.getCmp('btnGenerarPDF').enable();
			Ext.getCmp('btnGenerarVar').enable();
			Ext.getCmp('btnGenerarFijo').enable();
			
			Ext.getCmp('btnTCPDF').disable();//
			Ext.getCmp('TCCSV').disable();////
		}
	}
	
}

	var procesarConsultaData = function(store,arrRegistros,opts){
		accionBotones();
			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				var numTC = false;
				for(var i = 0; i<arrRegistros.length;i++){
					if(arrRegistros[i].json.ESTATUS =="Operada TC"){
						numTC= true;
					}
				}
				//INHABILITAR LOS BOTONES 
				var estatus = Ext.getCmp('estatus').getValue();
					accioneBotonesDisabledPorEstatus(estatus);
					Ext.getCmp('btnTCPDF').enable();
					Ext.getCmp('TCCSV').enable();
				/*if(!numTC){
					Ext.getCmp('btnTCPDF').disable();
					Ext.getCmp('TCCSV').disable();
				}else{
					Ext.getCmp('btnTCPDF').enable();
					Ext.getCmp('TCCSV').enable();
				}*/
				el.unmask();
			}else{
					Ext.getCmp('btnTCPDF').disable();
					Ext.getCmp('TCCSV').disable();
					el.mask('No se encontr� ning�n registro', 'x-mask');
					accionBotonesDisabled();
				}
			}
		}
/*
var procesarSuccessFailureGenerarPDFPag =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFpag');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFpag');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
/*
var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('');
	}

var procesarSuccessFailureGenerarArchivoTodas =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarArchivoTodas');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarArchivoTodas');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarArchivoVar =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarVar');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarVar');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivoFijo =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarFijo');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarFijo');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
//////////////////////////////////////////
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var VisorAcusesDeCarga = function(rec){
		var ic_acuse = rec.get('CC_ACUSE');
		if(ic_acuse != ''){
			var win = new NE.AcusesDeCarga.VisorAcusesDeCarga(ic_acuse);
		}
	}
//----------------Stores--------------------------

var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
	var catalogoTipoCobroData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTipoCobroDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});
	
	var catalogoEstatus = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEstatus'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

var catalogoDist = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoDist'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError
		}
  });
  
var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '24consulta01ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
		{name: 'IC_DOCUMENTO'},
		{name: 'NOMBRE_EPO'},
		{name: 'NOMBRE_PYME'},
		{name: 'TIPO_CREDITO'},
		{name: 'RESPONSABLE_INTERES'},
		{name: 'TIPO_COBRANZA'},
		{name: 'FECHA_OPERACION'},
		{name: 'MONEDA'},
		{name: 'MONTO'},
		{name: 'REFERENCIA_TASA'},
		{name: 'TASA_INTERES'},
		{name: 'PLAZO'},
		{name: 'FECHA_VENCIMIENTO'},
		{name: 'MONTO_INTERES'},
		{name: 'TIPO_COBRO_INTERES'},
		{name: 'ESTATUS'},
		{name: 'COMISION_APLICABLE'},//FODEA-013-2014 MOD(SE AGREGARON LAS NUEVAS COLUMNAS AL GRID)
		{name: 'MONTO_COMISION'},//FODEA-013-2014 MOD(SE AGREGARON LAS NUEVAS COLUMNAS AL GRID)
		{name: 'MONTO_DEPOSITAR'},//FODEA-013-2014 MOD(SE AGREGARON LAS NUEVAS COLUMNAS AL GRID)
		{name: 'OPERA_TARJETA'},
		{name: 'ID_ORDEN_ENVIADO'},
		{name: 'ID_OPERACION'},
		{name: 'CODIGO_AUTORIZACION'},
		{name: 'FECHA_REGISTRO'},
		{name: 'BINS'},
		{name: 'NUM_TC'},
		{name: 'IG_TIPO_PAGO'},
		{name: 'CC_ACUSE'},
		{name: 'MUESTRA_VISOR'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad: {fn: function(store, options){
			Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
		}},
		load: procesarConsultaData,
		exception: {
			fn: function(proxy,type,action,optionsRequest,response,args){
				NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
				procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
			}
		}
	}
});

var consultaTotales = new Ext.data.JsonStore({
	root: 'registros',
	url: '24consulta01ext.data.jsp',
	baseParams: {
		informacion: 'Totales'
	},
	fields: [
				{name: 'CD_NOMBRE'},
				{name: 'COUNT(1)'},
				{name: 'SUM(MONTO)'},
				{	name: 'SUM(MONTO_DEPOSITAR)'}//FODEA-013-2014 MOD(SE AGREGARON LAS NUEVAS COLUMNAS AL GRID)					
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaTotal,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaTotal(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catologoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

var catalogoCredito;
if (strPerfil === "IF FACT RECURSO"){
	catalogoCredito = new Ext.data.ArrayStore({
			  fields: ['myId',	'displayText'],
			  data: [['F', 'Factoraje con recurso']]
		 });
}else{
	catalogoCredito = new Ext.data.ArrayStore({
			  fields: ['myId',	'displayText'],			 
			  data : [	['D','Modalidad 1 (Riesgo Empresa de Primer Orden )'],	['C','Modalidad 2 (Riesgo Distribuidor) ']	]	
		 });
}

//---------------Componentes------------------------------

var elementosForma = [
		{
			xtype: 'panel',
			layout:'column',
				items:[{
					xtype: 'container',
					id:	'panelIzq',
					labelWidth:110,
					columnWidth:.5,
					width: '50%',
					defaults: {	msgTarget: 'side'},
					layout: 'form',
					items: [
						{
							anchor: '90%',
							xtype: 'combo',
							editable:false,
							fieldLabel: 'Tipo de Cr�dito',
							displayField: 'displayText',
							valueField: 'myId',
							triggerAction: 'all',
							typeAhead: true,
							minChars: 1,
							name:'HctCredito',
							store: catalogoCredito,
							id: 'ctCredito',
							mode: 'local',
							hiddenName: 'HctCredito',
							hidden: false,
							emptyText: 'Seleccionar tipo de Cr�dito'
		},{
		anchor: '90%',
				xtype: 'combo',
				fieldLabel: 'Nombre de la EPO',
				emptyText: 'Seleccionar una EPO',
				displayField: 'descripcion',
				//allowBlank: false,
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo,
				tpl:'<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}</div>'+
				'</tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
				'</div></tpl></tpl>',
				name:'HicEpo',
				id: 'icEpo',
				mode: 'local',
				hiddenName: 'HicEpo',
				forceSelection: true,
				listeners: {
							select: {
								fn: function(combo) {
									Ext.getCmp('icDist').setValue();
									var cEpo = combo.getValue();
									var cDist = Ext.getCmp('icDist');
									cDist.setValue('');
									cDist.store.removeAll();
						
									cDist.store.load({
											params: {
												HicEpo: combo.getValue()
											}
									});
								
								}
							}
					}
			},{
						anchor: '90%',
						xtype: 'combo',
						emptyText: 'Seleccionar un Distribuidor',
						fieldLabel: 'Nombre del Distribuidor',
						displayField: 'descripcion',
						valueField: 'clave',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: catalogoDist,
						tpl:'<tpl for=".">' +
						'<tpl if="!Ext.isEmpty(loadMsg)">'+
						'<div class="loading-indicator">{loadMsg}</div>'+
						'</tpl>'+
						'<tpl if="Ext.isEmpty(loadMsg)">'+
						'<div class="x-combo-list-item">' +
						'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
						'</div></tpl></tpl>',
						name:'HicDist',
						id: 'icDist',
						mode: 'local',
						hiddenName: 'HicDist',
						//allowBlank: false,
						forceSelection: true
				},
				{
							xtype: 'numberfield',
							name: 'numCred',
							id: 'numCred',
							fieldLabel: 'N�mero de Documento Final',
							anchor: '90%',
							maxLength: 9
						},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de Operaci�n',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaOper1',
									id: 'fechaOper1',
							
									allowBlank: true,
									startDay: 0,
									width: 128,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaOper2'/*,
									margins: '0 20 0 0' */ //necesario para mostrar el icono de error
								},{
									xtype: 'displayfield',
									value: '',
									width: 9
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 22
								},
								{
									xtype: 'datefield',
									name: 'fechaOper2',
									id: 'fechaOper2',
									
									allowBlank: true,
									startDay: 1,
									width: 128,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: 'fechaOper1'/*,
									margins: '0 20 0 0' */ //necesario para mostrar el icono de error
								}
							]
						},{
							xtype: 'compositefield',
							fieldLabel: 'Monto',
							msgTarget: 'side',
							combineErrors: false,
							items: [
								{
									xtype: 'numberfield',
									name: 'monto1',
									id: 'monto1',
									allowBlank: true,
									maxLength: 9,
									width: 128,
									msgTarget: 'side',
									vtype: 'rangoValor',
									campoFinValor: 'monto2'/*,
									margins: '0 20 0 0' */ //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: '',
									width: 9
								},
								{
									xtype: 'displayfield',
									value: 'a',
									width: 22
								},
								{
									xtype: 'numberfield',
									name: 'monto2',
									id: 'monto2',
									allowBlank: true,
									maxLength: 9,
									width: 128,
									msgTarget: 'side',
									vtype: 'rangoValor',
									campoInicioValor: 'monto1'/*,
									margins: '0 20 0 0'	  *///necesario para mostrar el icono de error
								}
								
							]
						},
						{
									anchor: '90%',
									xtype: 'combo',
									fieldLabel: 'Moneda',
									displayField: 'descripcion',
									valueField: 'clave',
									triggerAction: 'all',
									typeAhead: true,
									minChars: 1,
									name:'HcbMoneda',
									id: 'cbMoneda',
									mode: 'local',
									forceSelection : true,
									//allowBlank: false,
									hiddenName: 'HcbMoneda',
									hidden: false,
									emptyText: 'Seleccionar Moneda',
									store: catalogoMoneda,
									tpl: NE.util.templateMensajeCargaCombo
						}
				]
			},
			{
				xtype: 'container',
				id:	'panelDer',
				columnWidth:.5,
				width: '50%',
				layout: 'form',
				defaults: {	msgTarget: 'side'},
				items: [
						{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de vencimiento',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaVenc1',
									id: 'fechaVenc1',
									allowBlank: true,
									startDay: 0,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaVenc2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 23
								},
								{
									xtype: 'datefield',
									name: 'fechaVenc2',
									id: 'fechaVenc2',
									allowBlank: true,
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: 'fechaVenc1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},{
							anchor: '92%',
							xtype: 'combo',
							name: 'HtipoCoInt',
							id:	'tipoCoInt',
							fieldLabel: 'Tipo de cobro de inter�s',
							emptyText: 'Seleccione Tipo de cobro',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'HtipoCoInt',
							forceSelection: true,
							triggerAction: 'all',
							typeAhead: true,
							minChars: 1,
							store: catalogoTipoCobroData,
							tpl: NE.util.templateMensajeCargaCombo
						},{
						anchor: '92%',
							xtype: 'combo',
							name: 'Hestatus',
							id:	'estatus',
							fieldLabel: 'Estatus',
							emptyText: 'Seleccione estatus',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'Hestatus',
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoEstatus,
							tpl : NE.util.templateMensajeCargaCombo
						},{
							anchor:         '92%',
							xtype:          'combo',
							id:             'tipo_pago_id',
							name:           'tipo_pago',
							hiddenName:     'tipo_pago',
							fieldLabel:     'Tipo de pago',
							msgTarget:      'side',
							mode:           'local',
							emptyText:      'Seleccione modalidad de pago...',
							triggerAction:  'all',
							forceSelection: true,
							typeAhead:      true,
							store:          [['1','Financiamiento con intereses'], ['2','Meses sin intereses']] // No existe catalogo para este combo
						}
					]
				}
				
			]
		}
	]

var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 949,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		//defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) {
						// El siguiente IF es solo para validar el formato de las fechas
						if(!Ext.getCmp('forma').getForm().isValid()){
							return;
						}
						if(verificaFechas('fechaOper1','fechaOper2')&&verificaFechas('monto1','monto2')&&verificaFechas('fechaVenc1','fechaVenc2')){
						gridTotales.hide();
						grid.show();
						consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15})
								});
						}
						var botonTcPDF =Ext.getCmp('btnTCPDF');
						var botonTcCSV =Ext.getCmp('TCCSV');
						var combo =Ext.getCmp('estatus');
						if(combo.getValue()==32){
							botonTcPDF.enable();
							botonTcCSV.enable();
						}else{
							//botonTcPDF.disable();
							//botonTcCSV.disable();
						}
						/*//Mi acci�n
						grid.show();
						consulta.load({ params: Ext.apply(fp.getForm().getValues(),{txt_ne: Ext.getCmp('txt_ne').getValue()})
						});				
*/
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({
		store: consultaTotales,
		title: 'Totales',
		id: 'gridTotales',
		hidden:	true,
		style: 'margin: 0 auto;',
		columns: [
			{
				header: 'Moneda',
				dataIndex: 'CD_NOMBRE',
				align: 'center',	width: 232
			},{
				header: 'Total Registros',
				dataIndex: 'COUNT(1)',
				width: 232,	align: 'right'
			},{
				header: 'Total Monto',
				dataIndex: 'SUM(MONTO)',
				width: 232,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')//
			},{
				header: 'Monto a Depositar por Operaci�n',
				dataIndex: 'SUM(MONTO_DEPOSITAR)',
				width: 232,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')//MONTO_DEPOSITAR
			}
		],
		width: 940,
		height: 100,
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	});
	
	var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				columns:[
				//CAMPOS DEL GRID
						{
						
						header:'N�mero Documento Final',
						tooltip: 'N�mero Documento Final',
						sortable: true,
						dataIndex: 'IC_DOCUMENTO',
						width: 130,
						align: 'center'
						},
						{
						width:     180,
						xtype:     'actioncolumn',
						header:    'No. Acuse Autorizaci�n',
						tooltip:   'No. Acuse Autorizaci�n',
						dataIndex: 'CC_ACUSE',
						align:     'center',
						sortable:  true,
						renderer: function(value, metadata, record, rowindex, colindex, store){
							return (record.get('CC_ACUSE'));
						},
						items: [{
							getClass: function(value, metadata, registro, rowIndex, colIndex, store){
								if(registro.get('CC_ACUSE') != '' && registro.get('MUESTRA_VISOR') == 'S'){
									this.items[0].tooltip = 'Ver';
									return 'iconoLupa';
								} else{
									return value;
								}
							},
							handler: function(grid, rowIndex, colIndex){
								var rec = consulta.getAt(rowIndex);
								VisorAcusesDeCarga(rec);
							}
						}]
						},
						{
						
						header: 'EPO',
						tooltip: 'EPO',
						sortable: true,
						dataIndex: 'NOMBRE_EPO',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Distribuidor',
						tooltip: 'Distribuidor',
						sortable: true,
						dataIndex: 'NOMBRE_PYME',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Tipo de Cr�dito',
						tooltip: 'Tipo de Cr�dito',
						sortable: true,
						dataIndex: 'TIPO_CREDITO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Resp. pago de inter�s',
						tooltip: 'Resp. pago de inter�s',
						sortable: true,
						dataIndex: 'RESPONSABLE_INTERES',
						width: 130,
						align: 'center'
						
						},
						{
						align:'center',
						header: 'Tipo de cobranza',
						tooltip: 'Tipo de cobranza',
						sortable: true,
						dataIndex: 'TIPO_COBRANZA',
						width: 130
						},{
						
						header:'Fecha de operaci�n',
						tooltip: 'Fecha de operaci�n',
						sortable: true,
						dataIndex: 'FECHA_OPERACION',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'MONEDA',
						width: 150,
						align: 'center'
						},
						{
						header: 'Monto',
						tooltip: 'Monto',
						sortable: true,
						dataIndex: 'MONTO',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: '% Comisi�n<BR>Aplicable<BR>de Terceros',
							tooltip: '% Comisi�n Aplicable de Terceros',
							dataIndex: 'COMISION_APLICABLE',
							sortable: true,
							width: 100,			
							resizable: true,				
							align: 'center',
							//renderer: Ext.util.Format.numberRenderer('0.00%')
							renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
							
								if (registro.get('ESTATUS') != 'Operada TC'){
									 return value = 'N/A';
								}else{
									return Ext.util.Format.number(value,'0.00%');
								}
							}
						},
						{
							header: 'Monto Comisi�n<br>de Terceros<br>(IVA Incluido)',
							tooltip: 'Monto Comisi�n<br>de Terceros<br>(IVA Incluido)',
							dataIndex: 'MONTO_COMISION',
							sortable: true,
							width: 150,			
							resizable: true,				
							align: 'center',
							renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') != 'Operada TC'){
									return value = 'N/A';
								}else{
									return '<div align=right >'+Ext.util.Format.number(value,'$0,000.00')+'</div>';
								}
							}
						},
						{
							header: 'Monto a Depositar<br>por Operaci�n',
							tooltip: 'Monto a Depositar por Operaci�n',
							dataIndex: 'MONTO_DEPOSITAR',
							sortable: true,
							width: 150,			
							resizable: true,				
							align: 'center',
							renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') != 'Operada TC'){
									return value = 'N/A';
								}else{
									return '<div align=right >'+Ext.util.Format.number(value,'$0,000.00')+'</div>';
								}
							}
						},
						{
						header: 'Referencia tasa',
						tooltip: 'Referencia tasa',
						sortable: true,
						dataIndex: 'REFERENCIA_TASA',
						width: 150,
						align: 'center',
						/*renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							},*/
							renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') == 'Operada TC'){
									value = 'N/A';
									return value;
								}else{
									metaData.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
									return value;
								}
								
							}
						},
						{
						header: 'Tasa de inter�s',
						tooltip: 'Tasa de inter�s',
						sortable: true,
						dataIndex: 'TASA_INTERES',
						width: 130,
						align: 'center',
						//renderer: Ext.util.Format.numberRenderer('0.00%'),
							renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') == 'Operada TC'){
									return	value = 'N/A';
								}else{
									return value= Ext.util.Format.number(value,'0.00%');	
								}
								
							}
						
						},
						{
						align:'center',
						header: 'Plazo',
						tooltip: 'Plazo',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 130,
							renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
								//INICIO: FODEA 09-2015
								if(registro.get('IG_TIPO_PAGO') == '2'){
									value = value + ' (M)';
								}
								//FIN: FODEA 09-2015
								if (registro.get('ESTATUS') == 'Operada TC'){
									value = 'N/A';
								}
								return value;
							}
						},
						{
						header: 'Fecha de vencimiento',
						tooltip: 'Fecha de vencimiento',
						sortable: true,
						dataIndex: 'FECHA_VENCIMIENTO',
						width: 130,
						align: 'center',
							renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') == 'Operada TC'){
									value = 'N/A';
								}
								return value;
							}
						},
						{
						header: 'Monto inter�s',
						tooltip: 'Monto inter�s',
						sortable: true,
						dataIndex: 'MONTO_INTERES',
						width: 130,
						align: 'right',
						//renderer: Ext.util.Format.numberRenderer('$0,0.00'),
							renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') == 'Operada TC'){
									return value = 'N/A';
								}else {
									return value = Ext.util.Format.number(value,'$0,0.00');
								}
							}
						},
						{
						header: 'Tipo de cobro inter�s',
						tooltip: 'Tipo de cobro inter�s',
						sortable: true,
						dataIndex: 'TIPO_COBRO_INTERES',
						width: 150,
						align: 'center'
						},
						{
						header: 'Estatus',
						tooltip: 'Estatus',
						sortable: true,
						dataIndex: 'ESTATUS',
						width: 150,
						align: 'center'
						},
						{
						header: "Nombre del Producto",
						tooltip: "Nombre del Producto",
						sortable: true,
						dataIndex: 'BINS',
						width: 150,
						align: 'center',
							renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') != 'Operada TC'){
									value = 'N/A';
								}
								return value;
							}
						},
						{
							header:'ID Orden enviado',
							tooltip:'ID Orden enviado',
							dataIndex:'ID_ORDEN_ENVIADO',
							align: 'center',
							sortable : true,	width : 120,	align: 'right', hidden: false,
							renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') != 'Operada TC'){
									value = 'N/A';
								}
								return value;
							}
						},
						{
							header:'Respuesta de Operaci�n',
							tooltip:'Respuesta de Operaci�n',
							dataIndex:'ID_OPERACION',
							align: 'center',
							sortable : true,	width : 120,	align: 'right', hidden: false,
							renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') != 'Operada TC'){
									value = 'N/A';
								}
								return value;
							}
						},
						{
							header:'C�digo de<br>Autorizaci�n',
							tooltip:'C�digo de<br>Autorizaci�n',
							dataIndex:'CODIGO_AUTORIZACION',
							align: 'center',
							sortable : true,	width : 120,	align: 'center', hidden: false,
							renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') != 'Operada TC'){
									value = 'N/A';
								}
								return value;
							}
						},//
						{
							header : 'N�mero Tarjeta de Cr�dito', tooltip: 'N�mero Tarjeta de Cr�dito',
							dataIndex : 'NUM_TC',
							hidden	:true,
							sortable : true,	width : 200,	align: 'center', hidden: false,
							renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								if (registro.get('ESTATUS') != 'Operada TC'){
									value = 'N/A';
								}else {
									if(value != ''){
										value = 'XXXX-XXXX-XXXX-'+value;
									}
									
								}
								return value;
							}	
						}
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					autoScroll:true,
					height: 45,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					
					items: ['->','-',
								{
										xtype: 'button',
										text: 'Totales',
										id: 'btnTotales',
										hidden: false,
										handler: function(boton, evento) {
												consultaTotales.load();
												gridTotales.show();
												gridTotales.el.mask('Enviando...', 'x-mask-loading');
										}
								},/*
								{
									xtype: 'button',
									//height: 40,
									text: 'Generar PDF X Pagina',
									id: 'btnGenerarPDFpag',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
										Ext.Ajax.request({
											url: '24consulta01ext.data.jsp',
											params: Ext.apply({//fp.getForm().getValues(),{
												informacion: 'ArchivoPaginaPDF',
												start: cmpBarraPaginacion.cursor,
													limit: cmpBarraPaginacion.pageSize}),
											callback: procesarSuccessFailureGenerarPDFPag
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar<br/> PDF',
									id: 'btnBajarPDFpag',
									hidden: true
								},*/'-',
								{
									xtype: 'button',
									text: 'Generar Todo PDF',
									tooltip: 'Imprime los registros en formato PDF.',
									id: 'btnGenerarPDF',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '24consulta01ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoPDF'}),
											callback: procesarSuccessFailureGenerarPDF
										});
									}
								},/*
								{
									xtype: 'button',
									text: 'Bajar<br/> PDF',
									id: 'btnBajarPDF',
									hidden: true
								},*/'-',
								{
									xtype: 'button',
									//height: 40,
									text: 'Generar Archivo',
									id: 'btnGenerarArchivoTodas',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '24consulta01ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV'}),
											callback: procesarSuccessFailureGenerarArchivoTodas
										});
									}
								},'-',
								{
									xtype: 'button',
									text: 'Bajar<br/> Archivo',
									id: 'btnBajarArchivoTodas',
									hidden: true
								},'-',{
									xtype: 'button',
									//height: 40,
									text: 'Exportar Interfase Variable',
									id: 'btnGenerarVar',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '24consulta01ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoVar'}),
											callback: procesarSuccessFailureGenerarArchivoVar
										});
									}
								},'-',
								{
									xtype: 'button',
									text: 'Bajar<br/> Archivo',
									id: 'btnBajarVar',
									hidden: true
								},'-',
								{
									xtype: 'button',
									//height: 40,
									text: 'Exportar Interfase Fijo',
									id: 'btnGenerarFijo',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '24consulta01ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoFijo'}),
											callback: procesarSuccessFailureGenerarArchivoFijo
										});
									}
								},'-',
								{
									xtype: 'button',
									text: 'Bajar<br/> Archivo',
									id: 'btnBajarFijo',
									hidden: true
								},'-',
								{
									xtype : 'button',
									id : 'btnTCPDF',
									text : 'Reporte operaciones TC � PDF',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '24consulta01ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoTCPDF'
											}),
											success : function(response) {
												boton.setIconClass('');
												boton.setDisabled(false);
				
											},
											callback: procesarDescargaArchivos
										});
									}
									
								},'-',
								{
									xtype : 'button',
									id : 'TCCSV',
									text : 'Reporte operaciones TC � CSV',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '24consulta01ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoTCCSV'}),
											success : function(response) {
												boton.setIconClass('');
												boton.setDisabled(false);
				
											},
											callback: procesarDescargaArchivos
											
										});
									}
								},
								
								'-']
				}
		});

//----------------Principal------------------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [ 
	  fp,
	    NE.util.getEspaciador(30),
		grid,
		gridTotales
		 
		 ]
  });
		catalogoEpo.load();
		catalogoMoneda.load();
		catalogoTipoCobroData.load();
		catalogoEstatus.load();
});