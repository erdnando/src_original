/*
*Reporte de IF-Cambio de estatus, By Hugo VC
*/
Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

	var cveEstatus	=	"";
	var valGrid = {gridUno:false, gridDos:false, gridTres:false,gridCuatro:false}

	function muestraToolBar(uno, dos, tres,cuatro){
		if(uno || dos || tres || cuatro ){

			var btnGenerarArchivo = Ext.getCmp("btnGenerarArchivo");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');

			btnGenerarArchivo.setHandler(
				function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '24reporte02exta.jsp',
						params:	{ic_cambio_estatus:	cveEstatus},
						callback: procesarSuccessFailureGenerarArchivo
					});
				}
			);

			btnGenerarPDF.setHandler(
				function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '24reporte02extpdf.jsp',
						params: {ic_cambio_estatus:	cveEstatus},
						callback: procesarSuccessFailureGenerarPDF
					});
				}
			);
			toolbar.show();
		}else{
			toolbar.hide();
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdf');
			toolbar.setWidth(toolbar.getWidth()+70);
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			toolbar.setWidth(toolbar.getWidth()+80);
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	function descargaArchivo(opts, success, response) {//btnGenerarCSV_22
		Ext.getCmp('btnGenerarCSV_22').enable();
		var btnImprimirCSV = Ext.getCmp('btnGenerarCSV_22');
		btnImprimirCSV.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function mostrarArchivoPDF(opts, success, response) {
		Ext.getCmp('btnImprimir_22').enable();
		
		var btnImprimirPDF = Ext.getCmp('btnImprimir_22');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	/****** Handler�s Grid�s *******/
	var procesarConsultaData14 = function(store, arrRegistros, opts) {
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnTotales = Ext.getCmp('btnTotales14');
			if (!grid14.isVisible()) {
				grid14.show();
			}
			var el = grid14.getGridEl();
			var regM = totalesData14.getAt(0);
			regM.set('NOMMONEDA','');
			regM.set('TOTAL_REGISTROS','');
			regM.set('TOTAL_MONTO','');
			regM.set('TOTAL_MONTO_VALUADO','');
			regM.set('TOTAL_MONTO_CREDITO','');
			var regD = totalesData14.getAt(1);
			regD.set('NOMMONEDA','');
			regD.set('TOTAL_REGISTROS','');
			regD.set('TOTAL_MONTO','');
			regD.set('TOTAL_MONTO_VALUADO','');
			regD.set('TOTAL_MONTO_CREDITO','');

			if(store.getTotalCount() > 0) {
				var sumMontoMN=0, montoValuadoMN=0,	montoCreditoMN=0;
				var sumMontoDL=0,	montoValuadoDL=0,	montoCreditoDL=0;
				var countMN = 0;
				var countDL = 0;
				var montoDescontar=0, montoValuado=0;

				consultaData14.each(function(registro){
					montoDescontar = (parseFloat(registro.get('FN_MONTO'))*(parseFloat(registro.get('FN_PORC_DESCUENTO'))/100)	);
					montoValuado	= (parseFloat(registro.get('FN_MONTO'))-montoDescontar)*parseFloat(registro.get('TIPO_CAMBIO'));

					if (registro.get('IC_MONEDA') === "1"){
						countMN++;
						sumMontoMN += parseFloat(registro.get('FN_MONTO'));
						montoValuadoMN += montoValuado;
						montoCreditoMN +=	parseFloat(registro.get('MONTO_CREDITO'));
					}else	{
						countDL++;
						sumMontoDL += parseFloat(registro.get('FN_MONTO'));
						if(	!(registro.get('IC_MONEDA') == (registro.get('MONEDA_LINEA'))) ){
							montoValuadoDL += montoValuado;
						}
						montoCreditoDL +=	parseFloat(registro.get('MONTO_CREDITO'));
					}
				});
				if (countMN > 0){
					var regMN = totalesData14.getAt(0);
					regMN.set('NOMMONEDA','Moneda Nacional');
					regMN.set('TOTAL_REGISTROS',countMN);
					regMN.set('TOTAL_MONTO',sumMontoMN);
					regMN.set('TOTAL_MONTO_VALUADO',"");	//montoValuadoMN
					regMN.set('TOTAL_MONTO_CREDITO',montoCreditoMN);

					if(countDL > 0){
						var regDL = totalesData14.getAt(1);
						regDL.set('NOMMONEDA','Total D�lares');
						regDL.set('TOTAL_REGISTROS',countDL);
						regDL.set('TOTAL_MONTO',sumMontoDL);
						regDL.set('TOTAL_MONTO_VALUADO',montoValuadoDL);
						regDL.set('TOTAL_MONTO_CREDITO',montoCreditoDL);
					}
				}else if(countDL > 0){
					var regDL = totalesData14.getAt(0);
					regDL.set('NOMMONEDA','Total D�lares');
					regDL.set('TOTAL_REGISTROS',countDL);
					regDL.set('TOTAL_MONTO',sumMontoDL);
					regDL.set('TOTAL_MONTO_VALUADO',montoValuadoDL);
					regDL.set('TOTAL_MONTO_CREDITO',montoCreditoDL);
				}

				btnTotales.enable();
				el.unmask();
				valGrid.gridUno = true;
			} else {
				btnTotales.disable();
				valGrid.gridUno = false;
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
		muestraToolBar(valGrid.gridUno, valGrid.gridDos, valGrid.gridTres,valGrid.gridCuatro);
		if (Ext.isEmpty(cveEstatus)){
			consultaData2.load({params:	{ic_cambio_estatus:"2"}});
		}
		
	}

	var procesarConsultaData2 = function(store, arrRegistros, opts) {
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnTotales = Ext.getCmp('btnTotales2');
			if (!grid2.isVisible()) {
				grid2.show();
			}
			var el = grid2.getGridEl();

			var regM = totalesData2.getAt(0);
			regM.set('NOMMONEDA','');
			regM.set('TOTAL_REGISTROS','');
			regM.set('TOTAL_MONTO','');
			regM.set('TOTAL_MONTO_VALUADO','');
			regM.set('TOTAL_MONTO_CREDITO','');
			var regD = totalesData2.getAt(1);
			regD.set('NOMMONEDA','');
			regD.set('TOTAL_REGISTROS','');
			regD.set('TOTAL_MONTO','');
			regD.set('TOTAL_MONTO_VALUADO','');
			regD.set('TOTAL_MONTO_CREDITO','');

			if(store.getTotalCount() > 0) {

				var sumMontoMN=0, montoValuadoMN=0,	montoCreditoMN=0;
				var sumMontoDL=0,	montoValuadoDL=0,	montoCreditoDL=0;
				var countMN = 0;
				var countDL = 0;
				var montoDescontar=0, montoValuado=0;

				consultaData2.each(function(registro){
					montoDescontar = (parseFloat(registro.get('FN_MONTO'))*(parseFloat(registro.get('FN_PORC_DESCUENTO'))/100)	);
					montoValuado	= (parseFloat(registro.get('FN_MONTO'))-montoDescontar)*parseFloat(registro.get('TIPO_CAMBIO'));

					if (registro.get('IC_MONEDA') === "1"){
						countMN++;
						sumMontoMN += parseFloat(registro.get('FN_MONTO'));
						montoValuadoMN += montoValuado;
						montoCreditoMN +=	parseFloat(registro.get('MONTO_CREDITO'));
					}else	{
						countDL++;
						sumMontoDL += parseFloat(registro.get('FN_MONTO'));
						if(	!(registro.get('IC_MONEDA') == (registro.get('MONEDA_LINEA'))) ){
							montoValuadoDL += montoValuado;
						}
						montoCreditoDL +=	parseFloat(registro.get('MONTO_CREDITO'));
					}
				});
				if (countMN > 0){
					var regMN = totalesData2.getAt(0);
					regMN.set('NOMMONEDA','Moneda Nacional');
					regMN.set('TOTAL_REGISTROS',countMN);
					regMN.set('TOTAL_MONTO',sumMontoMN);
					regMN.set('TOTAL_MONTO_VALUADO',"");	//montoValuadoMN
					regMN.set('TOTAL_MONTO_CREDITO',montoCreditoMN);

					if(countDL > 0){
						var regDL = totalesData2.getAt(1);
						regDL.set('NOMMONEDA','Total D�lares');
						regDL.set('TOTAL_REGISTROS',countDL);
						regDL.set('TOTAL_MONTO',sumMontoDL);
						regDL.set('TOTAL_MONTO_VALUADO',montoValuadoDL);
						regDL.set('TOTAL_MONTO_CREDITO',montoCreditoDL);
					}
				}else if(countDL > 0){
					var regDL = totalesData2.getAt(0);
					regDL.set('NOMMONEDA','Total D�lares');
					regDL.set('TOTAL_REGISTROS',countDL);
					regDL.set('TOTAL_MONTO',sumMontoDL);
					regDL.set('TOTAL_MONTO_VALUADO',montoValuadoDL);
					regDL.set('TOTAL_MONTO_CREDITO',montoCreditoDL);
				}

				btnTotales.enable();
				el.unmask();
				valGrid.gridDos = true;
			} else {
				btnTotales.disable();
				valGrid.gridDos = false;
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
		muestraToolBar(valGrid.gridUno, valGrid.gridDos, valGrid.gridTres,valGrid.gridCuatro);
		if (Ext.isEmpty(cveEstatus)){
			consultaData21.load({params:	{ic_cambio_estatus:"21"}});
		}
	}
	var procesarConsultaData22 = function(store, arrRegistros, opts) {
		fp.el.unmask();
		
		var jsonData = store.reader.jsonData;	
		var grid22 = Ext.getCmp('grid22');	//grid22
		var btnGenerarArchivo = Ext.getCmp('btnGenerarCSV_22');
		var btnImprimir = Ext.getCmp('btnImprimir_22');

		if (arrRegistros != null) {
			var btnTotales = Ext.getCmp('btnTotales22');
			if (!grid22.isVisible()) {
				Ext.getCmp('grid22').setTitle(jsonData.tituloGrid);
				grid22.show();
			}
			var el = grid22.getGridEl();
			var regM = totalesData22.getAt(0);
			regM.set('NOMMONEDA','');
			regM.set('TOTAL_REGISTROS','');
			regM.set('TOTAL_MONTO','');
			//regM.set('TOTAL_MONTO_VALUADO','');
			regM.set('TOTAL_MONTO_CREDITO','');
			var regD = totalesData22.getAt(1);
			regD.set('NOMMONEDA','');
			regD.set('TOTAL_REGISTROS','');
			regD.set('TOTAL_MONTO','');
			//regD.set('TOTAL_MONTO_VALUADO','');
			regD.set('TOTAL_MONTO_CREDITO','');

			if(store.getTotalCount() > 0) {

				var sumMontoMN=0, montoValuadoMN=0,	montoCreditoMN=0;
				var sumMontoDL=0,	montoValuadoDL=0,	montoCreditoDL=0;
				var countMN = 0;
				var countDL = 0;
				var montoDescontar=0, montoValuado=0;

				consultaData22.each(function(registro){
					montoDescontar = (parseFloat(registro.get('FN_MONTO'))*(parseFloat(registro.get('FN_PORC_DESCUENTO'))/100)	);
					montoValuado	= (parseFloat(registro.get('FN_MONTO'))-montoDescontar)*parseFloat(registro.get('TIPO_CAMBIO'));

					if (registro.get('IC_MONEDA') === "1"){
						countMN++;
						sumMontoMN += parseFloat(registro.get('FN_MONTO'));
						montoValuadoMN += montoValuado;
						montoCreditoMN +=	parseFloat(registro.get('MONTO_CREDITO'));
					}else	{
						countDL++;
						sumMontoDL += parseFloat(registro.get('FN_MONTO'));
						if(	!(registro.get('IC_MONEDA') == (registro.get('MONEDA_LINEA'))) ){
							montoValuadoDL += montoValuado;
						}
						montoCreditoDL +=	parseFloat(registro.get('MONTO_CREDITO'));
					}
				});
				if (countMN > 0){
					var regMN = totalesData22.getAt(0);
					regMN.set('NOMMONEDA','Moneda Nacional');
					regMN.set('TOTAL_REGISTROS',countMN);
					regMN.set('TOTAL_MONTO',sumMontoMN);
					//regMN.set('TOTAL_MONTO_VALUADO',"");	//montoValuadoMN
					regMN.set('TOTAL_MONTO_CREDITO',montoCreditoMN);

					if(countDL > 0){
						var regDL = totalesData22.getAt(1);
						regDL.set('NOMMONEDA','Total D�lares');
						regDL.set('TOTAL_REGISTROS',countDL);
						regDL.set('TOTAL_MONTO',sumMontoDL);
						//regDL.set('TOTAL_MONTO_VALUADO',montoValuadoDL);
						regDL.set('TOTAL_MONTO_CREDITO',montoCreditoDL);
					}
				}else if(countDL > 0){
					var regDL = totalesData22.getAt(0);
					regDL.set('NOMMONEDA','Total D�lares');
					regDL.set('TOTAL_REGISTROS',countDL);
					regDL.set('TOTAL_MONTO',sumMontoDL);
					//regDL.set('TOTAL_MONTO_VALUADO',montoValuadoDL);
					regDL.set('TOTAL_MONTO_CREDITO',montoCreditoDL);
				}

				btnTotales.enable();
				el.unmask();
			} else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnImprimir.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
		//muestraToolBar(valGrid.gridUno, valGrid.gridDos, valGrid.gridTres,valGrid.gridCuatro);
		if (Ext.isEmpty(cveEstatus)){
			consultaData22.load({params:	{ic_cambio_estatus:Ext.getCmp('_ic_cambio_estatus').getValue()}});
		}
	}

	var procesarConsultaData21 = function(store, arrRegistros, opts) {
		fp.el.unmask();
		pnl.el.unmask();
		if (arrRegistros != null) {
			var btnTotales = Ext.getCmp('btnTotales21');
			if (!grid21.isVisible()) {
				grid21.show();
			}
			var el = grid21.getGridEl();

			var regM = totalesData21.getAt(0);
			regM.set('NOMMONEDA','');
			regM.set('TOTAL_REGISTROS','');
			regM.set('TOTAL_MONTO','');
			regM.set('TOTAL_MONTO_VALUADO','');
			regM.set('TOTAL_MONTO_CREDITO','');
			var regD = totalesData21.getAt(1);
			regD.set('NOMMONEDA','');
			regD.set('TOTAL_REGISTROS','');
			regD.set('TOTAL_MONTO','');
			regD.set('TOTAL_MONTO_VALUADO','');
			regD.set('TOTAL_MONTO_CREDITO','');

			if(store.getTotalCount() > 0) {
				var sumMontoMN=0, montoValuadoMN=0,	montoCreditoMN=0;
				var sumMontoDL=0,	montoValuadoDL=0,	montoCreditoDL=0;
				var countMN = 0;
				var countDL = 0;
				var montoDescontar=0, montoValuado=0;

				consultaData21.each(function(registro){
					montoDescontar = (parseFloat(registro.get('FN_MONTO'))*(parseFloat(registro.get('FN_PORC_DESCUENTO'))/100)	);
					montoValuado	= (parseFloat(registro.get('FN_MONTO'))-montoDescontar)*parseFloat(registro.get('TIPO_CAMBIO'));

					if (registro.get('IC_MONEDA') === "1"){
						countMN++;
						sumMontoMN += parseFloat(registro.get('FN_MONTO'));
						montoValuadoMN += montoValuado;
						montoCreditoMN +=	parseFloat(registro.get('MONTO_CREDITO'));
					}else	{
						countDL++;
						sumMontoDL += parseFloat(registro.get('FN_MONTO'));
						if(	!(registro.get('IC_MONEDA') == (registro.get('MONEDA_LINEA'))) ){
							montoValuadoDL += montoValuado;
						}
						montoCreditoDL +=	parseFloat(registro.get('MONTO_CREDITO'));
					}
				});
				if (countMN > 0){
					var regMN = totalesData21.getAt(0);
					regMN.set('NOMMONEDA','Moneda Nacional');
					regMN.set('TOTAL_REGISTROS',countMN);
					regMN.set('TOTAL_MONTO',sumMontoMN);
					regMN.set('TOTAL_MONTO_VALUADO',"");	//montoValuadoMN
					regMN.set('TOTAL_MONTO_CREDITO',montoCreditoMN);

					if(countDL > 0){
						var regDL = totalesData21.getAt(1);
						regDL.set('NOMMONEDA','Total D�lares');
						regDL.set('TOTAL_REGISTROS',countDL);
						regDL.set('TOTAL_MONTO',sumMontoDL);
						regDL.set('TOTAL_MONTO_VALUADO',montoValuadoDL);
						regDL.set('TOTAL_MONTO_CREDITO',montoCreditoDL);
					}
				}else if(countDL > 0){
					var regDL = totalesData21.getAt(0);
					regDL.set('NOMMONEDA','Total D�lares');
					regDL.set('TOTAL_REGISTROS',countDL);
					regDL.set('TOTAL_MONTO',sumMontoDL);
					regDL.set('TOTAL_MONTO_VALUADO',montoValuadoDL);
					regDL.set('TOTAL_MONTO_CREDITO',montoCreditoDL);
				}
				btnTotales.enable();
				valGrid.gridTres = true;
				el.unmask();
			} else {
				btnTotales.disable();
				valGrid.gridTres = false;
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
		muestraToolBar(valGrid.gridUno, valGrid.gridDos, valGrid.gridTres,valGrid.gridCuatro);
	}

	/**** End Handler�s Grid�s ****/

	var procesaCatEstatusData = function(store, arrRegistros, opts) {
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione estatus',	loadMsg: null	})	);
			Ext.getCmp('_ic_cambio_estatus').setValue("");
	}

	var catalogoEstatus = new Ext.data.JsonStore({
		root:'registros',
		fields:['clave', 'descripcion', 'loadMsg'],
		url:'24reporte02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEstatusDist'	},
		totalProperty:'total',	autoLoad:false,	listeners:{	load:procesaCatEstatusData,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo}
	});

	/****** Store�s Grid�s *******/
	var consultaData14 = new Ext.data.JsonStore({
		root:'registros',	url:'24reporte02ext.data.jsp',	baseParams:{informacion:'Consulta'},	totalProperty:'total',	messageProperty:'msg',	autoLoad:false,
		fields: [
			{name: 'NOMBRE_DIST'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_CARGA',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO',	type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO',type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_IF'},
			{name: 'TIPO_COBRANZA'},
			{name: 'TIPO_CREDITO'},
			{name: 'MONTO_CREDITO',type: 'float'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'TIPO_COBRO_INT'},
			{name: 'REFERENCIA_INT'},
			{name: 'VALOR_TASA_INT'},
			{name: 'MONTO_TASA_INT',type: 'float'},
			{name: 'FECHA_PUBLICACION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC_CREDITO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_CAMBIO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CAUSA'},
			{name: 'MONEDA_LINEA'}
		],
		listeners: {
			load: procesarConsultaData14,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData14(null, null, null);
				}
			}
		}
	});

	var totalesData14 = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO'},
			{name: 'TOTAL_MONTO_CREDITO'},
			{name: 'TOTAL_MONTO_VALUADO'}
		],
		data:[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''},
				{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''}],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData2 = new Ext.data.JsonStore({
		root:'registros',
		url:'24reporte02ext.data.jsp',
		baseParams:{informacion:'Consulta'},
		totalProperty:'total',
		messageProperty:'msg',	
		autoLoad:false,
		fields: [
			{name: 'NOMBRE_DIST'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_CARGA',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO',	type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO',type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_IF'},
			{name: 'TIPO_COBRANZA'},
			{name: 'TIPO_CREDITO'},
			{name: 'MONTO_CREDITO',type: 'float'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'TIPO_COBRO_INT'},
			{name: 'REFERENCIA_INT'},
			{name: 'VALOR_TASA_INT'},
			{name: 'MONTO_TASA_INT',type: 'float'},
			{name: 'FECHA_PUBLICACION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC_CREDITO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_CAMBIO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CAUSA'},
			{name: 'MONEDA_LINEA'}
		],
		listeners: {
			load: procesarConsultaData2,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData2(null, null, null);
				}
			}
		}
	});
	var consultaData22 = new Ext.data.JsonStore({
		root:'registros',
		url:'24reporte02ext.data.jsp',
		baseParams:{informacion:'Consulta'},
		totalProperty:'total',
		messageProperty:'msg',	
		autoLoad:false,
		fields: [
			{name: 'NOMBRE_DIST'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_CARGA',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO',	type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO',type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_IF'},
			{name: 'TIPO_COBRANZA'},
			{name: 'TIPO_CREDITO'},
			{name: 'MONTO_CREDITO',type: 'float'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'TIPO_COBRO_INT'},
			{name: 'REFERENCIA_INT'},
			{name: 'VALOR_TASA_INT'},
			{name: 'MONTO_TASA_INT',type: 'float'},
			{name: 'FECHA_PUBLICACION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC_CREDITO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_CAMBIO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CAUSA'},
			{name: 'MONEDA_LINEA'}
		],
		listeners: {
			load: procesarConsultaData22,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData22(null, null, null);
				}
			}
		}
	});

	var totalesData2 = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO'},
			{name: 'TOTAL_MONTO_CREDITO'},
			{name: 'TOTAL_MONTO_VALUADO'}
		],
		data:[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''},
				{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''}],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});
	var totalesData22 = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO'},
			{name: 'TOTAL_MONTO_CREDITO'},
			{name: 'TOTAL_MONTO_VALUADO'}
		],
		data:[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''},
				{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''}],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData21 = new Ext.data.JsonStore({
		root:'registros',	url:'24reporte02ext.data.jsp',	baseParams:{informacion:'Consulta'},	totalProperty:'total',	messageProperty:'msg',	autoLoad:false,
		fields: [
			{name: 'NOMBRE_DIST'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_CARGA',			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO',			type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_IF'},
			{name: 'TIPO_COBRANZA'},
			{name: 'TIPO_CREDITO'},
			{name: 'FECHA_PUBLICACION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_CAMBIO',		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_EMISION_ANT',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC_ANT',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_MONTO_ANTERIOR',type: 'float'},
			{name: 'MODO_PLAZO_ANT'},
			{name: 'MONEDA_LINEA'}
		],
		listeners: {
			load: procesarConsultaData21,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData21(null, null, null);
				}
			}
		}
	});

	var totalesData21 = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO'},
			{name: 'TOTAL_MONTO_CREDITO'},
			{name: 'TOTAL_MONTO_VALUADO'}
		],
		data:[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''},
				{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''}],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});
	
	/**** End Store�s Grid�s ****/

	/**** Grid�s ****/

	var grid14 = new Ext.grid.GridPanel({
		title:'Estatus:	Seleccionado Pyme a Rechazado IF',//NOMBRE_DIST
		store: consultaData14,	stripeRows: true,	loadMask:true,	height:400,	width:940,	frame:false, hidden:true, header:true,
		columns: [
			{
				header:'EPO', tooltip:'EPO',	dataIndex:'NOMBRE_EPO',	sortable:true,	resizable:true,	width:200,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Distribuidor', tooltip: 'Distribuidor',	dataIndex:'NOMBRE_DIST',	sortable:true,	resizable:true,	width:200,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'N�m. acuse de carga', tooltip: 'N�mero acuse de carga',	dataIndex:'CC_ACUSE',	sortable:true,	width:120,	resizable:true
			},{
				header:'N�m. docto. inicial', tooltip:'N�mero de documento inicial',	dataIndex:'IG_NUMERO_DOCTO',	sortable:true, width:120
			},{
				header:'Fecha de Emisi�n', tooltip:'Fecha de Emisi�n',	dataIndex:'DF_FECHA_EMISION',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de Vencimiento', tooltip:'Fecha de Vencimiento',	dataIndex:'DF_FECHA_VENC',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de publicaci�n', tooltip:'Fecha de publicaci�n',	dataIndex:'FECHA_PUBLICACION',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Plazo docto.', tooltip:'Plazo docto.',	dataIndex:'IG_PLAZO_DOCTO',	sortable:true,	width:120, align:"center"
			},{
				header:'Moneda', tooltip: 'Moneda',	dataIndex:'MONEDA',	sortable:true,	width:130,align:"center"
			},{
				header:'Monto', tooltip:'Monto',	dataIndex:'FN_MONTO',	sortable:true,	width:120,	align:'right',	renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Plazo para descuento en d�as', tooltip:'Plazo para descuento en d�as',	dataIndex:'IG_PLAZO_DESCUENTO',	sortable:true,	width:120,align:"center"
			},{
				header:'% de Descuento',	tooltip: 'Porcentaje de Descuento', dataIndex:'FN_PORC_DESCUENTO',	sortable:true,	width:100, align:'right',
				renderer:Ext.util.Format.numberRenderer('0.00 %')
			},{
				header:'Monto a descontar', tooltip: 'Monto a descontar',	dataIndex:'',	sortable:true,	width:120,	align:'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											value = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
											return Ext.util.Format.number(value, '$ 0,0.00');
										}
			},{
				header:'Tipo conv.', tooltip: 'Tipo conv.',	dataIndex:'TIPO_CONVERSION',	sortable:true,	width:120,	align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}
											return value;
										}
			},{	// ? 
				header:'Tipo cambio', tooltip:'Tipo cambio',	dataIndex:'TIPO_CAMBIO',	sortable:true,	width:120,	align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}else{
												value = Ext.util.Format.number(value, '$ 0,0.00');
											}
											return value;
										}
			},{
				header:'Monto valuado', tooltip: 'Monto valuado en pesos',	dataIndex:'',	sortable:true,	width:120,	align:"right",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}else{
												var montoDescontar = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
												var valor = (registro.get('FN_MONTO')-montoDescontar)*registro.get('TIPO_CAMBIO');
												value = Ext.util.Format.number(valor, '$ 0,0.00');
											}
											return value;
										}
			},{
				header:'Modalidad de plazo', tooltip: 'Modalidad de plazo',	dataIndex:'MODO_PLAZO',	sortable:true,	width:100,	align:"center"
			},{
				header: 'N�m. de docto. final', tooltip: 'N�mero de documento final',	dataIndex: 'IC_DOCUMENTO',	sortable:true,	width:120,	resizable:true,	align:"center"
			},{
				header: 'Tipo de cr�dito', tooltip: 'Tipo de cr�dito',	dataIndex: 'TIPO_CREDITO',	sortable:true,	width:130,	resizable:true,	align:"center"
			},{
				header: 'IF', tooltip: 'Intermediario financiero',	dataIndex: 'NOMBRE_IF',	sortable:true,	width:150,	resizable:true,	align:"center",
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Monto docto. final', tooltip: 'Monto docto. final',	dataIndex:'MONTO_CREDITO',	sortable:true,	width:120,	align:"right",	hidden: false,	renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header:'Plazo', tooltip: 'Plazo',	dataIndex:'IG_PLAZO_CREDITO',	sortable:true,	width:120,	align:"center"
			},{
				header:'Fecha de Vencimiento', tooltip:'Fecha de Vencimiento',	dataIndex:'FECHA_VENC_CREDITO',align:"center",	sortable:true,	width:100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Referencia tasa de inter�s', tooltip: 'Referencia tasa de inter�s',	dataIndex:'REFERENCIA_INT',	sortable:true,	width:120
			},{
				header:'Valor tasa de inter�s', tooltip: 'Valor tasa de inter�s',	dataIndex:'VALOR_TASA_INT',	sortable:true,	width:120,	hidden:false,	align:"center",
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								var numero = value.toString();
								var dectext="";
								if (numero.indexOf('.') == -1) numero += ".";
									dectext = numero.substring(numero.indexOf('.')+1, numero.length);
								switch(dectext.length) {
									case 1:
										value = Ext.util.Format.number(value,'0.0%');
										break;
									case 2:
										value = Ext.util.Format.number(value,'0.00%');
										break;
									case 3:
										value = Ext.util.Format.number(value,'0.000%');
										break;
									case 4:
										value = Ext.util.Format.number(value,'0.0000%');
										break;
									case 5:
										value = Ext.util.Format.number(value,'0.00000%');
										break;
									default:
										value = Ext.util.Format.number(value,'0%');
								}
								return value;
							}
			},{
				header:'Monto de intereses', tooltip: 'Monto de intereses',	dataIndex:'MONTO_TASA_INT',	sortable:true,	width:120,	hidden:false,	align:"right",	renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Monto total de Capital e Inter�s', tooltip:'Monto total de Capital e Inter�s',	sortable:true,	width:120,	align:"right",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									value = "";
									if (registro.get('MONTO_CREDITO') != "" && registro.get('MONTO_TASA_INT') != "")	{
										value = (parseFloat(registro.get('MONTO_CREDITO')) + parseFloat(registro.get('MONTO_TASA_INT')));	
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},{
				header:'Fecha de rechazo', tooltip: 'Fecha de rechazo',	dataIndex:'FECHA_CAMBIO',	sortable:true,	width:130, align:"center",	renderer:Ext.util.Format.dateRenderer('d/m/Y'),headeable:false, hidden:true
			},{
				header:'Causa', tooltip: 'Causa',	dataIndex:'CAUSA',	sortable:true,	width:150, headeable:false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			}
		],
		bbar: {
			items: [
				{
					text:'Totales', id: 'btnTotales14',
					handler: function(boton, evento) {
									var totalesCmp = Ext.getCmp('gridTotales14');
									if (!totalesCmp.isVisible()) {
										totalesCmp.show();
										totalesCmp.el.dom.scrollIntoView();
									}
								}
				},'-'
			]
		}
	});

	var gridTotales14 = {
		xtype:'grid',	store:totalesData14,	id:'gridTotales14',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:940,	height:100,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 100,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),menuDisabled:true
			},{
				header: 'Monto Autorizado',dataIndex: 'TOTAL_MONTO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Valuado',	dataIndex: 'TOTAL_MONTO_VALUADO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Cr�dito',	dataIndex: 'TOTAL_MONTO_CREDITO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	};

	var grid2 = new Ext.grid.GridPanel({
		title:'Estatus:	Seleccionado Pyme a Negociable',
		store: consultaData2,	stripeRows: true,	loadMask:true,	height:400,	width:940,	frame:false, hidden:true, header:true,
		columns: [
			{
				header:'EPO', tooltip:'EPO',	dataIndex:'NOMBRE_EPO',	sortable:true,	resizable:true,	width:200,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Distribuidor', tooltip: 'Distribuidor',	dataIndex:'NOMBRE_DIST',	sortable:true,	resizable:true,	width:200,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'N�m. acuse de carga', tooltip: 'N�mero acuse de carga',	dataIndex:'CC_ACUSE',	sortable:true,	width:120,	resizable:true
			},{
				header:'N�m. docto. inicial', tooltip:'N�mero de documento inicial',	dataIndex:'IG_NUMERO_DOCTO',	sortable:true, width:120
			},{
				header:'Fecha de Emisi�n', tooltip:'Fecha de Emisi�n',	dataIndex:'DF_FECHA_EMISION',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de Vencimiento', tooltip:'Fecha de Vencimiento',	dataIndex:'DF_FECHA_VENC',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de publicaci�n', tooltip:'Fecha de publicaci�n',	dataIndex:'FECHA_PUBLICACION',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Plazo docto.', tooltip:'Plazo docto.',	dataIndex:'IG_PLAZO_DOCTO',	sortable:true,	width:120, align:"center"
			},{
				header:'Moneda', tooltip: 'Moneda',	dataIndex:'MONEDA',	sortable:true,	width:130,align:"center"
			},{
				header:'Monto', tooltip:'Monto',	dataIndex:'FN_MONTO',	sortable:true,	width:120,	align:'right',	renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Plazo para descuento en d�as', tooltip:'Plazo para descuento en d�as',	dataIndex:'IG_PLAZO_DESCUENTO',	sortable:true,	width:120,align:"center"
			},{
				header:'% de Descuento',	tooltip: 'Porcentaje de Descuento', dataIndex:'FN_PORC_DESCUENTO',	sortable:true,	width:100, align:'right',
				renderer:Ext.util.Format.numberRenderer('0.00 %')
			},{
				header:'Monto a descontar', tooltip: 'Monto a descontar',	dataIndex:'',	sortable:true,	width:120,	align:'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											value = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
											return Ext.util.Format.number(value, '$ 0,0.00');
										}
			},{
				header:'Tipo conv.', tooltip: 'Tipo conv.',	dataIndex:'TIPO_CONVERSION',	sortable:true,	width:120,	align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}
											return value;
										}
			},{	// ? 
				header:'Tipo cambio', tooltip:'Tipo cambio',	dataIndex:'TIPO_CAMBIO',	sortable:true,	width:120,	align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}else{
												value = Ext.util.Format.number(value, '$ 0,0.00');
											}
											return value;
										}
			},{
				header:'Monto valuado en Pesos', tooltip: 'Monto valuado en Pesos',	dataIndex:'',	sortable:true,	width:120,	align:"right",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}else{
												var montoDescontar = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
												var valor = (registro.get('FN_MONTO')-montoDescontar)*registro.get('TIPO_CAMBIO');
												value = Ext.util.Format.number(valor, '$ 0,0.00');
											}
											return value;
										}
			},{
				header:'Modalidad de plazo', tooltip: 'Modalidad de plazo',	dataIndex:'MODO_PLAZO',	sortable:true,	width:100,	align:"center"
			},{
				header: 'N�m. de docto. final', tooltip: 'N�mero de documento final',	dataIndex: 'IC_DOCUMENTO',	sortable:true,	width:120,	resizable:true,	align:"center"
			},{
				header: 'Tipo de cr�dito', tooltip: 'Tipo de cr�dito',	dataIndex: 'TIPO_CREDITO',	sortable:true,	width:130,	resizable:true,	align:"center"
			},{
				header: 'IF', tooltip: 'Intermediario financiero',	dataIndex: 'NOMBRE_IF',	sortable:true,	width:150,	resizable:true,	align:"center",
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Monto docto. final', tooltip: 'Monto docto. final',	dataIndex:'MONTO_CREDITO',	sortable:true,	width:120,	align:"right",	hidden: false,	renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header:'Plazo', tooltip: 'Plazo',	dataIndex:'IG_PLAZO_CREDITO',	sortable:true,	width:120,	align:"center"
			},{
				header:'Fecha de Vencimiento', tooltip:'Fecha de Vencimiento',	dataIndex:'FECHA_VENC_CREDITO',align:"center",	sortable:true,	width:100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Referencia tasa de inter�s', tooltip: 'Referencia tasa de inter�s',	dataIndex:'REFERENCIA_INT',	sortable:true,	width:120
			},{
				header:'Valor tasa de inter�s', tooltip: 'Valor tasa de inter�s',	dataIndex:'VALOR_TASA_INT',	sortable:true,	width:120,	hidden:false,	align:"center",	
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								var numero = value.toString();
								var dectext="";
								if (numero.indexOf('.') == -1) numero += ".";
									dectext = numero.substring(numero.indexOf('.')+1, numero.length);
								switch(dectext.length) {
									case 1:
										value = Ext.util.Format.number(value,'0.0%');
										break;
									case 2:
										value = Ext.util.Format.number(value,'0.00%');
										break;
									case 3:
										value = Ext.util.Format.number(value,'0.000%');
										break;
									case 4:
										value = Ext.util.Format.number(value,'0.0000%');
										break;
									case 5:
										value = Ext.util.Format.number(value,'0.00000%');
										break;
									default:
										value = Ext.util.Format.number(value,'0%');
								}
								return value;
							}
			},{
				header:'Monto de intereses', tooltip: 'Monto de intereses',	dataIndex:'MONTO_TASA_INT',	sortable:true,	width:120,	hidden:false,	align:"right",	renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Monto total de Capital e Inter�s', tooltip:'Monto total de Capital e Inter�s',	sortable:true,	width:120,	align:"right",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									value = "";
									if (registro.get('MONTO_CREDITO') != "" && registro.get('MONTO_TASA_INT') != "")	{
										value = (parseFloat(registro.get('MONTO_CREDITO')) + parseFloat(registro.get('MONTO_TASA_INT')));	
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},{
				header:'Fecha de rechazo', tooltip: 'Fecha de rechazo',	dataIndex:'FECHA_CAMBIO',	sortable:true,	width:130, align:"center",	renderer:Ext.util.Format.dateRenderer('d/m/Y'),headeable:false, hidden:true
			},{
				header:'Causa', tooltip: 'Causa',	dataIndex:'CAUSA',	sortable:true,	width:150, headeable:false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			}
		],
		bbar: {
			items: [
				{
					text:'Totales', id: 'btnTotales2',
					handler: function(boton, evento) {
									var totalesCmp = Ext.getCmp('gridTotales2');
									if (!totalesCmp.isVisible()) {
										totalesCmp.show();
										totalesCmp.el.dom.scrollIntoView();
									}
								}
				},'-'
			]
		}
	});
	var grid22 = new Ext.grid.GridPanel({
		id: 'grid22',
		store: consultaData22,
		stripeRows: true,
		loadMask:true,	
		height:400,
		width:940,	
		frame:false,
		hidden:true,
		header:true,
		columns: [
			{
				header:'EPO', 
				tooltip:'EPO',	
				dataIndex:'NOMBRE_EPO',
				sortable:true,	
				resizable:true,	
				width:200,
				align:"left",
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex:'NOMBRE_DIST',
				sortable:true,
				resizable:true,
				width:200,
				align:"left",
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'N�mero de Acuse de carga', 
				tooltip: 'N�mero de Acuse de carga',
				dataIndex:'CC_ACUSE',
				sortable:true,
				width:120,
				align:"center",
				resizable:true
			},{
				header:'N�mero de documento inicial',
				tooltip:'N�mero de documento inicial',
				dataIndex:'IG_NUMERO_DOCTO',
				sortable:true,
				align:"center",
				width:120
			},{
				header:'Fecha de emisi�n',
				tooltip:'Fecha de emisi�n',	
				dataIndex:'DF_FECHA_EMISION',
				sortable:true,	
				width:100,
				align:"center",
				renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de vencimiento', 
				tooltip:'Fecha de vencimiento',
				dataIndex:'DF_FECHA_VENC',
				sortable:true,
				width:100,	
				align:"center",
				renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de publicaci�n',
				tooltip:'Fecha de publicaci�n',	
				dataIndex:'FECHA_PUBLICACION',
				sortable:true,
				width:100,
				align:"center",
				renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Plazo de documento',
				tooltip:'Plazo de documento',	
				dataIndex:'IG_PLAZO_DOCTO',
				sortable:true,
				width:120,
				align:"center"
			},{
				header:'Moneda',
				tooltip: 'Moneda',
				dataIndex:'MONEDA',
				sortable:true,	
				width:130,
				align:"center"
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex:'FN_MONTO',	
				sortable:true,
				width:120,	
				align:'right',	
				renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Plazo para descuento en d�as',
				tooltip:'Plazo para descuento en d�as',	
				dataIndex:'IG_PLAZO_DESCUENTO',
				sortable:true,
				width:120,
				align:"center"
			},{
				header:'Porcentaje de descuento',
				tooltip: 'Porcentaje de descuento',
				dataIndex:'FN_PORC_DESCUENTO',
				sortable:true,
				width:100, 
				align:'center',
				renderer:Ext.util.Format.numberRenderer('0.00 %')
			},{
				header: 'Monto porcentaje de descuento ',
				tooltip: 'Monto porcentaje de descuento ',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											value = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
											return Ext.util.Format.number(value, '$ 0,0.00');
										}				
			},{
				header:'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex:'MODO_PLAZO',
				sortable:true,
				width:100,
				align:"center"
			},{
				header: 'N�mero de documento final',
				tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO',	
				sortable:true,
				width:120,
				resizable:true,
				align:"center"
			},{
				header: 'Tipo de cr�dito', 
				tooltip: 'Tipo de cr�dito',
				dataIndex: 'TIPO_CREDITO',
				sortable:true,
				width:130,
				resizable:true,
				align:"center"
			},{
				header: 'IF', 
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',	
				sortable:true,
				width:150,
				resizable:true,
				align:"left",
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Monto documento Final',
				tooltip: 'Monto documento Final',
				dataIndex:'MONTO_CREDITO',	
				sortable:true,
				width:120,
				align:"right",
				hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header:'Plazo', 
				tooltip: 'Plazo',
				dataIndex:'IG_PLAZO_CREDITO',
				sortable:true,	
				width:120,
				align:"center"
			},{
				header:'Fecha de vencimiento',
				tooltip:'Fecha de vencimiento',
				dataIndex:'FECHA_VENC_CREDITO',
				align:"center",
				sortable:true,
				width:100,
				hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Referencia de tasa de inter�s',
				tooltip: 'Referencia de tasa de inter�s',	
				dataIndex:'REFERENCIA_INT',	
				sortable:true,
				align:"center",
				width:120
			},{
				header:'Valor Tasa de inter�s',
				tooltip: 'Valor Tasa de inter�s',
				dataIndex:'VALOR_TASA_INT',
				sortable:true,
				width:120,
				hidden:false,
				align:"center",	
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								var numero = value.toString();
								var dectext="";
								if (numero.indexOf('.') == -1) numero += ".";
									dectext = numero.substring(numero.indexOf('.')+1, numero.length);
								switch(dectext.length) {
									case 1:
										value = Ext.util.Format.number(value,'0.0%');
										break;
									case 2:
										value = Ext.util.Format.number(value,'0.00%');
										break;
									case 3:
										value = Ext.util.Format.number(value,'0.000%');
										break;
									case 4:
										value = Ext.util.Format.number(value,'0.0000%');
										break;
									case 5:
										value = Ext.util.Format.number(value,'0.00000%');
										break;
									default:
										value = Ext.util.Format.number(value,'0%');
								}
								return value;
							}
			},{
				header:'Monto de Inter�s',
				tooltip: 'Monto de Inter�s',
				dataIndex:'MONTO_TASA_INT',
				sortable:true,
				width:120,
				hidden:false,
				align:"right",
				renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Monto total de capital',
				tooltip:'Monto total de capital',
				sortable:true,	
				width:120,
				align:"right",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									value = "";
									if (registro.get('MONTO_CREDITO') != "" && registro.get('MONTO_TASA_INT') != "")	{
										value = (parseFloat(registro.get('MONTO_CREDITO')) + parseFloat(registro.get('MONTO_TASA_INT')));	
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},{
				header:'Fecha de rechazo', 
				tooltip: 'Fecha de rechazo',
				dataIndex:'FECHA_CAMBIO',
				sortable:true,	
				width:130, 
				align:"center",
				renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Causa ',
				tooltip: 'Causa',
				dataIndex:'CAUSA',
				sortable:true,	
				width:150,
				align:"center",
				headeable:false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			}
		],
		bbar: {
			items: [
				{
					text:'Totales', id: 'btnTotales22',
					handler: function(boton, evento) {
									var totalesCmp = Ext.getCmp('gridTotales22');
									if (!totalesCmp.isVisible()) {
										totalesCmp.show();
										totalesCmp.el.dom.scrollIntoView();
									}
								}
				},'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo ',
					iconCls: 'icoXls',
					id: 'btnGenerarCSV_22',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
						url: '24reporte02exta.jsp',
						params:	{ic_cambio_estatus:	cveEstatus},
						callback: descargaArchivo
					});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir_22',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
						url: '24reporte02extpdf.jsp',
						params: {ic_cambio_estatus:	cveEstatus},
						callback: mostrarArchivoPDF
					});
					}
				}
			]
		}
	});
	var gridTotales22 = {
		xtype:'grid',	store:totalesData22,	id:'gridTotales22',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:940,	height:100,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 100,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),menuDisabled:true
			},{
				header: 'Monto Autorizado',dataIndex: 'TOTAL_MONTO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Cr�dito',	dataIndex: 'TOTAL_MONTO_CREDITO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	};

	var gridTotales2 = {
		xtype:'grid',	store:totalesData2,	id:'gridTotales2',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:940,	height:100,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 100,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),menuDisabled:true
			},{
				header: 'Monto Autorizado',dataIndex: 'TOTAL_MONTO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Valuado',	dataIndex: 'TOTAL_MONTO_VALUADO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Cr�dito',	dataIndex: 'TOTAL_MONTO_CREDITO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	};
	
	//Modificacion
	var grid21 = new Ext.grid.GridPanel({
		title:'Estatus:	Modificaci�n',
		store: consultaData21,	stripeRows: true,	loadMask:true,	height:400,	width:940,	frame:false, hidden:true, header:true,
		columns: [
			{
				header:'EPO', tooltip:'EPO',	dataIndex:'NOMBRE_EPO',	sortable:true,	resizable:true,	width:200,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Distribuidor', tooltip: 'Distribuidor',	dataIndex:'NOMBRE_DIST',	sortable:true,	resizable:true,	width:200,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'N�m. acuse de carga', tooltip: 'N�mero acuse de carga',	dataIndex:'CC_ACUSE',	sortable:true,	width:120,	resizable:true
			},{
				header:'N�m. docto. inicial', tooltip:'N�mero de documento inicial',	dataIndex:'IG_NUMERO_DOCTO',	sortable:true, width:120
			},{
				header:'Fecha de Emisi�n', tooltip:'Fecha de Emisi�n',	dataIndex:'DF_FECHA_EMISION',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de Vencimiento', tooltip:'Fecha de Vencimiento',	dataIndex:'DF_FECHA_VENC',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de publicaci�n', tooltip:'Fecha de publicaci�n',	dataIndex:'FECHA_PUBLICACION',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Plazo docto.', tooltip:'Plazo docto.',	dataIndex:'IG_PLAZO_DOCTO',	sortable:true,	width:120, align:"center"
			},{
				header:'Moneda', tooltip: 'Moneda',	dataIndex:'MONEDA',	sortable:true,	width:130,align:"center"
			},{
				header:'Monto', tooltip:'Monto',	dataIndex:'FN_MONTO',	sortable:true,	width:120,	align:'right',	renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Plazo para descuento en d�as', tooltip:'Plazo para descuento en d�as',	dataIndex:'IG_PLAZO_DESCUENTO',	sortable:true,	width:120,align:"center"
			},{
				header:'% de Descuento',	tooltip: 'Porcentaje de Descuento', dataIndex:'FN_PORC_DESCUENTO',	sortable:true,	width:100, align:'right',
				renderer:Ext.util.Format.numberRenderer('0.00 %')
			},{
				header:'Monto a descontar', tooltip: 'Monto a descontar',	dataIndex:'',	sortable:true,	width:120,	align:'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											value = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
											return Ext.util.Format.number(value, '$ 0,0.00');
										}
			},{
				header:'Tipo conv.', tooltip: 'Tipo conv.',	dataIndex:'TIPO_CONVERSION',	sortable:true,	width:120,	align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}
											return value;
										}
			},{	// ? 
				header:'Tipo cambio', tooltip:'Tipo cambio',	dataIndex:'TIPO_CAMBIO',	sortable:true,	width:120,	align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}else{
												value = Ext.util.Format.number(value, '$ 0,0.00');
											}
											return value;
										}
			},{
				header:'Monto valuado en Pesos', tooltip: 'Monto valuado en Pesos',	dataIndex:'',	sortable:true,	width:120,	align:"right",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}else{
												var montoDescontar = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
												var valor = (registro.get('FN_MONTO')-montoDescontar)*registro.get('TIPO_CAMBIO');
												value = Ext.util.Format.number(valor, '$ 0,0.00');
											}
											return value;
										}
			},{
				header:'Modalidad de plazo', tooltip: 'Modalidad de plazo',	dataIndex:'MODO_PLAZO',	sortable:true,	width:100,	align:"center"
			},{
				header:'Fecha de Cambio', tooltip:'Fecha de Cambio',	dataIndex:'FECHA_CAMBIO',	align:"center",	sortable:true,	width:100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Anterior Fecha de Emisi�n', tooltip:'Anterior Fecha de Emisi�n',	dataIndex:'FECHA_EMISION_ANT',	align:"center",	sortable:true,	width:100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Anterior Fecha de Vencimiento', tooltip:'Anterior Fecha de Vencimiento',	dataIndex:'FECHA_VENC_ANT',	align:"center",	sortable:true,	width:100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Anterior Monto del Documento', tooltip:'Anterior Monto del Documento',	dataIndex:'FN_MONTO_ANTERIOR',	sortable:true,	width:120,	align:'right',	renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Anterior Modalidad del Plazo', tooltip: 'Anterior Modalidad del Plazo',	dataIndex:'MODO_PLAZO_ANT',	sortable:true,	width:130,align:"center"
			}
		],
		bbar: {
			items: [
				{
					text:'Totales', id: 'btnTotales21',
					handler: function(boton, evento) {
									var totalesCmp = Ext.getCmp('gridTotales21');
									if (!totalesCmp.isVisible()) {
										totalesCmp.show();
										totalesCmp.el.dom.scrollIntoView();
									}
								}
				},'-'
			]
		}
	});

	var gridTotales21 = {
		xtype:'grid',	store:totalesData21,	id:'gridTotales21',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:940,	height:100,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 100,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),menuDisabled:true
			},{
				header: 'Monto Autorizado',dataIndex: 'TOTAL_MONTO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Valuado',	dataIndex: 'TOTAL_MONTO_VALUADO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Cr�dito',	dataIndex: 'TOTAL_MONTO_CREDITO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	};

	/**** Fin Grid�s ****/

	var toolbar = new Ext.Toolbar({
		id:'barBotones',
		style: 'margin:0 auto;',
		autoScroll : true,
		width: 250,
		height: 40,
		 items: [
			'->','-',
			{
				text:'Generar Archivo',	id:'btnGenerarArchivo',	iconCls:'icoXls', scale:'medium'
			},{
				text:'Bajar Archivo',	id:'btnBajarArchivo',	hidden:true, scale:'medium'
			},'-',{
				xtype: 'tbspacer', width: 2
			},{
				text:'Generar Pdf',	id:'btnGenerarPDF',	iconCls:'icoPdf',scale:'medium'
			},{
				text:'Bajar Pdf',	id:'btnBajarPdf',	hidden:true, scale:'medium'
			},'-',{
				xtype: 'tbspacer', width: 2
			}
		]
	});

	var elementosForma = [
		{
			xtype:'combo',
			name:	'ic_cambio_estatus',
			id:	'_ic_cambio_estatus',
			hiddenName:	'ic_cambio_estatus',
			fieldLabel:	'Estatus',
			emptyText:	'Seleccione Estatus . . .',
			forceSelection:true,	triggerAction:'all',	typeAhead: true,	minChars:1,	mode: 'local',	displayField:'descripcion',	valueField:'clave',
			store:catalogoEstatus,	tpl:NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
							Ext.getCmp('btnGenerarArchivo').enable();
							Ext.getCmp('btnGenerarPDF').enable();
							Ext.getCmp('btnBajarPdf').hide();
							Ext.getCmp("btnBajarArchivo").hide();
							valGrid.gridUno=false;
							valGrid.gridDos=false;
							valGrid.gridTres=false;
							valGrid.gridCuatro=false;
							toolbar.hide();
							grid14.hide();
							Ext.getCmp('gridTotales14').hide();
							grid2.hide();
							Ext.getCmp('gridTotales2').hide();
							grid22.hide();
							Ext.getCmp('gridTotales22').hide();
							grid21.hide();
							Ext.getCmp('gridTotales21').hide();
							toolbar.setWidth(250);
							cveEstatus = combo.value;
							if(!Ext.isEmpty(cveEstatus)){
								if(cveEstatus == "14"){
									
									fp.el.mask('Enviando...', 'x-mask-loading');
									consultaData14.load({params:	{ic_cambio_estatus:cveEstatus}});
								}else if(cveEstatus	==	"2"){
									fp.el.mask('Enviando...', 'x-mask-loading');
									consultaData2.load({params:	{ic_cambio_estatus:cveEstatus}});
								}else if(cveEstatus == "21"){
									fp.el.mask('Enviando...', 'x-mask-loading');
									consultaData21.load({params:	{ic_cambio_estatus:cveEstatus}});
								}else if(cveEstatus == "23"||cveEstatus == "34"){
									fp.el.mask('Enviando...', 'x-mask-loading');
									consultaData22.load({params:	{ic_cambio_estatus:cveEstatus}});
								}						
							}
					}
				}
			}
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		frame:true,
		labelWidth: 60,
		style: ' margin:0 auto;',
		title: '<div><center>Cambios de Estatus</div>',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items:	elementosForma,
		monitorValid: true
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,		NE.util.getEspaciador(20),
			grid14,	gridTotales14,NE.util.getEspaciador(10),
			grid2,	gridTotales2, NE.util.getEspaciador(10),
			grid22,	gridTotales22, NE.util.getEspaciador(10),
			grid21,	gridTotales21,NE.util.getEspaciador(20),
			toolbar,	NE.util.getEspaciador(10)
		]
	});
	toolbar.hide();
	catalogoEstatus.load();
	//pnl.el.mask('Enviando...', 'x-mask-loading');
	//consultaData14.load({params:	{ic_cambio_estatus:"14"}});

});//Fin de funcion principal