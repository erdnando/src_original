<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String ic_estatus	=	(request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");

try {

	if(iNoCliente != null && !iNoCliente.equals("") ) {
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
		String nombreArchivo = null;

		//variables de despliegue
		String epo				= "";
		String distribuidor 	= "";
		String numAcuse			= "";
		String numDocto			= "";
		String numAcuseCarga	= "";
		String fechaEmision		= "";
		String fechaPublicacion	= "";
		String fechaVencimiento	= "";
		String plazoDocto		= "";
		String moneda 			= "";
		String icMoneda			= "";
		double monto 			= 0;
		String tipoConversion	= "";
		double tipoCambio		= 0;
		double montoValuado		= 0;
		String plazoDescuento	= "";
		String porcDescuento	= "";
		double montoDescontar	= 0;
		String modoPlazo		= "";
		String estatus 			= "";
		String icDocumento		= "";
		String intermediario	= "";
		String tipoCredito		= "";
		String fechaOperacion	= "";
		double montoCredito		= 0;
		String plazoCredito		= "";
		String fechaVencCredito	= "";
		String referenciaTasaInt= "";
		double valorTasaInt		= 0;
		double montoTasaInt 	= 0;
		String tipoCobranza		= "";
		String tipoCobroInt		= ""; 
		String numeroPago	 	= "";
		String causa			= "";
		String fechaCambio		= "";
		String antFechaEmision	= "";
		String antFechaVto		= "";
		double antMonto			= 0;
		String antModoPlazo		= "";
		String monedaLinea		= "";
		//totales
		int		i = 0;
		int		totalDoctosMN		= 0;
		int 		totalDoctosUSD		= 0;
		double	totalMontoMN		= 0;
		double	totalMontoUSD		= 0;
		double	totalMontoValuadoMN	= 0;
		double	totalMontoValuadoUSD	= 0;
		double	totalMontoCreditoMN	= 0;
		double	totalMontoCreditoUSD	= 0;

		Registros reg	=	new Registros();
		RepCambiosEstatusIf reporte = new RepCambiosEstatusIf();

		if("14".equals(ic_estatus) ||"".equals(ic_estatus)	){
			totalDoctosMN = 0;
			totalMontoMN = 0;
			totalMontoValuadoMN = 0;
			totalDoctosUSD = 0;
			totalMontoUSD = 0;
			totalMontoValuadoUSD = 0;
			i=0;
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			reporte.setClaveEstatus("14");
			reporte.setQrysentencia();
			reg = reporte.executeQuery();
			contenidoArchivo.append("\n \nSeleccionado Pyme a Negociable\nEpo,Distribuidor,Número de Acuse de Carga,Num. docto.,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda"+
											",Monto,Plazo para descuento en dias,% de descuento,Monto a descontar,Tipo conv.,Tipo cambio,Monto valuado en pesos,Modalidad de plazo"+",Num. de crédito,Tipo de Crédito,IF,"+
											"Monto crédito,Plazo,Fecha de Vencimiento,Referencia tasa de interés,Valor tasa de interés,Monto de Intereses,Monto Total de Capital e Interés,Fecha de Rechazo,Causa\n");
			while(reg.next()){
				epo			 		= (reg.getString("NOMBRE_EPO")==null)?"":reg.getString("NOMBRE_EPO");
				distribuidor 		= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
				numDocto				= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
				numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
				//fechaPublicacion	= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
				fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
				plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
				moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
				monto 				= Double.parseDouble(reg.getString("FN_MONTO").equals("")?"0":reg.getString("FN_MONTO"));
				tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
				tipoCambio			= Double.parseDouble(reg.getString("TIPO_CAMBIO").equals("")?"0":reg.getString("TIPO_CAMBIO"));
				plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
				porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
				montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
				montoValuado		= (monto-montoDescontar)*tipoCambio;
				modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
				estatus 				= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
				icMoneda				= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				tipoCobranza		= (reg.getString("TIPO_COBRANZA")==null)?"":reg.getString("TIPO_COBRANZA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				intermediario		= (reg.getString("NOMBRE_IF")==null)?"":reg.getString("NOMBRE_IF");
				tipoCredito			= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
				montoCredito		= Double.parseDouble(reg.getString("MONTO_CREDITO").equals("")?"0":reg.getString("MONTO_CREDITO"));
				plazoCredito		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
				tipoCobroInt		= (reg.getString("TIPO_COBRO_INT")==null)?"":reg.getString("TIPO_COBRO_INT");
				referenciaTasaInt	= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
				valorTasaInt		= Double.parseDouble(reg.getString("VALOR_TASA_INT").equals("")?"0":reg.getString("VALOR_TASA_INT"));
				montoTasaInt		= Double.parseDouble(reg.getString("MONTO_TASA_INT").equals("")?"0":reg.getString("MONTO_TASA_INT"));
				numAcuse				= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaPublicacion	= (reg.getString("FECHA_PUBLICACION")==null)?"":reg.getString("FECHA_PUBLICACION");
				fechaVencCredito	= (reg.getString("FECHA_VENC_CREDITO")==null)?"":reg.getString("FECHA_VENC_CREDITO");
				fechaCambio			= (reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
				causa					= (reg.getString("CAUSA")==null)?"":reg.getString("CAUSA");
				monedaLinea			= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += montoCredito;
				}else{
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					if(!icMoneda.equals(monedaLinea))
						totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += montoCredito;
				}
				contenidoArchivo.append(epo.replace(',',' ')+","+distribuidor.replace(',',' ')+","+numAcuse+","+numDocto+","+fechaEmision+","+fechaVencimiento+","+fechaPublicacion+","+plazoDocto+","+
									moneda+","+monto+","+plazoDescuento+","+porcDescuento+","+montoDescontar+",");
				if(!icMoneda.equals(monedaLinea)){
					contenidoArchivo.append(tipoConversion+","+tipoCambio+","+montoValuado+",");
				}else{
					contenidoArchivo.append(",,,");
				}
				contenidoArchivo.append(modoPlazo+","+icDocumento+","+tipoCredito+","+intermediario.replace(',',' ')+","+montoCredito+","+plazoCredito+","+fechaVencCredito+","+
									referenciaTasaInt.replace(',',' ')+","+valorTasaInt+"%,"+montoTasaInt+","+(montoCredito+montoTasaInt)+","+fechaCambio+","+causa+"\n");
				i++;
			}
			if(i==0){
				contenidoArchivo.append("\nNo se encontró ningún registro");
			}
			if(totalDoctosMN>0){
				contenidoArchivo.append("\nTotal M.N.,"+totalDoctosMN+",,,,,,,,"+totalMontoMN+",,,,,,,,,,,"+totalMontoCreditoMN);
			}
			if(totalDoctosUSD>0){
				contenidoArchivo.append("\nTotal Dolares,"+totalDoctosUSD+",,,,,,,,"+totalMontoUSD+",,,,,,"+totalMontoValuadoUSD+",,,,,"+totalMontoCreditoUSD);
			}
		}
		if("2".equals(ic_estatus) ||"".equals(ic_estatus)	){
			totalDoctosMN = 0;
			totalMontoMN = 0;
			totalMontoValuadoMN = 0;
			totalDoctosUSD = 0;
			totalMontoUSD = 0;
			totalMontoValuadoUSD = 0;
			i=0;
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			reporte.setClaveEstatus("2");
			reporte.setQrysentencia();
			reg = reporte.executeQuery();
			contenidoArchivo.append("\n \nSeleccionado Pyme a Negociable\nEpo,Distribuidor,Número de Acuse de Carga,Num. docto.,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda"+
										",Monto,Plazo para descuento en dias,% de descuento,Monto a descontar,Tipo conv.,Tipo cambio,Monto valuado en pesos,Modalidad de plazo"+",Num. de crédito,Tipo de Crédito,IF,"+
										"Monto crédito,Plazo,Fecha de Vencimiento,Referencia tasa de interés,Valor tasa de interés,Monto de Intereses,Monto Total de Capital e Interés,Fecha de Rechazo,Causa\n");

			while(reg.next()){
				epo			 		= (reg.getString("NOMBRE_EPO")==null)?"":reg.getString("NOMBRE_EPO");
				distribuidor 		= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
				numDocto				= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
				numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
				//fechaPublicacion	= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
				fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
				plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
				moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
				monto 				= Double.parseDouble(reg.getString("FN_MONTO").equals("")?"0":reg.getString("FN_MONTO"));
				tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
				tipoCambio			= Double.parseDouble(reg.getString("TIPO_CAMBIO").equals("")?"0":reg.getString("TIPO_CAMBIO"));
				plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
				porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
				montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
				montoValuado		= (monto-montoDescontar)*tipoCambio;
				modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
				estatus 				= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
				icMoneda				= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				tipoCobranza		= (reg.getString("TIPO_COBRANZA")==null)?"":reg.getString("TIPO_COBRANZA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				intermediario		= (reg.getString("NOMBRE_IF")==null)?"":reg.getString("NOMBRE_IF");
				tipoCredito			= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
				montoCredito		= Double.parseDouble(reg.getString("MONTO_CREDITO").equals("")?"0":reg.getString("MONTO_CREDITO"));
				plazoCredito		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
				tipoCobroInt		= (reg.getString("TIPO_COBRO_INT")==null)?"":reg.getString("TIPO_COBRO_INT");
				referenciaTasaInt	= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
				valorTasaInt		= Double.parseDouble(reg.getString("VALOR_TASA_INT").equals("")?"0":reg.getString("VALOR_TASA_INT"));
				montoTasaInt		= Double.parseDouble(reg.getString("MONTO_TASA_INT").equals("")?"0":reg.getString("MONTO_TASA_INT"));
				numAcuse				= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaPublicacion	= (reg.getString("FECHA_PUBLICACION")==null)?"":reg.getString("FECHA_PUBLICACION");
				fechaVencCredito	= (reg.getString("FECHA_VENC_CREDITO")==null)?"":reg.getString("FECHA_VENC_CREDITO");
				fechaCambio			= (reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
				causa					= (reg.getString("CAUSA")==null)?"":reg.getString("CAUSA");
				monedaLinea			= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += montoCredito;
				}else{
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					if(!icMoneda.equals(monedaLinea))
						totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += montoCredito;
				}
				contenidoArchivo.append(epo.replace(',',' ')+","+distribuidor.replace(',',' ')+","+numAcuse+","+numDocto+","+fechaEmision+","+fechaVencimiento+","+fechaPublicacion+","+plazoDocto+","+
									moneda+","+monto+","+plazoDescuento+","+porcDescuento+","+montoDescontar+",");
				if(!icMoneda.equals(monedaLinea)){
					contenidoArchivo.append(tipoConversion+","+tipoCambio+","+montoValuado+",");
				}else{
					contenidoArchivo.append(",,,");
				}
				contenidoArchivo.append(modoPlazo+","+icDocumento+","+tipoCredito+","+intermediario.replace(',',' ')+","+montoCredito+","+plazoCredito+","+fechaVencCredito+","+
									referenciaTasaInt.replace(',',' ')+","+valorTasaInt+"%,"+montoTasaInt+","+(montoCredito+montoTasaInt)+","+fechaCambio+","+causa+"\n");
				i++;
			}
			if(i==0){
				contenidoArchivo.append("\nNo se encontró ningún registro");
			}
			if(totalDoctosMN>0){
				contenidoArchivo.append("\nTotal M.N.,"+totalDoctosMN+",,,,,,,,"+totalMontoMN+",,,,,,"+totalMontoValuadoMN+",,,,,"+totalMontoCreditoMN);
			}
			if(totalDoctosUSD>0){
				contenidoArchivo.append("\nTotal Dolares,"+totalDoctosUSD+",,,,,,,,"+totalMontoUSD+",,,,,,"+totalMontoValuadoUSD+",,,,,"+totalMontoCreditoUSD);
			}
		}
		///yo lo agregue
		if("23".equals(ic_estatus) ||"34".equals(ic_estatus)	){
			totalDoctosMN = 0;
			totalMontoMN = 0;
			totalMontoValuadoMN = 0;
			totalDoctosUSD = 0;
			totalMontoUSD = 0;
			totalMontoValuadoUSD = 0;
			i=0;
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			if("23".equals(ic_estatus)){
				reporte.setClaveEstatus("23");
			}else if("34".equals(ic_estatus)){
				reporte.setClaveEstatus("34");
			}
			reporte.setQrysentencia();
			reg = reporte.executeQuery();
			if("23".equals(ic_estatus)){
				contenidoArchivo.append("\n \nEn Proceso de Autorizacion IF a Negociable\nEpo,Distribuidor,Número de Acuse de carga,Número de documento inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo de documento,Moneda"+
										",Monto,Plazo para descuento en días,Porcentaje de descuento,Monto porcentaje de descuento,Modalidad de plazo"+",Número de documento final,Tipo de Crédito,IF,"+
										"Monto documeto Final ,Plazo,Fecha de vencimiento,Referencia de  tasa de interés,Valor Tasa de interés,Monto de Interés,Monto Total de Capital,Fecha de rechazo,Causa\n");

			}else{
			
			contenidoArchivo.append("\n \nEn Proceso de Autorizacion IF a Seleccionado Pyme\nEpo,Distribuidor,Número de Acuse de carga,Número de documento inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo de documento,Moneda"+
										",Monto,Plazo para descuento en días,Porcentaje de descuento,Monto porcentaje de descuento,Modalidad de plazo"+",Número de documento final,Tipo de Crédito,IF,"+
										"Monto documeto Final ,Plazo,Fecha de vencimiento,Referencia de  tasa de interés,Valor Tasa de interés,Monto de Interés,Monto Total de Capital,Fecha de rechazo,Causa\n");
			}
			while(reg.next()){
				epo			 		= (reg.getString("NOMBRE_EPO")==null)?"":reg.getString("NOMBRE_EPO");
				distribuidor 		= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
				numDocto				= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
				numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
				//fechaPublicacion	= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
				fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
				plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
				moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
				monto 				= Double.parseDouble(reg.getString("FN_MONTO").equals("")?"0":reg.getString("FN_MONTO"));
				tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
				tipoCambio			= Double.parseDouble(reg.getString("TIPO_CAMBIO").equals("")?"0":reg.getString("TIPO_CAMBIO"));
				plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
				porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
				montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
				montoValuado		= (monto-montoDescontar)*tipoCambio;
				modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
				estatus 				= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
				icMoneda				= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				tipoCobranza		= (reg.getString("TIPO_COBRANZA")==null)?"":reg.getString("TIPO_COBRANZA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				intermediario		= (reg.getString("NOMBRE_IF")==null)?"":reg.getString("NOMBRE_IF");
				tipoCredito			= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
				montoCredito		= Double.parseDouble(reg.getString("MONTO_CREDITO").equals("")?"0":reg.getString("MONTO_CREDITO"));
				plazoCredito		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
				tipoCobroInt		= (reg.getString("TIPO_COBRO_INT")==null)?"":reg.getString("TIPO_COBRO_INT");
				referenciaTasaInt	= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
				valorTasaInt		= Double.parseDouble(reg.getString("VALOR_TASA_INT").equals("")?"0":reg.getString("VALOR_TASA_INT"));
				montoTasaInt		= Double.parseDouble(reg.getString("MONTO_TASA_INT").equals("")?"0":reg.getString("MONTO_TASA_INT"));
				numAcuse				= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaPublicacion	= (reg.getString("FECHA_PUBLICACION")==null)?"":reg.getString("FECHA_PUBLICACION");
				fechaVencCredito	= (reg.getString("FECHA_VENC_CREDITO")==null)?"":reg.getString("FECHA_VENC_CREDITO");
				fechaCambio			= (reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
				causa					= (reg.getString("CAUSA")==null)?"":reg.getString("CAUSA");
				monedaLinea			= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoCreditoMN += montoCredito;
				}else{
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					
					totalMontoCreditoUSD += montoCredito;
				}
				contenidoArchivo.append(epo.replace(',',' ')+","+distribuidor.replace(',',' ')+","+numAcuse+","+numDocto+","+fechaEmision+","+fechaVencimiento+","+fechaPublicacion+","+plazoDocto+","+
									moneda+","+"$"+monto+","+plazoDescuento+","+porcDescuento+"%"+","+"$"+montoDescontar+",");
				
				contenidoArchivo.append(modoPlazo+","+icDocumento+","+tipoCredito+","+intermediario.replace(',',' ')+","+"$"+monto+","+plazoCredito+","+fechaVencCredito+","+
									referenciaTasaInt.replace(',',' ')+","+valorTasaInt+"%,"+"$"+montoTasaInt+","+"$"+(montoCredito+montoTasaInt)+","+fechaCambio+","+causa+"\n");
				i++;
			}
			if(i==0){
				contenidoArchivo.append("\nNo se encontró ningún registro");
			}
			if(totalDoctosMN>0){
				contenidoArchivo.append("\nTotal M.N.,"+totalDoctosMN+",,,,,,,,"+"$"+totalMontoMN+",,,,,,,,"+"$"+totalMontoCreditoMN);
				
			}
			if(totalDoctosUSD>0){
				contenidoArchivo.append("\nTotal Dolares,"+totalDoctosUSD+",,,,,,,,"+"$"+totalMontoUSD+",,,,,,,,"+"$"+totalMontoCreditoMN);
			}
		}
	///yo lo agregue 77
		if("21".equals(ic_estatus) ||"".equals(ic_estatus)	){
			totalDoctosMN = 0;
			totalMontoMN = 0;
			totalMontoValuadoMN = 0;
			totalDoctosUSD = 0;
			totalMontoUSD = 0;
			totalMontoValuadoUSD = 0;
			i=0;
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			reporte.setClaveEstatus("21");
			reporte.setQrysentencia();
			reg = reporte.executeQuery();
			contenidoArchivo.append("\n \nModificación\nEpo,Distribuidor,Número de Acuse de Carga,Num. docto.,Fecha de emisión,Fecha de vencimiento,Fecha de Publicación,Plazo docto."+
								",Moneda,Monto,Plazo para descuento en dias,% de descuento,Monto a descontar,Tipo conv.,Tipo cambio,Monto valuado en pesos,Modalidad de plazo"+
								",Fecha Cambio,Anterior Fecha de Emisi&oacute;n,Anterior Fecha de Vencimiento,Anterior Monto del Documento,Anterior Modalidad de Plazo\n");
			while(reg.next()){
				epo			 			= (reg.getString("NOMBRE_EPO")==null)?"":reg.getString("NOMBRE_EPO");
				distribuidor 			= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
				numDocto					= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
				numAcuseCarga			= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaEmision			= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
				//fechaPublicacion		= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
				fechaVencimiento		= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
				plazoDocto				= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
				moneda 					= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
				monto						= Double.parseDouble(reg.getString("FN_MONTO").equals("")?"0":reg.getString("FN_MONTO"));
				tipoConversion			= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
				tipoCambio				= Double.parseDouble(reg.getString("TIPO_CAMBIO").equals("")?"0":reg.getString("TIPO_CAMBIO"));
				plazoDescuento			= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
				porcDescuento			= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
				montoDescontar			= monto*Double.parseDouble(porcDescuento)/100;
				montoValuado			= (monto-montoDescontar)*tipoCambio;
				modoPlazo				= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
				icMoneda					= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
				icDocumento				= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				tipoCobranza			= (reg.getString("TIPO_COBRANZA")==null)?"":reg.getString("TIPO_COBRANZA");
				tipoCredito				= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
				numAcuse					= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaPublicacion		= (reg.getString("FECHA_PUBLICACION")==null)?"":reg.getString("FECHA_PUBLICACION");
				fechaCambio				= (reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
				antFechaEmision		= (reg.getString("FECHA_EMISION_ANT")==null)?"":reg.getString("FECHA_EMISION_ANT");
				antFechaVto				= (reg.getString("FECHA_VENC_ANT")==null)?"":reg.getString("FECHA_VENC_ANT");
				antMonto					= Double.parseDouble((reg.getString("FN_MONTO_ANTERIOR").equals(""))?"0":reg.getString("FN_MONTO_ANTERIOR"));
				antModoPlazo			= (reg.getString("MODO_PLAZO_ANT")==null)?"":reg.getString("MODO_PLAZO_ANT");
				icMoneda					= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
				monedaLinea				= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
				}else{
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					if(!icMoneda.equals(monedaLinea))
						totalMontoValuadoUSD += montoValuado;
				}
				contenidoArchivo.append(epo.replace(',',' ')+","+distribuidor.replace(',',' ')+","+numAcuse+","+numDocto+","+fechaEmision+","+fechaVencimiento+","+fechaPublicacion+","+plazoDocto+","+
									moneda+","+monto+","+plazoDescuento+","+porcDescuento+","+montoDescontar+",");
				if(!icMoneda.equals(monedaLinea)){
					contenidoArchivo.append(tipoConversion+","+tipoCambio+","+montoValuado+",");
				}else{
					contenidoArchivo.append(",,,");
				}
				contenidoArchivo.append(modoPlazo+","+fechaCambio+","+antFechaEmision+","+antFechaVto+","+antMonto+","+antModoPlazo+"\n");
				i++;
			}
			if(i==0){
				contenidoArchivo.append("\nNo se encontró ningún registro");
			}
			if(totalDoctosMN>0){
				contenidoArchivo.append("\nTotal M.N.,"+totalDoctosMN+",,,,,,,,"+totalMontoMN+",,,,"+totalMontoValuadoMN);
			}
			if(totalDoctosUSD>0){
				contenidoArchivo.append("\nTotal Dolares,"+totalDoctosUSD+",,,,,,,,"+totalMontoUSD+",,,,"+totalMontoValuadoUSD);
			}
		}

		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		} else {
			nombreArchivo = archivo.nombre;
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>