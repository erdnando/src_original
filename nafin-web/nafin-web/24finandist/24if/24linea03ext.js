Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

	var formulario = new Array();
	var strPerfil = Ext.get('_strPerfil').getValue();
	var editAmount = 0;

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if( Ext.util.JSON.decode(response.responseText).diaHabilUno){
				var ventana = Ext.getCmp('winInhabil');
				if (ventana) {
					ventana.destroy();
				}
				new Ext.Window({
					layout: 'fit',
					width: 940,
					height: 500,
					modal:true,
					id: 'winInhabil',
					closable:false,
					items: [	gridInhabil	],
					title: 'L�neas de Cr�dito a vencer en los pr�ximos 90 d�as'
				}).show();
				inhabilData.load();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarConfirmar(opts, success, response) {
		pnl.el.unmask();
		Ext.getCmp('btnConfirmar').setIconClass('aceptar');
		confirmaData.loadData('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			fp.hide();
			grid.hide();
			Ext.getCmp('gridTotales').hide();
			if (!gridConfirma.isVisible()){
				gridConfirma.show();
				gridConfirma.setTitle("Usted realiz� cambios a las siguientes l�neas.");
			}
			if (infoR.regs != undefined && infoR.regs.length > 0){
				confirmaData.loadData(infoR.regs);
				if (strPerfil === "IF FACT RECURSO"){
					var cm = gridConfirma.getColumnModel();
					cm.setHidden(cm.findColumnIndex('IG_CUENTA_BANCARIA'),false);
					cm.setHidden(cm.findColumnIndex('CG_USUARIO_CAMBIO'),false);
					cm.setColumnHeader(cm.findColumnIndex('ESTATUS'),"Estatus");
					cm.setColumnHeader(cm.findColumnIndex('FECHA_HOY'),"Fecha y hora �ltima modificaci�n");
					cm.setColumnTooltip(cm.findColumnIndex('FECHA_HOY'),"Fecha y hora �ltima modificaci�n");
				}
				gridConfirma.getGridEl().unmask();
				if (infoR.totales != undefined && infoR.totales.length > 0 ){
					Ext.getCmp('gridTotConfirma').show();
					Ext.each(infoR.totales, function(item,index){
						resumenTotConfirmaData.loadData('');
						var reg = Ext.data.Record.create(['NOMMONEDA', 'TOTAL_REGISTROS','TOTAL_MONTO_DOCUMENTOS']);
						if (parseFloat(item.numRegistrosMN)!=0) {
							resumenTotConfirmaData.add(	new reg({	NOMMONEDA:'Total Moneda Nacional',TOTAL_REGISTROS:item.numRegistrosMN ,	TOTAL_MONTO_DOCUMENTOS:item.montoTotalMN}));
						}
						if (parseFloat(item.numRegistrosDL)!=0)	{
							resumenTotConfirmaData.add(	new reg({	NOMMONEDA:'Total D�lares',TOTAL_REGISTROS:item.numRegistrosDL ,	TOTAL_MONTO_DOCUMENTOS:item.montoTotalDL}));
						}
					});
				}
			}else{
				gridConfirma.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
			}
			Ext.getCmp('btnImprimeConfirma').setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = infoR.urlArchivo;
				forma.submit();
			});
			/*if (infoR.msg == "ok"){
				Ext.getCmp('btnImprimeConfirma').setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = infoR.urlArchivo;
					forma.submit();
				});
			}else{
				Ext.Msg.alert('Mensaje de p�gina web',msg,
				function(){
					window.location = '24linea03ext.jsp';
					return;
				});
			}*/
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivoIn =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivoIn');
		btnGenerarArchivo.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivoIn');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDFIn =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFIn');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFIn');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarInhabilData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			if (!gridInhabil.isVisible()) {
				gridInhabil.show();
			}
			Ext.getCmp('btnBajarPDFIn').hide();
			Ext.getCmp('btnBajarArchivoIn').hide();
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivoIn');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDFIn');
			var el = gridInhabil.getGridEl();

			if(store.getTotalCount() > 0) {
				btnGenerarPDF.enable();
				btnGenerarArchivo.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
/*
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var cm = grid.getColumnModel();
			cm.setHidden(cm.findColumnIndex('IG_CUENTA_BANCARIA'),true);
			cm.setHidden(cm.findColumnIndex('CG_USUARIO_CAMBIO'),true);
			if (strPerfil === "IF FACT RECURSO"){
				cm.setHidden(cm.findColumnIndex('IG_CUENTA_BANCARIA'),false);
				cm.setHidden(cm.findColumnIndex('CG_USUARIO_CAMBIO'),false);
				cm.setColumnHeader(cm.findColumnIndex('FECHA_CAMBIO'),"Fecha y hora �ltima modificaci�n");
				cm.setColumnTooltip(cm.findColumnIndex('FECHA_CAMBIO'),"Fecha y hora �ltima modificaci�n");
			}
			//Ext.getCmp('btnBajarPDF').hide();
			Ext.getCmp('btnBajarArchivo').hide();
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnTotales = Ext.getCmp('btnTotales');
			var btnConfirmar = Ext.getCmp('btnConfirmar');

			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				btnTotales.enable();
				btnGenerarPDF.enable();
				btnGenerarArchivo.enable();
				btnConfirmar.enable();
				el.unmask();
			} else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnConfirmar.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var tipoCreditoData;
	if (strPerfil === "IF FACT RECURSO"){
		tipoCreditoData = new Ext.data.ArrayStore({
			 fields: ['clave', 'descripcion'],
			 data : [
				['','Seleccione tipo de cr�dito'],
				['F','Factoraje con recurso']
			 ]
		});
	}else{
		tipoCreditoData = new Ext.data.ArrayStore({
			 fields: ['clave', 'descripcion'],
			 data : [
				['','Seleccione tipo de cr�dito'],
				['D','Modalidad 1 (Riesgo Empresa de Primer Orden )'],
				['C','Modalidad 2 (Riesgo Distribuidor)'],
				['A','Ambos']
			 ]
		});
	}

	var tipoSolData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['I','INICIAL']/*,
			['A','AMPLIACI�N'],
			['R','RENOVACI�N']*/
		 ]
	});

	var procesaCatalogoEpo = function(store, arrRegistros, opts) {
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione EPO',	loadMsg: null	})	);
			Ext.getCmp('_ic_epo').setValue("");
	}

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea03ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load: procesaCatalogoEpo, exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea03ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDistribuidor'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesaCatalogoStatus = function(store, arrRegistros, opts) {
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'Todos los estatus',	loadMsg: null	})	);
			Ext.getCmp('_ic_estatus_linea').setValue("");
	}

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea03ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEstatusDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesaCatalogoStatus,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesaCatalogoEstatusAsignar = function(store, arrRegistros, opts) {
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "0",	descripcion: 'Seleccione estatus',	loadMsg: null	})	);
	}

	var catalogoEstatusAsignar_A = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea03ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEstatusAsignar_A'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesaCatalogoEstatusAsignar,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoEstatusAsignar_B = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea03ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEstatusAsignar_B'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesaCatalogoEstatusAsignar,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoEstatusAsignar_C = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [ ['0',' -- No aplica -- '] ]
	});

	var inhabilData = new Ext.data.JsonStore({
		root:'registros',	url:'24linea03ext.data.jsp',	baseParams:{informacion:'ConsultaInhabil'}, totalProperty:'total',	messageProperty:'msg',	autoLoad:false,
		fields: [
			{name: 'FOLIO'},
			{name: 'TIPOCREDITO'},
			{name: 'TIPO_SOL'},
			{name: 'NEPO'},
			{name: 'EPO'},
			{name: 'NPYME'},
			{name: 'PYME'},
			{name: 'FOLIO_REL'},
			{name: 'FECHA_SOL',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_AUTO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'MONEDA'},
			{name: 'ESTATUS'},
			{name: 'PLAZO'},
			{name: 'MONTO_TOTAL', type: 'float'},
			{name: 'SALDO_TOTAL', type: 'float'},
			{name: 'IC_MONEDA'},
			{name: 'IC_TIPOCREDITO'}
			
		],
		listeners:{
			load: procesarInhabilData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarInhabilData(null, null, null);
				}
			}
		}
	});

	var confirmaData = new Ext.data.JsonStore({
		fields: [
			{name:'FOLIO'},
			{name:'TIPOCREDITO'},
			{name:'TIPO_SOL'},
			{name:'NUMEPO'},
			{name:'EPO'},
			{name:'NUMPYME'},
			{name:'PYME'},
			{name:'FOLIOSOL_RELAC'},
			{name: 'FECHA_SOL',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_AUTO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'},
			{name:'MONEDA'},
			{name:'ESTATUS'},
			{name:'CAUSAS'},
			{name:'PLAZO'},
			{name:'BANCO_SERV'},
			{name:'BANCO_EPO'},
			{name:'BANCO_IF'},
			{name:'MONTO_AUTO'},
			{name:'MONTO_SOL'},
			{name:'IC_MONEDA'},
			{name:'FECHA_HOY'},
			{name:'IG_CUENTA_BANCARIA'},
			{name:'CG_USUARIO_CAMBIO'},
			{name:'IC_TIPOCREDITO'}
			
		],
		totalProperty : 'total',	data:	[{	'FOLIO':''}],	autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData = new Ext.data.JsonStore({
		root:'registros',	url:'24linea03ext.data.jsp',	baseParams:{informacion: 'Consulta'	},	totalProperty : 'total',	messageProperty: 'msg',	autoLoad: false,
		fields: [
			{name: 'FOLIO'},
			{name: 'TIPOCREDITO'},
			{name: 'TIPO_SOL'},
			{name: 'NEPO'},
			{name: 'EPO'},
			{name: 'NPYME'},
			{name: 'PYME'},
			{name: 'LINEAPADRE'},
			{name: 'FECHASOLIC',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_AUTO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'MONEDA'},
			{name: 'CAUSAS'},
			{name: 'PLAZO'},
			{name: 'CD_NOMBRE'},
			{name: 'CD_DESCRIPCION'},
			{name: 'CUENTA_EPO'},
			{name: 'CUENTA_IF'},
			{name: 'FN_MONTO_AUTORIZADO_TOTAL', type: 'float'},
			{name: 'SALDO_TOTAL', type: 'float'},
			{name: 'IC_MONEDA'},
			{name: 'IC_ESTATUS_LINEA'},
			{name: 'FECHA_CAMBIO'},
			{name: 'ESTATUS_ASIGNA'},
			{name: 'IG_CUENTA_BANCARIA'},
			{name: 'CG_USUARIO_CAMBIO'},
			{name: 'IC_TIPOCREDITO'}
			
		],
		listeners:{
			beforeLoad:	{fn:	function(store, options){
										Ext.apply(options.params, {
											tipo_credito		:formulario.tipo_credito,
											tipo_solic			:formulario.tipo_solic,
											ic_epo				:formulario.ic_epo,
											ic_pyme				:formulario.ic_pyme,
											ic_estatus_linea	:formulario.ic_estatus_linea,
											dc_fecha_solic_de	:formulario.dc_fecha_solic_de,
											dc_fecha_solic_a	:formulario.dc_fecha_solic_a,
											dc_fecha_auto_de	:formulario.dc_fecha_auto_de,
											dc_fecha_auto_a	:formulario.dc_fecha_auto_a
										});
									}},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var resumenTotConfirmaData = new Ext.data.JsonStore({
		fields: [{name: 'NOMMONEDA'},	{name: 'TOTAL_REGISTROS'},	{name: 'TOTAL_MONTO_DOCUMENTOS'}],
		data:	[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_DOCUMENTOS':''}],
		totalProperty : 'total',
		autoLoad: true,
		listeners: {	exception: NE.util.mostrarDataProxyError}
	});

	var gridTotConfirma = {
		xtype: 'grid',	store: resumenTotConfirmaData,	id: 'gridTotConfirma',	hidden:true, frame: false, width: 940,	height: 80,
		columns: [
			{header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 450, resizable:false,menuDisabled:true},
			{header: 'Registros',dataIndex: 'TOTAL_REGISTROS',	width: 240,	align: 'right',resizable:false,menuDisabled:true},
			{header: 'Monto Documentos',dataIndex: 'TOTAL_MONTO_DOCUMENTOS',width: 240,	align: 'right',resizable:false,menuDisabled:true}
		]
	};

	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24linea03ext.data.jsp',
		baseParams: {	informacion: 'ResumenTotales'	},
		fields: [
			{name: 'IC_MONEDA'},
			{name: 'CD_NOMBRE'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float'},
			{name: 'SALDO_TOTAL', type: 'float'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var gridInhabil = new Ext.grid.GridPanel({
		id:'gridInhabil',store: inhabilData,	stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
		columns:[
			{
				header:'Folio de solicitud',	tooltip:'Folio de solicitud',	dataIndex:'FOLIO',	sortable:false,	width:120,	align:'center', hideable:false
			},{
				header:'Tipo de cr�dito',	tooltip:'Tipo de cr�dito',	dataIndex:'TIPOCREDITO',	sortable:true,	resizable:true,	width:150,	align:'center', hideable:false
			},{
				header:'Tipo de solicitud',	tooltip:'Tipo de solicitud',	dataIndex:'TIPO_SOL',	sortable:true,	resizable:true,	width:150,	align:'center'
			},{
				header:'N�m. EPO',	tooltip:'N�m. EPO',	dataIndex:'NEPO',	sortable:true,	resizable:true,	width:100,	align:'center'
			},{
				header:'EPO',	tooltip:'EPO',	dataIndex:'EPO',	sortable:true,	resizable:true,	width:130,	align:'center', 
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'N�m. Distribuidor',	tooltip:'N�m. Distribuidor',	dataIndex:'NPYME',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header: 'Distribuidor',	tooltip: 'Distribuidor',	dataIndex:'PYME',	sortable:true,	resizable:true,	width:200,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Folio de solicitud relacionada',	tooltip:'Folio de solicitud relacionada',	dataIndex:'FOLIO_REL',	sortable:false,	width:120,	align:'center'
			},{
				header:'Fecha Solicitud', tooltip:'Fecha Solicitud',	dataIndex : 'FECHA_SOL',	sortable : true, width : 100, align: 'center',renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha autorizaci�n', tooltip:'Fecha autorizaci�n',	dataIndex:'FECHA_AUTO',
				sortable:true, width:110, align:'center', hidden:false,	renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Estatus',	tooltip:'Estatus',	dataIndex:'ESTATUS',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Plazo',	tooltip:'Plazo',	dataIndex:'PLAZO',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header : 'Fecha de vencimiento', tooltip: 'Fecha de vencimiento',	dataIndex:'FECHA_VENC',
				sortable:true, width:120, align:'center', hidden:false,	renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Monto autorizado', tooltip: 'Monto autorizado',	dataIndex : 'MONTO_TOTAL',
				sortable : true, width : 120, align: 'right', renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), hidden:false, hideable:false
			},{
				header : 'Saldo disponible', tooltip: 'Saldo disponible',	dataIndex : 'SALDO_TOTAL',
				sortable : true, width : 120, align: 'right', renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), hidden:false, hideable:false
			}
		],
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'tbspacer', width: 5
				},{
					xtype:'button',	text:'Generar Archivo',	id:'btnGenerarArchivoIn', iconCls:'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24linea03ext.data.jsp',
							params:	{informacion:"ArchivoCSV_inhabil"},
							callback: procesarSuccessFailureGenerarArchivoIn
						});
					}
				},{
					xtype: 'button',	text: 'Bajar Archivo',	id: 'btnBajarArchivoIn',	hidden: true
				},{
					xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5
				},{
					xtype:'button',	text:'Generar PDF',	id:'btnGenerarPDFIn',	iconCls:'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24linea03ext.data.jsp',
							params: {	informacion:"ArchivoPDF_inhabil"	},
							callback: procesarSuccessFailureGenerarPDFIn
						});
					}
				},{
					xtype:'button',	text:'Bajar PDF',	id: 'btnBajarPDFIn',hidden: true
				},{
					xtype: 'tbspacer', width: 5},{xtype: 'tbspacer', width: 5
				},'-',{
					xtype:'button',	text:'Cerrar',	id: 'btnCerrar',
					handler: function(boton){
									Ext.getCmp('winInhabil').hide();
								}
				},'-'
			]
		}
	});

	var gridTotales = {
		xtype:'grid',	store:resumenTotalesData,	id:'gridTotales',	hidden:true,	width:940,	height:100,	title: 'Totales', view:new Ext.grid.GridView({forceFit:true}),
		columns: [
			{
				header: 'Totales',	dataIndex: 'CD_NOMBRE',	align: 'left',	width: 250, menuDisabled:true
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 200,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'), menuDisabled:true
			},{
				header: 'Monto Autorizado',	dataIndex: 'TOTAL_MONTO',	width: 250,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00'), menuDisabled:true
			},{
				header: 'Saldo disponible',	dataIndex: 'SALDO_TOTAL',	width: 250,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00'), menuDisabled:true
			}
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	var gridConfirma = new Ext.grid.GridPanel({
		id:'gridConfirma',store: confirmaData,	stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,	hidden:true, header:true,
		columns:[
			{
				header:'N�m. EPO',	tooltip:'N�m. EPO',	dataIndex:'NUMEPO',	sortable:true,	resizable:true,	width:100,	align:'center', hideable:false
			},{
				header:'EPO',	tooltip:'EPO',	dataIndex:'EPO',	sortable:true,	resizable:true,	width:130,	align:'center', 
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Folio de solicitud',	tooltip:'Folio de solicitud',	dataIndex:'FOLIO',	sortable:false,	width:120,	align:'center'
			},{
				header:'Tipo de cr�dito',	tooltip:'Tipo de cr�dito',	dataIndex:'TIPOCREDITO',	sortable:true,	resizable:true,	width:150,	align:'center'
			},{
				header : 'Monto autorizado', tooltip: 'Monto autorizado',	dataIndex : 'MONTO_AUTO',
				sortable : true, width : 120, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden:false, hideable:false
			},{
				header : 'Saldo disponible', tooltip: 'Saldo disponible',	dataIndex : 'MONTO_SOL',
				sortable : true, width : 120, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden:false, hideable:false
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Fecha autorizaci�n', tooltip:'Fecha autorizaci�n',	dataIndex:'FECHA_AUTO',
				sortable:true, width:110, align:'center', hidden:false,	renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de vencimiento', tooltip: 'Fecha de vencimiento',	dataIndex:'FECHA_VENC',
				sortable:true, width:120, align:'center', hidden:false,	renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Estatus Nuevo',	tooltip:'Estatus Nuevo',	dataIndex:'ESTATUS',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Cuenta Bancaria PYME',	tooltip:'Cuenta Bancaria PYME',	dataIndex:'IG_CUENTA_BANCARIA',	sortable:false,	width:110,	align:'center', hidden:true, hideable:false
			},{
				header:'Usuario �ltima modificaci�n',	tooltip:'Usuario �ltima modificaci�n',	dataIndex:'CG_USUARIO_CAMBIO',	sortable:false,	width:200,	align:'center', hidden:true, hideable:false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Fecha de cambio de estatus', tooltip:'Fecha de cambio de estatus',	dataIndex:'FECHA_HOY',
				sortable:true, width:150, align:'center', hidden:false,	hideable:false
			}
		],
		bbar: {
			items: [
				'->',
				{
					xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5
				},{
					xtype:'button',	text:'Imprimir PDF',	id:'btnImprimeConfirma', iconCls:'icoPdf'
				},{
					xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5
				},{
					xtype:'button',	text:'Salir',	id: 'btnSalir',
					handler: function(boton){
									var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
									gridConfirma.hide();
									Ext.getCmp('gridTotConfirma').hide();
									consultaData.load({	params: Ext.apply({	start: cmpBarraPaginacion.cursor,	limit: cmpBarraPaginacion.pageSize	})	});
									fp.show();
								}
				},'-'
			]
		}
	});

	var colModel = new Ext.grid.ColumnModel({
		columns:[
			{
				header:'N�m. EPO',	tooltip:'N�m. EPO',	dataIndex:'NEPO',	sortable:true,	resizable:true,	width:100,	align:'center'
			},{
				header:'EPO',	tooltip:'EPO',	dataIndex:'EPO',	sortable:true,	resizable:true,	width:130,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Folio de solicitud',	tooltip:'Folio de solicitud',	dataIndex:'FOLIO',	sortable:false,	width:120,	align:'center'
			},{
				header:'Tipo de cr�dito',	tooltip:'Tipo de cr�dito',	dataIndex:'TIPOCREDITO',	sortable:true,	resizable:true,	width:150,	align:'center'
			},{
				header : 'Monto autorizado', tooltip: 'Monto autorizado',	dataIndex : 'FN_MONTO_AUTORIZADO_TOTAL',editor: new Ext.form.TextField({}),
				sortable : true, width : 120, align: 'right', hidden:false, hideable:false,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(strPerfil != "IF FACT RECURSO"){
									return Ext.util.Format.number(value,'$0,0.00');
								}else{
									return NE.util.colorCampoEdit(Ext.util.Format.number(value,'$0,0.00'), metadata,registro);
								}
								
				}
			},{
				header : 'Saldo disponible', tooltip: 'Saldo disponible',	dataIndex : 'SALDO_TOTAL',
				sortable : true, width : 120, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden:false, hideable:false
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Fecha autorizaci�n', tooltip:'Fecha autorizaci�n',	dataIndex:'FECHA_AUTO',
				sortable:true, width:110, align:'center', hidden:false,	renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de vencimiento', tooltip: 'Fecha de vencimiento',	dataIndex:'FECHA_VENC',	editor: new Ext.form.DateField({}),
				sortable:true, width:110, align:'center', hidden:false,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(strPerfil != "IF FACT RECURSO"){
									return Ext.util.Format.date(value,'d/m/Y');
								}else{
									return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'), metadata,registro);
								}
				}
			},{
				header:'Estatus actual',	tooltip:'Estatus actual',	dataIndex:'CD_DESCRIPCION',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Estatus por asignar',	tooltip:'Estatus por asignar',	dataIndex:'ESTATUS_ASIGNA',	sortable:true,	resizable:true,	width:130,	align:'center', editor: new Ext.form.TextField({}),
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								var valor = registro.get("IC_ESTATUS_LINEA");
								if(!Ext.isEmpty(valor)){
									if (valor == "7" || valor == "9" ){
										var dato = catalogoEstatusAsignar_A.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoEstatusAsignar_A.getAt(dato);
											value = reg.get('descripcion');
										}
									}else if (valor == "12"){
										var dato = catalogoEstatusAsignar_B.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoEstatusAsignar_B.getAt(dato);
											value = reg.get('descripcion');
										}
									}else{
										value = " -- No aplica -- ";
									}
								}else{
									value = " -- No aplica -- ";
								}
								if(Ext.isEmpty(value)){
									value = "Seleccione estatus";
								}
								return NE.util.colorCampoEdit(value,metadata,registro);
				}
			},{
				header:'Cuenta Bancaria PYME',	tooltip:'Cuenta Bancaria PYME',	dataIndex:'IG_CUENTA_BANCARIA',	sortable:false,	width:110,	align:'center', hidden:true, hideable:false
			},{
				header:'Usuario �ltima modificaci�n',	tooltip:'Usuario �ltima modificaci�n',	dataIndex:'CG_USUARIO_CAMBIO',	sortable:false,	width:200,	align:'center', hidden:true, hideable:false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Fecha de cambio de estatus', tooltip:'Fecha de cambio de estatus',	dataIndex:'FECHA_CAMBIO',
				sortable:true, width:120, align:'center', hidden:false,	hideable:false
			}
		],
		editors: {
			'A': new Ext.grid.GridEditor(new Ext.form.ComboBox({
						id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection:true,	triggerAction:'all',fixed:true,
						typeAhead: true,	minChars : 1,	emptyText:'Seleccione estatus',	store:catalogoEstatusAsignar_A, editable:true, value:'',
						listeners:{
										select:{ fn:function (comboField, record, index) {
															comboField.fireEvent('blur'); //Ext.getCmp('grid').getView().refresh();
														}
												 }
						}
					})),
			'B': new Ext.grid.GridEditor(new Ext.form.ComboBox({
						id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection:true,	triggerAction:'all',fixed:true,
						typeAhead: true,	minChars : 1,	emptyText:'Seleccione estatus',	store:catalogoEstatusAsignar_B,	editable:true, value:'',
						listeners:{ //focus:{ fn: function (comboField) { comboField.doQuery(comboField.allQuery, true); comboField.expand(); }},
										select:{ fn:function (comboField, record, index) {
															comboField.fireEvent('blur'); //Ext.getCmp('grid').getView().refresh();
														}
												 }
						}
					})),
			'C': new Ext.grid.GridEditor(new Ext.form.ComboBox({
						id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection : false,	triggerAction : 'all', lazyRender:true,
						typeAhead: true,	minChars : 1,	emptyText:'Seleccione estatus',	store:catalogoEstatusAsignar_C,	editable:false,
						listeners:{ //focus:{ fn: function (comboField) { comboField.doQuery(comboField.allQuery, true); comboField.expand(); }},
										select:{ fn:function (comboField, record, index) {
															comboField.fireEvent('blur'); //Ext.getCmp('grid').getView().refresh();
														}
												 }
						}
					}))
		},
		getCellEditor: function(colIndex, rowIndex) {
			var field = this.getDataIndex(colIndex);
			if (field == 'ESTATUS_ASIGNA') {
				var valor = grid.store.reader.jsonData.registros[rowIndex].IC_ESTATUS_LINEA;
				if (valor == "7" || valor == "9"){
					return this.editors['A'];
				}else if (valor == "12"){
					return this.editors['B'];
				}else{
					return this.editors['C'];
				}
			}
			return Ext.grid.ColumnModel.prototype.getCellEditor.call(this, colIndex, rowIndex);
		}
	});

	var grid = new Ext.grid.EditorGridPanel({
		id:'grid',store: consultaData,	columnLines:true, clicksToEdit:1, hidden:true,
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
		cm:colModel,
		listeners:{
			beforeedit: {
				fn: function(obj) {
					if(strPerfil != "IF FACT RECURSO"){
						if (obj.field === 'FECHA_VENC' || obj.field === 'FN_MONTO_AUTORIZADO_TOTAL') {
							obj.cancel=true;
						}
					}/*else{
						if (obj.field === 'FN_MONTO_AUTORIZADO_TOTAL') {
							editAmount = obj.value;
						}
					}*/
				}
			},
			afteredit:	function(obj){
				if(strPerfil === "IF FACT RECURSO"){
					if (obj.field === 'FN_MONTO_AUTORIZADO_TOTAL') {
						var opera='';
						var res=obj.originalValue-obj.value;
						var saldoTot = obj.record.data['SALDO_TOTAL'];
						if (obj.value <= obj.originalValue){
							opera='resta';
						}else{
							res = res*-1;
						}
						var valor = 0;
						if(opera === 'resta'){
							valor = saldoTot - res;
						}else{
							valor = saldoTot + res;
						}
						if (valor < 0){
							Ext.Msg.alert('Mensaje','Operaci�n no v�lida',function(){obj.record.reject();});
							return;
						}else{
							obj.record.data['SALDO_TOTAL'] = valor;
						}
						//obj.record.commit();
						grid.getView().refresh();
					}
				}
			}
		},
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',{xtype: 'tbspacer', width: 5},
				{
					xtype:'button',	text:'Totales',	id:'btnTotales',	hidden:false,
					handler: function(boton, evento) {
						resumenTotalesData.load();
						var totalesCmp = Ext.getCmp('gridTotales');
						if (!totalesCmp.isVisible()) {
							totalesCmp.show();
							totalesCmp.el.dom.scrollIntoView();
						}
					}
				},{
					xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5
				},{
					xtype:'button',	text:'Confirmar', id:'btnConfirmar', iconCls:'correcto',
					handler:	function(btn){
									var modificados = consultaData.getModifiedRecords();
									var tot = consultaData.getTotalCount();
									var flag = false;
									if (modificados.length > 0) {
										var jsonData = consultaData.data.items;
										Ext.each(modificados, function(item,index,arrItem){
											if(strPerfil != "IF FACT RECURSO"){
												if (!Ext.isEmpty(item.data.ESTATUS_ASIGNA) && item.data.ESTATUS_ASIGNA != '0'){
													flag = true;
													return false;
												}
											}else{
												if (!Ext.isEmpty(item.data.FN_MONTO_AUTORIZADO_TOTAL) && item.data.FN_MONTO_AUTORIZADO_TOTAL != '0'){
													flag = true;
													return false;
												}
												if (!Ext.isEmpty(item.data.FECHA_VENC)){
													flag = true;
													return false;
												}
												if (!Ext.isEmpty(item.data.ESTATUS_ASIGNA) && item.data.ESTATUS_ASIGNA != '0'){
													flag = true;
													return false;
												}
											}
										});
									}else{
										if (tot == 1){
											Ext.Msg.alert(btn.text,'No ha realizado ningun cambio a la linea.');
										}else if (tot > 1){
											Ext.Msg.alert(btn.text,'No ha realizado ningun cambio a las lineas.');
										}
										return;
									}
									if (!flag){
										if (tot == 1){
											Ext.Msg.alert(btn.text,'No ha realizado ningun cambio a la l�nea.',
												function(){
													var gridEl = Ext.getCmp('grid').getGridEl();
													var col = Ext.getCmp('grid').getColumnModel().findColumnIndex('ESTATUS_ASIGNA')
													var rowEl = Ext.getCmp('grid').getView().getCell( 0, col);
													rowEl.scrollIntoView(gridEl,false);
													return;
												}
											);
										}else if (tot > 1){
											Ext.Msg.alert(btn.text,'No ha realizado ningun cambio a las l�neas.',
												function(){
													var gridEl = Ext.getCmp('grid').getGridEl();
													var col = Ext.getCmp('grid').getColumnModel().findColumnIndex('ESTATUS_ASIGNA')
													var rowEl = Ext.getCmp('grid').getView().getCell( 0, col);
													rowEl.scrollIntoView(gridEl,false);
													return;
												}
											);
										}
									}else{
										btn.setIconClass('loading-indicator');
										var aFolio		= [];
										var aEstatus	= [];
										var aTipoCred	= [];
										var aMontos		= [];
										var aSaldoTot	= [];
										var aFechaVence= [];
										
										if(strPerfil != "IF FACT RECURSO"){
											Ext.each(modificados, function(item,index,arrItem){
												if (!Ext.isEmpty(item.data.ESTATUS_ASIGNA)){
													aFolio.push(item.data.FOLIO);
													aEstatus.push(item.data.ESTATUS_ASIGNA);
													aTipoCred.push(item.data.IC_TIPOCREDITO);
												}
											});
										}else{
											Ext.each(modificados, function(item,index,arrItem){
												if (	!Ext.isEmpty(item.data.FN_MONTO_AUTORIZADO_TOTAL)	
													||	!Ext.isEmpty(item.data.FECHA_VENC)	
													||	!Ext.isEmpty(item.data.ESTATUS_ASIGNA) ){

													aFolio.push(item.data.FOLIO);
													aTipoCred.push(item.data.IC_TIPOCREDITO);
													
													if (	!Ext.isEmpty(item.data.FN_MONTO_AUTORIZADO_TOTAL) ){
														aMontos.push(item.data.FN_MONTO_AUTORIZADO_TOTAL);
													}else{
														aMontos.push("");
													}
													if (	!Ext.isEmpty(item.data.SALDO_TOTAL) ){
														aSaldoTot.push(item.data.SALDO_TOTAL);
													}else{
														aSaldoTot.push("");
													}
													if(	!Ext.isEmpty(item.data.FECHA_VENC)	){
														aFechaVence.push( Ext.util.Format.date(item.data.FECHA_VENC,'d/m/Y') );
													}else{
														aFechaVence.push("");
													}
													if(	!Ext.isEmpty(item.data.ESTATUS_ASIGNA)	){
														aEstatus.push(item.data.ESTATUS_ASIGNA);
													}else{
														aEstatus.push("");
													}
												}
											});
										}

										pnl.el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: '24linea03ext.data.jsp',
											params:	{	informacion:	"Confirmar",
															folioSolic:		aFolio,
															estatus:			aEstatus,
															tipoCredito:	aTipoCred,
															montoAut:		aMontos,
															saldoTot:		aSaldoTot,
															fechaVence:		aFechaVence
														},
											callback: procesarConfirmar
										});
									}
								}
				},{
					xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5
				},{
					xtype:'button',	text:'Generar Archivo',	id:'btnGenerarArchivo', iconCls:'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24linea03ext.data.jsp',
							params:	Ext.apply(formulario,{informacion:"ArchivoCSV"}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},{
					xtype: 'button',	text: 'Bajar Archivo',	id: 'btnBajarArchivo',	hidden: true
				},{
					xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5
				},{
					xtype:'button',	text:'Generar Todo', tooltip: 'Imprime los registros en formato PDF.', id:'btnGenerarPDF',	iconCls:'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '24linea03ext.data.jsp',
							params: Ext.apply(formulario,{
								informacion:"ArchivoPDF",
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				}/*,{
					xtype:'button',	text:'Bajar PDF',	id: 'btnBajarPDF',hidden: true
				},'-'*/
			]
		}
	});

	var elementosForma = [
		{
			xtype: 'combo',
			id: 	'_tipo_credito',
			name: 'tipo_credito',
			hiddenName : 'tipo_credito',
			fieldLabel: 'Tipo de cr�dito',
			emptyText: 'Seleccione tipo de credito',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			allowBlank:false,
			lazyRender:true,
			typeAhead: true,
			minChars : 1,
			value:"",
			store: tipoCreditoData,
			listeners:{
				'select':function(cbo){
								grid.hide();
								Ext.getCmp('gridTotales').hide();
								var comboEpo = Ext.getCmp('_ic_epo');
								var comboStat = Ext.getCmp('_ic_estatus_linea');
								var comboPyme = Ext.getCmp('_ic_pyme');
								comboEpo.setVisible(false);
								comboStat.setVisible(false);
								comboPyme.setVisible(false);
								Ext.getCmp('fechaSol').setVisible(false);
								if( !Ext.isEmpty(cbo.getValue()) ){
									var comboEpo = Ext.getCmp('_ic_epo');
									comboEpo.setValue('');
									comboEpo.store.removeAll();
									comboEpo.store.reload({	params: {tipo_credito: cbo.getValue()}	});
									comboEpo.setVisible(true);

									if(strPerfil != "IF FACT RECURSO"){
										comboStat.setValue('');
										comboStat.store.removeAll();
										comboStat.store.reload({	params: {tipo_credito: cbo.getValue()}	});
										comboStat.setVisible(true);
									}

									if( cbo.getValue() === "C" || cbo.getValue() === "A" || cbo.getValue() === "F" ) {
										if( !Ext.isEmpty(comboEpo.getValue()) ) {
											comboPyme.setValue('');
											comboPyme.store.removeAll();
											comboPyme.store.reload({	params: {epo: comboEpo.getValue()}	});
											comboPyme.setVisible(true);
										}
										if(strPerfil != "IF FACT RECURSO"){
											Ext.getCmp('fechaSol').setVisible(true);
										}
									}
								}
							}
			}
		},{
			xtype: 'combo',
			id:	'_tipo_solic',
			name: 'tipo_solic',
			hiddenName : 'tipo_solic',
			emptyText: 'Seleccione Tipo de solicitud',
			fieldLabel: 'Tipo de solicitud',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			lazyRender:true,
			typeAhead: true,
			minChars : 1,
			store: tipoSolData
		},{
			xtype: 'combo',
			id:	'_ic_epo',
			name: 'ic_epo',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO',
			emptyText: 'Seleccione EPO. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1, hidden:true,
			store: catalogoEpoDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				'select':function(cbo){
								grid.hide();
								Ext.getCmp('gridTotales').hide();
								var comboCred = Ext.getCmp('_tipo_credito');
								var comboPyme = Ext.getCmp('_ic_pyme');
								if( ((comboCred.getValue() == "C" || comboCred.getValue() == "A") && !Ext.isEmpty(cbo.getValue())) || comboCred.getValue() == "F" ) {
									comboPyme.setValue('');
									comboPyme.store.removeAll();
									comboPyme.store.reload({	params: {epo: cbo.getValue()}	});
									comboPyme.setVisible(true);
								}else{
									comboPyme.setVisible(false);
								}
							}
			}
		},{
			xtype: 'combo',
			id:	'_ic_pyme',
			name: 'ic_pyme',
			hiddenName : 'ic_pyme',
			fieldLabel: (strPerfil === "IF FACT RECURSO")?'Proveedor':'Distribuidor',
			emptyText: 'Seleccionar Distribuidor. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			minChars : 1,
			store: catalogoPymeDistData,
			listeners:{
				'select':function(cbo){
								grid.hide();
								Ext.getCmp('gridTotales').hide();
							}
			}
		},{
			xtype: 'combo',
			id:	'_ic_estatus_linea',
			name: 'ic_estatus_linea',
			hiddenName : 'ic_estatus_linea',
			fieldLabel: 'Estatus',
			emptyText: 'Seleccione',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			hidden:true,
			typeAhead:true,
			minChars : 1,
			store: catalogoEstatusData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				'select':function(cbo){
								grid.hide();
								Ext.getCmp('gridTotales').hide();
							}
			}
		},{
			xtype: 'compositefield',	id:	'fechaSol',	fieldLabel: 'Fecha de solicitud',	combineErrors: false,	msgTarget: 'side', hidden:true,
			items: [
				{
					xtype: 'datefield',
					name: 'dc_fecha_solic_de',
					id: '_dc_fecha_solic_de',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: '_dc_fecha_solic_a',
					margins: '0 20 0 0'
				},{
					xtype: 'displayfield',	value: 'al',	width: 24
				},{
					xtype: 'datefield',
					name: 'dc_fecha_solic_a',
					id: '_dc_fecha_solic_a',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: '_dc_fecha_solic_de',
					margins: '0 20 0 0'
				}
			]
		},{
			xtype: 'compositefield',	fieldLabel: 'Fecha de autorizaci�n',	combineErrors: false,	msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'dc_fecha_auto_de',
					id: '_dc_fecha_auto_de',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: '_dc_fecha_auto_a',
					margins: '0 20 0 0'
				},{
					xtype: 'displayfield',	value: 'al',	width: 24
				},{
					xtype: 'datefield',
					name: 'dc_fecha_auto_a',
					id: '_dc_fecha_auto_a',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: '_dc_fecha_auto_de',
					margins: '0 20 0 0'
				}
			]
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title:'<div align="center">Criterios de B�squeda</div>',
		width: 500,
		frame: true,
		collapsible: true,
		titleCollapse: true,
		labelWidth:120,
		labelAlign:'right',
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',	id: 'btnConsultar',	iconCls: 'icoBuscar',	formBind:true,
				handler: function(boton, evento) {
								if (strPerfil === "IF FACT RECURSO"){
									if ( Ext.isEmpty(Ext.getCmp('_tipo_credito').getValue()) ){
										Ext.getCmp('_tipo_credito').markInvalid("Seleccione un tipo de cr�dito");
										return;
									}
								}

								var fechaSolMin = Ext.getCmp('_dc_fecha_solic_de');
								var fechaSolMax = Ext.getCmp('_dc_fecha_solic_a');
								var fechaAutMin = Ext.getCmp('_dc_fecha_auto_de');
								var fechaAutMax = Ext.getCmp('_dc_fecha_auto_a');

								if (!Ext.isEmpty(fechaSolMin.getValue()) || !Ext.isEmpty(fechaSolMax.getValue()) ) {
									if(Ext.isEmpty(fechaSolMin.getValue()))	{
										fechaSolMin.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
										fechaSolMin.focus();
										return;
									}else if (Ext.isEmpty(fechaSolMax.getValue())){
										fechaSolMax.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
										fechaSolMax.focus();
										return;
									}
								}

								if (!Ext.isEmpty(fechaAutMin.getValue()) || !Ext.isEmpty(fechaAutMax.getValue()) ) {
									if(Ext.isEmpty(fechaAutMin.getValue()))	{
										fechaAutMin.markInvalid('Debe capturar ambas fechas de autorizaci�n o dejarlas en blanco');
										fechaAutMin.focus();
										return;
									}else if (Ext.isEmpty(fechaAutMax.getValue())){
										fechaAutMax.markInvalid('Debe capturar ambas fechas de autorizaci�n o dejarlas en blanco');
										fechaAutMax.focus();
										return;
									}
								}

								grid.hide();
								Ext.getCmp('gridTotales').hide();
								formulario = fp.getForm().getValues();
								catalogoEstatusAsignar_A.load();
								catalogoEstatusAsignar_B.load();
								fp.el.mask('Enviando...', 'x-mask-loading');
								consultaData.load({
									params: Ext.apply(/*fp.getForm().getValues(),*/{
										operacion: 'Generar', //Generar datos para la consulta
										start: 0,
										limit: 15
									})
								});
							}
			},{
				text: 'Limpiar',	id:'btnLimpiar',	iconCls:'icoLimpiar',	handler: function() {location.reload();}
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid,	gridTotales,	NE.util.getEspaciador(10),
			gridConfirma,	gridTotConfirma,	NE.util.getEspaciador(10)
		]
	});

	if (strPerfil === "IF FACT RECURSO"){
		Ext.getCmp('_tipo_solic').setVisible(false);
	}

	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url : '24linea03ext.data.jsp',
		params:	{informacion: "valoresIniciales"},
		callback:procesaValoresIniciales
	});

});