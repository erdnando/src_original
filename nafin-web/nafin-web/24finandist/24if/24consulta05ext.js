Ext.onReady(function(){



function procesarConsultaTotal(store,arrRegistros,opts) {
			
			if(arrRegistros!=null){
			var el = gridTotales.getGridEl();
			if(store.getTotalCount()>0){
				if(arrRegistros==''){
					consultaTotales.load();
				}
				gridTotales.el.unmask();
			}else{		
					gridTotales.el.mask('No se cargaron los totales', 'x-mask');
				}
			}
		}
		
		
	function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}		

var procesarEstatus= function(){
	Ext.getCmp('estatus').setValue('11');
}
var accionBotones= function(){
	//Ext.getCmp('btnBajarPDF').hide();
	//Ext.getCmp('btnBajarArchivoTodas').hide();
	Ext.getCmp('btnGenerarArchivoTodas').enable();
	Ext.getCmp('btnGenerarPDF').enable();
	Ext.getCmp('btnTotales').enable();
}
var accionBotonesDisabled= function(){
	Ext.getCmp('btnTotales').disable();
	Ext.getCmp('btnGenerarArchivoTodas').disable();
	Ext.getCmp('btnGenerarPDF').disable();
}
		
		var procesarConsultaData = function(store,arrRegistros,opts){
		accionBotones();
			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					accionBotonesDisabled();
				}
			}
		}

var procesarSuccessFailureGenerarPDFPag =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFpag');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFpag');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
/*
var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

var procesarSuccessFailureGenerarArchivoTodas =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarArchivoTodas');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarArchivoTodas');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

	var procesarSuccessFailureGenerarArchivoTodas =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivoTodas').enable();
		Ext.getCmp('btnGenerarArchivoTodas').setIconClass('icoXls');
	}

	var procesarSuccessFailureGenerarArchivoVar =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarVar');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarVar');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivoFijo =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarFijo');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarFijo');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
//----------------Stores--------------------------

var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta05ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
	var catalogoTipoCobroData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta05ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTipoCobroDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});
	
	var catalogoEstatus = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta05ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEstatus'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,
		beforeload: NE.util.initMensajeCargaCombo,
		load: procesarEstatus
		}
	});

var catalogoDist = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta05ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoDist'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError
		}
  });
  
var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '24consulta05ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'NUM_CREDITO'},
				{name: 'EPO'},
				{name: 'DISTRIBUIDOR'},
				{name: 'TIPO_CREDITO'},
				{name: 'RESPONSABLE_INTERES'},
				{name: 'TIPO_COBRANZA'},
				{name: 'FECHA_OPERACION'},
				{name: 'TIPO_MONEDA'},
				{name: 'MONTO'},
				{name: 'REF_TASA'},
				{name: 'TASA_INTERES'},
				{name: 'PLAZO'},
				{name: 'FCH_VENCIMIENTO'},
				{name: 'MONTO_INTERES'},
				{name: 'TIPO_COBRO_INTE'},
				{name: 'ESTATUS_SOLI'},
				{name: 'FECHA_PAGO'},
				{name: 'NUM_PAGO'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
						}	},
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var consultaTotales = new Ext.data.JsonStore({
	root: 'registros',
	url: '24consulta05ext.data.jsp',
	baseParams: {
		informacion: 'Totales'
	},
	fields: [
				{name: 'CD_NOMBRE'},
				{name: 'REGISTROS'},
				{name: 'SUMA_RECIBIR'},
				{name: 'SUMA_INTERES'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaTotal,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaTotal(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});



var catalogoCredito = new Ext.data.ArrayStore({
        fields: [
            'myId',
            'displayText'
        ],
        data : [	['D','Modalidad 1 (Riesgo Empresa de Primer Orden )'],	['C','Modalidad 2 (Riesgo Distribuidor) ']	]	
    });



//---------------Componentes------------------------------

var dt= Date();
var fecha=Ext.util.Format.date(dt,'d/m/Y');

	var elementosForma = [{
		xtype: 'panel',
		layout:'column',
		items:[{
			xtype: 'container',
			id:	'panelIzq',
			labelWidth:110,
			columnWidth:.5,
			width:'50%',
			layout: 'form',
			items: [{
				anchor: '95%',
				xtype: 'combo',
				editable:false,
				fieldLabel: 'Tipo de Cr�dito',
				displayField: 'displayText',
				valueField: 'myId',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				name:'HcdCredito',
				store: catalogoCredito,
				id: 'ctCredito',
				mode: 'local',
				hiddenName: 'HctCredito',
				hidden: false,
				emptyText: 'Seleccionar tipo de Cr�dito',
				listeners: {
					select: {
						fn: function(combo) {
							Ext.getCmp('icEpo').setValue();
							var cEpo = combo.getValue();
							var cDist = Ext.getCmp('icEpo');
							cDist.setValue('');
							cDist.store.removeAll();
							cDist.store.load({
								params: {
									ctCredito: combo.getValue()
								}
							});
						}
					}
				}
		},{
				anchor: '95%',
				xtype: 'combo',
				fieldLabel: 'Nombre de la EPO',
				emptyText: 'Seleccionar una EPO',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo,
				//tpl: NE.util.templateMensajeCargaCombo,
				name:'HicEpo',
				id: 'icEpo',
				mode: 'local',
				hiddenName: 'HicEpo',
				forceSelection: true,
				listeners: {
					select: {
						fn: function(combo) {
							Ext.getCmp('icDist').setValue();
							var cEpo = combo.getValue();
							var cDist = Ext.getCmp('icDist');
							cDist.setValue('');
							cDist.store.removeAll();
							cDist.store.load({
								params: {
									icEpo: combo.getValue()
								}
							});
						}
					}
				},
				tpl:  '<tpl for=".">' +
						'<tpl if="!Ext.isEmpty(loadMsg)">'+
						'<div class="loading-indicator">{loadMsg}</div>'+
						'</tpl>'+
						'<tpl if="Ext.isEmpty(loadMsg)">'+
						'<div class="x-combo-list-item">' +
						'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
						'</div></tpl></tpl>'
		},{
			//distribuidor
			xtype: 'combo',
			anchor: '95%',
			emptyText: 'Seleccionar un Distribuidor',
			fieldLabel: 'Nombre del Distribuidor',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoDist,
			//tpl: NE.util.templateMensajeCargaCombo,
			name:'HicDist',
			id: 'icDist',
			mode: 'local',
			hiddenName: 'HicDist',
			//allowBlank: false,
			forceSelection: true,
			tpl:  '<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}</div>'+
				'</tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
				'</div></tpl></tpl>'
		},{
						xtype: 'numberfield',
							name: 'numCred',
							id: 'numCred',
							msgTarget: 'side',
							fieldLabel: 'N�mero de Documento Final',
							anchor: '95%',
							maxLength: 9
						},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de operaci�n',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaOper1',
									id: 'fechaOper1',
									value: fecha,
									allowBlank: true,
									startDay: 0,
									width: 130,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaOper2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaOper2',
									id: 'fechaOper2',
									value: fecha,
									allowBlank: true,
									startDay: 1,
									width: 130,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: 'fechaOper1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},{
							xtype: 'compositefield',
							fieldLabel: 'Monto',
							msgTarget: 'side',
							combineErrors: false, anchor: '98%',
							items: [
								{
									xtype: 'numberfield',
									name: 'monto1',
									id: 'monto1',
									allowBlank: true,
									maxLength: 9,
									width: 130,
									msgTarget: 'side',
									vtype: 'rangoValor',
									campoFinValor: 'monto2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'a',
									width: 24
								},
								{
									xtype: 'numberfield',
									name: 'monto2',
									id: 'monto2',
									allowBlank: true,
									maxLength: 9,
									width: 130,
									msgTarget: 'side',
									vtype: 'rangoValor',
									campoInicioValor: 'monto1',
									margins: '0 20 0 0'	  //necesario para mostrar el icono de error
								}
								
							]
						},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de vencimiento',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaVenc1',
									id: 'fechaVenc1',
							
									allowBlank: true,
									startDay: 0,
									width: 130,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaVenc2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaVenc2',
									id: 'fechaVenc2',
									
									allowBlank: true,
									startDay: 1,
									width: 130,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: 'fechaVenc1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						}
				]
			},
			{
				xtype: 'container',
				id:	'panelDer',
				columnWidth:.5,
					width:'50%',
				layout: 'form',
				items: [
						{
							anchor: '95%',
							xtype: 'combo',
							name: 'HtipoCoInt',
							id:	'tipoCoInt',
							fieldLabel: 'Tipo de cobro de inter�s',
							emptyText: 'Seleccione Tipo de cobro',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'HtipoCoInt',
							forceSelection : false,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoTipoCobroData,
							tpl : NE.util.templateMensajeCargaCombo
						},{
							anchor: '95%',
							xtype: 'combo',
							name: 'Hestatus',
							id:	'estatus',
							fieldLabel: 'Estatus',
							emptyText: 'Seleccione estatus',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'Hestatus',
							forceSelection : false,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoEstatus,
							tpl : NE.util.templateMensajeCargaCombo
						},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de pago',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaPago1',
									id: 'fechaPago1',
							
									allowBlank: true,
									startDay: 0,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaPago2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaPago2',
									id: 'fechaPago2',
									
									allowBlank: true,
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: 'fechaPago1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						}
					]
				}
				
			]
		}
	]
					


var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 940,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						
						if((verificaFechas('fechaOper1','fechaOper2')&&verificaFechas('fechaVenc1','fechaVenc2')&&verificaFechas('fechaPago1','fechaPago2')&&verificaFechas('monto1','monto2'))){
						
							gridTotales.hide();
							grid.show();
							consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar', //Generar datos para la consulta
									start: 0,
									limit: 15})
									});
						}
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({
		store: consultaTotales,
		title: 'Totales',
		id: 'gridTotales',
		hidden:	true,
		style: 'margin: 0 auto;',
		columns: [
			{
				header: 'Moneda',
				dataIndex: 'CD_NOMBRE',
				align: 'center',	width: 230
			},{
				header: 'Total Registros',
				dataIndex: 'REGISTROS',
				width: 230,	align: 'right'
			},{
				header: 'Total Monto',
				dataIndex: 'SUMA_RECIBIR',
				width: 230,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header: 'Total Monto de Inter�s',
				dataIndex: 'SUMA_INTERES',
				width: 230,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			}
			//Tasa de inter�s
		],
		width: 940,
		height: 100,
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	});
	
	var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						
						header:'N�mero Documento Final',
						tooltip: 'N�mero Documento Final',
						sortable: true,
						dataIndex: 'NUM_CREDITO',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'EPO',
						tooltip: 'EPO',
						sortable: true,
						dataIndex: 'EPO',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Distribuidor',
						tooltip: 'Distribuidor',
						sortable: true,
						dataIndex: 'DISTRIBUIDOR',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Tipo de Cr�dito',
						tooltip: 'Tipo de Cr�dito',
						sortable: true,
						dataIndex: 'TIPO_CREDITO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Resp. pago de inter�s',
						tooltip: 'Resp. pago de inter�s',
						sortable: true,
						dataIndex: 'RESPONSABLE_INTERES',
						width: 130,
						align: 'center'
						
						},
						{
						align:'center',
						header: 'Tipo de cobranza',
						tooltip: 'Tipo de cobranza',
						sortable: true,
						dataIndex: 'TIPO_COBRANZA',
						width: 130
						},{
						
						header:'Fecha de operaci�n',
						tooltip: 'Fecha de operaci�n',
						sortable: true,
						dataIndex: 'FECHA_OPERACION',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'TIPO_MONEDA',
						width: 150,
						align: 'center'
						},
						{
						header: 'Monto',
						tooltip: 'Monto',
						sortable: true,
						dataIndex: 'MONTO',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						header: 'Referencia tasa',
						tooltip: 'Referencia tasa',
						sortable: true,
						dataIndex: 'REF_TASA',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Tasa de inter�s',
						tooltip: 'Tasa de inter�s',
						sortable: true,
						dataIndex: 'TASA_INTERES',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('0.00%')
						
						},
						{
						align:'center',
						header: 'Plazo',
						tooltip: 'Plazo',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 130
						},
						{
						header: 'Fecha de vencimiento',
						tooltip: 'Fecha de vencimiento',
						sortable: true,
						dataIndex: 'FCH_VENCIMIENTO',
						width: 130,
						align: 'center'
						},
						{
						header: 'Monto inter�s',
						tooltip: 'Monto inter�s',
						sortable: true,
						dataIndex: 'MONTO_INTERES',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						header: 'Tipo de cobro inter�s',
						tooltip: 'Tipo de cobro inter�s',
						sortable: true,
						dataIndex: 'TIPO_COBRO_INTE',
						width: 150,
						align: 'center'
						},
						{
						header: 'Estatus',
						tooltip: 'Estatus',
						sortable: true,
						dataIndex: 'ESTATUS_SOLI',
						width: 150,
						align: 'center'
						},
						{
						header: 'N�m. pago',
						tooltip: 'N�m. pago',
						sortable: true,
						dataIndex: 'NUM_PAGO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Fecha de cambio de estatus',
						tooltip: 'Fecha de cambio de estatus',
						sortable: true,
						dataIndex: 'FECHA_PAGO',
						width: 150,
						align: 'center'
						}
						
						
				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					autoScroll:true,
				
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					items: ['->','-',
								{
										xtype: 'button',
										text: 'Totales',
										id: 'btnTotales',
										hidden: false,
										handler: function(boton, evento) {
												consultaTotales.load();
												gridTotales.show();
												gridTotales.el.mask('Enviando...', 'x-mask-loading');
										}
								},'-',
								{
									xtype:   'button',
									text:    'Generar Todo',
									tooltip: 'Imprime los registros en formato PDF.',
									iconCls: 'icoPdf',
									id: 'btnGenerarPDF',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
										Ext.Ajax.request({
											url: '24consulta05ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoPaginaPDF',
												start: cmpBarraPaginacion.cursor,
												limit: cmpBarraPaginacion.pageSize}),
											callback: procesarSuccessFailureGenerarPDF
										});
									}
								},'-',/*
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarPDF',
									hidden: true
								},*/
								{
									xtype: 'button',
									text: 'Generar Archivo',
									ooltip: 'Imprime los registros en formato CSV.',
									iconCls: 'icoXls',
									id: 'btnGenerarArchivoTodas',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '24consulta05ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV'}),
											callback: procesarSuccessFailureGenerarArchivoTodas
										});
									}
								},/*
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivoTodas',
									hidden: true
								},*/
								'-']
				}
		});

//----------------Principal------------------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [ 
	  fp,
	    NE.util.getEspaciador(30),
		grid,
		gridTotales
		 
		 ]
  });
		catalogoEstatus.load();
		catalogoTipoCobroData.load();
		
});