<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*, java.math.BigDecimal,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.distribuidores.*,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("valoresIniciales"))	{

}else if (informacion.equals("CatalogoEPODist") ) {

		String tipo_credito = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");

		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClaveIf(iNoCliente);
		if (tipo_credito.equals("D")){
			cat.setTipoCredito("D");
		}else if (tipo_credito.equals("C")){
			cat.setTipoCredito("C");
		}
		cat.setCdoctos("S");
		cat.setEstatusDocto("3,24");//MOD +(24)
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();

}if (informacion.equals("CatalogoDistribuidor")){

	String ic_epo = (request.getParameter("epo")==null)?"":request.getParameter("epo");
	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}

}else if (informacion.equals("CatalogoTipoCobroDist")){

	String rs_epo_tipo_cobro_int	=	"1,2";
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_cobro_interes");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_tipo_cobro_interes");
	cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoRechazar")){
	String estatus= (request.getParameter("estatus")==null)?"":request.getParameter("estatus");//MOD +(ALL)
	CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_estatus_docto");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_estatus_docto");
	  if(estatus.equals("3")) {
			cat.setValoresCondicionIn("2", Integer.class);//MOD +(24)
		}else{
			cat.setValoresCondicionIn("3", Integer.class);//MOD +(24)
		}
	
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta")){

	String		fechaHoy 		= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	
	String		tipoLinCre		= (request.getParameter("tipoLinCre")==null)?"":request.getParameter("tipoLinCre");
	String		fechaVenc1		= (request.getParameter("fechaVenc1")==null)?"":request.getParameter("fechaVenc1");
	String		fechaVenc2		= (request.getParameter("fechaVenc2")==null)?"":request.getParameter("fechaVenc2");
	String		epo				= (request.getParameter("epo")==null)?"":request.getParameter("epo");
	String		tipoCoInt		= (request.getParameter("tipoCoInt")==null)?"":request.getParameter("tipoCoInt");
	String		distrib			= (request.getParameter("distrib")==null)?"":request.getParameter("distrib");
	String		numCred			= (request.getParameter("numCred")==null)?"":request.getParameter("numCred");
	String		fechaOper1		= (request.getParameter("fechaOper1")==null)?fechaHoy:request.getParameter("fechaOper1");
	String		fechaOper2		= (request.getParameter("fechaOper2")==null)?fechaHoy:request.getParameter("fechaOper2");
	String		monto1			= (request.getParameter("monto1")==null)?"":request.getParameter("monto1");
	String		monto2			= (request.getParameter("monto2")==null)?"":request.getParameter("monto2");
	//String		estatusdocto	= (request.getParameter("estatusdocto")==null)?"20":request.getParameter("estatusdocto");
	JSONObject jsonObj = new JSONObject();
	int numRegistros   = 0;
	int numRegistrosMN = 0;
	int numRegistrosDL = 0;
	BigDecimal montoTotalMN = new BigDecimal("0.00");
	BigDecimal montoTotalDL = new BigDecimal("0.00");
	List NuevoReg = new ArrayList();
	List totalReg = new ArrayList();
	
	Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);
	
	Vector vecColumnas = null;
	Vector vecFilas = null;
	
	vecFilas = lineadm.getSolicitudes(iNoCliente,tipoLinCre,fechaVenc1,fechaVenc2,epo,tipoCoInt,distrib,numCred,fechaOper1,fechaOper2,monto1,monto2);
	
	for(int i=0;i<vecFilas.size();i++){
		HashMap hash = new HashMap();
		vecColumnas = (Vector)vecFilas.get(i);

		String ic_documento		= (String)vecColumnas.get(0);				
		String num_credito		= (String)vecColumnas.get(1);
		String razons_epo		= (String)vecColumnas.get(2);
		String razons_pyme		= (String)vecColumnas.get(3);
		String tipo_credito	= "";				
		if("D".equals((String)vecColumnas.get(4)))
			tipo_credito		= "Modalidad 1 (Riesgo Empresa de Primer Orden";
		else {
			if("C".equals((String)vecColumnas.get(4)))
				tipo_credito		= "Modalidad 2 (Riesgo Distribuidor)";
			else
				if("A".equals((String)vecColumnas.get(4)))
					tipo_credito		= "Ambas";
		}
		String responsable		= (String)vecColumnas.get(5);
		String tipo_cobranza	= (String)vecColumnas.get(6);
		String fecha_oper 		= (String)vecColumnas.get(7);
		String moneda_desc 		= (String)vecColumnas.get(8);
		String monto 			= (String)vecColumnas.get(21);
		String tipo_conv 		= (String)vecColumnas.get(10);
		String tipo_cambio 		= (String)vecColumnas.get(11);
		String monto_valuado 	= (String)vecColumnas.get(12);
		String referencia_tasa 	= (String)vecColumnas.get(22);
		String tasa_interes 	= (String)vecColumnas.get(14);
		String plazo 			= (String)vecColumnas.get(23);
		String fecha_venc 		= (String)vecColumnas.get(16);
		String monto_interes 	= (String)vecColumnas.get(17);
		String tipo_cobro_int 	= (String)vecColumnas.get(18);
		String estatus 			= (String)vecColumnas.get(19);
		String rs_ic_mon		= (String)vecColumnas.get(20);
		String ic_estatus		= (String)vecColumnas.get(24);
		if ("1".equals(rs_ic_mon)){
			tipo_cambio="";
			numRegistrosMN++;
			if (monto!=null) montoTotalMN = montoTotalMN.add(new BigDecimal(monto));
		}
		else{//IC_MONEDA=54
			numRegistrosDL++;
			if (monto!=null) montoTotalDL = montoTotalDL.add(new BigDecimal(monto));
		}
		numRegistros++;

		hash.put("IC_MONEDA",rs_ic_mon);
		hash.put("TIPO_CONVERSION",tipo_conv);
		hash.put("TIPO_CAMBIO",tipo_cambio);
		hash.put("TIPO_CREDITO",tipo_credito);
		hash.put("FECHA_SOLICITUD",fecha_oper);
		hash.put("NOMBRE_EPO",razons_epo);
		hash.put("NOMBRE_PYME",razons_pyme);
		hash.put("MONEDA",moneda_desc);
		hash.put("MONTO",monto);
		hash.put("REFERENCIA_TASA",referencia_tasa);
		hash.put("TASA_INTERES",tasa_interes);
		hash.put("PLAZO",plazo);
		hash.put("FECHA_VENCIMIENTO",fecha_venc);
		hash.put("MONTO_INTERES",monto_interes);
		hash.put("TIPO_INTERES",tipo_cobro_int);
		hash.put("IC_DOCUMENTO",ic_documento);
		hash.put("IC_DOCUMENTO",ic_documento);
		hash.put("FLAG_CAUSA","");
		hash.put("FLAG_COMBO",ic_estatus);//MOD +(ic_estatus) -("20")
		hash.put("IC_ESTATUS_DOCTO",ic_estatus);
		NuevoReg.add(hash);
	}
	jsonObj.put("regs", NuevoReg);
	HashMap hashTot = new HashMap();
	hashTot.put("numRegistros",Integer.toString(numRegistros));
	hashTot.put("numRegistrosMN",Integer.toString(numRegistrosMN));
	hashTot.put("numRegistrosDL",Integer.toString(numRegistrosDL));
	hashTot.put("montoTotalMN","$ "+Comunes.formatoDecimal(montoTotalMN,2));
	hashTot.put("montoTotalDL","$ "+Comunes.formatoDecimal(montoTotalDL,2));
	totalReg.add(hashTot);
	jsonObj.put("totales", totalReg);

	jsonObj.put("success", new Boolean(true));	
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("CambiaEstatus")){

	JSONObject jsonObj = new JSONObject();

	try{
		Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);
		
		String ic_documento[]	= request.getParameterValues("ic_documento");
		String estatus[]		= request.getParameterValues("estatusdocto");
		String causa[]			= request.getParameterValues("causa");

		lineadm.setDisDocumento(ic_documento,estatus,causa);
		jsonObj.put("success", new Boolean(true));

	}catch(Exception e){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error en el proceso. "+e);
	}

	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>

