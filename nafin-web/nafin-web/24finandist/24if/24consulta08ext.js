Ext.onReady(function() {

	var aviso = ['<b>NOTA:<\/b> Los registros que se muestran en el grid de consulta en color AZUL, son los documentos que fueron financiados a meses sin intereses.'];
   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

	var formulario = new Array();

	var procesarSuccessFailureInterfases =  function(opts, success, response) {
		var btnGenerar, btnBajar;
		if (opts.params.informacion == 'Variable'){
			btnGenerar = Ext.getCmp('btnExportarVar');
			btnBajar = Ext.getCmp('btnBajaVar');
			btnGenerar.setIconClass('icoXls');
		}else{
			btnGenerar = Ext.getCmp('btnExportarFijo');
			btnBajar = Ext.getCmp('btnBajaFijo');
			btnGenerar.setIconClass('icoTxt');
		}
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if (opts.params.informacion == 'Variable'){
				btnBajar.show();
				btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajar.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
			}else{
				var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			}
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
/*
	var procesarSuccessFailureGenerarArchivo = function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarArchivo = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}
		
	function mostrarArchivoPDF(opts, success, response) {
		Ext.getCmp('btnReporteOperPDF').enable();
		var btnImprimirPDF = Ext.getCmp('btnReporteOperPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function mostrarArchivoCSV(opts, success, response) {
		Ext.getCmp('btnReporteOperCSV').enable();
		var btnImprimirCSV = Ext.getCmp('btnReporteOperCSV');
		btnImprimirCSV.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
/*
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF, btnBajarPDF;
			if (opts.params.informacion == 'ArchivoXpaginaPDF'){
				btnGenerarPDF = Ext.getCmp('btnPorPagina');
				btnBajarPDF = Ext.getCmp('btnBajarPagina');
			}else{
				btnGenerarPDF = Ext.getCmp('btnImprimirPdf');
				btnBajarPDF = Ext.getCmp('btnBajarPdf');
			}
			btnGenerarPDF.setIconClass('icoPdf');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnImprimirPdf').enable();
		Ext.getCmp('btnImprimirPdf').setIconClass('icoPdf');
	}

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

		/*	var cadEpoCemex =	" Las operaciones que aparecen en esta pantalla le fueron notificadas a la " +
			" EMPRESA DE PRIMER ORDEN y al DISTRIBUIDOR, de conformidad con lo dispuesto en los  " +
			" art�culos 32 C del C�digo Fiscal o  2038 y 2041 de C�digo Civil Federa seg�n corresponda " +
			" para los efectos legales conducentes.";*/
			//var operaContrato = Ext.getDom('operaContrato').value;
			var cadEpo =	"Las operaciones que aparecen en esta pantalla le fueron notificadas a la EMPRESA DE PRIMER ORDEN y al CLIENTE o DISTRIBUIDOR, de conformidad con lo dispuesto "+
                        "en los art�culos 32 C del C�digo Fiscal o 2038 y 2041 del C�digo Civil Federal seg�n corresponda, para los efectos legales conducentes.<br/><br/>"+
                        "Por otra parte, a partir del 17 de octubre del 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que s� ha "+
                        "emitido o emitir� a su CLIENTE o DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.";


			if (success == true && Ext.util.JSON.decode(response.responseText).cadEpo == true) {
				Ext.getCmp('msgInicial').body.update(cadEpo);
			}/*else{
				Ext.getCmp('msgInicial').body.update(cadEpoCemex);
			}*/
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		if (arrRegistros != null) {

			Ext.getCmp('formAviso').show();

			if (!grid.isVisible()) {
				grid.show();
			}
			//Ext.getCmp('btnBajarPagina').hide();
			//Ext.getCmp('btnBajarPdf').hide();
			Ext.getCmp('btnBajarArchivo').hide();
			Ext.getCmp('btnBajaVar').hide();
			Ext.getCmp('btnBajaFijo').hide();

			//var btnPorPagina = Ext.getCmp("btnPorPagina");
			var btnImprimirPdf = Ext.getCmp("btnImprimirPdf");
			var btnGenerarArchivo = Ext.getCmp("btnGenerarArchivo");
			var btnExportarVar = Ext.getCmp("btnExportarVar");
			var btnExportarFijo = Ext.getCmp("btnExportarFijo");
			
			var btnReporteOperPDF = Ext.getCmp("btnReporteOperPDF");
			var btnReporteOperCSV = Ext.getCmp("btnReporteOperCSV");

			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				/*
				btnGenerarArchivo.setHandler(
					function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params:	{ic_estatus_docto:	opts.params.ic_estatus_docto},
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				);

				btnGenerarPDF.setHandler(
					function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {ic_estatus_docto:	opts.params.ic_estatus_docto},
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				);
				*/
				//btnPorPagina.enable();
				btnImprimirPdf.enable();
				btnGenerarArchivo.enable();
				btnExportarVar.enable();
				btnExportarFijo.enable();
				btnReporteOperPDF.enable();
			   btnReporteOperCSV.enable();
				el.unmask();
			} else {
				//btnPorPagina.disable();
				btnImprimirPdf.disable();
				btnGenerarArchivo.disable();
				btnExportarVar.disable();
				btnExportarFijo.disable();
				btnReporteOperPDF.disable();
			   btnReporteOperCSV.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta08ext.data.jsp',
		baseParams: {informacion: 'CatalogoEPODist'},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta08ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDistribuidor'	},
		totalProperty : 'total',                
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo
                }
	});

	var totalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO'},
			{name: 'TOTAL_MONTO_CREDITO'},
			{name: 'TOTAL_MONTO_VALUADO'}
		],
		data:[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''},
				{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''}],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData = new Ext.data.JsonStore({
		root:'registros',	url:'24consulta08ext.data.jsp',	baseParams:{informacion:'Consulta'},	totalProperty:'total',	messageProperty:'msg',	autoLoad:false,
		fields: [
			{name: 'IG_TIPO_PAGO'},
			{name: 'DISTRIBUIDOR'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_PUBLICACION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO',	type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO',type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESCUENTO',	type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_ESTATUS_DOCTO'},
			{name: 'IC_MONEDA'},
			{name: 'MONEDA_LINEA'},
			{name: 'EPO'},
			{name: 'IC_DOCUMENTO'},
			{name: 'DIF'},
			{name: 'REFERENCIA_INT'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'DF_FECHA_VENC_CREDITO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'TIPO_COBRO_INTERES'},
			{name: 'MONTO_INTERES',type: 'float'},
			{name: 'VALOR_TASA'},
			{name: 'MONTO_CREDITO',type: 'float'},
			{name: 'DF_FECHA_HORA',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'COMISION_APLICABLE'},
			{name: 'MONTO_DEPOSITAR'},
			{name: 'MONTO_COMISION'},
			{name: 'IC_ORDEN_ENVIADO'},
			{name: 'IC_OPERACION_CCACUSE'},
			{name: 'CODIGO_AUTORIZADO'},
			{name: 'FECHA_REGISTRO'},
			{name: 'CG_DESCRIPCION_BINS'},
			{name: 'NUMERO_TC'}
		],
		listeners: {
			beforeLoad:	{fn:	function(store, options){
										Ext.apply(options.params, {
											ic_epo		:formulario.ic_epo,
											ic_pyme		:formulario.ic_pyme,
											Txtfchini	:formulario.Txtfchini,
											Txtffin		:formulario.Txtffin,
											numeroOrden: formulario.numeroOrden
										});
									}},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Datos Documento Inicial', colspan: 19, align: 'center'},
				{header: 'Datos Documento Final', colspan: 8, align: 'center'}
			]
		]
	});

	var gridTotales = {
		xtype:'grid',	store:totalesData,	id:'gridTotales',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:940,	height:100,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 100,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),menuDisabled:true
			},{
				header: 'Monto Autorizado',dataIndex: 'TOTAL_MONTO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Valuado',	dataIndex: 'TOTAL_MONTO_VALUADO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Cr�dito',	dataIndex: 'TOTAL_MONTO_CREDITO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	};

	function cambiaEstilo(value, record){
		if (record.get('IG_TIPO_PAGO') == '1'){
			return '<span style="color:green;">' + value + '</span>';
		} else{
			return value;
		}
	}
	
	var grid = new Ext.grid.GridPanel({
		/**
		 INICIO: FODEA 09-2015. Si el pago es a meses sin intereses, el renglon se muestra en color azul.
		 El estilo se obtiene de un bloque de c�digo css agregado en el archivo nafin/24finandist/24if/24consulta08ext.jsp
		*/
		viewConfig: {
			autoFill: false,
			getRowClass: function (record, rowIndex, rowParams, store) {
				if(record.get('IG_TIPO_PAGO') == '2'){
					return 'user-msi';
				} 
			}
		},
		//FIN: FODEA 09-2015
		store: consultaData,	stripeRows: true,	loadMask:true,	height:400,	width:940,	frame:false, hidden:true, plugins:grupos,
		columns: [
			/*{
				header:'IG_TIPO_PAGO', dataIndex:'IG_TIPO_PAGO', width:200
			},*/{
				header:'EPO', tooltip:'EPO',	dataIndex:'EPO',	sortable:true,	resizable:true,	width:200,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Distribuidor', tooltip: 'Distribuidor',	dataIndex:'DISTRIBUIDOR',	sortable:true,	resizable:true,	width:200,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'N�mero de documento inicial', tooltip:'N�mero de documento inicial',	dataIndex:'IG_NUMERO_DOCTO',	sortable:true, width:120
			},{
				header:'N�m. acuse de carga', tooltip: 'N�mero acuse de carga',	dataIndex:'CC_ACUSE',	sortable:true,	width:120,	resizable:true
			},{
				header:'Fecha de Emisi�n', tooltip:'Fecha de Emisi�n',	dataIndex:'DF_FECHA_EMISION',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de publicaci�n', tooltip:'Fecha de publicaci�n',	dataIndex:'DF_FECHA_PUBLICACION',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de Vencimiento', tooltip:'Fecha de Vencimiento',	dataIndex:'DF_FECHA_VENC',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de operaci�n', tooltip:'Fecha de operaci�n',	dataIndex:'DF_FECHA_HORA',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Plazo docto.', tooltip:'Plazo docto.',	dataIndex:'IG_PLAZO_DOCTO',	sortable:true,	width:120, align:"center"
			},{
				header:'Moneda', tooltip: 'Moneda',	dataIndex:'MONEDA',	sortable:true,	width:130,align:"center"
			},{
				header:'Monto', tooltip:'Monto',	dataIndex:'FN_MONTO',	sortable:true,	width:120,	align:'right',	renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Plazo para descuento en d�as', tooltip:'Plazo para descuento en d�as',	dataIndex:'IG_PLAZO_DESCUENTO',	sortable:true,	width:120,align:"center"
			},{
				header:'% de Descuento',	tooltip: 'Porcentaje de Descuento', dataIndex:'FN_PORC_DESCUENTO',	sortable:true,	width:100, align:'right',
				renderer:Ext.util.Format.numberRenderer('0.00 %')
			},{
				header:'Monto % de descuento', tooltip: 'Monto % de descuento',	dataIndex:'MONTO_DESCUENTO',	sortable:true,	width:120,	align:'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											value = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
											return Ext.util.Format.number(value, '$ 0,0.00');
										}
			},{
				header:'Tipo conv.', tooltip: 'Tipo conv.',	dataIndex:'TIPO_CONVERSION',	sortable:true,	width:120,	align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}
											return value;
										}
			},{
				header:'Tipo cambio', tooltip:'Tipo cambio',	dataIndex:'TIPO_CAMBIO',	sortable:true,	width:120,	align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}else{
												value = Ext.util.Format.number(value, '$ 0,0.00');
											}
											return value;
										}
			},{
				header:'Monto valuado', tooltip: 'Monto valuado',	dataIndex:'',	sortable:true,	width:120,	align:"right",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}else{
												var montoDescontar = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
												var valor = (registro.get('FN_MONTO')-montoDescontar)*registro.get('TIPO_CAMBIO');
												value = Ext.util.Format.number(valor, '$ 0,0.00');
											}
											return value;
										}
			},{
				header:'Modalidad de plazo', tooltip: 'Modalidad de plazo',	dataIndex:'MODO_PLAZO',	sortable:true,	width:100,	align:"center"
			},{
				header:'Estatus', tooltip: 'Estatus',	dataIndex:'ESTATUS',	sortable:true,	width:130,	align:"center"
			},{
				header: 'N�mero de documento final', 
				tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO',
				sortable:true,
				width:120,
				resizable:true,
				align:"center"
			},{
				header:'Monto',
				tooltip: 'Monto',
				dataIndex:'MONTO_CREDITO',
				sortable:true,
				width:120,
				align:"right",
				hidden: false,
				renderer:Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header:'% Comisi�n Aplicable de Terceros',
				tooltip: '% Comisi�n Aplicable de Terceros',
				dataIndex:'COMISION_APLICABLE',  
				sortable:true,
				width:120,
				align:"center",
				hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32"  ){
							value = "N/A";
						}else{
							value = Ext.util.Format.number(value,'0.00%');
						}
						return value;
				}
			},{
				header:'Monto Comisi�n de Terceros (IVA Incluido)',
				tooltip: 'Monto Comisi�n de Terceros (IVA Incluido)',
				dataIndex:'MONTO_COMISION',
				sortable:true,
				width:120,
				align:"right",
				hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32"  ){
							value = "N/A";
						}else{
							value = Ext.util.Format.number(value,'$ 0,0.00');
						}
						return value;
				}
			},{
				header:'Monto a Depositar por Operaci�n',
				tooltip: 'Monto a Depositar por Operaci�n',
				dataIndex:'MONTO_DEPOSITAR',
				sortable:true,
				width:120,
				align:"right",
				hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32" ){
							value = "N/A";
						}else{
							value = Ext.util.Format.number(value,'$ 0,0.00');
							
						}
						return value;
				}
			},{
				header:'Plazo',
				tooltip: 'Plazo',
				dataIndex:'IG_PLAZO_CREDITO',
				sortable:true,
				width:120,
				align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						//INICIO: FODEA 09-2015
						if(registro.get('IG_TIPO_PAGO') == '2' ){
							value = value + ' (M)';
						}
						//FIN: FODEA 09-2015
						if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
							value = "N/A";
						}
						return value;
				}
			},{
				header:'Fecha de Vencimiento', 
				tooltip:'Fecha de Vencimiento',	
				dataIndex:'DF_FECHA_VENC_CREDITO',
				align:"center",
				sortable:true,
				width:100, 
				hidden: false,
				//renderer:Ext.util.Format.dateRenderer('d/m/Y')
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
							value = "N/A";
						}
						return value;
				}
			},{
				header:'Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex:'REFERENCIA_INT',	
				sortable:true,
				width:120,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
							value = "N/A";
						}
						return value;
				}
			},{
				header:'Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex:'VALOR_TASA',
				sortable:true,	
				width:120,
				hidden:false,
				align:"center",
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								var numero = value.toString();
								var dectext="";
								if (numero.indexOf('.') == -1) numero += ".";
									dectext = numero.substring(numero.indexOf('.')+1, numero.length);
								switch(dectext.length) {
									case 1:
										if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
											value = "N/A";
										}else{
											value = Ext.util.Format.number(value,'0.0%');
										}
										break;
									case 2:
										if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
											value = "N/A";
										}else{
											value = Ext.util.Format.number(value,'0.00%');
										}
										break;
									case 3:
										if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
											value = "N/A";
										}else{
											value = Ext.util.Format.number(value,'0.000%');
										}
										break;
									case 4:
										if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
											value = "N/A";
										}else{
											value = Ext.util.Format.number(value,'0.0000%');
										}
										break;
									case 5:
										if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
											value = "N/A";
										}else{
											value = Ext.util.Format.number(value,'0.00000%');
										}
										break;
									default:
										if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
											value = "N/A";
										}else{
											value = Ext.util.Format.number(value,'0%');
										}
								}
								return value;
							}
			},{
				header:'Monto tasa de inter�s', 
				tooltip: 'Monto tasa de inter�s',	
				dataIndex:'MONTO_INTERES',
				sortable:true,	width:120,
				hidden:false,	
				align:"right",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
							value = "N/A";
						}else{
							value = Ext.util.Format.number(value,'$0,0.00');
							
						}
						return value;
				}
			},{
				header:'Tipo de cobro inter�s', tooltip: 'Tipo de cobro inter�s',	dataIndex:'TIPO_COBRO_INTERES',	sortable:true,	width:130
			},{
				header:"Nombre del Producto",
				tooltip: "Nombre del Producto",
				dataIndex:'CG_DESCRIPCION_BINS',
				sortable:true,
				width:120,
				align:"center",
				hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32" ){
							value = "N/A";
						}
						return value;
				}
			},{
				header:'ID Orden enviado',
				tooltip: 'ID Orden enviado',
				dataIndex:'IC_ORDEN_ENVIADO',
				sortable:true,
				width:120,
				align:"right",
				hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32" ){
							value = "N/A";
						}
						return value;
				}
			},{
				header:'Respuesta de Operaci�n',
				tooltip: 'Respuesta de Operaci�n',
				dataIndex:'IC_OPERACION_CCACUSE',
				sortable:true,
				width:120,
				align:"right",
				hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32"  ){
							value = "N/A";
						}
						return value;
				}
			},{
				header:'C�digo Autorizaci�n',
				tooltip: 'C�digo Autorizaci�n',
				dataIndex:'CODIGO_AUTORIZADO',
				sortable:true,
				width:120,
				align:"right",
				hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32"  ){
							value = "N/A";
						}
						return value;
				}
			},{
				header:'N�mero Tarjeta de Cr�dito',
				tooltip: 'N�mero Tarjeta de Cr�dito',
				dataIndex:'NUMERO_TC',
				sortable:true,
				width:200,
				align:"center",
				hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32"  ){
							return value = "N/A";
						}else{
							return value==""?"":"XXXX-XXXX-XXXX-"+value;
						}
				}
			}
		],
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			height:60,
			autoScroll:true,
			items: [
				'->','-',
				/*{
					xtype:'button',	iconCls:'icoPdf',	tooltip:'Imprimir X P�gina PDF',	id: 'btnPorPagina'	,text:'X P�gina', scale:'large',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '24consulta08extpdf.jsp',
							params: Ext.apply(formulario,{
								informacion: 'ArchivoXpaginaPDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},{
					xtype:'button',	tooltip:'Bajar X P�gina PDF',	id:'btnBajarPagina',	hidden:true,	text:'Bajar X <br>P�gina', scale:'large'
				},'-',*/{
					xtype:'button', iconCls:'icoPdf',	tooltip:'Imprime los registros en formato PDF', id: 'btnImprimirPdf', text:'Generar Todo',// scale:'large',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta08ext.data.jsp',//24consulta08extpdf.jsp
							params: Ext.apply(formulario,{ informacion: 'ArchivoTotalPDF'}),//ArchivoTotalPDF
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},/*{
					xtype:'button',	text:'Bajar<br>Todo',	tooltip:'Bajar Todo PDF',	id:'btnBajarPdf',	hidden: true//, scale:'large'
				},*/'-',{
					xtype:'button',	iconCls:'icoXls',	text:'Generar archivo',	tooltip:'Generar archivo CSV',	id: 'btnGenerarArchivo',// scale:'large',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta08exta.jsp',
							params:	Ext.apply(formulario,{	informacion: 'ArchivoCSV'}	),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},{
					xtype:'button',	text:'Bajar Archivo',	tooltip:'Descargar archivo CSV',	id:'btnBajarArchivo',	hidden:true//, scale:'large'
				},'-',{
					xtype:'button',	iconCls:'icoXls',	text:'I. Variable',	tooltip:'Exportar Interfase Variable',	id: 'btnExportarVar', //scale:'large',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta08extVF.jsp',
							params:	Ext.apply(formulario,{	informacion: 'Variable'}	),
							callback: procesarSuccessFailureInterfases
						});
					}
				},{
					xtype:'button',	text:'Bajar<br>I. Variable',	tooltip:'Bajar Exportar Interfase Variable',	id:'btnBajaVar',	hidden: true//, scale:'large'
				},'-',{
					xtype:'button',	iconCls:'icoTxt',	text:'I. Fijo',	tooltip:'Exportar Interfase Fijo',	id: 'btnExportarFijo', //scale:'large',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta08extVF.jsp',
							params:	Ext.apply(formulario,{	informacion: 'Fijo'}	),
							callback: procesarSuccessFailureInterfases
						});
					}
				},{
					xtype:'button',	text:'Bajar<br>I. Fijo',	tooltip:'Bajar Exportar Interfase Fijo',	id:'btnBajaFijo',	hidden:true//, scale:'large'
				},
				{
					xtype:'button',
					tooltip:'Reporte operaciones TC � PDF',	
					id:'btnReporteOperPDF',
					iconCls:'icoPdf',
					hidden:true,	
					text:'Reporte operaciones TC � PDF',	
					handler: function(boton, evento) {
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta08ext.data.jsp',
							params: Ext.apply(formulario,{
								tipoConsulta : "Operada TC",
								informacion: 'ArchivoReporteTcPDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
								
							}),
							callback: mostrarArchivoPDF
						});
					}
				},
				{
					xtype:'button',
					iconCls:'icoXls',
					tooltip:'Reporte operaciones TC � CSV',
					id: 'btnReporteOperCSV',
					text:'Reporte operaciones TC � CSV',	
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta08ext.data.jsp',
							params: Ext.apply(formulario,{
								tipoConsulta : "Operada TC",
								informacion: 'ArchivoReporteTcCSV'
							}),
							callback: mostrarArchivoCSV
						});
					}
				}
			]
		}
	});

	var elementosForma = [
		{
			layout:'form',
			items:[
				{
					xtype: 'combo',
					id:	'_ic_epo',
					name: 'ic_epo',
					hiddenName : 'ic_epo',
					fieldLabel: 'EPO',
					emptyText: 'Seleccionar EPO. . .',
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					forceSelection : true,
					triggerAction : 'all',
					allowBlank: true,
					typeAhead: true,
					minChars : 1,
					store: catalogoEpoDistData,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners:{
						'select':function(cbo){
										if(!Ext.isEmpty(cbo.getValue())){
												var comboPyme = Ext.getCmp('_ic_pyme');
												comboPyme.setValue('');
												comboPyme.store.removeAll();
												comboPyme.store.reload({	params: {ic_epo: cbo.getValue()}	});
                                                                                Ext.Ajax.request({
                                                                                        url : '24consulta08ext.data.jsp',
                                                                                            params:	{ic_epo: cbo.getValue(),informacion: "getLeyenda"},
                                                                                            callback:function(opts, success, response) {
                                                                                            if (success == true) {
                                                                                               var operaContrato = Ext.util.JSON.decode(response.responseText).operaContrato;
                                                                                               var otraLeyenda = 'Por otra parte, a partir del 17 de octubre del 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta ' +
                                                                                               'de decir verdad, que s� ha emitido o emitir� a su CLIENTE o DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta '+
                                                                                               'transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.';
                                                                                               if(operaContrato == 'S'){
                                                                                                    var leyenda = "Las operaciones que aparecen en esta pantalla le ser�n financiadas al CLIENTE o DISTRIBUIDOR "+
                                                                                                                  " exclusivamente con el fin de pagar a la EMPRESA DE PRIMER ORDEN las cuentas pendientes de pago. "+
                                                                                                                  "Los derechos de cobro de los financiamientos que amparan dichas operaciones fueron cedidos a Nacional Financiera, S.N.C., para todos los efectos a que haya lugar.";
                                                                                                    leyenda = leyenda + "<br/><br/>"+otraLeyenda;
                                                                                                    Ext.getCmp('msgInicial').body.update(leyenda);
                                                                                               }else{
                                                                                                    var cadEpo =" Las operaciones que aparecen en esta pantalla fueron generadas por el CLIENTE o DISTRIBUIDOR y a su " +
                                                                                                                " vez notificadas a la EMPRESA DE PRIMER ORDEN. ";
                                                                                                    cadEpo = cadEpo + "<br/><br/>"+otraLeyenda;
                                                                                                    Ext.getCmp('msgInicial').body.update(cadEpo);
                                                                                               }
                                                                                             
                                                                                            }else {
                                                                                                    NE.util.mostrarConnError(response,opts);
                                                                                            }
                                                                                            }
                                                                                            });
                                                                                
                                                                                }
									}
					}
				},{
					xtype: 'combo',
					id: '_ic_pyme',
					name: 'ic_pyme',
					hiddenName : 'ic_pyme',
					allowBlank: true,
					fieldLabel: 'Distribuidor',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					emptyText: 'Seleccione Distribuidor',
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store : catalogoPymeDistData,
					tpl : NE.util.templateMensajeCargaCombo
				},{
					xtype: 'compositefield',
					fieldLabel: 'Fecha de operaci�n',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype: 'datefield',
							name: 'Txtfchini',
							id: '_Txtfchini',
							allowBlank: true,
							startDay: 0,
							width: 100,
							msgTarget: 'side',
							vtype: 'rangofecha', 
							campoFinFecha: '_Txtffin',
							margins: '0 20 0 0' 
						},
						{
							xtype: 'displayfield',
							value: 'al',
							width: 24
						},
						{
							xtype: 'datefield',
							name: 'Txtffin',
							id: '_Txtffin',
							allowBlank: true,
							startDay: 1,
							width: 100,
							msgTarget: 'side',
							vtype: 'rangofecha', 
							campoInicioFecha: '_Txtfchini',
							margins: '0 20 0 0'
						}
					]
				},{
				xtype: 'compositefield',
				fieldLabel: 'ID Orden ',
				combineErrors: false,
				msgTarget: 'side',
				minChars : 1,
				triggerAction : 'all',
				items: [				
					{
						xtype: 'textfield',
						name: 'numeroOrden',
						id: 'tfNumeroOrden',
						allowBlank: true,
						maxLength: 30,
						width:150 ,
						mode: 'local',
						autoLoad: false,
						displayField: 'descripcion',			
						valueField: 'clave',
						hiddenName : 'numeroOrden',
						forceSelection : true,
						msgTarget: 'side'// para que se muestre el circulito de error
					}
				]	
			}
			],
			defaults: {msgTarget: 'side',anchor: '-20'},
			bodyStyle: 'padding: 6px', frame:true,
			buttons: [
				{
					text: 'Consultar',
					id: 'btnConsultar',
					iconCls: 'icoBuscar',
					formBind: true,
					handler: function(boton, evento) {
									grid.hide();
									formAviso.hide();
									Ext.getCmp('gridTotales').hide();
									// El siguiente IF es solo para validar el formato de las fechas
									if(!Ext.getCmp('forma').getForm().isValid()){
										return;
									}
									var fechaOperaMin = Ext.getCmp('_Txtfchini');
									var fechaOperaMax = Ext.getCmp('_Txtffin');
									if (!Ext.isEmpty(fechaOperaMin.getValue()) || !Ext.isEmpty(fechaOperaMax.getValue()) ) {
										if(Ext.isEmpty(fechaOperaMin.getValue()))	{
											fechaOperaMin.markInvalid('Debe capturar ambas fechas de operaci�n o dejarlas en blanco');
											fechaOperaMin.focus();
											return;
										}else if (Ext.isEmpty(fechaOperaMax.getValue())){
											fechaOperaMax.markInvalid('Debe capturar ambas fechas de operaci�n o dejarlas en blanco');
											fechaOperaMax.focus();
											return;
										}
									}
									formulario = fp.getForm().getValues();
									fp.el.mask('Enviando....', 'x-mask-loading');
									consultaData.load({
										params: Ext.apply(/*fp.getForm().getValues(),*/{
											operacion: 'Generar', //Generar datos para la consulta
											start: 0,
											limit: 15
										})
									});
					} //fin handler
				},{
					text: 'Limpiar',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '24consulta08ext.jsp';
					}
				}
			]
		},{
			border:true, frame:true,	id:'msgInicial',	html:''
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: ' margin:0 auto;',
		title:	'<div align="center">Criterios de b�squeda</div>',
		frame: false,
		collapsible: true,
		titleCollapse: false,
		labelWidth: 130,
		items: elementosForma,
		monitorValid: true
	});

	var formAviso = new Ext.form.FormPanel({
		width:     900,
		id:        'formAviso',
		style:     'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		frame:     false,
		hidden:    true,
		html:      aviso.join('')
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			formAviso,
			NE.util.getEspaciador(10),
			grid,
			gridTotales
		]
	});

	catalogoEpoDistData.load();
	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url : '24consulta08ext.data.jsp',
		params:	{informacion: "valoresIniciales"},
		callback:procesaValoresIniciales
	});

});