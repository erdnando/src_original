<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	netropology.utilerias.*,
	com.netro.exception.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();

String _acuse		= (request.getParameter("_acuse")==null)?"":request.getParameter("_acuse");
String fechaHoy	= (request.getParameter("fechaHoy")==null)?"":request.getParameter("fechaHoy");
String horaActual	= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");
String usuario		= (request.getParameter("usuario")==null)?"":request.getParameter("usuario");

String totMtoAuto	  	= (request.getParameter("totMtoAuto")==null)?"":request.getParameter("totMtoAuto");
String totDocs	  		= (request.getParameter("totDocs")==null)?"":request.getParameter("totDocs");
String totMtoAutoDol = (request.getParameter("totMtoAutoDol")==null)?"":request.getParameter("totMtoAutoDol");
String totDocsDol	  	= (request.getParameter("totDocsDol")==null)?"":request.getParameter("totDocsDol");

String anoNafinE[] 	= request.getParameterValues("noNafinEs_aux");
String ic_epos[] 		= request.getParameterValues("ic_epos_aux");
String asolic[] 		= request.getParameterValues("tipoSol_aux");
String afolio[] 		= request.getParameterValues("folios_aux");
String amoneda[] 		= request.getParameterValues("nomMoneda");
//String amonto[] 	= request.getParameterValues("montos_aux");
String amontoAuto[] 	= request.getParameterValues("montoAutos_aux");
String aplaz[] 		= request.getParameterValues("plazs_aux");
//String acobint[] 		= request.getParameterValues("cobints_aux");
String tipoCobro[] 	= request.getParameterValues("tipoCobro_aux");
String avencimiento[]= request.getParameterValues("vencimientos_aux");
String acliAfil[] 	= request.getParameterValues("cliAfils_aux");
String anCtaEpo[] 	= request.getParameterValues("nCtaEpos_aux");
String aifBanco[] 	= request.getParameterValues("ifBancos_aux");
String anCtaIf[] 		= request.getParameterValues("nCtaIfs_aux");

try {

	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;

	contenidoArchivo.append("Moneda Nacional,,Dolares");
	contenidoArchivo.append("\nNúmero de solicitudes capturadas, Monto de Solicitudes capturadas, Número de Solicitudes capturadas, Monto de Solicitudes capturadas");
	contenidoArchivo.append("\n"+totDocs+","+totMtoAuto+","+totDocsDol+","+totMtoAutoDol+"\nLeyenda legal");
	contenidoArchivo.append("\n\nNúmero Nafin-Electrónico,EPO,Tipo de solicitud,Folio solicitud relacionada,Moneda,Monto autorizado,Plazo,Tipo de cobro de intereses,Fecha de vencimiento,Num. clientes afiliados a la EPO,No. Cuenta EPO,IF Banco de Servicio,No. Cuenta IF");
	if (ic_epos.length > 0){
		for(int i=0;i<ic_epos.length;i++)	{
			String banco = aifBanco[i];
			contenidoArchivo.append("\n"+anoNafinE[i]+","+ic_epos[i].replace(',',' ')+","+asolic[i]+","+afolio[i]+","+amoneda[i]+","+amontoAuto[i]+","+aplaz[i]+","+tipoCobro[i]+","+avencimiento[i]+","+acliAfil[i]+","+anCtaEpo[i]+","+banco.replace(',',' ')+","+anCtaIf[i]);
		}	
	}

	if (!("0".equals(totDocs))){
		contenidoArchivo.append("\nTotales MN,"+totDocs+",,,"+totMtoAuto);
	}
	if (!("0".equals(totDocsDol))){
		contenidoArchivo.append("\nTotales Dolares,"+totDocsDol+",,,"+totMtoAutoDol);
	}
	contenidoArchivo.append("\n\n\nDatos de cifras de control");
	contenidoArchivo.append("\nNum. acuse,"+_acuse);
	contenidoArchivo.append("\nFecha,"+fechaHoy);
	contenidoArchivo.append("\nHora,"+horaActual);
	contenidoArchivo.append("\nNombre y número de usuario,"+usuario.replace(',',' '));

	if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivo = archivo.nombre;
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>