<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,	net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("valoresIniciales"))	{

	JSONObject jsonObj = new JSONObject();
	Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);

	String auxLink = lineadm.getDetalle(iNoCliente);
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("auxLink", auxLink);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("detalleCuenta"))	{

	String ic_epo=	(request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	String solic=	(request.getParameter("solic")!=null)?request.getParameter("solic"):"";
	String folio =	(request.getParameter("folio")!=null)?request.getParameter("folio"):"";
	String peticion =	(request.getParameter("peticion")!=null)?request.getParameter("peticion"):"";
	String ic_moneda =	(request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
	JSONObject jsonObj = new JSONObject();
	
	AceptacionPyme acepPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
	
	String montoMinimo =  acepPyme.getMontoTotalMocumentos  (ic_epo,ic_moneda,  iNoCliente , "1" , "",  folio );	
	
	if (!"".equals(folio)) {
		
		Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);
	
		String auxCad = "";
		String auxCad2 = "";
		String auxCad3 = "";
		
		Vector vecFilas2 = null, vecColumnas = null;
		vecFilas2 = lineadm.getDetalleInicial(folio);
		if (vecFilas2.size()>0){
			vecColumnas = (Vector)vecFilas2.get(0);
			if(peticion.equals("epo")){
				auxCad = (String)vecColumnas.get(3);
				jsonObj.put("auxCad", auxCad);
			}
			if(peticion.equals("if")){
				auxCad = (String)vecColumnas.get(0);
				auxCad2 = (String)vecColumnas.get(1);
				auxCad3 = (String)vecColumnas.get(2);
				jsonObj.put("auxCad", auxCad);
				jsonObj.put("auxCad2", auxCad2);
				jsonObj.put("auxCad3", auxCad3);
			}
		}
		Vector vecFilas = lineadm.getMontoAutFechaVto(iNoCliente,ic_epo,folio);
		String auxLink	= "",	minimo = "0", vencimiento="";
		if ("R".equals(solic)){
			auxLink = (String)vecFilas.get(0);
			String minVence = (String)vecFilas.get(1);
			jsonObj.put("auxLink", auxLink);
			jsonObj.put("minVence", minVence);
		}else if("A".equals(solic)){
			//minimo 		= (String)vecFilas.get(0);
			vencimiento = (String)vecFilas.get(1);
			jsonObj.put("minimo", montoMinimo);			
			jsonObj.put("vencimiento", vencimiento);
		}
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoEPODist") ) {

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveIf(iNoCliente);
	cat.setTipoCredito("D");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoTipoCobroDist")){

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	
	String ic_epo	=	(request.getParameter("epoDM")!=null)?request.getParameter("epoDM"):"";
	
	String rs_epo_tipo_cobro_int	=	"1,2";
	
	if(!"".equals(ic_epo))	{
		rs_epo_tipo_cobro_int = cargaDocto.getEpoTipoCobroInt(ic_epo);
	}
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_cobro_interes");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_tipo_cobro_interes");
	cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("TipoFolio")){

	Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);

	String ic_epo	=	(request.getParameter("epoDM")!=null)?request.getParameter("epoDM"):"";
	String solic	=	(request.getParameter("solic")!=null)?request.getParameter("solic"):"";
	String moneda	=	(request.getParameter("moneda")!=null)?request.getParameter("moneda"):"";
	Vector vecFilas = null;
	Vector vecColumnas = null;
	String icFin="";
	List coleccionElementos = new ArrayList();
	if(!ic_epo.equals("") && !solic.equals("") && !moneda.equals("")){
		vecFilas = lineadm.getComboSolicitudRelacionada(iNoCliente,ic_epo,solic,moneda);
		if (vecFilas.size() > 0){
			for(int i=0;i<vecFilas.size();i++) {
				vecColumnas = (Vector)vecFilas.get(i);
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				icFin = (String)vecColumnas.get(0);
				elementoCatalogo.setClave(icFin);
				elementoCatalogo.setDescripcion(icFin);
				coleccionElementos.add(elementoCatalogo);
			}
			Iterator it = coleccionElementos.iterator();
			JSONArray jsonArr = new JSONArray();
			while(it.hasNext()) {
				Object obj = it.next();
				if (obj instanceof netropology.utilerias.ElementoCatalogo) {
					ElementoCatalogo ec = (ElementoCatalogo)obj;
					jsonArr.add(JSONObject.fromObject(ec));
				}
			}
			infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
		}else{
			infoRegresar = "{\"success\": true, \"total\": 0, \"registros\": [] }";
		}
	}

}else if (informacion.equals("validaLinea")){

	Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);

	String ic_epo	=	(request.getParameter("epoDM")!=null)?request.getParameter("epoDM"):"";
	String solic	=	(request.getParameter("solic")!=null)?request.getParameter("solic"):"";
	String moneda	=	(request.getParameter("moneda")!=null)?request.getParameter("moneda"):"";
	String vencimiento	=	(request.getParameter("vencimiento")!=null)?request.getParameter("vencimiento"):"";

	JSONObject jsonObj = new JSONObject();
	boolean limpiar = false;
	try{
		lineadm.validaLinea(moneda,iNoCliente,ic_epo,vencimiento,solic);
	}catch(NafinException ne){
		limpiar = true;
		jsonObj.put("mensajeVal", ne.getMsgError());
	}
	jsonObj.put("limpiar", new Boolean(limpiar));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("capturaLinea")){

	Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);

	JSONObject jsonObj = new JSONObject();
	String ic_epo	=	(request.getParameter("epoDM")!=null)?request.getParameter("epoDM"):"";
	String solic	=	(request.getParameter("tipoSol")!=null)?request.getParameter("tipoSol"):"";
	String moneda	=	(request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
	String vencimiento	=	(request.getParameter("vencimiento")!=null)?request.getParameter("vencimiento"):"";
	String ifBanco	=	(request.getParameter("ifBanco")!=null)?request.getParameter("ifBanco"):"";
	String tipoInt	=	(request.getParameter("tipoInt")!=null)?request.getParameter("tipoInt"):"";
	
	Vector vecFilas = null;
	Vector vecColumnas = null;

	vecFilas = lineadm.getTotalAfiliados(ic_epo);
	vecColumnas = (Vector)vecFilas.get(0);
	String cliAfil = (String)vecColumnas.get(0);
	/*plazo = Integer.parseInt(plaz);
	plazo *= 30;*/
	//vecFilas = lineadm.getValores(ic_epo,solic,moneda,"1",vencimiento,ifBanco);	//Esta linea tenia clavado el valor 1 como tipo de interes...
	vecFilas = lineadm.getValores(ic_epo,solic,moneda,tipoInt,vencimiento,ifBanco);
	vecColumnas = (Vector)vecFilas.get(0);
	String plaz = (String)vecColumnas.get(4);
	String noNafinE = (String)vecColumnas.get(5);
	ic_epo = (String)vecColumnas.get(0);
	solic = (String)vecColumnas.get(1);
	String nomMoneda = (String)vecColumnas.get(2);
	String cobint = (String)vecColumnas.get(3);
	ifBanco = (String)vecColumnas.get(6);
	
	jsonObj.put("cliAfil", cliAfil);
	jsonObj.put("plaz", plaz);
	jsonObj.put("noNafinE", noNafinE);
	jsonObj.put("ic_epo", ic_epo);
	jsonObj.put("solic", solic);
	jsonObj.put("nomMoneda", nomMoneda);
	jsonObj.put("ic_moneda", moneda);
	jsonObj.put("cobint", cobint);
	jsonObj.put("ifBanco", ifBanco);
	
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Confirma")){

	Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);

	JSONObject jsonObj = new JSONObject();
	Vector vecFilas = null;
	Vector vecColumnas = null;

	String totMtoAuto	  	= (request.getParameter("totMtoAuto")==null)?"":request.getParameter("totMtoAuto");
	String totDocs	  		= (request.getParameter("totDocs")==null)?"":request.getParameter("totDocs");
	String totMtoAutoDol = (request.getParameter("totMtoAutoDol")==null)?"":request.getParameter("totMtoAutoDol");
	String totDocsDol	  	= (request.getParameter("totDocsDol")==null)?"":request.getParameter("totDocsDol");

	String anoNafinE_aux[] 		= request.getParameterValues("noNafinEs_aux");
	String ic_epos_aux[] 		= request.getParameterValues("eposDM_aux");
	String asolic_aux[] 			= request.getParameterValues("solics_aux");
	String afolio_aux[] 			= request.getParameterValues("folios_aux");
	String amoneda_aux[] 		= request.getParameterValues("monedas_aux");
	String amontoAuto_aux[] 	= request.getParameterValues("montoAutos_aux");
	String aplaz_aux[] 			= request.getParameterValues("plazs_aux");
	String acobint_aux[] 		= request.getParameterValues("cobints_aux");
	String avencimiento_aux[]	= request.getParameterValues("vencimientos_aux");
	String acliAfil_aux[] 		= request.getParameterValues("cliAfils_aux");
	String anCtaEpo_aux[] 		= request.getParameterValues("nCtaEpos_aux");
	String aifBanco_aux[] 		= request.getParameterValues("ifBancos_aux");
	String anCtaIf_aux[] 		= request.getParameterValues("nCtaIfs_aux");

	vecFilas = lineadm.setLineasCredito(iNoUsuario,totDocs,totMtoAuto,totDocsDol,totMtoAutoDol,anoNafinE_aux,ic_epos_aux,asolic_aux,afolio_aux,amoneda_aux,
																amontoAuto_aux,aplaz_aux,acobint_aux,avencimiento_aux,acliAfil_aux,anCtaEpo_aux,aifBanco_aux,anCtaIf_aux,iNoCliente);

	vecColumnas = (Vector)vecFilas.get(0);
	String cc_acuse 	= (String)vecColumnas.get(0);
	String acuseForm 	= (String)vecColumnas.get(1);
	String fecha 		= (String)vecColumnas.get(2);
	String hora 		= (String)vecColumnas.get(3);
	String usuario		= iNoUsuario + " " + strNombreUsuario;

	jsonObj.put("_acuse", acuseForm);
	jsonObj.put("fechaCarga", fecha);
	jsonObj.put("horaCarga", hora);
	jsonObj.put("usuario", usuario);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>
