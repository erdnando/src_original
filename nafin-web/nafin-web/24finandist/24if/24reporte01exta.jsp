<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String ic_estatus_docto	=	"";
String tipoSolic			=	"";
String ic_estatus	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
if(!"".equals(ic_estatus)){
	ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
	tipoSolic		 = ic_estatus.substring(0,1);
}

try {

	if(iNoCliente != null && !iNoCliente.equals("") && !"".equals(ic_estatus) ) {
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
		String nombreArchivo = null;

		//variables de despliegue
		String epo				= "";
		String distribuidor 	= "";
		String numDocto			= "";
		String numAcuseCarga	= "";
		String fechaEmision		= "";
		String fechaPublicacion	= "";
		String fechaVencimiento	= "";
		String plazoDocto		= "";
		String moneda 			= "";
		String icMoneda			= "";
		double monto 			= 0;
		String tipoConversion	= "";
		double tipoCambio		= 0;
		double montoValuado		= 0;
		String plazoDescuento	= "";
		String porcDescuento	= "";
		double montoDescontar	= 0;
		String modoPlazo		= "";
		String estatus 			= "";
		String icDocumento		= "";
		String intermediario	= "";
		String tipoCredito		= "";
		String fechaOperacion	= "";
		double montoCredito		= 0;
		String plazoCredito		= "";
		String fechaSeleccion	= "";
		String fechaVencCredito	= "";
		String fechaOperIF		= "";
		String referenciaTasaInt= "";
		double valorTasaInt		= 0;
		double montoTasaInt 	= 0;
		String tipoCobranza		= "";
		String tipoCobroInt		= ""; 
		String numeroPago	 	= "";
		String causa			= "";
		String fechaCambio		= "";
		String monedaLinea		= "";

		RepCreditosEstatusIf reporte = new RepCreditosEstatusIf();
		reporte.setClaveIf(iNoCliente);
		reporte.setClaveEstatus(ic_estatus_docto);
		reporte.setTipoSolic(tipoSolic);
		reporte.setQrysentencia();
		Registros reg = reporte.executeQuery();
		/*********
		Cabeceras
		*********/
		if(("3".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			contenidoArchivo.append("\nSeleccionado Pyme");
		}
		if(("1".equals(ic_estatus_docto)&&"C".equals(tipoSolic))){
			contenidoArchivo.append("\nSeleccionado IF/Operado");
		}
		if(("2".equals(ic_estatus_docto)&&"C".equals(tipoSolic))){
			contenidoArchivo.append("\nEn Proceso Nafin");
		}
		if(("3".equals(ic_estatus_docto)&&"C".equals(tipoSolic))){
			contenidoArchivo.append("\nOperado");
		}
		if(("4".equals(ic_estatus_docto)&&"C".equals(tipoSolic))){
			contenidoArchivo.append("\nRechazada Nafin");
		}
		if(("24".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){ //MOD +(("24".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	||)
			contenidoArchivo.append("\nEn Proceso de Autorización IF");
		}
		if(("3".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	||
			("24".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	||//MOD +(("24".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	||)
			("1".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	||
			("2".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	||
			("3".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	||
			("4".equals(ic_estatus_docto)&&"C".equals(tipoSolic)))	{
		contenidoArchivo.append("\nEPO,Distribuidor,Num. Acuse de Carga,Num. docto.inicial,Fecha de emisión,Fecha de vencimiento,Fecha de Publicación,Plazo docto.,Moneda"+
								",Monto,Plazo para descuento en dias,% de descuento,Monto % de descuento,Tipo conv.,Tipo cambio,Monto valuado,Modalidad de plazo"+
								",Num.docto.final,Monto,Plazo,Fecha de Vencimiento,Fecha de Operación Distribuidor,Referencia tasa de interés,Valor tasa de interés,Monto de Intereses"+
								",Monto Total de Capital e Interés\n");
		}
		if("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic)){
			contenidoArchivo.append("\nOperado Pagado\nEPO,Distribuidor,Num. Acuse de Carga,Num. docto.inicial,Fecha de emisión,Fecha de vencimiento,Fecha de Publicación,Plazo docto.,Moneda"+
								",Monto,Plazo para descuento en dias,% de descuento,Monto % de descuento,Tipo conv.,Tipo cambio,Monto valuado,Modalidad de plazo"+
								",Num.docto.final,Monto,Plazo,Fecha de Vencimiento,Fecha de Operación Distribuidor,Referencia tasa de interés,Valor tasa de interés,Monto de Intereses"+
								",Monto Total de Capital e Interés, Número de pago\n");
		}
		if(("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			contenidoArchivo.append("\nPendiente de pago IF\nEPO,Distribuidor,Num. Acuse de Carga,Num. docto.inicial,Fecha de emisión,Fecha de vencimiento,Fecha de Publicación,Plazo docto.,Moneda"+
								",Monto,Plazo para descuento en dias,% de descuento,Monto % de descuento,Tipo conv.,Tipo cambio,Monto valuado,Modalidad de plazo"+
								",Num.docto.final,Monto,Plazo,Fecha de Vencimiento,Fecha de Operación Distribuidor,Referencia tasa de interés,Valor tasa de interés,Monto de Intereses"+
								",Monto Total de Capital e Interés, Número de pago de interés relacionado\n");
		}
		if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			contenidoArchivo.append("\nRechazado IF\nEPO,Distribuidor,Num. Acuse de Carga,Num. docto.inicial,Fecha de emisión,Fecha de vencimiento,Fecha de Publicación,Plazo docto.,Moneda"+
								",Monto,Plazo para descuento en dias,% de descuento,Monto a descontar,Tipo conv.,Tipo cambio,Monto valuado,Modalidad de plazo"+
								",Num.docto. final,Monto,Plazo,Fecha de Vencimiento,Fecha de Operación Distribuidor,Referencia tasa de interés,Valor tasa de interés,Monto de Intereses"+
								",Monto Total de Capital e Interés,Fecha de Rechazo,Causa\n");
		}

		while(reg.next()){
			epo			 		= (reg.getString("NOMBRE_EPO")==null)?"":reg.getString("NOMBRE_EPO");
			distribuidor 		= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
			numDocto				= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
			numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
			fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
			fechaPublicacion	= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
			fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
			plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
			moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
			monto 				= Double.parseDouble(reg.getString("FN_MONTO"));
			tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
			tipoCambio			= Double.parseDouble(reg.getString("TIPO_CAMBIO"));
			plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
			porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
			montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
			montoValuado		= (monto-montoDescontar)*tipoCambio;
			modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
			estatus 				= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
			icMoneda				= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
			icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
			tipoCobranza		= (reg.getString("TIPO_COBRANZA")==null)?"":reg.getString("TIPO_COBRANZA");
			icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
	//		intermediario		= (reg.getString("NOMBRE_IF")==null)?"":reg.getString("NOMBRE_IF");
			tipoCredito			= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
			montoCredito		= Double.parseDouble(reg.getString("MONTO_CREDITO"));
			plazoCredito		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
			tipoCobroInt		= (reg.getString("TIPO_COBRO_INT")==null)?"":reg.getString("TIPO_COBRO_INT");
			referenciaTasaInt	= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
			valorTasaInt		= Double.parseDouble(reg.getString("VALOR_TASA_INT"));
			montoTasaInt		= Double.parseDouble(reg.getString("MONTO_TASA_INT"));
			fechaSeleccion		= (reg.getString("FECHA_SELECCION")==null)?"":reg.getString("FECHA_SELECCION");
			fechaVencCredito	= (reg.getString("FECHA_VENC_CREDITO")==null)?"":reg.getString("FECHA_VENC_CREDITO");
			monedaLinea			= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");

			if(	("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	|| ("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){		//documento negociable
				numeroPago			= (reg.getString("CG_NUMERO_PAGO")==null)?"":reg.getString("CG_NUMERO_PAGO");
			}
			if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
				causa			=	(reg.getString("CT_CAMBIO_MOTIVO")==null)?"":reg.getString("CT_CAMBIO_MOTIVO");
				fechaCambio	=	(reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
			}

			/***************
				Contenido
			****************/

			contenidoArchivo.append(epo.replaceAll(",","") +","+distribuidor.replaceAll(",","")+","+numAcuseCarga+","+numDocto+","+fechaEmision+","+fechaVencimiento+","+
						fechaPublicacion+","+plazoDocto+","+moneda+","+monto+","+plazoDescuento+","+porcDescuento+","+montoDescontar+","+
						((icMoneda.equals(monedaLinea))?"":tipoConversion)+","+
						((icMoneda.equals(monedaLinea))?"":""+tipoCambio)+","+
						((icMoneda.equals(monedaLinea))?"":""+montoValuado)+","+
						modoPlazo+","+
						icDocumento+","+
						montoCredito+","+
						plazoCredito+","+
						fechaVencCredito+","+
						fechaSeleccion+","+
//						intermediario+","+
						referenciaTasaInt.replace(',',' ')+","+
						valorTasaInt+"%,"+montoTasaInt+","+
						(montoCredito+montoTasaInt));

			if(	("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic)) || ("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	){
				contenidoArchivo.append(","+numeroPago);
			}
			if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
				contenidoArchivo.append(","+fechaCambio+","+causa);
			}
			contenidoArchivo.append("\n");
		}

		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		} else {
			nombreArchivo = archivo.nombre;
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>