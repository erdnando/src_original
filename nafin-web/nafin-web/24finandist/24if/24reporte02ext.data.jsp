<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,	net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "", tituloGrid="";



if (informacion.equals("CatalogoEstatusDist")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_cambio_estatus");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_cambio_estatus");
	cat.setValoresCondicionIn("23,34,21,2", Integer.class);
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta") ){

	JSONObject jsonObj = new JSONObject();
	String ic_estatus	=	(request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
	if(ic_estatus.equals("23")){
		tituloGrid = "Estatus: En Proceso de Autorizacion IF a Negociable";
	} else if(ic_estatus.equals("34")){
		tituloGrid = "Estatus: En Proceso de Autorizacion IF a Seleccionado Pyme";
	} 

	if(iNoCliente != null && !iNoCliente.equals("") ){

		Registros registros = new Registros();
		

		/*if(	"14".equals(ic_estatus)	){

			RepCambiosEstatusIf reporte = new RepCambiosEstatusIf();
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			reporte.setClaveEstatus(ic_estatus);
			reporte.setQrysentencia();
			registros = reporte.executeQuery();

		}else */
		if(	"2".equals(ic_estatus)|| "23".equals(ic_estatus) || "34".equals(ic_estatus)	){

			RepCambiosEstatusIf reporte = new RepCambiosEstatusIf();
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			reporte.setClaveEstatus(ic_estatus);
			reporte.setQrysentencia();
			registros = new Registros();
			registros = reporte.executeQuery();
		}else if(	"21".equals(ic_estatus)	){

			RepCambiosEstatusIf reporte = new RepCambiosEstatusIf();
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			reporte.setClaveEstatus(ic_estatus);
			reporte.setQrysentencia();
			registros = new Registros();
			registros = reporte.executeQuery();

		}else{
			infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
		}
		
		if("23".equals(ic_estatus)||"34".equals(ic_estatus)){
			
			infoRegresar = "{\"success\": true,\"tituloGrid\": \"" + tituloGrid + "\", \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
		
		}else{
			infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
		}
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
}

%>
<%=infoRegresar%>