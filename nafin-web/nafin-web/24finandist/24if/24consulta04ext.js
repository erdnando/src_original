Ext.onReady(function(){

//------------------Handlers-------------------------
	var procesarConsultaData = function(store,arrRegistros,opts){
		accionBotones();
			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					accionBotonesDisabled();
				}
			}
		}
	
function verificaPanel(panel){ 
		var myPanel = Ext.getCmp(panel);
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambas Fechas son necesarias');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambas Fechas son necesarias');
				fecha2.focus();
				return false;
			}
		}
	return true;
}

var accionBotones= function(){
	//Ext.getCmp('btnBajarPDF').hide();
	//Ext.getCmp('btnBajarArchivoTodas').hide();
	Ext.getCmp('btnGenerarArchivoTodas').enable();
	Ext.getCmp('btnGenerarPDF').enable();
	Ext.getCmp('btnTotales').enable();
}
var accionBotonesDisabled= function(){
	
	Ext.getCmp('btnTotales').disable();
	Ext.getCmp('btnGenerarArchivoTodas').disable();
	Ext.getCmp('btnGenerarPDF').disable();

}
/*
var procesarSuccessFailureGenerarArchivoTodas =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarArchivoTodas');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarArchivoTodas');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarArchivoTodas =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivoTodas').enable();
		Ext.getCmp('btnGenerarArchivoTodas').setIconClass('icoXls');
	}
/*
var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {         
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

function procesarConsultaTotal(store,arrRegistros,opts) {
			
			if(arrRegistros!=null){
			var el = gridTotales.getGridEl();
			if(store.getTotalCount()>0){
				if(arrRegistros==''){
					//consultaTotales.load();
				}
				gridTotales.el.unmask();
			}else{		
					gridTotales.el.mask('No se cargaron los totales', 'x-mask');
				}
			}
		}
//------------------Stores-----------------------

var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '24consulta04ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'IC_DOCUMENTO'},
				{name: 'EPO'},
				{name: 'PYME'},
				{name: 'CG_TIPO_CREDITO'},
				{name: 'RESPONSABLE'},
				{name: 'COBRANZA'},
				{name: 'FECHA_OPER'},
				{name: 'CD_NOMBRE'},
				{name: 'MONTO_VAL'},
				{name: 'CT_REFERENCIA'},
				{name: 'TASA_INT'},
				{name: 'IG_PLAZO_DESCUENTO'},
				{name: 'FECHA_VENC'},
				{name: 'MONTO_INT'},
				{name: 'TIPO_COB_INT'},
				{name: 'CAMBIOESTATUS'},
				{name: 'FECHACAMBIO'},
				{name: 'CAMBIOMOTIVO'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params,{
											HcdCredito:Ext.getCmp('ctCredito').getValue()});
						}	},
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
						procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var consultaTotales = new Ext.data.JsonStore({
	root: 'registros',
	url: '24consulta04ext.data.jsp',
	baseParams: {
		informacion: 'Totales'
	},
	fields: [
				{name: 'CD_NOMBRE'},
				{name: 'COUNT(*)'},
				{name: 'SUMA_MONTO'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaTotal,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaTotal(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var tipoCreditoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		data : [	['D','Modalidad 1 (Riesgo Empresa de Primer Orden )'],	['C','Modalidad 2 (Riesgo Distribuidor) ']	]	
	});
	 
var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta04ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpo'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

var catalogoTipoCobroData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTipoCobroDist'},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});
	
var catalogoRechazo = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoRechazo'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

var catalogoDist = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoDist'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError
		}
  });


//------------------Componentes--------------------
var dt= Date();
var elementosForma = [
		{
			xtype: 'panel',
			layout:'column',
				items:[{
					xtype: 'container',
					id: 'panelIzq',
					labelWidth:120,
					columnWidth:.5,
					width:'50%',
					layout: 'form',
					items: [
						{
							anchor: '90%',
							xtype: 'combo',
							name: 'HcdCredito',
							id: 'ctCredito',
							fieldLabel: 'Tipo de Cr�dito',
							emptyText: 'Seleccionar Cr�dito',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'HcdCredito',
							msgTarget: 'side',
							forceSelection : true,
							triggerAction : 'all',
							allowBlank: false,
							lazyRender:true,
							typeAhead: true,
							minChars : 1,
							store: tipoCreditoData,
							listeners:{
								'select':function(combo){
												var cDist = Ext.getCmp('icEpo');
												cDist.setValue('');
												cDist.store.removeAll();
									
												cDist.store.reload({
														params: {
															HctCredito: combo.getValue()
														}
												});
											}
							}
						},
						{
							anchor: '90%',
							xtype: 'combo',
							fieldLabel: 'Nombre de la EPO',
							emptyText: 'Seleccionar una EPO',
							displayField: 'descripcion',
							msgTarget: 'side',
							valueField: 'clave',
							triggerAction: 'all',
							typeAhead: true,
							minChars: 1,
							store: catalogoEpo,
							//tpl: NE.util.templateMensajeCargaCombo,
							tpl:'<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>',
							name:'HicEpo',
							id: 'icEpo',
							mode: 'local',
							hiddenName: 'HicEpo',
							forceSelection: true,
							listeners: {
										select: {
											fn: function(combo) {
												Ext.getCmp('icDist').setValue();
												var cEpo = combo.getValue();
												var cDist = Ext.getCmp('icDist');
												cDist.setValue('');
												cDist.store.removeAll();
									
												cDist.store.load({
														params: {
															HicEpo: combo.getValue()
														}
												});
											
											}
										}
								}
						},{
						//distribuidor
						anchor: '90%',
						xtype: 'combo',
						emptyText: 'Seleccionar un Distribuidor',
						fieldLabel: 'Nombre del Distribuidor',
						displayField: 'descripcion',
						valueField: 'clave',
						msgTarget: 'side',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: catalogoDist,
						//tpl: NE.util.templateMensajeCargaCombo,
						tpl:'<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>',
						name:'HicDist',
						id: 'icDist',
						mode: 'local',
						hiddenName: 'HicDist',
						forceSelection: true
						},
						{
						anchor: '90%',
						xtype: 'numberfield',
						name: 'numCred',
						id: 'numCred',
						msgTarget: 'side',
						margins: '0 20 0 0',
						fieldLabel: 'N�mero de Documento Final',
						maxLength: 9
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha de operaci�n',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fechaOper1',
								id: 'fechaOper1',
						
								allowBlank: true,
								startDay: 0,
								width: 115,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'fechaOper2',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 24
							},
							{
								xtype: 'datefield',
								name: 'fechaOper2',
								id: 'fechaOper2',
								
								allowBlank: true,
								startDay: 1,
								width: 115,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoInicioFecha: 'fechaOper1',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					}
				]
			},
				{
				xtype: 'container',
				id: 'panelDer',
				columnWidth: .5,
				width: '50%',
				layout: 'form',
				labelWidth: 140,
				items: [
						{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de vencimiento',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaVenc1',
									id: 'fechaVenc1',
									allowBlank: true,
									startDay: 0,
									width: 115,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaVenc2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaVenc2',
									id: 'fechaVenc2',
									allowBlank: true,
									startDay: 1,
									width: 115,
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: 'fechaVenc1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},{
							anchor: '95%',
							xtype: 'combo',
							name: 'HtipoCoInt',
							id: 'tipoCoInt',
							fieldLabel: 'Tipo de cobro de inter�s',
							emptyText: 'Seleccione Tipo de cobro',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'HtipoCoInt',
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoTipoCobroData
						},{
							anchor: '95%',
							xtype: 'combo',
							name: 'Hrechazo',
							id: 'rechazo',
							fieldLabel: 'Tipo de Rechazo',
							emptyText: 'Seleccionar',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'Hrechazo',
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoRechazo,
							tpl:'<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>'
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de rechazo',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaRe1',
									id: 'fechaRe1',
							
									allowBlank: true,
									startDay: 0,
									width: 115,
									msgTarget: 'side',
									value:Ext.util.Format.date(dt,'d/m/Y'),
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaRe2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaRe2',
									id: 'fechaRe2',
									
									allowBlank: true,
									startDay: 1,
									width: 115,
									value: Ext.util.Format.date(dt,'d/m/Y'),
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: 'fechaRe1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						}
					]
				}
				
			]
		}
	]
	
var gridTotales = new Ext.grid.EditorGridPanel({
		store: consultaTotales,
		title: 'Totales',
		id: 'gridTotales',
		hidden:	true,
		style: 'margin: 0 auto;',
		columns: [
			{
				header: 'Moneda',
				dataIndex: 'CD_NOMBRE',
				align: 'center',	width: 300
			},{
				header: 'Total Registros',
				dataIndex: 'COUNT(*)',
				width: 300,	align: 'right'
			},{
				header: 'Total Monto',
				dataIndex: 'SUMA_MONTO',
				width: 300,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			}
		],
		width: 940,
		height: 100,
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	}); 
var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				title: '<center>Criterios de B�squeda</center>',
				store: consulta,
				//style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						id: 'numDocto',
						header:'N�mero Documento Final',
						tooltip: 'N�mero Documento Final',
						sortable: true,
						dataIndex: 'IC_DOCUMENTO',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'EPO',
						tooltip: 'EPO',
						sortable: true,
						dataIndex: 'EPO',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Distribuidor',
						tooltip: 'Distribuidor',
						sortable: true,
						dataIndex: 'PYME',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Tipo de Cr�dito',
						tooltip: 'Tipo de Cr�dito',
						sortable: true,
						dataIndex: 'CG_TIPO_CREDITO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Resp. pago de inter�s',
						tooltip: 'Resp. pago de inter�s',
						sortable: true,
						dataIndex: 'RESPONSABLE',
						width: 130,
						align: 'center'
						
						},
						{
						align:'center',
						header: 'Tipo de cobranza',
						tooltip: 'Tipo de cobranza',
						sortable: true,
						dataIndex: 'COBRANZA',
						width: 130
						},{
						
						header:'Fecha de operaci�n',
						tooltip: 'Fecha de operaci�n',
						sortable: true,
						dataIndex: 'FECHA_OPER',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'CD_NOMBRE',
						width: 150,
						align: 'center'
						},
						{
						header: 'Monto',
						tooltip: 'Monto',
						sortable: true,
						dataIndex: 'MONTO_VAL',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						header: 'Referencia tasa',
						tooltip: 'Referencia tasa',
						sortable: true,
						dataIndex: 'CT_REFERENCIA',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Tasa de inter�s',
						tooltip: 'Tasa de inter�s',
						sortable: true,
						dataIndex: 'TASA_INT',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('0.00%')
						
						},
						{
						align:'center',
						header: 'Plazo',
						tooltip: 'Plazo',
						sortable: true,
						dataIndex: 'IG_PLAZO_DESCUENTO',
						width: 130
						},
						{
						header: 'Fecha de vencimiento',
						tooltip: 'Fecha de vencimiento',
						sortable: true,
						dataIndex: 'FECHA_VENC',
						width: 130,
						align: 'center'
						},
						{
						header: 'Monto inter�s',
						tooltip: 'Monto inter�s',
						sortable: true,
						dataIndex: 'MONTO_INT',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						header: 'Tipo de cobro inter�s',
						tooltip: 'Tipo de cobro inter�s',
						sortable: true,
						dataIndex: 'TIPO_COB_INT',
						width: 150,
						align: 'center'
						},
						{
						header: 'Estatus',
						tooltip: 'Estatus',
						sortable: true,
						dataIndex: 'CAMBIOESTATUS',
						width: 150,
						align: 'center'
						},
						{
						header: 'Fecha del rechazo',
						tooltip: 'Fecha del rechazo',
						sortable: true,
						dataIndex: 'FECHACAMBIO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Causa',
						tooltip: 'Causa',
						sortable: true,
						dataIndex: 'CAMBIOMOTIVO',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						}
						
						
				
				],
				//stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					//autoScroll:true,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					
					items: ['->','-',
								{
										xtype: 'button',
										text: 'Totales',
										id: 'btnTotales',
										hidden: false,
										handler: function(boton, evento) {
												consultaTotales.load();
												gridTotales.show();
												gridTotales.el.mask('Enviando...', 'x-mask-loading');
										}
								},'-',
								{
									xtype: 'button',
									text:    'Generar Todo',
									tooltip: 'Imprime los registros en formato PDF.',
									iconCls: 'icoPdf',
									id: 'btnGenerarPDF',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
										Ext.Ajax.request({
											url: '24consulta04ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoPaginaPDF',
												start: cmpBarraPaginacion.cursor,
													limit: cmpBarraPaginacion.pageSize}),
											callback: procesarSuccessFailureGenerarPDF
										});
									}
								},'-',/*
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarPDF',
									hidden: true
								},*/
								{
									xtype: 'button',
									text: 'Generar Archivo',
									tooltip: 'Imprime los registros en formato CSV.',
									iconCls: 'icoXls',
									id: 'btnGenerarArchivoTodas',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '24consulta04ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV'}),
											callback: procesarSuccessFailureGenerarArchivoTodas
										});
									}
								}/*,
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivoTodas',
									hidden: true
								},
								'-'*/]
				}
		});



var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 940,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		trackResetOnLoad: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: false,
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						
						credito=Ext.getCmp('ctCredito');
						if(credito.getValue()==''){
							credito.markInvalid('El tipo de credito es obligatorio');
							credito.focus();
						}else{
							if(verificaPanel('panelIzq')&&verificaPanel('panelDer'))
							{
								
								if(verificaFechas('fechaOper1','fechaOper2')&&verificaFechas('fechaVenc1','fechaVenc2')&&verificaFechas('fechaRe1','fechaRe2')){
								grid.show();
								gridTotales.hide();
								var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
								consulta.load({ 
									params: Ext.apply(fp.getForm().getValues(),{
											operacion: 'Generar', //Generar datos para la consulta
											informacion: 'Consulta',
											start:0,
											limit:15
									})
								 });
								}
							}
						}
						
						/*//Mi acci�n
						grid.show();
						consulta.load({ params: Ext.apply(fp.getForm().getValues(),{txt_ne: Ext.getCmp('txt_ne').getValue()})
						});				
*/
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});


//------------------Principal-------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [ 
			fp,
			NE.util.getEspaciador(30),
			grid,
			gridTotales
		 
		 ]
  });
  catalogoTipoCobroData.load();
  catalogoRechazo.load();
});