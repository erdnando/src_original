<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.pdf.*,net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
String ic_estatus	=	(request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");

try {

	if(iNoCliente != null && !iNoCliente.equals("") ) {

		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);

		//variables de despliegue
		String epo				= "";
		String distribuidor 	= "";
		String numAcuse			= "";
		String numDocto			= "";
		String numAcuseCarga	= "";
		String fechaEmision		= "";
		String fechaPublicacion	= "";
		String fechaVencimiento	= "";
		String plazoDocto		= "";
		String moneda 			= "";
		String icMoneda			= "";
		double monto 			= 0;
		String tipoConversion	= "";
		double tipoCambio		= 0;
		double montoValuado		= 0;
		String plazoDescuento	= "";
		String porcDescuento	= "";
		double montoDescontar	= 0;
		String modoPlazo		= "";
		String estatus 			= "";
		String icDocumento		= "";
		String intermediario	= "";
		String tipoCredito		= "";
		String fechaOperacion	= "";
		double montoCredito		= 0;
		String plazoCredito		= "";
		String fechaVencCredito	= "";
		String referenciaTasaInt= "";
		double valorTasaInt		= 0;
		double montoTasaInt 	= 0;
		String tipoCobranza		= "";
		String tipoCobroInt		= ""; 
		String numeroPago	 	= "";
		String causa			= "";
		String fechaCambio		= "";
		String antFechaEmision	= "";
		String antFechaVto		= "";
		double antMonto			= 0;
		String antModoPlazo		= "";
		String monedaLinea		= "";
		//totales
		int		i = 0;
		int		totalDoctosMN		= 0;
		int 		totalDoctosUSD		= 0;
		double	totalMontoMN		= 0;
		double	totalMontoUSD		= 0;
		double	totalMontoValuadoMN	= 0;
		double	totalMontoValuadoUSD	= 0;
		double	totalMontoCreditoMN	= 0;
		double	totalMontoCreditoUSD	= 0;

		Registros reg	=	new Registros();
		RepCambiosEstatusIf reporte = new RepCambiosEstatusIf();

		if("14".equals(ic_estatus) ||"".equals(ic_estatus)	){
			totalDoctosMN = 0;
			totalMontoMN = 0;
			totalMontoValuadoMN = 0;
			totalDoctosUSD = 0;
			totalMontoUSD = 0;
			totalMontoValuadoUSD = 0;
			i=0;
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			reporte.setClaveEstatus("14");
			reporte.setQrysentencia();
			reg = reporte.executeQuery();
			
			pdfDoc.setTable(11, 100);
			pdfDoc.setCell("Estatus: Seleccionado Pyme a Rechazado IF","celda01",ComunesPDF.LEFT,11);

			pdfDoc.setCell("A","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Epo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Distribuidor","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Número de Acuse de Carga","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. docto. inicial","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de emisión","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de publicación","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo docto.","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto","formasmenB",ComunesPDF.CENTER);

			pdfDoc.setCell("B","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo para descuento en días","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("% de descuento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto a descontar","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo conv.","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo cambio","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto valuado en Pesos","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Modalidad de plazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. de docto. final","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Crédito","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("IF","formasmenB",ComunesPDF.CENTER);

			pdfDoc.setCell("C","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto docto. final","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Referencia tasa de interés","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Valor tasa de interés","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto de Intereses","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Total de Capital e Interés","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Rechazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Causa","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);

			while(reg.next()){
				epo			 		= (reg.getString("NOMBRE_EPO")==null)?"":reg.getString("NOMBRE_EPO");
				distribuidor 		= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
				numDocto			= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
				numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
				//fechaPublicacion	= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
				fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
				plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
				moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
				monto 				= Double.parseDouble(reg.getString("FN_MONTO")==null?"0":reg.getString("FN_MONTO"));
				tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
				tipoCambio			= Double.parseDouble(reg.getString("TIPO_CAMBIO")==null?"0":reg.getString("TIPO_CAMBIO"));
				plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
				porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
				montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
				montoValuado		= (monto-montoDescontar)*tipoCambio;
				modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
				estatus 			= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
				icMoneda			= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				tipoCobranza		= (reg.getString("TIPO_COBRANZA")==null)?"":reg.getString("TIPO_COBRANZA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				intermediario		= (reg.getString("NOMBRE_IF")==null)?"":reg.getString("NOMBRE_IF");
				tipoCredito			= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
				montoCredito		= Double.parseDouble(reg.getString("MONTO_CREDITO")==null?"0":reg.getString("MONTO_CREDITO"));
				plazoCredito		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
				tipoCobroInt		= (reg.getString("TIPO_COBRO_INT")==null)?"":reg.getString("TIPO_COBRO_INT");
				referenciaTasaInt	= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
				valorTasaInt		= Double.parseDouble(reg.getString("VALOR_TASA_INT")==null?"0":reg.getString("VALOR_TASA_INT"));
				montoTasaInt		= Double.parseDouble(reg.getString("MONTO_TASA_INT")==null?"0":reg.getString("MONTO_TASA_INT"));
				numAcuse			= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaPublicacion	= (reg.getString("FECHA_PUBLICACION")==null)?"":reg.getString("FECHA_PUBLICACION");
				fechaVencCredito	= (reg.getString("FECHA_VENC_CREDITO")==null)?"":reg.getString("FECHA_VENC_CREDITO");
				fechaCambio			= (reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
				causa				= (reg.getString("CAUSA")==null)?"":reg.getString("CAUSA");
				monedaLinea			= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += montoCredito;
				}else{
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					if(!icMoneda.equals(monedaLinea))
						totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += montoCredito;
				}
				pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numAcuse,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);

				pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(porcDescuento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell((icMoneda.equals(monedaLinea))?"":tipoConversion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(tipoCambio,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":"$ "+Comunes.formatoDecimal(montoValuado,2)),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);

				pdfDoc.setCell("C","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoCredito,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(referenciaTasaInt,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(valorTasaInt,2)+"%","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoTasaInt,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal((montoCredito+montoTasaInt),2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				i++;
			}
			if(i==0){
				pdfDoc.setCell("No se encontró ningún registro","formas",ComunesPDF.CENTER);
			}
			if(totalDoctosMN>0){
				pdfDoc.setCell("Total Doctos M.N.","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell(Integer.toString(totalDoctosMN),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto Valuado M.N.","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
				pdfDoc.setCell("Total Monto M.N.","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto Crédito M.N.","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
			}
			if(totalDoctosUSD>0){
				pdfDoc.setCell("Total Doctos Dólares","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell(Integer.toString(totalDoctosUSD),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto Valuado Dólares","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.CENTER,4);
				pdfDoc.setCell("Total Monto Dólares","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto Crédito Dólares","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
			}
			pdfDoc.addTable();
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		}
		if("2".equals(ic_estatus) ||"".equals(ic_estatus)	){
			totalDoctosMN = 0;
			totalMontoMN = 0;
			totalMontoValuadoMN = 0;
			totalDoctosUSD = 0;
			totalMontoUSD = 0;
			totalMontoValuadoUSD = 0;
			i=0;
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			reporte.setClaveEstatus("2");
			reporte.setQrysentencia();
			reg = reporte.executeQuery();

			pdfDoc.setTable(11, 100);
			pdfDoc.setCell("Estatus: Seleccionado Pyme a Negociable","celda01",ComunesPDF.LEFT,11);

			pdfDoc.setCell("A","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Epo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Distribuidor","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Número de Acuse de Carga","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. docto. inicial","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de emisión","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de publicación","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo docto.","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto","formasmenB",ComunesPDF.CENTER);

			pdfDoc.setCell("B","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo para descuento en días","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("% de descuento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto a descontar","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo conv.","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo cambio","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto valuado en Pesos","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Modalidad de plazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. de docto. final","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Crédito","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("IF","formasmenB",ComunesPDF.CENTER);

			pdfDoc.setCell("C","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto docto. final","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Referencia tasa de interés","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Valor tasa de interés","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto de Intereses","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Total de Capital e Interés","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Rechazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Causa","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);

			while(reg.next()){
				epo			 		= (reg.getString("NOMBRE_EPO")==null)?"":reg.getString("NOMBRE_EPO");
				distribuidor 		= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
				numDocto			= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
				numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
				//fechaPublicacion	= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
				fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
				plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
				moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
				monto 				= Double.parseDouble(reg.getString("FN_MONTO")==null?"0":reg.getString("FN_MONTO"));
				tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
				tipoCambio			= Double.parseDouble(reg.getString("TIPO_CAMBIO")==null?"0":reg.getString("TIPO_CAMBIO"));
				plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
				porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
				montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
				montoValuado		= (monto-montoDescontar)*tipoCambio;
				modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
				estatus 			= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
				icMoneda			= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				tipoCobranza		= (reg.getString("TIPO_COBRANZA")==null)?"":reg.getString("TIPO_COBRANZA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				intermediario		= (reg.getString("NOMBRE_IF")==null)?"":reg.getString("NOMBRE_IF");
				tipoCredito			= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
				montoCredito		= Double.parseDouble(reg.getString("MONTO_CREDITO")==null?"0":reg.getString("MONTO_CREDITO"));
				plazoCredito		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
				tipoCobroInt		= (reg.getString("TIPO_COBRO_INT")==null)?"":reg.getString("TIPO_COBRO_INT");
				referenciaTasaInt	= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
				valorTasaInt		= Double.parseDouble(reg.getString("VALOR_TASA_INT")==null?"0":reg.getString("VALOR_TASA_INT"));
				montoTasaInt		= Double.parseDouble(reg.getString("MONTO_TASA_INT")==null?"0":reg.getString("MONTO_TASA_INT"));
				numAcuse			= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaPublicacion	= (reg.getString("FECHA_PUBLICACION")==null)?"":reg.getString("FECHA_PUBLICACION");
				fechaVencCredito	= (reg.getString("FECHA_VENC_CREDITO")==null)?"":reg.getString("FECHA_VENC_CREDITO");
				fechaCambio			= (reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
				causa				= (reg.getString("CAUSA")==null)?"":reg.getString("CAUSA");
				monedaLinea			= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += montoCredito;
				}else{
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					if(!icMoneda.equals(monedaLinea))
						totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += montoCredito;
				}
				pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numAcuse,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(porcDescuento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell((icMoneda.equals(monedaLinea))?"":tipoConversion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(tipoCambio,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":"$ "+Comunes.formatoDecimal(montoValuado,2)),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
				
				pdfDoc.setCell("C","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoCredito,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(referenciaTasaInt,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(valorTasaInt,2)+"%","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoTasaInt,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal((montoCredito+montoTasaInt),2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				i++;
			}
			if(i==0){
				pdfDoc.setCell("No se encontró ningún registro","formas",ComunesPDF.CENTER);
			}
			if(totalDoctosMN>0){
				pdfDoc.setCell("Total Doctos M.N.","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell(Integer.toString(totalDoctosMN),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto Valuado M.N.","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
				pdfDoc.setCell("Total Monto M.N.","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto Crédito M.N.","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
			}
			if(totalDoctosUSD>0){
				pdfDoc.setCell("Total Doctos Dólares","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell(Integer.toString(totalDoctosUSD),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto Valuado Dólares","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.CENTER,4);
				pdfDoc.setCell("Total Monto Dólares","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto Crédito Dólares","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
			}
			pdfDoc.addTable();
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		}
			if("23".equals(ic_estatus) ||"34".equals(ic_estatus)	){
			totalDoctosMN = 0;
			totalMontoMN = 0;
			totalMontoValuadoMN = 0;
			totalDoctosUSD = 0;
			totalMontoUSD = 0;
			totalMontoValuadoUSD = 0;
			i=0;
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			if("23".equals(ic_estatus)){
				reporte.setClaveEstatus("23");
			}else if("34".equals(ic_estatus)){
				reporte.setClaveEstatus("34");
			}
			reporte.setQrysentencia();
			reg = reporte.executeQuery();

			pdfDoc.setTable(14, 100);
			if("23".equals(ic_estatus)){
				pdfDoc.setCell("Estatus: En Proceso de Autorizacion IF a Negociable","celda01",ComunesPDF.LEFT,14);
			}else{
				pdfDoc.setCell("Estatus: En Proceso de Autorizacion IF a Seleccionado Pyme","celda01",ComunesPDF.LEFT,14);
			}

			pdfDoc.setCell("A","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Epo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Distribuidor","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Número de Acuse de carga","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Número de documento inicial","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de emisión","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de publicación","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo de documento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo para descuento en días","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Porcentaje de descuento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto porcentaje de descuento","formasmenB",ComunesPDF.CENTER);

			pdfDoc.setCell("B","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Modalidad de plazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Número de documento final","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Crédito","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("IF","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto documento Final","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Referencia de tasa de interés","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Valor Tasa de interés","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto de Intereses","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de capital","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Rechazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Causa","formasmenB",ComunesPDF.CENTER);
			

			while(reg.next()){
				epo			 		= (reg.getString("NOMBRE_EPO")==null)?"":reg.getString("NOMBRE_EPO");
				distribuidor 		= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
				numDocto			= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
				numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
				//fechaPublicacion	= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
				fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
				plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
				moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
				monto 				= Double.parseDouble(reg.getString("FN_MONTO")==null?"0":reg.getString("FN_MONTO"));
				tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
				tipoCambio			= Double.parseDouble(reg.getString("TIPO_CAMBIO")==null?"0":reg.getString("TIPO_CAMBIO"));
				plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
				porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
				montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
				montoValuado		= (monto-montoDescontar)*tipoCambio;
				modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
				estatus 			= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
				icMoneda			= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				tipoCobranza		= (reg.getString("TIPO_COBRANZA")==null)?"":reg.getString("TIPO_COBRANZA");
				icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				intermediario		= (reg.getString("NOMBRE_IF")==null)?"":reg.getString("NOMBRE_IF");
				tipoCredito			= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
				montoCredito		= Double.parseDouble(reg.getString("MONTO_CREDITO")==null?"0":reg.getString("MONTO_CREDITO"));
				plazoCredito		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
				tipoCobroInt		= (reg.getString("TIPO_COBRO_INT")==null)?"":reg.getString("TIPO_COBRO_INT");
				referenciaTasaInt	= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
				valorTasaInt		= Double.parseDouble(reg.getString("VALOR_TASA_INT")==null?"0":reg.getString("VALOR_TASA_INT"));
				montoTasaInt		= Double.parseDouble(reg.getString("MONTO_TASA_INT")==null?"0":reg.getString("MONTO_TASA_INT"));
				numAcuse			= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaPublicacion	= (reg.getString("FECHA_PUBLICACION")==null)?"":reg.getString("FECHA_PUBLICACION");
				fechaVencCredito	= (reg.getString("FECHA_VENC_CREDITO")==null)?"":reg.getString("FECHA_VENC_CREDITO");
				fechaCambio			= (reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
				causa				= (reg.getString("CAUSA")==null)?"":reg.getString("CAUSA");
				monedaLinea			= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += montoCredito;
				}else{
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					if(!icMoneda.equals(monedaLinea))
						totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += montoCredito;
				}
				pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numAcuse,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(porcDescuento+"% ","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoCredito,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(referenciaTasaInt,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(valorTasaInt,2)+"%","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoTasaInt,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal((montoCredito+montoTasaInt),2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
				i++;
			}
			if(i==0){
				pdfDoc.setCell("No se encontró ningún registro","formas",ComunesPDF.CENTER);
			}
			if(totalDoctosMN>0){
				pdfDoc.setCell("Total Doctos M.N.","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell(Integer.toString(totalDoctosMN),"formas",ComunesPDF.CENTER,2);
				//pdfDoc.setCell("Total Monto Valuado M.N.","formas",ComunesPDF.CENTER,2);
				//pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
				pdfDoc.setCell("Total Monto M.N.","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto Crédito M.N.","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.CENTER,2);
			}
			if(totalDoctosUSD>0){
				pdfDoc.setCell("Total Doctos Dólares","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell(Integer.toString(totalDoctosUSD),"formas",ComunesPDF.CENTER,2);
				//pdfDoc.setCell("Total Monto Valuado Dólares","formas",ComunesPDF.CENTER,2);
				//pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.CENTER,4);
				pdfDoc.setCell("Total Monto Dólares","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto Crédito Dólares","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.CENTER,2);
			}
			pdfDoc.addTable();
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		}
		
		if("21".equals(ic_estatus) ||"".equals(ic_estatus)	){
			totalDoctosMN = 0;
			totalMontoMN = 0;
			totalMontoValuadoMN = 0;
			totalDoctosUSD = 0;
			totalMontoUSD = 0;
			totalMontoValuadoUSD = 0;
			i=0;
			reporte = new RepCambiosEstatusIf();
			reporte.setClaveIf(iNoCliente);
			reporte.setClaveEstatus("21");
			reporte.setQrysentencia();
			reg = reporte.executeQuery();

			pdfDoc.setTable(12, 100);
			pdfDoc.setCell("Estatus: Modificación","celda01",ComunesPDF.LEFT,12);

			pdfDoc.setCell("A","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Epo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Distribuidor","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Número de Acuse de Carga","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. docto.","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de emisión","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de publicación","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo docto.","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo para descuento en días","formasmenB",ComunesPDF.CENTER);
			
			pdfDoc.setCell("B","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("% de descuento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto a descontar","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo conv.","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo cambio","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto valuado en	 Pesos","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Modalidad de plazo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Cambio","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Anterior Fecha de Emisión","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Anterior Fecha de Vencimiento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Anterior Monto del Documento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Anterior Modalidad de Plazo","formasmenB",ComunesPDF.CENTER);

			while(reg.next()){
				epo			 			= (reg.getString("NOMBRE_EPO")==null)?"":reg.getString("NOMBRE_EPO");
				distribuidor 			= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
				numDocto					= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
				numAcuseCarga			= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaEmision			= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
				//fechaPublicacion		= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
				fechaVencimiento		= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
				plazoDocto				= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
				moneda 					= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
				monto						= Double.parseDouble(reg.getString("FN_MONTO")==null?"0":reg.getString("FN_MONTO"));
				tipoConversion			= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
				tipoCambio				= Double.parseDouble(reg.getString("TIPO_CAMBIO")==null?"0":reg.getString("TIPO_CAMBIO"));
				plazoDescuento			= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
				porcDescuento			= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
				montoDescontar			= monto*Double.parseDouble(porcDescuento)/100;
				montoValuado			= (monto-montoDescontar)*tipoCambio;
				modoPlazo				= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
				icMoneda					= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
				icDocumento				= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
				tipoCobranza			= (reg.getString("TIPO_COBRANZA")==null)?"":reg.getString("TIPO_COBRANZA");
				tipoCredito				= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
				numAcuse					= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
				fechaPublicacion		= (reg.getString("FECHA_PUBLICACION")==null)?"":reg.getString("FECHA_PUBLICACION");
				fechaCambio				= (reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
				antFechaEmision		= (reg.getString("FECHA_EMISION_ANT")==null)?"":reg.getString("FECHA_EMISION_ANT");
				antFechaVto				= (reg.getString("FECHA_VENC_ANT")==null)?"":reg.getString("FECHA_VENC_ANT");
				antMonto					= Double.parseDouble((reg.getString("FN_MONTO_ANTERIOR").equals(""))?"0":reg.getString("FN_MONTO_ANTERIOR"));
				antModoPlazo			= (reg.getString("MODO_PLAZO_ANT")==null)?"":reg.getString("MODO_PLAZO_ANT");
				icMoneda					= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
				monedaLinea				= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
				}else{
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					if(!icMoneda.equals(monedaLinea))
						totalMontoValuadoUSD += montoValuado;
				}

				pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numAcuse,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);

				pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(porcDescuento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell((icMoneda.equals(monedaLinea))?"":tipoConversion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(tipoCambio,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":"$ "+Comunes.formatoDecimal(montoValuado,2)),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(antFechaEmision,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(antFechaVto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell((antMonto==0)?"":"$ "+Comunes.formatoDecimal(antMonto,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(antModoPlazo,"formas",ComunesPDF.CENTER);
				i++;
			}
			if(i==0){
				pdfDoc.setCell("No se encontró ningún registro","formas",ComunesPDF.CENTER);
			}
			if(totalDoctosMN>0){
				pdfDoc.setCell("Total M.N. de Documentos","formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell(Integer.toString(totalDoctosMN),"formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("Total Monto M.N.","formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("Total Monto Valuado M.N.","formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
			}
			if(totalDoctosUSD>0){
				pdfDoc.setCell("Total de Documentos Dólares","formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell(Integer.toString(totalDoctosUSD),"formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("Total Monto Dólares","formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("Total Monto Valuado Dólares","formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.CENTER,6);
			}
			pdfDoc.addTable();
		}
		pdfDoc.endDocument();

		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>