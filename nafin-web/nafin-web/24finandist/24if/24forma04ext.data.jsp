 <%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.distribuidores.*,
		net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("CatalogoEPODist") ) {

		String tipo_credito = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");

		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClaveIf(iNoCliente);
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();

}if (informacion.equals("CatalogoDistribuidor")){

	String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String tipoCredito = (request.getParameter("tipoCredito")==null)?"":request.getParameter("tipoCredito");

	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		cat.setTipoCredito(tipoCredito);
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();
	}

}else if (informacion.equals("CatalogoTipoCobroDist")){

	String rs_epo_tipo_cobro_int	=	"1,2";
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_cobro_interes");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_tipo_cobro_interes");
	cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoEstatusDist")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_docto");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_docto");
	cat.setValoresCondicionIn("11,22", Integer.class);
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta")){

	int start = 0;
	int limit = 0;

	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	String 	msIntermediario = iNoCliente,
			msNumEPO        = (request.getParameter("lstEpo")        	  == null) ? ""   : request.getParameter("lstEpo"),
			msNumPyme       = (request.getParameter("lstPyme")        	  == null) ? ""   : request.getParameter("lstPyme"),
			msNumCredito    = (request.getParameter("txtNumCredito") 	  == null) ? ""   : request.getParameter("txtNumCredito"),
			msFchOperIni    = (request.getParameter("txtFchOper1")   	  == null) ? ""   : request.getParameter("txtFchOper1"),
			msFchOperFin    = (request.getParameter("txtFchOper2")   	  == null) ? ""   : request.getParameter("txtFchOper2"),
			msMontoCredIni  = (request.getParameter("txtMonto1")       	  == null) ? "0"  : (request.getParameter("txtMonto1").equals("")) ? "0" : request.getParameter("txtMonto1"),
			msMontoCredFin  = (request.getParameter("txtMonto2")     	  == null) ? "0"  : (request.getParameter("txtMonto2").equals("")) ? "0" : request.getParameter("txtMonto2"),
			msFchVenciIni   = (request.getParameter("txtFchVencimiento1") == null) ? ""   : request.getParameter("txtFchVencimiento1"),
			msFchVenciFin   = (request.getParameter("txtFchVencimiento2") == null) ? ""   : request.getParameter("txtFchVencimiento2"),
			msEstatus       = (request.getParameter("lstEstatus")         == null) ? "11" : request.getParameter("lstEstatus"),
			msTipoCobroInte = (request.getParameter("lstTipoCobroInte")   == null) ? ""   : request.getParameter("lstTipoCobroInte"),
			lstTipoCredito	= (request.getParameter("lstTipoCredito")	  == null) ? ""   : request.getParameter("lstTipoCredito");

	double  mdMontoCredIni = Double.parseDouble( msMontoCredIni ),
			mdMontoCredFin = Double.parseDouble( msMontoCredFin );

	ConsPagoCreditos pagina = new ConsPagoCreditos();
	//pagina
	pagina.setTipoCredito(lstTipoCredito);
	pagina.setIc_epo(msNumEPO);
	pagina.setNumeroCredito(msNumCredito);
	pagina.setFechaOperaIni(msFchOperIni);
	pagina.setFechaOperaFin(msFchOperFin);
	pagina.setMontoIni(msMontoCredIni);
	pagina.setMontoFin(msMontoCredFin);
	pagina.setFechaVenceIni(msFchVenciIni);
	pagina.setFechaVenceFin(msFchVenciFin);
	pagina.setEstatus(msEstatus);
	pagina.setTipoCobro(msTipoCobroInte);
	pagina.setIc_pyme(msNumPyme);
	pagina.setIc_if(iNoCliente);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pagina);

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request);
		}
		infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}

} else if (informacion.equals("ResumenTotales")) {		//Datos para el Resumen de Totales

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

}else if (informacion.equals("GuardaCambios")){

	JSONObject jsonObj = new JSONObject();

	try{
		MantenimientoDist manttoDis = ServiceLocator.getInstance().lookup("MantenimientoDistEJB",MantenimientoDist.class);

		String[] mAsNumCredito   = request.getParameterValues("hddNumCredito"),
				 mAsEstatusXAsig = request.getParameterValues("lstEstatusXAsig"),
				 mAsNumRegPago   = request.getParameterValues("txtNumRegPago");

		int miNumRegistros = mAsNumCredito.length;
		Vector mVecRechazos = new Vector();

		if ( miNumRegistros > 0 )	{
			
			for ( int i=0; i<miNumRegistros; i++ )	{
				Vector lVecTemporal = new Vector();
				
				if ( !mAsEstatusXAsig[i].equals("") )	{
					lVecTemporal.add( mAsNumCredito[i] );
					lVecTemporal.add( mAsEstatusXAsig[i] );
					if ( mAsNumRegPago[i] == null || mAsNumRegPago[i].equals("") )	lVecTemporal.add(" ");
					else	lVecTemporal.add( mAsNumRegPago[i] );
					mVecRechazos.add( lVecTemporal );
				}
			}
			manttoDis.guardaPagosRechazados( mVecRechazos );
		}
		jsonObj.put("success", new Boolean(true));

	}catch(Exception e){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error en el proceso. "+e);
	}

	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>