	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,  
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONObject,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	com.netro.anticipos.*"
	
	
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/15cadenas/015secsession.jspf" %>

<%
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String informacion         =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String ic_epo					=(request.getParameter("HicEpo")    != null) ?   request.getParameter("HicEpo") :"";
	String icDist					=(request.getParameter("HicDist")    != null) ?   request.getParameter("HicDist") :"";
	String ctCredito				=(request.getParameter("HctCredito")    != null) ?   request.getParameter("HctCredito") :"";
	String ctTasa					=(request.getParameter("HctTasa")    != null) ?   request.getParameter("HctTasa") :"";
	String ic_moneda 				=(request.getParameter("HcbMoneda")    != null) ?   request.getParameter("HcbMoneda") :"";
	String ic_pyme					=(request.getParameter("HicDist")    != null) ?   request.getParameter("HicDist") :"";
	String tasa						=(request.getParameter("tipoTasa")    != null) ?   request.getParameter("tipoTasa") :"";
	String moneda 					=(request.getParameter("moneda")    != null) ?   request.getParameter("moneda") :"";

	if(informacion.equals("catologoEpo")){
	
		CatalogoEPODistribuidores catalogo = new CatalogoEPODistribuidores();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setTipoCredito(ctCredito);
		catalogo.setClaveIf(iNoCliente);
		infoRegresar=catalogo.getJSONElementos();
	}else if(informacion.equals("catologoDist")){
	
		CatalogoPymeDistribuidores catalogo = new CatalogoPymeDistribuidores();
		catalogo.setCampoClave("cp.ic_pyme");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveEpo(ic_epo);
		infoRegresar=catalogo.getJSONElementos();
	}else if (informacion.equals("catologoMonedaDist")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();	
	}else if(informacion.equals("Consulta")||informacion.equals("ArchivoCSV")||informacion.equals("ArchivoPDF")){
		CapturaTasas BeanTasas = netropology.utilerias.ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);
		HashMap	datos;
		List reg=new ArrayList();
		Vector lovTasas		= null;
		Vector tasasTemp =null;
		int total=0;
		
		if ("D".equals(ctCredito)){//MODALIDAD 1
			lovTasas = BeanTasas.ovgetTasasPreferNego("4",ctTasa,iNoCliente,ic_epo,ic_moneda,ic_pyme,"", ctCredito);
			if(informacion.equals("Consulta")){
			while(!lovTasas.isEmpty()){
			total++;
				tasasTemp=(Vector)lovTasas.remove(0);
				//System.out.println(tasasTemp);
				
					if(!tasasTemp.isEmpty()){
						datos=new HashMap();
						datos.put("FECHA",tasasTemp.get(12));
						datos.put("PLAZO",tasasTemp.get(3));
						datos.put("PYME",tasasTemp.get(10));
						datos.put("TIPO_TASA",tasasTemp.get(1)+" ("+tasasTemp.get(4)+") "+tasasTemp.get(5)+tasasTemp.get(6));	
						datos.put("VALOR",Comunes.formatoDecimal(Double.parseDouble(tasasTemp.get(11).toString()),4));
						if(ctTasa.equals("N")){
							datos.put("VALOR_TASA",tasasTemp.get(11));
						}
						reg.add(datos);
					}
				
			}
		}
			}else if("C".equals(ctCredito)){//MODALIDAD 2 "
					lovTasas = BeanTasas.ovgetTasasPreferNego("4",ctTasa,iNoCliente,ic_epo,ic_moneda,ic_pyme,"", ctCredito);
					if(informacion.equals("Consulta")){
						while(!lovTasas.isEmpty()){
						total++;
							tasasTemp=(Vector)lovTasas.remove(0);
							//System.out.println(tasasTemp);
							
								if(!tasasTemp.isEmpty()){
									datos=new HashMap();
									//datos.put("MONEDA",tasasTemp.get(1));
									datos.put("TIPO_CREDITO","Modalidad 2 (Riesgo Distribuidor)");
									datos.put("DISTRIBUIDOR",tasasTemp.get(10));
									datos.put("REFERENCIA",tasasTemp.get(16));//HACER UN AQUIVALENCIA
									datos.put("PLAZO",tasasTemp.get(3));
									datos.put("TIPO_TASA",tasasTemp.get(1));
									datos.put("VALOR",Comunes.formatoDecimal(Double.parseDouble(tasasTemp.get(4).toString()),4));
									datos.put("FECHA",tasasTemp.get(8));
									/*datos.put("VALOR_TASA",tasasTemp.get(12));
									
									datos.put("FECHA",tasasTemp.get(12));
									datos.put("PLAZO",tasasTemp.get(3));
									datos.put("PYME",tasasTemp.get(10));
									datos.put("TIPO_TASA",tasasTemp.get(1)+" ("+tasasTemp.get(4)+") "+tasasTemp.get(5)+tasasTemp.get(6));	
									datos.put("VALOR",Comunes.formatoDecimal(Double.parseDouble(tasasTemp.get(11).toString()),4));
									*/
									//if(ctTasa.equals("P")){
										datos.put("VALOR_TASA",tasasTemp.get(11));
									//}
									reg.add(datos);
								}
							
						}
					}
			
			}else{
			lovTasas = BeanTasas.ovgetTasasxPyme(4,ic_pyme,iNoCliente,"1,54");
			if(informacion.equals("Consulta")){
			while(!lovTasas.isEmpty()){
			total++;
				tasasTemp=(Vector)lovTasas.remove(0);
				//System.out.println(tasasTemp);
				if(!tasasTemp.isEmpty()){
						datos=new HashMap();
						datos.put("MONEDA",tasasTemp.get(0));
						datos.put("TASA",tasasTemp.get(1));
						datos.put("PLAZO",tasasTemp.get(3));
						datos.put("VALOR",tasasTemp.get(4));
						datos.put("RET_MAT",tasasTemp.get(5));
						datos.put("PUNTOS",Comunes.formatoDecimal(Double.parseDouble(tasasTemp.get(6).toString()),2));
						datos.put("TASA_APLICAR",Comunes.formatoDecimal(Double.parseDouble(tasasTemp.get(7).toString()),2));
						datos.put("FECHA_ACT",tasasTemp.get(8).toString());
						/*
						
						
						
						
						*/
						reg.add(datos);
						
					}
				}
			}
		}
		if(informacion.equals("Consulta")){
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("registros", reg);
			jsonObj.put("total",total+"");
			infoRegresar=jsonObj.toString();
		}
		
		 if(informacion.equals("ArchivoCSV")){
					String archivo;
					String valor;
					if ("D".equals(ctCredito)){
							tasasTemp=(Vector)lovTasas.get(0);
							archivo=tasasTemp.get(9)+"\n";
							archivo+="Tipo de Crédito,Distribuidor,Referencia,Plazo,Tipo Tasa,Valor,Fecha,Valor Tasa\n";
							while(!lovTasas.isEmpty()){
								tasasTemp=(Vector)lovTasas.remove(0);
								//System.out.println(tasasTemp);
								if(!tasasTemp.isEmpty()){
									archivo+="Modalidad 1 (Riesgo Empresa de Primer Orden ),"; //TIPO DE CREDITO
									archivo+=tasasTemp.get(10)+",";
									archivo+=tasa+",";
									archivo+="'"+tasasTemp.get(3)+"',";
									//archivo+=tasasTemp.get(1)+" ("+tasasTemp.get(4)+") "+tasasTemp.get(5)+tasasTemp.get(6)+",";
									archivo+=tasasTemp.get(1)+",";
									archivo+=Comunes.formatoDecimal(Double.parseDouble(tasasTemp.get(4).toString()),4)+",";
									valor=(String)tasasTemp.get(12);
									//System.out.println(valor);
									if(valor!=null)
										if(!valor.equals("null"))
										archivo+=valor+",";
										else
										archivo+=",";
									else
									archivo+=",";
									//if(ctTasa.equals("N")){
										archivo+=tasasTemp.get(11)+"\n";
									//}else{
									//	archivo+="\n";
									//}
								}
								
							}
					}else{
						//MODALIDAD 2
						tasasTemp=(Vector)lovTasas.get(0);
							archivo="\n"+tasasTemp.get(9).toString().replaceAll(",","") +"\n";
							archivo+="Tipo de Crédito,Distribuidor,Referencia,Plazo,Tipo Tasa,Valor,Fecha,Valor Tasa\n";
							while(!lovTasas.isEmpty()){
								tasasTemp=(Vector)lovTasas.remove(0);
								//System.out.println(tasasTemp);
								if(!tasasTemp.isEmpty()){
									archivo+="Modalidad 2 (Riesgo Distribuidor) "+",";
									archivo+=tasasTemp.get(10)+",";
									archivo+=tasa+",";
									archivo+="'"+tasasTemp.get(3)+"',";
									//archivo+=tasasTemp.get(1)+" ("+tasasTemp.get(4)+") "+tasasTemp.get(5)+tasasTemp.get(6)+",";
									archivo+=tasasTemp.get(1)+",";
									archivo+=Comunes.formatoDecimal(Double.parseDouble(tasasTemp.get(4).toString()),4)+",";
									archivo+=tasasTemp.get(8)+",";
									valor=(String)tasasTemp.get(12);
									if(valor!=null)
										if(!valor.equals("null"))
										archivo+=valor+",";
										else
										archivo+=",";
									else
									//archivo+=",";
									//if(ctTasa.equals("P")){
										archivo+=" "+tasasTemp.get(11)+"\n";
									//}else{
									//	archivo+="\n";
									//}
									
								}
							}
					
					}
					CreaArchivo creaArchivo = new CreaArchivo();
					String nombreArchivo;
					if(!creaArchivo.make(archivo, strDirectorioTemp, ".csv")) {
						jsonObj.put("success", new Boolean(false));
						jsonObj.put("msg", "Error al generar el archivo");
					} else {
						nombreArchivo = creaArchivo.nombre;
						jsonObj.put("success", new Boolean(true));
						jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					}
				
				
				infoRegresar=jsonObj.toString();
					
				}else if(informacion.equals("ArchivoPDF")){
					String[] meses= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
					//String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
				
					pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					if ("D".equals(ctCredito)){
							tasasTemp=(Vector)lovTasas.get(0);
							pdfDoc.addText(tasasTemp.get(9).toString(),"formas",ComunesPDF.CENTER);
							List lEncabezados = new ArrayList();
							//Tipo de Crédito,Distribuidor,
							//Referencia,Plazo,Tipo Tasa,Valor,Fecha,Valor Tasa
							lEncabezados.add("Tipo de Crédito");
							lEncabezados.add("Distribuidor");
							lEncabezados.add("Referencia");
							lEncabezados.add("Plazo");
							lEncabezados.add("Tipo Tasa");
							lEncabezados.add("Valor");
							lEncabezados.add("Fecha");
							lEncabezados.add("Valor Tasa");
							pdfDoc.setTable(lEncabezados, "formasmenB", 100);
							
							while(!lovTasas.isEmpty()){
								tasasTemp=(Vector)lovTasas.remove(0);
								pdfDoc.setCell("Modalidad 1 (Riesgo Empresa de Primer Orden )", "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell(tasasTemp.get(10).toString(), "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell(tasa, "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell(tasasTemp.get(3).toString(), "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell(tasasTemp.get(1).toString(), "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell(Comunes.formatoDecimal(Double.parseDouble(tasasTemp.get(4).toString()),4), "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell((tasasTemp.get(12)    != null) ?   tasasTemp.get(12).toString() :"", "formasmen", ComunesPDF.CENTER);
								if(ctTasa.equals("N")){
										pdfDoc.setCell((tasasTemp.get(11)    != null) ?   tasasTemp.get(11).toString() :"", "formasmen", ComunesPDF.CENTER);
									}else{
										pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
									}
							}
							pdfDoc.addTable();
							pdfDoc.endDocument();
				}else{
					tasasTemp=(Vector)lovTasas.get(0);
							pdfDoc.addText("Esquema de tasas para operaciones con Modalidad 2 (Riesgo Distribuidor)","formas",ComunesPDF.CENTER);
							List lEncabezados = new ArrayList();
							lEncabezados.add("Tipo de Crédito");
							lEncabezados.add("Distribuidor");
							lEncabezados.add("Referencia");
							lEncabezados.add("Plazo");
							lEncabezados.add("Tipo Tasa");
							lEncabezados.add("Valor");
							lEncabezados.add("Fecha");
							lEncabezados.add("Valor Tasa");
							pdfDoc.setTable(lEncabezados, "formasmenB", 100);
							
							while(!lovTasas.isEmpty()){
								tasasTemp=(Vector)lovTasas.remove(0);
								pdfDoc.setCell("Modalidad 2 (Riesgo Distribuidor)", "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell(tasasTemp.get(10).toString(), "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell(tasa, "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell(tasasTemp.get(3).toString(), "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell(tasasTemp.get(1).toString(), "formasmen", ComunesPDF.CENTER);
								pdfDoc.setCell(Comunes.formatoDecimal(Double.parseDouble(tasasTemp.get(4).toString()),4), "formasmen", ComunesPDF.CENTER);
								
								pdfDoc.setCell((tasasTemp.get(8)    != null) ?   tasasTemp.get(8).toString() :"", "formasmen", ComunesPDF.CENTER);
								
								//if(ctTasa.equals("P")){
										pdfDoc.setCell((tasasTemp.get(11)    != null) ?   tasasTemp.get(11).toString() :"", "formasmen", ComunesPDF.CENTER);
								//	}else{
								//		pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
								//	}
								
							}
							pdfDoc.addTable();
							pdfDoc.endDocument();
				
				
				}
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					infoRegresar=jsonObj.toString();
				
				}
	
}
%>

<%=infoRegresar%>