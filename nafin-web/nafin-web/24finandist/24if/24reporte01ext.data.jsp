<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,	net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("CatalogoEstatusDist")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_docto");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_docto");
	cat.setValoresCondicionIn("3,11,22,24", Integer.class);
	//cat.setOrden("1");
	List lista = cat.getListaElementos();
	
	CatalogoSimple cat_b = new CatalogoSimple();
	cat_b.setCampoClave("ic_estatus_solic");
	cat_b.setCampoDescripcion("cd_descripcion");
	cat_b.setTabla("comcat_estatus_solic");
	cat_b.setValoresCondicionIn("1,2,3,4", Integer.class);
	//cat.setOrden("1");
	List lista_b = cat_b.getListaElementos();

	JSONArray jsonArr = new JSONArray();

	Iterator it = lista.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			String clave = ec.getClave();
			ec.setClave("D"+clave);
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}

	Iterator it_b = lista_b.iterator();
	while(it_b.hasNext()) {
		Object obj = it_b.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			String clave = ec.getClave();
			ec.setClave("C"+clave);
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}

	infoRegresar = "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

}else if (informacion.equals("Consulta") ){

	String ic_estatus_docto	=	"";
	String tipoSolic			=	"";
	String ic_estatus	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
	if(!"".equals(ic_estatus)){
		ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
		tipoSolic		 = ic_estatus.substring(0,1);
	}

	if(iNoCliente != null && !iNoCliente.equals("") && !"".equals(ic_estatus)) {
		RepCreditosEstatusIf reporte = new RepCreditosEstatusIf();
		reporte.setClaveIf(iNoCliente);
		reporte.setClaveEstatus(ic_estatus_docto);
		reporte.setTipoSolic(tipoSolic);
		reporte.setQrysentencia();
		Registros registros = reporte.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
}

%>
<%=infoRegresar%>