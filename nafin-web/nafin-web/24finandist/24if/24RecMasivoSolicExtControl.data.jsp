<%@ page
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		com.netro.distribuidores.*,
		netropology.utilerias.*,
		netropology.utilerias.caracterescontrol.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	=	"";
	if (informacion.equals("validaCaracteresControl")) {

		JSONObject	resultado	= new JSONObject();
		String nombreArchivo 	= (request.getParameter("nombreArchivo")	== null)?"":request.getParameter("nombreArchivo");
		System.out.println("ANTES DEL HILO");
			System.out.println("Directorio "+strDirectorioTemp+nombreArchivo);
		session.removeAttribute("ValidaCaracteresControl");
		BuscaCaracteresControlThread ValidaCaractCon= new BuscaCaracteresControlThread();
		
		ResumenBusqueda resCaractCon = new ResumenBusqueda();
		
		ValidaCaractCon.setRutaArchivo(strDirectorioTemp+nombreArchivo);
		ValidaCaractCon.setRegistrador(resCaractCon);
		ValidaCaractCon.setCreaArchivoDetalle(true);
		ValidaCaractCon.setDirectorioTemporal(strDirectorioTemp);
		ValidaCaractCon.setSeparadorCampo("|");
		new Thread(ValidaCaractCon).start();
		session.setAttribute("ValidaCaracteresControl", ValidaCaractCon);
	//ValidaCaractCon.getMensaje();
		resultado.put("success",new Boolean(true));
		resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_THREAD"		);
		infoRegresar = resultado.toString();
		System.out.println("TERMINO Y EMPEZO EL THREAD ");
	
	}else if (    informacion.equals("avanceThreadCaracteres") 				)	{
		JSONObject	resultado	= new JSONObject();
		BuscaCaracteresControlThread ValidaCaractCon = (BuscaCaracteresControlThread)session.getAttribute("ValidaCaracteresControl");
		String[] mensa = {""};
		ResumenBusqueda resCaractCon = new ResumenBusqueda(); 
		int estatusThread = ValidaCaractCon.getStatus(mensa);
		System.out.println("ESTATUS "+estatusThread);
		if(estatusThread==200){
			resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_THREAD");	
		}else if(estatusThread==300 ){
			resCaractCon = (ResumenBusqueda)ValidaCaractCon.getRegistrador();
			boolean hayCaracteres = resCaractCon.hayCaracteresControl();
			if(hayCaracteres==true){
				resultado.put("estadoSiguiente", "HAY_CARACTERES_CONTROL");
				resultado.put("mns",resCaractCon.getMensaje());
				resultado.put("nombreArchivoCaractEsp",resCaractCon.getNombreArchivoDetalle());
				
			}else{
				resultado.put("estadoSiguiente", "VALIDA_CARGA_DOCTO");
				
			}
		}else if(estatusThread==400 ){
			resultado.put("estadoSiguiente", "ERROR_THREAD_CARACTERES");	
			resultado.put("mns", "Error en el archivo, favor de verificarlo");	
		}
		resultado.put("success",new Boolean(true));
		infoRegresar = resultado.toString();
		System.out.println("TERMINO Y EMPEZO EL THREAD "+infoRegresar);
	
	
}else if (informacion.equals("descargaArchivoDetalle")	){
		String nombreArchivo 	= (request.getParameter("archivo")	== null)?"":request.getParameter("archivo");
		JSONObject jsonObj3 = new JSONObject();
		jsonObj3.put("success", new Boolean(true));
		jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj3.toString();
}else if (informacion.equals("descargaArchivoDetalle")	){
		String nombreArchivo 	= (request.getParameter("archivo")	== null)?"":request.getParameter("archivo");
		JSONObject jsonObj3 = new JSONObject();
		jsonObj3.put("success", new Boolean(true));
		jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj3.toString();
}

	
%>
<%=infoRegresar%>

