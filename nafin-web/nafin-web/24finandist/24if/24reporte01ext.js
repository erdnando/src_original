/*
*Reporte de IF, By Hugo VC
*/
Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

	var cveEstatus	=	"";

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdf');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		if (arrRegistros != null) {
			Ext.getCmp('btnBajarPdf').hide();
			Ext.getCmp("btnBajarArchivo").hide();
			var btnGenerarArchivo = Ext.getCmp("btnGenerarArchivo");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnTotales = Ext.getCmp('btnTotales');
			if (!grid.isVisible()) {
				grid.show();
			}
			var cm = grid.getColumnModel();
			cm.setHidden(cm.findColumnIndex('CG_NUMERO_PAGO'), true);
			cm.setHidden(cm.findColumnIndex('FECHA_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('CT_CAMBIO_MOTIVO'), true);

			if (opts.params.ic_estatus_docto == "D3"){
				grid.setTitle('Estatus: Seleccionado Pyme');
			}else if (opts.params.ic_estatus_docto == "C1"){
				grid.setTitle('Estatus: Seleccionado IF / Operado');
			}else if (opts.params.ic_estatus_docto == "C2"){
				grid.setTitle('Estatus: En Proceso Nafin');
			}else if (opts.params.ic_estatus_docto == "C3"){
				grid.setTitle('Estatus: Operado');
			}else if (opts.params.ic_estatus_docto == "C4"){
				grid.setTitle('Estatus: Rechazada Nafin');
			}else if (opts.params.ic_estatus_docto == "D11"){
				grid.setTitle('Estatus: Operado Pagado');
			}else if (opts.params.ic_estatus_docto == "D22"){
				grid.setTitle('Estatus: Pendiente de pago IF');
			}else if (opts.params.ic_estatus_docto == "D20"){
				grid.setTitle('Estatus: Rechazado IF');
			}else if(opts.params.ic_estatus_docto == "D24"){
				grid.setTitle('En Proceso de Autorizaci�n IF');
			}

			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				if (opts.params.ic_estatus_docto == "D11")	{
					cm.setColumnHeader(cm.findColumnIndex('CG_NUMERO_PAGO'), "N�mero de pago");
					cm.setColumnTooltip(cm.findColumnIndex('CG_NUMERO_PAGO'), "N�mero de pago");
					cm.setHidden(cm.findColumnIndex('CG_NUMERO_PAGO'), false);
				}else if (opts.params.ic_estatus_docto == "D22")	{
					cm.setColumnHeader(cm.findColumnIndex('CG_NUMERO_PAGO'), "N�mero de pago de inter�s relacionado");
					cm.setColumnTooltip(cm.findColumnIndex('CG_NUMERO_PAGO'), "N�mero de pago de inter�s relacionado");
					cm.setHidden(cm.findColumnIndex('CG_NUMERO_PAGO'), false);
				}else if (opts.params.ic_estatus_docto == "D20")	{
					cm.setHidden(cm.findColumnIndex('FECHA_CAMBIO'), false);
					cm.setHidden(cm.findColumnIndex('CT_CAMBIO_MOTIVO'), false);
				}

				btnGenerarArchivo.setHandler(
					function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params:	{ic_estatus_docto:	opts.params.ic_estatus_docto},
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				);

				btnGenerarPDF.setHandler(
					function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {ic_estatus_docto:	opts.params.ic_estatus_docto},
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				);

				var sumMontoMN=0, montoValuadoMN=0,	montoCreditoMN=0;
				var sumMontoDL=0,	montoValuadoDL=0,	montoCreditoDL=0;
				var countMN = 0;
				var countDL = 0;
				var montoDescontar=0, montoValuado=0;

				var regTdM = totalesData.getAt(0);
				var regTdD = totalesData.getAt(1);
				regTdM.set('NOMMONEDA','');
				regTdM.set('TOTAL_REGISTROS','');
				regTdM.set('TOTAL_MONTO','');
				regTdM.set('TOTAL_MONTO_VALUADO','');
				regTdM.set('TOTAL_MONTO_CREDITO','');
				regTdD.set('NOMMONEDA','');
				regTdD.set('TOTAL_REGISTROS','');
				regTdD.set('TOTAL_MONTO','');
				regTdD.set('TOTAL_MONTO_VALUADO','');
				regTdD.set('TOTAL_MONTO_CREDITO','');

				consultaData.each(function(registro){
					montoDescontar = (parseFloat(registro.get('FN_MONTO'))*(parseFloat(registro.get('FN_PORC_DESCUENTO'))/100)	);
					montoValuado	= (parseFloat(registro.get('FN_MONTO'))-montoDescontar)*parseFloat(registro.get('TIPO_CAMBIO'));

					if (registro.get('IC_MONEDA') === "1"){
						countMN++;
						sumMontoMN += parseFloat(registro.get('FN_MONTO'));
						montoValuadoMN += montoValuado;
						montoCreditoMN +=	parseFloat(registro.get('MONTO_CREDITO'));
					}else	{
						countDL++;
						sumMontoDL += parseFloat(registro.get('FN_MONTO'));
						if(	!(registro.get('IC_MONEDA') == (registro.get('MONEDA_LINEA'))) ){
							montoValuadoDL += montoValuado;
						}
						montoCreditoDL +=	parseFloat(registro.get('MONTO_CREDITO'));
					}
				});
				if (countMN > 0){
					var regMN = totalesData.getAt(0);
					regMN.set('NOMMONEDA','Moneda Nacional');
					regMN.set('TOTAL_REGISTROS',countMN);
					regMN.set('TOTAL_MONTO',sumMontoMN);
					regMN.set('TOTAL_MONTO_VALUADO',"");	//montoValuadoMN
					regMN.set('TOTAL_MONTO_CREDITO',montoCreditoMN);

					if(countDL > 0){
						var regDL = totalesData.getAt(1);
						regDL.set('NOMMONEDA','Total Dolares');
						regDL.set('TOTAL_REGISTROS',countDL);
						regDL.set('TOTAL_MONTO',sumMontoDL);
						regDL.set('TOTAL_MONTO_VALUADO',montoValuadoDL);
						regDL.set('TOTAL_MONTO_CREDITO',montoCreditoDL);
					}
				}else if(countDL > 0){
					var regDL = totalesData.getAt(0);
					regDL.set('NOMMONEDA','Total Dolares');
					regDL.set('TOTAL_REGISTROS',countDL);
					regDL.set('TOTAL_MONTO',sumMontoDL);
					regDL.set('TOTAL_MONTO_VALUADO',montoValuadoDL);
					regDL.set('TOTAL_MONTO_CREDITO',montoCreditoDL);
				}

				btnTotales.enable();
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var totalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO'},
			{name: 'TOTAL_MONTO_CREDITO'},
			{name: 'TOTAL_MONTO_VALUADO'}
		],
		data:[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''},
				{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_MONTO_VALUADO':'','TOTAL_MONTO_CREDITO':''}],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData = new Ext.data.JsonStore({
		root:'registros',	url:'24reporte01ext.data.jsp',	
		baseParams:{informacion:'Consulta'},	
		totalProperty:'total',	
		messageProperty:'msg',	
		autoLoad:false,
		fields: [
			{name: 'NOMBRE_DIST'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_CARGA',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO',	type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO',type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'MONEDA_LINEA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBRE_EPO'},
			{name: 'TIPO_COBRANZA'},
			{name: 'TIPO_CREDITO'},
			{name: 'MONTO_CREDITO',type: 'float'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'TIPO_COBRO_INT'},
			{name: 'REFERENCIA_INT'},
			{name: 'VALOR_TASA_INT'},
			{name: 'MONTO_TASA_INT',type: 'float'},
			{name: 'CG_NUMERO_PAGO'},
			{name: 'CT_CAMBIO_MOTIVO'},
			{name: 'FECHA_SELECCION',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC_CREDITO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_OPER_IF',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_CAMBIO',	type: 'date', dateFormat: 'd/m/Y'}
		],
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var catalogoEstatus = new Ext.data.JsonStore({
		root:'registros',
		fields:['clave', 'descripcion', 'loadMsg'],
		url:'24reporte01ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEstatusDist'	},
		totalProperty:'total',	autoLoad:false,	listeners:{exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo}
	});

	var gridTotales = {
		xtype:'grid',	store:totalesData,	id:'gridTotales',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:940,	height:100,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 100,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),menuDisabled:true
			},{
				header: 'Monto Autorizado',dataIndex: 'TOTAL_MONTO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Valuado',	dataIndex: 'TOTAL_MONTO_VALUADO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Cr�dito',	dataIndex: 'TOTAL_MONTO_CREDITO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	};

	var grid = new Ext.grid.GridPanel({
		store: consultaData,	stripeRows: true,	loadMask:true,	height:400,	width:940,	frame:false, hidden:true, header:true,
		columns: [
			{
				header:'EPO', tooltip:'Nombre EPO',	dataIndex:'NOMBRE_EPO',	sortable:true,	resizable:true,	width:200,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Distribuidor', tooltip: 'Distribuidor',	dataIndex:'NOMBRE_DIST',	sortable:true,	resizable:true,	width:200,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'N�m. acuse de carga', tooltip: 'N�mero acuse de carga',	dataIndex:'CC_ACUSE',	sortable:true,	width:120,	resizable:true
			},{
				header:'N�mero de documento inicial', tooltip:'N�mero de documento inicial',	dataIndex:'IG_NUMERO_DOCTO',	sortable:true, width:120
			},{
				header:'Fecha de Emisi�n', tooltip:'Fecha de Emisi�n',	dataIndex:'DF_FECHA_EMISION',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de Vencimiento', tooltip:'Fecha de Vencimiento',	dataIndex:'DF_FECHA_VENC',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha de publicaci�n', tooltip:'Fecha de publicaci�n',	dataIndex:'DF_CARGA',	sortable:true,	width:100,	align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Plazo docto.', tooltip:'Plazo docto.',	dataIndex:'IG_PLAZO_DOCTO',	sortable:true,	width:120, align:"center"
			},{
				header:'Moneda', tooltip: 'Moneda',	dataIndex:'MONEDA',	sortable:true,	width:130,align:"center"
			},{
				header:'Monto', tooltip:'Monto',	dataIndex:'FN_MONTO',	sortable:true,	width:120,	align:'right',	renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Plazo para descuento en d�as', tooltip:'Plazo para descuento en d�as',	dataIndex:'IG_PLAZO_DESCUENTO',	sortable:true,	width:120,align:"center"
			},{
				header:'% de Descuento',	tooltip: 'Porcentaje de Descuento', dataIndex:'FN_PORC_DESCUENTO',	sortable:true,	width:100, align:'right',
				renderer:Ext.util.Format.numberRenderer('0.00 %')
			},{
				header:'Monto % de descuento', tooltip: 'Monto % de descuento',	dataIndex:'',	sortable:true,	width:120,	align:'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											value = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
											return Ext.util.Format.number(value, '$ 0,0.00');
										}
			},{
				header:'Tipo conv.', tooltip: 'Tipo conv.',	dataIndex:'TIPO_CONVERSION',	sortable:true,	width:120,	align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}
											return value;
										}
			},{	// ? 
				header:'Tipo cambio', tooltip:'Tipo cambio',	dataIndex:'TIPO_CAMBIO',	sortable:true,	width:120,	align:"center",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}else{
												value = Ext.util.Format.number(value, '$ 0,0.00');
											}
											return value;
										}
			},{
				header:'Monto valuado', tooltip: 'Monto valuado',	dataIndex:'',	sortable:true,	width:120,	align:"right",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == registro.get('MONEDA_LINEA'))	{
												value = "";
											}else{
												var montoDescontar = (registro.get('FN_MONTO')*(registro.get('FN_PORC_DESCUENTO')/100));
												var valor = (registro.get('FN_MONTO')-montoDescontar)*registro.get('TIPO_CAMBIO');
												value = Ext.util.Format.number(valor, '$ 0,0.00');
											}
											return value;
										}
			},{
				header:'Modalidad de plazo', tooltip: 'Modalidad de plazo',	dataIndex:'MODO_PLAZO',	sortable:true,	width:100,	align:"center"
			},{
				header: 'N�mero de documento final', tooltip: 'N�mero de documento final',	dataIndex: 'IC_DOCUMENTO',	sortable:true,	width:120,	resizable:true,	align:"center"
			},{
				header:'Monto', tooltip: 'Monto',	dataIndex:'MONTO_CREDITO',	sortable:true,	width:120,	align:"right",	hidden: false,	renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header:'Plazo', tooltip: 'Plazo',	dataIndex:'IG_PLAZO_CREDITO',	sortable:true,	width:120,	align:"center"
			},{
				header:'Fecha de Vencimiento', tooltip:'Fecha de Vencimiento',	dataIndex:'FECHA_VENC_CREDITO',align:"center",	sortable:true,	width:100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Fecha operaci�n distribuidor', tooltip: 'Fecha de operaci�n distribuidor',	dataIndex:'FECHA_SELECCION',	sortable:true,	width:150, align:"center",renderer:Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header:'Referencia tasa de inter�s', tooltip: 'Referencia tasa de inter�s',	dataIndex:'REFERENCIA_INT',	sortable:true,	width:120
			},{
				header:'Valor tasa de inter�s', tooltip: 'Valor tasa de inter�s',	dataIndex:'VALOR_TASA_INT',	sortable:true,	width:120,	hidden:false,	align:"center",	renderer:Ext.util.Format.numberRenderer('0.0%')
			},{
				header:'Monto de intereses', tooltip: 'Monto de intereses',	dataIndex:'MONTO_TASA_INT',	sortable:true,	width:120,	hidden:false,	align:"right",	renderer:Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Monto total de Capital e Inter�s', tooltip:'Monto total de Capital e Inter�s',	sortable:true,	width:120,	align:"right",
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									value = 0;
									var monto_cred=0;
									var monto_tasa=0;
									if ( !Ext.isEmpty(registro.get('MONTO_CREDITO')) ){
										monto_cred = parseFloat(registro.get('MONTO_CREDITO'));
									}
									if (!Ext.isEmpty(registro.get('MONTO_TASA_INT')) ){
										monto_tasa  = parseFloat(registro.get('MONTO_TASA_INT'));
									}
									value = monto_cred + monto_tasa;
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},{	//N�mero de pago � N�mero de pago de inter�s relacionado
				header:'', tooltip: '',	dataIndex:'CG_NUMERO_PAGO',	sortable:true,	width:120, headeable:false, hidden:true,	align:"right"
			},{
				header:'Fecha de rechazo', tooltip: 'Fecha de rechazo',	dataIndex:'FECHA_CAMBIO',	sortable:true,	width:130, align:"center",	renderer:Ext.util.Format.dateRenderer('d/m/Y'),headeable:false, hidden:true
			},{
				header:'Causa', tooltip: 'Causa',	dataIndex:'CT_CAMBIO_MOTIVO',	sortable:true,	width:120, headeable:false, hidden:true
			}
		],
		bbar: {
			items: [
				{
					text:'Totales', id: 'btnTotales',
					handler: function(boton, evento) {
									var totalesCmp = Ext.getCmp('gridTotales');
									if (!totalesCmp.isVisible()) {
										totalesCmp.show();
										totalesCmp.el.dom.scrollIntoView();
									}
								}
				},'->','-',
				{
					xtype:'button',	text:'Generar Archivo',	tooltip:'Generar archivo CSV',	id: 'btnGenerarArchivo'
				},{
					xtype:'button',	text:'Bajar Archivo',	tooltip:'Descargar archivo CSV',	id:'btnBajarArchivo',	hidden: true
				},'-',{
					xtype:'button',	text:'Generar PDF',	tooltip:'Generar archivo PDF',	id:'btnGenerarPDF'
				},{
					xtype: 'button',	text:'Bajar PDF',	tooltip:'Descargar archivo PDF',	id:'btnBajarPdf',	hidden:true
				}
			]
		}
	});

	var elementosForma = [
		{
			xtype:'combo',
			name:	'ic_estatus_docto',
			id:	'_ic_estatus_docto',
			hiddenName:	'ic_estatus_docto',
			fieldLabel:	'Estatus',
			emptyText:	'Seleccione Estatus . . .',
			forceSelection:true,	triggerAction:'all',	typeAhead: true,	minChars:1,	mode: 'local',	displayField:'descripcion',	valueField:'clave',
			store:catalogoEstatus,	tpl:NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
							cveEstatus = combo.value;
							grid.hide();
							Ext.getCmp('gridTotales').hide();
							fp.el.mask('Enviando...', 'x-mask-loading');
							if(!Ext.isEmpty(cveEstatus)){
								consultaData.load({params:	{ic_estatus_docto:cveEstatus}});
							}
					}
				}
			}
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		frame:true,
		labelWidth: 60,
		style: ' margin:0 auto;',
		title: '<div><center>Cr�ditos por Estatus</div>',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items:	elementosForma,
		monitorValid: true
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(20),
			grid,	gridTotales
		]
	});

	catalogoEstatus.load();

});//Fin de funcion principal