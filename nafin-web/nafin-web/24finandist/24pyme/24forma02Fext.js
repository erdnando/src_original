var texto1 = ['Se deber�n seleccionar el IF y plazo de financiamiento para obtener cr�dito de los siguientes documentos'];
var cadEpoCemex = ['En este acto manifiesto mi aceptaci�n para que sea(n) financiados el(los) DOCUMENTO(S) que han sido publicados, mismo(s) que contiene(n) Derechos de Cobro a mi cargo y a favor de la Empresa de Primer Orden por lo que en esta misma fecha me obligo a cubrir al Intermediario Financiero, los intereses que se detallan en esta pantalla. Manifiesto tambi�n mi obligaci�n y aceptaci�n de pagar el 100% del valor del(los) DOCUMENTO(S) en la fecha de vencimiento al Intermediario Financiero.'];
var cadEpo = ['En este acto manifiesto mi aceptaci�n para que sea(n) descontado(s) el(los) DOCUMENTO(S) que he seleccionado, mismo(s) que contiene(n) Derechos de Cobro a mi cargo y a favor de la Empresa de Primer Orden por lo que en esta misma fecha me obligo a cubrir al Intermediario Financiero, los intereses que se detallan en esta pantalla.Manifiesto tambi�n mi obligaci�n y aceptaci�n de pagar el 100% del valor del(los) DOCUMENTO(S) en la fecha de vencimiento al Intermediario Financiero.'];
var texto4 = ['NOTA: Las columnas con subIndice 1 son de Datos Documento Inicial   y subIndice 2 son de Datos Documento Final '];

var texto6 = ['En este acto manifiesto mi aceptaci�n para que sea(n) descontado(s) el(los) DOCUMENTO(S) que he seleccionado,',
'mismo(s) que contiene(n) Derechos de Cobro a mi cargo y a favor de la Empresa de Primer Orden por lo que en esta misma fecha me obligo a cubrir al Intermediario Financiero, los intereses que se detallan en esta pantalla.',
'Manifiesto tambi�n mi obligaci�n y aceptaci�n de pagar el 100% del valor del(los) DOCUMENTO(S) en la fecha de vencimiento al Intermediario Financiero. '
];

Ext.onReady(function() {

	//variables Globales
	var hidNumLineas;
	var hidNumTasas; 
	var Auxiliar;
	var fechaHoy = Ext.getDom('fechaHoy').value;	
	var diasInhabiles  = Ext.getDom('diasInhabiles').value;	
	var parTipoCambio = Ext.getDom('parTipoCambio').value;	
	var modificado = [];
	var ic_documento = [];
	var ic_moneda_docto = [];
	var ic_moneda_linea = [];
	var monto = [];
	var monto_valuado = [];
	var monto_credito = [];
	var monto_tasa_int = [];
	var monto_descuento = [];
	var plazo = [];
	var fecha_vto = [];
	var referencia_tasa = [];
	var valor_tasa_puntos = [];
	var ic_tasa = [];
	var cg_rel_mat = [];
	var fn_puntos = [];
	var ic_linea_credito = [];	
	var totalInteresMN =0;
	var totalInteresUSD=0;
	var totalCapitalInteresMN =0;
	var totalCapitalInteresUSD=0;
	var Noelementos=0; 
	var numDoctos =0;	
	var montoSeleccionado =0;
	var saldoDisponible =0;
	var totalDoctosMN =0;
	var totalMontoMN =0;
	var totalMontoDescMN =0;
	var totalInteresMN2=0;
	var totalMontoCreditoMN =0;	
	var totalDoctosUSD=0;
	var totalMontoUSD=0;
	var totalMontoImpUSD=0;
	var totalMontoDescUSD =0;
	var totalInteresUSD2=0;
	var totalMontoCreditoUSD =0;	
	var totalDoctosConv=0;
	var totalMontosConv=0;
	var totalMontoImpConv=0;
	var totalMontoDescConv=0;
	var totalMontoIntConv=0;
	var totalMontoCreditoConv =0;
	var totalInteresConv =0;
	var moneda; 
	var totalDoctos =0; 
	var totalMonto  =0;
	var totalDoctosF  =0;
	var totalMontoF  =0;
	var totalInteresF  =0;
	var monedaUS;
	var totalDoctosUSD  =0;
	var totalMontoUSD   =0;
	var totalDoctosFUSD   =0; 
	var totalMontoFUSD  =0;
	var totalInteresFUSD  =0;
	var totalMontoInteresF =0;
	var totalInteresF  =0;
	var totalConvPesos =0;
	var totalMontoInteresFUSD =0;	
	var totalMontoRecibirN=0;
	var totalMontoRecibirUSD=0;
	

	var cancelar =  function() {  
		modificado = [];
		ic_documento = [];
		ic_moneda_docto = [];
		ic_moneda_linea = [];
		monto = [];
		monto_valuado = [];
		monto_credito = [];
		monto_tasa_int = [];
		monto_descuento = [];
		plazo = [];
		fecha_vto = [];
		referencia_tasa = [];
		valor_tasa_puntos = [];
		ic_tasa = [];
		cg_rel_mat = [];
		fn_puntos = [];	 
		ic_linea_credito= [];	
	}
		
	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
		
	var mensajeAutorizacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutorizacion',							
		width:	'350',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 	xtype: 'displayfield',  id: 'mensajeAuto', 	value: '' }		
		]
	});
	
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'150',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{
				xtype: 'button',
				text: 'Salir',			
				iconCls: 'icoLimpiar',
				id: 'btnSalir',		 
				handler: function() {
					window.location = '24forma02Fext.jsp';
				}
			}	
		]
	});
	
	var procesarSuccessFailureGenerarCSVAcuse =  function(opts, success, response) {
		var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
		btnGenerarCSVAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSVAcuse = Ext.getCmp('btnBajarCSVAcuse');
			btnBajarCSVAcuse.show();
			btnBajarCSVAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSVAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarCSVAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPDFAcuse =  function(opts, success, response) {
		var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
		btnGenerarPDFAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			btnBajarPDFAcuse.show();
			btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPDFAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		fields: [
			{name: 'etiqueta'},
			{name: 'informacion'}
		]
	 });
	 
	//Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 150,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 150,
		style: 'margin:0 auto;',		
		title: 'Cifras de Control',
		frame: true
	});	 
// *********************grid del PreAcuse ***********************
	var consultaAcuseData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'NO_DOCUMENTO'},
			{name: 'NO_ACUSE'},				
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_VENCIMIENTO',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_PUBLICACION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'PLAZO_DOCTO',type: 'float' },
			{name: 'MONEDA' },
			{name: 'IC_MONEDA_DOCTO' },
			{name: 'IC_MONEDA_LINEA' },
			{name: 'IC_IF' },
			{name: 'TIPO_PISO' },				
			{name: 'MONTO' ,type: 'float' },
			{name: 'MONTO_DESCUENTO' ,type: 'float'},
			{name: 'MONTO_VALUADO' ,type: 'float'},
			{name: 'NO_DOCTO_FINAL' },
			{name: 'MONEDA_FINAL' },
			{name: 'MONTO_FINAL' ,type: 'float'},
			{name: 'MONTO_CREDITO' ,type: 'float'},
			{name: 'PLAZO_FINAL' ,type: 'float'},
			{name: 'FECHA_VENC_FINAL',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_HOY',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_OPERA_FINAL' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMBRE_IF' },
			{name: 'IC_LINEA_CREDITO' },				
			{name: 'REFERENCIA' },
			{name: 'IC_TASA' },
			{name: 'CG_REL_MAT' },
			{name: 'FN_PUNTOS' },
			{name: 'VALOR_TASA' },
			{name: 'VALOR_TASA_PUNTOS' ,type: 'float'},
			{name: 'MONTO_TASA_INTERES',type: 'float' },
			{name: 'MONTO_AUX_INTERES',type: 'float' },
			{name: 'MONTO_TOTAL_CAPITAL' ,type: 'float'},
			{name: 'PLAZO_UNICO' ,type: 'float'},
			{name: 'SELECCION' },
			{name: 'DIAS_MINIMO' ,type: 'float'},		
			{name: 'DIAS_MAXIMO' ,type: 'float'},
			{name: 'PLAZO' ,type: 'float'},
			{name: 'PLAZO_VALIDO' ,type: 'float'},
			{name: 'MONTO_RECIBIR' ,type: 'float'},
			{name: 'CUENTA_BANCARIA' ,type: 'float'}	
		]
	});
	
	var gruposAcuse = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Datos Documento Inicial', colspan: 9, align: 'center'},
				{header: 'Datos Documento Final', colspan: 12, align: 'center'}					
			]
		]
	});	
	
	var gridAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridAcuse',
		title: 'Acuse de Selecci�n de Factoraje con Recurso',
		hidden: true,
		store: consultaAcuseData,
		clicksToEdit: 1,
		plugins: gruposAcuse,
		columns: [						
			{
				header: 'Epo',
				tooltip: 'Epo',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex: 'NO_ACUSE',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{
				header: 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex: 'NO_DOCTO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Monto cr�dito',
				tooltip: 'Monto cr�dito',
				dataIndex: 'MONTO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Plazo', // es es editable 
				tooltip: 'Plazo',
				dataIndex: 'PLAZO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Fecha de vencimiento', // es es editable 
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de operaci�n',
				tooltip: 'Fecha de operaci�n',
				dataIndex: 'FECHA_OPERA_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_PUNTOS',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},			
			{
				header: 'Monto de intereses',
				tooltip: 'Monto de intereses',
				dataIndex: 'MONTO_TASA_INTERES',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{
				header: 'Monto Recibir',
				tooltip: 'Monto Recibir',
				dataIndex: 'MONTO_RECIBIR',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Cuenta Bancaria Pyme',
				tooltip: 'Cuenta Bancaria Pyme',
				dataIndex: 'CUENTA_BANCARIA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'		
			}					
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [
				'-',
				'->',
				{
					xtype: 'button',
					iconCls: 'icoPdf',
					text: 'Generar PDF',
					id: 'btnGenerarPDFAcuse'											
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					iconCls: 'icoPdf',
					id: 'btnBajarPDFAcuse',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					iconCls: 'icoXls',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVAcuse'											
				},			
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					iconCls: 'icoXls',
					id: 'btnBajarCSVAcuse',
					hidden: true
				},
				'-',
				{
					text: 'Salir',
					xtype: 'button',					
					iconCls: 'icoLimpiar',
					id: 'btnSalirA',
					handler: function() {						
						window.location = '24forma02Fext.jsp';
					}
				}				
			]
		}
	});
	
	// muestra la pantalla de acuse 
	var  procesarSuccessFailureAcuse =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			
			if (jsondeAcuse != null){
				
				var Comfirmacionclave = Ext.getCmp('Comfirmacionclave');
				if (Comfirmacionclave) {		
					Comfirmacionclave.destroy();		
				}																	
				if(jsondeAcuse.acuse!='') {
											
					var acuseCifras = [
						['N�mero de Acuse', jsondeAcuse.acuse],
						['Fecha de Carga', jsondeAcuse.fecCarga],
						['Hora de Carga', jsondeAcuse.horaCarga],
						['Usuario de Captura', jsondeAcuse.captUser]
					];
					
					storeCifrasData.loadData(acuseCifras);
					Ext.getCmp('gridMontos').show();
					Ext.getCmp('gridAcuse').show();
					Ext.getCmp('gridCifrasControl').show();
					Ext.getCmp('ctexto6').show();						
					Ext.getCmp('btnGenerarPDFAcuse').enable();						
					Ext.getCmp('btnGenerarCSVAcuse').enable();
					Ext.getCmp('gridPreAcuse').hide();
					Ext.getCmp('btnBajarPDFAcuse').hide();
					Ext.getCmp('btnBajarCSVAcuse').hide();	
					Ext.getCmp("acuse").setValue(jsondeAcuse.acuse);
				}else if(jsondeAcuse.acuse=='') {
				
					Ext.getCmp("mensajeAuto").setValue(jsondeAcuse.mensajeAuto);
					Ext.getCmp('mensajeAutorizacion').show();	
					Ext.getCmp('fpBotones').show();
					Ext.getCmp('gridPreAcuse').hide();
					Ext.getCmp('gridMontos').hide();
					Ext.getCmp('ctexto3').hide();
					Ext.getCmp('ctexto6').hide();	
					
				}
			
				//para el boton de PDF de Acuse 
				var btnGenerarAc = Ext.getCmp('btnGenerarPDFAcuse');
				btnGenerarAc.setHandler(function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');								
					Ext.Ajax.request({
						url: '24forma02FAr.jsp',
						params: Ext.apply(fp.getForm().getValues(),{						
							modificado:modificado,
							ic_documento:ic_documento,
							ic_moneda_docto:ic_moneda_docto,
							ic_moneda_linea:ic_moneda_linea,
							monto:monto,
							monto_valuado:monto_valuado,
							monto_credito:monto_credito,
							monto_tasa_int:monto_tasa_int,
							monto_descuento:monto_descuento,						
							plazo:plazo,
							fecha_vto:fecha_vto,
							referencia_tasa:referencia_tasa,
							valor_tasa_puntos:valor_tasa_puntos,
							ic_tasa:ic_tasa,
							cg_rel_mat:cg_rel_mat,
							fn_puntos:fn_puntos,						
							informacion:'ArchivoPDFAcuse',						
							tipoArchivo:'PDF',
							acuse:jsondeAcuse.acuse
						}),
						callback: procesarSuccessFailureGenerarPDFAcuse
					});
				});		
				
				//para el boton de CSV de Acuse 
				var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
				btnGenerarCSVAcuse.setHandler(function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');								
					Ext.Ajax.request({
					url: '24forma02FAr.jsp',
					params: Ext.apply(fp.getForm().getValues(),{	
						informacion:'ArchivoCSVAcuse',						
						tipoArchivo:'CSV',
						modificado:modificado,
						ic_documento:ic_documento,
						ic_moneda_docto:ic_moneda_docto,
						ic_moneda_linea:ic_moneda_linea,
						monto:monto,
						monto_valuado:monto_valuado,
						monto_credito:monto_credito,
						monto_tasa_int:monto_tasa_int,
						monto_descuento:monto_descuento,							
						plazo:plazo,
						fecha_vto:fecha_vto,
						referencia_tasa:referencia_tasa,
						valor_tasa_puntos:valor_tasa_puntos,
						ic_tasa:ic_tasa,
						cg_rel_mat:cg_rel_mat,
						fn_puntos:fn_puntos,
						acuse:jsondeAcuse.acuse
					}),
						callback: procesarSuccessFailureGenerarCSVAcuse
					});
				});
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//******************confirmacion **************************************
	function confirmacionClavesCesion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var bConfirma;
			jsondeConfirma = Ext.util.JSON.decode(response.responseText);
			if (jsondeConfirma != null){
				bConfirma= jsondeConfirma.bConfirma;								
			}		
			if(bConfirma ==false) {
				Ext.MessageBox.alert('Mensaje','La confirmaci�n es incorrecta, intente de nuevo');
				
			} else  if(bConfirma ==true) {		
				Ext.Ajax.request({
					url : '24forma02F.data.jsp',
					params : {
						informacion: 'GeneraAcuse',					
						modificado:modificado,
						ic_documento:ic_documento,
						ic_moneda_docto:ic_moneda_docto,
						ic_moneda_linea:ic_moneda_linea,
						monto:monto,
						monto_valuado:monto_valuado,
						monto_credito:monto_credito,
						monto_tasa_int:monto_tasa_int,
						monto_descuento:monto_descuento,
						plazo:plazo,
						fecha_vto:fecha_vto,
						referencia_tasa:referencia_tasa,
						valor_tasa_puntos:valor_tasa_puntos,
						ic_tasa:ic_tasa,
						cg_rel_mat:cg_rel_mat,
						fn_puntos:fn_puntos,
						ic_linea_credito:ic_linea_credito
					},
					callback: procesarSuccessFailureAcuse
				});
			}//bConfirma
		}//if
	}
	
	// *********************grid  Montos de PreAcuse y Acuse  ***********************	
	var storeMontosData = new Ext.data.ArrayStore({
		fields: [
			{name: 'totalDoctosMN', type: 'float'},
			{name: 'totalMontoMN', type: 'float'},
			{name: 'totalMontoSIntMN', type: 'float'},
			{name: 'totalInteresMN', type: 'float'},	
					
			{name: 'totalDoctosUSD', type: 'float'},			
			{name: 'totalMontoUSD', type: 'float'},
			{name: 'totalMontoSInUSD', type: 'float'},
			{name: 'totalInteresUSD', type: 'float'},
			
			{name: 'totalDoctosConv', type: 'float'},
			{name: 'totalMontosConv', type: 'float'},
			{name: 'totalMontoSinConv', type: 'float'},
			{name: 'totalInteresConv', type: 'float'}
		  ]
	 });	 
	 	
	var gruposMontos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Moneda nacional', colspan: 4, align: 'center'},
				{header: 'D�lares Americanos', colspan: 4, align: 'center'},				
				{header: 'Doctos. en DLL financiados en M.N.', colspan: 4, align: 'center'}					
			]
		]
	});

	var gridMontos = new Ext.grid.EditorGridPanel({	
		id: 'gridMontos',		
		store: storeMontosData,
		plugins: gruposMontos,
		hidden: true,
		columns: [		
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosMN',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos.',
				tooltip: 'Monto total de doctos.',
				dataIndex : 'totalMontoMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{							
				header : 'Monto del Financiamiento',
				tooltip: 'Monto del Financiamiento',
				dataIndex : 'totalMontoSIntMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{							
				header : 'Monto intereses',
				tooltip: 'Monto intereses',
				dataIndex : 'totalInteresMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},								
			//USD		
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosUSD',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos.',
				tooltip: 'Monto total de doctos.',
				dataIndex : 'totalMontoUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto del Financiamiento',
				tooltip: 'Monto del Financiamiento',
				dataIndex : 'totalMontoSInUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto intereses finales',
				tooltip: 'Monto intereses finales',
				dataIndex : 'totalInteresUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
		//Doctos. en DLL financiados en M.N.
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosConv',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos.',
				tooltip: 'Monto total de doctos.',
				dataIndex : 'totalMontosConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto del Financiamiento',
				tooltip: 'Monto del Financiamiento',
				dataIndex : 'totalMontoSinConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto intereses',
				tooltip: 'Monto intereses',
				dataIndex : 'totalInteresConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 150,
		width: 943,		
		frame: true	
	});
	
	// ********************* PreAcuse ***********************
	
	var procesarSuccessFailureGenerarPDFPreA =  function(opts, success, response) {
		var btnGenerarPDFA = Ext.getCmp('btnGenerarPDFPreAcuse');
		btnGenerarPDFA.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDFA.disable();
			var btnBajarPDFA = Ext.getCmp('btnBajarPDFPreAcuse');
			btnBajarPDFA.show();
			btnBajarPDFA.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDFA.focus();
			btnBajarPDFA.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFA.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var consultaPreAcuseData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'NO_DOCUMENTO'},
			{name: 'NO_ACUSE'},				
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_VENCIMIENTO',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_PUBLICACION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'PLAZO_DOCTO',type: 'float' },
			{name: 'MONEDA' },
			{name: 'IC_MONEDA_DOCTO' },
			{name: 'IC_MONEDA_LINEA' },
			{name: 'IC_IF' },
			{name: 'TIPO_PISO' },				
			{name: 'MONTO' ,type: 'float' },
			{name: 'MONTO_DESCUENTO' ,type: 'float'},
			{name: 'MONTO_VALUADO' ,type: 'float'},
			{name: 'NO_DOCTO_FINAL' },
			{name: 'MONEDA_FINAL' },
			{name: 'MONTO_FINAL' ,type: 'float'},
			{name: 'MONTO_CREDITO' ,type: 'float'},
			{name: 'PLAZO_FINAL' ,type: 'float'},
			{name: 'FECHA_VENC_FINAL',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_HOY',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_OPERA_FINAL' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMBRE_IF' },
			{name: 'IC_LINEA_CREDITO' },				
			{name: 'REFERENCIA' },
			{name: 'IC_TASA' },
			{name: 'CG_REL_MAT' },
			{name: 'FN_PUNTOS' },
			{name: 'VALOR_TASA' },
			{name: 'VALOR_TASA_PUNTOS' ,type: 'float'},
			{name: 'MONTO_TASA_INTERES',type: 'float' },
			{name: 'MONTO_AUX_INTERES',type: 'float' },
			{name: 'MONTO_TOTAL_CAPITAL' ,type: 'float'},
			{name: 'PLAZO_UNICO' ,type: 'float'},
			{name: 'SELECCION' },
			{name: 'DIAS_MINIMO' ,type: 'float'},		
			{name: 'DIAS_MAXIMO' ,type: 'float'},
			{name: 'PLAZO' ,type: 'float'},
			{name: 'PLAZO_VALIDO' ,type: 'float'},
			{name: 'MONTO_RECIBIR' ,type: 'float'},
			{name: 'CUENTA_BANCARIA' ,type: 'float'}			
		]
	});
	
	var gruposPreAcuse = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [ [
			{header: 'Datos Documento Inicial', colspan: 9, align: 'center'},
			{header: 'Datos Documento Final', colspan: 12, align: 'center'}					
		]]
	});	
	
	
	var gridPreAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridPreAcuse',
		title: 'PreAcuse de Selecci�n de Factoraje con Recurso',
		hidden: true,
		store: consultaPreAcuseData,
		clicksToEdit: 1,
		plugins: gruposPreAcuse,
		columns: [						
			{
				header: 'Epo',
				tooltip: 'Epo',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex: 'NO_ACUSE',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{
				header: 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex: 'NO_DOCTO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Monto cr�dito',
				tooltip: 'Monto cr�dito',
				dataIndex: 'MONTO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Plazo', // es es editable 
				tooltip: 'Plazo',
				dataIndex: 'PLAZO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Fecha de vencimiento', // es es editable 
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de operaci�n',
				tooltip: 'Fecha de operaci�n',
				dataIndex: 'FECHA_OPERA_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_PUNTOS',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},			
			{
				header: 'Monto de intereses',
				tooltip: 'Monto de intereses',
				dataIndex: 'MONTO_TASA_INTERES',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{
				header: 'Monto Recibir',
				tooltip: 'Monto Recibir',
				dataIndex: 'MONTO_RECIBIR',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Cuenta Bancaria Pyme',
				tooltip: 'Cuenta Bancaria Pyme',
				dataIndex: 'CUENTA_BANCARIA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'		
			}					
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [
				'-',
				'->',
				{
					xtype: 'button',
					text: 'Confirmar',
					iconCls: 'icoAceptar',
					id: 'btnConfirmarP'											
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					iconCls: 'icoPdf',
					id: 'btnGenerarPDFPreAcuse'											
				},				
				{
					xtype: 'button',
					text: 'Bajar PDF',
					iconCls: 'icoPdf',
					id: 'btnBajarPDFPreAcuse',
					hidden: true
				},
				'-',
				{
					text: 'Cancelar',
					iconCls: 'icoLimpiar',
					xtype: 'button',
					id: 'btnCancelarPre',
					handler: function() {						
						window.location = '24forma02Fext.jsp';
					}
				}
			]
		}
		
	});
	
	/******procesarConfirmar****************/
	var procesarConfirmar = function() {
		var  gridEditable = Ext.getCmp('gridEditable');
		var store = gridEditable.getStore();	
		cancelar(); //limpia las variables 
		var registrosPreAcu = []; //para agregar los registros al Preacuse  y acuse 		
		//se recorre grid principal para detectar los documentos seleccionados			
		store.each(function(record) {				
			if(record.data['SELECCION']=='S'){					
				//aqui van los parametros a recibir 
				modificado.push(record.data['NO_DOCTO_FINAL']);
				ic_documento.push(record.data['NO_DOCTO_FINAL']);
				ic_moneda_docto.push(record.data['IC_MONEDA_DOCTO']);
				ic_moneda_linea.push(record.data['IC_MONEDA_LINEA']);
				monto.push(record.data['MONTO']);
				monto_valuado.push(record.data['MONTO_VALUADO']);
				monto_credito.push(record.data['MONTO_CREDITO']);
				monto_tasa_int.push(record.data['MONTO_TASA_INTERES']);
				monto_descuento.push(record.data['MONTO_DESCUENTO']);
				plazo.push(record.data['PLAZO_FINAL']);							
				fecha_vto.push( Ext.util.Format.date(record.data['FECHA_VENC_FINAL'],'d/m/Y') ); 				
				referencia_tasa.push(record.data['REFERENCIA']);
				valor_tasa_puntos.push(record.data['VALOR_TASA_PUNTOS']);
				ic_tasa.push(record.data['IC_TASA']);
				cg_rel_mat.push(record.data['CG_REL_MAT']);
				fn_puntos.push(record.data['FN_PUNTOS']);
				ic_linea_credito.push(record.data['IC_LINEA_CREDITO']);	
				registrosPreAcu.push(record);	
			}						
		});
				
		//se le carga informacion al grid de preacuse y al grid de totales
		consultaPreAcuseData.add(registrosPreAcu);	
		consultaAcuseData.add(registrosPreAcu);
		
		if(modificado =='') {
			Ext.MessageBox.alert('Mensaje','Debe seleccionar por lo menos un documento para continuar');
			return false;	
		}			
		
		gridPreAcuse.show();
		gridEditable.hide();
		fp.hide();		
		gridMontos.show();
		gridMonitor.hide();
		gridTotales.hide();
		ctexto1.hide();
		ctexto4.hide();
		ctexto3.hide();	
		ctexto5.hide();
		ctexto6.show();
		
		//archivo PDF de Preacuse 
		var btnGenerar = Ext.getCmp('btnGenerarPDFPreAcuse');
		btnGenerar.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');								
				Ext.Ajax.request({
					url: '24forma02FAr.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ArchivoPDFPreAcuse',
						tipoArchivo:'PDF',
						modificado:modificado,
						ic_documento:ic_documento,
						ic_moneda_docto:ic_moneda_docto,
						ic_moneda_linea:ic_moneda_linea,
						monto:monto,
						monto_valuado:monto_valuado,
						monto_credito:monto_credito,
						monto_tasa_int:monto_tasa_int,
						monto_descuento:monto_descuento,							
						plazo:plazo,
						fecha_vto:fecha_vto,
						referencia_tasa:referencia_tasa,
						valor_tasa_puntos:valor_tasa_puntos,
						ic_tasa:ic_tasa,
						cg_rel_mat:cg_rel_mat,
						fn_puntos:fn_puntos		
					}),
					callback: procesarSuccessFailureGenerarPDFPreA
				});
			}
		);			
		//para el Preacuse 	
		var btnConfirmarP = Ext.getCmp('btnConfirmarP');			
		btnConfirmarP.setHandler(function(boton, evento) {	
			var  fLogCesion = new  NE.cesion.FormLoginCesion();
			fLogCesion.setHandlerBtnAceptar(function(){
				Ext.Ajax.request({
					url: '24forma02F.data.jsp',
					params: Ext.apply(fLogCesion.getForm().getValues(),{
						informacion: 'ConfirmacionClaves'
					}),
					callback: confirmacionClavesCesion
				});
			});				
			//se crea ventana de Confirmacion de Clave 
			var	ventanaConfir =  Ext.getCmp('Comfirmacionclave');	
			if (ventanaConfir) {
				ventanaConfir.show();
			} else {							
				new Ext.Window({
					layout: 'fit',
					width: 400,
					height: 200,			
					id: 'Comfirmacionclave',
					modal:true,
					closeAction: 'hide',
					items: [					
						fLogCesion
					],
					title: ''				
				}).show();
			}
		});			
	}
	
	// **********************************validaciones para la captura del Plazo*************************************+
	var revisaEntero1 =  function( campo) { 
		var numero=campo;
		if (!isdigit(numero)) {
			Ext.MessageBox.alert('Mensaje','El valor no es un n�mero v�lido.');
			return false;
		}	else{
			return true;
		}
	}

	var Tmes = new Array();
	var sCadena;
	Tmes[1] = "31"; Tmes[2] ="28"; Tmes[3]="31"; Tmes[4]="30"; Tmes[5]="31"; 
	Tmes[6]="30";
	Tmes[7] = "31"; Tmes[8] ="31"; Tmes[9]="30"; Tmes[10]="31";Tmes[11]="30"; 
	Tmes[12]="31";

	var fnDesentrama =  function( Cadena) { 
		var Dato;
		sCadena      = Cadena
		tamano       = sCadena.length;
		posicion     = sCadena.indexOf("/");
		Dato         = sCadena.substr(0,posicion);
		cadenanueva  = sCadena.substring(posicion + 1,tamano);
		sCadena      = cadenanueva;
		return Dato;
	}
	//Inicializa validaciones
	var inicializaTasasXPlazo =  function( valor, metadata, registro, info) { 
		var plazo = registro.data['PLAZO_FINAL'];			
		if(plazo!='0') {	
			return obtenTasaXPlazoIni( valor, metadata, registro, info);
		}else {
			return '0';
		}	
	}
	
	var obtenTasaXPlazoIni =  function( valor, metadata, registro, info) { 
		var i = 0;
		var plazo = registro.data['PLAZO_FINAL'];					
		var referenciaTasa = registro.data['REFERENCIA'];
		var valorTasa			= registro.data['VALOR_TASA']; 
		var icMoneda			= registro.data['IC_MONEDA_LINEA'];
		var icIf				  = registro.data['IC_IF']; 
		var icTasa				= registro.data['IC_TASA'];  
		var cgRelMat			= registro.data['CG_REL_MAT'];  
		var fnPuntos			= registro.data['FN_PUNTOS']; 
		var tipoPiso			= registro.data['TIPO_PISO'];  
		var montoAuxInt		= registro.data['MONTO_AUX_INTERES'];
		var montoCredito	= registro.data['MONTO_CREDITO']; 
		var montoInt			= registro.data['MONTO_TASA_INTERES'];  
		var montoCapitalInt = registro.data['MONTO_TOTAL_CAPITAL']; 
		var valorTasaPuntos = registro.data['VALOR_TASA_PUNTOS'];
		var monto	= registro.data['MONTO']; 
		var monto_recibir;
		var	numTasas = hidNumTasas;		
		var i=0;
		var auxIcIf;
		var auxReferenciaTasa;
		var auxIcTasa;
		var auxValorTasa;
		var auxIcMoneda;
		var auxPlazoDias;
		var iPlazoCredito = parseInt(plazo);
		var iPlazoTasa;
		var iPlazoTasaAux = 9999999999;
		var ok;  
			
		var fechaVto	= Ext.util.Format.date(registro.data['FECHA_VENC_FINAL'],'d/m/Y'); 
		var fechaEmision	= Ext.util.Format.date(registro.data['FECHA_EMISION'],'d/m/Y'); 
		if(fechaVto!='') {
			 plazo =  mtdCalculaDias(fechaEmision,fechaVto);
		}				
		//iPlazoCredito = parseInt(plazo);
		if(numTasas==1){
			auxIcIf			= eval('Auxiliar.auxIcIf0');   
			auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa0'); 
			auxIcTasa			= eval('Auxiliar.auxIcTasa0'); 
			auxCgRelMat		= eval('Auxiliar.auxCgRelMat0'); 
			auxFnPuntos		= eval('Auxiliar.auxFnPuntos0'); 
			auxValorTasa		= eval('Auxiliar.auxValorTasa0'); 
			auxIcMoneda		= eval('Auxiliar.auxIcMoneda0'); 
			auxPlazoDias		= eval('Auxiliar.auxPlazoDias0'); 
			iPlazoTasa			= parseInt(auxPlazoDias);
			if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
				iPlazoTasaAux			= iPlazoTasa;
				referenciaTasa	= auxReferenciaTasa;		 	
				valorTasa			= auxValorTasa; 
				icTasa			= auxIcTasa;
				cgRelMat			= auxCgRelMat;
				fnPuntos			= auxFnPuntos;
				if(cgRelMat=='+')
					valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
				else if(cgRelMat=='-')
					valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
				else if(cgRelMat=='*')
					valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
				else if(cgRelMat=='/')
					valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);	
				if(tipoPiso=="1"){
					montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360,2)*100)/100;
				}else{
					montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
				}
				montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
				montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);
				monto_recibir = parseFloat(monto)-parseFloat(montoInt);
			}
		}else{
			for(i=0;i<numTasas;i++){
				auxIcIf			= eval('Auxiliar.auxIcIf'+i);   
				auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa'+i); 
				auxIcTasa			= eval('Auxiliar.auxIcTasa'+i); 
				auxCgRelMat		= eval('Auxiliar.auxCgRelMat'+i); 
				auxFnPuntos		= eval('Auxiliar.auxFnPuntos'+i); 
				auxValorTasa		= eval('Auxiliar.auxValorTasa'+i); 
				auxIcMoneda		= eval('Auxiliar.auxIcMoneda'+i); 
				auxPlazoDias		= eval('Auxiliar.auxPlazoDias'+i); 
				iPlazoTasa			= parseInt(auxPlazoDias);
				//alert("iteracion "+ i + " plazo credito = "+iPlazoCredito+" Plazo Tasa = "+iPlazoTasa +" plazoTasaAux ="+iPlazoTasaAux);
				if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
					//alert("Si entro en el if "+iPlazoTasa + "iplazotasaaux = "+iPlazoTasaAux+" i:: "+i);
					iPlazoTasaAux 			= iPlazoTasa;
					referenciaTasa	= auxReferenciaTasa;		 	
					valorTasa 		= auxValorTasa; 			
					icTasa			= auxIcTasa;
					cgRelMat			= auxCgRelMat;
					fnPuntos			= auxFnPuntos;
					if(cgRelMat=='+')
						valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
					else if(cgRelMat=='-')
						valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
					else if(cgRelMat=='*')
						valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
					else if(cgRelMat=='/')
						valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);													
					if(tipoPiso=="1"){
						montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360)*100,2)/100;						
					}else{
						montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
					}					
					montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
					montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);
					monto_recibir = parseFloat(monto)-parseFloat(montoInt);
				}
			}//for   
		}	
		
		if(info =='ValorTasaInteres' ) { ok = valorTasaPuntos;    }
		if(info =='MontodeIntereses' ) { ok = montoInt; 	}
		if(info =='MontoCapitalInteres' ) { ok = montoCapitalInt; }
		if(info =='ReferenciaTasa' ) { ok = referenciaTasa; }		
		if(info =='ic_tasa') { ok = icTasa; }		
		if(info =='montoAuxi' ) { ok = montoAuxInt; }	
		if(info =='cg_rel_mat' ) { ok = cgRelMat; }	
		if(info =='fnPuntos' ) { ok = fnPuntos; }	
		if(info =='Monto_Recibir' ) { ok = monto_recibir; }	
		return ok;
	}

	var validaplazo =  function( opts, success, response, registro) { 	
		var plazoIF = registro.data['PLAZO_VALIDO'];  
		var dias_minimo = registro.data['DIAS_MINIMO'];  
		var dias_maximo = registro.data['DIAS_MAXIMO']; 
		var PlazoPyme= registro.data['PLAZO_FINAL'];			
		var x = 0;
		var  mensajetxt = "";
		if(plazoIF!="") {
			plazoIF = parseInt(plazoIF);
		}		
		if (PlazoPyme > plazoIF){    
			Ext.MessageBox.alert("Mensaje","El plazo capturado "+PlazoPyme+ " no puede ser mayor a "+plazoIF); 
			return false;
		}else {
			mensajetxt =sumaFecha( opts, success, response, registro);
			if(mensajetxt!=""){
				Ext.MessageBox.alert("Mensaje"," "+mensajetxt);	
				return false;
			}
			inicializaTasasXPlazo2(opts, success, response, registro);			
		   return true;
		}
	}//termina funcion

	var inicializaTasasXPlazo2 =  function( opts, success, response, registro) { 
		var plazo= registro.data['PLAZO_FINAL'];	
		if(plazo!='0') {	
			return obtenTasaXPlazo( opts, success, response, registro);
		}else {
			return '0';
		}	
	}	
	var objeto 	= 0;	
	var objeto2 = 0;
	var objeto3 = 0;

	var sumaFecha =  function( opts, success, response, registro) {
		var mensaje="";
		var dias_minimo = registro.data['DIAS_MINIMO'];  
		var dias_maximo = registro.data['DIAS_MAXIMO']; 		
		var txtPlazo= registro.data['PLAZO_FINAL'];				
		var fechaVto				= Ext.util.Format.date(registro.data['FECHA_VENC_FINAL'],'d/m/Y'); 
		var fechaEmision		= Ext.util.Format.date(registro.data['FECHA_EMISION'],'d/m/Y');  
		var fechaVenc				= Ext.util.Format.date(registro.data['FECHA_VENCIMIENTO'],'d/m/Y');   
		var montoInt				= registro.data['MONTO_TASA_INTERES'];   
		var montoCapitalInt	= registro.data['MONTO_TOTAL_CAPITAL']; 		
		var montoAuxInt			= registro.data['MONTO_AUX_INTERES']; 	
		var monto						= registro.data['MONTO']; 	
		var montoCredito		= registro.data['MONTO_CREDITO']; 
		var montoDocto			= registro.data['MONTO_VALUADO']; 
		var igPlazoDocto 		= registro.data['PLAZO_DOCTO']; 			 
		var referenciaTasa	= registro.data['REFERENCIA'];  
		var monedaLinea			= registro.data['IC_MONEDA_LINEA'];  
		var icIf						= registro.data['IC_IF'];  		
		var valorTasa				= registro.data['VALOR_TASA'];  
		var tipoPiso				= registro.data['TIPO_PISO'];  
		var icTasa					= registro.data['IC_TASA'];   
		var cgRelMat				= registro.data['CG_REL_MAT'];  
		var fnPuntos				= registro.data['FN_PUNTOS'];  
		var valorTasaPuntos	= registro.data['VALOR_TASA_PUNTOS'];        		
		var fec_venc_value = fechaVenc;
		var fec_venc_array = fec_venc_value.split("/");
		var TDate = new Date(fec_venc_array[2],fec_venc_array[1]-1,fec_venc_array[0]);				
		diaMes = TDate.getDate();  		
		if(objeto3==0)
			objeto3 = 1;		
		if(txtPlazo!="0") {
			if(!obtenTasaXPlazo(opts, success, response, registro)) {
      	if(objeto3==1){
				mensaje = "El documento no puede ser seleccionado con ese plazo porque no se encontro la tasa correspondiente \n ";				
			}			
			}else{
				objeto3=0;
			}		
		}//if(txtPlazo!="") 
		var AddDays = 0;
	   if(txtPlazo!=""){
			if(revisaEntero1(txtPlazo)){      
				AddDays = txtPlazo;
			}else{
				mensaje =   "El valor no es un n�mero v�lido.";
			}			
		}
		if(txtPlazo!="0"){
			diaMes = parseInt(diaMes)+ parseInt(AddDays);
			TDate.setDate(diaMes);
			var CurYear = TDate.getYear();
			var CurDay = TDate.getDate();
			var CurMonth = TDate.getMonth()+1;
			if(CurDay<10)
				CurDay = '0'+CurDay;
			if(CurMonth<10)
				CurMonth = '0'+CurMonth;
			TheDate = CurDay +'/'+CurMonth+'/'+CurYear;		
			fechaVto = TDate; 	
			registro.data['FECHA_VENC_FINAL']=fechaVto; 
			registro.commit();
			montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(txtPlazo),2);		
			plazo = parseInt(txtPlazo);		
			dias_maximo = dias_maximo - mtdCalculaDias(fechaEmision,fechaHoy)
			if(objeto==0)
				objeto = 1;
			if(plazo<dias_minimo){
				if(objeto==1){				
					mensaje += "Plazo minimo del financiamiento = "+dias_minimo+" \n";			
				}			
			}else if(plazo>dias_maximo){
				if(objeto==1){
					mensaje += "Plazo maximo del financiamiento = "+dias_maximo+" \n";
				}
			}else{
				objeto=0;
			}
			if(objeto2==0)
				objeto2 = 1;
			var diasInhabiles2 = diasInhabiles;
			var diaMes = CurDay +'/'+CurMonth;		
			if(diasInhabiles2.indexOf(diaMes)>0||TDate.getDay()==0||TDate.getDay()==6){
				if(objeto2==1){
					mensaje +="La Fecha de Vencimiento del Credito es un dia inhabil";				
				}
			}else{
				objeto2 = 0;
			}
		}
		return mensaje;
	}	
	var mtdCalculaDias =  function( sFecha,sFecha2) {
		var iResAno,DiaMes,DiasDif=0,iCont=0,iMes,DiaMesA,iBan=0;
		var DiasPar,Dato,VencDia,VencMes,AltaMes,AltaDia;
		sCadena   = sFecha
		AltaDia   = fnDesentrama(sCadena);
		AltaMes  = fnDesentrama(sCadena);
		AltaAno   = sCadena;
		sCadena     = sFecha2
		VencDia     = fnDesentrama(sCadena);
		VencMes     = fnDesentrama(sCadena);
		VencAno     = sCadena;
		Dato  = AltaMes.substr(0,1);
		if (Dato ==0){
			AltaMes = AltaMes.substr(1,2)
		}
		Dato  = AltaDia.substr(0,1);
		if (Dato ==0){
			AltaDia = AltaDia.substr(1,2)
		}
		Dato  = VencDia.substr(0,1);
		if (Dato ==0){
			VencDia = VencDia.substr(1,2)
		}
		Dato  = VencMes.substr(0,1);
		if (Dato ==0){
			VencMes = VencMes.substr(1,2)
		}
		while (VencAno != AltaAno || VencMes != AltaMes || VencDia != AltaDia){
			iCont++;
			while(AltaMes != VencMes || VencAno != AltaAno){
				iBan=1;
				iResAno = AltaAno % 4;
				if (iResAno==0){
					Tmes[2] =29;
				}else{
					Tmes[2] =28;
				}
				if (iCont == 1){
					DiaMesA = Tmes[AltaMes];
					DiasDif = DiaMesA - AltaDia;
				} else{
					DiaMesA  = Tmes[AltaMes];
					DiasDif += parseInt(DiaMesA);
				}
				if (AltaMes==12){
					AltaAno++;
					AltaMes=1;
				}else{
					AltaMes++;
				}
				iCont++;
			} // fin de while
			if (AltaMes == VencMes && VencAno == AltaAno){
				if (iBan==0){
					DiasDif = parseInt(VencDia) - parseInt(AltaDia);
					AltaDia = VencDia;
				} else if (iBan=1){
					DiasDif += parseInt(VencDia);
					AltaDia = VencDia;
			  }
			}
		}
		return DiasDif;
	}
	
	var obtenTasaXPlazo =  function( opts, success, response, registro) { 
		var i = 0;
		var plazo = registro.data['PLAZO_FINAL'];				
		var referenciaTasa = registro.data['REFERENCIA'];
		var valorTasa			= registro.data['VALOR_TASA']; 
		var icMoneda			= registro.data['IC_MONEDA_LINEA'];
		var icIf				  = registro.data['IC_IF']; 
		var icTasa				= registro.data['IC_TASA'];  
		var cgRelMat			= registro.data['CG_REL_MAT'];  
		var fnPuntos			= registro.data['FN_PUNTOS']; 
		var tipoPiso			= registro.data['TIPO_PISO'];  
		var montoAuxInt		= registro.data['MONTO_AUX_INTERES'];;
		var montoCredito	= registro.data['MONTO_CREDITO']; 
		var montoInt			= registro.data['MONTO_TASA_INTERES'];  
		var montoCapitalInt = registro.data['MONTO_TOTAL_CAPITAL']; 
		var valorTasaPuntos = registro.data['VALOR_TASA_PUNTOS'];
		var montoD	= registro.data['MONTO']; 
		var	numTasas = hidNumTasas;		
		var i=0;
		var auxIcIf;
		var auxReferenciaTasa;
		var auxIcTasa;
		var auxValorTasa;
		var auxIcMoneda;
		var auxPlazoDias;
		
		var iPlazoTasa;
		var iPlazoTasaAux = 9999999999;
		var ok = false;		
		var fechaVto	= Ext.util.Format.date(registro.data['FECHA_VENC_FINAL'],'d/m/Y'); 
		var fechaEmision	= Ext.util.Format.date(registro.data['FECHA_EMISION'],'d/m/Y'); 
		if(fechaVto!='') {
			var plazo =  mtdCalculaDias(fechaEmision,fechaVto);
		}	
		var iPlazoCredito =registro.data['PLAZO_FINAL'];
		
		if(numTasas==1){
			auxIcIf			= eval('Auxiliar.auxIcIf0');   
			auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa0'); 
			auxIcTasa			= eval('Auxiliar.auxIcTasa0'); 
			auxCgRelMat		= eval('Auxiliar.auxCgRelMat0'); 
			auxFnPuntos		= eval('Auxiliar.auxFnPuntos0'); 
			auxValorTasa		= eval('Auxiliar.auxValorTasa0'); 
			auxIcMoneda		= eval('Auxiliar.auxIcMoneda0'); 
			auxPlazoDias		= eval('Auxiliar.auxPlazoDias0'); 
			iPlazoTasa			= parseInt(auxPlazoDias);
			if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
				iPlazoTasaAux			= iPlazoTasa;
				referenciaTasa	= auxReferenciaTasa;		 	
				valorTasa			= auxValorTasa; 
				icTasa			= auxIcTasa;
				cgRelMat			= auxCgRelMat;
				fnPuntos			= auxFnPuntos;
				if(cgRelMat=='+')
					valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
				else if(cgRelMat=='-')
					valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
				else if(cgRelMat=='*')
					valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
				else if(cgRelMat=='/')
					valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);
					ok = true;
				if(tipoPiso=="1"){
					montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360,2)*100)/100;
				}else{
					montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
				}
				montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
				montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);
			}
		}else{
			for(i=0;i<numTasas;i++){
				auxIcIf			= eval('Auxiliar.auxIcIf'+i);   
				auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa'+i); 
				auxIcTasa			= eval('Auxiliar.auxIcTasa'+i); 
				auxCgRelMat		= eval('Auxiliar.auxCgRelMat'+i); 
				auxFnPuntos		= eval('Auxiliar.auxFnPuntos'+i); 
				auxValorTasa		= eval('Auxiliar.auxValorTasa'+i); 
				auxIcMoneda		= eval('Auxiliar.auxIcMoneda'+i); 
				auxPlazoDias		= eval('Auxiliar.auxPlazoDias'+i); 
				iPlazoTasa			= parseInt(auxPlazoDias);
				//alert("iteracion "+ i + " plazo credito = "+iPlazoCredito+" Plazo Tasa = "+iPlazoTasa +" plazoTasaAux ="+iPlazoTasaAux);
				if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
					//alert("Si entro en el if "+iPlazoTasa + "iplazotasaaux = "+iPlazoTasaAux+" i:: "+i);
					iPlazoTasaAux 			= iPlazoTasa;
					referenciaTasa	= auxReferenciaTasa;		 	
					valorTasa 		= auxValorTasa; 			
					icTasa			= auxIcTasa;
					cgRelMat			= auxCgRelMat;
					fnPuntos			= auxFnPuntos;
					if(cgRelMat=='+')
						valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
					else if(cgRelMat=='-')
						valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
					else if(cgRelMat=='*')
						valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
					else if(cgRelMat=='/')
						valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);	
						ok = true;
					if(tipoPiso=="1"){
						montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360)*100,2)/100;						
					}else{
						montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
					}	
					montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
					montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);
				}
			}//for   
		}			
		registro.data['MONTO_TASA_INTERES']=montoInt;
		registro.data['MONTO_TOTAL_CAPITAL']=montoCapitalInt;
		registro.data['VALOR_TASA_PUNTOS']=valorTasaPuntos;
		registro.data['REFERENCIA']=referenciaTasa;		
		registro.commit();	
		return ok;
	}

	var sumaTotalesIntereses =  function(opts, success, response, registro) {	
		var totalMN 		= 0.00;
		var totalUSD		= 0.00;
		var totalCapitalMN 	= 0.00;
		var totalCapitalUSD = 0.00;	
		var montoTasaInt;
		var montoCapitalInt;
		var icMoneda;
		var monedaLinea; 
		var monto_tasa_int = registro.data['MONTO_TASA_INTERES']; 
		var monto_capital_int =registro.data['MONTO_TOTAL_CAPITAL'];	
		montoTasaInt	= parseFloat(monto_tasa_int);
		montoCapitalInt	= parseFloat(monto_capital_int);
		icMoneda 	= registro.data['IC_MONEDA_DOCTO'];
		monedaLinea	= registro.data['IC_MONEDA_LINEA']; 		
		if(monedaLinea=="1"){
			totalMN += montoTasaInt;
			totalCapitalMN	+= montoCapitalInt;
		}else if(icMoneda=="54"&&monedaLinea=="54"){
			totalUSD += montoTasaInt;
			totalCapitalUSD	+= montoCapitalInt;
		}				
		totalInteresMN = roundOff(totalMN,2); //estos valores son los  que se muestran al seleccionar los registros 
		totalInteresUSD = roundOff(totalUSD,2);
		totalCapitalInteresMN = roundOff(totalCapitalMN,2);
		totalCapitalInteresUSD = roundOff(totalCapitalUSD,2);	
	}

	var seleccionaDocto =  function(opts, success, response, record) {
		var txtPlazo =record.data['PLAZO_FINAL'];
		var montoInt = record.data['MONTO_TASA_INTERES']; 
		var icTasa = record.data['IC_TASA'];	
		var plazo_unico=  record.data['PLAZO_UNICO'];		
		if(txtPlazo=="0"){
			Ext.MessageBox.alert("Mensaje","Debe capturar los plazos del credito en los documentos a aceptar");
			return false;
		}else {
			return true; 
		}		
		plazoIF = parseInt(plazo_unico); 
		PlazoPyme = parseInt(txtPlazo);  			
		if (PlazoPyme > plazoIF){
		 Ext.MessageBox.alert("Mensaje","El plazo capturado "+PlazoPyme+ " no puede ser mayor a "+plazoIF);
			return false ;
		}else {
			return true; 
		}
		if(validaciones(selectModel, rowIndex, keepExisting, record) ) {
			return true;
		}else {
			return false;
		}					
	}	
	

	var validaciones =  function(opts, success, response, record) {	
		gridEditable = Ext.getCmp('gridEditable');			
	
		var montoInt = inicializaTasasXPlazo( '', '', record,'MontodeIntereses');
		record.data['MONTO_TASA_INTERES']=montoInt;			
	
		var montoCap = inicializaTasasXPlazo( '', '', record,'MontoCapitalInteres');
		record.data['MONTO_TOTAL_CAPITAL']=montoCap;								
	
		var varTasa = inicializaTasasXPlazo( '', '', record,'ValorTasaInteres');
		record.data['VALOR_TASA_PUNTOS']=varTasa;				
		
		var referencia = inicializaTasasXPlazo( '', '', record,'ReferenciaTasa');		
		record.data['REFERENCIA']=referencia;	
			
		var montoAuxi = inicializaTasasXPlazo( '', '', record,'montoAuxi');
		record.data['MONTO_AUX_INTERES']=montoAuxi;	
								
		var tasa =  inicializaTasasXPlazo( '', '', record,'ic_tasa');
		record.data['IC_TASA']=tasa;	
			
		var relMat =  inicializaTasasXPlazo( '', '', record,'cg_rel_mat');
		record.data['CG_REL_MAT']=relMat;				
		
		var fnPuntos =  inicializaTasasXPlazo( '', '', record,'fnPuntos');
		record.data['FN_PUNTOS']=fnPuntos;		
				
		var monto_recibir =  inicializaTasasXPlazo( '', '', record,'Monto_Recibir');
		record.data['MONTO_RECIBIR']=monto_recibir;	
				
		record.commit();				
		
	}
	
	
	
//*** grid para mostrar el Monitor de lineas de Credito
	var procesarConsultaMonitorData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		var ic_epo = jsonData.ic_epo;
		hidNumLineas = jsonData.hidNumLineas;
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridMonitor.isVisible()) {
				gridMonitor.show();
			}			
			if(ic_epo == 26) {
				ctexto3.show();
			}else{
				ctexto4.show();
			}
			ctexto1.show();			
			ctexto5.show();
			var el = gridMonitor.getGridEl();	
			if(store.getTotalCount() > 0) {
				el.unmask();				
			} else {					
				el.mask('No existe l�nea de credito autorizada para el tipo de moneda seleccionada', 'x-mask');
			}			
		}
	}	
		
	var consultaMonitorData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma02F.data.jsp',
		baseParams: {
			informacion: 'ConsultaMonitor'
		},
		fields: [
			{name: 'LINEA_CREDITO_IF'},
			{name: 'IC_LINEA',type: 'float'},
			{name: 'MONEDA_LINEA',type: 'float'},				
			{name: 'SALDO_INICIAL' ,type: 'float'},
			{name: 'MONTOSELECCIONADO' ,type: 'float'},
			{name: 'NUMDOCTOS' ,type: 'float'},
			{name: 'SALDODISPONIBLE',type: 'float'},
			{name: 'HIDSALDODISPONIBLE',type: 'float'},
			{name: 'SELECCIONADO'}				
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaMonitorData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaMonitorData(null, null, null);						
				}
			}
		}
	});
		
	var gridMonitor = new Ext.grid.GridPanel({
		store: consultaMonitorData,
		hidden: true,
		margins: '20 0 0 0',		
		title: 'Monitor de l�neas de cr�dito',		
		columns: [
			{
				header: 'SELECCIONADO',
				tooltip: 'SELECCIONADO',
				dataIndex: 'SELECCIONADO',
				sortable: true,
				resizable: true,
				width: 30,
				hidden: true,
				align: 'left'
			},			
			{
				header: 'L�nea de Cr�dito IF',
				tooltip: 'L�nea de Cr�dito IF',
				dataIndex: 'LINEA_CREDITO_IF',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Saldo inicial',
				tooltip: 'Saldo inicial',
				dataIndex: 'SALDO_INICIAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto seleccionado',
				tooltip: 'Monto seleccionado',
				dataIndex: 'MONTOSELECCIONADO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Num. doctos. seleccionados',
				tooltip: 'Num. doctos. seleccionados',
				dataIndex: 'NUMDOCTOS',
				sortable: true,
				resizable: true,
				width: 150,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Saldo disponible',
				tooltip: 'Saldo disponible',
				dataIndex: 'SALDODISPONIBLE',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],			
		displayInfo: true,
		emptyMsg: "No hay registros.",
		loadMask: true,
		margins: '20 0 0 0',
		stripeRows: true,
		height: 150,
		width: 943,
		frame: false
	});
		
	// *****************para realizar validaciones al seleccionar 			
	var selectModel = new Ext.grid.CheckboxSelectionModel( {
		checkOnly: true,
		listeners: {
			//Para quitar lo selecccionado en el documentos ******************************************
			rowdeselect: function(selectModel, rowIndex, record) {	 
				record.data['SELECCION']='N';
				record.commit();					
				var monto_credito = record.data['MONTO_CREDITO'];
				var monto =  record.data['MONTO']; 
				var auxMonto = parseFloat(monto_credito);				
				var linea;				
				//esto es para obtener los datos del Gid de Monitor 
				var store = gridMonitor.getStore();			
				store.each(function(record) {							
					if(record.data['SELECCIONADO']=='S'){
						saldoDisponible  = record.data['SALDODISPONIBLE'];
						linea  = record.data['IC_LINEA'];
					}				
				});
			
				if(record.data['SELECCION'] =='N'){						
					if(auxMonto>0){
						numDoctos --;
						montoSeleccionado = roundOff(parseFloat(montoSeleccionado)-auxMonto,2);				
						saldoDisponible = roundOff(parseFloat(saldoDisponible)+auxMonto,2);											
					}else{
						return false;
					}
				}	
				
				store.each(function(record) {			
					if(record.data['SELECCIONADO'] =='S'){
						record.data['NUMDOCTOS'] = numDoctos;
						record.data['SALDODISPONIBLE'] = 	saldoDisponible;
						record.data['MONTOSELECCIONADO'] = montoSeleccionado;
						record.commit();
					}				
				});		
				
				if( record.data['IC_MONEDA_DOCTO']=='1') {							
					totalDoctosMN --;
					totalMontoMN		-= parseFloat(record.data['MONTO']);
					var montoSinInteres  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
					totalMontoDescMN	-= montoSinInteres
					totalInteresMN2		-= parseFloat(record.data['MONTO_TASA_INTERES']);				
					totalMontoCreditoMN	-= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
				}				
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='54' ){				
					totalDoctosUSD --;
					totalMontoUSD		-= parseFloat(record.data['MONTO']);
					var montoSinInteresUSD  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
					totalMontoDescUSD	-= montoSinInteresUSD;
					totalInteresUSD2		-= parseFloat(record.data['MONTO_TASA_INTERES']);	
					totalMontoCreditoUSD -=parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
				}
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){			
					totalDoctosConv --;
					totalMontosConv			-= parseFloat(record.data['MONTO']);
					var montoSinInteresConv  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']);					
					totalMontoDescConv 	-= montoSinInteresConv
					totalInteresConv		-= parseFloat(record.data['MONTO_TASA_INTERES']);
					totalConvPesos			-= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
					totalMontoCreditoConv	-= parseFloat(record.data['MONTO_DESCUENTO']);
				}		
				var dataTotales = [	[
					totalDoctosMN, totalMontoMN, totalMontoDescMN, totalInteresMN2, 
					totalDoctosUSD, totalMontoUSD, totalMontoDescUSD, totalInteresUSD2,
					totalDoctosConv, totalMontosConv, totalMontoDescConv, totalInteresConv
				]	];
						
				storeMontosData.loadData(dataTotales);
				if(record.data['IC_MONEDA_DOCTO']=='1'){								
					totalMontoInteresF  =  totalInteresMN2; 
					totalInteresF  =  totalMontoCreditoMN; 				
				}
				if(record.data['IC_MONEDA_DOCTO']=='54'){							 						 
					totalMontoInteresFUSD =  totalInteresUSD2; 
					totalInteresFUSD  =  totalMontoCreditoUSD; 
				}	
				if(record.data['IC_MONEDA_DOCTO']=='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){							 						 
					totalMontoInteresF =  totalInteresConv; 
					totalInteresF  =  totalConvPesos; 
				}	
				//esto es para obtener los datos del Gid de gridTotalesE 
				var store = gridTotales.getStore();			
				store.each(function(record) {		
					if(record.data['ICMONEDA']=='1'){
						record.data['MONTOINTERES'] = totalMontoInteresF;
						record.data['MONTOCAPITALINTERES'] = totalInteresF;
						record.data['MONTO_TOTAL_RECIBIR'] = totalInteresF;
						
					}
					if(record.data['ICMONEDA']=='54'){	
						record.data['MONTOINTERES'] = totalMontoInteresFUSD;
						record.data['MONTOCAPITALINTERES'] = totalInteresFUSD;
						record.data['MONTO_TOTAL_RECIBIR'] = totalInteresUSD;
					}	
					record.commit();	
				});					
			},	
			//Para selecccionar los documentos ******************************************
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //Se activa cuando una fila est� seleccionada, return false para cancelar.
		
				if(validaplazo('', '', '', record)) {
					record.data['SELECCION']= 'S';
					record.commit();					
				}else{
					record.data['SELECCION']= 'N';
					record.commit();
					return false;	
				}			
				if(seleccionaDocto (selectModel, rowIndex, keepExisting, record)) {
					record.data['SELECCION']= 'S';
					record.commit();									
				}else {
					record.data['SELECCION']= 'N';
					record.commit();
					return false;		
				}
				
				var tasa =  inicializaTasasXPlazo( '', '', record,'ic_tasa');
				record.data['IC_TASA']=tasa;
						
				var relMat =  inicializaTasasXPlazo( '', '', record,'cg_rel_mat');
				record.data['CG_REL_MAT']=relMat;				
					
				var fnPuntos =  inicializaTasasXPlazo( '', '', record,'fnPuntos');
				record.data['FN_PUNTOS']=fnPuntos;	
				
				var monto_credito = record.data['MONTO_CREDITO'];
				var monto =  record.data['MONTO']; 
				var auxMonto = parseFloat(monto_credito);				
				var linea;				
				//esto es para obtener los datos del Gid de Monitor 
				var store = gridMonitor.getStore();			
				store.each(function(record) {							
					if(record.data['SELECCIONADO']=='S'){
						saldoDisponible  = record.data['SALDODISPONIBLE'];
						linea  = record.data['IC_LINEA'];
					}				
				});
					
				if(record.data['SELECCION'] =='S'){						
					if(auxMonto>0){
						numDoctos ++;
						montoSeleccionado = roundOff(parseFloat(montoSeleccionado)+auxMonto,2);				
						saldoDisponible = roundOff(parseFloat(saldoDisponible)-auxMonto,2);								
					}
				}	
								
				if(parseInt(saldoDisponible)<0){
					Ext.MessageBox.alert("Mensaje","El saldo de la l�nea es insuficiente para el documento");
					record.data['SELECCION']= 'N';
					record.commit();
					return false;
				}
				store.each(function(record) {			
					if(record.data['SELECCIONADO'] =='S'){
						record.data['NUMDOCTOS'] = numDoctos;
						record.data['SALDODISPONIBLE'] = 	saldoDisponible;
						record.data['MONTOSELECCIONADO'] = montoSeleccionado;
						record.commit();
					}				
				});				
				
				record.data['IC_LINEA_CREDITO'] =  linea;
				record.commit();							
							
				sumaTotalesIntereses(selectModel, rowIndex, keepExisting, record);	
		
				if( record.data['IC_MONEDA_DOCTO']=='1') {							
					totalDoctosMN ++;
					totalMontoMN		+= parseFloat(record.data['MONTO']);
					var montoSinInteres  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
					totalMontoDescMN	+= montoSinInteres
					totalInteresMN2		+= parseFloat(record.data['MONTO_TASA_INTERES']);				
					totalMontoCreditoMN	+= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
				}				
				if(record.data['IC_MONEDA_DOCTO'] =='54'  &&  record.data['IC_MONEDA_LINEA'] =='54'){				
					totalDoctosUSD ++;
					totalMontoUSD		+= parseFloat(record.data['MONTO']);
					var montoSinInteresUSD  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
					totalMontoDescUSD	+= montoSinInteresUSD;
					totalInteresUSD2		+= parseFloat(record.data['MONTO_TASA_INTERES']);	
					totalMontoCreditoUSD+=parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
				}
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){			
					totalDoctosConv ++;
					totalMontosConv			+= parseFloat(record.data['MONTO']);
				  var montoSinInteresConv  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
					totalMontoDescConv 		+= montoSinInteresConv;				
					totalInteresConv		+= parseFloat(record.data['MONTO_TASA_INTERES']);
					totalConvPesos			+= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);   
					totalMontoCreditoConv	+= parseFloat(record.data['MONTO_DESCUENTO']);
				}
						
				var dataTotales = [ [
					totalDoctosMN, totalMontoMN, totalMontoDescMN, totalInteresMN2, 
					totalDoctosUSD, totalMontoUSD, totalMontoDescUSD, totalInteresUSD2,
					totalDoctosConv, totalMontosConv, totalMontoDescConv, totalInteresConv
				]	];
				
				storeMontosData.loadData(dataTotales);
				
				if(record.data['IC_MONEDA_DOCTO']=='1'){								
					totalMontoInteresF  =  totalInteresMN2; 
					totalInteresF  =  totalMontoCreditoMN; 
					totalMontoRecibirN= totalMontoMN-totalInteresMN2;
				}
				if(record.data['IC_MONEDA_DOCTO']=='54'){							 						 
					totalMontoInteresFUSD =  totalInteresUSD2; 
					totalInteresFUSD  =  totalMontoCreditoUSD; 
					totalMontoRecibirUSD= totalMontoUSD-totalInteresUSD2;
				}	
				if(record.data['IC_MONEDA_DOCTO']=='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){							 						 
					totalMontoInteresF =  totalInteresConv; 
					totalInteresF  =  totalConvPesos; 
				}	
				
			
				//esto es para obtener los datos del Gid de gridTotalesE 
				var store = gridTotales.getStore();			
				store.each(function(record) {						
					if(record.data['MONEDA']=='MONEDA NACIONAL'){
						record.data['MONTOINTERES'] = totalMontoInteresF;
						record.data['MONTOCAPITALINTERES'] = totalInteresF;
						record.data['MONTO_TOTAL_RECIBIR'] = totalMontoRecibirN;
					}
					if(record.data['MONEDA']=='MONEDA USD'){	
						record.data['MONTOINTERES'] = totalMontoInteresFUSD;
						record.data['MONTOCAPITALINTERES'] = totalInteresFUSD;
						record.data['MONTO_TOTAL_RECIBIR'] = totalMontoRecibirUSD;
					}	
					record.commit();	
				});			
			}
		}
	});
	
	
	var totalesData = new Ext.data.JsonStore({			
		root : 'registrosT',
		url : '24forma02F.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_epo: Ext.getCmp("ic_epo"),
			ic_moneda: Ext.getCmp("ic_moneda"),
			mto_descuento: Ext.getCmp("mto_descuento"),
			cc_acuse: Ext.getCmp("cc_acuse"),
			fn_monto_de: Ext.getCmp("fn_monto_de"),
			fn_monto_a: Ext.getCmp("fn_monto_a"),
			df_fecha_emision_de: Ext.getCmp("df_fecha_emision_de"),
			df_fecha_emision_a: Ext.getCmp("df_fecha_emision_a"),
			ig_numero_docto: Ext.getCmp("ig_numero_docto"),
			df_fecha_venc_de: Ext.getCmp("df_fecha_venc_de"),
			df_fecha_venc_a: Ext.getCmp("df_fecha_venc_a"),
			fecha_publicacion_de: Ext.getCmp("fecha_publicacion_de"),
			fecha_publicacion_a: Ext.getCmp("fecha_publicacion_a"),
			doctos_cambio: Ext.getCmp("doctos_cambio"),
			lineaTodos: Ext.getCmp("lineaTodos")
		},								
		fields: [			
			{name: 'MONEDA' , mapping: 'MONEDA'},
			{name: 'ICMONEDA' , mapping: 'ICMONEDA'},
			{name: 'TOTAL', mapping: 'TOTAL'},
			{name: 'MONTO', mapping: 'MONTO' },
			{name: 'TOTAL_MONTO_DESC',  mapping: 'TOTAL_MONTO_DESC'},
			{name: 'TOTAL_MONTO_VALUADO',  mapping: 'TOTAL_MONTO_VALUADO'},	
			{name: 'TOTAL_CREDITOS',  mapping: 'TOTAL_CREDITOS'},	
			{name: 'TOTAL_MONTO_CREDITOS',  mapping: 'TOTAL_MONTO_CREDITOS'},				
			{name: 'MONTOINTERES',  mapping: 'MONTOINTERES'},			
			{name: 'MONTOCAPITALINTERES',  mapping: 'MONTOCAPITALINTERES'},
			{name: 'MONTO_TOTAL_RECIBIR',  mapping: 'MONTO_TOTAL_RECIBIR'}
			
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});	
	
	//esto esta en duda como manejarlo
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Datos Documento Inicial', colspan: 5, align: 'center'},
				{header: 'Datos Documento Final', colspan: 4, align: 'center'}					
			]
		]
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		id: 'gridTotales',
		store: totalesData,
		margins: '20 0 0 0',
		align: 'center',
		title: 'Totales',
		plugins: grupos,	
		hidden: true,
		columns: [	
			{
					header: 'MONEDA',
					dataIndex: 'MONEDA',
					width: 150,
					align: 'left'			
			},	
			{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTAL',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'MONTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto total de descuento.',
				dataIndex: 'TOTAL_MONTO_DESC',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto total valuado en pesos',
				dataIndex: 'TOTAL_MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
		//Finales
			{							
				header : 'Num. total de doctos.',
				dataIndex : 'TOTAL_CREDITOS',
				width : 150,
				align: 'center'
			},
			{							
				header : 'Monto total de doctos.',			
				dataIndex : 'TOTAL_MONTO_CREDITOS',
				width : 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			{							
				header : 'Monto de Intereses',			
				dataIndex : 'MONTOINTERES',
				width : 150,
				align: 'right',				
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto a recibir',				
				dataIndex : 'MONTO_TOTAL_RECIBIR',
				width : 150,
				align: 'right',				
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		height: 150,	
		width : 943,
		frame: false
	});	
		
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		var no_epo = jsonData.ic_epo;
		Noelementos = jsonData.ElEMENTOS; 
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridEditable.isVisible()) {
				gridEditable.show();
			}	
			var btnCancelar = Ext.getCmp('btnCancelar');
			var btnConfirmar = Ext.getCmp('btnConfirmar');							
		
			if(no_epo == 26) {
				
				ctexto3.show();
			}else{
				ctexto4.show();
			}
			var el = gridEditable.getGridEl();	
			if(store.getTotalCount() > 0) {
				el.unmask();
				ctexto1.show();
				btnCancelar.enable();							
				btnConfirmar.enable();	
				gridTotales.show();			
				
			} else {					
				el.mask('No se encontr� ning�n registro', 'x-mask');
				btnCancelar.disable();							
				btnConfirmar.disable();	
				gridTotales.hide();
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma02F.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'NO_DOCUMENTO'},
			{name: 'NO_ACUSE'},				
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_VENCIMIENTO',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_PUBLICACION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'PLAZO_DOCTO',type: 'float' },
			{name: 'MONEDA' },
			{name: 'IC_MONEDA_DOCTO' },
			{name: 'IC_MONEDA_LINEA' },
			{name: 'IC_IF' },
			{name: 'TIPO_PISO' },				
			{name: 'MONTO' ,type: 'float' },
			{name: 'MONTO_DESCUENTO' ,type: 'float'},
			{name: 'MONTO_VALUADO' ,type: 'float'},
			{name: 'NO_DOCTO_FINAL' },
			{name: 'MONEDA_FINAL' },
			{name: 'MONTO_FINAL' ,type: 'float'},
			{name: 'MONTO_CREDITO' ,type: 'float'},
			{name: 'PLAZO_FINAL' ,type: 'float'},
			{name: 'FECHA_VENC_FINAL',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_HOY',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_OPERA_FINAL' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMBRE_IF' },
			{name: 'IC_LINEA_CREDITO' },				
			{name: 'REFERENCIA' },
			{name: 'IC_TASA' },
			{name: 'CG_REL_MAT' },
			{name: 'FN_PUNTOS' },
			{name: 'VALOR_TASA' },
			{name: 'VALOR_TASA_PUNTOS' ,type: 'float'},
			{name: 'MONTO_TASA_INTERES',type: 'float' },
			{name: 'MONTO_AUX_INTERES',type: 'float' },
			{name: 'MONTO_TOTAL_CAPITAL' ,type: 'float'},
			{name: 'PLAZO_UNICO' ,type: 'float'},
			{name: 'SELECCION' },
			{name: 'DIAS_MINIMO' ,type: 'float'},		
			{name: 'DIAS_MAXIMO' ,type: 'float'},
			{name: 'PLAZO' ,type: 'float'},
			{name: 'PLAZO_VALIDO' ,type: 'float'},
			{name: 'PLAZO_PRONTO_PAGO' ,type: 'float'},
			{name: 'MONTO_RECIBIR' ,type: 'float'},
			{name: 'CUENTA_BANCARIA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});
		
	var gridEditable = new Ext.grid.EditorGridPanel({
		id: 'gridEditable',
		title: 'Selecci�n Factoraje con Recursos ',
		hidden: true,
		store: consultaData,
		clicksToEdit: 1,		
		columns: [			
			{
				header: 'SELECCION',
				tooltip: 'SELECCION',
				dataIndex: 'SELECCION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: '<sup>1</sup>Epo',
				tooltip: 'Epo',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup>Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '<sup>1</sup>Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex: 'NO_ACUSE',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '<sup>1</sup>Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{
				header: '<sup>1</sup>Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>1</sup>Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>1</sup>Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},			
			{
				header: '<sup>1</sup>Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup>Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<sup>2</sup>N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex: 'NO_DOCTO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '<sup>2</sup>Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>2</sup>Monto cr�dito',
				tooltip: 'Monto cr�dito',
				dataIndex: 'MONTO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<sup>2</sup>Plazo', // es es editable 
				tooltip: 'Plazo',
				dataIndex: 'PLAZO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: false
				},
				renderer:function(value,metadata,registro){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			},
			{
				header: '<sup>2</sup>Fecha de vencimiento', // es es editable 
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>2</sup>Fecha de operaci�n',
				tooltip: 'Fecha de operaci�n',
				dataIndex: 'FECHA_OPERA_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>2</sup>IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>2</sup>Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:function(value,metadata,registro){		
					var select = registro.data['SELECCION'];	
					var refe = inicializaTasasXPlazo( value, metadata, registro,'ReferenciaTasa');
					if(select=='S'){
						refe = (value=='')?'':value;
					}
					return refe;
				}	
			},
			{
				header: '<sup>2</sup>Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_PUNTOS',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];	
					var valort = Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'ValorTasaInteres'),'0.0000%');
					if(select=='S'){
						valort =  Ext.util.Format.number((value=='0')?'':value, '0.0000%');					
					}
					return valort;	
				}
			},
			
			{
				header: '<sup>2</sup>Monto de intereses',
				tooltip: 'Monto de intereses',
				dataIndex: 'MONTO_TASA_INTERES',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];					
					var monto =  Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'MontodeIntereses'), '$0,0.00');	
					if(select=='S'){
						monto =  Ext.util.Format.number((value=='0')?'':value, '$0,0.00');
					}
					return monto;
				}
			},				
			{
				header: '<sup>2</sup>Cuenta Bancaria Pyme',
				tooltip: 'Cuenta Bancaria Pyme',
				dataIndex: 'CUENTA_BANCARIA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'		
			},	
			{
				header: '<sup>2</sup>Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTO_TOTAL_CAPITAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: true,
				align: 'right',
				renderer:function(value,metadata,registro){
					var monto =  Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'MontoCapitalInteres'), '$0,0.00');
					var select = registro.data['SELECCION'];
					if(select=='S'){
						monto =Ext.util.Format.number((value=='0')?'':value, '$0,0.00');	
					}
					return monto;
				}
			},			
			{
				header: '<sup>2</sup>Monto Recibir',
				tooltip: 'Monto Recibir',
				dataIndex: 'MONTO_RECIBIR',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer:function(value,metadata,registro){
					var monto =  Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'Monto_Recibir'), '$0,0.00');
					var select = registro.data['SELECCION'];
					if(select=='S'){
						monto =Ext.util.Format.number((value=='0')?'':value, '$0,0.00');	
					}
					return monto;
				}
			},
			{				
				header : '<sup>2</sup>IC_TASA',  //editable
				tooltip: 'IC_TASA',
				dataIndex : 'IC_TASA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var tasa =  inicializaTasasXPlazo( value, metadata, registro,'ic_tasa');
					if(select=='S'){
						tasa  = (value=='')?'':value;
					}
					return tasa	
				}					
			},
			{				
				header : '<sup>2</sup>MONTO_AUX_INTERES',  //editable
				tooltip: 'MONTO_AUX_INTERES',
				dataIndex : 'MONTO_AUX_INTERES',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var montoAuxi =  inicializaTasasXPlazo( value, metadata, registro,'montoAuxi');
					if(select=='S'){
						montoAuxi  = (value=='')?'':value;
					}
						return  Ext.util.Format.number(montoAuxi, '$0,0.00');	
				}	
			},
			{				
				header : '<sup>2</sup>CG_REL_MAT',  //editable
				tooltip: 'CG_REL_MAT',
				dataIndex : 'CG_REL_MAT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var relMat =  inicializaTasasXPlazo( value, metadata, registro,'cg_rel_mat');
					if(select=='S'){
						relMat  = (value=='')?'':value;
					}
					return relMat;	
				}	
			},
			{				
				header : '<sup>2</sup>FN_PUNTOS',  //editable
				tooltip: 'FN_PUNTOS',
				dataIndex : 'FN_PUNTOS',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var fnPuntos =  inicializaTasasXPlazo( value, metadata, registro,'fnPuntos');
					if(select=='S'){
						fnPuntos  = (value=='')?'':value;
					}
					return fnPuntos;	
				}	
			},			
			{				
				header : '<sup>2</sup>IC_LINEA_CREDITO',  //editable
				tooltip: 'IC_LINEA_CREDITO',
				dataIndex : 'IC_LINEA_CREDITO',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'				
			},	
			selectModel
		],	
		sm:selectModel,
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 950,
		align: 'center',
		frame: false,
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridEditable = e.gridEditable; 
				var campo= e.field;
				if(campo == 'PLAZO_FINAL'){
					return true;				
				}				
			},
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
				var record = e.record;
				var gridEditable = e.gridEditable; 
				var campo= e.field;
				
				var montoInt = inicializaTasasXPlazo( '', '', record,'MontodeIntereses');
				record.data['MONTO_TASA_INTERES']=montoInt;			
								
				var montoCap = inicializaTasasXPlazo( '', '', record,'MontoCapitalInteres');
				record.data['MONTO_TOTAL_CAPITAL']=montoCap;								
				
				var varTasa = inicializaTasasXPlazo( '', '', record,'ValorTasaInteres');
				record.data['VALOR_TASA_PUNTOS']=varTasa;				
	
				var referencia = inicializaTasasXPlazo( '', '', record,'ReferenciaTasa');
				record.data['REFERENCIA']=referencia;	
				
				var montoAuxi = inicializaTasasXPlazo( '', '', record,'montoAuxi');
				record.data['MONTO_AUX_INTERES']=montoAuxi;	
								
				var tasa =  inicializaTasasXPlazo( '', '', record,'ic_tasa');
				record.data['IC_TASA']=tasa;
					
				var relMat =  inicializaTasasXPlazo( '', '', record,'cg_rel_mat');
				record.data['CG_REL_MAT']=relMat;				
				
				var fnPuntos =  inicializaTasasXPlazo( '', '', record,'fnPuntos');
				record.data['FN_PUNTOS']=fnPuntos;						
						
				var monto_recibir =  inicializaTasasXPlazo( '', '', record,'Monto_Recibir');
				record.data['MONTO_RECIBIR']=monto_recibir;					
				
				if(campo=='PLAZO_FINAL') { 	 		validaplazo('', '', '', record); 			} //cuando se captura el plazo 
				
				record.commit();					
			}			
		},
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',
				'-',
				{
					text: 'Cancelar',
					iconCls: 'icoLimpiar',
					xtype: 'button',
					id: 'btnCancelar',
					handler: function() {						
						window.location = '24forma02Fext.jsp';
					}
				},
				'-',
				{
					xtype: 'button',
					id: 'btnConfirmar',
					iconCls: 'icoAceptar',
					text: 'Confirmar',
					handler: procesarConfirmar					
				}		
			]
		}		
	});
	
	
//********************Forma  de Criterios de Busquedad *********************************************
	
	function procesaValoresIniciales(opts, success, response) {			
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);				
			if (jsonValoresIniciales != null){
				Auxiliar = jsonValoresIniciales.Auxiliar;										
				hidNumTasas = jsonValoresIniciales.hidNumTasas;	
		
			}				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02F.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02F.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	

	var catalogoLineas = new Ext.data.JsonStore({
		id: 'catalogoLineasDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02F.data.jsp',
		baseParams: {
			informacion: 'CatalogoLineaTodos'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
		
				
	var elementosForma = [
		{ 	xtype: 'textfield',  hidden: true,  id: 'acuse', 	value: '' },	
		{
			xtype: 'compositefield',
			fieldLabel: 'EPO',
			combineErrors: false,
			msgTarget: 'side',
			width: 100,
			items: [
				{
					xtype: 'combo',
					name: 'ic_epo',
					id: 'ic_epo1',
					fieldLabel: 'Nombre de la EPO',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_epo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoEPOData,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 300,
					listeners: {
						select: {
							fn: function(combo) {
								Ext.Ajax.request({
									url: '24forma02F.data.jsp',
									params: {
										informacion: "SeleccionEPO",
										ic_epo: combo.getValue()
									},			
									callback: procesaValoresIniciales				
								});							
							}
						}	
					}
				},
				{
					xtype: 'displayfield',
					value: 'Moneda:',
					width: 100
				},
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. documento inicial',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
				xtype: 'textfield',
				name: 'ig_numero_docto',
				id: 'ig_numero_docto1',
				fieldLabel: 'Num. documento inicial',
				allowBlank: true,
				hidden: false,
				maxLength: 15,	
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'Monto documento',
					width: 120
				},
				{
					xtype: 'numberfield',
					name: 'fn_monto_de',
					id: 'fn_monto_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',				
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'numberfield',
					name: 'fn_monto_a',
					id: 'fn_monto_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',					
					margins: '0 20 0 0'  
				}
			]
		},			
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. acuse de carga',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'cc_acuse',
					id: 'cc_acuse1',
					fieldLabel: 'Num. acuse de carga',
					allowBlank: true,
					hidden: false,
					maxLength: 15,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				}	
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha emisi�n docto',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_emision_de',
					id: 'df_fecha_emision_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_emision_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_emision_a',
					id: 'df_fecha_emision_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_emision_de1',
					margins: '0 20 0 0'  
				}				
			]
		},			
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha vencimiento docto',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_de',
					id: 'df_fecha_venc_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_venc_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_a',
					id: 'df_fecha_venc_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_venc_de1',
					margins: '0 20 0 0'  
				}			
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha publicaci�n de',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_de',
					id: 'fecha_publicacion_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_publicacion_a1',					
					margins: '0 20 0 0',			
					value:fechaHoy
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_a',
					id: 'fecha_publicacion_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_publicacion_de1',
					margins: '0 20 0 0' ,
					value:fechaHoy
				}
			]
		},						
		{
			xtype: 'combo',
			name: 'lineaTodos',
			id: 'lineaTodos1',
			fieldLabel: 'Seleccionar Documentos con el siguiente IF',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'lineaTodos',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoLineas,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {										
						procesaLineas(combo.getValue());	
					}
				}	
			}
		}
	];		

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 885,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		style: 'margin:0 auto;',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
					var clave_epo = Ext.getCmp("ic_epo1");
					if (Ext.isEmpty(clave_epo.getValue()) ) {
						clave_epo.markInvalid('El valor de la EPO es requerido.');
						return;
					}	
					
					var ic_moneda = Ext.getCmp("ic_moneda1");
					if (Ext.isEmpty(ic_moneda.getValue()) ) {
						ic_moneda.markInvalid('El valor de la Moneda es requerido.');
						return;
					}	
					
					var fn_monto_de = Ext.getCmp("fn_monto_de1");
					var fn_monto_a = Ext.getCmp("fn_monto_a1");
					if (!Ext.isEmpty(fn_monto_de.getValue()) && Ext.isEmpty(fn_monto_a.getValue())    
						||  Ext.isEmpty(fn_monto_de.getValue()) && !Ext.isEmpty(fn_monto_a.getValue())    ) {
						fn_monto_a.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
						fn_monto_de.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
						return;
					}
									
					var df_fecha_emision_de = Ext.getCmp("df_fecha_emision_de1");
					var df_fecha_emision_a = Ext.getCmp("df_fecha_emision_a1");
					if (!Ext.isEmpty(df_fecha_emision_de.getValue()) && Ext.isEmpty(df_fecha_emision_a.getValue())    
						||  Ext.isEmpty(df_fecha_emision_de.getValue()) && !Ext.isEmpty(df_fecha_emision_a.getValue())    ) {
						df_fecha_emision_de.markInvalid('Debe capturar ambas fechas de emision o dejarlas en blanco');
						df_fecha_emision_a.markInvalid('Debe capturar ambas fechas de emision o dejarlas en blanco');
						return;
					}
					
					var df_fecha_venc_de = Ext.getCmp("df_fecha_venc_de1");
					var df_fecha_venc_a = Ext.getCmp("df_fecha_venc_a1");
					if (!Ext.isEmpty(df_fecha_venc_de.getValue()) && Ext.isEmpty(df_fecha_venc_a.getValue())    
						||  Ext.isEmpty(df_fecha_venc_de.getValue()) && !Ext.isEmpty(df_fecha_venc_a.getValue())    ) {
						df_fecha_venc_de.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
						df_fecha_venc_a.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
						return;
					}
					var fecha_publicacion_de = Ext.getCmp("fecha_publicacion_de1");
					var fecha_publicacion_a = Ext.getCmp("fecha_publicacion_a1");
					if (!Ext.isEmpty(fecha_publicacion_de.getValue()) && Ext.isEmpty(fecha_publicacion_a.getValue())    
						||  Ext.isEmpty(fecha_publicacion_de.getValue()) && !Ext.isEmpty(fecha_publicacion_a.getValue())    ) {
						fecha_publicacion_de.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');
						fecha_publicacion_a.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');
						return;
					}
					var lineaTodos = Ext.getCmp("lineaTodos1");
					if (Ext.isEmpty(lineaTodos.getValue()) ) {
						lineaTodos.markInvalid('El valor de IF es requerido.');
						return;
					}		
					
					fp.el.mask('Enviando...', 'x-mask-loading');
					//consulta para el montor de lineas de Credito
					consultaMonitorData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ConsultaMonitor'
						})
					});		
					//consulta para los registros 
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consulta'
						})
					});
					
					totalesData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ResumenTotales'
						})
					});						
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24forma02Fext.jsp';
				}
			}
		]			
	});	
	
	var ctexto1 = new Ext.form.FormPanel({		
		id: 'ctexto1',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: texto1.join('')			
	});
	
	var ctexto3 = new Ext.form.FormPanel({		
		id: 'ctexto3',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: cadEpoCemex.join('')			
	});
	
	
	var ctexto4 = new Ext.form.FormPanel({		
		id: 'ctexto4',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: cadEpo.join('')			
	});
		
	var ctexto5 = new Ext.form.FormPanel({		
		id: 'ctexto5',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: texto4.join('')			
	});
	var ctexto6 = new Ext.form.FormPanel({		
			id: 'ctexto6',
			width: 943,			
			frame: true,		
			titleCollapse: false,
			hidden: true,
			bodyStyle: 'padding: 6px',			
			defaultType: 'textfield',
			html: texto6.join('')			
		});
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			applyTo: 'areaContenido',
			width: 949,
			items: [
				fp,
				NE.util.getEspaciador(20),
				gridMonitor, 	
				mensajeAutorizacion, 
				NE.util.getEspaciador(20),
				ctexto1,
				fpBotones,
				NE.util.getEspaciador(20),
				gridEditable,
				gridPreAcuse,
				gridAcuse,	
				NE.util.getEspaciador(20),
				gridMontos,
				gridTotales,
				NE.util.getEspaciador(20),
				ctexto6,
				NE.util.getEspaciador(20),
				gridCifrasControl,							
				NE.util.getEspaciador(20),
				ctexto3,
				NE.util.getEspaciador(20),
				ctexto4,
				NE.util.getEspaciador(20),
				ctexto5, 	
				NE.util.getEspaciador(20)
			]
		});
	
	
	catalogoEPOData.load();
	catalogoMonedaData.load();	
	catalogoLineas.load();

	//esta funcion es para saber que linea de credito fue seleccionada
	var procesaLineas =  function(lineaTodos) {	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		if (gridMonitor.isVisible()) {			
			var store = gridMonitor.getStore();			
			store.each(function(record) {				
				if(record.data['IC_LINEA']==lineaTodos){														
					record.data['SELECCIONADO'] ='S';
					record.commit();
				}	else{
				record.data['SELECCIONADO'] ='N';
					record.commit();
				}
			});
		}	
		
	}
	
});