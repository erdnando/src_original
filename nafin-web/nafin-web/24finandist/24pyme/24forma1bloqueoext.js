
var bloquedado = ['Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>',
' Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes',
' Cd. De M�xico 50-89-61-07. <br>',
'	Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)'];

var horario = ['Para cualquier aclaracion o duda Favor de comunicarse al Centro de Atencion a clientes',
' Cd. De Mexico 50-89-61-07',
' Sin costo desde el interior de la Republica 01-800-NAFINSA (01800 623-4672).'];


var vencido = ['Usted tiene documentos con estatus Operado Vencido, por lo que no podr� seleccionar documentos hasta que hayan sido pagados'];


var noOpera= ['El Distribuidor no Opera este tipo de Credito'];
	
Ext.onReady(function() {
	

	var vencido1 = new Ext.form.FormPanel({		
		id: 'vencido2',
		width: 600,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',	
		style: 'margin:0 auto;',
		defaultType: 'textfield',
		html: vencido.join('')			
	});

	var bloquedado1 = new Ext.form.FormPanel({		
		id: 'bloquedado2',
		width: 600,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',	
		style: 'margin:0 auto;',
		defaultType: 'textfield',
		html: bloquedado.join('')			
	});


	var horario1 = new Ext.form.FormPanel({		
		id: 'horario2',
		width: 600,			
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: horario.join('')			
	});
	
		var noOpera1 = new Ext.form.FormPanel({		
		id: 'noOpera2',
		width: 300,			
		frame: true,	
		style: 'margin:0 auto;',
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: noOpera.join('')			
	});
	
	var mostrarTexto  = Ext.getDom('mostrarTexto').value;	
	
	if(mostrarTexto =='bloquedado'){
		bloquedado1.show();
	}
	if(mostrarTexto =='Horario'){
		horario1.show();
	}
	if(mostrarTexto =='Vencido'){
		vencido1.show();
	}

	if(mostrarTexto =='NOOpera'){
		noOpera1.show();
	}

	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			applyTo: 'areaContenido',
			width: 800,
			style: 'margin:0 auto;',
			items: [				
				horario1,
				vencido1,
				noOpera1,
				bloquedado1
			]
		});

});