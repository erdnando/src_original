<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	JSONObject jsonObj = new JSONObject();
	String ses_ic_pyme = iNoCliente;
	//PARAMETROS QUE PROVIENEN DE LA PAGINA ACTUAL O ANTERIOR
	String modificados[]		= request.getParameterValues("modificado");
	String ic_documentos[]		= request.getParameterValues("ic_documento");
	String ic_monedas[]			= request.getParameterValues("ic_moneda_docto");
	String ic_monedas_linea[]	= request.getParameterValues("ic_moneda_linea");
	String montos[]				= request.getParameterValues("montos");
	String montos_valuados[]	= request.getParameterValues("monto_valuado");
	String montos_credito[]		= request.getParameterValues("monto_credito");
	String montos_tasa_int[]	= request.getParameterValues("monto_tasa_int");
	String montos_descuento[]	= request.getParameterValues("monto_descuento");
	String tipos_cambio[]		= request.getParameterValues("tipo_cambio");
	String plazos_credito[]		= request.getParameterValues("plazo");
	String fechas_vto[]			= request.getParameterValues("fecha_vto");
	String referencias_tasa[]	= request.getParameterValues("referencia_tasa");
	String valores_tasa[]		= request.getParameterValues("valor_tasa");
	String valores_tasa_puntos[]= request.getParameterValues("valor_tasa_puntos");
	String ic_tasa[]			= request.getParameterValues("ic_tasa");
	String cg_rel_mat[]			= request.getParameterValues("cg_rel_mat");
	String fn_puntos[]			= request.getParameterValues("fn_puntos");
	String tipo_tasa[]			= request.getParameterValues("tipo_tasa");
	String puntos_pref[]		= request.getParameterValues("puntos_pref");
	String ic_epo				= request.getParameter("ic_epo");
	String fg_limite_pyme		= request.getParameter("hidLimPyme");
	String fg_limite_pyme_acum	= request.getParameter("hidLimUtil");	
	//VARIABLES DE USO LOCAL
	Vector	vecFilas	= null;
	Vector	vecFilas1	= null;
	Vector	vecFilas2	= null;
	Vector	vecColumnas	= null;	
	int		totalDoctosMN		= 0;
	int		totalDoctosUSD		= 0;
	double totalMontoMN			= 0;
	double totalMontoUSD		= 0;
	double totalMontoDescMN		= 0;
	double totalMontoDescUSD	= 0;
	double totalInteresMN		= 0;
	double totalInteresUSD		= 0;
	double totalMontoCreditoMN	= 0;
	double totalMontoCreditoUSD	= 0;
	double totalMontoCreditoConv= 0;
	int	 totalDoctosConv		= 0;
	double totalMontosConv		= 0;
	double totalMontoDescConv	= 0;
	double totalInteresConv		= 0;
	double totalConvPesos		= 0;
	String in					= "";
	int	i = 0, j = 0;
	String	icMoneda 			= "";
	String	fechaHoy			= "";
	// DE DESPLIEGUE
	String nombreEpo			=	"";
	String igNumeroDocto		=	"";
	String ccAcuse 				=	"";
	String dfFechaEmision		=	"";
	String dfFechaVencimiento	=	"";
	String dfFechaPublicacion 	= 	"";
	String igPlazoDocto			=	"";
	String moneda 				=	"";
	double fnMonto 				= 	0;
	String cgTipoConv			=	"";
	String tipoCambio			=   "";
	double montoValuado 		=	0;
	String igPlazoDescuento		= 	"";
	String fnPorcDescuento		= 	"";
	double montoDescuento 		=	0;
	String modoPlazo			= 	"";
	String estatus 				=	"";
	String cambios				= 	"";
	String numeroCredito		= 	"";
	String nombreIf				= 	"";
	String tipoLinea			=	"";
	String fechaOperacion		=	"";
	double montoCredito			=	0;
	String referencia			=	"";
	String plazoCredito			=	"";
	String fechaVencCredito		=	"";
	double tasaInteres			= 	0;
	double valorTasaInt			=	0;
	double montoTasaInt			=	0;
	double montoCapitalInt		= 	0;
	double fnPuntos				= 	0;
	String relMat				=	"";
	String tipoCobroInt			=	"";
	String  tipoPiso			=	"";
	String icTipoCobroInt		=	"";
	String icTasa				=	"";
	String icTipoFinanciamiento	=	"";
	String monedaLinea			=	"";
	String nombreMLinea			=	"";
	String seguridad			=	"";
	String nePyme 		= (String)session.getAttribute("strNePymeAsigna");
	String nombrePyme	= (String)session.getAttribute("strNombrePymeAsigna");

	try{
	//INSTANCIACION DE EJB'S
	AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
    ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

	if("PYME".equals(strTipoUsuario))	
		Horario.validarHorario(4, strTipoUsuario, iNoEPO);
		if("NAFIN".equals(strTipoUsuario))
			Horario.validarHorario(4,"PYME",ic_epo);
			
	fechaHoy 	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
    
    String operaContrato = BeanParametros.obtieneOperaSinCesion(iNoEPO); //PARAMETRO NUEVO JAGE 14012017
		
	for(i=0;i<ic_documentos.length;i++){
		//if("S".equals(modificados[i])){
		if("1".equals(ic_monedas[i])){
			totalDoctosMN ++;
			totalMontoMN		+= Double.parseDouble(montos[i]);
			totalMontoDescMN	+= Double.parseDouble(montos_descuento[i]);
			totalInteresMN		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);				
			totalMontoCreditoMN	+= Double.parseDouble(montos_credito[i]);					
		}
		if("54".equals(ic_monedas[i])&&"54".equals(ic_monedas_linea[i])){
			totalDoctosUSD ++;
			totalMontoUSD		+= Double.parseDouble(montos[i]);
			totalMontoDescUSD	+= Double.parseDouble(montos_descuento[i]);
			totalInteresUSD		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);
			totalMontoCreditoUSD+= Double.parseDouble(montos_credito[i]);
		}
		if("54".equals(ic_monedas[i])&&"1".equals(ic_monedas_linea[i])){
			totalDoctosConv ++;
			//SI SE DESPLIEGA EN MONEDA NACIONAL
			/*totalMontosConv			+= (Double.parseDouble(montos[i])*Double.parseDouble(tipos_cambio[i]));
			totalMontoDescConv 		+= (Double.parseDouble(montos_descuento[i])*Double.parseDouble(tipos_cambio[i]));
			totalInteresConv		+= (Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i])*Double.parseDouble(tipos_cambio[i]));
			totalConvPesos			+= (Double.parseDouble(montos_descuento[i])*Double.parseDouble(tipos_cambio[i]));
			totalMontoCreditoConv	+= (Double.parseDouble(montos_credito[i])*Double.parseDouble(tipos_cambio[i]));*/
			//SI SE DESPLIEGA EN DOLARES 
			totalMontosConv			+= Double.parseDouble(montos[i]);
			totalMontoDescConv 		+= Double.parseDouble(montos_descuento[i]);
			totalInteresConv		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);
			totalConvPesos			+= Double.parseDouble(montos_descuento[i]);
			totalMontoCreditoConv	+= Double.parseDouble(montos_credito[i]);
		}
		if(!"".equals(in))
			in += ",";
			in += ic_documentos[i];
		//}
	} //for
		
	vecFilas = aceptPyme.consultaDoctos(ses_ic_pyme,"","","","","", "","","","","","", "","","","",in);
	vecFilas1 = (Vector)vecFilas.get(0);
	vecFilas2 = (Vector)vecFilas.get(1);
	
	String contentType = "text/html;charset=ISO-8859-1";
	String 		nombreArchivo 	= null;	
	CreaArchivo archivo  = new CreaArchivo();
	archivo 			    = new CreaArchivo();
	nombreArchivo 	  = archivo.nombreArchivo()+".pdf";				
	ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
	//encabezado de Archivo PDF
	String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
	String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
	String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
	String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
	pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String) application.getAttribute("strDirectorioPublicacion"), "");
	String mensaje ="Al transmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, manifiesta su aceptación para que sea(n) sustituido(s) el(los) DOCUMENTO(S) INICIAL(ES) que ha seleccionado por el(los) DOCUMENTO(S) FINAL(ES) y estos últimos sean descontados a favor de la EMPRESA DE PRIMER ORDEN. Dicho(s) DOCUMENTO(S) FINALE(S) contiene(n) los Derechos de Cobro a su cargo por lo que Usted acepta pagar el 100% de su valor al INTERMEDIARIO FINANCIERO en su fecha de vencimiento. Manifiesta también su obligación de cubrir al INTERMEDIARIO FINANCIERO, los intereses que se detallan en esta pantalla, en la fecha de su operación, por haber aceptado la sustitución. ";
    if(operaContrato.equals("S")){
        mensaje = "En este acto manifiesto mi aceptación para que sea(n) financiado(s) el(los) DOCUMENTO(S) que he seleccionado, mismo(s) que contiene(n) cuentas pendientes de pago a mi cargo "+
        " y a favor de la EMPRESA DE PRIMER ORDEN por lo que en esta misma fecha me obligo a cubrir al INTERMEDIARIO FINANCIERO los intereses que se detallan en esta pantalla. "+
        "Manifiesto también mi obligación y aceptación de pagar el 100% del valor de(los) DOCUMENTO(S) en la fecha de vencimiento al INTERMEDIARIO FINANCIERO.";            
    }
	
    mensaje = mensaje +"\n\nPor otra parte, a partir del 17 de octubre de 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que sí ha emitido o emitirá a su CLIENTE o DISTRIBUIDOR el CFDI por la operación comercial que le dio origen a esta transacción, según sea el caso y conforme establezcan las disposiciones fiscales vigentes.";
	pdfDoc.setTable(12, 100);
	pdfDoc.setCell("Moneda nacional","celda01",ComunesPDF.CENTER,4);
	pdfDoc.setCell("Dólares Americanos","celda01",ComunesPDF.CENTER,4);
	pdfDoc.setCell("Doctos. en DLL financiados en M.N.","celda01",ComunesPDF.CENTER,4);
	pdfDoc.setCell("Num. total de doctos","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto total de doctos. iniciales","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto total de doctos. finales","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Num. total de doctos","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto total de doctos. iniciales","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto total de doctos. finales","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Num. total de doctos.","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto total de doctos. iniciales","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto total de doctos. finales","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
	
	pdfDoc.setCell(String.valueOf(totalDoctosMN) ,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoMN,2) ,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(totalInteresMN,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(String.valueOf(totalDoctosUSD),"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(totalInteresUSD,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontosConv,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoConv,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(totalInteresConv,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(totalInteresConv,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(mensaje,"formas",ComunesPDF.JUSTIFIED,12);
	pdfDoc.addTable();
	
	if(vecFilas1.size()>0)	{
	
		pdfDoc.addText(" Documentos seleccionados Modalidad 1 ","formas",ComunesPDF.CENTER);	
				
		for(i=0;i<vecFilas1.size();i++,j++){
			//while(!"S".equals(modificados[j]))
			//j++;
			vecColumnas = (Vector)vecFilas1.get(i);
			nombreEpo			=	(String)vecColumnas.get(0);
			igNumeroDocto		=	(String)vecColumnas.get(1);
			ccAcuse 			=	(String)vecColumnas.get(2);
			dfFechaEmision		=	(String)vecColumnas.get(3);
			dfFechaVencimiento	=	(String)vecColumnas.get(4);
			dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
			igPlazoDocto		=	(String)vecColumnas.get(6);
			moneda 				=	(String)vecColumnas.get(7);
			fnMonto 			= 	Double.parseDouble((String)vecColumnas.get(8).toString());
			cgTipoConv			=	(String)vecColumnas.get(9);
			tipoCambio			=   (String)vecColumnas.get(10);
			montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
			igPlazoDescuento	= 	(String)vecColumnas.get(12);
			fnPorcDescuento		= 	(String)vecColumnas.get(13);
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			modoPlazo			= 	(String)vecColumnas.get(14);
			estatus 			=	(String)vecColumnas.get(15);
			cambios				= 	(String)vecColumnas.get(16);
			numeroCredito		= 	(String)vecColumnas.get(17);
			nombreIf			= 	(String)vecColumnas.get(18);
			tipoLinea			=	(String)vecColumnas.get(19);
			fechaOperacion		=	(String)vecColumnas.get(20);
			plazoCredito		=	(String)vecColumnas.get(22);
			fechaVencCredito	=	(String)vecColumnas.get(23);
			relMat				= 	(String)vecColumnas.get(25);
			tipoCobroInt		=	(String)vecColumnas.get(27);
			referencia			=	(String)vecColumnas.get(29);
			tipoPiso			= 	(String)vecColumnas.get(30);
			icTipoCobroInt		= 	(String)vecColumnas.get(31);
			icMoneda			= 	(String)vecColumnas.get(32);
			icTasa				= 	(String)vecColumnas.get(33);
			icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			String   descuentoAforo1 = 	(String)vecColumnas.get(47)==null?"0":(String)vecColumnas.get(47); //F05-2014
			double  descuentoAforo= Double.parseDouble(vecColumnas.get(47).toString());  //F05-2014
			if (descuentoAforo1.equals("0"))  {  	descuentoAforo = 100;   	}	//F05-2014
			double fnMontoDescuento =0;
			
			if("+".equals(cg_rel_mat[j]))
				valorTasaInt = Double.parseDouble(valores_tasa[j])+Double.parseDouble(fn_puntos[j]);
			else if("-".equals(cg_rel_mat[j]))
				valorTasaInt = Double.parseDouble(valores_tasa[j])-Double.parseDouble(fn_puntos[j]);
			else if("*-".equals(cg_rel_mat[j]))
				valorTasaInt = Double.parseDouble(valores_tasa[j])*Double.parseDouble(fn_puntos[j]);
			else if("/".equals(cg_rel_mat[j]))
				valorTasaInt = Double.parseDouble(valores_tasa[j])/Double.parseDouble(fn_puntos[j]);
	
		
			plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
			if("1".equals(tipoPiso)){
				montoTasaInt = (double)Math.round((montoValuado*(valorTasaInt/100)/360)*100)/100;
			}else{
				montoTasaInt = (montoValuado*(valorTasaInt/100)/360);
			}
			montoCredito = montoValuado - (montoTasaInt*Double.parseDouble(plazoCredito));
			
			if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
				fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
			} else if("2".equals(icTipoFinanciamiento)  )  {			
				fnMontoDescuento	= fnMonto *(descuentoAforo/100);   //F05-2014
			}
				
				 
			
			if(i==0){
				//int csini = ("PYME".equals(strTipoUsuario))?19:20;
				int csini = 17;				
				pdfDoc.setTable(csini, 100);
				pdfDoc.setCell("Documento ","celda01",ComunesPDF.CENTER,csini);	
				if("NAFIN".equals(strTipoUsuario)){
				pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER,csini);	
				}
				pdfDoc.setCell("Epo ","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Num. docto. inicial","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Num. acuse","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Fecha de emisión","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Fecha vencimiento","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Fecha de Fecha de publicación","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Plazo docto.","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);						
				pdfDoc.setCell("Tipo conv.","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Plazo para descuento en dias","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("% de descuento","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto % de descuento","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Tipo cambio","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto valuado","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
			}
			
			if("NAFIN".equals(strTipoUsuario)){
				pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.CENTER);	
			}
			pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(ccAcuse,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaEmision,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaVencimiento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaPublicacion,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igPlazoDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(fnMonto,2),"formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(cgTipoConv,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igPlazoDescuento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(montoDescuento,2),"formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(tipoCambio,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMontoDescuento,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
			
		}	
		pdfDoc.addTable();
	}
	//*************************************************************************
	if(vecFilas2.size()>0){
	
		for(i=0;i<vecFilas2.size();i++,j++){		
			//while(!"S".equals(modificados[j]))
			//j++;
			vecColumnas = (Vector)vecFilas2.get(i);
			nombreEpo			=	(String)vecColumnas.get(0);
			igNumeroDocto		=	(String)vecColumnas.get(1);
			ccAcuse 			=	(String)vecColumnas.get(2);
			dfFechaEmision		=	(String)vecColumnas.get(3);
			dfFechaVencimiento	=	(String)vecColumnas.get(4);
			dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
			igPlazoDocto		=	(String)vecColumnas.get(6);
			moneda 				=	(String)vecColumnas.get(7);
			fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			cgTipoConv			=	(String)vecColumnas.get(9);
			tipoCambio			=   (String)vecColumnas.get(10);
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			igPlazoDescuento	= 	(String)vecColumnas.get(12);
			fnPorcDescuento		= 	(String)vecColumnas.get(13);
			modoPlazo			= 	(String)vecColumnas.get(14);
			estatus 			=	(String)vecColumnas.get(15);
			cambios				= 	(String)vecColumnas.get(16);
			//creditos por concepto de intereses
			numeroCredito		= 	(String)vecColumnas.get(17);
			nombreIf			= 	(String)vecColumnas.get(18);
			tipoLinea			=	(String)vecColumnas.get(19);
			fechaOperacion		=	(String)vecColumnas.get(20);
			plazoCredito		=	(String)vecColumnas.get(22);
			fechaVencCredito	=	(String)vecColumnas.get(23);
			relMat				= 	(String)vecColumnas.get(25);
			tipoCobroInt		=	(String)vecColumnas.get(27);
			referencia			=	(String)vecColumnas.get(29);
			tipoPiso			= 	(String)vecColumnas.get(30);
			icTipoCobroInt		= 	(String)vecColumnas.get(31);
			icMoneda			= 	(String)vecColumnas.get(32);
			icTasa				= 	(String)vecColumnas.get(33);
			icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			monedaLinea			= 	(String)vecColumnas.get(35);
			nombreMLinea		= 	(String)vecColumnas.get(40);			
			String   descuentoAforo1 = 	(String)vecColumnas.get(47)==null?"0":(String)vecColumnas.get(47); //F05-2014
			double  descuentoAforo= Double.parseDouble(vecColumnas.get(47).toString());  //F05-2014
			if (descuentoAforo1.equals("0"))  {  	descuentoAforo = 100;   	}	//F05-2014
			double fnMontoDescuento =0;  //F05-2014
		
		
			if("+".equals(cg_rel_mat[j]))
				valorTasaInt = Double.parseDouble(valores_tasa[j])+Double.parseDouble(fn_puntos[j]);
			else if("-".equals(cg_rel_mat[j]))
				valorTasaInt = Double.parseDouble(valores_tasa[j])-Double.parseDouble(fn_puntos[j]);
			else if("*-".equals(cg_rel_mat[j]))
				valorTasaInt = Double.parseDouble(valores_tasa[j])*Double.parseDouble(fn_puntos[j]);
			else if("/".equals(cg_rel_mat[j]))
				valorTasaInt = Double.parseDouble(valores_tasa[j])/Double.parseDouble(fn_puntos[j]);
				plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
			if("1".equals(icTipoFinanciamiento))
				montoCredito = fnMonto-montoDescuento;
			else if("2".equals(icTipoFinanciamiento))
				montoCredito = fnMonto;
				//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
				if("1".equals(icTipoFinanciamiento))
					montoCredito = montoValuado;
				else if("2".equals(icTipoFinanciamiento))
					montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
			}
			if("1".equals(tipoPiso)){
				montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
			}else{
				montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
			}
				montoCapitalInt = Double.parseDouble(montos_credito[j])+Double.parseDouble(montos_tasa_int[j]);
				
			if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
				fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
			} else if("2".equals(icTipoFinanciamiento)  )  {			
				fnMontoDescuento	= fnMonto *(descuentoAforo/100);   //F05-2014
			}
				
				 
			
			if(i==0){
				int csini = ("PYME".equals(strTipoUsuario))?16:19;
				
				pdfDoc.addText(" Documentos seleccionados Modalidad 1 ","formas",ComunesPDF.CENTER);
		
				pdfDoc.setTable(16, 100);
				pdfDoc.setCell("Datos Documento Inicial","celda01",ComunesPDF.CENTER,18);	
				if("NAFIN".equals(strTipoUsuario)){
					pdfDoc.setCell("Distribuidor","celda01",ComunesPDF.CENTER,11);
				}
				pdfDoc.setCell("Epo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Num. docto. inicial","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Num. acuse","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de emisión<","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de publicación","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo docto.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en dias","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo conv.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo cambio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto valuado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Datos Documento Final","celda01",ComunesPDF.CENTER,18);	
				pdfDoc.setCell("Num. docto. final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de operación","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia tasa de interés","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor tasa de interés","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto de intereses","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Total de Capital e interés","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);				
			}		
			
			if("NAFIN".equals(strTipoUsuario)){		
			pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.CENTER);
			}
			pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(ccAcuse,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaEmision,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaVencimiento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaPublicacion,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igPlazoDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMonto,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(igPlazoDescuento,"formas",ComunesPDF.CENTER);		
			if(!"2".equals(icTipoFinanciamiento)){
				pdfDoc.setCell(fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %"),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2),"formas",ComunesPDF.RIGHT);
			}	else{
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);				
			}
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){	
				pdfDoc.setCell(cgTipoConv,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipoCambio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMontoDescuento,2),"formas",ComunesPDF.CENTER);
			}else{	
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			}
			pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numeroCredito,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombreMLinea,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":""+Comunes.formatoMoneda2(montos_credito[j],false),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(plazos_credito[j],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechas_vto[j],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":referencias_tasa[j],"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(("S".equals(icTipoCobroInt))?" ":" "+Comunes.formatoDecimal(valores_tasa_puntos[j],2)+"%","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoMoneda2(montos_tasa_int[j],false),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoCapitalInt,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);			
			
		}
		pdfDoc.addTable();
	}
	
	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	}catch(NafinException ne){
		out.println(ne.getMsgError());
	}catch(Exception e){
		out.println("Error Archivo de Preacuse  "+e);
	}
%>

<%=jsonObj%>