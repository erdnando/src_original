<%@ page import="java.util.*, 
java.sql.*, 
netropology.utilerias.*,
com.netro.exception.*,
javax.naming.*,
net.sf.json.JSONArray,
net.sf.json.JSONObject,
com.netro.pdf.*,	
com.netro.distribuidores.*"
contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="../24secsession.jspf"%>
<%
	//PARAMETROS DE LA PAGINA ANTERIOR
	String modificados[]		= request.getParameterValues("modificado");
	String ic_documentos[]		= request.getParameterValues("ic_documento");
	String ic_monedas[]			= request.getParameterValues("ic_moneda_docto");
	String ic_monedas_linea[]	= request.getParameterValues("ic_moneda_linea");
	String montos[]				= request.getParameterValues("monto");
	String montos_valuados[]	= request.getParameterValues("monto_valuado");
	String montos_credito[]		= request.getParameterValues("monto_credito");
	String montos_tasa_int[]	= request.getParameterValues("monto_tasa_int");
	String montos_descuento[]	= request.getParameterValues("monto_descuento");
	String tipos_cambio[]		= request.getParameterValues("tipo_cambio");
	String plazos_credito[]		= request.getParameterValues("plazo");
	String fechas_vto[]			= request.getParameterValues("fecha_vto");
	String referencias_tasa[]	= request.getParameterValues("referencia_tasa");
	String valores_tasa[]		= request.getParameterValues("valor_tasa_puntos");
	String ic_tasa[]			= request.getParameterValues("ic_tasa");
	String cg_rel_mat[]		= request.getParameterValues("cg_rel_mat");
	String fn_puntos[]			= request.getParameterValues("fn_puntos");
	String lineaTodos			= (request.getParameter("lineaTodos")==null)?"":request.getParameter("lineaTodos");
	String	tipoArchivo		= (request.getParameter("tipoArchivo")==null)?"":request.getParameter("tipoArchivo");
	String	acuse	= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
  	
	String csTipoPago = (request.getParameter("csTipoPago")==null)?"":request.getParameter("csTipoPago");
  String totalDoctosAcuse = (request.getParameter("totalDoctosAcuse")==null)?"":request.getParameter("totalDoctosAcuse");
  String OrderId = (request.getParameter("OrderId")==null)?"":request.getParameter("OrderId");
  String montoTotalAcuse = (request.getParameter("montoTotalAcuse")==null)?"":request.getParameter("montoTotalAcuse");
  
	//VARIABLES DE USO LOCAL
	Vector		vecFilas	= null;
	Vector		vecColumnas	= null;	
	int			i = 0, j = 0;
	String		icMoneda 			= "";
	String		fechaHoy			= "";
	// DE DESPLIEGUE
	String nombreEpo			=	"";
	String igNumeroDocto		=	"";
	String ccAcuse 				=	"";
	String dfFechaEmision		=	"";
	String dfFechaVencimiento	=	"";
	String dfFechaPublicacion 	= 	"";
	String igPlazoDocto			=	"";
	String moneda 				=	"";
	double fnMonto 				= 	0;
	String cgTipoConv			=	"";
	String tipoCambio			=   "";
	double montoValuado 		=	0;
	String igPlazoDescuento		= 	"";
	String fnPorcDescuento		= 	"";
	double montoDescuento 		=	0;
	String modoPlazo			= 	"";
	String estatus 				=	"";
	String cambios				= 	"";
	String numeroCredito		= 	"";
	String nombreIf				= 	"";
	String tipoLinea			=	"";
	String fechaOperacion		=	"";
	double montoCredito			=	0;
	String referencia			=	"";
	String plazoCredito			=	"";
	String fechaVencCredito		=	"";
	double tasaInteres			= 	0;
	double valorTasaInt			=	0;
	double montoTasaInt			=	0;
	double fnPuntos				= 	0;
	String relMat				=	"";
	String tipoCobroInt			=	"";
	String  tipoPiso			=	"";
	String icTipoCobroInt		=	"";
	String icTasa				=	"";
	String acuseFormateado		=	"";
	String usuario				= 	"";
	String fechaCarga			= 	"";
	String horaCarga			= 	"";
	String icLineaCredito		= 	"";
	String monedaLinea			=	"";
	double montoCapitalInt		= 	0;
	String nombreMLinea			= 	"";
	
	String descBins = "";
	String idOrdebTrans = "";
	String codResp = "";
	String codDesc = "";
	String idTransaccion = "";
	String codAutoriza = "";
	String fechaAutoriza = "";
	String montoTotTrans = "";
  String numeroTarjeta = "";
  String codigoBin = "";
  String mesesInteres	="",  tipoPago = "";
  
	
	double totalMontoMN			= 0;
	double totalMontoUSD		= 0;
	double totalMontoDescMN		= 0;
	double totalMontoDescUSD	= 0;
	double totalInteresMN		= 0;
	double totalInteresUSD		= 0;
	double totalMontoCreditoMN	= 0;
	double totalMontoCreditoUSD	= 0;
	double totalMontoCreditoConv= 0;
	int	 totalDoctosConv		= 0;
	double totalMontosConv		= 0;
	double totalMontoDescConv	= 0;
	double totalInteresConv		= 0;
	double totalConvPesos		= 0;
	int		totalDoctosMN		= 0;
	int		totalDoctosUSD		= 0;
	
	String contentType = "text/html;charset=ISO-8859-1";
	String 		nombreArchivo 	= null;	
	CreaArchivo archivo 				= null;
	archivo  = new CreaArchivo();
	JSONObject jsonObj = new JSONObject();		
	StringBuffer contenidoArchivo = new StringBuffer();	


	try{
		//INSTANCIACION DE EJB'S
		AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class); 
		
		fechaHoy 	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	
		for(i=0;i<ic_documentos.length;i++){
			if("1".equals(ic_monedas[i])){
				totalDoctosMN ++;
				totalMontoMN		+= Double.parseDouble(montos[i]);
				totalMontoDescMN	+= Double.parseDouble(montos_descuento[i]);
				totalInteresMN		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);				
				totalMontoCreditoMN+= Double.parseDouble(montos_credito[i]);
			}
			if("54".equals(ic_monedas[i])&&"54".equals(ic_monedas_linea[i])){
				totalDoctosUSD ++;
				totalMontoUSD		+= Double.parseDouble(montos[i]);
				totalMontoDescUSD	+= Double.parseDouble(montos_descuento[i]);
				totalInteresUSD		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);
				totalMontoCreditoUSD+= Double.parseDouble(montos_credito[i]);
			}
			if("54".equals(ic_monedas[i])&&"1".equals(ic_monedas_linea[i])){
				totalDoctosConv ++;
				/*//SI SE DESPLIEGA EN MONEDA NACIONAL
				totalMontosConv			+= (Double.parseDouble(montos[i])*Double.parseDouble(tipos_cambio[i]));
				totalMontoDescConv 		+= (Double.parseDouble(montos_descuento[i])*Double.parseDouble(tipos_cambio[i]));
				totalInteresConv		+= (Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i])*Double.parseDouble(tipos_cambio[i]));
				totalConvPesos			+= (Double.parseDouble(montos_descuento[i])*Double.parseDouble(tipos_cambio[i]));
				totalMontoCreditoConv	+= (Double.parseDouble(montos_credito[i])*Double.parseDouble(tipos_cambio[i]));*/
				//SI SE DESPLIEGA EN DOLARES 
				totalMontosConv			+= Double.parseDouble(montos[i]);
				totalMontoDescConv 		+= Double.parseDouble(montos_descuento[i]);
				totalInteresConv		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);
				totalConvPesos			+= Double.parseDouble(montos_descuento[i]);
				totalMontoCreditoConv	+= Double.parseDouble(montos_credito[i]);
			}
		} //for
		//se realiza la consulta 
		if(csTipoPago.equals("1")){
			vecFilas = aceptPyme.consultaDoctosCCC(acuse);	
		}else if(csTipoPago.equals("2")){
			vecFilas = aceptPyme.consultaDoctosCCCTC(acuse);	
		}
		
		archivo 			    = new CreaArchivo();
		nombreArchivo 	  = archivo.nombreArchivo()+".pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
		
		if(tipoArchivo.equals("PDF")) {
	
			String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
			String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
			String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
			String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
			String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
      
			pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String) application.getAttribute("strDirectorioPublicacion"), "");
	
			String mensaje = "En este acto para los efectos de los art�culos 45 K de la Ley General de Organizaciones y Actividades Auxiliares del Cr�dito y el art�culo 2038 del C�digo Civil Federal me doy por notificado de la Cesi�n de Derechos de Cobro a mi cargo de los DOCUMENTO(S) detallado(s) en esta pantalla.";
        String mensjaLeyenda =	
       "Por otra parte, a partir del 17 de octubre del 2018 la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que sí ha emitido o emitirá a su CLIENTE o DISTRIBUIDOR "+
       "el CFDI por la operación comercial que le dio origen a esta transacción, según sea el caso y conforme establezcan las disposiciones fiscales vigentes.";

            pdfDoc.addText(mensjaLeyenda,"formas",ComunesPDF.LEFT);	
            pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	
			pdfDoc.addText("Selección de Documentos Modalidad 2 ","formas",ComunesPDF.CENTER);  
			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	
		
			int colspan = 0;
			if(csTipoPago.equals("1")){
				pdfDoc.setTable(12, 100);
				colspan=4;
			} else if(csTipoPago.equals("2")){
				pdfDoc.setTable(9, 100);
				colspan = 3;
			}
			
			//pdfDoc.setTable(12, 100);
			pdfDoc.setCell("Moneda nacional","celda01",ComunesPDF.CENTER,colspan);
			pdfDoc.setCell("Dólares Americanos","celda01",ComunesPDF.CENTER,colspan);
			pdfDoc.setCell("Doctos. en DLL financiados en M.N.","celda01",ComunesPDF.CENTER,colspan);
			pdfDoc.setCell("Num. total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto del Financiamiento","celda01",ComunesPDF.CENTER);
			if(csTipoPago.equals("1")){
				pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
			}
			pdfDoc.setCell("Num. total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto del Financiamiento","celda01",ComunesPDF.CENTER);
			if(csTipoPago.equals("1")){
				pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
			}
			pdfDoc.setCell("Num. total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de doctos.","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Monto del Financiamiento","celda01",ComunesPDF.CENTER);
			if(csTipoPago.equals("1")){
				pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
			}
			pdfDoc.setCell(String.valueOf(totalDoctosMN) ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.RIGHT);
			if(csTipoPago.equals("1")){
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalInteresMN,2),"formas",ComunesPDF.RIGHT);
			}
			pdfDoc.setCell(String.valueOf(totalDoctosUSD) ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.RIGHT);
			if(csTipoPago.equals("1")){
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalInteresUSD,2),"formas",ComunesPDF.RIGHT);
			}
			pdfDoc.setCell(String.valueOf(totalDoctosConv) ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontosConv,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoCreditoConv,2),"formas",ComunesPDF.RIGHT);
			if(csTipoPago.equals("1")){
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalInteresConv,2),"formas",ComunesPDF.RIGHT);
			}
			
      /*
			if(csTipoPago.equals("1")){
				pdfDoc.setCell(mensaje,"formas",ComunesPDF.JUSTIFIED,12);
			} else if(csTipoPago.equals("2")){
				pdfDoc.setCell(mensaje,"formas",ComunesPDF.JUSTIFIED,9);
			}
      */
			
			pdfDoc.addTable();
		}
		if (tipoArchivo.equals("CSV")) {	
			contenidoArchivo.append("Moneda nacional "+",,,,");
			contenidoArchivo.append("D�lares"+",,,,");		
			contenidoArchivo.append("Doctos. en DLL financiados en M.N."+",,,,");
			contenidoArchivo.append(" "+"\n");
			contenidoArchivo.append("Num. total de doctos."+",");
			contenidoArchivo.append("Monto total de doctos. iniciales "+",");
			contenidoArchivo.append("Monto total de doctos. finales "+",");
			if(csTipoPago.equals("1")){
				contenidoArchivo.append("Monto intereses "+",");
			}
			contenidoArchivo.append("Num. total de doctos."+",");
			contenidoArchivo.append("Monto total de doctos. iniciales "+",");
			contenidoArchivo.append("Monto total de doctos. finales "+",");
			if(csTipoPago.equals("1")){
				contenidoArchivo.append("Monto intereses "+",");
			}
			contenidoArchivo.append("Num. total de doctos. "+",");
			contenidoArchivo.append("Monto total de doctos. iniciales "+",");
			contenidoArchivo.append("Monto total de doctos. finales ");
			if(csTipoPago.equals("1")){
				contenidoArchivo.append(",Monto intereses "+"\n");				
			}else{contenidoArchivo.append("\n");}
			contenidoArchivo.append(String.valueOf(totalDoctosMN) +",");
			contenidoArchivo.append("\"$"+Comunes.formatoDecimal(totalMontoMN,2,true)+"\",");
			contenidoArchivo.append("\"$"+Comunes.formatoDecimal(totalMontoCreditoMN,2,true)+"\",");
			if(csTipoPago.equals("1")){
				contenidoArchivo.append("\"$"+Comunes.formatoDecimal(totalInteresMN,2,true)+"\",");
			}
			contenidoArchivo.append(String.valueOf(totalDoctosUSD) +",");
			contenidoArchivo.append("\"$"+Comunes.formatoDecimal(totalMontoUSD,2,true)+"\",");
			contenidoArchivo.append("\"$"+Comunes.formatoDecimal(totalMontoCreditoUSD,2,true)+"\",");
			if(csTipoPago.equals("1")){
				contenidoArchivo.append("\"$"+Comunes.formatoDecimal(totalInteresUSD,2,true)+"\",");
			}
			contenidoArchivo.append(String.valueOf(totalDoctosConv) +",");
			contenidoArchivo.append("\"$"+Comunes.formatoDecimal(totalMontosConv,2,true)+"\",");
			contenidoArchivo.append("\"$"+Comunes.formatoDecimal(totalMontoCreditoConv,2,true)+"\",");
			if(csTipoPago.equals("1")){
				contenidoArchivo.append("\"$"+Comunes.formatoDecimal(totalInteresConv,2,true)+"\",");
			}
			contenidoArchivo.append("\n");
		}		
		for(i=0;i<vecFilas.size();i++,j++){		
			vecColumnas = (Vector)vecFilas.get(i);
			nombreEpo			=	(String)vecColumnas.get(0);
			igNumeroDocto		=	(String)vecColumnas.get(1);
			ccAcuse 			=	(String)vecColumnas.get(2);
			dfFechaEmision		=	(String)vecColumnas.get(3);
			dfFechaVencimiento	=	(String)vecColumnas.get(4);
			dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
			igPlazoDocto		=	(String)vecColumnas.get(6);
			moneda 				=	(String)vecColumnas.get(7);
			fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			cgTipoConv			=	(String)vecColumnas.get(9);
			tipoCambio			=   (String)vecColumnas.get(10);
			montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
			igPlazoDescuento	= 	(String)vecColumnas.get(12);
			fnPorcDescuento		= 	(String)vecColumnas.get(13);
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			modoPlazo			= 	(String)vecColumnas.get(14);
			estatus 			=	(String)vecColumnas.get(15);
			cambios				= 	(String)vecColumnas.get(16);
			//creditos por concepto de intereses
			numeroCredito		= 	(String)vecColumnas.get(17);
			nombreIf			= 	(String)vecColumnas.get(18);
			tipoLinea			=	(String)vecColumnas.get(19);
			fechaOperacion		=	(String)vecColumnas.get(20);
			plazoCredito		=	(String)vecColumnas.get(22);
			fechaVencCredito	=	(String)vecColumnas.get(23);
			valorTasaInt		=	Double.parseDouble("".equals(vecColumnas.get(24).toString())?"0":vecColumnas.get(24).toString());
			relMat				= 	(String)vecColumnas.get(25);
			fnPuntos			=	Double.parseDouble("".equals(vecColumnas.get(26).toString())?"0":vecColumnas.get(26).toString());
			tipoCobroInt		=	(String)vecColumnas.get(27);
			referencia			=	(String)vecColumnas.get(29);
			tipoPiso			= 	(String)vecColumnas.get(30);
			icTipoCobroInt		= 	(String)vecColumnas.get(31);
			icMoneda			= 	(String)vecColumnas.get(32);
			icTasa				= 	(String)vecColumnas.get(33);
			monedaLinea			= 	(String)vecColumnas.get(35);
			icLineaCredito		= 	(String)vecColumnas.get(38);
			montoCredito		= 	Double.parseDouble(vecColumnas.get(21).toString());
			montoTasaInt		=	Double.parseDouble(vecColumnas.get(39).toString());
			acuseFormateado 	= 	(String)vecColumnas.get(40);
			fechaCarga			= 	(String)vecColumnas.get(41);
			horaCarga			= 	(String)vecColumnas.get(42);
			usuario				= 	(String)vecColumnas.get(43);
			nombreMLinea		=	(String)vecColumnas.get(44);
			if(csTipoPago.equals("2")){
        descBins				=	(String)vecColumnas.get(46);
        idOrdebTrans		=	(String)vecColumnas.get(47);
        codResp				=	(String)vecColumnas.get(48);
        codDesc				=	(String)vecColumnas.get(49);
        idTransaccion		=	(String)vecColumnas.get(50);
        codAutoriza			=	(String)vecColumnas.get(51);
        fechaAutoriza		=	(String)vecColumnas.get(52);
        montoTotTrans		=	(String)vecColumnas.get(53);
        codigoBin		=	(String)vecColumnas.get(54);
        numeroTarjeta		=	(String)vecColumnas.get(55);
      }else {
		
		  mesesInteres		=	(String)vecColumnas.get(46);
        tipoPago		   =	(String)vecColumnas.get(47);		  
		}
		
		
			if("+".equals(relMat)) {
				valorTasaInt += fnPuntos;
			}else if("-".equals(relMat)){
				valorTasaInt -= fnPuntos;
			}else if("*".equals(relMat)){
				valorTasaInt *= fnPuntos;
			}else if("/".equals(relMat)){
				valorTasaInt /= fnPuntos;
			}
			plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
			montoCapitalInt = montoCredito + montoTasaInt;
      //Fodea 014-2010 
			Calendar gcDiaFecha = new GregorianCalendar();
			String  diahabil ="";
			if(csTipoPago.equals("1")){
				java.util.Date fechaVencNvo = Comunes.parseDate(fechaVencCredito);
				gcDiaFecha.setTime(fechaVencNvo);
				int no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);      
				int menosdias =0;                     
				if(no_dia_semana==Calendar.SATURDAY ) { // 7 Sabado
					menosdias = -1;
				}
				if(no_dia_semana==Calendar.SUNDAY) { //domingo
					menosdias = -2;
				}
				if(no_dia_semana==Calendar.SATURDAY  ||  no_dia_semana==Calendar.SUNDAY ){ 
					diahabil  =  Fecha.sumaFechaDias(fechaVencCredito, menosdias);  
				} else {
					diahabil = fechaVencCredito;
				}
			}
			if(i==0){
				if(tipoArchivo.equals("PDF")) {
				
					pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	
				
					pdfDoc.setTable(16, 100);			
					pdfDoc.setCell("Documento","celda01",ComunesPDF.CENTER,17);					
					pdfDoc.setCell("Epo","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Num. docto. inicial","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Num. acuse","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de emisi�n","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha vencimiento","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha de publicaci�n","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Plazo docto.","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Monto % de descuento","celda01",ComunesPDF.CENTER);	
					if(csTipoPago.equals("1")){
						pdfDoc.setCell("Tipo conv","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Tipo cambio","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Monto valuado","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);	
					}else if(csTipoPago.equals("2")){
						pdfDoc.setCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);	
						//pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);	
						//pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);	
						//pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);	
					}
					pdfDoc.setCell("Financiamiento ","celda01",ComunesPDF.CENTER,16);	 
					pdfDoc.setCell("Num. de docto. final","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha de operaci�n","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);	
					if(csTipoPago.equals("2")){
						pdfDoc.setCell("Nombre del Producto","celda01",ComunesPDF.CENTER);	
					}
					pdfDoc.setCell("Referencia tasa de inter�s","celda01",ComunesPDF.CENTER);	
					if(csTipoPago.equals("1")){
						pdfDoc.setCell("Valor tasa de interés","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Monto de intereses","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Monto Total de Capital e Inter�s","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
					}else if(csTipoPago.equals("2")){
						pdfDoc.setCell("Monto Total","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("ID Orden Enviado","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Respuesta de Operación","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Código Autorización","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("Número Tarjeta de Crédito","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
					}
					
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
					//pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
					//pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
          if(csTipoPago.equals("2")){
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);	
					}
				}

				if(tipoArchivo.equals("CSV")) {
					contenidoArchivo.append("Documento"+",,,,,,,,,,,,,,,,");		 				
					contenidoArchivo.append("Financiamiento"+"\n");
					
					contenidoArchivo.append("Epo"+",");
					contenidoArchivo.append("Num. docto. inicial"+",");
					contenidoArchivo.append("Num. acuse"+",");
					contenidoArchivo.append("Fecha de emisi�n"+",");	
					contenidoArchivo.append("Fecha vencimiento"+",");	
					contenidoArchivo.append("Fecha de publicaci�n"+",");
					contenidoArchivo.append("Plazo docto."+",");
					contenidoArchivo.append("Moneda"+",");
					contenidoArchivo.append("Monto"+",");
					contenidoArchivo.append("Plazo para descuento en dias "+",");	
					contenidoArchivo.append("% de descuento "+",");
					contenidoArchivo.append("Monto % de descuento"+",");	
					//if(csTipoPago.equals("1")){
          if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv) && csTipoPago.equals("1")){
						contenidoArchivo.append("Tipo conv"+",");
						contenidoArchivo.append("Tipo cambio"+",");
						contenidoArchivo.append("Monto valuado"+",");
						//contenidoArchivo.append("Modalidad de plazo"+",");
					}//else if(csTipoPago.equals("2")){
					//	contenidoArchivo.append("Modalidad de plazo"+",");
						//contenidoArchivo.append(",,,");
					//}
          contenidoArchivo.append("Modalidad de plazo"+",");
								
					contenidoArchivo.append("Num. de docto. final"+",");
					contenidoArchivo.append("Moneda"+",");	
					contenidoArchivo.append("Monto"+",");
					contenidoArchivo.append("Plazo"+",");
					contenidoArchivo.append("Fecha de vencimiento"+",");	
					contenidoArchivo.append("Fecha de operaci�n"+",");	
					contenidoArchivo.append("IF"+",");
					if(csTipoPago.equals("2")){
						contenidoArchivo.append("Nombre del Producto"+",");
					}
					contenidoArchivo.append("Referencia tasa de inter�s"+",");	
					if(csTipoPago.equals("1")){
						contenidoArchivo.append("Valor tasa de inter�s"+",");
						contenidoArchivo.append("Monto de intereses"+",");	
						contenidoArchivo.append("Monto Total de Capital e Inter�s"+",");
					}else if(csTipoPago.equals("2")){
						contenidoArchivo.append("Monto Total"+",");
						contenidoArchivo.append("ID Orden Enviado"+",");	
						contenidoArchivo.append("Respuesta de Operaci�n"+",");
						contenidoArchivo.append("C�digo Autorizaci�n"+",");
            contenidoArchivo.append("N�mero Tarjeta de Cr�dito"+",");
					}
					contenidoArchivo.append("\n");
						
				}				
				
			}//if(i==0){
			if(tipoArchivo.equals("PDF")) {
				pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ccAcuse,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(dfFechaEmision,"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(dfFechaVencimiento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(dfFechaPublicacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(igPlazoDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMonto,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(igPlazoDescuento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %"),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2),"formas",ComunesPDF.CENTER);
				if(csTipoPago.equals("1")){
          if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv) ){
            pdfDoc.setCell(cgTipoConv,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(tipoCambio,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("$"+Comunes.formatoDecimal(montoValuado,2),"formas",ComunesPDF.CENTER);
            //pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
          }else {
            
            pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
          }
        }
				pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
        
				pdfDoc.setCell(numeroCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombreMLinea,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":""+Comunes.formatoMoneda2(montos_credito[j],false),"formas",ComunesPDF.CENTER);
				
				if(tipoPago.equals("1")) { //financiamiento con Intereses 
					pdfDoc.setCell(String.valueOf(plazoCredito),"formas",ComunesPDF.CENTER);				
				}else if(tipoPago.equals("2")) { // Meses sin intereses
					pdfDoc.setCell(mesesInteres +" Meses","formas",ComunesPDF.CENTER);					
				}
				
				pdfDoc.setCell(fechas_vto[i],"formas",ComunesPDF.CENTER); 
				
				pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
				if(csTipoPago.equals("2")){
					pdfDoc.setCell(codigoBin+ " - " +descBins,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":referencias_tasa[j],"formas",ComunesPDF.CENTER);
				if(csTipoPago.equals("1")){
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?" ":" "+valores_tasa[j]+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoMoneda2(montos_tasa_int[j],false),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoCapitalInt,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
				}else if(csTipoPago.equals("2")){
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(montoTotTrans,false),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(idOrdebTrans,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(idTransaccion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(codResp+" "+codDesc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("xxxx-xxxx-xxxx-"+numeroTarjeta,"formas",ComunesPDF.CENTER);
          pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
				}
				pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
				//pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
				//pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
        
        if(csTipoPago.equals("2")){
          pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
          
        }
				
			}
			///para generar archivo CSV
			
			if(tipoArchivo.equals("CSV")) {
					
				contenidoArchivo.append(nombreEpo.replace(',',' ')+",");
				contenidoArchivo.append(igNumeroDocto.replace(',',' ')+",");
				contenidoArchivo.append(ccAcuse+",");
				contenidoArchivo.append(dfFechaEmision+",");
        contenidoArchivo.append(dfFechaVencimiento+",");
				contenidoArchivo.append(dfFechaPublicacion+",");
				contenidoArchivo.append(igPlazoDocto+",");
				contenidoArchivo.append(moneda+",");
				contenidoArchivo.append("\"$"+Comunes.formatoDecimal(fnMonto,2,true)+"\",");
				contenidoArchivo.append(igPlazoDescuento+",");
				contenidoArchivo.append(fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %")+",");
				contenidoArchivo.append("\"$"+Comunes.formatoDecimal(montoDescuento,2,true)+"\",");
				if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv) && csTipoPago.equals("1")){
					contenidoArchivo.append(cgTipoConv+",");
					contenidoArchivo.append(tipoCambio+",");
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(montoValuado,2,true)+"\",");
					contenidoArchivo.append(modoPlazo+",");
				}else {
					contenidoArchivo.append(modoPlazo+",");
					//contenidoArchivo.append(" "+",");
					//contenidoArchivo.append(" "+",");
					//contenidoArchivo.append(" "+",");
				}
				
				contenidoArchivo.append(numeroCredito+",");
				contenidoArchivo.append(nombreMLinea+",");
				contenidoArchivo.append(("S".equals(icTipoCobroInt))?"":"\"$"+Comunes.formatoMoneda2(montos_credito[j],false)+"\",");
				
								
				if(tipoPago.equals("1")) { //financiamiento con Intereses 
					contenidoArchivo.append(String.valueOf(plazoCredito)+",");					
				}else if(tipoPago.equals("2")) { // Meses sin intereses
					contenidoArchivo.append(mesesInteres +" Meses"+",");					
				}
				contenidoArchivo.append(diahabil+",");						
				contenidoArchivo.append(fechaHoy+",");
				contenidoArchivo.append(nombreIf.replace(',',' ')+",");
				if(csTipoPago.equals("2")){
					contenidoArchivo.append(codigoBin+" - "+descBins.replace(',',' ')+",");
				}
				contenidoArchivo.append(("S".equals(icTipoCobroInt))?"":referencias_tasa[j]+",");
				if(csTipoPago.equals("1")){
					contenidoArchivo.append(("S".equals(icTipoCobroInt))?" ":" "+Comunes.formatoDecimal(valores_tasa[j],2,true)+"%"+"\",");
					contenidoArchivo.append(("S".equals(icTipoCobroInt))?"":"\"$"+Comunes.formatoMoneda2(montos_tasa_int[j],false)+"\",");
					contenidoArchivo.append(("S".equals(icTipoCobroInt))?"":"\"$"+Comunes.formatoDecimal(montoCapitalInt,2,true)+"\",");
				}else if(csTipoPago.equals("2")){
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(montoTotTrans,2,true)+"\",");
					contenidoArchivo.append("'"+idOrdebTrans+"',");
					contenidoArchivo.append(idTransaccion+",");
					contenidoArchivo.append(codResp+" "+codDesc+",");
          contenidoArchivo.append("xxxx-xxxx-xxxx-"+numeroTarjeta+",");
				}
				
				contenidoArchivo.append("\n");
			
				
			}
		
		} //for
		if(tipoArchivo.equals("PDF")) { 
			
			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	 
			
			pdfDoc.addTable();
			pdfDoc.setTable(2, 40);
			pdfDoc.setCell("Datos de cifras de control", "celda02", ComunesPDF.CENTER, 2);
			pdfDoc.setCell("Num. acuse", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(acuse, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(fechaCarga, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("Hora", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(horaCarga, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre y n�mero de usuario", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(iNoUsuario+" "+strNombreUsuario, "formas", ComunesPDF.CENTER);
      if(csTipoPago.equals("2")){
        pdfDoc.setCell("N�mero de Orden", "celda02", ComunesPDF.CENTER);
        pdfDoc.setCell(OrderId, "formas", ComunesPDF.CENTER);
        pdfDoc.setCell("N�mero de Documentos", "celda02", ComunesPDF.CENTER);
        pdfDoc.setCell(totalDoctosAcuse, "formas", ComunesPDF.CENTER);
        pdfDoc.setCell("Total", "celda02", ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalAcuse,2), "formas", ComunesPDF.CENTER);
      }
			

      pdfDoc.addTable();
      
			
			pdfDoc.endDocument();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		
		if(tipoArchivo.equals("CSV")) {		
			contenidoArchivo.append(" "+"\n");
			contenidoArchivo.append("Datos de cifras de control"+"\n");
			contenidoArchivo.append("Num. acuse"+",");
			contenidoArchivo.append(acuse+"\n");
			contenidoArchivo.append("Fecha"+",");
			contenidoArchivo.append(fechaCarga+"\n");
			contenidoArchivo.append("Hora"+",");
			contenidoArchivo.append(horaCarga+"\n");
			contenidoArchivo.append("Nombre y n�mero de usuario"+",");
			contenidoArchivo.append(iNoUsuario+" "+strNombreUsuario.replace(',',' ')+",");
      if(csTipoPago.equals("2")){
        contenidoArchivo.append("\nN�mero de Orden"+",");
        contenidoArchivo.append("'"+OrderId+"'\n");
        contenidoArchivo.append("N�mero de Documentos"+",");
        contenidoArchivo.append(totalDoctosAcuse+"\n");
        contenidoArchivo.append("Total"+",");
        contenidoArchivo.append("$"+Comunes.formatoDecimal(montoTotalAcuse,2)+"\n");
      }
		
			if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "Error al generar el archivo CSV ");
			} else {
				nombreArchivo = archivo.nombre;		
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			}
		
		}
	

}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	System.out.println("Error "+e);
	out.println("Error "+e);
}
%>


<%=jsonObj%>