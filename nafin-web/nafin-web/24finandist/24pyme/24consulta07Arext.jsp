<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	java.math.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,  
  com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	System.out.println("24consulta07Arext.jsp (E)");

	AccesoDB con = new AccesoDB();
	JSONObject jsonObj = new JSONObject();
	
try {
	
	CreaArchivo archivo = new CreaArchivo();
	
	String operacion = (request.getParameter("operacion") == null) ? "" : request.getParameter("operacion");
	String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
	String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	String fchinicial = (request.getParameter("Txtfchini") == null)?"":request.getParameter("Txtfchini");
	String fchfinal = (request.getParameter("Txtffin") == null)?"":request.getParameter("Txtffin");		
	String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
	String ic_pyme = request.getParameter("ic_pyme") == null?iNoCliente:(String)request.getParameter("ic_pyme");
	int start = Integer.parseInt(request.getParameter("start"));
	int limit = Integer.parseInt(request.getParameter("limit"));
	String responsable = request.getParameter("responsable") == null?"":(String)request.getParameter("responsable");
	String tiposCredito = request.getParameter("tiposCredito") == null?"":(String)request.getParameter("tiposCredito");
	String cgTipoConversion = request.getParameter("cgTipoConversion") == null?"":(String)request.getParameter("cgTipoConversion");
	String tipoCreditoXepo = request.getParameter("tipoCreditoXepo") == null?"":(String)request.getParameter("tipoCreditoXepo");
	String publicaDoctosFinanciables = (request.getParameter("publicaDoctosFinanciables") == null)?"":request.getParameter("publicaDoctosFinanciables");

	ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	if(!ic_epo.equals("")){		   
		responsable  = BeanParametro.obtieneResponsable(ic_epo);
		cgTipoConversion =  BeanParametro. obtieneTipoConversion(iNoCliente);		 
		tipoCreditoXepo =  BeanParametro.obtieneTipoCredito (ic_epo); 	
		tiposCredito = BeanParametro.getTiposCredito(iNoCliente, ic_epo);
	}			

	AvNotifPymeDist avNotifPymeDist = new com.netro.distribuidores.AvNotifPymeDist();
	avNotifPymeDist.setCgTipoConversion(cgTipoConversion);
	avNotifPymeDist.setTiposCredito(tiposCredito);
	avNotifPymeDist.setTipoCreditoXepo(tipoCreditoXepo);
	
	String epo	= "", distribuidor ="",  numdocto     = "", acuse      = "",  fchemision	    = "", fchpublica = "", 
	fchvence	  = "", plazodocto	 = "", moneda       = "", ic_moneda  = "",  tipoconversion	= "", tipocambio = "", 
	plzodescto  = "", porcdescto   = "", estatus	    = "",   ic_estatus	= "", numcredito = "",  descif			    = "", referencia = "", 
	plazocred		= "", fchvenccred	 = "", tipocobroint = "", valortaza	 = "",  fchopera	      = "", 
	monedaLinea	= "", modoPlazo		 = "", 	bandeVentaCartera ="", cuenta_bancaria= "", numero_tc ="";
	String ordenEnvio = "", opAcuse = "", codAutorizado = "", fechRegistro="", descrip_bins="" ;	
	String tipo_pago = "";

	double monto = 0 , mntovaluado = 0,  mntocredito	= 0, montoint			= 0,  mntodescuento	= 0;
	boolean validaFact = false;
	int numreg = 0;
	
	CQueryHelper queryHelper = null;		
	ResultSet	rs = null;
	con.conexionDB();
	queryHelper = new CQueryHelper(avNotifPymeDist);
	queryHelper.cleanSession(request);
	operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	String nombreArchivoPDF ="";
	
	if(informacion.equals("ArchivoTPDF")  || informacion.equals("ArchivoXPDF") ) {
		 nombreArchivoPDF = archivo.nombreArchivo()+".pdf";
	}
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {   
		throw new AppException("Error en los parametros recibidos", e);
	}
		
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
	if(informacion.equals("ArchivoTPDF")  || informacion.equals("ArchivoXPDF") ) {
	
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivoPDF);
	
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		(session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);	
	
		int numCols = 22;	
		if(!queryHelper.readyForPaging(request)) {
			queryHelper.executePKQueryIfNecessary(request,con);
		}
		//if (queryHelper.readyForPaging(request)) {
		
			if("ArchivoXPDF".equals(informacion)){
				rs = queryHelper.getPageResultSet(request,con,start,limit);				
			}else	if("ArchivoTPDF".equals(informacion)){
				rs = queryHelper.getCreateFile(request,con);				
			}
			while(rs.next()){
				
				numreg++;
				descif			= (rs.getString("DIF")==null)?"":rs.getString("DIF");
				ic_moneda		= (rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda");
				epo				= (rs.getString("EPO")==null)?"":rs.getString("EPO");
				numdocto		= (rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto");
				acuse			= (rs.getString("cc_acuse")==null)?"":rs.getString("cc_acuse");
				fchemision		= (rs.getString("df_fecha_emision")==null)?"":rs.getString("df_fecha_emision");
				fchpublica		= (rs.getString("df_fecha_publicacion")==null)?"":rs.getString("df_fecha_publicacion");
				fchvence		= (rs.getString("df_fecha_venc")==null)?"":rs.getString("df_fecha_venc");
				plazodocto		= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
				moneda			= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
				monto			= rs.getDouble("FN_MONTO");
				tipoconversion	= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
				tipocambio 		= (rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO");
				plzodescto		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
				porcdescto		= (rs.getString("fn_porc_descuento")==null)?"":rs.getString("fn_porc_descuento");
				mntodescuento	= monto*(Double.parseDouble(porcdescto)/100);
				mntovaluado		= (monto-mntodescuento)*Double.parseDouble(tipocambio);
				estatus			= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
				ic_estatus = (rs.getString("IC_ESTATUS_DOCTO")==null)?"":rs.getString("IC_ESTATUS_DOCTO");
				ordenEnvio = (rs.getString("IC_ORDEN_ENVIADO")==null)?"":rs.getString("IC_ORDEN_ENVIADO");
				opAcuse = (rs.getString("IC_OPERACION_CCACUSE")==null)?"":rs.getString("IC_OPERACION_CCACUSE");
				codAutorizado = (rs.getString("CODIGO_AUTORIZADO")==null)?"":rs.getString("CODIGO_AUTORIZADO");
				fechRegistro = (rs.getString("FECHA_REGISTRO")==null)?"":rs.getString("FECHA_REGISTRO");
				descrip_bins = (rs.getString("CG_DESCRIPCION_BINS")==null)?"":rs.getString("CG_DESCRIPCION_BINS");
				numero_tc = (rs.getString("NUMERO_TC")==null)?"":"XXXX-XXXX-XXXX-"+rs.getString("NUMERO_TC");
				
				/**********PARA CREDITO**************/				
				numcredito		= (rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
				monedaLinea		= (rs.getString("moneda_linea")==null)?"":rs.getString("moneda_linea");
				modoPlazo		= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
				if(responsable.equals("D")){				
					numcredito		= (rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");																				
					referencia		= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");																				
					plazocred		= (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");																						
					fchvenccred		= (rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO");																								
					tipocobroint	= (rs.getString("tipo_cobro_interes")==null)?"":rs.getString("tipo_cobro_interes");																										
					montoint		= rs.getDouble("MONTO_INTERES");	
					valortaza		= (rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA");
					mntocredito		= rs.getDouble("MONTO_CREDITO");
					fchopera		= (rs.getString("df_fecha_seleccion")==null)?"":rs.getString("df_fecha_seleccion");
				}
				bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
				if(tipoCreditoXepo.equals("F")) {
					cuenta_bancaria = (rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA");
				}
				String porc_Aforo	= (rs.getString("POR_AFRO")==null)?"0":rs.getString("POR_AFRO"); //F05-2014 
				BigDecimal  por_Aforo= new BigDecimal(porc_Aforo);  //F05-2014 
				BigDecimal  montoD= new BigDecimal(monto);  //F05-2014 /
				BigDecimal   MontoDesconta = por_Aforo.multiply(montoD);//F05-2014

				if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
				}
				if(numreg==1) { //Es primer registro:
					if(!"".equals(cgTipoConversion))
						numCols+=3;
					float init = 1;
					if(responsable.equals("D")) {
						numCols++;
						init = 0.35f;
					}
					if(tipoCreditoXepo.equals("F")) {
						numCols++;
					}
					
					int colMonto = 10;
					if(!responsable.equals("D"))
						colMonto++;
					int colMontoDsct = 2;
						
					float anchos[] = new float[numCols];
					for(int x=0;x<numCols;x++) {
						if(x==0)
							anchos[x] = init;
						else
							anchos[x] = 1f;
							
						if(x==colMonto||x==colMontoDsct)
							anchos[x] = 1.45f;
					}
					pdfDoc.setTable(numCols,100,anchos);
					pdfDoc.setCell("Datos del Documento Inicial","celda01rep",ComunesPDF.CENTER,numCols);
					if(responsable.equals("D"))
						pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("IF","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("EPO","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Emisión","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo docto.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("% de Descuento Aforo","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01rep",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Plazo Dscto Días","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("% Dscto","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto % Dscto","celda01rep",ComunesPDF.CENTER);
					if(!"".equals(cgTipoConversion)) {   //if(!"".equals(cgTipoConversion)){
						pdfDoc.setCell("Tipo conv.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo cambio","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto valuado","celda01rep",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Modo Plazo","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
					if(!responsable.equals("D")){
						pdfDoc.setCell("Nombre del Producto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("ID Orden enviado","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Respuesta de Operación","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Código Autorización","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Número Tarjeta de Crédito","celda01rep",ComunesPDF.CENTER);
						
					}else if(responsable.equals("D")){
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,5);
						pdfDoc.setCell("Datos Documento Final","celda01rep",ComunesPDF.CENTER,numCols);
						pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
						if(tipoCreditoXepo.equals("F")) {
							pdfDoc.setCell("Tipo Cred","celda01rep",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("No. Docto Final","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Oper.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Ref. Tasa Intés","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Valor Tasa Interés","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto Interés","celda01rep",ComunesPDF.CENTER);
						//pdfDoc.setCell("Tipo Cobro Interés","celda01rep",ComunesPDF.CENTER);
						
						pdfDoc.setCell("Nombre del Producto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("ID Orden enviado","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Respuesta de Operación","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Código Autorización","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Número Tarjeta de Crédito","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,numCols-14);
					}
				}//if(numreg==1)
				
				
				if(responsable.equals("D"))
					pdfDoc.setCell("A","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(descif,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(epo,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(numdocto,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(acuse,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(fchemision,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(fchpublica,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(fchvence,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(plazodocto,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.CENTER);
				
				pdfDoc.setCell(Comunes.formatoDecimal(porc_Aforo,2)+"%","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(MontoDesconta.toPlainString(),2),"formasrep",ComunesPDF.CENTER);
				
				
				pdfDoc.setCell(plzodescto,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(porcdescto,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(mntodescuento,2),"formasrep",ComunesPDF.CENTER);
				if(!"".equals(cgTipoConversion)){
					pdfDoc.setCell((ic_moneda.equals(monedaLinea))?"":tipoconversion,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell((ic_moneda.equals(monedaLinea))?"":tipocambio,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell((ic_moneda.equals(monedaLinea))?"":"$ "+Comunes.formatoDecimal(mntovaluado,2),"formasrep",ComunesPDF.CENTER);
				}
				pdfDoc.setCell(modoPlazo,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(estatus,"formasrep",ComunesPDF.CENTER);
				if(!responsable.equals("D")){
						
					if(ic_estatus.equals("32")){
						pdfDoc.setCell(descrip_bins,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(ordenEnvio,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(opAcuse,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(codAutorizado,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(numero_tc,"formasrep",ComunesPDF.CENTER);
					}else{
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
					}
				}else if(responsable.equals("D")){
					pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,5);
					pdfDoc.setCell("B","formasrep",ComunesPDF.CENTER);
					if(tipoCreditoXepo.equals("F")) {
						pdfDoc.setCell(cuenta_bancaria,"formasrep",ComunesPDF.CENTER);
					}
					pdfDoc.setCell(numcredito,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(mntocredito,2),"formasrep",ComunesPDF.CENTER);
					if(ic_estatus.equals("32")){
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
					}else{
						pdfDoc.setCell(plazocred,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(fchvenccred,"formasrep",ComunesPDF.CENTER);
					}
					pdfDoc.setCell(fchopera,"formasrep",ComunesPDF.CENTER);
					if(ic_estatus.equals("32")){
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
					}else{
						pdfDoc.setCell(referencia,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(valortaza,2),"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(montoint,2),"formasrep",ComunesPDF.CENTER);
					}
					
					//pdfDoc.setCell(tipocobroint,"formasrep",ComunesPDF.CENTER);
					if(ic_estatus.equals("32")){
						pdfDoc.setCell(descrip_bins,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(ordenEnvio,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(opAcuse,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(codAutorizado,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(numero_tc,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,numCols-14);
					}else{
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,numCols-14);
					}
				}
			}//while
			con.cierraStatement();
			
			//para los totales 
			Vector vecTotales = queryHelper.getResultCount(request);
			
				int numColTotal =22;
				int base =17;
			if (numreg != 0 && vecTotales.size()>0) {	//Hay registros
				Vector vecColumnas = null;
				//pdfDoc.setTable(13,100);
				for(int i=0; i<vecTotales.size(); i++){
					vecColumnas = (Vector)vecTotales.get(i);
					if(responsable.equals("D")){
						pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);   
						numColTotal++;
					}
					pdfDoc.setCell("Totales "+vecColumnas.get(1).toString(),"celda01rep",ComunesPDF.CENTER,3);
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 5);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal((String)vecColumnas.get(3),2),"formasrep",ComunesPDF.CENTER, 2);
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 4);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal((String)vecColumnas.get(4),2),"formasrep",ComunesPDF.CENTER, 2);
					if(!"".equals(cgTipoConversion)){
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 3);
						numColTotal=numColTotal+3;
						base=base+3;
					}
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 1);
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,(numColTotal-base));
					
					
					if(responsable.equals("D")) {
						pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal((String)vecColumnas.get(6),2),"formasrep",ComunesPDF.CENTER, 2);
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 4);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal((String)vecColumnas.get(7),2),"formasrep",ComunesPDF.CENTER, 2);
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,numCols-10);
					}
				}//fin del for
					
			} //fin del if
			pdfDoc.addTable();
		pdfDoc.endDocument();
		
		//} //fin del if readyForPaging
	
		
		validaFact = true;
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoPDF);
		
		
	}
	
	
	///*******************************************************
	if(informacion.equals("ArchivoCSV")) {
	String nombreArchivoCSV = "";
	StringBuffer contenidoArchivo = new StringBuffer();
	rs = queryHelper.getCreateFile(request,con);
	
	contenidoArchivo.append("IF, Epo ,Num. docto.inicial, Num. acuse carga, Fecha de emisión, Fecha de publicación, Fecha vencimiento, Plazo docto., Moneda, Monto,  % de Descuento Aforo , Monto a Descontar, Plazo para descuento, % de descuento, Monto con descuento,");
	
	if(!"".equals(cgTipoConversion))
		contenidoArchivo.append(" Tipo conv., Tipo cambio, Monto valuado,");
		contenidoArchivo.append("Modalidad de plazo,");
		if(publicaDoctosFinanciables.equals("S")){
			contenidoArchivo.append("Tipo de pago,");
		}
		contenidoArchivo.append("Estatus");
	if(tipoCreditoXepo.equals("F")) {
		contenidoArchivo.append(", Cuenta Bancaria Pyme ");
	}
	
	if(responsable.equals("D")){	
		contenidoArchivo.append(", Num. de docto. inicial, Monto, Plazo, Fecha de vencimiento,Fecha de operación,  Referencia,  Valor tasa de interés, Monto tasa de interés");
		contenidoArchivo.append(",Nombre del Producto ,Id Orden enviado, Respuesta de Operación, Código Autorización,Número Tarjeta de Crédito \n");
	}else{
		contenidoArchivo.append(",Nombre del Producto,Id Orden enviado, Respuesta de Operación, Código Autorización,Número Tarjeta de Crédito \n");
	}
	while(rs.next()){
		numreg++;
		descif			= (rs.getString("DIF")==null)?"":rs.getString("DIF");
		ic_moneda		= (rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda");
		epo				= (rs.getString("EPO")==null)?"":rs.getString("EPO");
		numdocto		= (rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto");
		acuse			= (rs.getString("cc_acuse")==null)?"":rs.getString("cc_acuse");
		fchemision		= (rs.getString("df_fecha_emision")==null)?"":rs.getString("df_fecha_emision");
		fchpublica		= (rs.getString("df_fecha_publicacion")==null)?"":rs.getString("df_fecha_publicacion");
		fchvence		= (rs.getString("df_fecha_venc")==null)?"":rs.getString("df_fecha_venc");
		plazodocto		= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
		moneda			= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
		monto			= rs.getDouble("FN_MONTO");
		tipoconversion	= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
		tipocambio 		= (rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO");
		plzodescto		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
		porcdescto		= (rs.getString("fn_porc_descuento")==null)?"":rs.getString("fn_porc_descuento");
		mntodescuento	= monto*(Double.parseDouble(porcdescto)/100);
		mntovaluado		= (monto-mntodescuento)*Double.parseDouble(tipocambio);
		estatus			= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
		ic_estatus		= (rs.getString("IC_ESTATUS_DOCTO")==null)?"":rs.getString("IC_ESTATUS_DOCTO");
		ordenEnvio = (rs.getString("IC_ORDEN_ENVIADO")==null)?"":rs.getString("IC_ORDEN_ENVIADO");
		opAcuse = (rs.getString("IC_OPERACION_CCACUSE")==null)?"":rs.getString("IC_OPERACION_CCACUSE");
		codAutorizado = (rs.getString("CODIGO_AUTORIZADO")==null)?"":rs.getString("CODIGO_AUTORIZADO");
		fechRegistro = (rs.getString("FECHA_REGISTRO")==null)?"":rs.getString("FECHA_REGISTRO");
		descrip_bins = (rs.getString("CG_DESCRIPCION_BINS")==null)?"":rs.getString("CG_DESCRIPCION_BINS");
		numero_tc = (rs.getString("NUMERO_TC")==null)?"":"XXXX-XXXX-XXXX-"+rs.getString("NUMERO_TC");
		/**********PARA CREDITO**************/
		numcredito		= (rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
		monedaLinea		= (rs.getString("moneda_linea")==null)?"":rs.getString("moneda_linea");
		modoPlazo		= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
		if(responsable.equals("D")){				
			numcredito		= (rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
			referencia		= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
			plazocred		= (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
			fchvenccred		= (rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO");
			tipocobroint	= (rs.getString("tipo_cobro_interes")==null)?"":rs.getString("tipo_cobro_interes");
			montoint		= rs.getDouble("MONTO_INTERES");
			valortaza		= (rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA");
			mntocredito		= rs.getDouble("MONTO_CREDITO");
			fchopera		= (rs.getString("df_fecha_seleccion")==null)?"":rs.getString("df_fecha_seleccion");
		}
		bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
		if(tipoCreditoXepo.equals("F")) {
		cuenta_bancaria = (rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA");
		}
		if (bandeVentaCartera.equals("S") ) {
			modoPlazo ="";
		}
		String porc_Aforo	= (rs.getString("POR_AFRO")==null)?"0":rs.getString("POR_AFRO"); //F05-2014 
		BigDecimal  por_Aforo= new BigDecimal(porc_Aforo);  //F05-2014 
		BigDecimal  montoD= new BigDecimal(monto);  //F05-2014 /			
		BigDecimal   MontoDesconta = por_Aforo.multiply(montoD);//F05-2014				
		tipo_pago = (rs.getString("IG_TIPO_PAGO")==null)?"":rs.getString("IG_TIPO_PAGO");
		if(tipo_pago.equals("1")){
			tipo_pago = "Financiamiento con intereses";
		} else if(tipo_pago.equals("2")){
			tipo_pago = "Meses sin intereses";
		}
		contenidoArchivo.append("\n" + descif.replace(',', ' ') + ","  +  epo.replace(',', ' ') + ","  + numdocto + "," + acuse + ","+ fchemision + "," + fchpublica + "," + fchvence + "," + plazodocto + "," + moneda +
					", "+ Comunes.formatoDecimal(monto,2,false) + ", " +porc_Aforo+", "+ Comunes.formatoDecimal(MontoDesconta.toPlainString(),2,false) + ", " +
					
					plzodescto + ","+ porcdescto + "," + Comunes.formatoDecimal(mntodescuento,2,false) + ",");
		if(!"".equals(cgTipoConversion))
			contenidoArchivo.append(((ic_moneda.equals(monedaLinea))?"":tipoconversion) + ", " +((ic_moneda.equals(monedaLinea))?"":""+tipocambio) + ", " +((ic_moneda.equals(monedaLinea))?"":Comunes.formatoDecimal(mntovaluado,2,false)) + ",");
		//contenidoArchivo.append(modoPlazo+","+estatus + ", "+ numcredito + ", ");
		contenidoArchivo.append(modoPlazo+",");
		if(publicaDoctosFinanciables.equals("S")){
			contenidoArchivo.append(tipo_pago + ",");
		}
		contenidoArchivo.append(estatus + ",");
		if(tipoCreditoXepo.equals("F")) {
			contenidoArchivo.append(cuenta_bancaria+ ",");
		}
		String auxNulo = "N/A";
		if("D".equals(responsable)){
			if(ic_estatus.equals("32")){
				
				contenidoArchivo.append(numcredito.replace(',', ' ') + ", "  + 
												Comunes.formatoDecimal(mntocredito,2,false) + " ,"  + 
												auxNulo + "," + 
												auxNulo + ","	+ 
												fchopera.replace(',', ' ') +", " +
												auxNulo.replace(',', ' ') + ", " +
												auxNulo.replace(',', ' ') + ", " + 
												auxNulo.replace(',', ' ') + ", " +
												descrip_bins.replace(',',' ')+", "+
												ordenEnvio.replace(',',' ')+"'"+", "+
												opAcuse.replace(',',' ')+", "+
												codAutorizado.replace(',',' ')+", "+
												numero_tc.replace(',',' ')+", "
											);
			}else{
				contenidoArchivo.append(numcredito.replace(',', ' ') + ", "  + 
												Comunes.formatoDecimal(mntocredito,2,false) + " ,"  + 
												plazocred + "," + 
												fchvenccred + ","	+ 
												fchopera.replace(',', ' ') +", " +
												referencia.replace(',', ' ') + ", " +
												Comunes.formatoDecimal(valortaza,2,false) + ", "+ 
												Comunes.formatoDecimal(montoint,2,false) + ", "+ 
												auxNulo.replace(',',' ')+", "+
												auxNulo.replace(',',' ')+", "+
												auxNulo.replace(',',' ')+", "+
												auxNulo.replace(',',' ')+", "+
												auxNulo.replace(',',' ')+", "
												
											);
			}
			contenidoArchivo.append("  "+"\n");		
		}else{
			if(ic_estatus.equals("32")){
				contenidoArchivo.append(
											 descrip_bins.replace(',',' ')+", "+
											 ordenEnvio.replace(',',' ')+"'"+", "+
											 opAcuse.replace(',',' ')+", "+
											 codAutorizado.replace(',',' ')+", "+
												numero_tc.replace(',',' ')+", "
											 
											);
			}else{
				contenidoArchivo.append(
											 auxNulo.replace(',',' ')+", "+
											 auxNulo.replace(',',' ')+", "+
											 auxNulo.replace(',',' ')+", "+
											 auxNulo.replace(',',' ')+", "+
											 auxNulo.replace(',',' ')+", "
											 
											);
			}
			contenidoArchivo.append("  "+"\n");	
		}
			
		
					
					
	}//while

	rs.close();
	con.cierraStatement();	
	
		if(numreg >= 1){
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivoCSV = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoCSV);
		}
	}
	
	}
	
	   
} catch(Exception e) {
	out.println("Error: "+e);
	System.out.println("Error  " +e); 
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
}
%>
<%=jsonObj%>

<%System.out.println("24consulta07Arext.jsp (S)"); %>