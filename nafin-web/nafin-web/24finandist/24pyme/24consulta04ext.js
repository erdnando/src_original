Ext.onReady(function() {
//----------------------------------FUNCIONES-----------------------------------
var jsonValoresIniciales = null;
var numdoc = "";
function procesaValoresIniciales(opts, success, response) {
		fp.el.mask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
				jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
				if(jsonValoresIniciales != null){
					fp.el.unmask();
				}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
}
//-----------------------------------HANDLER------------------------------------
/*
var procesarSuccessFailureGenerarTotalPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarTotalPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarTotalPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
}

var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
	var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	btnGenerarArchivo.setIconClass('');
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		btnBajarArchivo.show();
		btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivo.setHandler( function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	} else {
		btnGenerarArchivo.enable();
		NE.util.mostrarConnError(response,opts);
	}
}
*/
	var procesarSuccessFailureGenerarTotalPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarTotalPDF').enable();
		Ext.getCmp('btnGenerarTotalPDF').setIconClass('icoPdf');
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}
/*
var procesarSuccessFailureGenerarXpaginaPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarXpaginaPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarXpaginaPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
}
*/
var procesarConsultaData = function(store,arrRegistros,opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros!=null){
				if(!grid.isVisible()){
					grid.show();
				}
				//var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
				var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
				//var btnBajarXpaginaPDF = Ext.getCmp('btnBajarXpaginaPDF');
				//var btnGenerarXpaginaPDF = Ext.getCmp('btnGenerarXpaginaPDF');
				//var btnBajarTotalPDF = Ext.getCmp('btnBajarTotalPDF');
				var btnGenerarTotalPDF = Ext.getCmp('btnGenerarTotalPDF');
				var btnTotales = Ext.getCmp('btnTotales');
				var el = grid.getGridEl();
				
				var cm = grid.getColumnModel();
				cm.setHidden(13,true); //Tipo conversi�n
				cm.setHidden(14,true); //Tipo cambio
				cm.setHidden(15,true);//Monto valuado en pesos
				
				if(store.getTotalCount()>0){
					if(jsonValoresIniciales.cgTipoConversion != "" && jsonValoresIniciales.cgTipoConversion != undefined){
						cm.setHidden(13,false); //Tipo conversi�n
						cm.setHidden(14,false); //Tipo cambio
						cm.setHidden(15,false); //Monto valuado en pesos
					}
					btnTotales.enable();
					//btnGenerarXpaginaPDF.enable();
					btnGenerarTotalPDF.enable();
					//btnBajarXpaginaPDF.hide();
					//btnBajarTotalPDF.hide();
					btnGenerarArchivo.enable();
					/*btnBajarArchivo.hide();
					if(!btnBajarArchivo.isVisible()){
						btnGenerarArchivo.enable();
					} else {
						btnGenerarArchivo.disable();
					}*/
					el.unmask();
				} else {
					btnTotales.disable();
					btnGenerarArchivo.disable();
					//btnGenerarXpaginaPDF.disable();
					btnGenerarTotalPDF.disable();
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
		}
}
var procesarCambioDoctosData = function (store,arrRegistros,opts){
	if(arrRegistros != null){
		var gridCambios = Ext.getCmp('gridCambioDoctos');
		var el = gridCambios.getGridEl();
		if(store.getTotalCount() > 0) {
			el.unmask();
		} else {
			el.mask('No se encontr� ning�n cambio para este documento', 'x-mask');
		}
	}
}
var muestraGridCambios = function (grid, rowIndex,colIndex,item,event) {
	var registro = grid.getStore().getAt(rowIndex);
	var ic_documento = registro.get('CLAVE');
	cambioDoctosData.load({
		params: {
					numDoc: ic_documento
		}
	});
	var ventana = Ext.getCmp('cambioDoctos');
	if (ventana) {
		ventana.destroy();
	}
	new Ext.Window({
		layout: 'fit',
		width: 800,
		height: 200,
		id: 'cambioDoctos',
		closeAction: 'hide',
		items: [
			gridCambiosDoctos
		],
		title: 'Documento'
		}).show();
}
		
//------------------------------------STORE-------------------------------------
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPODist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoEstatusData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '24consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoMonedaData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '24consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoModoPlazoData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '24consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoModoPlazoDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var consultaData = new Ext.data.JsonStore({
			root: 'registros',
			url: '24consulta04ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
						{name: 'CLAVE'},
						{name: 'EPO'},
						{name: 'NUMDOC'},
						{name: 'ACUSE'},
						{name: 'FECHA_EMISION'},
						{name: 'DF_FECHA_VENC'},
						{name: 'FECHA_PUBLICACION'},
						{name: 'PLAZODOC'},
						{name: 'MONEDA'},
						{name: 'MONTO'},
						{name: 'CATEGORIA'},
						{name: 'POR_AFRO'},
						{name: 'MONTO_DESCONTAR'},
						{name: 'PLAZDESC'},
						{name: 'PORCDESC'},
						{name: 'MONTO_CON_DESC'},
						{name: 'CONVERSION'},
						{name: 'TIPCAMBIO'},
						{name: 'MONTO_VALUADO'},
						{name: 'MODALIDAD_PLAZO'},
						{name: 'ESTATUS'},
						{name: 'FECHACAMBIO'},
						{name: 'CAMBIOESTATUS'},
						{name: 'CAMBIOMOTIVO'},
						{name: 'CG_VENTACARTERA'},
						{name: 'IC_MONEDA'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
					load: procesarConsultaData,
					exception: {
								fn: function(proxy,type,action,optionsRequest,response,args){
										NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
										procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
								}
					}
			}
	});
	var resumenTotalesData = new Ext.data.JsonStore({
			root: 'registros',
			url: '24consulta04ext.data.jsp',
			baseParams: {
						informacion: 'ResumenTotales'
			},
			fields: [
						{name: 'NOMMONEDA', mapping: 'col1'},
						{name: 'TOTAL_REGISTROS',type:'float',mapping: 'col2'},
						{name: 'TOTAL_MONTO_DOCUMENTOS', type: 'float', mapping: 'col3'}
			],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
						exception: NE.util.mostrarDataProxyError
			}
	});
	var cambioDoctosData = new Ext.data.JsonStore({
			root: 'registros',
			url: '24consulta04ext.data.jsp',
			baseParams: {
					informacion: 'obtenCambioDoctos'
					},
			fields: [
						{name: 'FECH_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
						{name: 'CD_DESCRIPCION'},
						{name: 'CT_CAMBIO_MOTIVO'},
						{name: 'FECH_EMI_ANT', type: 'date', dateFormat: 'd/m/Y'},
						{name: 'FECH_EMI_NEW', type: 'date', dateFormat: 'd/m/Y'},
						{name: 'FN_MONTO_ANTERIOR', type: 'float'},
						{name: 'FN_MONTO_NUEVO', type: 'float'},
						{name: 'FECH_VENC_ANT', type: 'date', dateFormat: 'd/m/Y'},
						{name: 'FECH_VENC_NEW', type: 'date', dateFormat: 'd/m/Y'},
						{name: 'MODO_PLAZO_ANTERIOR'},
						{name: 'MODO_PLAZO_NUEVO'}
			],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
					load: procesarCambioDoctosData,
					exception: {
									fn: function(proxy, type, action, optionsRequest, response, args) {
									NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
									procesarCambioDoctosData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
									}
					}
			}
	});
//---------------------------------COMPONENTES----------------------------------
var elementosForma = [{
		xtype: 'container',
		//width: 885,
		anchor: '100%',
		layout: 'column',
		items: [
					{
						xtype: 'container',
						id:	'panelIzquierdo',
						columnWidth:.5,
						width: '50%',
						layout: 'form',
						items: [
									{
										xtype: 'combo',
										name: 'ic_epo',
										id: 'cmbEpo',
										allowBlank: true,
										fieldLabel: 'EPO',
										mode: 'local',
										displayField: 'descripcion',
										valueField: 'clave',
										hiddenName: 'ic_epo',
										emptyText: 'Seleccione EPO',
										forceSelection: true,
										triggerAction: 'all',
										typeAhead: true,
										minChars: 1,
										store: catalogoEPOData,
										tpl:'<tpl for=".">' +
										'<tpl if="!Ext.isEmpty(loadMsg)">'+
										'<div class="loading-indicator">{loadMsg}</div>'+
										'</tpl>'+
										'<tpl if="Ext.isEmpty(loadMsg)">'+
										'<div class="x-combo-list-item">' +
										'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
										'</div></tpl></tpl>',
										anchor: '90%'//,
											//listeners:
									},
									{
										xtype: 'combo',
										name: 'ic_cambio_estatus',
										id: 'cambioEstatusCmb',
										fieldLabel: 'Estatus',
										emptyText: 'Seleccione Estatus',
										allowBlank: true,
										mode: 'local',
										displayField: 'descripcion',
										valueField: 'clave',
										hiddenName : 'ic_cambio_estatus',
										forceSelection : true,
										triggerAction : 'all',
										typeAhead: true,
										minChars : 1,
										store: catalogoEstatusData,
										tpl:'<tpl for=".">' +
										'<tpl if="!Ext.isEmpty(loadMsg)">'+
										'<div class="loading-indicator">{loadMsg}</div>'+
										'</tpl>'+
										'<tpl if="Ext.isEmpty(loadMsg)">'+
										'<div class="x-combo-list-item">' +
										'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
										'</div></tpl></tpl>',
										anchor:'90%'	
									},
									{
										xtype: 'textfield',
										name: 'ig_numero_docto',
										id: 'no_doc_ini',
										fieldLabel: 'N�mero de documento inicial',
										allowBlank: true,
										maxLength: 15,
										anchor: '90%'
									},
									{
										xtype: 'textfield',
										name: 'cc_acuse',
										id: 'no_cc_acuse',
										fieldLabel: 'Num. acuse de carga',
										allowBlank: true,
										maxLength: 15,
										anchor: '90%'
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Fecha emisi�n docto.',
										combineErrors: false,
										msgTarget: 'side',
										anchor:'90%',
										items: [
													{
														xtype: 'datefield',
														name: 'df_fecha_emision_de',
														id: 'dc_fecha_emisionMin',
														allowBlank: true,
														startDay: 0,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoFinFecho: 'dc_fecha_emisionMax',
														margins: '0 20 0 0' //necesario para mostrar el icono de error*/
													},
													{
														xtype: 'displayfield',
														value: 'al',
														width: 24
													},
													{
														xtype: 'datefield',
														name: 'df_fecha_emision_a',
														id: 'dc_fecha_emisionMax',
														allowBlank: true,
														startDay: 1,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoInicioFecha: 'dc_fecha_emisionMin'
														//margins: '0 20 0 0' //necesario para mostrar el icono de error
													}
											]
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Fecha de vto. docto.',
										combineErrors: false,
										msgTarget: 'side',
										anchor: '90%',
										items: [
													{
														xtype: 'datefield',
														name: 'df_fecha_venc_de',
														id: 'dc_fecha_vencMin',
														allowBlank: true,
														stratDay: 0,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoFinFecha: 'dc_fecha_vencMax',
														margins: '0 20 0 0' //necesario para mostrar el icono de error
													},
													{
														xtype: 'displayfield',
														value: 'al',
														width: 24
													},
													{
														xtype: 'datefield',
														name: 'df_fecha_venc_a',
														id: 'dc_fecha_vencMax',
														allowBlank: true,
														startDay: 1,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoInicioFecha: 'dc_fecha_vencMin',
														margins: '0 20 0 0' //necesario para mostrar el icono de error
													}
										]
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Fecha de publicaci�n.',
										combineErrors: false,
										msgTarget: 'side',
										anchor: '90%',
										items: [
													{
														xtype: 'datefield',
														name: 'df_fecha_pub_de',
														id: 'dc_fecha_pubMin',
														allowBlank: true,
														startDay: 0,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoFinFecha: 'dc_fecha_pubMax',
														margins: '0 20 0 0' //Necesario para mostrar el icono de error
													},
													{
														xtype: 'displayfield',
														value: 'al',
														width: 24
													},
													{
														xtype: 'datefield',
														name: 'df_fecha_pub_a',
														id: 'dc_fecha_pubMax',
														allowBlank: true,
														startDay:1,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoInicioFecha: 'dc_fecha_pubMin',
														margins: '0 20 0 0' //necesario para mostrar el icono de error
													}
										]
									}
								]
					},
					{
						xtype: 'container',
						id: 'panelDerecho',
						columnWidth:.5,
						width: '50%',
						layout: 'form',
						items: [
									{
										xtype: 'combo',
										name: 'ic_moneda',
										fieldLabel: 'Moneda',
										emptyText: 'Seleccione Moneda',
										mode: 'local',
										displayField: 'descripcion',
										valueField: 'clave',
										hiddenName: 'ic_moneda',
										forceSelection: true,
										triggerAction: 'all',
										typeAhead: true,
										minChars: 1,
										store: catalogoMonedaData,
										tpl: NE.util.templateMensajeCargaCombo,
										anchor: '90%'
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Monto',
										msgTarget: 'side',
										combineErrors: false,
										anchor: '90%',
										items: [
													{
														xtype: 'numberfield',
														name: 'fn_monto_de',
														id: 'bMontoMin',
														allowBlank: true,
														maxLength: 12,
														width: 110,
														msgTarget: 'side',
														margins: '0 20 0 0' //necesario para mostrar el icono de error
													},
													{
														xtype: 'displayfield',
														value: 'a',
														width: 10
													},
													{
														xtype:'numberfield',
														name: 'fn_monto_a',
														id: 'bMontoMax',
														allowBlank: true,
														maxLength: 12, 
														width: 110,
														msgTarget: 'side',
														margins: '0 20 0 0' //necesario para mostrar el icono de error
													}
												]
									},
									{
										xtype: 'checkbox',
										boxLabel: 'Monto % de descuento',
										value: 'S',
										name: 'mto_descuento',
										anchor: '100%'
									},
									{
										xtype: 'checkbox',
										boxLabel: 'Solo documentos modificados',
										value: 'S',
										name: 'doctos_cambio',
										anchor: '100%'
									},
									{
										xtype: 'combo',
										name: 'modplazo',
										fieldLabel: 'Modalidad de plazo',
										emptyText: 'Seleccione modalidad',
										mode: 'local',
										displayField: 'descripcion',
										valueField: 'clave',
										hiddenName: 'modplazo',
										forceSelection:true,
										triggerAction: 'all',
										typeAhead: true,
										minChars: 1,
										store: catalogoModoPlazoData,
										tpl:'<tpl for=".">' +
										'<tpl if="!Ext.isEmpty(loadMsg)">'+
										'<div class="loading-indicator">{loadMsg}</div>'+
										'</tpl>'+
										'<tpl if="Ext.isEmpty(loadMsg)">'+
										'<div class="x-combo-list-item">' +
										'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
										'</div></tpl></tpl>',
										anchor: '90%'
									}
								]
					}
		]
}];
var gridTotales = {
	xtype: 'grid',
	store: resumenTotalesData,
	id: 'gridTotales',
	style: ' margin:0 auto;',
	hidden: true,
	columns: [
			    {
					header: 'TOTALES',
					dataIndex: 'NOMMONEDA',
					align: 'left',
					width: 250
				 },
				 {
					header: 'Registros',
					dataIndex: 'TOTAL_REGISTROS',
					width: 150,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('0,000')
				 },
				 {
					header: 'Monto Documentos',
					dataIndex: 'TOTAL_MONTO_DOCUMENTOS',
					width: 150,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				 }
				],
		width: 940,
		height: 93,
		title: 'Totales',
		tools: [
					{
						id: 'close',
						handler: function(evento, toolEl, panel, tc){
									panel.hide();
						}
					}
		],
		frame: false
};

var grid = new Ext.grid.GridPanel({
	store: consultaData,
	hidden: true,
	columns: [
					{
						header: 'EPO', tooltip: 'Nombre EPO',
						dataIndex: 'EPO',
						sortable: true, 
						resiazable: true, 
						align: 'center',
						width: 250
					},
					{
						header: 'N�mero de documento inicial',
						tooltip: 'N�mero de documento inicial',
						dataIndex: 'NUMDOC',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 200
					},
					{
						header: 'Num. acuse',
						tooltip: 'N�mero de acuse',
						dataIndex: 'ACUSE',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 120
					},
					{
						header: 'Fecha emisi�n',
						tooltip: 'Fecha de Emisi�n',
						dataIndex: 'FECHA_EMISION',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 120
					},
					{
						header: 'Fecha vencimiento',
						tooltip: 'Fecha de Vencimiento',
						dataIndex: 'DF_FECHA_VENC',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 120
					},
					{
						header: 'Fecha de publicaci�n',
						tooltip: 'Fecha de Publicaci�n',
						dataIndex: 'FECHA_PUBLICACION',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 120
					},
					{
						header: 'Plazo docto.',
						tooltip: 'Plazo docto.',
						dataIndex: 'PLAZODOC',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 120
					},
					{
						header: 'Moneda',
						tooltip: 'Moneda',
						dataIndex: 'MONEDA',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 130
					},
					{
						header: 'Monto',
						tooltip: 'Monto',
						dataIndex: 'MONTO',
						sortable: true,
						resiazable: true,
						width: 120,
						align: 'center',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						header: 'Categor�a',
						tooltip: 'Categor�a',
						dataIndex: 'CATEGORIA',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 120
					},
					{
						header : '% de Descuento Aforo ', tooltip: '% de Descuento Aforo ',
						dataIndex : 'POR_AFRO',
						sortable : true,	width : 120,	hidden: false, align: 'center',
						renderer: Ext.util.Format.numberRenderer('0.00%')
					}
					,{
						header : 'Monto a Descontar  ', tooltip: 'Monto a Descontar',
						dataIndex : 'MONTO_DESCONTAR',
						sortable : true,	width : 120,	hidden: false, align: 'right',
						renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
							var  valor  =  registro.get('MONTO')* (registro.get('POR_AFRO')/100);
							return Ext.util.Format.number(valor,'$0,0.00')
						}
					},	
					{
						header: 'Plazo para descuento en d�as',
						tooltip: 'Plazo para descuento en d�as',
						dataIndex: 'PLAZDESC',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 200
					},
					{
						header: '% de descuento',
						tooltip: '% de descuento',
						dataIndex: 'PORCDESC',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 150
					},
					{
						header: 'Monto % de descuento',
						tooltip: 'Monto % de descuento',
						dataIndex: 'MONTO_CON_DESC',
						sortable: true,
						resiazable: true,
						width: 150,
						align: 'center',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						header: 'Tipo conv.',
						tooltip: 'Tipo conv.',
						dataindex: 'CONVERSION',
						sortable: true,
						resiazable: true,
						hidden:false,
						width: 120,
						align: 'center',
						renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
											if (registro.get('TIPCAMBIO') == "")	{
													value = "";
											}
									return value;
						}
					},
					{
						header: 'Tipo cambio',
						tooltip: 'Tipo de cambio',
						dataIndex: 'TIPCAMBIO',
						sortable: true,
						resiazable: true,
						hidden: false,
						width: 150,
						align: 'center',
						renderer: function(value,metaData,registro,rowIndex,colIndex,store) {
											if (registro.get('TIPCAMBIO') == " ") {
												value = " ";
											}
											else if (registro.get('IC_MONEDA')==1) {
												value = "";
											}
											return Ext.util.Format.number(value,'$0,0.00');
						}
					},
					{
						header: 'Monto valuado en pesos',
						tooltip: 'Monto valuado en pesos',
						dataIndex: 'MONTO_VALUADO',
						sortable: true,
						resiazable: true,
						hidden: false,
						width: 180,
						align: 'center',
						renderer: function(value,metaData,registro,rowIndex,colIndex,store) {
											if(registro.get('TIPCAMBIO') == "") {
												value = " ";
											}
											return Ext.util.Format.number(value,'$0,0.00');			
						}
					},
					{
						header: 'Modalidad de plazo',
						tooltip: 'Modalidad de plazo',
						dataIndex: 'MODALIDAD_PLAZO',
						sortable: true,
						resiazable: true,
						hidden:false,
						width: 180,
						align: 'center',
						renderer: function (value,metaData,registro,rowIndex,colIndex,store) {
											if(registro.get('CG_VENTACARTERA')== "S") {
												value = "";
											}
											return value;
						}
					},
					{
						header: 'Estatus',
						tooltip: 'Estatus',
						dataIndex: 'ESTATUS',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 150
					},
					{
						xtype: 'actioncolumn',
						header: 'Cambios del documento',
						tooltip: 'Cambios del documento',
						dataIndex: 'NUMDOC',
						width: 180,
						align: 'center',
						items: [
									{
										getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
														if(registro.get('NUMDOC')!=''){
															this.items[0].tooltip = 'Ver';
															return 'iconoLupa';
														}
										},
										handler: muestraGridCambios
									}
						]
					},
					{
						header: 'Fecha de cambio de estatus',
						tooltip: 'Fecha de cambio de estatus',
						dataIndex: 'FECHACAMBIO',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 200
					},
					{
						header: 'Tipo de cambio de estatus',
						tooltip: 'Tipo de cambio de estatus',
						dataIndex: 'CAMBIOESTATUS',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 200
					},
					{
						header: 'Causa',
						tooltip: 'Causa',
						dataIndex: 'CAMBIOMOTIVO',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 120
					}			
				],
	stripeRows: true,
	loadMask: true,
	height: 400,
	width: 940,
	style: ' margin:0 auto;',
	title: 'Datos Documento Inicial',
	frame: true,
	bbar: {
				xtype: 'paging',
				pageSize: 15,
				buttonAlign: 'left',
				id: 'barraPaginacion',
				displayInfo: true,
				store: consultaData,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No hay registros.",
				items: [
							'->','-',
							{
								xtype: 'button',
								text: 'Totales',
								id: 'btnTotales',
								hidden: false,
								handler: function (boton,evento) {
											resumenTotalesData.load();
											var gridTotales = Ext.getCmp('gridTotales');
											if(!gridTotales.isVisible()){
													gridTotales.show();
													gridTotales.el.dom.scrollIntoView();
											};
								}
							},
							'-',
							{
								xtype: 'button',
								text: 'Generar Archivo',
								tooltip: 'Imprime los registros en formato CSV.',
								iconCls: 'icoXls',
								id: 'btnGenerarArchivo',
								handler: function(boton, evento) {
									boton.disable();
									boton.setIconClass('loading-indicator');
									Ext.Ajax.request({
										url: '24consulta04aext.jsp',
										params:	Ext.apply(fp.getForm().getValues(),{
											informacion: 'ArchivoCSV'
										}),
										callback: procesarSuccessFailureGenerarArchivo
									});
								}
							},/*
							{
								xtype: 'button',
								text: 'Bajar Archivo',
								id: 'btnBajarArchivo',
								hidden: true
							},
							'-',
							{
								xtype: 'button',
								text: 'Generar x P�gina PDF',
								id: 'btnGenerarXpaginaPDF',
								handler: function(boton, evento) {
									boton.disable();
									boton.setIconClass('loading-indicator');
									var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
									Ext.Ajax.request({
									url: '24consulta04iext.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
											informacion: 'ArchivoXpaginaPDF',
											start: cmpBarraPaginacion.cursor,
											limit: cmpBarraPaginacion.pageSize
									}),
									callback: procesarSuccessFailureGenerarXpaginaPDF
									});
								}
							},
							{
								xtype: 'button',
								text: 'Bajar x P�gina PDF',
								id: 'btnBajarXpaginaPDF',
								hidden: true
							},*/
							'-',
							{
								xtype: 'button',
								text: 'Generar Todo',
								tooltip: 'Imprime los registros en formato PDF.',
								iconCls: 'icoPdf',
								id: 'btnGenerarTotalPDF',
								handler: function(boton, evento) {
									boton.disable();
									boton.setIconClass('loading-indicator');
									Ext.Ajax.request({
											url: '24consulta04ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
											informacion: 'ArchivoTotalPDF'
										}),
										callback: procesarSuccessFailureGenerarTotalPDF
									});
								}
							}
							/*{
								xtype: 'button',
								text: 'Bajar Todo PDF',
								id: 'btnBajarTotalPDF',
								hidden: true
							},*/
					]
			}		
});
	var gridCambiosDoctos = {
		xtype: 'grid',
		store: cambioDoctosData,
		id: 'gridCambioDoctos',
		columns: [
			{
				header: 'Fecha del Cambio',
				dataIndex: 'FECH_CAMBIO',
				align: 'left',	
				width: 100, 
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Cambio Estatus',
				dataIndex: 'CD_DESCRIPCION',
				align: 'center',	
				width: 200
			},
			{
				header: 'Fecha Emision Anterior',
				dataIndex: 'FECH_EMI_ANT',
				align: 'left',	
				width: 100, 
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha Emision Nueva',
				dataIndex: 'FECH_EMI_NEW',
				align: 'left',	
				width: 100, 
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Monto Anterior',
				dataIndex: 'FN_MONTO_ANTERIOR',
				align: 'right',	
				width: 120,	
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Nuevo',
				dataIndex: 'FN_MONTO_NUEVO',
				align: 'right',	
				width: 120,	
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Fecha Vencimiento Anterior',
				dataIndex: 'FECH_VENC_ANT',
				align: 'left',	
				width: 100, 
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha Vencimiento Nuevo',
				dataIndex: 'FECH_VENC_NEW',
				align: 'left',	
				width: 100, 
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Modalidad de Plazo Anterior',
				dataIndex: 'MODO_PLAZO_ANTERIOR',
				align: 'left',	
				width: 150
			},
			{
				header: 'Modalidad de Plazo Nuevo',
				dataIndex: 'MODO_PLAZO_NUEVO',
				align: 'left',	
				width: 150
			}
		],
		height: 300,
		title: '',
		frame: false,
		loadMask: true
	};
var fp=new Ext.form.FormPanel({
	id: 'forma',
	width: 885,
	style: ' margin:0 auto;',
	title: 'Criterios de b�squeda',
	hidden: false,
	frame: true,
	collapsible: true,
	titleCollapse: false,
	//bodyStyle: 'padding: 6px',
	labelWidth: 130,
	defaultType: 'textfield',
	defaults: {
		msgTarget: 'side'/*,
		anchor: '-50'*/
	},
	items: elementosForma,
	monitorValid: false,
	buttons: [
					{
						text: 'Consultar',
						id: 'btnConsultar',
						iconCls: 'icoBuscar',
						formBind: true,
						handler: function (boton,evento){
									grid.hide();
									var totales = Ext.getCmp('gridTotales');
									if(totales.isVisible()){
											totales.hide();
									}
									var ventanaDoctos = Ext.getCmp('cambioDoctos');
									if (ventanaDoctos){
											ventanaDoctos.destroy();
									}
									if(!verificaPanelIzquierda){
										return;
									}
									if(!verificaPanelDerecha){
										return;
									}
									var fechaEmisionMin = Ext.getCmp('dc_fecha_emisionMin');
									var fechaEmisionMax = Ext.getCmp('dc_fecha_emisionMax');
									var fechaVenceMin = Ext.getCmp('dc_fecha_vencMin');
									var fechaVenceMax = Ext.getCmp('dc_fecha_vencMax');
									var fechaPubMin = Ext.getCmp('dc_fecha_pubMin');
									var fechaPubMax = Ext.getCmp('dc_fecha_pubMax');
									var der_montoMin = Ext.getCmp('bMontoMin');
									var der_montoMax = Ext.getCmp('bMontoMax');
									
									if(!Ext.isEmpty(fechaEmisionMin.getValue())||!Ext.isEmpty(fechaEmisionMax.getValue())){
											if(Ext.isEmpty(fechaEmisionMin.getValue())){
												fechaEmisionMin.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
												fechaEmisionMin.focus();
												return;
											}else if (Ext.isEmpty(fechaEmisionMax.getValue())){
												fechaEmisionMax.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
												fechaEmisionMax.focus();
												return;
											}
									}
									if (!Ext.isEmpty(fechaVenceMin.getValue())||!Ext.isEmpty(fechaVenceMax.getValue())){
											if(Ext.isEmpty(fechaVenceMin.getValue())){
												fechaVenceMin.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
												fechaVenceMin.focus();
												return;
											}else if (Ext.isEmpty(fechaVenceMax.getValue())){
												fechaVenceMax.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
												fechaVenceMax.focus();
												return;
											}
									}
									if (!Ext.isEmpty(fechaPubMin.getValue())||!Ext.isEmpty(fechaPubMax.getValue())){
											if(Ext.isEmpty(fechaPubMin.getValue())){
												fechaPubMin.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
												fechaPubMin.focus();
												return;
											}else if (Ext.isEmpty(fechaPubMax.getValue())){
												fechaPubMax.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
												fechaPubMax.focus();
												return;
											}
									}
									if (!Ext.isEmpty(der_montoMin.getValue())||!Ext.isEmpty(der_montoMax.getValue())){
											if(Ext.isEmpty(der_montoMin.getValue())){
													der_montoMin.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
													der_montoMin.focus();
													return;
											} else if (Ext.isEmpty(der_montoMax.getValue())){
													der_montoMax.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
													der_montoMax.focus();
													return;
											}
									}
									if( !Ext.getCmp('forma').getForm().isValid()){
										return;
									}
									fp.el.mask('Enviando...','x-mask-loading');
									consultaData.load({
											params: Ext.apply(fp.getForm().getValues(),{
											operacion: 'Generar', //Generar datos para la consulta
											start: 0,
											limit: 15
											})
									});
						}
					},
					{
						text: 'Limpiar',
						iconCls: 'icoLimpiar',
						handler: function(){
							window.location = '24consulta04ext.jsp'
						}
					}
	]
});
	function verificaPanelIzquierda(){
		var Panel = Ext.getCmp('panelIzquierda');
		var valid = true;
		Panel.items.each(function(panelItem, index, totalCount){
				if(!panelItem.isValid()) {
						valid = false;
						return false;
				}
		});
		return valid;
	}
	
	function verificaPanelDerecha(){
		var Panel = Ext.getCmp('panelDerecha');
		var valid = true;
		Panel.items.each(function(panelItem,index,totalCount){
				if(!panelItem.isValid()){
						valid = false;
						return false;
				}
		});
		return valid;
	};
//----------------------------------PRINCIPAL-----------------------------------
var pnl = new Ext.Container({
	id: 'contenedorPrincipal',
	applyTo: 'areaContenido',
	width: 949,
	style: 'margin:0 auto;',
	height: 'auto',
	items: [
		fp,
		NE.util.getEspaciador(20),
		grid,
		gridTotales
	]
});

//Peticion para obtener valores iniciales y la parametrizaci�n
Ext.Ajax.request({
		url: '24consulta04ext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
});

catalogoEPOData.load();
catalogoEstatusData.load();
catalogoMonedaData.load();
catalogoModoPlazoData.load();
});