var winwA = null;
var winwB = null;
 
var muestraRespuesta;
var preGuardaOrdenTrans;
var cancelaTransNetPay;
var idOrdenPreguardado = '';
function realizaPeticionTrans(winA, winB, IdOrden, vAccion){
	winwA = winA;
	winwB = winB;
	if(vAccion=='PREGUARDADO'){
		//var numOrdenId = winB.getElementById('OrderNumber').value;
		preGuardaOrdenTrans(IdOrden);
	}else if(vAccion=='ACEPTAR'){
		muestraRespuesta();
	}else if(vAccion=='CANCELAR'){
		cancelaTransNetPay();
	}
}

function reSizeWinIframe(xSize, ySize){
  reSizeIframe(xSize,ySize)

}


var texto1 = ['Se deber�n seleccionar el IF y plazo de financiamiento para obtener cr�dito de los siguientes documentos'];
var texto2 = ['La tasa de inter�s aplicada a los documentos seleccionados es informativa, el valor real de la tasa de inter�s ser� dada a conocer por el IF que realice la operaci�n.'];
var cadEpoCemex = ['En este acto manifiesto mi aceptaci�n para que sea(n) financiados el(los) DOCUMENTO(S) que han sido publicados, mismo(s) que contiene(n) Derechos de Cobro a mi cargo y a favor de la Empresa de Primer Orden por lo que en esta misma fecha me obligo a cubrir al Intermediario Financiero, los intereses que se detallan en esta pantalla. Manifiesto tambi�n mi obligaci�n y aceptaci�n de pagar el 100% del valor del(los) DOCUMENTO(S) en la fecha de vencimiento al Intermediario Financiero.'];
var cadEpo = ['En este acto manifiesto mi aceptaci�n para que sea(n) descontado(s) el(los) DOCUMENTO(S) que he seleccionado, mismo(s) que contiene(n) Derechos de Cobro a mi cargo y a favor de la Empresa de Primer Orden por lo que en esta misma fecha me obligo a cubrir al Intermediario Financiero, los intereses que se detallan en esta pantalla.Manifiesto tambi�n mi obligaci�n y aceptaci�n de pagar el 100% del valor del(los) DOCUMENTO(S) en la fecha de vencimiento al Intermediario Financiero.'];
var texto4 = ['NOTA: Las columnas con subIndice 1 son de Datos Documento Inicial   y subIndice 2 son de Datos Documento Final '];

var texto6 = ['En este acto manifiesto mi aceptaci�n para que sea(n) descontado(s) el(los) DOCUMENTO(S) que he seleccionado,',
'mismo(s) que contiene(n) Derechos de Cobro a mi cargo y a favor de la Empresa de Primer Orden por lo que en esta misma fecha me obligo a cubrir al Intermediario Financiero, los intereses que se detallan en esta pantalla.',
'Manifiesto tambi�n mi obligaci�n y aceptaci�n de pagar el 100% del valor del(los) DOCUMENTO(S) en la fecha de vencimiento al Intermediario Financiero. '
];

var texto7 = ['Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
							'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
							'Cd. De M�xico 50-89-61-07. <br>'+
							'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)']; 
              
var cadEpoNew = ['Manifiesto mi aceptaci�n para que se realice el DESCUENTO y/o FACTORAJE del (los) Documento (s) inicial (es) que he seleccionado, y sea (n) sustituido (s) por el (los) '+
                'DOCUMENTO (S) FINAL (ES) para tal efecto, en t�rminos del �Contrato de Financiamiento a Clientes y Distribuidores�, mismo (s) que contiene (n) Derechos de Cobro a mi cargo y a favor '+
                'de la Empresa de Primer Orden. Asimismo, en esta misma fecha me obligo a cubrir al intermediario Financiero los intereses que se detallan en esta pantalla, o bien al vencimiento de los '+
                'mismos, seg�n sea indicado. Adicionalmente manifiesto mi obligaci�n y aceptaci�n de pagar el 100% del valor de los DOCUMENTOS FINALES en la fecha de su vencimiento al Intermediario Final'+
                '<br/><br/>Por otra parte, a partir del 17 de octubre del 2018 la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que s� ha emitido o emitir� a su CLIENTE o DISTRIBUIDOR '+
                'el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.'];


var cadEpoNewTC = ['Manifiesto mi voluntad para que en t�rminos del �Contrato de Financiamiento a Clientes y Distribuidores�, pueda cubrir el 100% del valor del (los) Documento (s) inicial (es) '+
                 'que he seleccionado mediante el uso de Tarjeta de Cr�dito. El pago estar� sujeto a la autorizaci�n de la tarjeta de cr�dito y verificaci�n del Banco o intermediario emisor.'];
 
Ext.onReady(function() {
	var globalTipoPago = 1;
	var strNombreUsuario = Ext.getDom('strNombreUsuario').value;	
	//variables Globales
	var hidNumLineas;
	var hidNumTasas; 
	var Auxiliar;
	var fechaHoy = Ext.getDom('fechaHoy').value;	
	var diasInhabiles  = Ext.getDom('diasInhabiles').value;	
	var diasInhabilesXAnio  = Ext.getDom('diasInhabilesXAnio').value;	
	var parTipoCambio = Ext.getDom('parTipoCambio').value;	
        var operaContrato = Ext.getDom('operaContrato').value;
        if(operaContrato == "S"){
            cadEpoNew = ['En este acto manifiesto mi aceptaci�n para que sea(n) financiado(s) el(los) DOCUMENTO(S) que he seleccionado, mismo(s) que contiene(n) cuentas pendientes de pago a mi '+
                        'cargo y a favor de la EMPRESA DE PRIMER ORDEN por lo que en esta misma fecha me obligo a cubrir al INTERMEDIARIO FINANCIERO los intereses que se detallan en esta pantalla.'+
                        'Manifiesto tambi�n mi obligaci�n y aceptaci�n de pagar el 100% del valor de(los) DOCUMENTO(S) en la fecha de vencimiento al INTERMEDIARIO FINANCIERO.'];
            
        }
        
        
	var modificado = [];
	var ic_documento = [];
	var ic_moneda_docto = [];
	var ic_moneda_linea = [];
	var monto = [];
	var monto_valuado = [];
	var monto_credito = [];
	var monto_tasa_int = [];
	var monto_descuento = [];
	var tipo_cambio = [];
	var plazo = [];
	var fecha_vto = [];
	var referencia_tasa = [];
	var valor_tasa_puntos = [];
	var ic_tasa = [];
	var cg_rel_mat = [];
	var fn_puntos = [];
	var ic_linea_credito = [];	
	var ic_bins = [];	
	var totalInteresMN =0;
	var totalInteresUSD=0;
	var totalCapitalInteresMN =0;
	var totalCapitalInteresUSD=0;
	var Noelementos=0; 
	
	var OrderId = '';
	var TransactionId = '';
	var ResponseCode = '';
	var ResponseMessage = '';
	var AuthCode = '';
	var AuthDate = '';
  var Last4Digit = '';
  var montoTotalAcuse =0.0;
  var totalDoctosAcuse = 0;
  
  var vgblAfilComercio = '';
  var csMesesSinInt = '';
  var hidNumTasasMeses;
  var tasaMeses;	 
  var  tipoPago = [];
  var meseSinIntereses = [];

	var cancelar =  function() {  
		modificado = [];
		ic_documento = [];
		ic_moneda_docto = [];
		ic_moneda_linea = [];
		monto = [];
		monto_valuado = [];
		monto_credito = [];
		monto_tasa_int = [];
		monto_descuento = [];
		tipo_cambio = [];
		plazo = [];
		fecha_vto = [];
		referencia_tasa = [];
		valor_tasa_puntos = [];
		ic_tasa = [];
		cg_rel_mat = [];
		fn_puntos = [];	 
		ic_linea_credito= [];	
		ic_bins = [];
		tipoPago = [];
		meseSinIntereses = [];
		
		
	}
		
	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
	

	
	var revisaEntero1 =  function( campo) { 
		var numero=campo;
		if (!isdigit(numero)) {
			Ext.MessageBox.alert('Mensaje','El valor no es un n�mero v�lido.');
			return false;
		}	else{
			return true;
		}
	}

	var Tmes = new Array();
	var sCadena;
	Tmes[1] = "31"; Tmes[2] ="28"; Tmes[3]="31"; Tmes[4]="30"; Tmes[5]="31"; 
	Tmes[6]="30";
	Tmes[7] = "31"; Tmes[8] ="31"; Tmes[9]="30"; Tmes[10]="31";Tmes[11]="30"; 
	Tmes[12]="31";

	var fnDesentrama =  function( Cadena) { 
   var Dato;
   sCadena      = Cadena
   tamano       = sCadena.length;
   posicion     = sCadena.indexOf("/");
   Dato         = sCadena.substr(0,posicion);
   cadenanueva  = sCadena.substring(posicion + 1,tamano);
   sCadena      = cadenanueva;
   return Dato;
}

//-----------------------------------------------------------------------------
muestraRespuesta = function(){
	ventanaPagoTC.hide();
	
	OrderId = winwB.getElementById('OrderId').value;
	TransactionId = winwB.getElementById('TransactionId').value;
	ResponseCode = winwB.getElementById('ResponseCode').value;
	ResponseMessage = winwB.getElementById('ResponseMessage').value;
	AuthCode = winwB.getElementById('AuthCode').value;
	AuthDate = winwB.getElementById('AuthDate').value;
  Last4Digit = winwB.getElementById('Last4Digit').value;
	
	
						
		if(globalTipoPago==2){

			
			var gridColModAcuse = gridAcuse.getColumnModel();
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('TIPO_CONV'),true);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('TIPO_CAMBIO'),true);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('MONTO_VALUADO'),true);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('REFERENCIA'),true);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('VALOR_TASA_PUNTOS'),true);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('MONTO_TASA_INTERES'),true);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('MONTO_TOTAL_CAPITAL'),true);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('DESC_BIN'),false);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('ID_ORDEN_TRANS'),false);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('ID_TRANSACCION'),false);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('CODIGO_AUTORIZA'),false);
      gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('NUMERO_TARJETA'),false);
			
		
			
			var storeAcuse = gridAcuse.getStore();
			montoTotalAcuse =0.0;
      totalDoctosAcuse = 0;
			storeAcuse.each(function(record){
        totalDoctosAcuse += 1
        montoTotalAcuse += Number(record.data['MONTO_FINAL']);
				record.data['ID_ORDEN_TRANS']=OrderId;
				record.data['ID_TRANSACCION']=TransactionId;
				record.data['CODIGO_AUTORIZA']=ResponseCode+' - '+ResponseMessage;
        record.data['NUMERO_TARJETA']= 'XXXX-XXXX-XXXX-'+Last4Digit;
				record.commit();
			});
			
		}else{
			var gridColModAcuse = gridAcuse.getColumnModel();
			gridColModAcuse.setHidden(gridColModPreacuse.findColumnIndex('TIPO_CONV'),false);
			gridColModAcuse.setHidden(gridColModPreacuse.findColumnIndex('TIPO_CAMBIO'),false);
			gridColModAcuse.setHidden(gridColModPreacuse.findColumnIndex('MONTO_VALUADO'),false);
			gridColModAcuse.setHidden(gridColModPreacuse.findColumnIndex('REFERENCIA'),false);
			gridColModAcuse.setHidden(gridColModPreacuse.findColumnIndex('VALOR_TASA_PUNTOS'),false);
			gridColModAcuse.setHidden(gridColModPreacuse.findColumnIndex('MONTO_TASA_INTERES'),false);
			gridColModAcuse.setHidden(gridColModPreacuse.findColumnIndex('MONTO_TOTAL_CAPITAL'),false);
			gridColModAcuse.setHidden(gridColModPreacuse.findColumnIndex('DESC_BIN'),true);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('ID_ORDEN_TRANS'),true);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('ID_TRANSACCION'),true);
			gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('CODIGO_AUTORIZA'),true);
      gridColModAcuse.setHidden(gridColModAcuse.findColumnIndex('NUMERO_TARJETA'),true);
			
		}
							
		
		
		
		if(ResponseCode=="00" ){
			window.scrollTo(0, 0);
      Ext.Msg.alert('Aviso','La transacci�n fue aprobada');	
      
      gridPreAcuse.hide();
      gridEditable.hide();
      fp.hide();			
      gridMonitor.hide();
      gridMontos.show();
      gridAcuse.show();
      gridCifrasControl.show();
      gridTotales.hide();
      ctexto1.hide();
      ctexto2.hide();
      ctexto4.hide();
      ctexto3.hide();	
      ctexto5.hide();
      ctexto6.show();
      Ext.Ajax.request({
        url : '24forma02.data.jsp',
        params : {
          informacion: 'GeneraAcuse',					
          modificado:modificado,
          ic_documento:ic_documento,
          ic_moneda_docto:ic_moneda_docto,
          ic_moneda_linea:ic_moneda_linea,
          monto:monto,
          monto_valuado:monto_valuado,
          monto_credito:monto_credito,
          monto_tasa_int:monto_tasa_int,
          monto_descuento:monto_descuento,
          tipo_cambio:tipo_cambio,
          plazo:plazo,
          fecha_vto:fecha_vto,
          referencia_tasa:referencia_tasa,
          valor_tasa_puntos:valor_tasa_puntos,
          ic_tasa:ic_tasa,
          cg_rel_mat:cg_rel_mat,
          fn_puntos:fn_puntos,
          ic_linea_credito:ic_linea_credito,
          ic_bins: ic_bins,
          cmbTipoPago: globalTipoPago,
          OrderId: OrderId,
          TransactionId: TransactionId,
          ResponseCode: ResponseCode,
          ResponseMessage: ResponseMessage,
          AuthCode: AuthCode,
          AuthDate: AuthDate,
          Last4Digit:Last4Digit
        },
        callback: procesarSuccessFailureAcuse
      });
		}else{
			
			var cmbTC =  Ext.getCmp("cmbTCredTodos1");
			record = cmbTC.findRecord(cmbTC.valueField, cmbTC.getValue());		
			
			pnl.el.mask('Procesando Transacci�n... ', 'x-mask');
			Ext.Ajax.request({
				url : '24forma02.data.jsp',
				params : {
					informacion: 'TransaccionRechazada',
					cmbTipoPago: globalTipoPago,
					OrderId: idOrdenPreguardado,
					TransactionId: TransactionId,
					ResponseCode: ResponseCode,
					ResponseMessage: ResponseMessage,
					AuthCode: AuthCode,
					AuthDate: AuthDate,
					modificado:modificado,
					ic_epo: Ext.getCmp("ic_epo1").getValue(),
					ic_if : record.data['claveIf']
				},
				callback: procesarSuccessFailureTransRechazada
			});
		}
		
	//});
}

reSizeIframe = function(x, y){
  ventanaPagoTC.setSize(x+30,y+55);
	iframeExt.setSize(x+20,y+25);

}

cancelaTransNetPay = function(){
	ventanaPagoTC.hide();
}

preGuardaOrdenTrans = function(numOrdenId){
	idOrdenPreguardado = numOrdenId;
	ventanaPagoTC.setTitle('Informaci�n del Pago');
	Ext.Ajax.request({
			url : '24forma02.data.jsp',
			params : {
				informacion: 'PreOrdenTransaccionTmp',					
				modificado:modificado,
				ic_documento:ic_documento,
				ic_moneda_docto:ic_moneda_docto,
				
				monto_credito:monto_credito,
				ic_bins: ic_bins,
				cmbTipoPago: globalTipoPago,
				OrderId: numOrdenId
			},
			callback: procesarSuccessFailurePreGuardadoTrans
		});		
}

	//Inicializa validaciones
	var inicializaTasasXPlazo =  function( valor, metadata, registro, info) { 
		var plazo;	
		if(registro.data['TIPOPAGO'] ==1) {
			if(registro.data['IC_TIPO_FINANCIAMIENTO'] ==1) {
				plazo = registro.data['PLAZO_PRONTO_PAGO'];
			}else{		
				plazo= registro.data['PLAZO_FINAL'];	
			}					
			if(plazo!='0') {	
				return obtenTasaXPlazoIni( valor, metadata, registro, info);
			}else {
				return '0';
			}	
		}else if(registro.data['TIPOPAGO'] ==2) {		 	
			return obtenTasaXPlazoIniMeses( valor, metadata, registro, info);			 
		}
	}



	var obtenTasaXPlazoIniMeses =  function( valor, metadata, registro, info) { 
			var i = 0;
			var plazo = registro.data['PLAZO_FINAL'];
			var meses = registro.data['MESES_INTERESES']; 				
			
			var	numTasasM = hidNumTasasMeses;	  	
			var i=0;
			var auxIcTasa;	
			var auxIcMoneda;		
			var iPlazoCredito = parseInt(plazo);
			var icMoneda			= registro.data['IC_MONEDA_LINEA'];	
			var icTasa				= registro.data['IC_TASA'];   
			var  iPlazoTasaIni;
			var iPlazoTasaFinal;
			var ok;  
			var tipoPiso			= registro.data['TIPO_PISO'];
			var montoAuxInt		= registro.data['MONTO_AUX_INTERES'];
			var montoCredito	= registro.data['MONTO_CREDITO']; 
			var valorTasaPuntos = registro.data['VALOR_TASA_PUNTOS'];
			var montoCapitalInt = registro.data['MONTO_TOTAL_CAPITAL'];
			var montoInt;		 
			var cgRelMat =  registro.data['CG_REL_MAT'];
			var auxCgRelMat;
			
			if(numTasasM==1){		
				auxIcTasa		= eval('tasaMeses.ic_tasa0'); 
				auxIcMoneda		= eval('tasaMeses.ic_moneda0'); 
				iPlazoTasaIni				= eval('tasaMeses.plazo_ini0'); 
				iPlazoTasaFinal				= eval('tasaMeses.plazo_final0'); 				
				auxCgRelMat				= eval('tasaMeses.auxCgRelMat0'); 
								
				if(icMoneda==auxIcMoneda && iPlazoCredito>=iPlazoTasaIni && iPlazoCredito<=iPlazoTasaFinal ){
					
					icTasa			= auxIcTasa;	
					cgRelMat  =  auxCgRelMat;		
					if(tipoPiso=="1"){
						montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360,2)*100)/100;
					}else{
						montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
					}
					montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
					montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);				
				
				}
			}else{
				for(i=0;i<numTasasM;i++){
					auxIcTasa		= eval('tasaMeses.ic_tasa'+i); 
					auxIcMoneda		= eval('tasaMeses.ic_moneda'+i); 
					iPlazoTasaIni				= eval('tasaMeses.plazo_ini'+i); 
					iPlazoTasaFinal				= eval('tasaMeses.plazo_final'+i); 
					auxCgRelMat				=  eval('tasaMeses.auxCgRelMat'+i); 	
					
					if(icMoneda==auxIcMoneda && iPlazoCredito>=iPlazoTasaIni && iPlazoCredito<=iPlazoTasaFinal ){
						icTasa			= auxIcTasa;
						cgRelMat  =  auxCgRelMat;	
						
						if(tipoPiso=="1"){
							montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360,2)*100)/100;
						}else{
							montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
						}
						montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
						montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);		
					
					}
				} 
			}	
			
			
			if(info =='ic_tasa') { ok = icTasa; }	
			if(info =='ValorTasaInteres' ) { ok = valorTasaPuntos;    }
			if(info =='MontodeIntereses' ) { ok = montoInt; 	}
			if(info =='MontoCapitalInteres' ) { ok = montoCapitalInt; }
			if(info =='ReferenciaTasa' ) { ok = ''; }						
			if(info =='montoAuxi' ) { ok = '0'; }	
			if(info =='cg_rel_mat' ) { ok = cgRelMat; }	
			if(info =='fnPuntos' ) { ok = '0'; }			
			return ok;
			
	}

var obtenTasaXPlazoIni =  function( valor, metadata, registro, info) { 
		var i = 0;
		var plazo;
		if(registro.data['IC_TIPO_FINANCIAMIENTO'] ==1) {
		 plazo = registro.data['PLAZO_PRONTO_PAGO'];
		}else{
			plazo= registro.data['PLAZO_FINAL'];	
		}
		
		var referenciaTasa = registro.data['REFERENCIA'];
		var valorTasa			= registro.data['VALOR_TASA']; 
		var icMoneda			= registro.data['IC_MONEDA_LINEA'];
		var icIf				  = registro.data['IC_IF']; 
		var icTasa				= registro.data['IC_TASA'];  
		var cgRelMat			= registro.data['CG_REL_MAT'];  
		var fnPuntos			= registro.data['FN_PUNTOS']; 
		var tipoPiso			= registro.data['TIPO_PISO'];  
		var montoAuxInt		= registro.data['MONTO_AUX_INTERES'];
		var montoCredito	= registro.data['MONTO_CREDITO']; 
		var montoInt			= registro.data['MONTO_TASA_INTERES'];  
		var montoCapitalInt = registro.data['MONTO_TOTAL_CAPITAL']; 
		var valorTasaPuntos = registro.data['VALOR_TASA_PUNTOS'];
		var	numTasas = hidNumTasas;		
		var i=0;
		var auxIcIf;
		var auxReferenciaTasa;
		var auxIcTasa;
		var auxValorTasa;
		var auxIcMoneda;
		var auxPlazoDias;
		var iPlazoCredito = parseInt(plazo);
		var iPlazoTasa;
		var iPlazoTasaAux = 9999999999;
		var auxPuntos;
		var ok;  	
		if(numTasas==1){
			auxIcIf			= eval('Auxiliar.auxIcIf0');   
			auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa0'); 
			auxIcTasa			= eval('Auxiliar.auxIcTasa0'); 
			auxCgRelMat		= eval('Auxiliar.auxCgRelMat0'); 
			auxFnPuntos		= eval('Auxiliar.auxFnPuntos0'); 
			auxValorTasa		= eval('Auxiliar.auxValorTasa0'); 
			auxIcMoneda		= eval('Auxiliar.auxIcMoneda0'); 
			auxPlazoDias		= eval('Auxiliar.auxPlazoDias0'); 
			iPlazoTasa			= parseInt(auxPlazoDias);
					
			if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
				iPlazoTasaAux			= iPlazoTasa;
				referenciaTasa	= auxReferenciaTasa;		 	
				valorTasa			= auxValorTasa; 
				icTasa			= auxIcTasa;
				cgRelMat			= auxCgRelMat;
				valorTasaPuntos			= auxFnPuntos; 
				auxPuntos			= eval('Auxiliar.auxPuntos0'); 	
				
        if(csMesesSinInt=='S' &&  registro.data['TIPOPAGO'] ==2 ){
          valorTasaPuntos = 0.0;
        }
			
				if(tipoPiso=="1"){
					montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360,2)*100)/100;
				}else{
					montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
				}
				montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
				montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);
			}
		}else{
			for(i=0;i<numTasas;i++){
				auxIcIf			= eval('Auxiliar.auxIcIf'+i);   
				auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa'+i); 
				auxIcTasa			= eval('Auxiliar.auxIcTasa'+i); 
				auxCgRelMat		= eval('Auxiliar.auxCgRelMat'+i); 
				auxFnPuntos		= eval('Auxiliar.auxFnPuntos'+i); 
				auxValorTasa		= eval('Auxiliar.auxValorTasa'+i); 
				auxIcMoneda		= eval('Auxiliar.auxIcMoneda'+i); 
				auxPlazoDias		= eval('Auxiliar.auxPlazoDias'+i);
				
				iPlazoTasa			= parseInt(auxPlazoDias);
				//alert("iteracion "+ i + " plazo credito = "+iPlazoCredito+" Plazo Tasa = "+iPlazoTasa +" plazoTasaAux ="+iPlazoTasaAux);
				if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
					//alert("Si entro en el if "+iPlazoTasa + "iplazotasaaux = "+iPlazoTasaAux+" i:: "+i);
					iPlazoTasaAux 			= iPlazoTasa;
					referenciaTasa	= auxReferenciaTasa;		 	
					valorTasa 		= auxValorTasa; 			
					icTasa			= auxIcTasa;
					cgRelMat			= auxCgRelMat;
					valorTasaPuntos			= auxFnPuntos;
					auxPuntos			= eval('Auxiliar.auxPuntos'+i); 
          
          if(csMesesSinInt=='S' &&  registro.data['TIPOPAGO'] ==2 ) { 
            valorTasaPuntos = 0.0;
          }
					/*if(cgRelMat=='+')
						valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
					else if(cgRelMat=='-')
						valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
					else if(cgRelMat=='*')
						valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
					else if(cgRelMat=='/')
						valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);	
					*/
					if(tipoPiso=="1"){
						montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360)*100,2)/100;						
					}else{
						montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
					}
					montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
					montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);
				}
			}//for   
		}		
		if(info =='ValorTasaInteres' ) { ok = valorTasaPuntos;    }
		if(info =='MontodeIntereses' ) { ok = montoInt; 	}
		if(info =='MontoCapitalInteres' ) { ok = montoCapitalInt; }
		if(info =='ReferenciaTasa' ) { ok = referenciaTasa; }		
		if(info =='ic_tasa') { ok = icTasa; }		
		if(info =='montoAuxi' ) { ok = montoAuxInt; }	
		if(info =='cg_rel_mat' ) { ok = cgRelMat; }	
		if(info =='fnPuntos' ) { ok = auxPuntos; }	
		
		
	return ok;
}

	var validaplazo =  function( opts, success, response, registro) { 
		//function validaplazo(objeto, plazoIF,dias_minimo,dias_maximo,indice){	
		var plazoIF = registro.data['PLAZO_VALIDO'];  
		var dias_minimo = registro.data['DIAS_MINIMO'];  
		var dias_maximo = registro.data['DIAS_MAXIMO']; 
		var PlazoPyme;
		if(registro.data['IC_TIPO_FINANCIAMIENTO'] ==1) {
		 PlazoPyme = registro.data['PLAZO_PRONTO_PAGO'];
		}else{
			PlazoPyme= registro.data['PLAZO_FINAL'];	
		}		
		var x = 0;
		var  mensajetxt = "";
		if(plazoIF!="") {
			plazoIF = parseInt(plazoIF);
		}		
		if (PlazoPyme > plazoIF){    
			Ext.MessageBox.alert("Mensaje","El plazo capturado "+PlazoPyme+ " no puede ser mayor a "+plazoIF); 
			return false;
		}else {
			mensajetxt =sumaFecha( opts, success, response, registro);
			if(mensajetxt!=""){
				Ext.MessageBox.alert("Mensaje"," "+mensajetxt);	
				return false;
			}
			inicializaTasasXPlazo2(opts, success, response, registro);	
			
			if(validaFechaLineaCredito('', '', '', registro)==false) {
				registro.data['MONTO_TASA_INTERES']=0;
				registro.data['MONTO_TOTAL_CAPITAL']=0;
				registro.data['VALOR_TASA_PUNTOS']=0;
				registro.data['REFERENCIA']=0;		
				registro.commit();	
				return false;
			}else {
				return true;
				registro.commit();				
			}
			
		  // return true;
		}
	}//termina funcion


	var inicializaTasasXPlazo2 =  function( opts, success, response, registro) { 
		var plazo;		
		
		if(registro.data['IC_TIPO_FINANCIAMIENTO'] ==1) {
		 plazo = registro.data['PLAZO_PRONTO_PAGO'];
		}else{
			plazo= registro.data['PLAZO_FINAL'];	
		}		
		
		if(plazo!='0') {	
			return obtenTasaXPlazo( opts, success, response, registro);
		}else {
			return '0';
		}	
	}
	

	var objeto 	= 0;	
	var objeto2 = 0;
	var objeto3 = 0;


	var sumaFecha =  function( opts, success, response, registro) {
		
		var mensaje="";
		var dias_minimo = registro.data['DIAS_MINIMO'];  
		var dias_maximo = registro.data['DIAS_MAXIMO']; 		
  	var	txtPlazo; 
		txtPlazo= registro.data['PLAZO_FINAL'];	
			
		var	fechaVto				= Ext.util.Format.date(registro.data['FECHA_VENC_FINAL'],'d/m/Y'); 
		var	fechaEmision		= Ext.util.Format.date(registro.data['FECHA_EMISION'],'d/m/Y');  
		var fechaVenc				= Ext.util.Format.date(registro.data['FECHA_OPERA_FINAL'],'d/m/Y');   
		var	montoInt				= registro.data['MONTO_TASA_INTERES'];   
		var montoCapitalInt	= registro.data['MONTO_TOTAL_CAPITAL']; 		
		var	montoAuxInt			= registro.data['MONTO_AUX_INTERES']; 	
		var	monto						= registro.data['MONTO']; 	
		var	montoCredito		= registro.data['MONTO_CREDITO']; 
		var	montoDocto			= registro.data['MONTO_VALUADO']; 
		var	igPlazoDocto 		= registro.data['PLAZO_DOCTO']; 			 
		var	referenciaTasa	= registro.data['REFERENCIA'];  
		var	monedaLinea			= registro.data['IC_MONEDA_LINEA'];  
		var	icIf						= registro.data['IC_IF'];  		
		var	valorTasa				= registro.data['VALOR_TASA'];  
		var tipoPiso				= registro.data['TIPO_PISO'];  
		var	icTasa					= registro.data['IC_TASA'];   
		var	cgRelMat				= registro.data['CG_REL_MAT'];  
		var	fnPuntos				= registro.data['FN_PUNTOS'];  
		var	valorTasaPuntos	= registro.data['VALOR_TASA_PUNTOS'];  
		var  ic_moneda_docto	= registro.data['IC_MONEDA_DOCTO'];  
		var  ic_tipo_financiamiento	= registro.data['IC_TIPO_FINANCIAMIENTO'];  
		
		var fec_venc_value = fechaVenc;
		var fec_venc_array = fec_venc_value.split("/");
		var TDate = new Date(fec_venc_array[2],fec_venc_array[1]-1,fec_venc_array[0]);
				
		diaMes = TDate.getDate();  		
		if(objeto3==0)
			objeto3 = 1;
		//alert(txtPlazo+'--'+referenciaTasa+'--'+valorTasa+'--'+monedaLinea+'--'+icIf+'--'+icTasa+'--'+cgRelMat+'--'+fnPuntos+'--'+tipoPiso+'--'+montoAuxInt+'--'+montoCredito+'--'+montoInt+'--'+montoCapitalInt+'--'+valorTasaPuntos);
	 if(txtPlazo!="0") {
			if(!obtenTasaXPlazo(opts, success, response, registro)) {
      	if(objeto3==1){
				mensaje = "El documento no puede ser seleccionado con ese plazo porque no se encontro la tasa correspondiente \n ";				
			}			
		}else{
			objeto3=0;
		}		
    }//if(txtPlazo!="") 
		var AddDays = 0;
    if(txtPlazo!=""){
    	if(revisaEntero1(txtPlazo)){      
				AddDays = txtPlazo;
			}else{
       mensaje =   "El valor no es un n�mero v�lido.";
      }
			
		}
			
  if(txtPlazo!="0"){
  
	if(ic_moneda_docto==54 && ic_tipo_financiamiento ==2 )  {
		AddDays = parseInt(AddDays)+1
	}
		diaMes = parseInt(diaMes)+ parseInt(AddDays);
		TDate.setDate(diaMes);
		
		var CurYear = TDate.getFullYear();		
		var CurDay = TDate.getDate();
		var CurMonth = TDate.getMonth()+1;
		if(CurDay<10)
			CurDay = '0'+CurDay;
		if(CurMonth<10)
			CurMonth = '0'+CurMonth;
		TheDate = CurDay +'/'+CurMonth+'/'+CurYear;
		
		//fechaVto = Ext.util.Format.date(TDate,'d/m/Y'); 
		fechaVto = TDate; 
		
		registro.data['FECHA_VENC_FINAL']=fechaVto; 
		registro.commit();
		
		montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(txtPlazo),2);		
		plazo = parseInt(txtPlazo);
		if( ic_tipo_financiamiento ==4 )  {
			dias_maximo = dias_maximo -mtdCalculaDias(fechaEmision,fechaHoy);				
		}
		
		if(objeto==0)
			objeto = 1;
		if(plazo<dias_minimo){
			if(objeto==1){				
				mensaje += "Plazo minimo del financiamiento = "+dias_minimo+" \n";			
			}			
		}else if(plazo>dias_maximo){
			if(objeto==1){
				mensaje += "Plazo maximo del financiamiento = "+dias_maximo+" \n";
			}
		}else{
			objeto=0;
		}
		if(objeto2==0)
			objeto2 = 1;
		var diasInhabiles2 = diasInhabiles;
		var diaMes = CurDay +'/'+CurMonth;
		
		var diasInhabilesXAnio2 =diasInhabilesXAnio; //F09-2015
		var diaMesAnio = CurDay +'/'+CurMonth+'/'+CurYear;
				
		if( (diasInhabiles2.indexOf(diaMes)>0||TDate.getDay()==0||TDate.getDay()==6) || (diasInhabilesXAnio2.indexOf(diaMesAnio)>0||TDate.getDay()==0||TDate.getDay()==6)    ){
		
			if(objeto2==1){
				mensaje +="La Fecha de Vencimiento del Cr�dito es un d�a inh�bil";				
			}
		}else{
			objeto2 = 0;
		}
		
  }
		return mensaje;
	}	
	


	var mtdCalculaDias =  function( sFecha,sFecha2) {
		var iResAno,DiaMes,DiasDif=0,iCont=0,iMes,DiaMesA,iBan=0;
    var DiasPar,Dato,VencDia,VencMes,AltaMes,AltaDia;
    sCadena   = sFecha
    AltaDia   = fnDesentrama(sCadena);
    AltaMes  = fnDesentrama(sCadena);
    AltaAno   = sCadena;
    sCadena     = sFecha2
    VencDia     = fnDesentrama(sCadena);
    VencMes     = fnDesentrama(sCadena);
    VencAno     = sCadena;
    Dato  = AltaMes.substr(0,1);
    if (Dato ==0){
			AltaMes = AltaMes.substr(1,2)
		}
    Dato  = AltaDia.substr(0,1);
    if (Dato ==0){
			AltaDia = AltaDia.substr(1,2)
    }
    Dato  = VencDia.substr(0,1);
    if (Dato ==0){
			VencDia = VencDia.substr(1,2)
    }
    Dato  = VencMes.substr(0,1);
		if (Dato ==0){
			VencMes = VencMes.substr(1,2)
    }
    while (VencAno != AltaAno || VencMes != AltaMes || VencDia != AltaDia){
			iCont++;
      while(AltaMes != VencMes || VencAno != AltaAno){
				iBan=1;
			  iResAno = AltaAno % 4;
			  if (iResAno==0){
					Tmes[2] =29;
			  }else{
					Tmes[2] =28;
			  }
			  if (iCont == 1){
					DiaMesA = Tmes[AltaMes];
			    DiasDif = DiaMesA - AltaDia;
			  } else{
					DiaMesA  = Tmes[AltaMes];
			    DiasDif += parseInt(DiaMesA);
			  }
			  if (AltaMes==12){
					AltaAno++;
			    AltaMes=1;
			  }else{
					AltaMes++;
			  }
			  iCont++;
			} // fin de while
      if (AltaMes == VencMes && VencAno == AltaAno){
				if (iBan==0){
					DiasDif = parseInt(VencDia) - parseInt(AltaDia);
					AltaDia = VencDia;
        } else if (iBan=1){
					DiasDif += parseInt(VencDia);
          AltaDia = VencDia;
        }
      }
		}
		return DiasDif;
	}
	
	//F020-2015
	var validaFechaLineaCredito =  function(opts, success, response, record) {
		var cg_lineaC 				= record.data['CG_LINEA_CREDITO'];
		var fechaVenLinea 				= Ext.util.Format.date(record.data['FECHA_VENC_LINEA'],'d/m/Y');  	
		var fechaVenDocto;
		
		var tipoPago	= record.data['TIPOPAGO']; //1Financiamiento con Interesess 2 Meses sin intereses
		var fechaVenDocto =Ext.util.Format.date(record.data['FECHA_VENC_FINAL'],'d/m/Y'); 
		
			
		var  gridEditable = Ext.getCmp('gridEditable');	
		var store = gridEditable.getStore();		
		var  recordNumber = store.indexOfId( record.id);
		
		//alert(cg_lineaC  +'--'+fechaVenDocto  +'--'+fechaVenLinea  +'recordNumber'+recordNumber);  
		
		if(globalTipoPago==1 && (tipoPago ==1 || tipoPago ==2 )  &&  cg_lineaC=='S'  &&  datecomp(fechaVenDocto,fechaVenLinea)==1 ) {
			
			Ext.Msg.alert('Mensaje','El documento no puede vencer el d�a '+fechaVenDocto+' por que la L�nea de Cr�dito que tiene con el Intermediario vence el d�a '+fechaVenLinea,function(){
			gridEditable.getSelectionModel().deselectRow(recordNumber);
			if(record.data['SELECCION']=='S') {
				record.data['SELECCION']='N';
				record.commit();	 				
			}
				
			});
			return false;	
		}
	}
	
	var obtenTasaXPlazo =  function( opts, success, response, registro) { 
		var i = 0;
		var plazo;
		
		if(registro.data['IC_TIPO_FINANCIAMIENTO'] ==1) {
		 plazo = registro.data['PLAZO_PRONTO_PAGO'];
		}else{
			plazo= registro.data['PLAZO_FINAL'];	
		}				
		var referenciaTasa = registro.data['REFERENCIA'];
		var valorTasa			= registro.data['VALOR_TASA']; 
		var icMoneda			= registro.data['IC_MONEDA_LINEA'];
		var icIf				  = registro.data['IC_IF']; 
		var icTasa				= registro.data['IC_TASA'];  
		var cgRelMat			= registro.data['CG_REL_MAT'];  
		var fnPuntos			= registro.data['FN_PUNTOS']; 
		var tipoPiso			= registro.data['TIPO_PISO'];  
		var montoAuxInt		= registro.data['MONTO_AUX_INTERES'];;
		var montoCredito	= registro.data['MONTO_CREDITO']; 
		var montoInt			= registro.data['MONTO_TASA_INTERES'];  
		var montoCapitalInt = registro.data['MONTO_TOTAL_CAPITAL']; 
		var valorTasaPuntos = registro.data['VALOR_TASA_PUNTOS'];
		var	numTasas = hidNumTasas;		
		var i=0;
		var auxIcIf;
		var auxReferenciaTasa;
		var auxIcTasa;
		var auxValorTasa;
		var auxIcMoneda;
		var auxPlazoDias;
		var iPlazoCredito = parseInt(plazo);
		var iPlazoTasa;
		var iPlazoTasaAux = 9999999999;
		var ok = false;
		
		//alert(numTasas);		
    if(numTasas==1){
			auxIcIf			= eval('Auxiliar.auxIcIf0');   
			auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa0'); 
			auxIcTasa			= eval('Auxiliar.auxIcTasa0'); 
			auxCgRelMat		= eval('Auxiliar.auxCgRelMat0'); 
			auxFnPuntos		= eval('Auxiliar.auxFnPuntos0'); 
			auxValorTasa		= eval('Auxiliar.auxValorTasa0'); 
			auxIcMoneda		= eval('Auxiliar.auxIcMoneda0'); 
			auxPlazoDias		= eval('Auxiliar.auxPlazoDias0'); 
			iPlazoTasa			= parseInt(auxPlazoDias);
			//alert("iteracion:1  plazo credito = "+iPlazoCredito+" Plazo Tasa = "+iPlazoTasa +" plazoTasaAux ="+iPlazoTasaAux+" auxIcIf = "+auxIcIf +" icIf ="+icIf+" icMoneda: "+ icMoneda + " auxIcMoneda = "+auxIcMoneda);
			if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
				
				iPlazoTasaAux			= iPlazoTasa;
				referenciaTasa	= auxReferenciaTasa;		 	
				valorTasa			= auxValorTasa; 
				icTasa			= auxIcTasa;
				cgRelMat			= auxCgRelMat;
				valorTasaPuntos			= auxFnPuntos;
        
         if(csMesesSinInt=='S' &&  registro.data['TIPOPAGO'] ==2  ){
            valorTasaPuntos = 0;
          }
				/*if(cgRelMat=='+')
					valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
				else if(cgRelMat=='-')
					valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
				else if(cgRelMat=='*')
					valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
				else if(cgRelMat=='/')
					valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);
				*/
					ok = true;
				if(tipoPiso=="1"){
					montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360,2)*100)/100;
				}else{
					montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
				}
				montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
				montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);
			}
		}else{
			for(i=0;i<numTasas;i++){
				auxIcIf			= eval('Auxiliar.auxIcIf'+i);   
				auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa'+i); 
				auxIcTasa			= eval('Auxiliar.auxIcTasa'+i); 
				auxCgRelMat		= eval('Auxiliar.auxCgRelMat'+i); 
				auxFnPuntos		= eval('Auxiliar.auxFnPuntos'+i); 
				auxValorTasa		= eval('Auxiliar.auxValorTasa'+i); 
				auxIcMoneda		= eval('Auxiliar.auxIcMoneda'+i); 
				auxPlazoDias		= eval('Auxiliar.auxPlazoDias'+i); 
				iPlazoTasa			= parseInt(auxPlazoDias);
				//alert("iteracion:"+i+" PlazoRevisar = "+auxPlazoDias+" ,  plazo credito = "+iPlazoCredito+" Plazo Tasa = "+iPlazoTasa +" plazoTasaAux ="+iPlazoTasaAux+" auxIcIf = "+auxIcIf +" icIf ="+icIf+" icMoneda: "+ icMoneda + " auxIcMoneda = "+auxIcMoneda);
				if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
					//alert("Si entro en el if "+iPlazoTasa + "iplazotasaaux = "+iPlazoTasaAux+" i:: "+i);
					iPlazoTasaAux 			= iPlazoTasa;
					referenciaTasa	= auxReferenciaTasa;		 	
					valorTasa 		= auxValorTasa; 			
					icTasa			= auxIcTasa;
					cgRelMat			= auxCgRelMat;
					valorTasaPuntos			= auxFnPuntos;
          
          if(csMesesSinInt=='S' &&  registro.data['TIPOPAGO'] ==2 ){
            valorTasaPuntos = 0;
          }
					/*if(cgRelMat=='+')
						valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
					else if(cgRelMat=='-')
						valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
					else if(cgRelMat=='*')
						valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
					else if(cgRelMat=='/')
						valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);
						*/
						ok = true;
					if(tipoPiso=="1"){
						montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360)*100,2)/100;						
					}else{
						montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
					}
					montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
					montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);
				}
			}//for   
		}	
		registro.data['MONTO_TASA_INTERES']=montoInt;
		registro.data['MONTO_TOTAL_CAPITAL']=montoCapitalInt;
    registro.data['VALOR_TASA_PUNTOS']=valorTasaPuntos;
		registro.data['REFERENCIA']=referenciaTasa;		
		//registro.commit();			
			
	return ok;
}

	var sumaTotalesIntereses =  function(opts, success, response, registro) {	
		var totalMN 		= 0.00;
		var totalUSD		= 0.00;
		var totalCapitalMN 	= 0.00;
		var totalCapitalUSD = 0.00;	
		var montoTasaInt;
		var montoCapitalInt;
		var icMoneda;
		var monedaLinea; 
		var monto_tasa_int = registro.data['MONTO_TASA_INTERES']; 
		var monto_capital_int =registro.data['MONTO_TOTAL_CAPITAL'];
	
		montoTasaInt	= parseFloat(monto_tasa_int);
		montoCapitalInt	= parseFloat(monto_capital_int);
		icMoneda 	= registro.data['IC_MONEDA_DOCTO'];
		monedaLinea	= registro.data['IC_MONEDA_LINEA']; 
		
		if(monedaLinea=="1"){
			totalMN += montoTasaInt;
			totalCapitalMN	+= montoCapitalInt;
		}else if(icMoneda=="54"&&monedaLinea=="54"){
			totalUSD += montoTasaInt;
			totalCapitalUSD	+= montoCapitalInt;
		}				
		totalInteresMN = roundOff(totalMN,2); //estos valores son los  que se muestran al seleccionar los registros 
		totalInteresUSD = roundOff(totalUSD,2);
		totalCapitalInteresMN = roundOff(totalCapitalMN,2);
		totalCapitalInteresUSD = roundOff(totalCapitalUSD,2);	
				
		//alert("totalInteresMN--"+totalInteresMN +"--totalCapitalInteresMN ---"+totalCapitalInteresMN);
		//alert("totalInteresUSD--"+totalInteresUSD +"--totalCapitalInteresUSD ---"+totalCapitalInteresUSD);
	
	}

	var seleccionaDocto =  function(opts, success, response, record) {
	
		if( record.data['TIPOPAGO']==1 )  {
		
			var txtPlazo =record.data['PLAZO_FINAL'];
			var montoInt = record.data['MONTO_TASA_INTERES']; 
			var icTasa = record.data['IC_TASA'];	
			var plazo_unico=  record.data['PLAZO_UNICO'];	
		
		
			//** Inicio  Fodea 025-2014  
			if(record.data['DIAS_OPERACION']!=0) {
				
				var	fechaDiasOperacion  = Ext.util.Format.date(record.data['FECHA_DIAS_OPERACION'],'d/m/Y');
				var FecdO = fechaDiasOperacion.split("/");
				fecDiasOpe = new Date(Number(FecdO[2]),Number(FecdO[1])-1,Number(FecdO[0]));
				
				var	fechaVenciIni = Ext.util.Format.date(record.data['FECHA_VENCIMIENTO'],'d/m/Y');
				var FecVecF = fechaVenciIni.split("/");
				fecVenF = new Date(Number(FecVecF[2]),Number(FecVecF[1])-1,Number(FecVecF[0]));
					
				if(fecDiasOpe.getTime() < fecVenF.getTime()){					
					Ext.MessageBox.alert("Mensaje", " La fecha m�nima para seleccionar este documento es " +fechaDiasOperacion+ ". <br> Favor de verificar.");
					return false;
				}
			}
			//** Fin  Fodea 025-2014 
			//if(txtPlazo=="0"){
			if(txtPlazo=="0" && globalTipoPago==1){
				Ext.MessageBox.alert("Mensaje","Debe capturar los plazos del credito en los documentos a aceptar");
				return false;
			}else {
				return true; 
			}
			
			plazoIF = parseInt(plazo_unico); 
			PlazoPyme = parseInt(txtPlazo);  
				
			if (PlazoPyme > plazoIF){
			 Ext.MessageBox.alert("Mensaje","El plazo capturado "+PlazoPyme+ " no puede ser mayor a "+plazoIF);
				return false ;
			}else {
				return true; 
			}		
		}
		
		//if(validaciones(selectModel, rowIndex, keepExisting, record) ) {
		if(validaciones(opts, success, response, registro) ) {
			return true;
		}else {
			return false;
		}
					
	}	
	

	var validaciones =  function(opts, success, response, record) {	
		gridEditable = Ext.getCmp('gridEditable');			
	
			var montoInt = inicializaTasasXPlazo( '', '', record,'MontodeIntereses');
			record.data['MONTO_TASA_INTERES']=montoInt;			
									
			var montoCap = inicializaTasasXPlazo( '', '', record,'MontoCapitalInteres');
			record.data['MONTO_TOTAL_CAPITAL']=montoCap;								
				
			var varTasa = inicializaTasasXPlazo( '', '', record,'ValorTasaInteres');
			record.data['VALOR_TASA_PUNTOS']=varTasa;				
		
			var referencia = inicializaTasasXPlazo( '', '', record,'ReferenciaTasa');		
			record.data['REFERENCIA']=referencia;	
			
			var montoAuxi = inicializaTasasXPlazo( '', '', record,'montoAuxi');
			record.data['MONTO_AUX_INTERES']=montoAuxi;	
								
			var tasa =  inicializaTasasXPlazo( '', '', record,'ic_tasa');
			record.data['IC_TASA']=tasa;	
			
			var relMat =  inicializaTasasXPlazo( '', '', record,'cg_rel_mat');
			record.data['CG_REL_MAT']=relMat;				
		
			var fnPuntos =  inicializaTasasXPlazo( '', '', record,'fnPuntos');
			record.data['FN_PUNTOS']=fnPuntos;		
					
			record.commit();				
		
	}
	
	//********************************
	var  procesarSuccessFailurePreGuardadoTrans =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			//ventanaPagoTC.setSize(500,500);
			//iframeExt.setSize(480,400);
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPDFAcuse =  function(opts, success, response) {
		var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
		btnGenerarPDFAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			btnBajarPDFAcuse.show();
			btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPDFAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//para el boton de CSV de acuse 
	var procesarSuccessFailureGenerarCSVAcuse =  function(opts, success, response) {
		var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
		btnGenerarCSVAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSVAcuse = Ext.getCmp('btnBajarCSVAcuse');
			btnBajarCSVAcuse.show();
			btnBajarCSVAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSVAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarCSVAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureTransRechazada =  function(opts, success, response) {	
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			
			Ext.Msg.alert('Aviso', '<p align="center">Transacci�n Rechazada<br>'+ResponseCode+' - '+ResponseMessage+'</p>',function(){
				ventanaPagoTC.hide();
				window.scrollTo(0, 0);
			});
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// muestra la pantalla de acuse 
	var  procesarSuccessFailureAcuse =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			var acuse;	
			if (jsondeAcuse != null){
				acuse= jsondeAcuse.acuse;									
			}
		
			var Comfirmacionclave = Ext.getCmp('Comfirmacionclave');
				if (Comfirmacionclave) {		
					Comfirmacionclave.destroy();		
				}									

			var acuseCifras;
      
      if(globalTipoPago==2){
        acuseCifras = [
					['N�mero de Acuse', jsondeAcuse.acuse],
					['Fecha de Carga', jsondeAcuse.fecCarga],
					['Hora de Carga', jsondeAcuse.horaCarga],
					['Usuario de Captura', jsondeAcuse.captUser],
          ['N�mero de Orden', OrderId],
          ['N�mero de Documentos', totalDoctosAcuse+" "],
          ['Total', montoTotalAcuse+" "]
        ]
      }else{
        acuseCifras = [
					['N�mero de Acuse', jsondeAcuse.acuse],
					['Fecha de Carga', jsondeAcuse.fecCarga],
					['Hora de Carga', jsondeAcuse.horaCarga],
					['Usuario de Captura', jsondeAcuse.captUser]
        ]
      }
				
				storeCifrasData.loadData(acuseCifras);
			ctexto6.hide();
			var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
			var btnBajarCSVAcuse = Ext.getCmp('btnBajarCSVAcuse');
      if(acuse!=''){				
				btnGenerarPDFAcuse.enable();
				btnBajarPDFAcuse.hide();
				btnGenerarCSVAcuse.enable();
				btnBajarCSVAcuse.hide();
			}else{
				btnGenerarPDFAcuse.disable();
				btnBajarPDFAcuse.hide();
				btnGenerarCSVAcuse.disable();
				btnBajarCSVAcuse.hide();
			}	
			
			//para el boton de PDF de Acuse 
			var btnGenerarAc = Ext.getCmp('btnGenerarPDFAcuse');
			btnGenerarAc.setHandler(function(boton, evento) {
				//boton.disable();
				//boton.setIconClass('loading-indicator');								
				Ext.Ajax.request({
					url: '24forma2bExtPDFCSV.jsp',
					params: Ext.apply(fp.getForm().getValues(),{														
						acuse: acuse,
						modificado:modificado,
						ic_documento:ic_documento,
						ic_moneda_docto:ic_moneda_docto,
						ic_moneda_linea:ic_moneda_linea,
						monto:monto,
						monto_valuado:monto_valuado,
						monto_credito:monto_credito,
						monto_tasa_int:monto_tasa_int,
						monto_descuento:monto_descuento,
						tipo_cambio:tipo_cambio,
						plazo:plazo,
						fecha_vto:fecha_vto,
						referencia_tasa:referencia_tasa,
						valor_tasa_puntos:valor_tasa_puntos,
						ic_tasa:ic_tasa,
						cg_rel_mat:cg_rel_mat,
						fn_puntos:fn_puntos	,						
						tipoArchivo:'PDF',
						csTipoPago: globalTipoPago,
            OrderId: OrderId,
            totalDoctosAcuse: totalDoctosAcuse,
            montoTotalAcuse: montoTotalAcuse,
				tipoPago:tipoPago,
				meseSinIntereses:meseSinIntereses
					}),
					callback: procesarSuccessFailureGenerarPDFAcuse
				});
			});
			
			//para el boton de CSV de Acuse 
			var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
			btnGenerarCSVAcuse.setHandler(function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');								
					Ext.Ajax.request({
						url: '24forma2bExtPDFCSV.jsp',
						params: Ext.apply(fp.getForm().getValues(),{														
							acuse: acuse,
							tipoArchivo:'CSV',
							modificado:modificado,
							ic_documento:ic_documento,
							ic_moneda_docto:ic_moneda_docto,
							ic_moneda_linea:ic_moneda_linea,
							monto:monto,
							monto_valuado:monto_valuado,
							monto_credito:monto_credito,
							monto_tasa_int:monto_tasa_int,
							monto_descuento:monto_descuento,
							tipo_cambio:tipo_cambio,
							plazo:plazo,
							fecha_vto:fecha_vto,
							referencia_tasa:referencia_tasa,
							valor_tasa_puntos:valor_tasa_puntos,
							ic_tasa:ic_tasa,
							cg_rel_mat:cg_rel_mat,
							fn_puntos:fn_puntos,
							csTipoPago: globalTipoPago,
						  OrderId: OrderId,
						  totalDoctosAcuse: totalDoctosAcuse,
						  montoTotalAcuse: montoTotalAcuse,
						  tipoPago:tipoPago,
						 meseSinIntereses:meseSinIntereses
				
						}),
						callback: procesarSuccessFailureGenerarCSVAcuse
					});
				});
				
				window.scrollTo(0, 0);
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//confirmacion de clave de cesion
	function confirmacionClavesCesion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var bConfirma;
			jsondeConfirma = Ext.util.JSON.decode(response.responseText);
				if (jsondeConfirma != null){
					bConfirma= jsondeConfirma.bConfirma;									
				}		
			if(bConfirma ==false) {
				Ext.MessageBox.alert('Mensaje','La confirmaci�n es incorrecta, intente de nuevo');
				
			} else  if(bConfirma ==true) {		
				
				if(globalTipoPago==1){
					gridPreAcuse.hide();
					gridEditable.hide();
					fp.hide();			
					gridMonitor.hide();
					gridMontos.show();
					gridAcuse.show();
					gridCifrasControl.show();
					gridTotales.hide();
					ctexto1.hide();
					ctexto2.hide();
					ctexto4.hide();
					ctexto3.hide();	
					ctexto5.hide();
					ctexto6.show();
					Ext.Ajax.request({
						url : '24forma02.data.jsp',
						params : {
							informacion: 'GeneraAcuse',					
							modificado:modificado,
							ic_documento:ic_documento,
							ic_moneda_docto:ic_moneda_docto,
							ic_moneda_linea:ic_moneda_linea,
							monto:monto,
							monto_valuado:monto_valuado,
							monto_credito:monto_credito,
							monto_tasa_int:monto_tasa_int,
							monto_descuento:monto_descuento,
							tipo_cambio:tipo_cambio,
							plazo:plazo,
							fecha_vto:fecha_vto,
							referencia_tasa:referencia_tasa,
							valor_tasa_puntos:valor_tasa_puntos,
							ic_tasa:ic_tasa,
							cg_rel_mat:cg_rel_mat,
							fn_puntos:fn_puntos,
							ic_linea_credito:ic_linea_credito,
							ic_bins: ic_bins,
							cmbTipoPago: globalTipoPago
						},
						callback: procesarSuccessFailureAcuse
					});
				}else if(globalTipoPago==2){
					var montoTotTC = 0.0;
					var numDocumento = '';
					var totalReg = 0;
					consultaPreAcuseData.each(function(record){
						montoTotTC += Number(record.data['MONTO_CREDITO']);
						numDocumento += record.data['NO_DOCUMENTO']+', ';
						totalReg++;
					});
					var Comfirmacionclave = Ext.getCmp('Comfirmacionclave');
					if (Comfirmacionclave) {		
						Comfirmacionclave.destroy();		
					}	
					ventanaPagoTC.setVisible(true);
					//ventanaPagoTC.setSize(500,240);
					//iframeExt.setSize(480,230);
					iframeExt.setUrl('/nafin/00utils/pagoEnLinea.jsp?montoTotTC='+montoTotTC+'&totalReg='+totalReg+'&strNombreUsuario='+strNombreUsuario+'&StoreID='+vgblAfilComercio);
				}
			}//bConfirma
		}//if
	}
	
	
	//para el boton de PDF de presAcuse 
	var procesarSuccessFailureGenerarPDFPreA =  function(opts, success, response) {
		var btnGenerarPDFA = Ext.getCmp('btnGenerarPDFPreAcuse');
		btnGenerarPDFA.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDFA.disable();
			var btnBajarPDFA = Ext.getCmp('btnBajarPDFPreAcuse');
			btnBajarPDFA.show();
			btnBajarPDFA.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDFA.focus();
			btnBajarPDFA.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFA.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	/******procesarConfirmar****************/
	var procesarConfirmar = function() {
		var  gridEditable = Ext.getCmp('gridEditable');
		var store = gridEditable.getStore();	
		cancelar(); //limpia las variables 
		var registrosPreAcu = []; //para agregar los registros al Preacuse  y acuse 		
		//se recorre grid principal para detectar los documentos seleccionados			
		store.each(function(record) {
			//var numRegistro = store.indexOf(record);			
			if(record.data['SELECCION']=='S'){					
					//aqui van los parametros a recibir 
				modificado.push(record.data['NO_DOCTO_FINAL']);
				ic_documento.push(record.data['NO_DOCTO_FINAL']);
				ic_moneda_docto.push(record.data['IC_MONEDA_DOCTO']);
				ic_moneda_linea.push(record.data['IC_MONEDA_LINEA']);
				monto.push(record.data['MONTO']);
				monto_valuado.push(record.data['MONTO_VALUADO']);
				monto_credito.push(record.data['MONTO_CREDITO']);
				monto_tasa_int.push(record.data['MONTO_TASA_INTERES']);
				monto_descuento.push(record.data['MONTO_DESCUENTO']);
				tipo_cambio.push(record.data['TIPO_CAMBIO']);
				plazo.push(record.data['PLAZO_FINAL']);							
				fecha_vto.push( Ext.util.Format.date(record.data['FECHA_VENC_FINAL'],'d/m/Y') ); 	 				
				referencia_tasa.push(record.data['REFERENCIA']);
				valor_tasa_puntos.push(record.data['VALOR_TASA_PUNTOS']);
				ic_tasa.push(record.data['IC_TASA']);
				cg_rel_mat.push(record.data['CG_REL_MAT']);
				fn_puntos.push(record.data['FN_PUNTOS']);
				ic_linea_credito.push(record.data['IC_LINEA_CREDITO']);
				ic_bins.push(record.data['CLAVE_BIN']);
				tipoPago.push(record.data['TIPOPAGO']); 
				meseSinIntereses.push(record.data['MESES_INTERESES']); 
				registrosPreAcu.push(record);	
			}						
		});
				 
		//se le carga informacion al grid de preacuse y al grid de totales
		consultaPreAcuseData.add(registrosPreAcu);
		consultaAcuseData.add(registrosPreAcu);
		
		if(modificado =='') {
			Ext.MessageBox.alert('Mensaje','Debe seleccionar por lo menos un documento para continuar');
			return false;	
		}			
		
		gridPreAcuse.show();
		gridEditable.hide();
		fp.hide();		
		gridMontos.show();
		gridMonitor.hide();
		gridAcuse.hide();
		gridCifrasControl.hide();
		gridTotales.hide();
		ctexto1.hide();
		ctexto2.hide();
		ctexto4.hide();
		ctexto3.hide();	
		ctexto5.hide();
		ctexto6.show();
		//archivo PDF de Preacuse 
		var btnGenerar = Ext.getCmp('btnGenerarPDFPreAcuse');
			btnGenerar.setHandler(
				function(boton, evento) {
					//boton.disable();
					//boton.setIconClass('loading-indicator'); 								
					Ext.Ajax.request({
						url: '24forma2aExtPDF.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF',
							csTipoPago:globalTipoPago,
							modificado:modificado,
							ic_documento:ic_documento,
							ic_moneda_docto:ic_moneda_docto,
							ic_moneda_linea:ic_moneda_linea,
							monto:monto,
							monto_valuado:monto_valuado,
							monto_credito:monto_credito,
							monto_tasa_int:monto_tasa_int,
							monto_descuento:monto_descuento,
							tipo_cambio:tipo_cambio,
							plazo:plazo,
							fecha_vto:fecha_vto,
							referencia_tasa:referencia_tasa,
							valor_tasa_puntos:valor_tasa_puntos,
							ic_tasa:ic_tasa,
							cg_rel_mat:cg_rel_mat,
							fn_puntos:fn_puntos,
							tipoPago:tipoPago,
							meseSinIntereses:meseSinIntereses
						}),
						callback: procesarSuccessFailureGenerarPDFPreA
					});
				}
			);
			
			
		//para el Preacuse 	
		var btnConfirmarP = Ext.getCmp('btnConfirmarP');
			
		if(globalTipoPago==2){
			btnConfirmarP.setText('Realizar Pago');
			
			/*var montoToTalCred = 0.0;
			for(var i=0; i<monto_credito.length; i++){
				montoToTalCred += Number(monto_credito[i]);
			}
			alert(montoToTalCred);
			*/
			
			var gridColModPreacuse = gridPreAcuse.getColumnModel();
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('TIPO_CONV'),true);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('TIPO_CAMBIO'),true);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('MONTO_VALUADO'),true);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('REFERENCIA'),true);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('VALOR_TASA_PUNTOS'),true);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('MONTO_TASA_INTERES'),true);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('MONTO_TOTAL_CAPITAL'),true);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('DESC_BIN'),false);
			
			
			var gridColModMontos = gridMontos.getColumnModel();
			gridColModMontos.setHidden(gridColModMontos.findColumnIndex('totalInteresMN'),true);
			gridColModMontos.setHidden(gridColModMontos.findColumnIndex('totalInteresUSD'),true);
			gridColModMontos.setHidden(gridColModMontos.findColumnIndex('totalInteresConv'),true);
			
			
			/*btnConfirmarP.setHandler(function(boton, evento) {
				ventanaPagoTC.setVisible(true);
				iframeExt.setUrl('/nafin/pruebaPagoEnLinea.jsp');
			});*/
		}else{
			var gridColModPreacuse = gridPreAcuse.getColumnModel();
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('TIPO_CONV'),false);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('TIPO_CAMBIO'),false);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('MONTO_VALUADO'),false);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('REFERENCIA'),false);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('VALOR_TASA_PUNTOS'),false);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('MONTO_TASA_INTERES'),false);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('MONTO_TOTAL_CAPITAL'),false);
			gridColModPreacuse.setHidden(gridColModPreacuse.findColumnIndex('DESC_BIN'),true);
			
			
			var gridColModMontos = gridMontos.getColumnModel();
			gridColModMontos.setHidden(gridColModMontos.findColumnIndex('totalInteresMN'),false);
			gridColModMontos.setHidden(gridColModMontos.findColumnIndex('totalInteresUSD'),false);
			gridColModMontos.setHidden(gridColModMontos.findColumnIndex('totalInteresConv'),false);
		}
		
		btnConfirmarP.setHandler(function(boton, evento) {	
			var  fLogCesion = new  NE.cesion.FormLoginCesion();
			fLogCesion.setHandlerBtnAceptar(function(){
				Ext.Ajax.request({
					url: '24forma02.data.jsp',
					params: Ext.apply(fLogCesion.getForm().getValues(),{
						informacion: 'ConfirmacionClaves'
					}),
					callback: confirmacionClavesCesion
				});					
				
			});
				
			//se crea ventana de Confirmacion de Clave 
			var	ventanaConfir =  Ext.getCmp('Comfirmacionclave');	
			if (ventanaConfir) {
				ventanaConfir.show();
			} else {							
				new Ext.Window({
					layout: 'fit',
					width: 400,
					height: 200,			
					id: 'Comfirmacionclave',
					modal:true,
					closeAction: 'hide',
					items: [					
						fLogCesion
					],
					title: ''				
				}).show();
			}
		});
		window.scrollTo(0, 0);
	}
	
	
	var procesarConsultaCmbLineas = function(store, arrRegistros, opts) 	{
		if (arrRegistros != null) {
			
			if(store.getTotalCount() > 0) {
				//el.unmask();				
			} else {					
				Ext.Msg.alert("Aviso","No cuenta con la parametrizaci�n de L�nea de Cr�dito favor de verificarlo");
			}			
		}
	}
	
	var procesarConsultaCmbBins = function(store, arrRegistros, opts) 	{
		if (arrRegistros != null) {
			
			if(store.getTotalCount() > 0) {
				
			} else {					
				Ext.Msg.alert("Aviso","No cuenta con la parametrizaci�n de Tarjeta de Cr�dito favor de verificarlo");
			}			
		}
	}
	
	function procesaValoresIniciales(opts, success, response) {			
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);				
			if (jsonValoresIniciales != null){
				Auxiliar = jsonValoresIniciales.Auxiliar;										
				hidNumTasas = jsonValoresIniciales.hidNumTasas;	
				hidNumTasasMeses = jsonValoresIniciales.hidNumTasasMeses;				
				tasaMeses = jsonValoresIniciales.tasaMeses;	
				  
					
					
				if (jsonValoresIniciales.bloqueoPymeEpo == 'B') {
					Ext.getCmp('ctexto7').show();
					Ext.getCmp('forma').hide();					
				}else  {
					Ext.getCmp('ctexto7').hide();
					Ext.getCmp('forma').show();	
				}
        csMesesSinInt = jsonValoresIniciales.csMesesSinInt;
        //alert(csMesesSinInt);
        vgblAfilComercio = jsonValoresIniciales.sAfilComercio;
        var tipoPago = (jsonValoresIniciales.csTipoPago).trim();
        if(tipoPago=='H'){
          var dataTipoPago = [[1, 'Linea de Cr�dito'],[2, 'Tarjeta de Cr�dito']];
          storeTipoPagoData.loadData(dataTipoPago);
        }else if(tipoPago=='LC'){
          var dataTipoPago = [[1, 'Linea de Cr�dito']];
          storeTipoPagoData.loadData(dataTipoPago);
        }else if(tipoPago=='TC'){
          var dataTipoPago = [[2, 'Tarjeta de Cr�dito']];
          storeTipoPagoData.loadData(dataTipoPago);
        }
			}				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
//------------------------------------------------------------------------------
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
	var catalogoModalidadPlazoData = new Ext.data.JsonStore({
		id: 'catalogoModalidadDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02.data.jsp',
		baseParams: {
			informacion: 'CatalogoModalidadPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});

	var catalogoLineas = new Ext.data.JsonStore({
		id: 'catalogoLineasDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma02.data.jsp',
		baseParams: {
			informacion: 'CatalogoLineaTodos'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaCmbLineas,			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoTC = new Ext.data.JsonStore({
		id: 'catalogoTCDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg', 'claveIf'],
		url : '24forma02.data.jsp',
		baseParams: {
			informacion: 'CatalogoIFBins'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaCmbBins,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		var no_epo = jsonData.ic_epo;
		Noelementos = jsonData.ElEMENTOS; 
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridEditable.isVisible()) {
				gridEditable.show();
			}	
			var btnCancelar = Ext.getCmp('btnCancelar');
			var btnConfirmar = Ext.getCmp('btnConfirmar');							
		
			//if(no_epo == 26) {
			//ctexto3.show();
			//}else{
				ctexto4.show();
			//}
			ctexto2.show();
			ctexto5.show();
			
			var cmbTP = Ext.getCmp('cmbTipoPago1');
			
			var el = gridEditable.getGridEl();	
			if(store.getTotalCount() > 0) {
				el.unmask();
				//var cmbTP = Ext.getCmp('cmbTipoPago1');
				ctexto1.show();
				btnCancelar.enable();							
				btnConfirmar.enable();	
				gridTotales.show();		
				
				var gridColumnMod = gridEditable.getColumnModel();
				var gridColumnModTot = gridTotales.getColumnModel();
				
				if(cmbTP.getValue()=='1'){
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('TIPO_CONV'),false);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('TIPO_CAMBIO'),false);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('MONTO_VALUADO'),false);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('REFERENCIA'),false);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('VALOR_TASA_PUNTOS'),false);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('MONTO_TASA_INTERES'),false);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('MONTO_TOTAL_CAPITAL'),false);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('DESC_BIN'),true);
					
					gridColumnModTot.setHidden(gridColumnModTot.findColumnIndex('TOTAL_MONTO_VALUADO'),false);
					gridColumnModTot.setHidden(gridColumnModTot.findColumnIndex('MONTOINTERES'),false);
					gridColumnModTot.setHidden(gridColumnModTot.findColumnIndex('MONTOCAPITALINTERES'),false);
					

				}else if(cmbTP.getValue()=='2'){
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('TIPO_CONV'),true);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('TIPO_CAMBIO'),true);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('MONTO_VALUADO'),true);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('REFERENCIA'),true);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('VALOR_TASA_PUNTOS'),true);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('MONTO_TASA_INTERES'),true);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('MONTO_TOTAL_CAPITAL'),true);
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('DESC_BIN'),false);
					
					gridColumnModTot.setHidden(gridColumnModTot.findColumnIndex('TOTAL_MONTO_VALUADO'),true);
					gridColumnModTot.setHidden(gridColumnModTot.findColumnIndex('MONTOINTERES'),true);
					gridColumnModTot.setHidden(gridColumnModTot.findColumnIndex('MONTOCAPITALINTERES'),true);
				}
				
				if(cmbTP.getValue()==2){
					ctexto1.hide();
					ctexto2.hide();
				}
			} else {					
				el.mask('No se encontr� ning�n registro', 'x-mask');
				btnCancelar.disable();							
				btnConfirmar.disable();	
				gridTotales.hide();
			}
		}
	}
	
	var procesarConsultaMonitorData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		var ic_epo = jsonData.ic_epo;
		hidNumLineas = jsonData.hidNumLineas;
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridMonitor.isVisible()) {
				gridMonitor.show();
			}	
		
			/*if(ic_epo == 26) {
				ctexto3.show();
			}else{*/
				ctexto4.show();
			//}
			ctexto1.show();			
			ctexto5.show();
			var el = gridMonitor.getGridEl();	
			if(store.getTotalCount() > 0) {
				el.unmask();				
			} else {					
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}			
		}
	}
		
		
	var consultaMonitorData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma02.data.jsp',
		baseParams: {
			informacion: 'ConsultaMonitor'
		},
		fields: [
			{name: 'LINEA_CREDITO_IF'},
			{name: 'IC_LINEA',type: 'float'},
			{name: 'MONEDA_LINEA',type: 'float'},				
			{name: 'SALDO_INICIAL' ,type: 'float'},
			{name: 'MONTOSELECCIONADO' ,type: 'float'},
			{name: 'NUMDOCTOS' ,type: 'float'},
			{name: 'SALDODISPONIBLE',type: 'float'},
			{name: 'HIDSALDODISPONIBLE',type: 'float'},
			{name: 'SELECCIONADO'}				
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaMonitorData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaMonitorData(null, null, null);						
				}
			}
		}
	});
		
	var gridMonitor = new Ext.grid.GridPanel({
		store: consultaMonitorData,
		hidden: true,
		margins: '20 0 0 0',		
		title: 'Monitor de l�neas de cr�dito',		
		columns: [
		{
				header: 'SELECCIONADO',
				tooltip: 'SELECCIONADO',
				dataIndex: 'SELECCIONADO',
				sortable: true,
				resizable: true	,
				width: 30,
				hidden: true,
				align: 'left'
			},			
			{
				header: 'L�nea de Cr�dito IF',
				tooltip: 'L�nea de Cr�dito IF',
				dataIndex: 'LINEA_CREDITO_IF',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Saldo inicial',
				tooltip: 'Saldo inicial',
				dataIndex: 'SALDO_INICIAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto seleccionado',
				tooltip: 'Monto seleccionado',
				dataIndex: 'MONTOSELECCIONADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Num. doctos. seleccionados',
				tooltip: 'Num. doctos. seleccionados',
				dataIndex: 'NUMDOCTOS',
				sortable: true,
				resizable: true	,
				width: 150,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Saldo disponible',
				tooltip: 'Saldo disponible',
				dataIndex: 'SALDODISPONIBLE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],			
		displayInfo: true,
		emptyMsg: "No hay registros.",
		loadMask: true,
		margins: '20 0 0 0',
		stripeRows: true,
		height: 150,
		width: 943,
		frame: false
	});
	

	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
 //Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
	id: 'gridCifrasControl',
	store: storeCifrasData,
	margins: '20 0 0 0',
	hideHeaders : true,
	hidden: true,
	align: 'center',
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 150,
			sortable : true
		},
		{
			header : 'Informacion',			
			dataIndex : 'informacion',
			width : 350,
			sortable : true,
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 500,
	style: 'margin:0 auto;',
	autoHeight : true,
	title: 'Cifras de Control',
	frame: true
	});
	
	
	var totalesData = new Ext.data.JsonStore({			
		root : 'registrosT',
		url : '24forma02.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_epo: Ext.getCmp("ic_epo"),
			ic_moneda: Ext.getCmp("ic_moneda"),
			mto_descuento: Ext.getCmp("mto_descuento"),
			cc_acuse: Ext.getCmp("cc_acuse"),
			fn_monto_de: Ext.getCmp("fn_monto_de"),
			fn_monto_a: Ext.getCmp("fn_monto_a"),
			df_fecha_emision_de: Ext.getCmp("df_fecha_emision_de"),
			df_fecha_emision_a: Ext.getCmp("df_fecha_emision_a"),
			ig_numero_docto: Ext.getCmp("ig_numero_docto"),
			df_fecha_venc_de: Ext.getCmp("df_fecha_venc_de"),
			df_fecha_venc_a: Ext.getCmp("df_fecha_venc_a"),
			fecha_publicacion_de: Ext.getCmp("fecha_publicacion_de"),
			fecha_publicacion_a: Ext.getCmp("fecha_publicacion_a"),
			doctos_cambio: Ext.getCmp("doctos_cambio"),
			lineaTodos: Ext.getCmp("lineaTodos")
		},								
		fields: [			
			{name: 'MONEDA' , mapping: 'MONEDA'},
			{name: 'ICMONEDA' , mapping: 'ICMONEDA'},
			{name: 'TOTAL', mapping: 'TOTAL'},
			{name: 'MONTO', mapping: 'MONTO' },
			{name: 'TOTAL_MONTO_DESC',  mapping: 'TOTAL_MONTO_DESC'},
			{name: 'TOTAL_MONTO_VALUADO',  mapping: 'TOTAL_MONTO_VALUADO'},	
			{name: 'TOTAL_CREDITOS',  mapping: 'TOTAL_CREDITOS'},	
			{name: 'TOTAL_MONTO_CREDITOS',  mapping: 'TOTAL_MONTO_CREDITOS'},				
			{name: 'MONTOINTERES',  mapping: 'MONTOINTERES'},			
			{name: 'MONTOCAPITALINTERES',  mapping: 'MONTOCAPITALINTERES'}
			
			
			
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});	
	
	//esto esta en duda como manejarlo
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Datos Documento Inicial', colspan: 5, align: 'center'},
					{header: 'Datos Documento Final', colspan: 4, align: 'center'}					
				]
			]
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		id: 'gridTotales',
		store: totalesData,
		margins: '20 0 0 0',
		align: 'center',
		title: 'Totales',
		plugins: grupos,	
		hidden: true,
		columns: [	
		{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'			
		},	
		{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTAL',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'MONTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto total de descuento.',
				dataIndex: 'TOTAL_MONTO_DESC',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			}	,
			{
				header: 'Monto total valuado en pesos',
				dataIndex: 'TOTAL_MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
		//Finales
			{							
				header : 'Num. total de doctos.',
				dataIndex : 'TOTAL_CREDITOS',
				width : 150,
				align: 'center'
			},
			{							
				header : 'Monto total de doctos.',			
				dataIndex : 'TOTAL_MONTO_CREDITOS',
				width : 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			{							
				header : 'Monto de Intereses',			
				dataIndex : 'MONTOINTERES',
				width : 150,
				align: 'right',				
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto Total de Capital e Inter�s',				
				dataIndex : 'MONTOCAPITALINTERES',
				width : 150,
				align: 'right',				
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
			],
			height: 150,	
			width : 943,
			frame: false
		});	
		
	//para mostrar totales  en el PreAcuse y acuse 
	var storeMontosData = new Ext.data.ArrayStore({
		fields: [
			{name: 'totalDoctosMN', type: 'float'},
			{name: 'totalMontoMN', type: 'float'},
			{name: 'totalMontoSIntMN', type: 'float'},
			{name: 'totalInteresMN', type: 'float'},	
					
			{name: 'totalDoctosUSD', type: 'float'},			
			{name: 'totalMontoUSD', type: 'float'},
			{name: 'totalMontoSInUSD', type: 'float'},
			{name: 'totalInteresUSD', type: 'float'},
			
			{name: 'totalDoctosConv', type: 'float'},
			{name: 'totalMontosConv', type: 'float'},
			{name: 'totalMontoSinConv', type: 'float'},
			{name: 'totalInteresConv', type: 'float'}
		  ]
	 });
	 	 
	 //esto esta en duda como manejarlo
	var gruposMontos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Moneda nacional', colspan: 4, align: 'center'},
				{header: 'D�lares Americanos', colspan: 4, align: 'center'}	,				
				{header: 'Doctos. en DLL financiados en M.N.', colspan: 4, align: 'center'}					
			]
		]
	});
	
//Grid del Acuse 
	var gridMontos = new Ext.grid.EditorGridPanel({	
		id: 'gridMontos',		
		store: storeMontosData,
		plugins: gruposMontos,
		hidden: true,
		columns: [		
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosMN',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos.',
				tooltip: 'Monto total de doctos.',
				dataIndex : 'totalMontoMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{							
				header : 'Monto del Financiamiento',
				tooltip: 'Monto del Financiamiento',
				dataIndex : 'totalMontoSIntMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{							
				header : 'Monto intereses',
				tooltip: 'Monto intereses',
				dataIndex : 'totalInteresMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},								
			//USD		
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosUSD',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos.',
				tooltip: 'Monto total de doctos.',
				dataIndex : 'totalMontoUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto del Financiamiento',
				tooltip: 'Monto del Financiamiento',
				dataIndex : 'totalMontoSInUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto intereses finales',
				tooltip: 'Monto intereses finales',
				dataIndex : 'totalInteresUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
		//Doctos. en DLL financiados en M.N.
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosConv',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos.',
				tooltip: 'Monto total de doctos.',
				dataIndex : 'totalMontosConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto del Financiamiento',
				tooltip: 'Monto del Financiamiento',
				dataIndex : 'totalMontoSinConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto intereses',
				tooltip: 'Monto intereses',
				dataIndex : 'totalInteresConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 150,
		width: 943,		
		frame: true
	
	});
	
	
	var consultaAcuseData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'NO_DOCUMENTO'},
			{name: 'NO_ACUSE'},				
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_VENCIMIENTO',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_PUBLICACION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'PLAZO_DOCTO',type: 'float' },
			{name: 'MONEDA' },
			{name: 'IC_MONEDA_DOCTO' },
			{name: 'IC_MONEDA_LINEA' },
			{name: 'IC_IF' },
			{name: 'TIPO_PISO' },				
			{name: 'MONTO' ,type: 'float' },
			{name: 'PLAZO_DESCUENTO' ,type: 'float'},
			{name: 'PORCENTAJE_DESCUENTO' },
			{name: 'MONTO_DESCUENTO' ,type: 'float'},
			{name: 'TIPO_CONV' },
			{name: 'TIPO_CAMBIO' },
			{name: 'MONTO_VALUADO' ,type: 'float'},
			{name: 'MODALIDAD_PLAZO' },
			{name: 'NO_DOCTO_FINAL' },
			{name: 'MONEDA_FINAL' },
			{name: 'MONTO_FINAL' ,type: 'float'},
			{name: 'MONTO_CREDITO' ,type: 'float'},
			{name: 'PLAZO_FINAL' ,type: 'float'},
			{name: 'FECHA_VENC_FINAL',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_HOY',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_OPERA_FINAL' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMBRE_IF' },
			{name: 'IC_LINEA_CREDITO' },				
			{name: 'REFERENCIA' },
			{name: 'IC_TASA' },
			{name: 'CG_REL_MAT' },
			{name: 'FN_PUNTOS' },
			{name: 'VALOR_TASA' },
			{name: 'VALOR_TASA_PUNTOS' ,type: 'float'},
			{name: 'MONTO_TASA_INTERES',type: 'float' },
			{name: 'MONTO_AUX_INTERES',type: 'float' },
			{name: 'MONTO_TOTAL_CAPITAL' ,type: 'float'},
			{name: 'PLAZO_UNICO' ,type: 'float'},
			{name: 'SELECCION' }	,
			{name: 'IC_TIPO_FINANCIAMIENTO' ,type: 'float'},
			{name: 'DIAS_MINIMO' ,type: 'float'},		
			{name: 'DIAS_MAXIMO' ,type: 'float'},
			{name: 'PLAZO' ,type: 'float'},
			{name: 'PLAZO_VALIDO' ,type: 'float'},
			{name: 'CLAVE_BIN'},
			{name: 'DESC_BIN'},
			{name: 'CODIGO_BIN'},
			{name: 'ID_ORDEN_TRANS'},
			{name: 'ID_TRANSACCION'},
			{name: 'CODIGO_AUTORIZA'},					
			{name: 'NUMERO_TARJETA'},
			{name: 'TIPOPAGO'},
			{name: 'CG_LINEA_CREDITO'},
			{name: 'FECHA_VENC_LINEA' ,type: 'date', dateFormat: 'd/m/Y'}	
		]
	});

	
	var gruposAcuse = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Datos Documento Inicial', colspan: 17, align: 'center'},
					{header: 'Datos Documento Final', colspan: 12, align: 'center'}					
				]
			]
	});	

	var gridAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridAcuse',
		title: 'Acuse de  Selecci�n de Documentos Modalidad 2',
		hidden: true,
		store: consultaAcuseData,
		clicksToEdit: 1,	
		plugins: gruposAcuse,
		columns: [		
			{
				header: 'SELECCION',
				tooltip: 'SELECCION',
				dataIndex: 'SELECCION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'Epo',
				tooltip: 'Epo',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex: 'NO_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{
				header: 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					return Ext.util.Format.date(causa,'d/m/Y');
          /*
          if(globalTipoPago==2){
						return 'N/A';
					}else{
						return Ext.util.Format.date(causa,'d/m/Y');
					}*/
				}
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left',
				renderer:  function (causa, columna, registro){
					if(globalTipoPago==2){
						return 'N/A';
					}else{
						return causa
					}
				}
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'PORCENTAJE_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},			
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONV',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Tipo cambio',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Monto valuado en pesos',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODALIDAD_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex: 'NO_DOCTO_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Monto cr�dito',
				tooltip: 'Monto cr�dito',
				dataIndex: 'MONTO_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Plazo', // es es editable 
				tooltip: 'Plazo',
				dataIndex: 'PLAZO_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',				
				renderer:function(value,metadata,registro){
					if(globalTipoPago==2){
						return 'N/A';
					}else{				
						var accion=registro.data['IC_TIPO_FINANCIAMIENTO'];
						var tipoPago=registro.data['TIPOPAGO']; // ( 1Financiamiento con Interesess, 2= Meses sin intereses )F09-2015
						if(accion!='1'){
							if(accion == '2' && tipoPago=='2' ){
								return value = registro.data['MESES_INTERESES'] +' Meses';  								
							}else  {								
								return value; 
							}							
						}else  {
							return value;  
						}						
					}
				}
			},
			{
				header: 'Fecha de vencimiento', // es es editable 
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					if(globalTipoPago==2){
						return 'N/A';
					}else{
						return Ext.util.Format.date(causa,'d/m/Y');
					}
				}				
			},		
			{
				header: 'Fecha de operaci�n',
				tooltip: 'Fecha de operaci�n',
				dataIndex: 'FECHA_OPERA_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: "Nombre del Producto",
				tooltip: 'Bin',
				dataIndex: 'DESC_BIN',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left',
        renderer:function(value,metadata,registro){		
					return registro.data['CODIGO_BIN'] + ' - '+value;
				}	
			},
			{
				header: 'Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'			
			},
			{
				header: 'Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_PUNTOS',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},			
			{
				header: 'Monto de intereses',
				tooltip: 'Monto de intereses',
				dataIndex: 'MONTO_TASA_INTERES',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTO_TOTAL_CAPITAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : 'IC_TASA',  //editable
				tooltip: 'IC_TASA',
				dataIndex : 'IC_TASA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right'				
			},
			{				
				header : 'MONTO_AUX_INTERES',  //editable
				tooltip: 'MONTO_AUX_INTERES',
				dataIndex : 'MONTO_AUX_INTERES',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : 'CG_REL_MAT',  //editable
				tooltip: 'CG_REL_MAT',
				dataIndex : 'CG_REL_MAT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right'
			},
			{				
				header : 'FN_PUNTOS',  //editable
				tooltip: 'FN_PUNTOS',
				dataIndex : 'FN_PUNTOS',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'
			},			
			{				
				header : 'IC_LINEA_CREDITO',  //editable
				tooltip: 'IC_LINEA_CREDITO',
				dataIndex : 'IC_LINEA_CREDITO',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'				
			},
			{				
				header : 'ID Orden enviado',  //editable
				tooltip: 'ID Orden enviado',
				dataIndex : 'ID_ORDEN_TRANS',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'				
			},
			{				
				header : 'Respuesta de Operaci�n',
				tooltip: 'ID Operaci�n',
				dataIndex : 'ID_TRANSACCION',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'				
			},
			{				
				header : 'C�digo de Autorizaci�n',
				tooltip: 'C�digo de Autorizaci�n',
				dataIndex : 'CODIGO_AUTORIZA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'				
			},
      {				
				header : 'N�mero Tarjeta de Cr�dito',
				tooltip: 'N�mero Tarjeta de Cr�dito',
				dataIndex : 'NUMERO_TARJETA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'				
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [
			'->',
			'-',						
			{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnGenerarPDFAcuse'											
			},			
			{
				xtype: 'button',
				text: 'Bajar PDF',
				id: 'btnBajarPDFAcuse',
				hidden: true
			},
			'-',
			{
				xtype: 'button',
				text: 'Generar Archivo',
				id: 'btnGenerarCSVAcuse'											
			},			
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				id: 'btnBajarCSVAcuse',
				hidden: true
			},
			'-',
			{
				text: 'Salir',
				xtype: 'button',
				id: 'btnSalir',
				handler: function() {						
					window.location = '24forma02ext.jsp';
				}
			}				
			]
		}
		
	});
				
	var consultaPreAcuseData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'NO_DOCUMENTO'},
			{name: 'NO_ACUSE'},				
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_VENCIMIENTO',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_PUBLICACION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'PLAZO_DOCTO',type: 'float' },
			{name: 'MONEDA' },
			{name: 'IC_MONEDA_DOCTO' },
			{name: 'IC_MONEDA_LINEA' },
			{name: 'IC_IF' },
			{name: 'TIPO_PISO' },				
			{name: 'MONTO' ,type: 'float' },
			{name: 'PLAZO_DESCUENTO' ,type: 'float'},
			{name: 'PORCENTAJE_DESCUENTO' },
			{name: 'MONTO_DESCUENTO' ,type: 'float'},
			{name: 'TIPO_CONV' },
			{name: 'TIPO_CAMBIO' },
			{name: 'MONTO_VALUADO' ,type: 'float'},
			{name: 'MODALIDAD_PLAZO' },
			{name: 'NO_DOCTO_FINAL' },
			{name: 'MONEDA_FINAL' },
			{name: 'MONTO_FINAL' ,type: 'float'},
			{name: 'MONTO_CREDITO' ,type: 'float'},
			{name: 'PLAZO_FINAL' ,type: 'float'},
			{name: 'FECHA_VENC_FINAL',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_HOY',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_OPERA_FINAL' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMBRE_IF' },
			{name: 'IC_LINEA_CREDITO' },				
			{name: 'REFERENCIA' },
			{name: 'IC_TASA' },
			{name: 'CG_REL_MAT' },
			{name: 'FN_PUNTOS' },
			{name: 'VALOR_TASA' },
			{name: 'VALOR_TASA_PUNTOS' ,type: 'float'},
			{name: 'MONTO_TASA_INTERES',type: 'float' },
			{name: 'MONTO_AUX_INTERES',type: 'float' },
			{name: 'MONTO_TOTAL_CAPITAL' ,type: 'float'},
			{name: 'PLAZO_UNICO' ,type: 'float'},
			{name: 'SELECCION' }	,
			{name: 'IC_TIPO_FINANCIAMIENTO' ,type: 'float'},
			{name: 'DIAS_MINIMO' ,type: 'float'},		
			{name: 'DIAS_MAXIMO' ,type: 'float'},
			{name: 'PLAZO' ,type: 'float'},
			{name: 'PLAZO_VALIDO' ,type: 'float'},
			{name: 'CLAVE_BIN'},
			{name: 'DESC_BIN'},
			{name: 'CODIGO_BIN'},
			{name: 'TIPOPAGO'},
			{name: 'CG_LINEA_CREDITO'},
			{name: 'FECHA_VENC_LINEA' ,type: 'date', dateFormat: 'd/m/Y'}
		]
	});
	
	var gruposPreAcuse = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Datos Documento Inicial', colspan: 17, align: 'center'},
					{header: 'Datos Documento Final', colspan: 12, align: 'center'}					
				]
			]
	});	
	
	var gridPreAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridPreAcuse',
		title: 'PreAcuse de Selecci�n de Documentos Modalidad 2 ',
		hidden: true,
		store: consultaPreAcuseData,
		clicksToEdit: 1,
		plugins: gruposPreAcuse,
		columns: [			
			{
				header: 'SELECCION',
				tooltip: 'SELECCION',
				dataIndex: 'SELECCION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'Epo',
				tooltip: 'Epo',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex: 'NO_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{
				header: 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					return Ext.util.Format.date(causa,'d/m/Y');
          /*if(globalTipoPago==2){
						return 'N/A';
					}else{
						return Ext.util.Format.date(causa,'d/m/Y');
					}*/
				}
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left',
				renderer:  function (causa, columna, registro){
					if(globalTipoPago==2){
						return 'N/A';
					}else{
						return causa
					}
				}
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'PORCENTAJE_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},			
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONV',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Tipo cambio',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Monto valuado en pesos',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODALIDAD_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex: 'NO_DOCTO_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Monto cr�dito',
				tooltip: 'Monto cr�dito',
				dataIndex: 'MONTO_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
				{
				header: 'Plazo', // es es editable 
				tooltip: 'Plazo',
				dataIndex: 'PLAZO_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',				
				renderer:function(value,metadata,registro){
					if(globalTipoPago==2){
						return 'N/A';
					}else{				
						var accion=registro.data['IC_TIPO_FINANCIAMIENTO'];
						var tipoPago=registro.data['TIPOPAGO']; // ( 1Financiamiento con Interesess, 2= Meses sin intereses )F09-2015
						if(accion!='1'){
							if(accion == '2' && tipoPago=='2' ){
								return value = registro.data['MESES_INTERESES'] +' Meses';  								
							}else  {
								NE.util.colorCampoEdit(value,metadata,registro);
								return value; 
							}							
						}else  {
							return value;  
						}						
					}
				}
			},
			{
				header: 'Fecha de vencimiento', // es es editable 
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					if(globalTipoPago==2){
						return 'N/A';
					}else{
						return Ext.util.Format.date(causa,'d/m/Y');
					}
				}				
			},		
			{
				header: 'Fecha de operaci�n',
				tooltip: 'Fecha de operaci�n',
				dataIndex: 'FECHA_OPERA_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: "<sup>2</sup>Nombre del Producto",
				tooltip: 'Bin',
				dataIndex: 'DESC_BIN',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left',
        renderer:function(value,metadata,registro){		
					return registro.data['CODIGO_BIN'] + ' - '+value;
				}	
			},
			{
				header: 'Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'			
			},
			{
				header: 'Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_PUNTOS',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},			
			{
				header: 'Monto de intereses',
				tooltip: 'Monto de intereses',
				dataIndex: 'MONTO_TASA_INTERES',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTO_TOTAL_CAPITAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : 'IC_TASA',  //editable
				tooltip: 'IC_TASA',
				dataIndex : 'IC_TASA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right'				
			},
			{				
				header : 'MONTO_AUX_INTERES',  //editable
				tooltip: 'MONTO_AUX_INTERES',
				dataIndex : 'MONTO_AUX_INTERES',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right'
			},
			{				
				header : 'CG_REL_MAT',  //editable
				tooltip: 'CG_REL_MAT',
				dataIndex : 'CG_REL_MAT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right'
			},
			{				
				header : 'FN_PUNTOS',  //editable
				tooltip: 'FN_PUNTOS',
				dataIndex : 'FN_PUNTOS',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'
			},			
			{				
				header : 'IC_LINEA_CREDITO',  //editable
				tooltip: 'IC_LINEA_CREDITO',
				dataIndex : 'IC_LINEA_CREDITO',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'				
			}				
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [
				'-',
				'->',
				{
					xtype: 'button',
					text: 'Confirmar',
					id: 'btnConfirmarP'											
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFPreAcuse'											
				},				
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFPreAcuse',
					hidden: true
				},
				'-',
				{
					text: 'Cancelar',
					xtype: 'button',
					id: 'btnCancelarPre',
					handler: function() {						
						window.location = '24forma02ext.jsp';
					}
				}
			]
		}
		
	});
	
	var numDoctos =0;	
	var montoSeleccionado =0;
	var saldoDisponible =0;
	var totalDoctosMN =0;
	var totalMontoMN =0;
	var totalMontoDescMN =0;
	var totalInteresMN2=0;
	var totalMontoCreditoMN =0;	
	var totalDoctosUSD=0;
	var totalMontoUSD=0;
	var totalMontoImpUSD=0;
	var totalMontoDescUSD =0;
	var totalInteresUSD2=0;
	var totalMontoCreditoUSD =0;	
	var totalDoctosConv=0;
	var totalMontosConv=0;
	var totalMontoImpConv=0;
	var totalMontoDescConv=0;
	var totalMontoIntConv=0;
	var totalMontoCreditoConv =0;
	var totalInteresConv =0;
	var moneda; 
	var totalDoctos =0; 
	var totalMonto  =0;
	var totalDoctosF  =0;
	var totalMontoF  =0;
	var totalInteresF  =0;
	var monedaUS;
	var totalDoctosUSD  =0;
	var totalMontoUSD   =0;
	var totalDoctosFUSD   =0; 
	var totalMontoFUSD  =0;
	var totalInteresFUSD  =0;
	var totalMontoInteresF =0;
	var totalInteresF  =0;
	var totalConvPesos =0;
	var totalMontoInteresFUSD =0;
	var hayDoctosSelect =0; 
					
	var selectModel = new Ext.grid.CheckboxSelectionModel( {
		checkOnly: true,		
		listeners: {
		//Para quitar lo selecccionado en el documentos ******************************************
			rowdeselect: function(selectModel, rowIndex, record) {	 
			
			//vetifico si hay documentos Selecciondos  
				hayDoctosSelect =0;
				
				var storeEdi = gridEditable.getStore();		
				storeEdi.each(function(record) {			
					if(record.data['SELECCION'] =='S'){
						hayDoctosSelect++;	
					}				
				});	
					
				record.data['SELECCION']='N';
				record.commit();	
				
				var monto_credito = record.data['MONTO_CREDITO'];
				var monto =  record.data['MONTO']; 
				var auxMonto = parseFloat(monto_credito);				
				var linea;				
				//esto es para obtener los datos del Gid de Monitor 
				var store = gridMonitor.getStore();			
				store.each(function(record) {							
					if(record.data['SELECCIONADO']=='S'){
						saldoDisponible  = record.data['SALDODISPONIBLE'];
						linea  = record.data['IC_LINEA'];
					}				
				});
			
				if(record.data['SELECCION'] =='N' && hayDoctosSelect >0 ){						
					if(auxMonto>0){
						numDoctos --;
						montoSeleccionado = roundOff(parseFloat(montoSeleccionado)-auxMonto,2);				
						saldoDisponible = roundOff(parseFloat(saldoDisponible)+auxMonto,2);											
					}else{
						return false;
					}
				}	
				
				store.each(function(record) {			
						if(record.data['SELECCIONADO'] =='S'){
							record.data['NUMDOCTOS'] = numDoctos;
							record.data['SALDODISPONIBLE'] = 	saldoDisponible;
							record.data['MONTOSELECCIONADO'] = montoSeleccionado;
							record.commit();
						}				
					});		
											
					if( record.data['IC_MONEDA_DOCTO']=='1') {							
						totalDoctosMN --;
						totalMontoMN		-= parseFloat(record.data['MONTO']);
						var montoSinInteres  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
						if(globalTipoPago==1){
							totalMontoDescMN	-= montoSinInteres
						}else{
							totalMontoDescMN	-= record.data['MONTO_FINAL'];
						}
						totalInteresMN2		-= parseFloat(record.data['MONTO_TASA_INTERES']);				
						totalMontoCreditoMN	-= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
					}				
					if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='54' ){				
						totalDoctosUSD --;
						totalMontoUSD		-= parseFloat(record.data['MONTO']);
						var montoSinInteresUSD  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
						totalMontoDescUSD	-= montoSinInteresUSD;
						totalInteresUSD2		-= parseFloat(record.data['MONTO_TASA_INTERES']);	
						totalMontoCreditoUSD -=parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
					}
					if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){			
						totalDoctosConv --;
						totalMontosConv			-= parseFloat(record.data['MONTO']);
						var montoSinInteresConv  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']);					
						totalMontoDescConv 	-= montoSinInteresConv
						totalInteresConv		-= parseFloat(record.data['MONTO_TASA_INTERES']);
						totalConvPesos			-= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
						totalMontoCreditoConv	-= parseFloat(record.data['MONTO_DESCUENTO']);
					}		
				
						var dataTotales = [	
						[
						totalDoctosMN, totalMontoMN, totalMontoDescMN, totalInteresMN2, 
						totalDoctosUSD, totalMontoUSD, totalMontoDescUSD, totalInteresUSD2,
						totalDoctosConv, totalMontosConv, totalMontoDescConv, totalInteresConv
						]	];
						
					storeMontosData.loadData(dataTotales);
					
					if(record.data['IC_MONEDA_DOCTO']=='1'){								
						 totalMontoInteresF  =  totalInteresMN2; 
						 totalInteresF  =  totalMontoCreditoMN; 				
						}
						if(record.data['IC_MONEDA_DOCTO']=='54'){							 						 
							 totalMontoInteresFUSD =  totalInteresUSD2; 
							 totalInteresFUSD  =  totalMontoCreditoUSD; 
					}	
					
					if(record.data['IC_MONEDA_DOCTO']=='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){							 						 
						totalMontoInteresF =  totalInteresConv; 
						totalInteresF  =  totalConvPesos; 
					}	


					//esto es para obtener los datos del Gid de gridTotalesE 
					var store = gridTotales.getStore();			
					store.each(function(record) {		
						if(record.data['ICMONEDA']=='1'){
						 record.data['MONTOINTERES'] = totalMontoInteresF;
						 record.data['MONTOCAPITALINTERES'] = totalInteresF;
						}
						if(record.data['ICMONEDA']=='54'){	
						 record.data['MONTOINTERES'] = totalMontoInteresFUSD;
						 record.data['MONTOCAPITALINTERES'] = totalInteresFUSD;
						}	
						record.commit();	
					});					
			},	
			//Para selecccionar los documentos ******************************************
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //Se activa cuando una fila est� seleccionada, return false para cancelar.
		
				var tipoPago=record.data['TIPOPAGO'];   // ( 1= Financiamiento con Interesess, 2= Meses sin intereses )F09-2015
				
				if(tipoPago==1){
					if(globalTipoPago==1){
						if(validaplazo('', '', '', record)) {
							record.data['SELECCION']= 'S';
							record.commit();					
						}else{
							record.data['SELECCION']= 'N';
							record.commit();
							return false;	
						}
					} 
				
					
					if(seleccionaDocto (selectModel, rowIndex, keepExisting, record)) {
						record.data['SELECCION']= 'S';
						record.commit();	
									
					}else {
						record.data['SELECCION']= 'N';
						record.commit();
						return false;		
					}
				
				}else {
					
					if(validaFechaLineaCredito('', '', '', record)==false) { //F020-2015			
						if(record.data['SELECCION']=='S') {
							record.data['SELECCION']= 'N';
							record.commit();	
							return false;	
						}
					}else  {
						record.data['SELECCION']= 'S';
						record.commit();	
					}			
						
				}
					var tasa =  inicializaTasasXPlazo( '', '', record,'ic_tasa');
					record.data['IC_TASA']=tasa;
							
					var relMat =  inicializaTasasXPlazo( '', '', record,'cg_rel_mat');
					record.data['CG_REL_MAT']=relMat;				
						
					var fnPuntos =  inicializaTasasXPlazo( '', '', record,'fnPuntos');
					record.data['FN_PUNTOS']=fnPuntos;	
					
					var montoCap = inicializaTasasXPlazo( '', '', record,'MontoCapitalInteres');
					record.data['MONTO_TOTAL_CAPITAL']=montoCap;		
				
					var montoInt = inicializaTasasXPlazo( '', '', record,'MontodeIntereses');
					record.data['MONTO_TASA_INTERES']=montoInt;		
					
					
					if(record.data['IC_TASA']=='') {
						Ext.MessageBox.alert("Mensaje","El documento con el plazo seleccionado no cuenta con una tasa parametrizada. Favor de capturarla");
						record.data['SELECCION']= 'N';
						record.commit();	
						return false;							
					}
					
				
				var monto_credito = record.data['MONTO_CREDITO'];
				var monto =  record.data['MONTO']; 
				var auxMonto = parseFloat(monto_credito);				
				var linea;				
				//esto es para obtener los datos del Gid de Monitor 
				var store = gridMonitor.getStore();			
				store.each(function(record) {							
					if(record.data['SELECCIONADO']=='S'){
						saldoDisponible  = record.data['SALDODISPONIBLE'];
						linea  = record.data['IC_LINEA'];
					}				
				});
					
				if(record.data['SELECCION'] =='S'){						
					if(auxMonto>0){
						numDoctos ++;
						montoSeleccionado = roundOff(parseFloat(montoSeleccionado)+auxMonto,2);				
						if(globalTipoPago==1){
							saldoDisponible = roundOff(parseFloat(saldoDisponible)-auxMonto,2);								
						}
					}
				}	
								
				if(globalTipoPago==1 && parseInt(saldoDisponible)<0){
					Ext.MessageBox.alert("Mensaje","El saldo de la l�nea es insuficiente para el documento");
					record.data['SELECCION']= 'N';
					record.commit();
					return false;
				}
				store.each(function(record) {			
						if(record.data['SELECCIONADO'] =='S'){
							record.data['NUMDOCTOS'] = numDoctos;
							record.data['SALDODISPONIBLE'] = 	saldoDisponible;
							record.data['MONTOSELECCIONADO'] = montoSeleccionado;
							record.commit();
						}				
					});				
				
				record.data['IC_LINEA_CREDITO'] =  linea;
				record.commit();							
				
				
				
				if(tipoPago==1) {		 // ( 1= Financiamiento con Interesess, 2= Meses sin intereses )F09-2015  	
					sumaTotalesIntereses(selectModel, rowIndex, keepExisting, record);	 
				}
				 
		
				if( record.data['IC_MONEDA_DOCTO']=='1') {							
					totalDoctosMN ++;
					totalMontoMN		+= parseFloat(record.data['MONTO']);
					var montoSinInteres  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
					if(globalTipoPago==1){
						totalMontoDescMN	+= montoSinInteres
					}else{
						totalMontoDescMN	+= record.data['MONTO_FINAL'];
					}
					
					totalInteresMN2		+= parseFloat(record.data['MONTO_TASA_INTERES']);				
					totalMontoCreditoMN	+= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
				}				
				if(record.data['IC_MONEDA_DOCTO'] =='54'  &&  record.data['IC_MONEDA_LINEA'] =='54'){				
					totalDoctosUSD ++;
					totalMontoUSD		+= parseFloat(record.data['MONTO']);
					var montoSinInteresUSD  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
					totalMontoDescUSD	+= montoSinInteresUSD;
					totalInteresUSD2		+= parseFloat(record.data['MONTO_TASA_INTERES']);	
					totalMontoCreditoUSD+=parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
				}
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){			
					totalDoctosConv ++;
					totalMontosConv			+= parseFloat(record.data['MONTO']);
				  var montoSinInteresConv  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
					totalMontoDescConv 		+= montoSinInteresConv;				
					totalInteresConv		+= parseFloat(record.data['MONTO_TASA_INTERES']);
					totalConvPesos			+= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);   
					totalMontoCreditoConv	+= parseFloat(record.data['MONTO_DESCUENTO']);
				}
						
					var dataTotales = [	
					[
					totalDoctosMN, totalMontoMN, totalMontoDescMN, totalInteresMN2, 
					totalDoctosUSD, totalMontoUSD, totalMontoDescUSD, totalInteresUSD2,
					totalDoctosConv, totalMontosConv, totalMontoDescConv, totalInteresConv
					]	];
					
					storeMontosData.loadData(dataTotales);
					
					
					if(record.data['IC_MONEDA_DOCTO']=='1'){								
						 totalMontoInteresF  =  totalInteresMN2; 
						 totalInteresF  =  totalMontoCreditoMN; 				
						}
						if(record.data['IC_MONEDA_DOCTO']=='54'){							 						 
							 totalMontoInteresFUSD =  totalInteresUSD2; 
							 totalInteresFUSD  =  totalMontoCreditoUSD; 
						}	
						if(record.data['IC_MONEDA_DOCTO']=='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){							 						 
							 totalMontoInteresF =  totalInteresConv; 
							 totalInteresF  =  totalConvPesos; 
						}	
						
					
						
						//esto es para obtener los datos del Gid de gridTotalesE 
						var store = gridTotales.getStore();			
						store.each(function(record) {						
							if(record.data['MONEDA']=='MONEDA NACIONAL'){
							 record.data['MONTOINTERES'] = totalMontoInteresF;
							 record.data['MONTOCAPITALINTERES'] = totalInteresF;
							}
							if(record.data['MONEDA']=='MONEDA USD'){	
							 record.data['MONTOINTERES'] = totalMontoInteresFUSD;
							 record.data['MONTOCAPITALINTERES'] = totalInteresFUSD;
							}	
							record.commit();	
						});
			}				
		}
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma02.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'NO_DOCUMENTO'},
			{name: 'NO_ACUSE'},				
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_VENCIMIENTO',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_PUBLICACION',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'PLAZO_DOCTO',type: 'float' },
			{name: 'MONEDA' },
			{name: 'IC_MONEDA_DOCTO' },
			{name: 'IC_MONEDA_LINEA' },
			{name: 'IC_IF' },
			{name: 'TIPO_PISO' },				
			{name: 'MONTO' ,type: 'float' },
			{name: 'PLAZO_DESCUENTO' ,type: 'float'},
			{name: 'PORCENTAJE_DESCUENTO' },
			{name: 'MONTO_DESCUENTO' ,type: 'float'},
			{name: 'TIPO_CONV' },
			{name: 'TIPO_CAMBIO' },
			{name: 'MONTO_VALUADO' ,type: 'float'},
			{name: 'MODALIDAD_PLAZO' },
			{name: 'NO_DOCTO_FINAL' },
			{name: 'MONEDA_FINAL' },
			{name: 'MONTO_FINAL' ,type: 'float'},
			{name: 'MONTO_CREDITO' ,type: 'float'},
			{name: 'PLAZO_FINAL' ,type: 'float'},
			{name: 'FECHA_VENC_FINAL' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_HOY',type: 'date', dateFormat: 'd/m/Y' },
			{name: 'FECHA_OPERA_FINAL' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMBRE_IF' },
			{name: 'IC_LINEA_CREDITO' },				
			{name: 'REFERENCIA' },
			{name: 'IC_TASA' },
			{name: 'CG_REL_MAT' },
			{name: 'FN_PUNTOS' },
			{name: 'VALOR_TASA' },
			{name: 'VALOR_TASA_PUNTOS' ,type: 'float'},
			{name: 'MONTO_TASA_INTERES',type: 'float' },
			{name: 'MONTO_AUX_INTERES',type: 'float' },
			{name: 'MONTO_TOTAL_CAPITAL' ,type: 'float'},
			{name: 'PLAZO_UNICO' ,type: 'float'},
			{name: 'SELECCION' }	,
			{name: 'IC_TIPO_FINANCIAMIENTO' ,type: 'float'},
			{name: 'DIAS_MINIMO' ,type: 'float'},		
			{name: 'DIAS_MAXIMO' ,type: 'float'},
			{name: 'PLAZO' ,type: 'float'},
			{name: 'PLAZO_VALIDO' ,type: 'float'},
			{name: 'PLAZO_PRONTO_PAGO' ,type: 'float'},
			{name: 'FECHA_DIAS_OPERACION' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DIAS_OPERACION' ,type: 'float'},
			{name: 'CLAVE_BIN'},
			{name: 'DESC_BIN'},
			{name: 'CODIGO_BIN'},
			{name: 'COMISION_BIN'},
			{name: 'MESES_INTERESES'},
			{name: 'TIPOPAGO'},
			{name: 'CG_LINEA_CREDITO'},
			{name: 'FECHA_VENC_LINEA' ,type: 'date', dateFormat: 'd/m/Y'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});
		
	var gridEditable = new Ext.grid.EditorGridPanel({
		id: 'gridEditable',
		title: 'Selecci�n de Documentos Modalidad 2',
		hidden: true,
		store: consultaData,
		clicksToEdit: 1,		
		columns: [			
			{
				header: 'SELECCION',
				tooltip: 'SELECCION',
				dataIndex: 'SELECCION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: '<sup>1</sup>Epo',
				tooltip: 'Epo',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup>Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '<sup>1</sup>Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex: 'NO_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '<sup>1</sup>Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{
				header: '<sup>1</sup>Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					return Ext.util.Format.date(causa,'d/m/Y');
          /*if(globalTipoPago==2){
						return 'N/A';
					}else{
						return Ext.util.Format.date(causa,'d/m/Y');
					}*/
				}
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>1</sup>Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>1</sup>Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left',
				renderer:  function (causa, columna, registro){
					if(globalTipoPago==2){
						return 'N/A';
					}else{
						return causa
					}
				}
			},			
			{
				header: '<sup>1</sup>Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup>Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<sup>1</sup>Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '<sup>1</sup>% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'PORCENTAJE_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},			
			{
				header: '<sup>1</sup>Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<sup>1</sup>Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONV',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '<sup>1</sup>Tipo cambio',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup>Monto valuado en pesos',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<sup>1</sup>Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODALIDAD_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>2</sup>N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex: 'NO_DOCTO_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '<sup>2</sup>Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>2</sup>Monto cr�dito',
				tooltip: 'Monto cr�dito',
				dataIndex: 'MONTO_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},				
			{
				header: '<sup>2</sup>Plazo', // es es editable 
				tooltip: 'Plazo',
				dataIndex: 'PLAZO_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: false
				},
				renderer:function(value,metadata,registro){
					if(globalTipoPago==2){
						return 'N/A';
					}else{				
						var accion=registro.data['IC_TIPO_FINANCIAMIENTO'];
						var tipoPago=registro.data['TIPOPAGO']; // ( 1Financiamiento con Interesess, 2= Meses sin intereses )F09-2015
						if(accion!='1'){
							if(accion == '2' && tipoPago=='2' ){
								return value = registro.data['MESES_INTERESES'] +' Meses';  								
							}else  {
								NE.util.colorCampoEdit(value,metadata,registro);
								return value; 
							}							
						}else  {
							return value;  
						}						
					}
				}
			},
			{
				header: '<sup>2</sup>Fecha de vencimiento', // es es editable 
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					if(globalTipoPago==2){
						return 'N/A';
					}else{
						return Ext.util.Format.date(causa,'d/m/Y');
					}
				}				
			},			
			{
				header: '<sup>2</sup>Fecha de operaci�n',
				tooltip: 'Fecha de operaci�n',
				dataIndex: 'FECHA_OPERA_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>2</sup>IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: "<sup>2</sup>Nombre del Producto",
				tooltip: 'Bin',
				dataIndex: 'DESC_BIN',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left',
        renderer:function(value,metadata,registro){		
					return registro.data['CODIGO_BIN'] + ' - '+value;
				}	
			},
			{
				header: '<sup>2</sup>Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left',
				renderer:function(value,metadata,registro){		
					var select = registro.data['SELECCION'];	
					var refe = inicializaTasasXPlazo( value, metadata, registro,'ReferenciaTasa');
					if(select=='S'){
						refe = (value=='')?'':value;
					}
					return refe;
				}	
			},
			{
				header: '<sup>2</sup>Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_PUNTOS',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];	
					var valort = Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'ValorTasaInteres'),'0.0000%');
					if(select=='S'){
            if(csMesesSinInt=='S'){
              valort =  Ext.util.Format.number((value=='0')?'0':value, '0.0000%');					
            }else{
              valort =  Ext.util.Format.number((value=='0')?'':value, '0.0000%');					
            }
						
					}
					return valort;	
				}
			},
			
			{
				header: '<sup>2</sup>Monto de intereses',
				tooltip: 'Monto de intereses',
				dataIndex: 'MONTO_TASA_INTERES',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];					
					var monto =  Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'MontodeIntereses'), '$0,0.00');	
					if(select=='S'){
						monto =  Ext.util.Format.number((value=='0')?'':value, '$0,0.00');
					}
					return monto;
				}
			},			
			{
				header: '<sup>2</sup>Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTO_TOTAL_CAPITAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer:function(value,metadata,registro){
					var monto =  Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'MontoCapitalInteres'), '$0,0.00');
					var select = registro.data['SELECCION'];
					if(select=='S'){
						monto =Ext.util.Format.number((value=='0')?'':value, '$0,0.00');	
					}
					return monto;
				}
			},
			{				
				header : '<sup>2</sup>IC_TASA',  //editable
				tooltip: 'IC_TASA',
				dataIndex : 'IC_TASA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var tasa =  inicializaTasasXPlazo( value, metadata, registro,'ic_tasa');
				/*	if(select=='S'){
						tasa  = (value=='')?'':value;
					}						
					*/
					return tasa	
				}					
			},			
			{				
				header : '<sup>2</sup>MONTO_AUX_INTERES',  //editable
				tooltip: 'MONTO_AUX_INTERES',
				dataIndex : 'MONTO_AUX_INTERES',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var montoAuxi =  inicializaTasasXPlazo( value, metadata, registro,'montoAuxi');
					if(select=='S'){
						montoAuxi  = (value=='')?'':value;
					}
						return  Ext.util.Format.number(montoAuxi, '$0,0.00');	
				}	
			},
			{				
				header : '<sup>2</sup>CG_REL_MAT',  //editable
				tooltip: 'CG_REL_MAT',
				dataIndex : 'CG_REL_MAT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var relMat =  inicializaTasasXPlazo( value, metadata, registro,'cg_rel_mat');
					if(select=='S'){
						relMat  = (value=='')?'':value;
					}
					return relMat;	
				}	
			},
			{				
				header : '<sup>2</sup>FN_PUNTOS',  //editable
				tooltip: 'FN_PUNTOS',
				dataIndex : 'FN_PUNTOS',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var fnPuntos =  inicializaTasasXPlazo( value, metadata, registro,'fnPuntos');
					if(select=='S'){
						fnPuntos  = (value=='')?'':value;
					}
					return fnPuntos;	
				}	
			},			
			{				
				header : '<sup>2</sup>IC_LINEA_CREDITO',  //editable
				tooltip: 'IC_LINEA_CREDITO',
				dataIndex : 'IC_LINEA_CREDITO',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'				
			}	,	
			{				
				header : 'MONTO_TASA_INTERES',  //editable
				tooltip: 'MONTO_TASA_INTERES',
				dataIndex : 'MONTO_TASA_INTERES',
				width : 150,
				sortable : false,								
				align: 'center',
				hidden: true
			}	,
			{				
				header : 'MONTO_CREDITO',  //editable
				tooltip: 'MONTO_CREDITO',
				dataIndex : 'MONTO_CREDITO',
				width : 150,
				sortable : false,					
				align: 'center',
				hidden: true
			},
				{				
				header : 'TIPO_PISO',  //editable
				tooltip: 'TIPO_PISO',
				dataIndex : 'TIPO_PISO',
				width : 150,
				sortable : false,					
				align: 'center',
				hidden: true
			},
			{				
				header : 'MONTO_AUX_INTERES',  //editable
				tooltip: 'MONTO_AUX_INTERES',
				dataIndex : 'MONTO_AUX_INTERES',
				width : 150,
				sortable : false,					
				align: 'center',
				hidden: true
			},
			{				
				header : 'MONTO_CREDITO',  //editable
				tooltip: 'MONTO_CREDITO',
				dataIndex : 'MONTO_CREDITO',
				width : 150,
				sortable : false,					
				align: 'center',
				hidden: true
			},
			{				
				header : 'VALOR_TASA_PUNTOS',  //editable
				tooltip: 'VALOR_TASA_PUNTOS',
				dataIndex : 'VALOR_TASA_PUNTOS',
				width : 150,
				sortable : false,					
				align: 'center',
				hidden: true
			},
			{				
				header : 'MONTO_TOTAL_CAPITAL',  //editable
				tooltip: 'MONTO_TOTAL_CAPITAL',
				dataIndex : 'MONTO_TOTAL_CAPITAL',
				width : 150,
				sortable : false,					
				align: 'center',
				hidden: true
			},			
			{				
				header : 'IC_MONEDA_LINEA',  //editable
				tooltip: 'IC_MONEDA_LINEA',
				dataIndex : 'IC_MONEDA_LINEA',
				width : 150,
				sortable : false,					
				align: 'center',
				hidden: true
			},
			
			selectModel
		],	
		sm:selectModel,
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 950,
		align: 'center',
		frame: false,
		listeners: {	
		beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridEditable = e.gridEditable; 
				var campo= e.field;
				var accion=record.data['IC_TIPO_FINANCIAMIENTO'];				
				if(campo == 'PLAZO_FINAL'){
					
					if(accion=='1' || globalTipoPago==2 ){ 
						return false;
					}else {
						if(  accion=='2' && record.data['TIPOPAGO'] == '2'  ) {
							return false;
						}else {
							return true; 
						}
					}
				}				
			},
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
				var record = e.record;
				var gridEditable = e.gridEditable; 
				var campo= e.field;
				
				var montoInt = inicializaTasasXPlazo( '', '', record,'MontodeIntereses');
				record.data['MONTO_TASA_INTERES']=montoInt;			
								
				var montoCap = inicializaTasasXPlazo( '', '', record,'MontoCapitalInteres');
				record.data['MONTO_TOTAL_CAPITAL']=montoCap;								
				
				var varTasa = inicializaTasasXPlazo( '', '', record,'ValorTasaInteres');
				record.data['VALOR_TASA_PUNTOS']=varTasa;				
	
				var referencia = inicializaTasasXPlazo( '', '', record,'ReferenciaTasa');
				record.data['REFERENCIA']=referencia;	
				
				var montoAuxi = inicializaTasasXPlazo( '', '', record,'montoAuxi');
				record.data['MONTO_AUX_INTERES']=montoAuxi;	
								
				var tasa =  inicializaTasasXPlazo( '', '', record,'ic_tasa');
				record.data['IC_TASA']=tasa;
					
				var relMat =  inicializaTasasXPlazo( '', '', record,'cg_rel_mat');
				record.data['CG_REL_MAT']=relMat;				
				
				var fnPuntos =  inicializaTasasXPlazo( '', '', record,'fnPuntos');
				record.data['FN_PUNTOS']=fnPuntos;						
						
				
				if(campo=='PLAZO_FINAL') { 	validaplazo('', '', '', record); 	} //cuando se captura el plazo 
				
				record.commit();		
			
			}
			
		},
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',
				'-',
				{
					text: 'Cancelar',
					xtype: 'button',
					id: 'btnCancelar',
					handler: function() {						
						window.location = '24forma02ext.jsp';
					}
				},
				'-',
				{
					xtype: 'button',
					id: 'btnConfirmar',
					text: 'Confirmar',
					handler: procesarConfirmar					
				}		
			]
		}
		
	});
				
  var storeTipoPagoData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'clave'},
				{name: 'descripcion'}
		  ]
	 });
  
	var elementosForma = [
		{
			xtype: 'compositefield',
			fieldLabel: 'EPO',
			combineErrors: false,
			msgTarget: 'side',
			width: 100,
			items: [
				{
					xtype: 'combo',
					name: 'ic_epo',
					id: 'ic_epo1',
					fieldLabel: 'Nombre de la EPO',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_epo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoEPOData,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 300,
					listeners: {
						select: {
							fn: function(combo) {										
								procesaIniciales(combo.getValue());
								if(Ext.getCmp('ic_epo1').getValue()!='' && Ext.getCmp('ic_moneda1').getValue()!='' && Ext.getCmp('cmbTipoPago1').getValue()!=''){
									catalogoTC.load({
										params:Ext.apply(fp.getForm().getValues())
									});
								}
							}
						}	
					}
				},
				{
					xtype: 'displayfield',
					value: 'Moneda:',
					width: 100
				},
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners: {
						select: {
							fn: function(combo) {
								Ext.getCmp("lineaTodos1").setValue('');
								if(Ext.getCmp('cmbTipoPago1').getValue()==2){
									combo.setValue("1");
								}
								if(Ext.getCmp('cmbTipoPago1').getValue()==1){
									catalogoLineas.load({
										params: {
											ic_moneda:combo.getValue()			
										}	
									});
								}
								
								if(Ext.getCmp('ic_epo1').getValue()!='' && Ext.getCmp('ic_moneda1').getValue()!='' && Ext.getCmp('cmbTipoPago1').getValue()!=''){
									catalogoTC.load({
										params:Ext.apply(fp.getForm().getValues())
									});
								}
								
								
								
							}
						}	
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. documento inicial',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
				xtype: 'textfield',
				name: 'ig_numero_docto',
				id: 'ig_numero_docto1',
				fieldLabel: 'Num. documento inicial',
				allowBlank: true,
				hidden: false,
				maxLength: 15,	
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'Monto documento',
					width: 120
				},
				{
					xtype: 'numberfield',
					name: 'fn_monto_de',
					id: 'fn_monto_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',				
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'numberfield',
					name: 'fn_monto_a',
					id: 'fn_monto_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',					
					margins: '0 20 0 0'  
				}
			]
		},			
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. acuse de carga',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'cc_acuse',
					id: 'cc_acuse1',
					fieldLabel: 'Num. acuse de carga',
					allowBlank: true,
					hidden: false,
					maxLength: 15,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				},				
				{
					xtype: 'checkbox',
					name: 'mto_descuento',
					id: 'mto_descuento1',
					fieldLabel: 'Monto % de descuento',
					allowBlank: true,
					hidden: false,					
					msgTarget: 'side',
					margins: '0 20 0 0',
					align: 'center'
				},
				{
					xtype: 'displayfield',
					value: 'Monto % de descuento',
					width: 200,
					align: 'left'
				}			
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha emisi�n docto',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_emision_de',
					id: 'df_fecha_emision_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_emision_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_emision_a',
					id: 'df_fecha_emision_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_emision_de1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'S�lo documentos modificados',
					width:120
				},
				{
					xtype: 'checkbox',
					name: 'doctos_cambio',
					id: 'doctos_cambio1',
					fieldLabel: 'S�lo documentos modificados',
					allowBlank: true,
					hidden: false,					
					msgTarget: 'side',
					margins: '0 20 0 0',
					value:	'S',
					align: 'center'
				}
				]
			}	,
			{
			xtype: 'compositefield',
			fieldLabel: 'Fecha vencimiento docto',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_de',
					id: 'df_fecha_venc_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_venc_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_a',
					id: 'df_fecha_venc_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_venc_de1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'Modalidad de plazo:',
					width: 150
				},				
				{
					xtype: 'combo',
					name: 'modo_plazo',
					id: 'modo_plazo1',
					fieldLabel: 'Modalidad de plazo',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'modo_plazo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoModalidadPlazoData,
					tpl : NE.util.templateMensajeCargaCombo				
				}						
				]
			},
			{
			xtype: 'compositefield',
			fieldLabel: 'Fecha publicaci�n de',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_de',
					id: 'fecha_publicacion_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_publicacion_a1',					
					margins: '0 20 0 0',			
					value:fechaHoy
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_a',
					id: 'fecha_publicacion_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_publicacion_de1',
					margins: '0 20 0 0' ,
					value:fechaHoy
				}
				]
			},
			{
				xtype: 'combo',
				name: 'cmbTipoPago',
				id: 'cmbTipoPago1',
				fieldLabel: 'Tipo de Pago',
        anchor: '50%',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'cmbTipoPago',
				emptyText: 'Seleccione...',					
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				allowBlank: true,
				store : storeTipoPagoData,
				//tpl : NE.util.templateMensajeCargaCombo,
				listeners: {
					change: {
						fn: function(combo, nVal, oVal) {
							var cmbTC = Ext.getCmp('cmbTCredTodos1');
							var cmbLT = Ext.getCmp('lineaTodos1');
							if(combo.getValue()==2){
									Ext.getCmp('ic_moneda1').setValue("1");
							}
							if(nVal != oVal){
								globalTipoPago = nVal;
								if(nVal==1){
                  ctexto4.update(cadEpoNew);
                  ctexto6.update(cadEpoNew);
									cmbTC.setValue('')
									cmbTC.hide();
									cmbLT.show();
									catalogoLineas.load({
										params: {
											ic_moneda:Ext.getCmp('ic_moneda1').getValue()
										}	
									});
									
								}else if(nVal==2){
                  ctexto4.update(cadEpoNewTC);
                  ctexto6.update(cadEpoNewTC);
									cmbLT.setValue('');
									cmbLT.hide();
									cmbTC.show();
									
									if(Ext.getCmp('ic_epo1').getValue()!='' && Ext.getCmp('ic_moneda1').getValue()!=''){
										catalogoTC.load({
											params:Ext.apply(fp.getForm().getValues())
										});
									}
								}
								
								ctexto1.hide();
								ctexto2.hide();
								ctexto3.hide();
								ctexto4.hide();
								ctexto5.hide();
								gridMonitor.hide();
								gridEditable.hide();
								gridTotales.hide();
								
							}
						}
					}	
				}
			},
			{
				xtype: 'combo',
				name: 'cmbTCredTodos',
				id: 'cmbTCredTodos1',
				fieldLabel: 'Seleccionar Documentos con la siguiente tarjeta',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'cmbTCredTodos',
				emptyText: 'Seleccione...',					
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				hidden: true,
				minChars : 1,
				allowBlank: true,
				store : catalogoTC,
				tpl : NE.util.templateMensajeCargaCombo,
				listeners: {
					select: {
						fn: function(combo) {										
							//procesaLineas(combo.getValue());	
							
						}
					}	
				}
			},
			{
					xtype: 'combo',
					name: 'lineaTodos',
					id: 'lineaTodos1',
					fieldLabel: 'Seleccionar Documentos con el siguiente IF',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'lineaTodos',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					hidden: true,
					store : catalogoLineas,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners: {
						select: {
							fn: function(combo) {										
								procesaLineas(combo.getValue());	
								
							}
						}	
					}
				}
		];	
		

		var fp = new Ext.form.FormPanel({
			id: 'forma',
			width: 885,
			title: 'Criterios de B�squeda',
			frame: true,
			collapsible: true,
			titleCollapse: false,
			bodyStyle: 'padding: 6px',
			labelWidth: 126,
			defaultType: 'textfield',
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			items: elementosForma,
			monitorValid: true,
			buttons: [
				{
					text: 'Buscar',
					iconCls: 'icoBuscar',
					formBind: true,
					handler: function(boton, evento) {	
						
					var clave_epo = Ext.getCmp("ic_epo1");
					if (Ext.isEmpty(clave_epo.getValue()) ) {
						clave_epo.markInvalid('El valor de la EPO es requerido.');
						return;
					}	
					
					var ic_moneda = Ext.getCmp("ic_moneda1");
					if (Ext.isEmpty(ic_moneda.getValue()) ) {
						ic_moneda.markInvalid('El valor de la Moneda requerido.');
						return;
					}
					
					var fn_monto_de = Ext.getCmp("fn_monto_de1");
					var fn_monto_a = Ext.getCmp("fn_monto_a1");
					if (!Ext.isEmpty(fn_monto_de.getValue()) && Ext.isEmpty(fn_monto_a.getValue())    
						||  Ext.isEmpty(fn_monto_de.getValue()) && !Ext.isEmpty(fn_monto_a.getValue())    ) {
						fn_monto_a.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
						fn_monto_de.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
						return;
					}
									
					var df_fecha_emision_de = Ext.getCmp("df_fecha_emision_de1");
					var df_fecha_emision_a = Ext.getCmp("df_fecha_emision_a1");
					if (!Ext.isEmpty(df_fecha_emision_de.getValue()) && Ext.isEmpty(df_fecha_emision_a.getValue())    
						||  Ext.isEmpty(df_fecha_emision_de.getValue()) && !Ext.isEmpty(df_fecha_emision_a.getValue())    ) {
						df_fecha_emision_de.markInvalid('Debe capturar ambas fechas de emision o dejarlas en blanco');
						df_fecha_emision_a.markInvalid('Debe capturar ambas fechas de emision o dejarlas en blanco');
						return;
					}
					
					var df_fecha_venc_de = Ext.getCmp("df_fecha_venc_de1");
					var df_fecha_venc_a = Ext.getCmp("df_fecha_venc_a1");
					if (!Ext.isEmpty(df_fecha_venc_de.getValue()) && Ext.isEmpty(df_fecha_venc_a.getValue())    
						||  Ext.isEmpty(df_fecha_venc_de.getValue()) && !Ext.isEmpty(df_fecha_venc_a.getValue())    ) {
						df_fecha_venc_de.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
						df_fecha_venc_a.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
						return;
					}
					var fecha_publicacion_de = Ext.getCmp("fecha_publicacion_de1");
					var fecha_publicacion_a = Ext.getCmp("fecha_publicacion_a1");
					if (!Ext.isEmpty(fecha_publicacion_de.getValue()) && Ext.isEmpty(fecha_publicacion_a.getValue())    
						||  Ext.isEmpty(fecha_publicacion_de.getValue()) && !Ext.isEmpty(fecha_publicacion_a.getValue())    ) {
						fecha_publicacion_de.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');
						fecha_publicacion_a.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');
						return;
					}
					
					var cmbTipoPago = Ext.getCmp("cmbTipoPago1");
					if (Ext.isEmpty(cmbTipoPago.getValue()) ) {
						cmbTipoPago.markInvalid('El valor del Tipo de Pago es  requerido.');
						return;
					}
					
					
						
					
					if(globalTipoPago==1){
						var lineaTodos = Ext.getCmp("lineaTodos1");
						if (Ext.isEmpty(lineaTodos.getValue()) ) {
							lineaTodos.markInvalid('El valor de IF es requerido.');
							return;
						}
					}else if(globalTipoPago==2){
						var cmbTCredTodos = Ext.getCmp("cmbTCredTodos1");
						//alert(cmbTCredTodos.getValue());
						if (Ext.isEmpty(cmbTCredTodos.getValue()) ) {
							cmbTCredTodos.markInvalid('El valor de la tarjeta es requerdo.');
							return;
						}
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');
					//consulta para el montor de lineas de Credito
					if(globalTipoPago==1){
						consultaMonitorData.load({
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ConsultaMonitor'
							})
						});
					}
					//consulta para los registros con CCC
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consulta'
						})
					});
					
					totalesData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ResumenTotales'
						})
					});						
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24forma02ext.jsp';
				}
			}
		]			
	});	
		
	var ctexto1 = new Ext.form.FormPanel({		
		id: 'ctexto1',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: texto1.join('')			
	});
	
	
	var ctexto2 = new Ext.form.FormPanel({		
		id: 'ctexto2',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: texto2.join('')			
	});
	
	
	var ctexto3 = new Ext.form.FormPanel({		
		id: 'ctexto3',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: cadEpoCemex.join('')			
	});
	
	
	var ctexto4 = new Ext.form.FormPanel({		
		id: 'ctexto4',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: cadEpoNew.join('')
    //html: cadEpo.join('')			
	});
		
	var ctexto5 = new Ext.form.FormPanel({		
		id: 'ctexto5',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: texto4.join('')			
	});
	var ctexto6 = new Ext.form.FormPanel({		
			id: 'ctexto6',
			width: 943,			
			frame: true,		
			titleCollapse: false,
			hidden: true,
			bodyStyle: 'padding: 6px',			
			defaultType: 'textfield',
			html: texto6.join('')			
		});
		
	var ctexto7= new Ext.form.FormPanel({		
		id: 'ctexto7',
		width: 600,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		defaultType: 'textfield',
		html: texto7.join('')			
	});
//-----------------------------------------------------------------------------
var iframeExt = new TYPO3.iframePanel({
	style: 'margin:0 auto;',
	frameborder: '2', 
	 width: 600,
	 height: 400
});
var ventanaPagoTC = new Ext.Window({
		width: 500,
		height: 240,
		modal: true,
		closable: true,
		closeAction: 'hide',
		resizable: true,
		minWidth: 500,
		//minHeight: 450,
		//height: 300,
		maximized: false,
		constrain: true,
		//layout: 'fit',
		items: [iframeExt],
		listeners:{
			close: function(){
				//alert('close fine');
			},
			hide: function(){
				//alert('hide fine');
			}
		}
});

	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			applyTo: 'areaContenido',
			width: 949,
			items: [
				fp,
				ctexto7,
				NE.util.getEspaciador(20),
				gridMonitor, 				
				NE.util.getEspaciador(20),
				ctexto1,	
				NE.util.getEspaciador(20),
				gridEditable,
				gridPreAcuse,
				gridAcuse,	
				NE.util.getEspaciador(20),
				gridMontos,
				gridTotales,
				NE.util.getEspaciador(20),
				ctexto6,
				NE.util.getEspaciador(20),
				gridCifrasControl,
				ctexto2,				
				NE.util.getEspaciador(20),
				ctexto3,
				NE.util.getEspaciador(20),
				ctexto4,
				NE.util.getEspaciador(20),
				ctexto5, 	
				NE.util.getEspaciador(20)
			]
		});
	
	
	catalogoEPOData.load();
	catalogoMonedaData.load();	
	catalogoModalidadPlazoData.load();
	//catalogoLineas.load();


	var procesaIniciales =  function(ic_epo) {	
		Ext.Ajax.request({
			url: '24forma02.data.jsp',
			params: {
				informacion: "SeleccionEPO",
				ic_epo: ic_epo
			},			
			callback: procesaValoresIniciales				
		});	
	}
	
	//esta funcion es para saber que linea de credito fue seleccionada
	var procesaLineas =  function(lineaTodos) {	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		if (gridMonitor.isVisible()) {			
			var store = gridMonitor.getStore();			
			store.each(function(record) {				
				if(record.data['IC_LINEA']==lineaTodos){														
					record.data['SELECCIONADO'] ='S';
					record.commit();
				}	else{
				record.data['SELECCIONADO'] ='N';
					record.commit();
				}
			});
		}	
		
	}
	
	
	
});