	/*var cadEpoCemex = [
	'Manifiesto mi obligaci�n y aceptaci�n de pagar el 100% del valor del (los) DOCUMENTO(S) FINAL(ES) al ',
	'INTERMEDIARIO FINANCIERO en su fecha de vencimiento.'	];
	*/
	var cadEpo =[
	'De conformidad con lo dispuesto en los art�culos 32 C del C�digo Fiscal 2038 y 2041 del C�digo  ',
	'Civil Federal, fui notificado de la Cesi�n de derechos de las operaciones descritas en la p�gina y ',
	'para los efectos legales conducentes.'	
	];
	
	var texto3 = [
    'De conformidad con lo dispuesto en los art�culos 32 C del C�digo Fiscal o 2038 y 2041 del C�digo Civil Federal, me doy por notificado de la Cesi�n de derechos de las operaciones descritas en esta p�gina y para los efectos legales conducentes.<br/>'+
    'Por otra parte, a partir del 17 de octubre de 2018 la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que s� ha emitido o emitir� a su CLIENTE o DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.<br/><br/>'+
    'NOTA:  las columnas un subIndice 1 son de Datos Documento Inicial   y subIndice 2 son de Datos Documento Final'  ];


	var columnas;
	Ext.onReady(function() {
	var formulario = new Array();
	var publicaDoctosFinanciables = ''; //F009-2015
	//--------------------------------- HANDLERS -------------------------------

	function procesaPublicaDocumentosFinanciables(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
				/***** Si tipo de credito es modalidad 2,e muestra el combo Tipo de pago *****/
				publicaDoctosFinanciables = jsonValoresIniciales.publicaDoctosFinanciables;
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

/*
	//GENERAR ARCHIVO X PAGINA PDF
	var procesarSuccessFailureXPaginaPDF =  function(opts, success, response) {
		var btnXPaginaPDF = Ext.getCmp('btnXPaginaPDF');
			btnXPaginaPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarXPDF = Ext.getCmp('btnBajarXPDF');
			btnBajarXPDF.show();
			btnBajarXPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarXPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnXPaginaPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailureTODOPDF =  function(opts, success, response) {
		var btnImprimirTPDF = Ext.getCmp('btnImprimirTPDF');
		btnImprimirTPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarTPDF = Ext.getCmp('btnBajarTPDF');
			btnBajarTPDF.show();
			btnBajarTPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarTPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnImprimirTPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}		//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailureGenerarArchivoCSV =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureTODOPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnImprimirTPDF').enable();
		Ext.getCmp('btnImprimirTPDF').setIconClass('icoPdf');
	}

	var procesarSuccessFailureGenerarArchivoCSV =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		var responsable = jsonData.RESPONSABLE;
		var tiposcredito = jsonData.TIPOSCREDITO;
		var cgtipoconversion = jsonData.CGTIPOCONVERSION;
		var icEpo = jsonData.icEpo;
		var tipoCreditoXepo =jsonData.tipoCreditoXepo;
		var tipoPago = jsonData.tipoPago;
		var fp = Ext.getCmp('forma');
			fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}

			//var fpC = Ext.getCmp('forma1');
			var fpO = Ext.getCmp('forma2');
			//forma1
			if(tipoPago == "N") {
				fpO.hide();
			}else{
				fpO.show();
			}
			var cm = grid.getColumnModel();
			var el = grid.getGridEl();	
			if(cgtipoconversion!=''){
			//	grid.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESCUENTO'), false);
			//	grid.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
			//	grid.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
			//	grid.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESCUENTO'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
			}
			if(responsable=='D'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('MONTO_CREDITO'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('IG_PLAZO_CREDITO'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('DF_FECHA_VENC_CREDITO'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_INT'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('VALOR_TASA'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INTERES'), false);
				/*
				grid.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COBRO_INTERES'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('RESPONSABLE'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOSCREDITO'), false);
				*/
			}
			if(tipoCreditoXepo=='F'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_NUMERO_CUENTA'), false);
			}
			if(publicaDoctosFinanciables == 'S'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('IG_TIPO_PAGO'), false);
			} else{
				grid.getColumnModel().setHidden(cm.findColumnIndex('IG_TIPO_PAGO'), true);
			}
			//var btnXPaginaPDF = Ext.getCmp('btnXPaginaPDF');
			//var btnBajarXPDF = Ext.getCmp('btnBajarXPDF');
			var btnImprimirTPDF = Ext.getCmp('btnImprimirTPDF');
			//var btnBajarTPDF = Ext.getCmp('btnBajarTPDF');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			//var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			var ctexto3 = Ext.getCmp('forma3');
			if(store.getTotalCount() > 0) {	
					resumenTotalesData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							ic_epo: Ext.getCmp('cmbEPO').getValue(),
							ic_if: Ext.getCmp('cmbIF').getValue(),
							Txtfchini: Ext.getCmp('Txtfchini1').getValue(),
							Txtffin: Ext.getCmp('Txtffin1').getValue()
						})
					});
					ctexto3.show();
					btnGenerarArchivo.enable();
					btnImprimirTPDF.enable();
					/*if(!btnBajarXPDF.isVisible()) {
						btnXPaginaPDF.enable();
					} else {
						btnXPaginaPDF.disable();
					}
					if(!btnBajarTPDF.isVisible()) {
						btnImprimirTPDF.enable();
					} else {
						//btnImprimirTPDF.disable();
					}
					if(!btnBajarCSV.isVisible()) {
						btnGenerarArchivo.enable();
					} else {
						btnGenerarArchivo.disable();
					}*/
				
					Ext.getCmp('gridTotales').show();
					el.unmask();
				} else {		
					btnGenerarArchivo.disable();
					btnImprimirTPDF.disable();
					//btnXPaginaPDF.disable();
					//btnXPaginaPDF.disable();					
					ctexto3.hide();
					Ext.getCmp('gridTotales').hide();
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
		//-------------------------------- STORES -----------------------------------

	var catalogoEPOData = new Ext.data.JsonStore({
			id: 'catalogoEPODataStore',
			root : 'registros',
			fields : ['clave', 'descripcion', 'loadMsg'],
			url : '24consulta07ext.data.jsp',
			baseParams: {
				informacion: 'CatalogoEPO'
			},
			totalProperty : 'total',
			autoLoad: false,		
			listeners: {
				exception: NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
	});
			
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta07ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo		
		}
	});
	//CONSULTA PARA LOS DEL GRID GENERAL
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta07ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'DIF'},
			{name: 'EPO'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO', type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESCUENTO', type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MODO_PLAZO'},
			{name: 'IG_TIPO_PAGO'},
			{name: 'ESTATUS'},
			{name: 'IC_ESTATUS_DOCTO'},
			{name: 'IC_DOCUMENTO'},
			{name: 'MONTO_CREDITO'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'DF_FECHA_VENC_CREDITO'},
			{name: 'REFERENCIA_INT'},
			{name: 'VALOR_TASA'},
			{name: 'MONTO_INTERES'},
			{name: 'TIPO_COBRO_INTERES'},
			{name: 'RESPONSABLE'},
			{name: 'TIPOSCREDITO'},
			{name: 'CGTIPOCONVERSION'},
			{name: 'CG_NUMERO_CUENTA'},
			{name: 'POR_AFRO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'CG_DESCRIPCION_BINS'},
			{name: 'IC_ORDEN_ENVIADO'},
			{name: 'IC_OPERACION_CCACUSE'},
			{name: 'CODIGO_AUTORIZADO'},
			{name: 'NUMERO_TC'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
					Ext.apply(options.params, {
						ic_epo: formulario.ic_epo,
						ic_if: formulario.ic_if,
						Txtfchini: formulario.Txtfchini,
						Txtffin: formulario.Txtffin,
						numeroOrden: formulario.numeroOrden
					});
			}},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta07ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'MONEDA', mapping: 'col1'},
			{name: 'MONTO', type: 'float', mapping: 'col3'},
			{name: 'MONTO_DESCUENTO', type: 'float', mapping: 'col4'},
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'col5'},
			{name: 'MONTO_CREDITO', type: 'float', mapping: 'col6'},
			{name: 'MONTO_INTERES', type: 'float', mapping: 'col7'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'cmbEPO',
			fieldLabel: 'EPO',
			mode: 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_epo',
			emptyText: 'Seleccione...',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPOData,
			tpl:  '<tpl for=".">' +
					'<tpl if="!Ext.isEmpty(loadMsg)">'+
					'<div class="loading-indicator">{loadMsg}</div>'+
					'</tpl>'+
					'<tpl if="Ext.isEmpty(loadMsg)">'+
					'<div class="x-combo-list-item">' +
					'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
					'</div></tpl></tpl>',
			listeners: {
				select: {
					fn: function(combo) {
						var ifComboCmp = Ext.getCmp('cmbIF');
						ifComboCmp.setValue('');
						ifComboCmp.setDisabled(false);
						ifComboCmp.store.load({
							params: {
								ic_epo: combo.getValue()
							}
						});
						//Fodea 09-2015
						Ext.Ajax.request({
							url: '24consulta07ext.data.jsp',
							params: {
								informacion: 'Publica_Documentos_Financiables',
								ic_epo: combo.getValue()
							},
							callback: procesaPublicaDocumentosFinanciables
						});
					}
				}
			}		
		},	
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'cmbIF',		
			fieldLabel: 'Intermediario finaciero',
			mode: 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_if',
			emptyText: 'Seleccione...',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoIFData,
			tpl:  '<tpl for=".">' +
					'<tpl if="!Ext.isEmpty(loadMsg)">'+
					'<div class="loading-indicator">{loadMsg}</div>'+
					'</tpl>'+
					'<tpl if="Ext.isEmpty(loadMsg)">'+
					'<div class="x-combo-list-item">' +
					'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
					'</div></tpl></tpl>'
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de operaci�n (dd/mm/aaaa)',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'Txtfchini',
					id: 'Txtfchini1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'Txtffin1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'Txtffin',
					id: 'Txtffin1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'Txtfchini1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'ID Orden ',
			combineErrors: false,
			msgTarget: 'side',
			minChars : 1,
			triggerAction : 'all',
			items: [				
				{
					xtype: 'textfield',
					name: 'numeroOrden',
					id: 'tfNumeroOrden',
					allowBlank: true,
					maxLength: 30,
					width:150 ,
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',			
					valueField: 'clave',
					hiddenName : 'numeroOrden',
					forceSelection : true,
					msgTarget: 'side'// para que se muestre el circulito de error
				}
			]	
		}
	];
		var gridTotales ={
			xtype: 'grid',
			store: resumenTotalesData,
			id: 'gridTotales',
			hidden: true,
			columns: [
				{
					header: 'Moneda',
					dataIndex: 'MONEDA',
					width: 200,
					align: 'left'
				},
				{
					header: '<sup>1</sup>Monto',
					dataIndex: 'MONTO_VALUADO',
					width: 150,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},
				{
					header: '<sup>1</sup>Monto con descuento',
					dataIndex: 'MONTO_DESCUENTO',
					width: 150,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},						
				{
					header: '<sup>1</sup>Monto Credito',
					dataIndex: 'MONTO_CREDITO',
					width: 150,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},
				{
					header: '<sup>2</sup>Monto tasa de inter�s',
					dataIndex: 'MONTO_INTERES',
					width: 150,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				}
				
			],
			height: 100,
			width: 943,
			title: '',
			frame: false
		};
			// create the Grid GENERAL
		var grid = new Ext.grid.GridPanel({
			store: consultaData,
			hidden: true,
			margins: '20 0 0 0',
			clicksToEdit: 1,				
			columns: [
			{
					id:'DIF',
					header: '<sup>1</sup>IF',
					tooltip: 'IF',
					dataIndex: 'DIF',
					sortable: true,
					resizable: true	,
					width: 130,
					hidden: false,
					align: 'left'
				},
				{
					id:'EPO',
					header: '<sup>1</sup>EPO',
					tooltip: 'EPO',
					dataIndex: 'EPO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'left'
					},			
					{
					id:'IG_NUMERO_DOCTO',
					header: '<sup>1</sup>N�mero de documento inicial',
					tooltip: 'N�mero de documento inicial',
					dataIndex: 'IG_NUMERO_DOCTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				}	,
				{
					id:'CC_ACUSE',
					header: '<sup>1</sup>Num. acuse carga',
					tooltip: 'Num. acuse carga',
					dataIndex: 'CC_ACUSE',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'DF_FECHA_EMISION',
					header: '<sup>1</sup>Fecha de emisi�n',
					tooltip: 'Fecha de emisi�n',
					dataIndex: 'DF_FECHA_EMISION',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{		
					id:'DF_FECHA_PUBLICACION',
					header: '<sup>1</sup>Fecha de publicaci�n',
					tooltip: 'Fecha de publicaci�n',
					dataIndex: 'DF_FECHA_PUBLICACION',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{		
					id:'DF_FECHA_VENC',
					header: '<sup>1</sup>Fecha vencimiento',
					tooltip: 'Fecha vencimiento',
					dataIndex: 'DF_FECHA_VENC',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{		
					id:'IG_PLAZO_DOCTO',
					header: '<sup>1</sup>Plazo docto',
					tooltip: 'Plazo docto',
					dataIndex: 'IG_PLAZO_DOCTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'MONEDA',
					header: '<sup>1</sup>Moneda',
					tooltip: 'Moneda',
					dataIndex: 'MONEDA',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'FN_MONTO',
					header: '<sup>1</sup>Monto',
					tooltip: 'Monto',
					dataIndex: 'FN_MONTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},
				{
					header : '<sup>1</sup> % de Descuento Aforo ', 
					tooltip: '% de Descuento Aforo ',
					dataIndex : 'POR_AFRO',
					sortable : true,	
					width : 130,	
					hidden: false, 
					align: 'center',
					renderer: Ext.util.Format.numberRenderer('0.00%')
				}
				,{
					header : '<sup>1</sup>Monto a Descontar ', 
					tooltip: 'Monto a Descontar',
					dataIndex : 'MONTO_DESCONTAR',
					sortable : true,	
					width : 120,	
					hidden: false, 
					align: 'right',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						var  valor  =  registro.get('FN_MONTO')*(registro.get('POR_AFRO')  /100);
						return Ext.util.Format.number(valor,'$0,0.00')
					}
				},
				{		
					id:'IG_PLAZO_DESCUENTO',
					header: '<sup>1</sup>Plazo para descuento',
					tooltip: 'Plazo para descuento',
					dataIndex: 'IG_PLAZO_DESCUENTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'FN_PORC_DESCUENTO',
					header: '<sup>1</sup> % de descuento',
					tooltip: '% de descuento',
					dataIndex: 'FN_PORC_DESCUENTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer: Ext.util.Format.numberRenderer('0.0000%')
				},
				{		
					id:'MONTO_DESCUENTO',
					header: '<sup>1</sup> Monto con descuento',
					tooltip: 'Monto con descuento',
					dataIndex: 'MONTO_DESCUENTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},			
				{		
					id:'TIPO_CONVERSION',
					header: '<sup>1</sup>Tipo conv',
					tooltip: 'Tipo conv',
					dataIndex: 'TIPO_CONVERSION',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},
				{		
					id:'TIPO_CAMBIO',
					header: '<sup>1</sup>Tipo Cambio',
					tooltip: 'Tipo Cambio',
					dataIndex: 'TIPO_CAMBIO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},
				{		
					id:'FN_MONTO',
					header: '<sup>1</sup>Monto Valuado',
					tooltip: 'Monto Valuado',
					dataIndex: 'FN_MONTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},
				{		
					id:'MODO_PLAZO',
					header: '<sup>1</sup>Modalidad de plazo',
					tooltip: 'Modalidad de plazo',
					dataIndex: 'MODO_PLAZO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{
					id:'IG_TIPO_PAGO',
					header: 'Tipo de pago',
					tooltip: 'Tipo de pago',
					dataIndex: 'IG_TIPO_PAGO',
					sortable: true,
					width: 180,
					resizable: true,
					align: 'center',
					renderer: function (value, metaData, registro, rowIndex, colIndex, store){
						if (registro.get('IG_TIPO_PAGO') == '1')	{
							value = 'Financiamiento con intereses';
						} else if (registro.get('IG_TIPO_PAGO') == '2')	{
							value = 'Meses sin intereses';
						} else{
							value = '';
						}
						return value;
					}
				},
				{
					id:'ESTATUS',
					header: '<sup>1</sup>Estatus',
					tooltip: 'Estatus',
					dataIndex: 'ESTATUS',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'CG_NUMERO_CUENTA',
					header: '<sup>1</sup>Cuenta Bancaria Pyme',
					tooltip: 'Cuenta Bancaria Pyme',
					dataIndex: 'CG_NUMERO_CUENTA',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},
				{		
					id:'IC_DOCUMENTO',
					header: '<sup>2</sup>N�mero de documento final',
					tooltip: 'N�mero de documento final',
					dataIndex: 'IC_DOCUMENTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},
				{		
					id:'MONTO_CREDITO',
					header: '<sup>2</sup>Monto',
					tooltip: 'Monto',
					dataIndex: 'MONTO_CREDITO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},
				{		
					id:'IG_PLAZO_CREDITO',
					header: '<sup>2</sup>Plazo',
					tooltip: 'Plazo',
					dataIndex: 'IG_PLAZO_CREDITO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
							value = "N/A";
						}
						return value;
					}
				},
				{		
					id:'DF_FECHA_VENC_CREDITO',
					header: '<sup>2</sup>Fecha de vencimiento',
					tooltip: 'Fecha de vencimiento',
					dataIndex: 'DF_FECHA_VENC_CREDITO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center',
					//renderer: Ext.util.Format.dateRenderer('d/m/Y')
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
							return "N/A";
						}else{
							return value;
						}
					}
				},
				{		
					id:'DF_FECHA_PUBLICACION',
					header: '<sup>2</sup>Fecha de operaci�n',
					tooltip: 'Fecha de operaci�n',
					dataIndex: 'DF_FECHA_PUBLICACION',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center',
					renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{		
					id:'REFERENCIA_INT',
					header: '<sup>2</sup>Referencia tasa de inter�s',
					tooltip: 'Referencia tasa de inter�s',
					dataIndex: 'REFERENCIA_INT',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
							value = "N/A";
						}
						return value;
					}
				},
				{		
					id:'VALOR_TASA',
					header: '<sup>2</sup>Valor tasa de inter�s',
					tooltip: 'Valor tasa de inter�s',
					dataIndex: 'VALOR_TASA',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
							return "N/A";
						}else{
						return Ext.util.Format.number(value,'0.00%');
						}
					}
				},
				{		
					id:'MONTO_INTERES',
					header: '<sup>2</sup>Monto tasa de inter�s',
					tooltip: 'Monto tasa de inter�s',
					dataIndex: 'MONTO_INTERES',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'right',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')=="32"  ){
							value = "N/A";
						}else{
							value = Ext.util.Format.number(value,'$0,0.00');
						}
						return value;
					}
				},
				{		
					id:'TIPO_COBRO_INTERES',
					header: '<sup>2</sup>Tipo de cobro inter�s',
					tooltip: 'Tipo de cobro inter�s',
					dataIndex: 'TIPO_COBRO_INTERES',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},
				{		
					header: "Nombre del Producto",
					tooltip: "Nombre del Producto",
					dataIndex: 'CG_DESCRIPCION_BINS',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32"  ){
							
							return "N/A";
						}else{
							return value;
						}
					}
				},
				{		
					header: 'ID Orden enviado',
					tooltip: 'ID Orden enviado',
					dataIndex: 'IC_ORDEN_ENVIADO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32"  ){
							
							return "N/A";
						}else{
							return value;
						}
					}
				},
				{		
					header: 'Respuesta de Operaci�n',
					tooltip: 'Respuesta de Operaci�n',
					dataIndex: 'IC_OPERACION_CCACUSE',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32"  ){
							
							return "N/A";
						}else{
							return value;
						}
					}
				},
				{		
					header: 'C�digo Autorizaci�n',
					tooltip: 'C�digo Autorizaci�n',
					dataIndex: 'CODIGO_AUTORIZADO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32"  ){
							
							return "N/A";
						}else{
							return value;
						}
					}
				},
				{		
					header: 'N�mero Tarjeta de Cr�dito',
					tooltip: 'N�mero Tarjeta de Cr�dito',
					dataIndex: 'NUMERO_TC',
					sortable: true,
					width: 200,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS_DOCTO')!="32"  ){
							return "N/A";
						}else{
							return value==""?"":"XXXX-XXXX-XXXX-"+value;
						}
					}
				}				
			],		
			displayInfo: true,		
			emptyMsg: "No hay registros.",
			loadMask: true,
			clicksToEdit: 1, 
			margins: '20 0 0 0',
			stripeRows: true,
			height: 400,
			width: 943,
			align: 'center',
			frame: false,		
			bbar: {
				xtype: 'paging',
				pageSize: 15,
				buttonAlign: 'left',
				id: 'barraPaginacion',
				displayInfo: true,
				store: consultaData,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No hay registros.",
				items: [
					/*{
						xtype: 'button',
						text: 'Imprimir x Pagina PDF',
						id: 'btnXPaginaPDF',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
							Ext.Ajax.request({
								url: '24consulta07Arext.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoXPDF',		
									operacion: 'Generar', //Generar datos para la consulta									
									start: cmpBarraPaginacion.cursor,
									limit: cmpBarraPaginacion.pageSize								
								}),
								callback: procesarSuccessFailureXPaginaPDF
							});
						}
					},
					{
						xtype: 'button',
						text: 'Bajar X Pagina PDF',
						id: 'btnBajarXPDF',
						hidden: true
					},*/
					'->','-',
					{
						xtype: 'button',
						text:    'Generar Todo',
						tooltip: 'Imprime los registros en formato PDF.',
						iconCls: 'icoPdf',
						id: 'btnImprimirTPDF',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url: '24consulta07ext.data.jsp',//24consulta07Arext.jsp
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoTPDF',
									start: 0,
									limit: 15,
									publicaDoctosFinanciables: publicaDoctosFinanciables
								}),
								callback: procesarSuccessFailureTODOPDF
							});
						}
					},/*
					{
						xtype: 'button',
						text: 'Bajar Todo PDF',
						id: 'btnBajarTPDF',
						hidden: true
					},*/
					'-',
					{
						xtype: 'button',
						text: 'Generar Archivo',
						tooltip: 'Imprime los registros en formato CSV.',
						iconCls: 'icoXls',
						id: 'btnGenerarArchivo',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url: '24consulta07Arext.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoCSV',
									start: 0,
									limit: 15,
									publicaDoctosFinanciables: publicaDoctosFinanciables
								}),
								callback: procesarSuccessFailureGenerarArchivoCSV
							});
						}
					},/*
					{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV',
						hidden: true
					},*/
					'-'
				]
			}
	
		});
		
	//-------------------------------- PRINCIPAL -----------------------------------
	
		var fp = new Ext.form.FormPanel({
			id: 'forma',
			width: 500,
			title: 'Criterios de B�squeda',
			frame: true,
			collapsible: true,
			titleCollapse: false,
			style: 'margin:0 auto;',
			bodyStyle: 'padding: 6px',
			labelWidth: 170,
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			items: elementosForma,			
			monitorValid: true,
			buttons: [
				{
					text: 'Buscar',
					iconCls: 'icoBuscar',
					formBind: true,
					handler: function(boton, evento) {
					
					var no_epo = Ext.getCmp("cmbEPO");
					if (Ext.isEmpty(no_epo.getValue()) ) {
							no_epo.markInvalid('Por favor, especifique la Epo');
							return;
					}		
					
					var Txtfchini2 = Ext.getCmp("Txtfchini1");
					var Txtffin2 = Ext.getCmp("Txtffin1");
					if (!Ext.isEmpty(Txtfchini2.getValue()) && Ext.isEmpty(Txtffin2.getValue())    
						||  Ext.isEmpty(Txtfchini2.getValue()) && !Ext.isEmpty(Txtffin2.getValue())    ) {
						Txtfchini2.markInvalid('Debe capturar ambas fechas de operaci�n o dejarlas en blanco');
						Txtffin2.markInvalid('Debe capturar ambas fechas de operaci�n o dejarlas en blanco');
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');
					var ventana = Ext.getCmp('winTotales');
					if (ventana) {
						ventana.destroy();
					}
					formulario = fp.getForm().getValues();
					consultaData.load({
							params: Ext.apply({
								operacion: 'Generar', 
								start: 0,
								limit: 15
							})
						});
						
						//Ext.getCmp('btnXPaginaPDF').disable();
						//Ext.getCmp('btnBajarXPDF').hide();
						Ext.getCmp('btnImprimirTPDF').disable();
						//Ext.getCmp('btnBajarTPDF').hide();
						Ext.getCmp('btnGenerarArchivo').disable();
						//Ext.getCmp('btnBajarCSV').hide();
						
					}
				},
				{
					text: 'Limpiar',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '24consulta07ext.jsp';
					}
				}
			]
		});
		
		var ctexto3 = new Ext.form.FormPanel({
		id: 'forma3',
		width: 943,
		frame: true,
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: texto3.join('')			
	});
		
		
		/*var fpC = new Ext.form.FormPanel({		
			id: 'forma1',
			width: 943,			
			frame: true,		
			titleCollapse: false,
			hidden: true,
			bodyStyle: 'padding: 6px',			
			defaultType: 'textfield',
			html: cadEpoCemex.join('')			
		});*/
		
	
		var fpO = new Ext.form.FormPanel({		
			id: 'forma2',
			width: 943,			
			frame: true,		
			titleCollapse: false,
			hidden: true,
			bodyStyle: 'padding: 6px',			
			defaultType: 'textfield',
			html: cadEpo.join('')			
		});
		
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			applyTo: 'areaContenido',
			width: 949,
			items: [
				fp,
				//NE.util.getEspaciador(20),
				//fpC,
				NE.util.getEspaciador(20),
				fpO,
				NE.util.getEspaciador(20),
				grid,
				gridTotales,
				NE.util.getEspaciador(20),
				ctexto3
			]
		});
	
	//-------------------------------- ----------------- -----------------------------------
	
		catalogoEPOData.load();
		catalogoIFData.load();

	});