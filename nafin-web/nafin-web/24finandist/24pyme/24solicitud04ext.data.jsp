<%@ page contentType="application/json;charset=UTF-8" 
			import="   java.util.*,
				netropology.utilerias.*,
				com.netro.distribuidores.ConsMonitorDist,
				com.netro.anticipos.*,
				com.netro.model.catalogos.CatalogoEPODistribuidores,
				com.netro.model.catalogos.CatalogoIFDistribuidores,
				com.netro.model.catalogos.CatalogoMoneda,   
				com.netro.model.catalogos.CatalogoSimple,
				net.sf.json.JSONObject"
			errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/24finandist/24secsession_extjs.jspf"%>
<% 

String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String claveEpo		=	(request.getParameter("claveEpo")!=null)?request.getParameter("claveEpo"):"";
String infoRegresar	=	"" ;

LineaCredito BeanLineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

if (informacion.equals("CatalogoEPO")) {
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClavePyme(iNoCliente);
	cat.setTipoCredito("C");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
} else if (informacion.equals("CatalogoIF")  ) {
	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social");
		if(claveEpo != null && !claveEpo.equals("")){
			cat.setClaveEpo(claveEpo);
		}	
		infoRegresar = cat.getJSONElementos();
	
} else if (informacion.equals("CatalogoEstatus")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_linea");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_linea");
	cat.setValoresCondicionIn("1,2,3,4,5,6", String.class);
	cat.setOrden("ic_estatus_linea");
	infoRegresar = cat.getJSONElementos();
	
} else if (informacion.equals("CatalogoMoneda")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setValoresCondicionIn("1,54", String.class);
	cat.setOrden("cd_nombre");
	infoRegresar = cat.getJSONElementos();

} else if (informacion.equals("Consulta") || informacion.equals("ArchivoPDF")){

	String fechaHoy		= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String operacion		= (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	String cmboEpo			= (request.getParameter("claveEpo")==null)?"":request.getParameter("claveEpo");		
	String cmboTSolic		= (request.getParameter("cmbTSolic")==null)?"":request.getParameter("cmbTSolic");
	String cmboestatus	= (request.getParameter("cmboestatus")==null)?"":request.getParameter("cmboestatus");
	String cmboMoneda		= (request.getParameter("cmboMoneda")==null)?"":request.getParameter("cmboMoneda");
	String cmboIF			= (request.getParameter("cmboIF")==null)?"":request.getParameter("cmboIF");
	String fecha_sol		= (request.getParameter("fecha_sol")==null)?fechaHoy:request.getParameter("fecha_sol");
	String cmboProducto	= "4";
	
	ConsMonitorDist paginador = new ConsMonitorDist();
	paginador.setIc_pyme(iNoCliente);
	paginador.setFecha_sol(fecha_sol);
	paginador.setIc_producto_nafin(cmboProducto);
	paginador.setIc_epo(claveEpo);
	paginador.setTipoSolic(cmboTSolic);
	paginador.setEestatus(cmboestatus);
	paginador.setIc_moneda(cmboMoneda);
	paginador.setIc_if(cmboIF);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
	Registros reg = queryHelper.doSearch();
	
	if (informacion.equals("Consulta")){
		try {
			infoRegresar = "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+"}";
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	} else if (informacion.equals("ArchivoPDF")){
		JSONObject jsonObj = new JSONObject();
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			
		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();
	}
}

%>
<%=infoRegresar%>
