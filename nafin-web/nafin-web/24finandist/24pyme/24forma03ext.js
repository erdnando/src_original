Ext.onReady(function(){
 var operaContrato = Ext.getDom('operaContrato').value;
 
   var leyenda = 'Autorizo expresamente que autom�ticamente sea(n) sustituido(s) el(los) DOCUMENTO(S) INICIAL(ES) que  sean publicados en la CADENA PRODUCTIVA, por el(los) DOCUMENTO(S) FINAL(ES) y estos �ltimos sean descontados a favor de la EMPRESA DE PRIMER ORDEN.';
        
        if(operaContrato == "S"){
            leyenda = 'Autorizo expresamente que autom�ticamente sea(n) sustituido(s) el(los) DOCUMENTO(S) INICIAL(ES) que sean publicados en la CADENA PRODUCTIVA por el(los) DOCUMENTO(S) FINAL(ES) y estos �ltimos sean financiados con el fin de pagar a la EMPRESA DE PRIMER ORDEN.';
        }
 
 
//-----------------------------------HANDLERS-----------------------------------
	var procesarSuccessFailureActualizarDatos = function (opts, success, response) {
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('Mensaje informativo',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){   
				location.href="/nafin/24finandist/24pyme/24forma03ext.jsp";
			});
		} else {
			Ext.Msg.alert('Mensaje de Error','�Error al actualizar los datos!',function(btn){  
					location.href="/nafin/24finandist/24pyme/24forma03ext.jsp";	 
			});
		}
	}
	var procesarSuccessNEclaveCesion = function(opts,success,response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if(Ext.util.JSON.decode(response.responseText).resultValidaCesion=="S"||Ext.util.JSON.decode(response.responseText).resultValidaCesion=="G"){
				var StoreGeneral = consultaData;
				var radioAutorizacion1 = Ext.getCmp('radioAutorizacion1');
				var radioAutorizacion2 = Ext.getCmp('radioAutorizacion2');
				var rad_autoriza = "";
				if(radioAutorizacion1.getValue()==true){
					rad_autoriza = "S"
				} else if(radioAutorizacion2.getValue()==true){
					rad_autoriza = "N"
				}	
				var moneda_n = new Array();
				var moneda_d = new Array();
				StoreGeneral.each(function(registro){
					var epo = registro.get('EPO');
					if(registro.get('COLUMNA_MONEDA')!=''){
						registro.data.MONEDA_N = 'N|' + epo;
					}
					if(registro.get('COLUMNA_MONEDA')==''){
						registro.data.MONEDA_N = 'XN|' + epo;
					}
					if(registro.get('COLUMNA_DOLAR')!=''){
						registro.data.MONEDA_D = 'D|' + epo;
					}
					if(registro.get('COLUMNA_DOLAR')==''){
						registro.data.MONEDA_D = 'XD|' + epo;
					}
					//Guardamos los cambios en las respectivas variables
					moneda_n.push(registro.get('MONEDA_N'));
					moneda_d.push(registro.get('MONEDA_D'));					
				});
					//Codificamos los cambios en JSON para podernos enviar   
					moneda_n = Ext.util.JSON.encode(moneda_n);
					moneda_d = Ext.util.JSON.encode(moneda_d);
  
					Ext.Ajax.request({  
						url: '24forma03ext.data.jsp',    
						params: {  
										informacion: 'Guardar',  
										moneda_n: moneda_n,
										moneda_d: moneda_d,
										rad_autoriza: rad_autoriza
									},
						callback: procesarSuccessFailureActualizarDatos
					});
			} else{
				Ext.Msg.alert('Mensaje de Error',"La contrase�a es incorrecta",function(btn){  
					return;	 
				});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaData = function(store,arrRegistros,opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var record = store.getAt(0);
		var radio1 = Ext.getCmp('radioAutorizacion1');
		var radio2 = Ext.getCmp('radioAutorizacion2');
		if (arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				if(record.get('RS_DSCTO_AUTO')=="S"){
					radio1.setValue(true);
				} else if(record.get('RS_DSCTO_AUTO')=="N"){
					radio2.setValue(true);
				}
				store.each(function(registro){
					if(registro.get('DOLAR')==''){
						registro.data.COLUMNA_DOLAR = '';					
					}
				});
			}
			el.unmask();
		} else {
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
	}
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
//------------------------------------STORES------------------------------------
	var consultaData = new Ext.data.JsonStore({
			root: 'registros',
			url: '24forma03ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields:[
						{name: 'EPO'},
						{name: 'NOMBRE_EPO'},
						{name: 'RS_DSCTO_AUTO'},
						{name: 'MONEDA'},
						{name: 'DOLAR'},
						{name: 'MONEDA_N'},
						{name: 'MONEDA_D'},
						{name: 'COLUMNA_MONEDA',convert: NE.util.string2boolean},
						{name: 'COLUMNA_DOLAR',convert: NE.util.string2boolean}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,
				excepion: {
								fn: function(proxy,type,action,optionsRequest,response,args){
										NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
										procesarConsultaData(null,null,null);
				}
			}
		}
	});
//----------------------------------COMPONENTES---------------------------------
	var elementoMensaje = [
									{
										xtype: 'box',
										html: leyenda+'</br>'+
												'</br>Nota:</br>'+
												'</br>S�lo aplica para Modalidad Pronto Pago y Pronto Pago Fijo'+
												'</br>S�lo aplica para Intereses al Vencimiento',
										style: 'font-family:arial;font-size:13;text-align:justify;display:block;height:120px;',
										width:895
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Operar descuento autom�tico',
										labelWidth: 130,
										combineError: false,
										style: 'font-size:13;',
										msgTarget: 'side',
										anchor: '90%',
										items: [
													{
														xtype: 'radio',
														boxLabel: 'Si autorizo',
														name: 'rad_autoriza',
														id: 'radioAutorizacion1',
														inputValue:'S',
														anchor: '50%'
													},
													{
														xtype: 'radio',
														boxLabel: 'No Autorizo',
														checked: false,
														name: 'rad_autoriza',
														id: 'radioAutorizacion2',
														inputValue:'N',
														anchor: '50%'
													}
										]
									}
	];
	var fp = new Ext.FormPanel({
		id: 'forma',
		style: 'margin:0 auto;',
		title: '',
		hidden: false,
		frame: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		height:150,
		width: 900,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		items: elementoMensaje,
		monitorValid: false
	}); 
	var grid = new Ext.grid.EditorGridPanel({
			id: 'grid',
			title: '',
			style: 'margin:0 auto;',
			store: consultaData,
			hidden: false,
			clicksToEdit: 1,
			columns: [			
							{
								header: 'PARAMETRIZAR ESTA EPO',
								tooltip: 'Parametrizar esta EPO',
								dataIndex: 'NOMBRE_EPO',
								sortable: true,
								resiazable: true,
								width: 485/*,
								editor: {
									xtype: 'textfield'
								}*/
							},
							{
								xtype: 'checkcolumn',
								header: 'MONEDA NACIONAL',
								tooltip: 'Moneda Nacional',
								dataIndex: 'COLUMNA_MONEDA',
								sortable: 'true',
								resiazable: true,
								width: 200,
								align: 'center'
							},
							{
								xtype: 'checkcolumn',
								header: 'DOLARES',
								tooltip: 'Dolares',
								dataIndex: 'COLUMNA_DOLAR',
								sortable: true,
								resiazable: true,
								width: 200,
								align: 'center'
							}
						],
			stripeRows: true,
			loadMask: true,
			height: 400,
			width: 885,
			title: 'Datos Documento Inicial',
			frame: true,
			bbar: {
				pageSize: 15,
				buttonAlign: 'left',
				displayInfo: true,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No hay registros.",
				items: [
							'-',
							{
								text: 'Guardar',
								id: 'btnGuardar',
								formBind: true,
								handler: function(boton,evento){	
								//Valida que al menos se seleccione una EPO
									var contador = 0; 
									var storeGeneral = consultaData;
									storeGeneral.each(function(registro){
										if(registro.get('COLUMNA_MONEDA')==false&&registro.get('COLUMNA_DOLAR')==false){
											contador++;
										}
									});
									if(contador==storeGeneral.getTotalCount()){
										Ext.Msg.alert('Mensaje Informativo',"Debe seleccionar por lo menos una EPO",function(btn){});
										return;
									}
									//Valida que se haya seleccionado un tipo de autorizaci�n
									var radioAutorizacion1 = Ext.getCmp('radioAutorizacion1');
									var radioAutorizacion2 = Ext.getCmp('radioAutorizacion2');
									var modifiedRecords = consultaData.getModifiedRecords();
									if (modifiedRecords.length > 0){
										if(radioAutorizacion1.getValue()==false&&radioAutorizacion2.getValue()==false){
											Ext.Msg.alert('Mensaje Error','El parametro de autorizaci�n es obligatorio',function(btn){});
											return;
										}
										var ventana = Ext.getCmp('winCesionDerechos1');
										var objFormLogCesion = new NE.cesion.FormLoginCesion();
										if(ventana){
											ventana.show();
										}else{
											new Ext.Window({
												layout: 'fit',
												autoHeight:true,
												x:300,
												y:100,
												width: 260,
												resizable:false,
												modal: true,
												id: 'winCesionDerechos1',
												title: ' ',
												bodyStyle: 'text.align:center',
												items: [
													objFormLogCesion
												]
											}).show();
										}
										objFormLogCesion.setHandlerBtnAceptar(function(){
											Ext.Ajax.request({
												url: '/nafin/00utils/NEcesionDerechos.jsp',
												params: Ext.apply(objFormLogCesion.getForm().getValues(),{
													informacion: 'validaClaveCesionDerec',
													cesionAltaCve: 'N',
													metodo: 'utilerias'
												}),
												callback: procesarSuccessNEclaveCesion
											});
										});
								 } else{
										Ext.Msg.alert('Mensaje Informativo',"No ha modificado ning�n par�metro.",function(btn){});
										return;
									}
								}
							},
							'-',
							{
								xtype: 'button',
								text: 'Generar PDF',
								id: 'btnGenerarPDF',
								handler: function(boton, evento) {
									boton.disable();
									boton.setIconClass('loading-indicator');
									Ext.Ajax.request({
											url: '24forma03iext.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
											informacion: 'ArchivoPDF'
											}),
									callback: procesarSuccessFailureGenerarPDF
									});
								}
							},
							{
								xtype: 'button',
								text: 'Bajar PDF',
								id: 'btnBajarPDF',
								hidden: true
							},
							'-'
					]
			},
			listeners:{
				afteredit: function(obj) {
					alert("Antes de editar");
			}
					/*isCellEditable: function(colIndex, rowIndex) {
						alert('algo');
						var field = this.getColumnModel().getDataIndex(colIndex);
						if (field == 'COLUMNA_DOLAR') {
							var record = this.getStore().getAt(rowIndex);
							if (!record.get('DOLAR').getValue()) { //enable_edit is field in record
							 return false;//return false to deny editing
							}
						}
						 return grid.prototype.isCellEditable.call(this, colIndex, rowIndex);
					}
					fn: function(obj){
						alert('algo');
						if (obj.column == 2) {
							if (!obj.record.get('DOLAR').getValue()) { //enable_edit is field in record
								return false;//return false to deny editing
							}
						}
					}
				}*/
			}
	});
//----------------------------------PRINCIPAL-----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	});
consultaData.load();
});
