<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion					=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String cveEstatus					=	(request.getParameter("claveStatus")==null)?"":request.getParameter("claveStatus");
String tiposCredito					=	(request.getParameter("tiposCredito")==null)?"":request.getParameter("tiposCredito");
String cgTipoConversion	=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
String tipoCredito = "";

CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

if("".equals(tiposCredito)){
	 tipoCredito = cargaDocto.getTiposCredito(iNoCliente, tiposCredito);
}

StringBuffer condicion = new StringBuffer();
String infoRegresar  = "";

if (informacion.equals("valoresIniciales"))	{
	if("".equals(cgTipoConversion)){
		cgTipoConversion = cargaDocto.getTipoConversion(iNoCliente);
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("cgTipoConversion",cgTipoConversion);
	jsonObj.put("tiposCredito", tipoCredito);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoEstatusDist")) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_docto");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_docto");
	cat.setValoresCondicionIn("24,1,2,3,4,5,9,11,22,32", Integer.class);//Modificado +(24)
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("consultaGridVenceSinOpera"))	{

	if(iNoCliente != null && !iNoCliente.equals("")) {
		RepEstatusPyme repVence = new RepEstatusPyme();
		repVence.setProducto(4);
		repVence.setIcePyme(iNoCliente);
		repVence.setCveEstatus("9");
		repVence.setQrysentencia();
		Registros registros = repVence.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaGridNegociable"))	{
    
	if(iNoCliente != null && !iNoCliente.equals("")) {
		RepEstatusPyme repNego = new RepEstatusPyme();
		repNego.setProducto(4);
		repNego.setIcePyme(iNoCliente);
    if(cveEstatus.equals("32"))
      repNego.setCveEstatus("32");
		else
      repNego.setCveEstatus("2");
    repNego.setQrysentencia();
		Registros registros = repNego.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaGridSelecPyme"))	{

	if(iNoCliente != null && !iNoCliente.equals("")) {
		RepEstatusPyme repSelec = new RepEstatusPyme();
		repSelec.setProducto(4);
		repSelec.setIcePyme(iNoCliente);
		repSelec.setCveEstatus("3");
		repSelec.setTiposCredito(tipoCredito);
		repSelec.setQrysentencia();
		Registros registros = repSelec.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaGridOperada"))	{

	if(iNoCliente != null && !iNoCliente.equals("")) {
		RepEstatusPyme repOpera = new RepEstatusPyme();
		repOpera.setProducto(4);
		repOpera.setIcePyme(iNoCliente);
		repOpera.setCveEstatus("4");
		repOpera.setTiposCredito(tipoCredito);
		repOpera.setQrysentencia();
		Registros registros = repOpera.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaGridOperaPaga"))	{

	if(iNoCliente != null && !iNoCliente.equals("")) {
		RepEstatusPyme repOperaPaga = new RepEstatusPyme();
		repOperaPaga.setProducto(4);
		repOperaPaga.setIcePyme(iNoCliente);
		repOperaPaga.setCveEstatus("11");
		repOperaPaga.setTiposCredito(tipoCredito);
		repOperaPaga.setQrysentencia();
		Registros registros = repOperaPaga.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaGridOperaVence"))	{

	if(iNoCliente != null && !iNoCliente.equals("")) {
		RepEstatusPyme repOperaVence = new RepEstatusPyme();
		repOperaVence.setProducto(4);
		repOperaVence.setIcePyme(iNoCliente);
		repOperaVence.setCveEstatus("22");
		repOperaVence.setTiposCredito(tipoCredito);
		repOperaVence.setQrysentencia();
		Registros registros = repOperaVence.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaGridBaja"))	{

	if(iNoCliente != null && !iNoCliente.equals("")) {
		RepEstatusPyme repBaja = new RepEstatusPyme();
		repBaja.setProducto(4);
		repBaja.setIcePyme(iNoCliente);
		repBaja.setCveEstatus("5");
		repBaja.setQrysentencia();
		Registros registros = repBaja.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaGridNoNego"))	{

	if(iNoCliente != null && !iNoCliente.equals("")) {
		RepEstatusPyme repNoNego = new RepEstatusPyme();
		repNoNego.setProducto(4);
		repNoNego.setIcePyme(iNoCliente);
		repNoNego.setCveEstatus("1");
		repNoNego.setQrysentencia();
		Registros registros = repNoNego.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaProcesoAutorizacion"))	{

	if(iNoCliente != null && !iNoCliente.equals("")) {
		RepEstatusPyme repEnProcesoAutorizacionIF = new RepEstatusPyme();
		repEnProcesoAutorizacionIF.setProducto(4);
		repEnProcesoAutorizacionIF.setIcePyme(iNoCliente);
		repEnProcesoAutorizacionIF.setCveEstatus("24");
		repEnProcesoAutorizacionIF.setTiposCredito(tipoCredito);
		repEnProcesoAutorizacionIF.setQrysentencia();
		Registros registros = repEnProcesoAutorizacionIF.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}	else if (informacion.equals("obtenCambioDoctos"))	{
	String ic_docto = (request.getParameter("ic_docto")!=null)?request.getParameter("ic_docto"):"";
	if(ic_docto != null && !ic_docto.equals("")) {
		DetalleCambiosDoctos dataCambios = new DetalleCambiosDoctos();
		dataCambios.setIcDocumento(ic_docto);
		Registros registros = dataCambios.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
}

%>
<%=infoRegresar%>