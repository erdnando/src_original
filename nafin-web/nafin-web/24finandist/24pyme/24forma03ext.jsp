<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>                
                
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
try{
ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
String operaContrato = BeanParametros.obtieneOperaSinCesion(iNoEPO); //PARAMETRO NUEVO JAGE 14012017

%>



<html>
	<head>
		<title>.:: N@fin Electrónico :: Financiamiento a Distribuidores ::.</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<%@ include file="/extjs.jspf" %>
		<%@ include file="/01principal/menu.jspf"%>
		<script language="JavaScript" src="/nafin/00utils/NEcesionDerechos.js?<%=session.getId()%>"></script>
		<script type="text/javascript" src="24forma03ext.js?<%=session.getId()%>"></script>
	</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01pyme/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01pyme/menuLateralFlotante.jspf"%>
						<div id="areaContenido"></div>
			<!--div id='areaContenido' style="margin-left: 3px; margin-top: 3px"></div-->
					</div>
				</div>
				<%@ include file="/01principal/01pyme/pie.jspf"%>
<form id='formParametros' name="formParametros">
    <input type="hidden" id="operaContrato" name="operaContrato" value="<%=operaContrato%>"/>
</form>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
<%
}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	out.println(" Error: "+e);
}
%>