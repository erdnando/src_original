<%@ page contentType="application/json;charset=UTF-8" 
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.seguridadbean.*,
	com.netro.model.catalogos.*,
	com.netro.model.catalogos.CatalogoEPODistribuidores,	
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	com.netro.afiliacion.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp" 
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String fechaHoy		= "",  horaCarga="";
	try{
		fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		horaCarga = new java.text.SimpleDateFormat("HH:mm").format(new java.util.Date());
	}catch(Exception e){
		fechaHoy = "";
		horaCarga = "";
	}

String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String clave_pyme = (String)request.getSession().getAttribute("iNoCliente");
String ic_epo 		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String ic_moneda 	= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
String mto_descuento 	= (request.getParameter("mto_descuento")==null)?"":request.getParameter("mto_descuento");
String cc_acuse 	= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
String fn_monto_de 	= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
String fn_monto_a 	= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
String df_fecha_emision_de 	= (request.getParameter("df_fecha_emision_de")==null)?"":request.getParameter("df_fecha_emision_de");
String df_fecha_emision_a 	= (request.getParameter("df_fecha_emision_a")==null)?"":request.getParameter("df_fecha_emision_a");
String ig_numero_docto 	= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
String df_fecha_venc_de 	= (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
String df_fecha_venc_a 	= (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
String fecha_publicacion_de 	= (request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
String fecha_publicacion_a 	= (request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
String doctos_cambio 	= (request.getParameter("doctos_cambio")==null)?"":request.getParameter("doctos_cambio");
String modo_plazo 	= (request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
String nePyme 		= (String)session.getAttribute("strNePymeAsigna");
String nombrePyme	= (String)session.getAttribute("strNombrePymeAsigna");
String lineaTodos 	= (request.getParameter("lineaTodos")==null)?"":request.getParameter("lineaTodos");
String cmbTCredTodos 	= (request.getParameter("cmbTCredTodos")==null)?"":request.getParameter("cmbTCredTodos");
String cmbTipoPago = (request.getParameter("cmbTipoPago")==null)?"":request.getParameter("cmbTipoPago");

String OrderId = (request.getParameter("OrderId")==null)?"":request.getParameter("OrderId");
String TransactionId = (request.getParameter("TransactionId")==null)?"":request.getParameter("TransactionId");
String ResponseCode = (request.getParameter("ResponseCode")==null)?"":request.getParameter("ResponseCode");
String ResponseMessage = (request.getParameter("ResponseMessage")==null)?"":request.getParameter("ResponseMessage");
String AuthCode = (request.getParameter("AuthCode")==null)?"":request.getParameter("AuthCode");
String AuthDate = (request.getParameter("AuthDate")==null)?"":request.getParameter("AuthDate");
String Last4Digit = (request.getParameter("Last4Digit")==null)?"":request.getParameter("Last4Digit");


/*System.out.println(" ic_epo "+ic_epo);
System.out.println(" ic_moneda "+ ic_moneda);
System.out.println(" ig_numero_docto"+ ig_numero_docto);
System.out.println(" Fn_monto_de "+fn_monto_de);
System.out.println(" fn_monto_a  "+ fn_monto_a);
System.out.println(" cc_acuse "+cc_acuse);
System.out.println(" mto_descuento "+mto_descuento);
System.out.println(" df_fecha_emision_de " +df_fecha_emision_de);
System.out.println(" df_fecha_emision_a "+df_fecha_emision_a);
System.out.println(" ig_numero_docto"+ig_numero_docto);
System.out.println(" df_fecha_venc_de "+df_fecha_venc_de);
System.out.println(" df_fecha_venc_a "+df_fecha_venc_a);
System.out.println(" fecha_publicacion_de "+fecha_publicacion_de);
System.out.println(" fecha_publicacion_a "+fecha_publicacion_a);
System.out.println(" Doctos_cambio "+doctos_cambio);
System.out.println(" lineaTodos  "+lineaTodos);
*/

if(!doctos_cambio.equals("") ) doctos_cambio ="S";
if(!mto_descuento.equals("") ) mto_descuento ="S";
boolean regValido = true;
String infoRegresar = "";
String ses_ic_pyme = iNoCliente;

if(ic_epo.equals("")){ ic_epo = iNoEPO; } 

String hidNumTasas ="",  hidNumLineas =  "";
//if(fecha_publicacion_de.equals("")){ fecha_publicacion_de = fechaHoy; } 
//if(fecha_publicacion_a.equals("")){ fecha_publicacion_a = fechaHoy; } 

		
//INSTANCIACION DE EJB'S

AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);

CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

com.netro.seguridadbean.Seguridad seguridadEJB = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);

Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
	
//se obtiene el parametro de descuento Automatico
String 	DescuentoAutomatico = BeanParametro.DesAutomaticoEpo(ic_epo,"PUB_EPO_DESC_AUTOMATICO"); 


//******	Inicializacion de Catalogo 
if (informacion.equals("CatalogoEPO") ) {		
		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClavePyme(clave_pyme);
		cat.setValoresCondicionIn (ic_epo, Integer.class);
		infoRegresar = cat.getJSONElementos();	
		
} else  if (informacion.equals("CatalogoMoneda") ) {

		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setTabla("comcat_moneda");		
		catalogo.setOrden("ic_moneda");
		catalogo.setValoresCondicionIn("1,54", Integer.class);
		infoRegresar = catalogo.getJSONElementos();	


} else  if (informacion.equals("CatalogoModalidadPlazo") ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_tipo_financiamiento");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_tipo_financiamiento");
	catalogo.setOrden("ic_tipo_financiamiento");
	catalogo.setValoresCondicionIn("1,2,3,4", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
	
} else  if (informacion.equals("CatalogoLineaTodos") ) {

	JSONArray registros = new JSONArray();
	HashMap info = new HashMap();
	
	List combo = aceptPyme.getLineasCCC(ses_ic_pyme, "", ic_moneda ) ;

	for(int i=0; i<combo.size(); i++){
		List dato = (List)combo.get(i);
		String clave = dato.get(0).toString(); 
		String descripcion = dato.get(1).toString(); 	
		info = new HashMap();
		info.put("clave", clave);
		info.put("descripcion", descripcion);	
		registros.add(info);
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";


} else  if (informacion.equals("CatalogoIFBins") ) {

	JSONArray registros = new JSONArray();
	HashMap info = new HashMap();
	System.out.println("ic_epo ==== "+ic_epo);
	System.out.println("ic_moneda ==== "+ic_moneda);
	List lstBins = BeanParametro.getBinsIFxEpo(ic_epo, ic_moneda ) ;
	registros = JSONArray.fromObject(lstBins);
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
} else  if (informacion.equals("SeleccionEPO") ) {

	JSONObject 	resultado	= new JSONObject();
	HashMap	datosAuxiliar = new HashMap();
		
	String plazo = "", ic_if="";
	int numTasas=0;
	String auxIcIf = "", auxReferenciaTasa	= "", auxIcTasa	="", auxCgRelMat ="", auxFnPuntos	="", 
	auxValorTasa ="", auxIcMoneda	="", auxPlazoDias	="";
	
	String bloqueoPymeEpo =   afiliacion.getPyme_Epo_Bloqueo("4",  clave_pyme, ic_epo);  
  Vector vParamEpo = BeanParametro.getParamEpo(ic_epo);
  String csTipoPago = (String)vParamEpo.get(8);
  
  String sAfilComercio = BeanParametro.getParamXepo(ic_epo,"CG_AFILIADO_COMERCIO");
	String mesesSinInt = BeanParametro.getParamXepo(ic_epo,"CS_MESES_SIN_INTERESES");
  
	Vector vecColumnas =null;
	// OBTENEMOS LAS TASAS POR EPO E IF Y LAS METEMOS EN HIDDENS AUXILIARES
	Vector vecFilas2 = BeanTasas.esquemaParticular(ic_epo,4,"",iNoCliente,"C"); // obtiene las EPO e IF que tienen una LC vigente	
  int contTasas = 0;
	for (int j=0; j<vecFilas2.size(); j++) {
		Vector lovDatosTas = (Vector)vecFilas2.get(j);
		boolean esTasaGeneral = false;
		ic_if = lovDatosTas.get(0).toString();
		int totalTasasMN = 0;
		int totalTasasUSD = 0;
		String in = "";		
		Vector vecFilas1 = BeanTasas.ovgetTasasxPyme(4,iNoCliente,ic_if,"1, 54");
		
		// F05-2014
		Vector vecFilasPN  = null;
		
		Vector vecFilasP = BeanTasas.ovgetTasasPreferNego("4","P",ic_if,ic_epo,ic_moneda,iNoCliente, "","C" ); //Preferecial	
		Vector vecFilasN = BeanTasas.ovgetTasasPreferNego("4","N",ic_if,ic_epo,ic_moneda,iNoCliente, "","C" ); //Negociables 
		
		//System.out.println("vecFilasP-->" +vecFilasP.size());
		//System.out.println("vecFilasN-->" +vecFilasN.size());
		
		if(vecFilasP.size()>0 && vecFilasN.size()>0)  { 	vecFilasPN = vecFilasN; 	}
		if(vecFilasP.size()>0 && vecFilasN.size()==0)  { 	vecFilasPN = vecFilasP; 	}
		if(vecFilasP.size()==0 && vecFilasN.size()>0)  { 	vecFilasPN = vecFilasN; 	}
		
		//System.out.println("vecFilasPN-->" +vecFilasPN);
		//System.out.println("vecFilas1-->" +vecFilas1);  
		
		if(vecFilasPN !=null)  {
			if(vecFilasPN.size()>0)  {
				for(int i=0;i<vecFilasPN.size();i++){
					vecColumnas = (Vector)vecFilasPN.get(i);				
					auxIcIf = ic_if;
					auxReferenciaTasa =(String)vecColumnas.get(1)+" "+(String)vecColumnas.get(5)+" "+(String)vecColumnas.get(6);
					auxIcTasa =(String)vecColumnas.get(2);
					auxCgRelMat = (String)vecColumnas.get(5);
					auxFnPuntos =(String)vecColumnas.get(11);
					auxValorTasa = (String)vecColumnas.get(4);
					auxIcMoneda =(String)vecColumnas.get(13);
					auxPlazoDias =(String)vecColumnas.get(14);			
					numTasas++;				
					datosAuxiliar.put("auxIcIf"+contTasas,auxIcIf);
					datosAuxiliar.put("auxReferenciaTasa"+contTasas,auxReferenciaTasa);
					datosAuxiliar.put("auxIcTasa"+contTasas,auxIcTasa);
					datosAuxiliar.put("auxCgRelMat"+contTasas,auxCgRelMat);
					datosAuxiliar.put("auxFnPuntos"+contTasas,auxFnPuntos);
					datosAuxiliar.put("auxValorTasa"+contTasas,auxValorTasa);
					datosAuxiliar.put("auxIcMoneda"+contTasas,auxIcMoneda);
					datosAuxiliar.put("auxPlazoDias"+contTasas,auxPlazoDias);
					datosAuxiliar.put("auxPuntos"+contTasas,(String)vecColumnas.get(6));
          /*
          System.out.println("auxIcIf = "+contTasas+" - "+auxIcIf);
          System.out.println("auxReferenciaTasa = "+i+" - "+auxReferenciaTasa);
          System.out.println("auxIcTasa = "+contTasas+" - "+auxIcTasa);
          System.out.println("auxCgRelMat = "+contTasas+" - "+auxCgRelMat);
          System.out.println("auxFnPuntos = "+contTasas+" - "+auxFnPuntos);
          System.out.println("auxValorTasa = "+contTasas+" - "+auxValorTasa);
          System.out.println("auxIcMoneda = "+contTasas+" - "+auxIcMoneda);
          System.out.println("auxPlazoDias = "+contTasas+" - "+auxPlazoDias);
          System.out.println("numTasas ============== "+numTasas);
          */
          contTasas++;
				}		
			}
		}else  {			
		
			for( int i=0;i<vecFilas1.size();i++){
				vecColumnas = (Vector)vecFilas1.get(i);	
				auxIcIf = ic_if;
				auxReferenciaTasa	= (String)vecColumnas.get(1)+" "+(String)vecColumnas.get(5)+" "+(String)vecColumnas.get(6);
				auxIcTasa			= (String)vecColumnas.get(2);
				auxCgRelMat		=(String)vecColumnas.get(5);
				auxFnPuntos		=(String)vecColumnas.get(7);
				auxValorTasa	=(String)vecColumnas.get(4);
				auxIcMoneda		=(String)vecColumnas.get(9);
				auxPlazoDias	=(String)vecColumnas.get(10);				
				if("1".equals(vecColumnas.get(9).toString())){
					totalTasasMN ++;
				}	else if("54".equals(vecColumnas.get(9).toString())){
					totalTasasUSD ++;
				}
				numTasas++;
							
				datosAuxiliar.put("auxIcIf"+contTasas,auxIcIf);
				datosAuxiliar.put("auxReferenciaTasa"+contTasas,auxReferenciaTasa);
				datosAuxiliar.put("auxIcTasa"+contTasas,auxIcTasa);
				datosAuxiliar.put("auxCgRelMat"+contTasas,auxCgRelMat);
				datosAuxiliar.put("auxFnPuntos"+contTasas,auxFnPuntos);
				datosAuxiliar.put("auxValorTasa"+contTasas,auxValorTasa);
				datosAuxiliar.put("auxIcMoneda"+contTasas,auxIcMoneda);
				datosAuxiliar.put("auxPlazoDias"+contTasas,auxPlazoDias);	
				datosAuxiliar.put("auxPuntos"+contTasas,(String)vecColumnas.get(6));	
        /*
        System.out.println("auxIcIf = "+contTasas+" - "+auxIcIf);
        System.out.println("auxReferenciaTasa = "+contTasas+" - "+auxReferenciaTasa);
        System.out.println("auxIcTasa = "+contTasas+" - "+auxIcTasa);
        System.out.println("auxCgRelMat = "+contTasas+" - "+auxCgRelMat);
        System.out.println("auxFnPuntos = "+contTasas+" - "+auxFnPuntos);
        System.out.println("auxValorTasa = "+contTasas+" - "+auxValorTasa);
        System.out.println("auxIcMoneda = "+contTasas+" - "+auxIcMoneda);
        System.out.println("auxPlazoDias = "+contTasas+" - "+auxPlazoDias);
        */
				contTasas++;
			}	
			
			int conteo =vecFilas1.size()-1;
			
			if(totalTasasMN==0||totalTasasUSD==0){
				if(totalTasasMN==0)
					in = "1";
				if(totalTasasUSD==0){
					if(!"".equals(in))
						in += ",";
					in += "54";
				}				
				vecFilas1 = BeanTasas.ovgetTasas(4,in);			
				esTasaGeneral = true;
			}else{
				vecFilas1 = new Vector();
			}	
				
			for( int i=0;i<vecFilas1.size();i++){
				vecColumnas = (Vector)vecFilas1.get(i);
				auxIcIf = ic_if;
				auxReferenciaTasa =(String)vecColumnas.get(1)+" "+(String)vecColumnas.get(5)+" "+(String)vecColumnas.get(6);
				auxIcTasa =(String)vecColumnas.get(2);
				auxCgRelMat = (String)vecColumnas.get(5);
				auxFnPuntos	=(String)vecColumnas.get(7);
				auxValorTasa =(String)vecColumnas.get(4);
				auxIcMoneda  =(String)vecColumnas.get(10);
				auxPlazoDias =(String)vecColumnas.get(11);
				numTasas++;
				conteo++;
				datosAuxiliar.put("auxIcIf"+contTasas,auxIcIf);
				datosAuxiliar.put("auxReferenciaTasa"+contTasas,auxReferenciaTasa);
				datosAuxiliar.put("auxIcTasa"+contTasas,auxIcTasa);
				datosAuxiliar.put("auxCgRelMat"+contTasas,auxCgRelMat);
				datosAuxiliar.put("auxFnPuntos"+contTasas,auxFnPuntos);
				datosAuxiliar.put("auxValorTasa"+contTasas,auxValorTasa);
				datosAuxiliar.put("auxIcMoneda"+contTasas,auxIcMoneda);
				datosAuxiliar.put("auxPlazoDias"+contTasas,auxPlazoDias);	
				datosAuxiliar.put("auxPuntos"+contTasas,(String)vecColumnas.get(6));	
				/*
        System.out.println("auxIcIf = "+contTasas+" - "+auxIcIf);
        System.out.println("auxReferenciaTasa = "+contTasas+" - "+auxReferenciaTasa);
        System.out.println("auxIcTasa = "+contTasas+" - "+auxIcTasa);
        System.out.println("auxCgRelMat = "+contTasas+" - "+auxCgRelMat);
        System.out.println("auxFnPuntos = "+contTasas+" - "+auxFnPuntos);
        System.out.println("auxValorTasa = "+contTasas+" - "+auxValorTasa);
        System.out.println("auxIcMoneda = "+contTasas+" - "+auxIcMoneda);
        System.out.println("auxPlazoDias = "+contTasas+" - "+auxPlazoDias);
        */
        contTasas++;
			}	
		
		}	
		
	}//for
	
	hidNumTasas = String.valueOf(numTasas);  //numero de tasas
  //System.out.println("hidNumTasas === "+hidNumTasas);
  //System.out.println("datosAuxiliar === "+datosAuxiliar);
  
  

	Vector lovTasasM = BeanTasas.ovgetTasas(4); //Tasas para Meses sin intereses   F09-2015 
	HashMap	tasaMeses = new HashMap();
	 int numTasasMes =0;
	 
	for (int i=0; i<lovTasasM.size(); i++) {
		Vector lovDatosTasaM = (Vector)lovTasasM.get(i); 
		String  lsMoneda = lovDatosTasaM.get(10).toString();
		String lsCveTasa = lovDatosTasaM.get(2).toString();
		String lsPlazoFinal = lovDatosTasaM.get(11).toString();
		String lsPlazoini = lovDatosTasaM.get(12).toString();
		String auxCgRelMatM = lovDatosTasaM.get(5).toString();		
		
		tasaMeses.put("ic_tasa"+numTasasMes, lsCveTasa);		
		tasaMeses.put("ic_moneda"+numTasasMes, lsMoneda);		
		tasaMeses.put("plazo_final"+numTasasMes, lsPlazoFinal);
		tasaMeses.put("plazo_ini"+numTasasMes, lsPlazoini);
		tasaMeses.put("auxCgRelMat"+numTasasMes, auxCgRelMatM);  
		 
		
		numTasasMes++; 
	
	} //for
 
  
	resultado.put("success", new Boolean(true));
	resultado.put("Auxiliar", datosAuxiliar);
	resultado.put("hidNumTasas", hidNumTasas);
	resultado.put("bloqueoPymeEpo", bloqueoPymeEpo);	
  resultado.put("csTipoPago", csTipoPago);
  resultado.put("csMesesSinInt", mesesSinInt);
  resultado.put("sAfilComercio", sAfilComercio);  
  resultado.put("tasaMeses", tasaMeses);//F09-2015 
  resultado.put("hidNumTasasMeses", String.valueOf(numTasasMes));//F09-2015   
        
	infoRegresar = resultado.toString();
	
	
} else  if (informacion.equals("ConsultaMonitor") ) {
	
	HashMap	datos = new HashMap();	
	JSONArray registros = new JSONArray();	
	Vector vecColumnas =null;
	//esta parte es para formar el Monitor 
	Vector vecFilas = aceptPyme.monitorLineas(ic_epo, "", ic_moneda, clave_pyme); 
	
	for(int i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector)vecFilas.get(i);
		String nombreIF =(String)vecColumnas.get(1);  //Línea de Crédito IF
		String ic_linea =(String)vecColumnas.get(0);
		String moneda_linea = (String)vecColumnas.get(4);		
		String saldo_inicial = vecColumnas.get(2).toString(); //Saldo inicial
		String montoSeleccionado ="0.00";//Monto seleccionado
		String numDoctos  ="0";//Num. doctos. seleccionados
		String saldoDisponible =vecColumnas.get(3).toString();
		String hidSaldoDisponible = vecColumnas.get(3).toString();		
		String seleccionado ="N";
		if(lineaTodos.equals(ic_linea)){
			seleccionado = "S";
		}
		
		datos = new HashMap();
		datos.put("LINEA_CREDITO_IF",nombreIF);
		datos.put("IC_LINEA",ic_linea);
		datos.put("MONEDA_LINEA",moneda_linea);
		datos.put("SALDO_INICIAL",saldo_inicial);
		datos.put("MONTOSELECCIONADO",montoSeleccionado);
		datos.put("NUMDOCTOS",numDoctos);
		datos.put("SALDODISPONIBLE",saldoDisponible);
		datos.put("HIDSALDODISPONIBLE",saldoDisponible);
		datos.put("SELECCIONADO",seleccionado); //Para habilitar el registro en el monitor 	
		
		registros.add(datos);
	}

	hidNumLineas   = String.valueOf(vecFilas.size());  //numero de lineas

	String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
	JSONObject 	resultado	= new JSONObject();
	resultado = JSONObject.fromObject(consulta);	
	resultado.put("hidNumLineas",hidNumLineas);	
	resultado.put("ic_epo",ic_epo);
	infoRegresar = resultado.toString();			

} else  if (informacion.equals("Consulta") ||  informacion.equals("ResumenTotales") ) {

	HashMap registrosTot = new HashMap();	
	JSONArray registrosTotales = new JSONArray();
		
	String icMoneda = "", parTipoCambio = "", 	diasInhabiles =	"", ic_if	=	"",
	nombreEpo	=	"", igNumeroDocto	=	"", ccAcuse =	"", dfFechaEmision =	"", dfFechaVencimiento	=	"",
	dfFechaPublicacion 	= 	"", igPlazoDocto	=	"",  moneda =	"", cgTipoConv	=	"",  tipoCambio	=   "", 
	igPlazoDescuento	= "",  fnPorcDescuento	= "", modoPlazo	= 	"",  estatus =	"",  cambios	= 	"",
	numeroCredito		= 	"",  nombreIf	= 	"", tipoLinea	=	"", fechaOperacion =	"", referencia	=	"", 
	plazoCredito	=	"", fechaVencCredito =	"", relMat	=	"", tipoCobroInt	=	"",  tipoPiso			=	"",
	icTipoCobroInt		=	"", icTipoFinanciamiento	=	"", monedaLinea	=	"", nombreMLinea	=	"", dias_minimo	= 	"",
	dias_maximo	=	"", icLineaCredito	= 	"", plazo_valido	=	"",  epo ="", inhabilEpoPyme = "", plazofinanciamiento = "",
	interes ="", credito = "", diahabil ="",  fechaVencimiento = "", plazopyme =  "",  plazoepo = "",  
	plazonafin  ="", plazo = "", fechaDiasOperacion ="", diasOperacion ="", meseSinIntereses= "", tipo_Pago="", fechaVen_meses ="",
	plazo_meses ="";
	
	String claveBin = "", descBin = "", codigoBin = "", comisionBin = "",  operalineaCreditoIF ="", fechaVencimientoLinea =""; 

	double fnMonto 	=0,  montoValuado =	0, montoDescuento =	0, montoCredito	=	0,  tasaInteres	= 	0, valorTasaInt	=	0, 
	montoTasaInt		=	0, fnPuntos		= 	0, 	totalMontoMN		=	0, 	totalMontoUSD		=	0, 	totalMontoDescMN	=	0,
	totalMontoDescUSD	=	0, 	totalInteresMN		=	0, 	totalInteresUSD		=	0, 	montoValuadoLinea	=	0, 	totalMontoValuadoMN	=	0,
	totalMontoValuadoUSD=	0, totalMontoCreditoMN	=	0, totalMontoCreditoUSD=	0;
	
	int		totalCreditosMN		= 	0, 	totalCreditosUSD	=	0,   plazo_valido1 = 0,  menosdias =0, no_dia_semana = 0, 
	plazoactual =0, 	totalDoctosMN		=	0, 	totalDoctosUSD =	0, 	i = 0, 	elementos = 0, numTasas = 0,  plazo_meses1 =0;
	
	Vector	vecColumnas	= null;
	
	HashMap	datos = new HashMap();	
	JSONArray registros = new JSONArray();	
	
	
	
	//se obtiene  los registro de la consulta
	Vector vecFilas = null;
	//System.out.println("cmbTCredTodos === " +cmbTCredTodos);
	//System.out.println("lineaTodos === " +lineaTodos);
	
	if(cmbTipoPago.equals("1")){
		vecFilas = aceptPyme.consultaDoctosCCC(ses_ic_pyme,ic_epo,ic_moneda,ig_numero_docto,fn_monto_de,fn_monto_a, cc_acuse,mto_descuento,df_fecha_emision_de,df_fecha_emision_a,doctos_cambio,df_fecha_venc_de,df_fecha_venc_a,modo_plazo,fecha_publicacion_de,fecha_publicacion_a,lineaTodos,null);
	}else if(cmbTipoPago.equals("2")){
		vecFilas = aceptPyme.consultaDoctosCccTC(ses_ic_pyme,ic_epo,ic_moneda,ig_numero_docto,fn_monto_de,fn_monto_a, cc_acuse,mto_descuento,df_fecha_emision_de,df_fecha_emision_a,doctos_cambio,df_fecha_venc_de,df_fecha_venc_a,modo_plazo,fecha_publicacion_de,fecha_publicacion_a,cmbTCredTodos,null);
	}

	if(vecFilas.size()>0){
		for(i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);
			nombreEpo			=	(String)vecColumnas.get(0);
			igNumeroDocto		=	(String)vecColumnas.get(1);
			ccAcuse 			=	(String)vecColumnas.get(2);
			dfFechaEmision		=	(String)vecColumnas.get(3);
			dfFechaVencimiento	=	(String)vecColumnas.get(4);
			dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
			igPlazoDocto		=	(String)vecColumnas.get(6);
			moneda 				=	(String)vecColumnas.get(7);
			fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			cgTipoConv			=	(String)vecColumnas.get(9);
			tipoCambio			=   (String)vecColumnas.get(10);
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			igPlazoDescuento	= 	(String)vecColumnas.get(12);
			fnPorcDescuento		= 	(String)vecColumnas.get(13);
			modoPlazo			= 	(String)vecColumnas.get(14);
			estatus 			=	(String)vecColumnas.get(15);
			cambios				= 	(String)vecColumnas.get(16);
			//creditos por concepto de intereses
			numeroCredito		= 	(String)vecColumnas.get(17);
			nombreIf			= 	(String)vecColumnas.get(18);
			tipoLinea			=	(String)vecColumnas.get(19);
			fechaOperacion		=	(String)vecColumnas.get(20);
			plazoCredito		=	(String)vecColumnas.get(22);
			fechaVencCredito	=	(String)vecColumnas.get(23);
			//			valorTasaInt		=	Double.parseDouble(vecColumnas.get(24).toString());
			relMat				= 	(String)vecColumnas.get(25);
			//			fnPuntos			=	Double.parseDouble(vecColumnas.get(26).toString());
			tipoCobroInt		=	(String)vecColumnas.get(27);
			referencia			=	(String)vecColumnas.get(29);
			tipoPiso			= 	(String)vecColumnas.get(30);
			icTipoCobroInt		= 	(String)vecColumnas.get(31);
			icMoneda			= 	(String)vecColumnas.get(32);
			icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			monedaLinea			= 	(String)vecColumnas.get(35);
			dias_minimo			= 	(String)vecColumnas.get(36);
			dias_maximo			= 	(String)vecColumnas.get(37);
			icLineaCredito		= 	(String)vecColumnas.get(38);
			ic_if				= 	(String)vecColumnas.get(39);
			nombreMLinea		=  	(String)vecColumnas.get(40);		
			plazo_valido =	(String)vecColumnas.get(42)==null?"0":(String)vecColumnas.get(42); 
			fechaVencimiento =	(String)vecColumnas.get(47)==null?"":(String)vecColumnas.get(47); 
			epo = (String)vecColumnas.get(48)==null?"":(String)vecColumnas.get(48); 
			plazopyme = (String)vecColumnas.get(49)==null?"0":(String)vecColumnas.get(49); 
			plazoepo = (String)vecColumnas.get(50)==null?"0":(String)vecColumnas.get(50);
			plazonafin = (String)vecColumnas.get(51)==null?"0":(String)vecColumnas.get(51); 
			fechaDiasOperacion = (String)vecColumnas.get(52)==null?"0":(String)vecColumnas.get(52); 
			diasOperacion  = (String)vecColumnas.get(53)==null?"0":(String)vecColumnas.get(53);
			if(cmbTipoPago.equals("2")){
				claveBin = (String)vecColumnas.get(54);
				descBin = (String)vecColumnas.get(55);
				codigoBin = (String)vecColumnas.get(56);
				comisionBin = (String)vecColumnas.get(57);
				monedaLinea	= (String)vecColumnas.get(58);
				nombreMLinea = moneda;				
			}
		  System.out.println ("vecColumnas.size()  "+vecColumnas.size()); 
		  System.out.println ("(String)vecColumnas.get(59)   "+(String)vecColumnas.get(59));  
		  
		  
			if(cmbTipoPago.equals("1")){  // Linea Credito				
				meseSinIntereses =  (String)vecColumnas.get(54); //F09-2015
				tipo_Pago =  (String)vecColumnas.get(55);  // F09-2015
				fechaVen_meses =  (String)vecColumnas.get(56);  // F09-2015
				
				if(vecColumnas.size()>59){
					operalineaCreditoIF =  (String)vecColumnas.get(57);  // F020-2015
					fechaVencimientoLinea =  (String)vecColumnas.get(58);  // F020-2015
					plazo_meses =  (String)vecColumnas.get(59);  // F09-2015
				}
			}
		
			plazo_valido = plazopyme;
			if (plazopyme.equals("0") && ( plazo_valido.equals("0") || plazo_valido.equals("")) ){
				plazo_valido = plazoepo;        
			}
			if (plazoepo.equals("0") && ( plazo_valido.equals("0") || plazo_valido.equals("") )){
				plazo_valido = plazonafin;
			}
			if(icMoneda.equals(monedaLinea)){
				cgTipoConv		= "";
				tipoCambio		= "";
				montoValuado	= fnMonto;
			}
			if("1".equals(icTipoFinanciamiento)){
				//plazoCredito = igPlazoDocto;
				fechaVencCredito = dfFechaVencimiento;
			}
			if("+".equals(relMat))
				valorTasaInt += fnPuntos;
			else if("-".equals(relMat))
				valorTasaInt -= fnPuntos;
			else if("*".equals(relMat))
				valorTasaInt *= fnPuntos;
			else if("/".equals(relMat))
				valorTasaInt /= fnPuntos;

			montoValuadoLinea = fnMonto;
				
			plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;

			if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
				montoCredito = fnMonto-montoDescuento;
			else if("2".equals(icTipoFinanciamiento))
				montoCredito = fnMonto;

			//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
				if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
					montoCredito = montoValuado;
				else if("2".equals(icTipoFinanciamiento))
					montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
			}				
				
			if("1".equals(tipoPiso)){
				montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
			}else{
				montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
			}
			plazofinanciamiento =(String)vecColumnas.get(44);
			interes =(String)vecColumnas.get(45);
			credito = (String)vecColumnas.get(46);
			
			if (DescuentoAutomatico.equals("S")) {
				plazoactual = Integer.parseInt(plazo_valido); 
			  diahabil  =  Fecha.sumaFechaDias(dfFechaVencimiento,+ plazoactual); 
				 
			  Calendar gcDiaFecha = new GregorianCalendar();
			  java.util.Date fechaVencNvo = Comunes.parseDate(diahabil);
			  gcDiaFecha.setTime(fechaVencNvo);
			  no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
				 
			  boolean Inhabil = false;
			  Inhabil  = aceptPyme.DiaInhabil(diahabil);  
			  inhabilEpoPyme =aceptPyme.getDiasInhabilesDes(iNoCliente,epo,diahabil);
        
			  if(no_dia_semana == Calendar.SATURDAY ) { // 7 Sabado
						if(Inhabil==true || !inhabilEpoPyme.equals(""))  {                
							menosdias = +1;
					plazo_valido1 = Integer.parseInt(plazo_valido)- menosdias; 
					plazo_valido=String.valueOf(plazo_valido1); 
					diahabil  =  Fecha.sumaFechaDias(diahabil, -1);  
				 }
					}
											 
			  if(no_dia_semana==Calendar.SUNDAY ) { //domingo
						if(Inhabil==true ||  !inhabilEpoPyme.equals("") )  {             
							menosdias = +2;
					plazo_valido1 = Integer.parseInt(plazo_valido)- menosdias; 
					plazo_valido=String.valueOf(plazo_valido1); 
					diahabil  =  Fecha.sumaFechaDias(diahabil, -2);  
						}
			  }   
				if(no_dia_semana!=Calendar.SATURDAY &&  no_dia_semana!=Calendar.SUNDAY )  {        
					if (!inhabilEpoPyme.equals("")) { 
						int menosdias1 = -1;                  
            plazo_valido1 = Integer.parseInt(plazo_valido)- 1; 
            plazo_valido=String.valueOf(plazo_valido1);                  
            fechaVencimiento  =  Fecha.sumaFechaDias(fechaVencimiento, menosdias1);
            Inhabil  = aceptPyme.DiaInhabil(fechaVencimiento);  
            inhabilEpoPyme =aceptPyme.getDiasInhabilesDes(iNoCliente,epo,fechaVencimiento);
            java.util.Date fechaVencNvo1 = Comunes.parseDate(fechaVencimiento);
            gcDiaFecha.setTime(fechaVencNvo1);
            no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
            if(no_dia_semana==Calendar.SATURDAY ) { // 7 Sabado
							if(Inhabil==true || !inhabilEpoPyme.equals(""))  { 
								menosdias = 1 ;
                plazo_valido1 = Integer.parseInt(plazo_valido)- menosdias; 
                plazo_valido=String.valueOf(plazo_valido1); 
                diahabil  =  Fecha.sumaFechaDias(diahabil, -1);  
              }
            }
            if(no_dia_semana==Calendar.SUNDAY ) {  //domingo
							if(Inhabil==true || !inhabilEpoPyme.equals(""))  { 
								menosdias = 2 ;  
								plazo_valido1 = Integer.parseInt(plazo_valido)- menosdias; 
                plazo_valido=String.valueOf(plazo_valido1);  
                diahabil  =  Fecha.sumaFechaDias(diahabil, -2);                   
              }
            }
            if(no_dia_semana!=Calendar.SATURDAY  &&  no_dia_semana!=Calendar.SUNDAY)  {
							if(Inhabil==true || !inhabilEpoPyme.equals(""))  {        
								menosdias = +1;
                plazo_valido1 = Integer.parseInt(plazo_valido)- menosdias; 
                plazo_valido=String.valueOf(plazo_valido1); 
                diahabil  =  Fecha.sumaFechaDias(diahabil, -1);
              }
            }
          }              
        }
        if(Inhabil==false &&  inhabilEpoPyme.equals(""))  { 
					plazo_valido=plazo_valido; 
        }
    }//if (DescuentoAutomatico.equals("S")) {    
  
			String plazo_final ="0"; 
		if("1".equals(icTipoFinanciamiento)){
			plazo_final = plazoCredito;			
			fechaVencCredito =fechaVencCredito;
		}else{
			if ("3".equals(plazofinanciamiento) && (DescuentoAutomatico.equals("N")|| DescuentoAutomatico.equals("S"))){  
				plazo_final = plazo;	
			} else if (DescuentoAutomatico.equals("N")|| DescuentoAutomatico.equals("")){  
				plazo_final = plazo;
			}else if (DescuentoAutomatico.equals("S") ) {
				plazo_final =  plazo_valido;
			} 
			fechaVencCredito = fechaVencCredito;
		}
		
		
		    
     if("1".equals(icTipoFinanciamiento)){  // cuando es pronto Pago
     
       plazoactual = Integer.parseInt(plazo_final); 
      // diahabil  =  Fecha.sumaFechaDias(dfFechaVencimiento, +plazoactual);	 //esta parte no aplica se quita n Fodea 09-2015 
		
		diahabil  =  dfFechaVencimiento;
        
      Calendar gcDiaFecha = new GregorianCalendar();
      java.util.Date fechaVencNvo = Comunes.parseDate(diahabil);
      gcDiaFecha.setTime(fechaVencNvo);
       no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
      
       menosdias =0;     
      boolean Inhabil = false;
       
      Inhabil  = aceptPyme.DiaInhabil(diahabil);     
      inhabilEpoPyme =aceptPyme.getDiasInhabilesDes(iNoCliente,epo,diahabil);      
  
       
       if(no_dia_semana==Calendar.SATURDAY ) { // 7 Sabado
        if(Inhabil==true  || !inhabilEpoPyme.equals(""))  {        
        menosdias = 1;
        diahabil  =  Fecha.sumaFechaDias(diahabil, -1);  
        plazoactual = Integer.parseInt(plazoCredito)-menosdias;
      }
      }
                           
       if(no_dia_semana==Calendar.SUNDAY) { // domingo
        if(Inhabil==true  || !inhabilEpoPyme.equals(""))  {        
          menosdias = 2;
         diahabil  =  Fecha.sumaFechaDias(diahabil, -2);  
         plazoactual = Integer.parseInt(plazoCredito)-menosdias;
        }
       }   
          
     if(no_dia_semana!=Calendar.SATURDAY &&  no_dia_semana!=Calendar.SUNDAY ) {
      if( !inhabilEpoPyme.equals(""))  {        
                
        diahabil  =  Fecha.sumaFechaDias(diahabil, -1);  
        plazoactual = Integer.parseInt(plazoCredito)-1;        
        
         Inhabil  = aceptPyme.DiaInhabil(diahabil);  
         inhabilEpoPyme =aceptPyme.getDiasInhabilesDes(iNoCliente,epo,diahabil);
           
         java.util.Date fechaVencNvo1 = Comunes.parseDate(diahabil);
         gcDiaFecha.setTime(fechaVencNvo1);
         no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
         
         
          if(no_dia_semana==Calendar.SATURDAY) { // 7 Sabado
              if(Inhabil==true  || !inhabilEpoPyme.equals(""))  {  
              menosdias = 1;
              diahabil  =  Fecha.sumaFechaDias(diahabil, -1);  
              plazoactual = plazoactual-menosdias;
            }
            }
                           
           if(no_dia_semana==Calendar.SUNDAY) { // domingo
            if(Inhabil==true  || !inhabilEpoPyme.equals(""))  {  
              menosdias = 2;
             diahabil  =  Fecha.sumaFechaDias(diahabil, -2);  
             plazoactual = plazoactual-menosdias;
            }
           }       
      }
      
     } 
   
  }else {  
  plazoactual = Integer.parseInt(plazoCredito); 
  diahabil = fechaVencCredito;
  }
	
	
	
	
	if("2".equals(tipo_Pago)){  // cuando es pronto Pago 
   	plazo_meses1 = Integer.parseInt(plazo_meses); 
    	
		diahabil  =  fechaVen_meses;
        
      Calendar gcDiaFecha = new GregorianCalendar();
      java.util.Date fechaVencNvo = Comunes.parseDate(diahabil);
      gcDiaFecha.setTime(fechaVencNvo);
      no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
      
      menosdias =0;     
      boolean Inhabil = false;
		boolean inhabilXAnio = false;
       
      Inhabil  = aceptPyme.DiaInhabil(diahabil);  //dias inhabiles      
      inhabilXAnio  =aceptPyme.getEsDiasInhabilesXanio(diahabil);  ///dias inhabiles   x habño      
  
       
      if(no_dia_semana==Calendar.SATURDAY ) { // 7 Sabado
			if(Inhabil==true ||  inhabilXAnio==true )  {        
				menosdias = 1;
				diahabil  =  Fecha.sumaFechaDias(diahabil, -1);  
				 plazo_meses1 = plazo_meses1-menosdias;
			
			}
      }
                           
       if(no_dia_semana==Calendar.SUNDAY) { // domingo
        if(Inhabil==true  ||  inhabilXAnio==true )  {        
          menosdias = 2;
          diahabil  =  Fecha.sumaFechaDias(diahabil, -2);
			 plazo_meses1 = plazo_meses1-menosdias;
        }
       }   
          
     if(no_dia_semana!=Calendar.SATURDAY &&  no_dia_semana!=Calendar.SUNDAY ) {
      if( inhabilXAnio==true )  {        
                
        diahabil  =  Fecha.sumaFechaDias(diahabil, -1);  
        Inhabil  = aceptPyme.DiaInhabil(diahabil);  //dias inhabiles      
        inhabilXAnio  =aceptPyme.getEsDiasInhabilesXanio(diahabil);  ///dias inhabiles   x Año    
           
         java.util.Date fechaVencNvo1 = Comunes.parseDate(diahabil);
         gcDiaFecha.setTime(fechaVencNvo1);
         no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
         
         if(no_dia_semana==Calendar.SATURDAY) { // 7 Sabado
				if(Inhabil==true  || inhabilXAnio==true  )  {  
					menosdias = 1;
               diahabil  =  Fecha.sumaFechaDias(diahabil, -1);  
					plazo_meses1 = plazo_meses1-menosdias;
            }
         }
                           
         if(no_dia_semana==Calendar.SUNDAY) { // domingo
				if(Inhabil==true  || inhabilXAnio==true  )  {  
					menosdias = 2;
					diahabil  =  Fecha.sumaFechaDias(diahabil, -2);  
					plazo_meses1 = plazo_meses1-menosdias;
            }
         }       
      }      
     }  
	  
	  fechaVen_meses = diahabil; 
	  plazo_meses=  String.valueOf(plazo_meses1);
	  
  }
  
			datos = new HashMap();
			datos.put("NOMBRE_EPO",nombreEpo);
			datos.put("NO_DOCUMENTO",igNumeroDocto);
			datos.put("NO_ACUSE",ccAcuse);	
			datos.put("FECHA_EMISION",dfFechaEmision);	
			datos.put("FECHA_VENCIMIENTO",dfFechaVencimiento);	
			datos.put("FECHA_PUBLICACION",dfFechaPublicacion);	
			datos.put("PLAZO_DOCTO",igPlazoDocto);				
			datos.put("MONEDA",moneda);	
			datos.put("IC_MONEDA_DOCTO",icMoneda);	
			datos.put("IC_MONEDA_LINEA",monedaLinea);	
			datos.put("IC_IF",ic_if);	
			datos.put("TIPO_PISO",tipoPiso);	
			datos.put("MONTO",Double.toString (fnMonto));	
			datos.put("PLAZO_DESCUENTO",igPlazoDescuento);	
			datos.put("PORCENTAJE_DESCUENTO",fnPorcDescuento);
			datos.put("MONTO_DESCUENTO",Double.toString (montoDescuento) );			

		if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
				datos.put("TIPO_CONV",cgTipoConv);
				datos.put("TIPO_CAMBIO",tipoCambio);
				datos.put("MONTO_VALUADO",Double.toString (montoValuado));		
		}else{	
				datos.put("TIPO_CONV","");
				datos.put("TIPO_CAMBIO","");
				datos.put("MONTO_VALUADO","0");	
		}		
		datos.put("MODALIDAD_PLAZO",modoPlazo);
		datos.put("NO_DOCTO_FINAL",numeroCredito);
		datos.put("MONEDA_FINAL",nombreMLinea);
		datos.put("MONTO_FINAL",Double.toString (montoCredito));
		datos.put("MONTO_CREDITO",Double.toString (((double)Math.round(montoCredito*100)/100)) );
		datos.put("PLAZO",plazo );
		datos.put("PLAZO_VALIDO",plazo_valido );
		
		if(!"1".equals(icTipoFinanciamiento)){ 

			if("2".equals(icTipoFinanciamiento)  && "2".equals(tipo_Pago)  ){
				
				datos.put("PLAZO_FINAL",plazo_meses );   
				datos.put("FECHA_VENC_FINAL",fechaVen_meses );	
				datos.put("PLAZO_PRONTO_PAGO","0" );
				
			}else { 		 // Financiamiento  con Interes
				datos.put("PLAZO_FINAL",plazo_final );
				datos.put("FECHA_VENC_FINAL",fechaVencCredito );	
				datos.put("PLAZO_PRONTO_PAGO","0" );	
				
			}
		
		}else {
			datos.put("PLAZO_FINAL",String.valueOf(plazoactual));
			datos.put("FECHA_VENC_FINAL",diahabil );	
			datos.put("PLAZO_PRONTO_PAGO",plazo_final );	
		
		}
		datos.put("FECHA_OPERA_FINAL",fechaHoy ); 	
		datos.put("NOMBRE_IF",nombreIf ); 	
		datos.put("IC_LINEA_CREDITO",icLineaCredito ); 	
		datos.put("REFERENCIA","" ); 	
		datos.put("IC_TASA","" ); 
		datos.put("CG_REL_MAT","" ); 
		datos.put("FN_PUNTOS","0" ); 
		datos.put("VALOR_TASA","0" );  
		datos.put("VALOR_TASA_PUNTOS",("S".equals(icTipoCobroInt))?"0":"0"+valorTasaInt ); 
		datos.put("MONTO_TASA_INTERES",("S".equals(icTipoCobroInt))?"":""+((double)Math.round(montoTasaInt*Double.parseDouble(plazoCredito)*100)/100)); 
		datos.put("MONTO_AUX_INTERES",Double.toString (montoTasaInt) ); 
		datos.put("MONTO_TOTAL_CAPITAL",""); 
		datos.put("PLAZO_UNICO",plazo_valido); 
		datos.put("SELECCION","N"); 
		datos.put("IC_TIPO_FINANCIAMIENTO",icTipoFinanciamiento); 
		datos.put("DIAS_MINIMO",dias_minimo); 
		datos.put("DIAS_MAXIMO",dias_maximo); 
		datos.put("FECHA_DIAS_OPERACION",fechaDiasOperacion);  
		datos.put("DIAS_OPERACION",diasOperacion);
		
		datos.put("CLAVE_BIN",claveBin);
		datos.put("DESC_BIN",descBin);
		datos.put("CODIGO_BIN",codigoBin);
		datos.put("COMISION_BIN",comisionBin);
		
		datos.put("MESES_INTERESES",meseSinIntereses);
		datos.put("TIPOPAGO",tipo_Pago);
		
		datos.put("CG_LINEA_CREDITO", operalineaCreditoIF.trim()  );	//F020-2015
		datos.put("FECHA_VENC_LINEA", fechaVencimientoLinea  ); //F020-2015
					
			
		registros.add(datos);	
		elementos++;


	//para los totales 
	if("1".equals(icMoneda)){
			totalDoctosMN++;
			totalMontoMN += fnMonto;
			totalMontoDescMN += montoDescuento;
			totalInteresMN += valorTasaInt;
		}else{
			totalDoctosUSD++;
			totalMontoUSD += fnMonto;
			totalMontoDescUSD += montoDescuento;
			totalInteresUSD += valorTasaInt;
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
				totalMontoValuadoUSD += montoValuado;
			}
		}
		if("1".equals(monedaLinea)){
			totalCreditosMN++;
			totalMontoCreditoMN += montoCredito;
		}else if("54".equals(monedaLinea)&&"54".equals(icMoneda)){
			totalCreditosUSD++;
			totalMontoCreditoUSD += montoCredito;
		}
	
	}	//for
	 
		for(int t =0; t<2; t++) {		
		registrosTot = new HashMap();		
			if(t==0){ 				
			registrosTot.put("MONEDA", "MONEDA NACIONAL");
			registrosTot.put("ICMONEDA", "1");
			registrosTot.put("TOTAL", Double.toString(totalDoctosMN) );	
			registrosTot.put("MONTO", Double.toString(totalMontoMN));	
			registrosTot.put("TOTAL_MONTO_DESC", Double.toString(totalMontoDescMN)  );	
			registrosTot.put("TOTAL_MONTO_VALUADO", Double.toString(totalMontoValuadoMN)  );
			registrosTot.put("TOTAL_CREDITOS", Double.toString(totalCreditosMN)  );
			registrosTot.put("TOTAL_MONTO_CREDITOS", Double.toString(totalMontoCreditoMN)  );			
			registrosTot.put("MONTOINTERES", "0.0" );	
			registrosTot.put("MONTOCAPITALINTERES", "0.0" );
		}
		if(t==1){ 			
			registrosTot.put("MONEDA", "MONEDA USD");
			registrosTot.put("ICMONEDA", "54");
			registrosTot.put("TOTAL", Double.toString(totalDoctosUSD)  );	
			registrosTot.put("MONTO", Double.toString(totalMontoUSD)  );	
			registrosTot.put("TOTAL_MONTO_DESC", Double.toString(totalMontoDescUSD)  );	
			registrosTot.put("TOTAL_MONTO_VALUADO", Double.toString(totalMontoValuadoUSD)  );	
			registrosTot.put("TOTAL_CREDITOS", Double.toString(totalCreditosUSD)  );
			registrosTot.put("TOTAL_MONTO_CREDITOS", Double.toString(totalMontoCreditoUSD)  );
			registrosTot.put("MONTOINTERES", "0.0" );	
			registrosTot.put("MONTOCAPITALINTERES", "0.0" );	
			
		}
		registrosTotales.add(registrosTot);
	}
	
	}
	if (informacion.equals("Consulta") ) {
	
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";	
		JSONObject 	resultado	= new JSONObject();
		resultado = JSONObject.fromObject(consulta);
		resultado.put("ic_epo",ic_epo);	
		resultado.put("ElEMENTOS", String.valueOf(elementos));		
		infoRegresar = resultado.toString();
	
	}
	
	if (informacion.equals("ResumenTotales") ) {	
		 infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registrosT\": " + registrosTotales.toString()+"}";
	}

}else  if (informacion.equals("ConfirmacionClaves") ) {

	String cesionUser 	= (request.getParameter("cesionUser")==null)?"":request.getParameter("cesionUser");
	String cesionPassword 	= (request.getParameter("cesionPassword")==null)?"":request.getParameter("cesionPassword");
	boolean		bConfirma = false;	
	//System.out.println("cesionUser" +cesionUser);
	//System.out.println("cesionPassword" +cesionPassword);
	
	if(!cesionUser.equals("") && !cesionPassword.equals("")) {
	try{
			seguridadEJB.validarUsuario(iNoUsuario, cesionUser , cesionPassword);
			//aceptPyme.validarUsuario(iNoCliente, cesionUser , cesionPassword, (String) request.getRemoteAddr(), (String) request.getRemoteHost(), (String) session.getAttribute("sesCveSistema"));
			bConfirma = true;
		}catch(Exception e){
			bConfirma = false;
		}		
	}
	
		JSONObject 	jsonObj	= new JSONObject();
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("bConfirma", new Boolean(bConfirma));	
		infoRegresar = jsonObj.toString();	
	
}else  if (informacion.equals("PreOrdenTransaccionTmp") ) {
	String ic_documento[] = request.getParameterValues("ic_documento");
	String monto_credito[] = request.getParameterValues("monto_credito");
	String ic_moneda_docto[] = request.getParameterValues("ic_moneda_docto");
	String numOrden = (request.getParameter("OrderId")==null)?"":request.getParameter("OrderId");
	
	int totalDoctosMN = 0;
	double totalMontoCreditoMN = 0;
	for(int i=0;i<ic_documento.length;i++){
		if("1".equals(ic_moneda_docto[i])){
			totalDoctosMN ++;
			totalMontoCreditoMN+= Double.parseDouble(monto_credito[i]);
		}
	}
	
	aceptPyme.preGuardadoIdOrdenTC(ic_documento, numOrden, Double.toString(totalMontoCreditoMN));
	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else  if (informacion.equals("TransaccionRechazada") ) {
	String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
	String modificado[] = request.getParameterValues("modificado");
	HashMap hm = new HashMap();
	hm.put("OrderId",OrderId);
	hm.put("ResponseCode",ResponseCode);
	hm.put("ResponseMessage",ResponseMessage);
	hm.put("iNoNafinElectronico",String.valueOf(iNoNafinElectronico));
	hm.put("iNoUsuario",iNoUsuario);
	hm.put("cveEpo",ic_epo);
	hm.put("cveIf",ic_if);
	aceptPyme.rechazoIdOrdenTC(hm,modificado); 
	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else  if (informacion.equals("GeneraAcuse") ) {

	String 		_acuse = "500";
	String modificado[] = request.getParameterValues("modificado");
	String ic_documento[] = request.getParameterValues("ic_documento");
	String ic_moneda_docto[] = request.getParameterValues("ic_moneda_docto");
	String ic_moneda_linea[] = request.getParameterValues("ic_moneda_linea");
	String monto[] = request.getParameterValues("monto");
	String monto_valuado[] = request.getParameterValues("monto_valuado");
	String monto_credito[] = request.getParameterValues("monto_credito");
	String monto_tasa_int[] = request.getParameterValues("monto_tasa_int");
	String monto_descuento[] = request.getParameterValues("monto_descuento");
	String tipo_cambio[] = request.getParameterValues("tipo_cambio");
	String plazo_credito[] = request.getParameterValues("plazo");
	String fecha_vto[] = request.getParameterValues("fecha_vto");
	String referencia_tasa[] = request.getParameterValues("referencia_tasa");
	String fn_valor_tasa[] = request.getParameterValues("valor_tasa_puntos");
	String ic_tasa[] = request.getParameterValues("ic_tasa");
	String cg_rel_mat[] = request.getParameterValues("cg_rel_mat");
	String fn_puntos[] = request.getParameterValues("fn_puntos");
	String ic_linea_credito[] = request.getParameterValues("ic_linea_credito");
	String ic_bins[] = request.getParameterValues("ic_bins");

	/*System.out.println("modificados---"+modificado[0]);
	System.out.println("ic_documentos---"+ic_documento[0]);
	System.out.println("ic_moneda_docto---"+ic_moneda_docto[0]);
	System.out.println("ic_monedas_linea---"+ic_moneda_linea[0]);
	System.out.println("montos---"+monto[0]);
	System.out.println("monto_valuado---"+monto_valuado[0]);
	System.out.println("monto_credito---"+monto_credito[0]);
	System.out.println("tipos_cambio---"+tipo_cambio[0]);
	System.out.println("plazos_credito---"+plazo_credito[0]);
	System.out.println("fechas_vto---"+fecha_vto[0]);
	System.out.println("referencias_tasa---"+referencia_tasa[0]);
	System.out.println("ic_tasa---"+ic_tasa[0]);
	System.out.println("cg_rel_mat---"+cg_rel_mat[0]);
	System.out.println("fn_puntos---"+fn_puntos[0]);
	System.out.println("ic_linea_credito---"+ic_linea_credito);
	*/
	
	String  in =""; 
	int		totalDoctosMN		= 0	, totalDoctosUSD = 0, totalDoctosConv		= 0;
	double totalMontoMN		= 0, totalMontoUSD = 0, totalMontoDescMN	= 0, totalMontoDescUSD	= 0,
	totalInteresMN	= 0, totalInteresUSD	= 0, totalMontoCreditoMN	= 0, totalMontoCreditoUSD	= 0,
	totalMontoCreditoConv= 0, totalMontosConv	= 0,	totalMontoDescConv	= 0, totalInteresConv		= 0,
	totalConvPesos		= 0;
	
	for(int i=0;i<ic_documento.length;i++){
		if("1".equals(ic_moneda_docto[i])){
			totalDoctosMN ++;
			totalMontoMN		+= Double.parseDouble(monto[i]);
			totalMontoDescMN	+= Double.parseDouble(monto_descuento[i]);
			totalInteresMN		+= Double.parseDouble("".equals(monto_tasa_int[i])?"0":monto_tasa_int[i]);				
			totalMontoCreditoMN+= Double.parseDouble(monto_credito[i]);
		}
		if("54".equals(ic_moneda_docto[i])&&"54".equals(ic_moneda_linea[i])){
			totalDoctosUSD ++;
			totalMontoUSD		+= Double.parseDouble(monto[i]);
			totalMontoDescUSD	+= Double.parseDouble(monto_descuento[i]);
			totalInteresUSD		+= Double.parseDouble("".equals(monto_tasa_int[i])?"0":monto_tasa_int[i]);
			totalMontoCreditoUSD+= Double.parseDouble(monto_credito[i]);
		}
		if("54".equals(ic_moneda_docto[i])&&"1".equals(ic_moneda_linea[i])){
			totalDoctosConv ++;
			totalMontosConv			+= Double.parseDouble(monto[i]);
			totalMontoDescConv 		+= Double.parseDouble(monto_descuento[i]);
			totalInteresConv		+= Double.parseDouble("".equals(monto_tasa_int[i])?"0":monto_tasa_int[i]);
			totalConvPesos			+= Double.parseDouble(monto_descuento[i]);
			totalMontoCreditoConv	+= Double.parseDouble(monto_credito[i]);
		}			
	} //for
	
	HashMap hmInfoTransTC = new HashMap();
	hmInfoTransTC.put("MONTO_TOTAL", Double.toString (totalMontoCreditoMN));
	hmInfoTransTC.put("ID_TRANSACCION", TransactionId);
	hmInfoTransTC.put("CODIGO_RESPUESTA", ResponseCode);
	hmInfoTransTC.put("CODIGO_DESC", ResponseMessage);
	hmInfoTransTC.put("CODIGO_AUTORIZACION", AuthCode);
	hmInfoTransTC.put("ESTATUS_TRANSACCION","A");
  hmInfoTransTC.put("ULTIMOS_DIGIT_TC", Last4Digit);
	
	String	acuse = "";
	if(cmbTipoPago.equals("1")){
		acuse = aceptPyme.confirmaDoctoCCC(
				Double.toString (totalMontoMN), Double.toString (totalInteresMN), Double.toString (totalMontoCreditoMN),
				Double.toString (totalMontoUSD), Double.toString (totalInteresUSD),	Double.toString (totalMontoCreditoUSD),
				iNoUsuario,_acuse, ic_documento,ic_tasa,cg_rel_mat,fn_puntos,fn_valor_tasa,monto_tasa_int,monto_credito,
				plazo_credito,fecha_vto,ic_linea_credito);	
	}else if(cmbTipoPago.equals("2")){
		acuse = aceptPyme.confirmaDoctoCCC(
				Double.toString (totalMontoMN), Double.toString (totalInteresMN), Double.toString (totalMontoCreditoMN),
				Double.toString (totalMontoUSD), Double.toString (totalInteresUSD),	Double.toString (totalMontoCreditoUSD),
				iNoUsuario,_acuse, ic_documento,ic_tasa,cg_rel_mat,fn_puntos,fn_valor_tasa,monto_tasa_int,monto_credito,
				plazo_credito,fecha_vto, ic_bins, "", OrderId, hmInfoTransTC);
	}	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("acuse",acuse);	 
	jsonObj.put("fecCarga",fechaHoy);	
	jsonObj.put("horaCarga",horaCarga);	
	jsonObj.put("captUser",iNoUsuario+" "+strNombreUsuario);	
	infoRegresar = jsonObj.toString();	
	


}else  if (informacion.equals("obtenTipoPagoEpo") ) {
  Vector vParamEpo = BeanParametro.getParamEpo(ic_epo);
  String csTipoPago = (String)vParamEpo.get(8);
  
  JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("csTipoPago",csTipoPago);	 
	
	infoRegresar = jsonObj.toString();	
}
%>
<%=infoRegresar%>

<% //System.out.println("infoRegresar" +infoRegresar); %> 
