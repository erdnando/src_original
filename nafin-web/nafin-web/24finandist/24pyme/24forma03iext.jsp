<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.sql.*, java.math.*,
	com.netro.distribuidores.*,com.netro.exception.*, javax.naming.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject,net.sf.json.JSONArray"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String informacion  =	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class);
		
Vector parametros 		= null;

String rs_epo					= "";
String rs_nomEpo			= "";
String rs_dscto_auto	= "";
String rs_moneda_auto	= "";
String rs_dolar				= "";
String monedaNacional	= "";
String dolar					= "";
String autorizacion		= "";
StringBuffer mensaje;
JSONArray jsonArray		= new JSONArray();	
	try{
//Inicia Creación de archivo
		if (informacion.equals("ArchivoPDF"))	{
			int nRow = 0;
			String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);
			
			parametros = BeanParametros.getParamDesctoAuto(iNoCliente);
                        
                        String operaContrato = BeanParametros.obtieneOperaSinCesion(iNoEPO); //PARAMETRO NUEVO JAGE 14012017
                        
			for(int p=0; p<parametros.size();p++){
				Vector paramEpo = (Vector)parametros.get(p);
				if (p==0){
					rs_dscto_auto = paramEpo.get(2).toString();
					autorizacion = "S".equals(rs_dscto_auto)?"Sí autorizo":"No autorizo";
					
					mensaje = new StringBuffer();
					
                                        if(operaContrato.equals("S")){
                                            mensaje.append("Autorizo expresamente que automáticamente sea(n) sustituido(s) el(los)" +
								" DOCUMENTO(S) INICIAL(ES) que sean publicados en la CADENA PRODUCTIVA," + 
								" por el(los) DOCUMENTO(S) FINAL(ES) y estos últimos sean financiados con el fin de pagar a la" +
								" EMPRESA DE PRIMER ORDEN.\n");
                                        }else{
                                            mensaje.append("Autorizo expresamente que automáticamente sea(n) sustituido(s) el(los)" +
								" DOCUMENTO(S) INICIAL(ES) que sean publicados en la CADENA PRODUCTIVA," + 
								" por el(los) DOCUMENTO(S) FINAL(ES) y estos últimos sean descontados a favor" +
								" de la EMPRESA DE PRIMER ORDEN.\n");
					}
                                        mensaje.append("Nota: \nSólo aplica para Modalidad Pronto Pago y Pronto Pago Fijo" +
											"\nSólo aplica para Intereses al Vencimiento" +
											"\nOperar descuento automático: "+autorizacion);
					pdfDoc.setTable(1,100);
					pdfDoc.addText(mensaje.toString(),"formasrep",ComunesPDF.JUSTIFIED);
					pdfDoc.addTable();
					pdfDoc.setTable(3,100);
					pdfDoc.setCell("PARAMETRIZAR ESTA EPO","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("MONEDA NACIONAl","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("DOLARES","celda01rep",ComunesPDF.CENTER);
				}
				rs_epo = paramEpo.get(0).toString();
				rs_nomEpo = paramEpo.get(1).toString();
				rs_moneda_auto = paramEpo.get(3).toString();
				rs_dolar = paramEpo.get(4).toString();

				pdfDoc.setCell(rs_nomEpo,"formasrep",ComunesPDF.LEFT);
				monedaNacional=("N".equals(rs_moneda_auto)||"A".equals(rs_moneda_auto))?"Si":"No";
				pdfDoc.setCell(monedaNacional,"formasrep",ComunesPDF.CENTER);
				if(!"".equals(rs_dolar)) {
					dolar=("D".equals(rs_moneda_auto)||"A".equals(rs_moneda_auto))?"Si":"No";
				}
				pdfDoc.setCell(dolar,"formasrep",ComunesPDF.CENTER);
				nRow++;
			}
			if(nRow == 0){
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else{
				pdfDoc.addTable();
			}
		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}catch (Exception e) {
		throw new AppException("Error al generar el archivo", e);
	} 
%>
<%=jsonObj%>
