<%@ page contentType="application/json;charset=UTF-8"
import="
	java.text.*, 
	java.math.*, 
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.anticipos.*,
	com.netro.descuento.*,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>
<%System.out.println("24consulta03extCSV.jsp (E)");%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_epo = request.getParameter("no_epo") == null?"":(String)request.getParameter("no_epo");
String infoRegresar = "";
int numRegistros = 0;
String noIfs ="";
String moneda ="1,54";
String ses_ic_pyme = iNoCliente;
int liRegistros=0;
int Registros1 = 0;
int Registros2 = 0;
int Registros3 = 0;
int index=0;
Vector vecFilas		= null;
Vector vecColumnas	= null;
Vector vecFilas2	= null;
Vector vecColumnas2 = null;
Vector lovTasas		= null;
Vector vector		= null;
String total = "",tipoCredito = "",pagoInt = "",encabezado="";
String ifRazonSoc = "",ic_if="",epoRazonSoc = "";
String aux="";
boolean DM=true,CCC=true;
AccesoDB con = new AccesoDB();
CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer("");
String nombreArchivo=null;
JSONObject jsonObj = new JSONObject();

try{
	con.conexionDB();

	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);
	
	vecFilas = BeanTasas.disOpera("",4,iNoCliente);
	for (int i = 0;i<vecFilas.size();i++) {
		vecColumnas = (Vector)vecFilas.get(i);
		total = (String)vecColumnas.get(0);
		tipoCredito = (String)vecColumnas.get(1);
		//para Descuento Mercantil
		if (Integer.parseInt(total) > 0 && "D".equals(tipoCredito)){
			pagoInt = BeanTasas.paramPagoInteres(ic_epo,4);
			if ("D".equals(pagoInt)){	//si la pyme es la responsable del pago de interes
				vecFilas2 = BeanTasas.esquemaParticular(ic_epo,4,"",iNoCliente); // obtiene las EPO e IF que tienen una LC vigente
				for (int j=0; j<vecFilas2.size(); j++) {
					DM = false;
					Vector lovDatosTas = (Vector)vecFilas2.get(j);
					ic_if = lovDatosTas.get(0).toString();
					ifRazonSoc = lovDatosTas.get(1).toString();
					epoRazonSoc = lovDatosTas.get(2).toString();	
					encabezado = "Esquema de tasas "+epoRazonSoc+" - "+ifRazonSoc;
					
					lovTasas = BeanTasas.ovgetTasasxEpo(4,ic_epo,ic_if);					
					contenidoArchivo.append(encabezado.replace(',',' ')+"\n");				
					if(lovTasas.size()>0){
						contenidoArchivo.append("Moneda,Tipo Tasa,Plazo,Valor,Rel. mat.,Puntos adicionales,Tasa a aplicar,Fecha última actualización");
						contenidoArchivo.append("\n");
						for (int y=0; y<lovTasas.size(); y++) {
							numRegistros++;
							Vector lovDatosTasa = (Vector)lovTasas.get(y);						
							contenidoArchivo.append(lovDatosTasa.get(0).toString().replace(',',' ')+","); 				
							contenidoArchivo.append(lovDatosTasa.get(1).toString()+","); 				
							contenidoArchivo.append("\" "+lovDatosTasa.get(3).toString()+"\",");
							contenidoArchivo.append(lovDatosTasa.get(4).toString()+","); 					
							contenidoArchivo.append(lovDatosTasa.get(5).toString()+","); 					
							contenidoArchivo.append(lovDatosTasa.get(6).toString()+",");			
							contenidoArchivo.append(lovDatosTasa.get(7).toString()+",");
							contenidoArchivo.append(lovDatosTasa.get(8).toString()+"\n");
						}
					}
									
				}//for (int j=0; j<vecFilas2.size(); j++)
			}
		}
		//-----------------------------para Credito Cuenta Corriente
		if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito)){
			
			//para Obtener los Intermediarios Financieros
			vecFilas2 = BeanTasas.esquemaParticular(ic_epo,4,"",iNoCliente,"C"); // obtiene las EPO e IF que tienen una LC vigente
			
			for (int j=0; j<vecFilas2.size(); j++) {
				DM = false;
				Vector lovDatosTas = (Vector)vecFilas2.get(j);
				ic_if = lovDatosTas.get(0).toString();
				ifRazonSoc = lovDatosTas.get(1).toString();
				epoRazonSoc = lovDatosTas.get(2).toString();					
				encabezado = "Esquema de tasas "+epoRazonSoc+" - "+ifRazonSoc;
				
				lovTasas = BeanTasas.ovgetTasasxPyme(4,ses_ic_pyme,ic_if,"1, 54");
				
				contenidoArchivo.append(encabezado.replace(',',' ')+"\n");
				
				if(lovTasas.size()>0){
					contenidoArchivo.append("Moneda,Tipo Tasa,Plazo,Valor,Rel. mat.,Puntos adicionales,Tasa a aplicar,Fecha última actualización"+"\n");
					for (int y=0; y<lovTasas.size(); y++) {
						numRegistros++;
						Vector lovDatosTasa = (Vector)lovTasas.get(y);
						contenidoArchivo.append(lovDatosTasa.get(0).toString().replace(',',' ')+","); 				
						contenidoArchivo.append(lovDatosTasa.get(1).toString()+","); 				
						contenidoArchivo.append("\" "+lovDatosTasa.get(3).toString()+"\",");
						contenidoArchivo.append(lovDatosTasa.get(4).toString()+","); 					
						contenidoArchivo.append(lovDatosTasa.get(5).toString()+","); 					
						contenidoArchivo.append(lovDatosTasa.get(6).toString()+",");			
						contenidoArchivo.append(lovDatosTasa.get(7).toString()+",");
						contenidoArchivo.append(lovDatosTasa.get(8).toString()+"\n");
					}
				}
			}//for (int j=0; j<vecFilas2.size(); j++)		
		}//if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito))
	}//for (int i = 0;i<vecFilas.size();i++) 
	
	if(numRegistros!=0){
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
	

}catch(Exception e){
	out.println(e);
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
}
%>
<%=jsonObj%>

<%System.out.println("24consulta03extCSV.jsp (S)");%>
