<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.CatalogoSimple,
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	java.beans.XMLEncoder"	
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
System.out.println("24reporte02ext.data.jsp (E)");
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String tipoArchivo = request.getParameter("tipoArchivo") == null?"":(String)request.getParameter("tipoArchivo");
String	noPyme = iNoCliente;
String	noEstatus	=	(request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
String	estatus	=	(request.getParameter("noEstatus")==null)?"":request.getParameter("noEstatus");

if(!estatus.equals("")){
	noEstatus = estatus;
}
String infoRegresar = "", tituloGrid="" , registrosStr="";  
AccesoDB con = new AccesoDB();
int start = 0;
int limit = 0;
JSONObject jsonObj = new JSONObject();
if(noEstatus.equals("23")){
	tituloGrid = "En Proceso de Autorizacion IF a Negociable";
} else if(noEstatus.equals("34")){
	tituloGrid = "En Proceso de Autorizacion IF a Seleccionado Pyme";
}
try {
	con.conexionDB();
				
	if (informacion.equals("catalogoEstatus") ) {
		 
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_cambio_estatus"); 
		catalogo.setCampoDescripcion("cd_descripcion"); 
		catalogo.setTabla("comcat_cambio_estatus");
		catalogo.setValoresCondicionIn("23,34,2,21,24",java.lang.Integer.class);	
		catalogo.setOrden("cd_descripcion");			
		infoRegresar =  catalogo.getJSONElementos();
		
	}	   
	
		CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	
		//obtengo parametrizacion de Tipo Credito	
		String tiposCredito  = cargaDocto.getTiposCredito(noPyme,"");		
		//obtengo resultado de reportes 
		
		List reporte  = cargaDocto.repCambioEstatus(noPyme, noEstatus,  tiposCredito);
		
		List reporte23 = new ArrayList();
		List reporte2 = new ArrayList();
		List reporte22 = new ArrayList();
		List reporte24 = new ArrayList();
		List reporte21 = new ArrayList();
		
		List totales14 = new ArrayList();
		List totales2 = new ArrayList();
		List totales22 = new ArrayList();
		List totales24 = new ArrayList();
		List totales21 = new ArrayList();
	 for(int i = 0; i<reporte.size(); i++) {
		 reporte23 =(List)reporte.get(0);
		 reporte2 = (List)reporte.get(1);
		 reporte22 = (List)reporte.get(2);
		 reporte24 = (List)reporte.get(3);
		 reporte21 = (List)reporte.get(4);

		 totales14 = (List)reporte.get(5);
 
		 totales2 = (List)reporte.get(6);
		 totales22 = (List)reporte.get(7);
		 totales24 = (List)reporte.get(8);
		 totales21 = (List)reporte.get(9);	 
		 
	 }
	 
System.out.println("informacion  "+informacion);
	if(informacion.equals("ConsultaGeneral") ){
			infoRegresar = "({\"success\": true,\"tituloGrid\": \"" + tituloGrid + "\", \"total\": \"" + reporte23.size() + "\", \"registros14\": " + reporte23.toString()+"})";
		}else if(informacion.equals("Consulta02") ){
			infoRegresar = "({\"success\": true, \"total\": \"" + reporte2.size() + "\", \"registros2\": " + reporte2.toString()+"})";
		}else if(informacion.equals("Consulta22") ){
			infoRegresar = "({\"success\": true, \"total\": \"" + reporte22.size() + "\", \"registros22\": " + reporte22.toString()+"})";
		}else if(informacion.equals("Consulta24")  ){
			infoRegresar = "({\"success\": true, \"total\": \"" + reporte24.size() + "\", \"registros24\": " + reporte24.toString()+"})";
		}else if(informacion.equals("Consulta21") ){
			infoRegresar = "({\"success\": true, \"total\": \"" + reporte21.size() + "\", \"registros21\": " + reporte21.toString()+"})";
		}
		 
		// System.out.println("infoRegresar  "+infoRegresar);
		 
		 if(informacion.equals("totales14") ){	
				infoRegresar = "({\"success\": true, \"total\": \"" + totales14.size() + "\", \"totales14\": " + totales14.toString()+"})";
		}else  if(informacion.equals("totales2") ){
			infoRegresar = "({\"success\": true, \"total\": \"" + totales2.size() + "\", \"totales2\": " + totales2.toString()+"})";
		}else  if(informacion.equals("totales22") ){
			infoRegresar = "({\"success\": true, \"total\": \"" + totales22.size() + "\", \"totales22\": " + totales22.toString()+"})";
		}else  if(informacion.equals("totales24") ){
			infoRegresar = "({\"success\": true, \"total\": \"" + totales24.size() + "\", \"totales24\": " + totales24.toString()+"})";
		}else  if(informacion.equals("totales21") ){
			infoRegresar = "({\"success\": true, \"total\": \"" + totales21.size() + "\", \"totales21\": " + totales21.toString()+"})";
		}
		
		 
//System.out.println("informacion  "+informacion);
	if(informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF") ){
	
		List datos = new ArrayList();
		
		String pais =(String)session.getAttribute("strPais");
		String nafinElectronico = ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString());
		String noExterno = 	(String)session.getAttribute("sesExterno");
		String nombre = 	(String) session.getAttribute("strNombre");
		String nombreUsua =	(String) session.getAttribute("strNombreUsuario");
		String logo = (String)session.getAttribute("strLogo");
		String directorioPubli = (String) application.getAttribute("strDirectorioPublicacion");
	
		datos.add(pais);
		datos.add(nafinElectronico);
		datos.add(noExterno);
		datos.add(nombre);
		datos.add(nombreUsua);
		datos.add(logo);
		datos.add(strDirectorioTemp);
		datos.add(noPyme);
		datos.add(tipoArchivo);
		datos.add(noEstatus);
		datos.add(tiposCredito);
		datos.add(directorioPubli);
		
			
		String nombreArchivo = cargaDocto.repCambioEstatusCSVPDF(datos);
		
	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				
	}
	

	
} catch(Exception e) { 
	out.println(e.getMessage()); 
} finally { 
	if (con.hayConexionAbierta()) {
			con.cierraConexionDB();	
	}
}

System.out.println("tipoArchivo--->"+tipoArchivo+"---");
System.out.println("infoRegresar--->"+infoRegresar+"---");

%>
<%if(tipoArchivo.equals("")){ %>
<%=infoRegresar%>
<%}else{%>
<%=jsonObj%>
<%}%>

<%System.out.println("24reporte02ext.data.jsp (S)");%>


