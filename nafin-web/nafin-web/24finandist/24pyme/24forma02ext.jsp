<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
	
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>
<%
	String  mostrarTexto ="";
	//INSTANCIACION DE EJB'S
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
		
	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

	AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);

	String diasInhabiles = aceptPyme.getDiasInhabiles(iNoCliente,"");
	String diasInhabilesXAnio =  aceptPyme.getDiasInhabilesXanio();  

	String fechaHoy		= "";
	try{
		fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	}catch(Exception e){
		fechaHoy = "";
	}


if("PYME".equals(strTipoUsuario)) {
	try{
			Horario.validarHorario(4, strTipoUsuario, iNoEPO);		
		
	}catch(NafinException ne){
		ne.printStackTrace();
		mostrarTexto ="Horario";
	}catch(Exception e){
		e.printStackTrace();
		e.printStackTrace();
	}
}
if(mostrarTexto.equals("")){
	try{
		aceptPyme.cambiaTipoFinanciamiento(iNoCliente);
		
	}catch(NafinException ne){
		ne.printStackTrace();
		mostrarTexto ="Vencido";
	}catch(Exception e){
		e.printStackTrace();
		e.printStackTrace();
	}
}

String tipoCredito = BeanParametros.obtieneTipoCredito (iNoEPO); 

String parTipoCambio = aceptPyme.getTipoCambio(iNoCliente);

String operaContrato = BeanParametros.obtieneOperaSinCesion(iNoEPO); //PARAMETRO NUEVO JAGE 14012017

%>
	
<html>
<head>
<title>.:: N@fin Electr?nico :: Financiamiento a Distribuidores ::.</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf"%>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/00utils/extjs/ux/IFramePanel.js"></script>
<script type="text/javascript" src="/nafin/00utils/NEcesionDerechos.js?<%=session.getId()%>"></script>
<!--<%@ include file="/00utils/componente_firma.jspf" %>-->

<%-- El jsp que contiene el dise?o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
<%
if (afiliacion.pymeBloqueada(iNoCliente, "4") ) {	%>
<script type="text/javascript" src="24forma1bloqueoext.js?<%=session.getId()%>"></script>
<% mostrarTexto ="bloquedado";%>
<% }else if (mostrarTexto.equals("Horario") ){%>
<script type="text/javascript" src="24forma1bloqueoext.js?<%=session.getId()%>"></script>
		<% mostrarTexto ="Horario";%>		
<%}else if (mostrarTexto.equals("Vencido") ){%>
<script type="text/javascript" src="24forma02Bloqueoext.js?<%=session.getId()%>"></script>
		<% mostrarTexto ="Vencido";%>
<% } else if (tipoCredito.equals("C") || tipoCredito.equals("A")) { %>
<script type="text/javascript" src="24forma02ext.js?<%=session.getId()%>"></script>
<%}else {%>
<script type="text/javascript" src="24forma1bloqueoext.js?<%=session.getId()%>"></script>
		<% mostrarTexto ="NOOpera";%>
<%}%>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">


<%@ include file="/01principal/01pyme/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01pyme/menuLateralFlotante.jspf"%>
	<div id="areaContenido"><div style="height:190px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01pyme/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>

<form id='formParametros' name="formParametros">
	<input type="hidden" id="mostrarTexto" name="mostrarTexto" value="<%=mostrarTexto%>"/>
	<input type="hidden" id="parTipoCambio" name="parTipoCambio" value="<%=parTipoCambio%>"/>
	<input type="hidden" id="fechaHoy" name="fechaHoy" value="<%=fechaHoy%>"/>
	<input type="hidden" id="diasInhabiles" name="diasInhabiles" value="<%=diasInhabiles%>"/>		
	<input type="hidden" id="strNombreUsuario" name="strNombreUsuario" value="<%=strNombreUsuario%>"/>
	<input type="hidden" id="diasInhabilesXAnio" name="diasInhabilesXAnio" value="<%=diasInhabilesXAnio%>"/>
        <input type="hidden" id="operaContrato" name="operaContrato" value="<%=operaContrato%>"/>
</form>

</body>
</html>