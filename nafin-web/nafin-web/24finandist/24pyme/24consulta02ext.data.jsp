<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.anticipos.*,
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String cboEpo	=	request.getParameter("ic_epo") != null ? request.getParameter("ic_epo") : "";
String numDocto = request.getParameter("numDoc") != null ? request.getParameter("numDoc") : "";
String infoRegresar	=	"";
String tipoCobro= "";

if(informacion.equals("CatalogoEPODistribuidores")) {
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClavePyme(iNoCliente);
	infoRegresar = cat.getJSONElementos();
}
else if(
		informacion.equals("Consulta") ||
		informacion.equals("ArchivoCSV") ||
		informacion.equals("ArchivoPDF")){
	com.netro.distribuidores.ConsDetallePagInteres paginador = new com.netro.distribuidores.ConsDetallePagInteres();
	paginador.setClavePyme(iNoCliente);
	paginador.setNumeroDocto(numDocto);
	paginador.setClaveEPO(cboEpo);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	if(informacion.equals("Consulta")){
		try {
			Registros reg = queryHelper.doSearch();
			infoRegresar =
				"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		} catch(Exception e) {
			throw new AppException("Error en la paginación");
		}
	} else if (informacion.equals("ArchivoCSV")) {
		JSONObject jsonObj = new JSONObject();
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		}catch(Throwable e) {
			jsonObj.put("success",new Boolean(false));
			throw new AppException("Error al generar el archivo CSV", e);
		}
		infoRegresar = jsonObj.toString();
	} else if (informacion.equals("ArchivoPDF")) {
		JSONObject jsonObj = new JSONObject();
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();
	}
}
%>
<%=infoRegresar%>


