<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*, java.sql.*, java.math.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String fechaHoy		= "";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}
String informacion               = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_epo                    = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String tipo_credito              = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
String cgTipoConversion          = (request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
String publicaDoctosFinanciables = (request.getParameter("publicaDoctosFinanciables")!=null)?request.getParameter("publicaDoctosFinanciables"):"";

CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

if("".equals(cgTipoConversion)){
	cgTipoConversion = cargaDocto.getTipoConversion(iNoCliente);
}
String epo 					= "";
String numDocto			= "";
String numAcuseCarga		= "";
String fechaEmision		= "";
String fechaPublicacion	= "";
String fechaVencimiento	= "";
String plazoDocto			= "";
String moneda 				= "";
String icMoneda			= "";
double monto 				= 0;
String tipoConversion	= "";
double tipoCambio			= 0;
double montoValuado		= 0;
String categoria			= "";
String plazoDescuento	= "";
String porcDescuento		= "";
double montoDescontar	= 0;
String modoPlazo			= "";
String estatus 			= "";
String icDocumento		= "";
String intermediario		= "";
String tipoCredito		= "";
String fechaOperacion	= "";
double montoCredito		= 0;
String plazoCredito		= "";
String fechaVencCredito	= "";
String referenciaTasaInt= "";
//double valorTasaInt		= 0;
String valorTasaInt		= "";
String montoTasaInt 		= "";
String tipoCobroInt		= ""; 
String monedaLinea		= "";
String bandeVentaCartera= "";

//FODEA-013-2014
String idOrdenEnviado = "";
String idOperacion = "";
String codigoAutorizacion = "";
String fechaRegistro = "";
String bins = "";

String operaTarjeta = "";
String numTarjeta = "";
String tipo_pago = "";

AccesoDB con = new AccesoDB();
try {
	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;
	if (informacion.equals("ArchivoCSV")) {
		CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsInfDocPymeDist());
		queryHelper.setMultiplesPaginadoresXPagina(true);
		con.conexionDB();
		ResultSet	rs = null;
		rs = queryHelper.getCreateFile(request,con);
		int aRow = 0;
		/*******************
		Cabecera del archivo
		********************/
		contenidoArchivo.append("EPO,Número de documento inicial,Num. acuse carga,Fecha de emisión,Fecha de publicación,Fecha vencimiento,Plazo docto,Moneda,Monto,Categoría, Plazo para descuento,% de descuento,Monto a descontar");
		if(!"".equals(cgTipoConversion))	{
			contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado");
		}
		contenidoArchivo.append(",Modalidad de plazo");
		if(publicaDoctosFinanciables.equals("S")){
			contenidoArchivo.append(", Tipo de pago");
		}
		contenidoArchivo.append(", Estatus");
		/***************************
		 Generacion del archivo
		/***************************/
		while(rs.next()){
			epo 					= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
			numDocto				= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
			numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
			fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
			fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
			fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
			plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
			moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
			monto 				= rs.getDouble("FN_MONTO");
			tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
			tipoCambio			= rs.getDouble("TIPO_CAMBIO");
			plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
			porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
			montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
			montoValuado		= (monto-montoDescontar)*tipoCambio;
			modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
			estatus 				= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
			icMoneda				= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
			icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
			monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
			categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
			bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
			tipo_pago = (rs.getString("IG_TIPO_PAGO")==null)?"":rs.getString("IG_TIPO_PAGO");
			if(tipo_pago.equals("1")){
				tipo_pago = "Financiamiento con intereses";
			} else if(tipo_pago.equals("2")){
				tipo_pago = "Meses sin intereses";
			}
			if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
			}
			contenidoArchivo.append("\n"+epo.replace(',',' ')+","+numDocto+","+numAcuseCarga+","+fechaEmision+","+fechaPublicacion+","+fechaVencimiento+","+plazoDocto+","+moneda+","+monto+","+categoria+","+plazoDescuento+","+porcDescuento+","+montoDescontar);
			if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:"") );
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?""+Comunes.formatoDecimal(tipoCambio,2,false):""));
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?""+Comunes.formatoDecimal(montoValuado,2,false):""));
			}
			contenidoArchivo.append(","+modoPlazo);
			if(publicaDoctosFinanciables.equals("S")){
			contenidoArchivo.append(","+tipo_pago);
			}
			contenidoArchivo.append(","+estatus);
			aRow++;
		}
		if (aRow == 0)	{
			contenidoArchivo.append("\nNo se Encontró Ningún Registro");
		}
	} else if (informacion.equals("ArchivoBCSV")) {
		int bRow = 0;
		CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsInfDocPymeDist2());
		queryHelper.setMultiplesPaginadoresXPagina(true);
		con.conexionDB();
		ResultSet	rs = null;
		rs = queryHelper.getCreateFile(request,con);
		/*******************
		Cabecera del archivo
		********************/
		contenidoArchivo.append("\nEPO,Número de documento inicial,Num. acuse carga,Fecha de emisión,Fecha de publicación,Fecha vencimiento,Plazo docto,Moneda,Monto,Categoría,  % de Descuento Aforo , Monto a Descontar, Plazo para descuento,% de descuento,Monto % de descuento");
		if(!"".equals(cgTipoConversion))	{
			contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado");
		}
		//contenidoArchivo.append(",Modalidad de plazo,Estatus,Número de docto. final, monto,plazoCredito,fechaVencCredito,fechaOperacion,intermediario,referenciaTasaInt,valorTasaInt,montoTasaInt");
		contenidoArchivo.append(",Modalidad de plazo");
		if(publicaDoctosFinanciables.equals("S")){
			contenidoArchivo.append(", Tipo de pago");
		}
		contenidoArchivo.append(",Estatus,Número de docto. final, Monto,Plazo,Fecha Venc.,Fecha Operacion,Intermediario,Nombre del Producto,Referencia Tasa Int,Valor Tasa Int,Monto Tasa Int,ID Orden enviado,Respuesta de Operación,CódigoAutorización,Número Tarjeta de Crédito");

		/***************************
		 Generacion del archivo
		/***************************/
		while(rs.next()){
			epo 					= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
			numDocto				= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
			numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
			fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
			fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
			fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
			plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
			moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
			monto 				= rs.getDouble("FN_MONTO");
			tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
			tipoCambio			= rs.getDouble("TIPO_CAMBIO");
			plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
			porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
			montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
			montoValuado		= (monto-montoDescontar)*tipoCambio;
			modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
			estatus 				= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
			icMoneda				= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
			icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
			intermediario		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
			tipoCredito 		= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
			fechaOperacion		= (rs.getString("DF_OPERACION_CREDITO")==null)?"":rs.getString("DF_OPERACION_CREDITO");
			montoCredito		= monto-montoDescontar;
			plazoCredito		= (rs.getString("PLAZO_CREDITO")==null)?"":rs.getString("PLAZO_CREDITO");
			fechaVencCredito	= (rs.getString("DF_VENC_CREDITO")==null)?"":rs.getString("DF_VENC_CREDITO");
			referenciaTasaInt	= (rs.getString("REF_TASA")==null)?"":rs.getString("REF_TASA");
			valorTasaInt		= rs.getString("FN_VALOR_TASA");
			montoTasaInt 		= rs.getString("FN_IMPORTE_INTERES");
			tipoCobroInt		= (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"); 
			monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
			categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
			bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
			String porc_Aforo	= (rs.getString("POR_AFRO")==null)?"0":rs.getString("POR_AFRO"); //F05-2014 
			BigDecimal  por_Aforo= new BigDecimal(porc_Aforo);  //F05-2014 
			BigDecimal  montoD= new BigDecimal(monto);  //F05-2014 /
			BigDecimal   MontoDesconta = montoD.multiply(por_Aforo.divide(new BigDecimal("100"),10,BigDecimal.ROUND_HALF_UP));
			tipo_pago = (rs.getString("IG_TIPO_PAGO")==null)?"":rs.getString("IG_TIPO_PAGO");
			if(tipo_pago.equals("1")){
				tipo_pago = "Financiamiento con intereses";
			} else if(tipo_pago.equals("2")){
				tipo_pago = "Meses sin intereses";
			}
			//FODEA-013-2014
			operaTarjeta	= (rs.getString("OPERA_TARJETA")==null)?"":rs.getString("OPERA_TARJETA");
						idOrdenEnviado	= (rs.getString("ID_ORDEN_ENVIADO")==null)?"":rs.getString("ID_ORDEN_ENVIADO")+"'";
						idOperacion	= (rs.getString("ID_OPERACION")==null)?"":rs.getString("ID_OPERACION");
						codigoAutorizacion	= (rs.getString("CODIGO_AUTORIZADO")==null)?"":rs.getString("CODIGO_AUTORIZADO");
						//fechaRegistro	= (rs.getString("FECHA_REGISTRO")==null)?"":rs.getString("FECHA_REGISTRO");
						numTarjeta	= (rs.getString("NUM_TC")==null)?"":rs.getString("NUM_TC");
						bins	= (rs.getString("BINS")==null)?"":rs.getString("BINS");
			
			if(!"Operada TC".equals(estatus)){//FODE-013-2014
						idOrdenEnviado	= "N/A";
						idOperacion	= "N/A";
						codigoAutorizacion	= "N/A";
						//fechaRegistro	= "N/A";
						operaTarjeta	= "N/A";
						bins	= "N/A";
						numTarjeta = "N/A";
						
						
					}else{//FODE-013-2014
						fechaVencimiento = "N/A";
						plazoDocto		  = "N/A";
						referenciaTasaInt	= "N/A";
						valorTasaInt		= "N/A";
						montoTasaInt		= "N/A";
						plazoCredito		= "N/A";
						fechaVencCredito	= "N/A";
						///
						
					}
			
			if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
			}
			if(!icMoneda.equals(monedaLinea))	{
				montoCredito	= montoValuado;
			}else 	{
				montoCredito	= monto-montoDescontar;
			}
			contenidoArchivo.append("\n"+epo.replace(',',' ')+","+numDocto+","+numAcuseCarga+","+fechaEmision+","+fechaPublicacion+","+fechaVencimiento+","+plazoDocto+","+moneda+","+monto+","+categoria+","+
			
			porc_Aforo+","+ Comunes.formatoDecimal(MontoDesconta.toPlainString() ,2,false) +","+	plazoDescuento+","+porcDescuento+","+montoDescontar);
			if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:"") );
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?""+Comunes.formatoDecimal(tipoCambio,2,false):""));
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?""+Comunes.formatoDecimal(montoValuado,2,false):""));
			}
			contenidoArchivo.append(","+modoPlazo);
			if(publicaDoctosFinanciables.equals("S")){
			contenidoArchivo.append(","+tipo_pago);
			}
			contenidoArchivo.append(","+estatus);
			if (!estatus.equals("Negociable")){
				contenidoArchivo.append(","+icDocumento+","+montoCredito+","+plazoCredito+","+fechaVencCredito+","+fechaOperacion+","+intermediario.replace(',', '.')+","+bins.replace(',', '.')+","+referenciaTasaInt.replace(',', '.')+","+valorTasaInt+","+montoTasaInt+","+idOrdenEnviado.replace(',', '.')+","+idOperacion.replace(',', '.')+","+codigoAutorizacion.replace(',', '.')+","+((numTarjeta.replace(',', '.').trim()!="N/A" && numTarjeta.replace(',', '.').trim()!="")?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta));
			} else{
				contenidoArchivo.append(",,,,,,,,,");
			}
			bRow++;
		}
		if (bRow == 0)	{
			contenidoArchivo.append("\nNo se Encontró Ningún Registro");
		}
	}
	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
%>
<%=jsonObj%>