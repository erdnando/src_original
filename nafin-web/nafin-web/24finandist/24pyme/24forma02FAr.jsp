<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	JSONObject jsonObj = new JSONObject();	
	String ses_ic_pyme = iNoCliente;
	//PARAMETROS QUE PROVIENEN DE LA PAGINA ACTUAL O ANTERIOR
	String modificados[]		= request.getParameterValues("modificado");
	String ic_documentos[]		= request.getParameterValues("ic_documento");
	String ic_monedas[]			= request.getParameterValues("ic_moneda_docto");
	String ic_monedas_linea[]	= request.getParameterValues("ic_moneda_linea");
	String montos[]				= request.getParameterValues("monto");
	String montos_valuados[]	= request.getParameterValues("monto_valuado");
	String montos_credito[]		= request.getParameterValues("monto_credito");
	String montos_tasa_int[]	= request.getParameterValues("monto_tasa_int");
	String montos_descuento[]	= request.getParameterValues("monto_descuento");
	String plazos_credito[]		= request.getParameterValues("plazo");
	String fechas_vto[]			= request.getParameterValues("fecha_vto");
	String referencias_tasa[]	= request.getParameterValues("referencia_tasa");
	String valores_tasa[]		= request.getParameterValues("valor_tasa_puntos");
	String ic_tasa[]			= request.getParameterValues("ic_tasa");
	String cg_rel_mat[]		= request.getParameterValues("cg_rel_mat");
	String fn_puntos[]			= request.getParameterValues("fn_puntos");
	String lineaTodos			= (request.getParameter("lineaTodos")==null)?"":request.getParameter("lineaTodos");
	String tipoArchivo			= (request.getParameter("tipoArchivo")==null)?"":request.getParameter("tipoArchivo");
	String informacion = (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
	String acuse			= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
	
	//VARIABLES DE USO LOCAL
	Vector	vecFilas	= null;	
	Vector	vecColumnas	= null;	
	int	totalDoctosMN	= 0,  totalDoctosUSD	= 0,  totalDoctosConv= 0, 	i = 0, j = 0,  plazoactual =0; 
	double totalMontoMN	= 0, totalMontoUSD = 0, totalMontoDescMN= 0, totalMontoDescUSD	= 0, totalInteresMN = 0, totalInteresUSD	= 0,
			totalMontoCreditoMN	= 0, totalMontoCreditoUSD	= 0,  totalMontoCreditoConv= 0, totalMontosConv	= 0, totalMontoDescConv	= 0,
			totalInteresConv		= 0, totalConvPesos		= 0, fnMonto = 0,  montoValuado =	0,  montoDescuento =	0,  tasaInteres= 	0,
			valorTasaInt	=	0, montoTasaInt =	0, montoCapitalInt = 	0, fnPuntos	= 	0, montoCredito=	0;
			
	String in= "", icMoneda = "", fechaHoy	= "", nombreEpo =	"", igNumeroDocto	=	"", ccAcuse =	"",  dfFechaEmision	=	"", 
			dfFechaVencimiento	=	"", dfFechaPublicacion 	= "",igPlazoDocto	=	"", moneda ="", cgTipoConv	=	"", tipoCambio	=   "",
			igPlazoDescuento		= 	"", fnPorcDescuento	= 	"", modoPlazo	= 	"", estatus =	"", cambios	= 	"", numeroCredito	= 	"",
			nombreIf	= 	"", tipoLinea	=	"", fechaOperacion =	"", referencia	=	"",  plazoCredito	=	"", fechaVencCredito	=	"",
			relMat =	"", tipoCobroInt	=	"",  tipoPiso	=	"",  icTipoCobroInt	=	"", icTasa	=	"",  icLineaCredito		= 	"", 
			monedaLinea	=	"", nombreMLinea	= 	"", epo ="", inhabilEpoPyme = "",  diahabil ="", fechaVencimiento ="", icTipoFinanciamiento ="",
			cuentaBancaria ="", acuseFormateado	="", usuario ="", fechaCarga ="", horaCarga ="";					
	String mensaje ="En este acto manifiesto mi aceptación para que sea(n) descontado(s) el(los) DOCUMENTO(S) que he seleccionado, mismo(s) que contiene(n) Derechos de Cobro a mi cargo y a favor de la Empresa de Primer Orden por lo que en esta misma fecha me obligo a cubrir al Intermediario Financiero, los intereses que se detallan en esta pantalla. Manifiesto también mi obligación y aceptación de pagar el 100% del valor del(los) DOCUMENTO(S) en la fecha de vencimiento al Intermediario Financiero.";
		
	String contentType = "text/html;charset=ISO-8859-1";
	String 		nombreArchivo 	= null;	
	CreaArchivo archivo  = new CreaArchivo();
	archivo 			    = new CreaArchivo();
	ComunesPDF pdfDoc = new ComunesPDF();
	StringBuffer contenidoArchivo = new StringBuffer();
	
	try{
		//INSTANCIACION DE EJB'S
		AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
		Horario.validarHorario(4, strTipoUsuario, iNoEPO);	
		fechaHoy 	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		
		for(i=0;i<ic_documentos.length;i++){
			if("1".equals(ic_monedas[i])){
				totalDoctosMN ++;
				totalMontoMN		+= Double.parseDouble(montos[i]);
				totalMontoDescMN	+= Double.parseDouble(montos_descuento[i]);
				totalInteresMN		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);				
				totalMontoCreditoMN+= Double.parseDouble(montos_credito[i]);
			}
			if("54".equals(ic_monedas[i])&&"54".equals(ic_monedas_linea[i])){
				totalDoctosUSD ++;
				totalMontoUSD		+= Double.parseDouble(montos[i]);
				totalMontoDescUSD	+= Double.parseDouble(montos_descuento[i]);
				totalInteresUSD		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);
				totalMontoCreditoUSD+= Double.parseDouble(montos_credito[i]);
			}
			if("54".equals(ic_monedas[i])&&"1".equals(ic_monedas_linea[i])){
				totalDoctosConv ++;
				totalMontosConv			+= Double.parseDouble(montos[i]);
				totalMontoDescConv 		+= Double.parseDouble(montos_descuento[i]);
				totalInteresConv		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);
				totalConvPesos			+= Double.parseDouble(montos_descuento[i]);
				totalMontoCreditoConv	+= Double.parseDouble(montos_credito[i]);
			}
			if(!"".equals(in))
				in += ",";
				in += ic_documentos[i];			
		} //for
					
		//encabezado de Archivo PDF
		String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
		String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
		String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
		String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
		String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		if(informacion.equals("ArchivoPDFPreAcuse") || informacion.equals("ArchivoPDFAcuse")) {
			if(informacion.equals("ArchivoPDFPreAcuse")) {
				vecFilas = aceptPyme.consultaDoctosFactRecurso(ses_ic_pyme,"","","","","", "","","","","","","",lineaTodos,in);
			}
			nombreArchivo 	  = archivo.nombreArchivo()+".pdf";		
			pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
			pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String) application.getAttribute("strDirectorioPublicacion"), "");
			
			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	
			pdfDoc.addText("Selección de Documentos Factoraje con Recuerso","formas",ComunesPDF.CENTER);
			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	
			
			pdfDoc.setTable(12, 100);
			pdfDoc.setCell("Moneda nacional","celda01",ComunesPDF.CENTER,4);
			pdfDoc.setCell("Dólares Americanos","celda01",ComunesPDF.CENTER,4);
			pdfDoc.setCell("Doctos. en DLL financiados en M.N.","celda01",ComunesPDF.CENTER,4);
			pdfDoc.setCell("Num. total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto del Financiamiento","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto del Financiamiento","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de doctos.","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Monto del Financiamiento","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);	
		
			pdfDoc.setCell(String.valueOf(totalDoctosMN) ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalInteresMN,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(String.valueOf(totalDoctosUSD) ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalInteresUSD,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(String.valueOf(totalDoctosConv) ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontosConv,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoCreditoConv,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalInteresConv,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(mensaje,"formas",ComunesPDF.JUSTIFIED,12);
			pdfDoc.addTable();	
			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	
			
			pdfDoc.setTable(12, 100);			
			pdfDoc.setCell("Documento","celda01",ComunesPDF.CENTER,16);					
			pdfDoc.setCell("Epo","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Num. docto. inicial","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. acuse","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de emisión","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Fecha vencimiento","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Fecha de publicación","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Plazo docto.","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. de docto. final","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Monto Crédito","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Fecha de operación","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Referencia tasa de interés","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Valor tasa de interés","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Monto de intereses","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Monto a Recibir","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Cuenta Bancaria Pyme","celda01",ComunesPDF.CENTER);	
			
		}
		
		if( informacion.equals("ArchivoCSVAcuse") && tipoArchivo.equals("CSV")) {
		
			contenidoArchivo.append("Moneda nacional ,,,,");
			contenidoArchivo.append("Dólares Americanos,,,,");
			contenidoArchivo.append("Doctos. en DLL financiados en M.N., \n ");
			contenidoArchivo.append("Num. total de doctos.,");
			contenidoArchivo.append("Monto total de doctos.,");
			contenidoArchivo.append("Monto del Financiamiento,");
			contenidoArchivo.append("Monto intereses,");
			contenidoArchivo.append("Num. total de doctos.,");
			contenidoArchivo.append("Monto total de doctos.,");
			contenidoArchivo.append("Monto del Financiamiento,");
			contenidoArchivo.append("Monto intereses ,");
			contenidoArchivo.append("Num. total de doctos.,");
			contenidoArchivo.append("Monto total de doctos., ");	
			contenidoArchivo.append("Monto del Financiamiento,");
			contenidoArchivo.append("Monto intereses \n");
			
			contenidoArchivo.append(String.valueOf(totalDoctosMN)+",");
			contenidoArchivo.append("$"+ Comunes.formatoDecimal(totalMontoMN,2)+",");
			contenidoArchivo.append("$"+ Comunes.formatoDecimal(totalMontoCreditoMN,2)+",");
			contenidoArchivo.append("$"+ Comunes.formatoDecimal(totalInteresMN,2)+",");
			contenidoArchivo.append(String.valueOf(totalDoctosUSD)+",");
			contenidoArchivo.append("$"+ Comunes.formatoDecimal(totalMontoUSD,2)+",");
			contenidoArchivo.append("$"+ Comunes.formatoDecimal(totalMontoCreditoUSD,2)+",");
			contenidoArchivo.append("$"+ Comunes.formatoDecimal(totalInteresUSD,2)+",");
			contenidoArchivo.append(String.valueOf(totalDoctosConv)+",");
			contenidoArchivo.append("$"+ Comunes.formatoDecimal(totalMontosConv,2)+",");
			contenidoArchivo.append("$"+ Comunes.formatoDecimal(totalMontoCreditoConv,2)+",");
			contenidoArchivo.append("$"+ Comunes.formatoDecimal(totalInteresConv,2)+",");
			contenidoArchivo.append("\n"+mensaje.replace(',',' ')+"\n");
					
			contenidoArchivo.append("\n");
			contenidoArchivo.append(" Datos Documento Inicial ,,,,,,,,,");
			contenidoArchivo.append(" Datos Documento Final"+"\n");								
			contenidoArchivo.append("Epo,");
			contenidoArchivo.append("Num. docto. inicial,");
			contenidoArchivo.append("Num. acuse,");
			contenidoArchivo.append("Fecha de emisión,");
			contenidoArchivo.append("Fecha vencimiento,");
			contenidoArchivo.append("Fecha de publicación,");
			contenidoArchivo.append("Plazo docto.,");
			contenidoArchivo.append("Moneda,");
			contenidoArchivo.append("Monto,");
			contenidoArchivo.append("Num. de docto. final,");
			contenidoArchivo.append("Moneda,");
			contenidoArchivo.append("Monto Crédito,");
			contenidoArchivo.append("Plazo,");
			contenidoArchivo.append("Fecha de vencimiento,");
			contenidoArchivo.append("Fecha de operación,");
			contenidoArchivo.append("IF,");
			contenidoArchivo.append("Referencia tasa de interés,");
			contenidoArchivo.append("Valor tasa de interés,");
			contenidoArchivo.append("Monto de intereses,");
			contenidoArchivo.append("Monto a Recibir,");
			contenidoArchivo.append("Cuenta Bancaria Pyme \n");		
		}
		
		if(informacion.equals("ArchivoPDFPreAcuse")) {
		
			if(vecFilas.size()>0) {
				for(i=0;i<vecFilas.size();i++,j++){				
					vecColumnas = (Vector)vecFilas.get(i);
					nombreEpo			=	(String)vecColumnas.get(0);
					igNumeroDocto		=	(String)vecColumnas.get(1);
					ccAcuse 			=	(String)vecColumnas.get(2);
					dfFechaEmision		=	(String)vecColumnas.get(3);
					dfFechaVencimiento	=	(String)vecColumnas.get(4);
					dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
					igPlazoDocto		=	(String)vecColumnas.get(6);
					moneda 				=	(String)vecColumnas.get(7);
					fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
					cgTipoConv			=	(String)vecColumnas.get(9);
					tipoCambio			=   (String)vecColumnas.get(10);
					montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
					igPlazoDescuento	= 	(String)vecColumnas.get(12);
					fnPorcDescuento		= 	(String)vecColumnas.get(13);
					montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
					modoPlazo			= 	(String)vecColumnas.get(14);
					estatus 			=	(String)vecColumnas.get(15);
					cambios				= 	(String)vecColumnas.get(16);
					//creditos por concepto de intereses
					numeroCredito		= 	(String)vecColumnas.get(17);
					nombreIf			= 	(String)vecColumnas.get(18);
					tipoLinea			=	(String)vecColumnas.get(19);
					fechaOperacion		=	(String)vecColumnas.get(20);
					plazoCredito		=	(String)vecColumnas.get(22);
					fechaVencCredito	=	(String)vecColumnas.get(23);
					relMat				= 	(String)vecColumnas.get(25);
					tipoCobroInt		=	(String)vecColumnas.get(27);
					referencia			=	(String)vecColumnas.get(29);
					tipoPiso			= 	(String)vecColumnas.get(30);
					icTipoCobroInt		= 	(String)vecColumnas.get(31);
					icMoneda			= 	(String)vecColumnas.get(32);
					icTasa				= 	(String)vecColumnas.get(33);
					monedaLinea			= 	(String)vecColumnas.get(35);
					icLineaCredito		= 	(String)vecColumnas.get(38);
					nombreMLinea		= 	(String)vecColumnas.get(40);
					fechaVencimiento =	(String)vecColumnas.get(47)==null?"":(String)vecColumnas.get(47); 
					icTipoFinanciamiento =  (String)vecColumnas.get(34)==null?"":(String)vecColumnas.get(34); 
					epo = (String)vecColumnas.get(48)==null?"":(String)vecColumnas.get(48);      
					if("+".equals(cg_rel_mat[j]))
						valorTasaInt = Double.parseDouble(valores_tasa[j])+Double.parseDouble(fn_puntos[j]);
					else if("-".equals(cg_rel_mat[j]))
						valorTasaInt = Double.parseDouble(valores_tasa[j])-Double.parseDouble(fn_puntos[j]);
					else if("*-".equals(cg_rel_mat[j]))
						valorTasaInt = Double.parseDouble(valores_tasa[j])*Double.parseDouble(fn_puntos[j]);
					else if("/".equals(cg_rel_mat[j]))
						valorTasaInt = Double.parseDouble(valores_tasa[j])/Double.parseDouble(fn_puntos[j]);
		
					plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
					if("1".equals(tipoPiso)){
						montoTasaInt = (double)Math.round((montoValuado*(valorTasaInt/100)/360)*100)/100;
					}else{
						montoTasaInt = (montoValuado*(valorTasaInt/100)/360);
					}
					montoCredito = montoValuado - (montoTasaInt*Double.parseDouble(plazoCredito));
					montoCapitalInt = Double.parseDouble(montos_credito[j])+Double.parseDouble(montos_tasa_int[j]);    
					plazoactual = Integer.parseInt(plazos_credito[j]);  
					diahabil = fechas_vto[j];	
					cuentaBancaria  = (String)vecColumnas.get(52)==null?"0":(String)vecColumnas.get(52);
					
					String intetes  = montos_tasa_int[j].toString();
					double montoRecibir = fnMonto-Double.parseDouble((String)intetes);					
			
					pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ccAcuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(dfFechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(dfFechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(dfFechaPublicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(igPlazoDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMonto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(numeroCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreMLinea,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":""+Comunes.formatoMoneda2(montos_credito[j],false),"formas",ComunesPDF.CENTER);					
					pdfDoc.setCell( String.valueOf(plazoactual),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(diahabil,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":referencias_tasa[j],"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?" ":" "+valores_tasa[j]+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoMoneda2(montos_tasa_int[j],false),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoRecibir,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cuentaBancaria,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
			}
		}else if(informacion.equals("ArchivoPDFAcuse") ||  informacion.equals("ArchivoCSVAcuse") ) {
								
			vecFilas = aceptPyme.consultaDoctosFactoRec(acuse) ;
			if(vecFilas.size()>0) {
				int mas =0;
				for(i=0;i<vecFilas.size();i++){
					vecColumnas = (Vector)vecFilas.get(i);
					nombreEpo			=	(String)vecColumnas.get(0);
					igNumeroDocto		=	(String)vecColumnas.get(1);
					ccAcuse 			=	(String)vecColumnas.get(2);
					dfFechaEmision		=	(String)vecColumnas.get(3);
					dfFechaVencimiento	=	(String)vecColumnas.get(4);
					dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
					igPlazoDocto		=	(String)vecColumnas.get(6);
					moneda 				=	(String)vecColumnas.get(7);
					fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
					cgTipoConv			=	(String)vecColumnas.get(9);
					tipoCambio			=   (String)vecColumnas.get(10);
					montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
					montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
					igPlazoDescuento	= 	(String)vecColumnas.get(12);
					fnPorcDescuento		= 	(String)vecColumnas.get(13);
					modoPlazo			= 	(String)vecColumnas.get(14);
					estatus 			=	(String)vecColumnas.get(15);
					cambios				= 	(String)vecColumnas.get(16);
					//creditos por concepto de intereses
					numeroCredito		= 	(String)vecColumnas.get(17);
					nombreIf			= 	(String)vecColumnas.get(18);
					tipoLinea			=	(String)vecColumnas.get(19);
					fechaOperacion		=	(String)vecColumnas.get(20);
					plazoCredito		=	(String)vecColumnas.get(22);
					fechaVencCredito	=	(String)vecColumnas.get(23);
					valorTasaInt		=	Double.parseDouble(vecColumnas.get(41).toString());
					relMat				= 	(String)vecColumnas.get(25);
					fnPuntos			=	Double.parseDouble(vecColumnas.get(26).toString());
					tipoCobroInt		=	(String)vecColumnas.get(27);
					referencia			=	(String)vecColumnas.get(29);
					tipoPiso			= 	(String)vecColumnas.get(30);
					icTipoCobroInt		= 	(String)vecColumnas.get(31);
					icMoneda			= 	(String)vecColumnas.get(32);
					acuseFormateado		=	(String)vecColumnas.get(34);
					usuario				= 	(String)vecColumnas.get(37);
					fechaCarga			= 	(String)vecColumnas.get(35);
					horaCarga			= 	(String)vecColumnas.get(36);
					icTipoFinanciamiento= 	(String)vecColumnas.get(39);
					monedaLinea			= 	(String)vecColumnas.get(38);
					nombreMLinea		= 	(String)vecColumnas.get(40);
					plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
					montoCredito = fnMonto -montoDescuento;
					if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
						if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
							montoCredito = montoValuado;
						else if("2".equals(icTipoFinanciamiento))
							montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
					}
					if("1".equals(tipoPiso)){
						montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
					}else{
						montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
					}
					
					montoCapitalInt = montoCredito +(montoTasaInt*Double.parseDouble(plazoCredito));
					String intetes  = montos_tasa_int[j].toString();					
					double montoRecibir = fnMonto-Double.parseDouble((String)intetes);						
					cuentaBancaria = (String)vecColumnas.get(42)==null?"0":(String)vecColumnas.get(42);
					
					if( tipoArchivo.equals("PDF")){
					
						pdfDoc.setCell(nombreEpo, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(igNumeroDocto, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(ccAcuse, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(dfFechaEmision, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(dfFechaVencimiento, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(dfFechaPublicacion, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(igPlazoDocto, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(moneda, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMonto,2), "formas", ComunesPDF.RIGHT);						
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);	
						
						pdfDoc.setCell(numeroCredito, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(nombreMLinea, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoCredito,2), "formas", ComunesPDF.RIGHT);
						pdfDoc.setCell("0".equals(plazoCredito)?" ":plazoCredito, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencCredito, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(fechaHoy, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(nombreIf, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell( ("S".equals(icTipoCobroInt))?"":referencia+" "+relMat+" "+fnPuntos, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(("S".equals(icTipoCobroInt))?" ":" "+Comunes.formatoDecimal(valorTasaInt,2)+"%", "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$ "+Comunes.formatoDecimal(montoTasaInt*Double.parseDouble(plazoCredito),2), "formas", ComunesPDF.RIGHT);
						pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoRecibir,2),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cuentaBancaria,"formas",ComunesPDF.CENTER);
					
					}else if( tipoArchivo.equals("CSV")){
					
						contenidoArchivo.append(nombreEpo.replace(',',' ')+",");
						contenidoArchivo.append(igNumeroDocto.replace(',',' ')+",");
						contenidoArchivo.append(ccAcuse.replace(',',' ')+",");
						contenidoArchivo.append(dfFechaEmision.replace(',',' ')+",");
						contenidoArchivo.append(dfFechaVencimiento.replace(',',' ')+",");
						contenidoArchivo.append(dfFechaPublicacion.replace(',',' ')+",");
						contenidoArchivo.append(igPlazoDocto.replace(',',' ')+",");
						contenidoArchivo.append(moneda.replace(',',' ')+",");
						contenidoArchivo.append("$"+Comunes.formatoDecimal(fnMonto,2).replace(',',' ')+",");
						contenidoArchivo.append(numeroCredito.replace(',',' ')+",");
						contenidoArchivo.append(nombreMLinea.replace(',',' ')+",");
						contenidoArchivo.append(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoCredito,2).replace(',',' ')+",");
						contenidoArchivo.append("0".equals(plazoCredito)?" ":plazoCredito.replace(',',' ')+",");
						contenidoArchivo.append(fechaVencCredito.replace(',',' ')+",");
						contenidoArchivo.append(fechaHoy.replace(',',' ')+",");
						contenidoArchivo.append(nombreIf.replace(',',' ')+",");
						contenidoArchivo.append( ("S".equals(icTipoCobroInt))?"":referencia+" "+relMat+" "+fnPuntos +"".replace(',',' ')+",");
						contenidoArchivo.append(("S".equals(icTipoCobroInt))?" ":" "+Comunes.formatoDecimal(valorTasaInt,2)+"%".replace(',',' ')+",");
						contenidoArchivo.append(("S".equals(icTipoCobroInt))?"":"$ "+Comunes.formatoDecimal(montoTasaInt*Double.parseDouble(plazoCredito),2).replace(',',' ')+",");
						contenidoArchivo.append(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoRecibir,2).replace(',',' ')+",");
						contenidoArchivo.append(cuentaBancaria.replace(',',' ')+"\n");				
					}					
					
				} //for
			
				if( tipoArchivo.equals("PDF")){
					pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	
					pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	
					pdfDoc.addTable();				
					pdfDoc.setTable(2, 40);
					pdfDoc.setCell("Datos de cifras de control", "celda02", ComunesPDF.CENTER, 2);
					pdfDoc.setCell("Num. acuse", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell(acuseFormateado, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell(fechaCarga, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("Hora", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell(horaCarga, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre y número de usuario", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell(iNoUsuario+" "+strNombreUsuario, "formas", ComunesPDF.CENTER);
					pdfDoc.addTable();
					
				}else if( tipoArchivo.equals("CSV")){
			
					contenidoArchivo.append(" "+"\n");
					contenidoArchivo.append("Datos de cifras de control"+"\n");
					contenidoArchivo.append("Num. acuse"+",");
					contenidoArchivo.append(acuseFormateado+"\n");
					contenidoArchivo.append("Fecha"+",");
					contenidoArchivo.append(fechaCarga+"\n");
					contenidoArchivo.append("Hora"+",");
					contenidoArchivo.append(horaCarga+"\n");
					contenidoArchivo.append("Nombre y número de usuario"+",");
					contenidoArchivo.append(iNoUsuario+" "+strNombreUsuario.replace(',',' ')+",");				
				}			
			} //if
		}

		if(informacion.equals("ArchivoPDFPreAcuse") || informacion.equals("ArchivoPDFAcuse") && tipoArchivo.equals("PDF")) {
			pdfDoc.endDocument();
		}
		if( informacion.equals("ArchivoCSVAcuse") && tipoArchivo.equals("CSV")) {	
			archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv");
			nombreArchivo = archivo.getNombre();
		}
	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	
	}catch(NafinException ne){
		out.println(ne.getMsgError());
	}catch(Exception e){
		out.println("Error "+e);
	}
%>

<%=jsonObj%>

