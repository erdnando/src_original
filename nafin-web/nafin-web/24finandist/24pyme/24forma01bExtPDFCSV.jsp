<%@ page import="java.util.*, 
java.sql.*, 
netropology.utilerias.*,
com.netro.exception.*,
javax.naming.*,
net.sf.json.JSONArray,
net.sf.json.JSONObject,
com.netro.pdf.*,		
com.netro.distribuidores.*"
contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>

<%
	//PARAMETROS QUE PROVIENEN DE LA PAGINA ACTUAL O ANTERIOR
	String ic_documentos[]		= request.getParameterValues("ic_documento");
	String ic_monedas[]			= request.getParameterValues("ic_moneda_docto");
	String ic_monedas_linea[]	= request.getParameterValues("ic_moneda_linea");
	String montos[]				= request.getParameterValues("montos");
	String montos_valuados[]	= request.getParameterValues("monto_valuado");
	String montos_credito[]		= request.getParameterValues("monto_credito");
	String montos_tasa_int[]	= request.getParameterValues("monto_tasa_int");
	String montos_descuento[]	= request.getParameterValues("monto_descuento");
	String tipos_cambio[]		= request.getParameterValues("tipo_cambio");
	String plazos_credito[]		= request.getParameterValues("plazo");
	String fechas_vto[]			= request.getParameterValues("fecha_vto");
	String referencias_tasa[]	= request.getParameterValues("referencia_tasa");
	String valores_tasa[]		= request.getParameterValues("valor_tasa");
	String valores_tasa_puntos[]= request.getParameterValues("valor_tasa_puntos");
	String ic_tasa[]			= request.getParameterValues("ic_tasa");
	String cg_rel_mat[]			= request.getParameterValues("cg_rel_mat");
	String fn_puntos[]			= request.getParameterValues("fn_puntos");
	String tipo_tasa[]			= request.getParameterValues("tipo_tasa");
	String puntos_pref[]		= request.getParameterValues("puntos_pref");
	String ic_epo				= request.getParameter("ic_epo");
	String fg_limite_pyme		= request.getParameter("hidLimPyme");
	String fg_limite_pyme_acum	= request.getParameter("hidLimUtil");
	String	acuse		= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
	String	tipoArchivo		= (request.getParameter("tipoArchivo")==null)?"":request.getParameter("tipoArchivo");
	//VARIABLES DE USO LOCAL
	Vector		vecFilas	= null;
	Vector		vecFilas1	= null;
	Vector		vecFilas2	= null;
	Vector		vecColumnas	= null;	
	int	totalDoctosMN		=0;
double	totalMontoMN		= 0;
double	totalMontoDescMN	= 0;
double	totalInteresMN		= 0;
double	totalMontoImpMN		= 0;
int	totalDoctosUSD		= 0;
double	totalMontoUSD		= 0;
double	totalMontoDescUSD	= 0;
double	totalInteresUSD		= 0;
double	totalMontoImpUSD	= 0;
double	totalDoctosConv		= 0;
double	totalMontosConv		= 0;
double	totalMontoDescConv	= 0;
double	totalConvPesos		= 0;
double	totalMontoImpConv	=0;
double	totalMontoIntConv	= 0;
String 		_acuse = "";
int			i = 0, j = 0;
String		icMoneda 			= "";
String		fechaHoy			= "";

// DE DESPLIEGUE
String nombreEpo			=	"";
String igNumeroDocto		=	"";
String ccAcuse 				=	"";
String dfFechaEmision		=	"";
String dfFechaVencimiento	=	"";
String dfFechaPublicacion 	= 	"";
String igPlazoDocto			=	"";
String moneda 				=	"";
double fnMonto 				= 	0;
String cgTipoConv			=	"";
String tipoCambio			=   "";
double montoValuado 		=	0;
String igPlazoDescuento		= 	"";
String fnPorcDescuento		= 	"";
double montoDescuento 		=	0;
String modoPlazo			= 	"";
String estatus 				=	"";
String cambios				= 	"";
String numeroCredito		= 	"";
String nombreIf				= 	"";
String tipoLinea			=	"";
String fechaOperacion		=	"";
double montoCredito			=	0;
String referencia			=	"";
String plazoCredito			=	"";
String fechaVencCredito		=	"";
double tasaInteres			= 	0;
double valorTasaInt			=	0;
double montoTasaInt			=	0;
double fnPuntos				= 	0;
String relMat				=	"";
String tipoCobroInt			=	"";
String  tipoPiso			=	"";
String icTipoCobroInt		=	"";
String icTasa				=	"";
String acuseFormateado		=	"";
String usuario				= 	"";
String fechaCarga			= 	"";
String horaCarga			= 	"";
String icTipoFinanciamiento	=	"";
String monedaLinea			=	"";
double montoCapitalInt		=	0;
String nombreMLinea			=	"";
String nePyme 		= (String)session.getAttribute("strNePymeAsigna");
String nombrePyme	= (String)session.getAttribute("strNombrePymeAsigna");
String contentType = "text/html;charset=ISO-8859-1";
String 		nombreArchivo 	= null;	
CreaArchivo archivo 				= null;
archivo  = new CreaArchivo();
JSONObject jsonObj = new JSONObject();		
StringBuffer contenidoArchivo = new StringBuffer();	

try{
//INSTANCIACION DE EJB'S
AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

if("PYME".equals(strTipoUsuario))	
		Horario.validarHorario(4, strTipoUsuario, iNoEPO);
        String operaContrato = BeanParametros.obtieneOperaSinCesion(iNoEPO); //PARAMETRO NUEVO JAGE 14012017
		if("NAFIN".equals(strTipoUsuario))
			Horario.validarHorario(4,"PYME",ic_epo);
			fechaHoy 	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			for(i=0;i<ic_documentos.length;i++){
				//if("S".equals(modificados[i])){
				System.out.println("ic_epo ------>"+ic_epo);
				if("1".equals(ic_monedas[i])){
					System.out.println("pasa las moneda nacional");
					System.out.println("montos[i]   "+montos[i]);
					System.out.println("montos_descuento[i]   "+montos_descuento[i]);
					System.out.println("montos_tasa_int[i]  "+montos_tasa_int[i]);
					totalDoctosMN ++;
					totalMontoMN		+= Double.parseDouble(montos[i]);
					totalMontoDescMN	+= Double.parseDouble(montos_descuento[i]);
					totalInteresMN		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);				
					totalMontoImpMN 	+= Double.parseDouble(montos_valuados[i]);
				}				
				if("54".equals(ic_monedas[i])&&"54".equals(ic_monedas_linea[i])){
					System.out.println("pasa dolares");
					totalDoctosUSD ++;
					totalMontoUSD		+= Double.parseDouble(montos[i]);
					totalMontoDescUSD	+= Double.parseDouble(montos_descuento[i]);
					totalInteresUSD		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);
					totalMontoImpUSD+= Double.parseDouble(montos_valuados[i]);
				}
				if("54".equals(ic_monedas[i])&&"1".equals(ic_monedas_linea[i])){
					totalDoctosConv ++;
					totalMontosConv			+= Double.parseDouble(montos[i]);
					totalMontoDescConv 		+= Double.parseDouble(montos_descuento[i]);
					totalMontoIntConv		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);
					totalConvPesos			+= Double.parseDouble(montos_descuento[i]);
					totalMontoImpConv	+= Double.parseDouble(montos_valuados[i]);
				}								
			//}
		} //for
		
	vecFilas = aceptPyme.consultaDoctos(acuse);
	vecFilas1 = (Vector)vecFilas.get(0);
	vecFilas2 = (Vector)vecFilas.get(1);
	
	if(tipoArchivo.equals("PDF")) {
		archivo 			    = new CreaArchivo();
		nombreArchivo 	  = archivo.nombreArchivo()+".pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
	
		String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
		String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
		String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
		String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
		String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
				
		pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String) application.getAttribute("strDirectorioPublicacion"), "");
		
		String mensaje = "Al transmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, manifiesta su aceptaci�n para que sea(n) sustituido(s) el(los) DOCUMENTO(S) INICIAL(ES) que ha seleccionado por el(los) DOCUMENTO(S) FINAL(ES) y estos �ltimos sean descontados a favor de la EMPRESA DE PRIMER ORDEN. Dicho(s) DOCUMENTO(S) FINALE(S) contiene(n) los Derechos de Cobro a su cargo por lo que Usted acepta pagar el 100% de su valor al INTERMEDIARIO FINANCIERO en su fecha de vencimiento. Manifiesta tambi�n su obligaci�n de cubrir al INTERMEDIARIO FINANCIERO, los intereses que se detallan en esta pantalla, en la fecha de su operaci�n, por haber aceptado la sustituci�n. ";
        if(operaContrato.equals("S")) {
            mensaje = "En este acto manifiesto mi aceptación para que sea(n) financiado(s) el(los) DOCUMENTO(S) que he seleccionado, mismo(s) que contiene(n) cuentas pendientes de pago a mi cargo "+
                " y a favor de la EMPRESA DE PRIMER ORDEN por lo que en esta misma fecha me obligo a cubrir al INTERMEDIARIO FINANCIERO los intereses que se detallan en esta pantalla. "+
                "Manifiesto también mi obligación y aceptación de pagar el 100% del valor de(los) DOCUMENTO(S) en la fecha de vencimiento al INTERMEDIARIO FINANCIERO.";            
        }
		mensaje = mensaje + "\n\nPor otra parte, a partir del 17 de octubre de 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que sí ha emitido o emitirá a su CLIENTE o DISTRIBUIDOR el CFDI por la operación comercial que le dio origen a esta transacción, según sea el caso y conforme establezcan las disposiciones fiscales vigentes.";
		pdfDoc.setTable(12, 100);
		pdfDoc.setCell(" Moneda nacional ", "celda02", ComunesPDF.CENTER, 4);
		pdfDoc.setCell(" D�lares ", "celda02", ComunesPDF.CENTER, 4);
		pdfDoc.setCell(" Doctos. en DLL financiados en M.N. ", "celda02", ComunesPDF.CENTER, 4);		
		pdfDoc.setCell(" Num. total de doctos.", "celda02 ",  ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto total de doctos. iniciales","celda02 ", ComunesPDF.CENTER,1);
		pdfDoc.setCell(" Monto total de doctos. finales","celda02 ", ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto intereses", "celda02 ",ComunesPDF.CENTER);
		pdfDoc.setCell(" Num. total de doctos.","celda02 ", ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto total de doctos. iniciales", "celda02 ",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto total de doctos. finales", "celda02 ",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto intereses", "celda02 ",ComunesPDF.CENTER);
		pdfDoc.setCell(" Num. total de doctos.", "celda02 ",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto total de doctos. iniciales", "celda02 ",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto total de doctos. finales", "celda02 ",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto intereses", "celda02 ",ComunesPDF.CENTER);
		
		pdfDoc.setCell(String.valueOf(totalDoctosMN) , "formas ",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoMN,2), "formas ",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoImpMN,2), "formas ",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalInteresMN,2), "formas ",ComunesPDF.RIGHT);
		pdfDoc.setCell(String.valueOf(totalDoctosUSD)  , "formas ",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoUSD,2), "formas ",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoImpUSD,2), "formas ",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalInteresUSD,2), "formas ",ComunesPDF.RIGHT);
		pdfDoc.setCell(Double.toString(totalDoctosConv), "formas ",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontosConv,2), "formas ",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoImpConv,2), "formas ",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoIntConv,2), "formas ",ComunesPDF.RIGHT);
		pdfDoc.setCell(mensaje,"formas",ComunesPDF.JUSTIFIED,12);
		pdfDoc.addTable();
		pdfDoc.addText(" ","formas",ComunesPDF.JUSTIFIED);
		
		
		if(vecFilas1.size()>0) {
			for(i=0;i<vecFilas1.size();i++,j++){
				vecColumnas = (Vector)vecFilas1.get(i);
				nombreEpo			=	(String)vecColumnas.get(0);
				igNumeroDocto		=	(String)vecColumnas.get(1);
				ccAcuse 			=	(String)vecColumnas.get(2);
				dfFechaEmision		=	(String)vecColumnas.get(3);
				dfFechaVencimiento	=	(String)vecColumnas.get(4);
				dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
				igPlazoDocto		=	(String)vecColumnas.get(6);
				moneda 				=	(String)vecColumnas.get(7);
				fnMonto 			= 	Double.parseDouble((String)vecColumnas.get(8).toString());
				cgTipoConv			=	(String)vecColumnas.get(9);
				tipoCambio			=   (String)vecColumnas.get(10);
				montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
				igPlazoDescuento	= 	(String)vecColumnas.get(12);
				fnPorcDescuento		= 	(String)vecColumnas.get(13);
				montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
				modoPlazo			= 	(String)vecColumnas.get(14);
				estatus 			=	(String)vecColumnas.get(15);
				cambios				= 	(String)vecColumnas.get(16);
				icMoneda			= 	(String)vecColumnas.get(32);
				acuseFormateado		=	(String)vecColumnas.get(34);
				usuario				= 	(String)vecColumnas.get(37);
				fechaCarga			= 	(String)vecColumnas.get(35);
				horaCarga			= 	(String)vecColumnas.get(36);
				icTipoFinanciamiento= 	(String)vecColumnas.get(39);
				String   descuentoAforo1 = 	(String)vecColumnas.get(42)==null?"0":(String)vecColumnas.get(42); //F05-2014
				double  descuentoAforo= 0;
				if (descuentoAforo1.equals("0") || descuentoAforo1.equals("") )  {  	
					descuentoAforo = 100;   	
				}else  {	//F05-2014 
					descuentoAforo =  	Double.parseDouble(descuentoAforo1);				
				}
				double fnMontoDescuento =0; //F05-2014
		
				if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
					fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
				} else if("2".equals(icTipoFinanciamiento)  )  {			
					fnMontoDescuento	= fnMonto *(descuentoAforo/100);   //F05-2014 
				}  	 
					
			if(i==0){
					int csini = ("PYME".equals(strTipoUsuario))?17:17;
					pdfDoc.setTable(csini, 100);
					pdfDoc.setCell("Documento", "celda02", ComunesPDF.CENTER, csini);
					if("NAFIN".equals(strTipoUsuario)){
						pdfDoc.setCell("Distribuidor", "celda02", ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Epo", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Num. docto. inicial", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Num. acuse", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de emisi�n", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha vencimiento", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de publicaci�n", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo docto.", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Monto", "celda02", ComunesPDF.CENTER);									
					pdfDoc.setCell("Tipo conv.", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo para descuento en dias", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("% de descuento", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de descuento", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo cambio", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Monto valuado", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Modalidad de plazo", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER);
				}
				if("NAFIN".equals(strTipoUsuario)){
					pdfDoc.setCell(nombrePyme,"formas", ComunesPDF.CENTER);
				}
					pdfDoc.setCell(nombreEpo,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(igNumeroDocto,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(ccAcuse,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(dfFechaEmision,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(dfFechaVencimiento,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(dfFechaPublicacion,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(igPlazoDocto,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMonto,2),"formas", ComunesPDF.RIGHT);
					pdfDoc.setCell(cgTipoConv,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(igPlazoDescuento,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %"),"formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2),"formas", ComunesPDF.RIGHT);					
					pdfDoc.setCell(tipoCambio,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMontoDescuento,2),"formas", ComunesPDF.RIGHT);
					pdfDoc.setCell(modoPlazo,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"formas", ComunesPDF.CENTER);
				}//for
				pdfDoc.addTable();
			}
			
			if(vecFilas2.size()>0) {
				int mas =0;
				for(i=0;i<vecFilas2.size();i++){
					vecColumnas = (Vector)vecFilas2.get(i);
					nombreEpo			=	(String)vecColumnas.get(0);
					igNumeroDocto		=	(String)vecColumnas.get(1);
					ccAcuse 			=	(String)vecColumnas.get(2);
					dfFechaEmision		=	(String)vecColumnas.get(3);
					dfFechaVencimiento	=	(String)vecColumnas.get(4);
					dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
					igPlazoDocto		=	(String)vecColumnas.get(6);
					moneda 				=	(String)vecColumnas.get(7);
					fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
					cgTipoConv			=	(String)vecColumnas.get(9);
					tipoCambio			=   (String)vecColumnas.get(10);
					montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
					montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
					igPlazoDescuento	= 	(String)vecColumnas.get(12);
					fnPorcDescuento		= 	(String)vecColumnas.get(13);
					modoPlazo			= 	(String)vecColumnas.get(14);
					estatus 			=	(String)vecColumnas.get(15);
					cambios				= 	(String)vecColumnas.get(16);
					//creditos por concepto de intereses
					numeroCredito		= 	(String)vecColumnas.get(17);
					nombreIf			= 	(String)vecColumnas.get(18);
					tipoLinea			=	(String)vecColumnas.get(19);
					fechaOperacion		=	(String)vecColumnas.get(20);
					plazoCredito		=	(String)vecColumnas.get(22);
					fechaVencCredito	=	(String)vecColumnas.get(23);
					valorTasaInt		=	Double.parseDouble(vecColumnas.get(41).toString());
					relMat				= 	(String)vecColumnas.get(25);
					fnPuntos			=	Double.parseDouble(vecColumnas.get(26).toString());
					tipoCobroInt		=	(String)vecColumnas.get(27);
					referencia			=	(String)vecColumnas.get(29);
					tipoPiso			= 	(String)vecColumnas.get(30);
					icTipoCobroInt		= 	(String)vecColumnas.get(31);
					icMoneda			= 	(String)vecColumnas.get(32);
					acuseFormateado		=	(String)vecColumnas.get(34);
					usuario				= 	(String)vecColumnas.get(37);
					fechaCarga			= 	(String)vecColumnas.get(35);
					horaCarga			= 	(String)vecColumnas.get(36);
					icTipoFinanciamiento= 	(String)vecColumnas.get(39);
					monedaLinea			= 	(String)vecColumnas.get(38);
					nombreMLinea		= 	(String)vecColumnas.get(40);
					plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
					String   descuentoAforo1 = 	(String)vecColumnas.get(42)==null?"0":(String)vecColumnas.get(42); //F05-2014
					double  descuentoAforo= 0;
					if (descuentoAforo1.equals("0") || descuentoAforo1.equals("") )  {  	
						descuentoAforo = 100;   	
					}else  {	//F05-2014 
						descuentoAforo =  	Double.parseDouble(descuentoAforo1);				
					}
					double fnMontoDescuento =0;
			
		
					montoCredito = fnMonto -montoDescuento;
					if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
						if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
							montoCredito = montoValuado;
						else if("2".equals(icTipoFinanciamiento))
							montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
					}
					if("1".equals(tipoPiso)){
						montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
					}else{
						montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
					}
					montoCapitalInt = montoCredito +(montoTasaInt*Double.parseDouble(plazoCredito));
					
					if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
					fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
				} else if("2".equals(icTipoFinanciamiento)  )  {			
					fnMontoDescuento	= fnMonto *(descuentoAforo/100);   //F05-2014 
				}  	 
							 
				 
				 
					if(i==0){
					
					int csini = ("PYME".equals(strTipoUsuario))?16:19;
					mas  = csini-11;
					
					pdfDoc.setTable(csini, 100);
					pdfDoc.setCell("Datos Documento Inicial", "celda02", ComunesPDF.CENTER, csini);
					
					if("NAFIN".equals(strTipoUsuario)){	
						pdfDoc.setCell("Distribuidor", "celda02", ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Epo", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Num. docto.", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Num. acuse", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de emisi�n", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha vencimiento", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de publicaci�n", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo docto.", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Monto", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo para descuento en dias", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("% de descuento", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de descuento", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo conv.", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo cambio", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Monto valuado", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Modalidad de plazo", "celda02", ComunesPDF.CENTER);					
					pdfDoc.setCell("Datos Documento Final", "celda02", ComunesPDF.CENTER, csini);
					pdfDoc.setCell("Num. de documento final", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Monto", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de vencimiento", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operaci�n ", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("IF", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia tasa de inter�s", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Valor tasa de inter�s", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de intereses", "celda02", ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Total de Capital e Inter�s", "celda02", ComunesPDF.CENTER);
					for(int p=0; p<mas; p++) {
						pdfDoc.setCell(" ", "celda02", ComunesPDF.CENTER);
					}
					
					}	
					if("NAFIN".equals(strTipoUsuario)){
					pdfDoc.setCell(nombrePyme, "formas", ComunesPDF.CENTER);
					}
					pdfDoc.setCell(nombreEpo, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(igNumeroDocto, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(ccAcuse, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(dfFechaEmision, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(dfFechaVencimiento, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(dfFechaPublicacion, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(igPlazoDocto, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(moneda, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMonto,2), "formas", ComunesPDF.RIGHT);
					pdfDoc.setCell(igPlazoDescuento, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("%"+fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %"), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2), "formas", ComunesPDF.RIGHT);
				
					if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
					pdfDoc.setCell(cgTipoConv, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(tipoCambio, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMontoDescuento,2), "formas", ComunesPDF.RIGHT);
					}else{
					pdfDoc.setCell(" ", "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(" ", "formas", ComunesPDF.CENTER);
					}		
					pdfDoc.setCell(modoPlazo, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(numeroCredito, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(nombreMLinea, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoCredito,2), "formas", ComunesPDF.RIGHT);
					pdfDoc.setCell("0".equals(plazoCredito)?" ":plazoCredito, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencCredito, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(fechaHoy, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(nombreIf, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell( ("S".equals(icTipoCobroInt))?"":referencia+" "+relMat+" "+fnPuntos, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?" ":" "+Comunes.formatoDecimal(valorTasaInt,2)+"%", "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$ "+Comunes.formatoDecimal(montoTasaInt*Double.parseDouble(plazoCredito),2), "formas", ComunesPDF.RIGHT);
					pdfDoc.setCell(("S".equals(icTipoCobroInt))?" ":"$ "+Comunes.formatoDecimal(montoCapitalInt,2), "formas", ComunesPDF.RIGHT);
					for(int p=0; p<mas; p++) {
						pdfDoc.setCell(" ", "formas", ComunesPDF.CENTER);
					}
			} //for
			pdfDoc.addTable();		
	} //if
	
			pdfDoc.setTable(2, 40);
			pdfDoc.setCell("Datos de cifras de control", "celda02", ComunesPDF.CENTER, 2);
			pdfDoc.setCell("Num. acuse", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(acuseFormateado, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(fechaCarga, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("Hora", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(horaCarga, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre y n�mero de usuario", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(iNoUsuario+" "+strNombreUsuario, "formas", ComunesPDF.CENTER);
			pdfDoc.addTable();
			
		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	
	}//termina PDF
	//******************************************************************************
		if(tipoArchivo.equals("CSV")) {
				
		String mensaje = "Al transmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, manifiesta su aceptaci�n para que sea(n) sustituido(s) el(los) DOCUMENTO(S) INICIAL(ES) que ha seleccionado por el(los) DOCUMENTO(S) FINAL(ES) y estos �ltimos sean descontados a favor de la EMPRESA DE PRIMER ORDEN. Dicho(s) DOCUMENTO(S) FINALE(S) contiene(n) los Derechos de Cobro a su cargo por lo que Usted acepta pagar el 100% de su valor al INTERMEDIARIO FINANCIERO en su fecha de vencimiento. Manifiesta tambi�n su obligaci�n de cubrir al INTERMEDIARIO FINANCIERO, los intereses que se detallan en esta pantalla, en la fecha de su operaci�n, por haber aceptado la sustituci�n. ";			
		
		contenidoArchivo.append("Moneda nacional "+",,,,");
		contenidoArchivo.append("D�lares"+",,,,");		
		contenidoArchivo.append("Doctos. en DLL financiados en M.N."+",,,,");
		contenidoArchivo.append(" "+"\n");
		contenidoArchivo.append("Num. total de doctos."+",");
		contenidoArchivo.append("Monto total de doctos. iniciales "+",");
		contenidoArchivo.append("Monto total de doctos. finales "+",");
		contenidoArchivo.append("Monto intereses "+",");
		contenidoArchivo.append("Num. total de doctos."+",");
		contenidoArchivo.append("Monto total de doctos. iniciales "+",");
		contenidoArchivo.append("Monto total de doctos. finales "+",");
		contenidoArchivo.append("Monto intereses "+",");
		contenidoArchivo.append("Num. total de doctos. "+",");
		contenidoArchivo.append("Monto total de doctos. iniciales "+",");
		contenidoArchivo.append("Monto total de doctos. finales "+","); 
		contenidoArchivo.append("Monto intereses "+"\n");
		contenidoArchivo.append(" "+"\n");
		
		contenidoArchivo.append(totalDoctosMN+",");
		contenidoArchivo.append("$"+Comunes.formatoDecimal(totalMontoMN,2,false)+",");
		contenidoArchivo.append("$"+Comunes.formatoDecimal(totalMontoImpMN,2,false)+",");
		contenidoArchivo.append("$"+Comunes.formatoDecimal(totalInteresMN,2,false)+",");
		contenidoArchivo.append(totalDoctosUSD+",");
		contenidoArchivo.append("$"+Comunes.formatoDecimal(totalMontoUSD,2,false)+",");
		contenidoArchivo.append("$"+Comunes.formatoDecimal(totalMontoImpUSD,2,false)+",");
		contenidoArchivo.append("$"+Comunes.formatoDecimal(totalInteresUSD,2,false)+",");
		contenidoArchivo.append(totalDoctosConv+",");
		contenidoArchivo.append("$"+Comunes.formatoDecimal(totalMontosConv,2,false)+",");
		contenidoArchivo.append("$"+Comunes.formatoDecimal(totalMontoImpConv,2,false)+",");
		contenidoArchivo.append("$"+Comunes.formatoDecimal(totalMontoIntConv,2,false)+",");
		contenidoArchivo.append("\n");
		
			
		
		if(vecFilas1.size()>0) {
			for(i=0;i<vecFilas1.size();i++,j++){
				vecColumnas = (Vector)vecFilas1.get(i);
				nombreEpo			=	(String)vecColumnas.get(0);
				igNumeroDocto		=	(String)vecColumnas.get(1);
				ccAcuse 			=	(String)vecColumnas.get(2);
				dfFechaEmision		=	(String)vecColumnas.get(3);
				dfFechaVencimiento	=	(String)vecColumnas.get(4);
				dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
				igPlazoDocto		=	(String)vecColumnas.get(6);
				moneda 				=	(String)vecColumnas.get(7);
				fnMonto 			= 	Double.parseDouble((String)vecColumnas.get(8).toString());
				cgTipoConv			=	(String)vecColumnas.get(9);
				tipoCambio			=   (String)vecColumnas.get(10);
				montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
				igPlazoDescuento	= 	(String)vecColumnas.get(12);
				fnPorcDescuento		= 	(String)vecColumnas.get(13);
				montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
				modoPlazo			= 	(String)vecColumnas.get(14);
				estatus 			=	(String)vecColumnas.get(15);
				cambios				= 	(String)vecColumnas.get(16);
				icMoneda			= 	(String)vecColumnas.get(32);
				acuseFormateado		=	(String)vecColumnas.get(34);
				usuario				= 	(String)vecColumnas.get(37);
				fechaCarga			= 	(String)vecColumnas.get(35);
				horaCarga			= 	(String)vecColumnas.get(36);
				icTipoFinanciamiento= 	(String)vecColumnas.get(39);
				String   descuentoAforo1 = 	(String)vecColumnas.get(42)==null?"0":(String)vecColumnas.get(42); //F05-2014				
				double  descuentoAforo= 0;
				if (descuentoAforo1.equals("0") || descuentoAforo1.equals("") )  {  	
					descuentoAforo = 100;   	
				}else  {	//F05-2014 
					descuentoAforo =  	Double.parseDouble(descuentoAforo1);				
				}
				double fnMontoDescuento =0;  //F05-2014
				
				if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
					fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
				} else if("2".equals(icTipoFinanciamiento)  )  {			
					fnMontoDescuento	= fnMonto *(descuentoAforo/100);   //F05-2014 
				}  	 
			
				 
			
				if(i==0){
					int csini = ("PYME".equals(strTipoUsuario))?19:20;
					
					contenidoArchivo.append("Documento"+"\n");
					if("NAFIN".equals(strTipoUsuario)){
						contenidoArchivo.append("Distribuidor"+",");
					}
					contenidoArchivo.append("Epo"+",");
					contenidoArchivo.append("Num. docto. inicial"+",");
					contenidoArchivo.append("Num. acuse"+",");
					contenidoArchivo.append("Fecha de emisi�n"+",");
					contenidoArchivo.append("Fecha vencimiento"+",");
					contenidoArchivo.append("Fecha de publicaci�n"+",");
					contenidoArchivo.append("Plazo docto."+",");
					contenidoArchivo.append("Moneda"+",");
					contenidoArchivo.append("Monto"+",");
					contenidoArchivo.append("Tipo conv."+",");
					contenidoArchivo.append("Plazo para descuento en dias"+",");
					contenidoArchivo.append("% de descuento"+",");
					contenidoArchivo.append("Monto de descuento"+",");					
					contenidoArchivo.append("Tipo cambio"+",");
					contenidoArchivo.append("Monto valuado"+",");
					contenidoArchivo.append("Modalidad de plazo"+",");
					contenidoArchivo.append("Estatus"+"\n");					
				}
				if("NAFIN".equals(strTipoUsuario)){
					contenidoArchivo.append(nombrePyme.replace(',',' ')+",");	
				}
					contenidoArchivo.append(nombreEpo.replace(',',' ')+",");	
					contenidoArchivo.append(igNumeroDocto+",");	
					contenidoArchivo.append(ccAcuse+",");	
					contenidoArchivo.append(dfFechaEmision+",");	
					contenidoArchivo.append(dfFechaVencimiento+",");	
					contenidoArchivo.append(dfFechaPublicacion+",");	
					contenidoArchivo.append(igPlazoDocto+",");	
					contenidoArchivo.append(moneda+",");	
					contenidoArchivo.append("$"+Comunes.formatoDecimal(fnMonto,2,false)+",");					
					contenidoArchivo.append(cgTipoConv+",");	
					contenidoArchivo.append(igPlazoDescuento+",");	
					contenidoArchivo.append(fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %")+",");	
					contenidoArchivo.append("$"+Comunes.formatoDecimal(montoDescuento,2,false)+",");	
					contenidoArchivo.append(tipoCambio+",");	
					contenidoArchivo.append("$"+Comunes.formatoDecimal(fnMontoDescuento,2,false)+",");	
					contenidoArchivo.append(modoPlazo+",");	
					contenidoArchivo.append(estatus+"\n");	
				}//for				
			}
			
			if(vecFilas2.size()>0) {
			
				for(i=0;i<vecFilas2.size();i++){
					vecColumnas = (Vector)vecFilas2.get(i);
					nombreEpo			=	(String)vecColumnas.get(0);
					igNumeroDocto		=	(String)vecColumnas.get(1);
					ccAcuse 			=	(String)vecColumnas.get(2);
					dfFechaEmision		=	(String)vecColumnas.get(3);
					dfFechaVencimiento	=	(String)vecColumnas.get(4);
					dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
					igPlazoDocto		=	(String)vecColumnas.get(6);
					moneda 				=	(String)vecColumnas.get(7);
					fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
					cgTipoConv			=	(String)vecColumnas.get(9);
					tipoCambio			=   (String)vecColumnas.get(10);
					montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
					montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
					igPlazoDescuento	= 	(String)vecColumnas.get(12);
					fnPorcDescuento		= 	(String)vecColumnas.get(13);
					modoPlazo			= 	(String)vecColumnas.get(14);
					estatus 			=	(String)vecColumnas.get(15);
					cambios				= 	(String)vecColumnas.get(16);
					//creditos por concepto de intereses
					numeroCredito		= 	(String)vecColumnas.get(17);
					nombreIf			= 	(String)vecColumnas.get(18);
					tipoLinea			=	(String)vecColumnas.get(19);
					fechaOperacion		=	(String)vecColumnas.get(20);
					plazoCredito		=	(String)vecColumnas.get(22);
					fechaVencCredito	=	(String)vecColumnas.get(23);
					valorTasaInt		=	Double.parseDouble(vecColumnas.get(41).toString());
					relMat				= 	(String)vecColumnas.get(25);
					fnPuntos			=	Double.parseDouble(vecColumnas.get(26).toString());
					tipoCobroInt		=	(String)vecColumnas.get(27);
					referencia			=	(String)vecColumnas.get(29);
					tipoPiso			= 	(String)vecColumnas.get(30);
					icTipoCobroInt		= 	(String)vecColumnas.get(31);
					icMoneda			= 	(String)vecColumnas.get(32);
					acuseFormateado		=	(String)vecColumnas.get(34);
					usuario				= 	(String)vecColumnas.get(37);
					fechaCarga			= 	(String)vecColumnas.get(35);
					horaCarga			= 	(String)vecColumnas.get(36);
					icTipoFinanciamiento= 	(String)vecColumnas.get(39);
					monedaLinea			= 	(String)vecColumnas.get(38);
					nombreMLinea		= 	(String)vecColumnas.get(40);
					plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
					montoCredito = fnMonto -montoDescuento;					
					String   descuentoAforo1 = 	(String)vecColumnas.get(42)==null?"0":(String)vecColumnas.get(42); //F05-2014
					double  descuentoAforo= 0;
					if (descuentoAforo1.equals("0") || descuentoAforo1.equals("") )  {  	
						descuentoAforo = 100;   	
					}else  {	//F05-2014 
						descuentoAforo =  	Double.parseDouble(descuentoAforo1);				
					}
					double fnMontoDescuento = 0;   //F05-2014
			
		
					if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
						if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
							montoCredito = montoValuado;
						else if("2".equals(icTipoFinanciamiento))
							montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
					}
					if("1".equals(tipoPiso)){
						montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
					}else{
						montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
					}
					montoCapitalInt = montoCredito +(montoTasaInt*Double.parseDouble(plazoCredito));
					
					if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
						fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
					} else if("2".equals(icTipoFinanciamiento)  )  {			
						fnMontoDescuento	= fnMonto *(descuentoAforo/100);   //F05-2014 
					}  			 
				 
					if(i==0){
					
					int csini = ("PYME".equals(strTipoUsuario))?16:19;
					int total  = csini+11;
					
					contenidoArchivo.append("\n");
					contenidoArchivo.append(" Datos Documento Inicial"+",,,,,,,,,,,,,,,,,,");
					contenidoArchivo.append(" Datos Documento Final"+"\n");
					if("NAFIN".equals(strTipoUsuario)){	
						contenidoArchivo.append(" Distribuidor"+",");
					}
					contenidoArchivo.append("Epo"+",");
					contenidoArchivo.append("Num. docto."+",");
					contenidoArchivo.append("Num. acuse"+",");
					contenidoArchivo.append("Fecha de emisi�n"+",");
					contenidoArchivo.append("Fecha vencimiento"+",");
					contenidoArchivo.append("Fecha de publicaci�n"+",");
					contenidoArchivo.append("Plazo docto."+",");
					contenidoArchivo.append("Moneda"+",");
					contenidoArchivo.append("Monto"+",");
					contenidoArchivo.append("Plazo para descuento en dias"+",");
					contenidoArchivo.append("% de descuento"+",");
					contenidoArchivo.append("Monto de descuento"+",");						
					contenidoArchivo.append("Tipo conv."+",");
					contenidoArchivo.append("Tipo cambio"+",");
					contenidoArchivo.append("Monto valuado"+",");
					contenidoArchivo.append("Modalidad de plazo"+",");
					contenidoArchivo.append("Num. de documento final"+",");
					contenidoArchivo.append("Moneda"+",");
					contenidoArchivo.append("Monto"+",");
					contenidoArchivo.append("Plazo"+",");
					contenidoArchivo.append("Fecha de vencimiento"+",");
					contenidoArchivo.append("Fecha de Operaci�n "+",");
					contenidoArchivo.append("IF "+",");
					contenidoArchivo.append("Referencia tasa de inter�s"+",");
					contenidoArchivo.append("Valor tasa de inter�s"+",");
					contenidoArchivo.append("Monto de intereses"+",");
					contenidoArchivo.append("Monto Total de Capital e Inter�s"+"\n");
					}	
					if("NAFIN".equals(strTipoUsuario)){
					contenidoArchivo.append(nombrePyme.replace(',',' ')+",");
					}
					
					contenidoArchivo.append(nombreEpo.replace(',',' ')+",");
					contenidoArchivo.append(igNumeroDocto+",");
					contenidoArchivo.append(ccAcuse+",");
					contenidoArchivo.append(dfFechaEmision+",");
					contenidoArchivo.append(dfFechaVencimiento+",");
					contenidoArchivo.append(dfFechaPublicacion+",");
					contenidoArchivo.append(igPlazoDocto+",");
					contenidoArchivo.append(moneda+",");
					contenidoArchivo.append("$"+Comunes.formatoDecimal(fnMonto,2,false)+",");
					contenidoArchivo.append(igPlazoDescuento+",");
					contenidoArchivo.append(fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %")+",");
					contenidoArchivo.append("$"+Comunes.formatoDecimal(montoDescuento,2,false)+",");
					if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
					contenidoArchivo.append(cgTipoConv+",");
					contenidoArchivo.append(tipoCambio+",");
					contenidoArchivo.append("$"+Comunes.formatoDecimal(fnMontoDescuento,2,false)+",");
					}else{
					contenidoArchivo.append(" "+",");
					contenidoArchivo.append(" "+",");
					contenidoArchivo.append(" "+",");
					}		
					contenidoArchivo.append(modoPlazo+",");
					contenidoArchivo.append(numeroCredito+",");
					contenidoArchivo.append(nombreMLinea+",");
					contenidoArchivo.append(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoCredito,2,false)+",");
					contenidoArchivo.append(("0".equals(plazoCredito))?" ":plazoCredito+",");
					contenidoArchivo.append(fechaVencCredito+",");
					contenidoArchivo.append(fechaHoy+",");
					contenidoArchivo.append(nombreIf.replace(',',' ')+",");
					contenidoArchivo.append(("S".equals(icTipoCobroInt))?"":referencia+" "+relMat+" "+fnPuntos+",");
					contenidoArchivo.append(("S".equals(icTipoCobroInt))?" ":" "+Comunes.formatoDecimal(valorTasaInt,2)+"%"+",");
					contenidoArchivo.append(("S".equals(icTipoCobroInt))?"":" $"+Comunes.formatoDecimal(montoTasaInt*Double.parseDouble(plazoCredito),2,false)+",");
					contenidoArchivo.append(("S".equals(icTipoCobroInt))?" ":"$"+Comunes.formatoDecimal(montoCapitalInt,2,false)+"\n");
			} //for			
		} //if
			contenidoArchivo.append(" "+"\n");
			contenidoArchivo.append("Datos de cifras de control"+"\n");
			contenidoArchivo.append("Num. acuse"+",");
			contenidoArchivo.append(acuseFormateado+"\n");
			contenidoArchivo.append("Fecha"+",");
			contenidoArchivo.append(fechaCarga+"\n");
			contenidoArchivo.append("Hora"+",");
			contenidoArchivo.append(horaCarga+"\n");
			contenidoArchivo.append("Nombre y n�mero de usuario"+",");
			contenidoArchivo.append(iNoUsuario+" "+strNombreUsuario.replace(',',' ')+",");
			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	
	if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		
	}//termina CSV
			
}catch(AppException ne){
	out.println("Error en la generacion del archivo PDF o CSV del Acuse"+ne);
}catch(Exception e){
	System.out.println("Error en la generacion del archivo PDF o CSV del Acuse 24forma01bExtPDFCSV.jsp  "+e);
	out.println(e);
}

%>

<%=jsonObj%>
