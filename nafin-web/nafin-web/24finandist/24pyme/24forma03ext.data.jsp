<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.distribuidores.*"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String infoRegresar = "";

	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class);
	
	if(informacion.equals("Consulta")){
		Vector parametros = null;

		String rs_epo = "";
		String rs_nomEpo = "";
		String rs_dscto_auto = "";
		String rs_moneda_auto = "";
		String rs_dolar = "";
		String checkboxMoneda="";
		String checkboxDolar="";
		
		JSONArray jsonArray = new JSONArray();
		
		parametros = BeanParametros.getParamDesctoAuto(iNoCliente);
		for(int p=0; p<parametros.size();p++){
			Vector paramEpo = (Vector)parametros.get(p);
			if (p==0){
				rs_dscto_auto = paramEpo.get(2).toString();
			}
			rs_epo = paramEpo.get(0).toString();
			rs_nomEpo = paramEpo.get(1).toString();
			rs_moneda_auto = paramEpo.get(3).toString();
			rs_dolar = paramEpo.get(4).toString();
			
			if("N".equals(rs_moneda_auto)||"A".equals(rs_moneda_auto)){
				checkboxMoneda="S";
			}else{
				checkboxMoneda="";
			}
			if(!"".equals(rs_dolar)){
				if("D".equals(rs_moneda_auto)||"A".equals(rs_moneda_auto)){
					checkboxDolar="S";
				}else{
					checkboxDolar="";
				}
			}
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("EPO",rs_epo);
			jsonObj.put("NOMBRE_EPO",rs_nomEpo);
			jsonObj.put("RS_DSCTO_AUTO",rs_dscto_auto);
			jsonObj.put("MONEDA",rs_moneda_auto);
			jsonObj.put("DOLAR",rs_dolar);
			jsonObj.put("MONEDA_N","");
			jsonObj.put("MONEDA_D","");
			jsonObj.put("COLUMNA_MONEDA",checkboxMoneda);
			jsonObj.put("COLUMNA_DOLAR",checkboxDolar);
			
			jsonArray.add(jsonObj);
		}
		infoRegresar = 						
							"{\"success\": true, \"total\": \""+ parametros.size() +"\", \"registros\": " + jsonArray.toString()+"}";
		
	}else if(informacion.equals("Guardar")){
		try{
			String datos [] = request.getParameterValues("changes");
		   String moneda_nacional = (request.getParameter("moneda_n") == null) ? "" : request.getParameter("moneda_n");
			String moneda_dolar = (request.getParameter("moneda_d") == null) ? "" : request.getParameter("moneda_d");
			JSONArray jsonArray1 = JSONArray.fromObject(moneda_nacional);
			JSONArray jsonArray2 = JSONArray.fromObject(moneda_dolar);
			Object moneda_nac[] = jsonArray1.toArray();
			Object moneda_dol[] = jsonArray2.toArray();
			String monedaAutoN[] = new String[moneda_nac.length]; 
			String monedaAutoD[] = new String[moneda_dol.length];
		
			for (int i=0;i<moneda_nac.length;i++) 
				monedaAutoN[i] = moneda_nac[i].toString();
			for (int i=0;i<moneda_dol.length;i++)
				monedaAutoD[i] = moneda_dol[i].toString();

			String rad_autoriza	= (request.getParameter("rad_autoriza") == null) ? "" : request.getParameter("rad_autoriza");

			boolean transac = BeanParametros.setParamDesctoAuto(iNoCliente,rad_autoriza,monedaAutoN,monedaAutoD);
			if(transac)
				infoRegresar = "{\"success\": true, \"mensaje\": \"Parametros actualizados satisfactoriamente.\"}";	
			else
				infoRegresar = "{\"success\": true, \"mensaje\": \"Hubo problemas al actualizar los parametros.\"}";	
		}catch(Exception e){
		throw new AppException("Error al actualizar los campos.",e);
		}
	}
	
	
%>
<%=infoRegresar%>
