<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	com.netro.model.catalogos.CatalogoIFDistribuidores,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	java.beans.XMLEncoder"	
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
System.out.println("27consulta07ext.data.jsp (E)");
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_epo = request.getParameter("ic_epo") == null?"":(String)request.getParameter("ic_epo");
String ic_if = request.getParameter("ic_if") == null?"":(String)request.getParameter("ic_if");
String ic_pyme = request.getParameter("ic_pyme") == null?iNoCliente:(String)request.getParameter("ic_pyme");
String Txtfchini = request.getParameter("Txtfchini") == null?"":(String)request.getParameter("Txtfchini");
String Txtffin = request.getParameter("Txtffin") == null?"":(String)request.getParameter("Txtffin");
String tipoCreditoXepo = request.getParameter("tipoCreditoXepo") == null?"":(String)request.getParameter("tipoCreditoXepo");
String publicaDoctosFinanciables = (request.getParameter("publicaDoctosFinanciables") == null)?"":request.getParameter("publicaDoctosFinanciables");

String noEPO = application.getInitParameter("EpoCemex");
String infoRegresar = "",  consulta = "",  parametros  = "";
AccesoDB con = new AccesoDB();
int start = 0;
int limit = 0;
JSONObject 	jsonObj	= new JSONObject();

//try {
	//con.conexionDB();
	
	if (informacion.equals("Publica_Documentos_Financiables") ) {

	try{
		ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
		//F009-2015 Obtener si la EPO publica documentos financiables a meses sin intereses
		publicaDoctosFinanciables = BeanParametro.DesAutomaticoEpo(ic_epo,"CS_MESES_SIN_INTERESES");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("publicaDoctosFinanciables", publicaDoctosFinanciables);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al obtener los valores iniciales. ", e);
	}
	infoRegresar = jsonObj.toString();

	} else if (informacion.equals("CatalogoEPO") ) {
		 
		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClavePyme(ic_pyme);
		infoRegresar = cat.getJSONElementos();	
	
	} else if (informacion.equals("CatalogoIF") ) {
		 
		CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClaveEpo(ic_epo);
		infoRegresar = cat.getJSONElementos();	
	
	} else if (informacion.equals("Consulta") ) {
	
		//Obtengo parametrizaciones  
		String responsable ="", tiposCredito= "", cgTipoConversion ="", tipoPago="";
		ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
		if(!ic_epo.equals("")){		   
		 responsable  = BeanParametro.obtieneResponsable(ic_epo);
		 cgTipoConversion =  BeanParametro. obtieneTipoConversion(iNoCliente);		 
		 tipoCreditoXepo =  BeanParametro.obtieneTipoCredito (ic_epo); 
		 tiposCredito = BeanParametro.getTiposCredito(iNoCliente, ic_epo);
		}	
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	
		try {
			 start = Integer.parseInt(request.getParameter("start"));
			 limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
				
	
		AvNotifPymeDist avNotifPymeDist = new com.netro.distribuidores.AvNotifPymeDist();
		avNotifPymeDist.setCgTipoConversion(cgTipoConversion);
		avNotifPymeDist.setTiposCredito(tiposCredito);
		avNotifPymeDist.setTipoCreditoXepo(tipoCreditoXepo);
		CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(avNotifPymeDist);
		JSONObject 	resultado	= new JSONObject();
		tipoPago = avNotifPymeDist.tipoPago(ic_epo); 
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);
			resultado = JSONObject.fromObject(consulta);
			resultado.put("RESPONSABLE",responsable);
			resultado.put("TIPOSCREDITO",tiposCredito);
			resultado.put("CGTIPOCONVERSION",cgTipoConversion);  
			resultado.put("icEpo",ic_epo);	
			resultado.put("tipoPago",tipoPago);
			resultado.put("tipoCreditoXepo",tipoCreditoXepo);			
			infoRegresar = resultado.toString();
				
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
			
		//Datos para el Resumen de Totales
	} else if (informacion.equals("ResumenTotales")) {		
		//Debe ser llamado despues de Consulta
		CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion	

	} else if(informacion.equals("ArchivoTPDF")){

		String responsable = request.getParameter("responsable") == null?"":(String)request.getParameter("responsable");
		String tiposCredito = request.getParameter("tiposCredito") == null?"":(String)request.getParameter("tiposCredito");
		String cgTipoConversion = request.getParameter("cgTipoConversion") == null?"":(String)request.getParameter("cgTipoConversion");
		String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");

		AvNotifPymeDist paginador = new AvNotifPymeDist();
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		paginador.setIc_pyme(ic_pyme);
		paginador.setIc_if(ic_if);
		paginador.setIc_epo(ic_epo);
		paginador.setNumOrden(numOrden);
		paginador.setFchinicial(Txtfchini);
		paginador.setFchfinal(Txtffin);
		paginador.setNoCliente(iNoCliente);
		paginador.setTipo_Credito_Xepo(tipoCreditoXepo);
		paginador.setTipos_Credito(tiposCredito);
		paginador.setResponsable(responsable);
		paginador.setCg_Tipo_Conversion(cgTipoConversion);
		paginador.setPublicaDoctosFinanciables(publicaDoctosFinanciables);
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();

	}

/*} catch(Exception e) {
	jsonObj.put("success", new Boolean(false));
	//out.println(e.getMessage()); 
} 
	finally { 
		if (con.hayConexionAbierta()) {
				con.cierraConexionDB();	
		}
	}*/
%>

<%=infoRegresar%>

<%System.out.println("27consulta07ext.data.jsp (S)");%>


