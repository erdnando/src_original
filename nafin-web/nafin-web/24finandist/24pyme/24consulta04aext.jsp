<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*, java.sql.*,java.math.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String fechaHoy		= "";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}
String informacion   			=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
String	cgTipoConversion		=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

if("".equals(cgTipoConversion)){
	cgTipoConversion = cargaDocto.getTipoConversion(iNoCliente);
}

	String clave="";
	String epo="";
	String numdoc="";
	String acuse="";
	String fecha_emision="";
	String df_fecha_venc="";
	String fecha_publicacion="";
	String plazodoc="";
	String moneda="";
	String monto="";
	String conversion="";
	String tipcambio="";
	String monto_valuado="";
	String plazdesc="";
	String porcdesc="";
	String monto_con_desc="";
	String modalidad_plazo="";
	String estatus="";
	String fechacambio="";
	String cambioestatus="";
	String cambiomotivo="";
	String bandeVentaCartera="";
	String categoria="";
	String 	rs_ic_mon="", porc_Aforo ="";
	
	try {
				CreaArchivo archivo = new CreaArchivo();
				StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
				String nombreArchivo = null;
				if (informacion.equals("ArchivoCSV")) {
					CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsNoOperPymeDist());
					AccesoDB con = new AccesoDB();
					con.conexionDB();
					ResultSet	rs = null;
					rs = queryHelper.getCreateFile(request,con);
					int aRow = 0;
					/*******************
					Cabecera del archivo
					********************/
					contenidoArchivo.append("EPO,Número de documento inicial,Num. acuse carga,Fecha de emisión,Fecha vencimiento,Fecha de publicación,Plazo docto,Moneda,Monto,Categoria, % de Descuento Aforo, Monto a Descontar ,  Plazo para descuento en días,% de descuento,Monto % de descuento");
					if(!"".equals(cgTipoConversion))	{
						contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado");
					}
					contenidoArchivo.append(",Modalidad de plazo,Estatus,Fecha de cambio de estatus,Tipo de cambio de estatus,Causa");
					/***************************
						Generacion del archivo
					/***************************/
						while(rs.next()){
						clave					= rs.getString("clave")==null?"":rs.getString("clave");
						epo					=	rs.getString("epo")==null?"":rs.getString("epo");
						numdoc				= rs.getString("numdoc")==null?"":rs.getString("numdoc");
						acuse 				= rs.getString("acuse")==null?"":rs.getString("acuse");
						fecha_emision		= rs.getString("fecha_emision")==null?"":rs.getString("fecha_emision");
						df_fecha_venc 		= rs.getString("df_fecha_venc")==null?"":rs.getString("df_fecha_venc");
						fecha_publicacion = rs.getString("fecha_publicacion")==null?"":rs.getString("fecha_publicacion");
						plazodoc 			= rs.getString("plazodoc")==null?"":rs.getString("plazodoc");
						moneda 				= rs.getString("moneda")==null?"":rs.getString("moneda");
						monto 				= rs.getString("monto")==null?"":rs.getString("monto");
						conversion			= rs.getString("conversion")==null?"":rs.getString("conversion");
						tipcambio 			= rs.getString("tipcambio")==null?"":rs.getString("tipcambio");
						monto_valuado	 	= rs.getString("monto_valuado")==null?"":rs.getString("monto_valuado");
						plazdesc 			= rs.getString("plazdesc")==null?"":rs.getString("plazdesc");
						porcdesc 			= rs.getString("porcdesc")==null?"":rs.getString("porcdesc");
						monto_con_desc 	= rs.getString("monto_con_desc")==null?"0":rs.getString("monto_con_desc");
						modalidad_plazo 	= rs.getString("modalidad_plazo")==null?"":rs.getString("modalidad_plazo");
						estatus 				= rs.getString("estatus")==null?"":rs.getString("estatus");
						fechacambio 		= rs.getString("fechacambio")==null?"":rs.getString("fechacambio");
						cambioestatus 		= rs.getString("cambioestatus")==null?"":rs.getString("cambioestatus");
						cambiomotivo 		= rs.getString("cambiomotivo")==null?"":rs.getString("cambiomotivo");
						rs_ic_mon 			= rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
						categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
						bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
						porc_Aforo	= (rs.getString("POR_AFRO")==null)?"0":rs.getString("POR_AFRO"); //F05-2014 
						BigDecimal  por_Aforo= new BigDecimal(porc_Aforo);  //F05-2014 
						BigDecimal  montoD= new BigDecimal(monto);  //F05-2014 /			
						BigDecimal   MontoDesconta = montoD.multiply(por_Aforo.divide(new BigDecimal("100"),10,BigDecimal.ROUND_HALF_UP)); //F05-2014			
			
						if (bandeVentaCartera.equals("S") ) {
							modalidad_plazo ="";
						}
						contenidoArchivo.append("\n"+epo.replace(',',' ')+","+numdoc+","+acuse+","+fecha_emision+","+df_fecha_venc+","+fecha_publicacion+","+plazodoc+","+moneda+","+monto+","+categoria+","+
						porc_Aforo+","+Comunes.formatoDecimal(MontoDesconta.toPlainString(),2,false)+","+// F05-2014
						plazdesc+","+porcdesc+","+monto_con_desc);
						if(!"".equals(cgTipoConversion))	{
							contenidoArchivo.append(","+((tipcambio.equals(""))?conversion:""));
							contenidoArchivo.append(","+((tipcambio.equals(""))?"":tipcambio));
							contenidoArchivo.append(","+((tipcambio.equals(""))?"":monto_valuado));
						}
						contenidoArchivo.append(","+modalidad_plazo+","+estatus+","+fechacambio+","+cambioestatus+","+cambiomotivo);
						aRow++;
						}	
						if (aRow == 0)	{
						contenidoArchivo.append("\nNo se Encontró Ningún Registro");	
						}
				}
				if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
					jsonObj.put("success", new Boolean(false));
					jsonObj.put("msg", "Error al generar el archivo");
				} else {
					nombreArchivo = archivo.nombre;
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
	
	} catch (Exception e) {
		throw new AppException("Error al generar el archivo", e);
	}	finally {
	}
%>
<%=jsonObj%>