<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.math.*,
	java.sql.*,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.seguridadbean.*,
	com.netro.model.catalogos.*,
	com.netro.descuento.*, 
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String clave_pyme = (String)request.getSession().getAttribute("iNoCliente");
String ic_epo 		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String ic_moneda 	= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
String mto_descuento 	= (request.getParameter("mto_descuento")==null)?"":request.getParameter("mto_descuento");
String cc_acuse 	= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
String fn_monto_de 	= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
String fn_monto_a 	= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
String df_fecha_emision_de 	= (request.getParameter("df_fecha_emision_de")==null)?"":request.getParameter("df_fecha_emision_de");
String df_fecha_emision_a 	= (request.getParameter("df_fecha_emision_a")==null)?"":request.getParameter("df_fecha_emision_a");
String ig_numero_docto 	= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
String df_fecha_venc_de 	= (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
String df_fecha_venc_a 	= (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
String fecha_publicacion_de 	= (request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
String fecha_publicacion_a 	= (request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
String doctos_cambio 	= (request.getParameter("doctos_cambio")==null)?"":request.getParameter("doctos_cambio");
String modo_plazo 	= (request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
String nePyme 		= (String)session.getAttribute("strNePymeAsigna");
String nombrePyme	= (String)session.getAttribute("strNombrePymeAsigna");
if(!doctos_cambio.equals("") ) doctos_cambio ="S";
if(!mto_descuento.equals("") ) mto_descuento ="S";
boolean regValido = true;

String infoRegresar = "";

//INSTANCIACION DE EJB'S
	AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
	
	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);
	
	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
	com.netro.seguridadbean.Seguridad seguridadEJB = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
		
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
		
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
	
	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);

	
	JSONArray registrosTotales = new JSONArray();	
	HashMap registrosTot = new HashMap();

	Vector	vecFilas			= null;
	Vector	vecFilas1			= null;
	Vector	vecFilas2			= null;
	Vector	vecFilasPN			= null;
	Vector	vecColumnas			= null;
	Combos	comboLinea			= null;
	String limitePyme = "";
	String limiteUtiliPyme = "";
	String mensaje_terminacion = "";
	String  ic_linea = "",  moneda_linea = "", saldo_inicial = "", montoSeleccionado ="", numDoctos = "",
	saldoDisponible = "",  hidSaldoDisponible  = "";		
	String ic_if ="";
	HashMap	documentos1 = new HashMap();
	HashMap	documentos2 = new HashMap();
	JSONObject 	resultado	= new JSONObject();
	JSONArray registros = new JSONArray();
	String	diasInhabiles = aceptPyme.getDiasInhabiles(clave_pyme,"");	
	int hidNumLineas =0;
	HashMap	MLineas = new HashMap();
	HashMap	datosAuxiliar = new HashMap();	
	//if(ic_epo.equals("")){ ic_epo = iNoEPO; } 	
	String auxIcIf = "", auxReferenciaTasa ="", auxIcTasa ="", auxCgRelMat = "",
	auxFnPuntos ="", auxValorTasa = "", auxIcMoneda ="", auxPlazoDias ="", 
	auxValorTasaPuntos  ="", auxTipoTasa  = "", auxPuntosPref = "";
	int numTasas  =0;	
	String fechaHoy		= "",  horaCarga="";
	try{
		fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		horaCarga = new java.text.SimpleDateFormat("HH:mm").format(new java.util.Date());
	}catch(Exception e){
		fechaHoy = "";
		horaCarga = "";
	}
	
if (!ic_epo.equals("")) {

	diasInhabiles = aceptPyme.getDiasInhabiles(clave_pyme,"");
	if(ic_epo!=null&&!"".equals(ic_epo)) {
		List lDatos = BeanParametros.getLimitePyme(ic_epo, clave_pyme, "");
		if(lDatos.size()>0) {
			lDatos 			= (ArrayList)lDatos.get(0);
			limitePyme 		= lDatos.get(2).toString();
			limiteUtiliPyme = lDatos.get(3).toString();
			mensaje_terminacion = lDatos.get(4).toString();
		}
	}
	//tipo de credito
	String tipoCredito = BeanParametros.obtieneTipoCredito (iNoEPO); 
	
	//monitor lineas de credito por Epo
	vecFilas = cargaDocto.monitorLineas(iNoEPO);
	if(vecFilas.size()>0) {		
	
		for(int i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);					
			ic_linea = (String)vecColumnas.get(0);
			moneda_linea = (String)vecColumnas.get(4);
			saldo_inicial = Comunes.formatoDecimal(vecColumnas.get(2).toString(),2,false);
			montoSeleccionado ="0.00";
			numDoctos = "0";
			saldoDisponible = Comunes.formatoDecimal(vecColumnas.get(3).toString(),2,false);
			hidSaldoDisponible  = Comunes.formatoDecimal(vecColumnas.get(3).toString(),2,false);			
			MLineas.put("ic_linea"+i,ic_linea);
			MLineas.put("moneda_linea"+i,moneda_linea);
			MLineas.put("saldo_inicial"+i,saldo_inicial);
			MLineas.put("montoSeleccionado"+i,montoSeleccionado);
			MLineas.put("numDoctos"+i,numDoctos);
			MLineas.put("saldoDisponible"+i,saldoDisponible);
			MLineas.put("hidSaldoDisponible"+i,hidSaldoDisponible);			
		}		
	}
	 hidNumLineas = vecFilas.size();
	

  // obtiene las EPO e IF que tienen una LC vigente	  
  	vecFilas2 = BeanTasas.esquemaParticular(ic_epo,4,"",iNoCliente); 
	int tm  =0;
	for (int j=0; j<vecFilas2.size(); j++) {
		Vector lovDatosTas = (Vector)vecFilas2.get(j);
		ic_if = lovDatosTas.get(0).toString();
		vecFilas1 = BeanTasas.ovgetTasasPreferNego("4","",ic_if,ic_epo,"",iNoCliente,"", "");
		
		//Reemplazamos por los valores de las tasas preferenciales
		vecFilasPN = BeanTasas.ovgetTasasPreferNego("4","P",ic_if,ic_epo,"",iNoCliente,"", "");
			

		for(int i=0;i<vecFilasPN.size();i++){
			String auxIcTasaBase	= "";
			String auxIcTasaPN		= "";
			vecColumnas = (Vector)vecFilasPN.get(i);
			auxIcTasaPN = (String)vecColumnas.get(2);
			for(int k=0;k<vecFilas1.size();k++){
				Vector vecColumnas2 = (Vector)vecFilas1.get(k);
				auxIcTasaBase = (String)vecColumnas2.get(2);
				if(auxIcTasaBase.equals(auxIcTasaPN)){
					vecFilas1.set(k,vecColumnas);
				}
			}
		}
		//out.println("SI PASO POR AQUI 2");
		//Reemplazamos por los valores de las tasas negociadas
		vecFilasPN = BeanTasas.ovgetTasasPreferNego("4","N",ic_if,ic_epo,"",iNoCliente,"", "");
		
		for(int i=0;i<vecFilasPN.size();i++){
			String auxIcTasaBase	= "";
			String auxIcTasaPN		= "";
			vecColumnas = (Vector)vecFilasPN.get(i);
			auxIcTasaPN = (String)vecColumnas.get(2);
			for(int k=0;k<vecFilas1.size();k++){
				Vector vecColumnas2 = (Vector)vecFilas1.get(k);
				auxIcTasaBase = (String)vecColumnas2.get(2);
				if(auxIcTasaBase.equals(auxIcTasaPN)){
					vecFilas1.set(k,vecColumnas);
				}
			}
		}	
		//out.println("SI PASO POR AQUI 3");
		for(int i=0;i<vecFilas1.size();i++){
			vecColumnas = (Vector)vecFilas1.get(i);	
			tm++;
			auxIcIf = ic_if;
			auxReferenciaTasa =(String)vecColumnas.get(1)+" "+(String)vecColumnas.get(5)+" "+(String)vecColumnas.get(6);
			auxIcTasa =(String)vecColumnas.get(2);
			auxCgRelMat = (String)vecColumnas.get(5);
			auxFnPuntos =(String)vecColumnas.get(6);
			auxValorTasa = (String)vecColumnas.get(4);
			auxIcMoneda =(String)vecColumnas.get(13);
			auxPlazoDias =(String)vecColumnas.get(14);
			auxValorTasaPuntos  =(String)vecColumnas.get(11);
			auxTipoTasa  = (String)vecColumnas.get(16);
			auxPuntosPref =  (String)vecColumnas.get(15);
			numTasas++;	
			
			datosAuxiliar.put("auxIcIf"+tm,auxIcIf);
			datosAuxiliar.put("auxReferenciaTasa"+tm,auxReferenciaTasa);
			datosAuxiliar.put("auxIcTasa"+tm,auxIcTasa);
			datosAuxiliar.put("auxCgRelMat"+tm,auxCgRelMat);
			datosAuxiliar.put("auxFnPuntos"+tm,auxFnPuntos);
			datosAuxiliar.put("auxValorTasa"+tm,auxValorTasa);
			datosAuxiliar.put("auxIcMoneda"+tm,auxIcMoneda);
			datosAuxiliar.put("auxPlazoDias"+tm,auxPlazoDias);
			datosAuxiliar.put("auxValorTasaPuntos"+tm,auxValorTasaPuntos);
			datosAuxiliar.put("auxTipoTasa"+tm,auxTipoTasa);
			datosAuxiliar.put("auxPuntosPref"+tm,auxPuntosPref);	
		}			
	}
	
	String bloqueoPymeEpo =   afiliacion.getPyme_Epo_Bloqueo("4",  clave_pyme, ic_epo);  //F015-2014
	
	
	resultado.put("success", new Boolean(true));
	resultado.put("MLineas", MLineas);
	resultado.put("Auxiliar", datosAuxiliar);
	resultado.put("bloqueoPymeEpo", bloqueoPymeEpo);
	
	infoRegresar = resultado.toString();
		
}
if (informacion.equals("ResumenTotales")  ||  informacion.equals("Consultar")  ||  !ic_epo.equals("")) {
 	
	//realiza la consulta 
	vecFilas = aceptPyme.consultaDoctos(clave_pyme,ic_epo,ic_moneda,ig_numero_docto,fn_monto_de,fn_monto_a, cc_acuse,mto_descuento,df_fecha_emision_de,df_fecha_emision_a,doctos_cambio,df_fecha_venc_de,df_fecha_venc_a,modo_plazo,fecha_publicacion_de,fecha_publicacion_a,null);
	vecFilas1 = (Vector)vecFilas.get(0);
	vecFilas2 = (Vector)vecFilas.get(1);

}
//******	Inicializacion de Catalogo 
if (informacion.equals("CatalogoEPO") ) {		
		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClavePyme(clave_pyme);
		cat.setValoresCondicionIn (ic_epo, Integer.class);
		infoRegresar = cat.getJSONElementos();	
		
} else  if (informacion.equals("CatalogoMoneda") ) {

		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setTabla("comcat_moneda");		
		catalogo.setOrden("ic_moneda");
		catalogo.setValoresCondicionIn("1,54", Integer.class);
		infoRegresar = catalogo.getJSONElementos();	


} else  if (informacion.equals("CatalogoModalidadPlazo") ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_tipo_financiamiento");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_tipo_financiamiento");
	catalogo.setOrden("ic_tipo_financiamiento");
	catalogo.setValoresCondicionIn("1,2,3,4", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
	
} else  if (informacion.equals("Consultar") ) {

	double valorTasaInt	=	0,  montoTasaInt	=	0,	 fnPuntos	= 0	,montoCredito = 0;
	int totalDoctosMN		=	0, totalDoctosUSD	=0,	totalCreditosMN	=	0, 	totalCreditosUSD	=	0;
	double totalMontoMN	=	0, totalMontoUSD		=	0, totalMontoDescMN		=	0, totalMontoDescUSD	=	0,
	totalInteresMN		  =	0, totalInteresUSD		=	0, totalMontoValuadoMN	= 	0, totalMontoValuadoUSD	= 	0,
	totalMontoCreditoMN	=	0, totalMontoCreditoUSD	=	0, montoValuadoLinea	= 	0,
	fnMonto =0,	 montoDescuento =0,  montoValuado  = 0;
	
	String nombreEpo ="", igNumeroDocto	="", ccAcuse  ="", dfFechaEmision ="", dfFechaVencimiento ="",
	dfFechaPublicacion 	="", igPlazoDocto ="", moneda  ="",  cgTipoConv	 ="", tipoCambio ="",
	igPlazoDescuento ="", fnPorcDescuento	="",  modoPlazo	 ="", estatus ="", cambios ="",
	numeroCredito ="", nombreIf	="",  tipoLinea	 ="",  fechaOperacion	="",  plazoCredito	="",
	fechaVencCredito ="", relMat ="", tipoCobroInt ="",  referencia	="", tipoPiso ="", icTipoCobroInt ="",
	icMoneda ="",  icTipoFinanciamiento="",  monedaLinea	="", dias_minimo ="",	dias_maximo ="",  
	icLineaCreditoDM	="", categoria="", nombreMLinea ="",   pymeBloqueadaDisxIF = "";
	String datosFinales = "N";
	int elementos =0;
	
	String bloqueoPymeEpo =   afiliacion.getPyme_Epo_Bloqueo("4",  clave_pyme, ic_epo);  //F015-2014
	String operalineaCreditoIF = "", fechaVencimientoLinea = "";
	
	
	//esto es para la consulta en la que los campos no son editables 
	if(vecFilas1.size()>0){
		datosFinales="N";		
		totalDoctosMN = 0;
		totalMontoMN = 0;
		totalMontoDescMN = 0;
		totalDoctosUSD= 0;
		totalMontoUSD = 0;
		totalMontoDescUSD = 0;		
		for(int i=0;i<vecFilas1.size();i++){		
			elementos++;
			vecColumnas = (Vector)vecFilas1.get(i);
			nombreEpo			=	(String)vecColumnas.get(0);
			igNumeroDocto		=	(String)vecColumnas.get(1);
			ccAcuse 			=	(String)vecColumnas.get(2);
			dfFechaEmision		=	(String)vecColumnas.get(3);
			dfFechaVencimiento	=	(String)vecColumnas.get(4);
			dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
			igPlazoDocto		=	(String)vecColumnas.get(6);
			moneda 				=	(String)vecColumnas.get(7);
			fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			cgTipoConv			=	(String)vecColumnas.get(9);
			tipoCambio			=   (String)vecColumnas.get(10);
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			igPlazoDescuento	= 	(String)vecColumnas.get(12);
			fnPorcDescuento		= 	(String)vecColumnas.get(13);
			modoPlazo			= 	(String)vecColumnas.get(14);
			estatus 			=	(String)vecColumnas.get(15);
			cambios				= 	(String)vecColumnas.get(16);
			//creditos por concepto de intereses
			numeroCredito		= 	(String)vecColumnas.get(17);
			nombreIf			= 	(String)vecColumnas.get(18);
			tipoLinea			=	(String)vecColumnas.get(19);
			fechaOperacion		=	(String)vecColumnas.get(20);
			plazoCredito		=	(String)vecColumnas.get(22);
			fechaVencCredito	=	(String)vecColumnas.get(23);
			relMat				= 	(String)vecColumnas.get(25);
			tipoCobroInt		=	(String)vecColumnas.get(27);
			referencia			=	(String)vecColumnas.get(29);
			tipoPiso			= 	(String)vecColumnas.get(30);
			icTipoCobroInt		= 	(String)vecColumnas.get(31);
			icMoneda			= 	(String)vecColumnas.get(32);
			icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			monedaLinea			= 	(String)vecColumnas.get(35);
			dias_minimo			= 	(String)vecColumnas.get(36);
			dias_maximo			= 	(String)vecColumnas.get(37);
		  ic_if				= 	(String)vecColumnas.get(38);
		  icLineaCreditoDM	= 	(String)vecColumnas.get(39);
			categoria			= 	(String)vecColumnas.get(41);		
			pymeBloqueadaDisxIF = (String)vecColumnas.get(46);	
			String   descuentoAforo1 = 	(String)vecColumnas.get(47)==null?"0":(String)vecColumnas.get(47); //F05-2014
			double  descuentoAforo= Double.parseDouble(vecColumnas.get(47).toString());  //F05-2014
			if (descuentoAforo1.equals("0"))  {  	descuentoAforo = 100;   	}	//F05-2014
			double fnMontoDescuento =  0;
			
			BigDecimal  porSubLimite= new BigDecimal(vecColumnas.get(48).toString());  //F05-2014 // % LineaSublimite
			
			int valor = porSubLimite.compareTo(new BigDecimal("0"));
			if(valor==0)  {
				porSubLimite=new BigDecimal("100");
				//System.out.println(" La Pyme no tiene parametrizado el  Sub-límites Distribuidor y se le asignara 100% ");
			}
			
			BigDecimal  montoLinea= new BigDecimal(vecColumnas.get(49).toString());  //F05-2014 // Monto Linea de Credito	
			BigDecimal   MontoSubLimiteDistribuidor  = montoLinea.multiply(porSubLimite).divide(new BigDecimal("100"),2);//F05-2014
		
			//System.out.println(" MontoSubLimiteDistribuidor   "+MontoSubLimiteDistribuidor);	
		
			String  montoDocutosT =  aceptPyme.getMontoTotalMocumentos(ic_epo, icMoneda ,ic_if, "1",  "", icLineaCreditoDM  ) ; //F05-2014  // Monto Total de doctos en estatus  3, 4, 24 
			BigDecimal  MontoTotalDocumentos= new BigDecimal(montoDocutosT);  //F05-2014 // Monto Linea de Credito
			BigDecimal  MontoDispoSubLimiteDistribuidor  = MontoSubLimiteDistribuidor.subtract(MontoTotalDocumentos);//F05-2014
			String saldoDispLinea			=	(String)vecColumnas.get(50);	 
			
			
			if("1".equals(icTipoFinanciamiento) || "4".equals(icTipoFinanciamiento)){
				fechaVencCredito = dfFechaVencimiento;
			}
			if("+".equals(relMat))
				valorTasaInt += fnPuntos;
			else if("-".equals(relMat))
				valorTasaInt -= fnPuntos;
			else if("*".equals(relMat))
				valorTasaInt *= fnPuntos;
			else if("/".equals(relMat))
				valorTasaInt /= fnPuntos;
				
			montoValuadoLinea = fnMonto;
			plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
			if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento)||"4".equals(icTipoFinanciamiento))
				montoCredito = fnMonto-montoDescuento;
			else if("2".equals(icTipoFinanciamiento))
				montoCredito = fnMonto;
			//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
				if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
					montoCredito = montoValuado;
				else if("2".equals(icTipoFinanciamiento))
					montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
			}
			if("1".equals(tipoPiso)){
				montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
			}else{
				montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
			}
		
			double monto_tasa_int =0; 
			if( !icTipoCobroInt.equals("S") ){
			monto_tasa_int	= ((double)Math.round(montoTasaInt*Double.parseDouble(plazoCredito)*100)/100);
			}
			
			if(icTipoCobroInt.equals("S") ){
				valorTasaInt = valorTasaInt;
			}
			double montoCredito2=  ((double)Math.round(montoCredito*100)/100);
			
			if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
				fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
			} else if("2".equals(icTipoFinanciamiento)  )  {			
				fnMontoDescuento	= fnMonto *(descuentoAforo/100);   //F05-2014 
			}  
			
			operalineaCreditoIF = (String)vecColumnas.get(51);	
			fechaVencimientoLinea = (String)vecColumnas.get(52);	
			
			documentos1 = new HashMap();
			documentos1.put("NOMBRE_PYME",nombrePyme);
			documentos1.put("NOMBRE_EPO",nombreEpo);
			documentos1.put("IG_DOCUMENTO",igNumeroDocto);
			documentos1.put("IC_DOCUMENTO",numeroCredito);
			documentos1.put("ACUSE",ccAcuse);
			documentos1.put("DF_FECHA_EMISION",dfFechaEmision);
			documentos1.put("DF_FECHA_VENCIMIENTO",dfFechaVencimiento);
			documentos1.put("DF_FECHA_PUBLICACION",dfFechaPublicacion);
			documentos1.put("IG_PLAZO_DOCTO",igPlazoDocto);
			documentos1.put("MONEDA",moneda);
			documentos1.put("IC_MONEDA_DOCTO",icMoneda);
			documentos1.put("IC_MONEDA_LINEA",monedaLinea);
			documentos1.put("IC_IF",ic_if);
			documentos1.put("TIPO_PISO",tipoPiso);
			documentos1.put("IC_LINEA_CREDITO_DM",icLineaCreditoDM);
			documentos1.put("FN_MONTO",Double.toString (fnMonto));			
			documentos1.put("CATEGORIA",categoria);
			documentos1.put("IG_PLAZO_DESCUENTO",igPlazoDescuento);
			documentos1.put("PORCENTAJE_DESCUENTO",fnPorcDescuento);
			documentos1.put("MONTO_DESCUENTO",Double.toString(montoDescuento));			
			documentos1.put("MONTO_CREDITO",Double.toString(montoCredito2));
			documentos1.put("CG_TIPO_CONV",cgTipoConv);
			documentos1.put("TIPO_CAMBIO",tipoCambio);
			documentos1.put("MONTO_VALUADO",Double.toString(fnMontoDescuento));
		  documentos1.put("MODO_PLAZO",modoPlazo);
			documentos1.put("ESTATUS",estatus);	
			documentos1.put("MODIFICADO","N");	
			documentos1.put("VALOR_TASA",auxValorTasa);	
			documentos1.put("VALOR_TASA_PUNTOS",Double.toString(valorTasaInt)  );
			documentos1.put("REFERENCIA_TASA", auxReferenciaTasa);	
			documentos1.put("FECHA_EMISION", dfFechaEmision);				
			documentos1.put("FECHA_VEN_CREDITO", fechaVencCredito);	
			documentos1.put("MONTO_TASA_INT", Double.toString(monto_tasa_int) );
			documentos1.put("MONTO_AUX_INT", Double.toString(montoTasaInt) );	
			documentos1.put("IC_TASA", auxIcTasa);	
			documentos1.put("CG_REL_MAT", relMat);	
			documentos1.put("FN_PUNTOS", auxFnPuntos );	
			documentos1.put("MONTO_CAPITAL_INT", "0.00");	//ESTE HAY QUE REVISARLO DE DONDE SE ESTA LLENANDO
			documentos1.put("TIPO_TASA", auxTipoTasa);	
			documentos1.put("PUNTOS_PREF", auxPuntosPref);	
			documentos1.put("IC_EPO", ic_epo);		
			documentos1.put("LIMITEPYME", limitePyme);	
			documentos1.put("LIMITE_ULTILI_PYME", limiteUtiliPyme);	
			documentos1.put("MENSAJE_TERMINACION", mensaje_terminacion);	
			documentos1.put("PLAZO_CREDITO", plazoCredito);				
			documentos1.put("DIAS_MINIMOS", dias_minimo);
			documentos1.put("DIAS_MAXIMO", dias_maximo);
			documentos1.put("DIAS_MAXFV", "");
			documentos1.put("FECHA_VENC_MAX", "");			
			documentos1.put("EDITABLE","N");			
			documentos1.put("SELECBOOL",new Boolean(regValido));
			documentos1.put("SELECCION","N");
			documentos1.put("HIDNUMTASAS", Double.toString(numTasas) );	
			documentos1.put("FECHA_OPERACION",fechaHoy);	
			documentos1.put("HIDNUMLINEAS", String.valueOf(hidNumLineas));
			documentos1.put("PYME_BLOQ_X_IF", pymeBloqueadaDisxIF);
			documentos1.put("DESCUENTO_AFORO", Double.toString (descuentoAforo)  );
			documentos1.put("MONTO_DESCONTAR", Double.toString (fnMontoDescuento)  );
			documentos1.put("MONTO_DIS_SUB_LIMITE", MontoDispoSubLimiteDistribuidor.toString()  );				
			documentos1.put("SALDO_DISPONIBLE", saldoDispLinea  );	
			documentos1.put("CG_LINEA_CREDITO", operalineaCreditoIF.trim()  );	
			documentos1.put("FECHA_VENC_LINEA", fechaVencimientoLinea  );	
					
			registros.add(documentos1);			
		}//for 	
	}

		//esta partet es para cuando los campos son Editables 
	if(vecFilas2.size()>0){
		datosFinales ="S";
		totalDoctosMN = 0;
		totalMontoMN = 0;
		totalMontoDescMN = 0;
		totalDoctosUSD= 0;
		totalMontoUSD = 0;
		totalMontoDescUSD = 0;
			
		for(int i=0;i<vecFilas2.size();i++){
		
			elementos++;		
			vecColumnas = (Vector)vecFilas2.get(i);
			nombreEpo			=	(String)vecColumnas.get(0)==null?"":(String)vecColumnas.get(0);
			igNumeroDocto		=	(String)vecColumnas.get(1)==null?"":(String)vecColumnas.get(1);
			ccAcuse 			=	(String)vecColumnas.get(2)==null?"":(String)vecColumnas.get(2);
			dfFechaEmision		=	(String)vecColumnas.get(3)==null?"":(String)vecColumnas.get(3);
			dfFechaVencimiento	=	(String)vecColumnas.get(4)==null?"":(String)vecColumnas.get(4);
			dfFechaPublicacion 	= 	(String)vecColumnas.get(5)==null?"":(String)vecColumnas.get(5);
			igPlazoDocto		=	(String)vecColumnas.get(6)==null?"":(String)vecColumnas.get(6);
			moneda 				=	(String)vecColumnas.get(7)==null?"":(String)vecColumnas.get(7);
			fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			cgTipoConv			=	(String)vecColumnas.get(9)==null?"":(String)vecColumnas.get(9);
			tipoCambio			=   (String)vecColumnas.get(10)==null?"":(String)vecColumnas.get(10);
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			igPlazoDescuento	= 	(String)vecColumnas.get(12)==null?"":(String)vecColumnas.get(12);
			fnPorcDescuento		= 	(String)vecColumnas.get(13)==null?"":(String)vecColumnas.get(13);
			modoPlazo			= 	(String)vecColumnas.get(14)==null?"":(String)vecColumnas.get(14);
			estatus 			=	(String)vecColumnas.get(15)==null?"":(String)vecColumnas.get(15);
			cambios				= 	(String)vecColumnas.get(16)==null?"":(String)vecColumnas.get(16);
			//creditos por concepto de intereses
			numeroCredito		= 	(String)vecColumnas.get(17);
			nombreIf			= 	(String)vecColumnas.get(18);
			tipoLinea			=	(String)vecColumnas.get(19);
			fechaOperacion		=	(String)vecColumnas.get(20);
			plazoCredito		=	(String)vecColumnas.get(22);
			fechaVencCredito	=	(String)vecColumnas.get(23)==null?"":(String)vecColumnas.get(23);
			relMat				= 	(String)vecColumnas.get(25);
			tipoCobroInt		=	(String)vecColumnas.get(27);
			referencia			=	(String)vecColumnas.get(29);
			tipoPiso			= 	(String)vecColumnas.get(30);
			icTipoCobroInt		= 	(String)vecColumnas.get(31);
			icMoneda			= 	(String)vecColumnas.get(32);
			icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			monedaLinea			= 	(String)vecColumnas.get(35);
			dias_minimo			= 	(String)vecColumnas.get(36);
			dias_maximo			= 	(String)vecColumnas.get(37);
			ic_if				= 	(String)vecColumnas.get(38);
			icLineaCreditoDM	= 	(String)vecColumnas.get(39);
			nombreMLinea		= 	(String)vecColumnas.get(40);
			categoria			= 	(String)vecColumnas.get(41);
			String diasMaxFV	=  	vecColumnas.get(42).toString();
			String fechaVencMax	=  	vecColumnas.get(43).toString();		
			
			pymeBloqueadaDisxIF = (String)vecColumnas.get(46);	 //F05-2014
			String   descuentoAforo1 = 	(String)vecColumnas.get(47)==null?"0":(String)vecColumnas.get(47); //F05-2014
			double  descuentoAforo= Double.parseDouble(vecColumnas.get(47).toString());  //F05-2014
			if (descuentoAforo1.equals("0"))  {  	descuentoAforo = 100;   	}	//F05-2014
			double fnMontoDescuento =0;   //F05-2014
		
			BigDecimal  porSubLimite= new BigDecimal(vecColumnas.get(48).toString());  //F05-2014 // % LineaSublimite	
			int valor = porSubLimite.compareTo(new BigDecimal("0"));
			if(valor==0)  {
				porSubLimite=new BigDecimal("100");
				//System.out.println(" La Pyme no tiene parametrizado el  Sub-límites Distribuidor y se le asignara 100% ");
			}
			
			BigDecimal  montoLinea= new BigDecimal(vecColumnas.get(49).toString());  //F05-2014 // Monto Linea de Credito	
			
			BigDecimal   MontoSubLimiteDistribuidor  = montoLinea.multiply(porSubLimite).divide(new BigDecimal("100"),2);//F05-2014
			//System.out.println(" MontoSubLimiteDistribuidor   "+MontoSubLimiteDistribuidor);		   
			
			String  montoDocutosT =  aceptPyme.getMontoTotalMocumentos(ic_epo, icMoneda ,ic_if, "1", clave_pyme , icLineaCreditoDM) ; //F05-2014  // Monto Total de doctos en estatus  3, 4, 24 
			BigDecimal  MontoTotalDocumentos= new BigDecimal(montoDocutosT);  //F05-2014 // Monto Linea de Credito
			BigDecimal  MontoDispoSubLimiteDistribuidor  = MontoSubLimiteDistribuidor.subtract(MontoTotalDocumentos);//F05-2014
			String saldoDispLinea			=	(String)vecColumnas.get(50);
		
		
			if("1".equals(icTipoFinanciamiento) || "4".equals(icTipoFinanciamiento)){
				fechaVencCredito = dfFechaVencimiento;
			}	
			if("+".equals(relMat))
				valorTasaInt += fnPuntos;
			else if("-".equals(relMat))
				valorTasaInt -= fnPuntos;
			else if("*".equals(relMat))
				valorTasaInt *= fnPuntos;
			else if("/".equals(relMat))
				valorTasaInt /= fnPuntos;	
			montoValuadoLinea = fnMonto;					
			plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;	
			if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento)||"4".equals(icTipoFinanciamiento))
				montoCredito = fnMonto-montoDescuento;
			else if("2".equals(icTipoFinanciamiento))
				montoCredito = fnMonto;					
			//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
			if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
				montoCredito = montoValuado;
			else if("2".equals(icTipoFinanciamiento))
				montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
			}			
			if("1".equals(tipoPiso)){
				montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
			}else{
				montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
			}			
			if("2".equals(icTipoFinanciamiento)){
				igPlazoDescuento = "";
				fnPorcDescuento =	"";				
			} 
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){					
		
			}else {
				cgTipoConv ="";
				tipoCambio ="";
				montoValuado =0;
			}			
			double monto_tasa_int =0; 
			if( !icTipoCobroInt.equals("S") ){
				monto_tasa_int	= ((double)Math.round(montoTasaInt*Double.parseDouble(plazoCredito)*100)/100);
			}						
			if( !icTipoCobroInt.equals("S") ){
				valorTasaInt = valorTasaInt;
			}
			if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
				fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
			} else if("2".equals(icTipoFinanciamiento)  )  {			
				fnMontoDescuento	= fnMonto *(descuentoAforo/100);   //F05-2014
			}
			
			operalineaCreditoIF = (String)vecColumnas.get(51);	
			fechaVencimientoLinea = (String)vecColumnas.get(52);	
			
			documentos2 = new HashMap();
			documentos2.put("NOMBRE_PYME",nombrePyme);
			documentos2.put("NOMBRE_EPO",nombreEpo);	
			documentos2.put("IG_DOCUMENTO",igNumeroDocto);		
			documentos2.put("ACUSE",ccAcuse);
			documentos2.put("DF_FECHA_EMISION",dfFechaEmision);
			documentos2.put("DF_FECHA_VENCIMIENTO",dfFechaVencimiento);
			documentos2.put("DF_FECHA_PUBLICACION",dfFechaPublicacion);
			documentos2.put("IG_PLAZO_DOCTO",igPlazoDocto);
			documentos2.put("IC_LINEA_CREDITO_DM",icLineaCreditoDM);
			documentos2.put("MONEDA",moneda);
			documentos2.put("IC_MONEDA_DOCTO",icMoneda);
			documentos2.put("IC_MONEDA_LINEA",monedaLinea);
			documentos2.put("IC_IF",ic_if);
			documentos2.put("TIPO_PISO",tipoPiso);
			documentos2.put("FN_MONTO",Double.toString (fnMonto));			
			documentos2.put("CATEGORIA",categoria);
			documentos2.put("IC_TIPO_FINANCIAMIENTO",icTipoFinanciamiento);	
			documentos2.put("IG_PLAZO_DESCUENTO",igPlazoDescuento);	
			documentos2.put("PORCENTAJE_DESCUENTO",fnPorcDescuento);
			documentos2.put("MONTO_DESCUENTO",Double.toString (montoDescuento));	
			documentos2.put("CG_TIPO_CONV",cgTipoConv);
			documentos2.put("TIPO_CAMBIO",tipoCambio);
			documentos2.put("MONTO_VALUADO",Double.toString(fnMontoDescuento));
			documentos2.put("MODO_PLAZO",modoPlazo);
			documentos2.put("IC_DOCUMENTO",numeroCredito);
			documentos2.put("NOMBREMLINEA",nombreMLinea);
			documentos2.put("MONTO_CREDITO",Double.toString(montoCredito));
			documentos2.put("PLAZO_CREDITO",plazoCredito);
			documentos2.put("FECHA_VEN_CREDITO",fechaVencCredito);			
			documentos2.put("NOMBRE_IF",nombreIf);
			documentos2.put("REFERENCIA_TASA",auxReferenciaTasa);
			documentos2.put("IC_TASA",auxIcTasa);
			documentos2.put("CG_REL_MAT",relMat);
			documentos2.put("FN_PUNTOS",auxFnPuntos);
			documentos2.put("VALOR_TASA",auxValorTasa);
			documentos2.put("TIPO_TASA",auxTipoTasa);
			documentos2.put("PUNTOS_PREF",auxPuntosPref);
			documentos2.put("VALOR_TASA_PUNTOS",Double.toString(valorTasaInt)  );
			documentos2.put("MONTO_CAPITAL_INT","0.00");
			documentos2.put("MODIFICADO","N");
			documentos2.put("HIDNUMLINEAS", String.valueOf(hidNumLineas));
			documentos2.put("MONTO_TASA_INT", Double.toString(monto_tasa_int) );	
			documentos2.put("IC_EPO", ic_epo);
			documentos2.put("LIMITEPYME", limitePyme);	
			documentos2.put("LIMITE_ULTILI_PYME", limiteUtiliPyme);	
			documentos2.put("MENSAJE_TERMINACION", mensaje_terminacion);	
			documentos2.put("DIAS_MINIMOS", dias_minimo);
			documentos2.put("DIAS_MAXIMO", dias_maximo);
			documentos2.put("DIAS_MAXFV", diasMaxFV);
			documentos2.put("FECHA_VENC_MAX", fechaVencMax);
			documentos2.put("MONTO_AUX_INT", Double.toString(montoTasaInt) );	
			documentos2.put("HIDNUMTASAS", Double.toString(numTasas) );	
			documentos2.put("EDITABLE","S");		
			documentos2.put("SELECBOOL",new Boolean(regValido));
			documentos2.put("SELECCION","N");			
			documentos2.put("FECHA_OPERACION",fechaHoy);	
			documentos2.put("PYME_BLOQ_X_IF", pymeBloqueadaDisxIF);
			documentos2.put("DESCUENTO_AFORO", Double.toString (descuentoAforo)  );
			documentos2.put("MONTO_DESCONTAR", Double.toString (fnMontoDescuento)  );
			documentos2.put("MONTO_DIS_SUB_LIMITE", MontoDispoSubLimiteDistribuidor.toString()  );
			documentos2.put("SALDO_DISPONIBLE", saldoDispLinea  );
			documentos2.put("CG_LINEA_CREDITO", operalineaCreditoIF.trim()  );	
			documentos2.put("FECHA_VENC_LINEA", fechaVencimientoLinea  );				
			registros.add(documentos2);	
			
		}	//for
	
		int rowspanCalcula = 0;
			int colspantot = 6;
		if(totalDoctosMN>0||totalCreditosMN>0) 
			rowspanCalcula ++;
		if(totalDoctosUSD>0||totalCreditosUSD>0)
			rowspanCalcula ++;
		if("NAFIN".equals(strTipoUsuario))
			colspantot++;
	}	
	
	String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consulta);	
	resultado.put("DATOSFINALES",datosFinales);
	resultado.put("ElEMENTOS", String.valueOf(elementos));		
	resultado.put("MLineas", MLineas);
	resultado.put("Auxiliar", datosAuxiliar);
	resultado.put("bloqueoPymeEpo", bloqueoPymeEpo);
	infoRegresar = resultado.toString();

} else  if (informacion.equals("ResumenTotales") ) {
		
	double valorTasaInt	=	0,  montoTasaInt	=	0,	 fnPuntos	= 0	,montoCredito = 0;
	int totalDoctosMN		=	0, totalDoctosUSD	=0,	totalCreditosMN	=	0, 	totalCreditosUSD	=	0;
	double totalMontoMN	=	0, totalMontoUSD		=	0, totalMontoDescMN		=	0, totalMontoDescUSD	=	0,
	totalInteresMN		  =	0, totalInteresUSD		=	0, totalMontoValuadoMN	= 	0, totalMontoValuadoUSD	= 	0,
	totalMontoCreditoMN	=	0, totalMontoCreditoUSD	=	0, montoValuadoLinea	= 	0,
	fnMonto =0,	 montoDescuento =0,  montoValuado  = 0, totalMontoMNDesA =0, totalMontoUSDDesA =0;	
	String tipoCambio ="", icMoneda ="", monedaLinea ="", cgTipoConv ="", icTipoFinanciamiento ="";
	double fnMontoDescuento =  0;
	
	
	if(vecFilas1.size()>0){
		totalDoctosMN = 0;
		totalMontoMN = 0;
		totalMontoDescMN = 0;
		totalDoctosUSD= 0;
		totalMontoUSD = 0;
		totalMontoDescUSD = 0;	
		
		for(int i=0;i<vecFilas1.size();i++){		
			vecColumnas = (Vector)vecFilas1.get(i);
			tipoCambio			=   (String)vecColumnas.get(10);
			fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			icMoneda			= 	(String)vecColumnas.get(32);	
			icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			String   descuentoAforo1 = 	(String)vecColumnas.get(47)==null?"0":(String)vecColumnas.get(47); //F05-2014
			double  descuentoAforo= Double.parseDouble(vecColumnas.get(47).toString());  //F05-2014
			if (descuentoAforo1.equals("0"))  {  	descuentoAforo = 100;   	}	//F05-2014
			fnMontoDescuento =0;   //F05-2014		
			
			if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
				fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
			} else if("2".equals(icTipoFinanciamiento)  )  {			
				fnMontoDescuento	=  fnMonto *(descuentoAforo/100);   //F05-2014
			}			
		
			if("1".equals(icMoneda)){
				totalDoctosMN++;
				totalMontoMN += fnMonto;
				totalMontoDescMN += montoDescuento;
				totalMontoValuadoMN += montoValuado;
				totalInteresMN += valorTasaInt;
				totalMontoMNDesA +=fnMontoDescuento; //F05-2014
			}else{
				totalDoctosUSD++;
				totalMontoUSD += fnMonto;
				totalMontoDescUSD += montoDescuento;
				totalMontoValuadoUSD += montoValuado;
				totalMontoUSDDesA +=fnMontoDescuento;  //F05-2014
			}			
		}//for 
		
		for(int t =0; t<2; t++) {		
		registrosTot = new HashMap();
		
			if(t==0){ 				
			registrosTot.put("MONEDAT", "MONEDA NACIONAL");
			registrosTot.put("ICMONEDA", "1");
			registrosTot.put("TOTALT", Double.toString(totalDoctosMN) );	
			registrosTot.put("MONTOT", Double.toString(totalMontoMN));	
			registrosTot.put("TOTAL_MONTO_DESCT", Double.toString(totalMontoDescMN)  );	
			registrosTot.put("TOTAL_MONTO_VALUADOT", Double.toString(totalMontoMNDesA)  );
			registrosTot.put("MONTOINTERESF", "0.0");	
			registrosTot.put("MONTOCAPITALINTERESF", "0.0");	
			registrosTot.put("MONTO_DESCONTAR", Double.toString(totalMontoMNDesA)); //F05-2014			
			
		}
		if(t==1){ 			
			registrosTot.put("MONEDAT", "MONEDA USD");
			registrosTot.put("ICMONEDA", "54");
			registrosTot.put("TOTALT", Double.toString(totalDoctosUSD)  );	
			registrosTot.put("MONTOT", Double.toString(totalMontoUSD)  );	
			registrosTot.put("TOTAL_MONTO_DESCT", Double.toString(totalMontoDescUSD)  );	
			registrosTot.put("TOTAL_MONTO_VALUADOT", Double.toString(totalMontoUSDDesA)  );			
			registrosTot.put("MONTOINTERESF", "0.0");	
			registrosTot.put("MONTOCAPITALINTERESF", "0.0");	
			registrosTot.put("MONTO_DESCONTAR", Double.toString(totalMontoUSDDesA)); //F05-2014		
		}
		registrosTotales.add(registrosTot);
	}	
		
	}
	if(vecFilas2.size()>0){				
		
		totalDoctosMN = 0;
		totalMontoMN = 0;
		totalMontoDescMN = 0;
		totalDoctosUSD= 0;
		totalMontoUSD = 0;
		totalMontoDescUSD = 0;
			
		for(int i=0;i<vecFilas2.size();i++){
		
			vecColumnas = (Vector)vecFilas2.get(i);
			fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			tipoCambio			=   (String)vecColumnas.get(10);
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			icMoneda			= 	(String)vecColumnas.get(32);
			monedaLinea			= 	(String)vecColumnas.get(35);
			cgTipoConv			=	(String)vecColumnas.get(9);
			icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			String   descuentoAforo1 = 	(String)vecColumnas.get(47)==null?"0":(String)vecColumnas.get(47); //F05-2014
			double  descuentoAforo= Double.parseDouble(vecColumnas.get(47).toString());  //F05-2014
			if (descuentoAforo1.equals("0"))  {  	descuentoAforo = 100;   	}	//F05-2014
			fnMontoDescuento =0;   //F05-2014				
			
			if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
				fnMontoDescuento	= (double)Math.round( (fnMonto-montoDescuento) *descuentoAforo);   //F05-2014					
			} else if("2".equals(icTipoFinanciamiento)  )  {			
				fnMontoDescuento	= (double)Math.round( fnMonto *descuentoAforo);   //F05-2014
			}
			
			if("1".equals(icMoneda)){
				totalDoctosMN++;
				totalMontoMN += fnMonto;
				totalMontoDescMN += montoDescuento;
				totalInteresMN += valorTasaInt;
				totalMontoMNDesA +=fnMontoDescuento; //F05-2014
			}else{
				totalDoctosUSD++;
				totalMontoUSD += fnMonto;
				totalMontoDescUSD += montoDescuento;
				totalInteresUSD += valorTasaInt;
				if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
					totalMontoValuadoUSD += montoValuado;
				}
				totalMontoUSDDesA +=fnMontoDescuento;  //F05-2014
				
			}
			if("1".equals(monedaLinea)){
				totalCreditosMN++;
				totalMontoCreditoMN += montoCredito;
			}else{
				totalCreditosUSD++;
				totalMontoCreditoUSD += montoCredito;
			}		
		}	
		
		for(int t =0; t<2; t++) {		
		registrosTot = new HashMap();		
			if(t==0){ 				
			registrosTot.put("MONEDAT", "MONEDA NACIONAL");
			registrosTot.put("TOTALT", Double.toString(totalDoctosMN) );	
			registrosTot.put("MONTOT", Double.toString(totalMontoMN));	
			registrosTot.put("TOTAL_MONTO_DESCT", Double.toString(totalMontoDescMN)  );	
			registrosTot.put("TOTAL_MONTO_VALUADOT", Double.toString(totalMontoMNDesA)  );
			registrosTot.put("MONTO_DESCONTAR", Double.toString(totalMontoMNDesA)); //F05-2014	
		}
		if(t==1){ 			
			registrosTot.put("MONEDAT", "MONEDA USD");
			registrosTot.put("TOTALT", Double.toString(totalDoctosUSD)  );	
			registrosTot.put("MONTOT", Double.toString(totalMontoUSD)  );	
			registrosTot.put("TOTAL_MONTO_DESCT", Double.toString(totalMontoDescUSD)  );	
			registrosTot.put("TOTAL_MONTO_VALUADOT", Double.toString(totalMontoUSDDesA)  );
			registrosTot.put("MONTO_DESCONTAR", Double.toString(totalMontoUSDDesA)); //F05-2014	
		}
		registrosTotales.add(registrosTot);
	}	
	
		/*if(totalDoctosMN>0||totalCreditosMN>0){ 		
			registrosTot.put("TOTAL", Double.toString(totalDoctosMN));
			registrosTot.put("MONTO", Double.toString(totalMontoMN));
			registrosTot.put("MONTO_DESCUENTO", Double.toString(totalMontoDescMN));
			registrosTot.put("MONTO_VALUADO ", Double.toString(totalMontoValuadoMN));
			registrosTot.put("TOTAL_CREDITO ", Double.toString(totalCreditosMN) );
			registrosTot.put("TOTAL_MONTO_CREDITO ", Double.toString(totalMontoCreditoMN) );	
			registrosTotales.add(registrosTot);
		}	*/	
	}
	
	infoRegresar = "({\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registrosT\": " + registrosTotales.toString()+"})";
	
} else  if (informacion.equals("ConfirmacionClaves") ) {

	String cesionUser 	= (request.getParameter("cesionUser")==null)?"":request.getParameter("cesionUser");
	String cesionPassword 	= (request.getParameter("cesionPassword")==null)?"":request.getParameter("cesionPassword");
	boolean		bConfirma = false;
	Horario.validarHorario(4, strTipoUsuario, iNoEPO);
	if(!cesionUser.equals("") && !cesionPassword.equals("")) {
	try{
			seguridadEJB.validarUsuario(iNoUsuario, cesionUser , cesionPassword);
			bConfirma = true;
		}catch(Exception e){
			bConfirma = false;
		}		
	}		
		JSONObject 	jsonObj	= new JSONObject();
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("bConfirma", new Boolean(bConfirma));	
		infoRegresar = jsonObj.toString();	

} else  if (informacion.equals("GeneraAcuse") ) {


	String ses_ic_pyme = iNoCliente;
	//PARAMETROS QUE PROVIENEN DE LA PAGINA ACTUAL O ANTERIOR
	String modificados[]		= request.getParameterValues("modificado");
	String ic_documentos[]		= request.getParameterValues("ic_documento");
	String ic_monedas[]			= request.getParameterValues("ic_moneda_docto");
	String ic_monedas_linea[]	= request.getParameterValues("ic_moneda_linea");
	String montos[]				= request.getParameterValues("montos");
	String montos_valuados[]	= request.getParameterValues("monto_valuado");
	String montos_credito[]		= request.getParameterValues("monto_credito");
	String montos_tasa_int[]	= request.getParameterValues("monto_tasa_int");
	String montos_descuento[]	= request.getParameterValues("monto_descuento");
	String tipos_cambio[]		= request.getParameterValues("tipo_cambio");
	String plazos_credito[]		= request.getParameterValues("plazo");
	String fechas_vto[]			= request.getParameterValues("fecha_vto");
	String referencias_tasa[]	= request.getParameterValues("referencia_tasa");
	String valores_tasa[]		= request.getParameterValues("valor_tasa");
	String valores_tasa_puntos[]= request.getParameterValues("valor_tasa_puntos");
	String ic_tasa[]			= request.getParameterValues("ic_tasa");
	String cg_rel_mat[]			= request.getParameterValues("cg_rel_mat");
	String fn_puntos[]			= request.getParameterValues("fn_puntos");
	String tipo_tasa[]			= request.getParameterValues("tipo_tasa");
	String puntos_pref[]		= request.getParameterValues("puntos_pref");
	String fg_limite_pyme		= request.getParameter("hidLimPyme");
	String fg_limite_pyme_acum	= request.getParameter("hidLimUtil");
   String fn_montoValuado[]		= request.getParameterValues("fn_montoValuado");

	
	//VARIABLES DE USO LOCAL
	int		totalDoctosMN		= 0;
	int		totalDoctosUSD		= 0;
	double totalMontoMN			= 0;
	double totalMontoUSD		= 0;
	double totalMontoDescMN		= 0;
	double totalMontoDescUSD	= 0;
	double totalInteresMN		= 0;
	double totalInteresUSD		= 0;
	double totalMontoCreditoMN	= 0;
	double totalMontoCreditoUSD	= 0;
	double totalMontoCreditoConv= 0;
	int	 totalDoctosConv		= 0;
	double totalMontosConv		= 0;
	double totalMontoDescConv	= 0;
	double totalInteresConv		= 0;
	double totalConvPesos		= 0;
	String in					= "";
	int	i = 0, j = 0;
	String	icMoneda 			= "";
	// DE DESPLIEGUE
	String nombreEpo			=	"";
	String igNumeroDocto		=	"";
	String ccAcuse 				=	"";
	String dfFechaEmision		=	"";
	String dfFechaVencimiento	=	"";
	String dfFechaPublicacion 	= 	"";
	String igPlazoDocto			=	"";
	String moneda 				=	"";
	double fnMonto 				= 	0;
	String cgTipoConv			=	"";
	String tipoCambio			=   "";
	double montoValuado 		=	0;
	String igPlazoDescuento		= 	"";
	String fnPorcDescuento		= 	"";
	double montoDescuento 		=	0;
	String modoPlazo			= 	"";
	String estatus 				=	"";
	String cambios				= 	"";
	String numeroCredito		= 	"";
	String nombreIf				= 	"";
	String tipoLinea			=	"";
	String fechaOperacion		=	"";
	double montoCredito			=	0;
	String referencia			=	"";
	String plazoCredito			=	"";
	String fechaVencCredito		=	"";
	double tasaInteres			= 	0;
	double valorTasaInt			=	0;
	double montoTasaInt			=	0;
	double montoCapitalInt		= 	0;
	double fnPuntos				= 	0;
	String relMat				=	"";
	String tipoCobroInt			=	"";
	String  tipoPiso			=	"";
	String icTipoCobroInt		=	"";
	String icTasa				=	"";
	String icTipoFinanciamiento	=	"";
	String monedaLinea			=	"";
	String nombreMLinea			=	"";
	String seguridad			=	"";

	if("PYME".equals(strTipoUsuario))	
		Horario.validarHorario(4, strTipoUsuario, iNoEPO);
		if("NAFIN".equals(strTipoUsuario))
			Horario.validarHorario(4,"PYME",ic_epo);			
			for(i=0;i<ic_documentos.length;i++){				
				if("1".equals(ic_monedas[i])){					
					totalDoctosMN ++;
					totalMontoMN		+= Double.parseDouble(montos[i]);
					totalMontoDescMN	+= Double.parseDouble(montos_descuento[i]);
					totalInteresMN		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);				
					totalMontoCreditoMN	+= Double.parseDouble(montos_credito[i]);
				}				
				if("54".equals(ic_monedas[i])&&"54".equals(ic_monedas_linea[i])){				
					totalDoctosUSD ++;
					totalMontoUSD		+= Double.parseDouble(montos[i]);
					totalMontoDescUSD	+= Double.parseDouble(montos_descuento[i]);
					totalInteresUSD		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);
					totalMontoCreditoUSD+= Double.parseDouble(montos_credito[i]);
				}
				if("54".equals(ic_monedas[i])&&"1".equals(ic_monedas_linea[i])){
					totalDoctosConv ++;
					totalMontosConv			+= Double.parseDouble(montos[i]);
					totalMontoDescConv 		+= Double.parseDouble(montos_descuento[i]);
					totalInteresConv		+= Double.parseDouble("".equals(montos_tasa_int[i])?"0":montos_tasa_int[i]);
					totalConvPesos			+= Double.parseDouble(montos_descuento[i]);
					totalMontoCreditoConv	+= Double.parseDouble(montos_credito[i]);
				}				
				if(!"".equals(in))
					in += ",";
				in += ic_documentos[i];			
		} //for
		String acuse ="";
	
		JSONObject 	jsonObj	= new JSONObject();
		jsonObj.put("success", new Boolean(true));
	
		try{
			
			 acuse = aceptPyme.confirmaPedido(
						Double.toString (totalMontoMN), 	Double.toString (totalInteresMN),
						Double.toString (totalMontoCreditoMN), 	Double.toString (totalMontoUSD),
						Double.toString (totalInteresUSD), 	Double.toString (totalMontoCreditoUSD),				
						iNoUsuario, 	"", 
						ic_documentos,		ic_tasa, 
						cg_rel_mat,		fn_puntos,
						valores_tasa_puntos, 	montos_tasa_int,
						montos_credito,  plazos_credito,
						fechas_vto, 		tipo_tasa, 		puntos_pref, fn_montoValuado );

			jsonObj.put("acuse",acuse);	 
			jsonObj.put("fecCarga",fechaHoy);	
			jsonObj.put("horaCarga",horaCarga);	
			jsonObj.put("captUser",iNoUsuario+" "+strNombreUsuario);	
	      jsonObj.put("mensaje","");	
			
		} catch(Exception e){
			jsonObj.put("mensaje",e.toString());	 
		}
	infoRegresar = jsonObj.toString();	

}
%>
<%=infoRegresar%>

