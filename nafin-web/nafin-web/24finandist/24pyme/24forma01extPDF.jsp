<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*, 
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String clave_pyme = (String)request.getSession().getAttribute("iNoCliente");
String ic_epo 		= (request.getParameter("ic_epo")==null)?iNoEPO:request.getParameter("ic_epo");
String ic_moneda 	= (request.getParameter("ic_moneda")==null)?iNoEPO:request.getParameter("ic_moneda");
String mto_descuento 	= (request.getParameter("mto_descuento")==null)?iNoEPO:request.getParameter("mto_descuento");
String cc_acuse 	= (request.getParameter("cc_acuse")==null)?iNoEPO:request.getParameter("cc_acuse");
String fn_monto_de 	= (request.getParameter("fn_monto_de")==null)?iNoEPO:request.getParameter("fn_monto_de");
String fn_monto_a 	= (request.getParameter("fn_monto_a")==null)?iNoEPO:request.getParameter("fn_monto_a");
String df_fecha_emision_de 	= (request.getParameter("df_fecha_emision_de")==null)?iNoEPO:request.getParameter("df_fecha_emision_de");
String df_fecha_emision_a 	= (request.getParameter("df_fecha_emision_a")==null)?iNoEPO:request.getParameter("df_fecha_emision_a");
String ig_numero_docto 	= (request.getParameter("ig_numero_docto")==null)?iNoEPO:request.getParameter("ig_numero_docto");
String df_fecha_venc_de 	= (request.getParameter("df_fecha_venc_de")==null)?iNoEPO:request.getParameter("df_fecha_venc_de");
String df_fecha_venc_a 	= (request.getParameter("df_fecha_venc_a")==null)?iNoEPO:request.getParameter("df_fecha_venc_a");
String fecha_publicacion_de 	= (request.getParameter("fecha_publicacion_de")==null)?iNoEPO:request.getParameter("fecha_publicacion_de");
String fecha_publicacion_a 	= (request.getParameter("fecha_publicacion_a")==null)?iNoEPO:request.getParameter("fecha_publicacion_a");
String doctos_cambio 	= (request.getParameter("doctos_cambio")==null)?iNoEPO:request.getParameter("doctos_cambio");
String modo_plazo 	= (request.getParameter("modo_plazo")==null)?iNoEPO:request.getParameter("modo_plazo");
String nePyme 		= (String)session.getAttribute("strNePymeAsigna");
String nombrePyme	= (String)session.getAttribute("strNombrePymeAsigna");

String contentType = "text/html;charset=ISO-8859-1";
String 		nombreArchivo 	= null;	
CreaArchivo archivo  = new CreaArchivo();
JSONObject jsonObj = new JSONObject();		
String infoRegresar = "";
AccesoDB con = new AccesoDB();
try{
	con.conexionDB();
//INSTANCIACION DE EJB'S
	AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);	
	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);		
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);		
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);		
	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

	archivo 			    = new CreaArchivo();
	nombreArchivo 	  = archivo.nombreArchivo()+".pdf";				
	ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
	//encabezado de Archivo PDF
	String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
	String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
	String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
	String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
    
    String operaContrato = BeanParametros.obtieneOperaSinCesion(iNoEPO); //PARAMETRO NUEVO JAGE 14012017
		
	pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String) application.getAttribute("strDirectorioPublicacion"), "");
	
	Vector	vecFilas			= null;
	Vector	vecFilas1			= null;
	Vector	vecFilas2			= null;
	Vector	vecFilasPN			= null;
	Vector	vecColumnas			= null;
	Combos	comboLinea			= null;

	String	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	diasInhabiles = aceptPyme.getDiasInhabiles(clave_pyme,"");
	diasInhabiles = aceptPyme.getDiasInhabiles(clave_pyme,"");

	String limitePyme = "";
	String limiteUtiliPyme = "";
	String mensaje_terminacion = "";
	if(ic_epo!=null&&!"".equals(ic_epo)) {
		List lDatos = BeanParametros.getLimitePyme(ic_epo, clave_pyme, "");
		if(lDatos.size()>0) {
			lDatos 			= (ArrayList)lDatos.get(0);
			limitePyme 		= lDatos.get(2).toString();
			limiteUtiliPyme = lDatos.get(3).toString();
			mensaje_terminacion = lDatos.get(4).toString();
		}
	}
	//tipo de credito
	String tipoCredito = BeanParametros.obtieneTipoCredito (iNoEPO); 
	
	//monitor lineas de credito por Epo
	String  ic_linea = "",  moneda_linea = "", saldo_inicial = "", montoSeleccionado ="", numDoctos = "",
	saldoDisponible = "",  hidSaldoDisponible  = "";			
	vecFilas = cargaDocto.monitorLineas(iNoEPO);
	for(int i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector)vecFilas.get(i);	
		ic_linea = (String)vecColumnas.get(0);
		moneda_linea = (String)vecColumnas.get(4);
		saldo_inicial = Comunes.formatoDecimal(vecColumnas.get(2).toString(),2,false);
		montoSeleccionado ="0.00";
		numDoctos = "0";
		saldoDisponible = Comunes.formatoDecimal(vecColumnas.get(3).toString(),2,false);
		hidSaldoDisponible  = Comunes.formatoDecimal(vecColumnas.get(3).toString(),2,false);
	}
	int hidNumLineas = vecFilas.size();
	
/****** if("C".equals(accion)){*******/
	String ic_if ="";
	vecFilas2 = BeanTasas.esquemaParticular(ic_epo,4,"",iNoCliente); // obtiene las EPO e IF que tienen una LC vigente
	for (int j=0; j<vecFilas2.size(); j++) {
		Vector lovDatosTas = (Vector)vecFilas2.get(j);
		ic_if = lovDatosTas.get(0).toString();
		vecFilas1 = BeanTasas.ovgetTasasPreferNego("4","",ic_if,ic_epo,"",iNoCliente,"", "");
		
		//Reemplazamos por los valores de las tasas preferenciales
		vecFilasPN = BeanTasas.ovgetTasasPreferNego("4","P",ic_if,ic_epo,"",iNoCliente,"", "");
		for(int i=0;i<vecFilasPN.size();i++){
			String auxIcTasaBase	= "";
			String auxIcTasaPN		= "";
			vecColumnas = (Vector)vecFilasPN.get(i);
			auxIcTasaPN = (String)vecColumnas.get(2);
			for(int k=0;k<vecFilas1.size();k++){
				Vector vecColumnas2 = (Vector)vecFilas1.get(k);
				auxIcTasaBase = (String)vecColumnas2.get(2);
				if(auxIcTasaBase.equals(auxIcTasaPN)){
					vecFilas1.set(k,vecColumnas);
				}
			}
		}
	
		//Reemplazamos por los valores de las tasas negociadas
		vecFilasPN = BeanTasas.ovgetTasasPreferNego("4","N",ic_if,ic_epo,"",iNoCliente,"", "");
		for(int i=0;i<vecFilasPN.size();i++){
			String auxIcTasaBase	= "";
			String auxIcTasaPN		= "";
			vecColumnas = (Vector)vecFilasPN.get(i);
			auxIcTasaPN = (String)vecColumnas.get(2);
			for(int k=0;k<vecFilas1.size();k++){
				Vector vecColumnas2 = (Vector)vecFilas1.get(k);
				auxIcTasaBase = (String)vecColumnas2.get(2);
				if(auxIcTasaBase.equals(auxIcTasaPN)){
					vecFilas1.set(k,vecColumnas);
				}
			}
		}	
		int numTasas  =0;
		for(int i=0;i<vecFilas1.size();i++){
			vecColumnas = (Vector)vecFilas1.get(i);	
			String auxIcIf = ic_if;
			String auxReferenciaTasa =(String)vecColumnas.get(1)+" "+(String)vecColumnas.get(5)+" "+(String)vecColumnas.get(6);
			String auxIcTasa =(String)vecColumnas.get(2);
			String auxCgRelMat = (String)vecColumnas.get(5);
			String auxFnPuntos =(String)vecColumnas.get(6);
			String auxValorTasa = (String)vecColumnas.get(4);
			String auxIcMoneda =(String)vecColumnas.get(13);
			String auxPlazoDias =(String)vecColumnas.get(14);
			String auxValorTasaPuntos  =(String)vecColumnas.get(11);
			String  auxTipoTasa  = (String)vecColumnas.get(16);
			String auxPuntosPref =  (String)vecColumnas.get(15);
			numTasas++;
		}			
	}

	//realiza la consulta 
	vecFilas = aceptPyme.consultaDoctos(clave_pyme,ic_epo,ic_moneda,ig_numero_docto,fn_monto_de,fn_monto_a, cc_acuse,mto_descuento,df_fecha_emision_de,df_fecha_emision_a,doctos_cambio,df_fecha_venc_de,df_fecha_venc_a,modo_plazo,fecha_publicacion_de,fecha_publicacion_a,null);
	
	vecFilas1 = (Vector)vecFilas.get(0); // esta es para cuando cg_responsable_interes= "E"
	vecFilas2 = (Vector)vecFilas.get(1);// esta es para cuando cg_responsable_interes!= "E"
		
	double valorTasaInt	=	0,  montoTasaInt	=	0,	 fnPuntos	= 0	,montoCredito = 0;
	int totalDoctosMN		=	0, totalDoctosUSD	=0,	totalCreditosMN	=	0, 	totalCreditosUSD	=	0;
	double totalMontoMN	=	0, totalMontoUSD		=	0, totalMontoDescMN		=	0, totalMontoDescUSD	=	0,
	totalInteresMN		  =	0, totalInteresUSD		=	0, totalMontoValuadoMN	= 	0, totalMontoValuadoUSD	= 	0,
	totalMontoCreditoMN	=	0, totalMontoCreditoUSD	=	0, montoValuadoLinea	= 	0,
	fnMonto =0,	 montoDescuento =0,  montoValuado  = 0,	totalMontoMNDesA =0,  totalMontoUSDDesA =0;
	
	String nombreEpo ="", igNumeroDocto	="", ccAcuse  ="", dfFechaEmision ="", dfFechaVencimiento ="",
	dfFechaPublicacion 	="", igPlazoDocto ="", moneda  ="",  cgTipoConv	 ="", tipoCambio ="",
	igPlazoDescuento ="", fnPorcDescuento	="",  modoPlazo	 ="", estatus ="", cambios ="",
	numeroCredito ="", nombreIf	="",  tipoLinea	 ="",  fechaOperacion	="",  plazoCredito	="",
	fechaVencCredito ="", relMat ="", tipoCobroInt ="",  referencia	="", tipoPiso ="", icTipoCobroInt ="",
	icMoneda ="",  icTipoFinanciamiento="",  monedaLinea	="", dias_minimo ="",	dias_maximo ="",  
	icLineaCreditoDM	="", categoria="", nombreMLinea ="";


	if(vecFilas1.size()>0){
	
		totalDoctosMN = 0;
		totalMontoMN = 0;
		totalMontoDescMN = 0;
		totalDoctosUSD= 0;
		totalMontoUSD = 0;
		totalMontoDescUSD = 0;
	
		pdfDoc.setTable(18, 100);
		pdfDoc.setCell("Epo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Num. docto. inicial","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Num. acuse ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de emisión","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha vencimiento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de publicación","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo docto","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Categoria","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo para descuento en dias","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" % de descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto de descuento","celda01",ComunesPDF.CENTER);		
		pdfDoc.setCell("Tipo conv","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo cambio","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto valuado en pesos","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
		
		for(int i=0;i<vecFilas1.size();i++){
			vecColumnas = (Vector)vecFilas1.get(i);
			nombreEpo			=	(String)vecColumnas.get(0);
			igNumeroDocto		=	(String)vecColumnas.get(1);
			ccAcuse 			=	(String)vecColumnas.get(2);
			dfFechaEmision		=	(String)vecColumnas.get(3);
			dfFechaVencimiento	=	(String)vecColumnas.get(4);
			dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
			igPlazoDocto		=	(String)vecColumnas.get(6);
			moneda 				=	(String)vecColumnas.get(7);
			fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			cgTipoConv			=	(String)vecColumnas.get(9);
			tipoCambio			=   (String)vecColumnas.get(10);
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			igPlazoDescuento	= 	(String)vecColumnas.get(12);
			fnPorcDescuento		= 	(String)vecColumnas.get(13);
			modoPlazo			= 	(String)vecColumnas.get(14);
			estatus 			=	(String)vecColumnas.get(15);
			cambios				= 	(String)vecColumnas.get(16);
			//creditos por concepto de intereses
			numeroCredito		= 	(String)vecColumnas.get(17);
			nombreIf			= 	(String)vecColumnas.get(18);
			tipoLinea			=	(String)vecColumnas.get(19);
			fechaOperacion		=	(String)vecColumnas.get(20);
			plazoCredito		=	(String)vecColumnas.get(22);
			fechaVencCredito	=	(String)vecColumnas.get(23);
			relMat				= 	(String)vecColumnas.get(25);
			tipoCobroInt		=	(String)vecColumnas.get(27);
			referencia			=	(String)vecColumnas.get(29);
			tipoPiso			= 	(String)vecColumnas.get(30);
			icTipoCobroInt		= 	(String)vecColumnas.get(31);
			icMoneda			= 	(String)vecColumnas.get(32);
			icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			monedaLinea			= 	(String)vecColumnas.get(35);
			dias_minimo			= 	(String)vecColumnas.get(36);
			dias_maximo			= 	(String)vecColumnas.get(37);
		  ic_if				= 	(String)vecColumnas.get(38);
		  icLineaCreditoDM	= 	(String)vecColumnas.get(39);
			categoria			= 	(String)vecColumnas.get(41);	
			
			String   descuentoAforo1 = 	(String)vecColumnas.get(47)==null?"0":(String)vecColumnas.get(47); //F05-2014
			double  descuentoAforo= Double.parseDouble(vecColumnas.get(47).toString());  //F05-2014
			if (descuentoAforo1.equals("0"))  {  	descuentoAforo = 100;   	}	//F05-2014
			double fnMontoDescuento = 0;
			
			if("1".equals(icTipoFinanciamiento) || "4".equals(icTipoFinanciamiento)){
				fechaVencCredito = dfFechaVencimiento;
			}
			if("+".equals(relMat))
				valorTasaInt += fnPuntos;
			else if("-".equals(relMat))
				valorTasaInt -= fnPuntos;
			else if("*".equals(relMat))
				valorTasaInt *= fnPuntos;
			else if("/".equals(relMat))
				valorTasaInt /= fnPuntos;

			montoValuadoLinea = fnMonto;
				
			plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;

			if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento)||"4".equals(icTipoFinanciamiento))
				montoCredito = fnMonto-montoDescuento;
			else if("2".equals(icTipoFinanciamiento))
				montoCredito = fnMonto;
				
			//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
				if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
					montoCredito = montoValuado;
				else if("2".equals(icTipoFinanciamiento))
					montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
			}
			
			if("1".equals(tipoPiso)){
				montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
			}else{
				montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
			}
	
			if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
				fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
			} else if("2".equals(icTipoFinanciamiento)  )  {			
				fnMontoDescuento	= fnMonto *(descuentoAforo/100);   //F05-2014
			}
				 
				
		
			pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(ccAcuse,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaEmision,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaVencimiento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaPublicacion,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igPlazoDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMonto,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(categoria,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igPlazoDescuento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fnPorcDescuento+"%","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2),"formas",ComunesPDF.RIGHT);			
			pdfDoc.setCell(cgTipoConv,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(tipoCambio,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMontoDescuento,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
	
		
			if("1".equals(icMoneda)){
				totalDoctosMN++;
				totalMontoMN += fnMonto;
				totalMontoDescMN += montoDescuento;
				totalMontoValuadoMN += montoValuado;
				totalMontoMNDesA +=fnMontoDescuento; //F05-2014
			}else{
				totalDoctosUSD++;
				totalMontoUSD += fnMonto;
				totalMontoDescUSD += montoDescuento;
				totalMontoValuadoUSD += montoValuado;
				totalMontoUSDDesA +=fnMontoDescuento;  //F05-2014
			}
			
		}//for
	
		if(totalDoctosMN>0){ 	
			pdfDoc.setCell("Total M.N.","formas",ComunesPDF.CENTER );
			pdfDoc.setCell(String.valueOf(totalDoctosMN)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,6);			
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoMN),2)  ,"formas",ComunesPDF.RIGHT);			
			pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);		
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoDescMN),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoMNDesA),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);			
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoMNDesA),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
		}
		if(totalDoctosUSD>0){ 		
			pdfDoc.setCell("Total USD.","formas",ComunesPDF.CENTER );
			pdfDoc.setCell(String.valueOf(totalDoctosUSD)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,6);		
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoUSD),2)  ,"formas",ComunesPDF.RIGHT);			
			pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);		
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoDescUSD),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoUSDDesA),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);		
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoUSDDesA),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
		}
		pdfDoc.addTable();		
	}

	// SEGUNDA LISTA DE DATOS 
	if(vecFilas2.size()>0){
		totalDoctosMN = 0;
		totalMontoMN = 0;
		totalMontoDescMN = 0;
		totalDoctosUSD= 0;
		totalMontoUSD = 0;
		totalMontoDescUSD = 0;
			
		pdfDoc.setTable(17, 100);
		pdfDoc.setCell("Datos Documento Inicial","celda01rep",ComunesPDF.CENTER,17);
		pdfDoc.setCell("Epo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Num. docto. inicial","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Num. acuse ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de emisión","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha vencimiento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de publicación","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo docto","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Categoria","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo para descuento en dias","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" % de descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto de descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo conv","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo cambio","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto valuado en pesos","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);
		//DATOS DEL DOCUMENTO fINAL 
		pdfDoc.setCell("Datos Documento Final","celda01rep",ComunesPDF.CENTER,17);		
		pdfDoc.setCell("Núm. de documento final","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de operación","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Referencia tasa de interés","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Valor tasa de interés ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto de Intereses ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto Total de Capital e Interés ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("","celda01",ComunesPDF.CENTER,6);
						
		
		for(int i=0;i<vecFilas2.size();i++){
		
			vecColumnas = (Vector)vecFilas2.get(i);
			nombreEpo			=	(String)vecColumnas.get(0);
			igNumeroDocto		=	(String)vecColumnas.get(1);
			ccAcuse 			=	(String)vecColumnas.get(2);
			dfFechaEmision		=	(String)vecColumnas.get(3);
			dfFechaVencimiento	=	(String)vecColumnas.get(4);
			dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
			igPlazoDocto		=	(String)vecColumnas.get(6);
			moneda 				=	(String)vecColumnas.get(7);
			fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			cgTipoConv			=	(String)vecColumnas.get(9);
			tipoCambio			=   (String)vecColumnas.get(10);
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			igPlazoDescuento	= 	(String)vecColumnas.get(12);
			fnPorcDescuento		= 	(String)vecColumnas.get(13);
			modoPlazo			= 	(String)vecColumnas.get(14);
			estatus 			=	(String)vecColumnas.get(15);
			cambios				= 	(String)vecColumnas.get(16);
			//creditos por concepto de intereses
			numeroCredito		= 	(String)vecColumnas.get(17);
			nombreIf			= 	(String)vecColumnas.get(18);
			tipoLinea			=	(String)vecColumnas.get(19);
			fechaOperacion		=	(String)vecColumnas.get(20);
			plazoCredito		=	(String)vecColumnas.get(22);
			fechaVencCredito	=	(String)vecColumnas.get(23);
			relMat				= 	(String)vecColumnas.get(25);
			tipoCobroInt		=	(String)vecColumnas.get(27);
			referencia			=	(String)vecColumnas.get(29);
			tipoPiso			= 	(String)vecColumnas.get(30);
			icTipoCobroInt		= 	(String)vecColumnas.get(31);
			icMoneda			= 	(String)vecColumnas.get(32);
			icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			monedaLinea			= 	(String)vecColumnas.get(35);
			dias_minimo			= 	(String)vecColumnas.get(36);
			dias_maximo			= 	(String)vecColumnas.get(37);
			ic_if				= 	(String)vecColumnas.get(38);
			icLineaCreditoDM	= 	(String)vecColumnas.get(39);
			nombreMLinea		= 	(String)vecColumnas.get(40);
			categoria			= 	(String)vecColumnas.get(41);
			String diasMaxFV	=  	vecColumnas.get(42).toString();
			String fechaVencMax	=  	vecColumnas.get(43).toString();
			String   descuentoAforo1 = 	(String)vecColumnas.get(47)==null?"0":(String)vecColumnas.get(47); //F05-2014
			double  descuentoAforo= Double.parseDouble(vecColumnas.get(47).toString());  //F05-2014
			if (descuentoAforo1.equals("0"))  {  	descuentoAforo = 100;   	}	//F05-2014
			double fnMontoDescuento =0;
			
	
			if("1".equals(icTipoFinanciamiento) || "4".equals(icTipoFinanciamiento)){
				fechaVencCredito = dfFechaVencimiento;
			}
			if("+".equals(relMat))
				valorTasaInt += fnPuntos;
			else if("-".equals(relMat))
				valorTasaInt -= fnPuntos;
			else if("*".equals(relMat))
				valorTasaInt *= fnPuntos;
			else if("/".equals(relMat))
				valorTasaInt /= fnPuntos;
	
			montoValuadoLinea = fnMonto;
					
			plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
	
			if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento)||"4".equals(icTipoFinanciamiento))
				montoCredito = fnMonto-montoDescuento;
			else if("2".equals(icTipoFinanciamiento))
				montoCredito = fnMonto;
					
			//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
			if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
				montoCredito = montoValuado;
			else if("2".equals(icTipoFinanciamiento))
				montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
			}
			
			if("1".equals(tipoPiso)){
				montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
			}else{
				montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
			}
			
			if("1".equals(icTipoFinanciamiento) ||  "3".equals(icTipoFinanciamiento)  )  {
				fnMontoDescuento	= (fnMonto-montoDescuento) *(descuentoAforo/100);   //F05-2014					
			} else if("2".equals(icTipoFinanciamiento)  )  {			
				fnMontoDescuento	= fnMonto *(descuentoAforo/100);   //F05-2014
			}
				 
				
			
			pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(ccAcuse,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaEmision,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaVencimiento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaPublicacion,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igPlazoDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMonto,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(categoria,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igPlazoDescuento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fnPorcDescuento+"%","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(cgTipoConv,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(tipoCambio,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMontoDescuento,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);						
			pdfDoc.setCell(numeroCredito,"formas",ComunesPDF.CENTER);		
			pdfDoc.setCell(nombreMLinea,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoCredito,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
	
			
			if("1".equals(icMoneda)){
				totalDoctosMN++;
				totalMontoMN += fnMonto;
				totalMontoDescMN += montoDescuento;
				totalInteresMN += valorTasaInt;
				totalMontoMNDesA +=fnMontoDescuento; //F05-2014
			}else{
				totalDoctosUSD++;
				totalMontoUSD += fnMonto;
				totalMontoDescUSD += montoDescuento;
				totalInteresUSD += valorTasaInt;
				totalMontoUSDDesA +=fnMontoDescuento;  //F05-2014
				if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
					totalMontoValuadoUSD += montoValuado;
				}
			}
			if("1".equals(monedaLinea)){
				totalCreditosMN++;
				totalMontoCreditoMN += montoCredito;
			}else{
				totalCreditosUSD++;
				totalMontoCreditoUSD += montoCredito;
			}
			
		}	//for
	
		int rowspanCalcula = 0;
			int colspantot = 6;
		if(totalDoctosMN>0||totalCreditosMN>0) 
			rowspanCalcula ++;
		if(totalDoctosUSD>0||totalCreditosUSD>0)
			rowspanCalcula ++;
		if("NAFIN".equals(strTipoUsuario))
			colspantot++;
	
		if(totalDoctosMN>0||totalCreditosMN>0){ 
			
			pdfDoc.setCell("Total M.N.","formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(String.valueOf(totalDoctosMN)  ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT,5);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT,3);
			
			if(!"2".equals(icTipoFinanciamiento)){
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoDescMN,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoMNDesA,2),"formas",ComunesPDF.RIGHT);//F05-2014			
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT,2);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoMNDesA,2),"formas",ComunesPDF.RIGHT);
			}else{
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT,2);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			}
		//	pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Total Doctos. Finales M.N.","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(String.valueOf(totalCreditosMN),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);	
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);	
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);		
				
		}
				
		//if(totalDoctosUSD>0){ 
			
			pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(String.valueOf(totalDoctosUSD)  ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT,5);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.RIGHT);			
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT,3);
			
			if(!"2".equals(icTipoFinanciamiento)){
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoDescUSD,1),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoUSDDesA,2),"formas",ComunesPDF.RIGHT); //F05-2014
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT,2);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoUSDDesA,2),"formas",ComunesPDF.RIGHT);
			}else{
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT,2);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);		
			}
			
			pdfDoc.setCell("Total Doctos. Finales USD","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(String.valueOf(totalCreditosUSD),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(""  ,"formas",ComunesPDF.RIGHT);			
			
		//}
		
		pdfDoc.addTable();	
	}	
	
	String mensaje ="Al transmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, manifiesta su aceptación para que sea(n) sustituido(s) el(los) DOCUMENTO(S) INICIAL(ES) que ha seleccionado por el(los) DOCUMENTO(S) FINAL(ES) y estos últimos sean descontados a favor de la EMPRESA DE PRIMER ORDEN. Dicho(s) DOCUMENTO(S) FINALE(S) contiene(n) los Derechos de Cobro a su cargo por lo que Usted acepta pagar el 100% de su valor al INTERMEDIARIO FINANCIERO en su fecha de vencimiento. Manifiesta también su obligación de cubrir al INTERMEDIARIO FINANCIERO, los intereses que se detallan en esta pantalla, en la fecha de su operación, por haber aceptado la sustitución. ";
    if(operaContrato.equals("S")){
        mensaje = "En este acto manifiesto mi aceptación para que sea(n) financiado(s) el(los) DOCUMENTO(S) que he seleccionado, mismo(s) que contiene(n) cuentas pendientes de pago a mi cargo "+
        " y a favor de la EMPRESA DE PRIMER ORDEN por lo que en esta misma fecha me obligo a cubrir al INTERMEDIARIO FINANCIERO los intereses que se detallan en esta pantalla. "+
        "Manifiesto también mi obligación y aceptación de pagar el 100% del valor de(los) DOCUMENTO(S) en la fecha de vencimiento al INTERMEDIARIO FINANCIERO.";            
    }
    mensaje = mensaje + "\n\nPor otra parte, a partir del 17 de octubre de 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que sí ha emitido o emitirá a su CLIENTE o DISTRIBUIDOR el CFDI por la operación comercial que le dio origen a esta transacción, según sea el caso y conforme establezcan las disposiciones fiscales vigentes.";
	pdfDoc.addText(" ","formas",ComunesPDF.JUSTIFIED);	
	pdfDoc.addText(mensaje,"formas",ComunesPDF.JUSTIFIED);	
	
			
	pdfDoc.endDocument();
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	
	System.out.println("infoRegresar "+infoRegresar);
	
}catch(Exception e){
	throw new AppException("Error al generar el archivo", e);
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
}
%>

<%=jsonObj%>