<%@ page contentType="application/json;charset=UTF-8"
import="
  java.text.*, 
  java.math.*, 
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.anticipos.*,
	com.netro.pdf.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>
<%System.out.println("13consulta03extPDF.jsp (E)");%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_epo = request.getParameter("no_epo") == null?"":(String)request.getParameter("no_epo");
String infoRegresar = "";
AccesoDB con = new AccesoDB();
CreaArchivo archivo = new CreaArchivo();
JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
String ses_ic_pyme = iNoCliente;
int liRegistros=0;
int Registros1 = 0;
int Registros2 = 0;
int Registros3 = 0;
int index=0;
Vector vecFilas		= null;
Vector vecColumnas	= null;
Vector vecFilas2	= null;
Vector vecColumnas2 = null;
Vector lovTasas		= null;
Vector vector		= null;
String total = "",tipoCredito = "",pagoInt = "",encabezado="";
String ifRazonSoc = "",ic_if="",epoRazonSoc = "";
String aux="";
boolean DM=true,CCC=true;
try{
	con.conexionDB();
	
	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);
	
	
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	

	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
	
	vecFilas = BeanTasas.disOpera("",4,iNoCliente);
	for (int i = 0;i<vecFilas.size();i++) {
		vecColumnas = (Vector)vecFilas.get(i);
		total = (String)vecColumnas.get(0);
		tipoCredito = (String)vecColumnas.get(1);
		//para Descuento Mercantil
		if (Integer.parseInt(total) > 0 && "D".equals(tipoCredito)){
			pagoInt = BeanTasas.paramPagoInteres(ic_epo,4);
			if ("D".equals(pagoInt)){	//si la pyme es la responsable del pago de interes
				vecFilas2 = BeanTasas.esquemaParticular(ic_epo,4,"",iNoCliente); // obtiene las EPO e IF que tienen una LC vigente
				for (int j=0; j<vecFilas2.size(); j++) {
					DM = false;
					Vector lovDatosTas = (Vector)vecFilas2.get(j);
					ic_if = lovDatosTas.get(0).toString();
					ifRazonSoc = lovDatosTas.get(1).toString();
					epoRazonSoc = lovDatosTas.get(2).toString();	
					encabezado = "Esquema de tasas "+epoRazonSoc+" - "+ifRazonSoc;
					
					lovTasas = BeanTasas.ovgetTasasxEpo(4,ic_epo,ic_if);						
					pdfDoc.setTable(8, 100);
					pdfDoc.setCell(encabezado,"celda01",ComunesPDF.LEFT,8);
					if(lovTasas.size()>0){
						
						pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo Tasa","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Valor","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Rel. mat","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Puntos adicionales","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Tasa a aplicar","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha última actualización","celda01",ComunesPDF.CENTER);
											
						
						for (int y=0; y<lovTasas.size(); y++) {
							
							Vector lovDatosTasa = (Vector)lovTasas.get(y);
							pdfDoc.setCell(lovDatosTasa.get(0).toString(),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(1).toString(),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(3).toString(),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(4).toString(),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(5).toString(),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(6).toString(),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(7).toString(),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(8).toString(),"formas",ComunesPDF.CENTER);
						}	
						pdfDoc.addTable();
					}
									
				}//for (int j=0; j<vecFilas2.size(); j++)
			}
		}
		//-----------------------------para Credito Cuenta Corriente
		if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito)){
			
			//para Obtener los Intermediarios Financieros
			vecFilas2 = BeanTasas.esquemaParticular(ic_epo,4,"",iNoCliente,"C"); // obtiene las EPO e IF que tienen una LC vigente
			
			for (int j=0; j<vecFilas2.size(); j++) {
				DM = false;
				Vector lovDatosTas = (Vector)vecFilas2.get(j);
				ic_if = lovDatosTas.get(0).toString();
				ifRazonSoc = lovDatosTas.get(1).toString();
				epoRazonSoc = lovDatosTas.get(2).toString();					
				encabezado = "Esquema de tasas "+epoRazonSoc+" - "+ifRazonSoc;
				lovTasas = BeanTasas.ovgetTasasxPyme(4,ses_ic_pyme,ic_if,"1, 54");
				pdfDoc.setTable(8, 100);
				pdfDoc.setCell(encabezado,"celda01",ComunesPDF.LEFT,8);
							
				if(lovTasas.size()>0){
						
						pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo Tasa","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Valor","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Rel. mat","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Puntos adicionales","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Tasa a aplicar","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha última actualización","celda01",ComunesPDF.CENTER);
								
					for (int y=0; y<lovTasas.size(); y++) {
						
						Vector lovDatosTasa = (Vector)lovTasas.get(y);
						pdfDoc.setCell(lovDatosTasa.get(0).toString(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(lovDatosTasa.get(1).toString(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(lovDatosTasa.get(3).toString(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(lovDatosTasa.get(4).toString(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(lovDatosTasa.get(5).toString(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(lovDatosTasa.get(6).toString(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(lovDatosTasa.get(7).toString(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(lovDatosTasa.get(8).toString(),"formas",ComunesPDF.CENTER);
					}
					pdfDoc.addTable();
				}
			}//for (int j=0; j<vecFilas2.size(); j++)		
		}//if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito))
	}//for (int i = 0;i<vecFilas.size();i++) 
	
	pdfDoc.endDocument();
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);


}catch(Exception e){
	throw new AppException("Error al generar el archivo", e);
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
}
%>
<%=jsonObj%>

<%System.out.println("13consulta03extPDF.jsp (S)");%>
