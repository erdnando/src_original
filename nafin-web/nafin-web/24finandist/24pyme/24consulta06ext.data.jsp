<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoIFDistribuidores,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
///////
String fechaHoy		= "";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}
//parametros que provienen de la pagina actual
String ic_epo       = (request.getParameter("ic_epo")      ==null)?"":request.getParameter("ic_epo");
String ic_if        = (request.getParameter("ic_if")       ==null)?"":request.getParameter("ic_if");
String ic_moneda    = (request.getParameter("ic_moneda")   ==null)?"":request.getParameter("ic_moneda");
String fecha_inicio = (request.getParameter("fecha_inicio")==null)?"":request.getParameter("fecha_inicio");
String fecha_final  = (request.getParameter("fecha_final") ==null)?"":request.getParameter("fecha_final");
String cliente      = (request.getParameter("disCliente")  ==null)?"":request.getParameter("disCliente");
String banco        = (request.getParameter("disBanco")    ==null)?"":request.getParameter("disBanco");
String informacion  = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String fecha_ini    = (request.getParameter("fecha_ini1")  ==null)?"":request.getParameter("fecha_ini1");
String fecha_fin    = (request.getParameter("fecha_fin1")  ==null)?"":request.getParameter("fecha_fin1");
String infoRegresar = "";

if (informacion.equals("valoresBusqueda"))	{
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	String tipoCredito = cargaDocto.getTipoCredito(iNoEPO, iNoCliente);
	Hashtable datos = new Hashtable();
	datos = cargaDocto.getNombres_pyme_if(iNoCliente, ic_if);
	if (datos != null){
		cliente = ((datos.get("NOMBRE_PYME")==null)?"":datos.get("NOMBRE_PYME")).toString();
		banco = ((datos.get("NOMBRE_IF")==null)?"":datos.get("NOMBRE_IF")).toString();
	}

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("tipoCredito",tipoCredito);
	jsonObj.put("Cliente",cliente);
	jsonObj.put("Banco", banco);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoEPODist")) {

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClavePyme(iNoCliente);
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoIfDist")){

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveEpo(ic_epo);
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

} else if (informacion.equals("Consulta") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")){
	int start = 0;
	int limit = 0;
	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	EstadoAdeudosPyme2 tipoD = new EstadoAdeudosPyme2();
	tipoD.setClaveEpo(ic_epo);
	tipoD.setClaveIf(ic_if);
	tipoD.setClavePyme(iNoCliente);
	tipoD.setClaveMoneda(ic_moneda);
	//tipoD.setFechaInicial(fecha_inicio);
	//tipoD.setFechaFinal(fecha_final);
	tipoD.setFechaInicial(fecha_ini);
	tipoD.setFechaFinal(fecha_fin);
	tipoD.setCliente(cliente);
	tipoD.setBanco(banco);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(tipoD);
	Registros reg = new Registros();
	reg = queryHelper.doSearch();
	if (informacion.equals("Consulta")) { //Datos para la Consulta con Paginacion
		try {
			if (operacion.equals("Generar")) { //Nueva consulta
				reg = queryHelper.doSearch();
				infoRegresar =  "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData() + "}";
			}
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	} else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	} else if (informacion.equals("ArchivoPDF")) {
		JSONObject jsonObj = new JSONObject();
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();
	}

} else if (informacion.equals("Consulta_B") || informacion.equals("ArchivoCSV_B") || informacion.equals("ArchivoPDF_B")){
	int start = 0;
	int limit = 0;

	EstadoAdeudosPyme tipoC = new EstadoAdeudosPyme();
	tipoC.setIc_if(ic_if);
	tipoC.setIc_epo(ic_epo);
	tipoC.setIc_moneda(ic_moneda);
	//tipoC.setFecha_inicio(fecha_inicio);
	//tipoC.setFecha_final(fecha_final);
	tipoC.setFecha_inicio(fecha_ini);
	tipoC.setFecha_final(fecha_fin);
	tipoC.setUsuario(iNoCliente);
	tipoC.setCliente(cliente);
	tipoC.setBanco(banco);
	CQueryHelperRegExtJS queryHelper1 = new CQueryHelperRegExtJS(tipoC);

	if (informacion.equals("Consulta_B")) { //Datos para la Consulta con Paginacion
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper1.executePKQuery(request);
			}
			infoRegresar = queryHelper1.getJSONPageResultSet(request,start,limit);
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("ArchivoCSV_B")) {
		JSONObject jsonObj = new JSONObject();
		try {
			String nombreArchivo = queryHelper1.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo CSV", e);
		}
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("ArchivoPDF_B")) {
		JSONObject jsonObj = new JSONObject();
		try {
			String nombreArchivo = queryHelper1.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();
	}
}
%>
<%=infoRegresar%>