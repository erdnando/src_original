<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.sql.*, java.math.*,
	com.netro.distribuidores.*,com.netro.exception.*, javax.naming.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String fechaHoy		= "";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}
String informacion   			=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
String	cgTipoConversion		=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

if("".equals(cgTipoConversion)){
	cgTipoConversion = cargaDocto.getTipoConversion(iNoCliente);
}

	String clave="";
	String epo="";
	String numdoc="";
	String acuse="";
	String fecha_emision="";
	String df_fecha_venc="";
	String fecha_publicacion="";
	String plazodoc="";
	String moneda="";
	double monto=0;
	String conversion="";
	double tipcambio=0;
	double monto_valuado=0;
	String plazdesc="";
	String porcdesc="";
	double monto_con_desc=0;
	String modalidad_plazo="";
	String estatus="";
	String fechacambio="";
	String cambioestatus="";
	String cambiomotivo="";
	String bandeVentaCartera="";
	String categoria="";
	String rs_ic_mon="", porc_Aforo ="";
	
	try {
		if(informacion.equals("ArchivoXpaginaPDF") || informacion.equals("ArchivoTotalPDF")){
				String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
				int start = 0;
				int limit = 0;
				int nRow = 0;
				Registros reg = new Registros();
//Inicia creacion de archivo*-*-*-*-*-*-*-*-*-*-*-*-
				ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		
				pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);

				CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsNoOperPymeDist());
			
				if (informacion.equals("ArchivoXpaginaPDF"))	{	//-*-*-*-*-*-*-	Obtiene los registros por pagina del primer grid
					try {
						start = Integer.parseInt(request.getParameter("start"));
						limit = Integer.parseInt(request.getParameter("limit"));
					} catch(Exception e) {
						throw new AppException("Error en los parametros recibidos", e);
					}
				reg = queryHelper.getPageResultSet(request,start,limit);
			/***************************/	/* Generacion del archivo *//***************************/
				while (reg.next()) {
					if(nRow == 0){
						int numCols = (!"".equals(cgTipoConversion)?23:20);
		
						float widths[];
						if(!"".equals(cgTipoConversion)){
							widths = new float[]{5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,6.0f,6.5f,5.5f,5.0f,5.0f,5.0f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f};
						}else{
							widths = new float[]{6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,7.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f};
						}
		
						pdfDoc.setTable(numCols,100,widths);  
		
						pdfDoc.setCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setCell("EPO","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Emisión","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo Docto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Categoria","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("% de Descuento Aforo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto a Descontar","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo para descuento en días","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("%   de descuento","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto % de descuento","celda01rep",ComunesPDF.CENTER);
						if(!"".equals(cgTipoConversion)){
							pdfDoc.setCell("Tipo Conv.","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Tipo Cambio","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto Valuado","celda01rep",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("Modalidad de plazo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Cambio Estatus","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo Cambio Estatus","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Causa","celda01rep",ComunesPDF.CENTER);
					}
						clave					= reg.getString("CLAVE")==null?"":reg.getString("CLAVE");
						epo					= reg.getString("EPO")==null?"":reg.getString("EPO");
						numdoc				= reg.getString("NUMDOC")==null?"":reg.getString("NUMDOC");
						acuse 				= reg.getString("ACUSE")==null?"":reg.getString("ACUSE");
						fecha_emision		= reg.getString("FECHA_EMISION")==null?"":reg.getString("FECHA_EMISION");
						df_fecha_venc 		= reg.getString("DF_FECHA_VENC")==null?"":reg.getString("DF_FECHA_VENC");
						fecha_publicacion = reg.getString("FECHA_PUBLICACION")==null?"":reg.getString("FECHA_PUBLICACION");
						plazodoc 			= reg.getString("PLAZODOC")==null?"":reg.getString("PLAZODOC");
						moneda 				= reg.getString("MONEDA")==null?"":reg.getString("MONEDA");
						monto 				= Double.parseDouble(reg.getString("MONTO"));
						conversion			= reg.getString("CONVERSION")==null?"":reg.getString("CONVERSION");
						tipcambio 			= Double.parseDouble(reg.getString("TIPCAMBIO"));
						monto_valuado	 	= Double.parseDouble(reg.getString("MONTO_VALUADO"));
						plazdesc 			= reg.getString("PLAZDESC")==null?"":reg.getString("PLAZDESC");
						porcdesc 			= reg.getString("PORCDESC")==null?"":reg.getString("PORCDESC");
						monto_con_desc 	= Double.parseDouble(reg.getString("MONTO_CON_DESC"));
						modalidad_plazo 	= reg.getString("MODALIDAD_PLAZO")==null?"":reg.getString("MODALIDAD_PLAZO");
						estatus 				= reg.getString("ESTATUS")==null?"":reg.getString("ESTATUS");
						fechacambio 		= reg.getString("FECHACAMBIO")==null?"":reg.getString("FECHACAMBIO");
						cambioestatus 		= reg.getString("CAMBIOESTATUS")==null?"":reg.getString("CAMBIOESTATUS");
						cambiomotivo 		= reg.getString("CAMBIOMOTIVO")==null?"":reg.getString("CAMBIOMOTIVO");
						rs_ic_mon 			= reg.getString("IC_MONEDA")==null?"":reg.getString("IC_MONEDA");
						categoria			= (reg.getString("CATEGORIA")==null)?"":reg.getString("CATEGORIA");
						bandeVentaCartera	= (reg.getString("CG_VENTACARTERA")==null)?"":reg.getString("CG_VENTACARTERA");
						porc_Aforo	= (reg.getString("POR_AFRO")==null)?"0":reg.getString("POR_AFRO"); //F05-2014 
						BigDecimal  por_Aforo= new BigDecimal(porc_Aforo);  //F05-2014 
						BigDecimal  montoD= new BigDecimal(monto);  //F05-2014 /			
						BigDecimal   MontoDesconta = montoD.multiply(por_Aforo.divide(new BigDecimal("100"),10,BigDecimal.ROUND_HALF_UP)); //F05-2014					
				
						if (bandeVentaCartera.equals("S") ) {
							modalidad_plazo ="";
						}
				/*************************/ /* Contenido del archivo */ /**************************/
					pdfDoc.setCell(epo.replace(',',' '),"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(numdoc,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(acuse,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_emision,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_venc,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_publicacion,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(plazodoc,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell('$'+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.RIGHT);
					pdfDoc.setCell(categoria,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(porc_Aforo+'%',"formasrep",ComunesPDF.CENTER);					
					pdfDoc.setCell('$'+Comunes.formatoDecimal(MontoDesconta.toPlainString(),2),"formasrep",ComunesPDF.RIGHT);					
					pdfDoc.setCell(plazdesc,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(porcdesc+'%',"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell('$'+Comunes.formatoDecimal(monto_con_desc,2),"formasrep",ComunesPDF.RIGHT);
					
					if(!"".equals(cgTipoConversion)){
						pdfDoc.setCell(((tipcambio==0)?conversion:""),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell('$'+((tipcambio==0)?"":Comunes.formatoDecimal(tipcambio,2,false)),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell('$'+((tipcambio==0)?"":Comunes.formatoDecimal(monto_valuado,2,false)),"formasrep",ComunesPDF.RIGHT);
					}
					pdfDoc.setCell(modalidad_plazo,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(estatus,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(fechacambio,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(cambioestatus,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(cambiomotivo,"formasrep",ComunesPDF.LEFT);
					nRow++;
				} //fin del while
			}
				else if (informacion.equals("ArchivoTotalPDF"))	{	//*-*-*-*-	Obtine los registros completos del primer grid
				AccesoDB con = new AccesoDB();
				con.conexionDB();
				ResultSet	rs = null;
				rs = queryHelper.getCreateFile(request,con);
				while (rs.next()) {
					if(nRow == 0){
						int numCols = (!"".equals(cgTipoConversion)?23:20);
		
						float widths[];
						if(!"".equals(cgTipoConversion)){
							widths = new float[]{5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,6.0f,6.5f,5.5f,5.0f,5.0f,5.0f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f};
						}else{
							widths = new float[]{6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,7.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f};
						}
						pdfDoc.setTable(numCols,100,widths);
						pdfDoc.setCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setCell("EPO","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Emisión","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo Docto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Categoria","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("% de Descuento Aforo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto a Descontar","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo para descuento en días","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("%   de descuento","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto % de descuento","celda01rep",ComunesPDF.CENTER);
						if(!"".equals(cgTipoConversion)){
							pdfDoc.setCell("Tipo Conv.","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Tipo Cambio","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto Valuado","celda01rep",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("Modalidad de plazo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Cambio Estatus","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo Cambio Estatus","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Causa","celda01rep",ComunesPDF.CENTER);
					}
						clave					= rs.getString("clave")==null?"":rs.getString("clave");
						epo					= rs.getString("EPO")==null?"":rs.getString("EPO");
						numdoc				= rs.getString("NUMDOC")==null?"":rs.getString("NUMDOC");
						acuse 				= rs.getString("ACUSE")==null?"":rs.getString("ACUSE");
						fecha_emision		= rs.getString("FECHA_EMISION")==null?"":rs.getString("FECHA_EMISION");
						df_fecha_venc 		= rs.getString("DF_FECHA_VENC")==null?"":rs.getString("DF_FECHA_VENC");
						fecha_publicacion = rs.getString("FECHA_PUBLICACION")==null?"":rs.getString("FECHA_PUBLICACION");
						plazodoc 			= rs.getString("PLAZODOC")==null?"":rs.getString("PLAZODOC");
						moneda 				= rs.getString("MONEDA")==null?"":rs.getString("MONEDA");
						monto 				= Double.parseDouble(rs.getString("MONTO"));
						conversion			= rs.getString("CONVERSION")==null?"":rs.getString("CONVERSION");
						tipcambio 			= Double.parseDouble(rs.getString("TIPCAMBIO"));
						monto_valuado	 	= Double.parseDouble(rs.getString("MONTO_VALUADO"));
						plazdesc 			= rs.getString("PLAZDESC")==null?"":rs.getString("PLAZDESC");
						porcdesc 			= rs.getString("PORCDESC")==null?"":rs.getString("PORCDESC");
						monto_con_desc 	= Double.parseDouble(rs.getString("MONTO_CON_DESC"));
						modalidad_plazo 	= rs.getString("MODALIDAD_PLAZO")==null?"":rs.getString("MODALIDAD_PLAZO");
						estatus 				= rs.getString("ESTATUS")==null?"":rs.getString("ESTATUS");
						fechacambio 		= rs.getString("FECHACAMBIO")==null?"":rs.getString("FECHACAMBIO");
						cambioestatus 		= rs.getString("CAMBIOESTATUS")==null?"":rs.getString("CAMBIOESTATUS");
						cambiomotivo 		= rs.getString("CAMBIOMOTIVO")==null?"":rs.getString("CAMBIOMOTIVO");
						rs_ic_mon 			= rs.getString("IC_MONEDA")==null?"":rs.getString("IC_MONEDA");
						categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
						bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
						porc_Aforo	= (rs.getString("POR_AFRO")==null)?"0":rs.getString("POR_AFRO"); //F05-2014 
						BigDecimal  por_Aforo= new BigDecimal(porc_Aforo);  //F05-2014 
						BigDecimal  montoD= new BigDecimal(monto);  //F05-2014 /			
						BigDecimal   MontoDesconta = montoD.multiply(por_Aforo.divide(new BigDecimal("100"),10,BigDecimal.ROUND_HALF_UP)); //F05-2014					
				
						if (bandeVentaCartera.equals("S") ) {
							modalidad_plazo ="";
						}
				/*************************/ /* Contenido del archivo */ /**************************/
					pdfDoc.setCell(epo.replace(',',' '),"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(numdoc,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(acuse,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_emision,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_venc,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_publicacion,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(plazodoc,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell('$'+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.RIGHT);
					pdfDoc.setCell(categoria,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(porc_Aforo+'%',"formasrep",ComunesPDF.CENTER);					
					pdfDoc.setCell('$'+Comunes.formatoDecimal(MontoDesconta.toPlainString(),2),"formasrep",ComunesPDF.RIGHT);	
					pdfDoc.setCell(plazdesc,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(porcdesc+'%',"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell('$'+Comunes.formatoDecimal(monto_con_desc,2),"formasrep",ComunesPDF.RIGHT);
					
					if(!"".equals(cgTipoConversion)){
						pdfDoc.setCell(((tipcambio==0)?conversion:""),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell('$'+((tipcambio==0)?"":Comunes.formatoDecimal(tipcambio,2,false)),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell('$'+((tipcambio==0)?"":Comunes.formatoDecimal(monto_valuado,2,false)),"formasrep",ComunesPDF.RIGHT);
					}
					pdfDoc.setCell(modalidad_plazo,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(estatus,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(fechacambio,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(cambioestatus,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(cambiomotivo,"formasrep",ComunesPDF.LEFT);
					nRow++;
				} //fin del while
			}

			if (nRow == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	} catch (Exception e) {
		throw new AppException("Error al generar el archivo", e);
	} finally {
	//Termina proceso de imprimir
	}
%>
<%=jsonObj%>
