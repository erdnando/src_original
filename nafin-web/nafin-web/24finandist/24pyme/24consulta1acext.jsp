<%@ page import="java.util.*, 
			java.sql.*, netropology.utilerias.*,
			com.netro.exception.*,	javax.naming.*,
			com.netro.distribuidores.*" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String acuse = (request.getParameter("acuse")==null)?"":request.getParameter("acuse");

Vector		vecFilas		= null;
Vector		vecFilas1	= null;
Vector		vecFilas2	= null;
Vector		vecColumnas	= null;	
String 		_acuse = "500";
int			i = 0, j = 0;
String		icMoneda 		= "";
String		fechaHoy			= "";

String nombreEpo				=	"";
String igNumeroDocto			=	"";
String ccAcuse 				=	"";
String dfFechaEmision		=	"";
String dfFechaVencimiento	=	"";
String dfFechaPublicacion 	= 	"";
String igPlazoDocto			=	"";
String moneda 					=	"";
double fnMonto 				= 	0;
String cgTipoConv				=	"";
String tipoCambio				=   "";
double montoValuado 			=	0;
String igPlazoDescuento		= 	"";
String fnPorcDescuento		= 	"";
double montoDescuento 		=	0;
String modoPlazo				= 	"";
String estatus 				=	"";
String cambios					= 	"";
String numeroCredito			= 	"";
String nombreIf				= 	"";
String tipoLinea				=	"";
String fechaOperacion		=	"";
String referencia				=	"";
String plazoCredito			=	"";
String fechaVencCredito		=	"";
String relMat					=	"";
String tipoCobroInt			=	"";
String  tipoPiso				=	"";
String icTipoCobroInt		=	"";
String icTasa					=	"";
//String acuseFormateado		=	"";
String usuario					= 	"";
String fechaCarga				= 	"";
String horaCarga				= 	"";
String icTipoFinanciamiento=	"";
String monedaLinea			=	"";
String nombreMLinea			=	"";
String acusePymeFormateado = "";
double montoCredito			=	0;
double tasaInteres			= 	0;
double valorTasaInt			=	0;
double montoTasaInt			=	0;
double fnPuntos				= 	0;
double montoCapitalInt		=	0;

try{
	AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
	fechaHoy 	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	Acuse	acusePyme = new Acuse(acuse);
	acusePymeFormateado = acusePyme.formatear();
	vecFilas = aceptPyme.consultaDoctos(acuse);
	vecFilas1 = (Vector)vecFilas.get(0);
	vecFilas2 = (Vector)vecFilas.get(1);
	%>
	<table width="620" cellpadding="1" cellspacing="2" border="0">

	<tr>
		<td class="formas">&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
		<table width="500" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
		<tr>
			<td class="celda01" colspan="4" align="center">Moneda nacional</td>
			<td class="celda01" colspan="4" align="center">D�lares</td>
			<td class="celda01" colspan="4" align="center">Doctos. en DLL financiados en M.N.</td>	
		</tr>
		<tr>
	<%//Titulo DM%>
			<td class="celda01" align="center">Num. total de doctos.</td>
			<td class="celda01" align="center">Monto total de doctos. iniciales</td>
			<td class="celda01" align="center">Monto total de doctos. finales</td>
			<td class="celda01" align="center">Monto intereses</td>
			<td class="celda01" align="center">Num. total de doctos.</td>
			<td class="celda01" align="center">Monto total de doctos. iniciales</td>
			<td class="celda01" align="center">Monto total de doctos. finales</td>
			<td class="celda01" align="center">Monto intereses</td>
			<td class="celda01" align="center">Num. total de doctos.</td>
			<td class="celda01" align="center">Monto total de doctos. iniciales</td>		
			<td class="celda01" align="center">Monto total de doctos. finales</td>
			<td class="celda01" align="center">Monto intereses</td>
		</tr>
	<%
	int		totalDoctosMN = 0;
	double	totalMontoMN = 0;
	double	totalMontoDescMN = 0;
	double	totalInteresMN = 0;
	double	totalMontoCreditoMN = 0;
	
	int		totalDoctosUSD = 0;
	double	totalMontoUSD = 0;
	double	totalMontoDescUSD = 0;
	double	totalInteresUSD = 0;
	double	totalMontoCreditoUSD = 0;
	
	int 		totalDoctosConv = 0;
	double	totalMontosConv = 0;
	double	totalMontoDescConv = 0;
	double	totalInteresConv = 0;
	double	totalConvPesos = 0;
	double	totalMontoCreditoConv = 0;
					
	for(i=0;i<vecFilas2.size();i++,j++){
		vecColumnas 		= (Vector)vecFilas2.get(i);
		nombreEpo			=	(String)vecColumnas.get(0);
		igNumeroDocto		=	(String)vecColumnas.get(1);
		ccAcuse 				=	(String)vecColumnas.get(2);
		dfFechaEmision		=	(String)vecColumnas.get(3);
		dfFechaVencimiento=	(String)vecColumnas.get(4);
		dfFechaPublicacion= 	(String)vecColumnas.get(5);
		igPlazoDocto		=	(String)vecColumnas.get(6);
		moneda 				=	(String)vecColumnas.get(7);
		fnMonto 				= 	Double.parseDouble(vecColumnas.get(8).toString());
		tipoCambio			=   (String)vecColumnas.get(10);
		montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
		igPlazoDescuento	= 	(String)vecColumnas.get(12);
		fnPorcDescuento	= 	(String)vecColumnas.get(13);
		modoPlazo			= 	(String)vecColumnas.get(14);
		estatus 				=	(String)vecColumnas.get(15);
		cambios				= 	(String)vecColumnas.get(16);
		//creditos por concepto de intereses
		numeroCredito		= 	(String)vecColumnas.get(17);
		nombreIf			= 	(String)vecColumnas.get(18);
		tipoLinea			=	(String)vecColumnas.get(19);
		fechaOperacion		=	(String)vecColumnas.get(20);
		fechaVencCredito	=	(String)vecColumnas.get(23);
		relMat				= 	(String)vecColumnas.get(25);
		fnPuntos				=	Double.parseDouble(vecColumnas.get(26).toString());
		tipoCobroInt		=	(String)vecColumnas.get(27);
		referencia			=	(String)vecColumnas.get(29);
		tipoPiso				= 	(String)vecColumnas.get(30);
		icTipoCobroInt		= 	(String)vecColumnas.get(31);
		//acuseFormateado	=	(String)vecColumnas.get(34);
		//foda 006-2005	= 	(String)vecColumnas.get(37);
		fechaCarga			= 	(String)vecColumnas.get(35);
		horaCarga			= 	(String)vecColumnas.get(36);
		icTipoFinanciamiento= 	(String)vecColumnas.get(39);
		nombreMLinea		= 	(String)vecColumnas.get(40);
	/************************************************************************************/
		cgTipoConv			=	(String)vecColumnas.get(9);
		montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
		plazoCredito		=	(String)vecColumnas.get(22);
		valorTasaInt		=	Double.parseDouble(vecColumnas.get(41).toString());
		icMoneda			= 	(String)vecColumnas.get(32);
		monedaLinea			= 	(String)vecColumnas.get(38);
		fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
	/************************************************************************************/
		plazoCredito = ("".equals(plazoCredito))?"0":plazoCredito;
		montoCredito = fnMonto - montoDescuento;				
		if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
			if("1".equals(icTipoFinanciamiento))
				montoCredito = montoValuado;
		}
		if("1".equals(tipoPiso)){
			montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
		}else{
			montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
		}
		montoCapitalInt = montoCredito +(montoTasaInt*Double.parseDouble(plazoCredito));

		if("1".equals(icMoneda)){
			totalDoctosMN		++;
			totalMontoMN		+= fnMonto;
			totalMontoCreditoMN += montoCredito;
	//					if("S".equals(icTipoCobroInt))
				totalInteresMN		+= montoTasaInt*Double.parseDouble(plazoCredito);
		} else if("54".equals(icMoneda)&&"54".equals(monedaLinea)) {
			totalDoctosUSD 		++;
			totalMontoUSD		+= fnMonto;
			totalMontoCreditoUSD+= montoCredito;
	//					if("S".equals(icTipoCobroInt))
				totalInteresUSD		+= montoTasaInt*Double.parseDouble(plazoCredito);
		} else if("54".equals(icMoneda)&&"1".equals(monedaLinea)) {
			totalDoctosConv		++;
			totalMontosConv		+= fnMonto;
			totalMontoCreditoConv+= montoCredito;
	//					if("S".equals(icTipoCobroInt))
				totalInteresConv	+= montoTasaInt*Double.parseDouble(plazoCredito);
		}
	}%>		
		<tr>
			<td class="formas">&nbsp;<%=totalDoctosMN%></td>
			<td class="formas">$&nbsp;<%=totalMontoMN%></td>
			<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(totalMontoCreditoMN,2)%></td>
			<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(totalInteresMN,2)%></td>
			<td class="formas">&nbsp;<%=totalDoctosUSD%></td>
			<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(totalMontoUSD,2)%></td>
			<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(totalMontoCreditoUSD,2)%></td>
			<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(totalInteresUSD,2)%></td>
			<td class="formas">&nbsp;<%=totalDoctosConv%></td>
			<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(totalMontosConv,2)%></td>
			<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(totalMontoCreditoConv,2)%></td>
			<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(totalInteresConv,2)%></td>	
		</tr>
		<tr>
			<td class="formas" colspan="12">
				Al transmitir este mensaje de datos  y para todos los efectos legales Usted, bajo su responsabilidad, manifiesta su aceptaci&oacute;n para que sea(n) sustituido(s) el(los) DOCUMENTO(S)  INICIAL(ES) que ha seleccionado por el(los) DOCUMENTO(S) FINAL(ES) y estos &uacute;ltimos sean descontados a favor de la EMPRESA DE PRIMER ORDEN. 
				Dicho(s)  DOCUMENTO(S)  FINALE(S) contiene(n) los Derechos de Cobro a su cargo por lo que Usted   acepta pagar el 100% de su valor al INTERMEDIARIO FINANCIERO en su fecha de vencimiento.
				Manifiesta tambi&eacute;n su obligaci&oacute;n de cubrir  al INTERMEDIARIO FINANCIERO,  los intereses  que se detallan en esta pantalla, en la fecha de su operaci&oacute;n, por haber aceptado la sustituci&oacute;n.
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="formas">&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
		<table cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
	<%	for(i=0;i<vecFilas1.size();i++,j++){
			vecColumnas 		= (Vector)vecFilas1.get(i);
			nombreEpo			=	(String)vecColumnas.get(0);
			igNumeroDocto		=	(String)vecColumnas.get(1);
			ccAcuse 				=	(String)vecColumnas.get(2);
			dfFechaEmision		=	(String)vecColumnas.get(3);
			dfFechaVencimiento=	(String)vecColumnas.get(4);
			dfFechaPublicacion= 	(String)vecColumnas.get(5);
			igPlazoDocto		=	(String)vecColumnas.get(6);
			moneda 				=	(String)vecColumnas.get(7);
			fnMonto 				= 	Double.parseDouble((String)vecColumnas.get(8).toString());
			cgTipoConv			=	(String)vecColumnas.get(9);
			tipoCambio			=   (String)vecColumnas.get(10);
			montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
			igPlazoDescuento	= 	(String)vecColumnas.get(12);
			fnPorcDescuento	= 	(String)vecColumnas.get(13);
			montoDescuento 	=	Double.parseDouble(vecColumnas.get(28).toString());
			modoPlazo			= 	(String)vecColumnas.get(14);
			estatus 				=	(String)vecColumnas.get(15);
			cambios				= 	(String)vecColumnas.get(16);
			icMoneda				= 	(String)vecColumnas.get(32);
			//foda 006-2005 usuario				= 	(String)vecColumnas.get(37);
			fechaCarga			= 	(String)vecColumnas.get(35);
			horaCarga			= 	(String)vecColumnas.get(36);
			if(i==0){%>
				<tr>
					<td class="celda01" colspan="17">Documento</td>
				</tr>
				<tr>
					<td class="celda01" align="center">Epo</td>
					<td class="celda01" align="center">Num. docto. inicial</td>
					<td class="celda01" align="center">Num. acuse</td>
					<td class="celda01" align="center">Fecha de emisi&oacute;n</td>
					<td class="celda01" align="center">Fecha vencimiento</td>
					<td class="celda01" align="center">Fecha de publicaci&oacute;n</td>
					<td class="celda01" align="center">Plazo docto.</td>
					<td class="celda01" align="center">Moneda</td>
					<td class="celda01" align="center">Monto</td>
					<td class="celda01" align="center">Tipo conv.</td>
					<td class="celda01" align="center">Plazo para descuento en dias</td>
					<td class="celda01" align="center">% de descuento</td>
					<td class="celda01" align="center">Monto % de descuento</td>
					<td class="celda01" align="center">Tipo cambio</td>
					<td class="celda01" align="center">Monto valuado</td>
					<td class="celda01" align="center">Modalidad de plazo</td>
					<td class="celda01" align="center">Estatus</td>
				</tr>
	<%		}%>			
		<tr>
				<td class="formas">&nbsp;<%=nombreEpo%></td>
				<td class="formas">&nbsp;<%=igNumeroDocto%></td>
				<td class="formas">&nbsp;<%=ccAcuse%></td>
				<td class="formas">&nbsp;<%=dfFechaEmision%></td>
				<td class="formas">&nbsp;<%=dfFechaVencimiento%></td>
				<td class="formas">&nbsp;<%=dfFechaPublicacion%></td>
				<td class="formas">&nbsp;<%=igPlazoDocto%></td>
				<td class="formas">&nbsp;<%=moneda%></td>
				<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(fnMonto,2)%></td>
				<td class="formas" align="center">&nbsp;<%=cgTipoConv%></td>
				<td class="formas" align="center">&nbsp;<%=igPlazoDescuento%></td>
				<td class="formas" align="center">&nbsp;<%=fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %")%></td>
				<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(montoDescuento,2)%></td>
				<td class="formas">&nbsp;<%=tipoCambio%></td>
				<td class="formas">&nbsp;<%=Comunes.formatoDecimal(montoValuado,2)%></td>
				<td class="formas">&nbsp;<%=modoPlazo%></td>
				<td class="formas">&nbsp;<%=estatus%></td>	
		</tr>
	<%	}%>	
		</table>
		</td>
	</tr>
	<tr>
		<td class="formas">&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
		<table cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
	<%		for(i=0;i<vecFilas2.size();i++){
				vecColumnas = (Vector)vecFilas2.get(i);
				nombreEpo			=	(String)vecColumnas.get(0);
				igNumeroDocto		=	(String)vecColumnas.get(1);
				ccAcuse 				=	(String)vecColumnas.get(2);
				dfFechaEmision		=	(String)vecColumnas.get(3);
				dfFechaVencimiento=	(String)vecColumnas.get(4);
				dfFechaPublicacion= 	(String)vecColumnas.get(5);
				igPlazoDocto		=	(String)vecColumnas.get(6);
				moneda 				=	(String)vecColumnas.get(7);
				fnMonto 				= 	Double.parseDouble(vecColumnas.get(8).toString());
				cgTipoConv			=	(String)vecColumnas.get(9);
				tipoCambio			=   (String)vecColumnas.get(10);
				montoDescuento 	=	Double.parseDouble(vecColumnas.get(28).toString());
				montoValuado 		=	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
				igPlazoDescuento	= 	(String)vecColumnas.get(12);
				fnPorcDescuento	= 	(String)vecColumnas.get(13);
				modoPlazo			= 	(String)vecColumnas.get(14);
				estatus 				=	(String)vecColumnas.get(15);
				cambios				= 	(String)vecColumnas.get(16);
				//creditos por concepto de intereses
				numeroCredito		= 	(String)vecColumnas.get(17);
				nombreIf				= 	(String)vecColumnas.get(18);
				tipoLinea			=	(String)vecColumnas.get(19);
				fechaOperacion		=	(String)vecColumnas.get(20);
				plazoCredito		=	(String)vecColumnas.get(22);
				fechaVencCredito	=	(String)vecColumnas.get(23);
				valorTasaInt		=	Double.parseDouble(vecColumnas.get(41).toString());
				relMat				= 	(String)vecColumnas.get(25);
				fnPuntos				=	Double.parseDouble(vecColumnas.get(26).toString());
				tipoCobroInt		=	(String)vecColumnas.get(27);
				referencia			=	(String)vecColumnas.get(29);
				tipoPiso				= 	(String)vecColumnas.get(30);
				icTipoCobroInt		= 	(String)vecColumnas.get(31);
				icMoneda				= 	(String)vecColumnas.get(32);
				//acuseFormateado	=	(String)vecColumnas.get(34);
				//foda 006-2005 usuario				= 	(String)vecColumnas.get(37);
				fechaCarga			= 	(String)vecColumnas.get(35);
				horaCarga			= 	(String)vecColumnas.get(36);
				icTipoFinanciamiento= 	(String)vecColumnas.get(39);
				monedaLinea			= 	(String)vecColumnas.get(38);
				nombreMLinea		= 	(String)vecColumnas.get(40);

				plazoCredito = ("".equals(plazoCredito))?"0":plazoCredito;

				montoCredito = fnMonto -montoDescuento;
				
				if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
					if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
						montoCredito = montoValuado;
					else if("2".equals(icTipoFinanciamiento))
						montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
				}
				
				if("1".equals(tipoPiso)){
					montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
				}else{
					montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
				}
				montoCapitalInt = montoCredito +(montoTasaInt*Double.parseDouble(plazoCredito));
				if(i==0){
				%>
					<tr>
	<%//Titulo DM%>					
						<td class="celda01" colspan="16" align="center">Datos Documento Inicial</td>
						<td class="celda01" colspan="11" align="center">Datos Documento Final</td>
					</tr>
					<tr>
						<td class="celda01" align="center">Epo</td>
						<td class="celda01" align="center">Num. docto.</td>
						<td class="celda01" align="center">Num. acuse</td>
						<td class="celda01" align="center">Fecha de emisi&oacute;n</td>
						<td class="celda01" align="center">Fecha vencimiento</td>
						<td class="celda01" align="center">Fecha de publicaci&oacute;n</td>
						<td class="celda01" align="center">Plazo docto.</td>
						<td class="celda01" align="center">Moneda</td>
						<td class="celda01" align="center">Monto</td>
						<td class="celda01" align="center">Plazo para descuento en dias</td>
						<td class="celda01" align="center">% de descuento</td>
						<td class="celda01" align="center">Monto a descontar</td>
						<td class="celda01" align="center">Tipo conv.</td>
						<td class="celda01" align="center">Tipo cambio</td>
						<td class="celda01" align="center">Monto valuado</td>
						<td class="celda01" align="center">Modalidad de plazo</td>
	<!--					<td class="celda01" align="center">Estatus</td>-->
						<td class="celda01" align="center">Num. de documento final</td>
						<td class="celda01" align="center">Moneda</td>
						<td class="celda01" align="center">Monto</td>
						<td class="celda01" align="center">Plazo</td>
						<td class="celda01" align="center">Fecha de vencimiento</td>
						<td class="celda01" align="center">Fecha de operaci&oacute;n</td>
						<td class="celda01" align="center">IF</td>
						<td class="celda01" align="center">Referencia tasa de inter&eacute;s</td>
						<td class="celda01" align="center">Valor tasa de inter&eacute;s</td>
						<td class="celda01" align="center">Monto de intereses</td>
						<td class="celda01" align="center">Monto Total de Capital e Inter&eacute;s</td>
					</tr>
	<%			}	%>
			<tr>
				<td class="formas">&nbsp;<%=nombreEpo%></td>
				<td class="formas">&nbsp;<%=igNumeroDocto%></td>
				<td class="formas">&nbsp;<%=ccAcuse%></td>
				<td class="formas">&nbsp;<%=dfFechaEmision%></td>
				<td class="formas">&nbsp;<%=dfFechaVencimiento%></td>
				<td class="formas">&nbsp;<%=dfFechaPublicacion%></td>
				<td class="formas">&nbsp;<%=igPlazoDocto%></td>
				<td class="formas">&nbsp;<%=moneda%></td>
				<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(fnMonto,2)%></td>
				<td class="formas" align="center">&nbsp;<%=igPlazoDescuento%></td>
				<td class="formas" align="center">&nbsp;<%=fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %")%></td>
				<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(montoDescuento,2)%></td>
<%				if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){	%>
					<td class="formas" align="center">&nbsp;<%=cgTipoConv%></td>
					<td class="formas">&nbsp;<%=tipoCambio%></td>
					<td class="formas">$&nbsp;<%=Comunes.formatoDecimal(montoValuado,2)%></td>
<%				}else{	%>
					<td class="formas">&nbsp;</td>
					<td class="formas">&nbsp;</td>
					<td class="formas">&nbsp;</td>
<%				}		%>
				<td class="formas">&nbsp;<%=modoPlazo%></td>
	<!--		Cr�dito por concepto de intereses	-->
				<td class="formas">&nbsp;<%=numeroCredito%></td>
				<td class="formas">&nbsp;<%=nombreMLinea%></td>
				<td class="formas"><%=("S".equals(icTipoCobroInt))?"":"$&nbsp;"+Comunes.formatoDecimal(montoCredito,2)%></td>
				<td class="formas"><%="0".equals(plazoCredito)?"&nbsp;":plazoCredito%></td>
				<td class="formas">&nbsp;<%=fechaVencCredito%></td>
				<td class="formas">&nbsp;<%=fechaHoy%></td>
				<td class="formas">&nbsp;<%=nombreIf%></td>
				<td class="formas">&nbsp;<%=("S".equals(icTipoCobroInt))?"":referencia+" "+relMat+" "+fnPuntos%></td>
				<td class="formas"><%=("S".equals(icTipoCobroInt))?"&nbsp;":"&nbsp;"+Comunes.formatoDecimal(valorTasaInt,2)+"%"%></td>
				<td class="formas"><%=("S".equals(icTipoCobroInt))?"&nbsp;":"$&nbsp;"+Comunes.formatoDecimal(montoTasaInt*Double.parseDouble(plazoCredito),2)%></td>
				<td class="formas"><%=("S".equals(icTipoCobroInt))?"&nbsp;":"$&nbsp;"+Comunes.formatoDecimal(montoCapitalInt,2)%></td>
			</tr>
	<%	}	%>
		</table>
		</td>
	</tr>
	<tr>
		<td align="center">
		<table cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
		<tr>
			<td class="celda01" colspan="2" align="center">Datos de cifras de control</td>
		</tr>
		<tr>
			<td class="formas">Num. acuse</td>
			<td class="formas">&nbsp;<%=acusePymeFormateado%></td>
		</tr>
		<tr>
			<td class="formas">Fecha</td>
			<td class="formas">&nbsp;<%=fechaCarga%></td>
		</tr>
		<tr>
			<td class="formas">Hora</td>
			<td class="formas">&nbsp;<%=horaCarga%></td>
		</tr>
		<tr>
			<td class="formas">N�mero de usuario</td>
			<!--foda 006-2005<td class="formas">Nombre y n�mero de usuario</td>-->
			<!--foda 006-2005<td class="formas">&nbsp;<//%=iNoUsuario+" "+usuario%></td>-->
			<td class="formas">&nbsp;<%=iNoUsuario%></td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="right">
		<table cellpadding="2" cellspacing="1" border="0">
		<tr>&nbsp;</tr>
		</table>
		</td>
	</tr>
	</table>
<%
} catch (Exception e) {
	throw new AppException("Error al mostrar informaci�n", e);
} finally {
}
%>
