<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.anticipos.*,
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<% 
//com.netro.etiquetas.CatalogoEPO, hay que poner esta clase
System.out.println("13consulta03ext.data.jsp (E)"); 
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_epo = request.getParameter("no_epo") == null?"":(String)request.getParameter("no_epo");
String infoRegresar = "";
AccesoDB con = new AccesoDB();
String ses_ic_pyme = iNoCliente;

try {
	con.conexionDB();

	if (informacion.equals("CatalogoEPO") ) {
		 
		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClavePyme(ses_ic_pyme);
		infoRegresar = cat.getJSONElementos();	
		
	} else if (informacion.equals("Consulta") && !ses_ic_pyme.equals("") && !ic_epo.equals("")) {
	
		String moneda ="1,54";
		int liRegistros=0;
		int Registros1 = 0;
		int Registros2 = 0;
		int Registros3 = 0;
		int index=0;
		Vector vecFilas		= null;
		Vector vecColumnas	= null;
		Vector vecFilas2	= null;
		Vector vecColumnas2 = null;
		Vector lovTasas		= null;
		Vector vector		= null;
		String total = "",tipoCredito = "",pagoInt = "",encabezado="";
		String ifRazonSoc = "",ic_if="",epoRazonSoc = "";
		String aux="";
		boolean DM=true,CCC=true;
		JSONArray registros = new JSONArray();
		HashMap registro = null;
	
		CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);
	
		
		vecFilas = BeanTasas.disOpera("",4,iNoCliente);
		for (int i = 0;i<vecFilas.size();i++) {
			vecColumnas = (Vector)vecFilas.get(i);
			total = (String)vecColumnas.get(0);
			tipoCredito = (String)vecColumnas.get(1);
			//para Descuento Mercantil
			if (Integer.parseInt(total) > 0 && "D".equals(tipoCredito)){
				pagoInt = BeanTasas.paramPagoInteres(ic_epo,4);
				if ("D".equals(pagoInt)){	//si la pyme es la responsable del pago de interes
					vecFilas2 = BeanTasas.esquemaParticular(ic_epo,4,"",iNoCliente); // obtiene las EPO e IF que tienen una LC vigente
					for (int j=0; j<vecFilas2.size(); j++) {
						DM = false;
						Vector lovDatosTas = (Vector)vecFilas2.get(j);
						ic_if = lovDatosTas.get(0).toString();
						ifRazonSoc = lovDatosTas.get(1).toString();
						epoRazonSoc = lovDatosTas.get(2).toString();					
						lovTasas = BeanTasas.ovgetTasasxEpo(4,ic_epo,ic_if);					
						
						
						if(lovTasas.size()>0){
							for (int y=0; y<lovTasas.size(); y++) {
								Vector lovDatosTasa = (Vector)lovTasas.get(y);
								registro = new HashMap();						
								registro.put("NOMBREIF", encabezado );
								registro.put("NOMBREMONEDA", lovDatosTasa.get(0).toString()); 				
								registro.put("NOMBRETASA",lovDatosTasa.get(1).toString()); 				
								registro.put("RANGOPLAZO",lovDatosTasa.get(3).toString()); 					
								registro.put("VALOR",lovDatosTasa.get(4).toString()); 					
								registro.put("RELMAT",lovDatosTasa.get(5).toString()); 					
								registro.put("PTOS",lovDatosTasa.get(6).toString()); 					
								registro.put("TASA_APLICAR",lovDatosTasa.get(7).toString()); 	
								registro.put("FECHAULTIMA",lovDatosTasa.get(8).toString());								
								registros.add(registro);
							}
						}
					}//for (int j=0; j<vecFilas2.size(); j++)
				}
			}
			//-----------------------------para Credito Cuenta Corriente
			if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito)){
			
				//para Obtener los Intermediarios Financieros
				vecFilas2 = BeanTasas.esquemaParticular(ic_epo,4,"",iNoCliente,"C"); // obtiene las EPO e IF que tienen una LC vigente
				
				for (int j=0; j<vecFilas2.size(); j++) {
					DM = false;
					Vector lovDatosTas = (Vector)vecFilas2.get(j);
					ic_if = lovDatosTas.get(0).toString();
					ifRazonSoc = lovDatosTas.get(1).toString();
					epoRazonSoc = lovDatosTas.get(2).toString();					
					encabezado = epoRazonSoc+" - "+ifRazonSoc;
					lovTasas = BeanTasas.ovgetTasasxPyme(4,ses_ic_pyme,ic_if,"1, 54");
					
						if(lovTasas.size()>0){
							for (int y=0; y<lovTasas.size(); y++) {
								Vector lovDatosTasa = (Vector)lovTasas.get(y);
								registro = new HashMap();						
								registro.put("NOMBREIF", encabezado );
								registro.put("NOMBREMONEDA", lovDatosTasa.get(0).toString()); 				
								registro.put("NOMBRETASA",lovDatosTasa.get(1).toString()); 				
								registro.put("RANGOPLAZO",lovDatosTasa.get(3).toString()); 					
								registro.put("VALOR",lovDatosTasa.get(4).toString()); 					
								registro.put("RELMAT",lovDatosTasa.get(5).toString()); 					
								registro.put("PTOS",lovDatosTasa.get(6).toString()); 					
								registro.put("TASA_APLICAR",lovDatosTasa.get(7).toString()); 	
								registro.put("FECHAULTIMA",lovDatosTasa.get(8).toString()); 
								registros.add(registro);
							}
						}
				}//for (int j=0; j<vecFilas2.size(); j++)		
			}//if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito))
		}//for (int i = 0;i<vecFilas.size();i++) 
		
		infoRegresar =  "({\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"})";
				
	}
} catch(Exception e) { 
	out.println(e.getMessage()); } 
	finally { 
		if (con.hayConexionAbierta()) {
				con.cierraConexionDB();	
		}
	}
%>

<%=infoRegresar%>

<%System.out.println("13consulta03ext.data.jsp (S)");%>


