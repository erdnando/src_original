Ext.onReady(function() {

	var jsonValoresIniciales = null;
	var claveDocto = "";
	var claveEpo = "";
	
	function procesaValoresBusqueda(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
				var hiddenCliente = Ext.getCmp('hidCliente');
				hiddenCliente.setRawValue(jsonValoresIniciales.Cliente);
				var hiddenBanco = Ext.getCmp('hidBanco');
				hiddenBanco.setRawValue(jsonValoresIniciales.Banco);
				// Se le da formato a los campos de las fechas
				var fecha_ini1 = '';
				var fecha_fin1 = '';
				if(Ext.getCmp('fecha_ini').getValue() != ''){
					fecha_ini1 = new Date(Ext.getCmp('fecha_ini').getValue()).format('d/m/Y');
				}
				if(Ext.getCmp('fecha_fin').getValue() != ''){
					fecha_fin1 = new Date(Ext.getCmp('fecha_fin').getValue()).format('d/m/Y');
				}
				if (jsonValoresIniciales.tipoCredito == 'D')	{
					consultaData_A.load({params: Ext.apply(fp.getForm().getValues(),{operacion: 'Generar', fecha_ini1:fecha_ini1, fecha_fin1:fecha_fin1, start: 0,limit: 15})});
				}else	{
					consultaData_B.load({params: Ext.apply(fp.getForm().getValues(),{operacion: 'Generar', fecha_ini1:fecha_ini1, fecha_fin1:fecha_fin1, start: 0,limit: 15})});
				}
				var displayCliente = Ext.getCmp('dis_cliente');
				displayCliente.setValue(jsonValoresIniciales.Cliente);
				if (!displayCliente.isVisible())	{
					displayCliente.show();
				}
				var displayBanco = Ext.getCmp('dis_banco');
				displayBanco.setValue(jsonValoresIniciales.Banco);
				if (!displayBanco.isVisible())	{
					displayBanco.show();
				}
				if (!fpInfo.isVisible())	{
					fpInfo.show();
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData_A = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma_A');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (Ext.getCmp('fecha_ini').getValue() != null && Ext.getCmp('fecha_ini').getValue() != "" && Ext.getCmp('fecha_fin').getValue() != null && Ext.getCmp('fecha_fin').getValue() != ""){
				fecha_ini = Ext.getCmp('fecha_ini').getValue().format('d/m/Y');
				fecha_fin = Ext.getCmp('fecha_fin').getValue().format('d/m/Y');
				grid_A.setTitle('<div><div style="float:left">Resumen estado de adeudos</div><div style="float:right">Movimientos del ' + fecha_ini + ' al ' + fecha_fin +'</div></div>');
			}
			if (!grid_A.isVisible()) {
				grid_A.show();
			}
			//var btnBajarArchivo = Ext.getCmp('btnBajArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenArchivo');
			var btnGenerarPDF = Ext.getCmp('btnGenPDF');
			//var btnBajarPDF = Ext.getCmp('btnBajPDF');
			var btnTotales = Ext.getCmp('btnTotales');
			var el = grid_A.getGridEl();
			if(store.getTotalCount() > 0) {
				btnTotales.enable();
				btnGenerarPDF.enable();
				btnGenerarArchivo.enable();
				/*btnBajarPDF.hide();
				if(!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}
				*/
				el.unmask();
			}else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
/*
	var procesarSuccessFailureGenArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenArchivo');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenArchivo').enable();
		Ext.getCmp('btnGenArchivo').setIconClass('icoXls');
	}

/*
	var procesarSuccessFailureGenPDF =  function(opts, success, response) {
		var btnGeneraPDF = Ext.getCmp('btnGenPDF');
		btnGeneraPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajaPDF = Ext.getCmp('btnBajPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenPDF').enable();
		Ext.getCmp('btnGenPDF').setIconClass('icoPdf');
	}
//-*-*-*-*-*-*-*-*-*
	var procesarConsultaData_B = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma_A');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (Ext.getCmp('fecha_ini').getValue() != null && Ext.getCmp('fecha_ini').getValue() != "" && Ext.getCmp('fecha_fin').getValue() != null && Ext.getCmp('fecha_fin').getValue() != ""){
				fecha_ini = Ext.getCmp('fecha_ini').getValue().format('d/m/Y');
				fecha_fin = Ext.getCmp('fecha_fin').getValue().format('d/m/Y');
				grid_B.setTitle('<div><div style="float:left">Resumen estado de adeudos</div><div style="float:right">Movimientos del ' + fecha_ini + ' al ' + fecha_fin +'</div></div>');
			}
			if (!grid_B.isVisible()) {
				grid_B.show();
			}
			//var btnBajarArchivo = Ext.getCmp('btnBajArchivo_B');
			var btnGenerarArchivo = Ext.getCmp('btnGenArchivo_B');
			var btnGenerarPDF = Ext.getCmp('btnGenPDF_B');
			//var btnBajarPDF = Ext.getCmp('btnBajPDF_B');
			var btnTotales = Ext.getCmp('btnTotales_B');
			var el = grid_B.getGridEl();
			if(store.getTotalCount() > 0) {
				btnTotales.enable();
				btnGenerarPDF.enable();
				btnGenerarArchivo.enable();
				//btnBajarPDF.hide();
				/*if(!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}*/
				el.unmask();
			}else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarPanelDocto = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		claveDocto = registro.get('NUM_DOCTO');
		claveEpo = Ext.getCmp('cmbEpo').getValue();
		consultaData.load({params: {ic_epo: claveEpo, numDoc: claveDocto, operacion: 'Generar', start: 0, limit: 15}});
		var ventana = Ext.getCmp('verNumDocto');
		if (ventana) {
			ventana.show();
		}else{
			new Ext.Window({
				layout: 'column',
				width: 815,
				height: '500',
				resizable: true,
				//height: 'auto',
				id: 'verNumDocto',
				closeAction: 'hide',
				items: [
					fp1,NE.util.getEspaciador(1),gridDetalleDoctos,gridTotal
				],
				title: 'Detalle de Pago de Inter�s'
			}).show();
		}
	}

	var procesarConsultaData = function (store, arrRegistros, opts) {
		var lblFechaPago = Ext.getCmp('lblFechaPago');
		var lblNumDocto = Ext.getCmp('lblNumDocto');
      var fecha_dpo = "";
		var gridDetalle = Ext.getCmp('gridGeneral');
		if(arrRegistros!= null) {
			/*if(!gridDetalle.isVisible()) {
				gridDetalle.show();
			}*/
			//var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			//var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnTotal = Ext.getCmp('btnTotal');
			
			var el = gridDetalle.getGridEl();
			
			if(store.getTotalCount() > 0) {
				var row = store.getAt(0);
		      fecha_dpo = row.get('FECHA_DPO');
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				//btnBajarArchivo.hide();
				//btnBajarPDF.hide();
				btnTotal.enable();
				lblNumDocto.getEl().update(claveDocto);
				lblFechaPago.getEl().update(fecha_dpo);
				btnGenerarArchivo.enable();
				/*if(!btnBajarArchivo.isVisible()){
					btnGenerarArchivo.enable;
				} else {
					btnGenerarPDF.disable();
				}
				if(!btnBajarPDF.isVisible()){
					btnGenerarPDF.enable();
				} else {
					btnGenerarPDF.disable();
				}*/
				el.unmask();
			}else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnTotal.disable();
				lblNumDocto.getEl().update('');
				lblFechaPago.getEl().update('');
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
/*
	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarArchivo = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}
/*
	var procesarSuccessFailureGenerarPDF = function (opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarPDF.setHandler(function (boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarPDF = function (opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}
/*
	var procesarSuccessFailureGenArchivo_B =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenArchivo_B');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajArchivo_B');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenArchivo_B =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenArchivo_B').enable();
		Ext.getCmp('btnGenArchivo_B').setIconClass('icoXls');
	}
/*
	var procesarSuccessFailureGenPDF_B =  function(opts, success, response) {
		var btnGeneraPDF = Ext.getCmp('btnGenPDF_B');
		btnGeneraPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajaPDF = Ext.getCmp('btnBajPDF_B');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenPDF_B =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenPDF_B').enable();
		Ext.getCmp('btnGenPDF_B').setIconClass('icoPdf');
	}

	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta06ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPODist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoEPODistData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPODistribuidores'
		},
		totalProperty: 'total',
		autoload: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta06ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIfDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta06ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMonedaDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'IC_DOCUMENTO'},
			{name: 'FECHA_DPO'},
			{name: 'FECHA_PAGO'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'TASA_REFERENCIA'},
			{name: 'VALOR_TASA_INT'},
			{name: 'MONTO_TASA_INT', type:'float'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});
	var totalData = new Ext.data.JsonStore({
		fields: [
			{name: 'MONTO_TASA_INT',type: 'float'}
		],
		data: [
			{MONTO_TASA_INT:'0'}
		],
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var consultaData_A = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta06ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'FECHA_OPERA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'NUM_CLIENTE'},
			{name: 'EPO'},
			{name: 'NUM_DOCTO'},
			{name: 'NUM_CREDITO'},
			{name: 'F_VENC_CREDITO',	type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'MONTO_CRED',			type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'INT_GENERADOS',		type: 'float'},
			{name: 'PAGO_A_CAPITAL',	type: 'float'},
			{name: 'PAGO_INT',			type: 'float'},
			{name: 'SALDO_CRE',			type: 'float'},
			{name: 'TIPO_COB_INT'},
			{name: 'ESTATUS'},
			{name: 'IC_TIPO_COBRO_INTERESES'},
			{name: 'IC_ESTATUS_DOCTO'},
			{name: 'MONTO_DOC_FINAL',	type: 'float'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData_A,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData_A(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var enviaParametros =  function(store, options){
		var valEpo = Ext.getCmp('forma_A').getForm().findField("ic_epo").getValue();
		var valIF = Ext.getCmp('forma_A').getForm().findField("ic_if").getValue();
		var valMoneda = Ext.getCmp('forma_A').getForm().findField("ic_moneda").getValue();
		var valFec_ini = Ext.getCmp('forma_A').getForm().findField("fecha_inicio").getValue();
		var valFec_fin = Ext.getCmp('forma_A').getForm().findField("fecha_final").getValue();
		Ext.apply(options.params, {ic_epo: valEpo,ic_if:valIF,ic_moneda:valMoneda,fecha_inicio:valFec_ini,fecha_final:valFec_fin});
	}
	
	var consultaData_B = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta06ext.data.jsp',
		baseParams: {
			informacion: 'Consulta_B'
		},
		fields: [
			{name: 'IC_DOCUMENTO'},
			{name: 'FECHA_OPERA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'NUM_CLIENTE'},
			{name: 'EPO'},
			{name: 'NUM_DOCTO'},
			{name: 'NUM_CREDITO'},
			{name: 'F_VENC_CREDITO',	type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'MONTO_CRED',			type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'INT_GENERADOS',		type: 'float'},
			{name: 'PAGO_A_CAPITAL',	type: 'float'},
			{name: 'PAGO_INT',			type: 'float'},
			{name: 'SALDO_CRE',			type: 'float'},
			{name: 'TIPO_COB_INT'},
			{name: 'ESTATUS'},
			{name: 'IC_TIPO_COBRO_INTERESES'},
			{name: 'IC_ESTATUS_DOCTO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad: enviaParametros,
			load: procesarConsultaData_B,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData_B(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var resumenTotalesAData = new Ext.data.JsonStore({
		fields: [
			{name: 'TIPO_MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float'},
			{name: 'TOTAL_MONTO_DOC_INICIAL', type: 'float'},
			{name: 'TOTAL_MONTO_DOC_FINAL', type: 'float'},
			{name: 'TOTAL_CAPITAL_PAGADO', type: 'float'},
			{name: 'TOTAL_INTERESES_PAGADOS', type: 'float'},
			{name: 'TOTAL_PAGADO', type: 'float'},
			{name: 'TOTAL_SALDO_DEL_CREDITO', type: 'float'}
		],
		data:	[
			{'TIPO_MONEDA':'','TOTAL_REGISTROS':0,'TOTAL_MONTO_DOC_INICIAL':0,'TOTAL_MONTO_DOC_FINAL':0,
			'TOTAL_CAPITAL_PAGADO':0,'TOTAL_INTERESES_PAGADOS':0,'TOTAL_PAGADO':0,'TOTAL_SALDO_DEL_CREDITO':0}
		],
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var resumenTotales_BData = new Ext.data.JsonStore({
		fields: [
			{name: 'TIPO_MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float'},
			{name: 'TOTAL_MONTO_FINAN', type: 'float'},
			{name: 'TOTAL_CAPITAL_PAGADO', type: 'float'},
			{name: 'TOTAL_INTERESES_PAGADOS', type: 'float'},
			{name: 'TOTAL_PAGADO', type: 'float'},
			{name: 'TOTAL_SALDO_DEL_CREDITO', type: 'float'}
		],
		data:	[
			{'TIPO_MONEDA':'','TOTAL_REGISTROS':0,'TOTAL_MONTO_FINAN':0,'TOTAL_CAPITAL_PAGADO':0,'TOTAL_INTERESES_PAGADOS':0,'TOTAL_PAGADO':0,'TOTAL_SALDO_DEL_CREDITO':0}
		],
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var gridTotalesA = {
		xtype: 'grid',
		store: resumenTotalesAData,
		id: 'gridTotalesA',
		hidden:	true,
		columns: [
			{
				header: 'TOTALES',
				dataIndex: 'TIPO_MONEDA',
				align: 'left',	width: 150
			},{
				header: 'Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 80,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},{
				header: 'Monto Documento Inicial',
				dataIndex: 'TOTAL_MONTO_DOC_INICIAL',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto Documento Final',
				dataIndex: 'TOTAL_MONTO_DOC_FINAL',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Capital Pagado',
				dataIndex: 'TOTAL_CAPITAL_PAGADO',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Intereses Pagados',
				dataIndex: 'TOTAL_INTERESES_PAGADOS',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Total Pagado',
				dataIndex: 'TOTAL_PAGADO',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Saldo del Cr�dito',
				dataIndex: 'TOTAL_SALDO_DEL_CREDITO',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	var gridTotales_B = {
		xtype: 'grid',
		store: resumenTotales_BData,
		id: 'gridTotales_B',
		hidden:	true,
		columns: [
			{
				header: 'TOTALES',
				dataIndex: 'TIPO_MONEDA',
				align: 'left',	width: 150
			},{
				header: 'Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 80,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},{
				header: 'Monto del Financiamiento',
				dataIndex: 'TOTAL_MONTO_FINAN',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Capital Pagado',
				dataIndex: 'TOTAL_CAPITAL_PAGADO',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Intereses Pagados',
				dataIndex: 'TOTAL_INTERESES_PAGADOS',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Total Pagado',
				dataIndex: 'TOTAL_PAGADO',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Saldo del Cr�dito',
				dataIndex: 'TOTAL_SALDO_DEL_CREDITO',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};


	var gridTotal = {
		xtype: 'grid',
		id: 'gridTotal',
		hidden: true,
		store: totalData,
		columns: [
			{
				header: 'Monto de intereses a pagar',
				dataIndex: 'MONTO_TASA_INT',
				width: 200,
				align: 'center'
			}
		],
		width: 800,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		frame: false
	};
	var gridDetalleDoctos = new Ext.grid.GridPanel({
		id:'gridGeneral',
		store:consultaData,
		hidden: false,
		columns: [
				{
					header: 'N�mero pago',tooltip: 'N�mero pago',dataIndex: 'IC_DOCUMENTO',
					sortable: true,hidden: false,width: 100,align: 'center'
				},{
					header: 'Fecha pago',tooltip: 'Fecha pago',dataIndex: 'FECHA_PAGO',
					sortable: true,hidden: false,width: 110,align: 'center'
				},{
					header: 'Plazo',tooltip: 'Plazo',dataIndex: 'IG_PLAZO_CREDITO',
					sortable: true,hidden:false,width: 120,align: 'center'
				},{
					header: 'Tasa referenciada',tooltip: 'Tasa referenciada',dataIndex: 'TASA_REFERENCIA',
					sortable: true,hidden: false,width: 220,align: 'center'
				},{
					header: 'Tasa de inter�s',tooltip: 'Tasa de inter�s',dataIndex: 'VALOR_TASA_INT',
					sortable: true,hidden: false,	width: 110,align: 'center',
					renderer: Ext.util.Format.numberRenderer('0,0.00')
				},{
					header: 'Monto intereses a pagar',tooltip: 'Monto de intereses a pagar',
					dataIndex: 'MONTO_TASA_INT',
					sortable: true,hidden: false,width: 135,align: 'right',
					renderer: Ext.util.Format.numberRenderer('0,0.00')
				}
			],
			margins: '20 0 0 0',
			stripeRows: true,
			loadMask: true,
			frame: false,
			width: 800,
			height: 200,
			bbar: {
				items: [
					'->',
					'-',
					{
						xtype: 'button',
						text: 'Total monto',
						id: 'btnTotal',
						hidden: false,
						handler: function (boton,evento) {
							var storeGeneral = consultaData;
							var sum=0;
							if(storeGeneral.getTotalCount() > 0) {
								storeGeneral.each(function(registro){
									sum += registro.get('MONTO_TASA_INT');
								});
								var reg = totalData.getAt(0);//Primer registro porque son totales
								reg.set('MONTO_TASA_INT',sum);
							}
							var totalesCmp = Ext.getCmp('gridTotal');
							if (!totalesCmp.isVisible()) {
								totalesCmp.show();
								totalesCmp.el.dom.scrollIntoView();
							}
						}
					},'-',{
						xtype: 'button',
						text: 'Generar Archivo',
						tooltip: 'Imprime los registros en formato CSV.',
						iconCls: 'icoXls',
						id: 'btnGenerarArchivo',
						handler: function(boton,evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							var cmbNumDoc = Ext.getCmp("cmbNumDoc");
							var cboEpo = Ext.getCmp('cboEpo');
							Ext.Ajax.request({
								url: '24consulta02ext.data.jsp',
								params: {
									informacion: 'ArchivoCSV',
									numDoc: claveDocto,
									ic_epo: claveEpo
								},
								callback: procesarSuccessFailureGenerarArchivo
							});
						}
					},
					/*{
						xtype: 'button',
						text: 'BajarArchivo',
						id: 'btnBajarArchivo',
						hidden: true
					},'-',*/{
						xtype: 'button',
						text: 'Generar Todo',
						tooltip: 'Imprime los registros en formato PDF.',
						iconCls: 'icoPdf',
						id: 'btnGenerarPDF',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							var cmbNumDoc = Ext.getCmp("cmbNumDoc");
							var cboEpo = Ext.getCmp('cboEpo');
							Ext.Ajax.request({
								url: '24consulta02ext.data.jsp',
								params: {
									informacion: 'ArchivoPDF',
									numDoc: claveDocto,
									ic_epo: claveEpo
								},
								callback: procesarSuccessFailureGenerarPDF
							});
						}
					}/*,
					{
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDF',
						hidden: true
					},
					'-'*/
				]
			}
	});

	var grid_A = new Ext.grid.GridPanel({
		store: consultaData_A,
		hidden: true,
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'Fecha de operaci�n', tooltip: 'Fecha de operaci�n', dataIndex : 'FECHA_OPERA',
				sortable : true,	width : 110, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'EPO', tooltip: 'EPO', dataIndex: 'EPO',
				sortable: true,	width: 200,	resizable: true, hidden: false
			},{
				header: 'Num. de docto. inicial', tooltip: 'Num. de docto. inicial', dataIndex: 'NUM_DOCTO',
				sortable: true,	width: 150,	resizable: true, hidden: false
			},{
				header : 'Monto docto. inicial', tooltip: 'Monto docto. inicial', dataIndex : 'MONTO_CRED',
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Num. de docto. final', tooltip: 'Num. de docto. final',	dataIndex : 'NUM_CREDITO',
				sortable : true, width : 150, hidden : false
			},{
				header : 'Monto docto. final', tooltip: 'Monto docto. final', dataIndex : 'MONTO_DOC_FINAL',
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Fecha de vencimiento final', tooltip: 'Fecha de vencimiento final',
				dataIndex : 'F_VENC_CREDITO',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Estatus', tooltip: 'Plazo', dataIndex : 'ESTATUS', 
				sortable : true, width : 100, hidden : false, align: 'center'
			},{
				header : 'Intereses generados', tooltip: 'Intereses generados', dataIndex : 'INT_GENERADOS', 
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Capital pagado', tooltip: 'Capital pagado', dataIndex : 'PAGO_A_CAPITAL', 
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Intereses Pagados', tooltip: 'Intereses Pagados', dataIndex : 'PAGO_INT', 
				sortable : true, width : 120, hidden : false, align: 'right',
				renderer: function (value, metaData, registro, rowIndex, colIndex, store)	{
								value = "0";
								if(registro.get('TIPO_COB_INT') == "1")	{
									value = registro.get('INT_GENERADOS');
								}else	{
									if(registro.get('TIPO_COB_INT') == "2")	{
										if (registro.get('IC_ESTATUS_DOCTO') == "11"){
											value = registro.get('INT_GENERADOS');
										}
									}
								}
								return Ext.util.Format.number(value,'$ 0,0.00');
							}
			},{
				header : 'Total Pagado', tooltip: 'Total Pagado', dataIndex : '', 
				sortable : true, width : 120, hidden : false, align: 'right',
				renderer: function (value, metaData, registro, rowIndex, colIndex, store)	{
								var rs_pago_int = "0";
								if(registro.get('TIPO_COB_INT') == "1")	{
									rs_pago_int = registro.get('INT_GENERADOS');
								}else	{
									if(registro.get('TIPO_COB_INT') == "2")	{
										if (registro.get('IC_ESTATUS_DOCTO') == "11"){
											rs_pago_int = registro.get('INT_GENERADOS');
										}
									}
								}
								value = parseFloat(registro.get('PAGO_A_CAPITAL')) + parseFloat(rs_pago_int);
								return Ext.util.Format.number(value,'$ 0,0.00');
							}
			},{
				header : 'Saldo del cr�dito', tooltip: 'Saldo del cr�dito', dataIndex : 'SALDO_CRE',
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Tipo de cobro a intereses', tooltip: 'Tipo de cobro a intereses',
				dataIndex : 'TIPO_COB_INT', sortable : true, width : 150, hidden : false
			},{
				xtype:	'actioncolumn',
				header : 'Detalle de pago de intereses', tooltip: 'Detalle de pago de intereses',
				dataIndex : 'NUM_DOCTO',
				width:	150,	align: 'center',
				/*renderer: function(value, metadata, record, rowindex, colindex, store) {
								return (record.get('NUM_DOCTO'));
							 },*/
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							var auxAcuse = value.substring(1, 2);
							if ((registro.get('NUM_DOCTO'))	!=	"") {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else	{
								return value;
							}
						},
						handler:	procesarPanelDocto
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Res�men estado de adeudos',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						var comboMoneda = Ext.getCmp('cmbMoneda');
						var storeGrid = consultaData_A;
						var sumMontoCre=0;
						var sumMontoFin=0;
						var sumPagoCapital=0;
						var sumPagoInt=0;
						var sumTotalPagado=0;
						var sumSaldoCre=0;
						var count = 0;
						if(storeGrid.getTotalCount() > 0) {
							count = storeGrid.getTotalCount();
							var pago_int = 0;
							var total_pagado = 0;
							storeGrid.each(function(registro){
								pago_int = 0;
								total_pagado = 0;
								if(registro.get('TIPO_COB_INT') == "1")	{
									pago_int = parseFloat(registro.get('INT_GENERADOS'));
								}else	{
									if(registro.get('TIPO_COB_INT') == "2")	{
										if (registro.get('IC_ESTATUS_DOCTO') == "11"){
											pago_int = parseFloat(registro.get('INT_GENERADOS'));
										}
									}
								}
								total_pagado = parseFloat(registro.get('PAGO_A_CAPITAL')) + pago_int;
								sumPagoInt += pago_int;
								sumTotalPagado += total_pagado;
								sumMontoCre += registro.get('MONTO_CRED');
								sumMontoFin += registro.get('MONTO_DOC_FINAL');
								sumPagoCapital += registro.get('PAGO_A_CAPITAL');
								sumSaldoCre += registro.get('SALDO_CRE');
							});
							var reg = resumenTotalesAData.getAt(0);//Primer registro de conteo
							reg.set('TOTAL_REGISTROS',count);
							reg.set('TOTAL_MONTO_DOC_INICIAL',sumMontoCre);
							reg.set('TOTAL_MONTO_DOC_FINAL',sumMontoFin);
							reg.set('TOTAL_CAPITAL_PAGADO',sumPagoCapital);
							reg.set('TOTAL_INTERESES_PAGADOS',sumPagoInt);
							reg.set('TOTAL_PAGADO',sumTotalPagado);
							reg.set('TOTAL_SALDO_DEL_CREDITO',sumSaldoCre);
							if (comboMoneda.getValue() == "1"){
								reg.set('TIPO_MONEDA','MONEDA NACIONAL');
							}else{
								if (comboMoneda.getValue() == "54"){
									reg.set('TIPO_MONEDA','DOLAR AMERICANO');
								}
							}
						}
						var totalesACmp = Ext.getCmp('gridTotalesA');
						if (!totalesACmp.isVisible()) {
							totalesACmp.show();
							totalesACmp.el.dom.scrollIntoView();
						}
					}
				},'-',{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id: 'btnGenArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						// Se le da formato a los campos de las fechas
						var fecha_ini1 = '';
						var fecha_fin1 = '';
						if(Ext.getCmp('fecha_ini').getValue() != '')
							fecha_ini1 = new Date(Ext.getCmp('fecha_ini').getValue()).format('d/m/Y');
						if(Ext.getCmp('fecha_fin').getValue() != '')
							fecha_fin1 = new Date(Ext.getCmp('fecha_fin').getValue()).format('d/m/Y');
						Ext.Ajax.request({
							url: '24consulta06ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								fecha_ini1:  fecha_ini1,
								fecha_fin1:  fecha_fin1,
								tipo:        'CSV'
              }),
							callback: procesarSuccessFailureGenArchivo
						});
					}
				},/*{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajArchivo',
					hidden: true
				},*/'-',{
					xtype: 'button',
					text:    'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id: 'btnGenPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						//Se le da formato a los campos fecha
						var fecha_ini1 = '';
						var fecha_fin1 = '';
						if(Ext.getCmp('fecha_ini').getValue() != '')
							fecha_ini1 = new Date(Ext.getCmp('fecha_ini').getValue()).format('d/m/Y');
						if(Ext.getCmp('fecha_fin').getValue() != '')
							fecha_fin1 = new Date(Ext.getCmp('fecha_fin').getValue()).format('d/m/Y');
						Ext.Ajax.request({
							url: '24consulta06ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								fecha_ini1:  fecha_ini1,
								fecha_fin1:  fecha_fin1,
								tipo:        'PDF'
							}),
							callback: procesarSuccessFailureGenPDF
						});
					}
				}/*,{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajPDF',
					hidden: true
				}*/
			]
		}
	});

	var grid_B = new Ext.grid.GridPanel({
		store: consultaData_B,
		hidden:	true,
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'Fecha de operaci�n', tooltip: 'Fecha de operaci�n', dataIndex : 'FECHA_OPERA',
				sortable : true,	width : 110, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'EPO', tooltip: 'EPO', dataIndex: 'EPO',
				sortable: true,	width: 200,	resizable: true, hidden: false
			},{
				header: 'Num. de Docto.', tooltip: 'Num. de Docto.', dataIndex: 'NUM_DOCTO',
				sortable: true,	width: 150,	resizable: true, hidden: false
			},{
				header : 'Num. de Financiamiento', tooltip: 'Num. de Financiamiento',	dataIndex : 'NUM_CREDITO',
				sortable : true, width : 150, hidden : false
			},{
				header : 'Fecha de vencimiento del financiamiento', tooltip: 'Fecha de vencimiento del financiamiento',
				dataIndex : 'F_VENC_CREDITO',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Monto del Financiamiento', tooltip: 'Monto del Financiamiento', dataIndex : 'MONTO_CRED', 
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Estatus del Financiamiento', tooltip: 'Estatus del Financiamiento', dataIndex : 'ESTATUS', 
				sortable : true, width : 100, hidden : false, align: 'center'
			},{
				header : 'Intereses generados', tooltip: 'Intereses generados', dataIndex : 'INT_GENERADOS', 
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Capital pagado', tooltip: 'Capital pagado', dataIndex : 'PAGO_A_CAPITAL', 
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Intereses Pagados', tooltip: 'Intereses Pagados', dataIndex : 'PAGO_INT', 
				sortable : true, width : 120, hidden : false, align: 'right',
				renderer: function (value, metaData, registro, rowIndex, colIndex, store)	{
								value = "0";
								if(registro.get('TIPO_COB_INT') == "1")	{
									value = registro.get('INT_GENERADOS');
								}else	{
									if(registro.get('TIPO_COB_INT') == "2")	{
										if (registro.get('IC_ESTATUS_DOCTO') == "11"){
											value = registro.get('INT_GENERADOS');
										}
									}
								}
								return Ext.util.Format.number(value,'$ 0,0.00');
							}
			},{
				header : 'Total Pagado', tooltip: 'Total Pagado', dataIndex : '', 
				sortable : true, width : 120, hidden : false, align: 'right',
				renderer: function (value, metaData, registro, rowIndex, colIndex, store)	{
								var rs_pago_int = "0";
								if(registro.get('TIPO_COB_INT') == "1")	{
									rs_pago_int = registro.get('INT_GENERADOS');
								}else	{
									if(registro.get('TIPO_COB_INT') == "2")	{
										if (registro.get('IC_ESTATUS_DOCTO') == "11"){
											rs_pago_int = registro.get('INT_GENERADOS');
										}
									}
								}
								value = parseFloat(registro.get('PAGO_A_CAPITAL')) + parseFloat(rs_pago_int);
								return Ext.util.Format.number(value,'$ 0,0.00');
							}
			},{
				header : 'Saldo del cr�dito', tooltip: 'Saldo del cr�dito', dataIndex : 'SALDO_CRE',
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Tipo de cobro a intereses', tooltip: 'Tipo de cobro a intereses',
				dataIndex : 'TIPO_COB_INT', sortable : true, width : 200, hidden : false
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Resumen estado de adeudos',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion_B',
			displayInfo: true,
			store: consultaData_B,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales_B',
					hidden: false,
					handler: function(boton, evento) {
						var comboMoneda = Ext.getCmp('cmbMoneda');
						var storeGrid_B = consultaData_B;
						var sumMontoCre=0;
						var sumPagoCapital=0;
						var sumPagoInt=0;
						var sumTotalPagado=0;
						var sumSaldoCre=0;
						var count = 0;
						if(storeGrid_B.getTotalCount() > 0) {
							count = storeGrid_B.getTotalCount();
							var pago_int = 0;
							var total_pagado = 0;
							storeGrid_B.each(function(registro){
								pago_int = 0;
								total_pagado = 0;
								if(registro.get('TIPO_COB_INT') == "1")	{
									pago_int = parseFloat(registro.get('INT_GENERADOS'));
								}else	{
									if(registro.get('TIPO_COB_INT') == "2")	{
										if (registro.get('IC_ESTATUS_DOCTO') == "11"){
											pago_int = parseFloat(registro.get('INT_GENERADOS'));
										}
									}
								}
								total_pagado = parseFloat(registro.get('PAGO_A_CAPITAL')) + pago_int;
								sumPagoInt += pago_int;
								sumTotalPagado += total_pagado;
								sumMontoCre += registro.get('MONTO_CRED');
								sumPagoCapital += registro.get('PAGO_A_CAPITAL');
								sumSaldoCre += registro.get('SALDO_CRE');
							});
							var reg = resumenTotales_BData.getAt(0);//Primer registro de conteo
							reg.set('TOTAL_REGISTROS',count);
							reg.set('TOTAL_MONTO_FINAN',sumMontoCre);
							reg.set('TOTAL_CAPITAL_PAGADO',sumPagoCapital);
							reg.set('TOTAL_INTERESES_PAGADOS',sumPagoInt);
							reg.set('TOTAL_PAGADO',sumTotalPagado);
							reg.set('TOTAL_SALDO_DEL_CREDITO',sumSaldoCre);
							if (comboMoneda.getValue() == "1"){
								reg.set('TIPO_MONEDA','MONEDA NACIONAL');
							}else{
								if (comboMoneda.getValue() == "54"){
									reg.set('TIPO_MONEDA','DOLAR AMERICANO');
								}
							}
						}
						var totalesBCmp = Ext.getCmp('gridTotales_B');
						if (!totalesBCmp.isVisible()) {
							totalesBCmp.show();
							totalesBCmp.el.dom.scrollIntoView();
						}
					}
				},'-',{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id: 'btnGenArchivo_B',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						//Se le da formato a los campos fecha
						var fecha_ini1 = '';
						var fecha_fin1 = '';
						if(Ext.getCmp('fecha_ini').getValue() != '')
							fecha_ini1 = new Date(Ext.getCmp('fecha_ini').getValue()).format('d/m/Y');
						if(Ext.getCmp('fecha_fin').getValue() != '')
							fecha_fin1 = new Date(Ext.getCmp('fecha_fin').getValue()).format('d/m/Y');
						Ext.Ajax.request({
							url: '24consulta06ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV_B',
								fecha_ini1:  fecha_ini1,
								fecha_fin1:  fecha_fin1,
								tipo:        'CSV'
              }),
							callback: procesarSuccessFailureGenArchivo_B
						});
					}
				},/*{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajArchivo_B',
					hidden: true
				},*/'-',{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id: 'btnGenPDF_B',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion_B");
						//Se le da formato a los campos fecha
						var fecha_ini1 = '';
						var fecha_fin1 = '';
						if(Ext.getCmp('fecha_ini').getValue() != '')
							fecha_ini1 = new Date(Ext.getCmp('fecha_ini').getValue()).format('d/m/Y');
						if(Ext.getCmp('fecha_fin').getValue() != '')
							fecha_fin1 = new Date(Ext.getCmp('fecha_fin').getValue()).format('d/m/Y');
						Ext.Ajax.request({
							url: '24consulta06ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF_B',
							fecha_ini1: fecha_ini1,
							fecha_fin1: fecha_fin1,
							tipo: 'PDF',
							start: cmpBarraPaginacion.cursor,
							limit: cmpBarraPaginacion.pageSize
						}),
							callback: procesarSuccessFailureGenPDF_B
						});
					}
				}/*,{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajPDF_B',
					hidden: true
				}*/
			]
		}
	});

	var fpInfo = new Ext.form.FormPanel({
		id: 'formaInfo',
		width: 600,
		frame: true,
		border: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		labelWidth: 130,
		hidden: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:	[{xtype: 'displayfield', id: 'dis_cliente', fieldLabel: 'Cliente',	name: 'disCliente', hidden: true,	value: '' },
					{xtype: 'displayfield', id: 'dis_banco', fieldLabel: 'Banco de Referencia', 	name: 'disBanco', hidden: true,	value: ''}],
		monitorValid: true
	});

	var fp1=new Ext.FormPanel({
		height:50,
		width: 800,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		labelWidth: 130,
		items: [
		{
        xtype: 'label',
		  id: 'lblNumDocto',
        fieldLabel: 'N�mero Documento',
		  text: '-'
		  },{
        xtype: 'label',
		  id: 'lblFechaPago',
        fieldLabel: 'Fecha de operaci�n',
		  text: '-'
		  }
		]
	});

	var elementosForma = [
					{
						xtype: 'combo',
						name: 'ic_epo',
						id: 'cmbEpo',
						allowBlank: false,
						fieldLabel: 'Nombre EPO',
						mode: 'local',
						displayField : 'descripcion',
						valueField : 'clave',
						hiddenName : 'ic_epo',
						emptyText: 'Todas las EPO�s',
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store : catalogoEPOData,
						tpl : NE.util.templateMensajeCargaCombo,
						listeners: {
							select: {
								fn: function(combo) {
									var ventanaGridDetalle = Ext.getCmp('verNumDocto');
									if (ventanaGridDetalle) {
										ventanaGridDetalle.destroy();
									}
									var totalesACmp = Ext.getCmp('gridTotalesA');
									if (totalesACmp.isVisible()) {
										totalesACmp.hide();
									}
									var totalesCmpB = Ext.getCmp('gridTotales_B');
									if (totalesCmpB.isVisible()) {
										totalesCmpB.hide();
									}
									fpInfo.hide();
									grid_A.hide();
									grid_B.hide();
									var comboIF = Ext.getCmp('cmbIF');
									comboIF.setValue('');
									comboIF.store.removeAll();
									comboIF.setDisabled(false);
									comboIF.store.load({
											params: {
												ic_epo: combo.getValue()
											}
									});
								}
							}
						}
					},{
						xtype: 'combo',
						name: 'ic_if',
						id:	'cmbIF',
						allowBlank: false,
						fieldLabel: 'Nombre IF',
						emptyText: 'Seleccione el Intermediario Financiero',
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'ic_if',
						forceSelection : false,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoIfData,
						tpl : NE.util.templateMensajeCargaCombo
					},{
						xtype: 'combo',
						id:	'cmbMoneda',
						name: 'ic_moneda',
						allowBlank: false,
						fieldLabel: 'Moneda',
						emptyText: 'Seleccione Moneda',
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'ic_moneda',
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoMonedaData,
						tpl : NE.util.templateMensajeCargaCombo
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha inicio',	
						combineErrors: false,
						msgTarget: 'side',
						anchor:'90%',
						items: [
							{
								xtype: 'datefield',
								name: 'fecha_inicio',
								id: 'fecha_ini',
								allowBlank: true,
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'fecha_fin',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'Fecha final:',
								width: 70
							},
							{
								xtype: 'datefield',
								name: 'fecha_final',
								id: 'fecha_fin',
								allowBlank: true,
								startDay: 1,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'fecha_ini',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},{
								xtype:'hidden',
								id:'hidCliente',
								name:'disCliente',
								value:''
							},{
								xtype:'hidden',
								id:'hidBanco',
								name:'disBanco', 
								value:''
							}
						]
					}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma_A',
		width: 600,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		title:	'<div><center>Criterios de B�squeda</div>',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var fecha_inicio = Ext.getCmp('fecha_ini');
					var fecha_final = Ext.getCmp('fecha_fin');
					grid_A.hide();
					grid_B.hide();
					fpInfo.hide();
					var ventanaGridDetalle = Ext.getCmp('verNumDocto');
					if (ventanaGridDetalle) {
						ventanaGridDetalle.destroy();
					}
					var totalesACmp = Ext.getCmp('gridTotalesA');
					if (totalesACmp.isVisible()) {
						totalesACmp.hide();
					}
					var totalesCmpB = Ext.getCmp('gridTotales_B');
					if (totalesCmpB.isVisible()) {
						totalesCmpB.hide();
					}
					if(!Ext.isEmpty(fecha_inicio.getValue())||!Ext.isEmpty(fecha_final.getValue())){
						if(Ext.isEmpty(fecha_inicio.getValue())){
							fecha_inicio.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
							fecha_inicio.focus();
							return;
						}else if (Ext.isEmpty(fecha_final.getValue())){
							fecha_final.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
							fecha_final.focus();
							return;
						}
					}
					fp.el.mask('Enviando...', 'x-mask-loading');
					var combo_if = Ext.getCmp('cmbIF');
					Ext.Ajax.request({
						url: '24consulta06ext.data.jsp',
						params:	{ic_if:	combo_if.getValue(),
									informacion: 'valoresBusqueda'},
						callback: procesaValoresBusqueda
					});
				} //fin handler
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24consulta06ext.jsp';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,		NE.util.getEspaciador(10),
			fpInfo,	NE.util.getEspaciador(10),
			grid_A,	gridTotalesA,
			grid_B,	gridTotales_B
		]
	});

//	Carga datos de los combos...

	catalogoEPOData.load();
	catalogoMonedaData.load();

});