Ext.onReady(function() {

//_--------------------------------- HANDLERS -------------------------------
/*	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGeneraPDF = Ext.getCmp('btnGenerarPDF');
		btnGeneraPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajaPDF = Ext.getCmp('btnBajarPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			//var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnTotal = Ext.getCmp('btnTotal');
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				btnGenerarPDF.enable();
				//btnBajarPDF.hide();
				btnTotal.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				btnTotal.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
//-------------------------------- STORES -----------------------------------
	var gridImpresionesData = new Ext.data.JsonStore({
		fields:	[{name: 'CD_DOCUMENTO'},{name: 'CG_NOMBRE_ARCHIVO'}],
		data:		[{'CD_DOCUMENTO':''},{'CG_NOMBRE_ARCHIVO':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var solicData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['I','Inicial'],
			['A','Ampliaci�n'],
			['R','Renovaci�n']
		 ]
	});

//CATALOGO EPO
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24solicitud04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//IF
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24solicitud04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
//ESTATUS
	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24solicitud04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});	

//MONEDA
	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24solicitud04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24solicitud04ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},	
		fields: [
			{name: 'IC_LINEA_CREDITO'}, 
			{name: 'CG_TIPO_SOLICITUD'}, 
			{name: 'TIPO_SOLICITUD'},                    
			{name: 'NOMBRE_IF'}, 
			{name: 'MONEDA'},                    //Moneda
			{name: 'FECHA_CAPTURA', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'ESTATUS'},
			{name: 'TIPO_COBRO'},
			{name: 'FN_MONTO_AUTORIZADO', type: 'float'},
			{name: 'FECHA_VEN',  type: 'date', dateFormat: 'd/m/Y'},			
			{name: 'BANCO_SERVICIO'},
			{name: 'CG_NUMERO_CUENTA'},
			{name: 'CS_ACEPTACION_PYME'},
			{name: 'CG_NUMERO_CUENTA_IF'},
			{name: 'CG_CAUSA_RECHAZO'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});
	
	//Store para totales
	var totalData = new Ext.data.JsonStore({
		fields: [
			{name: 'TOTAL_DOCUMENTOS'},
			{name: 'REGISTROS',type: 'float'},
			{name: 'MONTO_AUTORIZADO',type: 'float'}
		],
		data: [{'TOTAL_DOCUMENTOS':'MONEDA NACIONAL', 'REGISTROS':0, 'MONTO_AUTORIZADO':0},{'TOTAL_DOCUMENTOS':'DOLAR AMERICANO', 'REGISTROS':0, 'MONTO_AUTORIZADO':0}],
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var misTotales = [{'TOTAL_DOCUMENTOS':'MONEDA NACIONAL', 'REGISTROS':0, 'MONTO_AUTORIZADO':0},{'TOTAL_DOCUMENTOS':'DOLAR AMERICANO', 'REGISTROS':0, 'MONTO_AUTORIZADO':0}];

	function verificaPanel(idPanel){
		var myPanel = Ext.getCmp(idPanel);
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype != 'label')	{	//El metodo isValid() no aplica para los tipos de objetos label.
				if (!panelItem.isValid())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}

	var elementosForma = [
		{
			xtype: 'container',
			layout:'table',
			defaults:	{bodyStyle: 'padding: 5px'},
			layoutConfig:	{columns: 3},
				items:[{
					xtype: 'panel',id:	'izqForma',
					colspan: 1,
					width:	120,
					layout: 'form',
					border:false
				},{
					xtype: 'panel',	id:	'centroForma',
					width:	700,
					title :	'<div><center>Criterios de B�squeda</div>',
					collapsible: true,
					titleCollapse: false,
					colspan: 1,
					layout: 'form',
					defaults: {
						msgTarget: 'side',
						anchor: '-20'
					},
					frame: true,
					border: true,
					items: [
						{
							xtype: 'combo',
							name: 'cmbEpo',
							id: 'cmbEpo1',
							fieldLabel: 'Empresa de primer orden',
							mode: 'local', 
							displayField : 'descripcion',
							valueField : 'clave',
							hiddenName : 'cmbEpo',
							emptyText: 'Seleccione una EPO',
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store : catalogoEPOData,
							tpl : NE.util.templateMensajeCargaCombo,
							listeners: {
								select: {
									fn: function(combo) {
										grid.hide();
										var totalesCmp = Ext.getCmp('gridTotales');
										if (totalesCmp.isVisible()) {
											totalesCmp.hide();
										}
										var cmboIf = Ext.getCmp('cmboIf1');
										cmboIf.setValue('');
										cmboIf.setDisabled(false);
										cmboIf.store.reload({
												params: {
													claveEpo: combo.getValue()	
												}
										});
									}
								}
							}
						},{
							xtype: 'combo',
							name: 'cmboIF',
							id: 'cmboIf1',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							emptyText: 'Seleccione un IF',
							hiddenName : 'cmboIF',
							fieldLabel: 'Intermediario Financiero',
							forceSelection : false,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoIFData,
							tpl : NE.util.templateMensajeCargaCombo
						},
						{
							xtype: 'combo',
							name: 'cmbTSolic',
							id: 'cmbTSolic1',
							mode: 'local',
							emptyText: 'Todos los tipos de solicitudes...',
							hiddenName : 'cmbTSolic',
							fieldLabel: 'Tipo de solicitud',
							disabled: false,
							forceSelection : false,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							valueField: 'clave',
							displayField: 'descripcion',
							store: solicData
						},{
							xtype: 'combo',
							name: 'cmboestatus',
							id: 'cmboestatus1',
							fieldLabel: 'Estatus',
							emptyText: 'Todos los estatus...',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'cmboestatus',
							forceSelection : false,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoEstatusData,
							tpl : NE.util.templateMensajeCargaCombo
						},{
							xtype: 'combo',
							name: 'cmboMoneda',
							id: 'cmboMoneda1',
							fieldLabel: 'Moneda',
							emptyText: 'Seleccione una moneda...',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'cmboMoneda',
							forceSelection : false,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoMonedaData,
							tpl : NE.util.templateMensajeCargaCombo
						},{
							xtype: 'datefield',
							fieldLabel: 'Solicitudes de cr�dito al', // (dd/mm/yyyy)
							name: 'fecha_sol',
							id: 'fecha_solic',
							maxlenght: 10,
							allowBlank: false,
							startDay: 0,
							width: 100,
							msgTarget: 'side',
							margins: '0 20 0 0',
							anchor: '39%',
							vtype: 'rangofecha',
							campoInicioFecha: 'fecha_solic',
							value:	(new Date().format('d/m/Y'))
						}
					],
					buttons: [
						{
							text: 'Consultar',
							iconCls: 'icoBuscar',
							handler: function(boton,evento) {
										grid.hide();
										var totalesCmp = Ext.getCmp('gridTotales');
										if (totalesCmp.isVisible()) {
											totalesCmp.hide();
										}
										var fecha_solic = Ext.getCmp('fecha_solic');
										var dt = new Date();
										res = datecomp(fecha_solic.value,(dt.format('d/m/Y')));
										if(res==1 || res == -1){
											fecha_solic.markInvalid("La fecha de Solicitud no debe de ser mayor al dia de hoy");
											return;
										}
										if(!verificaPanel('centroForma')) {
											return;
										}
										fp.el.mask('Enviando...','x-msk-loading');
										consultaData.load({
											params: Ext.apply(fp.getForm().getValues(), {
												operacion: 'Generar',
												start: 0,
												limit: 15
											})
										});
										Ext.getCmp('btnTotal').disable();
							}
						},
						{
							text: 'Limpiar',
							iconCls: 'icoLimpiar',
							handler: function() {
								fp.getForm().reset();
								grid.hide();
								var totalesCmp = Ext.getCmp('gridTotales');
								if (totalesCmp.isVisible()) {
									totalesCmp.hide();
								}
							}
						}
					]
				},{
						xtype: 'panel',id:	'derForma',
						colspan: 1,
						layout: 'form',
						border:false
					}
			]
		}
	];

	//grid Totales
	var gridTotales = {
		xtype: 'grid',
		id: 'gridTotales',
		store: totalData,
		hidden: true,
		columns: [
			{
				header: '',
				dataIndex: 'TOTAL_DOCUMENTOS',
				width: 350,	resizable: false,align: 'center'
			},
			{
				header: 'Registros',
				dataIndex: 'REGISTROS',
				width: 260,	resizable: false,align: 'center'
			},
			{
				header: 'Monto Autorizado',
				dataIndex: 'MONTO_AUTORIZADO',
				width: 270, resizable: false,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	
		],
		view: new Ext.grid.GridView({markDirty: false}),
		height: 100,
		width:940,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	//grid general
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header: 'Tipo Solicitud', tooltip: 'Tipo Solicitud',
				dataIndex: 'TIPO_SOLICITUD',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},{
				header : 'Nombre IF', tooltip: 'Nombre IF',		
				dataIndex : 'NOMBRE_IF',
				width : 250,sortable : true, hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store){
								if (record.get("NOMBRE_IF") == ''){
									return 'N/A';
								} else {
									return value;
								}
							}
			},{
				header : 'Moneda', tooltip: 'Moneda',
				dataIndex : 'MONEDA',width : 130,sortable : true, 	hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store){
								if (record.get("MONEDA") == ''){
									return 'N/A';
								} else {
									return value;
								}
							}
			},{
				header : 'Fecha de Captura', tooltip: 'Fecha de Captura',		
				dataIndex : 'FECHA_CAPTURA',width : 110,sortable : true, hidden: false,
				renderer:	Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Estatus', tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				width : 130,sortable : true, hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store){
								if (record.get("ESTATUS") == ''){
									return 'N/A';
								} else {
									return value;
								}
							}
			},{
				header: 'Tipo de Cobro de interes', ooltip: 'Tipo de Cobro de interes',
				dataIndex : 'TIPO_COBRO',
				width : 150,sortable : true, hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store){
								if (record.get("TIPO_COBRO") == ''){
									return 'N/A';
								} else {
									return value;
								}
							}
			},{
				header : 'Monto Autorizado', tooltip: 'Monto Autorizado',
				dataIndex : 'FN_MONTO_AUTORIZADO',
				sortable : true,width : 120,align: 'right',hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		
				dataIndex : 'FECHA_VEN',
				width : 130,sortable : true, hidden: false,
				renderer:	Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Banco de Servicio', tooltip: 'Banco de Servicio',
				dataIndex : 'BANCO_SERVICIO',
				sortable : true,width : 200,hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store){
								if (record.get("BANCO_SERVICIO") == ''){
									return 'N/A';
								} else {
									return value;
								}
							}
			},{
				header : 'Cuenta Propia Num. Cuenta', tooltip: 'Cuenta Propia Num. Cuenta',
				dataIndex : 'CG_NUMERO_CUENTA',
				sortable : true,width : 150,hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store){
								if (record.get("CG_NUMERO_CUENTA") == ''){
									return 'N/A';
								} else {
									return value;
								}
							}
			},{
				header : 'IF Num. Cuenta', tooltip: 'IF Num. Cuenta',
				dataIndex : 'CS_ACEPTACION_PYME',
				sortable : true,width : 150,hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store){
								if (record.get("CS_ACEPTACION_PYME") == ''){
									return 'N/A';
								} else {
									if (record.get("CG_NUMERO_CUENTA") == ''){
										return 'N/A';
									} else {
										return value;
									}
								}
							}
			},{
				header : 'Causa de Rechazo', tooltip: 'Causa de Rechazo',
				dataIndex : 'CG_CAUSA_RECHAZO',
				sortable : true,width : 150,hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store){
								if (record.get("CG_CAUSA_RECHAZO") == ''){
									return 'N/A';
								} else {
									return value;
								}
							}
			}
		],
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		frame: true,
		width: 940,
		height: 400,
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotal',
					hidden: false,
					handler: function (boton,evento) {
						var storeGeneral = consultaData;
						var sumMN=0;
						var countMN=0;
						var sumDL=0;
						var countDL=0;
						if(storeGeneral.getTotalCount() > 0) {
							totalData.loadData(misTotales);
							storeGeneral.each(function(registro,items,xx,yy){
								if (registro.get('IC_MONEDA') == "1"){
									sumMN += registro.get('FN_MONTO_AUTORIZADO');
									countMN++;
								}
								if (registro.get('IC_MONEDA') == "54"){
									sumDL += registro.get('FN_MONTO_AUTORIZADO');
									countDL++;
								}
							});
							var regMN = totalData.getAt(0);//Primer registro porque son totales
							if (countMN > 0){
								regMN.set('MONTO_AUTORIZADO',sumMN);
								regMN.set('REGISTROS',countMN);
							}else{
								totalData.remove(totalData.getAt(0));
							}
							
							var regDL = totalData.getAt(1);//Primer registro porque son totales
							if (countDL > 0){
								regDL.set('MONTO_AUTORIZADO',sumDL);
								regDL.set('REGISTROS',countDL);
							}else{
								totalData.remove(totalData.getAt(1));
							}
						}
						var totalesCmp = Ext.getCmp('gridTotales');
						if (!totalesCmp.isVisible()) {
							totalesCmp.show();
							totalesCmp.el.dom.scrollIntoView();
						}
					}
				},'-',
				{
					xtype: 'button',
					text:    'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24solicitud04ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{informacion: 'ArchivoPDF',tipo: 'PDF'}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},/*
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},*/
				'-'
			]
		}
	});	

	var fp = new Ext.form.FormPanel({
		id :	'forma',
		width :	949,
		frame :	false,
		border: false,
		style: 'margin:0 auto;',
		bodyStyle : 'padding: 6px',
		labelWidth : 170,
		defaultType : 'textfield',
		defaults : {
			msgTarget :	'side',
			anchor : '-20'
		},
		items: elementosForma
	});

	var pnl = new Ext.Container({
		id :	'contenedorPrincipal',
		applyTo :	'areaContenido',
		width : 949,
		height : 'auto',
		items :	[
			fp,NE.util.getEspaciador(10),
			grid,gridTotales
		]		
	});

	catalogoEPOData.load();
	catalogoIFData.load();
	catalogoEstatusData.load();
	catalogoMonedaData.load();
});