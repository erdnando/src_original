	
	Ext.onReady(function() {
	

		//_--------------------------------- HANDLERS -------------------------------	
		
	//Seleccionado Pyme a Rechazado  IF
	var procesarConsultaGeneralData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
				fp.el.unmask();
		var estatus = Ext.getCmp("cmbEstatus").getValue();
		var nombreGrid = "";
		if(estatus==23){
			nombreGrid = "En Proceso de Autorizacion IF a Negociable";
		}else if(estatus==34){
			nombreGrid = "En Proceso de Autorizacion IF a Seleccionado Pyme";
		}
		Ext.getCmp('gridGeneral').setTitle(nombreGrid);
		if(estatus==14 ||estatus==23 ||estatus==34){
				grid02.hide();
				grid22.hide();
				grid24.hide();
				grid21.hide();
		}
		 
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
				
				gridGeneral.show();								
			}		
			var el = gridGeneral.getGridEl();
				
			if(store.getTotalCount() > 0) {			
				el.unmask();
					Ext.getCmp('btnTotales14').enable();	
					Ext.getCmp('btnGenerarArchivo14').enable();
					Ext.getCmp('btnGenerarPDF14').enable();
					Ext.getCmp('btnBajarCSV14').hide();
					Ext.getCmp('btnBajarPDF14').hide();
					
			} else {							
					Ext.getCmp('btnTotales14').disable();	
					Ext.getCmp('btnGenerarArchivo14').disable();
					Ext.getCmp('btnGenerarPDF14').disable();
					Ext.getCmp('btnBajarCSV14').hide();
					Ext.getCmp('btnBajarPDF14').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}		
		}
	}
	//Seleccionado Pyme Negociable
	var procesarConsulta02Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
			fp.el.unmask();
			var estatus = Ext.getCmp("cmbEstatus").getValue();
		 
		 	if(estatus==2){
				gridGeneral.hide();
				grid22.hide();
				grid24.hide();
				grid21.hide();
			}
		
			if (arrRegistros != null) {
			
			if (!grid02.isVisible()) {
				grid02.show();				
			}		
			var el = grid02.getGridEl();				
			if(store.getTotalCount() > 0) {				
				el.unmask();
				Ext.getCmp('btnTotales2').enable();
				Ext.getCmp('btnGenerarArchivo02').enable();
				Ext.getCmp('btnGenerarPDF02').enable();
				Ext.getCmp('btnBajarCSV02').hide();
				Ext.getCmp('btnBajarPDF02').hide();
			} else {							 
					Ext.getCmp('btnTotales2').disable();	
					Ext.getCmp('btnGenerarArchivo02').disable();
					Ext.getCmp('btnGenerarPDF02').disable();
					Ext.getCmp('btnBajarCSV02').hide();
					Ext.getCmp('btnBajarPDF02').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}				
		}
	}
	
	
	// Negociable a vencido Sin Operar
	var procesarConsulta22Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
			fp.el.unmask();
		var estatus = Ext.getCmp("cmbEstatus").getValue();
		
			if(estatus==22){
				gridGeneral.hide();
				grid02.hide();
				grid24.hide();
				grid21.hide();
			}
			
		if (arrRegistros != null) {
			
			if (!grid22.isVisible()) {
				grid22.show();				
			}		
			var el = grid22.getGridEl();				
			
			if(store.getTotalCount() > 0) {
					el.unmask();
					Ext.getCmp('btnTotales22').enable();				
					Ext.getCmp('btnGenerarArchivo22').enable();
					Ext.getCmp('btnGenerarPDF22').enable();
					Ext.getCmp('btnBajarCSV22').hide();
					Ext.getCmp('btnBajarPDF22').hide();
								
			} else {							
					Ext.getCmp('btnTotales22').disable();
					Ext.getCmp('btnGenerarArchivo22').disable();
					Ext.getCmp('btnGenerarPDF22').disable();
					Ext.getCmp('btnBajarCSV22').hide();
					Ext.getCmp('btnBajarPDF22').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}			
		}
	}
		
		//Seleccionado Pyme a Rechazado  IF
	var procesarConsulta24Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
			fp.el.unmask();
		var estatus = Ext.getCmp("cmbEstatus").getValue();
			if(estatus==24){
				gridGeneral.hide();
				grid02.hide();
				grid22.hide();
				grid21.hide();
			}
			
		if (arrRegistros != null) {
					
			if (!grid24.isVisible()) {
				grid24.show();				
			}		
			var el = grid24.getGridEl();
				
			if(store.getTotalCount() > 0) {
					el.unmask();
					Ext.getCmp('btnTotales24').enable();					
					Ext.getCmp('btnGenerarArchivo24').enable();
					Ext.getCmp('btnGenerarPDF24').enable();
					Ext.getCmp('btnBajarCSV24').hide();
					Ext.getCmp('btnBajarPDF24').hide();
					
			}else {							
					Ext.getCmp('btnTotales24').disable();	
					Ext.getCmp('btnGenerarArchivo24').disable();
					Ext.getCmp('btnGenerarPDF24').disable();
					Ext.getCmp('btnBajarCSV24').hide();
					Ext.getCmp('btnBajarPDF24').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}			
		}
	}
		
	//Modificacion
	var procesarConsulta21Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
			fp.el.unmask();
		var estatus = Ext.getCmp("cmbEstatus").getValue();
				
			if(estatus==21){
				gridGeneral.hide();
				grid02.hide();
				grid22.hide();
				grid24.hide();
			}
			
		if (arrRegistros != null) {
			if (!grid21.isVisible()) {
				grid21.show();				
			}		
			var el = grid21.getGridEl();
				
			if(store.getTotalCount() > 0) {
					el.unmask();
					Ext.getCmp('btnTotales21').enable();	
					Ext.getCmp('btnGenerarArchivo21').enable();
					Ext.getCmp('btnGenerarPDF21').enable();
					Ext.getCmp('btnBajarCSV21').hide();
					Ext.getCmp('btnBajarPDF21').hide();
								
			} else {							
					Ext.getCmp('btnTotales21').disable();
					Ext.getCmp('btnGenerarArchivo21').disable();
					Ext.getCmp('btnGenerarPDF21').disable();
					Ext.getCmp('btnBajarCSV21').hide();
					Ext.getCmp('btnBajarPDF21').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}	
			
		}
	}	
	
	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailureGenerarArchivoCSV14 =  function(opts, success, response) {
		var btnGenerarArchivo14 = Ext.getCmp('btnGenerarArchivo14');
		btnGenerarArchivo14.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV14 = Ext.getCmp('btnBajarCSV14');
			btnBajarCSV14.show();
			btnBajarCSV14.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV14.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarArchivo14.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF14 =  function(opts, success, response) {
		var btnGenerarPDF14 = Ext.getCmp('btnGenerarPDF14');
			btnGenerarPDF14.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF14 = Ext.getCmp('btnBajarPDF14');
			btnBajarPDF14.show();
			btnBajarPDF14.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF14.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF14.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailureGenerarArchivoCSV02 =  function(opts, success, response) {
		var btnGenerarArchivo02 = Ext.getCmp('btnGenerarArchivo02');
		btnGenerarArchivo02.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV02 = Ext.getCmp('btnBajarCSV02');
			btnBajarCSV02.show();
			btnBajarCSV02.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV02.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarArchivo02.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF02 =  function(opts, success, response) {
		var btnGenerarPDF02 = Ext.getCmp('btnGenerarPDF02');
			btnGenerarPDF02.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF02 = Ext.getCmp('btnBajarPDF02');
			btnBajarPDF02.show();
			btnBajarPDF02.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF02.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF02.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
		//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailureGenerarArchivoCSV21 =  function(opts, success, response) {
		var btnGenerarArchivo21 = Ext.getCmp('btnGenerarArchivo21');
		btnGenerarArchivo21.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV21 = Ext.getCmp('btnBajarCSV21');
			btnBajarCSV21.show();
			btnBajarCSV21.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV21.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarArchivo21.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF21 =  function(opts, success, response) {
		var btnGenerarPDF21 = Ext.getCmp('btnGenerarPDF21');
			btnGenerarPDF21.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF21 = Ext.getCmp('btnBajarPDF21');
			btnBajarPDF21.show();
			btnBajarPDF21.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF21.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF21.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailureGenerarArchivoCSV22 =  function(opts, success, response) {
		var btnGenerarArchivo22 = Ext.getCmp('btnGenerarArchivo22');
		btnGenerarArchivo22.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV22 = Ext.getCmp('btnBajarCSV22');
			btnBajarCSV22.show();
			btnBajarCSV22.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV22.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarArchivo22.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF22 =  function(opts, success, response) {
		var btnGenerarPDF22 = Ext.getCmp('btnGenerarPDF22');
			btnGenerarPDF22.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF22 = Ext.getCmp('btnBajarPDF22');
			btnBajarPDF22.show();
			btnBajarPDF22.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF22.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF22.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
			//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailureGenerarArchivoCSV24 =  function(opts, success, response) {
		var btnGenerarArchivo24 = Ext.getCmp('btnGenerarArchivo24');
		btnGenerarArchivo24.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV24 = Ext.getCmp('btnBajarCSV24');
			btnBajarCSV24.show();
			btnBajarCSV24.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV24.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarArchivo24.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF24 =  function(opts, success, response) {
		var btnGenerarPDF24 = Ext.getCmp('btnGenerarPDF24');
			btnGenerarPDF24.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF24 = Ext.getCmp('btnBajarPDF24');
			btnBajarPDF24.show();
			btnBajarPDF24.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF24.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF24.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
		//-------------------------------- STORES -----------------------------------

	var catalogoEstatusData = new Ext.data.JsonStore({
			id: 'catalogoEstatusDataStore',
			root : 'registros',
			fields : ['clave', 'descripcion', 'loadMsg'],
			url : '24reporte02ext.data.jsp',
			baseParams: {
				informacion: 'catalogoEstatus'				
			},
			totalProperty : 'total',
			autoLoad: false,		
			listeners: {
				exception: NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});
			
			///Totales de los Grid
			
		var totales14Data = new Ext.data.JsonStore({			
		root : 'totales14',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'totales14',
			ic_cambio_estatus: 23
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'},
			{name: 'MONTO_CREDITO', type: 'float', mapping: 'MONTO_CREDITO'}
	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	var totales142Data = new Ext.data.JsonStore({			
		root : 'totales14',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'totales14',
			ic_cambio_estatus: 34
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'},
			{name: 'MONTO_CREDITO', type: 'float', mapping: 'MONTO_CREDITO'}
	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
		//var gridTotales14 = new Ext.grid.GridPanel({
		var gridTotales14 = {
		xtype: 'grid',
		store: totales14Data,
		id: 'gridTotales14',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto Cr�dito',
				dataIndex: 'MONTO_CREDITO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		height: 200,
		title: '',
		frame: false
	};
	var gridTotales142 = {
		xtype: 'grid',
		store: totales142Data,
		id: 'gridTotales142',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto Cr�dito',
				dataIndex: 'MONTO_CREDITO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		height: 200,
		title: '',
		frame: false
	};
	
			
			
	var totales2Data = new Ext.data.JsonStore({			
		root : 'totales2',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'totales2'			
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	//var gridTotales2 = new Ext.grid.GridPanel({
		var gridTotales2 = {
		xtype: 'grid',
		store: totales2Data,
		id: 'gridTotales2',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 200,
		title: '',
		frame: false
	};
	
			
	var totales21Data = new Ext.data.JsonStore({			
		root : 'totales21',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'totales21'			
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	//var gridTotales21 = new Ext.grid.GridPanel({
	var gridTotales21 = {
		xtype: 'grid',
		store: totales21Data,
		id: 'gridTotales21',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 200,
		title: '',
		frame: false
	};
	
		var totales22Data = new Ext.data.JsonStore({			
		root : 'totales22',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'totales22'			
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	//var gridTotales22 = new Ext.grid.GridPanel({
	var gridTotales22 = {
		xtype: 'grid',
		store: totales22Data,
		id: 'gridTotales22',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 200,
		title: '',
		frame: false
	};
			
		var totales24Data = new Ext.data.JsonStore({			
		root : 'totales24',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'totales24'			
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	
	var gridTotales24 = {
		xtype: 'grid',		
		store: totales24Data,
		id: 'gridTotales24',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 200,
		title: '',
		frame: false
	};
	
		//Seleccionado Pyme a Rechazado IF
		var consultaGeneralData = new Ext.data.JsonStore({
			root : 'registros14',
			url : '24reporte02ext.data.jsp',
			baseParams: {
				informacion: 'ConsultaGeneral'
			},
			fields: [
				{name: 'NOMBRE_EPO'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_CARGA', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'}, 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'},  
				{name: 'MONTO_PORC_DESCUENTO'}, 		
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},
				{name: 'MONT_VALUADO', type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'IC_DOCUMENTO'},	
				{name: 'MONTO_CREDITO', type: 'float'},
				{name: 'IG_PLAZO_CREDITO'},					
				{name: 'FECHA_VENC_CREDITO', type: 'date', dateFormat: 'd/m/Y'},					
				{name: 'FECHA_SELECCION', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'NOMBRE_IF'},
				{name: 'REFERENCIA_INT'},
				{name: 'VALOR_TASA_INT'},
				{name: 'MONTO_TASA_INT', type: 'float'},
				{name: 'MONTOTOTAL_TASA_INT', type: 'float'},
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'CAUSA'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaGeneralData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaGeneralData(null, null, null);						
					}
				}
			}
		});
		
		//Seleccionado Pyme a Negociable
		var consulta02Data = new Ext.data.JsonStore({
			root : 'registros2',
			url : '24reporte02ext.data.jsp',
			baseParams: {
				informacion: 'Consulta02'
			},
			fields: [
				{name: 'NOMBRE_EPO'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_CARGA', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'}, 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'},  
				{name: 'MONTO_PORC_DESCUENTO'}, 		
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},
				{name: 'MONT_VALUADO', type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'IC_DOCUMENTO'},	
				{name: 'MONTO_CREDITO', type: 'float'},
				{name: 'IG_PLAZO_CREDITO'},					
				{name: 'FECHA_VENC_CREDITO', type: 'date', dateFormat: 'd/m/Y'},					
				{name: 'FECHA_SELECCION', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'NOMBRE_IF'},
				{name: 'REFERENCIA_INT'},
				{name: 'VALOR_TASA_INT'},
				{name: 'MONTO_TASA_INT', type: 'float'},
				{name: 'MONTOTOTAL_TASA_INT', type: 'float'},
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'CAUSA'}				
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta02Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta02Data(null, null, null);						
					}
				}
			}	
		});
		
		//Negociable a Vencido sin Operar
		var consulta22Data = new Ext.data.JsonStore({
			root : 'registros22',
			url : '24reporte02ext.data.jsp',
			baseParams: {
				informacion: 'Consulta22'
			},
			fields: [
				{name: 'NOMBRE_EPO'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_CARGA', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'}, 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'}, 
				{name: 'MONTO_PORC_DESCUENTO'}, 				
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},
				{name: 'MONT_VALUADO'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'}			
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta22Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta22Data(null, null, null);						
					}
				}
			}	
		});
				
		//No Negociable a Baja
		var consulta24Data = new Ext.data.JsonStore({
			root : 'registros24',
			url : '24reporte02ext.data.jsp',
			baseParams: {
				informacion: 'Consulta24'
			},
			fields: [
				{name: 'NOMBRE_EPO'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_CARGA', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'}, 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'},  
				{name: 'MONTO_PORC_DESCUENTO',type: 'float'}, 		
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},
				{name: 'MONT_VALUADO',type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'CAUSA'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta24Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta24Data(null, null, null);						
					}
				}
			}
		});
		
		
			//No Negociable a Baja
		var consulta21Data = new Ext.data.JsonStore({
			root : 'registros21',
			url : '24reporte02ext.data.jsp',
			baseParams: {
				informacion: 'Consulta21'
			},
			fields: [
				{name: 'NOMBRE_EPO'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_CARGA', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'FN_MONTO', type: 'float'},
				{name: 'MONEDA' }, 				 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'},  
				{name: 'MONTO_PORC_DESCUENTO',type: 'float'}, 		
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},
				{name: 'MONT_VALUADO',type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'FECHA_EMISION_ANT', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'FECHA_VENC_ANT', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'FN_MONTO_ANTERIOR',type: 'float'},
				{name: 'MODO_PLAZO_ANT',type: 'float'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta21Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta21Data(null, null, null);						
					}
				}
			}
		});
		
		
	//Seleccionado Pyme a Rechazado IF
	var gridGeneral = new Ext.grid.GridPanel({
		store: consultaGeneralData,
		id: 'gridGeneral',
		hidden: true,
		margins: '20 0 0 0',
		title: ' ',
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},			
			{
				header: 'N�mero de Acuse de carga',
				tooltip: 'N�mero de Acuse de carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo de documento',
				tooltip: 'Plazo de documento',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Plazo para descuento',
				tooltip: 'Plazo para descuento',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'Porcentaje de descuento',  //hay q realizar un calculo monto*Double.parseDouble(porcDescuento)/100;
				tooltip: 'Porcenteje de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto porcentaje de descuento',  //hay q realizar un calculo monto*Double.parseDouble(porcDescuento)/100;
				tooltip: 'Monto porcentaje de descuento',
				dataIndex: 'MONTO_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo de conversi�n',
				tooltip: 'Tipo de conversi�n',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'Tipo cambio',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'Monto valuado en pesos', //monto*tipoCambio;
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONT_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	

			{
				header: 'Modalidad de plazo', 
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			
			{
				header: 'N�mero de documento final', 
				tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'Monto', 
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},				
			{
				header: 'Plazo', 
				tooltip: 'Plazo',
				dataIndex: 'IG_PLAZO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center' 
			},				
			{
				header: 'Fecha de vencimiento', 
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},	
			{
				header: 'IF', 
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'				
			},	
			{
				header: 'Referencia tasa de inter�s', 
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA_INT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},	
			{
				header: 'Valor Tasa de inter�s', 
				tooltip: 'Valor Tasa de inter�s',
				dataIndex: 'VALOR_TASA_INT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			{
				header: 'Monto de Inter�s', 
				tooltip: 'Monto de Inter�s',
				dataIndex: 'MONTO_TASA_INT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Monto Total de Capital e Inter�s', 
				tooltip: 'Monto total de Capital e Inter�s',
				dataIndex: 'MONTOTOTAL_TASA_INT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Fecha de cambio', 
				tooltip: 'Fecha de cambio',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Causa', 
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			}	
		],
	 	
		stripeRows: true,
		loadMask: true,
		height: 500,
		width: 943,		
		frame: true,
		bbar: {
			id: 'barraPaginacion',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales14',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales14');
						var tipoTotal = Ext.getCmp('cmbEstatus').getValue();
						if(tipoTotal==23){
							if (ventana) {
							ventana.show();
							} else {
								totales14Data.load();					
								new Ext.Window({
									layout: 'fit',
									width: 800,
									height: 105,
									id: 'winTotales14',
									closeAction: 'hide',
									items: [
										gridTotales14
									],								
									title: 'En Proceso de Autorizacion IF a Negociable'
								}).show().alignTo(gridGeneral.el);
							}
						}else{
							if (ventana) {
							ventana.show();
							} else {
								totales142Data.load();					
								new Ext.Window({
									layout: 'fit',
									width: 800,
									height: 105,
									id: 'winTotales14',
									closeAction: 'hide',
									items: [
										gridTotales142
									],								
									title: 'En Proceso de Autorizacion IF a Seleccionado Pyme'
								}).show().alignTo(gridGeneral.el);
							}
						}
						
					}
				},
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo14',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoCSV',
									tipoArchivo:'csv',
									noEstatus:Ext.getCmp('cmbEstatus').getValue()
								}),
								callback: procesarSuccessFailureGenerarArchivoCSV14
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV14',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF14',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoPDF',
									tipoArchivo:'pdf',
									noEstatus:Ext.getCmp('cmbEstatus').getValue()
								}),
								callback: procesarSuccessFailureGenerarPDF14
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF14',
					hidden: true
				}					
			]
		}
		
	});
		
	//Seleccionado Pyme a Negociable
	var grid02 = new Ext.grid.GridPanel({
		store: consulta02Data,
		hidden: true,
		margins: '20 0 0 0',
		title: 'Seleccionado Pyme a Negociable',
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},			
			{
				header: 'N�mero de Acuse de Carga',
				tooltip: 'N�mero de Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},						
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: '% de descuento',  //hay q realizar un calculo monto*Double.parseDouble(porcDescuento)/100;
				tooltip: ' % de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},			
			{
				header: 'Monto % de descuento', 
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'Tipo cambio',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'Monto valuado en Pesos', 
				tooltip: 'Monto valuado en Pesos',
				dataIndex: 'MONT_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Modalidad de plazo', 
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			
			{
				header: 'N�mero de documento final',
				tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},				
			{
				header: 'Monto', 
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{
				header: 'Plazo', 
				tooltip: 'Plazo',
				dataIndex: 'IG_PLAZO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},		
			{
				header: 'Fecha de vencimiento', 
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},	
			{
				header: 'Fecha de vencimiento', 
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},	
			{
				header: 'Fecha de Operaci�n Distribuidor', 
				tooltip: 'Fecha de Operaci�n Distribuidor',
				dataIndex: 'FECHA_SELECCION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},	
			{
				header: 'IF', 
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'		
			},	
			{
				header: 'Referencia tasa de inter�s', 
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA_INT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'		
			},	
			{
				header: 'Valor tasa de inter�s', 
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_INT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			{
				header: 'Monto de Intereses', 
				tooltip: 'Monto de Intereses',
				dataIndex: 'MONTO_TASA_INT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 		
			},	
			{
				header: 'Monto Total de Capital e Inter�s', 
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTOTOTAL_TASA_INT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},		
			{
				header: 'Fecha de Rechazo', 
				tooltip: 'Fecha de Rechazo',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Causa', 
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			}	
		],		
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {
			id: 'barraPaginacion02',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales2',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales2');
						if (ventana) {
							ventana.show();
						} else {
								totales2Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 800,
								height: 105,
								id: 'winTotales2',
								closeAction: 'hide',
								items: [
									gridTotales2
								],								
								title: 'Totales Seleccionado Pyme a Negociable'
							}).show().alignTo(grid02.el);
						}
					}
				},
					'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo02',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoCSV',
									tipoArchivo:'csv',
									noEstatus:'2'
								}),
								callback: procesarSuccessFailureGenerarArchivoCSV02
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV02',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF02',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoPDF',
									tipoArchivo:'pdf',
									noEstatus:'2'
								}),
								callback: procesarSuccessFailureGenerarPDF02
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF02',
					hidden: true
				}					
			]	
		}
		
	});
	
	//Negociable a Vencido sin Operar
	var grid22 = new Ext.grid.GridPanel({
		store: consulta22Data,
		hidden: true,
		margins: '20 0 0 0',
		title: 'Negociable a Vencido sin Operar',
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},			
			{
				header: 'N�mero de Acuse de Carga',
				tooltip: 'N�mero de Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'denter',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto',
				tooltip: 'Plazo docto',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},			
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_PORC_DESCUENTO', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Tipo cambio',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Monto valuado en Pesos',
				tooltip: 'Monto valuado en Pesos',
				dataIndex: 'MONT_VALUADO',  
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',  
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de Cambio',
				tooltip: 'Fecha de Cambio',
				dataIndex: 'FECHA_CAMBIO',  
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',				
				renderer: Ext.util.Format.dateRenderer('d/m/Y')				
			}
			],
			stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,	
		frame: true,
		bbar: {
			id: 'barraPaginacion22',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales22',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales22');
						if (ventana) {
							ventana.show();
						} else {
							totales22Data.load();
							new Ext.Window({
								layout: 'fit',
								width: 800,
								height: 105,
								id: 'winTotales22',
								closeAction: 'hide',
								items: [
									gridTotales22
								],								
								title: 'Totales Negociable a Vencido sin Operar'
							}).show().alignTo(grid22.el);
						}
					}
				},
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo22',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoCSV',
									tipoArchivo:'csv',
									noEstatus:'22'
								}),
								callback: procesarSuccessFailureGenerarArchivoCSV22
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV22',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF22',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoPDF',
									tipoArchivo:'pdf',
									noEstatus:'22'
								}),
								callback: procesarSuccessFailureGenerarPDF22
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF22',
					hidden: true
				}					
			]
		}	
		
		});
	
		
	//Modificaci�n 
	var grid21 = new Ext.grid.GridPanel({
		store: consulta21Data,
		hidden: true,
		margins: '20 0 0 0',
		title: 'Modificaci�n',
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},			
			{
				header: 'N�mero de Acuse de Carga',
				tooltip: 'N�mero de Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: '% de descuento',  
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto % de descuento', 
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'Tipo cambio',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'Monto valuado en Pesos', 
				tooltip: 'Monto valuado en Pesos',
				dataIndex: 'MONT_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Modalidad de plazo', 
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},				
			{
				header: 'Fecha de Cambio', 
				tooltip: 'Fecha de Cambio',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Anterior Fecha de Emisi�n', 
				tooltip: 'Anterior Fecha de Emisi�n',
				dataIndex: 'FECHA_EMISION_ANT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	,
			{
				header: 'Anterior Fecha de Vencimiento', 
				tooltip: 'Anterior Fecha de Vencimiento',
				dataIndex: 'FECHA_VENC_ANT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{
				header: 'Anterior Monto del Documento', 
				tooltip: 'Anterior Monto del Documento',
				dataIndex: 'FN_MONTO_ANTERIOR',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}	,			
			{
				header: 'Anterior Modalidad de Plazo', 
				tooltip: 'Anterior Modalidad de Plazo',
				dataIndex: 'MODO_PLAZO_ANT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}	
			
		],		
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
			bbar: {
			id: 'barraPaginacion21',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales21',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales21');
						if (ventana) {
							ventana.show();
						} else {
							totales21Data.load();							
							new Ext.Window({
								layout: 'fit',
								width: 800,
								height: 105,
								id: 'winTotales21',
								closeAction: 'hide',
								items: [
									gridTotales21
								],								
								title: 'Totales Modificaci�n'
							}).show().alignTo(grid21.el);
						}
					}
				},
					'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo21',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoCSV',
									tipoArchivo:'csv',
									noEstatus:'21'
								}),
								callback: procesarSuccessFailureGenerarArchivoCSV21
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV21',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF21',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoPDF',
									tipoArchivo:'pdf',
									noEstatus:'21'
								}),
								callback: procesarSuccessFailureGenerarPDF21
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF21',
					hidden: true
				}					
			]
		}
	});
	
	
	
		//No Negociable a Baja
	var grid24 = new Ext.grid.GridPanel({
		store: consulta24Data,
		hidden: true,
		margins: '20 0 0 0',
		title: 'No Negociable a Baja',
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},			
			{
				header: 'N�mero de Acuse de Carga',
				tooltip: 'N�mero de Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: '% de descuento',  
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},			
			{
				header: 'Monto % de descuento', 
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'Tipo cambio',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'Monto valuado en Pesos', 
				tooltip: 'Monto valuado en Pesos',
				dataIndex: 'MONT_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	

			{
				header: 'Modalidad de plazo', //monto*tipoCambio;
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},				
			{
				header: 'Fecha de Cambio', 
				tooltip: 'Fecha de Cambio',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Causa', 
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			}	
		],		
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
			bbar: {
			id: 'barraPaginacion24',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales24',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales24');
						if (ventana) {
							ventana.show();
						} else {
							totales24Data.load();
							 new Ext.Window({
								layout: 'fit',
								width: 800,
								height: 105,
								id: 'winTotales24',
								closeAction: 'hide',
								items: [
									gridTotales24
								],								
								title: 'Totales No Negociable a Baja'
							}).show().alignTo(grid24.el);
						}
					}
				},
					'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo24',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoCSV',
									tipoArchivo:'csv',
									noEstatus:'24'
								}),
								callback: procesarSuccessFailureGenerarArchivoCSV24
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV24',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF24',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoPDF',
									tipoArchivo:'pdf',
									noEstatus:'24'
								}),
								callback: procesarSuccessFailureGenerarPDF24
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF24',
					hidden: true
				}					
			]
		}
	});
	
	
	var elementosForma = [
			{
				xtype: 'combo',
				name: 'ic_cambio_estatus',
				id: 'cmbEstatus',
				fieldLabel: 'Estatus',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'ic_cambio_estatus',
				emptyText: 'Seleccione...',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : catalogoEstatusData,				
				listeners: {
        select: {
						fn: function (combo) {
						var estatus = Ext.getCmp("cmbEstatus").getValue();	
						if(estatus ==23 || estatus ==34){						
							consultaGeneralData.load({
									params: Ext.apply(fp.getForm().getValues())
								});	
						}
						if(estatus ==2) {					
							consulta02Data.load({					
							params: Ext.apply(fp.getForm().getValues())
							});
						}
						if(estatus ==22 ) {	
							consulta22Data.load({					
								params: Ext.apply(fp.getForm().getValues())
							});
						}
						if(estatus ==24 ) {					
							consulta24Data.load({					
								params: Ext.apply(fp.getForm().getValues())
							});
						}
						if(estatus ==21 ) {
							consulta21Data.load({					
								params: Ext.apply(fp.getForm().getValues())
							});
						}		
						
						Ext.getCmp('btnTotales14').disable();	
						Ext.getCmp('btnGenerarArchivo14').disable();
						Ext.getCmp('btnGenerarPDF14').disable();
						Ext.getCmp('btnBajarCSV14').hide();
						Ext.getCmp('btnBajarPDF14').hide();
						Ext.getCmp('btnTotales2').disable();	
						Ext.getCmp('btnGenerarArchivo02').disable();
						Ext.getCmp('btnGenerarPDF02').disable();
						Ext.getCmp('btnBajarCSV02').hide();
						Ext.getCmp('btnBajarPDF02').hide();
						Ext.getCmp('btnTotales22').disable();	
						Ext.getCmp('btnGenerarArchivo22').disable();
						Ext.getCmp('btnGenerarPDF22').disable();
						Ext.getCmp('btnBajarCSV22').hide();
						Ext.getCmp('btnBajarPDF22').hide();
						
						Ext.getCmp('btnTotales24').disable();	
						Ext.getCmp('btnGenerarArchivo24').disable();
						Ext.getCmp('btnGenerarPDF24').disable();
						Ext.getCmp('btnBajarCSV24').hide();
						Ext.getCmp('btnBajarPDF24').hide();
						
						Ext.getCmp('btnTotales21').disable();	
						Ext.getCmp('btnGenerarArchivo21').disable();
						Ext.getCmp('btnGenerarPDF21').disable();
						Ext.getCmp('btnBajarCSV21').hide();
						Ext.getCmp('btnBajarPDF21').hide();
					
					
						}
					}
      },
			tpl : NE.util.templateMensajeCargaCombo	
			
				}		
			];
			
			
		
		
	//-------------------------------- PRINCIPAL -----------------------------------
	
		var fp = new Ext.form.FormPanel({
			id: 'forma',
			width: 500,
			title: 'Cambios de Estatus',
			frame: true,
			collapsible: true,
			titleCollapse: false,
			style: 'margin:0 auto;',
			bodyStyle: 'padding: 6px',
			labelWidth: 170,
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			items: elementosForma,			
			monitorValid: true		
		});		
			
			
			
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			applyTo: 'areaContenido',
			width: 949,
			items: [
				fp,
				NE.util.getEspaciador(20),
				grid22,
				gridGeneral,
				grid02,
				grid21,
				grid24,
				NE.util.getEspaciador(20)				
			]
		});
	
	//-------------------------------- ----------------- -----------------------------------
	catalogoEstatusData.load();

	});