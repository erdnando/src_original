<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*, java.math.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();

try {

	if(iNoCliente != null && !iNoCliente.equals("")) {
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
		String nombreArchivo = null;
		String informacion					=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
		String cveEstatus					=	(request.getParameter("claveStatus")==null)?"":request.getParameter("claveStatus");
		String tiposCredito					=	(request.getParameter("tiposCredito")==null)?"":request.getParameter("tiposCredito");
		String cgTipoConversion	=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
		String tipoCredito			=	(request.getParameter("tiposCredito")==null)?"":request.getParameter("tiposCredito");
		
		CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB",CargaDocDist.class);

		if("".equals(cgTipoConversion)){
			cgTipoConversion = cargaDocto.getTipoConversion(iNoCliente);
		}
		
		if("".equals(tiposCredito)){
			 tipoCredito = cargaDocto.getTiposCredito(iNoCliente, tiposCredito);
		}
		String rsEpo = "",rsNumDoc = "",rsAcuse = "",rsdfEmision = "",rsdfVenc = "",rsdfCarga = "";
		String rsPlazoD = "",rsMoneda = "",rsMontoDoc = "",rsTipConv = "", rsMontoTotal="";
		String rsTipCam = "",rsMontoVal = "",rsPlazDesc = "",rsPorcDesc = "",rsMontoDesc	= "";
		String rsModoPlaz = "",rsEstatus = "",rsic_mon = "",rsic_doc = "";
		String rsNumCre ="",rsIf="",rsTipoLc="",rsdfOperacion="",rsMontoC="",rsPlazoC="";
		String rsdfVencC="",rsTasaInt="",rsValorInt="",rsMontoT="",rsTipoCob="";
		String 	rsFechaOperIF="",rsFechaCambio="", bandeVentaCartera ="";
		double rsMontoCapInt = 0;
		
		if (cveEstatus.equals("9")){																																																										  //-----[gridVenceSinOpera]
			int rowVenceSin = 0;
			RepEstatusPyme repVence = new RepEstatusPyme();
			repVence.setProducto(4);
			repVence.setIcePyme(iNoCliente);
			repVence.setCveEstatus("9");
			repVence.setQrysentencia();
			Registros regVenceSin = repVence.executeQuery();
			/*******************
			Cabecera del archivo
			********************/
			contenidoArchivo.append("\nEstatus,Vencido sin Operar");
			contenidoArchivo.append("\nEPO,Num. acuse de carga,Num. docto. inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda,Monto,Plazo para descuento en dias,% de descuento,Monto % de descuento");
			if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado en pesos");
			}
			contenidoArchivo.append(",Modalidad de plazo");
			/***************************
			 Generacion del archivo
			/***************************/
			while(regVenceSin.next()){
				rsEpo				=	regVenceSin.getString("NOMBREEPO")==null?"":regVenceSin.getString("NOMBREEPO");
				rsNumDoc		= regVenceSin.getString("IG_NUMERO_DOCTO")==null?"":regVenceSin.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regVenceSin.getString("CC_ACUSE")==null?"":regVenceSin.getString("CC_ACUSE");
				rsdfEmision	= regVenceSin.getString("FECHA_EMISION")==null?"":regVenceSin.getString("FECHA_EMISION");
				rsdfVenc		= regVenceSin.getString("FECHA_VENC")==null?"":regVenceSin.getString("FECHA_VENC");
				rsdfCarga		= regVenceSin.getString("FECHA_CARGA")==null?"":regVenceSin.getString("FECHA_CARGA");
				rsPlazoD			= regVenceSin.getString("IG_PLAZO_DOCTO")==null?"":regVenceSin.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regVenceSin.getString("CD_NOMBRE")==null?"":regVenceSin.getString("CD_NOMBRE");
				rsMontoDoc	= regVenceSin.getString("FN_MONTO")==null?"":regVenceSin.getString("FN_MONTO");
				rsTipConv		= regVenceSin.getString("TIPO_CONVERSION")==null?"":regVenceSin.getString("TIPO_CONVERSION");
				rsTipCam		= regVenceSin.getString("TIPO_CAMBIO")==null?"":regVenceSin.getString("TIPO_CAMBIO");
				rsMontoVal	= regVenceSin.getString("MONTO_VAL")==null?"":regVenceSin.getString("MONTO_VAL");
				rsPlazDesc	= regVenceSin.getString("IG_PLAZO_DESCUENTO")==null?"":regVenceSin.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regVenceSin.getString("FN_PORC_DESCUENTO")==null?"":regVenceSin.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regVenceSin.getString("MONTO_DESC")==null?"":regVenceSin.getString("MONTO_DESC");
				rsModoPlaz	= regVenceSin.getString("MODO_PLAZO")==null?"":regVenceSin.getString("MODO_PLAZO");
				rsEstatus		= regVenceSin.getString("ESTATUS")==null?"":regVenceSin.getString("ESTATUS");
				rsic_mon			= regVenceSin.getString("IC_MONEDA")==null?"":regVenceSin.getString("IC_MONEDA");
				rsic_doc			= regVenceSin.getString("IC_DOCUMENTO")==null?"":regVenceSin.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regVenceSin.getString("CG_VENTACARTERA")==null)?"":regVenceSin.getString("CG_VENTACARTERA");						
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}
				contenidoArchivo.append("\n"+rsEpo.replace(',',' ')+","+rsAcuse+","+rsNumDoc+","+rsdfEmision+","+rsdfVenc+","+rsdfCarga+","+rsPlazoD+","+rsMoneda+","+rsMontoDoc+","+rsPlazDesc+","+rsPorcDesc+","+rsMontoDesc);
				if(!"".equals(cgTipoConversion))	{
					contenidoArchivo.append(","+rsTipConv+","+rsTipCam+","+rsMontoVal);
				}
				contenidoArchivo.append(","+rsModoPlaz);
				rowVenceSin++;
			}
			if (rowVenceSin == 0)	{
				contenidoArchivo.append("\nNo se Encontró Ningún Registro");
			}
		}else if (cveEstatus.equals("2") || cveEstatus.equals("32")){																																																						    		 //-----[gridNegociable]
			int rowNego = 0;
			RepEstatusPyme repNego = new RepEstatusPyme();
			repNego.setProducto(4);
			repNego.setIcePyme(iNoCliente);
      if (cveEstatus.equals("32"))
        repNego.setCveEstatus("32");
      else
        repNego.setCveEstatus("2");
			repNego.setQrysentencia();
			Registros regNego = repNego.executeQuery();
			/*******************
			Cabecera del archivo
			********************/
			if (cveEstatus.equals("32"))
          contenidoArchivo.append("\nEstatus,Operada TC");  
      else 
        contenidoArchivo.append("\nEstatus,Negociable");
			
      contenidoArchivo.append("\nEPO,Num. acuse de carga,Num. docto. inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda,Monto,Plazo para descuento en dias,% de descuento,Monto % de descuento");
			if (cveEstatus.equals("32")){ 
        contenidoArchivo.append(",Modalidad de plazo, Número de Documento Final, Monto, Plazo,Fecha de Vencimiento, Fecha de Operación,IF, Nombre del Producto, Monto Total, ID Orden Enviado, Respuesta de Operación, Código Autorización, Número de Tarjeta de Crédito ");   
      } else {
      if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado en pesos");
        }
			contenidoArchivo.append(",Modalidad de plazo"); 
      }
			/***************************
			 Generacion del archivo
			/***************************/
			while (regNego.next()) {
				rsEpo				=	regNego.getString("NOMBREEPO")==null?"":regNego.getString("NOMBREEPO");
				rsNumDoc		= regNego.getString("IG_NUMERO_DOCTO")==null?"":regNego.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regNego.getString("CC_ACUSE")==null?"":regNego.getString("CC_ACUSE");
				rsdfEmision	= regNego.getString("FECHA_EMISION")==null?"":regNego.getString("FECHA_EMISION");
				rsdfVenc		= regNego.getString("FECHA_VENC")==null?"":regNego.getString("FECHA_VENC");
				rsdfCarga		= regNego.getString("FECHA_CARGA")==null?"":regNego.getString("FECHA_CARGA");
				rsPlazoD			= regNego.getString("IG_PLAZO_DOCTO")==null?"":regNego.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regNego.getString("CD_NOMBRE")==null?"":regNego.getString("CD_NOMBRE");
				rsMontoDoc	= regNego.getString("FN_MONTO")==null?"":regNego.getString("FN_MONTO");
				if (cveEstatus.equals("32")){ 
          rsMontoTotal	= regNego.getString("MONTOFINAL")==null?"":regNego.getString("MONTOFINAL");
        } else {
        rsTipConv		= regNego.getString("TIPO_CONVERSION")==null?"":regNego.getString("TIPO_CONVERSION");
				rsTipCam		= regNego.getString("TIPO_CAMBIO")==null?"":regNego.getString("TIPO_CAMBIO");
				rsMontoVal	= regNego.getString("MONTO_VAL")==null?"":regNego.getString("MONTO_VAL");
				}
        rsPlazDesc	= regNego.getString("IG_PLAZO_DESCUENTO")==null?"":regNego.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regNego.getString("FN_PORC_DESCUENTO")==null?"":regNego.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regNego.getString("MONTO_DESC")==null?"":regNego.getString("MONTO_DESC");
				rsModoPlaz	= regNego.getString("MODO_PLAZO")==null?"":regNego.getString("MODO_PLAZO");
				rsEstatus		= regNego.getString("ESTATUS")==null?"":regNego.getString("ESTATUS");
				rsic_mon			= regNego.getString("IC_MONEDA")==null?"":regNego.getString("IC_MONEDA");
				rsic_doc			= regNego.getString("IC_DOCUMENTO")==null?"":regNego.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regNego.getString("CG_VENTACARTERA")==null)?"":regNego.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}
        String nomIf ="", cod_bins="",descBins="",ordEnv="",idOpe="",codAu="",tarjeta="";
        if (cveEstatus.equals("32")){
          nomIf			= regNego.getString("NOMBRE_IF")==null?"":regNego.getString("NOMBRE_IF");
          cod_bins		= regNego.getString("CG_CODIGO_BIN")==null?"":regNego.getString("CG_CODIGO_BIN");
			 descBins		= regNego.getString("DESC_BINS")==null?"":regNego.getString("DESC_BINS");
          ordEnv			= regNego.getString("ORDEN_ENVIADO")==null?"":regNego.getString("ORDEN_ENVIADO");
          idOpe			= regNego.getString("ID_OPERACION")==null?"":regNego.getString("ID_OPERACION");
          codAu			= regNego.getString("CODIGO_AUTORIZA")==null?"":regNego.getString("CODIGO_AUTORIZA");
          tarjeta			= regNego.getString("NUM_TARJETA")==null?"":regNego.getString("NUM_TARJETA");
        } else {
        
        contenidoArchivo.append("\n"+rsEpo.replace(',',' ')+","+rsAcuse+","+rsNumDoc+","+rsdfEmision+","+rsdfVenc+","+rsdfCarga+","+rsPlazoD+","+rsMoneda+","+rsMontoDoc+","+rsPlazDesc+","+rsPorcDesc+","+rsMontoDesc);
				}
        if (cveEstatus.equals("32")){ } else {
            contenidoArchivo.append(","+rsModoPlaz);
          if(!"".equals(cgTipoConversion))	{
            contenidoArchivo.append(","+rsTipConv+","+rsTipCam+","+rsMontoVal);
          }
        }
				
        if (cveEstatus.equals("32")){
        contenidoArchivo.append("\n"+rsEpo.replace(',',' ')+","+rsAcuse+","+rsNumDoc+","+rsdfEmision+","+"N/A"+","+rsdfCarga+","+"N/A"+","+rsMoneda+",$ "+rsMontoDoc+","+"N/A"+","+rsPorcDesc+"%,$"+rsMontoDesc);
          contenidoArchivo.append(","+"N/A");
          contenidoArchivo.append(","+rsic_doc+",$"+rsMontoTotal+","+"N/A"+","+"N/A"+","+rsdfEmision);
          contenidoArchivo.append(","+nomIf.replace(',',' ')+","+cod_bins+"-"+descBins+",$"+rsMontoTotal+","+"'"+ordEnv+","+idOpe+","+codAu+","+"XXXX-XXXX-XXXX-"+tarjeta);
        }
        
				rowNego++;
			}
			if (rowNego == 0)	{
				contenidoArchivo.append("\nNo se Encontró Ningún Registro");
			}
		}else if (cveEstatus.equals("5")) {																																																//-----[gridBaja]
			int rowBaja = 0;
			RepEstatusPyme repBaja = new RepEstatusPyme();
			repBaja.setProducto(4);
			repBaja.setIcePyme(iNoCliente);
			repBaja.setCveEstatus("5");
			repBaja.setQrysentencia();
			Registros regBaja = repBaja.executeQuery();
			/*******************
			Cabecera del archivo
			********************/
			contenidoArchivo.append("\nEstatus,Baja");
			contenidoArchivo.append("\nEPO,Num. acuse de carga,Num. docto. inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda,Monto,Plazo para descuento en dias,% de descuento,Monto % de descuento");
			if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado en pesos");
			}
			contenidoArchivo.append(",Modalidad de plazo");
			/***************************
			 Generacion del archivo
			/***************************/
			while (regBaja.next()) {
				rsEpo				=	regBaja.getString("NOMBREEPO")==null?"":regBaja.getString("NOMBREEPO");
				rsNumDoc		= regBaja.getString("IG_NUMERO_DOCTO")==null?"":regBaja.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regBaja.getString("CC_ACUSE")==null?"":regBaja.getString("CC_ACUSE");
				rsdfEmision	= regBaja.getString("FECHA_EMISION")==null?"":regBaja.getString("FECHA_EMISION");
				rsdfVenc		= regBaja.getString("FECHA_VENC")==null?"":regBaja.getString("FECHA_VENC");
				rsdfCarga		= regBaja.getString("FECHA_CARGA")==null?"":regBaja.getString("FECHA_CARGA");
				rsPlazoD			= regBaja.getString("IG_PLAZO_DOCTO")==null?"":regBaja.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regBaja.getString("CD_NOMBRE")==null?"":regBaja.getString("CD_NOMBRE");
				rsMontoDoc	= regBaja.getString("FN_MONTO")==null?"":regBaja.getString("FN_MONTO");
				rsTipConv		= regBaja.getString("TIPO_CONVERSION")==null?"":regBaja.getString("TIPO_CONVERSION");
				rsTipCam		= regBaja.getString("TIPO_CAMBIO")==null?"":regBaja.getString("TIPO_CAMBIO");
				rsMontoVal	= regBaja.getString("MONTO_VAL")==null?"":regBaja.getString("MONTO_VAL");
				rsPlazDesc	= regBaja.getString("IG_PLAZO_DESCUENTO")==null?"":regBaja.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regBaja.getString("FN_PORC_DESCUENTO")==null?"":regBaja.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regBaja.getString("MONTO_DESC")==null?"":regBaja.getString("MONTO_DESC");
				rsModoPlaz	= regBaja.getString("MODO_PLAZO")==null?"":regBaja.getString("MODO_PLAZO");
				rsEstatus		= regBaja.getString("ESTATUS")==null?"":regBaja.getString("ESTATUS");
				rsic_mon			= regBaja.getString("IC_MONEDA")==null?"":regBaja.getString("IC_MONEDA");
				rsic_doc			= regBaja.getString("IC_DOCUMENTO")==null?"":regBaja.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regBaja.getString("CG_VENTACARTERA")==null)?"":regBaja.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}
				contenidoArchivo.append("\n"+rsEpo.replace(',',' ')+","+rsAcuse+","+rsNumDoc+","+rsdfEmision+","+rsdfVenc+","+rsdfCarga+","+rsPlazoD+","+rsMoneda+","+rsMontoDoc+","+rsPlazDesc+","+rsPorcDesc+","+rsMontoDesc);
				if(!"".equals(cgTipoConversion))	{
					contenidoArchivo.append(","+rsTipConv+","+rsTipCam+","+rsMontoVal);
				}
				contenidoArchivo.append(","+rsModoPlaz);
				rowBaja++;
			}
			if (rowBaja == 0)	{
				contenidoArchivo.append("\nNo se Encontró Ningún Registro");
			}
		}else if (cveEstatus.equals("1")){																																																							   //-----[gridNoNegociable]
			int rowNoNego = 0;
			RepEstatusPyme repNoNego = new RepEstatusPyme();
			repNoNego.setProducto(4);
			repNoNego.setIcePyme(iNoCliente);
			repNoNego.setCveEstatus("1");
			repNoNego.setQrysentencia();
			Registros regNoNego = repNoNego.executeQuery();
			/*******************
			Cabecera del archivo
			********************/
			contenidoArchivo.append("\nEstatus,No Negociable");
			contenidoArchivo.append("\nEPO,Num. acuse de carga,Num. docto. inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda,Monto,Plazo para descuento en dias,% de descuento,Monto % de descuento");
			if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado en pesos");
			}
			contenidoArchivo.append(",Modalidad de plazo");
			/***************************
			 Generacion del archivo
			/***************************/
			while (regNoNego.next()) {
				rsEpo				=	regNoNego.getString("NOMBREEPO")==null?"":regNoNego.getString("NOMBREEPO");
				rsNumDoc		= regNoNego.getString("IG_NUMERO_DOCTO")==null?"":regNoNego.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regNoNego.getString("CC_ACUSE")==null?"":regNoNego.getString("CC_ACUSE");
				rsdfEmision	= regNoNego.getString("FECHA_EMISION")==null?"":regNoNego.getString("FECHA_EMISION");
				rsdfVenc		= regNoNego.getString("FECHA_VENC")==null?"":regNoNego.getString("FECHA_VENC");
				rsdfCarga		= regNoNego.getString("FECHA_CARGA")==null?"":regNoNego.getString("FECHA_CARGA");
				rsPlazoD			= regNoNego.getString("IG_PLAZO_DOCTO")==null?"":regNoNego.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regNoNego.getString("CD_NOMBRE")==null?"":regNoNego.getString("CD_NOMBRE");
				rsMontoDoc	= regNoNego.getString("FN_MONTO")==null?"":regNoNego.getString("FN_MONTO");
				rsTipConv		= regNoNego.getString("TIPO_CONVERSION")==null?"":regNoNego.getString("TIPO_CONVERSION");
				rsTipCam		= regNoNego.getString("TIPO_CAMBIO")==null?"":regNoNego.getString("TIPO_CAMBIO");
				rsMontoVal	= regNoNego.getString("MONTO_VAL")==null?"":regNoNego.getString("MONTO_VAL");
				rsPlazDesc	= regNoNego.getString("IG_PLAZO_DESCUENTO")==null?"":regNoNego.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regNoNego.getString("FN_PORC_DESCUENTO")==null?"":regNoNego.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regNoNego.getString("MONTO_DESC")==null?"":regNoNego.getString("MONTO_DESC");
				rsModoPlaz	= regNoNego.getString("MODO_PLAZO")==null?"":regNoNego.getString("MODO_PLAZO");
				rsEstatus		= regNoNego.getString("ESTATUS")==null?"":regNoNego.getString("ESTATUS");
				rsic_mon			= regNoNego.getString("IC_MONEDA")==null?"":regNoNego.getString("IC_MONEDA");
				rsic_doc			= regNoNego.getString("IC_DOCUMENTO")==null?"":regNoNego.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regNoNego.getString("CG_VENTACARTERA")==null)?"":regNoNego.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}
				contenidoArchivo.append("\n"+rsEpo.replace(',',' ')+","+rsAcuse+","+rsNumDoc+","+rsdfEmision+","+rsdfVenc+","+rsdfCarga+","+rsPlazoD+","+rsMoneda+","+rsMontoDoc+","+rsPlazDesc+","+rsPorcDesc+","+rsMontoDesc);
				if(!"".equals(cgTipoConversion))	{
					contenidoArchivo.append(","+rsTipConv+","+rsTipCam+","+rsMontoVal);
				}
				contenidoArchivo.append(","+rsModoPlaz);
				rowNoNego++;
			}
			if (rowNoNego == 0)	{
				contenidoArchivo.append("\nNo se Encontró Ningún Registro");
			}
		}else if ((cveEstatus.equals("3")) ){//-----[gridSelecPyme] 
			int rowSelec = 0;
			RepEstatusPyme repSelec = new RepEstatusPyme();
			repSelec.setProducto(4);
			repSelec.setIcePyme(iNoCliente);
			repSelec.setCveEstatus("3");
			repSelec.setTiposCredito(tipoCredito);
			repSelec.setQrysentencia();
			Registros regSelec = repSelec.executeQuery();
			/*******************
			Cabecera del archivo
			********************/
				contenidoArchivo.append("\nEstatus,Seleccionada Pyme");
			
			contenidoArchivo.append("\nEPO,Num. Acuse de Carga,Num. docto. inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda, Monto, Plazo para descuento,% de descuento,Monto % de descuento");
			if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado en pesos");
			}
			contenidoArchivo.append(",Modalidad de plazo,Num.docto. final,Monto,Plazo,Fecha de vencimiento,IF,Referencia tasa de interés,Valor tasa de interés,Monto de intereses,Monto total de Capital e Interés");
			/***************************
			 Generacion del archivo
			/***************************/
			while (regSelec.next()) {
				rsEpo				=	regSelec.getString("NOMBREEPO")==null?"":regSelec.getString("NOMBREEPO");
				rsNumDoc		= regSelec.getString("IG_NUMERO_DOCTO")==null?"":regSelec.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regSelec.getString("CC_ACUSE")==null?"":regSelec.getString("CC_ACUSE");
				rsdfEmision	= regSelec.getString("FECHA_EMISION")==null?"":regSelec.getString("FECHA_EMISION");
				rsdfVenc		= regSelec.getString("FECHA_VENC")==null?"":regSelec.getString("FECHA_VENC");
				rsdfCarga		= regSelec.getString("FECHA_CARGA")==null?"":regSelec.getString("FECHA_CARGA");
				rsPlazoD			= regSelec.getString("IG_PLAZO_DOCTO")==null?"":regSelec.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regSelec.getString("CD_NOMBRE")==null?"":regSelec.getString("CD_NOMBRE");
				rsMontoDoc	= regSelec.getString("FN_MONTO")==null?"":regSelec.getString("FN_MONTO");
				rsTipConv		= regSelec.getString("TIPO_CONVERSION")==null?"":regSelec.getString("TIPO_CONVERSION");
				rsTipCam		= regSelec.getString("TIPO_CAMBIO")==null?"":regSelec.getString("TIPO_CAMBIO");
				rsMontoVal	= regSelec.getString("MONTO_VAL")==null?"":regSelec.getString("MONTO_VAL");
				rsPlazDesc	= regSelec.getString("IG_PLAZO_DESCUENTO")==null?"":regSelec.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regSelec.getString("FN_PORC_DESCUENTO")==null?"":regSelec.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regSelec.getString("MONTO_DESC")==null?"":regSelec.getString("MONTO_DESC");
				rsModoPlaz	= regSelec.getString("MODO_PLAZO")==null?"":regSelec.getString("MODO_PLAZO");
				rsEstatus		= regSelec.getString("ESTATUS")==null?"":regSelec.getString("ESTATUS");
				rsIf						= regSelec.getString("NOMBREIF")==null?"":regSelec.getString("NOMBREIF");
				rsTipoLc			= regSelec.getString("TIPO_CRED")==null?"":regSelec.getString("TIPO_CRED");
				rsdfOperacion	= regSelec.getString("FECHA_OPERA")==null?"":regSelec.getString("FECHA_OPERA");
				rsMontoC		= regSelec.getString("FN_IMPORTE_RECIBIR")==null?"":regSelec.getString("FN_IMPORTE_RECIBIR");
				rsPlazoC			= regSelec.getString("IG_PLAZO_CREDITO")==null?"":regSelec.getString("IG_PLAZO_CREDITO");
				rsdfVencC		= regSelec.getString("FECHA_VENCRE")==null?"":regSelec.getString("FECHA_VENCRE");
				rsTasaInt		= regSelec.getString("REF_TASA")==null?"":regSelec.getString("REF_TASA");
				rsValorInt		= regSelec.getString("FN_VALOR_TASA")==null?"":regSelec.getString("FN_VALOR_TASA");
				rsMontoT		= regSelec.getString("FN_IMPORTE_INTERES")==null?"":regSelec.getString("FN_IMPORTE_INTERES");
				rsTipoCob		= regSelec.getString("TIPO_COB_INT")==null?"":regSelec.getString("TIPO_COB_INT");
				rsic_mon			= regSelec.getString("IC_MONEDA")==null?"":regSelec.getString("IC_MONEDA");
				rsic_doc			= regSelec.getString("IC_DOCUMENTO")==null?"":regSelec.getString("IC_DOCUMENTO");
				rsNumCre		= regSelec.getString("IC_DOCUMENTO")==null?"":regSelec.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regSelec.getString("CG_VENTACARTERA")==null)?"":regSelec.getString("CG_VENTACARTERA");
				
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}
				contenidoArchivo.append("\n"+rsEpo.replace(',',' ')+","+rsAcuse+","+rsNumDoc+","+rsdfEmision+","+rsdfVenc+","+rsdfCarga+","+rsPlazoD+","+rsMoneda+","+rsMontoDoc+","+
				rsPlazDesc+","+rsPorcDesc+","+rsMontoDesc);
				if(!"".equals(cgTipoConversion))	{
					contenidoArchivo.append(","+rsTipConv+","+rsTipCam+","+rsMontoVal);
				}
				contenidoArchivo.append(","+rsModoPlaz+","+rsNumCre+","+				
				rsMontoC+","+rsPlazoC+","+rsdfVencC+","+rsIf.replace(',',' ')+","+rsTasaInt.replace(',',' ')+","+rsValorInt+","+rsMontoT+","+(Double.parseDouble(rsMontoT)+Double.parseDouble(rsMontoC)));
				rowSelec++;
			}
			if (rowSelec == 0)	{
				contenidoArchivo.append("\nNo se Encontró Ningún Registro");
			}
		}else if ((cveEstatus.equals("24")) ){//-----[gridProcesoAutorizacion] 
			int rowSelec = 0;
			RepEstatusPyme repSelec = new RepEstatusPyme();
			repSelec.setProducto(4);
			repSelec.setIcePyme(iNoCliente);
			repSelec.setCveEstatus("24");
			repSelec.setTiposCredito(tipoCredito);
			repSelec.setQrysentencia();
			Registros regSelec = repSelec.executeQuery();
			/*******************
			Cabecera del archivo
			********************/
				contenidoArchivo.append("\nEstatus,En Proceso de Autorización IF");
			
			contenidoArchivo.append("\nEPO,Num. Acuse de Carga,Num. docto. inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda,Monto,Plazo para descuento,% de descuento,Monto % de descuento");
			if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado en pesos");
			}
			contenidoArchivo.append(",Modalidad de plazo,Num.docto. final,Monto,Plazo,Fecha de vencimiento,IF,Referencia tasa de interés,Valor tasa de interés,Monto de intereses,Monto total de Capital e Interés");
			/***************************
			 Generacion del archivo
			/***************************/
			while (regSelec.next()) {
				rsEpo				=	regSelec.getString("NOMBREEPO")==null?"":regSelec.getString("NOMBREEPO");
				rsNumDoc		= regSelec.getString("IG_NUMERO_DOCTO")==null?"":regSelec.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regSelec.getString("CC_ACUSE")==null?"":regSelec.getString("CC_ACUSE");
				rsdfEmision	= regSelec.getString("FECHA_EMISION")==null?"":regSelec.getString("FECHA_EMISION");
				rsdfVenc		= regSelec.getString("FECHA_VENC")==null?"":regSelec.getString("FECHA_VENC");
				rsdfCarga		= regSelec.getString("FECHA_CARGA")==null?"":regSelec.getString("FECHA_CARGA");
				rsPlazoD			= regSelec.getString("IG_PLAZO_DOCTO")==null?"":regSelec.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regSelec.getString("CD_NOMBRE")==null?"":regSelec.getString("CD_NOMBRE");
				rsMontoDoc	= regSelec.getString("FN_MONTO")==null?"":regSelec.getString("FN_MONTO");
				rsTipConv		= regSelec.getString("TIPO_CONVERSION")==null?"":regSelec.getString("TIPO_CONVERSION");
				rsTipCam		= regSelec.getString("TIPO_CAMBIO")==null?"":regSelec.getString("TIPO_CAMBIO");
				rsMontoVal	= regSelec.getString("MONTO_VAL")==null?"":regSelec.getString("MONTO_VAL");
				rsPlazDesc	= regSelec.getString("IG_PLAZO_DESCUENTO")==null?"":regSelec.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regSelec.getString("FN_PORC_DESCUENTO")==null?"":regSelec.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regSelec.getString("MONTO_DESC")==null?"":regSelec.getString("MONTO_DESC");
				rsModoPlaz	= regSelec.getString("MODO_PLAZO")==null?"":regSelec.getString("MODO_PLAZO");
				rsEstatus		= regSelec.getString("ESTATUS")==null?"":regSelec.getString("ESTATUS");
				rsIf						= regSelec.getString("NOMBREIF")==null?"":regSelec.getString("NOMBREIF");
				rsTipoLc			= regSelec.getString("TIPO_CRED")==null?"":regSelec.getString("TIPO_CRED");
				rsdfOperacion	= regSelec.getString("FECHA_OPERA")==null?"":regSelec.getString("FECHA_OPERA");
				rsMontoC		= regSelec.getString("FN_IMPORTE_RECIBIR")==null?"":regSelec.getString("FN_IMPORTE_RECIBIR");
				rsPlazoC			= regSelec.getString("IG_PLAZO_CREDITO")==null?"":regSelec.getString("IG_PLAZO_CREDITO");
				rsdfVencC		= regSelec.getString("FECHA_VENCRE")==null?"":regSelec.getString("FECHA_VENCRE");
				rsTasaInt		= regSelec.getString("REF_TASA")==null?"":regSelec.getString("REF_TASA");
				rsValorInt		= regSelec.getString("FN_VALOR_TASA")==null?"":regSelec.getString("FN_VALOR_TASA");
				rsMontoT		= regSelec.getString("FN_IMPORTE_INTERES")==null?"":regSelec.getString("FN_IMPORTE_INTERES");
				rsTipoCob		= regSelec.getString("TIPO_COB_INT")==null?"":regSelec.getString("TIPO_COB_INT");
				rsic_mon			= regSelec.getString("IC_MONEDA")==null?"":regSelec.getString("IC_MONEDA");
				rsic_doc			= regSelec.getString("IC_DOCUMENTO")==null?"":regSelec.getString("IC_DOCUMENTO");
				rsNumCre		= regSelec.getString("IC_DOCUMENTO")==null?"":regSelec.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regSelec.getString("CG_VENTACARTERA")==null)?"":regSelec.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}
				contenidoArchivo.append("\n"+rsEpo.replace(',',' ')+","+rsAcuse+","+rsNumDoc+","+rsdfEmision+","+rsdfVenc+","+rsdfCarga+","+rsPlazoD+","+rsMoneda+","+rsMontoDoc+","+rsPlazDesc+","+rsPorcDesc+","+rsMontoDesc);
				if(!"".equals(cgTipoConversion))	{
					contenidoArchivo.append(","+rsTipConv+","+rsTipCam+","+rsMontoVal);
				}
				contenidoArchivo.append(","+rsModoPlaz+","+rsNumCre+","+rsMontoC+","+rsPlazoC+","+rsdfVencC+","+rsIf.replace(',',' ')+","+rsTasaInt.replace(',',' ')+","+rsValorInt+","+rsMontoT+","+(Double.parseDouble(rsMontoT)+Double.parseDouble(rsMontoC)));
				rowSelec++;
			}
			if (rowSelec == 0)	{
				contenidoArchivo.append("\nNo se Encontró Ningún Registro");
			}
		}else if (cveEstatus.equals("4")){																																																								 	     //-----[gridOperada]
			int rowOpera = 0;
			RepEstatusPyme repOpera = new RepEstatusPyme();
			repOpera.setProducto(4);
			repOpera.setIcePyme(iNoCliente);
			repOpera.setCveEstatus("4");
			repOpera.setTiposCredito(tipoCredito);
			repOpera.setQrysentencia();
			Registros regOpera = repOpera.executeQuery();
			/*******************
			Cabecera del archivo
			********************/
			contenidoArchivo.append("\nEstatus,Operada");
			contenidoArchivo.append("\nEPO,Num. acuse de carga,Num. docto.inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda,Monto,   Plazo para descuento,% de descuento, Monto % de descuento");
			if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado en pesos");
			}
			contenidoArchivo.append(",Modalidad de plazo,Num. docto. final,Monto,Plazo,Fecha de vencimiento,Fecha de Operación IF,IF, Referencia tasa de interés,Valor tasa de interés,Monto de Intereses,Monto Total de Capital e Interés");
			/***************************
			 Generacion del archivo
			/***************************/
			while (regOpera.next()) {
				rsEpo				=	regOpera.getString("NOMBREEPO")==null?"":regOpera.getString("NOMBREEPO");
				rsNumDoc		= regOpera.getString("IG_NUMERO_DOCTO")==null?"":regOpera.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regOpera.getString("CC_ACUSE")==null?"":regOpera.getString("CC_ACUSE");
				rsdfEmision	= regOpera.getString("FECHA_EMISION")==null?"":regOpera.getString("FECHA_EMISION");
				rsdfVenc		= regOpera.getString("FECHA_VENC")==null?"":regOpera.getString("FECHA_VENC");
				rsdfCarga		= regOpera.getString("FECHA_CARGA")==null?"":regOpera.getString("FECHA_CARGA");
				rsPlazoD			= regOpera.getString("IG_PLAZO_DOCTO")==null?"":regOpera.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regOpera.getString("CD_NOMBRE")==null?"":regOpera.getString("CD_NOMBRE");
				rsMontoDoc	= regOpera.getString("FN_MONTO")==null?"":regOpera.getString("FN_MONTO");
				rsTipConv		= regOpera.getString("TIPO_CONVERSION")==null?"":regOpera.getString("TIPO_CONVERSION");
				rsTipCam		= regOpera.getString("TIPO_CAMBIO")==null?"":regOpera.getString("TIPO_CAMBIO");
				rsMontoVal	= regOpera.getString("MONTO_VAL")==null?"":regOpera.getString("MONTO_VAL");
				rsPlazDesc	= regOpera.getString("IG_PLAZO_DESCUENTO")==null?"":regOpera.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regOpera.getString("FN_PORC_DESCUENTO")==null?"":regOpera.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regOpera.getString("MONTO_DESC")==null?"":regOpera.getString("MONTO_DESC");
				rsModoPlaz	= regOpera.getString("MODO_PLAZO")==null?"":regOpera.getString("MODO_PLAZO");
				rsEstatus		= regOpera.getString("ESTATUS")==null?"":regOpera.getString("ESTATUS");
				rsIf						= regOpera.getString("NOMBREIF")==null?"":regOpera.getString("NOMBREIF");
				rsTipoLc			= regOpera.getString("TIPO_CRED")==null?"":regOpera.getString("TIPO_CRED");
				rsdfOperacion	= regOpera.getString("FECHA_OPERA")==null?"":regOpera.getString("FECHA_OPERA");
				rsMontoC		= regOpera.getString("FN_IMPORTE_RECIBIR")==null?"":regOpera.getString("FN_IMPORTE_RECIBIR");
				rsPlazoC			= regOpera.getString("IG_PLAZO_CREDITO")==null?"":regOpera.getString("IG_PLAZO_CREDITO");
				rsdfVencC		= regOpera.getString("FECHA_VENCRE")==null?"":regOpera.getString("FECHA_VENCRE");
				rsTasaInt		= regOpera.getString("REF_TASA")==null?"":regOpera.getString("REF_TASA");
				rsValorInt		= regOpera.getString("FN_VALOR_TASA")==null?"":regOpera.getString("FN_VALOR_TASA");
				rsMontoT		= regOpera.getString("FN_IMPORTE_INTERES")==null?"":regOpera.getString("FN_IMPORTE_INTERES");
				rsTipoCob		= regOpera.getString("TIPO_COB_INT")==null?"":regOpera.getString("TIPO_COB_INT");
				rsic_mon			= regOpera.getString("IC_MONEDA")==null?"":regOpera.getString("IC_MONEDA");
				rsic_doc			= regOpera.getString("IC_DOCUMENTO")==null?"":regOpera.getString("IC_DOCUMENTO");
				rsFechaOperIF= regOpera.getString("FECHA_OPER_IF")==null?"":regOpera.getString("FECHA_OPER_IF");
				rsNumCre		= regOpera.getString("IC_DOCUMENTO")==null?"":regOpera.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regOpera.getString("CG_VENTACARTERA")==null)?"":regOpera.getString("CG_VENTACARTERA");
					
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}
				contenidoArchivo.append("\n"+rsEpo.replace(',',' ')+","+rsAcuse+","+rsNumDoc+","+rsdfEmision+","+rsdfVenc+","+rsdfCarga+","+rsPlazoD+","+rsMoneda+","+rsMontoDoc+","+
				rsPlazDesc+","+rsPorcDesc+","+rsMontoDesc);
				if(!"".equals(cgTipoConversion))	{
					contenidoArchivo.append(","+rsTipConv+","+rsTipCam+","+rsMontoVal);
				}
				contenidoArchivo.append(","+rsModoPlaz+","+rsNumCre+","+Comunes.formatoDecimal(rsMontoC,2,false) +","+rsPlazoC+","+rsFechaOperIF+","+rsdfOperacion+","+rsIf.replace(',',' ')+","+rsTasaInt.replace(',',' ')+","+rsValorInt+","+rsMontoT+","+(Double.parseDouble(rsMontoC)+Double.parseDouble(rsMontoT)));
				rowOpera++;
			}
			if (rowOpera == 0)	{
				contenidoArchivo.append("\nNo se Encontró Ningún Registro");
			}
		}else if (cveEstatus.equals("11")){																																																								 //-----[gridOperaPaga]
			int rowOperaPaga = 0;
			RepEstatusPyme repOperaPaga = new RepEstatusPyme();
			repOperaPaga.setProducto(4);
			repOperaPaga.setIcePyme(iNoCliente);
			repOperaPaga.setCveEstatus("11");
			repOperaPaga.setTiposCredito(tipoCredito);
			repOperaPaga.setQrysentencia();
			Registros regOperaPaga = repOperaPaga.executeQuery();
			/*******************
			Cabecera del archivo
			********************/
			contenidoArchivo.append("\nEstatus,Operada Pagada");
			contenidoArchivo.append("\nEPO,Num. acuse de carga,Num. docto. inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda,Monto,  Plazo para descuento,% de descuento,Monto % de descuento");
			if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado en pesos");
			}
			contenidoArchivo.append(",Modalidad de plazo,Num.docto. final,Monto,Plazo,Fecha de vencimiento,Fecha de Operación IF,IF,Referencia tasa de interés,Valor tasa de interés,Monto de Intereses,Monto Total de Capital e Interés");
			/***************************
			 Generacion del archivo
			/***************************/
			while (regOperaPaga.next()) {
				rsEpo				=	regOperaPaga.getString("NOMBREEPO")==null?"":regOperaPaga.getString("NOMBREEPO");
				rsNumDoc		= regOperaPaga.getString("IG_NUMERO_DOCTO")==null?"":regOperaPaga.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regOperaPaga.getString("CC_ACUSE")==null?"":regOperaPaga.getString("CC_ACUSE");
				rsdfEmision	= regOperaPaga.getString("FECHA_EMISION")==null?"":regOperaPaga.getString("FECHA_EMISION");
				rsdfVenc		= regOperaPaga.getString("FECHA_VENC")==null?"":regOperaPaga.getString("FECHA_VENC");
				rsdfCarga		= regOperaPaga.getString("FECHA_CARGA")==null?"":regOperaPaga.getString("FECHA_CARGA");
				rsPlazoD			= regOperaPaga.getString("IG_PLAZO_DOCTO")==null?"":regOperaPaga.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regOperaPaga.getString("CD_NOMBRE")==null?"":regOperaPaga.getString("CD_NOMBRE");
				rsMontoDoc	= regOperaPaga.getString("FN_MONTO")==null?"":regOperaPaga.getString("FN_MONTO");
				rsTipConv		= regOperaPaga.getString("TIPO_CONVERSION")==null?"":regOperaPaga.getString("TIPO_CONVERSION");
				rsTipCam		= regOperaPaga.getString("TIPO_CAMBIO")==null?"":regOperaPaga.getString("TIPO_CAMBIO");
				rsMontoVal	= regOperaPaga.getString("MONTO_VAL")==null?"":regOperaPaga.getString("MONTO_VAL");
				rsPlazDesc	= regOperaPaga.getString("IG_PLAZO_DESCUENTO")==null?"":regOperaPaga.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regOperaPaga.getString("FN_PORC_DESCUENTO")==null?"":regOperaPaga.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regOperaPaga.getString("MONTO_DESC")==null?"":regOperaPaga.getString("MONTO_DESC");
				rsModoPlaz	= regOperaPaga.getString("MODO_PLAZO")==null?"":regOperaPaga.getString("MODO_PLAZO");
				rsEstatus		= regOperaPaga.getString("ESTATUS")==null?"":regOperaPaga.getString("ESTATUS");
				rsIf						= regOperaPaga.getString("NOMBREIF")==null?"":regOperaPaga.getString("NOMBREIF");
				rsTipoLc			= regOperaPaga.getString("TIPO_CRED")==null?"":regOperaPaga.getString("TIPO_CRED");
				rsdfOperacion	= regOperaPaga.getString("FECHA_OPERA")==null?"":regOperaPaga.getString("FECHA_OPERA");
				rsMontoC		= regOperaPaga.getString("FN_IMPORTE_RECIBIR")==null?"":regOperaPaga.getString("FN_IMPORTE_RECIBIR");
				rsPlazoC			= regOperaPaga.getString("IG_PLAZO_CREDITO")==null?"":regOperaPaga.getString("IG_PLAZO_CREDITO");
				rsdfVencC		= regOperaPaga.getString("FECHA_VENCRE")==null?"":regOperaPaga.getString("FECHA_VENCRE");
				rsTasaInt		= regOperaPaga.getString("REF_TASA")==null?"":regOperaPaga.getString("REF_TASA");
				rsValorInt		= regOperaPaga.getString("FN_VALOR_TASA")==null?"":regOperaPaga.getString("FN_VALOR_TASA");
				rsMontoT		= regOperaPaga.getString("FN_IMPORTE_INTERES")==null?"":regOperaPaga.getString("FN_IMPORTE_INTERES");
				rsTipoCob		= regOperaPaga.getString("TIPO_COB_INT")==null?"":regOperaPaga.getString("TIPO_COB_INT");
				rsic_mon			= regOperaPaga.getString("IC_MONEDA")==null?"":regOperaPaga.getString("IC_MONEDA");
				rsic_doc			= regOperaPaga.getString("IC_DOCUMENTO")==null?"":regOperaPaga.getString("IC_DOCUMENTO");
				rsFechaOperIF= regOperaPaga.getString("FECHA_OPER_IF")==null?"":regOperaPaga.getString("FECHA_OPER_IF");
				rsNumCre		= regOperaPaga.getString("IC_DOCUMENTO")==null?"":regOperaPaga.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regOperaPaga.getString("CG_VENTACARTERA")==null)?"":regOperaPaga.getString("CG_VENTACARTERA");
				
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}
				contenidoArchivo.append("\n"+rsEpo.replace(',',' ')+","+rsAcuse+","+rsNumDoc+","+rsdfEmision+","+rsdfVenc+","+rsdfCarga+","+rsPlazoD+","+rsMoneda+","+rsMontoDoc+","+
				rsPlazDesc+","+rsPorcDesc+","+rsMontoDesc);
				if(!"".equals(cgTipoConversion))	{
					contenidoArchivo.append(","+rsTipConv+","+rsTipCam+","+rsMontoVal);
				}
				contenidoArchivo.append(","+rsModoPlaz+","+rsNumCre+","+rsMontoC+","+rsPlazoC+","+rsdfVencC+","+rsFechaOperIF+","+rsIf.replace(',',' ')+","+rsTasaInt.replace(',',' ')+","+rsValorInt+","+rsMontoT+","+(Double.parseDouble(rsMontoC)+Double.parseDouble(rsMontoT)));
				rowOperaPaga++;
			}
			if (rowOperaPaga == 0)	{
				contenidoArchivo.append("\nNo se Encontró Ningún Registro");
			}
		}else if (cveEstatus.equals("22")){																																																							 //-----[gridOperaVence]
			int rowOperaVence = 0;
			RepEstatusPyme repOperaVence = new RepEstatusPyme();
			repOperaVence.setProducto(4);
			repOperaVence.setIcePyme(iNoCliente);
			repOperaVence.setCveEstatus("22");
			repOperaVence.setTiposCredito(tipoCredito);
			repOperaVence.setQrysentencia();
			Registros regOperaVence = repOperaVence.executeQuery();
			/*******************
			Cabecera del archivo
			********************/
			contenidoArchivo.append("\nEstatus,Pendiente de pago IF");
			contenidoArchivo.append("\nEPO,Num. acuse de carga,Num. docto.inicial,Fecha de emisión,Fecha de vencimiento,Fecha de publicación,Plazo docto.,Moneda,Monto,Plazo para descuento,% de descuento,Monto % de descuento");
			if(!"".equals(cgTipoConversion))	{
				contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado en pesos");
			}
			contenidoArchivo.append(",Modalidad de plazo,Num.docto. final,Monto,Plazo,Fecha de vencimiento,Fecha de Operación IF,IF,Referencia tasa de interés,Valor tasa de interés,Monto de Intereses,Monto Total de Capital e Interés,Fecha de Cambio de Estatus");
			/***************************
			 Generacion del archivo
			/***************************/
			while (regOperaVence.next()) {
				rsEpo				=	regOperaVence.getString("NOMBREEPO")==null?"":regOperaVence.getString("NOMBREEPO");
				rsNumDoc		= regOperaVence.getString("IG_NUMERO_DOCTO")==null?"":regOperaVence.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regOperaVence.getString("CC_ACUSE")==null?"":regOperaVence.getString("CC_ACUSE");
				rsdfEmision	= regOperaVence.getString("FECHA_EMISION")==null?"":regOperaVence.getString("FECHA_EMISION");
				rsdfVenc		= regOperaVence.getString("FECHA_VENC")==null?"":regOperaVence.getString("FECHA_VENC");
				rsdfCarga		= regOperaVence.getString("FECHA_CARGA")==null?"":regOperaVence.getString("FECHA_CARGA");
				rsPlazoD			= regOperaVence.getString("IG_PLAZO_DOCTO")==null?"":regOperaVence.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regOperaVence.getString("CD_NOMBRE")==null?"":regOperaVence.getString("CD_NOMBRE");
				rsMontoDoc	= regOperaVence.getString("FN_MONTO")==null?"":regOperaVence.getString("FN_MONTO");
				rsTipConv		= regOperaVence.getString("TIPO_CONVERSION")==null?"":regOperaVence.getString("TIPO_CONVERSION");
				rsTipCam		= regOperaVence.getString("TIPO_CAMBIO")==null?"":regOperaVence.getString("TIPO_CAMBIO");
				rsMontoVal	= regOperaVence.getString("MONTO_VAL")==null?"":regOperaVence.getString("MONTO_VAL");
				rsPlazDesc	= regOperaVence.getString("IG_PLAZO_DESCUENTO")==null?"":regOperaVence.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regOperaVence.getString("FN_PORC_DESCUENTO")==null?"":regOperaVence.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regOperaVence.getString("MONTO_DESC")==null?"":regOperaVence.getString("MONTO_DESC");
				rsModoPlaz	= regOperaVence.getString("MODO_PLAZO")==null?"":regOperaVence.getString("MODO_PLAZO");
				rsEstatus		= regOperaVence.getString("ESTATUS")==null?"":regOperaVence.getString("ESTATUS");
				rsIf						= regOperaVence.getString("NOMBREIF")==null?"":regOperaVence.getString("NOMBREIF");
				rsTipoLc			= regOperaVence.getString("TIPO_CRED")==null?"":regOperaVence.getString("TIPO_CRED");
				rsdfOperacion	= regOperaVence.getString("FECHA_OPERA")==null?"":regOperaVence.getString("FECHA_OPERA");
				rsMontoC		= regOperaVence.getString("FN_IMPORTE_RECIBIR")==null?"":regOperaVence.getString("FN_IMPORTE_RECIBIR");
				rsPlazoC			= regOperaVence.getString("IG_PLAZO_CREDITO")==null?"":regOperaVence.getString("IG_PLAZO_CREDITO");
				rsdfVencC		= regOperaVence.getString("FECHA_VENCRE")==null?"":regOperaVence.getString("FECHA_VENCRE");
				rsTasaInt		= regOperaVence.getString("REF_TASA")==null?"":regOperaVence.getString("REF_TASA");
				rsValorInt		= regOperaVence.getString("FN_VALOR_TASA")==null?"":regOperaVence.getString("FN_VALOR_TASA");
				rsMontoT		= regOperaVence.getString("FN_IMPORTE_INTERES")==null?"":regOperaVence.getString("FN_IMPORTE_INTERES");
				rsTipoCob		= regOperaVence.getString("TIPO_COB_INT")==null?"":regOperaVence.getString("TIPO_COB_INT");
				rsic_mon			= regOperaVence.getString("IC_MONEDA")==null?"":regOperaVence.getString("IC_MONEDA");
				rsic_doc			= regOperaVence.getString("IC_DOCUMENTO")==null?"":regOperaVence.getString("IC_DOCUMENTO");
				rsFechaOperIF= regOperaVence.getString(31)==null?"":regOperaVence.getString(31);
				rsNumCre		= regOperaVence.getString("IC_DOCUMENTO")==null?"":regOperaVence.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regOperaVence.getString("CG_VENTACARTERA")==null)?"":regOperaVence.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}
				contenidoArchivo.append("\n"+rsEpo.replace(',',' ')+","+rsAcuse+","+rsNumDoc+","+rsdfEmision+","+rsdfVenc+","+rsdfCarga+","+rsPlazoD+","+rsMoneda+","+rsMontoDoc+","+rsPlazDesc+","+rsPorcDesc+","+rsMontoDesc);
				if(!"".equals(cgTipoConversion))	{
					contenidoArchivo.append(","+rsTipConv+","+rsTipCam+","+rsMontoVal);
				}
				contenidoArchivo.append(","+rsModoPlaz+","+rsNumCre+","+rsMontoC+","+rsPlazoC+","+rsdfVencC+","+rsFechaOperIF+","+rsIf.replace(',',' ')+","+rsTasaInt.replace(',',' ')+","+rsValorInt+","+rsMontoT+","+Double.parseDouble(rsMontoC)+Double.parseDouble(rsMontoT)+","+rsFechaCambio);
				rowOperaVence++;
			}
			if (rowOperaVence == 0)	{
				contenidoArchivo.append("\nNo se Encontró Ningún Registro");
			}
		}

		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		} else {
			nombreArchivo = archivo.nombre;
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>