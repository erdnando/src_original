var panelPublico = '';
var panelPrivado = '';

Ext.onReady(function() {

	var jsonValoresIniciales
	var ic_documento = "";
	var publicaDoctosFinanciables = ''; //F009-2015

	//F009-2015 Se guarda el valor para saber si publica documentos financiables.
	function procesaPublicaDocumentosFinanciables(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if (Ext.util.JSON.decode(response.responseText) != null){
				/***** Si tipo de credito es modalidad 2,e muestra el combo Tipo de pago *****/
				publicaDoctosFinanciables = Ext.util.JSON.decode(response.responseText).publicaDoctosFinanciables;
				if(publicaDoctosFinanciables == 'S'){
					Ext.getCmp('tipo_pago_id').show();
				} else if(publicaDoctosFinanciables == 'N'){
					Ext.getCmp('tipo_pago_id').reset();
					Ext.getCmp('tipo_pago_id').hide();
				}
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesaValoresXEPO(opts, success, response) {
		fp.el.mask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales  = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
			
			if(jsonValoresIniciales.tipoCreditoXepo=='F') {
				Ext.getCmp('cmbTipoCobro').disable();
				Ext.getCmp('montoPorDes').disable();
				Ext.getCmp('tipoCreditoXepo').setValue(jsonValoresIniciales.tipoCreditoXepo);
			}else{
				Ext.getCmp('cmbTipoCobro').enable();
				Ext.getCmp('montoPorDes').enable();
			}
				fp.el.unmask();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//ventana de Acuse
	var procesarPanelAcuse = function(grid, rowIndex, colIndex, item, event) {
	var registro = grid.getStore().getAt(rowIndex);
	var cc_acuse = registro.get('CC_ACUSE');
	var ventana = Ext.getCmp('verAcuse');		
		
	if (ventana) {	
		ventana.show();
	} else {	
		new Ext.Window({
			layout: 'fit',
			width: 640,
			height: 380,
			id: 'verAcuse',
			closeAction: 'hide',
			items: [
				panelAcuse
			],
			title: 'Acuse'
		}).show();
	}
	var bodyPanel = Ext.getCmp('panelAcuse').body;
	var mgr = bodyPanel.getUpdater();
	mgr.on('failure', 
			function(el, response) {
				x.update('');
				NE.util.mostrarErrorResponse(response);
			});		
		mgr.update({
			url: '24consulta1acext.jsp',
			scripts: false,
			params: {
				acuse:	cc_acuse
			},
			indicatorText: 'Cargando Acuse '
		});	
	}
/*
	var procesarSuccessFailureGenerarXpaginaPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarXpaginaPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarXpaginaPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
/*
	var procesarSuccessFailureGenerarTotalPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarTotalPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarTotalPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarTotalPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarTotalPDF').enable();
		Ext.getCmp('btnGenerarTotalPDF').setIconClass('icoPdf');
	}
/*
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}
/*
	var procesarSuccessFailureGenerarBXpaginaPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarBXpaginaPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarBXpaginaPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
/*
	var procesarSuccessFailureGenerarBTotalPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarBTotalPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarBTotalPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarBTotalPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarBTotalPDF').enable();
		Ext.getCmp('btnGenerarBTotalPDF').setIconClass('icoPdf');
	}
/*
	var procesarSuccessFailureGenerarBArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarBArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarBArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarBArchivo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarBArchivo').enable();
		Ext.getCmp('btnGenerarBArchivo').setIconClass('icoXls');
	}

	var muestraGridCambios = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		ic_documento = registro.get('IC_DOCUMENTO');
		cambioDoctosData.load({params:	{ic_docto:	ic_documento}});
		var ventana = Ext.getCmp('cambioDoctos');
		if (ventana) {
			ventana.destroy();
		}
		new Ext.Window({
			layout: 'fit',
			width: 800,
			height: 200,
			id: 'cambioDoctos',
			closeAction: 'hide',
			items: [
				gridCambiosDoctos
			],
			title: 'Documento'
			}).show();
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var jsonData = store.reader.jsonData;
			
			//var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			//var btnBajarXpaginaPDF = Ext.getCmp('btnBajarXpaginaPDF');
			//var btnGenerarXpaginaPDF = Ext.getCmp('btnGenerarXpaginaPDF');
			//var btnBajarTotalPDF = Ext.getCmp('btnBajarTotalPDF');
			var btnGenerarTotalPDF = Ext.getCmp('btnGenerarTotalPDF');
			var btnTotales = Ext.getCmp('btnTotales');
			var el = grid.getGridEl();

			var cm = grid.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(15, true);		//Monto valuado en pesos
			
			if(store.getTotalCount() > 0) {
				if (jsonData.cgTipoConversion != "")	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(15, false);	//Monto valuado en pesos
				}
				if(publicaDoctosFinanciables == 'S'){
					cm.setHidden(cm.findColumnIndex('IG_TIPO_PAGO'), false);
				} else{
					cm.setHidden(cm.findColumnIndex('IG_TIPO_PAGO'), true);
				}
				btnTotales.enable();
				//btnGenerarXpaginaPDF.enable();
				btnGenerarTotalPDF.enable();
				//btnBajarXpaginaPDF.hide();
				//btnBajarTotalPDF.hide();
				btnGenerarArchivo.enable();
				/*if(!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}*/
				el.unmask();
			} else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				//btnGenerarXpaginaPDF.disable();
				btnGenerarTotalPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaBData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		var gridB  =  Ext.getCmp('gridB');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridB.isVisible()) {
				gridB.show();
			}
			var jsonData = store.reader.jsonData;
			//var btnBajarBArchivo = Ext.getCmp('btnBajarBArchivo');
			var btnGenerarBArchivo = Ext.getCmp('btnGenerarBArchivo');
			//var btnBajarBXpaginaPDF = Ext.getCmp('btnBajarBXpaginaPDF');
			//var btnGenerarBXpaginaPDF = Ext.getCmp('btnGenerarBXpaginaPDF');
			//var btnBajarBTotalPDF = Ext.getCmp('btnBajarBTotalPDF');
			var btnGenerarBTotalPDF = Ext.getCmp('btnGenerarBTotalPDF');
			var btnTotalesB = Ext.getCmp('btnTotalesB');
			var el = gridB.getGridEl();

			var cm = gridB.getColumnModel();
			
			if(store.getTotalCount() > 0) {
			
				if (jsonData.cgTipoConversion != "")	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);					
					//cm.setHidden(cm.findColumnIndex('MONTO_VALUADO'), false);//Monto valuado en pesos
				}
				if(publicaDoctosFinanciables == 'S'){
					cm.setHidden(cm.findColumnIndex('IG_TIPO_PAGO'), false);
				} else{
					cm.setHidden(cm.findColumnIndex('IG_TIPO_PAGO'), true);
				}
				btnTotalesB.enable();
				//btnGenerarBXpaginaPDF.enable();
				btnGenerarBTotalPDF.enable();
				btnGenerarBArchivo.enable();
				//btnBajarBXpaginaPDF.hide();
				//btnBajarBTotalPDF.hide();
				/*if(!btnBajarBArchivo.isVisible()) {
					btnGenerarBArchivo.enable();
				} else {
					btnGenerarBArchivo.disable();
				}*/
				
				el.unmask();
			} else {
				btnTotalesB.disable();
				btnGenerarBArchivo.disable();
				//btnGenerarBXpaginaPDF.disable();
				btnGenerarBTotalPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarCambioDoctosData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var gridCambios = Ext.getCmp('gridCambios');
			var el = gridCambios.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ningun cambio para este documento', 'x-mask');
			}
		}
	}

	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPODist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});


	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMonedaDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoTipoCobroData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoTipoCobroDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIfDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoModoPlazoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoModoPlazoDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var cambioDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta01ext.data.jsp',
		baseParams: {informacion: 'obtenCambioDoctos'},
		fields: [
			{name: 'FECH_CAMBIO', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CD_DESCRIPCION'},
			{name: 'FECH_EMI_ANT', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECH_EMI_NEW', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_MONTO_ANTERIOR',	type: 'float'},
			{name: 'FN_MONTO_NUEVO', 		type: 'float'},
			{name: 'FECH_VENC_ANT', 		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECH_VENC_NEW', 		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'MODO_PLAZO_ANTERIOR'},
			{name: 'MODO_PLAZO_NUEVO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCambioDoctosData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarCambioDoctosData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'IG_NUMERO_DOCTO'},//inicial
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_CARGA', type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO', type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MODO_PLAZO'},
			{name: 'IG_TIPO_PAGO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},//final
			{name: 'IC_DOCUMENTO'},
			{name: 'MONEDA_LINEA'},
			{name: 'CATEGORIA'},
			{name: 'CG_VENTACARTERA'},
			{name: 'IC_ESTATUS_DOCTO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			beforeLoad:	{fn: function(store, options){Ext.apply(options.params, {
				tipo_credito: jsonValoresIniciales.tipo_credito,
				tipoCreditoXepo: jsonValoresIniciales.tipoCreditoXepo				
				});}	},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
		
	});

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Datos Documento Inicial', colspan: 24, align: 'center'},	//colspan:	19
				{header: 'Datos Documento Final', colspan: 6, align: 'center'}			//colspan:	9
			]
		]
	});

	var consultaBData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta_B'
		},
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'DF_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'DF_FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MODO_PLAZO'},
			{name: 'IG_TIPO_PAGO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'MONEDA_LINEA'},
			{name: 'CATEGORIA'},
			{name: 'POR_AFRO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'CG_VENTACARTERA'},
			{name: 'IC_ESTATUS_DOCTO'},
			{name: 'NOMBRE_IF'},
			{name: 'TIPO_CREDITO'},
			{name: 'DF_OPERACION_CREDITO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO_CREDITO'},
			{name: 'DF_VENC_CREDITO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'REF_TASA'},
			{name: 'FN_VALOR_TASA'},
			{name: 'FN_IMPORTE_INTERES',		type: 'float'},
			{name: 'TIPO_COBRO_INT'},
			{name: 'MONTO_VALUADO'},
			{name : 'ID_ORDEN_ENVIADO'},
			{name : 'ID_OPERACION'},
			{name : 'CODIGO_AUTORIZADO'},
			{name : 'FECHA_REGISTRO'},
			{name : 'OPERA_TARJETA'},
			{name : 'BINS'},
			{name : 'NUM_TC'}
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){Ext.apply(options.params, {
					tipo_credito: jsonValoresIniciales.tipo_credito,
					tipoCreditoXepo: jsonValoresIniciales.tipoCreditoXepo				
					});}	
			},
			load: procesarConsultaBData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaBData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
		
	});

	var resumenTotalesAData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta01ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotalesA'
		},
		fields: [
			{name: 'NOMMONEDA', mapping: 'col1' },
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'col2'},
			{name: 'TOTAL_MONTO_DOCUMENTOS', type: 'float', mapping: 'col3'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var resumenTotalesBData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta01ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotalesB'
		},
		fields: [
			{name: 'NOMMONEDA', mapping: 'col1' },
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'col2'},
			{name: 'TOTAL_MONTO_DOCUMENTOS', type: 'float', mapping: 'col3'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var gridTotalesA = {
		xtype: 'grid',
		store: resumenTotalesAData,
		id: 'gridTotalesA',
		hidden:	true,
		columns: [
			{
				header: 'TOTALES',
				dataIndex: 'NOMMONEDA',
				align: 'left',	width: 250
			},
			{
				header: 'Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},
			{
				header: 'Monto Documentos',
				dataIndex: 'TOTAL_MONTO_DOCUMENTOS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	var gridTotalesB = {
		xtype: 'grid',
		store: resumenTotalesBData,
		id: 'gridTotalesB',
		hidden:	true,
		columns: [
			{
				header: 'TOTALES',
				dataIndex: 'NOMMONEDA',
				align: 'left',	width: 250
			},
			{
				header: 'Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},
			{
				header: 'Monto Documentos',
				dataIndex: 'TOTAL_MONTO_DOCUMENTOS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},{
				header: 'Num. acuse carga', tooltip: 'N�mero acuse carga',
				dataIndex: 'CC_ACUSE', align: 'center',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO', align: 'center',
				sortable : true, width : 140,	hidden: false
			},{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',
				dataIndex : 'DF_CARGA', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'DF_FECHA_EMISION', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_FECHA_VENC', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO', align: 'center',
				sortable : true,	width : 120,	hidden: false
			},{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'MONEDA',
				sortable : true,	width : 130,	hidden: false
			},{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Categor�a', tooltip: 'Categor�a',
				dataIndex : 'CATEGORIA',
				sortable : true,	width : 120,	hidden: false
			}				
			,{
				header : 'Plazo para descuento.', tooltip: 'Plazo para descuento.',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true,	width : 120,	align: 'center', hidden: false
			},{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',	//	CALCULAR DE ACUERDO AL ESTATUS
				sortable : true,	width : 100, align: 'center', hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									if(registro.get('IC_ESTATUS_DOCTO') != 2 )	{value = 0;}
									return Ext.util.Format.number(value,'0%');
								}
			},{
				header : 'Monto a descontar', tooltip: 'Monto a descontar',			//Index::::	12
				dataIndex : '',	//	ESTE CAMPO ES CALCULADO	...
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	function(value, metaData, registro, rowIndex, colIndex, store) {
									var porcDescuento = registro.get("FN_PORC_DESCUENTO");
									if (registro.get("IC_CAMBIO_ESTATUS") != 2) {
										porcDescuento = 0;
									}
									return Ext.util.Format.number(registro.get('FN_MONTO')*(porcDescuento/100), '$0,0.00');
								}
			},{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true,
				renderer	: 	function(value, metaData, registro, rowIndex, colIndex, store) {
									if ((registro.get('TIPO_CAMBIO') != 1 && registro.get('IC_MONEDA') != registro.get('MONEDA_LINEA')) == false)	{
										value = "";
									}
									return value;
								}
			},{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer	:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if ((registro.get('TIPO_CAMBIO') != 1 && registro.get('IC_MONEDA') != registro.get('MONEDA_LINEA')) == false)	{
										value = "";
									}
									return Ext.util.Format.number(value,'$0,0.00');
								}
			},{
				header : 'Monto valuado en pesos', tooltip: 'Monto valuado en pesos',	//Index::::	15
				dataIndex : 'MONTO_VALUADO',	align: 'right', //CALCULADO
				sortable : true,	width : 120,	hidden: true,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								value = "";
									if (registro.get('TIPO_CAMBIO') != 1 && registro.get('IC_MONEDA') != registro.get('MONEDA_LINEA'))	{
										var porcDescuento = registro.get("FN_PORC_DESCUENTO");
										if (registro.get("IC_CAMBIO_ESTATUS") != 2) {
											porcDescuento = 0;
										}
										var montoDescontar = (registro.get('FN_MONTO')*(porcDescuento/100), '$0,0.00');
										value = ((registro.get('FN_MONTO')-montoDescontar)*registro.get('TIPO_CAMBIO'));
									}
								return Ext.util.Format.number(value, '$0,0.00');
								}
			},{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO', align: 'center', 
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},{
				header : 'Tipo de pago', tooltip: 'Tipo de pago',
				dataIndex : 'IG_TIPO_PAGO',  align: 'center',
				sortable : true,	width : 180,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get('IG_TIPO_PAGO') == '1')	{
												value = 'Financiamiento con intereses';
											} else if (registro.get('IG_TIPO_PAGO') == '2')	{
												value = 'Meses sin intereses';
											} else{
												value = '';
											}
											return value;
										}
			},{
				header : 'Estatus', tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable : true,	width : 150,	hidden: false
			},{
				xtype:	'actioncolumn',
				header : 'Cambios del documento', tooltip: 'Cambios del documento',
				dataIndex : 'IC_DOCUMENTO',
				width:	100,	align: 'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('IC_DOCUMENTO')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraGridCambios
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Datos Documento Inicial',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						resumenTotalesAData.load();
						var totalesACmp = Ext.getCmp('gridTotalesA');
						if (!totalesACmp.isVisible()) {
							totalesACmp.show();
							totalesACmp.el.dom.scrollIntoView();
						}
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta01exta.jsp',
							params:	Ext.apply(fp.getForm().getValues(),{
										informacion: 'ArchivoCSV',
										tipo_credito: jsonValoresIniciales.tipo_credito,
										tipoCreditoXepo: jsonValoresIniciales.tipoCreditoXepo,
										publicaDoctosFinanciables: publicaDoctosFinanciables
										}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},/*
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},*/
				'-',
				/*{
					xtype: 'button',
					text: 'Generar x P�gina PDF',
					id: 'btnGenerarXpaginaPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '24consulta01extpdf.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoXpaginaPDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize,
								tipo_credito: jsonValoresIniciales.tipo_credito,
								tipoCreditoXepo: jsonValoresIniciales.tipoCreditoXepo	
							}),
							callback: procesarSuccessFailureGenerarXpaginaPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar x P�gina PDF',
					id: 'btnBajarXpaginaPDF',
					hidden: true
				},
				'-',*/
				{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id: 'btnGenerarTotalPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta01ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoTotalPDF',
								tipo_credito: jsonValoresIniciales.tipo_credito,
								tipoCreditoXepo: jsonValoresIniciales.tipoCreditoXepo,
										publicaDoctosFinanciables: publicaDoctosFinanciables
							}),
							callback: procesarSuccessFailureGenerarTotalPDF
						});
					}
				}/*,
				{
					xtype: 'button',
					text: 'Bajar Todo PDF',
					id: 'btnBajarTotalPDF',
					hidden: true
				},'-'*/
			]
		}
	});

//---------->		GRIDBBBB
	var gridB = new Ext.grid.GridPanel({
		id:	'gridB',
		store: consultaBData,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},{
				xtype:	'actioncolumn',
				header : 'Num. acuse pyme', tooltip: 'Num. acuse pyme',
				dataIndex : 'CC_ACUSE',
				width:	150,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return (record.get('CC_ACUSE'));
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							var auxAcuse = value.substring(1, 2);
							if ((registro.get('CC_ACUSE')).substring(1, 2)	==	"5") {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else	{
								return value;
							}
						},
						handler:	procesarPanelAcuse
					}
				]
			},{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO', align: 'center',
				sortable : true, width : 140,	hidden: false
			},{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',
				dataIndex : 'DF_CARGA', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'DF_FECHA_EMISION', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_FECHA_VENC', align: 'center',
				sortable : true,	width : 100, hidden: false,	
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') == 'Operada TC'){
										return	value = 'N/A';
									}
									return Ext.util.Format.date(value,'d/m/Y');
								}
			},{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO', align: 'center',
				sortable : true,	width : 120,	hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') == 'Operada TC'){
										return value = 'N/A';
									}
									return value;
								}
			},{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'MONEDA',
				sortable : true,	width : 130,	hidden: false
			},{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Categor�a', tooltip: 'Categor�a',
				dataIndex : 'CATEGORIA',
				sortable : true,	width : 120,	hidden: false
			}
			,{
				header : '% de Descuento Aforo', tooltip: '% de Descuento Aforo ',
				dataIndex : 'POR_AFRO',
				sortable : true,	width : 120,	hidden: false, align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			}
			,{
				header : 'Monto a Descontar  ', tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				sortable : true,	width : 120,	hidden: false, align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
					var  valor  =  registro.get('FN_MONTO')* (registro.get('POR_AFRO')/100)
					return Ext.util.Format.number(valor,'$0,0.00')
				}
			}
			,{
				header : 'Plazo para descuento.', tooltip: 'Plazo para descuento.',
				dataIndex : 'IG_PLAZO_DESCUENTO', 
				sortable : true,	width : 120,	hidden: false, align: 'center'
			},{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',	//	CALCULAR DE ACUERDO AL ESTATUS
				sortable : true,	width : 100, align: 'center', hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
								if(((registro.get('MODO_PLAZO')=='Pronto pago')||(registro.get('MODO_PLAZO')=='Plazo mixto'))&&registro.get('IC_ESTATUS_DOCTO') == 32){
									return Ext.util.Format.number(value,'0%');
								}else{
									if(registro.get('IC_ESTATUS_DOCTO') != 2 )	{value = 0;}
									return Ext.util.Format.number(value,'0%');
								}
				
								
								}
			},{
				header : 'Monto a descontar', tooltip: 'Monto a descontar',			//Index::::	12
				dataIndex : '',	//	ESTE CAMPO ES CALCULADO	...
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	function(value, metaData, registro, rowIndex, colIndex, store) {
									var porcDescuento = registro.get("FN_PORC_DESCUENTO");
									if(((registro.get('MODO_PLAZO')=='Pronto pago')||(registro.get('MODO_PLAZO')=='Plazo mixto'))&&registro.get('IC_ESTATUS_DOCTO') == 32){
										return Ext.util.Format.number(registro.get('FN_MONTO')*(porcDescuento/100), '$0,0.00');
									}else{
										if (registro.get("IC_CAMBIO_ESTATUS") != 2) {
											porcDescuento = 0;
										}
										return Ext.util.Format.number(registro.get('FN_MONTO')*(porcDescuento/100), '$0,0.00');
									}
					}
			},{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true,
				renderer: 	function(value, metaData, registro, rowIndex, colIndex, store) {
									if ((registro.get('TIPO_CAMBIO') != 1 && registro.get('IC_MONEDA') != registro.get('MONEDA_LINEA')) == false)	{
										value = "";
									}
									return value;
								}
			},{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer	:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if ((registro.get('TIPO_CAMBIO') != 1 && registro.get('IC_MONEDA') != registro.get('MONEDA_LINEA')) == false)	{
										value = "";
									}
									return Ext.util.Format.number(value,'$0,0.00');
								}
			},{
				header : 'Monto valuado en pesos', tooltip: 'Monto valuado en pesos',	//Index::::	15
				dataIndex : '', align: 'right', 	//CALCULADO
				sortable : true,	width : 120,	hidden: true,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								value = "";
									if (registro.get('TIPO_CAMBIO') != 1 && registro.get('IC_MONEDA') != registro.get('MONEDA_LINEA'))	{
										var porcDescuento = registro.get("FN_PORC_DESCUENTO");
										if (registro.get("IC_CAMBIO_ESTATUS") != 2) {
											porcDescuento = 0;
										}
										var montoDescontar = (registro.get('FN_MONTO')*(porcDescuento/100), '$0,0.00');
										value = ((registro.get('FN_MONTO')-montoDescontar)*registro.get('TIPO_CAMBIO'));
									}
								return Ext.util.Format.number(value, '$0,0.00');
								}
			},{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO', align: 'center',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},{
				header : 'Tipo de pago', tooltip: 'Tipo de pago',
				dataIndex : 'IG_TIPO_PAGO',  align: 'center',
				sortable : true,	width : 180,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get('IG_TIPO_PAGO') == '1')	{
												value = 'Financiamiento con intereses';
											} else if (registro.get('IG_TIPO_PAGO') == '2')	{
												value = 'Meses sin intereses';
											} else{
												value = '';
											}
											return value;
										}
			},{
				header : 'Estatus', tooltip: 'Estatus',
				dataIndex : 'ESTATUS', align: 'center',
				sortable : true,	width : 150,	hidden: false
			},{
				xtype:	'actioncolumn',
				header : 'Cambios del documento', tooltip: 'Cambios del documento',
				dataIndex : 'IC_DOCUMENTO',
				width:	100,	align: 'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('IC_DOCUMENTO')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraGridCambios
					}
				]
			},{
				header : 'N�mero de documento final', tooltip: 'N�mero de documento final',
				dataIndex : 'IC_DOCUMENTO', align: 'center',
				sortable : false, width : 140,	hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') == 'Negociable'){
										value = '';
									}
								return value;
								}
			},{
				header : 'Monto', tooltip: 'Monto',	 align: 'right',
				dataIndex : '',	align: 'right',	//CALCULADO:::montoCredito
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {							
					if (registro.get('ESTATUS') == 'Negociable'){
						value = '';
					}else{
						var montoDescontar = (registro.get('FN_MONTO')*(registro.get("FN_PORC_DESCUENTO")/100));
						value = (registro.get('FN_MONTO')-montoDescontar);									
					}
					return Ext.util.Format.number(value, '$0,0.00');
					}
			},{
				header : 'Plazo', tooltip: 'Plazo', align: 'center',
				dataIndex : 'PLAZO_CREDITO', align: 'center',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') == 'Negociable'){
										value = '';
									}
									if (registro.get('ESTATUS') == 'Operada TC'){
										return value = 'N/A';
									}
									return value;
								}
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_VENC_CREDITO', align: 'center',
				sortable : true,	width : 100, hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') == 'Negociable'){
										value = '';
									}
									if (registro.get('ESTATUS') == 'Operada TC'){
										return	value = 'N/A';
									}
									return Ext.util.Format.date(value,'d/m/Y');
								}
			},{
				header : 'Fecha de Operaci�n', tooltip: 'Fecha de Operaci�n',
				dataIndex : 'DF_OPERACION_CREDITO', align: 'center',
				sortable : true,	width : 100, hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') == 'Negociable'){
										value = '';
									}
									return Ext.util.Format.date(value,'d/m/Y');
								}
			},{
				header : 'IF', tooltip: 'IF',
				dataIndex : 'NOMBRE_IF',
				sortable : true,	width : 150,	hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
					if (registro.get('ESTATUS') == 'Negociable'){
							value = '';
					}
					return value;
				}
			},{
				header : "Nombre del Producto", tooltip: "Nombre del Producto",
				dataIndex : 'BINS',
				sortable : true,	width : 150,	hidden: false,align: 'center',
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
					if (registro.get('ESTATUS') == 'Negociable'){
							value = '';
					}
					if (registro.get('ESTATUS') != 'Operada TC'){//Operada TC
						value = 'N/A';
					}
					return value;
				}
			},{
				header : 'Referencia de tasa de inter�s', tooltip: 'Referencia de tasa de inter�s',
				dataIndex : 'REF_TASA', align: 'center',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') == 'Negociable'){
										value = '';
									}
									if (registro.get('ESTATUS') == 'Operada TC'){//Operada TC
										value = 'N/A';
									}
									return value;
								}
			},{
				header : 'Valor tasa de inter�s', tooltip: 'Valor tasa de inter�s',
				dataIndex : 'FN_VALOR_TASA',
				sortable : true,	width : 120,	align: 'center', hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									
									if (registro.get('ESTATUS') == 'Negociable'){
										return value ='';
									}else	if (registro.get('ESTATUS') == 'Operada TC'){//Operada TC
										return	value = 'N/A';
									}else{
										return Ext.util.Format.number(value, '0.00%');
									}
								}
			},{
				header : 'Monto de intereses', tooltip: 'Monto de intereses',
				dataIndex : 'FN_IMPORTE_INTERES',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									//if (registro.get('ESTATUS') == 'Negociable'){
									//	value = '';
									//}
									if (registro.get('ESTATUS') == 'Operada TC'){//Operada TC
										if (registro.get('ESTATUS') == 'Negociable'){
											value = '';
										}else{
											value = 'N/A';
										}
											return value;
									}else{
										return Ext.util.Format.number(value, '$0,0.00');
									}
								}
			},{
				header : 'ID Orden enviado', tooltip: 'ID Orden enviado',
				dataIndex : 'ID_ORDEN_ENVIADO',
				sortable : true,	width : 120,	align: 'center', hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') != 'Operada TC'){//Operada TC
										value = 'N/A';
									}
									return (value);
								}
			},{
				header : 'Respuesta de Operaci�n', tooltip: 'Respuesta de Operaci�n',
				dataIndex : 'ID_OPERACION',
				sortable : true,	width : 120,	align: 'center', hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') != 'Operada TC'){//Operada TC
										value = 'N/A';
									}
									return (value);
								}
			},{
				header : 'C�digo de Autorizaci�n', tooltip: 'C�digo de Autorizaci�n',
				dataIndex : 'CODIGO_AUTORIZADO',
				sortable : true,	width : 120,	align: 'center', hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') != 'Operada TC'){//Operada TC
										value = 'N/A';
									}
									return (value);
								}
			},{
				header : 'N�mero Tarjeta de Cr�dito', tooltip: 'N�mero Tarjeta de Cr�dito',
				dataIndex : 'NUM_TC',
				hidden	:true,
				sortable : true,	width : 200,	align: 'center', hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
					if (registro.get('ESTATUS') != 'Operada TC'){
						value = 'N/A';
					}else {
						if(value != ''){
							value = 'XXXX-XXXX-XXXX-'+value;
						}
						
					}
					return value;
				}
			}
			
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		//title: 'Informaci�n de documentos',
		frame: true,
		plugins: grupos,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacionB',
			displayInfo: true,
			store: consultaBData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotalesB',
					hidden: false,
					handler: function(boton, evento) {
						resumenTotalesBData.load();
						var totalesBCmp = Ext.getCmp('gridTotalesB');
						if (!totalesBCmp.isVisible()) {
							totalesBCmp.show();
							totalesBCmp.el.dom.scrollIntoView();
						}
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id: 'btnGenerarBArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta01exta.jsp',
							params:	Ext.apply(fp.getForm().getValues(),{
										informacion: 'ArchivoBCSV',
										tipo_credito: jsonValoresIniciales.tipo_credito,
										tipoCreditoXepo: jsonValoresIniciales.tipoCreditoXepo,
										publicaDoctosFinanciables: publicaDoctosFinanciables	
										}),
							callback: procesarSuccessFailureGenerarBArchivo
						});
					}
				}/*,
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarBArchivo',
					hidden: true
				},
				{
					xtype: 'button',
					text: 'Generar x P�gina PDF',
					id: 'btnGenerarBXpaginaPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacionB = Ext.getCmp("barraPaginacionB");
						Ext.Ajax.request({
							url: '24consulta01extpdf.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoBXpaginaPDF',
								start: cmpBarraPaginacionB.cursor,
								limit: cmpBarraPaginacionB.pageSize,
								tipo_credito: jsonValoresIniciales.tipo_credito,
								tipoCreditoXepo: jsonValoresIniciales.tipoCreditoXepo	
							}),
							callback: procesarSuccessFailureGenerarBXpaginaPDF
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar x P�gina PDF',
					id: 'btnBajarBXpaginaPDF',
					hidden: true
				}*/,'-',
				{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id: 'btnGenerarBTotalPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta01ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoBTotalPDF',
								tipo_credito: jsonValoresIniciales.tipo_credito,
								tipoCreditoXepo: jsonValoresIniciales.tipoCreditoXepo,
								publicaDoctosFinanciables: publicaDoctosFinanciables
							}),
							callback: procesarSuccessFailureGenerarBTotalPDF
						});
					}
				}/*,
				{
					xtype: 'button',
					text: 'Bajar Todo PDF',
					id: 'btnBajarBTotalPDF',
					hidden: true
				},'-'*/
			]
		}
	});

	var gridCambiosDoctos = {
		xtype: 'grid',
		store: cambioDoctosData,
		id: 'gridCambios',
		columns: [
			{
				header: 'Fecha del Cambio',
				dataIndex: 'FECH_CAMBIO',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Cambio Estatus',
				dataIndex: 'CD_DESCRIPCION',		//CT_CAMBIO_MOTIVO
				align: 'left',	width: 200
			},{
				header: 'Fecha Emision Anterior',
				dataIndex: 'FECH_EMI_ANT',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Fecha Emision Nueva',
				dataIndex: 'FECH_EMI_NEW',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Monto Anterior',
				dataIndex: 'FN_MONTO_ANTERIOR',
				align: 'right',	width: 120,	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto Nuevo',
				dataIndex: 'FN_MONTO_NUEVO',
				align: 'right',	width: 120,	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Fecha Vencimiento Anterior',
				dataIndex: 'FECH_VENC_ANT',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Fecha Vencimiento Nuevo',
				dataIndex: 'FECH_VENC_NEW',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Modalidad de Plazo Anterior',
				dataIndex: 'MODO_PLAZO_ANTERIOR',
				align: 'left',	width: 150
			},{
				header: 'Modalidad de Plazo Nuevo',
				dataIndex: 'MODO_PLAZO_NUEVO',
				align: 'left',	width: 150
			}
		],
		height: 300,
		title: '',
		frame: false,
		loadMask: true
	};

	var panelAcuse = new Ext.Panel({
		id: 'panelAcuse',
		height: 'auto',
		hidden: false,
		align: 'center',
		autoScroll: true	
	});
	
	var elementosForma = [
		{ xtype: 'textfield',  hidden: true,  id: 'tipoCreditoXepo', 	value: '' },
		{
			xtype: 'panel',
			layout:'column',
				items:[{
					xtype: 'container',
					id:	'panelIzq',
					columnWidth:.5,
					width:'50%',
					layout: 'form',
					defaults: {
						msgTarget: 'side'
					},
					items: [
					{
						anchor: '90%',
						xtype: 'combo',
						name: 'ic_epo',
						id: 'cmbEpo',
						fieldLabel: 'EPO',
						mode: 'local', 
						allowBlank:false,
						displayField : 'descripcion',
						valueField : 'clave',
						hiddenName : 'ic_epo',
						emptyText: 'Seleccione EPO',
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store : catalogoEPOData,
						//tpl : NE.util.templateMensajeCargaCombo,
						tpl: '<tpl for=".">' +
								'<tpl if="!Ext.isEmpty(loadMsg)">'+
								'<div class="loading-indicator">{loadMsg}</div>'+
								'</tpl>'+
								'<tpl if="Ext.isEmpty(loadMsg)">'+
								'<div class="x-combo-list-item">' +
								'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
								'</div></tpl></tpl>',
						listeners: {
							select: {
								fn: function(combo) {
									grid.hide();
									gridB.hide();
									var totalesACmp = Ext.getCmp('gridTotalesA');
									if (totalesACmp.isVisible()) {
										totalesACmp.hide();
									}
									var totalesCmpB = Ext.getCmp('gridTotalesB');
									if (totalesCmpB.isVisible()) {
										totalesCmpB.hide();
									}
									var ventana = Ext.getCmp('cambioDoctos');
									if (ventana) {
										ventana.destroy();
									}
									var btnConsulta = Ext.getCmp('btnConsultar');
									var comboTipoCobro = Ext.getCmp('cmbTipoCobro');
									var comboIF = Ext.getCmp('cmbIF');
									var comboModalidad = Ext.getCmp('cmbModalidad');
									comboTipoCobro.setValue('');
									comboTipoCobro.store.removeAll();
									comboTipoCobro.setDisabled(false);
									comboTipoCobro.store.load({
										params: {
											ic_epo: combo.getValue()
										}
									});
									comboIF.setValue('');
									comboIF.store.removeAll();
									comboIF.setDisabled(false);
									comboIF.store.load({
										params: {
											ic_epo: combo.getValue()
										}
									});
									comboModalidad.setValue('');
									comboModalidad.store.removeAll();
									comboModalidad.setDisabled(false);
									comboModalidad.store.load({
										params: {
											ic_epo: combo.getValue()
										}
									});
									//Fodea 019-2012
									Ext.Ajax.request({
										url: '24consulta01ext.data.jsp',
										params: {
											informacion: "TipoCreditoXEPO",
											ic_epo: combo.getValue()
										},
										callback: procesaValoresXEPO
									});
									//Fodea 09-2015
									Ext.Ajax.request({
										url: '24consulta01ext.data.jsp',
										params: {
											informacion: 'Publica_Documentos_Financiables',
											ic_epo: combo.getValue()
										},
										callback: procesaPublicaDocumentosFinanciables
									});
								}
							}
						}
					}, {
						anchor: '90%',
						xtype: 'combo',
						name: 'ic_estatus_docto',
						id: 'cambioEstatusCmb',
						fieldLabel: 'Estatus',
						emptyText: 'Seleccione Estatus',
						allowBlank: true,
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'ic_estatus_docto',
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoEstatusData,
						tpl : NE.util.templateMensajeCargaCombo
					},{
						anchor: '90%',
						xtype: 'textfield',
						name: 'ig_numero_docto',
						id: 'no_doc_ini',
						fieldLabel: 'N�mero de documento inicial',
						allowBlank: true,
						maxLength: 12
					},{
						anchor: '90%',
						xtype: 'textfield',
						name: 'cc_acuse',
						id: 'no_cc_acuse',
						fieldLabel: 'Num. acuse de carga',
						allowBlank: true,
						maxLength: 13
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha emisi�n docto.',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fecha_emision_de',
								id: 'dc_fecha_emisionMin',
								allowBlank: true,
								startDay: 0,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'dc_fecha_emisionMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 22
							},
							{
								xtype: 'datefield',
								name: 'fecha_emision_a',
								id: 'dc_fecha_emisionMax',
								allowBlank: true,
								startDay: 1,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoInicioFecha: 'dc_fecha_emisionMin',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha de vto. docto.',	
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fecha_vto_de',
								id: 'dc_fecha_vencMin',
								allowBlank: true,
								startDay: 0,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'dc_fecha_vencMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 22
							},
							{
								xtype: 'datefield',
								name: 'fecha_vto_a',
								id: 'dc_fecha_vencMax',
								allowBlank: true,
								startDay: 1,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'dc_fecha_vencMin',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					},{
						anchor: '90%',
						xtype: 'combo',
						name: 'ic_moneda',
						fieldLabel: 'Moneda',
						emptyText: 'Seleccione Moneda',	
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'ic_moneda',
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoMonedaData,
						tpl : NE.util.templateMensajeCargaCombo
					},{
						xtype: 'compositefield',
						fieldLabel: 'Monto docto.',
						msgTarget: 'side',
						combineErrors: false, anchor: '98%',
						items: [
							{
								xtype: 'numberfield',
								name: 'fn_monto_de',
								id: 'montoMin',
								allowBlank: true,
								maxLength: 12,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangoValor',
								campoFinValor: 'montoMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'a',
								width: 22
							},
							{
								xtype: 'numberfield',
								name: 'fn_monto_a',
								id: 'montoMax',
								allowBlank: true,
								maxLength: 12,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangoValor',
								campoInicioValor: 'montoMin',
								margins: '0 20 0 0'	  //necesario para mostrar el icono de error
							}
						]
					},{
							xtype: 'checkbox',
							boxLabel:	'Monto % de descuento',
							id: 'montoPorDes',
							value:	'S',
							name: 'monto_con_descuento'
					},{
							xtype: 'checkbox',
							boxLabel:	'Solo documentos modificados',
							value:	'S',
							name: 'solo_cambio'
					}]
				},{
					xtype: 'container',
					id:	'panelDer',
					columnWidth:.5,
					width:'50%',
					layout: 'form',
					defaults: {
						msgTarget: 'side'
					},
					items: [{
						anchor: '90%',
						xtype: 'textfield',
						name: 'ic_documento',
						id: 'no_doc_fin',
						fieldLabel: 'N�mero de documento final',
						allowBlank: true,
						maxLength: 13
					},{
						anchor: '90%',
						xtype: 'compositefield',
						fieldLabel: 'Fecha de operaci�n',	
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fecha_seleccion_de',
								id: 'dc_fecha_operMin',
								allowBlank: true,
								startDay: 0,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'dc_fecha_operMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 22
							},
							{
								xtype: 'datefield',
								name: 'fecha_seleccion_a',
								id: 'dc_fecha_operMax',
								allowBlank: true,
								startDay: 1,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'dc_fecha_operMin',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Monto',
						msgTarget: 'side',
						combineErrors: false,
						items: [
							{
								xtype: 'numberfield',
								name: 'monto_credito_de',
								id: 'bMontoMin',
								allowBlank: true,
								maxLength: 12,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangoValor',
								campoFinValor: 'bMontoMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'a',
								width: 22
							},
							{
								xtype: 'numberfield',
								name: 'monto_credito_a',
								id: 'bMontoMax',
								allowBlank: true,
								maxLength: 12,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangoValor',
								campoInicioValor: 'bMontoMin',
								margins: '0 20 0 0'	  //necesario para mostrar el icono de error
							}
						]
					},{
						anchor: '90%',
						xtype: 'combo',
						name: 'ic_tipo_cobro_int',
						id:	'cmbTipoCobro',
						fieldLabel: 'Tipo de cobro de inter�s',
						emptyText: 'Seleccione tipo cobro de inter�s',
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'ic_tipo_cobro_int',
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoTipoCobroData,
						tpl : NE.util.templateMensajeCargaCombo
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha de publicaci�n',	
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fecha_publicacion_de',
								id: 'fecha_publicaMin',
								allowBlank: true,
								startDay: 0,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'fecha_publicaMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 22
							},
							{
								xtype: 'datefield',
								name: 'fecha_publicacion_a',
								id: 'fecha_publicaMax',
								allowBlank: true,
								startDay: 1,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'fecha_publicaMin',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha de vto. docto. final',	
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fecha_vto_credito_de',
								id: 'fin_fecha_vencMin',
								allowBlank: true,
								startDay: 0,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'fin_fecha_vencMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 22
							},
							{
								xtype: 'datefield',
								name: 'fecha_vto_credito_a',
								id: 'fin_fecha_vencMax',
								allowBlank: true,
								startDay: 1,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'fin_fecha_vencMin',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					},{
						anchor: '90%',
						xtype: 'combo',
						name: 'ic_if',
						id: 'cmbIF',
						fieldLabel: 'IF',
						emptyText: 'Seleccione IF',
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'ic_if',
						forceSelection: true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoIfData,
						tpl: '<tpl for=".">' +
								'<tpl if="!Ext.isEmpty(loadMsg)">'+
								'<div class="loading-indicator">{loadMsg}</div>'+
								'</tpl>'+
								'<tpl if="Ext.isEmpty(loadMsg)">'+
								'<div class="x-combo-list-item">' +
								'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
								'</div></tpl></tpl>'
					},{
						anchor: '90%',
						xtype: 'combo',
						name: 'modo_plazo',
						id:	'cmbModalidad',
						fieldLabel: 'Modalidad del plazo',
						emptyText: 'Seleccione modalidad de plazo',
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'modo_plazo',
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoModoPlazoData,
						tpl : NE.util.templateMensajeCargaCombo
					},{
						anchor:         '90%',
						xtype:          'combo',
						id:             'tipo_pago_id',
						name:           'tipo_pago',
						hiddenName:     'tipo_pago',
						fieldLabel:     'Tipo de pago',
						msgTarget:      'side',
						mode:           'local',
						emptyText:      'Seleccione modalidad de pago...',
						triggerAction:  'all',
						forceSelection: true,
						typeAhead:      true,
						hidden:         true,
						store:          [['1','Financiamiento con intereses'], ['2','Meses sin intereses']] // No existe catalogo para este combo
					}
				]
			}]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 940,
		style: ' margin:0 auto;',
		title:	'Criterios de b�squeda',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					gridB.hide();
					var totalesACmp = Ext.getCmp('gridTotalesA');
					if (totalesACmp.isVisible()) {
						totalesACmp.hide();
					}
					var totalesCmpB = Ext.getCmp('gridTotalesB');
					if (totalesCmpB.isVisible()) {
						totalesCmpB.hide();
					}
					var ventanaDoctos = Ext.getCmp('cambioDoctos');
					if (ventanaDoctos) {
						ventanaDoctos.destroy();
					}
					if(!verificaPanelIzq()) {
						return;
					}
					if(!verificaPanelDer()) {
						return;
					}
					var fechaEmisionMin = Ext.getCmp('dc_fecha_emisionMin');
					var fechaEmisionMax = Ext.getCmp('dc_fecha_emisionMax');
					var fechaVenceMin = Ext.getCmp("dc_fecha_vencMin");
					var fechaVenceMax = Ext.getCmp("dc_fecha_vencMax");
					var izq_montoMin = Ext.getCmp("montoMin");
					var izq_montoMax = Ext.getCmp("montoMax");
					var fechaOperaMin = Ext.getCmp("dc_fecha_operMin");
					var fechaOperaMax = Ext.getCmp("dc_fecha_operMax");
					var der_montoMin = Ext.getCmp("bMontoMin");
					var der_montoMax = Ext.getCmp("bMontoMax");
					var fechaPublicMin = Ext.getCmp("fecha_publicaMin");
					var fechaPublicMax = Ext.getCmp("fecha_publicaMax");
					var fechaFinVenceMin = Ext.getCmp("fin_fecha_vencMin");
					var fechaFinVenceMax = Ext.getCmp("fin_fecha_vencMax");
					if (!Ext.isEmpty(fechaEmisionMin.getValue()) || !Ext.isEmpty(fechaEmisionMax.getValue()) ) {
						if(Ext.isEmpty(fechaEmisionMin.getValue()))	{
							fechaEmisionMin.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
							fechaEmisionMin.focus();
							return;
						}else if (Ext.isEmpty(fechaEmisionMax.getValue())){
							fechaEmisionMax.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
							fechaEmisionMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaVenceMin.getValue()) || !Ext.isEmpty(fechaVenceMax.getValue()) ) {
						if(Ext.isEmpty(fechaVenceMin.getValue()))	{
							fechaVenceMin.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMin.focus();
							return;
						}else if (Ext.isEmpty(fechaVenceMax.getValue())){
							fechaVenceMax.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaFinVenceMin.getValue()) || !Ext.isEmpty(fechaFinVenceMax.getValue()) ) {
						if(Ext.isEmpty(fechaFinVenceMin.getValue()))	{
							fechaFinVenceMin.markInvalid('Debe capturar ambas fechas de vencimiento del credito o dejarlas en blanco');
							fechaFinVenceMin.focus();
							return;
						}else if (Ext.isEmpty(fechaFinVenceMax.getValue())){
							fechaFinVenceMax.markInvalid('Debe capturar ambas fechas de vencimiento del credito o dejarlas en blanco');
							fechaFinVenceMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaOperaMin.getValue()) || !Ext.isEmpty(fechaOperaMax.getValue()) ) {
						if(Ext.isEmpty(fechaOperaMin.getValue()))	{
							fechaOperaMin.markInvalid('Debe capturar ambas fechas de operaci�n o dejarlas en blanco');
							fechaOperaMin.focus();
							return;
						}else if (Ext.isEmpty(fechaOperaMax.getValue())){
							fechaOperaMax.markInvalid('Debe capturar ambas fechas de operaci�n o dejarlas en blanco');
							fechaOperaMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaPublicMin.getValue()) || !Ext.isEmpty(fechaPublicMax.getValue()) ) {
						if(Ext.isEmpty(fechaPublicMin.getValue()))	{
							fechaPublicMin.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
							fechaPublicMin.focus();
							return;
						}else if (Ext.isEmpty(fechaPublicMax.getValue())){
							fechaPublicMax.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
							fechaPublicMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(izq_montoMin.getValue()) || !Ext.isEmpty(izq_montoMax.getValue()) ) {
						if(Ext.isEmpty(izq_montoMin.getValue()))	{
							izq_montoMin.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							izq_montoMin.focus();
							return;
						}else if (Ext.isEmpty(izq_montoMax.getValue())){
							izq_montoMax.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							izq_montoMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(der_montoMin.getValue()) || !Ext.isEmpty(der_montoMax.getValue()) ) {
						if(Ext.isEmpty(der_montoMin.getValue()))	{
							der_montoMin.markInvalid('Debe capturar ambos montos del credito o dejarlos en blanco');
							der_montoMin.focus();
							return;
						}else if (Ext.isEmpty(der_montoMax.getValue())){
							der_montoMax.markInvalid('Debe capturar ambos montos del credito o dejarlos en blanco');
							der_montoMax.focus();
							return;
						}
					}

					fp.el.mask('Enviando...', 'x-mask-loading');
					
					//Ext.getCmp('btnBajarArchivo').hide();
					//Ext.getCmp('btnBajarXpaginaPDF').hide();
					//Ext.getCmp('btnBajarTotalPDF').hide();
					
					//Ext.getCmp('btnBajarBArchivo').hide();
					//Ext.getCmp('btnBajarBXpaginaPDF').hide();
					//Ext.getCmp('btnBajarBTotalPDF').hide();
					
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar', //Generar datos para la consulta
							start: 0,
							limit: 15
						})
					});
					
					
					
					consultaBData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar', //Generar datos para la consulta
							start: 0,
							limit: 15
						})
					});
					
					
				} //fin handler
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24consulta01ext.jsp';
				}
			}
		]
	});

	function verificaPanelIzq(){
		var myPanel = Ext.getCmp('panelIzq');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

	function verificaPanelDer(){
		var myPanel = Ext.getCmp('panelDer');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid,		gridTotalesA,	NE.util.getEspaciador(20),
			gridB,	gridTotalesB,	NE.util.getEspaciador(10)
		]
	});

//	Carga datos de los combos...
	catalogoEPOData.load();
	catalogoEstatusData.load();
	catalogoMonedaData.load();
	catalogoTipoCobroData.load();
	catalogoIfData.load();
	catalogoModoPlazoData.load();

});