var texto1 = ['Seleccione los documentos para Modalidad 1 '];
var texto2 = ['Al transmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, manifiesta su aceptaci�n para que sea(n) sustituido(s) el(los) DOCUMENTO(S) INICIAL(ES) que ha seleccionado por el(los) DOCUMENTO(S) FINAL(ES) y estos .',
'�ltimos sean descontados a favor de la EMPRESA DE PRIMER ORDEN. <br> Dicho(s) DOCUMENTO(S) FINALE(S) contiene(n) los Derechos de Cobro a su cargo por lo que Usted acepta pagar el 100% de su valor al INTERMEDIARIO FINANCIERO en su fecha de',
'vencimiento. Manifiesta tambi�n su obligaci�n de cubrir al INTERMEDIARIO FINANCIERO, los intereses que se detallan en esta pantalla, en la fecha de su operaci�n, por haber aceptado la sustituci�n.<br/>'+
'Por otra parte, a partir del 17 de octubre de 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que s� ha emitido o emitir� a su CLIENTE o DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.'

];
var texto3 = ['NOTA:  las columnas un subIndice 1 son de Datos Documento Inicial   y subIndice 2 son de Datos Documento Final '];

var texto5 = ['Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
							'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
							'Cd. De M�xico 50-89-61-07. <br>'+
							'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)'];

var  rangoMonto = function(min, max){
	if(Math.min(Math.max(this, min), max))
	rangoMontoText: 'Error en el monto $ '
	return
}
Ext.onReady(function() {

	var fechaHoy = Ext.getDom('fechaHoy').value;	
	var diasInhabiles  = Ext.getDom('diasInhabiles').value;	
	var diasInhabilesXAnio  = Ext.getDom('diasInhabilesXAnio').value;	
    var operaContrato = Ext.getDom('operaContrato').value;
	var columnasI;
	var columnasF;
	var MLineas;
	var Auxiliar;
	var modificados = [];
	var ic_documento = [];
	var ic_moneda_docto = [];
	var ic_moneda_linea = [];
	var montos = [];
	var monto_valuado = [];
	var monto_credito = [];
	var monto_tasa_int = [];
	var monto_descuento = [];
	var tipo_cambio = [];
	var plazo = [];
	var fecha_vto = [];
	var referencia_tasa = [];
	var valor_tasa = [];
	var valor_tasa_puntos = [];
	var ic_tasa = [];
	var cg_rel_mat = [];
	var fn_puntos = [];
	var tipo_tasa = [];
	var puntos_pref = [];
	var ic_epo;
	var hidLimPyme = [];
	var hidLimUtil = [];
	var	totalInteresMN;
	var totalInteresUSD;
	var totalCapitalInteresMN;
	var totalCapitalInteresUSD;
	var datosFinales;
	var EditoPlazo;
	var EditoFecha;
	var Noelementos; 
	var fn_montoValuado =[];
    
    if(operaContrato == "S") {
        texto2 = ['En este acto manifiesto mi aceptaci�n para que sea(n) financiado(s) el(los) DOCUMENTO(S) que he seleccionado, mismo(s) que contiene(n) cuentas pendientes de pago a mi '+
                    'cargo y a favor de la EMPRESA DE PRIMER ORDEN por lo que en esta misma fecha me obligo a cubrir al INTERMEDIARIO FINANCIERO los intereses que se detallan en esta pantalla.'+
                    'Manifiesto tambi�n mi obligaci�n y aceptaci�n de pagar el 100% del valor de(los) DOCUMENTO(S) en la fecha de vencimiento al INTERMEDIARIO FINANCIERO.'];
    }
	
				
	function procesaValoresIniciales(opts, success, response) {		
		var fp = Ext.getCmp('forma');				
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);				
			if (jsonValoresIniciales != null){
				if (jsonValoresIniciales.bloqueoPymeEpo != 'B') {	
					Auxiliar = jsonValoresIniciales.Auxiliar; 				
				}else  {
					ctexto5.show();
					fp.hide();	
				}
			}				
			fp.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var cancelar =  function() {  
	 modificados = [];
	 ic_documento= [];
	 ic_moneda_docto = [];
	 ic_moneda_linea = [];
	 montos = [];
	 monto_valuado = [];
	 monto_credito = [];
	 monto_tasa_int = [];
	 monto_descuento = [];
	 tipo_cambio = [];
	 plazo = [];
	 fecha_vto = [];
	 referencia_tasa = [];
	 valor_tasa = [];
	 valor_tasa_puntos = [];
	 ic_tasa = [];
	 cg_rel_mat = [];
	 fn_puntos = [];
	 tipo_tasa = [];
	 puntos_pref = [];
	 ic_epo='';
	 hidLimPyme = [];
	 hidLimUtil = [];		
	 fn_montoValuado =[];
	}
	
	//****************************************************************************************
	//******validaciones
	//****************************************************************************************
	
	var objeto = 0;	
	var objeto2 = 0;
	var objeto3 = 0;
	var objeto4 = 0;
	var bandera = false;
	var bandera = false;
	var Tmes = new Array();
	var sCadena;
	Tmes[1] = "31"; Tmes[2] ="28"; Tmes[3]="31"; Tmes[4]="30"; Tmes[5]="31"; 
	Tmes[6]="30";
	Tmes[7] = "31"; Tmes[8] ="31"; Tmes[9]="30"; Tmes[10]="31";Tmes[11]="30"; 
	Tmes[12]="31";
	
	var revisaFecha =  function(campoFecha) {
		if (!isdate(campoFecha) && campoFecha!='') {
			Ext.MessageBox.alert("Mensaje","La fecha es incorrecta.\nVerifique que el formato sea dd/mm/aaaa");
			return false;
		}else {
			return true;
		}
	}
	
	var roundOff =  function(valor, precision) {  
  valor = "" + valor //convert value to string
  precision = parseInt(precision);
  var iBandera = 0;  
  if (valor < 0)  {
   valor = Math.abs(valor);
   iBandera = 1;
  }   
  var whole = "" + Math.round(valor * Math.pow(10, precision));
  var decPoint  = whole.length - precision;
  if(decPoint > 0)  {
   result = whole.substring(0, decPoint);
   result += ".";
   result += whole.substring(decPoint, whole.length);
  }  else   {
   if (decPoint < 0)    {
    for (i = decPoint; i<0; i++)
    whole = "0" + whole;
   }
   whole = "0." + whole;
   result = whole;
  }
  if (isNaN(result) ||result == ".0")
         return "0.0";
      if(result.charAt(0) == ".")
       result = "0" + result;
  if (iBandera == 1)
   result = "-" + result;
  return result;
 }
 



//ESTA FUNCION VA EN LA CONSULTA 
var inicializaTasasXPlazo =  function( valor, metadata, registro, info) { 
	var plazo 				= registro.data['PLAZO_CREDITO'];
	if(plazo!='') {	
	return obtenTasaXPlazoIni( valor, metadata, registro, info);
	}else {
		return '0';
	}
	
}
//var obtenTasaXPlazoIni = function obtenTasaXPlazo( fechaVenCredito,numTasas, info, plazo,referenciaTasa,valorTasa,icMoneda,icIf,icTasa,cgRelMat,fnPuntos,tipoPiso,montoAuxInt,montoCredito,montoInt,montoCapitalInt,valorTasaPuntos,tipoTasa,puntosPref){
var obtenTasaXPlazoIni =  function( valor, metadata, registro, info) { 
	var i=0;
	var auxIcIf;
	var auxReferenciaTasa;
	var auxIcTasa;
	var auxValorTasa;
	var auxValorTasaPuntos;
	var auxIcMoneda;
	var auxPlazoDias;
	var auxTipoTasa;
	var auxPuntosPref;	
	var iPlazoTasa;
	var iPlazoTasaAux = 9999999999;
	var ok; 
	var plazo;
	var referenciaTasa;
	var valorTasa;
	var icMoneda;
	var icIf;
	var icTasa;
	var cgRelMat;
	var fnPuntos;
	var tipoPiso;
	var montoAuxInt;
	var montoCredito;
	var montoInt;
	var montoCapitalInt;
	var valorTasaPuntos;
	var puntosPref;
	var tipoTasa;	
	plazo 				= registro.data['PLAZO_CREDITO'];
	referenciaTasa		= registro.data['REFERENCIA_TASA']; 
	valorTasa			= registro.data['VALOR_TASA'];  
	icMoneda			= registro.data['IC_MONEDA_LINEA']; 
	icIf				  = registro.data['IC_IF'];
	icTasa				= registro.data['IC_TASA']; 
	cgRelMat			= registro.data['CG_REL_MAT']; 
	fnPuntos			= registro.data['FN_PUNTOS']; 
	tipoPiso			= registro.data['TIPO_PISO']; 
	montoAuxInt			= registro.data['MONTO_AUX_INT']; 
	montoCredito		= registro.data['MONTO_CREDITO']; 
	montoInt			= registro.data['MONTO_TASA_INT'];  
	montoCapitalInt 	= registro.data['MONTO_CAPITAL_INT'];
	valorTasaPuntos		= registro.data['VALOR_TASA_PUNTOS'];  
	tipoTasa			= registro.data['TIPO_TASA'];
	puntosPref			= registro.data['PUNTOS_PREF'];
	var fechaVenCredito = registro.data['FECHA_VEN_CREDITO'];
	var	numTasas = parseInt(registro.data['HIDNUMTASAS']);	
	var ic_documento =   registro.data['IG_DOCUMENTO']; 
	var iPlazoCredito = parseInt(plazo, 10);
	
	
	if(numTasas==1)  {
		auxIcIf			= eval('Auxiliar.auxIcIf1');      
		auxReferenciaTasa	=  eval('Auxiliar.auxReferenciaTasa1'); 
		auxIcTasa			= eval('Auxiliar.auxIcTasa1');    
		auxCgRelMat		= eval('Auxiliar.auxCgRelMat1'); 
		auxFnPuntos		= eval('Auxiliar.auxFnPuntos1');  
		auxValorTasa		= eval('Auxiliar.auxValorTasa1');  
		auxIcMoneda		= eval('Auxiliar.auxIcMoneda1');  
		auxPlazoDias		= eval('Auxiliar.auxPlazoDias1');   
		auxValorTasaPuntos		= eval('Auxiliar.auxValorTasaPuntos1');   
		auxTipoTasa		= eval('Auxiliar.auxTipoTasa1'); 
		auxPuntosPref		= eval('Auxiliar.auxPuntosPref1');
		iPlazoTasa			= parseInt(auxPlazoDias, 10);
		//alert(icMoneda+"=="+auxIcMoneda+"----"+iPlazoCredito+"<="+iPlazoTasa+"--"+iPlazoTasa+"<="+iPlazoTasaAux+"--"+auxIcIf+"=="+icIf);
		if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
			iPlazoTasaAux			= iPlazoTasa;
			referenciaTasa	= auxReferenciaTasa;		 	
			valorTasa			= auxValorTasa; 
			icTasa			= auxIcTasa;
			cgRelMat			= auxCgRelMat;
			fnPuntos			= auxFnPuntos;
			valorTasaPuntos	= auxValorTasaPuntos; 
			tipoTasa			= auxTipoTasa;
			puntosPref		= auxPuntosPref;			
			if(tipoPiso=='1'){
				montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360,2)*100)/100;
			}else{
				montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
			}
			montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
			montoCapitalInt = roundOff(parseFloat(montoInt)+parseFloat(montoCredito),2);
		 }		
		}else {				
			for(i=1;i<=numTasas;i++){
				auxIcIf			= eval('Auxiliar.auxIcIf'+i);      
				auxReferenciaTasa	=  eval('Auxiliar.auxReferenciaTasa'+i); 
				auxIcTasa			= eval('Auxiliar.auxIcTasa'+i);    
				auxCgRelMat		= eval('Auxiliar.auxCgRelMat'+i); 
				auxFnPuntos		= eval('Auxiliar.auxFnPuntos'+i);  
				auxValorTasa		= eval('Auxiliar.auxValorTasa'+i);  
				auxIcMoneda		= eval('Auxiliar.auxIcMoneda'+i);  
				auxPlazoDias		= eval('Auxiliar.auxPlazoDias'+i);   
				auxValorTasaPuntos		= eval('Auxiliar.auxValorTasaPuntos'+i);   
				auxTipoTasa		= eval('Auxiliar.auxTipoTasa'+i); 
				auxPuntosPref		= eval('Auxiliar.auxPuntosPref'+i); 
				iPlazoTasa			= parseInt(auxPlazoDias, 10);
				//alert(ic_documento +"---"+icMoneda+"=="+auxIcMoneda+"----"+iPlazoCredito+"<="+iPlazoTasa+"--"+iPlazoTasa+"<="+iPlazoTasaAux+"--"+auxIcIf+"=="+icIf);
				if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
					//alert("Si entro en el if "+iPlazoTasa + "iplazotasaaux = "+iPlazoTasaAux);iPlazoTasa + "iplazotasaaux = "+iPlazoTasaAux);
				 	iPlazoTasaAux 			= iPlazoTasa;
					referenciaTasa	= auxReferenciaTasa;		 	
					valorTasa 		= auxValorTasa; 			
					icTasa			= auxIcTasa;
					cgRelMat			= auxCgRelMat;
					fnPuntos			= auxFnPuntos;
					valorTasaPuntos	= auxValorTasaPuntos; 				
					tipoTasa			= auxTipoTasa;
					puntosPref		= auxPuntosPref;				
					//alert("Si entro en el if "+iPlazoTasa + "iplazotasaaux = "+iPlazoTasaAux);					
					if(tipoPiso=='1'){
						montoAuxInt = parseFloat(roundOff((parseFloat(montoCredito) *(parseFloat(valorTasaPuntos)/100)/360)*100,2))/100;
					}else{
						montoAuxInt = (parseFloat(montoCredito)*(parseFloat(valorTasaPuntos)/100)/360);
					}
					montoInt= roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
					montoCapitalInt = roundOff(parseFloat(montoInt)+parseFloat(montoCredito),2);
					//alert("entro "+ic_documento +"---"+cgRelMat);
				}				
			}
		}			
		
		if(info =='ValorTasaInteres' ) { ok = valorTasaPuntos;    }
		if(info =='MontodeIntereses' ) { ok = montoInt; 	}
		if(info =='MontoCapitalInteres' ) { ok = montoCapitalInt; }
		if(info =='ReferenciaTasa' ) { ok = referenciaTasa; }		
		if(info =='ic_tasa') { ok = icTasa; }
		if(info =='fechaVenciE' ) { ok = fechaVenCredito;  	}
		if(info =='montoAuxi' ) { ok = montoAuxInt; }	
		if(info =='puntos' ) { ok = fnPuntos; }	
		if(info =='CgRelMat') { ok = cgRelMat; }	
		
		return ok;	
}

	var sumaFecha =  function(opts, success, response, record) {
		var dias_minimo  = record.data['DIAS_MINIMOS'];	
		var dias_maximo  = record.data['DIAS_MAXIMO'];	
		var dias_max_fv  = record.data['DIAS_MAXFV'];	
			
		var fecha_venc_max  = Ext.util.Format.date(record.data['FECHA_VENC_MAX'],'d/m/Y'); 		
		var TDate = new Date();
		var txtFechaHoy  =  fechaHoy;	
		var dia = 	parseInt(txtFechaHoy.substring(0,2), 10);
		var mes = 	parseInt(txtFechaHoy.substring(3,5), 10) - 1;
		var anio = 	parseInt(txtFechaHoy.substring(6,10), 10)
		TDate.setFullYear(anio);
		TDate.setMonth(mes);
		TDate.setDate(dia);
		var diaMes = TDate.getDate();
		var txtPlazo;
		var fechaVto;
		var montoTasaInt;
		var montAuxInt;
		var montoCredito;
		var montoDocto;
		var monto;
		var igPlazoDocto;	
		var plazo;
		var referenciaTasa;	
		var valorTasa;
		var monedaLinea;
		var icIf;
		var tipoPiso;
		var icTasa;
		var cgRelMat;
		var fnPuntos;
		var montoCapitalInt;
		var valorTasaPuntos;
		var tipoTasa;
		var puntosPref;
		var fechaEmision;
		var mensaje = true;
		var  ic_moneda = record.data['IC_MONEDA_DOCTO']; 
		var  ic_tipoFinanciamiento = record.data['IC_TIPO_FINANCIAMIENTO'];   
		txtPlazo		= record.data['PLAZO_CREDITO'];
		fechaVto		= Ext.util.Format.date(record.data['FECHA_VEN_CREDITO'],'d/m/Y'); 
		fechaEmision	=  Ext.util.Format.date(record.data['DF_FECHA_EMISION'],'d/m/Y');  
		montoInt		= record.data['MONTO_TASA_INT'];  
		montoCapitalInt	=  record.data['MONTO_CAPITAL_INT'];   
		montoAuxInt		= record.data['MONTO_AUX_INT'];  
		monto			= record.data['FN_MONTO'];   
		montoCredito	= record.data['MONTO_CREDITO']; 
		montoDocto 		= record.data['MONTO_VALUADO']; 
		igPlazoDocto 	= record.data['IG_PLAZO_DOCTO']; 	
		referenciaTasa	= record.data['REFERENCIA_TASA'];  
		monedaLinea		= record.data['IC_MONEDA_LINEA']; 
		icIf		= record.data['IC_IF'];
		valorTasa		= record.data['VALOR_TASA'];  
		tipoPiso		=  record.data['TIPO_PISO'];   
		icTasa			= record.data['IC_TASA'];  
		cgRelMat		= record.data['CG_REL_MAT']; 
		fnPuntos		=  record.data['FN_PUNTOS'];  
		valorTasaPuntos	= record.data['VALOR_TASA_PUNTOS'];  
		tipoTasa 		= record.data['TIPO_TASA']; 			
		puntosPref		= record.data['PUNTOS_PREF'];  
		
		if(txtPlazo =='')
			return false;
		if(objeto3==0)
			objeto3 = 1;			
		if(!obtenTasaXPlazo(opts, success, response, record)) {	
			if(objeto3==1){
				Ext.MessageBox.alert("Mensaje","El documento no puede ser seleccionado con ese plazo porque no se encontro la tasa correspondiente");
				return false;
				mensaje = false;
				bandera = true;
			}else{
				return true ;
			}
		}else{
			objeto3=0;
		}			
		var AddDays = 0;
		if(txtPlazo!=''){			
			AddDays = txtPlazo;				
			if(ic_moneda ==54  &&  ic_tipoFinanciamiento ==2   )  {
				AddDays=  parseInt(AddDays)+1;
			}
			diaMes = parseInt(diaMes, 10)+ parseInt(AddDays, 10);
			TDate.setDate(diaMes);
			var CurYear = TDate.getFullYear();
			var CurDay = TDate.getDate();
			var CurMonth = TDate.getMonth()+1;
			if(CurDay<10)
				CurDay = '0'+CurDay;
			if(CurMonth<10)
				CurMonth = '0'+CurMonth;
			TheDate = CurDay +'/'+CurMonth+'/'+CurYear;
			fechaVto = CurDay +'/'+CurMonth+'/'+CurYear; //edita la Fecha de  FECHA_VEN_CREDITO
			montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(txtPlazo),2); //edita la Fecha de  MONTO_TASA_INT
			plazo = parseInt(txtPlazo, 10);			
			record.data['FECHA_VEN_CREDITO']=  fechaVto; 
			record.commit();
			
			if(validaFechaLineaCredito('', '', '', record)==false) {		 //F020-2015	
				if(record.data['SELECCION']=='S')  {
					record.data['SELECCION']='N';
					record.commit();					
				}
				return false;
			} 
			
			dias_maximo = dias_maximo - mtdCalculaDias(fechaEmision,fechaHoy);					
			if(objeto==0)
				objeto = 1;						
			if(plazo<dias_minimo){
				if(objeto==1){
					if(mensaje)
						Ext.MessageBox.alert("Mensaje","Excede el plazo m�nimo de financiamiento autorizado = "+dias_minimo);
						return false ;
					mensaje = false;
					bandera = true;
				}else{					
					return true;
				}
			}else if(plazo>dias_maximo){
				if(objeto==1){
					if(mensaje)
						Ext.MessageBox.alert("Mensaje","Excede el plazo m�ximo de financiamiento autorizado+ = "+dias_maximo);					
						return false;
					mensaje = false;
					bandera = true;
				}else				
					return true;
			}else{
				objeto=0;
			}
		
		if(objeto2==0)
			objeto2 = 1;
		var diasInhabiles2 =diasInhabiles;	
		var diaMes = CurDay +'/'+CurMonth;
		
		var diasInhabilesXAnio2 =diasInhabilesXAnio; //F09-2015		
		var diaMesAnio = fechaVto;
		
		//alert("fechaVto "+fechaVto+" diasInhabiles "+diasInhabiles +" diaMes == "+diaMes +" TDate.getDay()"+TDate.getDay());
		if( (diasInhabiles2.indexOf(diaMes)>0||TDate.getDay()==0||TDate.getDay()==6) || (diasInhabilesXAnio2.indexOf(diaMesAnio)>0||TDate.getDay()==0||TDate.getDay()==6)    ){
			if(objeto2==1){
				if(mensaje)
				Ext.MessageBox.alert("Mensaje","La Fecha de Vencimiento del Cr�dito es un d�a inh�bil");				
				return false ;
				mensaje = false;
				bandera = true;
			}else				
				return true;
		}else{
			objeto2 = 0;
		}
		var diferencia = datecomp(fechaVto, fecha_venc_max);				
		if(mensaje&&diferencia==1&&objeto2==0&&objeto4==0) {
			Ext.MessageBox.alert("Mensaje","Excede el plazo m�ximo de financiamiento autorizado para la pyme = "+dias_max_fv);
			if(objeto2==0) {			
				return false ;				
				bandera = true;							
			} else {			
				return true;
				bandera = true;							
			}
			objeto4 = 1;
			bandera = true;
			return false;			
		} else {
			objeto4 = 0;
			return true;
		}
		if(mensaje&&!limPyme(opts, success, response, record)) {
			bandera = true;
			return false;			
		}
	 }else{
	 	bandera = false;
	 }
	record.data['PLAZO_CREDITO']=plazo;
	record.data['MONTO_TASA_INT']=montoInt;
	
		
	}
	//funcion de obtenTasaXPlazo
	var obtenTasaXPlazo =  function(opts, success, response, record) {
		var plazo		= record.data['PLAZO_CREDITO'];
		var fechaVto		= Ext.util.Format.date(record.data['FECHA_VEN_CREDITO'],'d/m/Y');  
		var fechaEmision	=  Ext.util.Format.date(record.data['DF_FECHA_EMISION'],'d/m/Y');  
		var montoInt		= record.data['MONTO_TASA_INT'];  
		var montoCapitalInt	=  record.data['MONTO_CAPITAL_INT'];   
		var montoAuxInt		= record.data['MONTO_AUX_INT'];  
		var monto			= record.data['FN_MONTO'];   
		var montoCredito	= record.data['MONTO_CREDITO']; 
		var montoDocto 		= record.data['MONTO_VALUADO']; 
		var igPlazoDocto 	= record.data['IG_PLAZO_DOCTO']; 	
		var referenciaTasa	= record.data['REFERENCIA_TASA']; 
		var icMoneda		= record.data['IC_MONEDA_LINEA']; 
		var icIf		= record.data['IC_IF']; 
		var valorTasa		= record.data['VALOR_TASA'];  
		var tipoPiso		=  record.data['TIPO_PISO'];   
		var icTasa			= record.data['IC_TASA'];  
		var cgRelMat		= record.data['CG_REL_MAT']; 
		var fnPuntos		=  record.data['FN_PUNTOS'];  
		var valorTasaPuntos	= record.data['VALOR_TASA_PUNTOS'];  
		var tipoTasa 		= record.data['TIPO_TASA']; 			
		var puntosPref		= record.data['PUNTOS_PREF'];  
		var	numTasas = parseInt(record.data['HIDNUMTASAS']);
		var fechaVenCredito =Ext.util.Format.date(record.data['FECHA_VEN_CREDITO'],'d/m/Y');   
		
		var i=0;
		var auxIcIf;
		var auxReferenciaTasa;
		var auxIcTasa;
		var auxValorTasa;
		var auxValorTasaPuntos;
		var auxIcMoneda;
		var auxPlazoDias;
		var auxTipoTasa;
		var auxPuntosPref;
		var iPlazoCredito = parseInt(plazo, 10);
		var iPlazoTasa;
		var iPlazoTasaAux = 9999999999;
		var ok = false;	
		var auxCgRelMat;
			
		
		if(numTasas==1)  {
			auxIcIf			= eval('Auxiliar.auxIcIf1');      
			auxReferenciaTasa	=  eval('Auxiliar.auxReferenciaTasa1'); 
			auxIcTasa			= eval('Auxiliar.auxIcTasa1');    
			auxCgRelMat		= eval('Auxiliar.auxCgRelMat1'); 
			auxFnPuntos		= eval('Auxiliar.auxFnPuntos1');  
			auxValorTasa		= eval('Auxiliar.auxValorTasa1');  
			auxIcMoneda		= eval('Auxiliar.auxIcMoneda1');  
			auxPlazoDias		= eval('Auxiliar.auxPlazoDias1');   
			auxValorTasaPuntos		= eval('Auxiliar.auxValorTasaPuntos1');   
			auxTipoTasa		= eval('Auxiliar.auxTipoTasa1'); 
			auxPuntosPref		= eval('Auxiliar.auxPuntosPref1');
			iPlazoTasa			= parseInt(auxPlazoDias, 10);
			//alert(icMoneda+"=="+auxIcMoneda+"----"+iPlazoCredito+"<="+iPlazoTasa+"--"+iPlazoTasa+"<="+iPlazoTasaAux+"--"+auxIcIf+"=="+icIf);
			if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
				iPlazoTasaAux			= iPlazoTasa;
				referenciaTasa	= auxReferenciaTasa;		 	
				valorTasa			= auxValorTasa; 
				icTasa			= auxIcTasa;
				cgRelMat			= auxCgRelMat;
				fnPuntos			= auxFnPuntos;
				valorTasaPuntos	= auxValorTasaPuntos; 
				tipoTasa			= auxTipoTasa;
				puntosPref		= auxPuntosPref;
				ok = true;
				if(tipoPiso=='1'){
					montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360,2)*100)/100;
				}else{
					montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
				}
				montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
				montoCapitalInt = roundOff(parseFloat(montoInt)+parseFloat(montoCredito),2);
			 }		
			}else {				
				for(i=1;i<=numTasas;i++){
					auxIcIf			= eval('Auxiliar.auxIcIf'+i);      
					auxReferenciaTasa	=  eval('Auxiliar.auxReferenciaTasa'+i); 
					auxIcTasa			= eval('Auxiliar.auxIcTasa'+i);    
					auxCgRelMat		= eval('Auxiliar.auxCgRelMat'+i); 
					auxFnPuntos		= eval('Auxiliar.auxFnPuntos'+i);  
					auxValorTasa		= eval('Auxiliar.auxValorTasa'+i);  
					auxIcMoneda		= eval('Auxiliar.auxIcMoneda'+i);  
					auxPlazoDias		= eval('Auxiliar.auxPlazoDias'+i);   
					auxValorTasaPuntos		= eval('Auxiliar.auxValorTasaPuntos'+i);   
					auxTipoTasa		= eval('Auxiliar.auxTipoTasa'+i); 
					auxPuntosPref		= eval('Auxiliar.auxPuntosPref'+i); 
					iPlazoTasa			= parseInt(auxPlazoDias, 10);
			
			//alert("i ="+i+"   "+icMoneda+"=="+auxIcMoneda+"----"+iPlazoCredito+"<="+iPlazoTasa+"--"+iPlazoTasa+"<="+iPlazoTasaAux+"--"+auxIcIf+"=="+icIf);
			 if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
				//alert("Si entro en el if "+iPlazoTasa + "iplazotasaaux = "+iPlazoTasaAux);
			 	iPlazoTasaAux 			= iPlazoTasa;
				referenciaTasa	= auxReferenciaTasa;		 	
				valorTasa 		= auxValorTasa; 			
				icTasa			= auxIcTasa;
				cgRelMat			= auxCgRelMat;
				fnPuntos			= auxFnPuntos;
				valorTasaPuntos	= auxValorTasaPuntos; 				
				tipoTasa			= auxTipoTasa;
				puntosPref		= auxPuntosPref;
				ok = true;	
				if(tipoPiso=='1'){
					montoAuxInt = parseFloat(roundOff((parseFloat(montoCredito) *(parseFloat(valorTasaPuntos)/100)/360)*100,2))/100;
				}else{
					montoAuxInt = (parseFloat(montoCredito)*(parseFloat(valorTasaPuntos)/100)/360);
				}
				montoInt= roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
				montoCapitalInt = roundOff(parseFloat(montoInt)+parseFloat(montoCredito),2);
			 }
			}
		}
		record.data['MONTO_TASA_INT']=montoInt;
		record.data['MONTO_CAPITAL_INT']=montoCapitalInt;
		record.data['VALOR_TASA_PUNTOS']=valorTasaPuntos;
		record.data['REFERENCIA_TASA']=referenciaTasa;
		record.data['CG_REL_MAT']=auxCgRelMat;
	//	record.data['FECHA_VEN_CREDITO']=fechaVenCredito;
		record.commit();	
		
	return ok;
	
}

	var fnDesentrama =  function( Cadena) { 
   var Dato;
   sCadena      = Cadena
   tamano       = sCadena.length;
   posicion     = sCadena.indexOf("/");
   Dato         = sCadena.substr(0,posicion);
   cadenanueva  = sCadena.substring(posicion + 1,tamano);
   sCadena      = cadenanueva;
   return Dato;
}
	//mtdCalculaDias
		var mtdCalculaDias =  function( sFecha,sFecha2) {
		var iResAno,DiaMes,DiasDif=0,iCont=0,iMes,DiaMesA,iBan=0;
    var DiasPar,Dato,VencDia,VencMes,AltaMes,AltaDia;
    sCadena   = sFecha
    AltaDia   = fnDesentrama(sCadena);
    AltaMes  = fnDesentrama(sCadena);
    AltaAno   = sCadena;
    sCadena     = sFecha2
    VencDia     = fnDesentrama(sCadena);
    VencMes     = fnDesentrama(sCadena);
    VencAno     = sCadena;
    Dato  = AltaMes.substr(0,1);
    if (Dato ==0){
			AltaMes = AltaMes.substr(1,2)
		}	
    Dato  = AltaDia.substr(0,1);
    if (Dato ==0){
			AltaDia = AltaDia.substr(1,2)
    }
    Dato  = VencDia.substr(0,1);
    if (Dato ==0){
			VencDia = VencDia.substr(1,2)
    }
    Dato  = VencMes.substr(0,1);
		if (Dato ==0){
			VencMes = VencMes.substr(1,2)
    }
    while (VencAno != AltaAno || VencMes != AltaMes || VencDia != AltaDia){
			iCont++;
      while(AltaMes != VencMes || VencAno != AltaAno){
				iBan=1;
			  iResAno = AltaAno % 4;
			  if (iResAno==0){
					Tmes[2] =29;
			  }else{
					Tmes[2] =28;
			  }						
			  if (iCont == 1){
					DiaMesA = Tmes[AltaMes];
			    DiasDif = DiaMesA - AltaDia;
			  } else{
					DiaMesA  = Tmes[AltaMes];
			    DiasDif += parseInt(DiaMesA);
			  }			
			  if (AltaMes==12){
					AltaAno++;
			    AltaMes=1;
			  }else{
					AltaMes++;
			  }
			  iCont++;			
			} // fin de while
			
      if (AltaMes == VencMes && VencAno == AltaAno){
				if (iBan==0){
					DiasDif = parseInt(VencDia) - parseInt(AltaDia);
					AltaDia = VencDia;
        } else if (iBan=1){
					DiasDif += parseInt(VencDia);
          AltaDia = VencDia;
        }
      }
		}
		
		return DiasDif;
	}
	
	var restaFecha =  function(opts, success, response, record) {

	var txtPlazo;
	var fechaVto;
	var montoTasaInt;
	var montAuxInt;
	var montoCredito;
	var montoDocto;
	var plazo;	
	var igPlazoDocto;				
	var plazo;
	var referenciaTasa;	
	var valorTasa;
	var monedaLinea;
	var icIf;
	var tipoPiso;
	var icTasa;
	var cgRelMat;
	var fnPuntos;
	var montoCapitalInt;	
	var valorTasaPuntos;	
	var fechaEmision;
	var tipoTasa;
	var puntosPref;
	var mensaje = true;
	var  ic_moneda = record.data['IC_MONEDA_DOCTO'];
	var  ic_tipoFinanciamiento = record.data['IC_TIPO_FINANCIAMIENTO'];  
	txtPlazo		= record.data['PLAZO_CREDITO'];
	fechaEmision	=   Ext.util.Format.date(record.data['DF_FECHA_EMISION'],'d/m/Y'); 
	montoInt		= record.data['MONTO_TASA_INT'];  
	montoCapitalInt	=  record.data['MONTO_CAPITAL_INT'];   
	montoAuxInt		= record.data['MONTO_AUX_INT'];  
	monto			= record.data['FN_MONTO'];   
	montoCredito	= record.data['MONTO_CREDITO']; 
	montoDocto 		= record.data['MONTO_VALUADO']; 
	igPlazoDocto 	= record.data['IG_PLAZO_DOCTO']; 	
	referenciaTasa	= record.data['REFERENCIA_TASA'];  
	monedaLinea		= record.data['IC_MONEDA_LINEA']; 
	icIf		= record.data['IC_IF'];
	valorTasa		= record.data['VALOR_TASA'];  
	tipoPiso		=  record.data['TIPO_PISO'];   
	icTasa			= record.data['IC_TASA'];  
	cgRelMat		= record.data['CG_REL_MAT']; 
	fnPuntos		=  record.data['FN_PUNTOS'];  
	valorTasaPuntos	= record.data['VALOR_TASA_PUNTOS'];  
	tipoTasa 		= record.data['TIPO_TASA']; 			
	puntosPref		= record.data['PUNTOS_PREF'];  
	var fechaHoy2 = fechaHoy;
	var diasInhabiles2 = diasInhabiles;	
	var diasInhabilesXAnio2 =diasInhabilesXAnio; //F09-2015
	var dias_maximo = record.data['DIAS_MAXIMO']; 
	var dias_minimo = record.data['DIAS_MINIMOS']; 	
	var fechaVto =Ext.util.Format.date(record.data['FECHA_VEN_CREDITO'],'d/m/Y'); 
	var fecha_venc_max = Ext.util.Format.date(record.data['FECHA_VENC_MAX'],'d/m/Y');  
	var dias_max_fv  = record.data['DIAS_MAXFV'];		

	record.data['FECHA_VEN_CREDITO']=fechaVto;	
	record.commit();	
	
	if(validaFechaLineaCredito('', '', '', record)==false) {		 //F020-2015
		if(record.data['SELECCION']=='S')  {
			record.data['SELECCION']='N';
			record.commit();					
			return false;
		}
	} 
		
	if(fechaVto!=null){	
	if(datecomp(diasInhabiles2,fechaVto)==1 ||  datecomp(diasInhabilesXAnio2,fechaVto)==1   ){
		Ext.MessageBox.alert("Mensaje","La fecha de vencimiento del cr�dito debe ser mayor a hoy ");
		return false;
		mensaje = false;
		bandera = true;
	}		
	txtPlazo = mtdCalculaDias(fechaHoy2,fechaVto);	
	dias_maximo = dias_maximo - mtdCalculaDias(fechaEmision,fechaHoy2);
	}
	
	if(objeto3==0)
		objeto3 = 2;
		if(!obtenTasaXPlazo(opts, success, response, record)) {
			if(objeto3==2){
				if(mensaje){
					Ext.MessageBox.alert("Mensaje","El documento no puede ser seleccionado con ese plazo porque no se encontro la tasa correspondiente");				
					return false; 
				}
				mensaje = false;
				bandera = true;
			}else{			
				bandera = true;
			}
		}else{
			objeto3=0;
		}
		if(ic_moneda ==54  &&  ic_tipoFinanciamiento ==2   )  {
			txtPlazo	= txtPlazo-1;
		}
		
		plazo = parseInt(txtPlazo, 10);		
		record.data['PLAZO_CREDITO']=plazo;
		record.commit();
	
		if(objeto==0)
			objeto = 2;
		if(plazo<dias_minimo){
			if(objeto==2){
				if(mensaje){
					Ext.MessageBox.alert("Mensaje","Excede el plazo m�nimo de financiamiento autorizado = "+dias_minimo);	
					return false; 
				}
				mensaje = false;
				bandera = true;
			}else{
				bandera = true;
			}
		}else if (plazo>dias_maximo){
			if(objeto==2){
				if(mensaje){
					Ext.MessageBox.alert("Mensaje","Excede el plazo m�ximo de financiamiento autorizado = "+dias_maximo);				
					return false;
				}
				mensaje = false;
				bandera = true;
			}else{				
				bandera = true;	
			}
		}else{
			objeto = 0;
		}
		if(objeto2==0)
			objeto2 = 2;
		var diasInhabiles2 = diasInhabiles;
		var diasInhabilesXAnio2 =diasInhabilesXAnio; //F09-2015
		var diaMes = fechaVto.substring(0,5);
		var dia = fechaVto.substring(0,2);
		var mes = parseInt(fechaVto.substring(3,5), 10)-1;
		var anio = fechaVto.substring(6,10);
		
		var diaMesAnio = fechaVto;
		
		var TDate = new Date();
		TDate.setDate(1);
		TDate.setFullYear(anio);
		TDate.setMonth(mes);	
		TDate.setDate(dia);
		
		if( (  diasInhabiles2.indexOf(diaMes)>0||TDate.getDay()==0||TDate.getDay()==6 ) ||   (  diasInhabilesXAnio2.indexOf(diaMesAnio)>0||TDate.getDay()==0||TDate.getDay()==6 ) ){
			if(mensaje){
				Ext.MessageBox.alert("Mensaje","La Fecha de Vencimiento del Cr�dito es un d�a inh�bil");	
				return false;
			}
			mensaje = false;
			bandera = true;
			
		}else{
			objeto2=0;
		}
		
		var diferencia = datecomp(fechaVto, fecha_venc_max);
		if(mensaje&&diferencia==1&&objeto2==0&&objeto4==0) {
			Ext.MessageBox.alert("Mensaje","Excede el plazo m�ximo de financiamiento autorizado para la pyme = "+dias_max_fv);
			if(objeto2==0) {				
				bandera = true;
				return;
			} else {				
				bandera = true;
				return;
			}
			objeto4 = 1;
			bandera = true;
			return false;
		} else {
			objeto4 = 0;
		}
		if(mensaje&&!limPyme(opts, success, response, record)) {
			bandera = true;
			return false;			
		}else{
		bandera = false;
		}		
					
	}	
	
	
	//Funtionm de limPyme
	var limPyme =  function( opts, success, response, record, bOk) {
		var limitePyme = record.data['LIMITEPYME'];
		var limiteUtil = record.data['LIMITE_ULTILI_PYME'];
		var montoCapInt		= record.data['MONTO_CAPITAL_INT'];
		var mensaje_terminacion = record.data['MENSAJE_TERMINACION'];
		if(limitePyme!="") {
			var limite 		= parseFloat(limitePyme,10);
			var util 		= parseFloat(limiteUtil,10);
			var mtoCapInt 	= parseFloat(montoCapInt,10);
			var disponible 	= limite - util;
			if(disponible<mtoCapInt) {
				if(mensaje_terminacion == '')
					Ext.MessageBox.alert("Mensaje","Por el momento no es posible seleccionar los documentos,por favor llame al 01-800-NAFINSA para cualquier aclaraci�n");
				else
					Ext.MessageBox.alert("Mensaje"," "+mensaje_terminacion);
				return false;
			} else {
				limiteUtil = roundOff(util + mtoCapInt,2);
				return true;
			}		
	}//if(f.limitePyme!="")
	return true;
}

	var calculaMontos =  function(opts, success, response, record) {
		var numLineas = record.data['HIDNUMLINEAS'];  
		var tipoCambio = '';
		var valorTipoCambio=  0;
		var auxMonto		= 0;
		var ok = true;
		var i = 0;
		var montoSeleccionado =0;
		var numDoctos =0;	
		var saldoDisponible= 0;
		var elementos  = Noelementos;
			
		if(tipoCambio!='')
			valorTipoCambio = parseFloat(tipoCambio);
		//cuando una linea 		
			
		if(numLineas==1){
			if(record.data['IC_LINEA_CREDITO_DM']== eval('MLineas.ic_linea0')  ){
				auxMonto = parseFloat(record.data['MONTO_VALUADO']);
				if(auxMonto>0){
					numDoctos= eval('MLineas.numDoctos0')+1;
					montoSeleccionado =roundOff(parseFloat(eval('MLineas.montoSeleccionado0'))+auxMonto,2);				
					saldoDisponible =roundOff(parseFloat(eval('MLineas.saldoDisponible0'))-auxMonto,2);								
				}
				if( parseInt(saldoDisponible)<0){							
						Ext.MessageBox.alert("Mensaje","Por el momento alguno(s) de los documento(s) no puede ser seleccionado. Favor de comunicarse al centro de atenci�n a clientes NAFIN");							
					ok = false;	
				}
			}
				//cuando hay mas de una linea
		}else if(numLineas>1){
			for(i=0;i<numLineas;i++){
				montoSeleccionado = 0.0;
				numDoctos = 0;		
				saldoDisponible = (eval('MLineas.hidSaldoDisponible'+i)); 				
				if(record.data['IC_LINEA_CREDITO_DM']== eval('MLineas.ic_linea'+i)  ){
					auxMonto = parseFloat(record.data['MONTO_VALUADO']);  
					if(auxMonto>0){
						numDoctos =parseInt(eval('MLineas.numDoctos'+i))+1;
						montoSeleccionado = roundOff(parseFloat(eval('MLineas.montoSeleccionado'+i))+auxMonto,2);				
						saldoDisponible =roundOff(parseFloat(eval('MLineas.saldoDisponible'+i))-auxMonto,2);	
					}					
					if( parseInt(saldoDisponible)<0){						
							Ext.MessageBox.alert("Mensaje","Por el momento alguno(s) de los documento(s) no puede ser seleccionado. Favor de comunicarse al centro de atenci�n a clientes NAFIN");
							ok = false;						
					}					
				}
			}
		}
	return ok;
}


	var sumaTotalesIntereses =  function(opts, success, response, record) {
		var totalMN 		= 0.00;
		var totalUSD		= 0.00;
		var totalCapitalMN 	= 0.00;
		var totalCapitalUSD = 0.00;	
		var montoTasaInt;
		var montoCapitalInt;
		var icMoneda;
		var monedaLinea;
		var i;
		montoTasaInt	= parseFloat(record.data['MONTO_TASA_INT']);  
		montoCapitalInt	=parseFloat(record.data['MONTO_CAPITAL_INT']); 
		icMoneda 	=  record.data['IC_MONEDA_DOCTO'];     
		monedaLinea	= record.data['IC_MONEDA_LINEA'] ; 
		if(monedaLinea=='1'){
			totalMN += montoTasaInt;
			totalCapitalMN	+= montoCapitalInt;
		}else if(icMoneda=='54'&&monedaLinea=='54'){
			totalUSD += montoTasaInt;
			totalCapitalUSD	+= montoCapitalInt;
		}
		totalInteresMN =(roundOff(totalMN,2));
		totalInteresUSD  =(roundOff(totalUSD,2));
		totalCapitalInteresMN =(roundOff(totalCapitalMN,2));
		totalCapitalInteresUSD =(roundOff(totalCapitalUSD,2));	
	}
	
	//esta es la opcion de seleccionado
	var seleccionaDocto =  function(opts, success, response, record) {
		return ( calculaMontos(opts, success, response, record));
		if(limPyme(opts, success, response, record)) {
			return true;
		}	
		sumaTotalesIntereses(opts, success, response, record);
		
} 

//validaciones al seleccionar el  checkBox 
	var validaciones =  function(opts, success, response, record) {	
		var txtPlazo;
		var montoInt; 
		var icTasa;	
		var gridDoc;
		if(datosFinales =='N') {
		 gridDoc = Ext.getCmp('gridDoc');
		}else if(datosFinales =='S') {
		 gridDoc = Ext.getCmp('gridDocumentos');
		}
		
		if(validaFechaLineaCredito('', '', '', record)==true) {
		
			if (record.data['PYME_BLOQ_X_IF']=='S' )  {
				Ext.MessageBox.alert("Mensaje","Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado por el Intermediario Financiero. ");
				return false;
				
			}else  {		
		
				var monto_credito		= record.data['MONTO_VALUADO']; 
				var montoDisSubLimite		= record.data['MONTO_DIS_SUB_LIMITE']; 						
				var montoDisLinea = parseFloat(record.data['SALDO_DISPONIBLE']);
				
				if( monto_credito>montoDisSubLimite ) {			
					Ext.MessageBox.alert("Mensaje","Por el momento alguno(s) de los documento(s) no puede ser seleccionado. "+
						"<br>Favor de comunicarse al centro de atenci�n a clientes NAFIN.");
						return false ;		
				}
				
				if( monto_credito>montoDisLinea ) {
					Ext.MessageBox.alert("Mensaje","Por el momento alguno(s) de los documento(s) no puede ser seleccionado. "+
						"<br>Favor de comunicarse al centro de atenci�n a clientes NAFIN.");
						return false ;		
				}
				
				//esto es para guardar  los valores que se calcularon al inicio  ir pintando el gid 
				var montoInt = inicializaTasasXPlazo( '', '', record,'MontodeIntereses');
				record.data['MONTO_TASA_INT']=montoInt;			
										
				var montoCap = inicializaTasasXPlazo( '', '', record,'MontoCapitalInteres');
				record.data['MONTO_CAPITAL_INT']=montoCap;								
				
				var varTasa = inicializaTasasXPlazo( '', '', record,'ValorTasaInteres');
				record.data['VALOR_TASA_PUNTOS']=varTasa;				
			
				var referencia = inicializaTasasXPlazo( '', '', record,'ReferenciaTasa');
				record.data['REFERENCIA_TASA']=referencia;	
						
				var montoAuxi = inicializaTasasXPlazo( '', '', record,'montoAuxi');
				record.data['MONTO_AUX_INT']=montoAuxi;	
						
				var tasa =  inicializaTasasXPlazo( '', '', record,'ic_tasa');
				record.data['IC_TASA']=tasa;	
		
				var puntos=  inicializaTasasXPlazo( '', '', record,'puntos');
				record.data['FN_PUNTOS']=puntos;	
				
				var CgRelMat=  inicializaTasasXPlazo( '', '', record,'CgRelMat');
				record.data['CG_REL_MAT']=CgRelMat;	
				
							
				txtPlazo =record.data['PLAZO_CREDITO'];
				montoInt = record.data['MONTO_TASA_INT']; 
				icTasa = record.data['IC_TASA'];	
				
				if(txtPlazo==''){
					Ext.MessageBox.alert("Mensaje","Debe seleccionar los plazos del credito en los documentos a aceptar");			
					return false ;
				}
					
				if(montoInt =='0' || icTasa=='0'){
					Ext.MessageBox.alert("Mensaje","Existen documentos que no pueden ser seleccionados porque no existe ninguna tasa que se ajuste a su plazo");
					return false ;
				}		
					
				if(seleccionaDocto(opts, success, response, record)){
					
					return sumaFecha(opts, success, response, record);
				
					return restaFecha(opts, success, response, record);
				}else{
					return false;
				}			
				
				
				if(seleccionaDocto(opts, success, response, record)){
						return true;
				}else{
					return false;
				}
		
			}
		
		}	
		
		
	}
	//Valida que la fecha de Vencimiento de la linea de Credito no sea menor a la fecha de Vencimiento del Documento
	var validaFechaLineaCredito =  function(opts, success, response, record) {
		var cg_lineaC 				= record.data['CG_LINEA_CREDITO'];		
		var fechaVenLinea =Ext.util.Format.date(record.data['FECHA_VENC_LINEA'],'d/m/Y');
		var fechaVenDocto =record.data['FECHA_VEN_CREDITO']; 
		
		var  gridEditable = Ext.getCmp('gridDocumentos');	
		var store = gridEditable.getStore();		
		var  recordNumber = store.indexOfId( record.id);
	  
		if(cg_lineaC=='S'  &&  datecomp(fechaVenDocto,fechaVenLinea)==1 ) {
			Ext.MessageBox.alert('Mensaje','El documento no puede vencer el d�a '+fechaVenDocto+' por que la L�nea de Cr�dito que tiene con el Intermediario vence el d�a '+fechaVenLinea);						
			 if(record.data['SELECCION']=='S')  {
				gridEditable.getSelectionModel().deselectRow(recordNumber);
				record.data['SELECCION']='N';
				record.commit();					
			 }
			return false;
			
		}else  {
			return true;
		}
	}
	
//*Termina Validaciones****************************************************************************************

	//para el boton de PDF de acuse 
	var procesarSuccessFailureGenerarPDFAcuse =  function(opts, success, response) {
		var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
		btnGenerarPDFAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			btnBajarPDFAcuse.show();
			btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPDFAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//para el boton de CSV de acuse 
	var procesarSuccessFailureGenerarCSVAcuse =  function(opts, success, response) {
		var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
		btnGenerarCSVAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSVAcuse = Ext.getCmp('btnBajarCSVAcuse');
			btnBajarCSVAcuse.show();
			btnBajarCSVAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSVAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarCSVAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	
	//para el boton de PDF de presAcuse 
	var procesarSuccessFailureGenerarPDFPreA =  function(opts, success, response) {
		var btnGenerarPDFA = Ext.getCmp('btnGenerarPDFPreAcuse');
		btnGenerarPDFA.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDFA.disable();
			var btnBajarPDFA = Ext.getCmp('btnBajarPDFPreAcuse');
			btnBajarPDFA.show();
			btnBajarPDFA.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDFA.focus();
			btnBajarPDFA.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFA.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//Para el Boton d la Consulta
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDFE');
		btnImprimirPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFE');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		//Para el Boton d la Consulta
	var procesarSuccessFailurePDFNormal =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDFN');
		btnImprimirPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFN');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	// muestra la pantalla de acuse 
	function procesarSuccessFailureAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			var acuse;
			
			if(jsondeAcuse.mensaje!='' )  {			
				Ext.MessageBox.alert('Mensaje',jsondeAcuse.mensaje);				
				
			}else  {
				
				if (jsondeAcuse != null){
					 acuse= jsondeAcuse.acuse;								
				}	
					
				var Comfirmacionclave1 = Ext.getCmp('Comfirmacionclave');
						if (Comfirmacionclave1) {		
						Comfirmacionclave1.destroy();		
				}					
				
				var acuseCifras = [
						['N�mero de Acuse', jsondeAcuse.acuse],
						['Fecha de Carga', jsondeAcuse.fecCarga],
						['Hora de Carga', jsondeAcuse.horaCarga],
						['Usuario de Captura', jsondeAcuse.captUser]
					];
					
					storeCifrasData.loadData(acuseCifras);
					
					fp.hide();
					gridPreAcuse.hide();
					gridMontos.show();	
					gridAcuse.show();
					gridCifrasControl.show();
					gridTotalesE.hide();
					ctexto1.hide();
					ctexto4.show();	
					ctexto3.hide();
					ctexto2.hide();
			
				if(datosFinales=='N'){
					var cm = gridAcuse.getColumnModel();
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('MONTO_CAPITAL_INT'), true);	
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('MONTO_TASA_INT'), true);	
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('IC_DOCUMENTO'), true);	
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('MONTO_TASA_INT'), true);				
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('VALOR_TASA_PUNTOS'), true);	
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_TASA'), true);	
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), true);	
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('FECHA_OPERACION'), true);	
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VEN_CREDITO'), true);	
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_CREDITO'), true);	
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('MONTO_CREDITO'), true);	
					gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('NOMBREMLINEA'), true);	
				}
				var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
				var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
				var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
				var btnBajarCSVAcuse = Ext.getCmp('btnBajarCSVAcuse');
				if(acuse!=''){				
					btnGenerarPDFAcuse.enable();
					btnBajarPDFAcuse.hide();
					btnGenerarCSVAcuse.enable();
					btnBajarCSVAcuse.hide();
				}else{
					btnGenerarPDFAcuse.disable();
					btnBajarPDFAcuse.hide();
					btnGenerarCSVAcuse.disable();
					btnBajarCSVAcuse.hide();
				}	
				
				//para el boton de PDF de Acuse 
				var btnGenerarAc = Ext.getCmp('btnGenerarPDFAcuse');
				btnGenerarAc.setHandler(
					function(boton, evento) {
						//boton.disable();
						boton.setIconClass('loading-indicator');								
						Ext.Ajax.request({
							url: '24forma01bExtPDFCSV.jsp',
							params: Ext.apply(fp.getForm().getValues(),{														
								acuse: acuse,
								ic_documento :ic_documento,
								ic_moneda_docto : ic_moneda_docto,
								ic_moneda_linea :ic_moneda_linea,
								montos :montos ,
								monto_valuado :monto_valuado,
								monto_credito :monto_credito,
								monto_tasa_int :monto_tasa_int,
								monto_descuento :monto_descuento,
								tipo_cambio :tipo_cambio,
								plazo : plazo,
								fecha_vto :fecha_vto,
								referencia_tasa :referencia_tasa,
								valor_tasa :valor_tasa,
								valor_tasa_puntos : valor_tasa_puntos,
								ic_tasa : ic_tasa,
								cg_rel_mat :cg_rel_mat,
								fn_puntos :fn_puntos,
								tipo_tasa :tipo_tasa,
								puntos_pref : puntos_pref,
								ic_epo : ic_epo,
								hidLimPyme :hidLimPyme,
								hidLimUtil : hidLimUtil,							
								tipoArchivo:'PDF',
								fn_montoValuado:fn_montoValuado
							}),
							callback: procesarSuccessFailureGenerarPDFAcuse
						});
					}
				);
				
				//para el boton de CSV de Acuse 
				var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
				btnGenerarCSVAcuse.setHandler(
					function(boton, evento) {
						//boton.disable();
						boton.setIconClass('loading-indicator');								
						Ext.Ajax.request({
							url: '24forma01bExtPDFCSV.jsp',
							params: Ext.apply(fp.getForm().getValues(),{														
								acuse: acuse,
								tipoArchivo:'CSV',
								ic_documento :ic_documento,
								ic_moneda_docto : ic_moneda_docto,
								ic_moneda_linea :ic_moneda_linea,
								montos :montos ,
								monto_valuado :monto_valuado,
								monto_credito :monto_credito,
								monto_tasa_int :monto_tasa_int,
								monto_descuento :monto_descuento,
								tipo_cambio :tipo_cambio,
								plazo : plazo,
								fecha_vto :fecha_vto,
								referencia_tasa :referencia_tasa,
								valor_tasa :valor_tasa,
								valor_tasa_puntos : valor_tasa_puntos,
								ic_tasa : ic_tasa,
								cg_rel_mat :cg_rel_mat,
								fn_puntos :fn_puntos,
								tipo_tasa :tipo_tasa,
								puntos_pref : puntos_pref,
								ic_epo : ic_epo,
								hidLimPyme :hidLimPyme,
								hidLimUtil : hidLimUtil,
								fn_montoValuado:fn_montoValuado
							}),
							callback: procesarSuccessFailureGenerarCSVAcuse
						});
					}
				);
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// procesa el Acuse
	function confirmacionClavesCesion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
		var bConfirma;
		jsondeConfirma = Ext.util.JSON.decode(response.responseText);
			if (jsondeConfirma != null){
				bConfirma= jsondeConfirma.bConfirma;									
			}		
		if(bConfirma ==false) {
			Ext.MessageBox.alert('Mensaje','La confirmaci�n es incorrecta, intente de nuevo');
			
		} else  if(bConfirma ==true) {		
		
			Ext.Ajax.request({
				url : '24forma01.data.jsp',
				params : {
					informacion: 'GeneraAcuse',					
					ic_documento :ic_documento,
					ic_moneda_docto : ic_moneda_docto,
					ic_moneda_linea :ic_moneda_linea,
					montos :montos ,
					monto_valuado :monto_valuado,
					monto_credito :monto_credito,
					monto_tasa_int :monto_tasa_int,
					monto_descuento :monto_descuento,
					tipo_cambio :tipo_cambio,
					plazo : plazo,
					fecha_vto :fecha_vto,
					referencia_tasa :referencia_tasa,
					valor_tasa :valor_tasa,
					valor_tasa_puntos : valor_tasa_puntos,
					ic_tasa : ic_tasa,
					cg_rel_mat :cg_rel_mat,
					fn_puntos :fn_puntos,
					tipo_tasa :tipo_tasa,
					puntos_pref : puntos_pref,
					ic_epo : ic_epo,
					hidLimPyme :hidLimPyme,
					hidLimUtil : hidLimUtil,
					fn_montoValuado:fn_montoValuado
				},
				callback: procesarSuccessFailureAcuse
			});
			}//bConfirma
		}//if
	}
	

	/******procesarConfirmar****************/
	var procesarConfirmar = function() {
	
		var gridDoc;
		if(datosFinales =='N') {
		 gridDoc = Ext.getCmp('gridDoc');
		}else if(datosFinales =='S') {
		 gridDoc = Ext.getCmp('gridDocumentos');
		}
		
		var store = gridDoc.getStore();		
		cancelar();
		
		var registrosPreAcu = []; //para agregar los registros al Preacuse  y acuse 		
		//se recorre grid principal para detectar los documentos seleccionados			
		store.each(function(record) {
			var numRegistro = store.indexOf(record);			
			if(record.data['SELECCION']=='S'){							
				//informacion que se envia al Preacuse como []
				modificados.push(record.data['IC_DOCUMENTO']);
				ic_documento.push(record.data['IC_DOCUMENTO']);
				ic_moneda_docto.push(record.data['IC_MONEDA_DOCTO']);
				ic_moneda_linea.push(record.data['IC_MONEDA_LINEA']);
				montos.push(record.data['FN_MONTO']);
				monto_valuado.push(record.data['MONTO_VALUADO']);
				monto_credito.push(record.data['MONTO_CREDITO']);
				monto_tasa_int.push(record.data['MONTO_TASA_INT']);
				monto_descuento.push(record.data['MONTO_DESCUENTO']);
				tipo_cambio.push(record.data['TIPO_CAMBIO']);
				plazo.push(record.data['PLAZO_CREDITO']);		
				fecha_vto.push(record.data['FECHA_VEN_CREDITO']);				
				referencia_tasa.push(record.data['REFERENCIA_TASA']);
				valor_tasa.push(record.data['VALOR_TASA']);
				valor_tasa_puntos .push(record.data['VALOR_TASA_PUNTOS']);					
				ic_tasa.push(record.data['IC_TASA']);
				cg_rel_mat.push(record.data['CG_REL_MAT']);
				fn_puntos.push(record.data['FN_PUNTOS']);
				tipo_tasa.push(record.data['TIPO_TASA']);
				puntos_pref.push(record.data['PUNTOS_PREF']);
				ic_epo = record.data['IC_EPO'];
				hidLimPyme.push(record.data['LIMITEPYME']);
				hidLimUtil.push(record.data['LIMITE_ULTILI_PYME']);
				fn_montoValuado.push(record.data['MONTO_VALUADO']);
				
			//se le carga informacion al grid de preacuse y al grid de totales
				registrosPreAcu.push(record);			
			}
		});
		
		//se le carga informacion al grid de preacuse y al grid de totales
		storePreAcuData.add(registrosPreAcu);
		storeAcuseData.add(registrosPreAcu);
			
		if(modificados =='') {
			Ext.MessageBox.alert('Mensaje','Debe seleccionar por lo menos un documento para continuar');
			return false;	
		}			
		
		gridMontos.show();
		gridPreAcuse.show();				
		fp.hide();
		ctexto1.hide();
		ctexto4.show();
		ctexto3.hide();	
		ctexto2.hide();
		gridTotalesE.hide();		
		gridNormal.hide();
		gridEditable.hide();
		gridTotalesN.hide();	
		
			//archivo PDF de Preacuse 
			var btnGenerar = Ext.getCmp('btnGenerarPDFPreAcuse');
			btnGenerar.setHandler(
				function(boton, evento) {
					//boton.disable();
					boton.setIconClass('loading-indicator');								
					Ext.Ajax.request({
						url: '24forma01aExtPDF.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF',
							tipoArchivo:'pdf',
							ic_documento :ic_documento,
							ic_moneda_docto : ic_moneda_docto,
							ic_moneda_linea :ic_moneda_linea,
							montos :montos,
							monto_valuado :monto_valuado,
							monto_credito :monto_credito,
							monto_tasa_int :monto_tasa_int,
							monto_descuento :monto_descuento,
							tipo_cambio :tipo_cambio,
							plazo : plazo,
							fecha_vto :fecha_vto,
							referencia_tasa :referencia_tasa,
							valor_tasa :valor_tasa,
							valor_tasa_puntos : valor_tasa_puntos,
							ic_tasa : ic_tasa,
							cg_rel_mat :cg_rel_mat,
							fn_puntos :fn_puntos,
							tipo_tasa :tipo_tasa,
							puntos_pref : puntos_pref,
							ic_epo : ic_epo,
							hidLimPyme :hidLimPyme,
							hidLimUtil : hidLimUtil					
						}),
						callback: procesarSuccessFailureGenerarPDFPreA
					});
				}
			);
	
			if(datosFinales=='N'){
				var cm = gridPreAcuse.getColumnModel();
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('MONTO_CAPITAL_INT'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('MONTO_TASA_INT'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('IC_DOCUMENTO'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('MONTO_TASA_INT'), true);				
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('VALOR_TASA_PUNTOS'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_TASA'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('FECHA_OPERACION'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VEN_CREDITO'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_CREDITO'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('MONTO_CREDITO'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('NOMBREMLINEA'), true);	
		}

			//para el Preacuse 	
			var btnConfirmar = Ext.getCmp('btnConfirmar');			
			btnConfirmar.setHandler(function(boton, evento) {	
				var  fLogCesion = new  NE.cesion.FormLoginCesion();
				fLogCesion.setHandlerBtnAceptar(function(){
					Ext.Ajax.request({
						url: '24forma01.data.jsp',
						params: Ext.apply(fLogCesion.getForm().getValues(),{
						informacion: 'ConfirmacionClaves'
					}),
					callback: confirmacionClavesCesion
				});										
				});	
				
				//se crea ventana de Confirmacion de Clave 
			var	ventanaConfir =  Ext.getCmp('Comfirmacionclave');	
			if (ventanaConfir) {
				ventanaConfir.show();
			} else {							
				new Ext.Window({
					layout: 'fit',
					width: 400,
					height: 200,			
					id: 'Comfirmacionclave',
					modal:true,
					closeAction: 'hide',
					items: [					
						fLogCesion
					],
					title: ''				
				}).show();
			}
			}	);	
	}
		
	

 	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		var jsonData = store.reader.jsonData;
		datosFinales = jsonData.DATOSFINALES;				
		MLineas = jsonData.MLineas;
		Noelementos =   jsonData.ElEMENTOS;
		
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();		
		var el = gridNormal.getGridEl();	
		
		//PARA GRID NORMAL
		var gridDoc = Ext.getCmp('gridDoc');
		var btnImprimirPDFN = Ext.getCmp('btnImprimirPDFN');
		var btnBajarPDFN = Ext.getCmp('btnBajarPDFN');				
		var btnCancelarN = Ext.getCmp('btnCancelarN');
		var btnConfirmarN = Ext.getCmp('btnConfirmarN');	
				
		
		//PARA GRID EDITABLE 
		var gridDocumentos = Ext.getCmp('gridDocumentos');
		var btnImprimirPDFE = Ext.getCmp('btnImprimirPDFE');
		var btnBajarPDFE = Ext.getCmp('btnBajarPDFE');	
		var btnCancelarE = Ext.getCmp('btnCancelarE');
		var btnConfirmarE = Ext.getCmp('btnConfirmarE');			
		var gridTotalesE = Ext.getCmp('gridTotalesE');
		
		if (jsonData.bloqueoPymeEpo != 'B') {	
		
			if (arrRegistros != null) {		
				if(datosFinales=='S'){
					if (!gridEditable.isVisible()) {
							gridEditable.show();
							gridNormal.hide();	
							gridTotalesE.show();
							gridTotalesN.hide();
						}	
				}else if(datosFinales=='N'){
					if (!gridNormal.isVisible()) {
							gridNormal.show();
							gridEditable.hide();
							gridTotalesE.hide();
							gridTotalesN.show();
						}	
				}			
				if(store.getTotalCount() > 0) {	
					if(datosFinales=='N'){
						btnImprimirPDFN.enable();							
						btnBajarPDFN.hide();					
						btnCancelarN.enable();	
						btnConfirmarN.enable();	
							
					}else 	if(datosFinales=='S'){
						btnImprimirPDFE.enable();							
						btnBajarPDFE.hide();					
						btnCancelarE.enable();	
						btnConfirmarE.enable();	
						ctexto3.show();
						gridTotalesE.show();
					}
					ctexto1.show();
					ctexto2.show();					
					el.unmask();
					
				} else {	
					if(datosFinales=='N'){
						btnImprimirPDFN.disable();
						btnBajarPDFN.hide();	
						btnCancelarN.disable();	
						btnConfirmarN.disable();	
							
					}else 	if(datosFinales=='S'){
						btnImprimirPDFE.disable();
						btnBajarPDFE.hide();	
						btnCancelarE.disable();	
						btnConfirmarE.disable();	
						
					}
						ctexto1.hide();
						ctexto2.hide();	
						ctexto3.hide();
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		
		}else  {		
			ctexto5.show();
			fp.hide();	
			gridNormal.hide();
			gridEditable.hide();
			gridTotalesE.hide();
			gridTotalesN.hide();
			ctexto1.hide();
			ctexto2.hide();	
			ctexto3.hide();
		}
		
	}
	
	//VerCambioDocumentos
	var procesarCambioDocumentos = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_documento = registro.get('IC_DOCUMENTO');		
		var ventana = Ext.getCmp('VerCambioDocumentos');	
	
		if (ventana) {								
			ventana.show();
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 800,
				height: 305,			
				id: 'VerCambioDocumentos',
				closeAction: 'hide',
				items: [					
					panelCambios
				],
				title: 'Cambios del documento'				
			}).show();
		}
		var x = Ext.getCmp('panelCambios').body;
		var mgr = x.getUpdater();
		mgr.on('failure', 
		function(el, response) {
			x.update('');
			NE.util.mostrarErrorResponse(response);
		});		
		mgr.update({
			url: '/nafin/24finandist/24detallecambiosExt.jsp',	
			scripts: false,
			params: {
			ic_documento: ic_documento							
		},
		indicatorText: 'Cambios del documento'
		});			
	}
		
	var panelCambios = new Ext.Panel({
		id: 'panelCambios',	
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	});

	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
	
	var catalogoModalidadPlazoData = new Ext.data.JsonStore({
		id: 'catalogoModalidadDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01.data.jsp',
		baseParams: {
			informacion: 'CatalogoModalidadPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma = [
		{
			xtype: 'compositefield',
			fieldLabel: 'EPO',
			combineErrors: false,
			msgTarget: 'side',
			width: 800,
			items: [
				{
					xtype: 'combo',
					name: 'ic_epo',
					id: 'ic_epo1',
					fieldLabel: 'Nombre de la EPO',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_epo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					width: 400,
					store : catalogoEPOData,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners: {
						select: {
							fn: function(combo) {										
								procesaIniciales(combo.getValue());									
							}
						}	
					}
				},
				{
						xtype: 'displayfield',
						value: 'Moneda:',
						width: 50
				},
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. documento inicial',
			combineErrors: false,
			msgTarget: 'side',			
				items: [
				{
				xtype: 'textfield',
				name: 'ig_numero_docto',
				id: 'ig_numero_docto1',
				fieldLabel: 'Num. documento inicial',
				allowBlank: true,
				hidden: false,
				maxLength: 15,	
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0'  
				}
				]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Monto documento',
			combineErrors: false,
			msgTarget: 'side',			
				items: [
				{
					xtype: 'numberfield',
					name: 'fn_monto_de',
					id: 'fn_monto_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',				
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'numberfield',
					name: 'fn_monto_a',
					id: 'fn_monto_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',				
					margins: '0 20 0 0'  
					}
				]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. acuse de carga',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'cc_acuse',
					id: 'cc_acuse1',
					fieldLabel: 'Num. acuse de carga',
					allowBlank: true,
					hidden: false,
					maxLength: 15,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				},				
				{
					xtype: 'checkbox',
					name: 'mto_descuento',
					id: 'mto_descuento1',
					fieldLabel: 'Monto % de descuento',
					allowBlank: true,
					hidden: false,					
					msgTarget: 'side',
					margins: '0 20 0 0',
					align: 'center'
				},
				{
					xtype: 'displayfield',
					value: 'Monto % de descuento',
					width: 200,
					align: 'left'
				}			
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha emisi�n docto',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_emision_de',
					id: 'df_fecha_emision_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_emision_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_emision_a',
					id: 'df_fecha_emision_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_emision_de1',
					margins: '0 20 0 0'  
					}
				]
			}	,
			{
			xtype: 'compositefield',
			fieldLabel: 'Fecha vencimiento docto',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_de',
					id: 'df_fecha_venc_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_venc_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_a',
					id: 'df_fecha_venc_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_venc_de1',
					margins: '0 20 0 0'  
					}
				]
			},
			{
			xtype: 'compositefield',
			fieldLabel: 'Fecha publicaci�n de',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_de',
					id: 'fecha_publicacion_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_publicacion_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_a',
					id: 'fecha_publicacion_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_publicacion_de1',
					margins: '0 20 0 0'  
					}
				]
			},			
			{
				xtype: 'compositefield',
				fieldLabel: 'S�lo documentos modificados',
				combineErrors: false,
				msgTarget: 'side',
				width: 200,
				items: [
				{
					xtype: 'checkbox',
					name: 'doctos_cambio',
					id: 'doctos_cambio1',
					fieldLabel: 'S�lo documentos modificados',
					allowBlank: true,
					hidden: false,					
					msgTarget: 'side',
					margins: '0 20 0 0',
					value:	'S',
					align: 'center'
				},		
				{
					xtype: 'displayfield',
					value: 'Modalidad de plazo:',
					width: 150
				},				
				{
					xtype: 'combo',
					name: 'modo_plazo',
					id: 'modo_plazo1',
					fieldLabel: 'Modalidad de plazo',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'modo_plazo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoModalidadPlazoData,
					tpl : NE.util.templateMensajeCargaCombo				
				}
				]
			}
		];	
		
		var totalesDataE = new Ext.data.JsonStore({			
		root : 'registrosT',
		url : '24forma01.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_epo: Ext.getCmp("ic_epo1"),
			ic_moneda: Ext.getCmp("ic_moneda1"),
			mto_descuento: Ext.getCmp("ic_moneda1"),
			cc_acuse: Ext.getCmp("cc_acuse1"),
			fn_monto_de: Ext.getCmp("fn_monto_de1"),
			fn_monto_a: Ext.getCmp("fn_monto_a1"),
			df_fecha_emision_de: Ext.getCmp("df_fecha_emision_de1"),
			df_fecha_emision_a: Ext.getCmp("df_fecha_emision_a1"),
			ig_numero_docto: Ext.getCmp("ig_numero_docto1"),
			df_fecha_venc_de: Ext.getCmp("df_fecha_venc_de1"),
			df_fecha_venc_a: Ext.getCmp("df_fecha_venc_a1"),
			fecha_publicacion_de: Ext.getCmp("fecha_publicacion_de1"),
			fecha_publicacion_a: Ext.getCmp("fecha_publicacion_a1"),
			doctos_cambio: Ext.getCmp("doctos_cambio1"),
			modo_plazo: Ext.getCmp("modo_plazo1")
		},								
		fields: [			
			{name: 'MONEDAT' , mapping: 'MONEDAT'},
			{name: 'ICMONEDA' , mapping: 'ICMONEDA'},
			{name: 'TOTALT', mapping: 'TOTALT'},
			{name: 'MONTOT', mapping: 'MONTOT' },
			{name: 'TOTAL_MONTO_DESCT',  mapping: 'TOTAL_MONTO_DESCT'},
			{name: 'TOTAL_MONTO_VALUADOT',  mapping: 'TOTAL_MONTO_VALUADOT'},	
			{name: 'MONTOINTERESF',  mapping: 'MONTOINTERESF'},			
			{name: 'MONTOCAPITALINTERESF',  mapping: 'MONTOCAPITALINTERESF'},
			{name: 'MONTO_DESCONTAR',  mapping: 'MONTO_DESCONTAR'}
			
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});	
	
	//esto esta en duda como manejarlo
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Datos Documento Inicial', colspan: 6, align: 'center'},
					{header: 'Datos Documento Final', colspan: 4, align: 'center'}					
				]
			]
	});
	
	var gridTotalesE = new Ext.grid.EditorGridPanel({	
		id: 'gridTotalesE',
		store: totalesDataE,
		margins: '20 0 0 0',
		align: 'center',
		title: 'Totales',
		plugins: grupos,	
		hidden: true,
		columns: [	
		{
				header: 'MONEDA',
				dataIndex: 'MONEDAT',
				width: 150,
				align: 'left'			
		},
		{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTALT',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'MONTOT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto total de descuento.',
				dataIndex: 'TOTAL_MONTO_DESCT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			}	,
			{
				header: 'Total Monto  descuento.',
				dataIndex: 'MONTO_DESCONTAR',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			}	,
			
			{
				header: 'Monto total valuado en pesos',
				dataIndex: 'TOTAL_MONTO_VALUADOT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
		//Finales
			{							
				header : 'Num. total de doctos.',
				dataIndex : 'TOTALT',
				width : 150,
				align: 'center'
			},
			{							
				header : 'Monto total de doctos.',			
				dataIndex : 'MONTOT',
				width : 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			{							
				header : 'Monto de Intereses',			
				dataIndex : 'MONTOINTERESF',
				width : 150,
				align: 'right',				
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto Total de Capital e Inter�s',				
				dataIndex : 'MONTOCAPITALINTERESF',
				width : 150,
				align: 'right',				
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
			],
			height: 150,	
			width : 943,
			frame: false
		});	
		
	//para mostrar totales  en el PreAcuse y acuse 
	var storeMontosData = new Ext.data.ArrayStore({
		fields: [
			{name: 'totalDoctosMN', type: 'float'},
			{name: 'totalMontoMN', type: 'float'},
			{name: 'totalMontoSIntMN', type: 'float'},
			{name: 'totalInteresMN', type: 'float'},	
					
			{name: 'totalDoctosUSD', type: 'float'},			
			{name: 'totalMontoUSD', type: 'float'},
			{name: 'totalMontoSInUSD', type: 'float'},
			{name: 'totalInteresUSD', type: 'float'},
			
			{name: 'totalDoctosConv', type: 'float'},
			{name: 'totalMontosConv', type: 'float'},
			{name: 'totalMontoSinConv', type: 'float'},
			{name: 'totalInteresConv', type: 'float'}
		  ]
	 });
	 
	//esto esta en duda como manejarlo
	var gruposMontos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Moneda nacional', colspan: 4, align: 'center'},
				{header: 'D�lares Americanos', colspan: 4, align: 'center'}	,				
				{header: 'Doctos. en DLL financiados en M.N.', colspan: 4, align: 'center'}					
			]
		]
	});
	
	//Grid del Acuse 
	var gridMontos = new Ext.grid.EditorGridPanel({	
		id: 'gridMontos',		
		store: storeMontosData,
		plugins: gruposMontos,
		hidden: true,
		columns: [		
			{							
				header : 'Num. total de doctos.',
				tooltip: 'totalDoctosMN',
				dataIndex : 'totalDoctosMN',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos. iniciales',
				tooltip: 'totalMontoMN',
				dataIndex : 'totalMontoMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{							
				header : 'Monto total de doctos. finales',
				tooltip: 'Monto total de doctos. finales',
				dataIndex : 'totalMontoSIntMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{							
				header : 'Monto intereses',
				tooltip: 'Monto intereses',
				dataIndex : 'totalInteresMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},								
			//USD		
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosUSD',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos. iniciales',
				tooltip: 'Monto total de doctos. iniciales',
				dataIndex : 'totalMontoUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto total de doctos. finales',
				tooltip: 'Monto total de doctos. finales',
				dataIndex : 'totalMontoSInUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false				
			},
			{							
				header : 'Monto intereses finales',
				tooltip: 'Monto intereses finales',
				dataIndex : 'totalInteresUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
		//Doctos. en DLL financiados en M.N.
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosConv',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
				
			},
			{							
				header : 'Monto total de doctos. iniciales',
				tooltip: 'Monto total de doctos. iniciales',
				dataIndex : 'totalMontosConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto total de doctos. finales',
				tooltip: 'Monto total de doctos. finales',
				dataIndex : 'totalMontoSinConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto intereses finales',
				tooltip: 'Monto intereses finales',
				dataIndex : 'totalInteresConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 150,
		width: 943,		
		frame: true
	
	});
	
	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	 //Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
	id: 'gridCifrasControl',
	store: storeCifrasData,
	margins: '20 0 0 0',
	hideHeaders : true,
	hidden: true,
	align: 'center',
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 150,
			sortable : true
		},
		{
			header : 'Informacion',			
			dataIndex : 'informacion',
			width : 350,
			sortable : true,
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 500,
	autoHeight : true,
	style: 'margin:0 auto;',
	title: 'Cifras de Control',
	frame: true
	});
	
	
	var storeAcuseData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'IG_DOCUMENTO'},
			{name: 'ACUSE'},
			{name: 'DF_FECHA_EMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENCIMIENTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO'},
			{name: 'CATEGORIA'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'PORCENTAJE_DESCUENTO'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'CG_TIPO_CONV'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VALUADO'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_DOCUMENTO'},
			{name: 'IC_MONEDA_DOCTO'},
			{name: 'IC_MONEDA_LINEA'},
			{name: 'IC_IF'}	,
			{name: 'NOMBREMLINEA'},
			{name: 'MONTO_CREDITO'},
			{name: 'PLAZO_CREDITO'},
			{name: 'FECHA_VEN_CREDITO',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_OPERACION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMBRE_IF'},
			{name: 'REFERENCIA_TASA'},
			{name: 'VALOR_TASA'},
			{name: 'MONTO_AUX_INT',type: 'float'},
			{name: 'MONTO_CAPITAL_INT',type: 'float'},
			{name: 'HIDNUMLINEAS',type: 'float'},
			{name: 'IC_LINEA_CREDITO_DM'},
			{name: 'MONTO_TASA_INT',type: 'float'},
			{name: 'IC_TASA',type: 'float'},
			{name: 'VALOR_TASA_PUNTOS',type: 'float'},
			{name: 'CG_REL_MAT'},
			{name: 'FN_PUNTOS'},
			{name: 'TIPO_TASA'},
			{name: 'PUNTOS_PREF'},
			{name: 'IC_EPO',type: 'float'},
			{name: 'LIMITEPYME'},
			{name: 'LIMITE_ULTILI_PYME'},	
			{name: 'DIAS_MINIMOS'},
			{name: 'DIAS_MAXIMO'},	
			{name: 'DIAS_MAXFV'},	
			{name: 'FECHA_VENC_MAX',type: 'date', dateFormat: 'd/m/Y'},			
		  {name: 'HIDNUMTASAS'},			
			{name: 'EDITABLE'},				
			{name: 'SELECCION'}	, 	
			{name: 'IC_TIPO_FINANCIAMIENTO'},
			{name: 'DESCUENTO_AFORO'},	
			{name: 'MONTO_DESCONTAR'}	
		]
	});
	
	

	var storePreAcuData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'IG_DOCUMENTO'},
			{name: 'ACUSE'},
			{name: 'DF_FECHA_EMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENCIMIENTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO'},
			{name: 'CATEGORIA'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'PORCENTAJE_DESCUENTO'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'CG_TIPO_CONV'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VALUADO'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_DOCUMENTO'},
			{name: 'IC_MONEDA_DOCTO'},
			{name: 'IC_MONEDA_LINEA'},
			{name: 'IC_IF'}	,
			{name: 'NOMBREMLINEA'},
			{name: 'MONTO_CREDITO'},
			{name: 'PLAZO_CREDITO'},
			{name: 'FECHA_VEN_CREDITO' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_OPERACION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMBRE_IF'},
			{name: 'REFERENCIA_TASA'},
			{name: 'VALOR_TASA'},
			{name: 'MONTO_AUX_INT',type: 'float'},
			{name: 'MONTO_CAPITAL_INT',type: 'float'},
			{name: 'HIDNUMLINEAS',type: 'float'},
			{name: 'IC_LINEA_CREDITO_DM'},
			{name: 'MONTO_TASA_INT',type: 'float'},
			{name: 'IC_TASA',type: 'float'},
			{name: 'VALOR_TASA_PUNTOS',type: 'float'},
			{name: 'CG_REL_MAT'},
			{name: 'FN_PUNTOS'},
			{name: 'TIPO_TASA'},
			{name: 'PUNTOS_PREF'},
			{name: 'IC_EPO',type: 'float'},
			{name: 'LIMITEPYME'},
			{name: 'LIMITE_ULTILI_PYME'},	
			{name: 'DIAS_MINIMOS'},
			{name: 'DIAS_MAXIMO'},	
			{name: 'DIAS_MAXFV'},	
			{name: 'FECHA_VENC_MAX',type: 'date', dateFormat: 'd/m/Y'},			
		  {name: 'HIDNUMTASAS'},			
			{name: 'EDITABLE'},				
			{name: 'SELECCION'}	, 	
			{name: 'IC_TIPO_FINANCIAMIENTO'},
			{name: 'DESCUENTO_AFORO'},
			{name: 'MONTO_DESCONTAR'}			
		]
	});
	
		//esto esta en duda como manejarlo
	var gruposAcuse = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Datos Documento Inicial', colspan: 20, align: 'center'},
					{header: 'Datos Documento Final', colspan: 12, align: 'center'}					
				]
			]
	});
	
		var gridAcuse = new Ext.grid.GridPanel({
		id: 'gridAcuse',
		title: 'Acuse Selecci�n de Documentos Modalidad 1 ',
		hidden: true,
		store: storeAcuseData,
		plugins: gruposAcuse,
		columns: [		
				{							
				header : 'SELECCION',
				tooltip: 'SELECCION',
				dataIndex : 'SELECCION',
				width : 150,
				align: 'left',
				hidden: true,	
				sortable : false
			},
			{							
				header : 'Epo',
				tooltip: 'Epo',
				dataIndex : 'NOMBRE_EPO',
				width : 150,
				align: 'left',
				sortable : false
			},
			{				
				header : 'Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex : 'IG_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : false
			},	
			{				
				header : 'Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex : 'ACUSE',
				width : 150,
				align: 'center',
				sortable : false
			},	
			{				
				header : 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex : 'DF_FECHA_EMISION',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex : 'DF_FECHA_VENCIMIENTO',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex : 'DF_FECHA_PUBLICACION',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Plazo docto',
				tooltip: 'Plazo docto',
				dataIndex : 'IG_PLAZO_DOCTO',
				width : 150,
				sortable : false,
				align: 'center'
			},
			{				
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				sortable : false,
				align: 'left'
			},
			{				
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'FN_MONTO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : 'Categoria',
				tooltip: 'Categoria',
				dataIndex : 'CATEGORIA',
				width : 150,
				sortable : false,
				hidden: true,	
				align: 'left'
			},
			{				
				header : 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'center'
			},		
			{				
				header : '% de descuento',
				tooltip: '% de descuento',
				dataIndex : 'PORCENTAJE_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{				
				header : 'Monto de descuento',
				tooltip: 'Monto de descuentoo',
				dataIndex : 'MONTO_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{				
				header : 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex : 'CG_TIPO_CONV',
				width : 150,
				sortable : false,
				align: 'center'
			},
			{				
				header : 'Tipo cambio.',
				tooltip: 'Tipo cambio.',
				dataIndex : 'TIPO_CAMBIO',
				width :150,
				sortable : false,
				align: 'center'
			},
			{				
				header : 'Monto valuado en pesos',
				tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VALUADO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				width : 150,
				sortable : false,
				align: 'left'			
			},			
			{				
				header : 'N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex : 'IC_DOCUMENTO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'			
			},	
			{				
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'NOMBREMLINEA',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'			
			},	
			{				
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO_CREDITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'			
			},	
			{				
				header : 'IC_TIPO_FINANCIAMIENTO', 
				tooltip: 'IC_TIPO_FINANCIAMIENTO',
				dataIndex : 'IC_TIPO_FINANCIAMIENTO',
				width : 150,
				sortable : false,
				hidden: true,
				align: 'center'						
			},	
			{	
				header : 'Plazo', // es es editable 
				tooltip: 'Plazo ',
				dataIndex : 'PLAZO_CREDITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'						
			},				
			{				
				header : 'Fecha de vencimiento', // es es editable 
				tooltip: 'Fecha de vencimiento',
				dataIndex : 'FECHA_VEN_CREDITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},	
			{				
				header : 'Fecha de operaci�n', 
				tooltip: 'Fecha de operaci�n',
				dataIndex : 'FECHA_OPERACION',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{				
				header : 'IF', 
				tooltip: 'IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'left'			
			},
			{				
				header : 'Referencia tasa de inter�s', //este se llena  de acuerdo a lo caputurado en 
				tooltip: 'Referencia tasa de inter�s',
				dataIndex : 'REFERENCIA_TASA',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'							
			},
			{				
				header : 'Valor tasa de inter�s',  //editable 
				tooltip: 'Valor tasa de inter�s',
				dataIndex : 'VALOR_TASA_PUNTOS',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'				
				},
			 {				
				header : 'Monto de Intereses',  //editable 
				tooltip: 'Monto de Intereses',
				dataIndex : 'MONTO_TASA_INT',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'right',	
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			{				
				header : 'Monto Total de Capital e Inter�s',  //editable
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex : 'MONTO_CAPITAL_INT',
				width : 150,
				sortable : false,
				hidden: false,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 				
				},			
			{				
				header : 'IC_TASA',  //editable
				tooltip: 'IC_TASA',
				dataIndex : 'IC_TASA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'			
			},		
			{				
				header : 'MONTO_AUX_INT',  //editable
				tooltip: 'MONTO_AUX_INT',
				dataIndex : 'MONTO_AUX_INT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{				
				header : 'PUNTOS_PREF',  //editable
				tooltip: 'PUNTOS_PREF',
				dataIndex : 'PUNTOS_PREF',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right'
			}	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		frame: false,
		bbar: {
		xtype: 'toolbar',
			items: [				
				'->',
				'-',						
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFAcuse'											
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFAcuse',
					hidden: true
				},							
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVAcuse'											
				},
			
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSVAcuse',
					hidden: true
				},
				'-',
				{
					text: 'Salir',
					xtype: 'button',
					id: 'btnSAlir',
					handler: function() {						
						window.location = '24forma01ext.jsp';
					}
				}		
			]
		}		
	});
		
	//esto esta en duda como manejarlo
	var gruposPreAcuse = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Datos Documento Inicial', colspan: 19, align: 'center'},
					{header: 'Datos Documento Final', colspan: 12, align: 'center'}					
				]
			]
	});
	
	var gridPreAcuse = new Ext.grid.GridPanel({
		id: 'gridPreAcuse',
		title: 'Pre Acuse Selecci�n de Documentos Modalidad 1',		
		hidden: true,
		store: storePreAcuData,
		plugins: gruposPreAcuse,	
		columns: [		
				{							
				header : 'SELECCION',
				tooltip: 'SELECCION',
				dataIndex : 'SELECCION',
				width : 150,
				align: 'left',
				hidden: true,	
				sortable : false
			},
			{							
				header : 'Epo',
				tooltip: 'Epo',
				dataIndex : 'NOMBRE_EPO',
				width : 150,
				align: 'left',
				sortable : false
			},
			{				
				header : 'Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex : 'IG_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : false
			},	
			{				
				header : 'Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex : 'ACUSE',
				width : 150,
				align: 'center',
				sortable : false
			},	
			{				
				header : 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex : 'DF_FECHA_EMISION',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex : 'DF_FECHA_VENCIMIENTO',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex : 'DF_FECHA_PUBLICACION',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Plazo docto',
				tooltip: 'Plazo docto',
				dataIndex : 'IG_PLAZO_DOCTO',
				width : 150,
				sortable : false,
				align: 'center'
			},
			{				
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				sortable : false,
				align: 'left'
			},
			{				
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'FN_MONTO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : 'Categoria',
				tooltip: 'Categoria',
				dataIndex : 'CATEGORIA',
				width : 150,
				sortable : false,
				hidden: true,	
				align: 'left'
			},
			{				
				header : 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'center'
			},		
			{				
				header : '% de descuento',
				tooltip: '% de descuento',
				dataIndex : 'PORCENTAJE_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{				
				header : 'Monto de descuento',
				tooltip: 'Monto de descuentoo',
				dataIndex : 'MONTO_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{				
				header : 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex : 'CG_TIPO_CONV',
				width : 150,
				sortable : false,
				align: 'center'
			},
			{				
				header : 'Tipo cambio.',
				tooltip: 'Tipo cambio.',
				dataIndex : 'TIPO_CAMBIO',
				width :150,
				sortable : false,
				align: 'center'
			},
			{				
				header : 'Monto valuado en pesos',
				tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VALUADO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				width : 150,
				sortable : false,
				align: 'left'			
			},			
			{				
				header : 'N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex : 'IC_DOCUMENTO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'			
			},	
			{				
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'NOMBREMLINEA',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'			
			},	
			{				
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO_CREDITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'			
			},	
			{				
				header : 'IC_TIPO_FINANCIAMIENTO', 
				tooltip: 'IC_TIPO_FINANCIAMIENTO',
				dataIndex : 'IC_TIPO_FINANCIAMIENTO',
				width : 150,
				sortable : false,
				hidden: true,
				align: 'center'						
			},	
			{	
				header : 'Plazo', // es es editable 
				tooltip: 'Plazo ',
				dataIndex : 'PLAZO_CREDITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'						
			},				
			{				
				header : 'Fecha de vencimiento', // es es editable 
				tooltip: 'Fecha de vencimiento',
				dataIndex : 'FECHA_VEN_CREDITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},	
			{				
				header : 'Fecha de operaci�n', 
				tooltip: 'Fecha de operaci�n',
				dataIndex : 'FECHA_OPERACION',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{				
				header : 'IF', 
				tooltip: 'IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'left'			
			},
			{				
				header : 'Referencia tasa de inter�s', //este se llena  de acuerdo a lo caputurado en 
				tooltip: 'Referencia tasa de inter�s',
				dataIndex : 'REFERENCIA_TASA',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'							
			},
			{				
				header : 'Valor tasa de inter�s',  //editable 
				tooltip: 'Valor tasa de inter�s',
				dataIndex : 'VALOR_TASA_PUNTOS',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'				
				},
			 {				
				header : 'Monto de Intereses',  //editable 
				tooltip: 'Monto de Intereses',
				dataIndex : 'MONTO_TASA_INT',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'right',	
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			{				
				header : 'Monto Total de Capital e Inter�s',  //editable
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex : 'MONTO_CAPITAL_INT',
				width : 150,
				sortable : false,
				hidden: false,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 				
				},			
			{				
				header : 'IC_TASA',  //editable
				tooltip: 'IC_TASA',
				dataIndex : 'IC_TASA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'			
			},		
			{				
				header : 'MONTO_AUX_INT',  //editable
				tooltip: 'MONTO_AUX_INT',
				dataIndex : 'MONTO_AUX_INT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{				
				header : 'PUNTOS_PREF',  //editable
				tooltip: 'PUNTOS_PREF',
				dataIndex : 'PUNTOS_PREF',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right'
			}	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Confirmar',
					id: 'btnConfirmar'											
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFPreAcuse'											
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFPreAcuse',
					hidden: true
				},				
				{
					text: 'Cancelar',
					xtype: 'button',
					id: 'btnCancelarPre',
					handler: function() {												
						window.location = '24forma01ext.jsp';
					}
				}		
			]
		}
	});
	


	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'IG_DOCUMENTO'},
			{name: 'ACUSE'},
			{name: 'DF_FECHA_EMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENCIMIENTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO'},
			{name: 'CATEGORIA'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'PORCENTAJE_DESCUENTO'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'CG_TIPO_CONV'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VALUADO'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_DOCUMENTO'},
			{name: 'IC_MONEDA_DOCTO'},
			{name: 'IC_MONEDA_LINEA'},
			{name: 'IC_IF'}	,
			{name: 'NOMBREMLINEA'},
			{name: 'MONTO_CREDITO'},
			{name: 'PLAZO_CREDITO'},
			{name: 'FECHA_VEN_CREDITO'},
			{name: 'FECHA_OPERACION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMBRE_IF'},
			{name: 'REFERENCIA_TASA'},
			{name: 'VALOR_TASA'},
			{name: 'MONTO_AUX_INT',type: 'float'},
			{name: 'MONTO_CAPITAL_INT',type: 'float'},
			{name: 'HIDNUMLINEAS',type: 'float'},
			{name: 'IC_LINEA_CREDITO_DM'},
			{name: 'MONTO_TASA_INT',type: 'float'},
			{name: 'IC_TASA',type: 'float'},
			{name: 'VALOR_TASA_PUNTOS',type: 'float'},
			{name: 'CG_REL_MAT'},
			{name: 'FN_PUNTOS'},
			{name: 'TIPO_TASA'},
			{name: 'PUNTOS_PREF'},
			{name: 'IC_EPO',type: 'float'},
			{name: 'LIMITEPYME'},
			{name: 'LIMITE_ULTILI_PYME'},	
			{name: 'DIAS_MINIMOS'},
			{name: 'DIAS_MAXIMO'},	
			{name: 'DIAS_MAXFV'},	
			{name: 'FECHA_VENC_MAX',type: 'date', dateFormat: 'd/m/Y'},			
		  {name: 'HIDNUMTASAS'},			
			{name: 'EDITABLE'},				
			{name: 'SELECCION'}	, 	
			{name: 'IC_TIPO_FINANCIAMIENTO'},
			{name: 'PYME_BLOQ_X_IF'},
			{name: 'DESCUENTO_AFORO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'MONTO_DIS_SUB_LIMITE',type: 'float' },			
			{name: 'SALDO_DISPONIBLE',type: 'float' },
			{name: 'CG_LINEA_CREDITO'},
			{name: 'FECHA_VENC_LINEA', type: 'date', dateFormat: 'd/m/Y' }
			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}
			
		});
	
	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
	
	//para los totales del grid Nomal 
	var totalesDataN = new Ext.data.JsonStore({			
		root : 'registrosT',
		url : '24forma01.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_epo: Ext.getCmp("ic_epo1"),
			ic_moneda: Ext.getCmp("ic_moneda1"),
			mto_descuento: Ext.getCmp("ic_moneda1"),
			cc_acuse: Ext.getCmp("cc_acuse1"),
			fn_monto_de: Ext.getCmp("fn_monto_de1"),
			fn_monto_a: Ext.getCmp("fn_monto_a1"),
			df_fecha_emision_de: Ext.getCmp("df_fecha_emision_de1"),
			df_fecha_emision_a: Ext.getCmp("df_fecha_emision_a1"),
			ig_numero_docto: Ext.getCmp("ig_numero_docto1"),
			df_fecha_venc_de: Ext.getCmp("df_fecha_venc_de1"),
			df_fecha_venc_a: Ext.getCmp("df_fecha_venc_a1"),
			fecha_publicacion_de: Ext.getCmp("fecha_publicacion_de1"),
			fecha_publicacion_a: Ext.getCmp("fecha_publicacion_a1"),
			doctos_cambio: Ext.getCmp("doctos_cambio1"),
			modo_plazo: Ext.getCmp("modo_plazo1")
		},								
		fields: [			
			{name: 'MONEDAT' , mapping: 'MONEDAT'},
			{name: 'TOTALT', type: 'float', mapping: 'TOTALT'},
			{name: 'MONTOT' , type: 'float', mapping: 'MONTOT' },
			{name: 'TOTAL_MONTO_DESCT',type: 'float',   mapping: 'TOTAL_MONTO_DESCT'},
			{name: 'TOTAL_MONTO_VALUADOT', type: 'float',  mapping: 'TOTAL_MONTO_VALUADOT'},
			{name: 'MONTO_DESCONTAR',  mapping: 'MONTO_DESCONTAR'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});	
	
	var gridTotalesN = new Ext.grid.EditorGridPanel({	
		id: 'gridTotalesN',
		store: totalesDataN,
		margins: '20 0 0 0',
		align: 'center',
		title: 'Totales',
		//plugins: grupos,	
		hidden: true,
		columns: [	
		{
				header: 'MONEDA',
				dataIndex: 'MONEDAT',
				width: 150,
				align: 'left'				
		},
		{
				header: 'Total de Documentos ',
				dataIndex: 'TOTALT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Total Monto',
				dataIndex: 'MONTOT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Total Monto Descuento',
				dataIndex: 'TOTAL_MONTO_DESCT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},	
			{
				header: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},	
			{
				header: 'Total Monto valuado',
				dataIndex: 'TOTAL_MONTO_VALUADOT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			}
			],
			height: 100,
			width: 943,
			frame: false
		});	
	
		
	var totalDoctosMN =0;
	var totalMontoMN =0;
	var totalMontoDescMN  =0;
	var totalInteresMN=0;
	var totalMontoCreditoMN =0;
	
	var totalDoctosUSD=0;
	var totalMontoUSD=0;
	var totalMontoImpUSD=0;
	var totalMontoDescUSD =0;
	var totalInteresUSD=0;
	var totalMontoCreditoUSD =0;
	
	var totalDoctosConv=0;
	var totalMontosConv=0;
	var totalMontoImpConv=0;
	var totalMontoDescConv=0;
	var totalMontoIntConv=0;
	var totalMontoCreditoConv =0;
	var totalInteresConv =0;
	var monto_valuadoT=0;
					
					
	var selectModel = new Ext.grid.CheckboxSelectionModel( {
		checkOnly: false,
		listeners: {
			rowdeselect: function(selectModel, rowIndex, record) {	
						
				if( record.data['IC_MONEDA_DOCTO']=='1') {							
					totalDoctosMN --;
					totalMontoMN		-= parseFloat(record.data['FN_MONTO']);
				//	var montoSinInteres  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescMN	-= parseFloat(record.data['MONTO_DESCONTAR']);
					totalInteresMN		-= parseFloat(record.data['MONTO_TASA_INT']);				
					totalMontoCreditoMN	-= parseFloat(record.data['MONTO_CAPITAL_INT']);
				}				
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='54' ){				
					totalDoctosUSD --;
					totalMontoUSD		-= parseFloat(record.data['FN_MONTO']);
				//	var montoSinInteresUSD  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescUSD	-= parseFloat(record.data['MONTO_DESCONTAR']);
					totalInteresUSD		-= parseFloat(record.data['MONTO_TASA_INT']);	
					totalMontoCreditoUSD -=parseFloat(record.data['MONTO_CAPITAL_INT']);
				}
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){			
					totalDoctosConv --;
					totalMontosConv			-= parseFloat(record.data['FN_MONTO']);
					//var montoSinInteresConv  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescConv 		-= parseFloat(record.data['MONTO_DESCONTAR']);
					totalInteresConv			-= parseFloat(record.data['MONTO_TASA_INT']);
					totalMontoCreditoConv	-= parseFloat(record.data['MONTO_CAPITAL_INT']);
			}		
			
				var dataTotales = [	
					[
					totalDoctosMN, totalMontoMN, totalMontoDescMN, totalInteresMN, totalMontoCreditoMN, 
					totalDoctosUSD, totalMontoUSD, totalMontoDescUSD, totalInteresUSD, totalMontoCreditoUSD,
					totalDoctosConv, totalMontosConv, totalMontoDescConv, totalInteresConv,  totalMontoCreditoConv
					]	];
					
					
					//F05-2014 ----------------------
					monto_valuadoT  -= parseFloat(record.data['MONTO_VALUADO']);  															
					
					storeMontosData.loadData(dataTotales);
					record.data['SELECCION']='N';
				record.commit();		
			},	
		
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //se activa la fila seleccionada.
				if(validaciones(selectModel, rowIndex, keepExisting, record) ) {	
									
				if( record.data['IC_MONEDA_DOCTO']=='1') {							
					totalDoctosMN ++;
					totalMontoMN		+= parseFloat(record.data['FN_MONTO']);
					//var montoSinInteres  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescMN	+= parseFloat(record.data['MONTO_DESCONTAR']);
					totalInteresMN		+= parseFloat(record.data['MONTO_TASA_INT']);				
					totalMontoCreditoMN	+= parseFloat(record.data['MONTO_CAPITAL_INT']);
				}				
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='54' ){				
					totalDoctosUSD ++;
					totalMontoUSD		+= parseFloat(record.data['FN_MONTO']);
					//var montoSinInteresUSD  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescUSD	+= parseFloat(record.data['MONTO_DESCONTAR']);
					totalInteresUSD		+= parseFloat(record.data['MONTO_TASA_INT']);	
					totalMontoCreditoUSD+=parseFloat(record.data['MONTO_CAPITAL_INT']);
				}
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){			
					totalDoctosConv ++;
					totalMontosConv			+= parseFloat(record.data['FN_MONTO']);
					//var montoSinInteresConv  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescConv 		+= parseFloat(record.data['MONTO_DESCONTAR']);
					totalInteresConv		+= parseFloat(record.data['MONTO_DESCUENTO']);
					totalMontoCreditoConv	+= parseFloat(record.data['MONTO_TASA_INT']);
				}
								
					var dataTotales = [	
					[
					totalDoctosMN, totalMontoMN, totalMontoDescMN, totalInteresMN, 
					totalDoctosUSD, totalMontoUSD, totalMontoDescUSD, totalInteresUSD,
					totalDoctosConv, totalMontosConv, totalMontoDescConv, totalInteresConv
					]	];
					
				  storeMontosData.loadData(dataTotales);
				
				//F05-2014 ------------				
					monto_valuadoT  += parseFloat(record.data['MONTO_VALUADO']);  
					var montoDisSubLimite = parseFloat(record.data['MONTO_DIS_SUB_LIMITE']);
					var montoDisLinea = parseFloat(record.data['SALDO_DISPONIBLE']);					
				
					if(  (monto_valuadoT>montoDisSubLimite) ||  ( monto_valuadoT>montoDisLinea )  ) {
						Ext.MessageBox.alert("Mensaje","Por el momento alguno(s) de los documento(s) no puede ser seleccionado. "+
						"<br>Favor de comunicarse al centro de atenci�n a clientes NAFIN.");
						monto_valuadoT  -= parseFloat(record.data['MONTO_VALUADO']); 	
						record.data['SELECCION']='N';
						record.commit();	
						return false;
					
					}else {				
						record.data['SELECCION']='S';
						record.commit();	
						return true;
					}									
			}else{	
					record.data['SELECCION']='N';
					record.commit();	
					return false;
				}			
			}
		}
		});
	
	
		var gridNormal = new Ext.grid.EditorGridPanel({
		id: 'gridDoc',
		store: consultaData,
		title: 'Selecci�n de Documentos Modalidad 1',
		hidden: true,		
		clicksToEdit: 1,
		viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [			
			{							
				header : 'SELECCION',
				tooltip: 'SELECCION',
				dataIndex : 'SELECCION',
				width : 150,
				align: 'left',
				hidden: true,	
				sortable : false
			},				
			{							
				header : 'SALDO_DISPONIBLE',
				tooltip: 'SALDO_DISPONIBLE',
				dataIndex : 'SALDO_DISPONIBLE',
				width : 150,
				align: 'left',	
				hidden: true,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			
			{							
				header : 'MONTO_DIS_SUB_LIMITE',
				tooltip: 'MONTO_DIS_SUB_LIMITE',
				dataIndex : 'MONTO_DIS_SUB_LIMITE',
				width : 150,
				align: 'left',	
				hidden: true,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},					
			{							
				header : 'Epo',
				tooltip: 'Epo',
				dataIndex : 'NOMBRE_EPO',
				width : 150,
				align: 'left',
				sortable : false
			},
			{				
				header : 'Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex : 'IG_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : false
			},	
			{				
				header : 'Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex : 'ACUSE',
				width : 150,
				align: 'center',
				sortable : false
			},	
			{				
				header : 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex : 'DF_FECHA_EMISION',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex : 'DF_FECHA_VENCIMIENTO',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex : 'DF_FECHA_PUBLICACION',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Plazo docto',
				tooltip: 'Plazo docto',
				dataIndex : 'IG_PLAZO_DOCTO',
				width : 150,
				sortable : false,
				align: 'center'
			},
			{				
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				sortable : false,
				align: 'left'
			},
			{				
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'FN_MONTO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : 'Categoria',
				tooltip: 'Categoria',
				dataIndex : 'CATEGORIA',
				width : 150,
				sortable : false,
				align: 'left'
			},
			{				
				header : 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'center'
			},		
			{				
				header : '% de descuento',
				tooltip: '% de descuento',
				dataIndex : 'PORCENTAJE_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{				
				header : 'Monto de descuento',
				tooltip: 'Monto de descuentoo',
				dataIndex : 'MONTO_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{				
				header : 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex : 'CG_TIPO_CONV',
				width : 150,
				sortable : false,
				align: 'center'
			},
			{				
				header : 'Tipo cambio.',
				tooltip: 'Tipo cambio.',
				dataIndex : 'TIPO_CAMBIO',
				width :150,
				sortable : false,
				align: 'center'
			},
			{				
				header : 'Monto valuado en pesos',
				tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VALUADO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				width : 150,
				sortable : false,
				align: 'left'			
			},
			{				
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				width : 150,
				sortable : false,
				align: 'center'			
			},					
			{
	      xtype: 'actioncolumn',
				header: 'Cambios del documento',
				tooltip: 'Cambios del documento',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	handler: procesarCambioDocumentos
					}
				]				
			},		
			
			{				
				header : 'Referencia tasa de inter�s', //este se llena  de acuerdo a lo caputurado en 
				tooltip: 'Referencia tasa de inter�s',
				dataIndex : 'REFERENCIA_TASA',
				width : 150,
				sortable : false,
				hidden: true,
				align: 'center',
				renderer:function(value,metadata,registro){		
					var select = registro.data['SELECCION'];	
					var refe = inicializaTasasXPlazo( value, metadata, registro,'ReferenciaTasa')
					if(select=='E'){
						refe = (value=='')?'':value;
					}
					return refe;
				}			
			},
			{				
				header : 'Valor tasa de inter�s',  //editable 
				tooltip: 'Valor tasa de inter�s',
				dataIndex : 'VALOR_TASA_PUNTOS',
				width : 150,
				sortable : false,
				hidden: true,
				align: 'center',	
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];	
					var valort = Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'ValorTasaInteres'),'0.0000%');
					if(select=='E'){
						valort =  Ext.util.Format.number((value=='0')?'':value, '0.0000%');					
					}
					return valort;	
				}
				},
			 {				
				header : 'Monto de Intereses',  //editable 
				tooltip: 'Monto de Intereses',
				dataIndex : 'MONTO_TASA_INT',
				width : 150,
				sortable : false,
				hidden: true,
				align: 'right',	
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];					
					var monto =  Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'MontodeIntereses'), '$0,0.00');	
					if(select=='E'){
						monto =  Ext.util.Format.number((value=='0')?'':value, '$0,0.00');
					}
					return monto;
				}				
			},			
			{				
				header : 'Monto Total de Capital e Inter�s',  //editable
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex : 'MONTO_CAPITAL_INT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',	
				renderer:function(value,metadata,registro){
					var monto =  Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'MontoCapitalInteres'), '$0,0.00');
					var select = registro.data['SELECCION'];
					if(select=='E'){
						monto =Ext.util.Format.number((value=='0')?'':value, '$0,0.00');	
					}
					return monto;
				}			
			},			
			{				
				header : 'IC_TASA',  //editable
				tooltip: 'IC_TASA',
				dataIndex : 'IC_TASA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var tasa =  inicializaTasasXPlazo( value, metadata, registro,'ic_tasa');
					if(select=='E'){
						tasa  = (value=='')?'':value;
					}
					return tasa	
				}					
			},		
			{				
				header : 'MONTO_AUX_INT',  //editable
				tooltip: 'MONTO_AUX_INT',
				dataIndex : 'MONTO_AUX_INT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var montoAuxi =  inicializaTasasXPlazo( value, metadata, registro,'montoAuxi');
					if(select=='E'){
						montoAuxi  = (value=='')?'':value;
					}
						return  Ext.util.Format.number(montoAuxi, '$0,0.00');	
				}	
			}	,						
			{				
				header : 'Plazo editable', 
				tooltip: 'Plazo editable',
				dataIndex : 'PLAZO_CREDITO',
				width : 150,
				sortable : false,
				hidden: true,
				align: 'center'							
			},				
			{				
				header : 'Fecha de vencimiento', 
				tooltip: 'Fecha de vencimiento',
				dataIndex : 'FECHA_VEN_CREDITO',
				width : 150,
				sortable : false,
				hidden: true,
				align: 'center',							
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var fecha = inicializaTasasXPlazo(value, metadata, registro,'fechaVenciE');	
					if(select=='E'){
						fecha = (value=='')?'':value;
						//fecha = Ext.util.Format.dateRenderer((value=='')?'':value,'d/m/Y')
					}					
					return fecha
				}							
			},
			{				
				header : 'CG_REL_MAT',  //editable
				tooltip: 'CG_REL_MAT',
				dataIndex : 'CG_REL_MAT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var CgRelMat =  inicializaTasasXPlazo( value, metadata, registro,'CgRelMat');	
					if(select=='E'){
						CgRelMat  = (value=='')?'':value;
					}
						return CgRelMat;	
				}
			},
			selectModel
		],
		sm:selectModel,
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,	
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		frame: false,	
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',
				'-',
				{
					xtype: 'button',
					id: 'btnConfirmarN',
					text: 'Confirmar',
					handler: procesarConfirmar					
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnImprimirPDFN',
					handler: function(boton, evento) {
						//boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24forma01extPDF.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF'								
							}),
							callback: procesarSuccessFailurePDFNormal
						});
					}
				},				
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFN',
					hidden: true
				},
				'-',
				{
					text: 'Cancelar',
					xtype: 'button',
					id: 'btnCancelarN',
					handler: function() {						
						window.location = '24forma01ext.jsp';
					}
				}				
			]
		}
	});

	var moneda; 
	var totalDoctos =0; 
	var totalMonto  =0;
	var totalDoctosF  =0;
	var totalMontoF  =0;
	var totalInteresF  =0;
	var monedaUS;
	var totalDoctosUSD  =0;
	var totalMontoUSD   =0;
	var totalDoctosFUSD   =0; 
	var totalMontoFUSD  =0;
	var totalInteresFUSD  =0;
	var totalMontoDes =0;
	var totalMontoVa =0;
	var totalMontoDesUSD = 0;
	var totalMontoVaUSD =0;
	var totalMontoInteresFUSD =0;
	var totalMontoInteresF =0;
	var monto_valuadoT = 0;
	
	var selectModel2 = new Ext.grid.CheckboxSelectionModel( {		
		checkOnly: true,
		listeners: {
			rowdeselect: function(selectModel, rowIndex, record) {	
							
				if( record.data['IC_MONEDA_DOCTO']=='1') {							
					totalDoctosMN --;
					totalMontoMN		-= parseFloat(record.data['FN_MONTO']);
					var montoSinInteres  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescMN	-= montoSinInteres;
					totalInteresMN		-= parseFloat(record.data['MONTO_TASA_INT']);				
					totalMontoCreditoMN	-= parseFloat(record.data['MONTO_CAPITAL_INT']);
				}				
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='54' ){				
					totalDoctosUSD --;
					totalMontoUSD		-= parseFloat(record.data['FN_MONTO']);
					var montoSinInteresUSD  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescUSD	-= montoSinInteresUSD;
					totalInteresUSD		-= parseFloat(record.data['MONTO_TASA_INT']);	
					totalMontoCreditoUSD -=parseFloat(record.data['MONTO_CAPITAL_INT']);
				}
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){			
					totalDoctosConv --;
					totalMontosConv			-= parseFloat(record.data['FN_MONTO']);
					var montoSinInteresConv  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescConv 		-= montoSinInteresConv;
					totalInteresConv		-= parseFloat(record.data['MONTO_DESCUENTO']);
					totalConvPesos			-= parseFloat(record.data['MONTO_DESCUENTO']);
					totalMontoCreditoConv	-= parseFloat(record.data['MONTO_CAPITAL_INT']);
				}
			
				var dataTotales = [	
					[
					totalDoctosMN, totalMontoMN, totalMontoDescMN, totalInteresMN, 
					totalDoctosUSD, totalMontoUSD, totalMontoDescUSD, totalInteresUSD,
					totalDoctosConv, totalMontosConv, totalMontoDescConv, totalInteresConv
					]	];
					
				storeMontosData.loadData(dataTotales);
					
					if(record.data['IC_MONEDA_DOCTO']=='1'){								
					 totalMontoInteresF  =  totalInteresMN; 
					 totalInteresF  =  totalMontoCreditoMN; 				
					}
					if(record.data['IC_MONEDA_DOCTO']=='54'){							 						 
						 totalMontoInteresFUSD =  totalInteresUSD; 
						 totalInteresFUSD  =  totalMontoCreditoUSD; 
					}	
	
					if(record.data['IC_MONEDA_DOCTO']=='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){							 						 
						totalMontoInteresF =  totalInteresConv; 
						totalInteresF  =  totalConvPesos; 
					}	


				//esto es para obtener los datos del Gid de gridTotalesE 
				var store = gridTotalesE.getStore();			
				store.each(function(record) {							
					if(record.data['MONEDAT']=='MONEDA NACIONAL'){
					 record.data['MONTOINTERESF'] = totalMontoInteresF;
					 record.data['MONTOCAPITALINTERESF'] = totalInteresF;
					}
					if(record.data['MONEDAT']=='MONEDA USD'){	
					 record.data['MONTOINTERESF'] = totalMontoInteresFUSD;
					 record.data['MONTOCAPITALINTERESF'] = totalInteresFUSD;
					}	
					record.commit();	
				});
				
				//F05-2014 ----------------------------------
				monto_valuadoT  -= parseFloat(record.data['MONTO_VALUADO']);  					
				
				record.data['SELECCION']='N';
				record.commit();		
				
			},		
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //Se activa cuando una fila est� seleccionada, return false para cancelar.
			
			if(validaciones(selectModel, rowIndex, keepExisting, record) ) {			
					
					
				if( record.data['IC_MONEDA_DOCTO']=='1') {							
					totalDoctosMN ++;
					totalMontoMN		+= parseFloat(record.data['FN_MONTO']);
					var montoSinInteres  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescMN	+= montoSinInteres;
					totalInteresMN		+= parseFloat(record.data['MONTO_TASA_INT']);				
					totalMontoCreditoMN	+= parseFloat(record.data['MONTO_CAPITAL_INT']);
				}		
							
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='54' ){				
					totalDoctosUSD ++;
					totalMontoUSD		+= parseFloat(record.data['FN_MONTO']);
					var montoSinInteresUSD  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescUSD	+= montoSinInteresUSD;
					totalInteresUSD		+= parseFloat(record.data['MONTO_TASA_INT']);	
					totalMontoCreditoUSD+=parseFloat(record.data['MONTO_CAPITAL_INT']);
				}
				if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){			
					totalDoctosConv ++;
					totalMontosConv			+= parseFloat(record.data['FN_MONTO']);
					var montoSinInteresConv  = parseFloat(record.data['MONTO_CAPITAL_INT']) - parseFloat(record.data['MONTO_TASA_INT']); 
					totalMontoDescConv 		+= montoSinInteresConv;
					totalInteresConv		+= parseFloat(record.data['MONTO_DESCUENTO']);
					totalConvPesos			+= parseFloat(record.data['MONTO_DESCUENTO']);
					totalMontoCreditoConv	+= parseFloat(record.data['MONTO_CAPITAL_INT']);
				}
						
					var dataTotales = [	
					[
					totalDoctosMN, totalMontoMN, totalMontoDescMN, totalInteresMN, 
					totalDoctosUSD, totalMontoUSD, totalMontoDescUSD, totalInteresUSD,
					totalDoctosConv, totalMontosConv, totalMontoDescConv, totalInteresConv
					]	];
				  
					storeMontosData.loadData(dataTotales);
								
					if(record.data['IC_MONEDA_DOCTO']=='1'){								
					 totalMontoInteresF  =  totalInteresMN; 
					 totalInteresF  =  totalMontoCreditoMN; 				
					}
					if(record.data['IC_MONEDA_DOCTO']=='54'){							 						 
						 totalMontoInteresFUSD =  totalInteresUSD; 
						 totalInteresFUSD  =  totalMontoCreditoUSD; 
					}	
				
					if(record.data['IC_MONEDA_DOCTO']=='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){							 						 
						totalMontoInteresF =  totalInteresConv; 
						totalInteresF  =  totalConvPesos; 
					}	
						
				//esto es para obtener los datos del Gid de gridTotalesE 
				var store = gridTotalesE.getStore();			
				store.each(function(record) {							
					if(record.data['MONEDAT']=='MONEDA NACIONAL'){
					 record.data['MONTOINTERESF'] = totalMontoInteresF;
					 record.data['MONTOCAPITALINTERESF'] = totalInteresF;
					}
					if(record.data['MONEDAT']=='MONEDA USD'){	
					 record.data['MONTOINTERESF'] = totalMontoInteresFUSD;
					 record.data['MONTOCAPITALINTERESF'] = totalInteresFUSD;
					}	
					record.commit();	
				});
							
					//F05-2014 ---------------------------------------				
					monto_valuadoT  += parseFloat(record.data['MONTO_VALUADO']);  
					var montoDisSubLimite = parseFloat(record.data['MONTO_DIS_SUB_LIMITE']);
					var montoDisLinea = parseFloat(record.data['SALDO_DISPONIBLE']);			 		
								
					if(  (monto_valuadoT>montoDisSubLimite) ||  ( monto_valuadoT>montoDisLinea )  ) {
						Ext.MessageBox.alert("Mensaje","Por el momento alguno(s) de los documento(s) no puede ser seleccionado. "+
						"<br>Favor de comunicarse al centro de atenci�n a clientes NAFIN.");
						
						monto_valuadoT  -= parseFloat(record.data['MONTO_VALUADO']); 
						record.data['SELECCION']='N'; 
						record.commit();	
						return false;
					
					}else {				
						record.data['SELECCION']='S';
						record.commit();	
						return true;
					}
				}else{									
					record.data['SELECCION']='N';
					record.commit();	
					return false;
				}
				
			}
		}
	});
	
	
	var gridEditable = new Ext.grid.EditorGridPanel({
		id: 'gridDocumentos',
		title: 'Selecci�n de Documentos Modalidad 1 ',
		hidden: true,
		clicksToEdit: 1,			
		columns: [				
			{							
				header : 'SELECCION',
				tooltip: 'SELECCION',
				dataIndex : 'SELECCION',
				width : 150,
				align: 'left',
				hidden: true,	
				sortable : false
			},		
			{							
				header : 'SALDO_DISPONIBLE',
				tooltip: 'SALDO_DISPONIBLE',
				dataIndex : 'SALDO_DISPONIBLE',
				width : 150,
				align: 'left',	
				hidden: true,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			
			{							
				header : 'MONTO_DIS_SUB_LIMITE',
				tooltip: 'MONTO_DIS_SUB_LIMITE',
				dataIndex : 'MONTO_DIS_SUB_LIMITE',
				width : 150,
				align: 'left',	
				hidden: true,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : '<sup>1</sup>Epo',
				tooltip: 'Epo',
				dataIndex : 'NOMBRE_EPO',
				width : 150,
				align: 'left',
				sortable : false
			},
			{				
				header : '<sup>1</sup>Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex : 'IG_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : false
			},	
			{				
				header : '<sup>1</sup>Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex : 'ACUSE',
				width : 150,
				align: 'center',
				sortable : false
			},	
			{				
				header : '<sup>1</sup>Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex : 'DF_FECHA_EMISION',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : '<sup>1</sup>Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex : 'DF_FECHA_VENCIMIENTO',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : '<sup>1</sup>Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex : 'DF_FECHA_PUBLICACION',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : '<sup>1</sup>Plazo docto',
				tooltip: 'Plazo docto',
				dataIndex : 'IG_PLAZO_DOCTO',
				width : 150,
				sortable : false,
				align: 'center'
			},
			{				
				header : '<sup>1</sup>Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				sortable : false,
				align: 'left'
			},
			{				
				header : '<sup>1</sup>Monto',
				tooltip: 'Monto',
				dataIndex : 'FN_MONTO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : '<sup>1</sup>Categoria',
				tooltip: 'Categoria',
				dataIndex : 'CATEGORIA',
				width : 150,
				sortable : false,
				align: 'left'
			},
			{				
				header : '<sup>1</sup>Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'center'
			},		
			{				
				header : '<sup>1</sup>% de descuento',
				tooltip: '% de descuento',
				dataIndex : 'PORCENTAJE_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{				
				header : '<sup>1</sup>Monto de descuento',
				tooltip: 'Monto de descuentoo',
				dataIndex : 'MONTO_DESCUENTO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{				
				header : '<sup>1</sup>Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex : 'CG_TIPO_CONV',
				width : 150,
				sortable : false,
				align: 'center'
			},
			{				
				header : '<sup>1</sup>Tipo cambio.',
				tooltip: 'Tipo cambio.',
				dataIndex : 'TIPO_CAMBIO',
				width :150,
				sortable : false,
				align: 'center'
			},
			{				
				header : '<sup>1</sup>Monto valuado en pesos',
				tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VALUADO',
				width : 150,
				sortable : false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{				
				header : '<sup>1</sup>Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				width : 150,
				sortable : false,
				align: 'left'			
			},			
			{				
				header : '<sup>2</sup>N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex : 'IC_DOCUMENTO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'			
			},	
			{				
				header : '<sup>2</sup>Moneda',
				tooltip: 'Moneda',
				dataIndex : 'NOMBREMLINEA',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'			
			},	
			{				
				header : '<sup>2</sup>Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO_CREDITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'			
			},	
			{				
				header : '<sup>2</sup>IC_TIPO_FINANCIAMIENTO', 
				tooltip: 'IC_TIPO_FINANCIAMIENTO',
				dataIndex : 'IC_TIPO_FINANCIAMIENTO',
				width : 150,
				sortable : false,
				hidden: true,
				align: 'center'						
			},	
			{	
				header : '<sup>2</sup>Plazo', // es es editable 
				tooltip: 'Plazo ',
				dataIndex : 'PLAZO_CREDITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',					
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: false
				},
				renderer:function(value,metadata,registro){
				var accion=registro.data['IC_TIPO_FINANCIAMIENTO'];
				if(accion=='1' ||  accion=='4'){					
				}else {
					NE.util.colorCampoEdit(value,metadata,registro);
				}
					return value;	
				}
			},				
			{				
				header : '<sup>2</sup>Fecha de vencimiento', // es es editable 
				tooltip: 'Fecha de vencimiento',
				dataIndex : 'FECHA_VEN_CREDITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: campoFechas,				
				renderer:function(value,metadata,registro){
					var accion=registro.data['IC_TIPO_FINANCIAMIENTO'];
					if(accion=='1' ||  accion=='4'){
					}else {
						NE.util.colorCampoEdit(value,metadata,registro);
					}
					
					var select = registro.data['SELECCION'];
					var fecha;
					if(select=='E'){						
						fecha =  (value=='')?'':value;					
					}else {
						fecha = inicializaTasasXPlazo(value, metadata, registro,'fechaVenciE');	
					}			
					return fecha;				
				}	
			},	
			{				
				header : '<sup>2</sup>Fecha de operaci�n', 
				tooltip: 'Fecha de operaci�n',
				dataIndex : 'FECHA_OPERACION',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{				
				header : '<sup>2</sup>IF', 
				tooltip: 'IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'left'			
			},
			{				
				header : '<sup>2</sup>Referencia tasa de inter�s', //este se llena  de acuerdo a lo caputurado en 
				tooltip: 'Referencia tasa de inter�s',
				dataIndex : 'REFERENCIA_TASA',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				renderer:function(value,metadata,registro){		
					var select = registro.data['SELECCION'];	
					var refe = inicializaTasasXPlazo( value, metadata, registro,'ReferenciaTasa')
					if(select=='E'){
						refe = (value=='')?'':value;
					}
					return refe;
				}			
			},
			{				
				header : '<sup>2</sup>Valor tasa de inter�s',  //editable 
				tooltip: 'Valor tasa de inter�s',
				dataIndex : 'VALOR_TASA_PUNTOS',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',	
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];	
					var valort = Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'ValorTasaInteres'),'0.0000%');
					if(select=='E'){
						valort =  Ext.util.Format.number((value=='0')?'':value, '0.0000%');					
					}
					return valort;	
				}
				},
			 {				
				header : '<sup>2</sup>Monto de Intereses',  //editable 
				tooltip: 'Monto de Intereses',
				dataIndex : 'MONTO_TASA_INT',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'right',	
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];					
					var monto =  Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'MontodeIntereses'), '$0,0.00');	
					if(select=='E'){
						monto =  Ext.util.Format.number((value=='0')?'':value, '$0,0.00');
					}
					return monto;
				}				
			},			
			{				
				header : '<sup>2</sup>Monto Total de Capital e Inter�s',  //editable
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex : 'MONTO_CAPITAL_INT',
				width : 150,
				sortable : false,
				hidden: false,				
				align: 'right',	
				renderer:function(value,metadata,registro){
					var monto =  Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'MontoCapitalInteres'), '$0,0.00');
					var select = registro.data['SELECCION'];
					if(select=='E'){
						monto =Ext.util.Format.number((value=='0')?'':value, '$0,0.00');	
					}
					return monto;
				}			
			},			
			{				
				header : 'IC_TASA',  //editable
				tooltip: 'IC_TASA',
				dataIndex : 'IC_TASA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var tasa =  inicializaTasasXPlazo( value, metadata, registro,'ic_tasa');
					if(select=='E'){
						tasa  = (value=='')?'':value;
					}
					return tasa	
				}					
			},		
			{				
				header : 'MONTO_AUX_INT',  //editable
				tooltip: 'MONTO_AUX_INT',
				dataIndex : 'MONTO_AUX_INT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var montoAuxi =  inicializaTasasXPlazo( value, metadata, registro,'montoAuxi');
					if(select=='E'){
						montoAuxi  = (value=='')?'':value;
					}
						return  Ext.util.Format.number(montoAuxi, '$0,0.00');	
				}	
			},
			{				
				header : 'FN_PUNTOS',  //editable
				tooltip: 'FN_PUNTOS',
				dataIndex : 'FN_PUNTOS',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var puntos =  inicializaTasasXPlazo( value, metadata, registro,'puntos');
					if(select=='E'){
						puntos  = (value=='')?'':value;
					}
						return puntos;	
				}	
			},
			{				
				header : 'CG_REL_MAT',  //editable
				tooltip: 'CG_REL_MAT',
				dataIndex : 'CG_REL_MAT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var CgRelMat =  inicializaTasasXPlazo( value, metadata, registro,'CgRelMat');
					if(select=='E'){
						CgRelMat  = (value=='')?'':value;
					}
						return CgRelMat;	
				}
			},
			selectModel2
		],
		sm:selectModel2,	
		displayInfo: true,
		store: consultaData,
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		frame: false,		
		listeners: {	
		beforeedit: function(e){				
				var record = e.record;
				var gridEditable = e.gridEditable; 
				var campo= e.field;
				if(campo == 'PLAZO_CREDITO' || campo == 'FECHA_VEN_CREDITO'){
					var accion=record.data['IC_TIPO_FINANCIAMIENTO'];
					if(accion=='1' ||  accion=='4'){//retorno
						return false;
					}else {
						return true;
					}
				}				
			},			
			afteredit : function(e){
				var record = e.record;
				var gridEditable = e.gridEditable; 
				var campo= e.field;
				
				record.data['SELECCION']='E';			
				
				var montoInt = inicializaTasasXPlazo( '', '', record,'MontodeIntereses');
				record.data['MONTO_TASA_INT']=montoInt;			
								
				var montoCap = inicializaTasasXPlazo( '', '', record,'MontoCapitalInteres');
				record.data['MONTO_CAPITAL_INT']=montoCap;								
				
				var varTasa = inicializaTasasXPlazo( '', '', record,'ValorTasaInteres');
				record.data['VALOR_TASA_PUNTOS']=varTasa;				
	
				var referencia = inicializaTasasXPlazo( '', '', record,'ReferenciaTasa');
				record.data['REFERENCIA_TASA']=referencia;	
				
				var montoAuxi = inicializaTasasXPlazo( '', '', record,'montoAuxi');
				record.data['MONTO_AUX_INT']=montoAuxi;	
								
				var tasa =  inicializaTasasXPlazo( '', '', record,'ic_tasa');
				record.data['IC_TASA']=tasa;			
				
				var puntos=  inicializaTasasXPlazo( '', '', record,'puntos');
				record.data['FN_PUNTOS']=puntos;	
								
				var CgRelMat =  inicializaTasasXPlazo( '', '', record,'CgRelMat');				
				record.data['CG_REL_MAT']=CgRelMat;	
								
				if(campo=='FECHA_VEN_CREDITO') { 			restaFecha('', '', '', record); 		}
							
				if(campo=='PLAZO_CREDITO') { 				 sumaFecha('', '', '', record); 	}
				
				record.commit();		
			
			}
			
		},
		
		bbar: {
			xtype: 'toolbar',
			items: [					
				'->',
				'-',
				{
					xtype: 'button',
					id: 'btnConfirmarE',
					text: 'Confirmar',
					handler: procesarConfirmar					
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnImprimirPDFE',
					handler: function(boton, evento) {
						//boton.disable();
						boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '24forma01extPDF.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF'								
							}),
							callback: procesarSuccessFailurePDF
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFE',
					hidden: true
				},
				//-',
				{
					text: 'Cancelar',
					xtype: 'button',
					id: 'btnCancelarE',
					handler: function() {												
						window.location = '24forma01ext.jsp';
					}
				}				
			]
		}
	});	
	
	var ctexto1 = new Ext.form.FormPanel({		
		id: 'forma1',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',	
		style: 'margin:0 auto;',
		defaultType: 'textfield',
		html: texto1.join('')			
	});
		
	
	var ctexto2 = new Ext.form.FormPanel({		
		id: 'forma2',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',		
		style: 'margin:0 auto;',
		defaultType: 'textfield',
		html: texto2.join('')			
	});
	
		var ctexto3 = new Ext.form.FormPanel({		
		id: 'forma3',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',	
		style: 'margin:0 auto;',
		defaultType: 'textfield',
		html: texto3.join('')			
	});
		
		var ctexto4 = new Ext.form.FormPanel({		
		id: 'forma4',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',	
		style: 'margin:0 auto;',
		defaultType: 'textfield',
		html: texto2.join('')			
	});
	
	var ctexto5= new Ext.form.FormPanel({		
		id: 'forma5',
		width: 600,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',	
		style: 'margin:0 auto;',
		align: 'center',	
		defaultType: 'textfield',
		html: texto5.join('')			
	});
	
	
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 885,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
						
				var Comfirmacionclave1 = Ext.getCmp('Comfirmacionclave');
					if (Comfirmacionclave1) {		
					Comfirmacionclave1.destroy();		
				}
				var ventanaN = Ext.getCmp('winTotalesN');
				if (ventanaN) {					
					ventanaN.destroy();
				}	
										
				var clave_epo = Ext.getCmp("ic_epo1");
				if (Ext.isEmpty(clave_epo.getValue()) ) {
					clave_epo.markInvalid('El valor de la EPO es requerido.');
					return;
				}	
				
				var fn_monto_de = Ext.getCmp("fn_monto_de1");
				var fn_monto_a = Ext.getCmp("fn_monto_a1");
				if (!Ext.isEmpty(fn_monto_de.getValue()) && Ext.isEmpty(fn_monto_a.getValue())    
					||  Ext.isEmpty(fn_monto_de.getValue()) && !Ext.isEmpty(fn_monto_a.getValue())    ) {
					fn_monto_a.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
					fn_monto_de.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
					return;
				}
									
				var df_fecha_emision_de = Ext.getCmp("df_fecha_emision_de1");
				var df_fecha_emision_a = Ext.getCmp("df_fecha_emision_a1");
				if (!Ext.isEmpty(df_fecha_emision_de.getValue()) && Ext.isEmpty(df_fecha_emision_a.getValue())    
					||  Ext.isEmpty(df_fecha_emision_de.getValue()) && !Ext.isEmpty(df_fecha_emision_a.getValue())    ) {
					df_fecha_emision_de.markInvalid('Debe capturar ambas fechas de emision o dejarlas en blanco');
					df_fecha_emision_a.markInvalid('Debe capturar ambas fechas de emision o dejarlas en blanco');
					return;
				}
				var df_fecha_venc_de = Ext.getCmp("df_fecha_venc_de1");
				var df_fecha_venc_a = Ext.getCmp("df_fecha_venc_a1");
				if (!Ext.isEmpty(df_fecha_venc_de.getValue()) && Ext.isEmpty(df_fecha_venc_a.getValue())    
					|| Ext.isEmpty(df_fecha_venc_de.getValue()) && !Ext.isEmpty(df_fecha_venc_a.getValue())    ) {
					df_fecha_venc_de.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
					df_fecha_venc_a.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
					return;
				}
				var fecha_publicacion_de = Ext.getCmp("fecha_publicacion_de1");
				var fecha_publicacion_a = Ext.getCmp("fecha_publicacion_a1");
				if (!Ext.isEmpty(fecha_publicacion_de.getValue()) && Ext.isEmpty(fecha_publicacion_a.getValue())    
					||  Ext.isEmpty(fecha_publicacion_de.getValue()) && !Ext.isEmpty(fecha_publicacion_a.getValue())    ) {
					fecha_publicacion_de.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');
					fecha_publicacion_a.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');
					return;
				}		
					
					
					fp.el.mask('Enviando...', 'x-mask-loading');				
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});	
					
					
					totalesDataN.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ResumenTotales'
						})
					});	
					
					totalesDataE.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ResumenTotales'
						})
					});	
					
					monto_valuadoT = 0; //se limpia Variable que 
					
					Ext.getCmp('btnImprimirPDFN').disable();
					Ext.getCmp('btnBajarPDFN').hide();		
					Ext.getCmp('btnCancelarN').disable();
					Ext.getCmp('btnConfirmarN').disable();	
					
				
					Ext.getCmp('btnImprimirPDFE').disable();
					Ext.getCmp('btnBajarPDFE').hide();
					Ext.getCmp('btnCancelarE').disable();
					Ext.getCmp('btnConfirmarE').disable();				
					gridTotalesE.hide();					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24forma01ext.jsp';
				}
			}
		]			
	});	
	
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			applyTo: 'areaContenido',
			width: 949,
			style: 'margin:0 auto;',
			items: [
				fp,			
				NE.util.getEspaciador(20),
				ctexto1,	
				ctexto5,	
				NE.util.getEspaciador(20),
				gridEditable,				
				gridNormal,
				gridPreAcuse,
				gridAcuse,
				NE.util.getEspaciador(20),	
				gridTotalesN,
				gridTotalesE ,	
				gridMontos,
				NE.util.getEspaciador(20),
				ctexto2,
				ctexto4,
				NE.util.getEspaciador(20),
				ctexto3,
				gridCifrasControl
			]
		});
		
	catalogoEPOData.load();
	catalogoMonedaData.load();	
	catalogoModalidadPlazoData.load();
		
	var procesaIniciales =  function(ic_epo) {	
	fp.el.mask('Cargando Valores Iniciales...', 'x-mask-loading');	
		Ext.Ajax.request({
			url: '24forma01.data.jsp',
			params: {
				informacion: "valoresIniciales",
				ic_epo: ic_epo
			},			
			callback: procesaValoresIniciales				
		});	
	}
	
	});