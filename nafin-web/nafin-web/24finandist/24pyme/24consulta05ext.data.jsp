<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion   = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
StringBuffer condicion = new StringBuffer();
String infoRegresar  = "";

if (informacion.equals("CatalogoEPODist")) {

  CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClavePyme(iNoCliente);
	infoRegresar = cat.getJSONElementos();
  
}else if (
	informacion.equals("Consulta") || 
	informacion.equals("ArchivoCSV") ||
	informacion.equals("ArchivoPDF") ||
	informacion.equals("Consulta_b") || 
	informacion.equals("ArchivoCSV_b") ||
	informacion.equals("ArchivoPDF_b") ){
  
	String _ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
  
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
  
	String tipoCredito = cargaDocto.getTipoCredito(_ic_epo, iNoCliente);
  
	com.netro.distribuidores.Cons3InfDocEpoDist paginador = new com.netro.distribuidores.Cons3InfDocEpoDist();
	paginador.setClaveEpo(_ic_epo);
	paginador.setClavePyme(iNoCliente);
	paginador.setCgTipoCredito(tipoCredito);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros reg = queryHelper.doSearch();
	if (informacion.equals("Consulta") && "D".equals(tipoCredito)) {		//Datos para la Consulta con Paginacion
		try {
			reg = queryHelper.doSearch();
			infoRegresar =  "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData() +", \"tipoCredito\":\"" + tipoCredito + "\"}";
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("Consulta_b") && "C".equals(tipoCredito)) {		//Datos para la Consulta con Paginacion
      try {
			reg = queryHelper.doSearch();
			infoRegresar =  "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+"}";
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
      }
  } else if (informacion.equals("ArchivoCSV") && "D".equals(tipoCredito)) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ArchivoPDF") && "D".equals(tipoCredito)) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}else if (informacion.equals("ArchivoCSV_b") && "C".equals(tipoCredito)) {
      try {
        String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV_b");
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
        infoRegresar = jsonObj.toString();
      } catch(Throwable e) {
        throw new AppException("Error al generar el archivo CSV", e);
      }
  }else if (informacion.equals("ArchivoPDF_b") && "C".equals(tipoCredito)) {
      try {
        String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF_b");
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
        infoRegresar = jsonObj.toString();
      } catch(Throwable e) {
        throw new AppException("Error al generar el archivo PDF", e);
      }
  }
}
%>
<%=infoRegresar%>

