<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoIFDistribuidores,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
//Parámetros provenientes de la página actual
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String numDocto = request.getParameter("numDoc")!= null ? request.getParameter("numDoc"):"";

String fechaHoy		= "";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}
String ic_epo              = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");	
String ic_moneda           = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
String ic_cambio_estatus   = (request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
String modplazo            = (request.getParameter("modplazo")==null)?"":request.getParameter("modplazo");
String fn_monto_de         = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
String fn_monto_a          = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
String ig_numero_docto     = (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
String mto_descuento       = (request.getParameter("mto_descuento")==null)?"N":request.getParameter("mto_descuento");
String cc_acuse            = (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
String doctos_cambio       = (request.getParameter("doctos_cambio")==null)?"N":request.getParameter("doctos_cambio");
String df_fecha_emision_de = (request.getParameter("df_fecha_emision_de")==null)?"":request.getParameter("df_fecha_emision_de");
String df_fecha_emision_a  = (request.getParameter("df_fecha_emision_a")==null)?"":request.getParameter("df_fecha_emision_a");
String df_fecha_venc_de    = (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
String df_fecha_venc_a     = (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
String df_fecha_pub_de     = (request.getParameter("df_fecha_pub_de")==null)?"":request.getParameter("df_fecha_pub_de");
String df_fecha_pub_a      = (request.getParameter("df_fecha_pub_a")==null)?"":request.getParameter("df_fecha_pub_a");
String hidAction           =(request.getParameter("hidAction")==null)?fechaHoy:request.getParameter("hidAction");

String tipo_credito = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
String cgTipoConversion	= (request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
String infoRegresar = "";

if(informacion.equals("valoresIniciales")) {
		if("".equals(tipo_credito)){
				tipo_credito = cargaDocto.getTipoCredito(iNoEPO,iNoCliente);
		}
		if("".equals(cgTipoConversion)){
				cgTipoConversion = cargaDocto.getTipoConversion(iNoCliente);
		}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("cgTipoConversion",cgTipoConversion);
		jsonObj.put("tipo_credito",tipo_credito);
		infoRegresar = jsonObj.toString();
}
else if (informacion.equals("obtenCambioDoctos")) {	
	if(numDocto != null && !numDocto.equals("")) {
		DetalleCambiosDoctos dataCambios = new DetalleCambiosDoctos();
		dataCambios.setIcDocumento(numDocto);
		Registros registros = dataCambios.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}	
}
else if(informacion.equals("CatalogoEPODist")){
		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setClavePyme(iNoCliente);
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
}
else if (informacion.equals("CatalogoEstatusDist")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_cambio_estatus");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_cambio_estatus");
		cat.setValoresCondicionIn("34,23,2,24", Integer.class);
		cat.setOrden("ic_cambio_estatus");
		infoRegresar = cat.getJSONElementos();
}
else if (informacion.equals("CatalogoMonedaDist")){
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre");
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();
}
else if (informacion.equals("CatalogoModoPlazoDist")){
		String valida	=	"";
		valida = cargaDocto.getValidaIn(iNoEPO,iNoCliente);
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_tipo_financiamiento");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_tipo_financiamiento");
		cat.setValoresCondicionIn(valida, Integer.class);
		infoRegresar = cat.getJSONElementos();
}
else if (informacion.equals("Consulta")) {
		int start = 0;
		int limit = 0;
		
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
		} catch (Exception e) {
				throw new AppException("Error en los parámetros recibidos",e);
		}
		CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsNoOperPymeDist());
		try{
				if (operacion.equals("Generar")) { //Nueva consulta
						queryHelper.executePKQuery(request);
				}
				infoRegresar=queryHelper.getJSONPageResultSet(request,start,limit);
		} catch(Exception e){
				throw new AppException("Error en la paginación", e);
		}
} 
else if (informacion.equals("ResumenTotales")){
		CQueryHelperExtJS queryHelper = new CQueryHelperExtJS();//toma el valor de sesión
		infoRegresar = queryHelper.getJSONResultCount(request);

} else if(informacion.equals("ArchivoTotalPDF")){

	JSONObject jsonObj               = new JSONObject();
	ConsNoOperPymeDist paginador     = new ConsNoOperPymeDist();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String ic_pyme                   = request.getSession().getAttribute("iNoCliente").toString();

	if("".equals(cgTipoConversion)){
		cgTipoConversion = cargaDocto.getTipoConversion(iNoCliente);
	}

	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_epo(ic_epo);
	paginador.setIc_moneda(ic_moneda);
	paginador.setIc_cambio_estatus(ic_cambio_estatus);
	paginador.setModplazo(modplazo);
	paginador.setFn_monto_de(fn_monto_de);
	paginador.setFn_monto_a(fn_monto_a);
	paginador.setIg_numero_docto(ig_numero_docto);
	paginador.setMto_descuento(mto_descuento);
	paginador.setCc_acuse(cc_acuse);
	paginador.setDoctos_cambio(doctos_cambio);
	paginador.setDf_fecha_emision_de(df_fecha_emision_de);
	paginador.setDf_fecha_emision_a(df_fecha_emision_a);
	paginador.setDf_fecha_venc_de(df_fecha_venc_de);
	paginador.setDf_fecha_venc_a(df_fecha_venc_a);
	paginador.setDf_fecha_pub_de(df_fecha_pub_de);
	paginador.setDf_fecha_pub_a(df_fecha_pub_a);
	paginador.setCgTipoConversion(cgTipoConversion);
	paginador.setTipo_credito(tipo_credito);

	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	infoRegresar = jsonObj.toString();

}
%>
<%=infoRegresar%>
