Ext.onReady(function() {

	var jsonValoresIniciales = null;
	var cveEstatus		= "";
	var ic_documento	= "";
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
				if (!fp.isVisible())	{
					fp.show();
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//--------Handlers---------
	var muestraGridCambios = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		ic_documento = registro.get('IC_DOCUMENTO');
		cambioDoctosData.load({params:	{ic_docto:	ic_documento}});
		var ventana = Ext.getCmp('cambioDoctos');
		if (ventana) {
			ventana.destroy();
		}
		new Ext.Window({
			layout: 'fit',
			width: 800,
			height: 200,
			id: 'cambioDoctos',
			closeAction: 'hide',
			items: [
				gridCambiosDoctos
			],
			title: 'Documento'
			}).show();//.alignTo(grid.el,"l");
	}
//
	var procesarCambioDoctosData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var gridCambios = Ext.getCmp('gridCambios');
			var el = gridCambios.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ningun cambio para este documento', 'x-mask');
			}
		}
	}

	var procesarSuccessFailureGraArvoVenceSin =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGraArvoVenceSin');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArvoVenceSin');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfVenceSin =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfVenceSin');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfVenceSin');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGraArvoNego =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGraArvoNego');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArvoNego');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfNego =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfNego');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNego');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGraArvoSelec =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGraArvoSelec');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArvoSelec');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfSelec =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfSelec');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfSelec');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGraArvoOpera =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGraArvoOpera');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArvoOpera');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfOpera =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfOpera');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfOpera');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGraArvoOperaPaga =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGraArvoOperaPaga');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArvoOperaPaga');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfOperaPaga =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfOperaPaga');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfOperaPaga');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGraArvoOperaVence =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGraArvoOperaVence');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArvoOperaVence');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfOperaVence =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfOperaVence');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfVence');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGraArvoBaja =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGraArvoBaja');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArvoBaja');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfBaja =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfBaja');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfBaja');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGraArvoNoNego =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGraArvoNoNego');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArvoNoNego');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfNoNego =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfNoNego');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNoNego');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaVenceSinOpera = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfVenceSin');
			var btnBajarArchivo = Ext.getCmp("btnBajarArvoVenceSin");
			btnBajarPDF.hide();
			btnBajarArchivo.hide();
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfVenceSin');
			var btnGenerarArchivo = Ext.getCmp("btnGraArvoVenceSin");
			if (!gridVenceSinOpera.isVisible()) {gridVenceSinOpera.show();}
			var el = gridVenceSinOpera.getGridEl();
			var cm = gridVenceSinOpera.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
				if(store.getTotalCount() > 0) {
					if (jsonValoresIniciales.cgTipoConversion != "" &&	jsonValoresIniciales.cgTipoConversion != undefined)	{
						cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
						cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
						cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);
					}
					btnGenerarPDF.enable();
					btnGenerarArchivo.enable();
					el.unmask();
				} else {
					btnGenerarArchivo.disable();
					btnGenerarPDF.disable();
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
		}
	}

	var procesarConsultaNegociable = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNego');
			var btnBajarArchivo = Ext.getCmp("btnBajarArvoNego");
			btnBajarPDF.hide();
			btnBajarArchivo.hide();
			var btnGenerarArchivo = Ext.getCmp("btnGraArvoNego");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfNego');
			if (!gridNegociable.isVisible()) {
				gridNegociable.show();
			}
			var el = gridNegociable.getGridEl();
			var cm = gridNegociable.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.cgTipoConversion != "" && jsonValoresIniciales.cgTipoConversion != undefined)	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);
				}
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

///*****************-*-*-*-*--*-*
	var procesarConsultaNegociable32 = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNego');
			var btnBajarArchivo = Ext.getCmp("btnBajarArvoNego");
			btnBajarPDF.hide();
			btnBajarArchivo.hide();
			var btnGenerarArchivo = Ext.getCmp("btnGraArvoNego");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfNego');
			if (!gridNegociable32.isVisible()) {
				gridNegociable32.show();
			}
			var el = gridNegociable32.getGridEl();
//			var cm = gridNegociable32.getColumnModel();
		//	cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
		//	cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
		//	cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
			if(store.getTotalCount() > 0) {
		/*		if (jsonValoresIniciales.cgTipoConversion != "" && jsonValoresIniciales.cgTipoConversion != undefined)	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);
				}*/
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
///******************-*--*-*-***-

	var procesarConsultaSelecPyme = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfSelec');
			var btnBajarArchivo = Ext.getCmp("btnBajarArvoSelec");
			btnBajarPDF.hide();
			btnBajarArchivo.hide();
			var btnGenerarArchivo = Ext.getCmp("btnGraArvoSelec");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfSelec');
			if (!gridSelecPyme.isVisible()) {
				gridSelecPyme.show();
			}
			var el = gridSelecPyme.getGridEl();
			var cm = gridSelecPyme.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.cgTipoConversion != "" && jsonValoresIniciales.cgTipoConversion != undefined)	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);
				}
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaOperada = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfOpera');
			var btnBajarArchivo = Ext.getCmp("btnBajarArvoOpera");
			btnBajarPDF.hide();
			btnBajarArchivo.hide();
			var btnGenerarArchivo = Ext.getCmp("btnGraArvoOpera");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfOpera');
			if (!gridOperada.isVisible()) {
				gridOperada.show();
			}
			var el = gridOperada.getGridEl();
			var cm = gridOperada.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.cgTipoConversion != "" && jsonValoresIniciales.cgTipoConversion != undefined)	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);
				}
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaOperaPaga = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfOperaPaga');
			var btnBajarArchivo = Ext.getCmp("btnBajarArvoOperaPaga");
			btnBajarPDF.hide();
			btnBajarArchivo.hide();
			var btnGenerarArchivo = Ext.getCmp("btnGraArvoOperaPaga");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfOperaPaga');
			if (!gridOperaPaga.isVisible()) {
				gridOperaPaga.show();
			}
			var el = gridOperaPaga.getGridEl();
			var cm = gridOperaPaga.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.cgTipoConversion != "" && jsonValoresIniciales.cgTipoConversion != undefined)	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);
				}
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaOperaVence = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfOperaVence');
			var btnBajarArchivo = Ext.getCmp("btnBajarArvoOperaVence");
			btnBajarPDF.hide();
			btnBajarArchivo.hide();
			var btnGenerarArchivo = Ext.getCmp("btnGraArvoOperaVence");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfOperaVence');
			if (!gridOperaVence.isVisible()) {
				gridOperaVence.show();
			}
			var el = gridOperaVence.getGridEl();
			var cm = gridOperaVence.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.cgTipoConversion != "" && jsonValoresIniciales.cgTipoConversion != undefined)	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);
				}
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaBaja = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfBaja');
			var btnBajarArchivo = Ext.getCmp("btnBajarArvoBaja");
			btnBajarPDF.hide();
			btnBajarArchivo.hide();
			var btnGenerarArchivo = Ext.getCmp("btnGraArvoBaja");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfBaja');
			if (!gridBaja.isVisible()) {
				gridBaja.show();
			}
			var el = gridBaja.getGridEl();
			var cm = gridBaja.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.cgTipoConversion != "" && jsonValoresIniciales.cgTipoConversion != undefined)	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);
				}
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaNoNego = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNoNego');
			var btnBajarArchivo = Ext.getCmp("btnBajarArvoNoNego");
			btnBajarPDF.hide();
			btnBajarArchivo.hide();
			var btnGenerarArchivo = Ext.getCmp("btnGraArvoNoNego");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfNoNego');
			if (!gridNoNego.isVisible()) {
				gridNoNego.show();
			}
			var el = gridNoNego.getGridEl();
			var cm = gridNoNego.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.cgTipoConversion != "" && jsonValoresIniciales.cgTipoConversion != undefined)	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);
				}
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
//NUEVO handler para el nuevo estatus En Proceso de Autorizacion
	var procesaConsultaProcesoAutorizacion = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfProcesoAutorizacion');
			var btnBajarArchivo = Ext.getCmp("btnBajarProcesoAutorizacion");
			btnBajarPDF.hide();
			btnBajarArchivo.hide();
			var btnGenerarArchivo = Ext.getCmp("btnGraArvoProcesoAutorizacion");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfProcesoAutorizacion');
			if (!gridProcesoAutorizacion.isVisible()) {
				gridProcesoAutorizacion.show();
			}
			var el = gridProcesoAutorizacion.getGridEl();
			var cm = gridProcesoAutorizacion.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.cgTipoConversion != "" && jsonValoresIniciales.cgTipoConversion != undefined)	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);
				}
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
////Handler para dscargar los archivos PDF CSV En Proceso de autorizacion IF
var procesarSuccessFailureGraArvoProcesoAutorizacion =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGraArvoProcesoAutorizacion');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarProcesoAutorizacion');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfProcesoAutorizacion =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfProcesoAutorizacion');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfProcesoAutorizacion');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
//Fin de handlers.------

//---------------------------store--------------------------

	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatusStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaVenceSinOpera = new Ext.data.JsonStore({
		root: 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridVenceSinOpera'
		},
		fields: [
			{name: 'NOMBREEPO'},	//NOMBREEPO
			{name: 'IG_NUMERO_DOCTO'}, 
			{name: 'CC_ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FECHA_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'CD_NOMBRE'},	//MONEDA
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VAL',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESC',type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'PANTALLA'},
			{name: 'CG_VENTACARTERA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaVenceSinOpera,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaVenceSinOpera(null, null, null);
				}
			}
		}
	});

	var consultaNegociable = new Ext.data.JsonStore({
		root: 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridNegociable'
		},
		fields: [
			{name: 'NOMBREEPO'},	//NOMBREEPO
			{name: 'IG_NUMERO_DOCTO'}, 
			{name: 'CC_ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FECHA_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'CD_NOMBRE'},	//MONEDA
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VAL',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESC',type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'PANTALLA'},
			{name: 'CG_VENTACARTERA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaNegociable,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaNegociable(null, null, null);
				}
			}
		}
	});

//*********--**-*--**
	var consultaNegociable32 = new Ext.data.JsonStore({
		root: 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridNegociable',
      claveStatus: 32
		},
		fields: [
			{name: 'NOMBREEPO'},	
			{name: 'IG_NUMERO_DOCTO'}, 
			{name: 'CC_ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FECHA_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'CD_NOMBRE'},	//MONEDA
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESC',type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'CG_VENTACARTERA'},
      {name: 'IC_DOCUMENTO'},//nuevos
      {name: 'FN_MONTO',		type: 'float'},
      {name: 'IG_PLAZO_DESCUENTO'},
      {name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
      {name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
      {name : 'NOMBRE_IF'},
      {name : 'MONTOFINAL'},
		{name	: 'CG_CODIGO_BIN'},
      {name : 'DESC_BINS'},
      {name : 'FN_MONTO',		type: 'float'},//**doc fin
      {name : 'ORDEN_ENVIADO'},
      {name : 'ID_OPERACION'},
      {name : 'CODIGO_AUTORIZA'},
      {name : 'FECHA_REGISTRO'},
		{name : 'NUM_TARJETA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaNegociable32,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaNegociable32(null, null, null);
				}
			}
		}
	});
///****-**--*

	var consultaSelecPyme = new Ext.data.JsonStore({
		root: 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridSelecPyme'
		},
		fields: [
			{name: 'NOMBREEPO'},	//NOMBREEPO
			{name: 'IG_NUMERO_DOCTO'}, 
			{name: 'CC_ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FECHA_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'CD_NOMBRE'},	//MONEDA
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VAL',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESC',type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'NOMBREIF'},
			{name: 'TIPO_CRED'},
			{name: 'FECHA_OPERA',		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_IMPORTE_RECIBIR',		type: 'float'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'FECHA_VENCRE',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'REF_TASA'},
			{name: 'FN_VALOR_TASA',		type: 'float'},
			{name: 'FN_IMPORTE_INTERES',		type: 'float'},
			{name: 'TIPO_COB_INT'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'CG_VENTACARTERA'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaSelecPyme,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaSelecPyme(null, null, null);
				}
			}
		}
	});

	var consultaOperada = new Ext.data.JsonStore({
		root: 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridOperada'
		},
		fields: [
			{name: 'NOMBREEPO'},	//NOMBREEPO
			{name: 'IG_NUMERO_DOCTO'}, 
			{name: 'CC_ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FECHA_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'CD_NOMBRE'},	//MONEDA
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VAL',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESC',type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'NOMBREIF'},
			{name: 'TIPO_CRED'},
			{name: 'FECHA_OPERA',		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_IMPORTE_RECIBIR',		type: 'float'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'FECHA_VENCRE',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'REF_TASA'},
			{name: 'FN_VALOR_TASA',		type: 'float'},
			{name: 'FN_IMPORTE_INTERES',		type: 'float'},
			{name: 'TIPO_COB_INT'},
			{name: 'FECHA_OPER_IF',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_CAMBIO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'CG_VENTACARTERA'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaOperada,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaOperada(null, null, null);
				}
			}
		}
	});

	var consultaOperaPaga = new Ext.data.JsonStore({
		root: 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridOperaPaga'
		},
		fields: [
			{name: 'NOMBREEPO'},	//NOMBREEPO
			{name: 'IG_NUMERO_DOCTO'}, 
			{name: 'CC_ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FECHA_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'CD_NOMBRE'},	//MONEDA
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VAL',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESC',type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'NOMBREIF'},
			{name: 'TIPO_CRED'},
			{name: 'FECHA_OPERA',		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_IMPORTE_RECIBIR',		type: 'float'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'FECHA_VENCRE',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'REF_TASA'},
			{name: 'FN_VALOR_TASA',		type: 'float'},
			{name: 'FN_IMPORTE_INTERES',		type: 'float'},
			{name: 'TIPO_COB_INT'},
			{name: 'FECHA_OPER_IF',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_CAMBIO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'CG_VENTACARTERA'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaOperaPaga,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaOperaPaga(null, null, null);
				}
			}
		}
	});

	var consultaOperaVence = new Ext.data.JsonStore({
		root: 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridOperaVence'
		},
		fields: [
			{name: 'NOMBREEPO'},	//NOMBREEPO
			{name: 'IG_NUMERO_DOCTO'}, 
			{name: 'CC_ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FECHA_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'CD_NOMBRE'},	//MONEDA
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VAL',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESC',type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'NOMBREIF'},
			{name: 'TIPO_CRED'},
			{name: 'FECHA_OPERA',		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_IMPORTE_RECIBIR',		type: 'float'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'FECHA_VENCRE',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'REF_TASA'},
			{name: 'FN_VALOR_TASA',		type: 'float'},
			{name: 'FN_IMPORTE_INTERES',		type: 'float'},
			{name: 'TIPO_COB_INT'},
			{name: 'FECHA_OPER_IF',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_CAMBIO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'CG_VENTACARTERA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaOperaVence,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaOperaVence(null, null, null);
				}
			}
		}
	});

	var consultaBaja = new Ext.data.JsonStore({
		root: 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridBaja'
		},
		fields: [
			{name: 'NOMBREEPO'},	//NOMBREEPO
			{name: 'IG_NUMERO_DOCTO'}, 
			{name: 'CC_ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FECHA_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'CD_NOMBRE'},	//MONEDA
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VAL',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESC',type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'PANTALLA'},
			{name: 'CG_VENTACARTERA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaBaja,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaBaja(null, null, null);
				}
			}
		}
	});

	var consultaNoNego = new Ext.data.JsonStore({
		root: 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridNoNego'
		},
		fields: [
			{name: 'NOMBREEPO'},	//NOMBREEPO
			{name: 'IG_NUMERO_DOCTO'}, 
			{name: 'CC_ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FECHA_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'CD_NOMBRE'},	//MONEDA
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VAL',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESC',type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'PANTALLA'},
			{name: 'CG_VENTACARTERA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaNoNego,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaNoNego(null, null, null);
				}
			}
		}
	});

	var cambioDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {informacion: 'obtenCambioDoctos'},
		fields: [
			{name: 'FECH_CAMBIO', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CD_DESCRIPCION'},
			{name: 'FECH_EMI_ANT', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECH_EMI_NEW', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_MONTO_ANTERIOR',	type: 'float'},
			{name: 'FN_MONTO_NUEVO', 		type: 'float'},
			{name: 'FECH_VENC_ANT', 		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECH_VENC_NEW', 		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'MODO_PLAZO_ANTERIOR'},
			{name: 'MODO_PLAZO_NUEVO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCambioDoctosData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarCambioDoctosData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});
	
	//STORE PARA EL NUEVO ESTATUS "En Proceso de Autorizacion IF"
var consultaProcesoAutorizacion = new Ext.data.JsonStore({
		root: 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaProcesoAutorizacion'
		},
		fields: [
			{name: 'NOMBREEPO'},	//NOMBREEPO
			{name: 'IG_NUMERO_DOCTO'}, 
			{name: 'CC_ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FECHA_CARGA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'CD_NOMBRE'},	//MONEDA
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VAL',		type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESC',type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'NOMBREIF'},
			{name: 'TIPO_CRED'},
			{name: 'FECHA_OPERA',		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_IMPORTE_RECIBIR',		type: 'float'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'FECHA_VENCRE',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'REF_TASA'},
			{name: 'FN_VALOR_TASA',		type: 'float'},
			{name: 'FN_IMPORTE_INTERES',		type: 'float'},
			{name: 'TIPO_COB_INT'},
			{name: 'FECHA_OPER_IF',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_CAMBIO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'CG_VENTACARTERA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesaConsultaProcesoAutorizacion,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesaConsultaProcesoAutorizacion(null, null, null);
				}
			}
		}
	});
//----------------------------------Contenido----------------------------------
// NUEVO grid para el nuevo estatus En Proceso de Autorizacion IF
	var gridProcesoAutorizacion = new Ext.grid.GridPanel({
		store: consultaProcesoAutorizacion,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},
			{
				header: 'Num. acuse de carga', tooltip: 'N�mero acuse de carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true, width : 120,	hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',	
				dataIndex : 'FECHA_CARGA',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'CD_NOMBRE',
				sortable : true,	width : 130,	hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo para descuento', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,	width : 100, align: 'right', hidden: false,
				renderer:	Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_DESC',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true
			},
			{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == 1)	{
												value = "";
											}
											return Ext.util.Format.number(value, '$0,0.00');
										}
			},
			{
				header : 'Monto valuado', tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VAL',
				sortable : true,	width : 120,	hidden: true,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},
			{
				header: 'N�mero de documento final', tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',
				dataIndex : 'FN_IMPORTE_RECIBIR',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo', tooltip: 'Plazo',
				dataIndex : 'IG_PLAZO_CREDITO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECHA_VENCRE',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}/*,
			{
				header : 'Fecha de Operaci�n IF', tooltip: 'Fecha de Operaci�n IF',
				dataIndex : 'FECHA_OPER_IF',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}*/,
			{
				header : 'IF', tooltip: 'IF',
				dataIndex : 'NOMBREIF',	
				sortable : true,	width : 150,	hidden: false
			},
			{
				header : 'Referencia tasa de inter�s', tooltip: 'Referencia tasa de inter�s',
				dataIndex : 'REF_TASA',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Valor tasa de inter�s', tooltip: 'Valor tasa de inter�s',
				dataIndex : 'FN_VALOR_TASA',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto de intereses', tooltip: 'Monto de intereses',
				dataIndex : 'FN_IMPORTE_INTERES',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto total de Capital e Inter�s', tooltip: 'Monto total de Capital e Inter�s',
				//dataIndex : '',		CALCULADO
				sortable : true,	width : 120,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){ 
											value = "";
											if (registro.get('FN_IMPORTE_RECIBIR') != "" && registro.get('FN_IMPORTE_INTERES') != "")	{
												value = (parseFloat(registro.get('FN_IMPORTE_RECIBIR')) + parseFloat(registro.get('FN_IMPORTE_INTERES')));	
											}
											return Ext.util.Format.number(value, '$0,0.00');	
										}
			}/*,
			{
				header : 'Fecha de Cambio de Estatus', tooltip: 'Fecha de Cambio de Estatus',
				dataIndex : 'FECHA_CAMBIO',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Cambios del documento', tooltip: 'Cambios del documento',
				dataIndex : 'ESTATUS',		//IC_DOCUMENTO
				sortable : true,	width : 150,	hidden: false
			}*/
		],
		stripeRows: true,	loadMask: true,	height: 400,	width: 940,	frame: true,
		title: 'Estatus: En Proceso de Autorizacion IF',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',	tooltip:	'Generar archivo CSV',
					id: 'btnGraArvoProcesoAutorizacion',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGraArvoProcesoAutorizacion
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',	tooltip:	'Descargar archivo CSV',
					id: 'btnBajarProcesoAutorizacion',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',	tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfProcesoAutorizacion',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfProcesoAutorizacion
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',	tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfProcesoAutorizacion',	hidden: true
				}
			]
		}
	});

	var gridVenceSinOpera = new Ext.grid.GridPanel({
		store: consultaVenceSinOpera,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},
			{
				header: 'Num. acuse de carga', tooltip: 'N�mero acuse de carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true, width : 120,	hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',	
				dataIndex : 'FECHA_CARGA',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'CD_NOMBRE',
				sortable : true,	width : 130,	hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo para descuento en d�as', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,	width : 100, align: 'right', hidden: false,
				renderer:	Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_DESC',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true
			},
			{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == 1)	{
												value = "";
											}
											return Ext.util.Format.number(value, '$0,0.00');
										}
			},
			{
				header : 'Monto valuado en pesos', tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VAL',
				sortable : true,	width : 120,	hidden: true,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},{
				xtype:	'actioncolumn',
				header : 'Cambios del documento', tooltip: 'Cambios del documento',
				dataIndex : 'IC_DOCUMENTO',
				width:	100,	align: 'center',
				//renderer: function(value, metadata, record, rowindex, colindex, store) {
				//				return (record.get('IC_DOCUMENTO'));
				//			 },
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('IC_DOCUMENTO')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraGridCambios
					}
				]
			}
		],
		stripeRows: true,	loadMask: true,	height: 400,	width: 940,	frame: true,
		title: 'Estatus: Vencido sin Operar',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',	tooltip:	'Generar archivo CSV',
					id: 'btnGraArvoVenceSin',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params:	{claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGraArvoVenceSin
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',	tooltip:	'Descargar archivo CSV',
					id: 'btnBajarArvoVenceSin',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',	tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfVenceSin',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfVenceSin
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',	tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfVenceSin',	hidden: true
				}
			]
		}
	});

	var gridNegociable = new Ext.grid.GridPanel({
		store: consultaNegociable,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},
			{
				header: 'Num. acuse de carga', tooltip: 'N�mero acuse de carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true, width : 120,	hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',	
				dataIndex : 'FECHA_CARGA',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'CD_NOMBRE',
				sortable : true,	width : 130,	hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo para descuento en d�as', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,	width : 100, align: 'right', hidden: false,
				renderer:	Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_DESC',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true
			},
			{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == 1)	{
												value = "";
											}
											return Ext.util.Format.number(value, '$0,0.00');
										}
			},
			{
				header : 'Monto valuado en pesos', tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VAL',
				sortable : true,	width : 120,	hidden: true,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},{
				xtype:	'actioncolumn',
				header : 'Cambios del documento', tooltip: 'Cambios del documento',
				dataIndex : 'IC_DOCUMENTO',
				width:	100,	align: 'center',
				//renderer: function(value, metadata, record, rowindex, colindex, store) {
				//				return (record.get('IC_DOCUMENTO'));
				//			 },
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('IC_DOCUMENTO')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraGridCambios
					}
				]
			}
		],
		stripeRows: true,	loadMask: true,	height: 400,	width: 940, frame: true,
		title: 'Estatus: Negociable',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',	tooltip:	'Generar archivo CSV',
					id: 'btnGraArvoNego',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params:	{claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGraArvoNego
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',	tooltip:	'Descargar archivo CSV',
					id: 'btnBajarArvoNego',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',	tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfNego',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfNego
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',	tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfNego',	hidden: true
				}
			]
		}
	});

//*****************-*-*-**-*-*-**-

	var gridNegociable32 = new Ext.grid.GridPanel({
		store: consultaNegociable32,
	  hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},
			{
				header: 'Num. acuse de carga', tooltip: 'N�mero acuse de carga',
				dataIndex: 'CC_ACUSE',align: 'center',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO',align: 'center',
				sortable : true, width : 120,	hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC',
				sortable : true,	width : 100, hidden: false, align: 'center',
        renderer:function(value){
          return "N/A"
        }
			},
			{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',	
				dataIndex : 'FECHA_CARGA',align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO',align: 'center',
				sortable : true,	width : 120,	hidden: false,
        renderer:function(value){
          return "N/A"
        }
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'CD_NOMBRE',align: 'center',
				sortable : true,	width : 130,	hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'center', hidden: false,
        renderer: function(v,params,record){
          return "<div align='right'>"+Ext.util.Format.usMoney(v)+"</div>"
        }
			//	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo para descuento en d�as', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true,	width : 120,	hidden: false, align: 'center',
        renderer:function(value){
          return "N/A"
        }
			},
			{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,	width : 100, align: 'center', hidden: false,
				renderer:	Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_DESC',
				sortable : true,	width : 120,	align: 'center', hidden: false,
        renderer: function(v,params,record){
          return "<div align='right'>"+Ext.util.Format.usMoney(v)+"</div>"
        }
			//	renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',align: 'center',
				sortable : true,	width : 100,	hidden: false,
				renderer:function(value){
          return "N/A"
        }
        /*renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}*/
			},{
				header : 'N�mero de Documento Final', tooltip: 'N�mero de Documento Final',
				dataIndex : 'IC_DOCUMENTO',align: 'center',
				sortable : true,	width : 120,	hidden: false
			},{
				header : 'Monto', tooltip: 'Monto',
				//dataIndex : 'FN_MONTO',
        dataIndex : 'MONTOFINAL',align: 'center', 
				sortable : true,	width : 120,	align: 'center', hidden: false,
        renderer: function(v,params,record){
          return "<div align='right'>"+Ext.util.Format.usMoney(v)+"</div>"
        }
				//renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Plazo', tooltip: 'Plazo',
				dataIndex : 'IG_PLAZO_DESCUENTO',align: 'center',
				sortable : true,	width : 120,	hidden: false,
        renderer:function(value){
          return "N/A"
        }
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC',align: 'center',
				sortable : true,	width : 100, hidden: false,
        renderer:function(value){
          return "N/A"
        }
			},{
				header : 'Fecha Operaci�n', tooltip: 'Fecha Operaci�n',
				dataIndex : 'FECHA_EMISION',align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'IF', tooltip: 'IF',
				dataIndex : 'NOMBRE_IF',
				sortable : true,	width : 120,	hidden: false
			},{
				header : 'Nombre del Producto', tooltip: 'Nombre del Producto',
				dataIndex : 'DESC_BINS',align: 'center',
				sortable : true,	width : 120,	hidden: false,
				renderer : function (v,params,record){
					return "<div-align='left'>"+record.data.CG_CODIGO_BIN +"-"+record.data.DESC_BINS+"</div>"
				}
			},{
				header : 'Monto total', tooltip: 'Monto Total',
			//	dataIndex : 'FN_MONTO',align: 'center', 
      	dataIndex : 'MONTOFINAL',align: 'center', 
				sortable : true,	width : 120,	hidden: false,
        renderer: function(v,params,record){
          return "<div align='right'>"+Ext.util.Format.usMoney(v)+"</div>"
        }
			},{
				header : 'ID Orden Enviado', tooltip: 'ID Orden Enviado',
				dataIndex : 'ORDEN_ENVIADO',align: 'center',
				sortable : true,	width : 120,	hidden: false
			},{
				header : 'Respuesta de Operaci�n', tooltip: 'Respuesta de Operaci�n',
				dataIndex : 'ID_OPERACION',align: 'center',
				sortable : true,	width : 120,	hidden: false
			},{
				header : 'C�digo Autorizaci�n', tooltip: 'C�digo Autorizaci�n',
				dataIndex : 'CODIGO_AUTORIZA',align: 'center',
				sortable : true,	width : 120,	hidden: false
			},{
				header : 'N�mero Tarjeta de Cr�dito', tooltip: 'N�mero Tarjeta de Cr�dito',
				dataIndex : 'NUM_TARJETA',align: 'center',
				sortable : true,	width : 200, hidden: false,
				renderer: function(v,params,record){
				 return "<div align='right'>"+"XXXX-XXXX-XXXX-"+v+"</div>";
			  }
			}
		],
		stripeRows: true,	loadMask: true,	height: 400,	width: 940, frame: true,
		title: 'Estatus: Operada TC',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',	tooltip:	'Generar archivo CSV',
					id: 'btnGraArvoNego',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params:	{claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGraArvoNego
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',	tooltip:	'Descargar archivo CSV',
					id: 'btnBajarArvoNego',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',	tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfNego',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfNego
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',	tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfNego',	hidden: true
				}
			]
		}
	});
//*****************-*-*-****-*---********

	var gridSelecPyme = new Ext.grid.GridPanel({
		store: consultaSelecPyme,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},
			{
				header: 'Num. acuse de carga', tooltip: 'N�mero acuse de carga',
				dataIndex: 'CC_ACUSE', align: 'center',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO', align: 'center',
				sortable : true, width : 120,	hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',	
				dataIndex : 'FECHA_CARGA', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO', align: 'center',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'CD_NOMBRE',
				sortable : true,	width : 130,	hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			
			{
				header : 'Plazo para descuento', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true,	width : 120,	hidden: false, align: 'CENTER'
			},
			{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,	width : 100, align: 'center', hidden: false,
				renderer:	Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_DESC',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION', 
				sortable : true,	width : 120,	hidden: true
			},
			{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO', align: 'right',
				sortable : true,	width : 120,	hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == 1)	{
												value = "";
											}
											return Ext.util.Format.number(value, '$0,0.00');
										}
			},
			{
				header : 'Monto valuado', tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VAL',  align: 'right',
				sortable : true,	width : 120,	hidden: true,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},
			{
				header: 'N�mero de documento final', tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO', align: 'center', 
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',
				dataIndex : 'FN_IMPORTE_RECIBIR',  align: 'right',
				sortable : true,	width : 120,	hidden: false, align: 'right',
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo', tooltip: 'Plazo',
				dataIndex : 'IG_PLAZO_CREDITO', align: 'CENTER',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECHA_VENCRE', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'IF', tooltip: 'IF',
				dataIndex : 'NOMBREIF',	
				sortable : true,	width : 150,	hidden: false
			},
			{
				header : 'Referencia tasa de inter�s', tooltip: 'Referencia tasa de inter�s',
				dataIndex : 'REF_TASA',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Valor tasa de inter�s', tooltip: 'Valor tasa de inter�s',
				dataIndex : 'FN_VALOR_TASA',  align: 'right',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto de intereses', tooltip: 'Monto de intereses',
				dataIndex : 'FN_IMPORTE_INTERES',  align: 'right',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto total de Capital e Inter�s', tooltip: 'Monto total de Capital e Inter�s',
				//dataIndex : '',		CALCULADO
				sortable : true,	width : 120,	hidden: false,  align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){ 
											value = "";
											if (registro.get('FN_IMPORTE_RECIBIR') != "" && registro.get('FN_IMPORTE_INTERES') != "")	{
												value = (parseFloat(registro.get('FN_IMPORTE_RECIBIR')) + parseFloat(registro.get('FN_IMPORTE_INTERES')));	
											}
											return Ext.util.Format.number(value, '$0,0.00');	
										}
			}
		],
		stripeRows: true,	loadMask: true,	height: 400,	width: 940,	frame: true,
		title: 'Estatus: Seleccionada PYME',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',	tooltip:	'Generar archivo CSV',
					id: 'btnGraArvoSelec',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params:	{claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGraArvoSelec
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',	tooltip:	'Descargar archivo CSV',
					id: 'btnBajarArvoSelec',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',	tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfSelec',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfSelec
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',	tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfSelec',	hidden: true
				}
			]
		}
	});

	var gridOperada = new Ext.grid.GridPanel({
		store: consultaOperada,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},
			{
				header: 'Num. acuse de carga', tooltip: 'N�mero acuse de carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false, align: 'center'
			},
			{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true, width : 120,	hidden: false ,align: 'center'
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',	
				dataIndex : 'FECHA_CARGA', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO', align: 'center',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'CD_NOMBRE',
				sortable : true,	width : 130,	hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header : 'Plazo para descuento', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO', align: 'center',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,	width : 100, align: 'center', hidden: false,
				renderer:	Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_DESC', 
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true
			},
			{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == 1)	{
												value = "";
											}
											return Ext.util.Format.number(value, '$0,0.00');
										}
			},
			{
				header : 'Monto valuado', tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VAL', align: 'right',
				sortable : true,	width : 120,	hidden: true,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},
			{
				header: 'N�mero de documento final', tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO', align: 'center',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',
				dataIndex : 'FN_IMPORTE_RECIBIR',align: 'right',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo', tooltip: 'Plazo',
				dataIndex : 'IG_PLAZO_CREDITO', align: 'center',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECHA_VENCRE', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Operaci�n IF', tooltip: 'Fecha de Operaci�n IF',
				dataIndex : 'FECHA_OPER_IF', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'IF', tooltip: 'IF',
				dataIndex : 'NOMBREIF',	
				sortable : true,	width : 150,	hidden: false
			},
			{
				header : 'Referencia tasa de inter�s', tooltip: 'Referencia tasa de inter�s',
				dataIndex : 'REF_TASA',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Valor tasa de inter�s', tooltip: 'Valor tasa de inter�s',
				dataIndex : 'FN_VALOR_TASA', align: 'right',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto de intereses', tooltip: 'Monto de intereses',
				dataIndex : 'FN_IMPORTE_INTERES', align: 'right',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto total de Capital e Inter�s', tooltip: 'Monto total de Capital e Inter�s', align: 'right',
				//dataIndex : '',		CALCULADO
				sortable : true,	width : 120,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){ 
											value = "";
											if (registro.get('FN_IMPORTE_RECIBIR') != "" && registro.get('FN_IMPORTE_INTERES') != "")	{
												value = (parseFloat(registro.get('FN_IMPORTE_RECIBIR')) + parseFloat(registro.get('FN_IMPORTE_INTERES')));	
											}
											return Ext.util.Format.number(value, '$0,0.00');	
										}
			}
		],
		stripeRows: true,	loadMask: true,	height: 400,	width: 940,	frame: true,
		title: 'Estatus: Operada',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',	tooltip:	'Generar archivo CSV',
					id: 'btnGraArvoOpera',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGraArvoOpera
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',	tooltip:	'Descargar archivo CSV',
					id: 'btnBajarArvoOpera',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',	tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfOpera',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfOpera
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',	tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfOpera',	hidden: true
				}
			]
		}
	});

	var gridOperaPaga = new Ext.grid.GridPanel({
		store: consultaOperaPaga,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},
			{
				header: 'Num. acuse de carga', tooltip: 'N�mero acuse de carga',
				dataIndex: 'CC_ACUSE', align: 'center',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO', align: 'center',
				sortable : true, width : 120,	hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',	
				dataIndex : 'FECHA_CARGA', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO', align: 'center',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'CD_NOMBRE',
				sortable : true,	width : 130,	hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},				
			{
				header : 'Plazo para descuento', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO', align: 'center',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO', align: 'center',
				sortable : true,	width : 100, hidden: false,
				renderer:	Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_DESC',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true
			},
			{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == 1)	{
												value = "";
											}
											return Ext.util.Format.number(value, '$0,0.00');
										}
			},
			{
				header : 'Monto valuado', tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VAL', align: 'right',
				sortable : true,	width : 120,	hidden: true,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},
			{
				header: 'N�mero de documento final', tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO', align: 'center',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',
				dataIndex : 'FN_IMPORTE_RECIBIR', align: 'right',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo', tooltip: 'Plazo',
				dataIndex : 'IG_PLAZO_CREDITO', align: 'center',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECHA_VENCRE', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Operaci�n IF', tooltip: 'Fecha de Operaci�n IF',
				dataIndex : 'FECHA_OPER_IF', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'IF', tooltip: 'IF',
				dataIndex : 'NOMBREIF',	
				sortable : true,	width : 150,	hidden: false
			},
			{
				header : 'Referencia tasa de inter�s', tooltip: 'Referencia tasa de inter�s',
				dataIndex : 'REF_TASA',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Valor tasa de inter�s', tooltip: 'Valor tasa de inter�s',
				dataIndex : 'FN_VALOR_TASA',align: 'right',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto de intereses', tooltip: 'Monto de intereses',
				dataIndex : 'FN_IMPORTE_INTERES', align: 'right',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto total de Capital e Inter�s', tooltip: 'Monto total de Capital e Inter�s',
				//dataIndex : '',		CALCULADO
				sortable : true,	width : 120,	hidden: false, align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){ 
											value = "";
											if (registro.get('FN_IMPORTE_RECIBIR') != "" && registro.get('FN_IMPORTE_INTERES') != "")	{
												value = (parseFloat(registro.get('FN_IMPORTE_RECIBIR')) + parseFloat(registro.get('FN_IMPORTE_INTERES')));	
											}
											return Ext.util.Format.number(value, '$0,0.00');	
										}
			},
			{
				header : 'Fecha de Cambio de Estatus', tooltip: 'Fecha de Cambio de Estatus',
				dataIndex : 'FECHA_CAMBIO', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Cambios del documento', tooltip: 'Cambios del documento',
				dataIndex : 'ESTATUS',		//IC_DOCUMENTO
				sortable : true,	width : 150,	hidden: false
			}
		],
		stripeRows: true,	loadMask: true,	height: 400,	width: 940,	frame: true,
		title: 'Estatus: Operada Pagada',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',	tooltip:	'Generar archivo CSV',
					id: 'btnGraArvoOperaPaga',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGraArvoOperaPaga
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',	tooltip:	'Descargar archivo CSV',
					id: 'btnBajarArvoOperaPaga',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',	tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfOperaPaga',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfOperaPaga
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',	tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfOperaPaga',	hidden: true
				}
			]
		}
	});

	var gridOperaVence = new Ext.grid.GridPanel({
		store: consultaOperaVence,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},
			{
				header: 'Num. acuse de carga', tooltip: 'N�mero acuse de carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true, width : 120,	hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',	
				dataIndex : 'FECHA_CARGA',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'CD_NOMBRE',
				sortable : true,	width : 130,	hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo para descuento', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,	width : 100, align: 'right', hidden: false,
				renderer:	Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_DESC',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true
			},
			{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == 1)	{
												value = "";
											}
											return Ext.util.Format.number(value, '$0,0.00');
										}
			},
			{
				header : 'Monto valuado', tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VAL',
				sortable : true,	width : 120,	hidden: true,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},
			{
				header: 'N�mero de documento final', tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',
				dataIndex : 'FN_IMPORTE_RECIBIR',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo', tooltip: 'Plazo',
				dataIndex : 'IG_PLAZO_CREDITO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECHA_VENCRE',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Operaci�n IF', tooltip: 'Fecha de Operaci�n IF',
				dataIndex : 'FECHA_OPER_IF',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'IF', tooltip: 'IF',
				dataIndex : 'NOMBREIF',	
				sortable : true,	width : 150,	hidden: false
			},
			{
				header : 'Referencia tasa de inter�s', tooltip: 'Referencia tasa de inter�s',
				dataIndex : 'REF_TASA',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Valor tasa de inter�s', tooltip: 'Valor tasa de inter�s',
				dataIndex : 'FN_VALOR_TASA',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto de intereses', tooltip: 'Monto de intereses',
				dataIndex : 'FN_IMPORTE_INTERES',
				sortable : true,	width : 120,	hidden: false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto total de Capital e Inter�s', tooltip: 'Monto total de Capital e Inter�s',
				//dataIndex : '',		CALCULADO
				sortable : true,	width : 120,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){ 
											value = "";
											if (registro.get('FN_IMPORTE_RECIBIR') != "" && registro.get('FN_IMPORTE_INTERES') != "")	{
												value = (parseFloat(registro.get('FN_IMPORTE_RECIBIR')) + parseFloat(registro.get('FN_IMPORTE_INTERES')));	
											}
											return Ext.util.Format.number(value, '$0,0.00');	
										}
			},
			{
				header : 'Fecha de Cambio de Estatus', tooltip: 'Fecha de Cambio de Estatus',
				dataIndex : 'FECHA_CAMBIO',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Cambios del documento', tooltip: 'Cambios del documento',
				dataIndex : 'ESTATUS',		//IC_DOCUMENTO
				sortable : true,	width : 150,	hidden: false
			}
		],
		stripeRows: true,	loadMask: true,	height: 400,	width: 940,	frame: true,
		title: 'Estatus: Pendiente de pago IF',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',	tooltip:	'Generar archivo CSV',
					id: 'btnGraArvoOperaVence',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGraArvoOperaVence
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',	tooltip:	'Descargar archivo CSV',
					id: 'btnBajarArvoOperaVence',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',	tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfOperaVence',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfOperaVence
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',	tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfOperaVence',	hidden: true
				}
			]
		}
	});


	var gridBaja = new Ext.grid.GridPanel({
		store: consultaBaja,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},
			{
				header: 'Num. acuse de carga', tooltip: 'N�mero acuse de carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true, width : 120,	hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',	
				dataIndex : 'FECHA_CARGA',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'CD_NOMBRE',
				sortable : true,	width : 130,	hidden: false
			},
			{
				header : 'Monto a descontar', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo para descuento en d�as', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,	width : 100, align: 'right', hidden: false,
				renderer:	Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_DESC',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true
			},
			{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == 1)	{
												value = "";
											}
											return Ext.util.Format.number(value, '$0,0.00');
										}
			},
			{
				header : 'Monto valuado en pesos', tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VAL',
				sortable : true,	width : 120,	hidden: true,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},{
				xtype:	'actioncolumn',
				header : 'Cambios del documento', tooltip: 'Cambios del documento',
				dataIndex : 'IC_DOCUMENTO',
				width:	100,	align: 'center',
				//renderer: function(value, metadata, record, rowindex, colindex, store) {
				//				return (record.get('IC_DOCUMENTO'));
				//			 },
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('IC_DOCUMENTO')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraGridCambios
					}
				]
			}
		],
		stripeRows: true,	loadMask: true,	height: 400,	width: 940,	frame: true,
		title: 'Estatus: Baja',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',	tooltip:	'Generar archivo CSV',
					id: 'btnGraArvoBaja',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGraArvoBaja
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',	tooltip:	'Descargar archivo CSV',
					id: 'btnBajarArvoBaja',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfBaja',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfBaja
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',	tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfBaja',	hidden: true
				}
			]
		}
	});

	var gridNoNego = new Ext.grid.GridPanel({
		store: consultaNoNego,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},
			{
				header: 'Num. acuse de carga', tooltip: 'N�mero acuse de carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true, width : 120,	hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//4
				dataIndex : 'FECHA_VENC',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',	
				dataIndex : 'FECHA_CARGA',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'CD_NOMBRE',
				sortable : true,	width : 130,	hidden: false
			},
			{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo para descuento en d�as', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true,	width : 120,	hidden: false
			},
			{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,	width : 100, align: 'right', hidden: false,
				renderer:	Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_DESC',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true
			},
			{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if(registro.get('IC_MONEDA') == 1)	{
												value = "";
											}
											return Ext.util.Format.number(value, '$0,0.00');
										}
			},
			{
				header : 'Monto valuado en pesos', tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VAL',
				sortable : true,	width : 120,	hidden: true,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},{
				xtype:	'actioncolumn',
				header : 'Cambios del documento', tooltip: 'Cambios del documento',
				dataIndex : 'IC_DOCUMENTO',
				width:	100,	align: 'center',
				//renderer: function(value, metadata, record, rowindex, colindex, store) {
				//				return (record.get('IC_DOCUMENTO'));
				//			 },
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('IC_DOCUMENTO')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraGridCambios
					}
				]
			}
		],
		stripeRows: true,	loadMask: true,	height: 400,	width: 940,	frame: true,
		title: 'Estatus: No Negociable',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',	tooltip:	'Generar archivo CSV',
					id: 'btnGraArvoNoNego',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01exta.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGraArvoNoNego
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',	tooltip:	'Descargar archivo CSV',
					id: 'btnBajarArvoNoNego',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfNoNego',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfNoNego
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfNoNego',	hidden: true
				}
			]
		}
	});
// Fin de declaraci�n de Grid�s

	var gridCambiosDoctos = {
		xtype: 'grid',
		store: cambioDoctosData,
		id: 'gridCambios',
		columns: [
			{
				header: 'Fecha del Cambio',
				dataIndex: 'FECH_CAMBIO',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Cambio Estatus',
				dataIndex: 'CD_DESCRIPCION',		//CT_CAMBIO_MOTIVO
				align: 'left',	width: 150
			},{
				header: 'Fecha Emision Anterior',
				dataIndex: 'FECH_EMI_ANT',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Fecha Emision Nueva',
				dataIndex: 'FECH_EMI_NEW',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Monto Anterior',
				dataIndex: 'FN_MONTO_ANTERIOR',
				align: 'right',	width: 120,	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto Nuevo',
				dataIndex: 'FN_MONTO_NUEVO',
				align: 'right',	width: 120,	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Fecha Vencimiento Anterior',
				dataIndex: 'FECH_VENC_ANT',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Fecha Vencimiento Nuevo',
				dataIndex: 'FECH_VENC_NEW',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Modalidad de Plazo Anterior',
				dataIndex: 'MODO_PLAZO_ANTERIOR',
				align: 'left',	width: 150
			},{
				header: 'Modalidad de Plazo Nuevo',
				dataIndex: 'MODO_PLAZO_NUEVO',
				align: 'left',	width: 150
			}
		],
		height: 300,
		title: '',
		frame: false,
		loadMask: true
	};


	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'status',	
			id: 'cmbStatus',	
			fieldLabel: 'Estatus', 
			mode: 'local',	
			hiddenName : 'status',	
			emptyText: 'Seleccione Estatus . . . ',
			forceSelection : true,	triggerAction : 'all',	typeAhead: true,
			minChars : 1,	
			store : catalogoEstatus,
			displayField : 'descripcion',	valueField : 'clave',
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var ventana = Ext.getCmp('cambioDoctos');
						if (ventana) {
							ventana.destroy();
						}
						cveEstatus = combo.getValue();
						gridVenceSinOpera.hide();
						gridNegociable.hide();
            gridNegociable32.hide();
						gridSelecPyme.hide();
						gridOperada.hide();
						gridOperaPaga.hide();
						gridOperaVence.hide();
						gridBaja.hide();
						gridNoNego.hide();
						//Nuevo grid
						gridProcesoAutorizacion.hide();
						fp.el.mask('Enviando...', 'x-mask-loading');
						if (combo.value == "9")	{
							//gridVenceSinOpera.setTitle('<div><div style="float:left">Estatus: Vencido sin Operar</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaVenceSinOpera.load();
						}
						if (combo.value == "2")	{
							//gridNegociable.setTitle('<div><div style="float:left">Estatus: Proceso Negociable</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');						
							consultaNegociable.load();
						}
            if (combo.value == "32")	{
							//gridNegociable.setTitle('<div><div style="float:left">Estatus: Proceso Negociable</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');						
							consultaNegociable32.load();
						}
						if (combo.value == "3")	{
							//gridSelecPyme.setTitle('<div><div style="float:left">Estatus: Seleccionada PYME</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaSelecPyme.load();
						}
						if (combo.value == "4")	{
							//gridOperada.setTitle('<div><div style="float:left">Estatus: Operada</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaOperada.load();
						}
						if (combo.value == "11")	{
							//gridOperaPaga.setTitle('<div><div style="float:left">Estatus: Operada Pagada</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaOperaPaga.load();
						}
						if (combo.value == "22")	{
							//gridOperaVence.setTitle('<div><div style="float:left">Estatus: Operado Vencido</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							gridOperaVence.setTitle('Estatus: '+combo.getRawValue());
							consultaOperaVence.load();
						}
						if (combo.value == "5")	{
							//gridBaja.setTitle('<div><div style="float:left">Estatus: Baja</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaBaja.load();
						}
						if (combo.value == "1")	{
							//gridNoNego.setTitle('<div><div style="float:left">Estatus: No Negociable</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaNoNego.load();
						}
						if(combo.value == "24"){
							//Ext.Msg.alert('Funcion',combo.getRawValue());
							consultaProcesoAutorizacion.load();
							//gridProcesoAutorizacion.setTitle('Estatus: '+combo.getRawValue());
						}
					}
				}
			}
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: ' margin:0 auto;',
		frame: true,
		border: false,
		title: '<div><center>Documentos por Estatus</div>',
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		hidden: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:	elementosForma,
		monitorValid: true,
		hidden: true
	});

//Simulacion en un contenedor
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(20),
			gridVenceSinOpera,
			gridNegociable,
			gridSelecPyme,
			gridOperada,
			gridOperaPaga,
			gridOperaVence,
			gridBaja,
			gridNoNego,
			gridProcesoAutorizacion,
      NE.util.getEspaciador(20),
      gridNegociable32
		]
	});
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '24reporte01ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});

catalogoEstatus.load();
});//Fin de funcion principal