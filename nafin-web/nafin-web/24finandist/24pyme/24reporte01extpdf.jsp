<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,java.math.*,
	com.netro.distribuidores.*,com.netro.exception.*, javax.naming.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

try {
	if(iNoCliente != null && !iNoCliente.equals("")) {
		String informacion					=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
		String cveEstatus					=	(request.getParameter("claveStatus")==null)?"":request.getParameter("claveStatus");
		String tiposCredito					=	(request.getParameter("tiposCredito")==null)?"":request.getParameter("tiposCredito");
		String cgTipoConversion	=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
		String tipoCredito			=	(request.getParameter("tiposCredito")==null)?"":request.getParameter("tiposCredito");
		
		CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

		if("".equals(cgTipoConversion)){
			cgTipoConversion = cargaDocto.getTipoConversion(iNoCliente);
		}
		
		if("".equals(tiposCredito)){
			 tipoCredito = cargaDocto.getTiposCredito(iNoCliente, tiposCredito);
		}
		String rsEpo = "",rsNumDoc = "",rsAcuse = "",rsdfEmision = "",rsdfVenc = "",rsdfCarga = "";
		String rsPlazoD = "",rsMoneda = "",rsMontoDoc = "",rsTipConv = "";
		String rsTipCam = "",rsMontoVal = "",rsPlazDesc = "",rsPorcDesc = "",rsMontoDesc	= "", rsMontoFinal="";
		String rsModoPlaz = "",rsEstatus = "",rsic_mon = "",rsic_doc = "";
		String rsNumCre ="",rsIf="",rsTipoLc="",rsdfOperacion="",rsMontoC="",rsPlazoC="";
		String rsdfVencC="",rsTasaInt="",rsValorInt="",rsMontoT="",rsTipoCob="";
		String 	rsFechaOperIF="",rsFechaCambio="", bandeVentaCartera ="";
		double rsMontoCapInt = 0;
		
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);

		if (cveEstatus.equals("9")){																																																											//-----[gridVenceSinOpera]
			int rowVenceSin = 0;
			RepEstatusPyme repVence = new RepEstatusPyme();
			repVence.setProducto(4);
			repVence.setIcePyme(iNoCliente);
			repVence.setCveEstatus("9");
			repVence.setQrysentencia();
			Registros regVenceSin = repVence.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
			pdfDoc.addText("Estatus:		Vencido sin Operar","formasB",ComunesPDF.LEFT);
			while (regVenceSin.next()) {
				if(rowVenceSin == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("EPO");
					lEncabezados.add("Num. acuse de carga");
					lEncabezados.add("Número de documento inicial");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Fecha de publicación");
					lEncabezados.add("Plazo docto.");
					lEncabezados.add("Moneda");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo para descuento en días");
					lEncabezados.add("% de descuento");
					lEncabezados.add("Monto % de descuento");
					if(!"".equals(cgTipoConversion)){
						lEncabezados.add("Tipo conv.");
						lEncabezados.add("Tipo cambio");
						lEncabezados.add("Monto valuado en pesos");
					}
					lEncabezados.add("Modalidad de plazo");
					//lEncabezados.add("Cambios del documento");

					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				rsEpo				=	regVenceSin.getString("NOMBREEPO")==null?"":regVenceSin.getString("NOMBREEPO");
				rsNumDoc		= regVenceSin.getString("IG_NUMERO_DOCTO")==null?"":regVenceSin.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regVenceSin.getString("CC_ACUSE")==null?"":regVenceSin.getString("CC_ACUSE");
				rsdfEmision	= regVenceSin.getString("FECHA_EMISION")==null?"":regVenceSin.getString("FECHA_EMISION");
				rsdfVenc		= regVenceSin.getString("FECHA_VENC")==null?"":regVenceSin.getString("FECHA_VENC");
				rsdfCarga		= regVenceSin.getString("FECHA_CARGA")==null?"":regVenceSin.getString("FECHA_CARGA");
				rsPlazoD			= regVenceSin.getString("IG_PLAZO_DOCTO")==null?"":regVenceSin.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regVenceSin.getString("CD_NOMBRE")==null?"":regVenceSin.getString("CD_NOMBRE");
				rsMontoDoc	= regVenceSin.getString("FN_MONTO")==null?"":regVenceSin.getString("FN_MONTO");
				rsTipConv		= regVenceSin.getString("TIPO_CONVERSION")==null?"":regVenceSin.getString("TIPO_CONVERSION");
				rsTipCam		= regVenceSin.getString("TIPO_CAMBIO")==null?"":regVenceSin.getString("TIPO_CAMBIO");
				rsMontoVal	= regVenceSin.getString("MONTO_VAL")==null?"":regVenceSin.getString("MONTO_VAL");
				rsPlazDesc	= regVenceSin.getString("IG_PLAZO_DESCUENTO")==null?"":regVenceSin.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regVenceSin.getString("FN_PORC_DESCUENTO")==null?"":regVenceSin.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regVenceSin.getString("MONTO_DESC")==null?"":regVenceSin.getString("MONTO_DESC");
				rsModoPlaz	= regVenceSin.getString("MODO_PLAZO")==null?"":regVenceSin.getString("MODO_PLAZO");
				rsEstatus		= regVenceSin.getString("ESTATUS")==null?"":regVenceSin.getString("ESTATUS");
				rsic_mon			= regVenceSin.getString("IC_MONEDA")==null?"":regVenceSin.getString("IC_MONEDA");
				rsic_doc			= regVenceSin.getString("IC_DOCUMENTO")==null?"":regVenceSin.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regVenceSin.getString("CG_VENTACARTERA")==null)?"":regVenceSin.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}				
				if ("1".equals(rsic_mon)) {
					rsTipCam="";
				}
				else { //IC_MONEDA=54 
					rsTipCam = ""+Comunes.formatoDecimal(rsTipCam,2);
				}
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(rsEpo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsAcuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumDoc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfEmision, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVenc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfCarga, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoD, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsMoneda, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDoc,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazDesc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(rsPorcDesc,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDesc,2), "formasmen", ComunesPDF.CENTER);
				if(!"".equals(cgTipoConversion)){
					pdfDoc.setCell(rsTipConv, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(rsTipCam, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoVal,2), "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell(rsModoPlaz, "formasmen", ComunesPDF.CENTER);
				//pdfDoc.setCell(rsic_doc, "formasmen", ComunesPDF.CENTER);	//revisar esta linea
				rowVenceSin++;
			} //fin del while
			if (rowVenceSin == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("2") || cveEstatus.equals("32")){																																																									 //-----[gridNegociable]
			int rowNego = 0;
			RepEstatusPyme repNego = new RepEstatusPyme();
			repNego.setProducto(4);
			repNego.setIcePyme(iNoCliente); if (cveEstatus.equals("32"))repNego.setCveEstatus("32");else
			repNego.setCveEstatus("2");
			repNego.setQrysentencia();
			Registros regNego = repNego.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/ if (cveEstatus.equals("32"))pdfDoc.addText("Estatus:		Operada TC","formasB",ComunesPDF.LEFT);else
			pdfDoc.addText("Estatus:		Negociable","formasB",ComunesPDF.LEFT);
			while (regNego.next()) {
				if(rowNego == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("EPO");
					lEncabezados.add("Num. acuse de carga");
					lEncabezados.add("Número de documento inicial");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Fecha de publicación");
					lEncabezados.add("Plazo docto.");
					lEncabezados.add("Moneda");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo para descuento en días");
					lEncabezados.add("% de descuento");
					lEncabezados.add("Monto % de descuento"); 
          if (cveEstatus.equals("32")){
            lEncabezados.add("Modalidad de plazo"); 
            lEncabezados.add("Número de Documento");
            
            lEncabezados.add("Monto");
            lEncabezados.add("Plazo");
            lEncabezados.add("Fecha de Vencimiento");
            lEncabezados.add("Fecha de Operación");
            lEncabezados.add("IF");
            lEncabezados.add("Nombre del Producto");
            lEncabezados.add("Monto total");
            lEncabezados.add("ID Orden enviado");
            lEncabezados.add("Respuesta de Operación");
            lEncabezados.add("Código Autorizacion");
            lEncabezados.add("Número Tarjeta de Crédito");
            
          }else{
            if(!"".equals(cgTipoConversion)){
              lEncabezados.add("Tipo conv.");
              lEncabezados.add("Tipo cambio");
              lEncabezados.add("Monto valuado en pesos");
            } 
            lEncabezados.add("Modalidad de plazo");
          }
					
					//lEncabezados.add("Cambios del documento");

					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				rsEpo				=	regNego.getString("NOMBREEPO")==null?"":regNego.getString("NOMBREEPO");
				rsNumDoc		= regNego.getString("IG_NUMERO_DOCTO")==null?"":regNego.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regNego.getString("CC_ACUSE")==null?"":regNego.getString("CC_ACUSE");
				rsdfEmision	= regNego.getString("FECHA_EMISION")==null?"":regNego.getString("FECHA_EMISION");
				rsdfVenc		= regNego.getString("FECHA_VENC")==null?"":regNego.getString("FECHA_VENC");
				rsdfCarga		= regNego.getString("FECHA_CARGA")==null?"":regNego.getString("FECHA_CARGA");
				rsPlazoD			= regNego.getString("IG_PLAZO_DOCTO")==null?"":regNego.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regNego.getString("CD_NOMBRE")==null?"":regNego.getString("CD_NOMBRE");
				rsMontoDoc	= regNego.getString("FN_MONTO")==null?"":regNego.getString("FN_MONTO"); 
        
        String numDoc = "",nomIf="",codBin ="", descBins="",ordEn="",idOp="",codAu="",tarjeta="" ;
        if (cveEstatus.equals("32")){
          numDoc	= regNego.getString("IC_DOCUMENTO")==null?"":regNego.getString("IC_DOCUMENTO");  
          nomIf   = regNego.getString("NOMBRE_IF")==null?"":regNego.getString("NOMBRE_IF");  
          codBin  = regNego.getString("CG_CODIGO_BIN")==null?"":regNego.getString("CG_CODIGO_BIN");  
			 descBins= regNego.getString("DESC_BINS")==null?"":regNego.getString("DESC_BINS");  
          ordEn	= regNego.getString("ORDEN_ENVIADO")==null?"":regNego.getString("ORDEN_ENVIADO");  
          idOp   	= regNego.getString("ID_OPERACION")==null?"":regNego.getString("ID_OPERACION");  
          codAu	= regNego.getString("CODIGO_AUTORIZA")==null?"":regNego.getString("CODIGO_AUTORIZA");  
          tarjeta = regNego.getString("NUM_TARJETA")==null?"":regNego.getString("NUM_TARJETA");  
          
        }else{
				rsTipConv		= regNego.getString("TIPO_CONVERSION")==null?"":regNego.getString("TIPO_CONVERSION");
				rsTipCam		= regNego.getString("TIPO_CAMBIO")==null?"":regNego.getString("TIPO_CAMBIO");
				rsMontoVal	= regNego.getString("MONTO_VAL")==null?"":regNego.getString("MONTO_VAL"); 
        }
				rsPlazDesc	= regNego.getString("IG_PLAZO_DESCUENTO")==null?"":regNego.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regNego.getString("FN_PORC_DESCUENTO")==null?"":regNego.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regNego.getString("MONTO_DESC")==null?"":regNego.getString("MONTO_DESC");
				rsModoPlaz	= regNego.getString("MODO_PLAZO")==null?"":regNego.getString("MODO_PLAZO");
				rsEstatus		= regNego.getString("ESTATUS")==null?"":regNego.getString("ESTATUS");
				rsic_mon			= regNego.getString("IC_MONEDA")==null?"":regNego.getString("IC_MONEDA");
				rsic_doc			= regNego.getString("IC_DOCUMENTO")==null?"":regNego.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regNego.getString("CG_VENTACARTERA")==null)?"":regNego.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}		if (cveEstatus.equals("32")){
          rsMontoFinal= regNego.getString("MONTOFINAL")==null?"":regNego.getString("MONTOFINAL");
        }else{		
          if ("1".equals(rsic_mon)) {
					rsTipCam="";
				}
				else { //IC_MONEDA=54 
					rsTipCam = ""+Comunes.formatoDecimal(rsTipCam,2);
				} }
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(rsEpo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsAcuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumDoc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfEmision, "formasmen", ComunesPDF.CENTER);
        
        if (cveEstatus.equals("32")){ 
          pdfDoc.setCell("N/A", "formasmen", ComunesPDF.CENTER);
        }else{
          pdfDoc.setCell(rsdfVenc, "formasmen", ComunesPDF.CENTER);
        }
				
				pdfDoc.setCell(rsdfCarga, "formasmen", ComunesPDF.CENTER);
        
        if (cveEstatus.equals("32")){ 
          pdfDoc.setCell("N/A", "formasmen", ComunesPDF.CENTER);
        }else{
          pdfDoc.setCell(rsPlazoD, "formasmen", ComunesPDF.CENTER);
        }
				
        
				pdfDoc.setCell(rsMoneda, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(rsMontoDoc,2), "formasmen", ComunesPDF.CENTER); 
       
        if (cveEstatus.equals("32")){ 
          pdfDoc.setCell("N/A", "formasmen", ComunesPDF.CENTER);
        }else{
          pdfDoc.setCell(rsPlazDesc, "formasmen", ComunesPDF.CENTER);
        }
				pdfDoc.setCell(rsPorcDesc+"%", "formasmen", ComunesPDF.CENTER);  
				pdfDoc.setCell("$"+Comunes.formatoDecimal(rsMontoDesc,2), "formasmen", ComunesPDF.CENTER);
				//pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDesc,2), "formasmen", ComunesPDF.CENTER); 
        if (cveEstatus.equals("32")){ }else{
          if(!"".equals(cgTipoConversion)){
            pdfDoc.setCell(rsTipConv, "formasmen", ComunesPDF.CENTER);
            pdfDoc.setCell(rsTipCam, "formasmen", ComunesPDF.CENTER);
            pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoVal,2), "formasmen", ComunesPDF.CENTER);
          } 
        }
				
				//pdfDoc.setCell(rsic_doc, "formasmen", ComunesPDF.CENTER);	//revisar esta linea
        if (cveEstatus.equals("32")){ 
          pdfDoc.setCell("N/A", "formasmen", ComunesPDF.CENTER);
          pdfDoc.setCell(numDoc, "formasmen", ComunesPDF.CENTER);          
          pdfDoc.setCell("$"+rsMontoFinal, "formasmen", ComunesPDF.CENTER);
          pdfDoc.setCell("N/A", "formasmen", ComunesPDF.CENTER);
          pdfDoc.setCell("N/A", "formasmen", ComunesPDF.CENTER);
          pdfDoc.setCell(rsdfEmision, "formasmen", ComunesPDF.CENTER);
          pdfDoc.setCell(nomIf, "formasmen", ComunesPDF.CENTER);
          pdfDoc.setCell(codBin+"-"+descBins, "formasmen", ComunesPDF.CENTER);
          pdfDoc.setCell("$"+rsMontoFinal, "formasmen", ComunesPDF.CENTER);
          
          pdfDoc.setCell(ordEn, "formasmen", ComunesPDF.CENTER);
          pdfDoc.setCell(idOp, "formasmen", ComunesPDF.CENTER);
          pdfDoc.setCell(codAu, "formasmen", ComunesPDF.CENTER);
          pdfDoc.setCell("XXXX-XXXX-XXXX-"+tarjeta, "formasmen", ComunesPDF.CENTER);
          
        } else 
          pdfDoc.setCell(rsModoPlaz, "formasmen", ComunesPDF.CENTER);
        
        
				rowNego++;
			} //fin del while
			if (rowNego == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("5")){																																																												 	//-----[gridBaja]
			int rowBaja = 0;
			RepEstatusPyme repBaja = new RepEstatusPyme();
			repBaja.setProducto(4);
			repBaja.setIcePyme(iNoCliente);
			repBaja.setCveEstatus("5");
			repBaja.setQrysentencia();
			Registros regBaja = repBaja.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
			pdfDoc.addText("Estatus:		Baja","formasB",ComunesPDF.LEFT);
			while (regBaja.next()) {
				if(rowBaja == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("EPO");
					lEncabezados.add("Num. acuse de carga");
					lEncabezados.add("Número de documento inicial");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Fecha de publicación");
					lEncabezados.add("Plazo docto.");
					lEncabezados.add("Moneda");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo para descuento en días");
					lEncabezados.add("% de descuento");
					lEncabezados.add("Monto % de descuento");
					if(!"".equals(cgTipoConversion)){
						lEncabezados.add("Tipo conv.");
						lEncabezados.add("Tipo cambio");
						lEncabezados.add("Monto valuado en pesos");
					}
					lEncabezados.add("Modalidad de plazo");
					//lEncabezados.add("Cambios del documento");

					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				rsEpo				=	regBaja.getString("NOMBREEPO")==null?"":regBaja.getString("NOMBREEPO");
				rsNumDoc		= regBaja.getString("IG_NUMERO_DOCTO")==null?"":regBaja.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regBaja.getString("CC_ACUSE")==null?"":regBaja.getString("CC_ACUSE");
				rsdfEmision	= regBaja.getString("FECHA_EMISION")==null?"":regBaja.getString("FECHA_EMISION");
				rsdfVenc		= regBaja.getString("FECHA_VENC")==null?"":regBaja.getString("FECHA_VENC");
				rsdfCarga		= regBaja.getString("FECHA_CARGA")==null?"":regBaja.getString("FECHA_CARGA");
				rsPlazoD			= regBaja.getString("IG_PLAZO_DOCTO")==null?"":regBaja.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regBaja.getString("CD_NOMBRE")==null?"":regBaja.getString("CD_NOMBRE");
				rsMontoDoc	= regBaja.getString("FN_MONTO")==null?"":regBaja.getString("FN_MONTO");
				rsTipConv		= regBaja.getString("TIPO_CONVERSION")==null?"":regBaja.getString("TIPO_CONVERSION");
				rsTipCam		= regBaja.getString("TIPO_CAMBIO")==null?"":regBaja.getString("TIPO_CAMBIO");
				rsMontoVal	= regBaja.getString("MONTO_VAL")==null?"":regBaja.getString("MONTO_VAL");
				rsPlazDesc	= regBaja.getString("IG_PLAZO_DESCUENTO")==null?"":regBaja.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regBaja.getString("FN_PORC_DESCUENTO")==null?"":regBaja.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regBaja.getString("MONTO_DESC")==null?"":regBaja.getString("MONTO_DESC");
				rsModoPlaz	= regBaja.getString("MODO_PLAZO")==null?"":regBaja.getString("MODO_PLAZO");
				rsEstatus		= regBaja.getString("ESTATUS")==null?"":regBaja.getString("ESTATUS");
				rsic_mon			= regBaja.getString("IC_MONEDA")==null?"":regBaja.getString("IC_MONEDA");
				rsic_doc			= regBaja.getString("IC_DOCUMENTO")==null?"":regBaja.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regBaja.getString("CG_VENTACARTERA")==null)?"":regBaja.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}				
				if ("1".equals(rsic_mon)) {
					rsTipCam="";
				}
				else { //IC_MONEDA=54 
					rsTipCam = ""+Comunes.formatoDecimal(rsTipCam,2);
				}
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(rsEpo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsAcuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumDoc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfEmision, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVenc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfCarga, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoD, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsMoneda, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDoc,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazDesc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(rsPorcDesc,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDesc,2), "formasmen", ComunesPDF.CENTER);
				if(!"".equals(cgTipoConversion)){
					pdfDoc.setCell(rsTipConv, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(rsTipCam, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoVal,2), "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell(rsModoPlaz, "formasmen", ComunesPDF.CENTER);
				//pdfDoc.setCell(rsic_doc, "formasmen", ComunesPDF.CENTER);	//revisar esta linea
				rowBaja++;
			} //fin del while
			if (rowBaja == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("1")){																																																								 //-----[gridNoNegociable]
			int rowNoNego = 0;
			RepEstatusPyme repNoNego = new RepEstatusPyme();
			repNoNego.setProducto(4);
			repNoNego.setIcePyme(iNoCliente);
			repNoNego.setCveEstatus("1");
			repNoNego.setQrysentencia();
			Registros regNoNego = repNoNego.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
			pdfDoc.addText("Estatus:		No Negociable","formasB",ComunesPDF.LEFT);
			while (regNoNego.next()) {
				if(rowNoNego == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("EPO");
					lEncabezados.add("Num. acuse de carga");
					lEncabezados.add("Número de documento inicial");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Fecha de publicación");
					lEncabezados.add("Plazo docto.");
					lEncabezados.add("Moneda");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo para descuento en días");
					lEncabezados.add("% de descuento");
					lEncabezados.add("Monto % de descuento");
					if(!"".equals(cgTipoConversion)){
						lEncabezados.add("Tipo conv.");
						lEncabezados.add("Tipo cambio");
						lEncabezados.add("Monto valuado en pesos");
					}
					lEncabezados.add("Modalidad de plazo");
					//lEncabezados.add("Cambios del documento");

					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				rsEpo				=	regNoNego.getString("NOMBREEPO")==null?"":regNoNego.getString("NOMBREEPO");
				rsNumDoc		= regNoNego.getString("IG_NUMERO_DOCTO")==null?"":regNoNego.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regNoNego.getString("CC_ACUSE")==null?"":regNoNego.getString("CC_ACUSE");
				rsdfEmision	= regNoNego.getString("FECHA_EMISION")==null?"":regNoNego.getString("FECHA_EMISION");
				rsdfVenc		= regNoNego.getString("FECHA_VENC")==null?"":regNoNego.getString("FECHA_VENC");
				rsdfCarga		= regNoNego.getString("FECHA_CARGA")==null?"":regNoNego.getString("FECHA_CARGA");
				rsPlazoD			= regNoNego.getString("IG_PLAZO_DOCTO")==null?"":regNoNego.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regNoNego.getString("CD_NOMBRE")==null?"":regNoNego.getString("CD_NOMBRE");
				rsMontoDoc	= regNoNego.getString("FN_MONTO")==null?"":regNoNego.getString("FN_MONTO");
				rsTipConv		= regNoNego.getString("TIPO_CONVERSION")==null?"":regNoNego.getString("TIPO_CONVERSION");
				rsTipCam		= regNoNego.getString("TIPO_CAMBIO")==null?"":regNoNego.getString("TIPO_CAMBIO");
				rsMontoVal	= regNoNego.getString("MONTO_VAL")==null?"":regNoNego.getString("MONTO_VAL");
				rsPlazDesc	= regNoNego.getString("IG_PLAZO_DESCUENTO")==null?"":regNoNego.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regNoNego.getString("FN_PORC_DESCUENTO")==null?"":regNoNego.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regNoNego.getString("MONTO_DESC")==null?"":regNoNego.getString("MONTO_DESC");
				rsModoPlaz	= regNoNego.getString("MODO_PLAZO")==null?"":regNoNego.getString("MODO_PLAZO");
				rsEstatus		= regNoNego.getString("ESTATUS")==null?"":regNoNego.getString("ESTATUS");
				rsic_mon			= regNoNego.getString("IC_MONEDA")==null?"":regNoNego.getString("IC_MONEDA");
				rsic_doc			= regNoNego.getString("IC_DOCUMENTO")==null?"":regNoNego.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regNoNego.getString("CG_VENTACARTERA")==null)?"":regNoNego.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}				
				if ("1".equals(rsic_mon)) {
					rsTipCam="";
				}
				else { //IC_MONEDA=54 
					rsTipCam = ""+Comunes.formatoDecimal(rsTipCam,2);
				}
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(rsEpo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsAcuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumDoc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfEmision, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVenc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfCarga, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoD, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsMoneda, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDoc,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazDesc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(rsPorcDesc,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDesc,2), "formasmen", ComunesPDF.CENTER);
				if(!"".equals(cgTipoConversion)){
					pdfDoc.setCell(rsTipConv, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(rsTipCam, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoVal,2), "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell(rsModoPlaz, "formasmen", ComunesPDF.CENTER);
				//pdfDoc.setCell(rsic_doc, "formasmen", ComunesPDF.CENTER);	//revisar esta linea
				rowNoNego++;
			} //fin del while
			if (rowNoNego == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("3")){//-----[gridSelecPyme] 
			int rowSelec = 0;
			RepEstatusPyme repSelec = new RepEstatusPyme();
			repSelec.setProducto(4);
			repSelec.setIcePyme(iNoCliente);
			repSelec.setCveEstatus("3");//MOD +(cveEstatus) -("3")//repSelec.setCveEstatus("3");
			repSelec.setTiposCredito(tipoCredito);
			repSelec.setQrysentencia();
			Registros regSelec = repSelec.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
				pdfDoc.addText("Estatus:		Seleccionada Pyme","formasB",ComunesPDF.LEFT);
			
			while (regSelec.next()) {
				if(rowSelec == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("EPO");
					lEncabezados.add("Num. acuse de carga");
					lEncabezados.add("Número de documento inicial");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Fecha de publicación");
					lEncabezados.add("Plazo docto.");
					lEncabezados.add("Moneda");
					lEncabezados.add("Monto");			
				
					lEncabezados.add("Plazo para descuento en días");
					lEncabezados.add("% de descuento");
					lEncabezados.add("Monto % de descuento");
					if(!"".equals(cgTipoConversion)){
						lEncabezados.add("Tipo conv.");
						lEncabezados.add("Tipo cambio");
						lEncabezados.add("Monto valuado en pesos");
					}
					lEncabezados.add("Modalidad de plazo");
					lEncabezados.add("Número del documento final");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo");
					lEncabezados.add("Fecha de vencimiento");
					lEncabezados.add("IF");
					lEncabezados.add("Referencia tasa de interés");
					lEncabezados.add("Valor tasa de interés");
					lEncabezados.add("Monto de intereses");
					lEncabezados.add("Monto total de Capital e Interés");

					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				rsEpo				=	regSelec.getString("NOMBREEPO")==null?"":regSelec.getString("NOMBREEPO");
				rsNumDoc		= regSelec.getString("IG_NUMERO_DOCTO")==null?"":regSelec.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regSelec.getString("CC_ACUSE")==null?"":regSelec.getString("CC_ACUSE");
				rsdfEmision	= regSelec.getString("FECHA_EMISION")==null?"":regSelec.getString("FECHA_EMISION");
				rsdfVenc		= regSelec.getString("FECHA_VENC")==null?"":regSelec.getString("FECHA_VENC");
				rsdfCarga		= regSelec.getString("FECHA_CARGA")==null?"":regSelec.getString("FECHA_CARGA");
				rsPlazoD			= regSelec.getString("IG_PLAZO_DOCTO")==null?"":regSelec.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regSelec.getString("CD_NOMBRE")==null?"":regSelec.getString("CD_NOMBRE");
				rsMontoDoc	= regSelec.getString("FN_MONTO")==null?"":regSelec.getString("FN_MONTO");
				rsTipConv		= regSelec.getString("TIPO_CONVERSION")==null?"":regSelec.getString("TIPO_CONVERSION");
				rsTipCam		= regSelec.getString("TIPO_CAMBIO")==null?"":regSelec.getString("TIPO_CAMBIO");
				rsMontoVal	= regSelec.getString("MONTO_VAL")==null?"":regSelec.getString("MONTO_VAL");
				rsPlazDesc	= regSelec.getString("IG_PLAZO_DESCUENTO")==null?"":regSelec.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regSelec.getString("FN_PORC_DESCUENTO")==null?"":regSelec.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regSelec.getString("MONTO_DESC")==null?"":regSelec.getString("MONTO_DESC");
				rsModoPlaz	= regSelec.getString("MODO_PLAZO")==null?"":regSelec.getString("MODO_PLAZO");
				rsEstatus		= regSelec.getString("ESTATUS")==null?"":regSelec.getString("ESTATUS");
				rsIf						= regSelec.getString("NOMBREIF")==null?"":regSelec.getString("NOMBREIF");
				rsTipoLc			= regSelec.getString("TIPO_CRED")==null?"":regSelec.getString("TIPO_CRED");
				rsdfOperacion	= regSelec.getString("FECHA_OPERA")==null?"":regSelec.getString("FECHA_OPERA");
				rsMontoC		= regSelec.getString("FN_IMPORTE_RECIBIR")==null?"":regSelec.getString("FN_IMPORTE_RECIBIR");
				rsPlazoC			= regSelec.getString("IG_PLAZO_CREDITO")==null?"":regSelec.getString("IG_PLAZO_CREDITO");
				rsdfVencC		= regSelec.getString("FECHA_VENCRE")==null?"":regSelec.getString("FECHA_VENCRE");
				rsTasaInt		= regSelec.getString("REF_TASA")==null?"":regSelec.getString("REF_TASA");
				rsValorInt		= regSelec.getString("FN_VALOR_TASA")==null?"":regSelec.getString("FN_VALOR_TASA");
				rsMontoT		= regSelec.getString("FN_IMPORTE_INTERES")==null?"":regSelec.getString("FN_IMPORTE_INTERES");
				rsTipoCob		= regSelec.getString("TIPO_COB_INT")==null?"":regSelec.getString("TIPO_COB_INT");
				rsic_mon			= regSelec.getString("IC_MONEDA")==null?"":regSelec.getString("IC_MONEDA");
				rsic_doc			= regSelec.getString("IC_DOCUMENTO")==null?"":regSelec.getString("IC_DOCUMENTO");
				rsNumCre		= regSelec.getString("IC_DOCUMENTO")==null?"":regSelec.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regSelec.getString("CG_VENTACARTERA")==null)?"":regSelec.getString("CG_VENTACARTERA");
				
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}				
				if ("1".equals(rsic_mon)) {
					rsTipCam="";
				}
				else { //IC_MONEDA=54 
					rsTipCam = ""+Comunes.formatoDecimal(rsTipCam,2);
				}
				if (!"".equals(rsMontoC) && !"".equals(rsMontoT))	{
					rsMontoCapInt	=	Double.parseDouble(regSelec.getString("FN_IMPORTE_RECIBIR")) +	Double.parseDouble(regSelec.getString("FN_IMPORTE_INTERES"));
				}
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(rsEpo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsAcuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumDoc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfEmision, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVenc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfCarga, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoD, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsMoneda, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDoc,2), "formasmen", ComunesPDF.RIGHT);
				
				pdfDoc.setCell(rsPlazDesc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(rsPorcDesc,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDesc,2), "formasmen", ComunesPDF.RIGHT);
				if(!"".equals(cgTipoConversion)){
					pdfDoc.setCell(rsTipConv, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(rsTipCam, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoVal,2), "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell(rsModoPlaz, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumCre, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoC,2), "formasmen", ComunesPDF.RIGHT);
				pdfDoc.setCell(rsPlazoC, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVencC, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsIf, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsTasaInt, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsValorInt,2), "formasmen", ComunesPDF.RIGHT);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoT,2), "formasmen", ComunesPDF.RIGHT);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoCapInt,2), "formasmen", ComunesPDF.RIGHT);
				rowSelec++;
			} //fin del while
			if (rowSelec == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("24")){//-----[gridSelecPyme] //MOD +(cveEstatus.equals("24"))
			int rowSelec = 0;
			RepEstatusPyme repSelec = new RepEstatusPyme();
			repSelec.setProducto(4);
			repSelec.setIcePyme(iNoCliente);
			repSelec.setCveEstatus("24");//MOD +(cveEstatus) -("3")//repSelec.setCveEstatus("3");
			repSelec.setTiposCredito(tipoCredito);
			repSelec.setQrysentencia();
			Registros regSelec = repSelec.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
				pdfDoc.addText("Estatus:		En Proceso de Autorización IF","formasB",ComunesPDF.LEFT);
			
			while (regSelec.next()) {
				if(rowSelec == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("EPO");
					lEncabezados.add("Num. acuse de carga");
					lEncabezados.add("Número de documento inicial");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Fecha de publicación");
					lEncabezados.add("Plazo docto.");
					lEncabezados.add("Moneda");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo para descuento en días");
					lEncabezados.add("% de descuento");
					lEncabezados.add("Monto % de descuento");
					if(!"".equals(cgTipoConversion)){
						lEncabezados.add("Tipo conv.");
						lEncabezados.add("Tipo cambio");
						lEncabezados.add("Monto valuado en pesos");
					}
					lEncabezados.add("Modalidad de plazo");
					lEncabezados.add("Número del documento final");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo");
					lEncabezados.add("Fecha de vencimiento");
					lEncabezados.add("IF");
					lEncabezados.add("Referencia tasa de interés");
					lEncabezados.add("Valor tasa de interés");
					lEncabezados.add("Monto de intereses");
					lEncabezados.add("Monto total de Capital e Interés");

					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				rsEpo				=	regSelec.getString("NOMBREEPO")==null?"":regSelec.getString("NOMBREEPO");
				rsNumDoc		= regSelec.getString("IG_NUMERO_DOCTO")==null?"":regSelec.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regSelec.getString("CC_ACUSE")==null?"":regSelec.getString("CC_ACUSE");
				rsdfEmision	= regSelec.getString("FECHA_EMISION")==null?"":regSelec.getString("FECHA_EMISION");
				rsdfVenc		= regSelec.getString("FECHA_VENC")==null?"":regSelec.getString("FECHA_VENC");
				rsdfCarga		= regSelec.getString("FECHA_CARGA")==null?"":regSelec.getString("FECHA_CARGA");
				rsPlazoD			= regSelec.getString("IG_PLAZO_DOCTO")==null?"":regSelec.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regSelec.getString("CD_NOMBRE")==null?"":regSelec.getString("CD_NOMBRE");
				rsMontoDoc	= regSelec.getString("FN_MONTO")==null?"":regSelec.getString("FN_MONTO");
				rsTipConv		= regSelec.getString("TIPO_CONVERSION")==null?"":regSelec.getString("TIPO_CONVERSION");
				rsTipCam		= regSelec.getString("TIPO_CAMBIO")==null?"":regSelec.getString("TIPO_CAMBIO");
				rsMontoVal	= regSelec.getString("MONTO_VAL")==null?"":regSelec.getString("MONTO_VAL");
				rsPlazDesc	= regSelec.getString("IG_PLAZO_DESCUENTO")==null?"":regSelec.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regSelec.getString("FN_PORC_DESCUENTO")==null?"":regSelec.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regSelec.getString("MONTO_DESC")==null?"":regSelec.getString("MONTO_DESC");
				rsModoPlaz	= regSelec.getString("MODO_PLAZO")==null?"":regSelec.getString("MODO_PLAZO");
				rsEstatus		= regSelec.getString("ESTATUS")==null?"":regSelec.getString("ESTATUS");
				rsIf						= regSelec.getString("NOMBREIF")==null?"":regSelec.getString("NOMBREIF");
				rsTipoLc			= regSelec.getString("TIPO_CRED")==null?"":regSelec.getString("TIPO_CRED");
				rsdfOperacion	= regSelec.getString("FECHA_OPERA")==null?"":regSelec.getString("FECHA_OPERA");
				rsMontoC		= regSelec.getString("FN_IMPORTE_RECIBIR")==null?"":regSelec.getString("FN_IMPORTE_RECIBIR");
				rsPlazoC			= regSelec.getString("IG_PLAZO_CREDITO")==null?"":regSelec.getString("IG_PLAZO_CREDITO");
				rsdfVencC		= regSelec.getString("FECHA_VENCRE")==null?"":regSelec.getString("FECHA_VENCRE");
				rsTasaInt		= regSelec.getString("REF_TASA")==null?"":regSelec.getString("REF_TASA");
				rsValorInt		= regSelec.getString("FN_VALOR_TASA")==null?"":regSelec.getString("FN_VALOR_TASA");
				rsMontoT		= regSelec.getString("FN_IMPORTE_INTERES")==null?"":regSelec.getString("FN_IMPORTE_INTERES");
				rsTipoCob		= regSelec.getString("TIPO_COB_INT")==null?"":regSelec.getString("TIPO_COB_INT");
				rsic_mon			= regSelec.getString("IC_MONEDA")==null?"":regSelec.getString("IC_MONEDA");
				rsic_doc			= regSelec.getString("IC_DOCUMENTO")==null?"":regSelec.getString("IC_DOCUMENTO");
				rsNumCre		= regSelec.getString("IC_DOCUMENTO")==null?"":regSelec.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regSelec.getString("CG_VENTACARTERA")==null)?"":regSelec.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}				
				if ("1".equals(rsic_mon)) {
					rsTipCam="";
				}
				else { //IC_MONEDA=54 
					rsTipCam = ""+Comunes.formatoDecimal(rsTipCam,2);
				}
				if (!"".equals(rsMontoC) && !"".equals(rsMontoT))	{
					rsMontoCapInt	=	Double.parseDouble(regSelec.getString("FN_IMPORTE_RECIBIR")) +	Double.parseDouble(regSelec.getString("FN_IMPORTE_INTERES"));
				}
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(rsEpo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsAcuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumDoc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfEmision, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVenc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfCarga, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoD, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsMoneda, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDoc,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazDesc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(rsPorcDesc,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDesc,2), "formasmen", ComunesPDF.CENTER);
				if(!"".equals(cgTipoConversion)){
					pdfDoc.setCell(rsTipConv, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(rsTipCam, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoVal,2), "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell(rsModoPlaz, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumCre, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoC,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoC, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVencC, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsIf, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsTasaInt, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsValorInt,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoT,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoCapInt,2), "formasmen", ComunesPDF.CENTER);
				rowSelec++;
			} //fin del while
			if (rowSelec == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("4")){																																																								 		//-----[gridOperada]
			int rowOpera = 0;
			RepEstatusPyme repOpera = new RepEstatusPyme();
			repOpera.setProducto(4);
			repOpera.setIcePyme(iNoCliente);
			repOpera.setCveEstatus("4");
			repOpera.setTiposCredito(tipoCredito);
			repOpera.setQrysentencia();
			Registros regOpera = repOpera.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
			pdfDoc.addText("Estatus:		Operada","formasB",ComunesPDF.LEFT);
			while (regOpera.next()) {
				if(rowOpera == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("EPO");
					lEncabezados.add("Num. acuse de carga");
					lEncabezados.add("Número de documento inicial");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Fecha de publicación");
					lEncabezados.add("Plazo docto.");
					lEncabezados.add("Moneda");
					lEncabezados.add("Monto");							
					
					lEncabezados.add("Plazo para descuento en días");
					lEncabezados.add("% de descuento");
					lEncabezados.add("Monto % de descuento");
					if(!"".equals(cgTipoConversion)){
						lEncabezados.add("Tipo conv.");
						lEncabezados.add("Tipo cambio");
						lEncabezados.add("Monto valuado en pesos");
					}
					lEncabezados.add("Modalidad de plazo");
					lEncabezados.add("Número del documento final");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo");
					lEncabezados.add("Fecha de vencimiento");
					lEncabezados.add("Fecha de Operación IF");
					lEncabezados.add("IF");
					lEncabezados.add("Referencia tasa de interés");
					lEncabezados.add("Valor tasa de interés");
					lEncabezados.add("Monto de intereses");
					lEncabezados.add("Monto total de Capital e Interés");

					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				rsEpo				=	regOpera.getString("NOMBREEPO")==null?"":regOpera.getString("NOMBREEPO");
				rsNumDoc		= regOpera.getString("IG_NUMERO_DOCTO")==null?"":regOpera.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regOpera.getString("CC_ACUSE")==null?"":regOpera.getString("CC_ACUSE");
				rsdfEmision	= regOpera.getString("FECHA_EMISION")==null?"":regOpera.getString("FECHA_EMISION");
				rsdfVenc		= regOpera.getString("FECHA_VENC")==null?"":regOpera.getString("FECHA_VENC");
				rsdfCarga		= regOpera.getString("FECHA_CARGA")==null?"":regOpera.getString("FECHA_CARGA");
				rsPlazoD			= regOpera.getString("IG_PLAZO_DOCTO")==null?"":regOpera.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regOpera.getString("CD_NOMBRE")==null?"":regOpera.getString("CD_NOMBRE");
				rsMontoDoc	= regOpera.getString("FN_MONTO")==null?"":regOpera.getString("FN_MONTO");
				rsTipConv		= regOpera.getString("TIPO_CONVERSION")==null?"":regOpera.getString("TIPO_CONVERSION");
				rsTipCam		= regOpera.getString("TIPO_CAMBIO")==null?"":regOpera.getString("TIPO_CAMBIO");
				rsMontoVal	= regOpera.getString("MONTO_VAL")==null?"":regOpera.getString("MONTO_VAL");
				rsPlazDesc	= regOpera.getString("IG_PLAZO_DESCUENTO")==null?"":regOpera.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regOpera.getString("FN_PORC_DESCUENTO")==null?"":regOpera.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regOpera.getString("MONTO_DESC")==null?"":regOpera.getString("MONTO_DESC");
				rsModoPlaz	= regOpera.getString("MODO_PLAZO")==null?"":regOpera.getString("MODO_PLAZO");
				rsEstatus		= regOpera.getString("ESTATUS")==null?"":regOpera.getString("ESTATUS");
				rsIf						= regOpera.getString("NOMBREIF")==null?"":regOpera.getString("NOMBREIF");
				rsTipoLc			= regOpera.getString("TIPO_CRED")==null?"":regOpera.getString("TIPO_CRED");
				rsdfOperacion	= regOpera.getString("FECHA_OPERA")==null?"":regOpera.getString("FECHA_OPERA");
				rsMontoC		= regOpera.getString("FN_IMPORTE_RECIBIR")==null?"":regOpera.getString("FN_IMPORTE_RECIBIR");
				rsPlazoC			= regOpera.getString("IG_PLAZO_CREDITO")==null?"":regOpera.getString("IG_PLAZO_CREDITO");
				rsdfVencC		= regOpera.getString("FECHA_VENCRE")==null?"":regOpera.getString("FECHA_VENCRE");
				rsTasaInt		= regOpera.getString("REF_TASA")==null?"":regOpera.getString("REF_TASA");
				rsValorInt		= regOpera.getString("FN_VALOR_TASA")==null?"":regOpera.getString("FN_VALOR_TASA");
				rsMontoT		= regOpera.getString("FN_IMPORTE_INTERES")==null?"":regOpera.getString("FN_IMPORTE_INTERES");
				rsTipoCob		= regOpera.getString("TIPO_COB_INT")==null?"":regOpera.getString("TIPO_COB_INT");
				rsic_mon			= regOpera.getString("IC_MONEDA")==null?"":regOpera.getString("IC_MONEDA");
				rsic_doc			= regOpera.getString("IC_DOCUMENTO")==null?"":regOpera.getString("IC_DOCUMENTO");
				rsFechaOperIF= regOpera.getString("FECHA_OPER_IF")==null?"":regOpera.getString("FECHA_OPER_IF");
				rsNumCre		= regOpera.getString("IC_DOCUMENTO")==null?"":regOpera.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regOpera.getString("CG_VENTACARTERA")==null)?"":regOpera.getString("CG_VENTACARTERA");
								
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}				
				if ("1".equals(rsic_mon)) {
					rsTipCam="";
				}
				else { //IC_MONEDA=54 
					rsTipCam = ""+Comunes.formatoDecimal(rsTipCam,2);
				}
				if (!"".equals(rsMontoC) && !"".equals(rsMontoT))	{
					rsMontoCapInt	=	Double.parseDouble(regOpera.getString("FN_IMPORTE_RECIBIR")) +	Double.parseDouble(regOpera.getString("FN_IMPORTE_INTERES"));
				}
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(rsEpo, "formasmen", ComunesPDF.LEFT);
				pdfDoc.setCell(rsAcuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumDoc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfEmision, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVenc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfCarga, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoD, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsMoneda, "formasmen", ComunesPDF.LEFT);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDoc,2), "formasmen", ComunesPDF.RIGHT);				
					
				pdfDoc.setCell(rsPlazDesc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(rsPorcDesc,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDesc,2), "formasmen", ComunesPDF.RIGHT);
				if(!"".equals(cgTipoConversion)){
					pdfDoc.setCell(rsTipConv, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(rsTipCam, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoVal,2), "formasmen", ComunesPDF.RIGHT);
				}
				pdfDoc.setCell(rsModoPlaz, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumCre, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoC,2), "formasmen", ComunesPDF.RIGHT);
				pdfDoc.setCell(rsPlazoC, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVencC, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsFechaOperIF, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsIf, "formasmen", ComunesPDF.LEFT);
				pdfDoc.setCell(rsTasaInt, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsValorInt,2), "formasmen", ComunesPDF.RIGHT);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoT,2), "formasmen", ComunesPDF.RIGHT);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoCapInt,2), "formasmen", ComunesPDF.RIGHT);
				rowOpera++;
			} //fin del while
			if (rowOpera == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("11")){																																																								 //-----[gridOperaPaga]
			int rowOperaPaga = 0;
			RepEstatusPyme repOperaPaga = new RepEstatusPyme();
			repOperaPaga.setProducto(4);
			repOperaPaga.setIcePyme(iNoCliente);
			repOperaPaga.setCveEstatus("11");
			repOperaPaga.setTiposCredito(tipoCredito);
			repOperaPaga.setQrysentencia();
			Registros regOperaPaga = repOperaPaga.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
			pdfDoc.addText("Estatus:		Operada Pagada","formasB",ComunesPDF.LEFT);
			while (regOperaPaga.next()) {
				if(rowOperaPaga == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("EPO");
					lEncabezados.add("Num. acuse de carga");
					lEncabezados.add("Número de documento inicial");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Fecha de publicación");
					lEncabezados.add("Plazo docto.");
					lEncabezados.add("Moneda");
					lEncabezados.add("Monto");									
					lEncabezados.add("Plazo para descuento en días");
					lEncabezados.add("% de descuento");
					lEncabezados.add("Monto % de descuento");
					if(!"".equals(cgTipoConversion)){
						lEncabezados.add("Tipo conv.");
						lEncabezados.add("Tipo cambio");
						lEncabezados.add("Monto valuado en pesos");
					}
					lEncabezados.add("Modalidad de plazo");
					lEncabezados.add("Número del documento final");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo");
					lEncabezados.add("Fecha de vencimiento");
					lEncabezados.add("Fecha de Operación IF");
					lEncabezados.add("IF");
					lEncabezados.add("Referencia tasa de interés");
					lEncabezados.add("Valor tasa de interés");
					lEncabezados.add("Monto de intereses");
					lEncabezados.add("Monto total de Capital e Interés");

					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				rsEpo				=	regOperaPaga.getString("NOMBREEPO")==null?"":regOperaPaga.getString("NOMBREEPO");
				rsNumDoc		= regOperaPaga.getString("IG_NUMERO_DOCTO")==null?"":regOperaPaga.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regOperaPaga.getString("CC_ACUSE")==null?"":regOperaPaga.getString("CC_ACUSE");
				rsdfEmision	= regOperaPaga.getString("FECHA_EMISION")==null?"":regOperaPaga.getString("FECHA_EMISION");
				rsdfVenc		= regOperaPaga.getString("FECHA_VENC")==null?"":regOperaPaga.getString("FECHA_VENC");
				rsdfCarga		= regOperaPaga.getString("FECHA_CARGA")==null?"":regOperaPaga.getString("FECHA_CARGA");
				rsPlazoD			= regOperaPaga.getString("IG_PLAZO_DOCTO")==null?"":regOperaPaga.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regOperaPaga.getString("CD_NOMBRE")==null?"":regOperaPaga.getString("CD_NOMBRE");
				rsMontoDoc	= regOperaPaga.getString("FN_MONTO")==null?"":regOperaPaga.getString("FN_MONTO");
				rsTipConv		= regOperaPaga.getString("TIPO_CONVERSION")==null?"":regOperaPaga.getString("TIPO_CONVERSION");
				rsTipCam		= regOperaPaga.getString("TIPO_CAMBIO")==null?"":regOperaPaga.getString("TIPO_CAMBIO");
				rsMontoVal	= regOperaPaga.getString("MONTO_VAL")==null?"":regOperaPaga.getString("MONTO_VAL");
				rsPlazDesc	= regOperaPaga.getString("IG_PLAZO_DESCUENTO")==null?"":regOperaPaga.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regOperaPaga.getString("FN_PORC_DESCUENTO")==null?"":regOperaPaga.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regOperaPaga.getString("MONTO_DESC")==null?"":regOperaPaga.getString("MONTO_DESC");
				rsModoPlaz	= regOperaPaga.getString("MODO_PLAZO")==null?"":regOperaPaga.getString("MODO_PLAZO");
				rsEstatus		= regOperaPaga.getString("ESTATUS")==null?"":regOperaPaga.getString("ESTATUS");
				rsIf						= regOperaPaga.getString("NOMBREIF")==null?"":regOperaPaga.getString("NOMBREIF");
				rsTipoLc			= regOperaPaga.getString("TIPO_CRED")==null?"":regOperaPaga.getString("TIPO_CRED");
				rsdfOperacion	= regOperaPaga.getString("FECHA_OPERA")==null?"":regOperaPaga.getString("FECHA_OPERA");
				rsMontoC		= regOperaPaga.getString("FN_IMPORTE_RECIBIR")==null?"":regOperaPaga.getString("FN_IMPORTE_RECIBIR");
				rsPlazoC			= regOperaPaga.getString("IG_PLAZO_CREDITO")==null?"":regOperaPaga.getString("IG_PLAZO_CREDITO");
				rsdfVencC		= regOperaPaga.getString("FECHA_VENCRE")==null?"":regOperaPaga.getString("FECHA_VENCRE");
				rsTasaInt		= regOperaPaga.getString("REF_TASA")==null?"":regOperaPaga.getString("REF_TASA");
				rsValorInt		= regOperaPaga.getString("FN_VALOR_TASA")==null?"":regOperaPaga.getString("FN_VALOR_TASA");
				rsMontoT		= regOperaPaga.getString("FN_IMPORTE_INTERES")==null?"":regOperaPaga.getString("FN_IMPORTE_INTERES");
				rsTipoCob		= regOperaPaga.getString("TIPO_COB_INT")==null?"":regOperaPaga.getString("TIPO_COB_INT");
				rsic_mon			= regOperaPaga.getString("IC_MONEDA")==null?"":regOperaPaga.getString("IC_MONEDA");
				rsic_doc			= regOperaPaga.getString("IC_DOCUMENTO")==null?"":regOperaPaga.getString("IC_DOCUMENTO");
				rsFechaOperIF= regOperaPaga.getString("FECHA_OPER_IF")==null?"":regOperaPaga.getString("FECHA_OPER_IF");
				rsNumCre		= regOperaPaga.getString("IC_DOCUMENTO")==null?"":regOperaPaga.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regOperaPaga.getString("CG_VENTACARTERA")==null)?"":regOperaPaga.getString("CG_VENTACARTERA");
			
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}				
				if ("1".equals(rsic_mon)) {
					rsTipCam="";
				}
				else { //IC_MONEDA=54 
					rsTipCam = ""+Comunes.formatoDecimal(rsTipCam,2);
				}
				if (!"".equals(rsMontoC) && !"".equals(rsMontoT))	{
					rsMontoCapInt	=	Double.parseDouble(regOperaPaga.getString("FN_IMPORTE_RECIBIR")) +	Double.parseDouble(regOperaPaga.getString("FN_IMPORTE_INTERES"));
				}
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(rsEpo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsAcuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumDoc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfEmision, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVenc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfCarga, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoD, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsMoneda, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDoc,2), "formasmen", ComunesPDF.CENTER);				
				pdfDoc.setCell(rsPlazDesc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(rsPorcDesc,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDesc,2), "formasmen", ComunesPDF.CENTER);
				if(!"".equals(cgTipoConversion)){
					pdfDoc.setCell(rsTipConv, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(rsTipCam, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoVal,2), "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell(rsModoPlaz, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumCre, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoC,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoC, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVencC, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsFechaOperIF, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsIf, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsTasaInt, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsValorInt,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoT,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoCapInt,2), "formasmen", ComunesPDF.CENTER);
				rowOperaPaga++;
			} //fin del while
			if (rowOperaPaga == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("22")){																																																								 //-----[gridOperaVence]
			int rowOperaVence = 0;
			RepEstatusPyme repOperaVence = new RepEstatusPyme();
			repOperaVence.setProducto(4);
			repOperaVence.setIcePyme(iNoCliente);
			repOperaVence.setCveEstatus("22");
			repOperaVence.setTiposCredito(tipoCredito);
			repOperaVence.setQrysentencia();
			Registros regOperaVence = repOperaVence.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
			pdfDoc.addText("Estatus:		Pendiente de pago IF","formasB",ComunesPDF.LEFT);
			while (regOperaVence.next()) {
				if(rowOperaVence == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("EPO");
					lEncabezados.add("Num. acuse de carga");
					lEncabezados.add("Número de documento inicial");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Fecha de publicación");
					lEncabezados.add("Plazo docto.");
					lEncabezados.add("Moneda");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo para descuento en días");
					lEncabezados.add("% de descuento");
					lEncabezados.add("Monto % de descuento");
					if(!"".equals(cgTipoConversion)){
						lEncabezados.add("Tipo conv.");
						lEncabezados.add("Tipo cambio");
						lEncabezados.add("Monto valuado en pesos");
					}
					lEncabezados.add("Modalidad de plazo");
					lEncabezados.add("Número del documento final");
					lEncabezados.add("Monto");
					lEncabezados.add("Plazo");
					lEncabezados.add("Fecha de vencimiento");
					lEncabezados.add("Fecha de Operación IF");
					lEncabezados.add("IF");
					lEncabezados.add("Referencia tasa de interés");
					lEncabezados.add("Valor tasa de interés");
					lEncabezados.add("Monto de intereses");
					lEncabezados.add("Monto total de Capital e Interés");
					lEncabezados.add("Fecha de Cambio de Estatus");

					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				rsEpo				=	regOperaVence.getString("NOMBREEPO")==null?"":regOperaVence.getString("NOMBREEPO");
				rsNumDoc		= regOperaVence.getString("IG_NUMERO_DOCTO")==null?"":regOperaVence.getString("IG_NUMERO_DOCTO");
				rsAcuse			= regOperaVence.getString("CC_ACUSE")==null?"":regOperaVence.getString("CC_ACUSE");
				rsdfEmision	= regOperaVence.getString("FECHA_EMISION")==null?"":regOperaVence.getString("FECHA_EMISION");
				rsdfVenc		= regOperaVence.getString("FECHA_VENC")==null?"":regOperaVence.getString("FECHA_VENC");
				rsdfCarga		= regOperaVence.getString("FECHA_CARGA")==null?"":regOperaVence.getString("FECHA_CARGA");
				rsPlazoD			= regOperaVence.getString("IG_PLAZO_DOCTO")==null?"":regOperaVence.getString("IG_PLAZO_DOCTO");
				rsMoneda		= regOperaVence.getString("CD_NOMBRE")==null?"":regOperaVence.getString("CD_NOMBRE");
				rsMontoDoc	= regOperaVence.getString("FN_MONTO")==null?"":regOperaVence.getString("FN_MONTO");
				rsTipConv		= regOperaVence.getString("TIPO_CONVERSION")==null?"":regOperaVence.getString("TIPO_CONVERSION");
				rsTipCam		= regOperaVence.getString("TIPO_CAMBIO")==null?"":regOperaVence.getString("TIPO_CAMBIO");
				rsMontoVal	= regOperaVence.getString("MONTO_VAL")==null?"":regOperaVence.getString("MONTO_VAL");
				rsPlazDesc	= regOperaVence.getString("IG_PLAZO_DESCUENTO")==null?"":regOperaVence.getString("IG_PLAZO_DESCUENTO");
				rsPorcDesc	= regOperaVence.getString("FN_PORC_DESCUENTO")==null?"":regOperaVence.getString("FN_PORC_DESCUENTO");
				rsMontoDesc= regOperaVence.getString("MONTO_DESC")==null?"":regOperaVence.getString("MONTO_DESC");
				rsModoPlaz	= regOperaVence.getString("MODO_PLAZO")==null?"":regOperaVence.getString("MODO_PLAZO");
				rsEstatus		= regOperaVence.getString("ESTATUS")==null?"":regOperaVence.getString("ESTATUS");
				rsIf						= regOperaVence.getString("NOMBREIF")==null?"":regOperaVence.getString("NOMBREIF");
				rsTipoLc			= regOperaVence.getString("TIPO_CRED")==null?"":regOperaVence.getString("TIPO_CRED");
				rsdfOperacion	= regOperaVence.getString("FECHA_OPERA")==null?"":regOperaVence.getString("FECHA_OPERA");
				rsMontoC		= regOperaVence.getString("FN_IMPORTE_RECIBIR")==null?"":regOperaVence.getString("FN_IMPORTE_RECIBIR");
				rsPlazoC			= regOperaVence.getString("IG_PLAZO_CREDITO")==null?"":regOperaVence.getString("IG_PLAZO_CREDITO");
				rsdfVencC		= regOperaVence.getString("FECHA_VENCRE")==null?"":regOperaVence.getString("FECHA_VENCRE");
				rsTasaInt		= regOperaVence.getString("REF_TASA")==null?"":regOperaVence.getString("REF_TASA");
				rsValorInt		= regOperaVence.getString("FN_VALOR_TASA")==null?"":regOperaVence.getString("FN_VALOR_TASA");
				rsMontoT		= regOperaVence.getString("FN_IMPORTE_INTERES")==null?"":regOperaVence.getString("FN_IMPORTE_INTERES");
				rsTipoCob		= regOperaVence.getString("TIPO_COB_INT")==null?"":regOperaVence.getString("TIPO_COB_INT");
				rsic_mon			= regOperaVence.getString("IC_MONEDA")==null?"":regOperaVence.getString("IC_MONEDA");
				rsic_doc			= regOperaVence.getString("IC_DOCUMENTO")==null?"":regOperaVence.getString("IC_DOCUMENTO");
				rsFechaOperIF= regOperaVence.getString(31)==null?"":regOperaVence.getString(31);
				rsNumCre		= regOperaVence.getString("IC_DOCUMENTO")==null?"":regOperaVence.getString("IC_DOCUMENTO");
				bandeVentaCartera	=	(regOperaVence.getString("CG_VENTACARTERA")==null)?"":regOperaVence.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
					rsModoPlaz ="";
				}				
				if ("1".equals(rsic_mon)) {
					rsTipCam="";
				}
				else { //IC_MONEDA=54 
					rsTipCam = ""+Comunes.formatoDecimal(rsTipCam,2);
				}
				if (!"".equals(rsMontoC) && !"".equals(rsMontoT))	{
					rsMontoCapInt	=	Double.parseDouble(regOperaVence.getString("FN_IMPORTE_RECIBIR")) +	Double.parseDouble(regOperaVence.getString("FN_IMPORTE_INTERES"));
				}
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(rsEpo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsAcuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumDoc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfEmision, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVenc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfCarga, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoD, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsMoneda, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDoc,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazDesc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(rsPorcDesc,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoDesc,2), "formasmen", ComunesPDF.CENTER);
				if(!"".equals(cgTipoConversion)){
					pdfDoc.setCell(rsTipConv, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(rsTipCam, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoVal,2), "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell(rsModoPlaz, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsNumCre, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoC,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsPlazoC, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsdfVencC, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsFechaOperIF, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsIf, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsTasaInt, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsValorInt,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoT,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ " + Comunes.formatoDecimal(rsMontoCapInt,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(rsFechaOperIF, "formasmen", ComunesPDF.CENTER);
				rowOperaVence++;
			} //fin del while
			if (rowOperaVence == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}

		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>