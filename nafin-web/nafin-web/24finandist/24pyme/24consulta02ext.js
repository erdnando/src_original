Ext.onReady(function() {
//--------------------------------HANDLERS--------------------------------------
	var procesarConsultaData = function (store, arrRegistros, opts) {
		var lblFechaPago = Ext.getCmp('lblFechaPago');
      var fecha_dpo = "";
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros!= null) {
			if(!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnTotal = Ext.getCmp('btnTotal');
			
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				var row = store.getAt(0);
		      fecha_dpo = row.get('FECHA_DPO');
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();
				btnBajarArchivo.hide();
				btnBajarPDF.hide();
				btnTotal.enable();
				lblFechaPago.getEl().update(fecha_dpo);
				
				if(!btnBajarArchivo.isVisible()){
					btnGenerarArchivo.enable;
				} else {
					btnGenerarPDF.disable();					
				}
				if(!btnBajarPDF.isVisible()){
					btnGenerarPDF.enable();
				} else {
					btnGenerarPDF.disable();
				}
				el.unmask();
			}else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnTotal.disable();
				lblFechaPago.getEl().update('');
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPDF = function (opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarPDF.setHandler(function (boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
//---------------------------------STORES------------------------------------
	var catalogoEPODistData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPODistribuidores'
		},
		totalProperty: 'total',
		autoload: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '24consulta02ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'IC_DOCUMENTO'},
			{name: 'FECHA_DPO'},
			{name: 'FECHA_PAGO'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'TASA_REFERENCIA'},
			{name: 'VALOR_TASA_INT'},
			{name: 'MONTO_TASA_INT', type:'float'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});
	var totalData = new Ext.data.JsonStore({
		fields: [
			{name: 'MONTO_TASA_INT',type: 'float'}
		],
		data: [
			{MONTO_TASA_INT:'0'}
		],
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});
//------------------------------COMPONENTES----------------------------------
		var elementosForma = [
			{
				xtype: 'combo',
				name: 'ic_epo',
				id: 'cboEpo',
				allowBlank: false,
				fieldLabel: 'Nombre de la EPO',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				hiddenName: 'ic_epo',
				emptyText: 'Seleccione un EPO',
				forceSeleccion: true,
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEPODistData,
				tpl: NE.util.templateMensajeCargaCombo
			},
			{
				xtype: 'textfield',
				name: 'numDoc',
				id: 'cmbNumDoc',
				allowBlank: false,
				fieldLabel: 'Num. documento'
			}
		];

	var fp1=new Ext.FormPanel({
		height:30,
		width: 890,
		style: 'margin:0 auto',
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		labelWidth: 130,
		items: [{
        xtype: 'label',
		  id: 'lblFechaPago',
        fieldLabel: 'Fecha de operaci�n',
		  text: '-'
		}]
});
	var gridTotal = {
		xtype: 'grid',
		id: 'gridTotal',
		store: totalData,
		columns: [
			{
				header: 'Monto de intereses a pagar',
				dataIndex: 'MONTO_TASA_INT',
				width: 200,
				align: 'center'
			}
		],
		height: 50,
		width:880,
		title: '',
		frame: false
	};
	var grid = new Ext.grid.GridPanel({
		id:'gridGeneral',
		store:consultaData,
		style: 'margin:0 auto',
		hidden: false,
		columns: [
				{
					header: 'N�mero pago',
					tooltip: 'N�mero pago',
					sortable: true,
					dataIndex: 'IC_DOCUMENTO',
					hidden: false,
					width: 110,
					align: 'center'
				},
				{
					header: 'Fecha pago',
					tooltip: 'Fecha pago',
					sortable: true,
					dataIndex: 'FECHA_PAGO',
					hidden: false,
					width: 110,
					align: 'center'
				},
				{
					header: 'Plazo',
					tooltip: 'Plazo',
					sortable: true,
					dataIndex: 'IG_PLAZO_CREDITO',
					hidden:false,
					width: 110,
					align: 'center'
				},
				{
					header: 'Tasa referenciada',
					tooltip: 'Tasa referenciada',
					sortable: true,
					dataIndex: 'TASA_REFERENCIA',
					hidden: false,
					width: 250,
					align: 'center'
				},
				{
					header: 'Tasa de inter�s',
					tooltip: 'Tasa de inter�s',
					sortable: true,
					dataIndex: 'VALOR_TASA_INT',
					hidden: false,
					width: 110,
					renderer: Ext.util.Format.numberRenderer('0,0.00'),
					align: 'center'
				},
				{
					header: 'Monto de intereses a pagar',
					tooltip: 'Monto de intereses a pagar',
					sortable: true,
					dataIndex: 'MONTO_TASA_INT',
					hidden: false,
					width: 195,
					renderer: Ext.util.Format.numberRenderer('0,0.00'),
					align: 'center'
				}
			],
			margins: '20 0 0 0',
			stripeRows: true,
			loadMask: true,
			frame: true,
			width: 890,
			height: 350,
			bbar: {
				items: [
					'->',
					'-',
					{
						xtype: 'button',
						text: 'Total monto',
						id: 'btnTotal',
						hidden: false,
						handler: function (boton,evento) {
							var storeGeneral = consultaData;
							var sum=0;
							if(storeGeneral.getTotalCount() > 0) {
								storeGeneral.each(function(registro){
									sum += registro.get('MONTO_TASA_INT');
								});
								var reg = totalData.getAt(0);//Primer registro porque son totales
								reg.data.MONTO_TASA_INT = sum;
							}	
								
							var ventana = Ext.getCmp('ventanaTotal');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									layout: 'fit',
									width: 890,
									height: 80,
									id: 'ventanaTotal',
									closeAction: 'hide',
									items: [
										gridTotal
									],
									title: 'Totales'
								}).show().alignTo(grid.el);
							}
						}
					},
					'-',
					{
						xtype: 'button',
						text: 'Generar Archivo',
						id: 'btnGenerarArchivo',
						handler: function(boton,evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							var cmbNumDoc = Ext.getCmp("cmbNumDoc");
							var cboEpo = Ext.getCmp('cboEpo');
							Ext.Ajax.request({
								url: '24consulta02ext.data.jsp',
								params: {
									informacion: 'ArchivoCSV',
									numDoc: cmbNumDoc.getValue(),
									ic_epo: cboEpo.getValue()
								},
								callback: procesarSuccessFailureGenerarArchivo
							});
						}
					},
					{
						xtype: 'button',
						text: 'BajarArchivo',
						id: 'btnBajarArchivo',
						hidden: true
					},
					{
						xtype: 'button',
						text: 'Generar PDF',
						id: 'btnGenerarPDF',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							var cmbNumDoc = Ext.getCmp("cmbNumDoc");
							var cboEpo = Ext.getCmp('cboEpo');
							Ext.Ajax.request({
								url: '24consulta02ext.data.jsp',
								params: {
									informacion: 'ArchivoPDF',
									numDoc: cmbNumDoc.getValue(),
									ic_epo: cboEpo.getValue()
								},
								callback: procesarSuccessFailureGenerarPDF
							});
						}
					},
					{
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDF',
						hidden: true
					},
					'-'
				]
			}
	});
	//-------------------------------PRINCIPAL-----------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Buscar por',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto',
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
					msgTarget: 'side',
					anchor: '-20'
		},
		items: elementosForma,
		monitorValid:true,
		buttons: [
					{
						text: 'Consultar',
						iconCls: 'icoBuscar',
						formBind: true,
						handler: function(boton,evento) {
							fp.el.mask('Enviando...','x-msk-loading');
							var ventana = Ext.getCmp('ventanaTotal');
							if(ventana){
								ventana.destroy();
							}
							consultaData.load({
								params: Ext.apply(fp.getForm().getValues(), {
									operacion: 'Generar',
									start: 0,
									limit: 5
								})
							});
							Ext.getCmp('btnGenerarArchivo').enable();
							Ext.getCmp('btnBajarArchivo').hide();
							Ext.getCmp('btnGenerarPDF').enable();
							Ext.getCmp('btnBajarPDF').hide();
							Ext.getCmp('btnTotal').disable();
						}
					},
					{
						text:'Limpiar',
						iconCls: 'icoLimpiar',
						handler: function() {
							fp.getForm().reset();
			            grid.hide();
							var ventana = Ext.getCmp('ventanaTotal');
							if(ventana){
								ventana.close();
							}
						var lblFechaPago = Ext.getCmp('lblFechaPago');
						lblFechaPago.getEl().update('-');
						}
					}		
			]
	});
	//La aplicaci�n se mostrar� en un div de cierto ancho, para lo cual est� definido
	//el siguiente contenedor.
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			fp1,
			grid
		]
	});
//------------------------------------------------------------------------------
	catalogoEPODistData.load();
});