Ext.onReady(function() {
//_--------------------------------- HANDLERS -------------------------------
//Para grid_b
	var procesarConsultaData_b = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid_b.isVisible()) {
				grid_b.show();
			}
			var btnBajarArchivo_b = Ext.getCmp('btnBajarArchivo_b');
			var btnGenerarArchivo_b = Ext.getCmp('btnGenerarArchivo_b');
			var btnGenerarPDF_b = Ext.getCmp('btnGenerarPDF_b');
			var btnBajarPDF_b = Ext.getCmp('btnBajarPDF_b');
			 
			var el_b = grid_b.getGridEl();
	
			if(store.getTotalCount() > 0) {
				btnGenerarPDF_b.enable();
				if(!btnBajarArchivo_b.isVisible()) {
					btnGenerarArchivo_b.enable();
				} else {
					btnGenerarArchivo_b.disable();
				}
				el_b.unmask();
			} else {
				btnGenerarArchivo_b.disable();
				btnGenerarPDF_b.disable();
				el_b.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
//fin de grid_b

//Inicio de grid
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
      }
      var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
      var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
      var btnBajarPDF = Ext.getCmp('btnBajarPDF');
      
      var el = grid.getGridEl();
       
      if(store.getTotalCount() > 0) {
			btnGenerarPDF.enable();
			btnBajarPDF.hide();
			if(!btnBajarArchivo.isVisible()) {
				btnGenerarArchivo.enable();
			} else {
				btnGenerarArchivo.disable();
			}
			el.unmask();
		}else {
         btnGenerarArchivo.disable();
         btnGenerarPDF.disable();
         el.mask('No se encontr� ning�n registro', 'x-mask');
     }
	 }
	}
  //***generacion de archivo .pdf para grid_b
	var procesarSuccessFailureGenerarPDF_b =  function(opts, success, response) {
		var btnGenerarPDF_b = Ext.getCmp('btnGenerarPDF_b');
		btnGenerarPDF_b.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF_b = Ext.getCmp('btnBajarPDF_b');
			btnBajarPDF_b.show();
			btnBajarPDF_b.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF_b.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF_b.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
//*** para otro grid.
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGeneraPDF = Ext.getCmp('btnGenerarPDF');
		btnGeneraPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajaPDF = Ext.getCmp('btnBajarPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
//*** fin de generacion de archivos .pdf

//***generacion de archivos .csv
	var procesarSuccessFailureGenerarArchivo_b =  function(opts, success, response) {
		var btnGenerarArchivo_b = Ext.getCmp('btnGenerarArchivo_b');
		btnGenerarArchivo_b.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo_b = Ext.getCmp('btnBajarArchivo_b');
			btnBajarArchivo_b.show();
			btnBajarArchivo_b.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo_b.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo_b.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
//para otro grid
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
//***Fin de generacion de archivos .csv
	//-------------------------------- STORES -----------------------------------
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta05ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPODist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
  
  //store para grid_b
	var consultaData_b = new Ext.data.GroupingStore({
		root : 'registros',
		url : '24consulta05ext.data.jsp',
		baseParams: {
			informacion: 'Consulta_b'
		},
    reader: new Ext.data.JsonReader({
    	root : 'registros', totalProperty: 'total',
      fields: [
			{name: 'NOMBRE_EPO_CONCAT'},
			{name: 'NOMBRE_IF'},
  			{name: 'NUMERO_LINEA'},
			{name: 'MONEDA'},
			{name: 'FECHA_AUTORIZACION'},
			{name: 'IN_PLAZO_DIAS'},
			{name: 'FECHA_VENC'},     //type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_MONTO_AUTORIZADO_TOTAL', type: 'float'},
			{name: 'ESQUEMA_INTERES'}
      ]
    }),
    groupField: 'NOMBRE_EPO_CONCAT',	sortInfo:{field: 'NOMBRE_EPO_CONCAT', direction: "ASC"},
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false, 
    listeners: {
    load: procesarConsultaData_b,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData_b(null, null, null);
				}
			}
		}
	});  
  //fin de store grid_b
  
  //store para grid
	var consultaData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '24consulta05ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields: [
			{name: 'NOMBRE_EPO_CONCAT'},
			{name: 'TIPO_CREDITO'},
      	{name: 'NOMBRE_IF'},
  			{name: 'MONEDA'},
    		{name: 'ESQUEMA_INTERES'}
      ]
		}),
		groupField: 'NOMBRE_EPO_CONCAT',	sortInfo:{field: 'NOMBRE_EPO_CONCAT', direction: "ASC"},
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});
  
// Elementos que se muestran en la pantalla.
  var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'cmbEpo',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_epo',
			emptyText: 'Seleccione una EPO',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo
      }
      ];

    // create the Grid_b para consulta 2
	var grid_b = new Ext.grid.GridPanel({
		store: consultaData_b,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'EPO', dataIndex: 'NOMBRE_EPO_CONCAT',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header: 'IF Relacionado', tooltip: 'IF Relacionado', dataIndex: 'NOMBRE_IF',
				sortable: true,	width: 250,	resizable: true, hidden: false
			},      
			{
				header : 'Num. L�nea Cr�dito', tooltip: 'Num. L�nea Cr�dito',	dataIndex : 'NUMERO_LINEA',
				sortable : true, width : 200, hidden : false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'MONEDA',
				sortable : true, width : 250, hidden : false
			},
			{
				header : 'Fecha autorizaci�n', tooltip: 'Fecha autorizaci�n', dataIndex : 'FECHA_AUTORIZACION', 
        sortable : true, width : 200, hidden : false, align: 'center'
			},
			{
				header : 'Plazo', tooltip: 'Plazo', dataIndex : 'IN_PLAZO_DIAS', 
        sortable : true, width : 100, hidden : false, align: 'center'
			},
			{
				header : 'Fecha vencimiento', tooltip: 'Fecha vencimiento', dataIndex : 'FECHA_VENC', 
        sortable : true, width : 200, hidden : false, align: 'center'
			},
			{
				header : 'Monto l�nea', tooltip: 'Monto l�nea', dataIndex : 'FN_MONTO_AUTORIZADO_TOTAL', 
        sortable : true, width : 150, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
        hidden : false, align: 'right'
			},      
			{
				header : 'Esquema de cobro a intereses', tooltip: 'Esquema de cobro a intereses',
				dataIndex : 'ESQUEMA_INTERES', sortable : true, width : 250, hidden : false
			}
		],
		view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 400,
		width: 940,
		title: '',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo_b',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta05ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
                informacion: 'ArchivoCSV_b',
                tipo: 'CSV_b'
              }),
							callback: procesarSuccessFailureGenerarArchivo_b
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo_b',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF_b',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta05ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF_b',
								tipo: 'PDF_b'
              }),
							callback: procesarSuccessFailureGenerarPDF_b
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF_b',
					hidden: true
        }
      ]
    }
  });      //Fin de grid_b

    // create the Grid para consulta 1
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'EPO', dataIndex: 'NOMBRE_EPO_CONCAT', 
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header: 'IF Relacionado', tooltip: 'IF Relacionado', dataIndex: 'NOMBRE_IF',
				sortable: true,	width: 250,	resizable: true, hidden: false
			},      
			{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'MONEDA',
				sortable : true, width : 250, hidden : false 
			},
			{
				header : 'Esquema de cobro a intereses', tooltip: 'Esquema de cobro a intereses',
				dataIndex : 'ESQUEMA_INTERES', sortable : true, width : 250, hidden : false
			}
		],
		view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 400,
		width: 940,
		title: '',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta05ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								tipo: 'CSV'
              }),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta05ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF',
							tipo: 'PDF'
              }),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
  });      
 
//-------------------------------- PRINCIPAL -----------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: '.:: N@fin Electr�nico :: Financiamiento a Distribuidores ::.',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 30,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var _epo = Ext.getCmp("cmbEpo");
					if (Ext.isEmpty(_epo.getValue()) ) {
							_epo.markInvalid('Por favor, seleccione una EPO');
							return;
					}
					grid.hide();
					grid_b.hide();
					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({params: Ext.apply(fp.getForm().getValues(),{})});
					consultaData_b.load({params: Ext.apply(fp.getForm().getValues())});
					Ext.getCmp('btnGenerarArchivo').disable();
					Ext.getCmp('btnBajarArchivo').hide();
					Ext.getCmp('btnGenerarPDF').disable();
					Ext.getCmp('btnBajarPDF').hide();
					Ext.getCmp('btnGenerarArchivo_b').disable();
					Ext.getCmp('btnBajarArchivo_b').hide();
					Ext.getCmp('btnGenerarPDF_b').disable();
					Ext.getCmp('btnBajarPDF_b').hide();
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					fp.getForm().reset();
					grid.hide();
					grid_b.hide();
				}
			}
		]
	});
  
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid,
			grid_b 
		]
	});
  catalogoEPOData.load();
});