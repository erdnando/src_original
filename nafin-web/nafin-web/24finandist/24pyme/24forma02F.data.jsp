<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.seguridadbean.*,
	com.netro.model.catalogos.*,
	com.netro.model.catalogos.CatalogoEPODistribuidores,	
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String clave_pyme = (String)request.getSession().getAttribute("iNoCliente");
String ic_epo 		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String ic_moneda 	= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
String cc_acuse 	= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
String fn_monto_de 	= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
String fn_monto_a 	= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
String df_fecha_emision_de 	= (request.getParameter("df_fecha_emision_de")==null)?"":request.getParameter("df_fecha_emision_de");
String df_fecha_emision_a 	= (request.getParameter("df_fecha_emision_a")==null)?"":request.getParameter("df_fecha_emision_a");
String ig_numero_docto 	= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
String df_fecha_venc_de 	= (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
String df_fecha_venc_a 	= (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
String fecha_publicacion_de 	= (request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
String fecha_publicacion_a 	= (request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
String nePyme 		= (String)session.getAttribute("strNePymeAsigna");
String nombrePyme	= (String)session.getAttribute("strNombrePymeAsigna");
String lineaTodos 	= (request.getParameter("lineaTodos")==null)?"":request.getParameter("lineaTodos");
boolean regValido = true;
String infoRegresar = "",  hidNumTasas ="",  hidNumLineas =  "", ses_ic_pyme = iNoCliente,  fechaHoy= "",  horaCarga="", consulta ="",
icMoneda = "", parTipoCambio = "", 	diasInhabiles =	"", ic_if	=	"",nombreEpo	=	"", igNumeroDocto	=	"", ccAcuse =	"", dfFechaEmision =	"",
dfFechaVencimiento	=	"",dfFechaPublicacion 	= 	"", igPlazoDocto	=	"",  moneda =	"", cgTipoConv	=	"",  tipoCambio	=   "", 
igPlazoDescuento	= "",  fnPorcDescuento	= "",  estatus =	"",  cambios	= 	"",numeroCredito		= 	"",  nombreIf	= 	"", 
tipoLinea	=	"", fechaOperacion =	"", referencia	=	"", plazoCredito	=	"", fechaVencCredito =	"", relMat	=	"", tipoCobroInt	=	"",  tipoPiso=	"",
icTipoCobroInt		=	"",  monedaLinea	=	"", nombreMLinea	=	"", dias_minimo	= 	"",dias_maximo	=	"", icLineaCredito	= 	"", 
plazo_valido	=	"",  epo ="", inhabilEpoPyme = "", plazofinanciamiento = "",interes ="", credito = "", diahabil ="",  fechaVencimiento = "", plazopyme =  "",  
plazoepo = "", plazonafin  ="", plazo = "", cuentaBancaria ="", auxIcIf = "", auxReferenciaTasa	= "", auxIcTasa	="", auxCgRelMat ="", auxFnPuntos	="", 	
auxValorTasa ="", auxIcMoneda	="", auxPlazoDias	="";
			
double fnMonto 	=0,  montoValuado =	0, montoDescuento =	0, montoCredito	=	0,  tasaInteres	= 	0, valorTasaInt	=	0, 
montoTasaInt		=	0, fnPuntos		= 	0, 	totalMontoMN		=	0, 	totalMontoUSD		=	0, 	totalMontoDescMN	=	0,
totalMontoDescUSD	=	0, 	totalInteresMN		=	0, 	totalInteresUSD		=	0, 	montoValuadoLinea	=	0, 	totalMontoValuadoMN	=	0,
totalMontoValuadoUSD=	0, totalMontoCreditoMN	=	0, totalMontoCreditoUSD=	0, totalDoctosConv =0, 	totalMontosConv	=0,
totalMontoDescConv 	=0, totalInteresConv	=0, 	totalConvPesos	=0, totalMontoCreditoConv	=0;

int		totalCreditosMN		= 	0, 	totalCreditosUSD	=	0,   plazo_valido1 = 0,  menosdias =0, no_dia_semana = 0, 
plazoactual =0, 	totalDoctosMN		=	0, 	totalDoctosUSD =	0, 	elementos = 0, numTasas = 0;
		
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	horaCarga = new java.text.SimpleDateFormat("HH:mm").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
	horaCarga = "";
}

JSONObject jsonObj = new JSONObject();
Vector	vecColumnas	= null;
HashMap registrosTot = new HashMap();	
JSONArray registrosTotales = new JSONArray();
HashMap	datos = new HashMap();	
JSONArray registros = new JSONArray();
			
//INSTANCIACION DE EJB'S
AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);

CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

com.netro.seguridadbean.Seguridad seguridadEJB = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);

//******	Inicializacion de Catalogo 
if (informacion.equals("CatalogoEPO") ) {		
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClavePyme(clave_pyme);
	cat.setValoresCondicionIn (ic_epo, Integer.class);
	infoRegresar = cat.getJSONElementos();	
		
} else  if (informacion.equals("CatalogoMoneda") ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
 
} else  if (informacion.equals("CatalogoLineaTodos") ) {
	
	registros = new JSONArray();
	datos = new HashMap();	
	List combo = aceptPyme.getLineasCCC(ses_ic_pyme) ;	
	for(int i=0; i<combo.size(); i++){
		List dato = (List)combo.get(i);
		String clave = dato.get(0).toString(); 
		String descripcion = dato.get(1).toString(); 	
		datos = new HashMap();
		datos.put("clave", clave);
		datos.put("descripcion", descripcion);	
		registros.add(datos);
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else  if (informacion.equals("SeleccionEPO") ) {
	
	datos = new HashMap();	
	vecColumnas =null;
	int   i=0;
	// OBTENEMOS LAS TASAS POR EPO E IF Y LAS METEMOS EN HIDDENS AUXILIARES
	//le pongo "C" porque las condiciones serian las mismas para tipo Factoraje con Recurso
	Vector vecFilas2 = BeanTasas.esquemaParticular(ic_epo,4,"",iNoCliente,"C"); // obtiene las EPO e IF que tienen una LC vigente	
	for (int j=0; j<vecFilas2.size(); j++) {
		Vector lovDatosTas = (Vector)vecFilas2.get(j);
		boolean esTasaGeneral = false;
		ic_if = lovDatosTas.get(0).toString();
		int totalTasasMN = 0;
		int totalTasasUSD = 0;
		String in = "";		
		Vector vecFilas1 = BeanTasas.ovgetTasasxPyme(4,iNoCliente,ic_if,"1, 54");		
		for( i=0;i<vecFilas1.size();i++){
			vecColumnas = (Vector)vecFilas1.get(i);	
			auxIcIf = ic_if;
			auxReferenciaTasa	= (String)vecColumnas.get(1)+" "+(String)vecColumnas.get(5)+" "+(String)vecColumnas.get(6);
			auxIcTasa			= (String)vecColumnas.get(2);
			auxCgRelMat		=(String)vecColumnas.get(5);
			auxFnPuntos		=(String)vecColumnas.get(6);
			auxValorTasa	=(String)vecColumnas.get(4);
			auxIcMoneda		=(String)vecColumnas.get(9);
			auxPlazoDias	=(String)vecColumnas.get(10);				
			if("1".equals(vecColumnas.get(9).toString())){
				totalTasasMN ++;
			}	else if("54".equals(vecColumnas.get(9).toString())){
				totalTasasUSD ++;
			}
			numTasas++;						
			datos.put("auxIcIf"+i,auxIcIf);
			datos.put("auxReferenciaTasa"+i,auxReferenciaTasa);
			datos.put("auxIcTasa"+i,auxIcTasa);
			datos.put("auxCgRelMat"+i,auxCgRelMat);
			datos.put("auxFnPuntos"+i,auxFnPuntos);
			datos.put("auxValorTasa"+i,auxValorTasa);
			datos.put("auxIcMoneda"+i,auxIcMoneda);
			datos.put("auxPlazoDias"+i,auxPlazoDias);				
		}	
		
		int conteo =vecFilas1.size()-1;		
		if(totalTasasMN==0||totalTasasUSD==0){
			if(totalTasasMN==0)
				in = "1";
			if(totalTasasUSD==0){
				if(!"".equals(in))
					in += ",";
				in += "54";
			}				
			vecFilas1 = BeanTasas.ovgetTasas(4,in);			
			esTasaGeneral = true;
		}else{
			vecFilas1 = new Vector();
		}	
			
		for( i=0;i<vecFilas1.size();i++){
			vecColumnas = (Vector)vecFilas1.get(i);
			auxIcIf = ic_if;
			auxReferenciaTasa =(String)vecColumnas.get(1)+" "+(String)vecColumnas.get(5)+" "+(String)vecColumnas.get(6);
			auxIcTasa =(String)vecColumnas.get(2);
			auxCgRelMat = (String)vecColumnas.get(5);
			auxFnPuntos	=(String)vecColumnas.get(6);
			auxValorTasa =(String)vecColumnas.get(4);
			auxIcMoneda  =(String)vecColumnas.get(10);
			auxPlazoDias =(String)vecColumnas.get(11);
			numTasas++;
			conteo++;
			datos.put("auxIcIf"+conteo,auxIcIf);
			datos.put("auxReferenciaTasa"+conteo,auxReferenciaTasa);
			datos.put("auxIcTasa"+conteo,auxIcTasa);
			datos.put("auxCgRelMat"+conteo,auxCgRelMat);
			datos.put("auxFnPuntos"+conteo,auxFnPuntos);
			datos.put("auxValorTasa"+conteo,auxValorTasa);
			datos.put("auxIcMoneda"+conteo,auxIcMoneda);
			datos.put("auxPlazoDias"+conteo,auxPlazoDias);			
		}			
	}//for
	hidNumTasas = String.valueOf(numTasas);  //numero de tasas
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("Auxiliar", datos);
	jsonObj.put("hidNumTasas", hidNumTasas);
	infoRegresar = jsonObj.toString();
	
	
} else  if (informacion.equals("ConsultaMonitor") ) {
	
	datos = new HashMap();	
	registros = new JSONArray();	
	vecColumnas =null;
	//esta parte es para formar el Monitor 
	 Vector vecFilas  = aceptPyme.monitorLineas(ses_ic_pyme, ic_if,  ic_moneda, clave_pyme );
	
	for(int i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector)vecFilas.get(i);
		String nombreIF =(String)vecColumnas.get(1);  //Línea de Crédito IF
		String ic_linea =(String)vecColumnas.get(0);
		String moneda_linea = (String)vecColumnas.get(4);		
		String saldo_inicial = vecColumnas.get(2).toString(); //Saldo inicial
		String montoSeleccionado ="0.00";//Monto seleccionado
		String numDoctos  ="0";//Num. doctos. seleccionados
		String saldoDisponible =vecColumnas.get(3).toString();
		String hidSaldoDisponible = vecColumnas.get(3).toString();		
		String seleccionado ="N";
		if(lineaTodos.equals(ic_linea)){
			seleccionado = "S";
		}		
		datos = new HashMap();
		datos.put("LINEA_CREDITO_IF",nombreIF);
		datos.put("IC_LINEA",ic_linea);
		datos.put("MONEDA_LINEA",moneda_linea);
		datos.put("SALDO_INICIAL",saldo_inicial);
		datos.put("MONTOSELECCIONADO",montoSeleccionado);
		datos.put("NUMDOCTOS",numDoctos);
		datos.put("SALDODISPONIBLE",saldoDisponible);
		datos.put("HIDSALDODISPONIBLE",saldoDisponible);
		datos.put("SELECCIONADO",seleccionado); //Para habilitar el registro en el monitor 			
		registros.add(datos);
	}

	hidNumLineas   = String.valueOf(vecFilas.size());  //numero de lineas
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";	
	JSONObject 	resultado	= new JSONObject();
	resultado = JSONObject.fromObject(consulta);	
	resultado.put("hidNumLineas",hidNumLineas);	
	resultado.put("ic_epo",ic_epo);
	infoRegresar = resultado.toString();			

} else  if (informacion.equals("Consulta") ||  informacion.equals("ResumenTotales") ) {

	//se obtiene  los registro de la consulta
	Vector vecFilas = aceptPyme.consultaDoctosFactRecurso(ses_ic_pyme,ic_epo,ic_moneda,ig_numero_docto,fn_monto_de,fn_monto_a, cc_acuse,df_fecha_emision_de,df_fecha_emision_a,df_fecha_venc_de,df_fecha_venc_a,fecha_publicacion_de,fecha_publicacion_a,lineaTodos,null);

	if(vecFilas.size()>0){
		for(int i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);
			nombreEpo			=	(String)vecColumnas.get(0);
			igNumeroDocto		=	(String)vecColumnas.get(1);
			ccAcuse 			=	(String)vecColumnas.get(2);
			dfFechaEmision		=	(String)vecColumnas.get(3);
			dfFechaVencimiento	=	(String)vecColumnas.get(4);
			dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
			igPlazoDocto		=	(String)vecColumnas.get(6);
			moneda 				=	(String)vecColumnas.get(7);
			fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			cgTipoConv			=	(String)vecColumnas.get(9);
			tipoCambio			=   (String)vecColumnas.get(10);
			montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			igPlazoDescuento	= 	(String)vecColumnas.get(12);
			fnPorcDescuento		= 	(String)vecColumnas.get(13);
			estatus 			=	(String)vecColumnas.get(15);
			cambios				= 	(String)vecColumnas.get(16);
			//creditos por concepto de intereses
			numeroCredito		= 	(String)vecColumnas.get(17);
			nombreIf			= 	(String)vecColumnas.get(18);
			tipoLinea			=	(String)vecColumnas.get(19);
			fechaOperacion		=	(String)vecColumnas.get(20);
			plazoCredito		=	(String)vecColumnas.get(22);
			fechaVencCredito	=	(String)vecColumnas.get(23);
			relMat				= 	(String)vecColumnas.get(25);
			tipoCobroInt		=	(String)vecColumnas.get(27);
			referencia			=	(String)vecColumnas.get(29);
			tipoPiso			= 	(String)vecColumnas.get(30);
			icTipoCobroInt		= 	(String)vecColumnas.get(31);
			icMoneda			= 	(String)vecColumnas.get(32);
			monedaLinea			= 	(String)vecColumnas.get(35);
			dias_minimo			= 	(String)vecColumnas.get(36);
			dias_maximo			= 	(String)vecColumnas.get(37);
			icLineaCredito		= 	(String)vecColumnas.get(38);
			ic_if				= 	(String)vecColumnas.get(39);
			nombreMLinea		=  	(String)vecColumnas.get(40);		
 			plazo_valido =	(String)vecColumnas.get(42)==null?"0":(String)vecColumnas.get(42); 
			fechaVencimiento =	(String)vecColumnas.get(47)==null?"":(String)vecColumnas.get(47); 
			epo = (String)vecColumnas.get(48)==null?"":(String)vecColumnas.get(48); 
			plazopyme = (String)vecColumnas.get(49)==null?"0":(String)vecColumnas.get(49); 
			plazoepo = (String)vecColumnas.get(50)==null?"0":(String)vecColumnas.get(50);
			plazonafin = (String)vecColumnas.get(51)==null?"0":(String)vecColumnas.get(51); 
			cuentaBancaria  = (String)vecColumnas.get(52)==null?"0":(String)vecColumnas.get(52);
			plazo_valido = plazopyme;
			if (plazopyme.equals("0") && ( plazo_valido.equals("0") || plazo_valido.equals("")) ){
				plazo_valido = plazoepo;        
			}
			if (plazoepo.equals("0") && ( plazo_valido.equals("0") || plazo_valido.equals("") )){
				plazo_valido = plazonafin;
			}
			if(icMoneda.equals(monedaLinea)){
				cgTipoConv		= "";
				tipoCambio		= "";
				montoValuado	= fnMonto;
			}			
			if("+".equals(relMat))
				valorTasaInt += fnPuntos;
			else if("-".equals(relMat))
				valorTasaInt -= fnPuntos;
			else if("*".equals(relMat))
				valorTasaInt *= fnPuntos;
			else if("/".equals(relMat))
				valorTasaInt /= fnPuntos;

			montoValuadoLinea = fnMonto;				
			plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
			montoCredito = fnMonto-montoDescuento;
				
			if("1".equals(tipoPiso)){
				montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
			}else{
				montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
			}
			plazofinanciamiento =(String)vecColumnas.get(44);
			interes =(String)vecColumnas.get(45);
			credito = (String)vecColumnas.get(46);
			plazoactual = Integer.parseInt(plazoCredito); 
			diahabil = fechaVencCredito;
 	
			datos = new HashMap();
			datos.put("NOMBRE_EPO",nombreEpo);
			datos.put("NO_DOCUMENTO",igNumeroDocto);
			datos.put("NO_ACUSE",ccAcuse);	
			datos.put("FECHA_EMISION",dfFechaEmision);	
			datos.put("FECHA_VENCIMIENTO",dfFechaVencimiento);	
			datos.put("FECHA_PUBLICACION",dfFechaPublicacion);	
			datos.put("PLAZO_DOCTO",igPlazoDocto);				
			datos.put("MONEDA",moneda);	
			datos.put("IC_MONEDA_DOCTO",icMoneda);	
			datos.put("IC_MONEDA_LINEA",monedaLinea);	
			datos.put("IC_IF",ic_if);	
			datos.put("TIPO_PISO",tipoPiso);	
			datos.put("MONTO",Double.toString (fnMonto));	
			datos.put("PLAZO_DESCUENTO",igPlazoDescuento);	
			datos.put("PORCENTAJE_DESCUENTO",fnPorcDescuento);
			datos.put("MONTO_DESCUENTO",Double.toString (montoDescuento) );	
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
					datos.put("TIPO_CONV",cgTipoConv);
					datos.put("TIPO_CAMBIO",tipoCambio);
					datos.put("MONTO_VALUADO",Double.toString (montoValuado));		
			}else{	
					datos.put("TIPO_CONV","");
					datos.put("TIPO_CAMBIO","");
					datos.put("MONTO_VALUADO","0");	
			}		
			datos.put("NO_DOCTO_FINAL",numeroCredito);
			datos.put("MONEDA_FINAL",nombreMLinea);
			datos.put("MONTO_FINAL",Double.toString (montoCredito));
			datos.put("MONTO_CREDITO",Double.toString (((double)Math.round(montoCredito*100)/100)) );
			datos.put("PLAZO",plazo );
			datos.put("PLAZO_VALIDO",plazo_valido);
			datos.put("PLAZO_FINAL",String.valueOf(plazoactual));
			datos.put("FECHA_VENC_FINAL",diahabil );	
			datos.put("PLAZO_PRONTO_PAGO",plazo );	
			datos.put("FECHA_OPERA_FINAL",fechaHoy ); 	
			datos.put("NOMBRE_IF",nombreIf ); 	
			datos.put("IC_LINEA_CREDITO",icLineaCredito ); 	
			datos.put("REFERENCIA","" ); 	
			datos.put("IC_TASA","" ); 
			datos.put("CG_REL_MAT","" ); 
			datos.put("FN_PUNTOS","" ); 
			datos.put("VALOR_TASA","" ); 
			datos.put("VALOR_TASA_PUNTOS",("S".equals(icTipoCobroInt))?"0":"0"+valorTasaInt ); 
			datos.put("MONTO_TASA_INTERES",("S".equals(icTipoCobroInt))?"":""+((double)Math.round(montoTasaInt*Double.parseDouble(plazoCredito)*100)/100)); 
			datos.put("MONTO_AUX_INTERES",Double.toString (montoTasaInt) ); 
			datos.put("MONTO_TOTAL_CAPITAL",""); 
			datos.put("PLAZO_UNICO",plazo_valido); 
			datos.put("SELECCION","N"); 
			datos.put("DIAS_MINIMO",dias_minimo); 
			datos.put("DIAS_MAXIMO",dias_maximo); 	
			datos.put("CUENTA_BANCARIA",cuentaBancaria); 	
			registros.add(datos);	
			elementos++;
			//para los totales 
			if("1".equals(icMoneda)){
				totalDoctosMN++;
				totalMontoMN += fnMonto;
				totalMontoDescMN += montoDescuento;
				totalInteresMN += valorTasaInt;
			}else{
				totalDoctosUSD++;
				totalMontoUSD += fnMonto;
				totalMontoDescUSD += montoDescuento;
				totalInteresUSD += valorTasaInt;
				if("54".equals(icMoneda)&&"54".equals(monedaLinea)&&!"".equals(cgTipoConv)){
					totalMontoValuadoUSD += montoValuado;
				}
			}
			if("1".equals(monedaLinea) &&"1".equals(icMoneda)){
				totalCreditosMN++;
				totalMontoCreditoMN += montoCredito;
			}else if("54".equals(monedaLinea)&&"54".equals(icMoneda)){
				totalCreditosUSD++;
				totalMontoCreditoUSD += montoCredito;
			}
	
		}	//for
	 
		for(int t =0; t<2; t++) {		
			registrosTot = new HashMap();		
				if(t==0){ 				
				registrosTot.put("MONEDA", "MONEDA NACIONAL");
				registrosTot.put("ICMONEDA", "1");
				registrosTot.put("TOTAL", Double.toString(totalDoctosMN) );	
				registrosTot.put("MONTO", Double.toString(totalMontoMN));	
				registrosTot.put("TOTAL_MONTO_DESC", Double.toString(totalMontoDescMN)  );	
				registrosTot.put("TOTAL_MONTO_VALUADO", Double.toString(totalMontoValuadoMN)  );
				//FINALES
				registrosTot.put("TOTAL_CREDITOS", Double.toString(totalCreditosMN)  );
				registrosTot.put("TOTAL_MONTO_CREDITOS", Double.toString(totalMontoCreditoMN)  );			
				registrosTot.put("MONTOINTERES", "0.0" );	
				registrosTot.put("MONTOCAPITALINTERES", "0.0" );
				registrosTot.put("MONTO_TOTAL_RECIBIR", "0.0" );
				
			}
			if(t==1){ 			
				registrosTot.put("MONEDA", "MONEDA USD");
				registrosTot.put("ICMONEDA", "54");
				registrosTot.put("TOTAL", Double.toString(totalDoctosUSD)  );	
				registrosTot.put("MONTO", Double.toString(totalMontoUSD)  );	
				registrosTot.put("TOTAL_MONTO_DESC", Double.toString(totalMontoDescUSD)  );	
				registrosTot.put("TOTAL_MONTO_VALUADO", Double.toString(totalMontoValuadoUSD)  );
				//FINALES
				registrosTot.put("TOTAL_CREDITOS", Double.toString(totalCreditosUSD)  );
				registrosTot.put("TOTAL_MONTO_CREDITOS", Double.toString(totalMontoCreditoUSD)  );
				registrosTot.put("MONTOINTERES", "0.0" );	
				registrosTot.put("MONTOCAPITALINTERES", "0.0" );
				registrosTot.put("MONTO_TOTAL_RECIBIR", "0.0" );
			}
			registrosTotales.add(registrosTot);
		}	
	}
	
	if (informacion.equals("Consulta") ) {
	
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";	
		jsonObj	= new JSONObject();
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("ic_epo",ic_epo);	
		jsonObj.put("ElEMENTOS", String.valueOf(elementos));		
		infoRegresar = jsonObj.toString();	
	}
	
	if (informacion.equals("ResumenTotales") ) {	
		 infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registrosT\": " + registrosTotales.toString()+"}";
	}	
		
}else  if (informacion.equals("ConfirmacionClaves") ) {

	String cesionUser 	= (request.getParameter("cesionUser")==null)?"":request.getParameter("cesionUser");
	String cesionPassword 	= (request.getParameter("cesionPassword")==null)?"":request.getParameter("cesionPassword");
	boolean		bConfirma = false;	
		
	if(!cesionUser.equals("") && !cesionPassword.equals("")) {
		try{
			//seguridadEJB.validarUsuario(iNoUsuario, cesionUser , cesionPassword);
			bConfirma = true;
		}catch(Exception e){
			bConfirma = false;
		}		
	}
	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("bConfirma", new Boolean(bConfirma));	
	infoRegresar = jsonObj.toString();	
			
}else  if (informacion.equals("GeneraAcuse") ) {

	String 		_acuse = "500";
	String ic_documento[] = request.getParameterValues("ic_documento");
	String monto[] = request.getParameterValues("monto");
	String ic_moneda_docto[] = request.getParameterValues("ic_moneda_docto");
	String monto_descuento[] = request.getParameterValues("monto_descuento");	
	String monto_tasa_int[] = request.getParameterValues("monto_tasa_int");
	String monto_credito[] = request.getParameterValues("monto_credito");
	String ic_moneda_linea[] = request.getParameterValues("ic_moneda_linea");
	String ic_tasa[] = request.getParameterValues("ic_tasa");
	String cg_rel_mat[] = request.getParameterValues("cg_rel_mat");
	String fn_puntos[] = request.getParameterValues("fn_puntos");
	String fn_valor_tasa[] = request.getParameterValues("valor_tasa_puntos");
	String plazo_credito[] = request.getParameterValues("plazo");
	String fecha_vto[] = request.getParameterValues("fecha_vto");
	String ic_linea_credito[] = request.getParameterValues("ic_linea_credito");	
	
	System.out.println("ic_documentos---"+ic_documento[0]);
	System.out.println("ic_moneda_docto---"+ic_moneda_docto[0]);
	System.out.println("ic_monedas_linea---"+ic_moneda_linea[0]);
	System.out.println("montos---"+monto[0]);	
	System.out.println("monto_credito---"+monto_credito[0]);	
	System.out.println("plazos_credito---"+plazo_credito[0]);
	System.out.println("fechas_vto---"+fecha_vto[0]);	
	System.out.println("ic_tasa---"+ic_tasa[0]);
	System.out.println("cg_rel_mat---"+cg_rel_mat[0]);
	System.out.println("fn_puntos---"+fn_puntos[0]);
	System.out.println("ic_linea_credito---"+ic_linea_credito);
	

	for(int i=0;i<ic_documento.length;i++){
		if("1".equals(ic_moneda_docto[i])){
			totalDoctosMN ++;
			totalMontoMN		+= Double.parseDouble(monto[i]);
			totalMontoDescMN	+= Double.parseDouble(monto_descuento[i]);
			totalInteresMN		+= Double.parseDouble("".equals(monto_tasa_int[i])?"0":monto_tasa_int[i]);				
			totalMontoCreditoMN+= Double.parseDouble(monto_credito[i]);
		}
		if("54".equals(ic_moneda_docto[i])&&"54".equals(ic_moneda_linea[i])){
			totalDoctosUSD ++;
			totalMontoUSD		+= Double.parseDouble(monto[i]);
			totalMontoDescUSD	+= Double.parseDouble(monto_descuento[i]);
			totalInteresUSD		+= Double.parseDouble("".equals(monto_tasa_int[i])?"0":monto_tasa_int[i]);
			totalMontoCreditoUSD+= Double.parseDouble(monto_credito[i]);
		}
		if("54".equals(ic_moneda_docto[i])&&"1".equals(ic_moneda_linea[i])){
			totalDoctosConv ++;
			totalMontosConv			+= Double.parseDouble(monto[i]);
			totalMontoDescConv 		+= Double.parseDouble(monto_descuento[i]);
			totalInteresConv		+= Double.parseDouble("".equals(monto_tasa_int[i])?"0":monto_tasa_int[i]);
			totalConvPesos			+= Double.parseDouble(monto_descuento[i]);
			totalMontoCreditoConv	+= Double.parseDouble(monto_credito[i]);
		}			
	} //for
	
	String	mensajeAuto ="", acuse ="";
	acuse = aceptPyme.confirmaDoctoFactoRec(
										Double.toString (totalMontoMN), Double.toString (totalInteresMN), Double.toString (totalMontoCreditoMN),
										Double.toString (totalMontoUSD), Double.toString (totalInteresUSD),	Double.toString (totalMontoCreditoUSD),
										iNoUsuario,_acuse, ic_documento,ic_tasa,cg_rel_mat,fn_puntos,fn_valor_tasa,monto_tasa_int,monto_credito,
										plazo_credito,fecha_vto,ic_linea_credito);											
	if(acuse.equals("")){
		mensajeAuto =" <b>La autentificación no se llevo acabo con éxito.</b>";
	}
	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("acuse",acuse);
	jsonObj.put("mensajeAuto",mensajeAuto);
	jsonObj.put("fecCarga",fechaHoy);	
	jsonObj.put("horaCarga",horaCarga);	
	jsonObj.put("captUser",iNoUsuario+" "+strNombreUsuario);	
	infoRegresar = jsonObj.toString();	

}
%>
<%=infoRegresar%>
