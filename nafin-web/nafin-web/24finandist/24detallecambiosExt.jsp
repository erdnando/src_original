<%@ page import="
java.sql.*,
java.math.*,
java.io.*,
netropology.utilerias.*,
java.text.*,
java.util.*,
com.netro.exception.*"%>
<%@ include file="24secsession.jspf"%>
<%
try {
  String ic_doc = request.getParameter("ic_documento");
  String		qrySentencia 	= "";
	String dc_fecha_cambio 	= "";
	String ct_cambio_motivo 	= "";
	String df_fecha_emision_anterior 	= "";
	String df_fecha_emision_nueva 	= "";
	String fn_monto_anterior 	= "";
	String fn_monto_nuevo 	= "";
	String df_fecha_venc_anterior 	= "";
	String df_fecha_venc_nueva 	= "";
	String estatus 	= "";	
	String modo_plazo_ant = "";
	String modo_plazo_nuevo = "";	
  AccesoDB	con  = null;
  ResultSet	rs 		= null;
  boolean flag=true;
	//variables locales
	Vector		vecFilas 		= null;
	Vector		vecColumnas 	= null;  
%>

<table align="center" width="620" cellpadding="1" cellspacing="2" border="0">
<tr>
	<td class="formas">&nbsp;</td>
</tr>
<tr>
	<td align="center">
	<table align="center" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
<%	
   	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
          "SELECT distinct TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fech_cambio, "+
          "       TO_CHAR (ce.df_fecha_emision_anterior, 'dd/mm/yyyy') AS fech_emi_ant, "+
          "       ce.ct_cambio_motivo, ce.fn_monto_anterior, ce.fn_monto_nuevo, "+
          "       TO_CHAR (ce.df_fecha_emision_nueva, 'dd/mm/yyyy') AS fech_emi_new, "+
          "       TO_CHAR (ce.df_fecha_venc_anterior, 'dd/mm/yyyy') AS fech_venc_ant, "+
          "       TO_CHAR (ce.df_fecha_venc_nueva, 'dd/mm/yyyy') AS fech_venc_new, "+
          "       cce.cd_descripcion "+
					"		  ,TFA.cd_descripcion as modo_plazo_anterior"+
					"		  ,TFN.cd_descripcion as modo_plazo_nuevo"+
							"  FROM dis_cambio_estatus ce, comcat_cambio_estatus cce "+
					" 	,comcat_tipo_financiamiento TFA,comcat_tipo_financiamiento TFN"+
							" WHERE ic_documento = "+ic_doc+" "+
							"   AND cce.ic_cambio_estatus = ce.ic_cambio_estatus "+
					"	  AND ce.ic_tipo_finan_ant = TFA.ic_tipo_financiamiento(+)"+
					"	  AND ce.ic_tipo_finan_nuevo = TFN.ic_tipo_financiamiento(+)";
						System.out.println(qrySentencia);
						rs = con.queryDB(qrySentencia);
						while(rs.next()) {
					if (flag){flag=false;%>
	<tr>
		<td class="celda01" colspan="23" align="center"><span class="titulos">Documento</span></td>
	</tr>
	<tr>
		<td class="celda01" align="center">Fecha del Cambio</td>
		<td class="celda01" align="center">Cambio Estatus</td>			
		<td class="celda01" align="center">Fecha Emision Anterior</td>
		<td class="celda01" align="center">Fecha Emision Nueva</td>
		<td class="celda01" align="center">Monto Anterior</td>
		<td class="celda01" align="center">Monto Nuevo</td>
		<td class="celda01" align="center">Fecha Vencimiento Anterior</td>
		<td class="celda01" align="center">Fecha Vencimiento Nuevo</td>
		<td class="celda01" align="center">Modalidad de Plazo Anterior</td>
		<td class="celda01" align="center">Modalidad de Plazo Nuevo</td>		
	</tr>			
<%	}
			dc_fecha_cambio				= rs.getString("fech_cambio")==null?"":rs.getString("fech_cambio");
			ct_cambio_motivo			= rs.getString("ct_cambio_motivo")==null?"":rs.getString("ct_cambio_motivo");
			df_fecha_emision_anterior	= rs.getString("fech_emi_ant")==null?"":rs.getString("fech_emi_ant");
			df_fecha_emision_nueva		= rs.getString("fech_emi_new")==null?"":rs.getString("fech_emi_new");
			fn_monto_anterior			= rs.getString("fn_monto_anterior")==null?"":rs.getString("fn_monto_anterior");
			fn_monto_nuevo				= rs.getString("fn_monto_nuevo")==null?"":rs.getString("fn_monto_nuevo");
			df_fecha_venc_anterior		= rs.getString("fech_venc_ant")==null?"":rs.getString("fech_venc_ant");
			df_fecha_venc_nueva			= rs.getString("fech_venc_new")==null?"":rs.getString("fech_venc_new");
			estatus 					= rs.getString("cd_descripcion")==null?"":rs.getString("cd_descripcion");
			modo_plazo_ant				= rs.getString("modo_plazo_anterior")==null?"":rs.getString("modo_plazo_anterior");
			modo_plazo_nuevo			= rs.getString("modo_plazo_nuevo")==null?"":rs.getString("modo_plazo_nuevo");
%>
	<tr>
		<td class="formas">&nbsp;<%=dc_fecha_cambio%></td>
		<td class="formas">&nbsp;<%=estatus%></td>				
		<td class="formas">&nbsp;<%=df_fecha_emision_anterior%></td>
		<td class="formas">&nbsp;<%=df_fecha_emision_nueva%></td>
		<td class="formas">&nbsp;<%=("".equals(fn_monto_anterior))?"":"$&nbsp;"+Comunes.formatoDecimal(fn_monto_anterior,2)%></td>
		<td class="formas">&nbsp;<%=("".equals(fn_monto_nuevo))?"":"$&nbsp;"+Comunes.formatoDecimal(fn_monto_nuevo,2)%></td>
		<td class="formas">&nbsp;<%=df_fecha_venc_anterior%></td>
		<td class="formas">&nbsp;<%=df_fecha_venc_nueva%></td>
		<td class="formas">&nbsp;<%=modo_plazo_ant%></td>
		<td class="formas">&nbsp;<%=modo_plazo_nuevo%></td>		
	</tr>

<%}
con.cierraStatement();
} catch(Exception e){
	throw new NafinException("SIST0001");
} finally {
	if(con.hayConexionAbierta())
  con.cierraConexionDB();
}%>
	</table>
	</td>
</tr>
<%
if (flag){%>
		<table align="center" width="610" border="1" cellspacing="0" cellpadding="2" bordercolor="#A5B8BF">
			<tr> 
				<td class="celda01" align="center" valign="top">No se encontr&oacute; ning&uacute;n cambio para este documento</td>
			</tr>
		</table>
<%}%>
<tr>
	<td class="formas">&nbsp;</td>
</tr>
</table>
<%
}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	out.println(e);
} %>

