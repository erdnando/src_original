<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,java.text.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB",Lineadm.class);
ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
String distribuidor = (request.getParameter("distribuidor")==null)?"":request.getParameter("distribuidor");
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar = "";

if (informacion.equals("valoresIniciales")){

	String NOnegociable =  BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_NO_NEGOCIABLES");
	String perfil = "";
	if(strPerfil.equals("EPO MAN0 DISTR") || strPerfil.equals("EPO MAN1 DISTR") || strPerfil.equals("EPO MAN2 DISTR")){
		perfil = "NO_ES_EPO";
	} else{
		perfil = "EPO";
	}

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success",      new Boolean(true));
	jsonObj.put("ses_ic_epo",   iNoCliente       );
	jsonObj.put("NOnegociable", NOnegociable     );
	jsonObj.put("perfil",       perfil           );
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("CatalogoDistribuidor")){

	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setClaveEpo(iNoCliente);
	cat.setCampoClave("cp.ic_pyme");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoEstatusDist")){

	String condicion = "";
	//System.err.println("strPerfil: " + strPerfil);
	if(strPerfil.equals("EPO MAN0 DISTR")){
		condicion ="30";
	} else if(strPerfil.equals("EPO MAN1 DISTR")){
		condicion ="28";
	} else if(strPerfil.equals("EPO MAN2 DISTR")){
		condicion ="2";
	} else{
		condicion ="1, 2";
	}

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_docto");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_docto");
	cat.setValoresCondicionIn(condicion, Integer.class);
	cat.setOrden("ic_estatus_docto");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CatalogoNuevoEstatusDist")){

	String nuevoEstatus = (request.getParameter("nuevoEstatus")!=null)?request.getParameter("nuevoEstatus"):"";
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_cambio_estatus");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_cambio_estatus");
	cat.setOrden("ic_cambio_estatus");
	cat.setValoresCondicionIn(nuevoEstatus, Integer.class);
	infoRegresar = cat.getJSONElementos();

} else if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoModoPlazoDist")){
	
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

	String valida	=	"";
	valida = cargaDocto.getValidaIn(iNoCliente,distribuidor);
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_financiamiento");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_tipo_financiamiento");
	cat.setValoresCondicionIn(valida, Integer.class);
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("obtenDetalles")){

	String ic_documento = (request.getParameter("ic_docto")==null)?"":request.getParameter("ic_docto");
	Registros regDatos = BeanParametros.getDetalleCambiosDoctos(ic_documento);
	
	JSONObject jsonObj = new JSONObject();

	if (regDatos != null){
		jsonObj.put("datos_detalle", regDatos.getJSONData() );
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Consulta")){

	JSONObject jsonObj	= new JSONObject();
	String fecha_hoy		= new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String operacion		= (request.getParameter("operacion")     == null)?"":request.getParameter("operacion");
	String moneda 			= (request.getParameter("moneda")        == null)?"":request.getParameter("moneda");
	String estatusdocto 	= (request.getParameter("estatusdocto")  == null)?"":request.getParameter("estatusdocto");
	String monto1			= (request.getParameter("monto1")        == null)?"":request.getParameter("monto1");
	String monto2			= (request.getParameter("monto2")        == null)?"":request.getParameter("monto2");
	String numdoc			= (request.getParameter("numdoc")        == null)?"":request.getParameter("numdoc");
	String cdescuento		= (request.getParameter("cdescuento")    == null)?"":request.getParameter("cdescuento");
	String numaccuse		= (request.getParameter("numaccuse")     == null)?"":request.getParameter("numaccuse");
	String fechaemision1	= (request.getParameter("fechaemision1") == null)?"":request.getParameter("fechaemision1");
	String fechaemision2	= (request.getParameter("fechaemision2") == null)?"":request.getParameter("fechaemision2");
	String fechavenc1		= (request.getParameter("fechavenc1")    == null)?"":request.getParameter("fechavenc1");
	String fechavenc2		= (request.getParameter("fechavenc2")    == null)?"":request.getParameter("fechavenc2");
	String fechapub1		= (request.getParameter("fechapub1")     == null)?fecha_hoy:request.getParameter("fechapub1");
	String fechapub2		= (request.getParameter("fechapub2")     == null)?fecha_hoy:request.getParameter("fechapub2");
	String modplazo		= (request.getParameter("modplazo")      == null)?"":request.getParameter("modplazo");
	String NOnegociable	=(request.getParameter("NOnegociable")   == null)?"":request.getParameter("NOnegociable");//Fodea 029-2010 Fase III

	Vector vecFilas            = null;
	Vector vecColumnas 	      = null;
	String ic_documento        = "";
	String cg_razon_social		= "";
	String ig_numero_docto		= "";
	String cc_acuse				= "";
	String fecha_emision			= "";
	String fecha_publicacion	= "";
	String df_fecha_venc			= "";
	String ig_plazo_docto		= "";
	String cd_nombre_moneda		= "";
	String fn_monto				= "";
	String conversion				= "";
	String fn_tipo_cambio		= "";
	String monto_valuado			= "";
	String ig_plazo_descuento	= "";
	String fn_porc_descuento	= "";
	String monto_con_desc		= "";
	String modalidad_plazo		= "";
	String ic_estatus_docto		= "";
	String rs_ic_mon				= "";
	String bandeVentaCartera	= "";
	String noEstatus				= "";	//pendiente...
	double dblMontoValuado		= 0;
	double dblFnMonto				= 0;
	double dblMontoDesc			= 0;
	int numRegistros				= 0;
	int numRegistrosMN			= 0;
	int numRegistrosDL			= 0;
	double montoTotalMN			= 0;
	double montoTotalDL			= 0;
	double montoValDL				= 0;
	double montoDescMN			= 0;
	double montoDescDL			= 0;
	boolean tipoConversion		= false;
	
	vecFilas = lineadm.getConsultaBajaDocs(iNoCliente,distribuidor,moneda,estatusdocto,monto1,monto2,numdoc,cdescuento,numaccuse,fechaemision1,fechaemision2,fechavenc1,fechavenc2,fechapub1,fechapub2,modplazo,NOnegociable);
	if (vecFilas.size()>0) {
		List NuevoReg = new ArrayList();
		for (int i=0;i<vecFilas.size();i++) {
			HashMap hash = new HashMap();
			vecColumnas = (Vector)vecFilas.get(i);
			ic_documento		= (String)vecColumnas.get(0);
			cg_razon_social	= (String)vecColumnas.get(1);
			ig_numero_docto	= (String)vecColumnas.get(2);
			cc_acuse				= (String)vecColumnas.get(3);
			fecha_emision		= (String)vecColumnas.get(4);
			fecha_publicacion	= (String)vecColumnas.get(5);
			df_fecha_venc		= (String)vecColumnas.get(6);
			ig_plazo_docto		= (String)vecColumnas.get(7);
			cd_nombre_moneda	= (String)vecColumnas.get(8);
			fn_monto				= (String)vecColumnas.get(9);
			conversion			= (String)vecColumnas.get(10);
			fn_tipo_cambio		= (String)vecColumnas.get(11);
			monto_valuado		= (String)vecColumnas.get(12);
			ig_plazo_descuento= (String)vecColumnas.get(13);
			fn_porc_descuento	= (String)vecColumnas.get(14);
			monto_con_desc		= (String)vecColumnas.get(15);
			modalidad_plazo	= (String)vecColumnas.get(16);
			ic_estatus_docto	= (String)vecColumnas.get(17);
			rs_ic_mon			= (String)vecColumnas.get(18);
			noEstatus			= (String)vecColumnas.get(19);

			numRegistros++;
			if ("1".equals(rs_ic_mon)){
				numRegistrosMN++;
				montoTotalMN 	+= "".equals((String)vecColumnas.get(9))?0:Double.parseDouble((String)vecColumnas.get(9));
				montoDescMN 	+= "".equals((String)vecColumnas.get(15))?0:Double.parseDouble((String)vecColumnas.get(15));
				monto_valuado = "&nbsp;";
				fn_tipo_cambio="";
			}
			else {//Dolares
				numRegistrosDL++;
				dblFnMonto		=  "".equals((String)vecColumnas.get(9))?0:Double.parseDouble((String)vecColumnas.get(9));
				montoTotalDL 	+= dblFnMonto;
				dblMontoDesc 	= "".equals((String)vecColumnas.get(15))?0:Double.parseDouble((String)vecColumnas.get(15));
				montoDescDL 	+= dblMontoDesc;
				dblMontoValuado = (dblFnMonto-dblMontoDesc)*Double.parseDouble(fn_tipo_cambio);
				montoValDL 		+= dblMontoValuado;
				fn_tipo_cambio = "$&nbsp;"+Comunes.formatoDecimal(fn_tipo_cambio,4);
				monto_valuado = "$&nbsp;"+Comunes.formatoDecimal(dblMontoValuado,2);
			}
			bandeVentaCartera			= (String)vecColumnas.get(20);
			if (bandeVentaCartera.equals("S") ) {
				modalidad_plazo ="";
			}
			if(!conversion.equals("")){
				tipoConversion = true;
			}

			hash.put("CLAVE",             ic_documento      );
			hash.put("PYME",              cg_razon_social   );
			hash.put("NUMDOC",            ig_numero_docto   );
			hash.put("ACUSE",             cc_acuse          );
			hash.put("FECHA_EMISION",     fecha_emision     );
			hash.put("FECHA_PUBLICACION", fecha_publicacion );
			hash.put("DF_FECHA_VENC",     df_fecha_venc     );
			hash.put("PLAZO_DOC",         ig_plazo_docto    );
			hash.put("MONEDA",            cd_nombre_moneda  );
			hash.put("MONTO",             fn_monto          );
			hash.put("CONVERSION",        conversion        );
			hash.put("TIPO_CAMBIO",       fn_tipo_cambio    );
			hash.put("MONTO_VALUADO",     monto_valuado     );
			hash.put("PLAZDESC",          ig_plazo_descuento);
			hash.put("PORCDESC",          fn_porc_descuento );
			hash.put("MONTO_CON_DESC",    monto_con_desc    );
			hash.put("MODALIDAD_PLAZO",   modalidad_plazo   );
			hash.put("ESTATUS",           ic_estatus_docto  );
			hash.put("IC_MONEDA",         rs_ic_mon         );
			hash.put("NOESTATUS",         noEstatus         );
			hash.put("CG_VENTACARTERA",   bandeVentaCartera );
			hash.put("FLAG_CHECK",        ""                );
			hash.put("FLAG_CAUSA",        ""                );
			NuevoReg.add(hash);
		}//End-for: VecFilas
		jsonObj.put("tipoConversion", new Boolean(tipoConversion));
		if (NuevoReg != null){
			JSONArray jsObjArray = new JSONArray();
			jsObjArray = JSONArray.fromObject(NuevoReg);
			jsonObj.put("registros", jsObjArray);
			List totalReg = new ArrayList();
			HashMap hash = new HashMap();
			if(numRegistrosMN>0){
				hash.put("NOMMONEDA",              "MONEDA NACIONAL"              );
				hash.put("TOTAL_REGISTROS",        Double.toString(numRegistrosMN));
				hash.put("TOTAL_MONTO_DOCUMENTOS", Double.toString(montoTotalMN)  );
				hash.put("TOTAL_MONTO_DESCUENTO",  Double.toString(montoDescMN)   );
				hash.put("TOTAL_MONTO_VALUADO",    "0"                            );
				totalReg.add(hash);
			}
			if(numRegistrosDL>0){
				hash = new HashMap();
				hash.put("NOMMONEDA",              "DOLARES AMERICANOS"           );
				hash.put("TOTAL_REGISTROS",        Double.toString(numRegistrosDL));
				hash.put("TOTAL_MONTO_DOCUMENTOS", Double.toString(montoTotalDL)  );
				hash.put("TOTAL_MONTO_DESCUENTO",  Double.toString(montoDescDL)   );
				hash.put("TOTAL_MONTO_VALUADO",    Double.toString(montoValDL)    );
				totalReg.add(hash);
			}
			if (totalReg != null){
				JSONArray jsObjArr = new JSONArray();
				jsObjArr = JSONArray.fromObject(totalReg);
				jsonObj.put("totales", jsObjArr);
			}
		}
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Captura")){

	JSONObject jsonObj    = new JSONObject();
	String ic_documento[] = request.getParameterValues("ic_documento");
	String selecc[]       = request.getParameterValues("selecc");
	String causa[]        = request.getParameterValues("causa");
	String nuevo_estatus  = request.getParameter("nuevo_estatus");
	lineadm.setDisDocumentoBaja(ic_documento,selecc,nuevo_estatus,causa);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>