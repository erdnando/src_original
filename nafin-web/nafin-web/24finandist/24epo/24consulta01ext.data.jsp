<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.model.catalogos.CatalogoIFDistribuidores,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
///////
String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

/*****/
String fechaHoy                  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()); 
String ic_epo                    = (request.getParameter("ic_epo")                    ==null)?"":request.getParameter("ic_epo");
String ic_pyme                   = (request.getParameter("ic_pyme")                   ==null)?"":request.getParameter("ic_pyme");
String ic_if                     = (request.getParameter("ic_if")                     ==null)?"":request.getParameter("ic_if");
String ic_estatus_docto          = (request.getParameter("ic_estatus_docto")          ==null)?"":request.getParameter("ic_estatus_docto");
String ig_numero_docto           = (request.getParameter("ig_numero_docto")           ==null)?"":request.getParameter("ig_numero_docto");
String fecha_seleccion_de        = (request.getParameter("fecha_seleccion_de")        ==null)?"":request.getParameter("fecha_seleccion_de");
String fecha_seleccion_a         = (request.getParameter("fecha_seleccion_a")         ==null)?"":request.getParameter("fecha_seleccion_a");
String cc_acuse                  = (request.getParameter("cc_acuse")                  ==null)?"":request.getParameter("cc_acuse");
String monto_credito_de          = (request.getParameter("monto_credito_de")          ==null)?"":request.getParameter("monto_credito_de");
String monto_credito_a           = (request.getParameter("monto_credito_a")           ==null)?"":request.getParameter("monto_credito_a");
String fecha_emision_de          = (request.getParameter("fecha_emision_de")          ==null)?"":request.getParameter("fecha_emision_de");
String fecha_emision_a           = (request.getParameter("fecha_emision_a")           ==null)?"":request.getParameter("fecha_emision_a");
String fecha_vto_de              = (request.getParameter("fecha_vto_de")              ==null)?"":request.getParameter("fecha_vto_de");
String fecha_vto_a               = (request.getParameter("fecha_vto_a")               ==null)?"":request.getParameter("fecha_vto_a");
String fecha_vto_credito_de      = (request.getParameter("fecha_vto_credito_de")      ==null)?"":request.getParameter("fecha_vto_credito_de");
String fecha_vto_credito_a       = (request.getParameter("fecha_vto_credito_a")       ==null)?"":request.getParameter("fecha_vto_credito_a");
String ic_tipo_cobro_int         = (request.getParameter("ic_tipo_cobro_int")         ==null)?"":request.getParameter("ic_tipo_cobro_int");
String ic_moneda                 = (request.getParameter("ic_moneda")                 ==null)?"":request.getParameter("ic_moneda");
String fn_monto_de               = (request.getParameter("fn_monto_de")               ==null)?"":request.getParameter("fn_monto_de");
String fn_monto_a                = (request.getParameter("fn_monto_a")                ==null)?"":request.getParameter("fn_monto_a");
String monto_con_descuento       = (request.getParameter("monto_con_descuento")       ==null)?"":"checked";
String solo_cambio               = (request.getParameter("solo_cambio")               ==null)?"":request.getParameter("solo_cambio");
String modo_plazo                = (request.getParameter("modo_plazo")                ==null)?"":request.getParameter("modo_plazo");
String tipo_credito              = (request.getParameter("tipo_credito")              ==null)?"":request.getParameter("tipo_credito");
String ic_documento              = (request.getParameter("ic_documento")              ==null)?"":request.getParameter("ic_documento");
String fecha_publicacion_de      = (request.getParameter("fecha_publicacion_de")      ==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
String fecha_publicacion_a       = (request.getParameter("fecha_publicacion_a")       ==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
String NOnegociable              = (request.getParameter("NOnegociable")              ==null)?"":request.getParameter("NOnegociable");
String tipo_pago                 = (request.getParameter("tipo_pago")                 ==null)?"":request.getParameter("tipo_pago");
String publicaDoctosFinanciables = (request.getParameter("publicaDoctosFinanciables") ==null)?"":request.getParameter("publicaDoctosFinanciables");
String datosGridTotales          = (request.getParameter("datosGridTotales")          ==null)?"":request.getParameter("datosGridTotales");
/*****/

CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

if (informacion.equals("valoresIniciales")) {

	tipo_credito	=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");

	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
	NOnegociable =  BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_NO_NEGOCIABLES");
	tipo_credito =  BeanParametros.obtieneTipoCredito (iNoCliente); //Fodea 029-2010 Distribuodores Fase III

	//Fodea 9-2015 Se supone que debo obtener si la EPO publica documentos financiables a meses sin intereses
	publicaDoctosFinanciables = "";
	publicaDoctosFinanciables = BeanParametros.DesAutomaticoEpo(iNoCliente,"CS_MESES_SIN_INTERESES");  

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("publicaDoctosFinanciables", publicaDoctosFinanciables); //F009-2015
	jsonObj.put("tipo_credito", tipo_credito);
	jsonObj.put("ses_ic_epo", iNoCliente);
	jsonObj.put("NOnegociable", NOnegociable);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenCambioDoctos")) {
	ic_documento = (request.getParameter("ic_docto")==null)?"":request.getParameter("ic_docto");

	if(ic_documento != null && !ic_documento.equals("")) {
		DetalleCambiosDoctos dataCambios = new DetalleCambiosDoctos();
		dataCambios.setIcDocumento(ic_documento);
		Registros registros = dataCambios.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}if (informacion.equals("CatalogoDistribuidor")){

	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setClaveEpo(iNoCliente);
	cat.setCampoClave("cp.ic_pyme");
	cat.setCampoDescripcion("cp.cg_razon_social");
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();
 
}else if (informacion.equals("CatalogoEstatusDist")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_docto");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_docto");
	cat.setValoresCondicionIn("1,2,3,4,5,9,11,20,22,24,32,28,30", Integer.class);//MOD +(24)//FODEA-013-2014 MOD(se agrego el estatus 32)
	cat.setOrden("ic_estatus_docto");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoTipoCobroDist")){

	String rs_epo_tipo_cobro_int	=	"1,2";
	if(!"".equals(iNoCliente))	{
		rs_epo_tipo_cobro_int = cargaDocto.getEpoTipoCobroInt(iNoCliente);
	}
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_cobro_interes");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_tipo_cobro_interes");
	cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
	//cat.setOrden("ic_tipo_cobro_interes");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoIfDist")){

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveEpo(iNoCliente);
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("CatalogoModoPlazoDist")){
	
	ic_pyme	=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String valida	=	"";
	valida = cargaDocto.getValidaIn(iNoCliente,ic_pyme);
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_financiamiento");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_tipo_financiamiento");
	cat.setValoresCondicionIn(valida, Integer.class);
	//cat.setOrden("ic_tipo_financiamiento");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta")){
	int start = 0;
	int limit = 0;
	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsInfDocEpoDist());
	queryHelper.setMultiplesPaginadoresXPagina(true);

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request);
		}
		infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
}else if (informacion.equals("ResumenTotalesA")) {		//Datos para el Resumen de Totales
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsInfDocEpoDist());
	queryHelper.setMultiplesPaginadoresXPagina(true);
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

}else if (informacion.equals("Consulta_B")){	//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
	int start = 0;
	int limit = 0;

	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	CQueryHelperExtJS queryHelper1 = new CQueryHelperExtJS(new Cons2InfDocEpoDist());//,"ids1","totales1");
	queryHelper1.setMultiplesPaginadoresXPagina(true);

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper1.executePKQuery(request);
		}
		infoRegresar = queryHelper1.getJSONPageResultSet(request,start,limit);
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
}else if (informacion.equals("ResumenTotalesB")) {		//Datos para el Resumen de Totales
	CQueryHelperExtJS queryHelper1 = new CQueryHelperExtJS(new Cons2InfDocEpoDist());
	queryHelper1.setMultiplesPaginadoresXPagina(true);
	infoRegresar  = queryHelper1.getJSONResultCount(request);	//los saca de sesion

} else if(informacion.equals("ArchivoTotalPDF")){

	ConsInfDocEpoDist paginador = new ConsInfDocEpoDist();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	JSONObject jsonObj = new JSONObject();
	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_if(ic_if);
	paginador.setIc_estatus_docto(ic_estatus_docto);
	paginador.setIg_numero_docto(ig_numero_docto);
	paginador.setFecha_seleccion_de(fecha_seleccion_de);
	paginador.setFecha_seleccion_a(fecha_seleccion_a);
	paginador.setCc_acuse(cc_acuse);
	paginador.setMonto_credito_de(monto_credito_de);
	paginador.setMonto_credito_a(monto_credito_a);
	paginador.setFecha_emision_de(fecha_emision_de);
	paginador.setFecha_emision_a(fecha_emision_a);
	paginador.setFecha_vto_de(fecha_vto_de);
	paginador.setFecha_vto_a(fecha_vto_a);
	paginador.setFecha_vto_credito_de(fecha_vto_credito_de);
	paginador.setFecha_vto_credito_a(fecha_vto_credito_a);
	paginador.setIc_tipo_cobro_int(ic_tipo_cobro_int);
	paginador.setIc_moneda(ic_moneda);
	paginador.setFn_monto_de(fn_monto_de);
	paginador.setFn_monto_a(fn_monto_a);
	paginador.setMonto_con_descuento(monto_con_descuento);
	paginador.setSolo_cambio(solo_cambio);
	paginador.setModo_plazo(modo_plazo);
	paginador.setTipo_credito(tipo_credito);
	paginador.setIc_documento(ic_documento);
	paginador.setFecha_publicacion_de(fecha_publicacion_de);
	paginador.setFecha_publicacion_a(fecha_publicacion_a);
	paginador.setNOnegociable(NOnegociable);
	paginador.setTipo_pago(tipo_pago);
	paginador.setPublicaDoctosFinanciables(publicaDoctosFinanciables);

	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("ArchivoBTotalPDF")){

// Obtengo los datos del grid totales
	JSONArray arrayRegNuevo = JSONArray.fromObject(datosGridTotales);

	Cons2InfDocEpoDist paginador = new Cons2InfDocEpoDist();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	JSONObject jsonObj = new JSONObject();
	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_if(ic_if);
	paginador.setIc_estatus_docto(ic_estatus_docto);
	paginador.setIg_numero_docto(ig_numero_docto);
	paginador.setFecha_seleccion_de(fecha_seleccion_de);
	paginador.setFecha_seleccion_a(fecha_seleccion_a);
	paginador.setCc_acuse(cc_acuse);
	paginador.setMonto_credito_de(monto_credito_de);
	paginador.setMonto_credito_a(monto_credito_a);
	paginador.setFecha_emision_de(fecha_emision_de);
	paginador.setFecha_emision_a(fecha_emision_a);
	paginador.setFecha_vto_de(fecha_vto_de);
	paginador.setFecha_vto_a(fecha_vto_a);
	paginador.setFecha_vto_credito_de(fecha_vto_credito_de);
	paginador.setFecha_vto_credito_a(fecha_vto_credito_a);
	paginador.setIc_tipo_cobro_int(ic_tipo_cobro_int);
	paginador.setIc_moneda(ic_moneda);
	paginador.setFn_monto_de(fn_monto_de);
	paginador.setFn_monto_a(fn_monto_a);
	paginador.setMonto_con_descuento(monto_con_descuento);
	paginador.setSolo_cambio(solo_cambio);
	paginador.setModo_plazo(modo_plazo);
	paginador.setTipo_credito(tipo_credito);
	paginador.setIc_documento(ic_documento);
	paginador.setFecha_publicacion_de(fecha_publicacion_de);
	paginador.setFecha_publicacion_a(fecha_publicacion_a);
	paginador.setNOnegociable(NOnegociable);
	paginador.setTipo_pago(tipo_pago);
	paginador.setPublicaDoctosFinanciables(publicaDoctosFinanciables);
	paginador.setGridTotales(arrayRegNuevo);
	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	infoRegresar = jsonObj.toString();

}
%>
<%=infoRegresar%>