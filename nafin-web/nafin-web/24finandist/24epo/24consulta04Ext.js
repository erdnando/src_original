	
Ext.onReady(function() {
	
	
	var procesarSuccessFailureGenerarPDF=  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	
	var procesarSuccessFailureGenerarCSV=  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}		
	}
	

		//GRID GENERAL
	var procesarConsultaIFData = function(store, arrRegistros, opts) 	{
	
		var jsonData = store.reader.jsonData;
		var nombrePyme2 = jsonData.NOMBREPYME;
		var  descripcion2 = jsonData.DESCRIPCION;
	
		formatexto2.show();
		var nombrePyme = Ext.getCmp('nombrePyme');
		nombrePyme.getEl().update(nombrePyme2);
		
		var descripcion = Ext.getCmp('descripcion');
		descripcion.getEl().update(descripcion2);
		
		Ext.getCmp('btnBajarCSV').hide();
		Ext.getCmp('btnBajarPDF').hide();						
	
		Ext.getCmp('btnGenerarCSV').enable();
		Ext.getCmp('btnGenerarPDF').enable();	
		
		var fp = Ext.getCmp('forma');
		var el = gridIF.getGridEl();	

		if (arrRegistros != null) {
			if (!gridIF.isVisible()) {
				gridIF.show();				
			}			
					
			if(store.getTotalCount() > 0) {
					el.unmask();
				} else {					
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	
		//GRID GENERAL
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
	
		var jsonData = store.reader.jsonData;
		var responsable2= jsonData.RESPONSABLE;
		var tipoFinan2 = jsonData.TIPO_FINAN;
			
		
		var fp = Ext.getCmp('forma');
		var el = grid.getGridEl();	
		
		var responsable = Ext.getCmp('responsable');
		responsable.getEl().update(responsable2);
		
		var tipoFinan = Ext.getCmp('tipoFinan');
		tipoFinan.getEl().update(tipoFinan2);
		
		formatexto2.hide();
		
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();				
			}			
								
				if(store.getTotalCount() > 0) {
					el.unmask();
				} else {					
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	
	
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta04Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var consultaData = new Ext.data.JsonStore({
			root : 'registros',
			url : '24consulta04Ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
				{name: 'NOMBREIF'},
				{name: 'NUMLINEA'}, 				
				{name: 'MONEDA'},
				{name: 'FECHA_AUTORIZACION'}, 
				{name: 'PLAZO'}, 
				{name: 'MONTO_LINEA'},
				{name: 'ESQUEMA'}						
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});
	
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: false,
		margins: '20 0 0 0',
		title: '',
		columns: [
			{
				header: 'IF Relacionado',
				tooltip: 'IF Relacionado',
				dataIndex: 'NOMBREIF',
				sortable: true,
				resizable: true,
				width: 150,				
				align: 'left'
			},				
			{
				header: 'Num. l�nea cr�dito',
				tooltip: 'Num. l�nea cr�dito',
				dataIndex: 'NUMLINEA',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha autorizaci�n',
				tooltip: 'Fecha autorizaci�n',
				dataIndex: 'FECHA_AUTORIZACION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			 
			},				
			
			{
				header: 'Monto l�nea',
				tooltip: 'Monto l�nea',
				dataIndex: 'MONTO_LINEA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
				
			},	
			{
				header: 'Esquema de cobro de intereses',
				tooltip: 'Esquema de cobro de intereses',
				dataIndex: 'ESQUEMA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'		
			}								
			],			
			stripeRows: true,
			loadMask: true,
			height: 400,
			width: 943,				
			frame: true
		});
				
	
	
	
		var consultaIFData = new Ext.data.JsonStore({
			root : 'registros',
			url : '24consulta04Ext.data.jsp',
			baseParams: {
				informacion: 'ConsultaIF'
			},
			fields: [
				{name: 'NOMBREIF'},							
				{name: 'MONEDA'},			
				{name: 'ESQUEMA'}						
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaIFData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaIFData(null, null, null);						
					}
				}
			}
		});
	
	var gridIF = new Ext.grid.GridPanel({
		store: consultaIFData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title: '',
		columns: [
			{
				header: 'IF Relacionado',
				tooltip: 'IF Relacionado',
				dataIndex: 'NOMBREIF',
				sortable: true,
				resizable: true,
				width: 200,				
				align: 'left'
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true,
				width: 200,
				hidden: false,
				align: 'center'
			},			
				
			{
				header: 'Esquema de cobro de intereses',
				tooltip: 'Esquema de cobro de intereses',
				dataIndex: 'ESQUEMA',
				sortable: true,
				resizable: true	,
				width: 200,
				hidden: false,
				align: 'left'		
			}								
			],			
			stripeRows: true,
			loadMask: true,
			height: 200,
			width: 620,				
			frame: true
		});
				
				

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_pyme',
			id: 'ic_pyme1',
			fieldLabel: 'Distribuidor',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_pyme',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoPYMEData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
        select: {
						fn: function (combo) {
						consultaIFData.load({					
							params: Ext.apply(fp.getForm().getValues())
						});
						}		
					}
				}
      }
	];
			
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 400,
		title: '',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true	
	});
	
		var formatexto1=new Ext.FormPanel({
		height:'auto',
		width: 'auto',
		style: 'margin:0 auto',
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		labelWidth: 300,
		items: [
			{ 
			xtype:   'label',  
			html:		'Parametrizaci�n General Vigente', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},		
		{
			xtype: 'label',
		  id: 'responsable',
      fieldLabel: 'Responsable de Pago de Inter�s en DM',
			align: 'left'	
		},
		{
			xtype: 'label',
		  id: 'tipoFinan',
      fieldLabel: 'Modalidades de Plazo operables en DM y CCC',
			align: 'left'	
		},		
		{
			xtype: 'label',
		  id: 'lineas',
      fieldLabel: 'L�neas de cr�dito autorizadas para operar en DM',
			align: 'left'	
		}	
		
		]
})

	
	
	var formatexto2=new Ext.FormPanel({
		height:'auto',
		width: 'auto',
		style: 'margin:0 auto',
		//align: 'center',
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		labelWidth: 150,		
		items: [
		{
			xtype: 'label',
		  id: 'nombrePyme',
      fieldLabel: 'Nombre del cliente',
			align: 'left'	
		},
		{
			xtype: 'label',
		  id: 'descripcion',
      fieldLabel: 'Tipo de cr�dito',
			align: 'left'	
		}
		]
})
	

	
	var fpGenerar = new Ext.Container({
		layout: 'table',
		id: 'formaI',
		hidden: false,
		layoutConfig: {
			columns: 6
		},
		width:	'100',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
		{
			xtype: 'button',
			text: 'Generar Archivo',
			id: 'btnGenerarCSV',			
			handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
				url: '24consulta04ExtPDFCSV.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					tipoArchivo: 'CSV',
					informacion: 'GenerarArchivo'						
					}),
					callback: procesarSuccessFailureGenerarCSV
				});
			}
		}, 
		{
			xtype: 'button',
			text: ' Bajar Archivo',
			id: 'btnBajarCSV',
			hidden: true
		}	,		
		{
			xtype: 'button',
			text: 'Generar PDF',
			id: 'btnGenerarPDF',			
			handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
				url: '24consulta04ExtPDFCSV.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					tipoArchivo: 'PDF',
					informacion: 'GenerarArchivo'						
					}),
					callback: procesarSuccessFailureGenerarPDF
				});
			}
		},
		{
			xtype: 'button',
			text: 'Bajar PDF',
			id: 'btnBajarPDF',
			hidden: true
		}	
		
	]
	});
	
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [	
		formatexto1,
		NE.util.getEspaciador(20),
			grid,
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			formatexto2,
			NE.util.getEspaciador(20),
			gridIF,
			NE.util.getEspaciador(20),
			fpGenerar
		]
	});
	
	catalogoPYMEData.load();
	consultaData.load();

} );