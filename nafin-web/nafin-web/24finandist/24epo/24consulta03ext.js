var tipoConversion = false;
Ext.onReady(function() {

	var jsonValoresIniciales = null;
	var botonConsulta = false;
	function procesaValoresIniciales(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null && botonConsulta){
				fp.el.mask('Enviando...', 'x-mask-loading');
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar', //Generar datos para la consulta
						start: 0,
						limit: 15
					})
				});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnTotales = Ext.getCmp('btnTotales');

			var cm = grid.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(15, true);		//Monto valuado en pesos
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				if (tipoConversion)	{
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
					cm.setHidden(15, false);	//Monto valuado en pesos
				}
				btnTotales.enable();
				btnGenerarPDF.enable();
				btnGenerarArchivo.enable();
				el.unmask();
			} else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var catalogoDisData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoDistribuidor'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMonedaDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoModoPlazoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoModoPlazoDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOMBRE_DIST'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'DF_FECHA_PUBLICACION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'DF_FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO',		type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MODO_PLAZO'},
			{name: 'TIPO_CAMBIO_ESTATUS'},
			{name: 'FECHA_CAMBIO',		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IC_MONEDA'},
			{name: 'CATEGORIA'},
			{name: 'CG_VENTACARTERA'},
			{name: 'OBSERVACIONES'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			beforeLoad:	{fn: function(store, options){
							tipoConversion = false;
							Ext.apply(options.params, {NOnegociable: jsonValoresIniciales.NOnegociable,	ic_epo : jsonValoresIniciales.ses_ic_epo});
							}	},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var resumenTotalesAData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotalesA'
		},
		fields: [
			{name: 'NOMMONEDA', mapping: 'col1' },
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'col2'},
			{name: 'TOTAL_MONTO_DOCUMENTOS', type: 'float', mapping: 'col3'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var gridTotalesA = {
		xtype: 'grid',
		store: resumenTotalesAData,
		id: 'gridTotalesA',
		hidden:	true,
		columns: [
			{
				header: 'TOTALES',
				dataIndex: 'NOMMONEDA',
				align: 'left',	width: 250
			},
			{
				header: 'Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},
			{
				header: 'Monto Documentos',
				dataIndex: 'TOTAL_MONTO_DOCUMENTOS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header: 'Distribuidor', tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true, resizable: true,width : 140,	hidden: false
			},{
				header: 'Num. acuse carga', tooltip: 'N�mero acuse carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'DF_FECHA_EMISION',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',
				dataIndex : 'DF_FECHA_PUBLICACION',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_FECHA_VENC',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'IG_PLAZO_DOCTO',
				sortable : true, align: 'center',	width : 120,	hidden: false
			},{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'MONEDA',
				sortable : true,	width : 130,	hidden: false
			},{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'FN_MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Categoria', tooltip: 'Categoria',
				dataIndex : 'CATEGORIA',
				sortable : true,	width : 120,	hidden: false
			},{
				header : 'Plazo para descuento en d�as', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true, align:'center',	width : 120,	hidden: false
			},{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,	width : 100, align: 'center', hidden: false
			},{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : '',	//	ESTE CAMPO ES CALCULADO	...
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	function(value, metaData, registro, rowIndex, colIndex, store) {
									return Ext.util.Format.number(registro.get('FN_MONTO')*(registro.get("FN_PORC_DESCUENTO")/100), '$0,0.00');
								}
			},{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true,
				renderer: 	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('TIPO_CAMBIO') != 1)	{
										value = "";
									}
									if (value != ""){
										tipoConversion = true;
									}
									return value;
								}
			},{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,
				renderer	:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('TIPO_CAMBIO') != 1)	{
										value = "";
									}
									return Ext.util.Format.number(value,'$0,0.00');
								}
			},{
				header : 'Monto valuado en pesos', tooltip: 'Monto valuado en pesos',
				dataIndex : '',	//CALCULADO
				sortable : true,	width : 120,	hidden: true,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
								value = "";
									if (registro.get('TIPO_CAMBIO') != 1)	{
										var porcDescuento = registro.get("FN_PORC_DESCUENTO");
										var montoDescontar = (registro.get('FN_MONTO')*(porcDescuento/100), '$0,0.00');
										value = ((registro.get('FN_MONTO')-montoDescontar)*registro.get('TIPO_CAMBIO'));
									}
								return Ext.util.Format.number(value, '$0,0.00');
								}
			},{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,	width : 100,	hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},{
				header : 'Tipo de cambio de estatus', tooltip: 'Tipo de cambio de estatus',
				dataIndex : 'TIPO_CAMBIO_ESTATUS',
				sortable : true,	width : 150,	hidden: false
			},{
				header : 'Fecha de cambio de estatus', tooltip: 'Fecha de cambio de estatus',
				dataIndex : 'FECHA_CAMBIO',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Observaciones', tooltip: 'Observaciones',
				dataIndex : 'OBSERVACIONES',
				sortable : true,	width : 100, hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Datos Documento Inicial',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						resumenTotalesAData.load();
						var totalesACmp = Ext.getCmp('gridTotalesA');
						if (!totalesACmp.isVisible()) {
							totalesACmp.show();
							totalesACmp.el.dom.scrollIntoView();
						}
					}
				},'-',{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta03ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								ic_epo : jsonValoresIniciales.ses_ic_epo,
								NOnegociable: jsonValoresIniciales.NOnegociable
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},'-',{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta03exta.jsp',
							params:	Ext.apply(fp.getForm().getValues(),{
										informacion: 'ArchivoCSV',
										ic_epo : jsonValoresIniciales.ses_ic_epo,
										NOnegociable: jsonValoresIniciales.NOnegociable}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				}
			]
		}
	});

	var elementosForma = [
		{
			xtype: 'panel',
			layout:'column',
				items:[{
					xtype: 'container',
					id:	'panelIzq',
					columnWidth:.5,
					width:             '50%',
					layout: 'form',
					items: [
					{
						xtype: 'combo',
						name: 'ic_pyme',
						id: 'cmbDis',
						allowBlank: false,
						fieldLabel: 'Distribuidor',
						mode: 'local', 
						displayField : 'descripcion',
						valueField : 'clave',
						hiddenName : 'ic_pyme',
						emptyText: 'Seleccione Distribuidor',
						forceSelection : false,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store : catalogoDisData,
						 tpl:  '<tpl for=".">' +
								 '<tpl if="!Ext.isEmpty(loadMsg)">'+
								 '<div class="loading-indicator">{loadMsg}</div>'+
								 '</tpl>'+
								 '<tpl if="Ext.isEmpty(loadMsg)">'+
								 '<div class="x-combo-list-item">' +
								 '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
								 '</div></tpl></tpl>'
					},{
						xtype: 'textfield',
						name: 'ig_numero_docto',
						id: 'no_doc_ini',
						fieldLabel: 'N�mero de documento inicial',
						allowBlank: true,
						maxLength: 13
					},{
						xtype: 'textfield',
						name: 'cc_acuse',
						id: 'no_cc_acuse',
						fieldLabel: 'Num. acuse de carga',
						allowBlank: true,
						maxLength: 15
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha emisi�n docto.',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fecha_emi_de',
								id: 'dc_fecha_emisionMin',
								allowBlank: true,
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'dc_fecha_emisionMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 24
							},
							{
								xtype: 'datefield',
								name: 'fecha_emi_a',
								id: 'dc_fecha_emisionMax',
								allowBlank: true,
								startDay: 1,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoInicioFecha: 'dc_fecha_emisionMin',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha vencimiento docto.',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fecha_vto_de',
								id: 'dc_fecha_vencMin',
								allowBlank: true,
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'dc_fecha_vencMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 24
							},
							{
								xtype: 'datefield',
								name: 'fecha_vto_a',
								id: 'dc_fecha_vencMax',
								allowBlank: true,
								startDay: 1,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'dc_fecha_vencMin',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha de publicaci�n',	
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fecha_publicacion_de',
								id: 'fecha_publicaMin',
								allowBlank: true,
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'fecha_publicaMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 24
							},
							{
								xtype: 'datefield',
								name: 'fecha_publicacion_a',
								id: 'fecha_publicaMax',
								allowBlank: true,
								startDay: 1,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'fecha_publicaMin',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					}
					]
				},{
					xtype: 'container',
					id:	'panelDer',
					columnWidth:.5,
					width:             '50%',
					layout: 'form',
					items: [
					{
						xtype: 'combo',
						name: 'ic_moneda',
						fieldLabel: 'Moneda',
						emptyText: 'Seleccione Moneda',	
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'ic_moneda',
						forceSelection : false,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoMonedaData,
						tpl : NE.util.templateMensajeCargaCombo
					},{
						xtype: 'compositefield',
						fieldLabel: 'Monto docto. inicial',
						msgTarget: 'side',
						combineErrors: false, anchor: '100%',
						items: [
							{
								xtype: 'numberfield',
								name: 'fn_monto_de',
								id: 'montoMin',
								allowBlank: true,
								maxLength: 12,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangoValor',
								campoFinValor: 'montoMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'a',
								width: 20
							},
							{
								xtype: 'numberfield',
								name: 'fn_monto_a',
								id: 'montoMax',
								allowBlank: true,
								maxLength: 12,
								width: 110,
								msgTarget: 'side',
								vtype: 'rangoValor',
								campoInicioValor: 'montoMin',
								margins: '0 20 0 0'	  //necesario para mostrar el icono de error
							}
						]
					},{
							xtype: 'checkbox',
							//boxLabel:	'Monto % de descuento',
							fieldLabel:	'Monto % de descuento',
							value:	'S',
							name: 'monto_con_descuento'
					},{
						xtype: 'combo',
						name: 'modo_plazo',
						id:	'cmbModalidad',
						fieldLabel: 'Modalidad del plazo',
						emptyText: 'Seleccione modalidad de plazo',
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'modo_plazo',
						forceSelection : false,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoModoPlazoData,
						tpl : NE.util.templateMensajeCargaCombo
					},{
						xtype: 'combo',
						name: 'ic_cambio_estatus',
						id: 'cambioEstatusCmb',
						fieldLabel: 'Tipo de cambio de estatus',
						emptyText: 'Seleccione Estatus',
						allowBlank: true,
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'ic_cambio_estatus',
						forceSelection : false,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoEstatusData,
						 tpl:  '<tpl for=".">' +
								 '<tpl if="!Ext.isEmpty(loadMsg)">'+
								 '<div class="loading-indicator">{loadMsg}</div>'+
								 '</tpl>'+
								 '<tpl if="Ext.isEmpty(loadMsg)">'+
								 '<div class="x-combo-list-item">' +
								 '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
								 '</div></tpl></tpl>'
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha de cambio de estatus',	
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fecha_cambio_de',
								id: 'fecha_cambioMin',
								allowBlank: true,
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'fecha_cambioMax',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 24
							},
							{
								xtype: 'datefield',
								name: 'fecha_cambio_a',
								id: 'fecha_cambioMax',
								allowBlank: true,
								startDay: 1,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'fecha_cambioMin',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					}
				]
			}]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 940,
		style: ' margin:0 auto;',
		title:	'Criterios de b�squeda',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					var totalesACmp = Ext.getCmp('gridTotalesA');
					if (totalesACmp.isVisible()) {
						totalesACmp.hide();
					}
					if(!verificaPanelIzq()) {
						return;
					}
					if(!verificaPanelDer()) {
						return;
					}
					var fechaEmisionMin = Ext.getCmp('dc_fecha_emisionMin');
					var fechaEmisionMax = Ext.getCmp('dc_fecha_emisionMax');
					var fechaVenceMin = Ext.getCmp('dc_fecha_vencMin');
					var fechaVenceMax = Ext.getCmp('dc_fecha_vencMax');
					var izq_montoMin = Ext.getCmp('montoMin');
					var izq_montoMax = Ext.getCmp('montoMax');
					var fechaCambioMin = Ext.getCmp('fecha_cambioMin');
					var fechaCambioMax = Ext.getCmp('fecha_cambioMax');
					var fechaPublicMin = Ext.getCmp('fecha_publicaMin');
					var fechaPublicMax = Ext.getCmp('fecha_publicaMax');
					var cambioEstatusCmb = Ext.getCmp('cambioEstatusCmb');
					if (!Ext.isEmpty(fechaEmisionMin.getValue()) || !Ext.isEmpty(fechaEmisionMax.getValue()) ) {
						if(Ext.isEmpty(fechaEmisionMin.getValue()))	{
							fechaEmisionMin.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
							fechaEmisionMin.focus();
							return;
						}else if (Ext.isEmpty(fechaEmisionMax.getValue())){
							fechaEmisionMax.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
							fechaEmisionMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaVenceMin.getValue()) || !Ext.isEmpty(fechaVenceMax.getValue()) ) {
						if(Ext.isEmpty(fechaVenceMin.getValue()))	{
							fechaVenceMin.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMin.focus();
							return;
						}else if (Ext.isEmpty(fechaVenceMax.getValue())){
							fechaVenceMax.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaCambioMin.getValue()) || !Ext.isEmpty(fechaCambioMax.getValue()) ) {
						if(Ext.isEmpty(fechaCambioMin.getValue()))	{
							fechaCambioMin.markInvalid('Debe capturar ambas fechas de operaci�n o dejarlas en blanco');
							fechaCambioMin.focus();
							return;
						}else if (Ext.isEmpty(fechaCambioMax.getValue())){
							fechaCambioMax.markInvalid('Debe capturar ambas fechas de operaci�n o dejarlas en blanco');
							fechaCambioMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaPublicMin.getValue()) || !Ext.isEmpty(fechaPublicMax.getValue()) ) {
						if(Ext.isEmpty(fechaPublicMin.getValue()))	{
							fechaPublicMin.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
							fechaPublicMin.focus();
							return;
						}else if (Ext.isEmpty(fechaPublicMax.getValue())){
							fechaPublicMax.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
							fechaPublicMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(izq_montoMin.getValue()) || !Ext.isEmpty(izq_montoMax.getValue()) ) {
						if(Ext.isEmpty(izq_montoMin.getValue()))	{
							izq_montoMin.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							izq_montoMin.focus();
							return;
						}else if (Ext.isEmpty(izq_montoMax.getValue())){
							izq_montoMax.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							izq_montoMax.focus();
							return;
						}
					}
					if (cambioEstatusCmb.getValue() == 24 && jsonValoresIniciales.NOnegociable == 'N'){
						cambioEstatusCmb.markInvalid('La EPO no se encuentra parametrizada para publicar documentos No Negociables');
						cambioEstatusCmb.focus();
						return;
					}
					botonConsulta = true;
					fp.el.mask('Enviando...', 'x-mask-loading');
					var ic_pyme = Ext.getCmp('cmbDis').getValue();
					Ext.Ajax.request({
						url: '24consulta03ext.data.jsp',
						params: {
							informacion: "valoresIniciales",
							ic_pyme:	ic_pyme
						},
						callback: procesaValoresIniciales
					});
				} //fin handler
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24consulta03ext.jsp';
				}
			}
		]
	});

	function verificaPanelIzq(){
		var myPanel = Ext.getCmp('panelIzq');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

	function verificaPanelDer(){
		var myPanel = Ext.getCmp('panelDer');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid,		gridTotalesA
		]
	});

	fp.el.mask('Enviando...', 'x-mask-loading');
	var ic_pyme = Ext.getCmp('cmbDis').getValue();
	Ext.Ajax.request({
		url: '24consulta03ext.data.jsp',
		params: {
			informacion: "valoresIniciales",
			ic_pyme:	ic_pyme
		},
		callback: procesaValoresIniciales
	});

	catalogoDisData.load();
	catalogoEstatusData.load();
	catalogoMonedaData.load();
	catalogoModoPlazoData.load();

});