<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoIFDistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

//parametros que provienen de la pagina actual
String	ic_if				=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
String	ic_moneda		=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
String 	informacion   	= 	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String 	infoRegresar  	= 	"";

CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	
if (informacion.equals("valoresBusqueda")){

	JSONObject jsonObj = new JSONObject();

	if (iNoCliente != null && !iNoCliente.equals("") && ic_if !=null && !ic_if.equals("") && ic_moneda !=null && !ic_moneda.equals("") ){
		//Hashtable datos = new Hashtable();
		Vector vecFilas	= new Vector();
		Vector vecColumnas= null;
		int 		i=0;
		
		vecFilas = cargaDocto.getLineaCredito(iNoCliente, ic_if,ic_moneda);
		List cuenta		= new ArrayList();
		List banco_ref	= new ArrayList();
		List monto_auto= new ArrayList();
		List fecha_venc= new ArrayList();
		List moneda		= new ArrayList();
		List dispuesto	= new ArrayList();
		List disponible= new ArrayList();
		List estatus = new ArrayList();
		
		for(i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);
			cuenta.add((String)vecColumnas.get(0));
			banco_ref.add((String)vecColumnas.get(1));
			monto_auto.add((String)vecColumnas.get(2));
			fecha_venc.add((String)vecColumnas.get(3));
			moneda.add((String)vecColumnas.get(4));
			dispuesto.add((String)vecColumnas.get(5));
			disponible.add((String)vecColumnas.get(6));
			estatus.add((String)vecColumnas.get(7));
		}
		jsonObj.put("cuenta",		cuenta);
		jsonObj.put("banco_ref",	banco_ref);
		jsonObj.put("monto_auto",	monto_auto);
		jsonObj.put("fecha_venc",	fecha_venc);
		jsonObj.put("moneda",		moneda);
		jsonObj.put("dispuesto",	dispuesto);
		jsonObj.put("disponible",	disponible);
		jsonObj.put("estatus",		estatus);
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoIfDist")){

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveEpo(iNoCliente);
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("CatalogoCliente")){
	
	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setClaveEpo(iNoCliente);
	cat.setCampoClave("cp.ic_pyme");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta")	||	informacion.equals("ArchivoCSV")	||	informacion.equals("ArchivoPDF")){

	String fecha_inicio = (request.getParameter("fecha_inicio") ==null)?"":request.getParameter("fecha_inicio");
	String fecha_final  = (request.getParameter("fecha_final")  ==null)?"":request.getParameter("fecha_final");
	String cliente      = (request.getParameter("ic_cliente")   ==null)?"":request.getParameter("ic_cliente");
	String banco        = (request.getParameter("disBanco")     ==null)?"":request.getParameter("disBanco");

	if( fecha_inicio.equals("") && fecha_final.equals("")){
		fecha_inicio ="";
		fecha_final  ="";
	}
	if (cliente.equals("")){
		cliente = "0";
	}

	int start = 0;
	int limit = 0;
	EstadoAdeudos edoA = new EstadoAdeudos();
	edoA.setIc_if(ic_if);
	edoA.setIc_pyme(cliente);
	edoA.setUsuario(iNoCliente);
	edoA.setIc_moneda(ic_moneda);
	edoA.setDf_fecha_oper_de(fecha_inicio);
	edoA.setDf_fecha_oper_a(fecha_final);

	Vector vecFilas    = new Vector();
	Vector vecColumnas = null;
	int i = 0;

	if (iNoCliente != null && !iNoCliente.equals("") && ic_if !=null && !ic_if.equals("")){
		vecFilas = cargaDocto.getLineaCredito(iNoCliente, ic_if,ic_moneda);
	}
	if (vecFilas.size()>=0 ){
		edoA.setDatosVector(vecFilas);
	}

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(edoA);

	if (informacion.equals("Consulta")){//Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) { //Nueva consulta
				queryHelper.executePKQuery(request);
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

		} catch(Exception e){
			throw new AppException("Error en la paginacion", e);
		}
	} else if (informacion.equals("ArchivoCSV")) {
		JSONObject jsonObj = new JSONObject();
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo CSV", e);
		}
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("ArchivoPDF")){
		JSONObject jsonObj = new JSONObject();
		try {
			//String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();
	}
}
%>
<%=infoRegresar%>