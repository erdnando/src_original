<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.anticipos.*,
	netropology.utilerias.*,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String infoRegresar	=	"";
	HashMap registro = new HashMap();
	JSONArray registros = new JSONArray();
	JSONObject jsonObj = new JSONObject();

	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	int liRegistros=0;
	int Registros1 = 0;
	int Registros2 = 0;
	int Registros3 = 0;
	int index=0;
	Vector vecFilas		= null;
	Vector vecColumnas	= null;
	Vector vecFilas2	= null;
	Vector vecColumnas2 = null;
	Vector lovTasas		= null;
	Vector vector		= null;
	String total = "",tipoCredito = "",pagoInt = "";		
	String aux="";
	boolean DM=true,CCC=true;
	
 if(informacion.equals("GenerarArchivo")  ) {

	String tipoArchivo = (request.getParameter("tipoArchivo") != null) ? request.getParameter("tipoArchivo") : "";
	
	if(tipoArchivo.equals("PDF")){
	
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";	
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		
		vecFilas = BeanTasas.disOpera(iNoCliente,4,"");
		for (int i = 0;i<vecFilas.size();i++) {
			vecColumnas = (Vector)vecFilas.get(i);
			total = (String)vecColumnas.get(0);
			tipoCredito = (String)vecColumnas.get(1);
			if (Integer.parseInt(total) > 0 && "D".equals(tipoCredito)){
				vecFilas2 = BeanTasas.esquemaParticular(iNoCliente,4,"","");
				for (int j=0; j<vecFilas2.size(); j++) {
					DM = false;
					Vector lovDatosTas = (Vector)vecFilas2.get(j);
					String ic_if = lovDatosTas.get(0).toString();
					String ifRazonSoc = lovDatosTas.get(1).toString();
					lovTasas = BeanTasas.ovgetTasasxEpo(4,iNoCliente,ic_if);
					String encabezado = ifRazonSoc;
					
					pdfDoc.setTable(8, 100);
					pdfDoc.setCell(encabezado,"celda01",ComunesPDF.LEFT,8);	
					
					if(lovTasas.size()>0){				
						pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo Tasa","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Valor","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Rel. mat.","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Puntos adicionales","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Tasa a aplicar","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha última actualización","celda01",ComunesPDF.CENTER);				
		
						for (int y=0; y<lovTasas.size(); y++) {
							Vector lovDatosTasa = (Vector)lovTasas.get(y);
							pdfDoc.setCell(lovDatosTasa.get(0).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(1).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(3).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(4).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(5).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(6).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(7).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(8).toString() ,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.addTable();
						
					}else{
						pdfDoc.setCell("No se encontraron tasas" ,"formas",ComunesPDF.JUSTIFIED,8);
						pdfDoc.addTable();
					}
				}//for (int j=0; j<vecFilas2.size(); j++)
			}
			if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito)){
				CCC = false;		
				String encabezado = "Esquema de tasas para operaciones con Crédito en Cuenta Corriente";
				lovTasas = BeanTasas.ovgetTasas(4);
				pdfDoc.setTable(8, 100);
				pdfDoc.setCell(encabezado,"celda01",ComunesPDF.LEFT,8);
				if(lovTasas.size()>0){
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Valor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Rel. mat.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Puntos adicionales","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa a aplicar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha última actualización","celda01",ComunesPDF.CENTER);	
						
						
					for (int y=0; y<lovTasas.size(); y++) {
						Vector lovDatosTasa = (Vector)lovTasas.get(y);							
							pdfDoc.setCell(lovDatosTasa.get(0).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(1).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(3).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(4).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(5).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(6).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(7).toString() ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(lovDatosTasa.get(8).toString() ,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.addTable();
				}	
			}//if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito))
		}//for (int i = 0;i<vecFilas.size();i++) 
			
		pdfDoc.endDocument();		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}

	if(tipoArchivo.equals("CSV")){
	
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo=null;
	
		vecFilas = BeanTasas.disOpera(iNoCliente,4,"");
		for (int i = 0;i<vecFilas.size();i++) {
			vecColumnas = (Vector)vecFilas.get(i);
			total = (String)vecColumnas.get(0);
			tipoCredito = (String)vecColumnas.get(1);
			if (Integer.parseInt(total) > 0 && "D".equals(tipoCredito)){
				vecFilas2 = BeanTasas.esquemaParticular(iNoCliente,4,"","");
				for (int j=0; j<vecFilas2.size(); j++) {
					DM = false;
					Vector lovDatosTas = (Vector)vecFilas2.get(j);
					String ic_if = lovDatosTas.get(0).toString();
					String ifRazonSoc = lovDatosTas.get(1).toString();
					lovTasas = BeanTasas.ovgetTasasxEpo(4,iNoCliente,ic_if);
					String encabezado = ifRazonSoc;
					
					contenidoArchivo.append(encabezado.replace(',',' ')+"\n");	
					if(lovTasas.size()>0){	
						contenidoArchivo.append("Moneda,Tipo Tasa,Plazo,Valor,Rel. mat.,Puntos adicionales,Tasa a aplicar,Fecha última actualización");
						contenidoArchivo.append("\n");								
		
						for (int y=0; y<lovTasas.size(); y++) {
							Vector lovDatosTasa = (Vector)lovTasas.get(y);
							contenidoArchivo.append(lovDatosTasa.get(0).toString().replace(',',' ')+","); 				
							contenidoArchivo.append(lovDatosTasa.get(1).toString()+","); 				
							contenidoArchivo.append("\" "+lovDatosTasa.get(3).toString()+"\",");
							contenidoArchivo.append(lovDatosTasa.get(4).toString()+","); 					
							contenidoArchivo.append(lovDatosTasa.get(5).toString()+","); 					
							contenidoArchivo.append(lovDatosTasa.get(6).toString()+",");			
							contenidoArchivo.append(lovDatosTasa.get(7).toString()+",");
							contenidoArchivo.append(lovDatosTasa.get(8).toString()+"\n");
						}					
						
					}else{
						contenidoArchivo.append("No se encontraron tasas"+"\n");						
					}
				}//for (int j=0; j<vecFilas2.size(); j++)
			}
			if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito)){
				CCC = false;		
				String encabezado = "Esquema de tasas para operaciones con Crédito en Cuenta Corriente";
				lovTasas = BeanTasas.ovgetTasas(4);
				contenidoArchivo.append(encabezado.replace(',',' ')+"\n");	
				if(lovTasas.size()>0){
					contenidoArchivo.append("Moneda,Tipo Tasa,Plazo,Valor,Rel. mat.,Puntos adicionales,Tasa a aplicar,Fecha última actualización");
					contenidoArchivo.append("\n");
					for (int y=0; y<lovTasas.size(); y++) {
						Vector lovDatosTasa = (Vector)lovTasas.get(y);
						contenidoArchivo.append(lovDatosTasa.get(0).toString().replace(',',' ')+","); 				
						contenidoArchivo.append(lovDatosTasa.get(1).toString()+","); 				
						contenidoArchivo.append("\" "+lovDatosTasa.get(3).toString()+"\",");
						contenidoArchivo.append(lovDatosTasa.get(4).toString()+","); 					
						contenidoArchivo.append(lovDatosTasa.get(5).toString()+","); 					
						contenidoArchivo.append(lovDatosTasa.get(6).toString()+",");			
						contenidoArchivo.append(lovDatosTasa.get(7).toString()+",");
						contenidoArchivo.append(lovDatosTasa.get(8).toString()+"\n");
					}
					
				}	
			}//if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito))
		}//for (int i = 0;i<vecFilas.size();i++) 
		
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		
	}

}
%>



<%=jsonObj%>