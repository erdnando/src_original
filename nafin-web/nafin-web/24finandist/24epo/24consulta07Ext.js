
var texto3 = ['NOTA:  Las columnas con sub�ndice 1 pertenecen a "Datos Documento Inicial" y con sub�ndice 2 pertenecen a "Datos Documento Final '];
var texto4 = ['La informaci�n del porcentaje de "% Comisi�n aplicable de Terceros" (Adquirencia), "Monto Comisi�n de Terceros (IVA Incluido)" y "Monto a Depositar por operaci�n", es meramente informativa, y depende del servicio de Adquirencia que tenga contratado con terceros, favor de consultar su Estado de Cuenta referenciada. '];
		
Ext.onReady(function() {
	var formulario = new Array();
	var publicaDoctosFinanciables = '';
         var operaContrato = Ext.getDom('operaContrato').value;
        var leyenda = 'Me doy por enterado de las operaciones descritas en esta p�gina para todos los efectos legales conducentes.';
        var leyenda ='De conformidad a lo dispuesto en los art�culos 32 C del C�digo Fiscal o 2038 y 2041 del C�digo Civil Federal seg�n corresponda,'+
        'me doy por notificado de la cesi�n de derechos de las operaciones descritas en esta p�gina y para los efectos legales conducentes.<br/>'+
        'Por otra parte, manifiesto que, de los documentos y/o facturas publicadas y notificadas a partir del 17 de octubre, s� he emitido o emitir� al' +
        'CLIENTE o DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan ' +
        'las disposiciones fiscales vigentes.';
        
        if(operaContrato == "S"){
            leyenda = 'Otorgo mi anuencia sobre los documentos publicados en esta p�gina y para los efectos legales conducentes de conformidad con lo establecido en el Contrato de Financiamiento a Clientes y Distribuidores.';
        }
         
         
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
				/***** Si tipo de credito es modalidad 2, se muestra el combo Tipo de pago *****/
				publicaDoctosFinanciables = jsonValoresIniciales.publicaDoctosFinanciables;
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		fp.el.unmask();
	}

/*
	//GENERAR ARCHIVO TODO CSV
	var procesarSuccessFailureGenerarArchivoCSV =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailureTODOPDF =  function(opts, success, response) {
		var btnImprimirTPDF = Ext.getCmp('btnImprimirTPDF');
		btnImprimirTPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarTPDF = Ext.getCmp('btnBajarTPDF');
			btnBajarTPDF.show();
			btnBajarTPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarTPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnImprimirTPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//GENERAR ARCHIVO X PAGINA PDF
	var procesarSuccessFailureXPaginaPDF =  function(opts, success, response) {
		var btnXPaginaPDF = Ext.getCmp('btnXPaginaPDF');
		btnXPaginaPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarXPDF = Ext.getCmp('btnBajarXPDF');
			btnBajarXPDF.show();
			btnBajarXPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarXPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnXPaginaPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/

	//GENERAR ARCHIVO TODO CSV
	var procesarSuccessFailureGenerarArchivoCSV =  function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}

	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailureTODOPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnImprimirTPDF').enable();
		Ext.getCmp('btnImprimirTPDF').setIconClass('icoPdf');
	}

	var resumenTotalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24consulta07Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'			
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'NO_REGISTROS', mapping: 'NO_REGISTROS'}, 
			{name: 'MONTO', type: 'float', mapping: 'MONTO'},
			{name: 'MONTO_DESCUENTO', type: 'float', mapping: 'MONTO_DESCUENTO'},
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_CREDITO'},			
			{name: 'MONTO_INTERES', type: 'float', mapping: 'MONTO_INTERES'},
			{name: 'MONTO_DEPOSITAR', type: 'float', mapping: 'MONTO_DEPOSITAR'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
		
	var gridTotales ={
		xtype: 'grid',
		store: resumenTotalesData,			
		id: 'gridTotales',			
		columns: [	
			{	
				header: 'Moneda',
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'			
			},	
			{
				header: 'No de Registros',
				dataIndex: 'NO_REGISTROS',
				width: 150,
				align: 'center'			
			},	
			{
				header: '<sup>1</sup>Monto',
				dataIndex: 'MONTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{
				header: '<sup>1</sup>Monto con descuento',
				dataIndex: 'MONTO_DESCUENTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},						
			{
				header: '<sup>2</sup>Monto',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},				
			{
				header: '<sup>2</sup>Monto tasa de inter�s',
				dataIndex: 'MONTO_INTERES',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},				
			{
				header: 'Monto a Depositar por Operaci�n',
				dataIndex: 'MONTO_DEPOSITAR',
				width: 200,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 100,			
		width: 993,
		title: '',
		frame: false
	};
				
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		var responsable = jsonData.RESPONSABLE;		
		var cgtipoconversion = jsonData.CGTIPOCONVERSION;
		var tiposCredito = jsonData.TIPOSCREDITO; 
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
		}
		var cm = grid.getColumnModel();
		
		if(cgtipoconversion!=''){			
			grid.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('MONTO_VALUADO'), false);
		}	
		if(responsable!='D'){						
			grid.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CREDITO'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('IC_DOCUMENTO'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('MONTO_CREDITO'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('IG_PLAZO_CREDITO'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('DF_FECHA_VENC_CREDITO'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_INT'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('VALOR_TASA'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INTERES'), false);
		}
		if(tiposCredito=='F'){			
			grid.getColumnModel().setHidden(cm.findColumnIndex('CG_NUMERO_CUENTA'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('IG_PLAZO_CREDITO'), true);
			grid.getColumnModel().setHidden(cm.findColumnIndex('DF_FECHA_VENC_CREDITO'), true);
			grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_INT'), true);
			grid.getColumnModel().setHidden(cm.findColumnIndex('VALOR_TASA'), true);
			grid.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INTERES'), true);
		} else{
			grid.getColumnModel().setHidden(cm.findColumnIndex('IG_PLAZO_CREDITO'), false);
			grid.getColumnModel().setHidden(cm.findColumnIndex('DF_FECHA_VENC_CREDITO'), false);
		}
		if(publicaDoctosFinanciables == 'S'){
			grid.getColumnModel().setHidden(cm.findColumnIndex('IG_TIPO_PAGO'), false);
		} else{
			grid.getColumnModel().setHidden(cm.findColumnIndex('IG_TIPO_PAGO'), true);
		}

		//var btnXPaginaPDF =	Ext.getCmp('btnXPaginaPDF');
		var btnImprimirTPDF = Ext.getCmp('btnImprimirTPDF');
		var btnGenerarArchivo	=	Ext.getCmp('btnGenerarArchivo');
		var btnTotales = Ext.getCmp('btnTotales');
	
		//var btnBajarXPDF =  Ext.getCmp('btnBajarXPDF').hide();
		//var btnBajarTPDF = 	Ext.getCmp('btnBajarTPDF').hide();
		//var btnBajarCSV = 	Ext.getCmp('btnBajarCSV').hide();
		var btnTotales = 	Ext.getCmp('btnTotales').hide();

		var el = grid.getGridEl();
		if(store.getTotalCount() > 0) {
			ctexto4.show();
			ctexto3.show();
		
			btnTotales.show();
			/*if(!btnBajarXPDF.isVisible()) {
				btnXPaginaPDF.enable();
			} else {
				btnXPaginaPDF.disable();
			}
			if(!btnBajarTPDF.isVisible()) {
				btnImprimirTPDF.enable();
			} else {
				btnImprimirTPDF.disable();
			}
			if(!btnBajarCSV.isVisible()) {
				btnGenerarArchivo.enable();
			} else {
				btnGenerarArchivo.disable();
			}*/
			
			el.unmask();
		} else {
			ctexto4.hide();
			ctexto3.hide();
			//btnXPaginaPDF.disable();
			btnImprimirTPDF.disable();
			btnGenerarArchivo.disable();
			//btnTotales.disable();	
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}		
	}
	//CONSULTA PARA LOS DEL GRID GENERAL
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta07Ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'DIF'},
			{name: 'DISTRIBUIDOR'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'CC_ACUSE'},
			{name: 'DF_FECHA_EMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC',  type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_HORA',  type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_PLAZO_DOCTO' },
			{name: 'MONEDA' },
			{name: 'FN_MONTO', type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VALUADO', type: 'float'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESCUENTO', type: 'float'},
			{name: 'MODALIDAD_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_ESTATUS'},
			{name: 'TIPO_CREDITO'},
			{name: 'IC_DOCUMENTO'},
			{name: 'MONTO_CREDITO'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'IG_TIPO_PAGO'},
			{name: 'DF_FECHA_VENC_CREDITO'},
			{name: 'REFERENCIA_INT'},
			{name: 'VALOR_TASA'},
			{name: 'MONTO_INTERES'},
			{name: 'CG_NUMERO_CUENTA'},
			{name: 'COMISION_APLICABLE'},
			{name: 'MONTO_DEPOSITAR'},
			{name: 'MONTO_COMISION'},
			{name: 'IC_ORDEN_ENVIADO'},
			{name: 'IC_OPERACION_CCACUSE'},
			{name: 'CODIGO_AUTORIZADO'},
			{name: 'FECHA_REGISTRO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IC_TIPO_FINANCIAMIENTO'},
			{name: 'NUMERO_TC'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				beforeLoad:	{fn: function(store, options){
						Ext.apply(options.params, { /*params: Ext.apply(fp.getForm().getValues()),*/
						ic_pyme: formulario.ic_pyme,
						ic_if: formulario.ic_if,
						Txtfchini: formulario.Txtfchini,
						Txtffin: formulario.Txtffin,
						numeroOrden: formulario.numeroOrden

						
						});
				}}, 
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}	
		});
		// create the Grid GENERAL
		var grid = new Ext.grid.GridPanel({
			store: consultaData,
			hidden: true,
			margins: '20 0 0 0',
			clicksToEdit: 1,				
			columns: [
				{
					id:'DIF',
					header: '<sup>1</sup>IF',
					tooltip: 'IF',
					dataIndex: 'DIF',
					sortable: true,
					resizable: true	,
					width: 130,
					hidden: false,
					align: 'left'
				},
				{
					id:'DISTRIBUIDOR',
					header: '<sup>1</sup>Distribuidor',
					tooltip: 'Distribuidor',
					dataIndex: 'DISTRIBUIDOR',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'left'
				},			
				{
					id:'IG_NUMERO_DOCTO',
					header: '<sup>1</sup>N�mero de documento inicial',
					tooltip: 'N�mero de documento inicial',
					dataIndex: 'IG_NUMERO_DOCTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				}	,
				{
					id:'CC_ACUSE',
					header: '<sup>1</sup>Num. acuse carga',
					tooltip: 'Num. acuse carga',
					dataIndex: 'CC_ACUSE',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'DF_FECHA_EMISION',
					header: '<sup>1</sup>Fecha de emisi�n',
					tooltip: 'Fecha de emisi�n',
					dataIndex: 'DF_FECHA_EMISION',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{		
					id:'DF_FECHA_PUBLICACION',
					header: '<sup>1</sup>Fecha de publicaci�n',
					tooltip: 'Fecha de publicaci�n',
					dataIndex: 'DF_FECHA_PUBLICACION',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{		
					id:'DF_FECHA_VENC',
					header: '<sup>1</sup>Fecha vencimiento',
					tooltip: 'Fecha vencimiento',
					dataIndex: 'DF_FECHA_VENC',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{		
					id:'DF_FECHA_HORA',
					header: '<sup>1</sup>Fecha de operaci�n',
					tooltip: 'Fecha de operaci�n',
					dataIndex: 'DF_FECHA_HORA',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},				
				{		
					id:'IG_PLAZO_DOCTO',
					header: '<sup>1</sup>Plazo docto',
					tooltip: 'Plazo docto',
					dataIndex: 'IG_PLAZO_DOCTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'MONEDA',
					header: '<sup>1</sup>Moneda',
					tooltip: 'Moneda',
					dataIndex: 'MONEDA',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'TIPO_CONVERSION',
					header: '<sup>1</sup>Tipo conv',
					tooltip: 'Tipo conv',
					dataIndex: 'TIPO_CONVERSION',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},
				{		
					id:'TIPO_CAMBIO',
					header: '<sup>1</sup>Tipo Cambio',
					tooltip: 'Tipo Cambio',
					dataIndex: 'TIPO_CAMBIO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},
				{		
					id:'MONTO_VALUADO',
					header: '<sup>1</sup>Monto Valuado',
					tooltip: 'Monto Valuado',
					dataIndex: 'MONTO_VALUADO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},							
				{		
					id:'FN_MONTO',
					header: '<sup>1</sup>Monto',
					tooltip: 'Monto',
					dataIndex: 'FN_MONTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},
				{		
					id:'IG_PLAZO_DESCUENTO',
					header: '<sup>1</sup>Plazo para descuento en d�as',
					tooltip: 'Plazo para descuento en d�as',
					dataIndex: 'IG_PLAZO_DESCUENTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'FN_PORC_DESCUENTO',
					header: '<sup>1</sup> % de descuento',
					tooltip: '% de descuento',
					dataIndex: 'FN_PORC_DESCUENTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer: Ext.util.Format.numberRenderer('0.0000%')
				},
				{		
					id:'MONTO_DESCUENTO',
					header: '<sup>1</sup> Monto % de descuento',
					tooltip: 'Monto % de descuento',
					dataIndex: 'MONTO_DESCUENTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},			
				{		
					id:'MODALIDAD_PLAZO',
					header: '<sup>1</sup>Modalidad de plazo',
					tooltip: 'Modalidad de plazo',
					dataIndex: 'MODALIDAD_PLAZO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},				
				{		
					id:'ESTATUS',
					header: '<sup>1</sup>Estatus',
					tooltip: 'Estatus',
					dataIndex: 'ESTATUS',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'CG_NUMERO_CUENTA',
					header: '<sup>1</sup>Cuenta Bancaria Pyme',
					tooltip: 'Cuenta Bancaria Pyme',
					dataIndex: 'CG_NUMERO_CUENTA',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},				
				//Datos Documento Final
				{		
					id:'TIPO_CREDITO',
					header: '<sup>2</sup>Tipo de cr�dito',
					tooltip: 'Tipo de cr�dito',
					dataIndex: 'TIPO_CREDITO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center'
				},
				{		
					id:'IC_DOCUMENTO',
					header: '<sup>2</sup>N�mero de documento final',
					tooltip: 'N�mero de documento final',
					dataIndex: 'IC_DOCUMENTO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center'
				},
				{		
					id:'MONTO_CREDITO',
					header: '<sup>2</sup>Monto',
					tooltip: 'Monto',
					dataIndex: 'MONTO_CREDITO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},
				{		
					header: '% Comisi�n aplicable de Terceros',
					tooltip: '% Comisi�n aplicable de Terceros',
					dataIndex: 'COMISION_APLICABLE',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')!="32"  ){
							value = "N/A";
						}else{
							value = Ext.util.Format.number(value,'0.00%');
						}
						return value;
					}
					
				},
				{		
					header: 'Monto Comisi�n de Terceros (IVA Incluido)',
					tooltip: 'Monto Comisi�n de Terceros (IVA Incluido)',
					dataIndex: 'MONTO_COMISION',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'right',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')!="32"  ){
							value = "N/A";
						}else{
							value = Ext.util.Format.number(value,'$ 0,0.00');
						}
						return value;
					}
				},
				{		
					id:'',
					header: 'Monto a Depositar por Operaci�n',
					tooltip: 'Monto a Depositar por Operaci�n',
					dataIndex: 'MONTO_DEPOSITAR',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'right',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')!="32"  ){
							value = "N/A";
						}else{
							value = Ext.util.Format.number(value,'$ 0,0.00');
						}
						return value;
					}
				},
				{		
					id:'IG_PLAZO_CREDITO',
					header: '<sup>2</sup>Plazo',
					tooltip: 'Plazo',
					dataIndex: 'IG_PLAZO_CREDITO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')=="32"  ){
							value = "N/A";
						}
						return value;
					}
				},
				{
					id:'IG_TIPO_PAGO',
					header: 'Tipo de pago',
					tooltip: 'Tipo de pago',
					dataIndex: 'IG_TIPO_PAGO',
					sortable: true,
					width: 180,
					resizable: true,
					align: 'center',
					renderer: function (value, metaData, registro, rowIndex, colIndex, store){
						if (registro.get('IG_TIPO_PAGO') == '1')	{
							value = 'Financiamiento con intereses';
						} else if (registro.get('IG_TIPO_PAGO') == '2')	{
							value = 'Meses sin intereses';
						} else{
							value = '';
						}
						return value;
					}
				},
				{
					id:'DF_FECHA_VENC_CREDITO',
					header: '<sup>2</sup>Fecha de vencimiento',
					tooltip: 'Fecha de vencimiento',
					dataIndex: 'DF_FECHA_VENC_CREDITO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center',
					//renderer: Ext.util.Format.dateRenderer('d/m/Y')
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')=="32"  ){
							value = "N/A";
						}
						return value;
					}
				},
				
				{		
					id:'REFERENCIA_INT',
					header: '<sup>2</sup>Referencia tasa de inter�s',
					tooltip: 'Referencia tasa de inter�s',
					dataIndex: 'REFERENCIA_INT',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')=="32"  ){
							value = "N/A";
						}
						return value;
					}
				},
				{		
					id:'VALOR_TASA',
					header: '<sup>2</sup>Valor tasa de inter�s',
					tooltip: 'Valor tasa de inter�s',
					dataIndex: 'VALOR_TASA',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')=="32"  ){
							value = "N/A";
						}else{
							value = Ext.util.Format.number(value,'0.00%');
						}
						return value;
					}
				},
				{		
					id:'MONTO_INTERES',
					header: '<sup>2</sup>Monto tasa de inter�s',
					tooltip: 'Monto tasa de inter�s',
					dataIndex: 'MONTO_INTERES',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: true,
					align: 'right',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')=="32"  ){
							value = "N/A";
						}else{
							value = Ext.util.Format.number(value,'$0,0.00');
						}
						return value;
					}
				},
				{		
					header: 'Id Orden enviado',
					tooltip: 'Id Orden enviado',
					dataIndex: 'IC_ORDEN_ENVIADO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')!="32"  ){
							value = "N/A";
						}
						return value;
					}
				},
				{		
					header: 'Respuesta de Operaci�n',
					tooltip: 'Respuesta de Operaci�n',
					dataIndex: 'IC_OPERACION_CCACUSE',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')!="32"  ){
							value = "N/A";
						}
						return value;
					}
				},
				{		
					header: 'C�digo Autorizaci�n',
					tooltip: 'C�digo Autorizaci�n',
					dataIndex: 'CODIGO_AUTORIZADO',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')!="32"  ){
							value = "N/A";
						}
						return value;
					}
				},
				{		
					header: 'N�mero Tarjeta de Cr�dito',
					tooltip: 'N�mero Tarjeta de Cr�dito',
					dataIndex: 'NUMERO_TC',
					sortable: true,
					width: 130,
					resizable: true,
					hidden: false,
					align: 'center',
					renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
						if( registro.get('IC_ESTATUS')!="32"  ){
							return value = "N/A";
						}else{
							return value==""?"":"XXXX-XXXX-XXXX-"+value;
						}
					}
				}
			],		
			displayInfo: true,		
			emptyMsg: "No hay registros.",
			loadMask: true,
			clicksToEdit: 1, 
			margins: '20 0 0 0',
			stripeRows: true,
			height: 400,
			width: 943,
			align: 'center',
			frame: false,		
			bbar: {
				xtype: 'paging',
				pageSize: 15,
				buttonAlign: 'left',
				id: 'barraPaginacion',
				displayInfo: true,
				store: consultaData,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No hay registros.",
				items: ['->','-',
					{
						xtype: 'button',
						text: 'Totales',
						id: 'btnTotales',
						hidden: false,
						handler: function(boton, evento) {
							var ventana = Ext.getCmp('winTotales');
							if (ventana) {
								ventana.show();
							} else {
								resumenTotalesData.load({
									params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'ResumenTotales'
									})
								});	
										
								new Ext.Window({
									layout: 'fit',
									width: 600,
									height: 105,
									id: 'winTotales',
									closeAction: 'hide',
									items: [
										gridTotales
									],								
									title: 'Totales'
								}).show().alignTo(grid.el);  
							}
						}
					},
					'-',
					/*{
						xtype: 'button',
						text: 'Imprimir x Pagina PDF',
						id: 'btnXPaginaPDF',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
							Ext.Ajax.request({
								url: '24consulta07Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoXPDF',		
									operacion: 'Generar', //Generar datos para la consulta
									start: cmpBarraPaginacion.cursor,
									limit: cmpBarraPaginacion.pageSize
								}),
								callback: procesarSuccessFailureXPaginaPDF
							});
						}
					},
					{
						xtype: 'button',
						text: 'Bajar X Pagina PDF',
						id: 'btnBajarXPDF',
						hidden: true
					},
					'-',*/
					{
						xtype: 'button',
						text: 'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
						id: 'btnImprimirTPDF',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url: '24consulta07Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoTPDF',
									publicaDoctosFinanciables: publicaDoctosFinanciables
								}),
								callback: procesarSuccessFailureTODOPDF
							});
						}
					},/*
					{
						xtype: 'button',
						text: 'Bajar Todo PDF',
						id: 'btnBajarTPDF',
						hidden: true
					},*/
					'-',
					{
						xtype: 'button',
						text: 'Generar Archivo',
						tooltip: 'Imprime los registros en formato CSV.',
						iconCls: 'icoXls',
						id: 'btnGenerarArchivo',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url: '24consulta07Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoCSV',
									publicaDoctosFinanciables: publicaDoctosFinanciables
								}),
								callback: procesarSuccessFailureGenerarArchivoCSV
							});
						}
					}/*,
					{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV',
						hidden: false
					},
					'-'*/
				]
			}
		});
		
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta07Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta07Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo		
		}
	});
		
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_pyme',
			id: 'ic_pyme1',
			fieldLabel: 'Distribuidor',
			mode: 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_pyme',
			emptyText: 'Seleccione...',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoPYMEData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'ic_if1',		
			fieldLabel: 'Intermediario finaciero',
			mode: 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_if',
			emptyText: 'Seleccione...',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoIFData,
			tpl : NE.util.templateMensajeCargaCombo
		},		
		{
					xtype: 'compositefield',
					fieldLabel: 'Fecha de operaci�n (dd/mm/aaaa)',
					combineErrors: false,
					msgTarget: 'side',
					items: [   
						{
							xtype: 'datefield',
							name: 'Txtfchini',
							id: 'Txtfchini1',
							allowBlank: true,
							startDay: 0,
							width: 100,
							msgTarget: 'side',
							vtype: 'rangofecha', 
							campoFinFecha: 'Txtffin1',
							margins: '0 20 0 0' 
						},
						{
							xtype: 'displayfield',
							value: 'al',
							width: 24
						},
						{
							xtype: 'datefield',
							name: 'Txtffin',
							id: 'Txtffin1',
							allowBlank: true,
							startDay: 1,
							width: 100,
							msgTarget: 'side',
							vtype: 'rangofecha', 
							campoInicioFecha: 'Txtfchini1',
							margins: '0 20 0 0'
						}
					]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'ID Orden ',
				combineErrors: false,
				msgTarget: 'side',
				minChars : 1,
				triggerAction : 'all',
				items: [				
					{
						xtype: 'textfield',
						name: 'numeroOrden',
						id: 'tfNumeroOrden',
						allowBlank: true,
						maxLength: 30,
						width:150 ,
						mode: 'local',
						autoLoad: false,
						displayField: 'descripcion',			
						valueField: 'clave',
						hiddenName : 'numeroOrden',
						forceSelection : true,
						msgTarget: 'side'// para que se muestre el circulito de error
					}
				]	
			},
			{ 
				xtype:   'label',  
				html:		leyenda, 
				cls:		'x-form-item', 
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				} 
			}		
			];
			
		var fp = new Ext.form.FormPanel({
			id: 'forma',
			width: 500,
			title: 'Criterios de B�squeda',
			frame: true,
			collapsible: true,
			titleCollapse: false,
			style: 'margin:0 auto;',
			bodyStyle: 'padding: 6px',
			labelWidth: 170,
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			items: elementosForma,			
			monitorValid: true,
			buttons: [
				{
					text: 'Buscar',
					iconCls: 'icoBuscar',
					formBind: true,
					handler: function(boton, evento) {
						
						var ventana = Ext.getCmp('winTotales');
						if (ventana) {
							ventana.destroy();
						}
					var Txtfchini = Ext.getCmp("Txtfchini1");
					var Txtffin = Ext.getCmp("Txtffin1");					
					if (!Ext.isEmpty(Txtfchini.getValue())  && Ext.isEmpty(Txtffin.getValue()) ) {
						Txtfchini.markInvalid('Debe capturar ambas fechas de  operaci�n   o dejarlas en blanco');	
						Txtffin.markInvalid('Debe capturar ambas fechas de operaci�n  o dejarlas en blanco');	
						return;
					}	
					if (Ext.isEmpty(Txtfchini.getValue())  && !Ext.isEmpty(Txtffin.getValue()) ) {
						Txtfchini.markInvalid('Debe capturar ambas fechas de  operaci�n   o dejarlas en blanco');	
						Txtffin.markInvalid('Debe capturar ambas fechas de operaci�n  o dejarlas en blanco');	
						return;
					}
					var distri = Ext.getCmp("ic_pyme1");
					if (Ext.isEmpty(distri.getValue()) ) {
							distri.markInvalid('Por favor, especifique el Distribuidor');
							return;
					}
					formulario = fp.getForm().getValues();
					fp.el.mask('Enviando...', 'x-mask-loading');
						consultaData.load({
							params: Ext.apply(/*fp.getForm().getValues(),*/{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15
							})
						});
							
						//Ext.getCmp('btnXPaginaPDF').enable();
						//Ext.getCmp('btnBajarXPDF').hide();
						Ext.getCmp('btnImprimirTPDF').enable();
						//Ext.getCmp('btnBajarTPDF').hide();
						Ext.getCmp('btnGenerarArchivo').enable();
						//Ext.getCmp('btnBajarCSV').hide();
						Ext.getCmp('btnTotales').enable();
					}
				},
				{
					text: 'Limpiar',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '24consulta07Ext.jsp';
					}
				}
			]			
		});

	var ctexto3 = new Ext.form.FormPanel({		
		id: 'forma3',
		width: 943,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: texto3.join('')			
	});
	var ctexto4 = new Ext.form.FormPanel({		
		id: 'forma4',
		width: 400,			
		frame: true,
		style: 'margin:0 auto;',
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: texto4.join('')			
	});

	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			applyTo: 'areaContenido',
			width: 949,
			items: [
				fp,
				NE.util.getEspaciador(20),
				grid,
				NE.util.getEspaciador(20),
				ctexto4,
				NE.util.getEspaciador(20),
				ctexto3,
				NE.util.getEspaciador(20)
			]
		});

		catalogoPYMEData.load();
		catalogoIFData.load();

	fp.el.mask('Cargando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24consulta07Ext.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});

});