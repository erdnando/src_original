<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.sql.*, java.math.*,
	com.netro.distribuidores.*,com.netro.exception.*, javax.naming.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
CreaArchivo archivo = new CreaArchivo();

String informacion   			=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String tipo_credito   			= 	(request.getParameter("tipo_credito")!=null)?request.getParameter("tipo_credito"):"";

String dist					= "";
String numDocto			= "";
String numAcuseCarga		= "";
String fechaEmision		= "";
String fechaPublicacion	= "";
String fechaVencimiento	= "";
String plazoDocto			= "";
String moneda 				= "";
String icMoneda			= "";
double monto 				= 0;
String tipoConversion	= "";
double tipoCambio			= 0;
double montoValuado		= 0;
String categoria			= "";
String plazoDescuento	= "";
String porcDescuento		= "";
double montoDescontar	= 0;
String modoPlazo			= "";
String estatus 			= "";
String icDocumento		= "";
String intermediario		= "";
String tipoCredito		= "";
String fechaOperacion	= "";
double montoCredito		= 0;
String plazoCredito		= "";
String fechaVencCredito	= "";
String referenciaTasaInt= "";
//double valorTasaInt		= 0;
String valorTasaInt		= "";
//double montoTasaInt 		= 0;
String montoTasaInt 		= "";
String tipoCobroInt		= ""; 
String monedaLinea		= "";
String bandeVentaCartera= "";
String cuenta_Bancaria ="";

String comisionAplicable = "";
String montoComision = "";
String montoDepositar = "";
String bins = "";

String idOrdenEnviado= "";
String idOperacion = "";
String codigoAutorizacion = "";
String fechaRegistro = "";
String numTarjeta	= "";

try {
	if(informacion.equals("ArchivoXpaginaPDF")  || informacion.equals("ArchivoBXpaginaPDF") || informacion.equals("ArchivoTotalPDF") || informacion.equals("ArchivoBTotalPDF")){
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
		int start = 0;
		int limit = 0;
		int nRow = 0;
		Registros reg = new Registros();
//Inicia creacion de archivo*-*-*-*-*-*-*-*-*-*-*-*-
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);

		if (informacion.equals("ArchivoXpaginaPDF") || informacion.equals("ArchivoTotalPDF")){
			CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsInfDocEpoDist());
			queryHelper.setMultiplesPaginadoresXPagina(true);
			
			if (informacion.equals("ArchivoXpaginaPDF"))	{	//-*-*-*-*-*-*-	Obtiene los registros por pagina del primer grid
				try {
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));
				} catch(Exception e) {
					throw new AppException("Error en los parametros recibidos", e);
				}
				reg = queryHelper.getPageResultSet(request,start,limit);
			/***************************/	/* Generacion del archivo *//***************************/
				while (reg.next()) {
					dist 					= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
					numDocto				= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
					numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
					fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
					fechaPublicacion	= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
					fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
					plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
					moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
					monto 				= Double.parseDouble(reg.getString("FN_MONTO"));
					tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
					tipoCambio			= Double.parseDouble(reg.getString("TIPO_CAMBIO"));
					plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
					porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
					montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
					montoValuado		= (monto-montoDescontar)*tipoCambio;
					modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
					estatus 				= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
					icMoneda				= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
					icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
					monedaLinea			= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
					categoria			= (reg.getString("CATEGORIA")==null)?"":reg.getString("CATEGORIA");
					bandeVentaCartera		= (reg.getString("CG_VENTACARTERA")==null)?"":reg.getString("CG_VENTACARTERA");
					
					
					if (bandeVentaCartera.equals("S") ) {
						modoPlazo ="";
					}
					if (tipo_credito.equals("F") ) {
						cuenta_Bancaria		= (reg.getString("CUENTA_BANCARIA")==null)?"":reg.getString("CUENTA_BANCARIA");
					}
					
					if(nRow == 0){
						int numCols = (!"".equals(tipoConversion)?18:15);
		
						float widths[];
						if(!"".equals(tipoConversion)){
							widths = new float[]{5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,6.0f,6.5f,5.5f,5.0f,5.0f,5.0f,5.5f,5.5f,5.5f,5.5f,5.5f};
						}else{
							widths = new float[]{6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,7.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f};
						}
						if (tipo_credito.equals("F") ) {  numCols =numCols+1; } 
			
						pdfDoc.setTable(numCols,100);
						pdfDoc.setCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setCell("Dist.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Emisión","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo Docto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Categoría","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo Descuento Días","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("%   Descuento","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto % Descuento","celda01rep",ComunesPDF.CENTER);
						if(!"".equals(tipoConversion)){
							pdfDoc.setCell("Tipo Conv.","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Tipo Cambio","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto Valuado","celda01rep",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("Modalidad de Plazo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
						if (tipo_credito.equals("F") ) {
						pdfDoc.setCell("Cuenta Bancaria Pyme","celda01rep",ComunesPDF.CENTER);
						}
					}
				/*************************/ /* Contenido del archivo */ /**************************/
					pdfDoc.setCell(dist.replace(',',' '),"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(numDocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(numAcuseCarga,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaEmision,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaPublicacion,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.RIGHT);
					pdfDoc.setCell(categoria,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(plazoDescuento,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(porcDescuento+"%","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formasrep",ComunesPDF.RIGHT);
					
					if(!"".equals(tipoConversion)){
						pdfDoc.setCell(((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:""),"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(tipoCambio,2,false):""),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(montoValuado,2,false):""),"formasrep",ComunesPDF.RIGHT);
					}
					pdfDoc.setCell(modoPlazo,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(estatus,"formasrep",ComunesPDF.CENTER);
					if (tipo_credito.equals("F") ) {
						pdfDoc.setCell(cuenta_Bancaria,"formasrep",ComunesPDF.CENTER);
					}
					nRow++;
				} //fin del while
			}else if (informacion.equals("ArchivoTotalPDF"))	{	//*-*-*-*-	Obtine los registros completos del primer grid
				AccesoDB con = new AccesoDB();
				con.conexionDB();
				ResultSet	rs = null;
				rs = queryHelper.getCreateFile(request,con);
				while (rs.next()) {
					dist 					= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto				= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
					fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto 				= Double.parseDouble(rs.getString("FN_MONTO"));
					tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio			= Double.parseDouble(rs.getString("TIPO_CAMBIO"));
					plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
					montoValuado		= (monto-montoDescontar)*tipoCambio;
					modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus 				= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda				= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
					categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
					bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					
					
					if (bandeVentaCartera.equals("S") ) {
						modoPlazo ="";
					}
					if (tipo_credito.equals("F") ) {
						cuenta_Bancaria		= (rs.getString("CUENTA_BANCARIA")==null)?"":rs.getString("CUENTA_BANCARIA");
					}
					if(nRow == 0){
						int numCols = (!"".equals(tipoConversion)?18:15);
		
						float widths[];
						if(!"".equals(tipoConversion)){
							widths = new float[]{5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,6.0f,6.5f,5.5f,5.0f,5.0f,5.0f,5.5f,5.5f,5.5f,5.5f,5.5f};
						}else{
							widths = new float[]{6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,7.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f};
						}
						if (tipo_credito.equals("F") ) { numCols=numCols+1;  }
						
						pdfDoc.setTable(numCols,100);
						pdfDoc.setCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setCell("Distribuidor","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Emisión","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo Docto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Categoría","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo Descuento Días","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("%   Descuento","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto % Descuento","celda01rep",ComunesPDF.CENTER);
						if(!"".equals(tipoConversion)){
							pdfDoc.setCell("Tipo Conv.","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Tipo Cambio","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto Valuado","celda01rep",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("Modalidad de Plazo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
						
						if (tipo_credito.equals("F") ) {
							pdfDoc.setCell("Cuenta Bancaria Pyme","celda01rep",ComunesPDF.CENTER);
						}
					}
				/*************************/ /* Contenido del archivo */ /**************************/
					pdfDoc.setCell(dist.replace(',',' '),"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(numDocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(numAcuseCarga,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaEmision,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaPublicacion,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.RIGHT);
					pdfDoc.setCell(categoria,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(plazoDescuento,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(porcDescuento+"%","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formasrep",ComunesPDF.RIGHT);
					
					if(!"".equals(tipoConversion)){
						pdfDoc.setCell(((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:""),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(tipoCambio,2,false):""),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(montoValuado,2,false):""),"formasrep",ComunesPDF.RIGHT);
					}
					pdfDoc.setCell(modoPlazo,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(estatus,"formasrep",ComunesPDF.LEFT);
					if (tipo_credito.equals("F") ) {
						pdfDoc.setCell(cuenta_Bancaria,"formasrep",ComunesPDF.CENTER);
					}
					nRow++;
				} //fin del while
			}
			List vecTotales = queryHelper.getResultCount(request);
			if (nRow != 0 && vecTotales.size()>0) {
				int i = 0;
				int cp = 0;
				List vecColumnas = null;
				for(i=0;i<vecTotales.size();i++){
					vecColumnas = (List)vecTotales.get(i);
					pdfDoc.setCell("TOTAL "+vecColumnas.get(1).toString(),"celda01rep",ComunesPDF.CENTER,3);
					pdfDoc.setCell(vecColumnas.get(2).toString(),"celda01rep",ComunesPDF.LEFT);
					pdfDoc.setCell(" ","celda01rep",ComunesPDF.LEFT,4);
					if(!"".equals(tipoConversion)){
						cp = 10;
					}else{
						cp = 7;
					}
					if (tipo_credito.equals("F") ) {  cp =cp+1; } 
					pdfDoc.setCell("$"+Comunes.formatoDecimal(vecColumnas.get(3).toString(),2),"celda01rep",ComunesPDF.RIGHT,cp);
				}
			}
			if (nRow == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (informacion.equals("ArchivoBXpaginaPDF") || informacion.equals("ArchivoBTotalPDF")){
			CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new Cons2InfDocEpoDist());
			queryHelper.setMultiplesPaginadoresXPagina(true);
			String condicion = "";
			if (informacion.equals("ArchivoBXpaginaPDF"))	{	//-*-*-*-*-*-*-	Obtiene los registros por pagina del primer grid
				try {
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));
				} catch(Exception e) {
					throw new AppException("Error en los parametros recibidos", e);
				}
				reg = queryHelper.getPageResultSet(request,start,limit);
			/***************************/	/* Generacion del archivo *//***************************/
				while (reg.next()) {
					dist 					= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
					numDocto				= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
					numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
					fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
					fechaPublicacion	= (reg.getString("DF_CARGA")==null)?"":reg.getString("DF_CARGA");
					fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
					plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
					moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
					monto 				= Double.parseDouble(reg.getString("FN_MONTO"));
					tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
					tipoCambio			= Double.parseDouble(reg.getString("TIPO_CAMBIO"));
					plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
					porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
					montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
					montoValuado		= (monto-montoDescontar)*tipoCambio;
					modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
					estatus 				= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
					icMoneda				= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
					icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
					intermediario		= (reg.getString("NOMBRE_IF")==null)?"":reg.getString("NOMBRE_IF");
					tipoCredito 		= (reg.getString("TIPO_CREDITO")==null)?"":reg.getString("TIPO_CREDITO");
					fechaOperacion		= (reg.getString("DF_OPERACION_CREDITO")==null)?"":reg.getString("DF_OPERACION_CREDITO");
					montoCredito		= monto-montoDescontar;
					plazoCredito		= (reg.getString("PLAZO_CREDITO")==null)?"":reg.getString("PLAZO_CREDITO");
					fechaVencCredito	= (reg.getString("DF_VENC_CREDITO")==null)?"":reg.getString("DF_VENC_CREDITO");
					referenciaTasaInt	= (reg.getString("REF_TASA")==null)?"":reg.getString("REF_TASA");
					//valorTasaInt		= Double.parseDouble(reg.getString("FN_VALOR_TASA"));
					valorTasaInt		= (reg.getString("FN_VALOR_TASA"));
					//montoTasaInt 		= Double.parseDouble(reg.getString("FN_IMPORTE_INTERES"));
					montoTasaInt 		= (reg.getString("FN_IMPORTE_INTERES"));
					tipoCobroInt		= (reg.getString("TIPO_COBRO_INT")==null)?"":reg.getString("TIPO_COBRO_INT"); 
					monedaLinea			= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
					categoria			= (reg.getString("CATEGORIA")==null)?"":reg.getString("CATEGORIA");
					bandeVentaCartera	= (reg.getString("CG_VENTACARTERA")==null)?"":reg.getString("CG_VENTACARTERA");
					
					comisionAplicable		= (reg.getString("COMISION_APLICABLE")==null)?"":reg.getString("COMISION_APLICABLE");
					montoComision		= (reg.getString("MONTO_COMISION")==null)?"":reg.getString("MONTO_COMISION");
					montoDepositar		= (reg.getString("MONTO_DEPOSITAR")==null)?"":reg.getString("MONTO_DEPOSITAR");
					bins		= (reg.getString("BINS")==null)?"":reg.getString("BINS");
					
					idOrdenEnviado		= (reg.getString("ID_ORDEN_ENVIADO")==null)?"":reg.getString("ID_ORDEN_ENVIADO");
					idOperacion		= (reg.getString("ID_OPERACION")==null)?"":reg.getString("ID_OPERACION");
					codigoAutorizacion		= (reg.getString("CODIGO_AUTORIZACION")==null)?"":reg.getString("CODIGO_AUTORIZACION");
					fechaRegistro		= (reg.getString("FECHA_REGISTRO")==null)?"":reg.getString("FECHA_REGISTRO");
					
					String operaTajeta		= (reg.getString("OPERA_TARJETA")==null)?"":reg.getString("OPERA_TARJETA");
					numTarjeta		= (reg.getString("NUM_TC")==null)?"":reg.getString("NUM_TC");
					//Validaciones 
					//Si no es Operada TC
					if(!estatus.equals("Operada TC")){
						bins		= "N/A";
						comisionAplicable = "N/A";
						montoComision = "N/A";
						montoDepositar = "N/A";
						
						idOrdenEnviado = "N/A";
						idOperacion = "N/A";
						codigoAutorizacion = "N/A";
						fechaRegistro = "N/A";
						numTarjeta	=	"N/A";
					}else{
					//if(	operaTajeta.equals("S")	){
						fechaVencimiento = "N/A";
						plazoDocto = "N/A";
						plazoCredito = "N/A";
						fechaVencCredito = "N/A";
						valorTasaInt = "N/A";
						montoTasaInt = "N/A";
						referenciaTasaInt = "N/A";
					}
					
					
					if("F".equals(tipo_credito)){
					cuenta_Bancaria	= (reg.getString("CUENTA_BANCARIA")==null)?"":reg.getString("CUENTA_BANCARIA");
					}
					if (bandeVentaCartera.equals("S") ) {
						modoPlazo ="";
					}
					if(!icMoneda.equals(monedaLinea)){
						montoCredito	= montoValuado;
					}else {
						montoCredito	= monto-montoDescontar;
					}		    		
					if(nRow == 0){
						int numCols = 18;
						float widths[];
						if(!"".equals(tipoConversion)){
							numCols = 20;
							widths = new float[]{1f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,6.0f,6.5f,5.0f,5.0f,5.0f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f};
						}else{
							numCols = 18;
							widths = new float[]{1f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f};
						}
						if("F".equals(tipo_credito)){
							numCols++;
						}		
						
						pdfDoc.setTable(numCols,100);
						pdfDoc.setCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Distribuidor","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Emisión","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo Docto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Categoría","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo Descuento Días","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("%   Descuento","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto % Descuento","celda01rep",ComunesPDF.CENTER);
						if(!"".equals(tipoConversion)){
							pdfDoc.setCell("Tipo Conv.","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Tipo Cambio","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto Valuado","celda01rep",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("Modalidad de  Plazo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo Cred.","celda01rep",ComunesPDF.CENTER);
						if("F".equals(tipo_credito)){
						pdfDoc.setCell("Cuenta Bancaria Pyme","celda01rep",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER);
		
						pdfDoc.setCell("Datos del Documento Final","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);						
						pdfDoc.setCell("No. Docto Final","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
						
						pdfDoc.setCell("% Comisión Aplicable\nde Terceros","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto Comisión de Terceros (IVA Incluido)","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto a Depositar\npor\nOperacion","celda01rep",ComunesPDF.CENTER);
						
						if(!"F".equals(tipo_credito)){
							pdfDoc.setCell("Plazo","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha Oper","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("IF","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Nombre del Producto","celda01rep",ComunesPDF.CENTER);
							
							pdfDoc.setCell("Ref. Tasa Interés","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Valor Tasa Interés","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto Interés","celda01rep",ComunesPDF.CENTER);
							
							pdfDoc.setCell("ID Orden enviado","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Respuesta de Operación","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Código Autorización","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Número Tarjeta de Crédito","celda01rep",ComunesPDF.CENTER);
						}else if("F".equals(tipo_credito)){
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							
							
						}					
						if(!"".equals(tipoConversion)){
							//pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						}
						//pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						//pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						//pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						//pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						//pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						//pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						//pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						if("F".equals(tipo_credito)){
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						}
					}
				/*************************/ /* Contenido del archivo */ /**************************/
					pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell(dist.replace(',',' '),"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(numDocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(numAcuseCarga,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaEmision,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaPublicacion,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.RIGHT);
					pdfDoc.setCell(categoria,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(plazoDescuento,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(porcDescuento+"%","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formasrep",ComunesPDF.RIGHT);
					
					if(!"".equals(tipoConversion)){
						pdfDoc.setCell(((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:""),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(tipoCambio,2,false):""),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(montoValuado,2,false):""),"formasrep",ComunesPDF.RIGHT);
					}
					pdfDoc.setCell(modoPlazo,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(estatus,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(tipoCredito,"formasrep",ComunesPDF.LEFT);
					
					if("F".equals(tipo_credito)){
						pdfDoc.setCell(cuenta_Bancaria,"formasrep",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("","formasrep",ComunesPDF.LEFT);///////////////////////??????????????????????????????????????
					
					pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
					if(!estatus.equals("Negociable")){						
						pdfDoc.setCell(icDocumento,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(montoCredito,2),"formasrep",ComunesPDF.RIGHT);
						
						/*pdfDoc.setCell(Comunes.formatoDecimal(comisionAplicable,2)+"%","formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(montoComision,2),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDepositar,2),"formasrep",ComunesPDF.RIGHT);
						*/
						pdfDoc.setCell((comisionAplicable!="N/A")?Comunes.formatoDecimal(comisionAplicable,2)+"%":comisionAplicable,"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell((montoComision!="N/A")?"$"+Comunes.formatoDecimal(montoComision,2):montoComision,"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell((montoDepositar!="N/A")?"$"+Comunes.formatoDecimal(montoDepositar,2):montoDepositar,"formasrep",ComunesPDF.RIGHT);
						
						if(!"F".equals(tipo_credito)){
							pdfDoc.setCell(plazoCredito,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaVencCredito,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaOperacion,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(intermediario,"formasrep",ComunesPDF.LEFT);
							pdfDoc.setCell(bins,"formasrep",ComunesPDF.CENTER);
							
							pdfDoc.setCell(referenciaTasaInt.replace(',', '.'),"formasrep",ComunesPDF.LEFT);
							//pdfDoc.setCell("$"+Comunes.formatoDecimal(valorTasaInt,2),"formasrep",ComunesPDF.RIGHT);
							//pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTasaInt,2),"formasrep",ComunesPDF.RIGHT);
							pdfDoc.setCell((valorTasaInt!="N/A")?"$"+Comunes.formatoDecimal(valorTasaInt,2):valorTasaInt,"formasrep",ComunesPDF.RIGHT);
							pdfDoc.setCell((montoTasaInt!="N/A")?"$"+Comunes.formatoDecimal(montoTasaInt,2):montoTasaInt,"formasrep",ComunesPDF.RIGHT);
							
							pdfDoc.setCell(idOrdenEnviado,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(idOperacion,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(codigoAutorizacion,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell((numTarjeta.trim()!="" && numTarjeta.trim()!="N/A")?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta,"formasrep",ComunesPDF.CENTER);
							
						}else if("F".equals(tipo_credito)){
							pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						}
					}else{
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);						
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);	
					}					
					if(!"".equals(tipoConversion)){
					//	pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					}
					//pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					//pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					//pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					//pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					//pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					//pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					//pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);					
					if("F".equals(tipo_credito)){
						pdfDoc.setCell("","formasrep",ComunesPDF.CENTER);
					}
					nRow++;
				} //fin del while
			}else if (informacion.equals("ArchivoBTotalPDF"))	{	//*-*-*-*-	Obtine los registros completos del primer grid
				AccesoDB con = new AccesoDB();
				con.conexionDB();
				ResultSet	rs = null;
				rs = queryHelper.getCreateFile(request,con);
				while (rs.next()) {
					dist 					= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto				= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
					fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto 				= Double.parseDouble(rs.getString("FN_MONTO"));
					tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio			= Double.parseDouble(rs.getString("TIPO_CAMBIO"));
					plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
					montoValuado		= (monto-montoDescontar)*tipoCambio;
					modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus 				= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda				= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					intermediario		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					tipoCredito 		= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					fechaOperacion		= (rs.getString("DF_OPERACION_CREDITO")==null)?"":rs.getString("DF_OPERACION_CREDITO");
					montoCredito		= monto-montoDescontar;
					plazoCredito		= (rs.getString("PLAZO_CREDITO")==null)?"":rs.getString("PLAZO_CREDITO");
					fechaVencCredito	= (rs.getString("DF_VENC_CREDITO")==null)?"":rs.getString("DF_VENC_CREDITO");
					referenciaTasaInt	= (rs.getString("REF_TASA")==null)?"":rs.getString("REF_TASA");
					//valorTasaInt		= Double.parseDouble( (rs.getString("FN_VALOR_TASA")==null)?"0":rs.getString("FN_VALOR_TASA")  );  
					valorTasaInt		= ( (rs.getString("FN_VALOR_TASA")==null)?"0":rs.getString("FN_VALOR_TASA")  );  
					//montoTasaInt 		= Double.parseDouble( (rs.getString("FN_IMPORTE_INTERES")==null)?"0":rs.getString("FN_IMPORTE_INTERES")  );
					montoTasaInt 		= ( (rs.getString("FN_IMPORTE_INTERES")==null)?"0":rs.getString("FN_IMPORTE_INTERES")  );
					tipoCobroInt		= (rs.getString("TIPO_COBRO_INT")==null)?"0":rs.getString("TIPO_COBRO_INT"); 
					monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
					categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
					
					comisionAplicable		= (rs.getString("COMISION_APLICABLE")==null)?"":rs.getString("COMISION_APLICABLE"); 				montoComision		= (rs.getString("MONTO_COMISION")==null)?"":rs.getString("MONTO_COMISION");
					montoDepositar		= (rs.getString("MONTO_DEPOSITAR")==null)?"":rs.getString("MONTO_DEPOSITAR");
					bins		= (rs.getString("BINS")==null)?"":rs.getString("BINS");
					
					idOrdenEnviado		= (rs.getString("ID_ORDEN_ENVIADO")==null)?"":rs.getString("ID_ORDEN_ENVIADO");
					idOperacion		= (rs.getString("ID_OPERACION")==null)?"":rs.getString("ID_OPERACION");
					codigoAutorizacion		= (rs.getString("CODIGO_AUTORIZACION")==null)?"":rs.getString("CODIGO_AUTORIZACION");
					fechaRegistro		= (rs.getString("FECHA_REGISTRO")==null)?"":rs.getString("FECHA_REGISTRO");
					
					String operaTajeta		= (rs.getString("OPERA_TARJETA")==null)?"":rs.getString("OPERA_TARJETA");
					numTarjeta		= (rs.getString("NUM_TC")==null)?"":rs.getString("NUM_TC");
					//Validaciones 
					//Si no es Operada TC
					if(!estatus.equals("Operada TC")){
						bins		= "N/A";
						comisionAplicable = "N/A";
						montoComision = "N/A";
						montoDepositar = "N/A";
						
						idOrdenEnviado = "N/A";
						idOperacion = "N/A";
						codigoAutorizacion = "N/A";
						fechaRegistro = "N/A";
						numTarjeta = "N/A";
					}else{
					//if(	operaTajeta.equals("S")	){
						fechaVencimiento = "N/A";
						plazoDocto = "N/A";
						plazoCredito = "N/A";
						fechaVencCredito = "N/A";
						valorTasaInt = "N/A";
						montoTasaInt = "N/A";
						referenciaTasaInt = "N/A";
					}
					
					if("F".equals(tipo_credito)){
					cuenta_Bancaria	= (rs.getString("CUENTA_BANCARIA")==null)?"":rs.getString("CUENTA_BANCARIA");
					}
					if(!icMoneda.equals(monedaLinea))
						montoCredito	= montoValuado;
					else 
						montoCredito	= monto-montoDescontar;
							bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					
					
					if (bandeVentaCartera.equals("S") ) {
						modoPlazo ="";
					}
					if(nRow == 0){
						int numCols = 18;
						float widths[];
						if(!"".equals(tipoConversion)){
							numCols = 20;//????????????????????????
							widths = new float[]{1f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,6.0f,6.5f,5.0f,5.0f,5.0f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f};
						}else{
							numCols = 18;
							widths = new float[]{1f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f};
						}
						if("F".equals(tipo_credito)){
							numCols++;
						}
						pdfDoc.setTable(numCols,100);
						pdfDoc.setCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Distribuidor","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Emisión","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo Docto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Categoría","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo Descuento Días","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("%   Descuento","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto % Descuento","celda01rep",ComunesPDF.CENTER);
						if(!"".equals(tipoConversion)){
							pdfDoc.setCell("Tipo Conv.","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Tipo Cambio","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto Valuado","celda01rep",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("Modalidad de Plazo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo Cred.","celda01rep",ComunesPDF.CENTER);
						if("F".equals(tipo_credito)){
						pdfDoc.setCell("Cuenta Bancaria Pyme","celda01rep",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER);////////////
		
						pdfDoc.setCell("Datos del Documento Final","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
						
						pdfDoc.setCell("No. Docto Final","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
						
						pdfDoc.setCell("% Comisión Aplicable\nde Terceros ","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto Comisión de Terceros (IVA Incluido)","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto a Depositar\npor\nOperacion","celda01rep",ComunesPDF.CENTER);
						
						if(!"F".equals(tipo_credito)){
							pdfDoc.setCell("Plazo","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha Oper","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("IF","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Nombre del Producto","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Ref. Tasa Interés","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Valor Tasa Interés","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto Interés","celda01rep",ComunesPDF.CENTER);
							
							pdfDoc.setCell("ID Orden enviado","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Respuesta de Operación","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Código Autorización","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Número Tarjeta de Crédito","celda01rep",ComunesPDF.CENTER);
							
						}else if("F".equals(tipo_credito)){
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						}	
						
						
						if(!"".equals(tipoConversion)){
							//pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							//pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						}
						/*pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						*/
						if("F".equals(tipo_credito)){
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
						}
					}
				/*************************/ /* Contenido del archivo */ /**************************/
					pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell(dist.replace(',',' '),"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(numDocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(numAcuseCarga,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaEmision,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaPublicacion,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.RIGHT);
					pdfDoc.setCell(categoria,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(plazoDescuento,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(porcDescuento+"%","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formasrep",ComunesPDF.RIGHT);
					
					if(!"".equals(tipoConversion)){
						pdfDoc.setCell(((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:""),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(tipoCambio,2,false):""),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(montoValuado,2,false):""),"formasrep",ComunesPDF.RIGHT);
					}
					pdfDoc.setCell(modoPlazo,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(estatus,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setCell(tipoCredito,"formasrep",ComunesPDF.LEFT);
					if("F".equals(tipo_credito)){					
						pdfDoc.setCell(cuenta_Bancaria,"formasrep",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("","formasrep",ComunesPDF.LEFT);
					
					pdfDoc.setCell("B ","celda01rep",ComunesPDF.CENTER);
					if(!estatus.equals("Negociable")){						
						pdfDoc.setCell(icDocumento,"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(montoCredito,2),"formasrep",ComunesPDF.RIGHT);
						
						pdfDoc.setCell((comisionAplicable!="N/A")?Comunes.formatoDecimal(comisionAplicable,2)+"%":comisionAplicable,"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell((montoComision!="N/A")?"$"+Comunes.formatoDecimal(montoComision,2):montoComision,"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell((montoDepositar!="N/A")?"$"+Comunes.formatoDecimal(montoDepositar,2):montoDepositar,"formasrep",ComunesPDF.RIGHT);
						
						if(!"F".equals(tipo_credito)){
							pdfDoc.setCell(plazoCredito,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaVencCredito,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaOperacion,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(intermediario,"formasrep",ComunesPDF.LEFT);
							pdfDoc.setCell(bins,"formasrep",ComunesPDF.CENTER);
							
							pdfDoc.setCell(referenciaTasaInt.replace(',', '.'),"formasrep",ComunesPDF.LEFT);
							pdfDoc.setCell((valorTasaInt!="N/A")?"$"+Comunes.formatoDecimal(valorTasaInt,2):valorTasaInt,"formasrep",ComunesPDF.RIGHT);
							pdfDoc.setCell((montoTasaInt!="N/A")?"$"+Comunes.formatoDecimal(montoTasaInt,2):montoTasaInt,"formasrep",ComunesPDF.RIGHT);
							
							pdfDoc.setCell(idOrdenEnviado,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(idOperacion,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell(codigoAutorizacion,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setCell((numTarjeta.trim()!="" && numTarjeta.trim()!="N/A")?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta,"formasrep",ComunesPDF.CENTER);
							
						}else if("F".equals(tipo_credito)){
							pdfDoc.setCell("","formasrep",ComunesPDF.LEFT);
							pdfDoc.setCell("","formasrep",ComunesPDF.LEFT);
							pdfDoc.setCell("","formasrep",ComunesPDF.LEFT);
							pdfDoc.setCell("","formasrep",ComunesPDF.LEFT);
							pdfDoc.setCell("","formasrep",ComunesPDF.LEFT);
							pdfDoc.setCell("","formasrep",ComunesPDF.LEFT);	
							pdfDoc.setCell("","formasrep",ComunesPDF.LEFT);						
						}
					}else{
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);		
						
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					}
					
					if(!"".equals(tipoConversion)){
						//pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						//pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					}
					/*
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					*/
					if("F".equals(tipo_credito)){
						pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER);
					}
					nRow++;
				} //fin del while
			}
			List vecTotales = queryHelper.getResultCount(request);
			if (nRow != 0 && vecTotales.size()>0) {
				int i = 0;
				List vecColumnas = null;
				for(i=0;i<vecTotales.size();i++){
					vecColumnas = (List)vecTotales.get(i);
					pdfDoc.setCell("TOTAL "+vecColumnas.get(1).toString(),"celda01rep",ComunesPDF.LEFT,4);
					pdfDoc.setCell(vecColumnas.get(2).toString(),"celda01rep",ComunesPDF.LEFT);
					pdfDoc.setCell(" ","celda01rep",ComunesPDF.LEFT,4);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(vecColumnas.get(3).toString(),2),"celda01rep",ComunesPDF.RIGHT,2);
					if(!"".equals(tipoConversion)){
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.LEFT,3);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(vecColumnas.get(4).toString(),2),"celda01rep",ComunesPDF.RIGHT,3);
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.LEFT);
					}else{
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.LEFT,5);
					}
					if("F".equals(tipo_credito)){
						pdfDoc.setCell(" ","celda01rep",ComunesPDF.LEFT);
					}
						pdfDoc.setCell("$"+Comunes.formatoDecimal(vecColumnas.get(5).toString(),2),"celda01rep",ComunesPDF.RIGHT,2);//////////////////////////7
				}
			}
			if (nRow == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}
		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>