<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,java.text.*,
		com.netro.model.catalogos.CatalogoIFDistribuidores,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
///////
CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if (informacion.equals("valoresIniciales")){

	String condicion = cargaDocto.getTipoCreditoEpo(iNoCliente);
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("condicion", condicion);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoIFDistribuidor")){

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setClaveEpo(iNoCliente);
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta") || informacion.equals("ArchivoPDF") || informacion.equals("ArchivoCSV")){
	String fecha_hoy 		= new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String ic_if			= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
	String fecha_act		= (request.getParameter("fecha")==null)?"":request.getParameter("fecha");
	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	ConsLineaCreditoDist paginador = new ConsLineaCreditoDist();
	paginador.setClaveIf(ic_if);
	paginador.setClavePyme(iNoCliente);
	if (fecha_act != null && !fecha_act.equals("")){
		paginador.setFechaSolCred(fecha_act);
	}else{
		paginador.setFechaSolCred(fecha_hoy);
	}
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros reg = queryHelper.doSearch();

	if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				reg = queryHelper.doSearch();
				infoRegresar =  "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData() + "}";
				System.out.println("infoRegresar- - - - - "+infoRegresar.toString());
			}
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("ArchivoPDF")){
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}else if (informacion.equals("ArchivoCSV")){
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
}else if ( informacion.equals("obtenHistorico") || informacion.equals("ArchivoPDFHisto") ){

	String	folio			=	(request.getParameter("folio")==null)?"":request.getParameter("folio");
	String	tipo_credito=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
	Registros reg = cargaDocto.getHistoricoCredito(folio,tipo_credito);
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (informacion.equals("obtenHistorico")){
		if (reg != null){
			jsonObj.put("registros", reg.getJSONData());
		}
	}else if ( informacion.equals("ArchivoPDFHisto") ){
		if (reg != null){
			ConsLineaCreditoDist pag = new ConsLineaCreditoDist();
			String nombreArchivo = pag.crearPageCustomFile(request, reg, strDirectorioTemp, "PDF");
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>

