<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.anticipos.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	
	JSONObject jsonObj = new JSONObject();
	// variables de despliegue
	String tipoCreditoOperar			= 	"";
	String responsabilidadPagoInteres	= 	"";
	String modalidadPlazoOperar			= 	"";
	String tipoCobranza					= 	"";
	String esquemaPagoInteres			=	"";

//	String	tipoCreditoOperar			= 	"";
	String	diasMinimosDocCarga			=	"";
	String	diasMaximosDocCarga			=	"";
	String	tipoConversion				=	"";
	String	opPantallaControl			=	"";
	String	plazoDescuento				=	"";
	String	porcDescuento				=	"";
	String	proxVenc					=	"";
	String	strFechaVenc					=	"";
	
	System.out.println("informacion ------->"+informacion); 


if(informacion.equals("GenerarArchivo")  ) {
	
try {
	
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

	String tipoArchivo = (request.getParameter("tipoArchivo") != null) ? request.getParameter("tipoArchivo") : "";
	
	System.out.println("tipoArchivo ------->"+tipoArchivo); 
		
	if(tipoArchivo.equals("PDF")){
		
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";	
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	
		pdfDoc.addText("Mexico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		List info=cargaDocto.esquemasOperacion(iNoCliente);		
	 
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sección 1: Esquemas de operación","celda01",ComunesPDF.CENTER);
	
		  
		if(info.size()>0){  
			pdfDoc.setTable(3, 100);
			pdfDoc.setCell("Nombre del parámetro","celda01",ComunesPDF.CENTER);		
			pdfDoc.setCell("Tipo de dato","celda01",ComunesPDF.CENTER);		
			pdfDoc.setCell("Valores","celda01",ComunesPDF.CENTER);		
			
			tipoCreditoOperar			= info.get(0).toString();
	responsabilidadPagoInteres	= 	info.get(1).toString();
	tipoCobranza				= info.get(2).toString();
	esquemaPagoInteres			=	info.get(3).toString();
	modalidadPlazoOperar		=	info.get(4).toString();
		//TIPO_CREDITO_OPERAR
	pdfDoc.setCell("Tipo de crédito a operar" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("C" ,"formas",ComunesPDF.CENTER);	
							
		if(tipoCreditoOperar.equals("D"))		
			pdfDoc.setCell("Modalidad 1 (Riesgo Empresa de Primer Orden)" ,"formas",ComunesPDF.CENTER);	
		else if(tipoCreditoOperar.equals("C"))		
			pdfDoc.setCell("Modalidad 2 (Riesgo Distribuidor)" ,"formas",ComunesPDF.CENTER);	
			
		else if(tipoCreditoOperar.equals("A"))
		pdfDoc.setCell("Ambos" ,"formas",ComunesPDF.CENTER);	
		
		if(tipoCreditoOperar.equals("D")||tipoCreditoOperar.equals("A")){
		pdfDoc.setCell("Responsabilidad de Pago de Interés"+
													"(Solo si opera DM)" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("C" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell( responsabilidadPagoInteres ,"formas",ComunesPDF.CENTER);
		}
		pdfDoc.setCell("Modalidad de plazo a operar" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("C" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell( modalidadPlazoOperar ,"formas",ComunesPDF.CENTER);
				
		if(tipoCreditoOperar.equals("D")||tipoCreditoOperar.equals("A")){
			pdfDoc.setCell("Tipo de cobranza"+
													"(Sólo si opera DM)" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("C" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell( tipoCobranza ,"formas",ComunesPDF.CENTER);
		}
		pdfDoc.setCell("Esquema de pago de intereses" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("C" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell( esquemaPagoInteres ,"formas",ComunesPDF.CENTER);
			
			
			pdfDoc.addTable();
		}
		//Segunda tabla (parametros)
		info=cargaDocto.parametros(iNoCliente);
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sección 2: Parámetros","celda01",ComunesPDF.CENTER);
		if(info.size()>0){
			pdfDoc.setTable(3, 100);
			pdfDoc.setCell("Nombre del parámetro","celda01",ComunesPDF.CENTER);		
			pdfDoc.setCell("Tipo de dato","celda01",ComunesPDF.CENTER);		
			pdfDoc.setCell("Valores","celda01",ComunesPDF.CENTER);	
			
	diasMinimosDocCarga			= 	 info.get(0).toString();
	diasMaximosDocCarga			=  info.get(1).toString();
	tipoConversion				=  info.get(2).toString();
	opPantallaControl			= 	 info.get(3).toString();
	plazoDescuento				= 	 info.get(4).toString();
	porcDescuento				= 	 info.get(5).toString();
	proxVenc					= 	 info.get(6).toString();
	strFechaVenc				= 	 info.get(7).toString();
	
	pdfDoc.setCell("Plazo mínimo del financiamiento" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("N" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell( diasMinimosDocCarga ,"formas",ComunesPDF.CENTER);
		
		pdfDoc.setCell("Plazo máximo del financiamiento" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("N" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell( diasMaximosDocCarga ,"formas",ComunesPDF.CENTER);
		
		
		pdfDoc.setCell("Días Máximos para Ampliar la Fecha de Vencimiento" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("N" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell( strFechaVenc ,"formas",ComunesPDF.CENTER);
		
		pdfDoc.setCell("Tipo de conversión" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("C" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell( tipoConversion ,"formas",ComunesPDF.CENTER);
		
		pdfDoc.setCell("Operar con pantalla de cifras de control" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("C" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell( opPantallaControl ,"formas",ComunesPDF.CENTER);
		
			pdfDoc.setCell("Plazo máximo para tomar el descuento,\n Porcentaje de descuento que otorga la EPO" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("N" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo: "+plazoDescuento+" Porcentaje: "+porcDescuento+" %" ,"formas",ComunesPDF.CENTER);
		
			pdfDoc.setCell("Aviso de próximos vencimientos en:" ,"formas",ComunesPDF.CENTER);	
		pdfDoc.setCell("N" ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell( proxVenc +" días hábiles" ,"formas",ComunesPDF.CENTER);
	
		pdfDoc.addTable();
		}
	pdfDoc.endDocument();		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	
	}
	
	if(tipoArchivo.equals("CSV")){
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo="";
	
	List info=cargaDocto.esquemasOperacion(iNoCliente);
		
		contenidoArchivo.append("Sección 1: Esquemas de operación"+"\n"); 				
		contenidoArchivo.append("  "+"\n"); 
 
		if(info.size()>0){
			
			contenidoArchivo.append("Nombre del parámetro,Tipo de dato , Valores"+"\n");
		
					
					tipoCreditoOperar			= info.get(0).toString();
	responsabilidadPagoInteres	= 	info.get(1).toString();
	tipoCobranza				= info.get(2).toString();
	esquemaPagoInteres			=	info.get(3).toString();
	modalidadPlazoOperar		=	info.get(4).toString();
		//TIPO_CREDITO_OPERAR
		contenidoArchivo.append("Tipo de crédito a operar"+","); 				
		contenidoArchivo.append("C"+","); 				
									
		if(tipoCreditoOperar.equals("D"))		
			contenidoArchivo.append("Modalidad 1 (Riesgo Empresa de Primer Orden)" +"\n");	
		else if(tipoCreditoOperar.equals("C"))		
			contenidoArchivo.append("Modalidad 2 (Riesgo Distribuidor)" +"\n");	
		else if(tipoCreditoOperar.equals("A"))
		contenidoArchivo.append("Ambos" +"\n");	
		
		
		if(tipoCreditoOperar.equals("D")||tipoCreditoOperar.equals("A")){
		contenidoArchivo.append("Responsabilidad de Pago de Interés"+
													"(Solo si opera DM)"+",");	
		contenidoArchivo.append("C"+",");
		contenidoArchivo.append( responsabilidadPagoInteres +"\n");
		}
		contenidoArchivo.append("Modalidad de plazo a operar"+",");	
		contenidoArchivo.append("C"+",");
		contenidoArchivo.append( modalidadPlazoOperar+"\n");
				
		if(tipoCreditoOperar.equals("D")||tipoCreditoOperar.equals("A")){
			contenidoArchivo.append("Tipo de cobranza"+
													"(Sólo si opera DM)"+",");	
		contenidoArchivo.append("C"+",");
		contenidoArchivo.append( tipoCobranza +"\n");
		}
		contenidoArchivo.append("Esquema de pago de intereses"+",");	
		contenidoArchivo.append("C"+",");
		contenidoArchivo.append( esquemaPagoInteres+"\n");
			
			
		}
		contenidoArchivo.append("  "+"\n"); 
		contenidoArchivo.append("  "+"\n"); 
		//Generar tabla de parametros
		info=cargaDocto.parametros(iNoCliente);
		contenidoArchivo.append("Sección 2: Parámetros"+"\n"); 				
		contenidoArchivo.append("  "+"\n"); 
		if(info.size()>0){
		
		contenidoArchivo.append("Nombre del parámetro,Tipo de dato , Valores"+"\n");
		
		diasMinimosDocCarga			= 	 info.get(0).toString();
	diasMaximosDocCarga			=  info.get(1).toString();
	tipoConversion				=  info.get(2).toString();
	opPantallaControl			= 	 info.get(3).toString();
	plazoDescuento				= 	 info.get(4).toString();
	porcDescuento				= 	 info.get(5).toString();
	proxVenc					= 	 info.get(6).toString();
	strFechaVenc				= 	 info.get(7).toString();
	
	contenidoArchivo.append("Plazo mínimo del financiamiento"+",");	
		contenidoArchivo.append("N"+",");
		contenidoArchivo.append( diasMinimosDocCarga+ "\n");
		
		contenidoArchivo.append("Plazo máximo del financiamiento" +",");	
		contenidoArchivo.append("N" +",");
		contenidoArchivo.append( diasMaximosDocCarga +"\n");
		
		
		contenidoArchivo.append("Días Máximos para Ampliar la Fecha de Vencimiento" +",");	
		contenidoArchivo.append("N"+",");
		contenidoArchivo.append( strFechaVenc +"\n");
		
		contenidoArchivo.append("Tipo de conversión" +",");	
		contenidoArchivo.append("C"+",");
		contenidoArchivo.append( tipoConversion +"\n");
		
		contenidoArchivo.append("Operar con pantalla de cifras de control"+",");	
		contenidoArchivo.append("C"+",");
		contenidoArchivo.append( opPantallaControl+"\n");
		
			contenidoArchivo.append("Plazo máximo para tomar el descuento Porcentaje de descuento que otorga la EPO"+",");	
		contenidoArchivo.append("N"+",");
		contenidoArchivo.append("Plazo: "+plazoDescuento+"  Porcentaje: "+porcDescuento+" %" +"\n");
		
			contenidoArchivo.append("Aviso de próximos vencimientos en:"+",");	
		contenidoArchivo.append("N" +",");
		contenidoArchivo.append( proxVenc +" días hábiles"+"\n");
	
	
	
		}
		
		
		
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}	


} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
		
}
	
%>
<%=jsonObj%>