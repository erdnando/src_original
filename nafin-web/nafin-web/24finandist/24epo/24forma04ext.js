var dt = new Date();
Ext.onReady(function() {

	var jsonValoresIniciales = null;
	var tipoConversion = null;
	function procesaValoresIniciales(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaConsulta(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				tipoConversion = infoR.tipoConversion;
				consultaData.loadData('');
				resumenTotalesData.loadData('');
				if (!grid.isVisible()){
					grid.show();
				}
				if (infoR.registros != undefined){
					consultaData.loadData(infoR.registros);
					resumenTotalesData.loadData(infoR.totales);
					var cm = grid.getColumnModel();
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
					cm.setHidden(cm.findColumnIndex('MONTO_VALUADO'), true);
					if (infoR.tipoConversion != undefined && infoR.tipoConversion)	{
						cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
						cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
						cm.setHidden(cm.findColumnIndex('MONTO_VALUADO'), false);
					}
					var cmbEstatus = Ext.getCmp('cmbEstatus');
					var nuevoEstatus = "";
					
					if(jsonValoresIniciales.perfil == 'EPO'){
						if (jsonValoresIniciales.NOnegociable == "S" ){
							nuevoEstatus ="4, 24";
						} else{
							nuevoEstatus ="4";
						}
					} else{
						if (cmbEstatus.getValue() == "1"){ //No Negociable
							nuevoEstatus ="24"; //No Negociable a Baja
						} else if (cmbEstatus.getValue() == "2"){ //Negociable
							nuevoEstatus ="4"; //Negociable a Baja
						} else if (cmbEstatus.getValue() == "28"){ //Pre Negociable
							nuevoEstatus ="37"; //Pre Negociable a Baja
						} else if (cmbEstatus.getValue() == "30"){ //Pendiente Negociable
							nuevoEstatus ="44"; //Pendiente Negociable a Baja
						}
					}

					catalogoNuevoEstatusData.load({params: Ext.apply({nuevoEstatus:nuevoEstatus})});
					grid.getGridEl().unmask();
					Ext.getCmp('btnTotales').enable();
					Ext.getCmp('lblEstatus').enable();
					Ext.getCmp('cboNuevo').enable();
					Ext.getCmp('btnCambiaEstatus').enable();
					Ext.getCmp('btnCancelar').enable();
				}else{
					grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					Ext.getCmp('btnTotales').disable();
					Ext.getCmp('lblEstatus').disable();
					Ext.getCmp('cboNuevo').disable();
					Ext.getCmp('cboNuevo').setRawValue('');
					Ext.getCmp('btnCambiaEstatus').disable();
					Ext.getCmp('btnCancelar').disable();
				}
			}
			
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesaCaptura = function (opts, success, response) {
		Ext.getCmp('btnCambiaEstatus').setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var cm = grid.getColumnModel();
			cm.setHidden(17, true);
			cm.setEditable(cm.findColumnIndex('FLAG_CAUSA'), false);
			cm.setHidden(cm.findColumnIndex('FLAG_CHECK'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VALUADO'), true);
			if (tipoConversion != null && tipoConversion)	{
				cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
				cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
				cm.setHidden(cm.findColumnIndex('MONTO_VALUADO'), false);
			}

			grid.getGridEl().unmask();
			fp.hide();
			grid.setTitle('<div align="center">Documento</div>');
			Ext.getCmp('btnTotales').hide();
			Ext.getCmp('lblEstatus').hide();
			Ext.getCmp('cboNuevo').hide();
			Ext.getCmp('btnCambiaEstatus').hide();
			Ext.getCmp('btnCancelar').setText('Regresar');
			var totalesACmp = Ext.getCmp('gridTotalesA');
			if (totalesACmp.isVisible()) {
				totalesACmp.hide();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarCatalogoNuevoEstatus = function(store, arrRegistros, opts){
		var jsonData = store.reader.jsonData;
		Ext.each(jsonData.registros, function(item,index,arrItem){
			if (item.clave == "24"){
				Ext.getCmp('cboNuevo').setValue("24");
			} else if (item.clave == "37"){
				Ext.getCmp('cboNuevo').setValue("37");
			} else if (item.clave == "44"){
				Ext.getCmp('cboNuevo').setValue("44");
			} else if(item.clave == "4"){
				Ext.getCmp('cboNuevo').setValue("4");
			}
		});
	}

	function procesarMuestraGridDetalle(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.datos_detalle != undefined)	{
				gridDetalleData.loadData(infoR.datos_detalle);
			}
			var ventana = Ext.getCmp('VerCambioDocumentos');
			if (ventana) {								
				ventana.show();
			} else {
				new Ext.Window({
					layout: 'fit',
					resizable: false,
					width: 800,
					height: 250,			
					modal:true,
					id: 'VerCambioDocumentos',
					closeAction: 'hide',
					items: [					
						gridDetalle
					],
					title: 'Cambios del documento'
				}).show();
			}
			var tot = gridDetalleData.getTotalCount();
			if (tot <= 0){
				gridDetalle.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
			}else {
				gridDetalle.getGridEl().unmask();
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var catalogoDisData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoDistribuidor'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '24forma04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoModoPlazoData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '24forma04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoModoPlazoDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoNuevoEstatusData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '24forma04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoNuevoEstatusDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNuevoEstatus,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name: 'CLAVE'},
			{name: 'PYME'},
			{name: 'NUMDOC'},
			{name: 'ACUSE'},
			{name: 'FECHA_EMISION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'FECHA_PUBLICACION',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'DF_FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'PLAZO_DOC'},
			{name: 'MONEDA'},
			{name: 'MONTO',		type: 'float'},
			{name: 'CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VALUADO'},
			{name: 'PLAZDESC'},
			{name: 'PORCDESC'},
			{name: 'MONTO_CON_DESC'},
			{name: 'MODALIDAD_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'NOESTATUS'},
			{name: 'CG_VENTACARTERA'},
			{name: 'FLAG_CAUSA'},
			{name: 'FLAG_CHECK'}
		],
		data:[{
			'PYME':'',
			'CLAVE':'',
			'ACUSE':'',
			'FECHA_EMISION':'',
			'FECHA_PUBLICACION':'',
			'DF_FECHA_VENC':'',
			'PLAZO_DOC':'',
			'MONEDA':'',
			'MONTO':'',
			'CONVERSION':'',
			'TIPO_CAMBIO':'',
			'MONTO_VALUADO':'',
			'PLAZDESC':'',
			'PORCDESC':'',
			'MONTO_CON_DESC':'',
			'MODALIDAD_PLAZO':'',
			'ESTATUS':'',
			'IC_MONEDA':'',
			'NOESTATUS':'',
			'CG_VENTACARTERA':''
		}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO_DOCUMENTOS'},
			{name: 'TOTAL_MONTO_DESCUENTO'},
			{name: 'TOTAL_MONTO_VALUADO'}
		],
		data:[{'MONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_DOCUMENTOS':'','TOTAL_MONTO_DESCUENTO':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var procesarCambioDocumentos = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_documento = registro.get('CLAVE');
		gridDetalleData.loadData('');
		Ext.Ajax.request({url: '24forma04ext.data.jsp',params: Ext.apply({informacion: "obtenDetalles",ic_docto:	ic_documento}),callback: procesarMuestraGridDetalle});
	}

	var gridDetalleData = new Ext.data.JsonStore({
		fields: [{name: 'FECHA_CAMBIO'},{name: 'FECHA_EMI_ANT'},{name: 'CT_CAMBIO_MOTIVO'},{name: 'FN_MONTO_ANTERIOR'},
					{name: 'FN_MONTO_NUEVO'},{name: 'FECHA_EMI_NEW'},{name: 'FECHA_VENC_ANT'},{name: 'FECHA_VENC_NEW'},
					{name: 'CD_DESCRIPCION'},{name: 'MODO_PLAZO_ANTERIOR'},{name: 'MODO_PLAZO_NUEVO'}],
		data: [{'FECHA_CAMBIO':'','FECHA_EMI_ANT':'','CT_CAMBIO_MOTIVO':'','FN_MONTO_ANTERIOR':'','FN_MONTO_NUEVO':'','FECHA_EMI_NEW':'',
				'FECHA_VENC_ANT':'','FECHA_VENC_NEW':'','CD_DESCRIPCION':'','MODO_PLAZO_ANTERIOR':'','MODO_PLAZO_NUEVO':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridDetalle = new Ext.grid.GridPanel({
		store: gridDetalleData,
		columns: [
			{header: 'Fecha del Cambio',             tooltip: 'Fecha del Cambio',             dataIndex: 'FECHA_CAMBIO',         width: 100,  align: 'center'},
			{header: 'Cambio Estatus',               tooltip: 'Cambio Estatus',               dataIndex: 'CT_CAMBIO_MOTIVO',     width: 150,  align: 'center'},
			{header: 'Fecha Emisi�n Anterior',       tooltip: 'Fecha Emisi�n Anterior',       dataIndex: 'FECHA_EMI_ANT',        width: 100,  align: 'center'},
			{header: 'Fecha Emisi�n Nueva',          tooltip: 'Fecha Emisi�n Nueva',          dataIndex: 'FECHA_EMI_NEW',        width: 100,  align: 'center'},
			{header: 'Monto Anterior',               tooltip: 'Monto Anterior',               dataIndex: 'FN_MONTO_ANTERIOR',    width: 100,  align: 'right' },
			{header: 'Monto Nuevo',                  tooltip: 'Monto Nuevo',                  dataIndex: 'FN_MONTO_NUEVO',       width: 100,  align: 'right' },
			{header: 'Fecha Vencimiento Anterior',   tooltip: 'Fecha Vencimiento Anterior',   dataIndex: 'FECHA_VENC_ANT',       width: 100,  align: 'center'},
			{header: 'Fecha Vencimiento Nuevo',      tooltip: 'Fecha Vencimiento Nuevo',      dataIndex: 'FECHA_VENC_NEW',       width: 100,  align: 'center'},
			{header: 'Modalidad de Plazo Anterior',  tooltip: 'Modalidad de Plazo Anterior',  dataIndex: 'MODO_PLAZO_ANTERIOR',  width: 130,  align: 'center'},
			{header: 'Modalidad de Plazo Nuevo',     tooltip: 'Modalidad de Plazo Nuevo',     dataIndex: 'MODO_PLAZO_NUEVO',     width: 130,  align: 'center'}
		],
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 150,
		width: 800,
		title: '<div align="center">Documento</div>',
		frame: false,
		hidden: false
	});

	var gridTotalesA = {
		xtype: 'grid',
		store: resumenTotalesData,
		id: 'gridTotalesA',
		hidden:	true,
		columns: [
			{
				header: 'TOTALES',
				dataIndex: 'NOMMONEDA',
				align: 'left',	width: 250
			},{
				header: 'Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},{
				header: 'Monto Documentos',
				dataIndex: 'TOTAL_MONTO_DOCUMENTOS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header: 'Monto Descuento',
				dataIndex: 'TOTAL_MONTO_DESCUENTO',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header: 'Monto Valuado',
				dataIndex: 'TOTAL_MONTO_VALUADO',
				width: 150,	align: 'right', hidden: true,
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			}
		],
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	var grid = new Ext.grid.EditorGridPanel({
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		columns: [
			{
				header: 'Distribuidor', tooltip: 'Distribuidor',
				dataIndex: 'PYME',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},{
				header : 'N�mero de documento inicial', tooltip: 'N�mero de documento inicial',
				dataIndex : 'NUMDOC',
				sortable : true, resizable: true,width : 140,	hidden: false
			},{
				header: 'N�m. acuse carga', tooltip: 'N�mero acuse carga',
				dataIndex: 'ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de publicaci�n', tooltip: 'Fecha de publicaci�n',
				dataIndex : 'FECHA_PUBLICACION', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_FECHA_VENC', align: 'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Plazo docto.', tooltip: 'Plazo docto.',	
				dataIndex : 'PLAZO_DOC',
				sortable : true, align: 'center',	width : 120,	hidden: false
			},{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'MONEDA',
				sortable : true,	width : 130,	hidden: false
			},{
				header : 'Monto', tooltip: 'Monto',	
				dataIndex : 'MONTO',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Plazo para descuento en d�as', tooltip: 'Plazo para descuento en d�as',
				dataIndex : 'PLAZDESC',
				sortable : true, align:'center',	width : 120,	hidden: false
			},{
				header : '% de Descuento',	tooltip: 'Porcentaje de Descuento', 
				dataIndex : 'PORCDESC',
				sortable : true,	width : 100, align: 'center', hidden: false
			},{
				header : 'Monto % de descuento', tooltip: 'Monto % de descuento',
				dataIndex : 'MONTO_CON_DESC',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: 	function(value, metaData, registro, rowIndex, colIndex, store) {
									return Ext.util.Format.number(registro.get('MONTO')*(registro.get("PORCDESC")/100), '$0,0.00');
								}
			},{
				header : 'Tipo conv.', tooltip: 'Tipo conv.',
				dataIndex : 'TIPO_CONVERSION',
				sortable : true,	width : 120,	hidden: true, hideable: false,
				renderer: 	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('IC_MONEDA') == 1)	{
										value = "";
									}
									return value;
								}
			},{
				header : 'Tipo cambio', tooltip: 'Tipo cambio',
				dataIndex : 'TIPO_CAMBIO',
				sortable : true,	width : 120,	hidden: true,  hideable: false,
				renderer	:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('IC_MONEDA') == 1)	{
										value = "";
									}
									return Ext.util.Format.number(value,'$0,0.00');
								}
			},{
				header : 'Monto valuado en pesos', tooltip: 'Monto valuado en pesos',
				dataIndex : 'MONTO_VALUADO',
				sortable : true,	width : 120,	hidden: true, hideable: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('IC_MONEDA') == 1)	{
										value = "";
									}
									return Ext.util.Format.number(value,'$0,0.00');
								}
			},{
				header : 'Modalidad de plazo', tooltip: 'Modalidad de plazo',
				dataIndex : 'MODALIDAD_PLAZO',
				sortable : true,	width : 120,	hidden: false, align: 'center',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
											if (registro.get("CG_VENTACARTERA") == "S")	{
												value = "";
											}
											return value;
										}
			},{
				header : 'Estatus', tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable : true,	width : 150, align: 'center',	hidden: false
			},{//17
				xtype: 'actioncolumn',
				header: 'Cambios del documento', tooltip: 'Cambios del documento',
				width: 130,align: 'center', hideable: false,
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	handler: procesarCambioDocumentos
					}
				]
			},{//18
				xtype: 'checkcolumn',
				header: 'Seleccionar', tooltip: 'Seleccionar', dataIndex: 'FLAG_CHECK',
				sortable : false,	width : 80,	align: 'center', hideable: false
			},{//19
				header : 'Observaciones', tooltip: 'Observaciones',
				dataIndex : 'FLAG_CAUSA',
				sortable : false,	width : 230,	hiddeable: false,
				editor:{
					xtype:	'textarea',
					id: 'txtArea',
					maxLength: 260,
					enableKeyEvents: true,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 260){
								field.setValue((field.getValue()).substring(0,259));
								Ext.Msg.alert('Observaciones','La causa del cambio de estatus no debe sobrepasar los 260 caracteres.');
								field.focus();
							}
						}
					}
				}
			}
		],
		stripeRows: true,
		clicksToEdit: 1,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Datos Documento Inicial',
		frame: true,
		listeners: {
			afteredit : function(e){
				var inx = e.row;
				var check = Ext.getCmp('chk'+inx.toString());
				if (check){
					chek.checked = true;
				}
				var record = e.record;
				record.commit();		
			}
		},
		bbar: {
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						var totalesACmp = Ext.getCmp('gridTotalesA');
						if (!totalesACmp.isVisible()) {
							totalesACmp.show();
							totalesACmp.el.dom.scrollIntoView();
						}
					}
				},'-','->','-',{
					xtype: 'label',
					id:'lblEstatus',
					text:'Estatus: '
				},{
					xtype: 'combo',
					name: 'nuevo_estatus',
					id: 'cboNuevo',
					allowBlank: false,
					width: 250,
					fieldLabel: 'Estatus',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'nuevo_estatus',
					emptyText: 'Seleccione estatus',
					forceSelection : false,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store : catalogoNuevoEstatusData,
					tpl : NE.util.templateMensajeCargaCombo
				},'-',{
					xtype: 'button',
					text: 'Cambiar Estatus',
					id: 'btnCambiaEstatus',
					handler: function(boton, evento) {
							var flag = false;
							var jsonData = consultaData.data.items;
							Ext.each(jsonData, function(item,index,arrItem){
								if (item.data.FLAG_CHECK){
									flag = true;
									return false;
								}
							});
							if (!flag){
								Ext.Msg.alert(boton.text,'Para poder cambiar el estatus debe seleccionar por lo menos uno');
								return;
							}
							if (!Ext.getCmp('cboNuevo').isValid()){
								Ext.Msg.alert(boton.text,'Debe seleccionar un estatus nuevo para los documentos seleccionados');
								return;
							}
							Ext.Msg.show({
								title:boton.text,
								msg: 'Este proceso cambiara el estatus de los documentos seleccionados\n�Desea continuar?',
								buttons: Ext.Msg.OKCANCEL,
								fn: processResult,
								animEl: 'elId',
								icon: Ext.MessageBox.QUESTION
							});
							boton.setIconClass('loading-indicator');
					}
				},'-',{	
					xtype: 'button',
					text: 'Cancelar',
					id: 'btnCancelar',
					handler: function(boton, evento) {
									window.location = '24forma04ext.jsp';
					}
				},'-'
			]
		}
	});

	function processResult(res){
		if (res == 'ok'){
			var jsonData = consultaData.data.items;
			var newData = [];
			var icDocumento = [];
			var causa = [];
			var selecc = [];
			Ext.each(jsonData, function(item,index,arrItem){
				if (item.data.FLAG_CHECK){
						item.data.ESTATUS = 'Baja';
						newData.push(item.data);
						icDocumento.push(item.data.CLAVE);
						causa.push(item.data.FLAG_CAUSA);
						selecc.push('ok');
				}
			});
			if (newData.length > 0) {
				consultaData.loadData(newData);
			}
			var comboNuevo = Ext.getCmp('cboNuevo');
			Ext.Ajax.request({
				url: '24forma04ext.data.jsp',
				params: Ext.apply({
					informacion: 'Captura',
					ic_documento: icDocumento,
					causa: causa,
					selecc:	selecc,
					nuevo_estatus:	comboNuevo.getValue()
				}),
				callback: procesaCaptura
			});
		}else{
			Ext.getCmp('btnCambiaEstatus').setIconClass('');
		}
	}

	var elementosForma = [
		{
			xtype: 'panel',
			layout:'column',
				items:[{
					xtype: 'panel',
					id:	'panelIzq',
					columnWidth:.5,
					defaults: {
						msgTarget: 'side',
						anchor: '100'
					},
					layout: 'form',
					width: 450,
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: 'Distribuidor',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'combo',
									name: 'distribuidor',
									id: 'cmbDis',
									allowBlank: true,
									//fieldLabel: 'Distribuidor',
									mode: 'local', 
									displayField : 'descripcion',
									valueField : 'clave',
									hiddenName : 'distribuidor',
									emptyText: 'Seleccione Distribuidor',
									forceSelection : true,
									triggerAction : 'all',
									typeAhead: true,
									minChars : 1,
									width: 300,
									store : catalogoDisData,
									tpl : NE.util.templateMensajeCargaCombo
								}
							]
					}
					,{
							xtype: 'compositefield',
							fieldLabel: 'Estatus',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'combo',
									name: 'estatusdocto',
									id: 'cmbEstatus',
									//fieldLabel: 'Estatus',
									emptyText: 'Seleccione Estatus',
									allowBlank: false,
									mode: 'local',
									displayField: 'descripcion',
									valueField: 'clave',
									hiddenName : 'estatusdocto',
									forceSelection : true,
									triggerAction : 'all',
									typeAhead: true,
									minChars : 1,
									width: 300,
									store: catalogoEstatusData,
									tpl : NE.util.templateMensajeCargaCombo
								}
							]
					},{
							xtype: 'compositefield',
							fieldLabel: 'N�mero de documento inicial',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'textfield',
									name: 'numdoc',
									id: 'no_doc_ini',
									width: 200,
									//fieldLabel: 'N�mero de documento inicial',
									allowBlank: true,
									maxLength: 13
								}
							]
					},{
							xtype: 'compositefield',
							fieldLabel: 'N�m. acuse de carga',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'textfield',
									name: 'numaccuse',
									id: 'no_cc_acuse',
									width: 200,
									allowBlank: true,
									maxLength: 15
								}
							]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha emisi�n docto.',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fechaemision1',
								id: 'dc_fecha_emisionMin',
								allowBlank: true,
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'dc_fecha_emisionMax',
								margins: '0 20 0 0' 
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 24
							},
							{
								xtype: 'datefield',
								name: 'fechaemision2',
								id: 'dc_fecha_emisionMax',
								allowBlank: true,
								startDay: 1,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoInicioFecha: 'dc_fecha_emisionMin',
								margins: '0 20 0 0'
							}
						]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha vencimiento docto.',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fechavenc1',
								id: 'dc_fecha_vencMin',
								allowBlank: true,
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'dc_fecha_vencMax',
								margins: '0 20 0 0'
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 24
							},
							{
								xtype: 'datefield',
								name: 'fechavenc2',
								id: 'dc_fecha_vencMax',
								allowBlank: true,
								startDay: 1,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'dc_fecha_vencMin',
								margins: '0 20 0 0'
							}
						]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha de publicaci�n',	
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fechapub1',
								id: 'fecha_publicaMin',
								value: dt.format('d/m/Y'),
								allowBlank: true,
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'fecha_publicaMax',
								margins: '0 20 0 0'
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 24
							},
							{
								xtype: 'datefield',
								name: 'fechapub2',
								id: 'fecha_publicaMax',
								value: dt.format('d/m/Y'),
								allowBlank: true,
								startDay: 1,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'fecha_publicaMin',
								margins: '0 20 0 0'
							}
						]
					}
					]
				},{
					xtype: 'panel',
					id:	'panelDer',
					columnWidth:.5,
					layout: 'form',
					defaults: {
						msgTarget: 'side',
						anchor: '100'
					},
					width: 440,
					items: [
					{
							xtype: 'compositefield',
							fieldLabel: 'Moneda',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'combo',
									name: 'moneda',
									width: 300,
									emptyText: 'Seleccione Moneda',	
									mode: 'local',
									displayField: 'descripcion',
									valueField: 'clave',
									hiddenName : 'moneda',
									forceSelection : true,
									triggerAction : 'all',
									typeAhead: true,
									minChars : 1,
									store: catalogoMonedaData,
									tpl : NE.util.templateMensajeCargaCombo
								}
							]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Monto docto. inicial',
						msgTarget: 'side',
						combineErrors: false,
						items: [
							{
								xtype: 'numberfield',
								name: 'monto1',
								id: 'montoMin',
								allowBlank: true,
								maxLength: 12,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangoValor',
								campoFinValor: 'montoMax',
								margins: '0 20 0 0'
							},
							{
								xtype: 'displayfield',
								value: 'a',
								width: 20
							},
							{
								xtype: 'numberfield',
								name: 'monto2',
								id: 'montoMax',
								allowBlank: true,
								maxLength: 12,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangoValor',
								campoInicioValor: 'montoMin',
								margins: '0 20 0 0'
							}
						]
					},{
							xtype: 'checkbox',
							fieldLabel:	'Monto % de descuento',
							value:	'S',
							name: 'cdescuento'
					},{
							xtype: 'compositefield',
							fieldLabel: 'Modalidad del plazo',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'combo',
									name: 'modplazo',
									id:	'cmbModalidad',
									width:300,
									emptyText: 'Seleccione modalidad de plazo',
									mode: 'local',
									displayField: 'descripcion',
									valueField: 'clave',
									hiddenName : 'modplazo',
									forceSelection : true,
									triggerAction : 'all',
									typeAhead: true,
									minChars : 1,
									store: catalogoModoPlazoData,
									tpl : NE.util.templateMensajeCargaCombo
								}
							]
					}
				]
			}]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 940,
		style: ' margin:0 auto;',
		title:	'Criterios de b�squeda',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					var totalesACmp = Ext.getCmp('gridTotalesA');
					if (totalesACmp.isVisible()) {
						totalesACmp.hide();
					}
					if(!verificaPanelIzq()) {
						return;
					}
					if(!verificaPanelDer()) {
						return;
					}
					var fechaEmisionMin = Ext.getCmp('dc_fecha_emisionMin');
					var fechaEmisionMax = Ext.getCmp('dc_fecha_emisionMax');
					var fechaVenceMin = Ext.getCmp('dc_fecha_vencMin');
					var fechaVenceMax = Ext.getCmp('dc_fecha_vencMax');
					var izq_montoMin = Ext.getCmp('montoMin');
					var izq_montoMax = Ext.getCmp('montoMax');
					var fechaPublicMin = Ext.getCmp('fecha_publicaMin');
					var fechaPublicMax = Ext.getCmp('fecha_publicaMax');
					var cmbEstatus = Ext.getCmp('cmbEstatus');
					if (!Ext.isEmpty(fechaEmisionMin.getValue()) || !Ext.isEmpty(fechaEmisionMax.getValue()) ) {
						if(Ext.isEmpty(fechaEmisionMin.getValue()))	{
							fechaEmisionMin.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
							fechaEmisionMin.focus();
							return;
						}else if (Ext.isEmpty(fechaEmisionMax.getValue())){
							fechaEmisionMax.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
							fechaEmisionMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaVenceMin.getValue()) || !Ext.isEmpty(fechaVenceMax.getValue()) ) {
						if(Ext.isEmpty(fechaVenceMin.getValue()))	{
							fechaVenceMin.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMin.focus();
							return;
						}else if (Ext.isEmpty(fechaVenceMax.getValue())){
							fechaVenceMax.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaPublicMin.getValue()) || !Ext.isEmpty(fechaPublicMax.getValue()) ) {
						if(Ext.isEmpty(fechaPublicMin.getValue()))	{
							fechaPublicMin.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
							fechaPublicMin.focus();
							return;
						}else if (Ext.isEmpty(fechaPublicMax.getValue())){
							fechaPublicMax.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
							fechaPublicMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(izq_montoMin.getValue()) || !Ext.isEmpty(izq_montoMax.getValue()) ) {
						if(Ext.isEmpty(izq_montoMin.getValue()))	{
							izq_montoMin.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							izq_montoMin.focus();
							return;
						}else if (Ext.isEmpty(izq_montoMax.getValue())){
							izq_montoMax.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							izq_montoMax.focus();
							return;
						}
					}
					// Este if solo aplica si el usuario es una EPO
					if (cmbEstatus.getValue() == 1 && jsonValoresIniciales.NOnegociable == 'N'){
						cmbEstatus.markInvalid('La EPO no se encuentra parametrizada para publicar documentos No Negociables');
						cmbEstatus.focus();
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');

					Ext.Ajax.request({
						url: '24forma04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{informacion: 'Consulta'}),
						callback: procesaConsulta
					});
				} //fin handler
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24forma04ext.jsp';
				}
			}
		]
	});

	function verificaPanelIzq(){
		var myPanel = Ext.getCmp('panelIzq');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

	function verificaPanelDer(){
		var myPanel = Ext.getCmp('panelDer');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid,		gridTotalesA
		]
	});

	fp.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24forma04ext.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});

	catalogoDisData.load();
	catalogoEstatusData.load();
	catalogoMonedaData.load();
	catalogoModoPlazoData.load();

});