<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
  com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	System.out.println("24consulta07Arext.jsp (E)");

	AccesoDB con = new AccesoDB();
	JSONObject jsonObj = new JSONObject();
	String informacion = (request.getParameter("informacion") == null)?"":request.getParameter("informacion");

	int start =0;
	int limit =0;
	//de despliegue
	String distribuidor		= "";
	String numdocto			= "";
	String acuse			= "";
	String fchemision		= "";
	String fchpublica		= "";
	String fchvence			= "";
	String plazodocto		= "";
	String moneda			= "";
	String ic_moneda		= "";
	double monto			= 0;
	String tipoconversion	= "";
	String tipocambio		= "";
	double mntovaluado		= 0;
	String plzodescto		= "";
	String porcdescto		= "";
	double mntodescuento	= 0;
	String estatus			= "";
	String numcredito		= "";
	String descif			= "";
	String referencia		= "";
	String plazocred		= "";
	String fchvenccred		= "";
	String tipocobroint		= "";
	double montoint			= 0;
	String valortaza		= "";
	String tipoCredito		= "";
	double mntocredito		= 0;
	String fchopera			= "";
	String monedaLinea		= "";
	String modoPlazo		= "";
	String bandeVentaCartera ="";
	int registros =0;


	try {
	
		//Obtengo parametrizaciones  
		String tiposCredito= "", responsable ="", cgTipoConversion ="";			
		ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
		
		List datos  =   BeanParametro.obtieneParametrosAviso(iNoCliente);  
		responsable = (String)datos.get(0);
		tiposCredito = (String)datos.get(1);
		cgTipoConversion = (String)datos.get(2);
			
		CreaArchivo archivo = new CreaArchivo();
		
		CQueryHelper queryHelper = null;			
		con.conexionDB();
		AvNotifEPODist avNotifEPODist = new com.netro.distribuidores.AvNotifEPODist();
		queryHelper = new CQueryHelper(avNotifEPODist);
		queryHelper.cleanSession(request);
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		String nombreArchivoPDF ="";
		
		if(informacion.equals("ArchivoXPDF") ) {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}	
		}
		
		if(!queryHelper.readyForPaging(request)) {
				queryHelper.executePKQueryIfNecessary(request,con);
		}	
		if(informacion.equals("ArchivoTPDF")  || informacion.equals("ArchivoXPDF") ) {
			nombreArchivoPDF = archivo.nombreArchivo()+".pdf";
		
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivoPDF);
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			(session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);	
		
			int numCols = 17;	
		
	
			ResultSet	rs = null;
			if("ArchivoXPDF".equals(informacion)){
				rs = queryHelper.getPageResultSet(request,con,start,limit);						
			}else	if("ArchivoTPDF".equals(informacion)){
				rs = queryHelper.getCreateFile(request,con);				
			}
			while(rs.next()){
			
				ic_moneda = (rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda");	
				distribuidor = (rs.getString("DISTRIBUIDOR")==null)?"":rs.getString("DISTRIBUIDOR");
				numdocto = (rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto");
				acuse = (rs.getString("cc_acuse")==null)?"":rs.getString("cc_acuse");
				fchemision = (rs.getString("df_fecha_emision")==null)?"":rs.getString("df_fecha_emision");
				fchpublica = (rs.getString("df_fecha_publicacion")==null)?"":rs.getString("df_fecha_publicacion");		
				fchvence = (rs.getString("df_fecha_venc")==null)?"":rs.getString("df_fecha_venc");		
				plazodocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");				
				moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");				
				monto = rs.getDouble("FN_MONTO");
				tipoconversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");								
				tipocambio = (rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO");										
				plzodescto = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");														
				porcdescto = (rs.getString("fn_porc_descuento")==null)?"":rs.getString("fn_porc_descuento");														
				mntodescuento = rs.getDouble("MONTO_DESCUENTO");
				mntovaluado = (monto-mntodescuento)*Double.parseDouble(tipocambio);
				estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");																		
				modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
				fchopera = (rs.getString("df_fecha_hora")==null)?"":rs.getString("df_fecha_hora");		
				numcredito =(rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
				descif = (rs.getString("DIF")==null)?"":rs.getString("DIF");
				monedaLinea = (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
				numcredito =(rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
				referencia = (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
				tipoCredito = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
				plazocred = (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
				fchvenccred = (rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO");
				tipocobroint = (rs.getString("tipo_cobro_interes")==null)?"":rs.getString("tipo_cobro_interes");				
				montoint = rs.getDouble("MONTO_INTERES");
				valortaza = (rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA");
				mntocredito = rs.getDouble("MONTO_CREDITO");
			
				bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					if (bandeVentaCartera.equals("S") ) {
					modoPlazo ="";
				}		
				
				if(!"".equals(tipoconversion)){
					numCols  +=3;
			}
			
			if(registros ==0){
				pdfDoc.setTable(numCols,100);
				pdfDoc.setCell("Datos del Documento Inicial","celda01rep",ComunesPDF.CENTER,numCols);	
				pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("IF","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Distribuidor","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Emisión","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Oper.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo docto.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
				
				if(!"".equals(tipoconversion)) {
				pdfDoc.setCell("Tipo conv.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo cambio","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto valuado","celda01rep",ComunesPDF.CENTER);
				}
				
				pdfDoc.setCell("Plazo Dscto Días","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("% Dscto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto % Dscto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Modo Plazo","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
				
				pdfDoc.setCell("Datos Documento Final","celda01rep",ComunesPDF.CENTER,numCols);
				pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Cred.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Docto Final","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
				
				if(!responsable.equals("D")) {					
					pdfDoc.setCell("Ref. Tasa Interés","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Valor Tasa de Interés","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Interés","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,numCols-9);
				}else{
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,numCols-6);
				}
			}
			
			pdfDoc.setCell("A","formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(descif,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(distribuidor ,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(numdocto,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(acuse,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchemision,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchpublica,"formasrep",ComunesPDF.CENTER);	
			pdfDoc.setCell(fchvence,"formasrep",ComunesPDF.CENTER);	
			pdfDoc.setCell(fchopera,"formasrep",ComunesPDF.CENTER);	
			pdfDoc.setCell(plazodocto,"formasrep",ComunesPDF.CENTER);	
			pdfDoc.setCell(moneda,"formasrep",ComunesPDF.CENTER);	
			pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.CENTER);	
			
			
			if(!"".equals(tipoconversion)) {
				pdfDoc.setCell(tipoconversion,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(tipocambio,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(mntovaluado,2),"formasrep",ComunesPDF.CENTER);
			}
				
			pdfDoc.setCell(plzodescto,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(porcdescto,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(mntodescuento,2),"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(modoPlazo,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(estatus,"formasrep",ComunesPDF.CENTER);
						
			pdfDoc.setCell("B","formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(tipoCredito,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(numcredito,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(mntocredito,2),"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(plazocred,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchvenccred,"formasrep",ComunesPDF.CENTER);
			
			if(!responsable.equals("D")) {					
				pdfDoc.setCell(referencia,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(valortaza,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoint,2),"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER,numCols-9);
			}else{
				pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER,numCols-6);
			}
					
			registros++;			 
			
		} // while
			int i = 0;
			Vector vecTotales = new Vector();
			vecTotales = queryHelper.getResultCount(request);
			if (vecTotales.size()>0) {
				i = 0;
				Vector vecColumnas = null;
				for(i=0;i<vecTotales.size();i++){
					vecColumnas = (Vector)vecTotales.get(i);
					pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("TOTAL "+vecColumnas.get(1).toString(),"celda01rep",ComunesPDF.CENTER,3);
					pdfDoc.setCell(vecColumnas.get(2).toString(),"formasrep",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 6);
					pdfDoc.setCell("$"+Comunes.formatoMN(vecColumnas.get(3).toString()),"formasrep",ComunesPDF.CENTER,1);
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 2);
					pdfDoc.setCell("$"+Comunes.formatoMN(vecColumnas.get(4).toString()),"formasrep",ComunesPDF.CENTER,1);
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 2);
					
					if(!responsable.equals("D")) {
						pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 2);
						pdfDoc.setCell(Comunes.formatoMN(vecColumnas.get(5).toString()),"formasrep",ComunesPDF.CENTER,1);
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER, 4);
						pdfDoc.setCell(Comunes.formatoMN(vecColumnas.get(6).toString()),"formasrep",ComunesPDF.CENTER,1);
						pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,numCols-9);
					}
				}//for(i=0;i<vecTotales.size();i++)
			}//if (vecTotales.size()>0)
	
			con.cierraStatement();
			pdfDoc.addTable();
			pdfDoc.endDocument();
				
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoPDF);
	
	
	}else  if(informacion.equals("ArchivoCSV")) {
		
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo="";
		
		ResultSet rs = queryHelper.getCreateFile(request,con);	
	
		contenidoArchivo.append(" IF, Distribuidor, No. Docto Inicial, No. Acuse Carga, Fecha Emisión, Fecha Pub., Fecha Venc., Fecha Oper., Plazo docto.,Moneda, Monto ");
				
		if(!"".equals(tipoconversion)) {
		contenidoArchivo.append(",  Tipo conv., Tipo cambio, Monto valuado ");
		}
		contenidoArchivo.append(", Plazo Dscto Días, % Dscto, Monto % Dscto, Modo Plazo, Estatus ,Tipo Cred., No. Docto Final, Monto,Plazo, Fecha Venc. ");
		
		if(!responsable.equals("D")) {					
		contenidoArchivo.append(",Ref. Tasa Interés, Valor Tasa de Interés, Monto Interés");
		}		
	  contenidoArchivo.append(" "+"\n");
	
		while(rs.next()){
			registros++;	
			ic_moneda = (rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda");	
			distribuidor = (rs.getString("DISTRIBUIDOR")==null)?"":rs.getString("DISTRIBUIDOR");
			numdocto = (rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto");
			acuse = (rs.getString("cc_acuse")==null)?"":rs.getString("cc_acuse");
			fchemision = (rs.getString("df_fecha_emision")==null)?"":rs.getString("df_fecha_emision");
			fchpublica = (rs.getString("df_fecha_publicacion")==null)?"":rs.getString("df_fecha_publicacion");		
			fchvence = (rs.getString("df_fecha_venc")==null)?"":rs.getString("df_fecha_venc");		
			plazodocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");				
			moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");				
			monto = rs.getDouble("FN_MONTO");
			tipoconversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");								
			tipocambio = (rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO");										
			plzodescto = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");														
			porcdescto = (rs.getString("fn_porc_descuento")==null)?"":rs.getString("fn_porc_descuento");														
			mntodescuento = rs.getDouble("MONTO_DESCUENTO");
			mntovaluado = (monto-mntodescuento)*Double.parseDouble(tipocambio);
			estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");																		
			modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
			fchopera = (rs.getString("df_fecha_hora")==null)?"":rs.getString("df_fecha_hora");		
			numcredito =(rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
			descif = (rs.getString("DIF")==null)?"":rs.getString("DIF");
			monedaLinea = (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
			numcredito =(rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
			referencia = (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
			tipoCredito = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
			plazocred = (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
			fchvenccred = (rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO");
			tipocobroint = (rs.getString("tipo_cobro_interes")==null)?"":rs.getString("tipo_cobro_interes");				
			montoint = rs.getDouble("MONTO_INTERES");
			valortaza = (rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA");
			mntocredito = rs.getDouble("MONTO_CREDITO");
			
			bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
				if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
			}
	
	
			contenidoArchivo.append(descif.replace(',',' ')+", ");
			contenidoArchivo.append(distribuidor.replace(',',' ')+", ");
			contenidoArchivo.append(numdocto.replace(',',' ')+", ");
			contenidoArchivo.append(acuse.replace(',',' ')+", ");
			contenidoArchivo.append(fchemision.replace(',',' ')+", ");
			contenidoArchivo.append(fchpublica.replace(',',' ')+", ");
			contenidoArchivo.append(fchvence.replace(',',' ')+", ");
			contenidoArchivo.append(fchopera.replace(',',' ')+", ");
			contenidoArchivo.append(plazodocto.replace(',',' ')+", ");
			contenidoArchivo.append(moneda.replace(',',' ')+", ");			
			contenidoArchivo.append(Comunes.formatoDecimal(monto,2,false)+",");	
		
			if(!"".equals(tipoconversion)) {
				contenidoArchivo.append(tipoconversion.replace(',',' ')+", ");	
				contenidoArchivo.append(tipocambio.replace(',',' ')+", ");	
				contenidoArchivo.append(Comunes.formatoDecimal(mntovaluado,2,false)+",");
			}
			
			contenidoArchivo.append(plzodescto.replace(',',' ')+", ");	
			contenidoArchivo.append(porcdescto.replace(',',' ')+", ");
			contenidoArchivo.append(Comunes.formatoDecimal(mntodescuento,2,false)+",");
			contenidoArchivo.append(modoPlazo.replace(',',' ')+", ");	
			contenidoArchivo.append(estatus.replace(',',' ')+", ");	
			
			contenidoArchivo.append(tipoCredito.replace(',',' ')+", ");	
			contenidoArchivo.append(numcredito.replace(',',' ')+", ");
			contenidoArchivo.append(Comunes.formatoDecimal(mntocredito,2,false)+",");					
			contenidoArchivo.append(plazocred.replace(',',' ')+", ");
			contenidoArchivo.append(fchvenccred.replace(',',' ')+", ");
			
			if(!responsable.equals("D")) {					
				contenidoArchivo.append(referencia.replace(',',' ')+", ");	
				contenidoArchivo.append(valortaza.replace(',',' ')+", ");
				contenidoArchivo.append(Comunes.formatoDecimal(montoint,2,false)+",");						
			}
					
			contenidoArchivo.append("  "+"\n");			
		}				
			
		rs.close();
		con.cierraStatement();
		
		
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
			
	
	}
	
		
} catch(Exception e) {
	out.println("Error: "+e);
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
}
%>
<%=jsonObj%>