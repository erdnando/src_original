<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ page import="com.netro.pdf.*"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>

<%

String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 


if (informacion.equals("iniciaCategorias"))	{
	String DescuentoAutomatico = BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_DESC_AUTOMATICO");    
   String obtieneTipoCredito =  BeanParametros.obtieneTipoCredito(iNoCliente); 
  
	JSONObject jsonObj = new JSONObject();
	Registros reg = BeanParametros.getClasificacionClientes(iNoCliente);
	if (reg != null){
		jsonObj.put("gCat", reg.getJSONData());
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("DescuentoAutomatico", DescuentoAutomatico);
	jsonObj.put("obtieneTipoCredito", obtieneTipoCredito);
	infoRegresar = jsonObj.toString();
	//System.out.println("infoRegresar- - - - - - - - - --" + infoRegresar);

}else if (informacion.equals("modificaCategorias")){

	JSONObject jsonObj = new JSONObject();
	String id_clas		=	(request.getParameter("id_clas")!=null)?request.getParameter("id_clas"):"";
	String cve_cat		=	(request.getParameter("cve_cat")==null)?"":request.getParameter("cve_cat");
	String catnueva	=	(request.getParameter("catnueva")==null)?"":request.getParameter("catnueva");
	String plazoAut	=	(request.getParameter("plazoAut")==null)?"":request.getParameter("plazoAut");
	boolean resultado;
	if (BeanParametros.cambiaClasificacion(id_clas,cve_cat,catnueva,plazoAut)){
			resultado = true;
	}else{
			resultado = false;
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", new Boolean(resultado));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("eliminaCategorias")){

	JSONObject jsonObj = new JSONObject();
	String id_clas		=	(request.getParameter("id_clas")!=null)?request.getParameter("id_clas"):"";
	boolean resultado;
	if (BeanParametros.eliminaClasificacion(id_clas,iNoCliente,4,1)){
			resultado = true;
	}else{
			resultado = false;
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", new Boolean(resultado));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("validaDiasCategorias")){

	JSONObject jsonObj = new JSONObject();
	String id_clas	=	(request.getParameter("id_clas")!=null)?request.getParameter("id_clas"):"";
	String igDias = BeanParametros.validaDiasCategorias(iNoCliente,id_clas);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("igDias", igDias);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("agregaCategorias")){

	JSONObject jsonObj= new JSONObject();
	String cve_cat		=	(request.getParameter("cve_cat")!=null)?request.getParameter("cve_cat"):"";
	String catnueva		=	(request.getParameter("catnueva")!=null)?request.getParameter("catnueva"):"";
	String plazoAut		=	(request.getParameter("plazoAut")!=null)?request.getParameter("plazoAut"):"";

	String resultado = BeanParametros.validaAgregaCategoria(iNoCliente,cve_cat);
	boolean res = true;
	if (resultado.equals("true")){
		res = false;
	}else if ( BeanParametros.insertaClasificacion(cve_cat,iNoCliente,4,catnueva,plazoAut) ){
		res = true;
	}

	Registros reg = BeanParametros.getClasificacionClientes(iNoCliente);
	if (reg != null){
		jsonObj.put("newCat", reg.getJSONData());
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", new Boolean(res));
	infoRegresar = jsonObj.toString();

/*}else if (informacion.equals("obtenDistCliente"))	{

	JSONObject jsonObj = new JSONObject();

*/
}else if (informacion.equals("CatalogoCategoriasClasificacion") ) {

	Registros registros = BeanParametros.getClasificacionClientes(iNoCliente);
	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (registros.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(registros.getString("IC_CLASIFICACION"));
		elementoCatalogo.setDescripcion(registros.getString("CG_CLAVE")+" "+registros.getString("CG_DESCRIPCION"));
		coleccionElementos.add(elementoCatalogo);
	}	
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar = "{\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString()+"}";

}else if (informacion.equals("CatalogoCategoriaDist") ) {
		//infoRegresar = cat.getJSONElementos();
		infoRegresar = "";

}else if (informacion.equals("CatalogoDistribuidorClasificacion")){

	String stat = (request.getParameter("stat")!=null)?request.getParameter("stat"):"";
	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setClaveEpo(iNoCliente);
	cat.setCampoClave("cp.ic_pyme");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClasifica(stat);
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("consultaClasificacion"))	{
	
	JSONObject jsonObj = new JSONObject();
	String ic_pyme				=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String categoria			=	(request.getParameter("categoria")!=null)?request.getParameter("categoria"):"";
	String txt_plazo_finan	=	(request.getParameter("txt_plazo_finan")!=null)?request.getParameter("txt_plazo_finan"):"";
	Registros reg = BeanParametros.getClasificacionClientes2(iNoCliente,ic_pyme,categoria,txt_plazo_finan);
	if (reg != null){
		jsonObj.put("gClas", reg.getJSONData());
		
		int nRow = 0;
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);
		
		while(reg.next()){
			if(nRow == 0){
				pdfDoc.setTable(3, 70);
				pdfDoc.setCell("Clasificación de Clientes","celda01rep",ComunesPDF.CENTER,3);
				pdfDoc.setCell("Cliente","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Categoría","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo máximo por cliente","celda01rep",ComunesPDF.CENTER);
			}
			pdfDoc.setCell(reg.getString("CG_RAZON_SOCIAL"),"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(reg.getString("CG_DESCRIPCION"),"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(reg.getString("IG_DIAS_MAXIMO"),"formasrep",ComunesPDF.CENTER);
			nRow++;
		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}

	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenDistribuidor")){
	
	JSONObject jsonObj = new JSONObject();
	String ic_pyme				=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	Registros reg = BeanParametros.getDistribuidorClasificacion(ic_pyme);
	if (reg.next()){
		jsonObj.put("dist", reg.getString(1)==null?"":reg.getString(1));
		jsonObj.put("desc", reg.getString(2)==null?"":reg.getString(2));
		jsonObj.put("txt_plazo_finan", reg.getString(3)==null?"":reg.getString(3));
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("modificaClasificacion")){

	JSONObject jsonObj= new JSONObject();
	String ic_pyme		=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String cve_cat		=	(request.getParameter("cla_categoria")==null)?"":request.getParameter("cla_categoria");
	String cla_plazo	=	(request.getParameter("cla_plazo_max")==null)?"":request.getParameter("cla_plazo_max");
	if (cve_cat.equals("")){cve_cat = "NULL";}
	if (cla_plazo.equals("")){cla_plazo = "NULL";}
	boolean resultado;
	if (BeanParametros.relacionaClasificacion(iNoCliente,ic_pyme,4,cve_cat,cla_plazo)){
			resultado = true;
	}else{
			resultado = false;
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", new Boolean(resultado));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("eliminaClasificacion")){

	JSONObject jsonObj= new JSONObject();
	String ic_pyme		=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	boolean resultado;
	if (BeanParametros.eliminaClasificacion(ic_pyme,iNoCliente,4,0)){
			resultado = true;
	}else{
			resultado = false;
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", new Boolean(resultado));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenPlazoFin") ) {
	
	JSONObject jsonObj= new JSONObject();
	String claveCat		=	(request.getParameter("claveCat")!=null)?request.getParameter("claveCat"):"";
	String plazoAut		=	"";
	Registros registros	= BeanParametros.getClasificacionClientes(iNoCliente);
	while (registros.next()){
		if (claveCat.equals(registros.getString("IC_CLASIFICACION"))){
			plazoAut = registros.getString("IG_PLAZO_AUT")==null?"":registros.getString("IG_PLAZO_AUT");
			break;	
		}
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("plazoAut", plazoAut);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("aceptarClasificacion")){

	JSONObject jsonObj= new JSONObject();
	String ic_pyme		=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String categoria	=	(request.getParameter("categoria")!=null)?request.getParameter("categoria"):"NULL";
	String txt_plazo_finan	=	(request.getParameter("txt_plazo_finan")!=null)?request.getParameter("txt_plazo_finan"):"NULL";
	if (categoria.equals("")){categoria = "NULL";}
	if (txt_plazo_finan.equals("")){txt_plazo_finan = "NULL";}
	System.out.println("*-*-*-*-*-*categoria-*-*-*-*-*-*-*"+categoria);
	boolean resultado;
	if (BeanParametros.relacionaClasificacion(iNoCliente,ic_pyme,4,categoria,txt_plazo_finan)){
			resultado = true;
	}else{
			resultado = false;
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", new Boolean(resultado));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("consultaPlazos"))	{

	JSONObject jsonObj	=	new JSONObject();
	String ic_categoria	=	(request.getParameter("ic_categoria")!=null)?request.getParameter("ic_categoria"):"";
	System.out.println("*-*-*-*-*-*-*ic_categoria-*-*-*-*-*-*     "+ic_categoria);
	Registros reg			=	BeanParametros.getPlazosClasificacionClientes(ic_categoria);
	if (reg != null){
		jsonObj.put("gPlazo", reg.getJSONData());
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	//System.out.println("infoRegresar- - - - - - - - - --" + infoRegresar);

}else if (informacion.equals("eliminaPlazos")){

	JSONObject jsonObj= new JSONObject();
	String ic_plazo	=	(request.getParameter("ic_plazo")!=null)?request.getParameter("ic_plazo"):"";
	boolean resultado;
	if (BeanParametros.eliminaPlazo(ic_plazo)){
			resultado = true;
	}else{
			resultado = false;
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", new Boolean(resultado));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("agregarPlazos")){

	JSONObject jsonObj= new JSONObject();
	String categoria		=	(request.getParameter("plazo_categoria")!=null)?request.getParameter("plazo_categoria"):"";
	String plazo_inicial	=	(request.getParameter("plazo_inicial")!=null)?request.getParameter("plazo_inicial"):"";
	String plazo_final	=	(request.getParameter("plazo_final")!=null)?request.getParameter("plazo_final"):"";
	String porcentaje		=	(request.getParameter("porcentaje")!=null)?request.getParameter("porcentaje"):"";

	boolean resultado;
	if(BeanParametros.insertaPlazo(categoria,plazo_inicial,plazo_final,porcentaje)){
		resultado = true;
	} else{
		resultado = false;
	}

	Registros reg	=	BeanParametros.getPlazosClasificacionClientes(categoria);
	if (reg != null){
		jsonObj.put("newPlazos", reg.getJSONData());
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", new Boolean(resultado));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("modificaPlazos")){

	JSONObject jsonObj= new JSONObject();
	String ic_plazo			= (request.getParameter("mod_ic_plazo_descuento")==null)?"":request.getParameter("mod_ic_plazo_descuento");
	String ic_plazo_ant		= (request.getParameter("mod_ic_plazo_descuento_ant")==null)?"0":request.getParameter("mod_ic_plazo_descuento_ant");
	String ic_plazo_sig		= (request.getParameter("mod_ic_plazo_descuento_sig")==null)?"0":request.getParameter("mod_ic_plazo_descuento_sig");
	String plazo_inicial 	= (request.getParameter("mod_plazo_inicial")==null)?"1":request.getParameter("mod_plazo_inicial");
	String plazo_final 		= (request.getParameter("mod_plazo_final")==null)?"0":request.getParameter("mod_plazo_final");
	String porcentaje 		= (request.getParameter("mod_porcentaje")==null)?"0":request.getParameter("mod_porcentaje");
	if (ic_plazo_ant.equals("")) {ic_plazo_ant = "0";}
	if (ic_plazo_sig.equals("")) {ic_plazo_sig = "0";}
	boolean resultado;
	if (BeanParametros.modificaPlazo(ic_plazo,ic_plazo_ant,ic_plazo_sig,plazo_inicial,plazo_final,porcentaje)){
			resultado = true;
	}else{
			resultado = false;
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", new Boolean(resultado));
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>
