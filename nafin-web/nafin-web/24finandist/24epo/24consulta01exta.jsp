<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*, java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	java.io.FileOutputStream.*, 
	javax.naming.*,
	java.io.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String informacion               = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String tipo_credito              = (request.getParameter("tipo_credito")!=null)?request.getParameter("tipo_credito"):"";
String publicaDoctosFinanciables = (request.getParameter("publicaDoctosFinanciables")!=null)?request.getParameter("publicaDoctosFinanciables"):"";

String dist 				= "";
String numDocto			= "";
String numAcuseCarga		= "";
String fechaEmision		= "";
String fechaPublicacion	= "";
String fechaVencimiento	= "";
String plazoDocto			= "";
String moneda 				= "";
String icMoneda			= "";
double monto 				= 0;
String tipoConversion	= "";
double tipoCambio			= 0;
double montoValuado		= 0;
String categoria			= "";
String plazoDescuento	= "";
String porcDescuento		= "";
double montoDescontar	= 0;
String modoPlazo			= "";
String estatus 			= "";
String icDocumento		= "";
String intermediario		= "";
String tipoCredito		= "";
String fechaOperacion	= "";
double montoCredito		= 0;
String plazoCredito		= "";
String fechaVencCredito	= "";
String referenciaTasaInt= "";
String valorTasaInt		= "";
String montoTasaInt 		= "";
String tipoCobroInt		= ""; 
String monedaLinea		= "";
String bandeVentaCartera= "";
String cuenta_Bancaria ="";

String comisionAplicable = "";
String montoComision = "";
String montoDepositar = "";
String bins = "";

String idOrdenEnviado= "";
String idOperacion = "";
String codigoAutorizacion = "";
String fechaRegistro = "";
String numTarjeta = "";
String tipo_pago = "";

AccesoDB con = new AccesoDB();
try {
	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;
	OutputStreamWriter writer = null;
	BufferedWriter buffer = null;
	
	if (informacion.equals("ArchivoCSV")) {	
	
		CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsInfDocEpoDist());
		queryHelper.setMultiplesPaginadoresXPagina(true);
		con.conexionDB();
		ResultSet	rs = null;
		rs = queryHelper.getCreateFile(request,con);
		int aRow = 0;		
		int total = 0;	
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(strDirectorioTemp + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
	
		/*******************
		Cabecera del archivo
		********************/
		contenidoArchivo = new StringBuffer();	
		contenidoArchivo.append("Datos Documento Inicial \nDistribuidor,Número de documento inicial,Num. acuse carga,Fecha de emisión,Fecha de publicación,Fecha vencimiento,Plazo docto,Moneda,Monto,Categoría,Plazo para descuento,% de descuento,Monto a descontar");
		if(!"".equals(tipoConversion))	{
			contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado");
		}
		contenidoArchivo.append(",Modalidad de plazo");
		if(publicaDoctosFinanciables.equals("S")){
			contenidoArchivo.append(", Tipo de pago");
		}
		contenidoArchivo.append(", Estatus");
		if (tipo_credito.equals("F") ) {
			contenidoArchivo.append(", Cuenta Bancaria Pyme");
		}
						
		/***************************
		 Generacion del archivo
		/***************************/
		while(rs.next()){
			dist 					= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
			numDocto				= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
			numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
			fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
			fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
			fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
			plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
			moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
			monto 				= rs.getDouble("FN_MONTO");
			tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
			tipoCambio			= rs.getDouble("TIPO_CAMBIO");
			plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
			porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
			montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
			montoValuado		= (monto-montoDescontar)*tipoCambio;
			modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
			estatus 				= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
			icMoneda				= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
			icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
			monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
			categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
			bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
			tipo_pago = (rs.getString("IG_TIPO_PAGO")==null)?"":rs.getString("IG_TIPO_PAGO");
			if(tipo_credito.equals("C")){
				if(tipo_pago.equals("1")){
					tipo_pago = "Financiamiento con intereses";
				} else if(tipo_pago.equals("2")){
					tipo_pago = "Meses sin intereses";
				}
			} else{
				tipo_pago = "N/A";
			}
			if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
			}
			if (tipo_credito.equals("F") ) {
				cuenta_Bancaria		= (rs.getString("CUENTA_BANCARIA")==null)?"":rs.getString("CUENTA_BANCARIA");
			}
			contenidoArchivo.append("\n"+dist.replace(',',' ')+","+numDocto+","+numAcuseCarga+","+fechaEmision+","+fechaPublicacion+","+fechaVencimiento+","+plazoDocto+","+moneda+","+monto+","+categoria+","+plazoDescuento+","+porcDescuento+","+montoDescontar);
			if(!"".equals(tipoConversion))	{
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:"") );
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?""+Comunes.formatoDecimal(tipoCambio,2,false):""));
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?""+Comunes.formatoDecimal(montoValuado,2,false):""));
			}
			contenidoArchivo.append(","+modoPlazo);
			if(publicaDoctosFinanciables.equals("S")){
			contenidoArchivo.append(","+tipo_pago);
			}
			contenidoArchivo.append(","+estatus);
			if (tipo_credito.equals("F") ) {
				contenidoArchivo.append(","+cuenta_Bancaria);
			}
			aRow++;
			
			total++;
			if(total==1000){
				total=0;	
				buffer.write(contenidoArchivo.toString());
				contenidoArchivo = new StringBuffer();//Limpio  
			}
				
		}//while		
		buffer.write(contenidoArchivo.toString());
		buffer.close();	
		contenidoArchivo = new StringBuffer();//Limpio   			
		
	}else if (informacion.equals("ArchivoBCSV")) { 
		
		int total = 0;	
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(strDirectorioTemp + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
	
	
		int bRow = 0;
		CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new Cons2InfDocEpoDist());
		queryHelper.setMultiplesPaginadoresXPagina(true);
		con.conexionDB();
		ResultSet	rs = null;
		rs = queryHelper.getCreateFile(request,con);
		/*******************
		Cabecera del archivo
		********************/
		contenidoArchivo = new StringBuffer();//Limpio 
		if(!"F".equals(tipo_credito))	{
			contenidoArchivo.append("\n \n Datos Documento Inicial,,,,,,,,,,,,,,,,Datos Documento Final");
		}else if("F".equals(tipo_credito))	{
			contenidoArchivo.append("\n \n Datos Documento Inicial,,,,,,,,,,,,,,,,,Datos Documento Final");
		}
		contenidoArchivo.append("\nDistribuidor,Número de documento inicial,No. Acuse Pyme,Fecha de emisión,Fecha de publicación,Fecha vencimiento,Plazo docto,Moneda,Monto,Categoría,Plazo para descuento,% de descuento,Monto % de descuento");
		if(!"".equals(tipoConversion))	{
			contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado");
		}
		contenidoArchivo.append(",Modalidad de plazo");
		if(publicaDoctosFinanciables.equals("S")){
			contenidoArchivo.append(", Tipo de pago");
		}
		contenidoArchivo.append(", Estatus");
		contenidoArchivo.append(" ,Tipo Crédito ");
		if("F".equals(tipo_credito))	{
			contenidoArchivo.append(",Cuenta Bancaria Pyme ");
		}
		
		contenidoArchivo.append(",Número de docto. final, Monto ");
		contenidoArchivo.append(",% Comisión Aplicable de Terceros,Monto Comisión de Terceros (IVA Incluido),Monto a Depositar por Operación");
		if(!"F".equals(tipo_credito))	{
			contenidoArchivo.append(" , Plazo Crédito, Fecha Venc Crédito, Fecha Operación, Intermediario,Nombre del Producto, Referencia Tasa Int, Valor Tasa Int, Monto Tasa Int");
		}
		contenidoArchivo.append(",ID Orden enviado,Respuesta de Operación,Código Autorización,Número Tarjeta de Crédito");
		/***************************
		 Generacion del archivo
		/***************************/
		while(rs.next()){
			dist 					= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
			numDocto				= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
			numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
			fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
			fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
			fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
			plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
			moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
			monto 				= rs.getDouble("FN_MONTO");
			tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
			tipoCambio			= rs.getDouble("TIPO_CAMBIO");
			plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
			porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
			montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
			montoValuado		= (monto-montoDescontar)*tipoCambio;
			modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
			estatus 				= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
			icMoneda				= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
			icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
			intermediario		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
			tipoCredito 		= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
			fechaOperacion		= (rs.getString("DF_OPERACION_CREDITO")==null)?"":rs.getString("DF_OPERACION_CREDITO");
			montoCredito		= monto-montoDescontar;
			plazoCredito		= (rs.getString("PLAZO_CREDITO")==null)?"":rs.getString("PLAZO_CREDITO");
			fechaVencCredito	= (rs.getString("DF_VENC_CREDITO")==null)?"":rs.getString("DF_VENC_CREDITO");
			referenciaTasaInt	= (rs.getString("REF_TASA")==null)?"":rs.getString("REF_TASA");
			valorTasaInt		= rs.getString("FN_VALOR_TASA");
			montoTasaInt 		= rs.getString("FN_IMPORTE_INTERES");
			tipoCobroInt		= (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"); 
			monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
			categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
			bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
			tipo_pago = (rs.getString("IG_TIPO_PAGO")==null)?"":rs.getString("IG_TIPO_PAGO");
			if(tipo_credito.equals("C")){
				if(tipo_pago.equals("1")){
					tipo_pago = "Financiamiento con intereses";
				} else if(tipo_pago.equals("2")){
					tipo_pago = "Meses sin intereses";
				}
			} else{
				tipo_pago = "N/A";
			}
			
			comisionAplicable		= (rs.getString("COMISION_APLICABLE")==null)?"":rs.getString("COMISION_APLICABLE");
			montoComision		= (rs.getString("MONTO_COMISION")==null)?"":rs.getString("MONTO_COMISION");
			montoDepositar		= (rs.getString("MONTO_DEPOSITAR")==null)?"":rs.getString("MONTO_DEPOSITAR");
			bins		= (rs.getString("BINS")==null)?"":rs.getString("BINS");
			
			idOrdenEnviado		= (rs.getString("ID_ORDEN_ENVIADO")==null)?"":rs.getString("ID_ORDEN_ENVIADO")+"'";
			idOperacion		= (rs.getString("ID_OPERACION")==null)?"":rs.getString("ID_OPERACION");
			codigoAutorizacion		= (rs.getString("CODIGO_AUTORIZACION")==null)?"":rs.getString("CODIGO_AUTORIZACION");
			fechaRegistro		= (rs.getString("FECHA_REGISTRO")==null)?"":rs.getString("FECHA_REGISTRO");
			numTarjeta		= (rs.getString("NUM_TC")==null)?"":rs.getString("NUM_TC");
			if(!estatus.equals("Operada TC")){
				idOrdenEnviado		= "N/A";
				idOperacion		= "N/A";
				codigoAutorizacion		=  "N/A";
				fechaRegistro		=  "N/A";
				
				comisionAplicable		=  "N/A";
				montoComision		=  "N/A";
				montoDepositar		=  "N/A";
				bins		=  "N/A";
				numTarjeta	=	"N/A";
			}else{
				fechaVencCredito ="N/A";
				plazoCredito ="N/A";
				fechaVencimiento ="N/A";
				plazoDocto ="N/A";
				referenciaTasaInt ="N/A";
				valorTasaInt ="N/A";
				montoTasaInt ="N/A";
			}
			if("F".equals(tipo_credito)){
			cuenta_Bancaria	= (rs.getString("CUENTA_BANCARIA")==null)?"":rs.getString("CUENTA_BANCARIA");
			}
			if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
			}
			if(!icMoneda.equals(monedaLinea))	{
				montoCredito	= montoValuado;
			}else 	{
				montoCredito	= monto-montoDescontar;
			}
			contenidoArchivo.append("\n"+dist.replace(',',' ')+","+numDocto+","+numAcuseCarga+","+fechaEmision+","+fechaPublicacion+","+fechaVencimiento+","+plazoDocto+","+moneda+","+monto+","+categoria+","+plazoDescuento+","+porcDescuento+","+montoDescontar);
			if(!"".equals(tipoConversion))	{
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:"") );
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?""+Comunes.formatoDecimal(tipoCambio,2,false):""));
				contenidoArchivo.append(","+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?""+Comunes.formatoDecimal(montoValuado,2,false):""));
			}
			contenidoArchivo.append(","+modoPlazo);
			if(publicaDoctosFinanciables.equals("S")){
				contenidoArchivo.append(","+tipo_pago);
			}
			contenidoArchivo.append(","+estatus+","+ tipoCredito+"");
			if("F".equals(tipo_credito))	{
				contenidoArchivo.append(","+cuenta_Bancaria);
			}
		
			if (!estatus.equals("Negociable")){
				contenidoArchivo.append(","+icDocumento+","+montoCredito);		
				contenidoArchivo.append(","+comisionAplicable+","+montoComision+","+montoDepositar);			
				if(!"F".equals(tipo_credito))	{
					contenidoArchivo.append(","+plazoCredito+","+fechaVencCredito+","+fechaOperacion+","+intermediario.replace(',', '.')+","+bins+","+referenciaTasaInt.replace(',', '.')+","+valorTasaInt+","+montoTasaInt);
				}
				contenidoArchivo.append(","+idOrdenEnviado+","+idOperacion+","+ codigoAutorizacion+","+((numTarjeta.trim()!="" && numTarjeta.trim()!="N/A")?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta));
			}else	{
				contenidoArchivo.append(",,,,,,,,");
			}
			bRow++;
			//System.out.println(" *total **" +total);     
			
			total++;
			if(total==1000){					
				total=0;	
				buffer.write(contenidoArchivo.toString());
				contenidoArchivo = new StringBuffer();//Limpio  
			}
			
		}//while
		
		buffer.write(contenidoArchivo.toString());
		buffer.close();	
		contenidoArchivo = new StringBuffer();//Limpio    
			
	}
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
%>
<%=jsonObj%>