Ext.onReady(function() {

	var newDataCategorias=[];
	var newDataClasificacion=[];
	var newDataPlazos=[];

	function procesaIniciaCategoria(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.DescuentoAutomatico!='S' || infoR.obtieneTipoCredito=='D' ) {
				Ext.getCmp('tabPanel').show();
				categoriaData.loadData('');
			
				var el = Ext.getCmp('gridCategorias').getGridEl();
				if (infoR.gCat != undefined && infoR.gCat.length > 0){
					el.unmask();
					categoriaData.loadData(infoR.gCat);
				}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}else {
				Ext.getCmp('mensajeServicio').update('<center>Este men� est� disponible s�lo para Descuento Mercantil y para CCC sin Descuento Autom�tico</center>');					
				Ext.getCmp('formMensaje').show();
				return;
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaValidaDiasCategorias(opts, success, response) {
		fpWinCategorias.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var flag = true;
			if (infoR.igDias != undefined && infoR.igDias != ""){
				if (Ext.getCmp('plazoModif').getValue() <= infoR.igDias) {
					flag = false;
					Ext.Msg.alert('','El Plazo Autorizado por categor�a debe ser mayor al par�metro Plazo autorizado por cliente');
				}
			}
			if (flag) {
				fpWinCategorias.el.mask('Enviando...', 'x-mask-loading');
				Ext.Ajax.request({
					url: '24forma07ext.data.jsp',
					params: Ext.apply(fpWinCategorias.getForm().getValues(),{informacion: "modificaCategorias", cve_cat : Ext.getCmp('claveModif').getValue()}),
					callback: procesaModificaCategorias
				});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaModificaCategorias(opts, success, response) {
		fpWinCategorias.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.resultado != undefined && infoR.resultado){
				if (newDataCategorias.length > 0){
					categoriaData.loadData(newDataCategorias);
				}
				Ext.Msg.alert('','Modificaci�n realizada');
				Ext.getCmp('winCategorias').hide();
			}else if (infoR.resultado != undefined && !infoR.resultado){
				Ext.Msg.alert('','Hubo un error en la actualizaci�n');
				Ext.getCmp('winCategorias').hide();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaEliminaCategorias(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.resultado != undefined && infoR.resultado){
				//if (newDataCategorias.length > 0){
					categoriaData.loadData(newDataCategorias);
				//}
				Ext.Msg.alert('','Registro eliminado');
			}else if (infoR.resultado != undefined && !infoR.resultado){
				Ext.Msg.alert('','Hubo un error en la eliminaci�n');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaAgregaCategorias(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.resultado != undefined && infoR.resultado){
				var el = Ext.getCmp('gridCategorias').getGridEl();
				if (infoR.newCat != undefined && infoR.newCat.length > 0){
					categoriaData.loadData('');
					el.unmask();
					categoriaData.loadData(infoR.newCat);
					Ext.Msg.alert('','Datos introducidos');
				}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}else if (infoR.resultado != undefined && !infoR.resultado){
				Ext.Msg.alert('','Ya existe esa clave. Introduzca una nueva.');
			}else {
				Ext.Msg.alert('','Hubo un error al dar de alta la categor�a');
			}
			fpCategorias.getForm().reset();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesaModificarCategorias = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var win = Ext.getCmp('winCategorias');
		if (win){
			win.show();
		}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				layout: 'form',
				width: 510,
				height: 180,
				x:250,
				y:10,
				id: 'winCategorias',
				closeAction: 'hide',
				items: [fpWinCategorias],
				title: 'Modificar'
			}).show().el.dom.scrollIntoView();
		}
		Ext.getCmp('hidModif').setValue(registro.get('IC_CLASIFICACION'));
		Ext.getCmp('claveModif').setValue(registro.get('CG_CLAVE'));
		Ext.getCmp('catModif').setValue(registro.get('CG_DESCRIPCION'));
		Ext.getCmp('plazoModif').setValue(registro.get('IG_PLAZO_AUT'));
	}

	var procesaEliminarCategorias = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var data = categoriaData.reader.jsonData;
		newDataCategorias=[];
		Ext.each(data, function(item, index, arrItems){
			if (item.IC_CLASIFICACION != registro.get('IC_CLASIFICACION')){
				newDataCategorias.push(item);
			}
		});
		tp.el.mask('Enviando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '24forma07ext.data.jsp',
			params: {informacion: "eliminaCategorias", 
						id_clas: registro.get('IC_CLASIFICACION')},
			callback: procesaEliminaCategorias
		});
	}

	function procesaObtenDistribuidor(opts, success, response) {
		fpWinClasificacion.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.dist != undefined){
				Ext.getCmp('cla_distribuidor').setValue(infoR.dist);
			}
			/*if (infoR.dist != undefined){
				Ext.getCmp('cla_cliente').setValue(infoR.desc);
			}*/
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaObtenPlazoFin(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.plazoAut != undefined && infoR.plazoAut != ""){
				if (parseFloat(infoR.plazoAut) < Ext.getCmp('txt_plazo_finan').getValue()){
					Ext.Msg.alert('Aceptar',"El Plazo Autorizado por categor�a debe ser mayor al par�metro Plazo autorizado por cliente");
					return;
				}else{
					tp.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24forma07ext.data.jsp',
						params: Ext.apply(Ext.getCmp('formaClasificacion').getForm().getValues(),{informacion: "aceptarClasificacion"}),
						callback: procesaAceptaClasificacion
					});
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesaModificarClasificacion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var win = Ext.getCmp('winClasificacion');
		if (win){
			win.show();
		}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				layout: 'form',
				width: 510,
				height: 205,
				x:250,
				y:10,
				id: 'winClasificacion',
				closeAction: 'hide',
				items: [fpWinClasificacion],
				title: 'Modificar'
			}).show().el.dom.scrollIntoView();
		}
		Ext.getCmp('cla_distribuidor').setValue('');
		Ext.getCmp('cla_ic_pyme').setValue(registro.get('IC_PYME'));
		Ext.getCmp('cla_cliente').setValue(registro.get('CG_RAZON_SOCIAL'));
		Ext.getCmp('cmbCategoria').setValue(registro.get('IC_CLASIFICACION'));
		Ext.getCmp('cla_plazo_max').setValue(registro.get('IG_DIAS_MAXIMO'));
		Ext.getCmp('cla_plazo_finan').setValue(registro.get('IG_PLAZO_AUT'));
		fpWinClasificacion.el.mask('Enviando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '24forma07ext.data.jsp',
			params: {informacion: "obtenDistribuidor", 
						ic_pyme: registro.get('IC_PYME')},
			callback: procesaObtenDistribuidor
		});
	}

	function procesaModificaClasificacion(opts, success, response) {
		fpWinClasificacion.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.resultado != undefined && infoR.resultado){
				if (newDataClasificacion.length > 0){
					clasificacionData.loadData(newDataClasificacion);
				}
				Ext.Msg.alert('','Modificaci�n realizada');
				Ext.getCmp('winClasificacion').hide();
				Ext.getCmp('gridClasificacion').hide();
				//Ext.getCmp('formaClasificacion').getForm().reset();
				Ext.getCmp('txt_plazo_finan').reset();
				Ext.getCmp('cboCategoria').reset();
				Ext.getCmp('cboCliente').reset();
				Ext.getCmp('cg_num_distribuidor').reset();
			}else if (infoR.resultado != undefined && !infoR.resultado){
				Ext.Msg.alert('','Hubo un error en la actualizaci�n');
				Ext.getCmp('winClasificacion').hide();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesaEliminarClasificacion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var data = clasificacionData.reader.jsonData;
		newDataClasificacion=[];
		Ext.each(data, function(item, index, arrItems){
			if (item.IC_PYME == registro.get('IC_PYME')){
				item.CG_DESCRIPCION = '';
				item.IC_CLASIFICACION = '';
				newDataClasificacion.push(item);
				return false;
			}
		});
		tp.el.mask('Enviando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '24forma07ext.data.jsp',
			params: {informacion: "eliminaClasificacion", ic_pyme: registro.get('IC_PYME')},
			callback: procesaEliminaClasificacion
		});
	}

	function procesaEliminaClasificacion(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.resultado != undefined && infoR.resultado){
				if (newDataClasificacion.length > 0){
					clasificacionData.loadData(newDataClasificacion);
				}
				Ext.Msg.alert('','Registro eliminado');
			}else if (infoR.resultado != undefined && !infoR.resultado){
				Ext.Msg.alert('','Hubo un error en la eliminaci�n');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaConsultaClasificacion(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			clasificacionData.loadData('');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var gridClasifica = Ext.getCmp('gridClasificacion');
			if (!gridClasifica.isVisible()){
				gridClasifica.show();
			}
			var el = gridClasifica.getGridEl();
			if (infoR.gClas != undefined && infoR.gClas.length > 0){
				Ext.getCmp('btnAbrirPDF').enable();
				Ext.getCmp('btnClasificaSalir').enable();
				el.unmask();
				clasificacionData.loadData(infoR.gClas);
				Ext.getCmp('btnAbrirPDF').setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = infoR.urlArchivo;
					forma.submit();
				});
			}else{
				Ext.getCmp('btnAbrirPDF').disable();
				Ext.getCmp('btnClasificaSalir').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaAceptaClasificacion(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.resultado != undefined && infoR.resultado){
				Ext.Msg.alert('Registro','Modificaci�n realizada');
				Ext.getCmp('txt_plazo_finan').reset();
				Ext.getCmp('cboCategoria').reset();
				Ext.getCmp('cboCliente').reset();
				Ext.getCmp('cg_num_distribuidor').reset();
			}else if (infoR.resultado != undefined && !infoR.resultado){
				Ext.Msg.alert('','Hubo un error en la modificaci�n');
				Ext.getCmp('formaClasificacion').getForm().reset();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaConsultaPlazos(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			plazosData.loadData('');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var el = Ext.getCmp('gridPlazos').getGridEl();
			if (infoR.gPlazo != undefined && infoR.gPlazo.length > 0){
				el.unmask();
				var indice = infoR.gPlazo.length - 1;
				plazosData.loadData(infoR.gPlazo);
				var data = plazosData.reader.jsonData;
				Ext.each(data, function(item, index, arrItems){
					if (index == indice){
						Ext.getCmp('porcentaje_ant').setValue(item.FG_PORCENTAJE);
						Ext.getCmp('pInicial').setValue(parseFloat(item.IG_PLAZO_FINAL)+1);
						return false;
					}
				});
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaAgregarPlazos(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('gridPlazos').show();
			if (infoR.resultado != undefined && infoR.resultado){
				var el = Ext.getCmp('gridPlazos').getGridEl();
				Ext.getCmp('pInicial').setValue(Ext.getCmp('pFinal').getValue()+1);
				if (infoR.newPlazos != undefined && infoR.newPlazos.length > 0){
					plazosData.loadData('');
					el.unmask();
					plazosData.loadData(infoR.newPlazos);
					Ext.getCmp('pFinal').reset();
					Ext.getCmp('pPorc').reset();
					Ext.Msg.alert('','Datos introducidos');
				}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}else {
				Ext.Msg.alert('','Hubo un error al dar de alta el plazo');
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesaModificarPlazos = function(grid, rowIndex, colIndex, item, event) {
		fpWinPlazos.getForm().reset();
		var registro = grid.getStore().getAt(rowIndex);
		var registro_ant = grid.getStore().getAt(rowIndex-1);
		var registro_sig = grid.getStore().getAt(rowIndex+1);
		if (registro_ant != undefined){
			Ext.getCmp('mod_ic_plazo_descuento_ant').setValue(registro_ant.get('IC_PLAZO_DESCUENTO'));
			Ext.getCmp('mod_porcentaje_ant').setValue(registro_ant.get('FG_PORCENTAJE'));
		}else {
			Ext.getCmp('mod_porcentaje_ant').setValue('');
		}
		if (registro_sig != undefined){
			Ext.getCmp('mod_ic_plazo_descuento_sig').setValue(registro_sig.get('IC_PLAZO_DESCUENTO'));
			Ext.getCmp('mod_porcentaje_sig').setValue(registro_sig.get('FG_PORCENTAJE'));
		}
		Ext.getCmp('mod_plazo_inicial').setValue(registro.get('IG_PLAZO_INICIAL'));
		Ext.getCmp('mod_plazo_inicial_mod').setValue(registro.get('IG_PLAZO_INICIAL'));
		if (registro.get('IG_PLAZO_INICIAL') == "1"){
			Ext.getCmp('mod_plazo_inicial').setDisabled(true);
		}else{
			Ext.getCmp('mod_plazo_inicial').setDisabled(false);
		}
		Ext.getCmp('mod_plazo_final').setValue(registro.get('IG_PLAZO_FINAL'));
		Ext.getCmp('mod_plazo_final_mod').setValue(registro.get('IG_PLAZO_FINAL'));
		Ext.getCmp('mod_porcentaje').setValue(registro.get('FG_PORCENTAJE'));
		Ext.getCmp('mod_porcentaje_mod').setValue(registro.get('FG_PORCENTAJE'));
		Ext.getCmp('mod_ic_plazo_descuento').setValue(registro.get('IC_PLAZO_DESCUENTO'));
		
		var win = Ext.getCmp('winPlazos');
		if (win){
			win.show();
		}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				layout: 'form',
				width: 310,
				height: 167,
				x:500,
				y:10,
				id: 'winPlazos',
				closeAction: 'hide',
				items: [fpWinPlazos],
				title: 'Modificar'
			}).show().el.dom.scrollIntoView();
		}
	}

	var procesaEliminarPlazos = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var data = plazosData.reader.jsonData;
		newDataPlazos=[];
		Ext.each(data, function(item, index, arrItems){
			if (item.IC_PLAZO_DESCUENTO != registro.get('IC_PLAZO_DESCUENTO')){
				newDataPlazos.push(item);
			}
		});
		tp.el.mask('Enviando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '24forma07ext.data.jsp',
			params: {informacion: "eliminaPlazos", 
						ic_plazo: registro.get('IC_PLAZO_DESCUENTO')},
			callback: procesaEliminaGridPlazos
		});
	}

	function procesaEliminaGridPlazos(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.resultado != undefined && infoR.resultado){

				plazosData.loadData(newDataPlazos);

				var tot = (plazosData.getTotalCount())-1;
				var data = plazosData.reader.jsonData;
				if (!Ext.isEmpty(data)){
					Ext.each(data, function(item, index, arrItems){
						if (index == tot){
							Ext.getCmp('porcentaje_ant').setValue(item.FG_PORCENTAJE);
							Ext.getCmp('pInicial').setValue(parseFloat(item.IG_PLAZO_FINAL)+1);
							return false;
						}
					});
				}else{
					Ext.getCmp('gridPlazos').getGridEl().mask('No se encontr� ning�n registro', 'x-mask');;
					Ext.getCmp('porcentaje_ant').reset();
					Ext.getCmp('pInicial').reset();
					Ext.getCmp('pFinal').reset();
					Ext.getCmp('pPorc').reset();
				}

				Ext.Msg.alert('','Registro eliminado');
			}else if (infoR.resultado != undefined && !infoR.resultado){
				Ext.Msg.alert('','Hubo un error en la eliminaci�n');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaModificaGridPlazos(opts, success, response) {
		fpWinPlazos.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.resultado != undefined && infoR.resultado){
				if (newDataPlazos.length > 0){
					plazosData.loadData(newDataPlazos);
				}
				Ext.Msg.alert('','Modificaci�n realizada');
				Ext.getCmp('winPlazos').hide();
			}else if (infoR.resultado != undefined && !infoR.resultado){
				Ext.Msg.alert('','Hubo un error en la actualizaci�n');
				Ext.getCmp('winPlazos').hide();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	/*var procesarCatalogoDisData = function(store, arrRegistros, opts) {
				var store = catalogoDisData;
				var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
				store.insert(0,	new reg({clave: 'T',	descripcion: 'Todos',	loadMsg: null	})	);
	}*/

	var catalogoDisData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma07ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoDistribuidorClasificacion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			//load: procesarCatalogoDisData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoCategoriaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma07ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCategoriasClasificacion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var categoriaData = new Ext.data.JsonStore({
		fields: [
			{name: 'IC_CLASIFICACION'},
			{name: 'CG_CLAVE'},
			{name: 'CG_DESCRIPCION'},
			{name: 'IG_PLAZO_AUT'}
		],
		data:[{'IC_CLASIFICACION':'','CG_CLAVE':'','CG_DESCRIPCION':'','IG_PLAZO_AUT':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var clasificacionData = new Ext.data.JsonStore({
		fields: [
			{name: 'IC_PYME'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'CG_DESCRIPCION'},
			{name: 'IC_CLASIFICACION'},
			{name: 'IG_DIAS_MAXIMO'},
			{name: 'IG_PLAZO_AUT'}
		],
		data:[{'IC_PYME':'','CG_RAZON_SOCIAL':'','CG_DESCRIPCION':'','IC_CLASIFICACION':'','IG_DIAS_MAXIMO':'','IG_PLAZO_AUT':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var plazosData = new Ext.data.JsonStore({
		fields: [
			{name: 'IC_PLAZO_DESCUENTO'},
			{name: 'IC_CLASIFICACION'},
			{name: 'IG_PLAZO_INICIAL'},
			{name: 'IG_PLAZO_FINAL'},
			{name: 'FG_PORCENTAJE'}
		],
		data:[{'IC_PLAZO_DESCUENTO':'','IC_CLASIFICACION':'','IG_PLAZO_INICIAL':'','IG_PLAZO_FINAL':'','FG_PORCENTAJE':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var elementosPlazos = [
		{
			xtype:'combo',
			name:	'plazo_categoria',
			id:	'plazoCategoria',
			fieldLabel:'Categor�a',
			mode: 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'plazo_categoria',
			emptyText:	'Seleccione categor�a',
			forceSelection : true,
			allowBlank: false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			anchor:'80%',
			store: catalogoCategoriaData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:	function(combo){
								var disp = combo.lastSelectionText;
								var desc = disp.slice(disp.indexOf(" ")+1);
								Ext.getCmp('gridPlazos').setTitle('<div align="center">'+desc+'</div>');
								Ext.getCmp('gridPlazos').show();
								Ext.getCmp('pInicial').reset();
								Ext.getCmp('pFinal').reset();
								Ext.getCmp('pPorc').reset();
								Ext.getCmp('porcentaje_ant').reset();
								Ext.Ajax.request({
									url: '24forma07ext.data.jsp',
									params: {informacion: "consultaPlazos", ic_categoria:combo.getValue()},
									callback: procesaConsultaPlazos
								});
							}
			}
		},{
			xtype:'numberfield',
			name:'plazo_inicial',
			id:	'pInicial',
			anchor:'40%',
			value: 1,
			maxLength: 3,
			allowBlank: false,
			fieldLabel:'Plazo Inicial'
		},{
			xtype:'numberfield',
			name:'plazo_final',
			id:	'pFinal',
			anchor:'40%',
			maxLength: 3,
			allowBlank: false,
			fieldLabel:'Plazo Final'
		},{
			xtype:'numberfield',
			name:'porcentaje',
			id:	'pPorc',
			anchor:'40%',
			maxLength: 7,
			allowBlank: false,
			fieldLabel:'Porcentaje'
		},{
				xtype:'hidden',
				name:	'porcentaje_ant',
				id:	'porcentaje_ant'
		},{
			xtype: 'grid',
			id:	'gridPlazos',
			hidden: true,
			store: plazosData,
			columns: [
				{//0
					header: 'Plazo Inicial', tooltip: 'Plazo Inicial',
					dataIndex: 'IG_PLAZO_INICIAL',align: 'center',hideable: false,
					sortable: true,	resizable: true,	width: 200,	hidden: false
				},{//1
					header : 'Plazo Final', tooltip: 'Plazo Final',
					dataIndex : 'IG_PLAZO_FINAL', align: 'center',hideable: false,
					sortable : true, resizable: true,width : 200,	hidden: false
				},{//2
					header: 'Porcentaje', tooltip: 'Porcentaje',
					dataIndex: 'FG_PORCENTAJE', align: 'center',hideable: false,
					sortable: true,	width: 200,	resizable: true, hidden: false,
					renderer: Ext.util.Format.numberRenderer('0,0.00 %')
				},{//4
					xtype:	'actioncolumn',
					header : 'Seleccionar', tooltip: 'Seleccionar',
					dataIndex : '',align: 'center', hideable: false,
					sortable : true,	width : 100, hidden: false,
					items: [
						{
							getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Modificar';
									return 'modificar';
							},
							handler:	procesaModificarPlazos
						},{
							getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
									var totRegistros = (store.getTotalCount())-1;
									if (rowIndex == totRegistros){
										this.items[1].tooltip = 'Eliminar';
										return 'borrar';
									}else{
										return '&nbsp;';
									}
							},
							handler:	procesaEliminarPlazos
						}
					]
				}
			],
			viewConfig: {forceFit: true},
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 200,
			width: 550,
			title:'Categoria',
			frame: false
		}
	];

	var gridClasificacion = {
		xtype: 'grid',
		id:	'gridClasificacion',
		store: clasificacionData,
		hidden:true,
		style: ' margin:0 auto;',
		columns: [
			{//0
				header: 'Cliente', tooltip: 'Cliente',
				dataIndex: 'CG_RAZON_SOCIAL',align: 'center',hideable: false,
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},{//1
				header : 'Categor�a', tooltip: 'Categor�a',
				dataIndex : 'CG_DESCRIPCION', align: 'left',hideable: false,
				sortable : true, resizable: true,width : 150,	hidden: false
			},{//2
				header: 'Plazo m�ximo por cliente', tooltip: 'Plazo m�ximo por cliente',
				dataIndex: 'IG_DIAS_MAXIMO', align: 'center',hideable: false,
				sortable: true,	width: 120,	resizable: true, hidden: false
			},{//3
				xtype:	'actioncolumn',
				header : 'Seleccionar', tooltip: 'Seleccionar',
				dataIndex : '',align: 'center', hideable: false,
				sortable : true,	width : 90, hidden: false,	//renderer: renderButton
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
								this.items[0].tooltip = 'Modificar';
								return 'modificar';
						},
						handler:	procesaModificarClasificacion
					},{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
								this.items[1].tooltip = 'Eliminar';
								return 'borrar';
						},
						handler:	procesaEliminarClasificacion
					}
				]
			}
		],
		viewConfig: {forceFit: true},
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 200,
		width: 695,
		frame: false,
		bbar:{
			items:[
				'->','-',
				{
					xtype:'button',
					text: 'Imprimir PDF',
					id: 'btnAbrirPDF'
				},'-',{
					xtype:'button',
					id:	'btnClasificaSalir',
					text: 'Salir',
					handler: function(boton, evento) {
						Ext.getCmp('gridClasificacion').hide();
					}
				}
			]
		}
	};

	var elementosClasificacion = [
		{
			xtype:'radiogroup',
			id:	'radioGp',
			columns: 2,
			items:[
				{boxLabel: 'Registro', name: 'stat', inputValue: 'R', checked: false,
					listeners:{
						check:	function(radio){
										if (radio.checked){
											var comboCliente = Ext.getCmp('cboCliente');
											comboCliente.setValue('');
											comboCliente.store.removeAll();
											comboCliente.store.reload({params: Ext.apply({stat:	radio.inputValue})});
											comboCliente.reset();
											//catalogoDisData.load({params: Ext.apply({stat:	radio.inputValue})});
											Ext.getCmp('formClas').setTitle('<div align="center">Registro</div>');
											Ext.getCmp('btnClas').setText('Aceptar');
											Ext.getCmp('btnClas').setIconClass('aceptar');
											Ext.getCmp('cboCliente').allowBlank = false;
											Ext.getCmp('formClas').show();
											Ext.getCmp('formClas').doLayout();
											Ext.getCmp('gridClasificacion').hide();
										}
									}
					}
				},
				{boxLabel: 'Consulta', name: 'stat', inputValue: 'Cl',checked: false,
					listeners:{
						check:	function(radio){
										if (radio.checked){
											var comboCliente = Ext.getCmp('cboCliente');
											comboCliente.setValue('');
											comboCliente.store.removeAll();
											comboCliente.store.reload({params: Ext.apply({stat:	radio.inputValue})});
											comboCliente.reset();
											//catalogoDisData.load({params: Ext.apply({stat:	radio.inputValue})});
											Ext.getCmp('formClas').setTitle('<div align="center">Consulta</div>');
											Ext.getCmp('btnClas').setText('Consultar');
											Ext.getCmp('btnClas').setIconClass('icoBuscar');
											Ext.getCmp('cboCliente').allowBlank = true;
											Ext.getCmp('formClas').show();
											Ext.getCmp('gridClasificacion').hide();
										}
									}
					}
				}
			]
		},
		{
				xtype		: 'panel',
				height: 'auto',
				labelWidth	: 300,
				items		: [{
					layout		: 'form',
					id:	'formClas',
					frame				: true,
					labelWidth	: 200,
					bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:2px;',
					items:[
						{
							xtype:	'textfield',
							fieldLabel:	'No. Distribuidor',
							name:	'cg_num_distribuidor',
							id:	'cg_num_distribuidor',
							width: 150,
							border:    	true,
							maxLength:25,
							listeners:	{
								blur:	function(field){
												var c = field.getValue().toString();
												var tam = c.length;
												var ban =0;
												var storeDistClie = catalogoDisData;
												storeDistClie.each(function(record){
													var lenR = record.get('descripcion').indexOf(" ");
													var desc = record.get('descripcion').substring(0,lenR);
													if (c == desc){
														ban = 1;
														Ext.getCmp('cboCliente').setValue(record.get('clave'));
														return false;
													}
												});
												var cliente = Ext.getCmp('cboCliente');
												var distr = Ext.getCmp('cg_num_distribuidor');
												if(ban!=1&&!Ext.isEmpty(distr.getValue())){
													cliente.setValue('');
													Ext.Msg.alert('Mensaje','No existe Cliente con est� No. Distribuidor');
													distr.setValue('');
													return;
												}
								}
							}
						},{
							xtype:'combo',
							name:	'ic_pyme',
							id:	'cboCliente',
							fieldLabel:'Cliente',
							mode: 'local',
							msgTarget: 'side',
							displayField : 'descripcion',
							valueField : 'clave',
							hiddenName : 'ic_pyme',
							emptyText:	'Seleccione distribuidor',
							forceSelection : true,
							triggerAction : 'all',
							allowBlank:false,
							typeAhead: true,
							width: 300,
							minChars : 1,
							store: catalogoDisData,
							tpl:'<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>',
							listeners: {
								select: {
									fn: function(combo) {
										var distr = Ext.getCmp('cg_num_distribuidor');
										distr.setValue('');
										
									}
								}
							}
						},{
							xtype:'combo',
							name:	'categoria',
							id:	'cboCategoria',
							fieldLabel:'Categor�a',
							mode: 'local',
							displayField : 'descripcion',
							valueField : 'clave',
							hiddenName : 'categoria',
							emptyText:	'Seleccione categor�a',
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							width: 300,
							minChars : 1,
							store: catalogoCategoriaData,
							tpl:'<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>'
						},{
							xtype: 'compositefield',
							combineErrors: false,
							items:[
								{
									xtype: 'textfield',
									name: 'txt_plazo_finan',
									id: 'txt_plazo_finan',
									width:60,
									maxLength: 3,
									fieldLabel: 'Plazo m�ximo por cliente',
									msgTarget: 'side',
									margins: '0 20 0 0'
								},{
									xtype: 'button',
									id:'btnAyudaClas',
									autoWidth: true,
									autoHeight: true,
									iconCls: 'icoAyuda',
									handler: function() {
													Ext.getCmp('panAyudaClas').show();
												}
								}
							]
						},{
							xtype:	'panel',
							id:		'panAyudaClas',
							layout:	'table',
							anchor:	'100%',
							hidden:	true,
							layoutConfig: {
								columns: 2
							},
							items:[
								{
									xtype: 'panel',
									html:'<div class="formas" align="left" style="color:green">'+
											'Debe ser menor al plazo m�ximo por categor�a.</div>'
								},{
									xtype: 'button',
									autoWidth: true,
									autoHeight: true,
									iconCls: 'rechazar',
									handler: function() {
													Ext.getCmp('panAyudaClas').hide();
												}
								}
							]
						}
			],buttons: [
						{
							text: '',
							id: 'btnClas',
							formBind: true,
							handler: function(boton, evento) {
								if (boton.text == 'Consultar'){																						//***************RadioButton->Consultar (Consultar)
		
									tp.el.mask('Enviando...', 'x-mask-loading');
									Ext.Ajax.request({
										url: '24forma07ext.data.jsp',
										params: Ext.apply(Ext.getCmp('formaClasificacion').getForm().getValues(),{informacion: "consultaClasificacion"}),
										callback: procesaConsultaClasificacion
									});
		
								}else if (boton.text == 'Aceptar'){																					//***************radioButton->Registro (Aceptar)
									var cliente =  Ext.getCmp('cboCliente');
									if (Ext.isEmpty(cliente.getValue())){
										cliente.markInvalid('Debe Seleccionar una PyME');
										cliente.focus();
										return;
									}
									if ( Ext.isEmpty(Ext.getCmp('cboCategoria').getValue()) && Ext.isEmpty(Ext.getCmp('txt_plazo_finan').getValue()) ){
										Ext.Msg.alert('Registro','Debe capturar al menos una parametrizaci�n.</br>1 - Categoria.</br>2 - Plazo M�ximo por Cliente');
										return;
									}
									tp.el.mask('Enviando...', 'x-mask-loading');
									if (!Ext.isEmpty(Ext.getCmp('cboCategoria').getValue())){
										Ext.Ajax.request({
											url: '24forma07ext.data.jsp',
											params: {informacion: "obtenPlazoFin",
														claveCat: Ext.getCmp('cboCategoria').getValue()},
											callback: procesaObtenPlazoFin
										});
									}else{
										Ext.Ajax.request({
											url: '24forma07ext.data.jsp',
											params: Ext.apply(Ext.getCmp('formaClasificacion').getForm().getValues(),{informacion: "aceptarClasificacion"}),
											callback: procesaAceptaClasificacion
										});
									}
								}
							} //fin handler
						},{
							text: 'Limpiar',
							iconCls: 'icoLimpiar',
							handler: function() {
								Ext.getCmp('txt_plazo_finan').reset();
								Ext.getCmp('cboCategoria').reset();
								Ext.getCmp('cboCliente').reset();
								Ext.getCmp('cg_num_distribuidor').reset();
								
								Ext.getCmp('panAyudaClas').hide();
								Ext.getCmp('gridClasificacion').hide();
							}
						}
					]
				}]
		}
	];

	var fpWinCategorias = new Ext.form.FormPanel({
		id: 'formWinCategorias',
		width: 500,
		style: ' margin:0 auto;',
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype:'hidden',
				name:	'id_clas',
				id:	'hidModif'
			},{
				xtype: 'textfield',
				name:	'cve_cat',
				id: 'claveModif',
				anchor:'60%',
				maxLength: 25,
				allowBlank:false,
				disabled: true,
				fieldLabel: 'Clave'
			},{
				xtype: 'textfield',
				name:	'catnueva',
				id: 'catModif',
				anchor:'95%',
				maxLength: 60,
				allowBlank:false,
				fieldLabel: 'Categor�a'
			},{
				xtype: 'numberfield',
				name:	'plazoAut',
				id: 'plazoModif',
				anchor:'30%',
				maxLength: 3,
				allowBlank:false,
				fieldLabel: 'Plazo autorizado por categor�a'
			}
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptaCategoria',
				iconCls: 'aceptar',
				formBind: true,
				handler: function(boton, evento) {
					var data = categoriaData.reader.jsonData;
					newDataCategorias=[];
					Ext.each(data, function(item, index, arrItems){
						if (item.IC_CLASIFICACION == Ext.getCmp('hidModif').getValue()){
							item.CG_CLAVE=Ext.getCmp('claveModif').getValue()
							item.CG_DESCRIPCION=Ext.getCmp('catModif').getValue()
							item.IG_PLAZO_AUT=Ext.getCmp('plazoModif').getValue()
						}
						newDataCategorias.push(item);
					});
					fpWinCategorias.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24forma07ext.data.jsp',
						params: {informacion: "validaDiasCategorias", id_clas: Ext.getCmp('hidModif').getValue()},
						callback: procesaValidaDiasCategorias
					});
				} //fin handler
			},{
				text: 'Cancelar',
				iconCls: 'rechazar',
				handler: function() {
					Ext.getCmp('winCategorias').hide();
				}
			}
		]
	});

	var elementosCategorias = [
		{
			xtype: 'textfield',
			name: 'cve_cat',
			id: 'cve_cat',
			anchor:'50%',
			maxLength: 25,
			allowBlank:false,
			fieldLabel: 'Clave'
		},{
			xtype: 'textfield',
			name: 'catnueva',
			id: 'catnueva',
			anchor:'97%',
			maxLength: 60,
			allowBlank:false,
			fieldLabel: 'Categor�a'
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'compositefield1',
			height: 25,
			defaults: {
          height: 25
			},
			items:[
				{
					xtype: 'numberfield',
					name: 'plazoAut',
					id: 'plazoAut',
					width: 40,
					maxLength: 3,
					
					allowBlank:true,
					fieldLabel: 'Plazo autorizado por categor�a',
					msgTarget: 'side',
					margins: '0 20 0 0'
				},{
					xtype: 'button',
					id:'btnAyuda',
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(boton) {
									Ext.getCmp('panAyuda').show();
								}
				}
			]
		
		},{
			xtype:	'panel',
			id:		'panAyuda',
			layout:	'table',
			anchor:	'100%',
			hidden:	true,
			layoutConfig: {
				columns: 2
			},
			items:[
				{
					xtype: 'panel',
					//id:'lblA',
					html:'<div class="formas" align="left" style="color:green">'+
							'Plazo m�ximo por categor�a. En caso de que no este capturado se tomara</br>'+
							'el Plazo Autorizado por el Cliente.</br></br></div>'
				},{
					xtype: 'button',
					autoWidth: true,
					autoHeight: true,
					iconCls: 'rechazar',
					handler: function() {
									Ext.getCmp('panAyuda').hide();
								}
				}
			]
		},{
			xtype: 'grid',
			id:	'gridCategorias',
			store: categoriaData,
			columns: [
				{//0
					header: 'Clave', tooltip: 'Clave',
					dataIndex: 'CG_CLAVE',align: 'center',hideable: false,
					sortable: true,	resizable: true,	width: 100,	hidden: false
				},{//1
					header : 'Categor�a', tooltip: 'Categor�a',
					dataIndex : 'CG_DESCRIPCION', align: 'left',hideable: false,
					sortable : true, resizable: true,width : 280,	hidden: false
				},{//2
					header: 'Plazo Autorizado', tooltip: 'Plazo Autorizado',
					dataIndex: 'IG_PLAZO_AUT', align: 'center',hideable: false,
					sortable: true,	width: 100,	resizable: true, hidden: false
				},{//4
					xtype:	'actioncolumn',
					header : 'Seleccionar', tooltip: 'Seleccionar',
					dataIndex : '',align: 'center', hideable: false,
					sortable : true,	width : 90, hidden: false,	//renderer: renderButton
					items: [
						{
							getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Modificar';
									return 'modificar';
							},
							handler:	procesaModificarCategorias
						},{
							getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
									this.items[1].tooltip = 'Eliminar';
									return 'borrar';
							},
							handler:	procesaEliminarCategorias
						}
					]
				}
			],
			viewConfig: {forceFit: true},
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 200,
			width: 550,
			frame: false
		}
	];

	function resultPlazoInicial(res){
		if (res == 'no'){
			Ext.getCmp('mod_plazo_inicial').setValue(Ext.getCmp('mod_plazo_inicial_mod').getValue());
			return;
		}
	}

	function resultPlazoFinal(res){
		if (res == 'no'){
			Ext.getCmp('mod_plazo_final').setValue(Ext.getCmp('mod_plazo_final_mod').getValue());
			return;
		}
	}

	var fpWinPlazos = new Ext.form.FormPanel({
		id: 'formWinPlazos',
		width: 300,
		style: ' margin:0 auto;',
		frame: true,
		border: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype:'numberfield',
				name:'mod_plazo_inicial',
				id:	'mod_plazo_inicial',
				anchor:'65%',
				maxLength: 3,
				allowBlank: false,
				fieldLabel:'Plazo Inicial',
				listeners:{
					change:	function(field){
									Ext.Msg.confirm(field.fieldLabel,'Al cambiar el plazo inicial, se modificar� el plazo final del plazo anterior. �Desea continuar?',resultPlazoInicial)
								}
				}
			},{
				xtype:'numberfield',
				name:'mod_plazo_final',
				id:	'mod_plazo_final',
				anchor:'65%',
				maxLength: 3,
				allowBlank: false,
				fieldLabel:'Plazo Final',
				listeners:{
					change:	function(field){
									if (!Ext.isEmpty(Ext.getCmp('mod_ic_plazo_descuento_sig').getValue())){
										Ext.Msg.confirm(field.fieldLabel,'Al cambiar el plazo final, se modificar� el plazo inicial del siguiente plazo. �Desea continuar?',resultPlazoFinal)
									}
									
								}
				}
			},{
				xtype:'numberfield',
				name:'mod_porcentaje',
				id:	'mod_porcentaje',
				anchor:'65%',
				maxLength: 7,
				allowBlank: false,
				fieldLabel:'Porcentaje',
				listeners:{
					change:	function(field){
									var p = Ext.getCmp('mod_porcentaje_mod').getValue();
									var porc = field.getValue();
									var pant = Ext.getCmp('mod_porcentaje_ant').getValue();
									var psig = Ext.getCmp('mod_porcentaje_sig').getValue();
								
									if(parseFloat(pant) == 0 || pant == ''){
										if (parseFloat(porc) <= parseFloat(psig)){
											Ext.Msg.alert(field.fieldLabel,'El porcentaje debe ser mayor a ' + psig);
											field.setValue(p);
											return;
										}
									} else if (parseFloat(psig) == 0 || psig == ''){
										if (parseFloat(porc) >= parseFloat(pant)){
											Ext.Msg.alert(field.fieldLabel,'El porcentaje debe ser menor a ' + pant);
											field.setValue(p);
											return;
										}
									} else {
										if (parseFloat(pant) <= parseFloat(porc)){
											Ext.Msg.alert(field.fieldLabel,'El porcentaje debe ser entre ' + pant + ' y ' + psig);
											field.setValue(p);
											return;
										}
										
										if(parseFloat(porc) <= parseFloat(psig)){
											Ext.Msg.alert(field.fieldLabel,'El porcentaje debe ser entre ' + pant + ' y ' + psig);
											field.setValue(p);
											return;
										}
									}
								}
				}
			},{
					xtype:'hidden',
					name:	'mod_ic_plazo_descuento',
					id:	'mod_ic_plazo_descuento'
			},{
					xtype:'hidden',
					name:	'mod_plazo_inicial_mod',
					id:	'mod_plazo_inicial_mod'
			},{
					xtype:'hidden',
					name:	'mod_plazo_final_mod',
					id:	'mod_plazo_final_mod'
			},{
					xtype:'hidden',
					name:	'mod_ic_plazo_descuento_ant',
					id:	'mod_ic_plazo_descuento_ant'
			},{
					xtype:'hidden',
					name:	'mod_ic_plazo_descuento_sig',
					id:	'mod_ic_plazo_descuento_sig'
			},{
					xtype:'hidden',
					name:	'mod_porcentaje_mod',
					id:	'mod_porcentaje_mod'
			},{
					xtype:'hidden',
					name:	'mod_porcentaje_ant',
					id:	'mod_porcentaje_ant'
			},{
					xtype:'hidden',
					name:	'mod_porcentaje_sig',
					id:	'mod_porcentaje_sig'
			}
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptaPlazos',
				iconCls: 'aceptar',
				formBind: true,
				handler: function(boton, evento) {
					var data = plazosData.reader.jsonData;
					newDataPlazos=[];
					Ext.each(data, function(item, index, arrItems){
						if (item.IC_PLAZO_DESCUENTO == Ext.getCmp('mod_ic_plazo_descuento').getValue()){
							item.IG_PLAZO_INICIAL = Ext.getCmp('mod_plazo_inicial').getValue()
							item.IG_PLAZO_FINAL = Ext.getCmp('mod_plazo_final').getValue()
							item.FG_PORCENTAJE = Ext.getCmp('mod_porcentaje').getValue()
						}
						newDataPlazos.push(item);
					});
					fpWinPlazos.el.mask('Enviando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '24forma07ext.data.jsp',
							params: Ext.apply(fpWinPlazos.getForm().getValues(),{informacion: "modificaPlazos"}),
							callback: procesaModificaGridPlazos
						});
				} //fin handler
			},{
				text: 'Cancelar',
				iconCls: 'rechazar',
				handler: function() {
					Ext.getCmp('winPlazos').hide();
				}
			}
		]
	});

	var fpPlazos = new Ext.form.FormPanel({
		id: 'formaPlazos',
		width: 600,
		style: ' margin:0 auto;',
		//title:	'',
		hidden: false,
		frame: true,
		//collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosPlazos,
		monitorValid: true,
		buttons: [
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					fpPlazos.getForm().reset();
					Ext.getCmp('gridPlazos').hide();
				}
			},{
				text: 'Agregar',
				id: 'btnAgregarPlazos',
				iconCls: 'aceptar',
				formBind: true,
				handler: function(boton, evento) {

					var tot = (plazosData.getTotalCount())-1;
					var data = plazosData.reader.jsonData;
					if (!Ext.isEmpty(data)){
						Ext.each(data, function(item, index, arrItems){
							if (index == tot){
								Ext.getCmp('porcentaje_ant').setValue(item.FG_PORCENTAJE);
								Ext.getCmp('pInicial').setValue(parseFloat(item.IG_PLAZO_FINAL)+1);
								return false;
							}
						});
					}

					if ( Ext.getCmp('pInicial').getValue()!= 1 ){
						var pant = Ext.getCmp('porcentaje_ant').getValue();
						var porc = Ext.getCmp('pPorc');
						if (parseFloat(pant) <= parseFloat(porc.getValue())){
							Ext.Msg.alert(boton.text,'El porcentaje debe ser menor a ' + pant);
							porc.reset();
							return;
						}
					}
					tp.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24forma07ext.data.jsp',
						params: Ext.apply(fpPlazos.getForm().getValues(),{informacion: 'agregarPlazos'}),
						callback: procesaAgregarPlazos
					});
				} //fin handler
			},{
				text: 'Terminar',
				//iconCls: '',
				handler: function() {
					window.location = '24forma07ext.jsp';
				}
			}
		]
	});

	var fpWinClasificacion = new Ext.form.FormPanel({
		id: 'formWinClasificacion',
		width: 500,
		style: ' margin:0 auto;',
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype:'hidden',
				name:	'ic_pyme',
				id:	'cla_ic_pyme'
			},{
				xtype:	'textfield',
				fieldLabel:	'No. Distribuidor',
				name:	'cla_distribuidor',
				id:	'cla_distribuidor',
				anchor:'50%',
				maxLength:25,
				disabled: true,
				allowBlank: false
			},{
				xtype:	'textfield',
				fieldLabel:	'Cliente',
				name:	'cla_cliente',
				id:	'cla_cliente',
				disabled: true,
				allowBlank: false
			},{
				xtype:'combo',
				name:	'cla_categoria',
				id:	'cmbCategoria',
				fieldLabel:'Categor�a',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'cla_categoria',
				emptyText:	'Seleccione categor�a',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store: catalogoCategoriaData,
				tpl:'<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>'
			},{
				xtype: 'numberfield',
				name: 'cla_plazo_max',
				id: 'cla_plazo_max',
				width: 40,
				maxLength: 3,
				anchor:'30%',
				fieldLabel: 'Plazo m�ximo por cliente'
			},{
				xtype:'hidden',
				name:	'cla_plazo_finan',
				id:	'cla_plazo_finan'
			}
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptaClasificacion',
				iconCls: 'aceptar',
				formBind: true,
				handler: function(boton, evento) {
					var flag = true;
					var data = clasificacionData.reader.jsonData;
					newDataClasificacion=[];
					var desc = "";
					Ext.each(data, function(item, index, arrItems){
						if (item.IC_PYME == Ext.getCmp('cla_ic_pyme').getValue()){
							item.CG_RAZON_SOCIAL = Ext.getCmp('cla_cliente').getValue()
							item.IC_CLASIFICACION = Ext.getCmp('cmbCategoria').getValue()
							var disp = Ext.getCmp('cmbCategoria').lastSelectionText;
							desc = disp.slice(disp.indexOf(" ")+1);
							item.CG_DESCRIPCION = desc;
							item.IG_DIAS_MAXIMO = Ext.getCmp('cla_plazo_max').getValue()
						}
						newDataClasificacion.push(item);
					});
					fpWinClasificacion.el.mask('Enviando...', 'x-mask-loading');
					if (!Ext.isEmpty(Ext.getCmp('cmbCategoria').getValue())){
						if ( !Ext.isEmpty(Ext.getCmp('cla_plazo_finan').getValue()) && !Ext.isEmpty(Ext.getCmp('cla_plazo_max').getValue()) ){
							if (Ext.getCmp('cla_plazo_finan').getValue() < Ext.getCmp('cla_plazo_max').getValue()){
								flag = false;
								fpWinClasificacion.el.unmask();
								Ext.Msg.alert('','El Plazo Autorizado por categor�a debe ser mayor al par�metro Plazo autorizado por cliente');
							}
						}
					}
					if (flag) {
						Ext.Ajax.request({
							url: '24forma07ext.data.jsp',
							params: Ext.apply(fpWinClasificacion.getForm().getValues(),{informacion: "modificaClasificacion"}),
							callback: procesaModificaClasificacion
						});
					}
				} //fin handler
			},{
				text: 'Cancelar',
				iconCls: 'rechazar',
				handler: function() {
					Ext.getCmp('winClasificacion').hide();
				}
			}
		]
	});

	var fpClasificacion = new Ext.form.FormPanel({
		id: 'formaClasificacion',
		width: 600,
		style: ' margin:0 auto;',
		//title:	'',
		hidden: false,
		frame: false,
		border:false,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosClasificacion,
		monitorValid: true
	});


	var fpCategorias = new Ext.form.FormPanel({
		id: 'formaCategorias',
		width: 600,
		style: ' margin:0 auto;',
		//title:	'',
		hidden: false,
		frame: true,
		//collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosCategorias,
		monitorValid: true,
		buttonAlign: 'center',
		buttons: [
			{
				text: 'Agregar',
				id: 'btnAgregarCategorias',
				iconCls: 'aceptar',
				formBind: true,
				handler: function(boton, evento) {
					//fpCategorias.el.mask('Enviando...', 'x-mask-loading');
					tp.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24forma07ext.data.jsp',
						params: Ext.apply(fpCategorias.getForm().getValues(),{informacion: 'agregaCategorias'}),
						callback: procesaAgregaCategorias
					});
				} //fin handler
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					fpCategorias.getForm().reset();
					Ext.getCmp('panAyuda').hide();
				}
			},{
				text: 'Terminar',
				//iconCls: '',
				handler: function() {
					window.location = '24forma07ext.jsp';
				}
			}
		]
	});

	var tp = new Ext.TabPanel({
		activeTab: 0,
		forceLayout : true,
		width:940,
		style: ' margin:0 auto;',
		plain:true,
		hidden :true,
		id:'tabPanel',
		defaults:{autoHeight: true},
		items:[
			{
				title: '<div align="center">Categor�as</div>',
				id: 'tabCategoria',
				items: [
					{
						xtype: 'panel',
						id: 'msjCate',
						width: 690,
						frame: true,
						hidden: true,
						style: 'margin: 0 auto'
					},
					NE.util.getEspaciador(10),
					fpCategorias,
					NE.util.getEspaciador(10)
				]
			},{
				title: '<div align="center">Clasificaci�n</div>',
				id: 'tabClasificacion',
				items: [
					{
						xtype: 'panel',
						id: 'msjClas',
						width: 690,
						frame: true,
						hidden: true,
						style: 'margin: 0 auto'
					},
					NE.util.getEspaciador(10),
					fpClasificacion,NE.util.getEspaciador(5), gridClasificacion,
					NE.util.getEspaciador(10)
				]
			},{
				title: '<div align="center">Plazos</div>',
				id: 'tabPlazos',
				items: [
					{
						xtype: 'panel',
						id: 'msjPlaz',
						width: 690,
						frame: true,
						hidden: true,
						style: 'margin: 0 auto'
					},
					NE.util.getEspaciador(10),
					fpPlazos,
					NE.util.getEspaciador(10)
				]
			}
		],
		listeners:{
			tabchange:	function(tab, pan){
								Ext.getCmp('formClas').hide();
								Ext.getCmp('gridClasificacion').hide();
								Ext.getCmp('radioGp').reset();
								catalogoCategoriaData.load();
							}
		}
	});
	var fp = new Ext.form.FormPanel({
		id					: 'formMensaje',
		layout			: 'form',
		width				: 850,
		style				: ' margin:0 auto;',
		frame				: true,		
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 8px',
		hidden :true,
		labelWidth			: 250,
		monitorValid: true,
		defaults: { 	msgTarget: 'side', 	anchor: '-20'
		},
		items				: 
		[  
			{
				xtype: 'label',
				width		: 500,	
				id: 'mensajeServicio',
				align: 'center',
				value: ''
			}
		
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		
		items: [	fp,tp	]
	});
	
	tp.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24forma07ext.data.jsp',
		params: {informacion: "iniciaCategorias"},
		callback: procesaIniciaCategoria
	});

	
	// fp.doLayout();
	//console.dir(xBoxInner);
});