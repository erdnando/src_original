<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.anticipos.*,
	netropology.utilerias.*,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String infoRegresar	=	"";
	HashMap registro = new HashMap();
	JSONArray registros = new JSONArray();
	JSONObject jsonObj = new JSONObject();

	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	int liRegistros=0;
	int Registros1 = 0;
	int Registros2 = 0;
	int Registros3 = 0;
	int index=0;
	Vector vecFilas		= null;
	Vector vecColumnas	= null;
	Vector vecFilas2	= null;
	Vector vecColumnas2 = null;
	Vector lovTasas		= null;
	Vector vector		= null;
	String total = "",tipoCredito = "",pagoInt = "";		
	String aux="";
	boolean DM=true,CCC=true;
	
if(informacion.equals("Consulta")) {


	vecFilas = BeanTasas.disOpera(iNoCliente,4,"");
	for (int i = 0;i<vecFilas.size();i++) {
		vecColumnas = (Vector)vecFilas.get(i);
		total = (String)vecColumnas.get(0);
		tipoCredito = (String)vecColumnas.get(1);
		if (Integer.parseInt(total) > 0 && "D".equals(tipoCredito)){
			vecFilas2 = BeanTasas.esquemaParticular(iNoCliente,4,"","");
			for (int j=0; j<vecFilas2.size(); j++) {
				DM = false;
				Vector lovDatosTas = (Vector)vecFilas2.get(j);
				String ic_if = lovDatosTas.get(0).toString();
				String ifRazonSoc = lovDatosTas.get(1).toString();
				lovTasas = BeanTasas.ovgetTasasxEpo(4,iNoCliente,ic_if);
				String encabezado = ifRazonSoc;
				if(lovTasas.size()>0){
					for (int y=0; y<lovTasas.size(); y++) {
						Vector lovDatosTasa = (Vector)lovTasas.get(y);
						registro = new HashMap();						
						registro.put("NOMBREIF", encabezado );
						registro.put("NOMBREMONEDA", lovDatosTasa.get(0).toString()); 				
						registro.put("NOMBRETASA",lovDatosTasa.get(1).toString()); 				
						registro.put("RANGOPLAZO",lovDatosTasa.get(3).toString()); 					
						registro.put("VALOR",lovDatosTasa.get(4).toString()); 					
						registro.put("RELMAT",lovDatosTasa.get(5).toString()); 					
						registro.put("PTOS",lovDatosTasa.get(6).toString()); 					
						registro.put("TASA_APLICAR",lovDatosTasa.get(7).toString()); 	
						registro.put("FECHAULTIMA",lovDatosTasa.get(8).toString());								
						registros.add(registro);
					}
				}else{
					registro = new HashMap();						
					registro.put("NOMBREIF", encabezado);
					registro.put("NOMBREMONEDA", ""); 				
					registro.put("NOMBRETASA",""); 					
					registro.put("RANGOPLAZO",""); 	 					
					registro.put("VALOR",""); 						
					registro.put("RELMAT",""); 						
					registro.put("PTOS",""); 					
					registro.put("TASA_APLICAR",""); 	
					registro.put("FECHAULTIMA",""); 								
					registros.add(registro);
				}
			}//for (int j=0; j<vecFilas2.size(); j++)
		}
		if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito)){
			CCC = false;		
			String encabezado = "Esquema de tasas para operaciones con Crédito en Cuenta Corriente";
			lovTasas = BeanTasas.ovgetTasas(4);
			if(lovTasas.size()>0){
				for (int y=0; y<lovTasas.size(); y++) {
					Vector lovDatosTasa = (Vector)lovTasas.get(y);
					registro = new HashMap();						
					registro.put("NOMBREIF", encabezado );
					registro.put("NOMBREMONEDA", lovDatosTasa.get(0).toString()); 				
					registro.put("NOMBRETASA",lovDatosTasa.get(1).toString()); 				
					registro.put("RANGOPLAZO",lovDatosTasa.get(3).toString()); 					
					registro.put("VALOR",lovDatosTasa.get(4).toString()); 					
					registro.put("RELMAT",lovDatosTasa.get(5).toString()); 					
					registro.put("PTOS",lovDatosTasa.get(6).toString()); 					
					registro.put("TASA_APLICAR",lovDatosTasa.get(7).toString()); 	
					registro.put("FECHAULTIMA",lovDatosTasa.get(8).toString());								
					registros.add(registro);
				}
			}	
		}//if (Integer.parseInt(total) > 0 && "C".equals(tipoCredito))
	}//for (int i = 0;i<vecFilas.size();i++) 
	
								
infoRegresar =  "({\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"})";

}
%>



<%=infoRegresar%>