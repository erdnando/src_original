
	
Ext.onReady(function() {

	//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarCSV24 =  function(opts, success, response) {
		
		var btnGeneraCSV24 = Ext.getCmp('btnGeneraCSV24');
			btnGeneraCSV24.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV24 = Ext.getCmp('btnBajarCSV24');
			btnBajarCSV24.show();
			btnBajarCSV24.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV24.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGeneraCSV24.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	

		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF24 =  function(opts, success, response) {
		
		var btnGenerarPDF24 = Ext.getCmp('btnGenerarPDF24');
			btnGenerarPDF24.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF24 = Ext.getCmp('btnBajarPDF24');
			btnBajarPDF24.show();
			btnBajarPDF24.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF24.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF24.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarCSV21 =  function(opts, success, response) {
		
		var btnGeneraCSV21 = Ext.getCmp('btnGeneraCSV21');
			btnGeneraCSV21.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV21 = Ext.getCmp('btnBajarCSV21');
			btnBajarCSV21.show();
			btnBajarCSV21.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV21.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGeneraCSV21.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	

		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF21 =  function(opts, success, response) {
		
		var btnGenerarPDF21 = Ext.getCmp('btnGenerarPDF21');
			btnGenerarPDF21.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF21 = Ext.getCmp('btnBajarPDF21');
			btnBajarPDF21.show();
			btnBajarPDF21.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF21.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF21.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	

	//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarCSV02 =  function(opts, success, response) {
		
		var btnGeneraCSV02 = Ext.getCmp('btnGeneraCSV02');
			btnGeneraCSV02.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV02 = Ext.getCmp('btnBajarCSV02');
			btnBajarCSV02.show();
			btnBajarCSV02.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV02.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGeneraCSV02.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	var procesarSuccessFailureGenerarCSV022 =  function(opts, success, response) {
		
		var btnGeneraCSV022 = Ext.getCmp('btnGeneraCSV022');
			btnGeneraCSV022.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV022 = Ext.getCmp('btnBajarCSV022');
			btnBajarCSV022.show();
			btnBajarCSV022.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV022.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGeneraCSV022.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPDF022 =  function(opts, success, response) {
		
		var btnGenerarPDF022 = Ext.getCmp('btnGenerarPDF022');
			btnGenerarPDF022.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF022 = Ext.getCmp('btnBajarPDF022');
			btnBajarPDF022.show();
			btnBajarPDF022.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF022.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF022.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	

		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF02 =  function(opts, success, response) {
		
		var btnGenerarPDF02 = Ext.getCmp('btnGenerarPDF02');
			btnGenerarPDF02.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF02 = Ext.getCmp('btnBajarPDF02');
			btnBajarPDF02.show();
			btnBajarPDF02.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF02.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF02.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarCSV14 =  function(opts, success, response) {
		
		var btnGeneraCSV14 = Ext.getCmp('btnGeneraCSV14');
			btnGeneraCSV14.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV14 = Ext.getCmp('btnBajarCSV14');
			btnBajarCSV14.show();
			btnBajarCSV14.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV14.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGeneraCSV14.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	

		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF14 =  function(opts, success, response) {
		
		var btnGenerarPDF14 = Ext.getCmp('btnGenerarPDF14');
			btnGenerarPDF14.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF14 = Ext.getCmp('btnBajarPDF14');
			btnBajarPDF14.show();
			btnBajarPDF14.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF14.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF14.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	

		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarCSV22 =  function(opts, success, response) {
		
		var btnGeneraCSV22 = Ext.getCmp('btnGeneraCSV22');
			btnGeneraCSV22.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV22 = Ext.getCmp('btnBajarCSV22');
			btnBajarCSV22.show();
			btnBajarCSV22.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV22.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGeneraCSV22.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	

		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF22 =  function(opts, success, response) {
		
		var btnGenerarPDF22 = Ext.getCmp('btnGenerarPDF22');
			btnGenerarPDF22.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF22 = Ext.getCmp('btnBajarPDF22');
			btnBajarPDF22.show();
			btnBajarPDF22.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF22.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF22.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	


		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarCSV04 =  function(opts, success, response) {
		
		var btnGeneraCSV04 = Ext.getCmp('btnGeneraCSV04');
			btnGeneraCSV04.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV04 = Ext.getCmp('btnBajarCSV04');
			btnBajarCSV04.show();
			btnBajarCSV04.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV04.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGeneraCSV04.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	

		//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF04 =  function(opts, success, response) {
		
		var btnGenerarPDF04 = Ext.getCmp('btnGenerarPDF04');
			btnGenerarPDF04.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF04 = Ext.getCmp('btnBajarPDF04');
			btnBajarPDF04.show();
			btnBajarPDF04.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF04.setHandler( function(boton, evento) {
				var formaI = Ext.getDom('formAux');
				formaI.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				formaI.submit();
			});
		} else {
			btnGenerarPDF04.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	//No negociable a baja
	var procesarConsulta24Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid24.isVisible()) {
				grid24.show();
				grid04.hide();
				grid022.hide();
				grid22.hide();
				grid14.hide();
				grid02.hide();
				grid21.hide();
			}
		}		
		
		var el = grid24.getGridEl();
		if(store.getTotalCount() > 0) {
			Ext.getCmp('btnTotales24').enable();	
			Ext.getCmp('btnGeneraCSV24').enable();
			Ext.getCmp('btnGenerarPDF24').enable();
			Ext.getCmp('btnBajarCSV24').hide();
			Ext.getCmp('btnBajarPDF24').hide();	
			el.unmask();
		} else {
			Ext.getCmp('btnTotales24').disable();	
			Ext.getCmp('btnGeneraCSV24').disable();
			Ext.getCmp('btnGenerarPDF24').disable();
			Ext.getCmp('btnBajarCSV24').hide();
			Ext.getCmp('btnBajarPDF24').hide();	
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}		
	}
	
	//MODIFICACION	
	var procesarConsulta21Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid21.isVisible()) {
				grid21.show();
				grid04.hide();
				grid22.hide();
				grid022.hide();
				grid14.hide();
				grid02.hide();
				grid24.hide();
			}
		}		
		
		var el = grid21.getGridEl();
		if(store.getTotalCount() > 0) {
			Ext.getCmp('btnTotales21').enable();	
			Ext.getCmp('btnGeneraCSV21').enable();
			Ext.getCmp('btnGenerarPDF21').enable();
			Ext.getCmp('btnBajarCSV21').hide();
			Ext.getCmp('btnBajarPDF21').hide();	
			
			el.unmask();
		} else {
			Ext.getCmp('btnTotales21').disable();	
			Ext.getCmp('btnGeneraCSV21').disable();
			Ext.getCmp('btnGenerarPDF21').disable();
			Ext.getCmp('btnBajarCSV21').hide();
			Ext.getCmp('btnBajarPDF21').hide();	
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}		
	}
	var procesarConsulta022Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		var grid022 = Ext.getCmp('grid022');
			var el = grid022.getGridEl();
		if (arrRegistros != null) {
			
		
			fp.el.unmask();
			var jsonData = store.reader.jsonData;	
			var cm = grid022.getColumnModel();
			if(store.getTotalCount() > 0) {		
				Ext.getCmp('grid022').setTitle(jsonData.tituloGrid);
				Ext.getCmp('grid022').show();
				Ext.getCmp('btnTotales022').enable();	
				Ext.getCmp('btnGeneraCSV022').enable();
				Ext.getCmp('btnGenerarPDF022').enable();
				Ext.getCmp('btnBajarCSV022').hide();
				Ext.getCmp('btnBajarPDF022').hide();	
				el.unmask();
			} else {		
				Ext.getCmp('grid022').setTitle(jsonData.tituloGrid);
				Ext.getCmp('grid022').show();
				Ext.getCmp('btnTotales022').disable();	
				Ext.getCmp('btnGeneraCSV022').disable();
				Ext.getCmp('btnGenerarPDF022').disable();
				Ext.getCmp('btnBajarCSV022').hide();
				Ext.getCmp('btnBajarPDF022').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		} else {
			
			NE.util.mostrarConnError(response,opts);
		}
	}

	////Seleccionada Pyme a Negociable	
	var procesarConsulta02Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid02.isVisible()) {
				grid02.show();
				grid04.hide();
				grid22.hide();
				grid022.hide();
				grid14.hide();
				grid21.hide();
				grid24.hide();
			}
		}		
		
		var el = grid02.getGridEl();
		if(store.getTotalCount() > 0) {			
			Ext.getCmp('btnTotales02').enable();	
			Ext.getCmp('btnGeneraCSV02').enable();
			Ext.getCmp('btnGenerarPDF02').enable();
			Ext.getCmp('btnBajarCSV02').hide();
			Ext.getCmp('btnBajarPDF02').hide();					
			el.unmask();
		} else {		
			Ext.getCmp('btnTotales02').disable();	
			Ext.getCmp('btnGeneraCSV02').disable();
			Ext.getCmp('btnGenerarPDF02').disable();
			Ext.getCmp('btnBajarCSV02').hide();
			Ext.getCmp('btnBajarPDF02').hide();
			
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}		
	}
	
	
	//seleccionado pyme a rechazado IF
	var procesarConsulta14Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid14.isVisible()) {
				grid14.show();
				grid04.hide();
				grid22.hide();
				grid022.hide();
				grid02.hide();
				grid21.hide();
				grid24.hide();
			}
		}		
		
		var el = grid14.getGridEl();
		if(store.getTotalCount() > 0) {
			Ext.getCmp('btnTotales14').enable();	
			Ext.getCmp('btnGeneraCSV14').enable();
			Ext.getCmp('btnGenerarPDF14').enable();
			Ext.getCmp('btnBajarCSV14').hide();
			Ext.getCmp('btnBajarPDF14').hide();	
			
			el.unmask();
		} else {
			Ext.getCmp('btnTotales14').disable();	
			Ext.getCmp('btnGeneraCSV14').disable();
			Ext.getCmp('btnGenerarPDF14').disable();
			Ext.getCmp('btnBajarCSV14').hide();
			Ext.getCmp('btnBajarPDF14').hide();	
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}		
	}
	
			//Negociable a Vencido sin Operar
		var procesarConsulta22Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid22.isVisible()) {
				grid22.show();
				grid04.hide();
				grid14.hide();
				grid022.hide();
				grid02.hide();
				grid21.hide();
				grid24.hide();
			}
		}		
		
		var el = grid22.getGridEl();
		if(store.getTotalCount() > 0) {
			Ext.getCmp('btnTotales22').enable();	
			Ext.getCmp('btnGeneraCSV22').enable();
			Ext.getCmp('btnGenerarPDF22').enable();
			Ext.getCmp('btnBajarCSV22').hide();
			Ext.getCmp('btnBajarPDF22').hide();	
			
			el.unmask();
		} else {
			Ext.getCmp('btnTotales22').disable();	
			Ext.getCmp('btnGeneraCSV22').disable();
			Ext.getCmp('btnGenerarPDF22').disable();
			Ext.getCmp('btnBajarCSV22').hide();
			Ext.getCmp('btnBajarPDF22').hide();	
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}		
	}
	
		//negociable a baja	
		var procesarConsulta04Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid04.isVisible()) {
				grid04.show();
				grid22.hide();
				grid14.hide();
				grid02.hide();
				grid022.hide();
				grid21.hide();
				grid24.hide();
			}
		}		
		
		var el = grid04.getGridEl();
		if(store.getTotalCount() > 0) {
			Ext.getCmp('btnTotales04').enable();	
			Ext.getCmp('btnGeneraCSV04').enable();
			Ext.getCmp('btnGenerarPDF04').enable();
			Ext.getCmp('btnBajarCSV04').hide();
			Ext.getCmp('btnBajarPDF04').hide();	
			el.unmask();
		} else {
			Ext.getCmp('btnTotales04').disable();	
			Ext.getCmp('btnGeneraCSV04').disable();
			Ext.getCmp('btnGenerarPDF04').disable();
			Ext.getCmp('btnBajarCSV04').hide();
			Ext.getCmp('btnBajarPDF04').hide();	
			
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}		
	}
	
	////////////////////////////  TOTALES /////////////////////////////////////////////////////////
	
	var totales21Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_cambio_estatus: 21	
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'}		
	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});	

	var gridTotales21 = {
		xtype: 'grid',
		store: totales21Data,
		id: 'gridTotales21',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	};
	
	
	
	var totales04Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_cambio_estatus: 4	
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'}		
	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales04 = {
		xtype: 'grid',
		store: totales04Data,
		id: 'gridTotales04',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	};
	
	
	
	var totales24Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_cambio_estatus: 24	
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'}		
	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales24 = {
		xtype: 'grid',
		store: totales24Data,
		id: 'gridTotales24',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	};
	
	
	
	var totales22Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_cambio_estatus: 22	
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'}		
	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales22 = {
		xtype: 'grid',
		store: totales22Data,
		id: 'gridTotales22',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	};
	var totales022Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_cambio_estatus: 23
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_DESCUENTO', type: 'float', mapping: 'MONTO_DESCUENTO'},		
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'},	
			{name: 'MONTO_CREDITO', type: 'float', mapping: 'MONTO_CREDITO'},	
			{name: 'MONTO_INTERES', type: 'float', mapping: 'MONTO_INTERES'},	
			{name: 'MONTO_CAPITAL_INT', type: 'float', mapping: 'MONTO_CAPITAL_INT'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	var totales0221Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_cambio_estatus: 34
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_DESCUENTO', type: 'float', mapping: 'MONTO_DESCUENTO'},		
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'},	
			{name: 'MONTO_CREDITO', type: 'float', mapping: 'MONTO_CREDITO'},	
			{name: 'MONTO_INTERES', type: 'float', mapping: 'MONTO_INTERES'},	
			{name: 'MONTO_CAPITAL_INT', type: 'float', mapping: 'MONTO_CAPITAL_INT'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var totales02Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_cambio_estatus: 2	
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_DESCUENTO', type: 'float', mapping: 'MONTO_DESCUENTO'},		
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'},	
			{name: 'MONTO_CREDITO', type: 'float', mapping: 'MONTO_CREDITO'},	
			{name: 'MONTO_INTERES', type: 'float', mapping: 'MONTO_INTERES'},	
			{name: 'MONTO_CAPITAL_INT', type: 'float', mapping: 'MONTO_CAPITAL_INT'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales02 = {
		xtype: 'grid',
		store: totales02Data,
		id: 'gridTotales02',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{
				header: 'Total Monto Descuento',
				dataIndex: 'MONTO_DESCUENTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto Credito',
				dataIndex: 'MONTO_CREDITO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto de Intereses',
				dataIndex: 'MONTO_INTERES',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Total(C�pital e Intereses)',
				dataIndex: 'MONTO_CAPITAL_INT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}					
			
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	};
		
	var totales14Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			ic_cambio_estatus: 14	
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_DESCUENTO', type: 'float', mapping: 'MONTO_DESCUENTO'},		
			{name: 'MONTO_VALUADO', type: 'float', mapping: 'MONTO_VALUADO'},	
			{name: 'MONTO_CREDITO', type: 'float', mapping: 'MONTO_CREDITO'},	
			{name: 'MONTO_INTERES', type: 'float', mapping: 'MONTO_INTERES'},	
			{name: 'MONTO_CAPITAL_INT', type: 'float', mapping: 'MONTO_CAPITAL_INT'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales14 = {
		xtype: 'grid',
		store: totales14Data,
		id: 'gridTotales14',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{
				header: 'Total Monto Descuento',
				dataIndex: 'MONTO_DESCUENTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto Valuado',
				dataIndex: 'MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto Credito',
				dataIndex: 'MONTO_CREDITO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto de Intereses',
				dataIndex: 'MONTO_INTERES',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Total(C�pital e Intereses)',
				dataIndex: 'MONTO_CAPITAL_INT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}								
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	};
	
	////////////////////////////////////////  CONSULTAS /////////////////////////////////////////////////////////////////////
	
		//no negociable a baja
		var consulta24Data = new Ext.data.JsonStore({
			root : 'registros',
			url : '24reporte02Ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [   
				{name: 'NOMBRE_DIST'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION' , type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'},    
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'}, 
				{name: 'MONTO_DESCUENTO'}, 						
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},				
				{name: 'MONTO_VALUADO', type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'CT_CAMBIO_MOTIVO'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta24Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta24Data(null, null, null);						
					}
				}
			}
		});
		
	
		//negociable a baja
		var consulta04Data = new Ext.data.JsonStore({
			root : 'registros',
			url : '24reporte02Ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
				{name: 'NOMBRE_DIST'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION' , type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'}, 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'}, 
				{name: 'MONTO_DESCUENTO'}, 						
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},				
				{name: 'MONTO_VALUADO', type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'CT_CAMBIO_MOTIVO'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta04Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta04Data(null, null, null);						
					}
				}
			}
		});
		
			//Negociable a Vencido sin Operar
		var consulta22Data = new Ext.data.JsonStore({
			root : 'registros',
			url : '24reporte02Ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
				{name: 'NOMBRE_DIST'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION' , type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'}, 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'}, 
				{name: 'MONTO_DESCUENTO'}, 						
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},				
				{name: 'MONTO_VALUADO', type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'CT_CAMBIO_MOTIVO'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta22Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta22Data(null, null, null);						
					}
				}
			}
		});
		
		
		//seleccionado pyme a rechazado IF
		var consulta14Data = new Ext.data.JsonStore({
			root : 'registros',
			url : '24reporte02Ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
				{name: 'NOMBRE_DIST'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION' , type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'}, 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'}, 
				{name: 'MONTO_DESCUENTO'}, 						
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},				
				{name: 'MONTO_VALUADO', type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'TIPO_CREDITO'},
				{name: 'MONTO_CREDITO'},
				{name: 'IG_PLAZO_CREDITO'},
				{name: 'FECHA_VENC_CREDITO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'NOMBRE_IF'},
				{name: 'REFERENCIA_INT'},
				{name: 'VALOR_TASA_INT'}	,
				{name: 'MONTO_CAP_INT'},
				{name: 'MONTO_TASA_INT'},
				{name: 'IC_DOCUMENTO'},
				{name: 'DF_OPERACIONIF'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta14Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta14Data(null, null, null);						
					}
				}
			}
		});
		
		
			//MODIFICACION
		var consulta21Data = new Ext.data.JsonStore({
			root : 'registros',
			url : '24reporte02Ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
				{name: 'NOMBRE_DIST'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION' , type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'}, 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'}, 
				{name: 'MONTO_DESCUENTO'}, 						
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},				
				{name: 'MONTO_VALUADO', type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'FECHA_EMISION_ANT', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'FECHA_VENC_ANT' , type: 'date', dateFormat: 'd/m/Y'},				
				{name: 'FN_MONTO_ANTERIOR'},
				{name: 'MODO_PLAZO_ANT'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta21Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta21Data(null, null, null);						
					}
				}
			}
		});
		
		
			////Seleccionada Pyme a Negociable	
		var consulta02Data = new Ext.data.JsonStore({
			root : 'registros',
			url : '24reporte02Ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
				{name: 'NOMBRE_DIST'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION' , type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'}, 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'}, 
				{name: 'MONTO_DESCUENTO'}, 						
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},				
				{name: 'MONTO_VALUADO', type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'TIPO_CREDITO'},
				{name: 'MONTO_CREDITO'},
				{name: 'IG_PLAZO_CREDITO'},
				{name: 'FECHA_VENC_CREDITO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'NOMBRE_IF'},
				{name: 'REFERENCIA_INT'},
				{name: 'VALOR_TASA_INT'}	,
				{name: 'MONTO_CAP_INT'},
				{name: 'MONTO_TASA_INT'},
				{name: 'IC_DOCUMENTO'},
				{name: 'DF_OPERACIONIF'},	
				{name: 'FN_PORC_AFORO'}	,
				{name: 'MONTO_DESC'},
				{name: 'FN_PORC_AFORO'}	,
				{name: 'MONTO_DESC'},
				{name: 'IC_DOCUMENTO'}	
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta02Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta02Data(null, null, null);						
					}
				}
			}
		});
		var consulta022Data = new Ext.data.JsonStore({
			root : 'registros',
			url : '24reporte02Ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
				{name: 'NOMBRE_DIST'},
				{name: 'CC_ACUSE'}, 
				{name: 'IG_NUMERO_DOCTO'}, 
				{name: 'DF_FECHA_EMISION' , type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'IG_PLAZO_DOCTO'}, 				
				{name: 'MONEDA' }, 
				{name: 'FN_MONTO', type: 'float'}, 
				{name: 'IG_PLAZO_DESCUENTO'}, 
				{name: 'FN_PORC_DESCUENTO'}, 
				{name: 'MONTO_DESCUENTO'}, 						
				{name: 'TIPO_CONVERSION'}, 
				{name: 'TIPO_CAMBIO'},				
				{name: 'MONTO_VALUADO', type: 'float'}, 
				{name: 'MODO_PLAZO'},	
				{name: 'FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'TIPO_CREDITO'},
				{name: 'MONTO_CREDITO'},
				{name: 'IG_PLAZO_CREDITO'},
				{name: 'FECHA_VENC_CREDITO', type: 'date', dateFormat: 'd/m/Y'},
				{name: 'NOMBRE_IF'},
				{name: 'REFERENCIA_INT'},
				{name: 'VALOR_TASA_INT'}	,
				{name: 'MONTO_CAP_INT'},
				{name: 'MONTO_TASA_INT'},
				{name: 'DF_OPERACIONIF'},	
				{name: 'NOMBRE_EPO'}	,
				{name: 'IC_DOCUMENTO'},
				{name: 'MONTO_DESC'}	
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			//autoLoad: false,
			listeners: {
				load: procesarConsulta022Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta022Data(null, null, null);						
					}
				}
			}
		});
		var gridTotales022 = {
		xtype: 'grid',
		store: totales022Data,
		id: 'gridTotales02',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{
				header: 'Total Monto Descuento',
				dataIndex: 'MONTO_DESCUENTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto Credito',
				dataIndex: 'MONTO_CREDITO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto de Intereses',
				dataIndex: 'MONTO_INTERES',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Total(C�pital e Intereses)',
				dataIndex: 'MONTO_CAPITAL_INT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}					
			
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	};
	var gridTotales0221 = {
		xtype: 'grid',
		store: totales0221Data,
		id: 'gridTotales02',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'Total Documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{
				header: 'Total Monto Descuento',
				dataIndex: 'MONTO_DESCUENTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto Credito',
				dataIndex: 'MONTO_CREDITO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto de Intereses',
				dataIndex: 'MONTO_INTERES',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Total(C�pital e Intereses)',
				dataIndex: 'MONTO_CAPITAL_INT',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}					
			
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	};
		var grid022 = new Ext.grid.GridPanel({
		id: 'grid022',
		store: consulta022Data,
		hidden: true,
		margins: '20 0 0 0',
		title: 'Seleccionado Pyme a Negociable',
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de Acuse de carga',
				tooltip: 'N�mero de Acuse de carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo de documento',
				tooltip: 'Plazo de documento',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Porcentaje de descuento',
				tooltip: 'Porcentaje de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto porcenteje de descuento',
				tooltip: 'Monto porcenteje de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'N�mero de documento final',
				tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'IG_PLAZO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')

			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'	
			},
			{
				header: 'Referencia de tasa de inter�s',
				tooltip: 'Referencia de tasa de inter�s',
				dataIndex: 'REFERENCIA_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Valor Tasa de Inter�s',
				tooltip: 'Valor Tasa de Inter�s',
				dataIndex: 'VALOR_TASA_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
				},
				{
				header: 'Monto de Inter�s',
				tooltip: 'Monto de Inter�s',
				dataIndex: 'MONTO_TASA_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Total de capital',
				tooltip: 'Monto Total de capital',
				dataIndex: 'MONTO_CAP_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		bbar: {
			id: 'barraPaginacion022',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales022',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales022');
						var tutulo = "";
						if(Ext.getCmp('ic_cambio_estatus1').getValue()=='23'){
							titulo = "En Proceso de Autorizacion IF a Negociable";
						}else{
							titulo = "Proceso de Autorizacion IF a Seleccionado Pyme";
						}
						if (ventana) {
							ventana.show();
						} else {
							if(Ext.getCmp('ic_cambio_estatus1').getValue()=='23'){
								titulo = "En Proceso de Autorizacion IF a Negociable";
								totales022Data.load();
							
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales022',
								closeAction: 'hide',
								items: [
									gridTotales022
								],								
								title: titulo
							}).show().alignTo(grid022.el);
							}else{
								titulo = "Proceso de Autorizacion IF a Seleccionado Pyme";
								totales0221Data.load();
							
								new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales022',
								closeAction: 'hide',
								items: [
									gridTotales0221
								],								
								title: titulo
								}).show().alignTo(grid022.el);
							}
						}
					}
				},
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGeneraCSV022',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoCSV'	
								}),
								callback: procesarSuccessFailureGenerarCSV022
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV022',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF022',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoPDF'								
								}),
								callback: procesarSuccessFailureGenerarPDF022
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF022',
					hidden: true
				}					
			]	
		}
	});
		
		var grid24 = new Ext.grid.GridPanel({
		store: consulta24Data,
		id: 'grid24',
		hidden: true,
		margins: '20 0 0 0',
		title: 'NO Negociable a Baja',
		columns: [
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de Acuse de Carga',
				tooltip: 'N�mero de Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Tipo cambio.',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto valuado en pesos.',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Fecha de Cambio',
				tooltip: 'Fecha de Cambio',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Causa',
				tooltip: 'Causa',
				dataIndex: 'CT_CAMBIO_MOTIVO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'			
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,	
		bbar: {
			id: 'barraPaginacion24',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales24',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales24');
						if (ventana) {
							ventana.show();
						} else {
								totales24Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales24',
								closeAction: 'hide',
								items: [
									gridTotales24
								],								
								title: 'Totales No Negociable a Baja'
							}).show().alignTo(grid24.el);
						}
					}
				},
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGeneraCSV24',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoCSV'	
								}),
								callback: procesarSuccessFailureGenerarCSV24
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV24',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF24',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoPDF'								
								}),
								callback: procesarSuccessFailureGenerarPDF24
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF24',
					hidden: true
				}					
			]	
		}
	});
	
		
		var grid04 = new Ext.grid.GridPanel({
		store: consulta04Data,
		hidden: true,
		margins: '20 0 0 0',
		id:'grid04',
		title: 'Negociable a Baja',
		columns: [
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de Acuse de Carga',
				tooltip: 'N�mero de Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Tipo cambio.',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto valuado en pesos.',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Fecha de Cambio',
				tooltip: 'Fecha de Cambio',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Causa',
				tooltip: 'Causa',
				dataIndex: 'CT_CAMBIO_MOTIVO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'			
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,	
		bbar: {
			id: 'barraPaginacion04',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales04',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales04');
						if (ventana) {
							ventana.show();
						} else {
								totales04Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales04',
								closeAction: 'hide',
								items: [
									gridTotales04
								],								
								title: 'Totales Negociable a Baja'
							}).show().alignTo(grid04.el);
						}
					}
				},
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGeneraCSV04',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoCSV'	
								}),
								callback: procesarSuccessFailureGenerarCSV04
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV04',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF04',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoPDF'								
								}),
								callback: procesarSuccessFailureGenerarPDF04
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF04',
					hidden: true
				}					
			]	
		}
	});
	
	//Negociable a Vencido sin Operar
	var grid22 = new Ext.grid.GridPanel({
		store: consulta22Data,
		hidden: true,
		id:'grid22',
		margins: '20 0 0 0',
		title: 'Negociable a Vencido sin Operar',
		columns: [
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de Acuse de Carga',
				tooltip: 'N�mero de Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Tipo cambio.',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto valuado en pesos.',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Fecha de Cambio',
				tooltip: 'Fecha de Cambio',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Causa',
				tooltip: 'Causa',
				dataIndex: 'CT_CAMBIO_MOTIVO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'			
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		bbar: {
			id: 'barraPaginacion22',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales22',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales22');
						if (ventana) {
							ventana.show();
						} else {
								totales22Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales22',
								closeAction: 'hide',
								items: [
									gridTotales22
								],								
								title: 'Totales Negociable a Vencido sin Operar'
							}).show().alignTo(grid22.el);
						}
					}
				},
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGeneraCSV22',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoCSV'	
								}),
								callback: procesarSuccessFailureGenerarCSV22
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV22',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF22',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoPDF'								
								}),
								callback: procesarSuccessFailureGenerarPDF22
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF22',
					hidden: true
				}					
			]	
		}
	});
	///
	
	//seleccionado pyme a rechazado IF
	var grid14 = new Ext.grid.GridPanel({
		store: consulta14Data,
		hidden: true,
		margins: '20 0 0 0',
		id:'grid14',
		title: 'Seleccionado pyme a rechazado IF',
		columns: [
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de Acuse de Carga',
				tooltip: 'N�mero de Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Tipo cambio.',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto valuado en pesos.',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Tipo de cr�dito',
				tooltip: 'Tipo de cr�dito',
				dataIndex: 'TIPO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'N�mero de documento final',
				tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'IG_PLAZO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de operaci�n IF',
				tooltip: 'Fecha de operaci�n IF',
				dataIndex: 'DF_OPERACIONIF', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'	
			},
			{
				header: 'Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'	
				},
				{
				header: 'Monto de Intereses',
				tooltip: 'Monto de Intereses',
				dataIndex: 'MONTO_TASA_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Total(C�pital e Intereses)',
				tooltip: 'Monto Total(C�pital e Intereses)',
				dataIndex: 'MONTO_CAP_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Fecha de Rechazo',
				tooltip: 'Fecha de Rechazo',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}		
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		bbar: {
			id: 'barraPaginacion14',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales14',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales14');
						if (ventana) {
							ventana.show();
						} else {
								totales14Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,								
								height: 105,
								id: 'winTotales14',
								closeAction: 'hide',
								items: [
									gridTotales14
								],								
								title: 'Totales Seleccionado pyme a rechazado IF'
							}).show().alignTo(grid14.el);
						}
					}
				},
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGeneraCSV14',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoCSV'	
								}),
								callback: procesarSuccessFailureGenerarCSV14
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV14',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF14',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoPDF'								
								}),
								callback: procesarSuccessFailureGenerarPDF14
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF14',
					hidden: true
				}					
			]	
		}
	});
		
		 
		
	//Seleccionada Pyme a Negociable	
	var grid02 = new Ext.grid.GridPanel({
		store: consulta02Data,
		hidden: true,
		margins: '20 0 0 0',
		id:'grid02',
		title: 'Seleccionado Pyme a Negociable',
		columns: [
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de Acuse de Carga',
				tooltip: 'N�mero de Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: '% de descuento Aforo',
				tooltip: '% de descuento Aforo',
				dataIndex: 'FN_PORC_AFORO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Monto a descontar',
				tooltip: 'Monto a descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto % de descuento',
				tooltip: '% de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Tipo cambio.',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto valuado en pesos.',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Tipo de cr�dito',
				tooltip: 'Tipo de cr�dito',
				dataIndex: 'TIPO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'N�mero de documento final',
				tooltip: 'N�mero de documento final',
				dataIndex: 'IC_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'IG_PLAZO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')

			},
			{
				header: 'Fecha de operaci�n IF',
				tooltip: 'Fecha de operaci�n IF',
				dataIndex: 'DF_OPERACIONIF', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')

			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'	
			},
			{
				header: 'Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
				},
				{
				header: 'Monto de Intereses',
				tooltip: 'Monto de Intereses',
				dataIndex: 'MONTO_TASA_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Total(C�pital e Intereses)',
				tooltip: 'Monto Total(C�pital e Intereses)',
				dataIndex: 'MONTO_CAP_INT', 
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Fecha de Rechazo',
				tooltip: 'Fecha de Rechazo',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}		
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		bbar: {
			id: 'barraPaginacion02',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales02',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales02');
						if (ventana) {
							ventana.show();
						} else {
								totales02Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales02',
								closeAction: 'hide',
								items: [
									gridTotales02
								],								
								title: 'Totales Seleccionado Pyme a Negociable'
							}).show().alignTo(grid02.el);
						}
					}
				},
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGeneraCSV02',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoCSV'	
								}),
								callback: procesarSuccessFailureGenerarCSV02
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV02',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF02',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoPDF'								
								}),
								callback: procesarSuccessFailureGenerarPDF02
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF02',
					hidden: true
				}					
			]	
		}
	});
	
	
	
	//MODIFICACION	
	var grid21 = new Ext.grid.GridPanel({
		store: consulta21Data,
		hidden: true,
		margins: '20 0 0 0',
		id:'grid21',
		title: 'MODIFICACION',		
		columns: [
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de Acuse de Carga',
				tooltip: 'N�mero de Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},	
			
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto % de descuento',
				tooltip: '% de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Tipo cambio.',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: 'Monto valuado en pesos.',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			},				
			{
				header: 'Fecha de Cambio',
				tooltip: 'Fecha de Cambio',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Anterior Fecha de Emisi�n',
				tooltip: 'Anterior Fecha de Emisi�n',
				dataIndex: 'FECHA_EMISION_ANT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	,
			{
				header: 'Anterior Fecha de Vencimiento',
				tooltip: 'Anterior Fecha de Vencimiento',
				dataIndex: 'FECHA_VENC_ANT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Anterior Monto del Documento',
				tooltip: 'Anterior Monto del Documento',
				dataIndex: 'FN_MONTO_ANTERIOR',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Anterior Modalidad de Plazo',
				tooltip: 'Anterior Modalidad de Plazo',
				dataIndex: 'MODO_PLAZO_ANT',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			}	
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		bbar: {
			id: 'barraPaginacion21',				
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales21',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales21');
						if (ventana) {
							ventana.show();
						} else {
								totales21Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales21',
								closeAction: 'hide',
								items: [
									gridTotales21
								],								
								title: 'Totales MODIFICACION'
							}).show().alignTo(grid21.el);
						}
					}
				},
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGeneraCSV21',						
					formBind: true,
					handler: function(boton, evento) {						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoCSV'	
								}),
								callback: procesarSuccessFailureGenerarCSV21
							});
					}
				}, 				
				{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV21',
						hidden: true
					},						
					{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF21',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '24reporte02Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consulta',
									operacion:'ArchivoPDF'								
								}),
								callback: procesarSuccessFailureGenerarPDF21
							});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF21',
					hidden: true
				}					
			]	
		}
	});
	
	
	
	
		var catalogoEstatusData = new Ext.data.JsonStore({
			id: 'catalogoEstatusDataStore',
			root : 'registros',
			fields : ['clave', 'descripcion', 'loadMsg'],
			url : '24reporte02Ext.data.jsp',
			baseParams: {
				informacion: 'catalogoEstatus'				
			},
			totalProperty : 'total',
			autoLoad: false,		
			listeners: {
				exception: NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});
	

	var elementosForma = [
			{
				xtype: 'combo',
				name: 'ic_cambio_estatus',
				id: 'ic_cambio_estatus1',
				fieldLabel: 'Estatus',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'ic_cambio_estatus',
				emptyText: 'Seleccione...',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : catalogoEstatusData,				
				listeners: {
        select: {
					fn: function (combo) {
						if(combo.getValue()=='23' || combo.getValue()=='34'){
								
								Ext.getCmp("grid04").hide();
								Ext.getCmp("grid22").hide();	
								Ext.getCmp("grid14").hide();
								Ext.getCmp('grid02').hide();
								Ext.getCmp('grid21').hide();
								Ext.getCmp('grid24').hide();
								fp.el.mask('Procesando...', 'x-mask-loading');
								consulta022Data.load({
									params: {
										ic_cambio_estatus:combo.getValue(),
										operacion: 'Generar'
									}				
								});
								
						} 
						
						var estatus = Ext.getCmp("ic_cambio_estatus1").getValue();	
						if(estatus ==4 ){						
							consulta04Data.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}
						
							if(estatus ==22 ){						
							consulta22Data.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}
						
						if(estatus ==14 ){						
							consulta14Data.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}
						
						if(estatus ==2 ){						
							consulta02Data.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}
						if(estatus ==21 ){						
							consulta21Data.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}
						
							if(estatus ==24 ){						
							consulta24Data.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}
						
											
						Ext.getCmp('btnTotales04').disable();	
						Ext.getCmp('btnGeneraCSV04').disable();
						Ext.getCmp('btnGenerarPDF04').disable();
						Ext.getCmp('btnBajarCSV04').hide();
						Ext.getCmp('btnBajarPDF04').hide();
						
						Ext.getCmp('btnTotales02').disable();	
						Ext.getCmp('btnGeneraCSV02').disable();
						Ext.getCmp('btnGenerarPDF02').disable();
						Ext.getCmp('btnBajarCSV02').hide();
						Ext.getCmp('btnBajarPDF02').hide();
						
						Ext.getCmp('btnTotales22').disable();	
						Ext.getCmp('btnGeneraCSV22').disable();
						Ext.getCmp('btnGenerarPDF22').disable();
						Ext.getCmp('btnBajarCSV22').hide();
						Ext.getCmp('btnBajarPDF22').hide();
						
						Ext.getCmp('btnTotales14').disable();	
						Ext.getCmp('btnGeneraCSV14').disable();
						Ext.getCmp('btnGenerarPDF14').disable();
						Ext.getCmp('btnBajarCSV14').hide();
						Ext.getCmp('btnBajarPDF14').hide();
												
						Ext.getCmp('btnTotales21').disable();	
						Ext.getCmp('btnGeneraCSV21').disable();
						Ext.getCmp('btnGenerarPDF21').disable();
						Ext.getCmp('btnBajarCSV21').hide();
						Ext.getCmp('btnBajarPDF21').hide();
						
						Ext.getCmp('btnTotales24').disable();	
						Ext.getCmp('btnGeneraCSV24').disable();
						Ext.getCmp('btnGenerarPDF24').disable();
						Ext.getCmp('btnBajarCSV24').hide();
						Ext.getCmp('btnBajarPDF24').hide();
						
						var winTotales04 = Ext.getCmp('winTotales04');
						if (winTotales04) {					
							winTotales04.destroy();
						}
						
						var winTotales21 = Ext.getCmp('winTotales21');
						if (winTotales21) {					
							winTotales21.destroy();
						}
						
						var winTotales22 = Ext.getCmp('winTotales22');
						if (winTotales22) {					
							winTotales22.destroy();
						}
						
						var winTotales24 = Ext.getCmp('winTotales24');
						if (winTotales24) {					
							winTotales24.destroy();
						}
						
						var winTotales02 = Ext.getCmp('winTotales02');
						if (winTotales02) {					
							winTotales02.destroy();
						}
						
						var winTotales022 = Ext.getCmp('winTotales022');
						if (winTotales022) {					
							winTotales022.destroy();
						}
						
						
						var winTotales14 = Ext.getCmp('winTotales14');
						if (winTotales14) {					
							winTotales14.destroy();
						}
					
						}
					}
      },
			tpl : NE.util.templateMensajeCargaCombo	
			
				}		
			]
	

	var fp = new Ext.form.FormPanel({
			id: 'forma',
			width: 500,
			title: 'Cambios de Estatus',
			frame: true,
			collapsible: true,
			titleCollapse: false,
			style: 'margin:0 auto;',
			bodyStyle: 'padding: 6px',
			labelWidth: 110,
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			items: elementosForma,			
			monitorValid: true		
		});		
		

		//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			applyTo: 'areaContenido',
			style: 'margin:0 auto;',
			width: 949,
			items: [
				fp,
				NE.util.getEspaciador(20),
				grid04,
				grid22,
				grid14,
				grid022,
				grid02,
				grid21,
				grid24,
				NE.util.getEspaciador(20)
			]
		});
	
	
	catalogoEstatusData.load();

} );