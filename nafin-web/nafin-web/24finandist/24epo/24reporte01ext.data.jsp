<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	if(informacion.equals("valoresIniciales")){
		CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB",CargaDocDist.class);
		String tipoCredito = cargaDocto.getTipoCredito(iNoCliente);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("tipoCredito",tipoCredito);
		infoRegresar = jsonObj.toString();
	}
	else if(informacion.equals("CatalogoEstatusDist")){
		CatalogoSimple cat1 = new CatalogoSimple();
		cat1.setCampoClave("ic_estatus_docto");
		cat1.setCampoDescripcion("cd_descripcion");
		cat1.setTabla("comcat_estatus_docto");
		cat1.setValoresCondicionIn("1,2,3,5,9,11,22,24,32", Integer.class);//MOD +(24)
		cat1.setOrden("1");
		List elementos = cat1.getListaElementos();
		JSONArray jsonArr = new JSONArray();
	
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			String cad = "D"+obj.toString();
			String[] result = cad.split("-");
			ElementoCatalogo eleCat = new ElementoCatalogo();
			for (int x=0; x<result.length; x++){
				eleCat.setClave(result[0]);
				eleCat.setDescripcion(result[1]);
			}
			jsonArr.add(JSONObject.fromObject(eleCat));	
		}
		
		CatalogoSimple cat2 = new CatalogoSimple();
		cat2.setCampoClave("ic_estatus_solic");
		cat2.setCampoDescripcion("cd_descripcion");
		cat2.setTabla("comcat_estatus_solic");
		cat2.setValoresCondicionIn("1", Integer.class);
		
		List elementos2 = cat2.getListaElementos();
		Iterator it2 = elementos2.iterator();
		while(it2.hasNext()) {
			Object obj = it2.next();
			String cad = "C"+obj.toString();
			String[] result = cad.split("-");
			ElementoCatalogo eleCat2 = new ElementoCatalogo();
			for (int x=0; x<result.length; x++){
				eleCat2.setClave(result[0]);
				eleCat2.setDescripcion("Seleccionado IF / Operado");
			}
			jsonArr.add(JSONObject.fromObject(eleCat2));
		}
		infoRegresar = "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}
	else if(informacion.equals("Consulta")
	||informacion.equals("ArchivoTotalPDF")||informacion.equals("ArchivoCSV")
	||informacion.equals("ResumenTotales")){
		CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB",CargaDocDist.class);
		String tipoCredito = cargaDocto.getTipoCredito(iNoCliente);
		String estatusDocto = "";
		String claveEstatus  = (request.getParameter("status")!=null)?request.getParameter("status"):"";
		com.netro.distribuidores.RepEstatusEpo paginador = new com.netro.distribuidores.RepEstatusEpo();
		paginador.setClaveEpo(iNoCliente);
		if(claveEstatus.length()==2){
			estatusDocto = claveEstatus.substring(1,2);			
		}else{
			estatusDocto = claveEstatus.substring(1,3);			
		}
		String tipoSolic = claveEstatus.substring(0,1);
		paginador.setEstatusDocto(estatusDocto);
		paginador.setTipoSolic(tipoSolic);
		paginador.setTipoCredito(tipoCredito);
			
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		if(informacion.equals("Consulta")){
			try{
				Registros reg	= queryHelper.doSearch();
				while(reg.next()){
					double monto = Double.parseDouble(reg.getString("FN_MONTO"));
					double tipoCambio = Double.parseDouble(reg.getString("TIPO_CAMBIO"));
					String porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
					double montoDescontar	= monto*Double.parseDouble(porcDescuento)/100;
					double montoValuado		= (monto-montoDescontar)*tipoCambio;
					reg.setObject("MONTO_VALUADO",Double.toString(montoValuado));
				}
				infoRegresar	=
					"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";			
			}catch(Exception e){
					throw new AppException("Error al generar la consulta", e);	
			}	
		}else if(informacion.equals("ArchivoCSV")){
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}else if(informacion.equals("ArchivoTotalPDF")){
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}else if(informacion.equals("ResumenTotales")){
			session.setAttribute("_resumenTotales", "S");
			Registros reg	= queryHelper.doSearch();
			int numRegistrosMN =0, numRegistrosDL =0;
			double montoTotalMN =0, montoTotalDL =0;
			double montoValuadoMN =0, montoValuadoDL =0;
			double montoCreditoMN =0, montoCreditoDL =0;
			double monto32Nac = 0, monto32Dol = 0;
			String iva ="";
			double nuevoIva = 0.0;
			while(reg.next()){
			  double montoIni=0, porDesc=0,monPorDesc=0,montoDos=0,comApl=0,monCom=0;
			   if ("32".equals(estatusDocto)){
				  iva			= (reg.getString("IVA")==null)?"0":reg.getString("IVA");
				 nuevoIva		= (Double.parseDouble(iva)/100)+1;
			  }
			  String  ic_moneda = reg.getString("IC_MONEDA");
				if(ic_moneda.equals("1")) {
					numRegistrosMN++;
					montoTotalMN += Double.parseDouble((reg.getString("FN_MONTO").equals(""))?"0":reg.getString("FN_MONTO"));
					montoValuadoMN += Double.parseDouble((reg.getString("MONTO_VALUADO").equals(""))?"0":reg.getString("MONTO_VALUADO"));
					montoCreditoMN += Double.parseDouble((reg.getString("MONTO_CREDITO").equals(""))?"0":reg.getString("MONTO_CREDITO"));
          try {
            montoIni= Double.parseDouble((reg.getString("FN_MONTO").equals(""))?"0":reg.getString("FN_MONTO"));
            porDesc = Double.parseDouble((reg.getString("FN_PORC_DESCUENTO").equals(""))?"0":reg.getString("FN_PORC_DESCUENTO"));
            monPorDesc= (montoIni * porDesc)/100;
            montoDos  = montoIni - monPorDesc;
            comApl    = Double.parseDouble((reg.getString("COMISION_APLICABLE").equals(""))?"0":reg.getString("COMISION_APLICABLE"));
            monCom    = (montoDos*comApl)/100;
				if ("32".equals(estatusDocto)){
					monCom	 = monCom * nuevoIva;
				}
            monto32Nac += montoDos - monCom;
          } catch (Exception e){ }
				}
				if(ic_moneda.equals("54")) {
					numRegistrosDL++;						
					montoTotalDL += Double.parseDouble((reg.getString("FN_MONTO").equals(""))?"0":reg.getString("FN_MONTO"));
					montoValuadoDL += Double.parseDouble((reg.getString("MONTO_VALUADO").equals(""))?"0":reg.getString("MONTO_VALUADO"));
					montoCreditoDL += Double.parseDouble((reg.getString("MONTO_CREDITO").equals(""))?"0":reg.getString("MONTO_CREDITO"));
          try {
            montoIni= Double.parseDouble((reg.getString("FN_MONTO").equals(""))?"0":reg.getString("FN_MONTO"));
            porDesc = Double.parseDouble((reg.getString("FN_PORC_DESCUENTO").equals(""))?"0":reg.getString("FN_PORC_DESCUENTO"));
            monPorDesc= (montoIni * porDesc)/100;
            montoDos  = montoIni - monPorDesc;
            comApl    = Double.parseDouble((reg.getString("COMISION_APLICABLE").equals(""))?"0":reg.getString("COMISION_APLICABLE"));
            monCom    = (montoDos*comApl)/100;
				if ("32".equals(estatusDocto)){
					monCom	 = monCom * nuevoIva;
				}
            monto32Dol += montoDos - monCom;
          } catch (Exception e){ }
				}					
			}	
			HashMap  registrosTot = new HashMap();
			JSONArray registrosTotales = new JSONArray();
			for(int x =0; x<2; x++) {		
			registrosTot = new HashMap();				
				if(x==0 ){ 			
					registrosTot.put("NOMBRE_MONEDA", "Nacional");		
					registrosTot.put("NUM_REGISTROS",  String.valueOf(numRegistrosMN) );	
					registrosTot.put("TOTAL_MONTO", Double.toString (montoTotalMN));
					registrosTot.put("TOTAL_MONTO_VALUADO", Double.toString (montoValuadoMN) );
					registrosTot.put("TOTAL_MONTO_CREDITO", Double.toString (montoCreditoMN) );			
					registrosTot.put("TOTAL_MONTO_DEP", Double.toString (monto32Nac) );	
				}		
				if(x==1){ 						
					registrosTot.put("NOMBRE_MONEDA", "Dólares");		
					registrosTot.put("NUM_REGISTROS",  String.valueOf(numRegistrosDL) );	
					registrosTot.put("TOTAL_MONTO", Double.toString (montoTotalDL));
					registrosTot.put("TOTAL_MONTO_VALUADO", Double.toString (montoValuadoDL) );
					registrosTot.put("TOTAL_MONTO_CREDITO", Double.toString (montoCreditoDL) );							
					registrosTot.put("TOTAL_MONTO_DEP", Double.toString (monto32Dol) );					
				}		
				registrosTotales.add(registrosTot);
			}
		
			infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+"}";
				
		}
	}
	else if(informacion.equals("Detalle")){
		try{
			String numDocto  = (request.getParameter("numDocto")!=null)?request.getParameter("numDocto"):"";
			DetalleCambiosDoctos cambiosDoctos = new DetalleCambiosDoctos();
			cambiosDoctos.setIcDocumento(numDocto);
			Registros registros = cambiosDoctos.executeQuery();
				infoRegresar	=
					"{\"success\": true, \"total\": \""	+	registros.getNumeroRegistros() + "\", \"registros\": " + registros.getJSONData()+ "}";	
		}catch(Exception e){
			throw new AppException("Error al intentar generar la información de detalle.",e);
		}
	}
%>
<%=infoRegresar%>
