var dt = new Date();
Ext.onReady(function() {

	var jsonValoresIniciales = null;
	var folio = null;
	var tipo_credito = null;
	function procesaValoresIniciales(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null && jsonValoresIniciales.condicion == 'C'){
				fp.el.mask('La EPO no opera este tipo de credito ');
			}else{
				fp.el.unmask();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPDFHisto =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFHisto');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFHisto');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var muestraGridHistorico = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		folio = registro.get('FOLIO');
		cgTipoCredito = (registro.get('TIPOCREDITO')).substring(0,1);
		historicoData.loadData('');
		var ventana = Ext.getCmp('winHistorico');
		if (ventana) {
			ventana.hide();
		}else{
			new Ext.Window({
				modal: true,
				//resizable: false,
				y: 20,
				x:50,
				layout: 'fit',
				width: 900,
				height: 'auto',
				//height: 350,
				id: 'winHistorico',
				closeAction: 'hide',
				items: [gridHistorico],
				title: 'Historico de L�neas',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',{xtype: 'button',	text: 'Generar PDF',	id: 'btnGenerarPDFHisto'}
										   ,{xtype: 'button',	text: 'Bajar PDF',	id: 'btnBajarPDFHisto',hidden: true},
										'-',{xtype: 'button',	text: 'Regresar',	id: 'btnRegresar', handler: function() {Ext.getCmp('winHistorico').hide();	}}]
				}
			});
		}
		Ext.Ajax.request({url: '24linea01ext.data.jsp',params: Ext.apply({informacion: "obtenHistorico",folio:	folio, tipo_credito:cgTipoCredito}),callback: procesarMuestraHistorico});
	}

	function procesarMuestraHistorico(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('winHistorico').show();
			var el = Ext.getCmp('gridHistorico').getGridEl();
			Ext.getCmp('btnBajarPDFHisto').hide()
			if (infoR.registros != undefined && infoR.registros.length > 0){
				historicoData.loadData(infoR.registros);
				var btnPdf = Ext.getCmp('btnGenerarPDFHisto');
				btnPdf.enable();
				btnPdf.setHandler(
					function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24linea01ext.data.jsp',
							params: Ext.apply({
								informacion: 'ArchivoPDFHisto',
								folio: folio, tipo_credito: cgTipoCredito
							}),
							callback: procesarGenerarPDFHisto
						});
					}
				);
				el.unmask();
			}else{
				Ext.getCmp('btnGenerarPDFHisto').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnTotales = Ext.getCmp('btnTotales');
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				btnTotales.enable();
				btnGenerarPDF.enable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				btnGenerarArchivo.enable();
				el.unmask();
			} else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var catalogoIFData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIFDistribuidor'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var historicoData = new Ext.data.JsonStore({
		fields: [
			{name: 'FOLIO'},
			{name: 'TIPOCREDITO'},
			{name: 'MONEDA'},
			{name: 'FECHA_AUTO_HIS'},
			{name: 'FECHA_VENC_HIS'},
			{name: 'MONTO_AUTO_HIS'},
			{name: 'FECHA_AUTO_ACT'},
			{name: 'FECHA_VENC_ACT'},
			{name: 'MONTO_AUTO_ACT'},
			{name: 'TIPO_SOL'}
		],
		data:[{'FOLIO':'','TIPOCREDITO':'','MONEDA':'','FECHA_AUTO_HIS':'','FECHA_VENC_HIS':'','MONTO_AUTO_HIS':'','FECHA_AUTO_ACT':'','FECHA_VENC_ACT':'','MONTO_AUTO_ACT':'','TIPO_SOL':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24linea01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOM_IF'},
			{name: 'TIPOCREDITO'},
			{name: 'FOLIO'},
			{name: 'FN_MONTO_AUTORIZADO_TOTAL',		type: 'float'},
			{name: 'FN_SALDO_TOTAL',		type: 'float'},
			{name: 'MONEDA'},
			{name: 'FECHA_AUTO',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'},
			{name: 'ESTATUS'},
			{name: 'FECHA_CAMBIO',		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var totalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO_AUTORIZADO'},
			{name: 'TOTAL_SALDO_AUTORIZADO'}
		],
		data:[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_AUTORIZADO':'','TOTAL_SALDO_AUTORIZADO':''},
				{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_AUTORIZADO':'','TOTAL_SALDO_AUTORIZADO':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridTotales = {
		xtype: 'grid',
		store: totalesData,
		id: 'gridTotales',
		hidden:	true,
		view: new Ext.grid.GridView({markDirty: false}),
		columns: [
			{
				header: 'TOTALES',
				dataIndex: 'NOMMONEDA',
				align: 'left',	width: 250
			},{
				header: 'Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},{
				header: 'Monto Autorizado',
				dataIndex: 'TOTAL_MONTO_AUTORIZADO',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Saldo Autorizado',
				dataIndex: 'TOTAL_SALDO_AUTORIZADO',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header: 'Nombre IF', tooltip: 'Nombre IF',
				dataIndex: 'NOM_IF',
				sortable: true,	resizable: true,	width: 250,	hidden: false
			},{
				header: 'Folio', tooltip: 'Folio',
				dataIndex: 'FOLIO',
				sortable: true,	width: 120,	resizable: true, hideable : true, hidden: false, align:'center'
			},{
				header : 'Monto Autorizado', tooltip: 'Monto Autorizado',
				dataIndex : 'FN_MONTO_AUTORIZADO_TOTAL',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header : 'Saldo Disponible', tooltip: 'Saldo Disponible',
				dataIndex : 'FN_SALDO_TOTAL',
				sortable : true,	width : 120,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header : 'Moneda', tooltip: 'Moneda',	
				dataIndex : 'MONEDA',
				sortable : true,	width : 130,	hidden: false
			},{
				header : 'Fecha de Autorizaci�n', tooltip: 'Fecha de Autorizaci�n',
				dataIndex : 'FECHA_AUTO', align:'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha vencimiento', tooltip: 'Fecha vencimiento',
				dataIndex : 'FECHA_VENC', align:'center',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Estatus', tooltip: 'Estatus',
				dataIndex : 'ESTATUS', align:'center',
				sortable : true,	width : 130,	hidden: false
			},{
				header : 'Fecha Cambio de Estatus', tooltip: 'Fecha Cambio de Estatus',
				dataIndex : 'FECHA_CAMBIO',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				xtype:	'actioncolumn',
				header : 'Historico de L�neas', tooltip: 'Historico de L�neas',
				dataIndex : '',
				width:	100,	align: 'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('FOLIO')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraGridHistorico
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		//title: '',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						var storeGrid = consultaData;
						var sumMontoMN=0;
						var sumSaldoMN=0;
						var sumMontoDL=0;
						var sumSaldoDL=0;
						var countMN = 0;
						var countDL = 0;
						if(storeGrid.getTotalCount() > 0) {
							storeGrid.each(function(registro){
								if (registro.get('IC_MONEDA') == "1"){
									countMN++;
									sumMontoMN += registro.get('FN_MONTO_AUTORIZADO_TOTAL');
									sumSaldoMN += registro.get('FN_SALDO_TOTAL');
								}else if(registro.get('IC_MONEDA') == "54"){
									countDL++;
									sumMontoDL += registro.get('FN_MONTO_AUTORIZADO_TOTAL');
									sumSaldoDL += registro.get('FN_SALDO_TOTAL');
								}
							});
							if (countMN > 0){
								var regMN = totalesData.getAt(0);
								regMN.set('NOMMONEDA','Moneda Nacional');
								regMN.set('TOTAL_REGISTROS',countMN);
								regMN.set('TOTAL_MONTO_AUTORIZADO',sumMontoMN);
								regMN.set('TOTAL_SALDO_AUTORIZADO',sumSaldoMN);

								var regDL = totalesData.getAt(1);
								regDL.set('NOMMONEDA','Dolares Americanos');
								regDL.set('TOTAL_REGISTROS',countDL);
								regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
								regDL.set('TOTAL_SALDO_AUTORIZADO',sumSaldoDL);
							}else if(countDL > 0){
								var regDL = totalesData.getAt(0);
								regDL.set('NOMMONEDA','Dolares Americanos');
								regDL.set('TOTAL_REGISTROS',countDL);
								regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
								regDL.set('TOTAL_SALDO_AUTORIZADO',sumSaldoDL);
							}
						}
						var totalesCmp = Ext.getCmp('gridTotales');
						if (!totalesCmp.isVisible()) {
							totalesCmp.show();
							totalesCmp.el.dom.scrollIntoView();
						}
					}
				},'-',{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24linea01ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF'
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},'-',{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24linea01ext.data.jsp',
							params:	Ext.apply(fp.getForm().getValues(),{
										informacion: 'ArchivoCSV'}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},'-'
			]
		}
	});

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'HISTORICO', colspan: 6, align: 'center'},
				{header: 'ACTUAL', colspan: 4, align: 'center'}
			]
		]
	});

	var gridHistorico = {
		xtype: 'grid',
		store: historicoData,
		id: 'gridHistorico',
		plugins: grupos,
		columns: [
			{
				header: 'Folio', tooltip: 'Folio',
				dataIndex: 'FOLIO', menuDisabled: false,
				sortable: true,	width: 100,	resizable: true, align:'center'
			},{
				header : 'Tipo de Credito', tooltip: 'Tipo de Credito',
				dataIndex : 'TIPOCREDITO',	menuDisabled: false,
				sortable : true,	width : 150,	align: 'center', hidden: false
			},{
				header : 'Moneda', tooltip: 'Moneda',
				dataIndex : 'MONEDA',	menuDisabled: false,
				sortable : true,	width : 130,	hidden: false
			},{
				header : 'Fecha de Autorizaci�n', tooltip: 'Fecha de Autorizaci�n',
				dataIndex : 'FECHA_AUTO_HIS', align:'center',	menuDisabled: false,
				sortable : true,	width : 100, hidden: false
			},{
				header : 'Fecha vencimiento', tooltip: 'Fecha vencimiento',
				dataIndex : 'FECHA_VENC_HIS', align:'center',	menuDisabled: false,
				sortable : true,	width : 100, hidden: false
			},{
				header : 'Monto Autorizado', tooltip: 'Monto Autorizado',
				dataIndex : 'MONTO_AUTO_HIS',	menuDisabled: false,
				sortable : true,	width : 105,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header : 'Fecha de Autorizaci�n', tooltip: 'Fecha de Autorizaci�n',
				dataIndex : 'FECHA_AUTO_ACT', align:'center',	menuDisabled: false,
				sortable : true,	width : 100, hidden: false
			},{
				header : 'Fecha vencimiento', tooltip: 'Fecha vencimiento',
				dataIndex : 'FECHA_VENC_ACT', align:'center',	menuDisabled: false,
				sortable : true,	width : 100, hidden: false
			},{
				header : 'Monto Autorizado', tooltip: 'Monto Autorizado',
				dataIndex : 'MONTO_AUTO_ACT',	menuDisabled: false,
				sortable : true,	width : 105,	align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header : 'Tipo de Solicitud', tooltip: 'Tipo de Solicitud',
				dataIndex : 'TIPO_SOL', align:'center',	menuDisabled: false,
				sortable : true,	width : 100,	hidden: false
			}
		],
		height: 300,
		title: '',
		frame: false,
		loadMask: true
	};
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'cmbIFDis',
			allowBlank: false,
			fieldLabel: 'Intermediario Financiero',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_if',
			emptyText: 'Seleccione el Intermediario Financiero',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoIFData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'datefield',
			name: 'fecha',
			id: 'dtFecha',
			anchor:'46%',
			fieldLabel: 'Estados de solicitudes de cr�dito al',
			value: dt.format('d/m/Y'),
			allowBlank: true,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: ' margin:0 auto;',
		title:	'Criterios de b�squeda',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					folio = "";
					cgTipoCredito = "";
					grid.hide();
					Ext.getCmp('gridTotales').hide();
					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({	params: Ext.apply(fp.getForm().getValues(),{operacion: 'Generar'})	});
				} //fin handler
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24linea01ext.jsp';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid,	gridTotales,	NE.util.getEspaciador(10)
		]
	});

	catalogoIFData.load();
	fp.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24linea01ext.data.jsp',
		params: {informacion: "valoresIniciales"},
		callback: procesaValoresIniciales
	});

});