<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,		
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>		
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus") != null) ? request.getParameter("ic_cambio_estatus") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String infoRegresar	=	"", tituloGrid = "";
String tiposCredito  =	"";
AccesoDB 	con = new AccesoDB();
if(ic_cambio_estatus.equals("23")){
	tituloGrid = "En Proceso de Autorizacion IF a Negociable";
}else if(ic_cambio_estatus.equals("34")){
	tituloGrid = "En Proceso de Autorizacion IF a Seleccionado Pyme";
}
 
try {
	con.conexionDB();
	
	String qrySentencia =	" SELECT NVL(PE.CG_TIPOS_CREDITO,PN.CG_TIPOS_CREDITO) as TIPOS_CREDITO "+
						" FROM COMREL_PRODUCTO_EPO PE,COMCAT_PRODUCTO_NAFIN PN"+
						" WHERE PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
						" AND PN.IC_PRODUCTO_NAFIN = 4"+
						" AND PE.IC_EPO = "+iNoCliente;
		ResultSet	rs = con.queryDB(qrySentencia);
		if(rs.next()){
			tiposCredito = rs.getString("TIPOS_CREDITO");
		}
		rs.close();
		con.cierraStatement();
		
	
	if (informacion.equals("catalogoEstatus") ) {
			 
			CatalogoSimple catalogo = new CatalogoSimple();
			catalogo.setCampoClave("ic_cambio_estatus");
			catalogo.setCampoDescripcion("cd_descripcion");
			catalogo.setTabla("comcat_cambio_estatus");
			catalogo.setValoresCondicionIn("23,34,4,21,2,24",java.lang.Integer.class);	
			catalogo.setOrden("cd_descripcion");			
			infoRegresar =  catalogo.getJSONElementos();
	
	} else 	if (informacion.equals("Consulta") ) {		 //Negociable a Baja
			
		CambioEstatusEpo paginador = new CambioEstatusEpo();
		paginador.setTiposCredito(tiposCredito);
		paginador.setNoEpo(iNoCliente);
		paginador.setNoEstatus(ic_cambio_estatus);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
		
		if (operacion.equals("Generar") ){
			try {
				Registros reg	=	queryHelper.doSearch();
				if(ic_cambio_estatus.equals("23") ||ic_cambio_estatus.equals("34")){
					infoRegresar	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\",\"tituloGrid\": \"" + tituloGrid + "\", \"registros\": " + reg.getJSONData()+ "}";

				}else{
					infoRegresar	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";

				}
			}	catch(Exception e) {
				throw new AppException("Error en la paginación", e);
			}
		}	
		
		if (operacion.equals("ArchivoPDF")){
		
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");			
				
				
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
				
				}catch(Throwable e){
					throw new AppException("Error al generar el archivo PDF ",e);
				}
				
		}	else if (operacion.equals("ArchivoCSV")){
		
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
				
				}catch(Throwable e){
					throw new AppException("Error al generar el archivo CSV",e);
				}  
		}
		
	}else	if (informacion.equals("ResumenTotales")) {
				
		CambioEstatusEpo paginador = new CambioEstatusEpo();
		paginador.setTiposCredito(tiposCredito);
		paginador.setNoEpo(iNoCliente);
		paginador.setNoEstatus(ic_cambio_estatus);
		
	 String resultado = "";
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
		
		infoRegresar = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "Totales");	
		
	}
	
	

}catch(Exception e){
	out.println("Error "+e);
}finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
}
	
	
%>
<%=infoRegresar%>

