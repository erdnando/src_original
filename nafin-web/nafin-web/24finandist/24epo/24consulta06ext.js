Ext.onReady(function() {
	
	function procesaValoresBusqueda(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fp.el.unmask();
			jsonValores = Ext.util.JSON.decode(response.responseText);
			if (jsonValores != null){
				//consultaData_A.load({params: Ext.apply(fp.getForm().getValues(),{operacion: 'Generar',start: 0,limit: 15})});
				consultaData_A.load({params: Ext.apply({operacion: 'Generar',start: 0,limit: 15})});
				fp.hide();
				var cm = gridInfo.getColumnModel();
				
				if (jsonValores.cuenta != undefined){
					var totCols = jsonValores.cuenta.length;
					var tam;
					if (totCols == 1){
						tam = 420;
					}else{
						tam = 210;
					}
					var numCol=1;
					Ext.each(jsonValores.cuenta, function(item, index){
							var curColConfig = cm.config;
							curColConfig.push({
								 //header: '',
								enableColumnMove:false,	align:'center',	width:tam,	menuDisabled:true,	dataIndex: ""+numCol,
								renderer:function(value, metadata, record, rowindex, colindex, store) {
												metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
												return value;
											}
							});
							cm.setConfig(curColConfig);
							numCol++;
					});
					
					numCol=1;
					Ext.each(jsonValores.cuenta, function(item, index){
						var reg = infoData.getAt(0);
						reg.set(""+numCol,item);
						numCol++;
					});
					numCol=1;
					Ext.each(jsonValores.banco_ref, function(item, index){
						var reg = infoData.getAt(1);
						reg.set(""+numCol,item);
						numCol++;
					});
					numCol=1;
					Ext.each(jsonValores.monto_auto, function(item, index){
						var reg = infoData.getAt(2);
						reg.set(""+numCol,Ext.util.Format.number(item,'$ 0,000.00'));
						numCol++;
					});
					numCol=1;
					Ext.each(jsonValores.fecha_venc, function(item, index){
						var reg = infoData.getAt(3);
						reg.set(""+numCol,item);
						numCol++;
					});
					numCol=1;
					Ext.each(jsonValores.moneda, function(item, index){
						var reg = infoData.getAt(4);
						reg.set(""+numCol,item);
						numCol++;
					});
					numCol=1;
					Ext.each(jsonValores.dispuesto, function(item, index){
						var reg = infoData.getAt(5);
						reg.set(""+numCol,Ext.util.Format.number(item,'$ 0,000.00'));
						numCol++;
					});
					numCol=1;
					Ext.each(jsonValores.disponible, function(item, index){
						var reg = infoData.getAt(6);
						reg.set(""+numCol,Ext.util.Format.number(item,'$ 0,000.00'));
						numCol++;
					});
					numCol=1;
					Ext.each(jsonValores.estatus, function(item, index){
						var reg = infoData.getAt(7);
						reg.set(""+numCol,item);
						numCol++;
					});
					if (!gridInfo.isVisible())	{
						gridInfo.show();
					}
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData_A = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			if (Ext.getCmp('fecha_ini').getValue() != null && Ext.getCmp('fecha_ini').getValue() != "" && Ext.getCmp('fecha_fin').getValue() != null && Ext.getCmp('fecha_fin').getValue() != ""){
				fecha_ini = Ext.getCmp('fecha_ini').getValue().format('d/m/Y');
				fecha_fin = Ext.getCmp('fecha_fin').getValue().format('d/m/Y');
				grid_A.setTitle('<div><div style="float:left">Resumen estado de adeudos</div><div style="float:right">Movimientos del ' + fecha_ini + ' al ' + fecha_fin +'</div></div>');
			}
			if (!grid_A.isVisible()) {
				grid_A.show();
			}
			var btnGenerarArchivo = Ext.getCmp('btnGenArchivo');
			var btnGenerarPDF = Ext.getCmp('btnGenPDF');
			var btnTotales = Ext.getCmp('btnTotales');
			var el = grid_A.getGridEl();

			if(store.getTotalCount() > 0) {
				btnTotales.enable();
				btnGenerarArchivo.enable();
				btnGenerarPDF.enable();

				el.unmask();
			}else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
/*
	var procesarSuccessFailureGenArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenPDF =  function(opts, success, response) {
		var btnGeneraPDF = Ext.getCmp('btnGenPDF');
		btnGeneraPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajaPDF = Ext.getCmp('btnBajPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

*/
	var procesarSuccessFailureGenArchivo = function (opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenArchivo').enable();
		Ext.getCmp('btnGenArchivo').setIconClass('icoXls');
	}

	var procesarSuccessFailureGenPDF = function (opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenPDF').enable();
		Ext.getCmp('btnGenPDF').setIconClass('icoPdf');
	}

	var catalogoIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta06ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIfDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoClienteData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta06ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCliente'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta06ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMonedaDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var totalData = new Ext.data.JsonStore({
		fields: [
			{name: 'MONTO_TASA_INT',type: 'float'}
		],
		data: [
			{MONTO_TASA_INT:'0'}
		],
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var enviaParametros =  function(store, options){
		var valCli = Ext.getCmp('forma_A').getForm().findField("ic_cliente").getValue();
		var valIF = Ext.getCmp('forma_A').getForm().findField("ic_if").getValue();
		var valMoneda = Ext.getCmp('forma_A').getForm().findField("ic_moneda").getValue();
		var valFec_ini = Ext.getCmp('forma_A').getForm().findField("fecha_inicio").getValue();
		var valFec_fin = Ext.getCmp('forma_A').getForm().findField("fecha_final").getValue();
		Ext.apply(options.params, {ic_cliente: valCli,ic_if:valIF,ic_moneda:valMoneda,fecha_inicio:Ext.util.Format.date(valFec_ini,'d/m/Y'),fecha_final:Ext.util.Format.date(valFec_fin,'d/m/Y')});
	}

	var consultaData_A = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta06ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'FECHA_OPERA',	type: 'date', dateFormat: 'd/m/Y'},	
			{name: 'NUM_CLIENTE'},
			{name: 'CLIENTE'},
			{name: 'NUM_DOCTO'},
			{name: 'NUM_CREDITO'},
			{name: 'F_VENC_CREDITO',	type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'MONTO_CRED',			type: 'float'},
			{name: 'ESTATUS_ALT'},
			{name: 'INT_GENERADOS',		type: 'float'},
			{name: 'PAGO_A_CAPITAL',	type: 'float'},
			{name: 'PAGO_INT',			type: 'float'},
			{name: 'SALDO_CRE',			type: 'float'},
			{name: 'IC_TIPO_COBRO_INTERES'},
			{name: 'TIPO_COB_INT'},
			{name: 'IC_ESTATUS_DOCTO'},
			{name: 'MONTO_DOC_FINAL',	type: 'float'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad: enviaParametros,
			load: procesarConsultaData_A,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData_A(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var resumenTotalesAData = new Ext.data.JsonStore({
		fields: [
			{name: 'TIPO_MONEDA'},
			{name: 'TOTAL_REGISTROS', type: 'float'},
			{name: 'TOTAL_MONTO_DOC_INICIAL', type: 'float'},
			{name: 'TOTAL_MONTO_DOC_FINAL', type: 'float'},
			{name: 'TOTAL_CAPITAL_PAGADO', type: 'float'},
			{name: 'TOTAL_INTERESES_PAGADOS', type: 'float'},
			{name: 'TOTAL_PAGADO', type: 'float'},
			{name: 'TOTAL_SALDO_DEL_CREDITO', type: 'float'}
		],
		data:	[
			{'TIPO_MONEDA':'','TOTAL_REGISTROS':0,'TOTAL_MONTO_DOC_INICIAL':0,'TOTAL_MONTO_DOC_FINAL':0,
			'TOTAL_CAPITAL_PAGADO':0,'TOTAL_INTERESES_PAGADOS':0,'TOTAL_PAGADO':0,'TOTAL_SALDO_DEL_CREDITO':0}
		],
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var infoData = new Ext.data.JsonStore({
		fields: [{name: 'INICIAL'}],
		data: [{'INICIAL':'No. Cuenta:'},{'INICIAL':'Banco de referencia:'},{'INICIAL':'Monto de la l�nea autorizada:'},{'INICIAL':'Fecha de Vencimiento:'},
				 {'INICIAL':'Moneda:'},{'INICIAL':'Monto dispuesto:'},{'INICIAL':'Saldo disponible:'},{'INICIAL':'Estatus:'}],	
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridTotalesA = {
		xtype: 'grid',
		store: resumenTotalesAData,
		id: 'gridTotalesA',
		hidden:	true,
		columns: [
			{
				header: 'TOTALES',
				dataIndex: 'TIPO_MONEDA',
				align: 'left',	width: 150
			},{
				header: 'Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 80,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},{
				header: 'Monto Documento Inicial',
				dataIndex: 'TOTAL_MONTO_DOC_INICIAL',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto Documento Final',
				dataIndex: 'TOTAL_MONTO_DOC_FINAL',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Capital Pagado',
				dataIndex: 'TOTAL_CAPITAL_PAGADO',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Intereses Pagados',
				dataIndex: 'TOTAL_INTERESES_PAGADOS',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Total Pagado',
				dataIndex: 'TOTAL_PAGADO',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Saldo del Cr�dito',
				dataIndex: 'TOTAL_SALDO_DEL_CREDITO',
				width: 130,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	var gridInfo = new Ext.grid.GridPanel({
		store:infoData,	hidden:false,	stripeRows:true,	loadMask:true,	height:250,	width:600,	title: '<div align="center">Res�men de l�nea de cr�dito</div>',
		view: new Ext.grid.GridView({markDirty:false}),	style:'margin:0 auto;', frame:true, columnLines:true,	enableColumnMove:false, hidden:true,	//forceFit:true, 
		columns: [
			{
				header : '&nbsp;',	dataIndex:'INICIAL',width:160, hideable:false, menuDisabled:true, fixed:true
			}
		]
	});

	var grid_A = new Ext.grid.GridPanel({
		store: consultaData_A,
		hidden: true,
		columns: [
			{
				header : 'Fecha de operaci�n', tooltip: 'Fecha de operaci�n', dataIndex : 'FECHA_OPERA',
				sortable : true,	width : 110, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Num. Cliente', tooltip: 'No. Cliente', dataIndex: 'NUM_CLIENTE',
				sortable: true,	width: 120,	resizable: true, hidden: false
			},{
				header: 'Nombre', tooltip: 'Nombre', dataIndex: 'CLIENTE',
				sortable: true,	width: 200,	resizable: true, hidden: false
			},{
				header: 'Num. de docto. inicial', tooltip: 'Num. de docto. inicial', dataIndex: 'NUM_DOCTO',
				sortable: true,	width: 150,	resizable: true, hidden: false
			},{
				header : 'Monto docto. inicial', tooltip: 'Monto docto. inicial', dataIndex : 'MONTO_CRED',
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Num. de docto. final', tooltip: 'Num. de docto. final',	dataIndex : 'NUM_CREDITO',
				sortable : true, width : 150, hidden : false
			},{
				header : 'Monto docto. final', tooltip: 'Monto docto. final', dataIndex : 'MONTO_DOC_FINAL',
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Fecha de vencimiento', tooltip: 'Fecha de vencimiento',
				dataIndex : 'F_VENC_CREDITO',
				sortable : true,	width : 100, hidden: false,	renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Estatus', tooltip: 'Estatus', dataIndex : 'ESTATUS_ALT',
				sortable : true, width : 100, hidden : false, align: 'center'
			},{
				header : 'Intereses generados', tooltip: 'Intereses generados', dataIndex : 'INT_GENERADOS',
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Pago capital', tooltip: 'Pago capital', dataIndex : 'PAGO_A_CAPITAL', 
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Pago intereses', tooltip: 'Pago intereses', dataIndex : 'PAGO_INT', 
				sortable : true, width : 120, hidden : false, align: 'right',
				renderer: function (value, metaData, registro, rowIndex, colIndex, store)	{
								value = "0";
								if(registro.get('IC_TIPO_COBRO_INTERES') == "1")	{
									value = registro.get('INT_GENERADOS');
								}else	{
									if(registro.get('IC_TIPO_COBRO_INTERES') == "2")	{
										if (registro.get('IC_ESTATUS_DOCTO') == "11"){
											value = registro.get('INT_GENERADOS');
										}
									}
								}
								return Ext.util.Format.number(value,'$ 0,0.00');
							}
			},{
				header : 'Total Pagado', tooltip: 'Total Pagado', dataIndex : '', 
				sortable : true, width : 120, hidden : false, align: 'right',
				renderer: function (value, metaData, registro, rowIndex, colIndex, store)	{
								var rs_pago_int = registro.get('PAGO_INT');
								if(registro.get('IC_TIPO_COBRO_INTERES') == "1")	{
									rs_pago_int = registro.get('INT_GENERADOS');
								}else	{
									if(registro.get('IC_TIPO_COBRO_INTERES') == "2")	{
										if (registro.get('IC_ESTATUS_DOCTO') == "11"){
											rs_pago_int = registro.get('INT_GENERADOS');
										}
									}
								}
								value = parseFloat(registro.get('PAGO_A_CAPITAL')) + parseFloat(rs_pago_int);
								return Ext.util.Format.number(value,'$ 0,0.00');
							}
			},{
				header : 'Saldo financiamiento', tooltip: 'Saldo financiamiento', dataIndex : 'SALDO_CRE',
				sortable : true, width : 120, renderer: Ext.util.Format.numberRenderer('$ 0,0.00'), 
				hidden : false, align: 'right'
			},{
				header : 'Tipo de cobro a intereses', tooltip: 'Tipo de cobro a intereses',
				dataIndex : 'TIPO_COB_INT', sortable : true, width : 150, hidden : false
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Res�men estado de adeudos',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData_A,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						var comboMoneda = Ext.getCmp('cmbMoneda');
						var storeGrid = consultaData_A;
						var sumMontoCre=0;
						var sumMontoFin=0;
						var sumPagoCapital=0;
						var sumPagoInt=0;
						var sumTotalPagado=0;
						var sumSaldoCre=0;
						var count = 0;
						pago_int = 0;
						if(storeGrid.getTotalCount() > 0) {
							var pago_int = 0;
							var total_pagado = 0;
							storeGrid.each(function(registro){
								count++;
								total_pagado = 0;
								pago_int = registro.get("PAGO_INT");
								if(registro.get('IC_TIPO_COBRO_INTERES') == "1")	{
									pago_int = parseFloat(registro.get('INT_GENERADOS'));
								}else	{
									if(registro.get('IC_TIPO_COBRO_INTERES') == "2")	{
										if (registro.get('IC_ESTATUS_DOCTO') == "11"){
											pago_int = parseFloat(registro.get('INT_GENERADOS'));
										}
									}
								}
								total_pagado = parseFloat(registro.get('PAGO_A_CAPITAL')) + pago_int;
								sumPagoInt += pago_int;
								sumTotalPagado += total_pagado;
								sumMontoCre += registro.get('MONTO_CRED');
								sumMontoFin += registro.get('MONTO_DOC_FINAL');
								sumPagoCapital += registro.get('PAGO_A_CAPITAL');
								sumSaldoCre += registro.get('SALDO_CRE');
							});
							var reg = resumenTotalesAData.getAt(0);//Primer registro de conteo
							reg.set('TOTAL_REGISTROS',count);
							reg.set('TOTAL_MONTO_DOC_INICIAL',sumMontoCre);
							reg.set('TOTAL_MONTO_DOC_FINAL',sumMontoFin);
							reg.set('TOTAL_CAPITAL_PAGADO',sumPagoCapital);
							reg.set('TOTAL_INTERESES_PAGADOS',sumPagoInt);
							reg.set('TOTAL_PAGADO',sumTotalPagado);
							reg.set('TOTAL_SALDO_DEL_CREDITO',sumSaldoCre);
							if (comboMoneda.getValue() == "1"){
								reg.set('TIPO_MONEDA','MONEDA NACIONAL');
							}else{
								if (comboMoneda.getValue() == "54"){
									reg.set('TIPO_MONEDA','DOLAR AMERICANO');
								}
							}
						}
						var totalesACmp = Ext.getCmp('gridTotalesA');
						if (!totalesACmp.isVisible()) {
							totalesACmp.show();
							totalesACmp.el.dom.scrollIntoView();
						}
					}
				},'-',{
					xtype:   'button',
					text:    'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id:      'btnGenArchivo',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta06ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								tipo: 'CSV'
							}),
							callback: procesarSuccessFailureGenArchivo
						});
					}
				},'-',{
					xtype:   'button',
					text:    'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id:      'btnGenPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta06ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								tipo: 'PDF'
							}),
							callback: procesarSuccessFailureGenPDF
						});
					}
				},'-',{
					text: 'Regresar',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '24consulta06ext.jsp';
					}
				}
			],
			listeners:{
				change: function(pag,data){
					var totalesACmp = Ext.getCmp('gridTotalesA');
					if (totalesACmp.isVisible()) {
						totalesACmp.hide();
					}
				}
			}
		}
	});

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_if',
			id:	'cmbIF',
			allowBlank: false,
			fieldLabel: 'Nombre IF',
			emptyText: 'Seleccione el Intermediario Financiero',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_if',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIfData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'combo',
			name: 'ic_cliente',
			id:	'cmbCliente',
			allowBlank: true,
			fieldLabel: 'Nombre cliente',
			emptyText: 'Todos los Clientes',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_cliente',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoClienteData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'combo',
			id:	'cmbMoneda',
			name: 'ic_moneda',
			allowBlank: false,
			fieldLabel: 'Moneda',
			emptyText: 'Seleccione Moneda',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_moneda',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha inicio',	
			combineErrors: false,
			msgTarget: 'side',
			anchor:'90%',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_inicio',
					id: 'fecha_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'Fecha final:',
					width: 70
				},
				{
					xtype: 'datefield',
					name: 'fecha_final',
					id: 'fecha_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},{
					xtype:'hidden',
					id:'hidCliente',
					name:'disCliente',
					value:''
				},{
					xtype:'hidden',
					id:'hidBanco',
					name:'disBanco', 
					value:''
				}
			]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma_A',
		width: 600,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		title:	'<div><center>Criterios de B�squeda</div>',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid_A.hide();
					gridInfo.hide();

					var totalesACmp = Ext.getCmp('gridTotalesA');
					if (totalesACmp.isVisible()) {
						totalesACmp.hide();
					}

					fp.el.mask('Enviando...', 'x-mask-loading');
					var combo_if = Ext.getCmp('cmbIF');
					var cmbMoneda = Ext.getCmp('cmbMoneda');
					Ext.Ajax.request({
						url: '24consulta06ext.data.jsp',
						params:	{ic_if:	combo_if.getValue(), ic_moneda:	cmbMoneda.getValue(),
									informacion: 'valoresBusqueda'},
						callback: procesaValoresBusqueda
					});
				} //fin handler
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24consulta06ext.jsp';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,		NE.util.getEspaciador(5),
			gridInfo,NE.util.getEspaciador(3),
			grid_A,	gridTotalesA,
			NE.util.getEspaciador(5)
		]
	});

//	Carga datos de los combos...
	catalogoIfData.load();
	catalogoClienteData.load();
	catalogoMonedaData.load();

});