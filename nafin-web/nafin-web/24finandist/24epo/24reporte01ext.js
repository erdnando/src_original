Ext.onReady(function(){
	Ext.QuickTips.init();
	var cveEstatus	= "";
	var montoValuadoUSD 	= 0;
	var montoValuadoMN	= 0;
	var tipoCambio = "";
	var icMoneda = "";
	var tipoConversion = "";
//----------------------------------HANDLERS------------------------------------

//---------------------------descargaArchivo------------------------------------
function descargaArchivo(opts, success, response) {
if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
    var infoR = Ext.util.JSON.decode(response.responseText);
    Ext.getCmp('btnCSV32').setIconClass('icoXls');			
    var archivo = infoR.urlArchivo;
    //archivo = archivo.replace('/nafin','');
    var params = {nombreArchivo: archivo};	
    fp.getForm().getEl().dom.action =  archivo;
    fp.getForm().getEl().dom.submit();
    Ext.getCmp('btnCSV32').enable();
    fp.el.unmask();
  }else {
    NE.util.mostrarConnError(response,opts);
  }
};
//---------------------------fin descargaArchivo--------------------------------

//---------------------------descargaArchivoPDF------------------------------------
function descargaArchivoPDF(opts, success, response) {
if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
    var infoR = Ext.util.JSON.decode(response.responseText);
    Ext.getCmp('btnPDF32').setIconClass('icoPdf');			

    Ext.getCmp('btnPDF32').enable();
    var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
    fp.el.unmask();
  }else {
    NE.util.mostrarConnError(response,opts);
  }
};
//---------------------------fin descargaArchivoPDF--------------------------------

	var procesarResumenTotalesData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridTotales = Ext.getCmp('gridTotales');
			var el = gridTotales.getGridEl();
			var cm = gridTotales.getColumnModel();
			var colMontoValuado = cm.findColumnIndex('TOTAL_MONTO_VALUADO');
			var colMontoCredito = cm.findColumnIndex('TOTAL_MONTO_CREDITO');
			cm.setHidden(colMontoCredito,false);
			cm.setHidden(colMontoValuado,false);
			if(store.getTotalCount() > 0) {
				if(cveEstatus == "D2" || cveEstatus == "D9" 
				|| cveEstatus == "D5" || cveEstatus == "D1"){
					cm.setHidden(colMontoCredito,true);
				}
				if(tipoConversion!=""){
					if(store.getTotalCount() == 2){
						var rec1 = store.getAt(1);
						rec1.data.TOTAL_MONTO_VALUADO = montoValuadoUSD;
						if(cveEstatus == "D11"){
							var rec0 = store.getAt(0);
							rec0.data.TOTAL_MONTO_VALUADO = montoValuadoMN;
						}
					}else{
						var rec = store.getAt(0);
						rec.data.TOTAL_MONTO_VALUADO = montoValuadoMN + montoValuadoUSD;
					}
				}else{
					cm.setHidden(colMontoValuado,true);
				}
				el.unmask();
			}
		}
	}
	var mostrarGrid = function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		var numDocto = registro.get('IC_DOCUMENTO');
		consultaDataDetalle.load({
			params:{
				numDocto: numDocto
			}
		});
		var ventanaDetalle = Ext.getCmp('ventanaDetalle');
		if(ventanaDetalle){
			ventanaDetalle.show();
		}else{
				new Ext.Window({
						layout: 'fit',
						modal: true,
						width: 800,
						height: 400,
						id: 'ventanaDetalle',
						title: 'Documento',
						closeAction: 'hide',
						items: [
							gridDetalle
						]
				}).show();
		}		
	}
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarTotalPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarArchivo2 =  function(opts, success, response) {
		var btnGenerarArchivo2 = Ext.getCmp('btnGenerarArchivo2');
		btnGenerarArchivo2.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo2 = Ext.getCmp('btnBajarArchivo2');
			btnBajarArchivo2.show();
			btnBajarArchivo2.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo2.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo2.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarTotalPDF2 =  function(opts, success, response) {
		var btnGenerarPDF2 = Ext.getCmp('btnGenerarPDF2');
		btnGenerarPDF2.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF2 = Ext.getCmp('btnBajarPDF2');
			btnBajarPDF2.show();
			btnBajarPDF2.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF2.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF2.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaDetalle = function (store,arrRegistros,opts){	
		if(arrRegistros != null){
				var grid = Ext.getCmp('gridDetalle');
				var el = grid.getGridEl(); 
				var store= consultaDataDetalle;
				if(store.getTotalCount()>0){
					el.unmask();
				}else{
					el.mask('No se encontr� ning�n cambio para el documento','x-mask');
				}
			}
	}
	var procesarConsultaGrid1 = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('forma');
		var grid1 = Ext.getCmp('grid1');
		fp.el.unmask();
		var el = grid1.getGridEl();
		if(arrRegistros != null){		
			if(!grid1.isVisible()){
				grid1.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnTotales = Ext.getCmp('btnTotales');
			var el = grid1.getGridEl();
			if(store.getTotalCount()>0){
				btnTotales.enable();
				btnGenerarPDF.enable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				if(!btnBajarArchivo.isVisible()){
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}
				//Calcular el valor del Monto Total Valuado USD
				store.each(function(registro){
					tipoCambio = registro.get('TIPO_CAMBIO');
					icMoneda = registro.get('IC_MONEDA');
					if(tipoCambio != 1 && icMoneda != 1){
						montoValuadoUSD += registro.get('MONTO_VALUADO');
					}
					tipoConversion = registro.get('TIPO_CONVERSION');
				});
				var cm = grid1.getColumnModel();
				
				
				
				if(tipoConversion!=""){
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'),false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'),false);
					cm.setHidden(cm.findColumnIndex('MONTO_VALUADO'),false);
				}
				if(cveEstatus != "D5"){
					cm.setHidden(cm.findColumnIndex('MAX_FECHA_CAMBIO'),true);
				}
				el.unmask();				
			}else{
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	var procesarConsultaGrid2 = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('forma');
		var grid2 = Ext.getCmp('grid2');
		fp.el.unmask();
		var el = grid2.getGridEl();
		if(arrRegistros != null){
			if(!grid2.isVisible()){
				grid2.show();
			}
			var btnBajarArchivo2 = Ext.getCmp('btnBajarArchivo2');
			var btnGenerarArchivo2 = Ext.getCmp('btnGenerarArchivo2');
			var btnBajarPDF2 = Ext.getCmp('btnBajarPDF2');
			var btnGenerarPDF2 = Ext.getCmp('btnGenerarPDF2');
			var btnTotales2 = Ext.getCmp('btnTotales2');
			if(store.getTotalCount()>0){
				btnTotales2.enable();
				btnGenerarPDF2.enable();
				btnBajarPDF2.hide();
				btnBajarArchivo2.hide();
				if(!btnBajarArchivo2.isVisible()){
					btnGenerarArchivo2.enable();
				} else {
					btnGenerarArchivo2.disable();
				}
			//Calcular el valor del Monto Total Valuado USD
				store.each(function(registro){
					tipoCambio = registro.get('TIPO_CAMBIO');
					icMoneda = registro.get('IC_MONEDA');
					if(cveEstatus!="D11"){
						if(tipoCambio != 1 && icMoneda != 1){
							montoValuadoUSD += registro.get('MONTO_VALUADO');
						}
					}else{
						if(icMoneda == 1){
							montoValuadoMN += registro.get('MONTO_VALUADO');
						}else if(tipoCambio==1){
							montoValuadoUSD += registro.get('MONTO_VALUADO');
						}
					}
					tipoConversion = registro.get('TIPO_CONVERSION');
				});
				var cm = grid2.getColumnModel();
				
				
							
				if(tipoConversion!=""){
					cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'),false);
					cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'),false);
					cm.setHidden(cm.findColumnIndex('MONTO_VALUADO'),false);
				}
				if(cveEstatus != "D20"){
					cm.setHidden(cm.findColumnIndex('MODO_PLAZO'),true);
				}else{
					cm.setHidden(cm.findColumnIndex('TIPO_CREDITO'),true);
				}
				el.unmask();
			}else{
				btnTotales2.disable();
				btnGenerarArchivo2.disable();
				btnGenerarPDF2.disable();
				btnBajarPDF2.hide();
				btnBajarArchivo2.hide();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
//*****************--------------------------------

	var procesarConsultaGrid3 = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('forma');
		var grid3 = Ext.getCmp('grid3');
		fp.el.unmask();
		var el = grid3.getGridEl();
		if(arrRegistros != null){		
			if(!grid3.isVisible()){
				grid3.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnTotales = Ext.getCmp('btnTotales');
			var el = grid3.getGridEl();
			if(store.getTotalCount()>0){
       Ext.getCmp('btnPDF32').show();
       Ext.getCmp('btnCSV32').show();
        Ext.getCmp('btnT32').show();
       
				btnTotales.enable();
				btnGenerarPDF.enable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				if(!btnBajarArchivo.isVisible()){
					btnGenerarArchivo.show();
				} else {
					btnGenerarArchivo.disable();
				}
        if(!btnTotales.isVisible()){
					btnTotales.show();
				} else {
					btnTotales.disable();
				}
        if(!btnGenerarPDF.isVisible()){
					btnGenerarArchivo.show();
				} else {
					btnGenerarPDF.disable();
				}
        
				el.unmask();				
			}else{
      Ext.getCmp('btnPDF32').hide();
       Ext.getCmp('btnCSV32').hide();
        Ext.getCmp('btnT32').hide();
        
       Ext.getCmp('btnTotales2').hide();
       Ext.getCmp('btnGenerarArchivo2').hide();
       Ext.getCmp('btnGenerarPDF2').hide();
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro','x-mask');
        
			}
		}
	}
  
//*****************--------------------------------

//-----------------------------------STORES--------------------------------------
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatusStore',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var consultaDataDetalle = new Ext.data.JsonStore({
		root: 'registros',
		url: '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'Detalle'
		},
		fields: [
			{name: 'FECH_CAMBIO'},
			{name: 'CD_DESCRIPCION'},
			{name: 'CT_CAMBIO_MOTIVO'},
			{name: 'FECH_EMI_ANT'},
			{name: 'FECH_EMI_NEW'},
			{name: 'FN_MONTO_ANTERIOR'},
			{name: 'FN_MONTO_NUEVO'},
			{name: 'FECH_VENC_ANT'},
			{name: 'FECH_VENC_NEW'},
			{name: 'MODO_PLAZO_ANTERIOR'},
			{name: 'MODO_PLAZO_NUEVO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDetalle,
			exception: {
				fn: function(proxy,type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					procesarConsultaDetalle(null,null,null);
				}
			}
		}
	});
	var consultaDataGrid1 = new Ext.data.JsonStore({
		root: 'registros',
		url: '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOMBRE_DIST'},
			{name: 'CC_ACUSE'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'DF_FECHA_EMISION'},
			{name: 'DF_FECHA_VENC'},
			{name: 'DF_CARGA'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'IC_MONEDA', type: 'int'},
			{name: 'FN_MONTO'},
			{name: 'CATEGORIA'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO', type: 'float'},
			{name: 'MODO_PLAZO'},
			{name: 'MAX_FECHA_CAMBIO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'MONTO_VALUADO'},
			{name: 'IC_DOCUMENTO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaGrid1,
			exception: {
				fn: function(proxy,type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					procesarConsultaGrid1(null,null,null);
				}
			}
		}
	});
	var consultaDataGrid2 = new Ext.data.JsonStore({
		root: 'registros',
		url: '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOMBRE_DIST'},
			{name: 'CC_ACUSE'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'DF_FECHA_EMISION'},
			{name: 'DF_FECHA_VENC'},
			{name: 'DF_CARGA'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'IC_MONEDA', type: 'int'},
			{name: 'FN_MONTO'},
			{name: 'CATEGORIA'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO', type: 'int'},
			{name: 'MODO_PLAZO'},
			{name: 'TIPO_CREDITO'},
			{name: 'IC_DOCUMENTO'},
			{name: 'MONTO_CREDITO', type: 'float'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'FECHA_VENC_CREDITO'},
			{name: 'FECHA_OPERAIF'},
			{name: 'NOMBRE_IF'},
			{name: 'REFERENCIA_INT'},
			{name: 'VALOR_TASA_INT'},
			{name: 'MONTO_TASA_INT', type: 'float'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'MONTO_VALUADO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaGrid2,
			exception: {
				fn: function(proxy,type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					procesarConsultaGrid2(null,null,null);
				}
			}
		}
	});
  
  ///******
	var consultaDataGrid3 = new Ext.data.JsonStore({
		root: 'registros',
		url: '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
		{name: 'NOMBRE_DIST'},
			{name: 'CC_ACUSE'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'DF_FECHA_EMISION'},
			{name: 'DF_FECHA_VENC'},
			{name: 'DF_CARGA'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'FN_MONTO'},
			{name: 'CATEGORIA'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESCONTAR'},
      //---NUEVO1
      {name: 'DESC_BINS'},
      {name : 'NOMBRE_IF'},
      {name : 'COMISION_APLICABLE'},
      {name : 'MON_COMISION'},{name : 'MODO_PLAZO'},
      {name : 'MON_DEPOSITAR'},
      {name : 'ORDEN_ENVIADO'},
      {name : 'ID_OPERACION'},
      {name : 'CODIGO_AUTORIZA'},
      {name : 'FECHA_REGISTRO'},
      //---fin nuevo1
		{name: 'IC_DOCUMENTO'},
		{name: 'IVA'},
		{name : 'NUM_TARJETA'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaGrid3,
			exception: {
				fn: function(proxy,type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					procesarConsultaGrid3(null,null,null);
				}
			}
		}
	});
  
  ///******
  
  
	var resumenTotalesData = new Ext.data.JsonStore({
			root: 'registros',
			url: '24reporte01ext.data.jsp',
			baseParams: {
						informacion: 'ResumenTotales'
			},
			fields: [
						{name: 'NOMBRE_MONEDA'},
						{name: 'NUM_REGISTROS'},
						{name: 'TOTAL_MONTO', type: 'float'},
						{name: 'TOTAL_MONTO_VALUADO', type: 'float'},
						{name: 'TOTAL_MONTO_CREDITO', type: 'float'},
            {name: 'TOTAL_MONTO_DEP', type: 'float'}
			],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
						load: procesarResumenTotalesData,
						exception: {
							fn: function(proxy,type,action,optionsRequest,response,args){
								NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
								procesarResumenTotalesData(null,null,null);
							}
						}
			}
	});
//--------------------------------COMPONENTES-----------------------------------
	//Grid Detalle
	var gridDetalle = {
		xtype: 'grid',
		store: consultaDataDetalle,
		id: 'gridDetalle',
		columns: [
						{
							header: 'Fecha del Cambio',
							tooltip: 'Fecha del Cambio',
							dataIndex: 'FECH_CAMBIO',
							sortable: true,
							align: 'center',
							width: 150
						},
						{
							header: 'Cambio Estatus',
							tooltip: 'Cambio Estatus',
							dataIndex: 'CD_DESCRIPCION',
							sortable: true,
							align: 'center',
							width: 150
						},
						{
							header: 'Fecha Emision Anterior',
							tooltip: 'Fecha Emision Anterior',
							dataIndex: 'FECH_EMI_ANT',
							sortable: true,
							align: 'center',
							width: 150
						},
						{
							header: 'Fecha Emision Nueva',
							tooltip: 'Fecha Emision Nueva',
							dataIndex: 'FECH_EMI_NEW',
							sortable: true,
							align: 'center',
							width: 150
						},
						{
							header: 'Monto Anterior',
							tooltip: 'Monto Anterior',
							dataIndex: 'FN_MONTO_ANTERIOR',
							sortable: true,
							align: 'center',
							width: 150,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Monto Nuevo',
							tooltip: 'Monto Nuevo',
							dataIndex: 'FN_MONTO_NUEVO',
							sortable: true,
							align: 'center',
							width: 150,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Fecha Vencimiento Anterior',
							tooltip: 'Fecha Vencimiento Anterior',
							dataIndex: 'FECH_VENC_ANT',
							sortable: true,
							align: 'center',
							width: 150
						},
						{
							header: 'Fecha Vencimiento Nuevo',
							tooltip: 'Fecha Vencimiento Nuevo',
							dataIndex: 'FECH_VENC_NEW',
							sortable: true,
							align: 'center',
							width: 150
						},
						{
							header: 'Modalidad de Plazo Anterior',
							tooltip: 'Modalidad de Plazo Anterior',
							dataIndex: 'MODO_PLAZO_ANTERIOR',
							sortable: true,
							align: 'center',
							width: 150
						},
						{
							header: 'Modalidad de Plazo Nuevo',
							tooltip: 'Modalidad de Plazo Nuevo',
							dataIndex: 'MODO_PLAZO_NUEVO',
							sortable: true,
							align: 'center',
							width: 150
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 885,
		title: '',
		frame: true
	};
	//Grid 1: para estatus Negociable, Vencido sin Operar, Baja, No Negociable
	var grid1 = new Ext.grid.GridPanel({
		id: 'grid1',
		store: consultaDataGrid1 ,
		hidden: true,
		columns: [
						{
							header: 'Distribuidor',
							tooltip: 'Distribuidor',
							dataIndex: 'NOMBRE_DIST',
							sortable:true, 
							resiazable: true,
							width: 250,
							hidden: false
						},
						{
							header: 'Num. de Acuse de Carga',
							tooltip: 'Num. de Acuse de Carga',
							dataIndex: 'CC_ACUSE',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'N�mero de documento inicial',
							tooltip: 'N�mero de documento inicial',
							dataIndex: 'IG_NUMERO_DOCTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Fecha de emisi�n',
							tooltip: 'Fecha de emisi�n',
							dataIndex: 'DF_FECHA_EMISION',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Fecha de vencimiento',
							tooltip: 'Fecha de vencimiento',
							dataIndex: 'DF_FECHA_VENC',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Fecha de publicaci�n',
							tooltip: 'Fecha de publicaci�n',
							dataIndex: 'DF_CARGA',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Plazo docto.',
							tooltip: 'Plazo docto.',
							dataIndex: 'IG_PLAZO_DOCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Moneda',
							tooltip: 'Moneda',
							dataIndex: 'MONEDA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
							
						},
						{
							header: 'Monto',
							tooltip: 'Monto',
							dataIndex: 'FN_MONTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('$0,0.00') 
						},
						{
							header: 'Categor�a',
							tooltip: 'Categor�a',
							dataIndex: 'CATEGORIA',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'Plazo para descuento <br> en d�as',
							tooltip: 'Plazo para descuento en d�as',
							dataIndex: 'IG_PLAZO_DESCUENTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: '% de descuento',
							tooltip: '% de descuento',
							dataIndex: 'FN_PORC_DESCUENTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Monto% de descuento',
							tooltip: 'Monto% de descuento',
							dataIndex: 'MONTO_DESCONTAR',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Tipo Conv.',
							tooltip: 'Tipo Conv.',
							dataIndex: 'TIPO_CONVERSION',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: true,
							renderer: function (value, metaData, registro, rowIndex, colIndex, store){
														if(registro.get('TIPO_CAMBIO') == 1){
															value = "";
														}
														return value;
							}
						},		
						{
							header: 'Tipo Cambio',
							tooltip: 'Tipo Cambio',
							dataIndex: 'TIPO_CAMBIO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: true,
							renderer: function (value, metaData, registro, rowIndex, colIndex, store){
														if(registro.get('IC_MONEDA') == 1 && registro.get('TIPO_CAMBIO'== 1)){
															value = ""; 
														}
														return Ext.util.Format.number(value, '$0,0.00');
							}
						},
						{
							header: 'Monto valuado',
							tooltip: 'Monto valuado',
							dataIndex: 'MONTO_VALUADO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: true,
							renderer: function (value, metaData, registro, rowIndex, colIndex, store){
														if(registro.get('TIPO_CAMBIO') == 1){
															value = "";
														}
														return Ext.util.Format.number(value, '$0,0.00');
							}
						},
						{
							header: 'Modalidad de plazo',
							tooltip: 'Modalidad de plazo',
							dataIndex: 'MODO_PLAZO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: true,
							renderer: function(value, metaData, registro, rowIndex, colIndex, store){
														if(registro.get("CG_VENTACARTERA") == "S"){
															value = "";
														}
											return value;
							}
						},
						{
							header: 'Fecha de Cambio <br> de Estatus',
							tooltip: 'Fecha de Cambio de Estatus',
							dataIndex: 'MAX_FECHA_CAMBIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false							
						},
						{
							xtype: 'actioncolumn',
							header: 'Cambios del <br> documento',
							tooltip: 'Cambios del documento',
							dataIndex: 'IC_DOCUMENTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 100,
							align: 'center',
							hidden: false,
							items: [
										{
											getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {				
													var cmbStatus = Ext.getCmp('cmbStatus');
													var estatus = cmbStatus.getValue();													
													if (estatus =='D2' || estatus == 'D9' || estatus == 'D5' || estatus == 'D1') {
														this.items[0].tooltip = 'Ver';								
														return 'iconoLupa';
													}else{
														return "N/A";
													}
											},
											handler: mostrarGrid
										}
							]
						}
		],
		stripeRows: true,
		loadMask: true, height: 400,
		width:940,
		frame: true,
		title: 'Estatus:',
		bbar: {
			items: [
						'-',
						{
							xtype: 'button',
							text: 'Totales',
							id: 'btnTotales',
							hidden: false,
							handler: function(boton,evento){
											resumenTotalesData.load({
													params: Ext.apply(fp.getForm().getValues())						
											});
											var gridTotales = Ext.getCmp('gridTotales');
											if(!gridTotales.isVisible()){
												gridTotales.show();
												gridTotales.el.dom.scrollIntoView();
											}
							}
						},
						{
							xtype: 'button',
							text: 'Generar Archivo',
							tooltip: 'Generar archivo CSV',
							id: 'btnGenerarArchivo',
							handler: function(boton,evento){
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '24reporte01ext.data.jsp',
									params: {
												informacion: 'ArchivoCSV',
												status: cveEstatus
									},
									callback: procesarSuccessFailureGenerarArchivo
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar Archivo',
							tooltip: 'Descargar archivo CSV',
							id: 'btnBajarArchivo',
							hidden: true
						},
						{
							xtype: 'button',
							text: 'Generar PDF',
							tooltip: 'Generar archivo PDF',
							id: 'btnGenerarPDF',
							handler: function(boton,evento){
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '24reporte01ext.data.jsp',
									params: {
												informacion: 'ArchivoTotalPDF',
												status: cveEstatus
												},
									callback: procesarSuccessFailureGenerarTotalPDF
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar PDF',
							tooltip: 'Descargar archivo PDF',
							id: 'btnBajarPDF',
							hidden: true
						}
			]
		}
	});
	//Grid 2: Estatus Seleccionado Pyme, Seleccionado IF/Operado, Operado Pagado, 
	//			 Pendiente de Pago IF , Rechazado IF y En Porceso de Autorizaci�n IF
	var grid2 = new Ext.grid.GridPanel({
		id: 'grid2',
		store: consultaDataGrid2,
		hidden: true,
		columns: [
						{
							header: 'Distribuidor',
							tooltip: 'Distribuidor',
							dataIndex: 'NOMBRE_DIST',
							sortable:true, 
							resiazable: true,
							width: 250,
							hidden: false
						},
						{
							header: 'Num. de Acuse de Carga',
							tooltip: 'Num. de Acuse de Carga',
							dataIndex: 'CC_ACUSE',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'N�mero de documento <br> inicial',
							tooltip: 'N�mero de documento inicial',
							dataIndex: 'IG_NUMERO_DOCTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'Fecha de emisi�n',
							tooltip: 'Fecha de emisi�n',
							dataIndex: 'DF_FECHA_EMISION',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Fecha de vencimiento',
							tooltip: 'Fecha de vencimiento',
							dataIndex: 'DF_FECHA_VENC',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Fecha de publicaci�n',
							tooltip: 'Fecha de publicaci�n',
							dataIndex: 'DF_CARGA',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Plazo docto.',
							tooltip: 'Plazo docto.',
							dataIndex: 'IG_PLAZO_DOCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Moneda',
							tooltip: 'Moneda',
							dataIndex: 'MONEDA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
							
						},
						{
							header: 'Monto',
							tooltip: 'Monto',
							dataIndex: 'FN_MONTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Categor�a',
							tooltip: 'Categor�a',
							dataIndex: 'CATEGORIA',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'Plazo para descuento <br> en d�as',
							tooltip: 'Plazo para descuento en d�as',
							dataIndex: 'IG_PLAZO_DESCUENTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: '% de descuento',
							tooltip: '% de descuento',
							dataIndex: 'FN_PORC_DESCUENTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Monto% de descuento',
							tooltip: 'Monto% de descuento',
							dataIndex: 'MONTO_DESCONTAR',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Tipo Conv.',
							tooltip: 'Tipo Conv.',
							dataIndex: 'TIPO_CONVERSION',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: true,
							renderer: function (value, metaData, registro, rowIndex, colIndex, store){
														if(registro.get('TIPO_CAMBIO') == 1){
															value = "";
														}
														return value;
							}
						},		
						{
							header: 'Tipo Cambio',
							tooltip: 'Tipo Cambio',
							dataIndex: 'TIPO_CAMBIO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: true,
							renderer: function (value, metaData, registro, rowIndex, colIndex, store){
														if(registro.get('IC_MONEDA') == 1 || registro.get('TIPO_CAMBIO'== 1)){
															value = ""; 
														}
														return Ext.util.Format.number(value, '$0,0.00');
							}
						},
						{
							header: 'Monto valuado',
							tooltip: 'Monto valuado',
							dataIndex: 'MONTO_VALUADO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: true,
							renderer: function (value, metaData, registro, rowIndex, colIndex, store){
														if(registro.get('TIPO_CAMBIO') == 1){
															value = "";
														}
														return Ext.util.Format.number(value, '$0,0.00');
							}
						},
						{
							header: 'Modalidad de plazo',
							tooltip: 'Modalidad de plazo',
							dataIndex: 'MODO_PLAZO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: true,
							renderer: function(value, metaData, registro, rowIndex, colIndex, store){
														var cmbStatus = Ext.getCmp('cmbStatus');
														if(cmbStatus.getValue() == "D20"){
															if(registro.get("CG_VENTACARTERA") == "S"){
																value = "";
															}
														}
											return value;
							}
						},
						{
							header: 'Tipo de Cr�dito',
							tooltip: 'Tipo de Cr�dito',
							dataIndex: 'TIPO_CREDITO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'N�mero de documento final',
							tooltip: 'N�mero de documento final',
							dataIndex: 'IC_DOCUMENTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 180,
							hidden: false
						},
						{
							header: 'Monto',
							tooltip: 'Monto',
							dataIndex: 'MONTO_CREDITO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Plazo',
							tooltip: 'Plazo',
							dataIndex: 'IG_PLAZO_CREDITO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'Fecha de vencimiento',
							tooltip: 'Fecha de vencimiento',
							dataIndex: 'FECHA_VENC_CREDITO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Fecha de operaci�n IF',
							tooltip: 'Fecha de operaci�n IF',
							dataIndex: 'FECHA_OPERAIF',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'IF',
							tooltip: 'IF',
							dataIndex: 'NOMBRE_IF',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'Referencia tasa de inter�s',
							tooltip: 'Referencia tasa de inter�s',
							dataIndex: 'REFERENCIA_INT',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'Valor tasa de inter�s',
							tooltip: 'Valor tasa de inter�s',
							dataIndex: 'VALOR_TASA_INT',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Monto de Intereses',
							tooltip: 'Monto de Intereses',
							dataIndex: 'MONTO_TASA_INT',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Monto total <br> (capital e intereses)',
							tooltip: 'Monto total (capital e intereses)',
							dataIndex: '',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: function(value,metaData,registro,rowIndex,colIndex,store){
											var montoTotal = registro.get('MONTO_CREDITO') + registro.get('MONTO_TASA_INT');
											value = montoTotal;
											return Ext.util.Format.number(value,'$0,0.00');
							}
						}
		],
		stripeRows: true,
		loadMask: true, height: 400,
		width:940,
		frame: true,
		title: 'Estatus:',
		bbar: {
			items: [
						'-',
						{
							xtype: 'button',
							text: 'Totales',
							id: 'btnTotales2',
							hidden: false,
							handler: function(boton,evento){
											resumenTotalesData.load({
													params: Ext.apply(fp.getForm().getValues())						
											});
											var gridTotales = Ext.getCmp('gridTotales');
											if(!gridTotales.isVisible()){
												gridTotales.show();
												gridTotales.el.dom.scrollIntoView();
											}
							}
						},
						{
							xtype: 'button',
							text: 'Generar Archivo',
							tooltip: 'Generar archivo CSV',
							id: 'btnGenerarArchivo2',
							handler: function(boton,evento){
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '24reporte01ext.data.jsp',
									params: {
												informacion: 'ArchivoCSV',
												status: cveEstatus
									},
									callback: procesarSuccessFailureGenerarArchivo2
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar Archivo',
							tooltip: 'Descargar archivo CSV',
							id: 'btnBajarArchivo2',
							hidden: true
						},
						{
							xtype: 'button',
							text: 'Generar PDF',
							tooltip: 'Generar archivo PDF',
							id: 'btnGenerarPDF2',
							handler: function(boton,evento){
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '24reporte01ext.data.jsp',
									params: {
												informacion: 'ArchivoTotalPDF',
												status: cveEstatus
												},
									callback: procesarSuccessFailureGenerarTotalPDF2
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar PDF',
							tooltip: 'Descargar archivo PDF',
							id: 'btnBajarPDF2',
							hidden: true
						}
			]
		}
	});
  
  
////***********************************
//Grid 3: Estatus Operada TC
	var grid3 = new Ext.grid.GridPanel({
		id: 'grid3',
		store: consultaDataGrid3,
		hidden: true,
		columns: [
						{
							header: 'Distribuidor',
							tooltip: 'Distribuidor',
							dataIndex: 'NOMBRE_DIST',
							sortable:true, 
							resiazable: true,
							width: 250,
							hidden: false
						},
						{
							header: 'Num. de Acuse de Carga',
							tooltip: 'Num. de Acuse de Carga',
							dataIndex: 'CC_ACUSE',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'N�mero de documento <br> inicial',
							tooltip: 'N�mero de documento inicial',
							dataIndex: 'IG_NUMERO_DOCTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'Fecha de emisi�n',
							tooltip: 'Fecha de emisi�n',
							dataIndex: 'DF_FECHA_EMISION',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Fecha de vencimiento',
							tooltip: 'Fecha de vencimiento',
							dataIndex: 'DF_FECHA_VENC',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
              renderer:function(value){
                return "N/A"
                },
							hidden: false
						},
						{
							header: 'Fecha de publicaci�n',
							tooltip: 'Fecha de publicaci�n',
							dataIndex: 'DF_CARGA',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 120,
							hidden: false
						},
						{
							header: 'Plazo docto.',
							tooltip: 'Plazo docto.',
							dataIndex: 'IG_PLAZO_DOCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 120,
              renderer:function(value){
                return "N/A"
                },
							hidden: false
						},
						{
							header: 'Moneda',
							tooltip: 'Moneda',
							dataIndex: 'MONEDA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
							
						},
						{
							header: 'Monto',
							tooltip: 'Monto',
							dataIndex: 'FN_MONTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Categor�a',
							tooltip: 'Categor�a',
							dataIndex: 'CATEGORIA',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false
						},
						{
							header: 'Plazo para descuento <br> en d�as',
							tooltip: 'Plazo para descuento en d�as',
							dataIndex: 'IG_PLAZO_DESCUENTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
              renderer:function(value){
                return "N/A"
                },
							hidden: false
						},
						{
							header: '% de descuento',
							tooltip: '% de descuento',
							dataIndex: 'FN_PORC_DESCUENTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Monto% de descuento',
							tooltip: 'Monto% de descuento',
							dataIndex: 'MONTO_DESCONTAR',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 150,
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},{
							header: 'IF',
							tooltip: 'Descripci�n de IF',
							dataIndex: 'NOMBRE_IF',
							sortable:true, 
							resiazable: true,
							align: 'center'
						}, {
							header: 'Monto',
							tooltip: 'Monto',
							dataIndex: '',
							sortable:true, 
							resiazable: true,
							align: 'center',
              renderer: function(v,params,record){
                var montodos= record.data.FN_MONTO - record.data.MONTO_DESCONTAR;
                  return "<div align='right'>"+Ext.util.Format.usMoney(montodos)+"</div>"	
                }
						},{
							header: '% Comisi�n aplicable de Terceros',
							tooltip: '% Comisi�n aplicable de Terceros',
							dataIndex: 'COMISION_APLICABLE',
							sortable:true, 
							resiazable: true,
							align: 'center',
              renderer: Ext.util.Format.numberRenderer('0.00%')
						},{
							header: 'Monto Comisi�n de Terceros (IVA Incluido)',
							tooltip: 'Monto Comisi�n de Terceros (IVA Incluido)',
							dataIndex: '',
							sortable:true, 
							resiazable: true,
							align: 'center',
							renderer: function(v,params,record){
							 var montodos= record.data.FN_MONTO - record.data.MONTO_DESCONTAR;
							 var montoCom= montodos * ( (record.data.COMISION_APLICABLE) / 100   ); 
							 var iva		 = record.data.IVA;
							 var nuevoIva = (iva/100) +1;
							 return "<div align='right'>"+Ext.util.Format.usMoney( montoCom * nuevoIva )+"</div>"	
							 }//fin renderer
						},{
							header: 'Monto a Depositar por Operaci�n',
							tooltip: 'Monto a Depositar por Operaci�n',
							dataIndex: 'MON_DEPOSITAR',
							sortable:true, 
							resiazable: true,
							align: 'center',
              renderer: function(v,params,record){
                var montodos= record.data.FN_MONTO - record.data.MONTO_DESCONTAR;
                var montoCom= montodos * ( (record.data.COMISION_APLICABLE) / 100   );
					 var iva		 = record.data.IVA;
					 var nuevoIva= (iva/100) +1;
					 var montoIva= montoCom * nuevoIva;
                var montoDep= montodos - montoIva;
                return "<div align='right'>"+Ext.util.Format.usMoney( montoDep )+"</div>"	
                }
						},{
							header: 'ID Orden Enviado',
							tooltip: 'ID Orden Enviado',
							dataIndex: 'ORDEN_ENVIADO',
							sortable:true, 
							resiazable: true,
							align: 'center'
						},{
							header: 'Respuesta de Operaci�n',
							tooltip: 'Respuesta de Operaci�n',
							dataIndex: 'ID_OPERACION',
							sortable:true, 
							resiazable: true,
							align: 'center'
						},{
							header: 'C�digo Autorizaci�n',
							tooltip: 'C�digo Autorizaci�n',
							dataIndex: 'CODIGO_AUTORIZA',
							sortable:true, 
							resiazable: true,
							align: 'center'
						},{
							header: 'N�mero Tarjeta de Cr�dito',
							tooltip: 'N�mero Tarjeta de Cr�dito',
							dataIndex: 'NUM_TARJETA',
							sortable:true, 
							resiazable: true,
							align: 'center',
							renderer: function(v,params,record){
							 return "<div align='right'>"+"XXXX-XXXX-XXXX-"+v+"</div>";
						  }							
						},{
							xtype: 'actioncolumn',
							header: 'Cambios del <br> documento',
							tooltip: 'Cambios del documento',
							dataIndex: 'IC_DOCUMENTO',
							sortable:true, 
							resiazable: true,
							align: 'center',
							width: 100,
							align: 'center',
							hidden: false,
							items: [
										{
											getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {				
													var cmbStatus = Ext.getCmp('cmbStatus');
													var estatus = cmbStatus.getValue();													
														this.items[0].tooltip = 'Ver';								
														return 'iconoLupa';
											},
											handler: mostrarGrid
										}
							]
						}
		],
		stripeRows: true,
		loadMask: true, height: 400,
		width:940,
		frame: true,
		title: 'Estatus:',
		bbar: {
			items: [
						'-',
						{
							xtype: 'button',
							text: 'Totales',
							id: 'btnT32',
							hidden: false,
							handler: function(boton,evento){
											resumenTotalesData.load({
													params: Ext.apply(fp.getForm().getValues())						
											});
											var gridTotales = Ext.getCmp('gridTotales32');
											if(!gridTotales.isVisible()){
												gridTotales.show();
												gridTotales.el.dom.scrollIntoView();
											}
							}
						},
						{
							xtype: 'button',
							text: 'Generar Archivo',
							tooltip: 'Generar archivo CSV',
							id: 'btnCSV32',
              iconCls		: 'icoXls',
							handler: function(boton,evento){
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '24reporte01ext.data.jsp',
									params: {
												informacion: 'ArchivoCSV',
												status: cveEstatus
									},
									callback: descargaArchivo//procesarSuccessFailureGenerarArchivo2
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar Archivo',
							tooltip: 'Descargar archivo CSV',
							id: 'btnCSV32b',
							hidden: true
						},
						{
							xtype: 'button',
							text: 'Generar PDF',
							tooltip: 'Generar archivo PDF',
							id: 'btnPDF32', 
              iconCls		: 'icoPdf',
							handler: function(boton,evento){
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '24reporte01ext.data.jsp',
									params: {
												informacion: 'ArchivoTotalPDF',
												status: cveEstatus
												},
									callback: descargaArchivoPDF//procesarSuccessFailureGenerarTotalPDF2
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar PDF',
							tooltip: 'Descargar archivo PDF',
							id: 'btnPDF32b',
							hidden: true
						}
			]
		}
	});

//*************************************gridTotales32
	var gridTotales32 = {
		xtype: 'grid',
		store: resumenTotalesData,
		id: 'gridTotales32',
		style: 'margin:0 auto;',
		hidden: true,
		columns: [
						{
							header: 'Moneda',
              tooltip:'Moneda',
							dataIndex: 'NOMBRE_MONEDA',
							align: 'left',
							width: 150
						},
						{
							header: 'Total Documentos',
              tooltip: 'Total Documentos',
							dataIndex: 'NUM_REGISTROS',
							align: 'center',
							width: 150
						},
						{
							header: 'Total Monto',
              tooltip: 'Total Monto',
							dataIndex: 'TOTAL_MONTO',
							align: 'center',
							width: 150,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},{
              header: 'Total Monto a Depositar por Operaci�n',
              tooltip: 'Total Monto a Depositar por Operaci�n',
							dataIndex: 'TOTAL_MONTO_DEP',
							align: 'center',
							width: 200,
              renderer:function(value){
                return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
              }
            }
		],
		width: 940,
		height: 110,
		title: 'Totalesss',
		tools: [
					{
						id: 'close',
						handler: function(evento,toolEl,panel,tc){
									panel.hide();
						}
					}
		],
		frame: false
	};

//**************************************
	
	var gridTotales = {
		xtype: 'grid',
		store: resumenTotalesData,
		id: 'gridTotales',
		style: 'margin:0 auto;',
		hidden: true,
		columns: [
						{
							header: 'TOTALES',
							dataIndex: 'NOMBRE_MONEDA',
							align: 'left',
							width: 250
						},
						{
							header: 'TOTAL DOCUMENTOS',
							dataIndex: 'NUM_REGISTROS',
							align: 'center',
							width: 250
						},
						{
							header: 'TOTAL MONTO',
							dataIndex: 'TOTAL_MONTO',
							align: 'center',
							width: 250,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'TOTAL MONTO VALUADO',
							dataIndex: 'TOTAL_MONTO_VALUADO',
							align: 'center',
							width: 250,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'TOTAL MONTO CREDITO',
							dataIndex: 'TOTAL_MONTO_CREDITO',
							align: 'center',
							width: 250,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						}
		],
		width: 940,
		height: 110,
		title: 'Totales',
		tools: [
					{
						id: 'close',
						handler: function(evento,toolEl,panel,tc){
									panel.hide();
						}
					}
		],
		frame: false
	};
  
  
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'status',
			id: 'cmbStatus',
			fieldLabel: 'Estatus',
			mode: 'local',
			hiddenName: 'status',
			emptyText: 'Seleccione Estatus...',
			forceSelection: true, 
			triggerAction:'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoEstatus,
			displayField: 'descripcion', 
			valueField: 'clave',
			tpl: NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{
					fn: function(combo){
            
            
            Ext.getCmp('gridTotales32').hide();
						var gridTotales = Ext.getCmp('gridTotales');
						if(gridTotales.isVisible()){
							gridTotales.hide();
						}
						cveEstatus = combo.getValue();	
						var fp = Ext.getCmp('forma');
						fp.el.mask('Enviando...','x-mask-loading');
            
            if (cveEstatus=="D32"){
              grid3.setTitle('Estatus: Operada TC');
              grid1.hide();grid2.hide();
							grid3.show();
              consultaDataGrid3.load({
								params: Ext.apply(fp.getForm().getValues())
							});
            } else {
            
						if(cveEstatus=="D2"||cveEstatus=="D9"||cveEstatus=="D5"||cveEstatus=="D1"){
							grid2.hide();grid3.hide();
							grid1.show();
							if(cveEstatus=="D2"){
								grid1.setTitle('Estatus Negociable');
							}else if(cveEstatus=="D9"){
								grid1.setTitle('Estatus Vencido sin Operar');
							}else if(cveEstatus=="D5"){
								grid1.setTitle('Estatus Baja');
							}else if(cveEstatus=="D1"){
								grid1.setTitle('Estatus No Negociable');
							}
							consultaDataGrid1.load({
								params: Ext.apply(fp.getForm().getValues())
							});
						}else{
							grid1.hide();grid3.hide();
							grid2.show();
							if(cveEstatus=="D3"){
								grid2.setTitle('Estatus: Seleccionado Pyme');
							}else if(cveEstatus=="C1"){
								grid2.setTitle('Seleccionado IF / Operado');
							}else if(cveEstatus=="D11"){
								grid2.setTitle('Estatus: Operado Pagado');
							}else if(cveEstatus=="D22"){
								grid2.setTitle('Estatus: Pendiente de Pago IF');
							}else if(cveEstatus=="D20"){
								grid2.setTitle('Estatus: Rechazado IF');
							}else if(cveEstatus=="D24"){
								grid2.setTitle('Estatus: En Proceso de Autorizaci�n IF');
							}
							consultaDataGrid2.load({
								params: Ext.apply(fp.getForm().getValues())
							});
						}
          }//else D32
					}
				}
			}
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: 'margin:0 auto;',
		frame: true,
		border: false,
		title: '<div><center>Documentos por Estatus</div>',
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		hidden: false,
		defaults:{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true
	});
//---------------------------------PRINCIPAL------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
					fp,
					NE.util.getEspaciador(20),
					grid1,
          NE.util.getEspaciador(20),
					grid2,
          NE.util.getEspaciador(20),
          grid3,
          NE.util.getEspaciador(20),
					gridTotales,
          NE.util.getEspaciador(20),
          gridTotales32
		]
	});
	catalogoEstatus.load();
});