<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*, java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj	=	new JSONObject();
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

String dist					= "";
String numDocto			= "";
String numAcuseCarga		= "";
String fechaEmision		= "";
String fechaPublicacion	= "";
String fechaVencimiento	= "";
String plazoDocto			= "";
String moneda				= "";
String monto				= "";
String tipoConversion	= "";
String tipoCambio			= "";
String categoria			= "";
String plazoDescuento	= "";
String porcDescuento		= "";
String modoPlazo			= "";
String estatus				= "";
String fechaCambio		= "";
String icMoneda			= "";
double montoValuado		= 0;
double montoDescontar	= 0;
String bandeVentaCartera= "",  observaciones ="";

//totales
int		totalDoctosMN			= 0;
int 		totalDoctosUSD			= 0;
double	totalMontoMN			= 0;
double	totalMontoUSD			= 0;
double	totalMontoValuadoMN	= 0;
double	totalMontoValuadoUSD	= 0;
AccesoDB con = new AccesoDB();
try {
	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;
	if (informacion.equals("ArchivoCSV")) {
		CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsBajaDocEpoDist());
		con.conexionDB();
		ResultSet	rs = null;
		rs = queryHelper.getCreateFile(request,con);
		int aRow = 0;
		/*******************
		Cabecera del archivo
		********************/
		contenidoArchivo.append("Datos Documento Inicial \nDistribuidor,Número de documento inicial,Num. acuse carga,Fecha de emisión,Fecha de publicación,Fecha vencimiento,Plazo docto,Moneda,Monto");
		if(!"".equals(tipoConversion))	{
			contenidoArchivo.append(",Tipo conv.,Tipo cambio,Monto valuado");
		}
		contenidoArchivo.append(",Categoria,Plazo para descuento en días,% de Descuento,Monto % de descuento,Modalidad de plazo,Tipo de cambio de estatus,Fecha de cambio de estatus, Observaciones");
		/***************************
		 Generacion del archivo
		/***************************/
		while(rs.next()){
			dist 					= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
			numDocto				= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
			numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
			fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
			fechaPublicacion	= (rs.getString("DF_FECHA_PUBLICACION")==null)?"":rs.getString("DF_FECHA_PUBLICACION");
			fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
			plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
			moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
			//monto 				= rs.getDouble("FN_MONTO");
			monto					= (rs.getString("FN_MONTO")==null)?"0":rs.getString("FN_MONTO");
			tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
			//tipoCambio			= ((rs.getString("TIPO_CAMBIO")==null)?"1":rs.getString("TIPO_CAMBIO")).equals("")?"1":rs.getString("TIPO_CAMBIO");
			tipoCambio			= (rs.getString("TIPO_CAMBIO")==null)?"1":rs.getString("TIPO_CAMBIO");
			plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
			porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
			montoDescontar		= Double.parseDouble((String)monto)*Double.parseDouble((String)porcDescuento)/100;
			montoValuado		= (Double.parseDouble((String)monto) -montoDescontar)* Double.parseDouble((String)tipoCambio);
			//montoValuado		= (monto-montoDescontar)*tipoCambio;
			//montoValuado		= (monto-montoDescontar)*Double.parseDouble((String)tipoCambio);
			modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
			estatus 				= (rs.getString("TIPO_CAMBIO_ESTATUS")==null)?"":rs.getString("TIPO_CAMBIO_ESTATUS");
			fechaCambio 		= (rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
			icMoneda				= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
			//icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
			categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
			bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
			observaciones		= (rs.getString("OBSERVACIONES")==null)?"":rs.getString("OBSERVACIONES");
			
			if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
			}
			if("1".equals(icMoneda)){
				totalDoctosMN ++;
				totalMontoMN += Double.parseDouble(monto);
			}else{
				totalDoctosUSD ++;
				totalMontoUSD += Double.parseDouble(monto);
				totalMontoValuadoUSD += montoValuado;
			}
			contenidoArchivo.append("\n"+dist.replace(',',' ')+","+numDocto+","+numAcuseCarga+","+fechaEmision+","+fechaPublicacion+","+fechaVencimiento+","+plazoDocto+","+moneda+","+monto);
			if(!"".equals(tipoConversion))	{
				contenidoArchivo.append(","+(("1".equals(tipoCambio))?tipoConversion:"") );
				contenidoArchivo.append(","+(("1".equals(tipoCambio))?""+Comunes.formatoDecimal(tipoCambio,2,false):""));
				contenidoArchivo.append(","+(("1".equals(tipoCambio))?""+Comunes.formatoDecimal(montoValuado,2,false):""));
			}
			contenidoArchivo.append("," + categoria + "," + plazoDescuento + "," + porcDescuento + "," + montoDescontar + "," + modoPlazo + "," + estatus + "," + fechaCambio + "," + observaciones.replace(',',' ') );
			aRow++;
		}
		if (aRow == 0)	{
			contenidoArchivo.append("\nNo se Encontró Ningún Registro");
		}
		if(aRow >= 1){
			contenidoArchivo.append("\nTotal M.N.,"+totalDoctosMN+",,,,,,,"+totalMontoMN);
			contenidoArchivo.append("\nTotal Dolares.,"+totalDoctosUSD+",,,,,,,"+totalMontoUSD+",,,"+totalMontoValuadoUSD);
		}
	}
	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
%>
<%=jsonObj%>