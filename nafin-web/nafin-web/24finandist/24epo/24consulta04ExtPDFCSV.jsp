<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.anticipos.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
	JSONObject jsonObj = new JSONObject();
	System.out.println("ic_pyme ------->"+ic_pyme);  
	System.out.println("informacion ------->"+informacion); 


if(informacion.equals("GenerarArchivo")  ) {
	
try {
	
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

	String tipoArchivo = (request.getParameter("tipoArchivo") != null) ? request.getParameter("tipoArchivo") : "";
	
	System.out.println("tipoArchivo ------->"+tipoArchivo); 
	
	
	if(tipoArchivo.equals("PDF")){
		
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";	
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	
		pdfDoc.addText("Mexico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		
		List info  = cargaDocto.esquemaCliente(iNoCliente); 
		String rs_respInt= "", rs_tipoFin= "";
		if(info.size()>0){
			rs_respInt=  info.get(0).toString();
			rs_tipoFin=  info.get(1).toString();
		}
		
		HashMap consulta  = cargaDocto.esquemaCliente2(iNoCliente); 
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Parametrización General Vigente","celda01",ComunesPDF.CENTER);
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Responsable de Pago de Interés en DM:  "+rs_respInt,"formas",ComunesPDF.CENTER);
		pdfDoc.addText("Modalidades de Plazo operables en DM y CCC:  "+rs_tipoFin,"formas",ComunesPDF.CENTER);
		pdfDoc.addText("Líneas de crédito autorizadas para operar en DM:","formas",ComunesPDF.CENTER);
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
		
		if(consulta.size()>0){
			pdfDoc.setTable(7, 100);
			pdfDoc.setCell("IF Relacionado","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Num.  línea crédito","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Fecha autorización","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto línea","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Esquema de cobro de intereses","celda01",ComunesPDF.CENTER);
			
			
			for (int i = 0; i <=consulta.size(); i++) {		
				HashMap datos = (HashMap)consulta.get("REGISTROS"+i);
				if(datos !=null) {
					pdfDoc.setCell(datos.get("NOMBREIF").toString() ,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(datos.get("NUMLINEA").toString() ,"formas",ComunesPDF.CENTER);		
					pdfDoc.setCell(datos.get("MONEDA").toString() ,"formas",ComunesPDF.CENTER);			
					pdfDoc.setCell(datos.get("FECHA_AUTORIZACION").toString() ,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(datos.get("PLAZO").toString() ,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+datos.get("MONTO_LINEA").toString() ,"formas",ComunesPDF.CENTER);		
					pdfDoc.setCell(datos.get("ESQUEMA").toString() ,"formas",ComunesPDF.CENTER);		
				}	
			}
			pdfDoc.addTable();
		}
		if(!ic_pyme.equals("")) {
			String rs_tipoCred ="",  rs_Pyme ="", descripcion= "";
			List info2  = cargaDocto.esquemaCliente3(iNoCliente, ic_pyme); 		
			if(info2.size()>0){
				rs_tipoCred=  info2.get(0).toString();
				rs_Pyme=  info2.get(1).toString();
			}
			
			if(rs_tipoCred.equals("C") ) {  descripcion ="Modalidad 2 (Riesgo Distribuidor)"; 	 }
			if(rs_tipoCred.equals("D") ) {  descripcion ="Modalidad 1 (Riesgo Empresa de Primer Orden)"; 	 }

			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("Nombre del cliente:  "+rs_Pyme,"formas",ComunesPDF.CENTER);
			pdfDoc.addText("Tipo de crédito::  "+descripcion,"formas",ComunesPDF.CENTER);
			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
 
	 
		if ("C".equals(rs_tipoCred.trim()) || "A".equals(rs_tipoCred.trim())) {
			
			HashMap consulta1  = cargaDocto.esquemaCliente4(iNoCliente, ic_pyme);
			
			pdfDoc.setTable(3, 100);
			pdfDoc.setCell("IF Relacionado","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Esquema de cobro de intereses","celda01",ComunesPDF.CENTER);	
			
			
			for (int i = 0; i <=consulta1.size(); i++) {		
				HashMap datos1 = (HashMap)consulta1.get("REGISTROS"+i);
				if(datos1 !=null) {
					pdfDoc.setCell(datos1.get("NOMBREIF").toString() ,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(datos1.get("MONEDA").toString() ,"formas",ComunesPDF.CENTER);			
					pdfDoc.setCell(datos1.get("ESQUEMA").toString() ,"formas",ComunesPDF.CENTER);	
				}					
			}	
			pdfDoc.addTable();
		}
	}
		
	pdfDoc.endDocument();		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	
	}
	
	if(tipoArchivo.equals("CSV")){
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo="";
	
		List info  = cargaDocto.esquemaCliente(iNoCliente); 
		String rs_respInt= "", rs_tipoFin= "";
		if(info.size()>0){
			rs_respInt=  info.get(0).toString();
			rs_tipoFin=  info.get(1).toString();
		}
		contenidoArchivo.append("Parametrización General Vigente"+"\n"); 				
		contenidoArchivo.append("Responsable de Pago de Interés en DM , "+rs_respInt+"\n"); 				
		contenidoArchivo.append("Modalidades de Plazo operables en DM y CCC: , "+rs_tipoFin+"\n"); 
		contenidoArchivo.append("Líneas de crédito autorizadas para operar en DM , "+"\n"); 
		contenidoArchivo.append("  "+"\n"); 


		HashMap consulta  = cargaDocto.esquemaCliente2(iNoCliente); 
		if(consulta.size()>0){
			contenidoArchivo.append("IF Relacionado, Num. línea crédito , Moneda, Fecha autorización ,Plazo, Monto línea, Esquema de cobro de intereses"+"\n");
			for (int i = 0; i <=consulta.size(); i++) {		
				HashMap datos = (HashMap)consulta.get("REGISTROS"+i);
				if(datos !=null) {
					contenidoArchivo.append(datos.get("NOMBREIF").toString().replace(',',' ')+","); 				
					contenidoArchivo.append(datos.get("NUMLINEA").toString().replace(',',' ')+","); 				
					contenidoArchivo.append(datos.get("MONEDA").toString().replace(',',' ')+","); 						
					contenidoArchivo.append(datos.get("FECHA_AUTORIZACION").toString().replace(',',' ')+",");
					contenidoArchivo.append(datos.get("PLAZO").toString().replace(',',' ')+","); 
					contenidoArchivo.append(datos.get("MONTO_LINEA").toString().replace(',',' ')+","); 				
					contenidoArchivo.append(datos.get("ESQUEMA").toString().replace(',',' ')+"\n"); 				
				}	
			}
		}  
		if(!ic_pyme.equals("")) {
			String rs_tipoCred ="",  rs_Pyme ="", descripcion= "";
			List info2  = cargaDocto.esquemaCliente3(iNoCliente, ic_pyme); 		
			if(info2.size()>0){
				rs_tipoCred=  info2.get(0).toString();
				rs_Pyme=  info2.get(1).toString();
			}		
			if(rs_tipoCred.equals("C") ) {  descripcion ="Modalidad 2 (Riesgo Distribuidor)"; 	 }
			if(rs_tipoCred.equals("D") ) {  descripcion ="Modalidad 1 (Riesgo Empresa de Primer Orden)"; 	 }
			
			contenidoArchivo.append("   "+"\n");
			contenidoArchivo.append("Nombre del cliente"+","+rs_Pyme+"\n");
			contenidoArchivo.append("Tipo de crédito"+","+descripcion+"\n");
			contenidoArchivo.append("  "+"\n"); 
			
			if ("C".equals(rs_tipoCred.trim()) || "A".equals(rs_tipoCred.trim())) {
				HashMap consulta1  = cargaDocto.esquemaCliente4(iNoCliente, ic_pyme);
				contenidoArchivo.append("IF Relacionado,Moneda,Esquema de cobro de intereses "+"\n");			
				
				for (int i = 0; i <=consulta1.size(); i++) {		
					HashMap datos1 = (HashMap)consulta1.get("REGISTROS"+i);
					if(datos1 !=null) {				
					contenidoArchivo.append(datos1.get("NOMBREIF").toString().replace(',',' ')+","); 						
					contenidoArchivo.append(datos1.get("MONEDA").toString().replace(',',' ')+","); 							
					contenidoArchivo.append(datos1.get("ESQUEMA").toString().replace(',',' ')+"\n"); 					
							
					}					
				}				
			}
		}
		
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}	


} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
		
}
	
%>
<%=jsonObj%>