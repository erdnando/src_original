	
Ext.onReady(function() {
	/*-------------INICIAN HANDLRES---------------------------------------------*/	
	var procesarSuccessFailureGenerarPDF=  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	
	var procesarSuccessFailureGenerarCSV=  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}		
	}
	

	//GRID ESQUEMA DE OPERACIONES
	var procesarConsultaDataEO = function(store, arrRegistros, opts) 	{
	
		var el = gridEO.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridEO.isVisible()) {
				gridEO.show();				
			}			
								
				if(store.getTotalCount() > 0) {
					el.unmask();
				} else {					
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
		//GRID PARAMETROS
	var procesarConsultaDataParametros = function(store, arrRegistros, opts) 	{
	
		var el = gridParametros.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridParametros.isVisible()) {
				gridParametros.show();				
			}			
								
				if(store.getTotalCount() > 0) {
					el.unmask();
				} else {					
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
/*-------------TERMINAN HANDLRES---------------------------------------------*/		
/*-------------INICIAN STORES---------------------------------------------*/	
		var consultaDataEO = new Ext.data.JsonStore({
			root : 'registros',
			url : '24consulta08Ext.data.jsp',
			baseParams: {
				informacion: 'ConsultaEO'
			},
			fields: [
				{name: 'NOMBREPARAMETRO'},
				{name: 'TIPODATO'}, 				
				{name: 'VALORES'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaDataEO,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaDataEO(null, null, null);						
					}
				}
			}
		});
		var consultaDataParametros = new Ext.data.JsonStore({
			root : 'registros',
			url : '24consulta08Ext.data.jsp',
			baseParams: {
				informacion: 'ConsultaParametros'
			},
			fields: [
				{name: 'NOMBREPARAMETRO'},
				{name: 'TIPODATO'}, 				
				{name: 'VALORES'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaDataParametros,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaDataEO(null, null, null);						
					}
				}
			}
		});
		var descripcionRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
			metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align: left;" ';
			return value;
			
		}
/*-------------TERMINAN STORES---------------------------------------------*/		
/*-------------INICIAN COMPONENTES GRAFICOS---------------------------------------------*/	
	var gridEO = new Ext.grid.GridPanel({
		store: consultaDataEO,
		hidden: false,
		margins: '20 0 0 0',
		style: ' margin:0 auto;',
		title: 'Secci�n 1: Esquemas de operaci�n ',
		columns: [
			{
				header: 'Nombre del par�metro',
				tooltip: 'Nombre del par�metro',
				dataIndex: 'NOMBREPARAMETRO',
				sortable: true,
				resizable: true,
				width: 150,				
				align: 'left',
				renderer:	descripcionRenderer
			},				
			{
				header: 'Tipo de dato',
				tooltip: 'Tipo de dato',
				dataIndex: 'TIPODATO',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Valores',
				tooltip: 'Valores',
				dataIndex: 'VALORES',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:	descripcionRenderer
			}									
			],			
			stripeRows: true,
			loadMask: true,
			height: 400,
			width: 400,
			//viewConfig: {forceFit: true} ,				
			frame: true
		});
		//componente grid Parametros
		var gridParametros = new Ext.grid.GridPanel({
		store: consultaDataParametros,
		hidden: false,
		margins: '20 0 0 0',
		style: ' margin:0 auto;',
		title: 'Secci�n 2: Par�metros ',
		columns: [
			{
				header: 'Nombre del par�metro',
				tooltip: 'Nombre del par�metro',
				dataIndex: 'NOMBREPARAMETRO',
				sortable: true,
				resizable: true,
				width: 150,				
				align: 'left',
				renderer:	descripcionRenderer
			},				
			{
				header: 'Tipo de dato',
				tooltip: 'Tipo de dato',
				dataIndex: 'TIPODATO',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Valores',
				tooltip: 'Valores',
				dataIndex: 'VALORES',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:	descripcionRenderer
			}									
			],			
			stripeRows: true,
			loadMask: true,
			height: 400,
			width: 400,
			//viewConfig: {forceFit: true} ,				
			frame: true
		});
	
	var fpGenerar = new Ext.Container({
		layout: 'table',
		id: 'formaI',
		hidden: false,
		style: ' margin:0 auto;',
		layoutConfig: {
			columns: 6
		},
		width:	'100',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
		{
			xtype: 'button',
			text: 'Generar Archivo',
			id: 'btnGenerarCSV',			
			handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
				url: '24consulta08ExtPDFCSV.jsp',
				params:{
					tipoArchivo: 'CSV',
					informacion: 'GenerarArchivo'						
					},
					callback: procesarSuccessFailureGenerarCSV
				});
			}
		}, 
		{
			xtype: 'button',
			text: 'Bajar Archivo',
			id: 'btnBajarCSV',
			hidden: true
		}	,		
		{
			xtype: 'button',
			text: 'Generar PDF',
			id: 'btnGenerarPDF',			
			handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
				url: '24consulta08ExtPDFCSV.jsp',
				params:{
					tipoArchivo: 'PDF',
					informacion: 'GenerarArchivo'						
					},
					callback: procesarSuccessFailureGenerarPDF
				});
			}
		},
		{
			xtype: 'button',
			text: 'Bajar Archivo',
			id: 'btnBajarPDF',
			hidden: true
		}	
		
	]
	});
/*-------------INICIAN COMPONENTES GRAFICOS---------------------------------------------*/	
/*---------------------CONTENEDOR GLOBAL-------------------------------*/
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		style: ' margin:0 auto;',
		items: [			
		NE.util.getEspaciador(20),
			gridEO,
			NE.util.getEspaciador(20),
			gridParametros,
			NE.util.getEspaciador(20),
			fpGenerar
		]
	});
/*----------INIT--------------*/	
	consultaDataEO.load();
	consultaDataParametros.load();

} );