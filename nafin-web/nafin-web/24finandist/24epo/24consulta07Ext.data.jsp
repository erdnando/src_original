<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,		
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
String ic_if = (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") : "";
String Txtfchini = (request.getParameter("Txtfchini") != null) ? request.getParameter("Txtfchini") : "";
String Txtffin = (request.getParameter("Txtffin") != null) ? request.getParameter("Txtffin") : "";
String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");
String publicaDoctosFinanciables = (request.getParameter("publicaDoctosFinanciables") == null)?"":request.getParameter("publicaDoctosFinanciables");

double  entMonto = 0;
double entComAplicable = 0;
double montoComision = 0;
double montoDepositar = 0;
double mntovaluado = 0;

String infoRegresar	=	"";
int start = 0;
int limit = 0;
try {

	if (informacion.equals("valoresIniciales") ) {

	JSONObject jsonObj = new JSONObject();
	try{
		ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
		//F009-2015 Obtener si la EPO publica documentos financiables a meses sin intereses
		publicaDoctosFinanciables = BeanParametro.DesAutomaticoEpo(iNoCliente,"CS_MESES_SIN_INTERESES");
                
                
                
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("publicaDoctosFinanciables", publicaDoctosFinanciables);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al obtener los valores iniciales. ", e);
	}
	infoRegresar = jsonObj.toString();
	} else if (informacion.equals("CatalogoPYME") ) {
	
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cp.cg_razon_social"); 
		//cat.setHabilitado("'S,'N'");
		cat.setClaveEpo(iNoCliente);	
		cat.setOrden("cp.cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	
	} else if (informacion.equals("CatalogoIF") ) {
			 
		CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClaveEpo(iNoCliente);
		infoRegresar = cat.getJSONElementos();	
		
	} else if (informacion.equals("Consulta") || informacion.equals("ArchivoTPDF")   || informacion.equals("ArchivoCSV")  || informacion.equals("ArchivoXPDF")   || informacion.equals("ResumenTotales")) {
		
		//Obtengo parametrizaciones  
		String tiposCredito= "", responsable ="", cgTipoConversion ="";			
		ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
                
                String operaContrato = BeanParametro.obtieneOperaSinCesion(iNoEPO); //PARAMETRO NUEVO JAGE 14012017
             	
		List datos  =   BeanParametro.obtieneParametrosAviso(iNoCliente);  
		responsable = (String)datos.get(0);
		tiposCredito = (String)datos.get(1);
		cgTipoConversion = (String)datos.get(2);
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		AvNotifEPODist avNotifEPODist = new com.netro.distribuidores.AvNotifEPODist();
		avNotifEPODist.setIc_if(ic_if);
		avNotifEPODist.setIc_pyme(ic_pyme);
		avNotifEPODist.setFchinicial(Txtfchini);
		avNotifEPODist.setFchfinal(Txtffin);
		avNotifEPODist.setNumOrden(numOrden);
		avNotifEPODist.setTipo_credito(tiposCredito);
		avNotifEPODist.setIc_epo(iNoCliente);
		avNotifEPODist.setResponsable(responsable);
		avNotifEPODist.setCgTipoConversion(cgTipoConversion);
		avNotifEPODist.setPublicaDoctosFinanciables(publicaDoctosFinanciables);
                avNotifEPODist.setOperaContrato(operaContrato);
                
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(avNotifEPODist);
		JSONObject 	resultado	= new JSONObject();
		if (informacion.equals("Consulta") ||  informacion.equals("ArchivoXPDF")  ) {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		}
		if (informacion.equals("Consulta") ) {
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				Registros 	reg = queryHelper.getPageResultSet(request,start,limit);
				String iva = "1."+avNotifEPODist.obtenIVA();
				while(reg.next()){
					String montoCredito = reg.getString("MONTO_CREDITO")==null?"0":reg.getString("MONTO_CREDITO");
					String comApli = reg.getString("COMISION_APLICABLE")==null?"0":reg.getString("COMISION_APLICABLE");
					montoComision = (Double.parseDouble(montoCredito))*(((Double.parseDouble(comApli))/100)*Double.parseDouble(iva));
					montoDepositar =(Double.parseDouble(montoCredito))-montoComision;
					reg.setObject("MONTO_COMISION",String.valueOf(montoComision));
					reg.setObject("MONTO_DEPOSITAR",String.valueOf(montoDepositar));
				}
				String consulta	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + reg.getJSONData()+"}";
				resultado = JSONObject.fromObject(consulta);
				resultado.put("RESPONSABLE",responsable);
				resultado.put("TIPOSCREDITO",tiposCredito);
				resultado.put("CGTIPOCONVERSION",cgTipoConversion);
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
			infoRegresar = resultado.toString(); 
		} else if (informacion.equals("ArchivoTPDF")){
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				resultado.put("success", new Boolean(true));
				resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				resultado.put("success", new Boolean(false));
				throw new AppException("Error al generar el archivo PDF", e);
			}		
			infoRegresar = resultado.toString(); 	
		} else if (informacion.equals("ArchivoCSV")){
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				resultado.put("success", new Boolean(true));
				resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
			infoRegresar = resultado.toString();
		} else if (informacion.equals("ArchivoXPDF")){
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
				resultado.put("success", new Boolean(true));
				resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}		
			infoRegresar = resultado.toString(); 
		} else if (informacion.equals("ResumenTotales")) {				
			//Debe ser llamado despues de Consulta
			queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
			infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion			
			
		}	
	}

} catch (Exception e) {		
	throw new AppException("24consulta07Ext.data.jsp", e);
} finally {
	//System.info("24consultaExtPDFCSV(S)");  
}
%>
<%=infoRegresar%>
