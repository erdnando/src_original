<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.CatalogoPymeDistribuidores,		
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
	String infoRegresar	=	"";


try {

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
		
 	JSONObject 	resultado	= new JSONObject();

if (informacion.equals("CatalogoPYME") ) {	


		
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cp.cg_razon_social"); 		
		cat.setClaveEpo(iNoCliente);	
		cat.setOrden("cp.cg_razon_social");
		infoRegresar = cat.getJSONElementos();
		
}else if (informacion.equals("Consulta") ) {		

	String rs_respInt= "", rs_tipoFin= "";
	List info  = cargaDocto.esquemaCliente(iNoCliente); 
	if(info.size()>0) {
		rs_respInt=  info.get(0).toString();
		rs_tipoFin=  info.get(1).toString();
	}	
	JSONArray registros = new JSONArray();	
	HashMap consulta  = cargaDocto.esquemaCliente2(iNoCliente); 	
	if(consulta.size()>0){
		for (int i = 0; i <=consulta.size(); i++) {		
			HashMap datos = (HashMap)consulta.get("REGISTROS"+i);
			if(datos !=null) {
				HashMap consulta1 = new HashMap();
				consulta1.put("NOMBREIF",datos.get("NOMBREIF"));
				consulta1.put("NUMLINEA", datos.get("NUMLINEA"));
				consulta1.put("MONEDA", datos.get("MONEDA"));
				consulta1.put("FECHA_AUTORIZACION", datos.get("FECHA_AUTORIZACION"));
				consulta1.put("PLAZO", datos.get("PLAZO"));
				consulta1.put("MONTO_LINEA", datos.get("MONTO_LINEA"));
				consulta1.put("ESQUEMA", datos.get("ESQUEMA"));			
				registros.add(consulta1);	
			}						
		}
	}
	
	String consultas = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consultas);
	resultado.put("RESPONSABLE",rs_respInt);
	resultado.put("TIPO_FINAN",rs_tipoFin);	
	infoRegresar = resultado.toString();
			
	
}else if (informacion.equals("ConsultaIF") ) {	

	String rs_tipoCred ="",  rs_Pyme ="", descripcion= ""; 
	JSONArray registros = new JSONArray();

	
	if(!ic_pyme.equals("")) {
		List info  = cargaDocto.esquemaCliente3(iNoCliente, ic_pyme); 	
		if(info.size()>0) {
			rs_tipoCred=  info.get(0).toString();
			rs_Pyme=  info.get(1).toString();
		}
		   
		if(rs_tipoCred.equals("C") ) {  
			descripcion ="Modalidad 2 (Riesgo Distribuidor)";
		}
		if(rs_tipoCred.equals("D") ) { 
			descripcion ="Modalidad 1 (Riesgo Empresa de Primer Orden)";
		}
	 
		if ("C".equals(rs_tipoCred.trim()) || "A".equals(rs_tipoCred.trim())) {
			
			HashMap consulta  = cargaDocto.esquemaCliente4(iNoCliente, ic_pyme);
			if(consulta.size()>0){
				for (int i = 0; i <=consulta.size(); i++) {		
					HashMap datos = (HashMap)consulta.get("REGISTROS"+i);
					if(datos !=null) {
						HashMap consulta1 = new HashMap();
						consulta1.put("NOMBREIF",datos.get("NOMBREIF"));
						consulta1.put("MONEDA", datos.get("MONEDA"));			
						consulta1.put("ESQUEMA", datos.get("ESQUEMA"));			
						registros.add(consulta1);	
					}						
				}			
			}
		}
	}
	
	String consultas = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consultas);
	resultado.put("TIPOCREDITO",rs_tipoCred);
	resultado.put("NOMBREPYME",rs_Pyme);
	resultado.put("DESCRIPCION",descripcion);
	infoRegresar = resultado.toString();
	
}

	} catch (Exception e) {		
			throw new AppException("24consultaExtPDFCSV", e);
		} finally {
			//System.info("24consultaExtPDFCSV(S)");
	}
	
	
%>
<%=infoRegresar%>
