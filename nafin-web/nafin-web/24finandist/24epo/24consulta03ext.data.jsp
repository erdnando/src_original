<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject" 
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
///////
String ic_pyme			=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String tipo_credito	=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";
/***** INICIO: F011-2015 *****/
String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String ic_epo               = (request.getParameter("ic_epo")               ==null)?"":request.getParameter("ic_epo");
String ic_moneda            = (request.getParameter("ic_moneda")            ==null)?"":request.getParameter("ic_moneda");
String ic_cambio_estatus    = (request.getParameter("ic_cambio_estatus")    ==null)?"":request.getParameter("ic_cambio_estatus");
String fn_monto_de          = (request.getParameter("fn_monto_de")          ==null)?"":request.getParameter("fn_monto_de");
String fn_monto_a           = (request.getParameter("fn_monto_a")           ==null)?"":request.getParameter("fn_monto_a");
String ig_numero_docto      = (request.getParameter("ig_numero_docto")      ==null)?"":request.getParameter("ig_numero_docto");
String monto_con_descuento  = (request.getParameter("monto_con_descuento")  ==null)?"":"checked";
String cc_acuse             = (request.getParameter("cc_acuse")             ==null)?"":request.getParameter("cc_acuse");
String modo_plazo           = (request.getParameter("modo_plazo")           ==null)?"":request.getParameter("modo_plazo");
String fecha_emi_de         = (request.getParameter("fecha_emi_de")         ==null)?"":request.getParameter("fecha_emi_de");
String fecha_emi_a          = (request.getParameter("fecha_emi_a")          ==null)?"":request.getParameter("fecha_emi_a");
String fecha_vto_de         = (request.getParameter("fecha_vto_de")         ==null)?"":request.getParameter("fecha_vto_de");
String fecha_vto_a          = (request.getParameter("fecha_vto_a")          ==null)?"":request.getParameter("fecha_vto_a");
String fecha_publicacion_de = (request.getParameter("fecha_publicacion_de") ==null)?"":request.getParameter("fecha_publicacion_de");
String fecha_publicacion_a  = (request.getParameter("fecha_publicacion_a")  ==null)?"":request.getParameter("fecha_publicacion_a");
String fecha_cambio_de      = (request.getParameter("fecha_cambio_de")      ==null)?fechaHoy:request.getParameter("fecha_cambio_de");
String fecha_cambio_a       = (request.getParameter("fecha_cambio_a")       ==null)?fechaHoy:request.getParameter("fecha_cambio_a");
String NOnegociable         = (request.getParameter("NOnegociable")         ==null)?"":request.getParameter("NOnegociable");
/***** FIN: F011-2015 *****/
if (informacion.equals("valoresIniciales"))	{

	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	NOnegociable =  BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_NO_NEGOCIABLES");

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("ses_ic_epo", iNoCliente);
	jsonObj.put("NOnegociable", NOnegociable);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoDistribuidor")){

	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setClaveEpo(iNoCliente);
	cat.setCampoClave("cp.ic_pyme");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();
 
}else if (informacion.equals("CatalogoEstatusDist")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_cambio_estatus");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_cambio_estatus");
	cat.setValoresCondicionIn("23,34,4,22,2,14,24", Integer.class);
	cat.setOrden("ic_cambio_estatus");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoModoPlazoDist")){
	
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

	String valida	=	"";
	valida = cargaDocto.getValidaIn(iNoCliente,ic_pyme);
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_financiamiento");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_tipo_financiamiento");
	cat.setValoresCondicionIn(valida, Integer.class);
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta")){
	int start = 0;
	int limit = 0;

	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsBajaDocEpoDist());

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request);
		}
		infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
}else if (informacion.equals("ResumenTotalesA")) {		//Datos para el Resumen de Totales

	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsBajaDocEpoDist());
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

} else if(informacion.equals("ArchivoPDF")){

	JSONObject jsonObj = new JSONObject();
	String nombreArchivo = "";
	ConsBajaDocEpoDist paginador = new ConsBajaDocEpoDist();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_moneda(ic_moneda);
	paginador.setIc_cambio_estatus(ic_cambio_estatus);
	paginador.setFn_monto_a(fn_monto_a);
	paginador.setFn_monto_de(fn_monto_de);
	paginador.setIg_numero_docto(ig_numero_docto);
	paginador.setMonto_con_descuento(monto_con_descuento);
	paginador.setCc_acuse(cc_acuse);
	paginador.setModo_plazo(modo_plazo);
	paginador.setFecha_emi_a(fecha_emi_a);
	paginador.setFecha_emi_de(fecha_emi_de);
	paginador.setFecha_vto_a(fecha_vto_a);
	paginador.setFecha_vto_de(fecha_vto_de);
	paginador.setFecha_publicacion_a(fecha_publicacion_a);
	paginador.setFecha_publicacion_de(fecha_publicacion_de);
	paginador.setFecha_cambio_a(fecha_cambio_a);
	paginador.setFecha_cambio_de(fecha_cambio_de);
	paginador.setNOnegociable(NOnegociable);

	try{
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e){
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	infoRegresar = jsonObj.toString();

}
%>
<%=infoRegresar%>

