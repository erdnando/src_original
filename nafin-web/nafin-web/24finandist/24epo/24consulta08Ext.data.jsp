<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String infoRegresar	=	"";
	
// variables de despliegue
	String tipoCreditoOperar			= 	"";
	String responsabilidadPagoInteres	= 	"";
	String modalidadPlazoOperar			= 	"";
	String tipoCobranza					= 	"";
	String esquemaPagoInteres			=	"";

//	String	tipoCreditoOperar			= 	"";
	String	diasMinimosDocCarga			=	"";
	String	diasMaximosDocCarga			=	"";
	String	tipoConversion				=	"";
	String	opPantallaControl			=	"";
	String	plazoDescuento				=	"";
	String	porcDescuento				=	"";
	String	proxVenc					=	"";
	String	strFechaVenc					=	"";
try {
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
		
 	JSONObject 	resultado	= new JSONObject();

	JSONArray registros;
	HashMap consulta1;
if (informacion.equals("ConsultaEO") ) {	
	
	registros = new JSONArray();
	consulta1 = new HashMap();
	
	List info=cargaDocto.esquemasOperacion(iNoCliente);
	
	if(info.size()>0) {
	tipoCreditoOperar			= info.get(0).toString();
	responsabilidadPagoInteres	= 	info.get(1).toString();
	tipoCobranza				= info.get(2).toString();
	esquemaPagoInteres			=	info.get(3).toString();
	modalidadPlazoOperar		=	info.get(4).toString();
	
		//TIPO_CREDITO_OPERAR
		consulta1.put("NOMBREPARAMETRO","Tipo de crédito a operar");
		consulta1.put("TIPODATO", "C");
						
		if(tipoCreditoOperar.equals("D"))	   	
			consulta1.put("VALORES", "Modalidad 1 (Riesgo Empresa de Primer Orden");
		else if(tipoCreditoOperar.equals("C"))		
			consulta1.put("VALORES", "Modalidad 2 (Riesgo Distribuidor)");
		else if(tipoCreditoOperar.equals("A"))		
			consulta1.put("VALORES", "Ambos");
		registros.add(consulta1);	
		if(tipoCreditoOperar.equals("D")||tipoCreditoOperar.equals("A")){
		consulta1 = new HashMap();
				consulta1.put("NOMBREPARAMETRO","Responsabilidad de Pago de Interés"+
													"(Solo si opera DM)");
				consulta1.put("TIPODATO", "C");
				consulta1.put("VALORES", responsabilidadPagoInteres);				
				registros.add(consulta1);	
	
		}
		consulta1 = new HashMap();
				consulta1.put("NOMBREPARAMETRO","Modalidad de plazo a operar");
				consulta1.put("TIPODATO", "C");
				consulta1.put("VALORES", modalidadPlazoOperar);				
				registros.add(consulta1);
		if(tipoCreditoOperar.equals("D")||tipoCreditoOperar.equals("A")){
		consulta1 = new HashMap();
				consulta1.put("NOMBREPARAMETRO","Tipo de cobranza"+
													"(Sólo si opera DM)");
				consulta1.put("TIPODATO", "C");
				consulta1.put("VALORES", tipoCobranza);				
				registros.add(consulta1);		
		}
		consulta1 = new HashMap();
				consulta1.put("NOMBREPARAMETRO","Esquema de pago de intereses");
				consulta1.put("TIPODATO", "C");
				consulta1.put("VALORES", esquemaPagoInteres);				
				registros.add(consulta1);
	
	}	
	String consultas = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consultas);
	
	infoRegresar = resultado.toString();
}else if (informacion.equals("ConsultaParametros") ) {	
	registros = new JSONArray();
	consulta1 = new HashMap();
	
	List info=cargaDocto.parametros(iNoCliente);
	
	if(info.size()>0) {
	diasMinimosDocCarga			= 	 info.get(0).toString();
	diasMaximosDocCarga			=  info.get(1).toString();
	tipoConversion				=  info.get(2).toString();
	opPantallaControl			= 	 info.get(3).toString();
	plazoDescuento				= 	 info.get(4).toString();
	porcDescuento				= 	 info.get(5).toString();
	proxVenc					= 	 info.get(6).toString();
	strFechaVenc				= 	 info.get(7).toString();
	
	consulta1.put("NOMBREPARAMETRO","Plazo mínimo del financiamiento");
	consulta1.put("TIPODATO", "N");
	consulta1.put("VALORES", diasMinimosDocCarga);
	registros.add(consulta1);
	
	consulta1 = new HashMap();
	consulta1.put("NOMBREPARAMETRO","Plazo máximo del financiamiento");
	consulta1.put("TIPODATO", "N");
	consulta1.put("VALORES", diasMaximosDocCarga);
	registros.add(consulta1);
	
	consulta1 = new HashMap();
	consulta1.put("NOMBREPARAMETRO","Días Máximos para Ampliar la Fecha de Vencimiento");
	consulta1.put("TIPODATO", "N");
	consulta1.put("VALORES", strFechaVenc);
	registros.add(consulta1);
	
	consulta1 = new HashMap();
	consulta1.put("NOMBREPARAMETRO","Tipo de conversión");
	consulta1.put("TIPODATO", "C");
	consulta1.put("VALORES", tipoConversion);
	registros.add(consulta1);
	
	consulta1 = new HashMap();
	consulta1.put("NOMBREPARAMETRO","Operar con pantalla de cifras de control");
	consulta1.put("TIPODATO", "C");
	consulta1.put("VALORES", opPantallaControl);
	registros.add(consulta1);
	
	consulta1 = new HashMap();
	consulta1.put("NOMBREPARAMETRO","Plazo máximo para tomar el descuento,\n Porcentaje de descuento que otorga la EPO");
	consulta1.put("TIPODATO", "N");
	consulta1.put("VALORES","Plazo: "+plazoDescuento+" Porcentaje: "+porcDescuento+" %");
	registros.add(consulta1);
	
	consulta1 = new HashMap();
	consulta1.put("NOMBREPARAMETRO","Aviso de próximos vencimientos en:");
	consulta1.put("TIPODATO", "N");
	consulta1.put("VALORES", proxVenc +"\tdías hábiles");
	registros.add(consulta1);

	}
	String consultas = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consultas);
	infoRegresar = resultado.toString();
}

	} catch (Exception e) {		
			throw new AppException("Error al hacer consulta", e);
		}
	
	
%>
<%=infoRegresar%>
