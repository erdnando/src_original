<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.sql.*, java.math.*,
	com.netro.distribuidores.*,com.netro.exception.*, javax.naming.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
CreaArchivo archivo = new CreaArchivo();

String informacion   			=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

	String dist					= "";
	String numDocto			= "";
	String numAcuseCarga		= "";
	String fechaEmision		= "";
	String fechaPublicacion	= "";
	String fechaVencimiento	= "";
	String plazoDocto			= "";
	String moneda				= "";
	double monto				= 0;
	String tipoConversion	= "";
	String tipoCambio			= "";
	String categoria			= "";
	String plazoDescuento	= "";
	String porcDescuento		= "";
	String modoPlazo			= "";
	String estatus				= "";
	String fechaCambio		= "";
	String icMoneda		= "", observaciones ="";
	double montoValuado	= 0;
	double montoDescontar= 0;
	String bandeVentaCartera= "";
try {
	if(informacion.equals("ArchivoPDF")){
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
		int start = 0;
		int limit = 0;
		int nRow = 0;
		Registros reg = new Registros();
//Inicia creacion de archivo*-*-*-*-*-*-*-*-*-*-*-*-
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);

		CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsBajaDocEpoDist());

		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		reg = queryHelper.getPageResultSet(request,start,limit);
	/***************************/	/* Generacion del archivo *//***************************/
		while (reg.next()) {
			if(nRow == 0){
				int numCols = (!"".equals(tipoConversion)?20:17);

				float widths[];
				if(!"".equals(tipoConversion)){
					widths = new float[]{5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f,6.0f,6.5f,5.5f,5.0f,5.0f,5.0f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f};
				}else{
					widths = new float[]{6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,7.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f};
				}

				pdfDoc.setTable(numCols,100,widths);
				pdfDoc.setCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
				pdfDoc.setCell("Distribuidor","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Emisión","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo Docto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Categoría","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en días","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("% de Descuento","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto % de descuento","celda01rep",ComunesPDF.CENTER);
				if(!"".equals(tipoConversion)){
					pdfDoc.setCell("Tipo Conv.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo Cambio","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Valuado","celda01rep",ComunesPDF.CENTER);
				}
				pdfDoc.setCell("Modalidad de plazo","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de cambio de estatus","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de cambio de estatus","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Observaciones","celda01rep",ComunesPDF.CENTER);
			}
			dist 					= (reg.getString("NOMBRE_DIST")==null)?"":reg.getString("NOMBRE_DIST");
			numDocto				= (reg.getString("IG_NUMERO_DOCTO")==null)?"":reg.getString("IG_NUMERO_DOCTO");
			numAcuseCarga		= (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE");
			fechaEmision		= (reg.getString("DF_FECHA_EMISION")==null)?"":reg.getString("DF_FECHA_EMISION");
			fechaPublicacion	= (reg.getString("DF_FECHA_PUBLICACION")==null)?"":reg.getString("DF_FECHA_PUBLICACION");
			fechaVencimiento	= (reg.getString("DF_FECHA_VENC")==null)?"":reg.getString("DF_FECHA_VENC");
			plazoDocto			= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
			moneda 				= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
			monto 				= Double.parseDouble(reg.getString("FN_MONTO"));
			tipoConversion		= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
			tipoCambio			= ((reg.getString("TIPO_CAMBIO")==null)?"1":reg.getString("TIPO_CAMBIO")).equals("")?"1":reg.getString("TIPO_CAMBIO");
			plazoDescuento		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
			porcDescuento		= (reg.getString("FN_PORC_DESCUENTO")==null)?"0":reg.getString("FN_PORC_DESCUENTO");
			montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
			montoValuado		= (monto-montoDescontar)*Double.parseDouble((String)tipoCambio);
			modoPlazo			= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
			estatus 				= (reg.getString("TIPO_CAMBIO_ESTATUS")==null)?"":reg.getString("TIPO_CAMBIO_ESTATUS");
			fechaCambio 				= (reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
			icMoneda				= (reg.getString("IC_MONEDA")==null)?"":reg.getString("IC_MONEDA");
			//icDocumento			= (reg.getString("IC_DOCUMENTO")==null)?"":reg.getString("IC_DOCUMENTO");
			categoria			= (reg.getString("CATEGORIA")==null)?"":reg.getString("CATEGORIA");
			bandeVentaCartera		= (reg.getString("CG_VENTACARTERA")==null)?"":reg.getString("CG_VENTACARTERA");
			observaciones		= (reg.getString("OBSERVACIONES")==null)?"":reg.getString("OBSERVACIONES");
			if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
			}
		/*************************/ /* Contenido del archivo */ /**************************/
			pdfDoc.setCell(dist.replace(',',' '),"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(numDocto,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(numAcuseCarga,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(fechaEmision,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(fechaPublicacion,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(fechaVencimiento,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(plazoDocto,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(moneda,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(categoria,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(plazoDescuento,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(porcDescuento,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDescontar,2),"formasrep",ComunesPDF.LEFT);
			
			if(!"".equals(tipoConversion)){
				pdfDoc.setCell((("1".equals(tipoCambio))?tipoConversion:""),"formasrep",ComunesPDF.LEFT);
				pdfDoc.setCell((("1".equals(tipoCambio))?Comunes.formatoDecimal(tipoCambio,2,false):""),"formasrep",ComunesPDF.LEFT);
				pdfDoc.setCell((("1".equals(tipoCambio))?Comunes.formatoDecimal(montoValuado,2,false):""),"formasrep",ComunesPDF.LEFT);
			}
			pdfDoc.setCell(modoPlazo,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(estatus,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(fechaCambio,"formasrep",ComunesPDF.LEFT);
			pdfDoc.setCell(observaciones,"formasrep",ComunesPDF.LEFT);
			nRow++;
		} //fin del while
		List vecTotales = queryHelper.getResultCount(request);
		if (nRow != 0 && vecTotales.size()>0) {
			int i = 0;
			int cp = 0;
			List vecColumnas = null;
			for(i=0;i<vecTotales.size();i++){
				vecColumnas = (List)vecTotales.get(i);
				pdfDoc.setCell("TOTAL "+vecColumnas.get(1).toString(),"celda01rep",ComunesPDF.LEFT,3);
				pdfDoc.setCell(vecColumnas.get(2).toString(),"celda01rep",ComunesPDF.LEFT);
				pdfDoc.setCell(" ","celda01rep",ComunesPDF.LEFT,4);
				if(!"".equals(tipoConversion)){
					cp = 12;
				}else{
					cp = 9;
				}
				pdfDoc.setCell(" $ "+Comunes.formatoDecimal(vecColumnas.get(3).toString(),2),"celda01rep",ComunesPDF.LEFT,cp);
			}
		}
		if (nRow == 0)	{
			pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
			pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
		}else	{
			pdfDoc.addTable();
		}
		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>