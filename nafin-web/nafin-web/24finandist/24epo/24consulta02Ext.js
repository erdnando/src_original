Ext.onReady(function() {



var procesarSuccessFailureGenerarCSV = function (opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarCSV.setHandler(function (boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	
	}
	
	var procesarSuccessFailureGenerarPDF = function (opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarPDF.setHandler(function (boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGeneralData = function(store, arrRegistros, opts) {
	var fp = Ext.getCmp('forma');	
		//fp.el.unmask();					
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
					gridGeneral.show();
			}				
		}		
		var el = gridGeneral.getGridEl();
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		var btnBajarCSV = Ext.getCmp('btnBajarCSV');
		var btnBajarPDF = Ext.getCmp('btnBajarPDF');
				
			
		if (arrRegistros == '') {
			el.mask('No se encontr� ning�n registro', 'x-mask');
			btnGenerarPDF.disable();
			btnGenerarCSV.disable();	
			btnBajarCSV.hide();
			btnBajarPDF.hide();
		}else if (arrRegistros != null){
			el.unmask();
			btnGenerarPDF.enable();
			btnGenerarCSV.enable();
		}
	}	
	

	var consultaGeneralData = new Ext.data.GroupingStore({	 
		root : 'registros',
		url : '24consulta02Ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},		
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
			{name: 'NOMBREIF'},
			{name: 'NOMBREMONEDA'},					
			{name: 'NOMBRETASA'},					
			{name: 'RANGOPLAZO'},					
			{name: 'VALOR'},					
			{name: 'RELMAT'},					
			{name: 'PTOS'},					
			{name: 'TASA_APLICAR'},	
			{name: 'FECHAULTIMA'}	
			]
		}),
		groupField: 'NOMBREIF',
		sortInfo:{field: 'NOMBREIF', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarGeneralData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarGeneralData(null, null, null);
				}
			}
		}		
	});
	
	//CREA EL GRID PARA LA OPCION DE VER TASAS 
		var gridGeneral = new Ext.grid.GridPanel({
		store: consultaGeneralData,
		id: 'gridGeneral',
		hidden: false,
		columns: [		
			{
				header: 'Esquema de tasas',
				tooltip: 'Esquema de tasas',
				dataIndex: 'NOMBREIF',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left'	
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'NOMBREMONEDA',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false, 	
				align: 'left'	
			},						
			{
				header: 'Tipo Tasa',
				tooltip: 'Tipo Tasa',
				dataIndex: 'NOMBRETASA',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false,
				align: 'center'	
			},			
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'RANGOPLAZO',
				sortable: false,
				width: 130,
				resizable: true,
				hidden: false,
				align: 'center'	
			},				
			{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'VALOR',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
			},						
			{
				header: 'Rel. Mat.',
				tooltip: 'Rel. Mat.',
				dataIndex: 'RELMAT',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'			
			},			
			{
				header: 'Puntos Adicionales',
				tooltip: 'Puntos Adicionales',
				dataIndex: 'PTOS',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},				
			{
				header: 'Tasa a Aplicar',
				tooltip: 'Tasa a Aplicar',
				dataIndex: 'TASA_APLICAR',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Fecha �ltima actualizaci�n',
				tooltip: 'Fecha �ltima actualizaci�n',
				dataIndex: 'FECHAULTIMA',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			}
			
		],	
		view: new Ext.grid.GroupingView({
            forceFit:true,
            groupTextTpl: '{text}'					 
        }),
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 400,
		width: 943,
		title: '',
		frame: true,
			bbar: {
			xtype: 'toolbar',			
			items: [				
				'Nota: S�lo deber� considerar las tasas que se ajusten a su plazo m�ximo de financiamiento que tiene parametrizado',				
				'->',
				'-',
					{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta02ExtPDFCSV.jsp',
							params:({
								tipoArchivo:'CSV',
								informacion: 'GenerarArchivo'								
							}),	
							callback: procesarSuccessFailureGenerarCSV
						});				
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '24consulta02ExtPDFCSV.jsp',	
							params:({
								tipoArchivo:'PDF',
								informacion: 'GenerarArchivo'								
							}),						
							callback: procesarSuccessFailureGenerarPDF											  
						});						
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
		
	});	
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			gridGeneral
		]
	});
	

	consultaGeneralData.load();
} );