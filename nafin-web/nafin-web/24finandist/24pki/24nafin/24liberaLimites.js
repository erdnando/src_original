Ext.onReady(function(){
	
	var documentos = [];
	var monto = [];
	var valores = []; 
	var tot_monto_nac=0;
	var tot_monto_dol=0;
	
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: false,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: '<h1><b>Captura</1></b>',			
				id: 'btnCaptura',					
				handler: function() {
					window.location = '/nafin/24finandist/24pki/24nafin/24liberaLimites.jsp';
				}
			},		
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',		id: 'btnConsulta',					

				text: 'Consulta',			
						handler: function() {
					window.location = '/nafin/24finandist/24nafin/24liberaLimitesCon.jsp';
				}
			}	
		]
	});
	
//---------------------------- HANDLER'S ---------------------------------

	var procesarConsultaRegistros = function(store, registros, opts){
		var jsonData = store.reader.jsonData;
		var titulo = jsonData.titulo;
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		if (registros != null) {
			var grid  = Ext.getCmp('grid');
			var grPre = Ext.getCmp('gridPreAcuse');
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.StoreMgr.key('totales').load({
										params:	Ext.apply(fp.getForm().getValues(),
												{
													informacion: 'Totales'
												}
											)
										});
				gridTot.show();
				if(titulo=='C'){
					grid.setTitle('Liberaci�n de l�mites por Distribuidor');
					grPre.setTitle('Liberaci�n de l�mites por Distribuidor');
				}
				else if(titulo=='D'){
					grid.setTitle('Liberaci�n de l�mites por EPO');
					grPre.setTitle('Liberaci�n de l�mites por EPO');
				}
				Ext.getCmp('btnLibVenc').enable();
				el.unmask();
			} else {
				Ext.getCmp('btnLibVenc').disable();
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);		}
	}
	
	function procesarSuccessFailureLiberar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('forma');
			var gr = Ext.getCmp('grid');
			var grT = Ext.getCmp('gridTotales')
			fp.el.unmask();
			
			var gridPreAcuse = Ext.getCmp('gridPreAcuse');
			var autentificacion = Ext.getCmp('autentificacion');
			var leyenda = Ext.getCmp('leyenda');
			Ext.getCmp("mensaje").setValue(jsonData.msjleyenda);
			leyenda.show();	
			var btnSalirP = Ext.getCmp('btnSalirP');						
			Ext.getCmp("mensajeAutenta").setValue(jsonData.mensajeAutentificacion);
			autentificacion.show();				

			Ext.getCmp("fpBotones").hide();
			
			if(jsonData.recibo !='')  {
			
				var acuseCifras = [
					['N�mero de Acuse', jsonData.acuse],
					['Fecha de Liberaci�n ', jsonData.fecha],
					['Hora de Liberaci�n', jsonData.hora],
					['Usuario de Captura ', jsonData.usuario]
				];
							
				storeCifrasData.loadData(acuseCifras);	
				gridCifrasControl.show();
				
				Ext.getCmp("documentos").setValue(jsonData.doctoSelec);
				
				// consulta  para  el Acuse 		
				//fp.el.mask('Enviando...', 'x-mask-loading');	
				Ext.getCmp('_hid_docs').setValue(jsonData.doctoSelec);
				consultaPreAcuseData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarPreAcuse',
						pant: 'PantallaAcuse',
						doctos:jsonData.doctoSelec
					})
				});
				fp.hide();
				gr.hide();
				grT.hide();
				gridPreAcuse.show();	
			} else {
				gridPreAcuse.hide();	
				btnSalirP.show();	
				leyenda.hide();	
				Ext.getCmp('leyenda2').hide();
				Ext.getCmp('forma').hide();
				Ext.getCmp('fpBotones').hide();
				Ext.getCmp('grid').hide();
				Ext.getCmp('gridTotales').hide();
			}			
						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	var confirmarAcuse =   function(pkcs7, textoFirmar, vdocumentos ,  vmonto){
			
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnLibVenc").enable();	
			return;	//Error en la firma. Termina...
				
		}else  {
		
			Ext.getCmp("btnLibVenc").disable();					
			Ext.Ajax.request({
				url : '24liberaLimites.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'LiberarVencimiento',
					documentos:vdocumentos,	
					monto:vmonto,
					textoFirmar:textoFirmar,
					pkcs7:pkcs7			
				}),				
				callback: procesarSuccessFailureLiberar
			});
		}	
	}

	var procesarLiberarNafin= function(record) {
		var gridConsulta = Ext.getCmp('grid');
		var store = gridConsulta.getStore();
		var textoFirmar = "Confirmaci�n de la liberaci�n de los vencimientos de los siguientes Intermediarios Financieros";
			 textoFirmar += '\n Pyme, N�mero de documento, Tasa de Fondeo, N�mero de Prestamo, Moneda, Monto a Descontar, Fecha de Vencimiento \n';
		
		if(sm.getCount()<=0)
			Ext.MessageBox.alert('Mensaje','Debes seleccionar al menos un Intermediario'); //Si no se ha seleccionado ningun docto.
		else{
			documentos = [];
			monto = [];
			store.each(function(record) {
				if(sm.isSelected(record)){
					documentos.push(record.data['IC_DOCUMENTO']);
					monto.push(record.data['MONTO_DESC']);
					
					textoFirmar += record.data['INT_FINAN']+", "+ record.data['NO_DOCTO']+", "+record.data['TASA_FON']+", "+record.data['NO_PRESTAMO']
									+", "+record.data['MONEDA']+", "+record.data['MONTO_DESC']+", "+record.data['FEC_VEN']+"\n";
				}		 	
			});
		
			
			NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar, documentos ,  monto  );
			
		}
	}
	
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){			
				var grid = Ext.getCmp("grid");
				
				Ext.getCmp("fechaHoy").setValue(jsonData.fechaHoy);
				Ext.getCmp("strTipoUsuario").setValue(jsonData.strTipoUsuario);
				
				if(jsonData.strTipoUsuario=='IF') {
					
				}else if(jsonData.strTipoUsuario=='NAFIN') {
									
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarGenerarPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('btnGenerarPDF').enable();
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			boton = Ext.getCmp('btnGenerarPDF');
			boton.setIconClass('icoPdf');
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	//-----------------------  MAQUINA DE EDOS.		---------------------------
	
	var accionConsulta = function(estadoSiguiente, respuesta){

		 if(  estadoSiguiente == "CONSULTAR" 		){
			bandera = true;
					
			if(bandera){
				var forma = Ext.getCmp("forma");
				forma.el.mask('Buscando...','x-mask-loading');
	
				// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
				Ext.getCmp("grid").hide(); 
				Ext.StoreMgr.key('registrosConsultados').load({
										params:	Ext.apply(fp.getForm().getValues(),
												{
													informacion: 'consultaDatos',
													pantalla: 'Cap'
												}
											)
				});
			}
		}
	}
	
//---------------------------- STORES ----------------------------------	
	
	var catalogoEpo = new Ext.data.JsonStore({
		id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '24liberaLimites.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '24liberaLimites.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var catalogoDist = new Ext.data.JsonStore({
		id: 'catalogoDist',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '24liberaLimites.data.jsp',
		baseParams: {
			informacion: 'catalogoDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	
	var consultaData = new Ext.data.JsonStore({
		id: 'registrosConsultados',
		root: 'registros',
		url: '24liberaLimites.data.jsp',
		baseParams: {
			informacion: 'consultaDatos'
		},
		fields: [
					{name: 'INT_FINAN'},
					{name: 'DISTRIBUIDOR'},
					{name: 'NO_DOCTO'},
					{name: 'TASA_FON'},
					{name: 'NO_PRESTAMO'},
					{name: 'MONEDA'},
					{name: 'MONTO_DESC'},
					{name: 'FEC_VEN'},
					{name: 'IC_DOCUMENTO'},
					{name: 'IC_MONEDA'},
					{name: 'SELECCION'},
					{name: 'IC_EPO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaRegistros,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});
	
	var consultaPreAcuseData = new Ext.data.JsonStore({
		root : 'registros',
		url: '24liberaLimites.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreAcuse'
		},
		hidden: true,
		fields: [	
					{name: 'INT_FINAN'},
					{name: 'DISTRIBUIDOR'},
					{name: 'NO_DOCTO'},
					{name: 'TASA_FON'},
					{name: 'NO_PRESTAMO'},
					{name: 'MONEDA'},
					{name: 'MONTO_DESC'},
					{name: 'FEC_VEN'},
					{name: 'IC_DOCUMENTO'},
					{name: 'IC_MONEDA'},
					{name: 'IC_EPO'}	
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			//load: procesarConsultaPreAcuseData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						//procesarConsultaPreAcuseData(null, null, null);						
					}
				}
			}			
	});
	
	var consultaTotales = new Ext.data.JsonStore({
		id: 'totales',
		root: 'registros',
		url: '24liberaLimites.data.jsp',
		baseParams: {
			informacion: 'Totales'
		},
		fields: [
					{name: 'IC_MONEDA'},
					{name: 'TOT_MONEDA'},
					{name: 'MONTO_TOT'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			//load: validacionDocs,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});
	
	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
//--------------------------- COMPONENTES -----------------------------------	 
	
	var btnSalirP = new Ext.Container({
		layout: 'table',		
		id: 'btnSalirP',							
		width:	'100',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,		
		items: [		
			{
				xtype: 'button',
				text: 'Salir',
				tooltip:	'Salir',
				id: 'btnSalirPP',		
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '/nafin/24finandist/24pki/24nafin/24liberaLimites.jsp';	
				}
			}
		]
	});
	
	var autentificacion = new Ext.Container({
		layout: 'table',		
		id: 'autentificacion',							
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,		
		items: [					
			{
				xtype: 'displayfield',
				id:	'mensajeAutenta',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}		
			}
		]
	});
	
	var leyenda = new Ext.Container({
		layout: 'table',		
		id: 'leyenda',							
		width:	'600',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,		
		items: [					
			{
				xtype: 'displayfield',
				id:	'mensaje',
				width:	'600',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}		
			}, 
			{ 	xtype: 'displayfield', hidden:true, id: 'documentos', 	value: '' }	
		]
	});
	
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 570,
		height: 200,
		style: 'margin:0 auto;',
		autoHeight : true,
		frame: true
	});
	
	//Columna de checkbox
	var sm = new Ext.grid.CheckboxSelectionModel({
		checkOnly:true,
		listeners: {
			rowdeselect: function(selectModel, rowIndex, record) {
				if(record.data['IC_MONEDA']=='1' && record.data['SELECCION']=='S'){
					tot_monto_nac -= parseFloat(record.data['MONTO_DESC']);
				}
				else if(record.data['IC_MONEDA']=='54' && record.data['SELECCION']=='S'){
					tot_monto_dol -= parseFloat(record.data['MONTO_DESC']);
				}
				
				record.data['SELECCION']='N';
				record.commit();
				
				var store = gridTot.getStore();			
				store.each(function(record) {							
					if(record.data['IC_MONEDA']=='1'){
					 record.data['MONTO_TOT'] = tot_monto_nac;
					}
					if(record.data['IC_MONEDA']=='54'){	
					 record.data['MONTO_TOT'] = tot_monto_dol;
					}	
					record.commit();	
				});
				
			},
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){
				if(record.data['IC_MONEDA']=='1' && record.data['SELECCION']=='N'){
					tot_monto_nac += parseFloat(record.data['MONTO_DESC']);
				}
				else if(record.data['IC_MONEDA']=='54' && record.data['SELECCION']=='N'){
					tot_monto_dol += parseFloat(record.data['MONTO_DESC']);
				}	
				record.data['SELECCION']='S';
				record.commit();
				
				var store = gridTot.getStore();	
				
				store.each(function(record) {							
					if(record.data['IC_MONEDA']=='1'){
					 record.data['MONTO_TOT'] = tot_monto_nac;
					}
					if(record.data['IC_MONEDA']=='54'){	
					 record.data['MONTO_TOT'] = tot_monto_dol;
					}	
					record.commit();	
				});
			}
		}
	});
	
	//Grid de estatus servicio
	var grid = new Ext.grid.GridPanel({
		id: 'grid',
		store: consultaData,
		height: 290,
		width: 920,
		hidden: true,
		loadMask: true,
		margins: '20 0 0 0',
		align: 'center',
		style: 'margin:0 auto',
		title: 'Liberaci�n de limites por Distribuidor',
		sm: sm,
		columns:[
			{
				header: 'Intermediario Financiero',
				tooltip: 'Nombre del IF',
				width: 200,
				dataIndex: 'INT_FINAN',
				sortable: true,
				resizable: true,
				align: 'left'
			},
			{
				header: 'Distribuidor',
				tooltip: 'Nombre del Distribuidor',
				width: 180,
				dataIndex: 'DISTRIBUIDOR',
				sortable: true,
				resizable: true,
				align: 'left'
			},			
			{
				header: 'N�mero de documento',
				tooltip: 'N�mero del documento',
				width: 128,
				dataIndex: 'NO_DOCTO',
				sortable: true,
				resizable: false,
				align: 'center'
			},{
				header: 'Tasa de Fondeo',
				tooltip: 'Tasa de fondeo',
				width: 100,
				dataIndex: 'TASA_FON',
				sortable: false,
				resizable: true,
				align: 'center',
				renderer:function(value,metadata,registro){    
					if(value =='0') {
						return '%';
					}else {
						return  Ext.util.Format.number(value, '0.0000%');
					}
				}
			},{
				header: 'N�mero de Prestamo',
				tooltip: 'N�mero de prestamo',
				width: 115,
				dataIndex: 'NO_PRESTAMO',
				sortable: false,
				resizable: true,
				align: 'center',
				renderer:function(value,metadata,registro){   
					if(value =='0') {
						return '';
					}else {
						return value;
					}
				}
			},{
				header: 'Moneda',
				tooltip: 'Tipo de moneda',
				width: 120,
				dataIndex: 'MONEDA',
				sortable: false,
				resizable: true,
				align: 'center'
			},{
				header: 'Monto a Descontar',
				tooltip: 'Monto a descontar',
				width: 120,
				dataIndex: 'MONTO_DESC',
				sortable: true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Fecha de Vencimiento del Cr�dito',
				tooltip: 'Fecha del vencimiento del cr�dito',
				width: 205,
				dataIndex: 'FEC_VEN',
				sortable: false,
				resizable: true,
				align: 'center'
			},
			sm
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Liberar Vencimientos',
					id: 'btnLibVenc',
					hidden: false,
					iconCls: 'icoAceptar',
					handler: procesarLiberarNafin
				},
				'-'
			]
		}

	});
	
	var gridPreAcuse = new Ext.grid.GridPanel({
		id: 'gridPreAcuse',				
		store: consultaPreAcuseData,	
		style: 'margin:0 auto;',
		title: 'Liberaci�n de limites por Distribuidor',
		hidden: true,	
		columns: [
			{							
				header: 'Intermediario Financiero',
				tooltip: 'Nombre del IF',
				width: 200,
				dataIndex: 'INT_FINAN',
				sortable: true,
				resizable: true,
				align: 'left'			
			},
			{							
				header: 'Distribuidor',
				tooltip: 'Nombre del Distribuidor',
				width: 180,
				dataIndex: 'DISTRIBUIDOR',
				sortable: true,
				resizable: true,
				align: 'left'			
			},
			{							
				header: 'N�mero de documento',
				tooltip: 'N�mero del documento',
				width: 128,
				dataIndex: 'NO_DOCTO',
				sortable: true,
				resizable: false,
				align: 'center'			
			},
			{							
				header: 'Tasa de Fondeo',
				tooltip: 'Tasa de fondeo',
				width: 100,
				dataIndex: 'TASA_FON',
				sortable: false,
				resizable: true,
				align: 'center',
				renderer:function(value,metadata,registro){    
					if(value =='0') {
						return '%';
					}else {
						return  Ext.util.Format.number(value, '0.0000%');
					}
				}				
			},
			{							
				header: 'N�mero de Prestamo',
				tooltip: 'N�mero de prestamo',
				width: 115,
				dataIndex: 'NO_PRESTAMO',
				sortable: false,
				resizable: true,
				align: 'center',
				renderer:function(value,metadata,registro){   
					if(value =='0') {
						return '';
					}else {
						return value;
					}
				}
			},
			{							
				header: 'Moneda',
				tooltip: 'Tipo de moneda',
				width: 120,
				dataIndex: 'MONEDA',
				sortable: false,
				resizable: true,
				align: 'center'			
			},
			{							
				header: 'Monto a Descontar',
				tooltip: 'Monto a descontar',
				width: 120,
				dataIndex: 'MONTO_DESC',
				sortable: true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header: 'Fecha de Vencimiento del Cr�dito',
				tooltip: 'Fecha del vencimiento del cr�dito',
				width: 205,
				dataIndex: 'FEC_VEN',
				sortable: false,
				resizable: true,
				align: 'center'
			}
		],					
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		margins: '20 0 0 0',
		stripeRows: true,
		height: 290,
		width: 920,
		align: 'center',
		frame: false,			
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						Ext.Ajax.request({
							url: '24liberaLimites.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoPDF',
								documentos:documentos,
								doctos: Ext.getCmp('_hid_docs').getValue(),
								pant: 'PantallaAcuse'							
							})					
							,callback: procesarGenerarPDF
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Salir',
					tooltip:	'Salir',
					id: 'btnSalir',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '/nafin/24finandist/24pki/24nafin/24liberaLimites.jsp';
					}
				}
			]
		}
	});
	
	var gridTot = new Ext.grid.GridPanel({
		id: 'gridTotales',
		store: consultaTotales,
		height: 80,
		width: 465,
		hidden: true,
		margins: '20 0 0 0',
		align: 'center',
		style: 'margin:0 auto',
		columns:[
			{
				header: 'Moneda',
				tooltip: 'Tipo de moneda',
				width: 180,
				dataIndex: 'TOT_MONEDA',
				sortable: false,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Monto Total de Documentos Seleccionados',
				tooltip: 'Monto total de los documentos seleccionados',
				width: 275,
				dataIndex: 'MONTO_TOT',
				sortable: false,
				resizable: true,
				align: 'center'
			}
		]
	});
	
	var elementosForma  = [
		{ 	xtype: 'displayfield', 	hidden: true, id: 'fechaHoy', 	value: '' },
		{
			xtype:'hidden',
			id:	'_hid_docs',
			hiddenName : 'hid_docs',
			value:''
		},
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la EPO',
			id: '_ic_epo',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,
			width: 200,
			store: catalogoEpo,
			listeners: {
				select: {
					fn: function(combo) {
							Ext.getCmp('_ic_nae_dis').reset();
							Ext.getCmp('_ic_if').reset();
							catalogoIF.load({
								params: {
									ic_epo: combo.getValue()
								}
							});
							
							catalogoDist.load({
								params: {
									ic_epo: combo.getValue()
								}
							});
					}
				}
			}			
		},{
			xtype: 'combo',
			fieldLabel: 'Distribuidor',
			id: '_ic_nae_dis',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_nae_dis',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,
			width: 200,
			store: catalogoDist
			},
			{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			id: '_ic_if',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_if',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,
			width: 200,
			store: catalogoIF
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento del Cr�dito',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaIni',
					id: 'fechaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaFin',
					margins: '0 20 0 0' ,
					formBind: true,
					listeners:{
						blur: function(field){ 									
							if(field.getValue() != '') {						
							
								var fechaHoy = Ext.getCmp('fechaHoy').getValue();														
								fechaHoy = new Date();
								dia = fechaHoy.getDate();
								mes = fechaHoy.getMonth();
								anio = fechaHoy.getFullYear();
								fechaHoy = new Date(anio,mes,dia);
								fecha = Ext.util.Format.date(field.getValue(),'d/m/Y');
								dataFec = fecha.split("/");
								fecha = new Date(Number(dataFec[2]),Number(dataFec[1])-1,Number(dataFec[0]));
																
								if(fecha.getTime() < fechaHoy.getTime()){
									Ext.MessageBox.alert('Mensaje','La fecha debe ser igual o mayor al dia actual');
									Ext.getCmp("fechaIni").setValue(fechaHoy);
								}
							}					
						}
					}
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaFin',
					id: 'fechaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fechaIni',
					margins: '0 20 0 0',
					formBind: true,
					listeners:{
						blur: function(field){ 									
							if(field.getValue() != '') {								
							
								var fechaHoy = Ext.getCmp('fechaHoy').getValue();														
								fechaHoy = new Date();
								dia = fechaHoy.getDate();
								mes = fechaHoy.getMonth();
								anio = fechaHoy.getFullYear();
								fechaHoy = new Date(anio,mes,dia);
								fecha = Ext.util.Format.date(field.getValue(),'d/m/Y');
								dataFec = fecha.split("/");
								fecha = new Date(Number(dataFec[2]),Number(dataFec[1])-1,Number(dataFec[0]));
																
								if(fecha.getTime() < fechaHoy.getTime()){
									Ext.MessageBox.alert('Mensaje','La fecha debe ser igual o mayor al dia actual');
									Ext.getCmp("fechaFin").setValue(fechaHoy);
								}
							}							
						}
					}
				}
			]
		},
		{ 	xtype: 'displayfield', hidden:true, id: 'strTipoUsuario', 	value: '' },
		{ 	xtype: 'displayfield', hidden:true, id: 'ic_pyme', 	value: '' }	
	];	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Captura Liberaci�n de L�mites',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,	  		
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
				formBind: true,		
				handler: function(boton, evento) {
									var epo = Ext.getCmp('_ic_epo');
									var nae = Ext.getCmp('_ic_nae_dis');
									var inf = Ext.getCmp('_ic_if');
									var fechaIni = Ext.getCmp("fechaIni");
									var fechaFin = Ext.getCmp("fechaFin");
									if(epo.getValue() == "")
										epo.markInvalid('Campo Obligatorio');
									else if(nae.getValue() == "")
										nae.markInvalid('Campo Obligatorio');
									else if(inf.getValue() == "")
										inf.markInvalid('Campo Obligatorio');
									else if(Ext.isEmpty(fechaIni.getValue()) || Ext.isEmpty(fechaFin.getValue())){
										if(Ext.isEmpty(fechaIni.getValue())){
											fechaIni.markInvalid('Debe capturar ambas fechas');
											fechaIni.focus();
											return;
										}
										if(Ext.isEmpty(fechaFin.getValue())){
											fechaFin.markInvalid('Debe capturar ambas fechas');
											fechaFin.focus();
											return;
										}
									}
									else{
										accionConsulta("CONSULTAR", null);
									}
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
								Ext.getCmp('forma').getForm().reset();
								grid.hide();
								gridTot.hide();	
								tot_monto_nac=0;
								tot_monto_dol=0;
				}				
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(15),
			fpBotones,
			NE.util.getEspaciador(20),
			fp,
			autentificacion,
			NE.util.getEspaciador(20),
			gridCifrasControl,
			leyenda,
			btnSalirP,
			NE.util.getEspaciador(20),
			grid,
			gridPreAcuse,
			NE.util.getEspaciador(15),
			gridTot
		]
	});
	
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '24liberaLimites.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});
	
	catalogoEpo.load();
	
});