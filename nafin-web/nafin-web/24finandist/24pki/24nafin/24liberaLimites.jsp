<!DOCTYPE html>
<%@page contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>
<% String version = (String)session.getAttribute("version"); %>

<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
      
      <%@ include file="/extjs.jspf" %>
		<%if(version!=null){%>
		<%@ include file="/01principal/menu.jspf"%>
		<%}%>
      <script language="JavaScript1.2" src="../../../../00utils/valida.js?<%=session.getId()%>"></script>
		<%@ include file="/00utils/componente_firma.jspf" %>
		<%@ include file="../certificado.jspf" %>	
		<script type="text/javascript" src="24liberaLimites.js?<%=session.getId()%>"></script>
      
      <title>Nafi@net - Liberacion de Limites</title>
   </head>   

  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
<div id="_menuApp"></div>
	<div id="Contcentral">
		<%if(version!=null){%>
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<%}%>
		<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
   </div>
</div>
<%if(version!=null){%>
<%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
