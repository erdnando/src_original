<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ page import="com.netro.pdf.*"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%

String sistema			=	(request.getParameter("sistemaMenu")==null)?"":request.getParameter("sistemaMenu");
String ic_epo			=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String ic_pyme			=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String cg_num_distribuidor	=	(request.getParameter("cg_num_distribuidor")==null)?"":request.getParameter("cg_num_distribuidor");
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 

if(!"NAFIN".equals(strTipoUsuario)) {
	ic_epo = iNoCliente;
}
	
if (informacion.equals("validarNumDist"))	{

	boolean dist = true;
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if(!BeanParametros.validarDistribuidor(ic_epo, cg_num_distribuidor)){
		dist = false;
	}
	jsonObj.put("distribuidor",new Boolean(dist));
	infoRegresar = jsonObj.toString();
	
}else  if (informacion.equals("validarMontoLimite"))	{

	String validaMontoLimite=  BeanParametros.getLimiteAcumulado(ic_epo,  ic_pyme);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("validaMontoLimite",validaMontoLimite);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();


}else if (informacion.equals("valoresIniciales") )	{
			
	double montoLimitemn = 0, montoLimitedl = 0;
	String NOnegociable = "N";
	JSONObject jsonObj = new JSONObject();
		
	if(!ic_epo.equals(""))  {
		NOnegociable =  BeanParametros.DesAutomaticoEpo(ic_epo,"PUB_EPO_NO_NEGOCIABLES");		
		List montosLimite = BeanParametros.validarLimitexMoneda2(ic_epo);		
		
		if(montosLimite.size()>0)  {
			for(int a=0;a<montosLimite.size();a++){
				List laux = new ArrayList();
				laux = (List)montosLimite.get(a);
				if(laux.get(1).toString().equals("1")){
					montoLimitemn = Double.parseDouble(laux.get(0).toString());
					jsonObj.put("montoLimitemn", laux.get(0).toString());
				}else if(laux.get(1).toString().equals("54")){
					montoLimitedl = Double.parseDouble(laux.get(0).toString());
					jsonObj.put("montoLimitedl", laux.get(0).toString());
				}
			}
		}else  {
			jsonObj.put("montoLimitemn", "0");
			jsonObj.put("montoLimitedl", "0");
		}
		
		String obtieneTipoCredito = (request.getParameter("obtieneTipoCredito")==null)?"":request.getParameter("obtieneTipoCredito");//Fodea 014-2010 Fase II
		obtieneTipoCredito =  BeanParametros.obtieneTipoCredito(ic_epo); 
		jsonObj.put("obtieneTipoCredito", obtieneTipoCredito);
	}
	
	
	
	if("NAFIN".equals(strTipoUsuario)) {
		jsonObj.put("NAFIN", new Boolean(true));
	}
	if("DCAPT".equals(sistema)){
		jsonObj.put("DCAPT", new Boolean(true));
	}
	jsonObj.put("NOnegociable", NOnegociable);
	jsonObj.put("ses_ic_epo", ic_epo);
	jsonObj.put("strTipoUsuario",strTipoUsuario);	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("ConsultaParametroMonto"))	{

	double montoLimitemn = 0, montoLimitedl = 0;
	JSONObject jsonObj = new JSONObject();

	List montosLimite = BeanParametros.validarLimitexMoneda2(ic_epo);
	for(int a=0;a<montosLimite.size();a++){
		List laux = new ArrayList();
		laux = (List)montosLimite.get(a);
		if(laux.get(1).toString().equals("1")){
			montoLimitemn = Double.parseDouble(laux.get(0).toString());
			jsonObj.put("montoLimitemn", laux.get(0).toString());
		}else if(laux.get(1).toString().equals("54")){
			montoLimitedl = Double.parseDouble(laux.get(0).toString());
			jsonObj.put("montoLimitedl", laux.get(0).toString());
		}
	}

	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoEPODist") ) {

		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		//cat.setClavePyme(ses_ic_pyme);
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoDistribuidor")){

	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setClaveEpo(ic_epo);
	cat.setCampoClave("cp.ic_pyme");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setTipoCredito("D");
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta")){
	List lDatos = BeanParametros.getLimitePyme(ic_epo, ic_pyme, cg_num_distribuidor);
	List NuevoReg = new ArrayList();
	for(int i=0;i<lDatos.size();i++) {
		HashMap hash = new HashMap();
		List lRegistro = (ArrayList)lDatos.get(i);
		String pyme 	= lRegistro.get(0).toString();
		String nomPyme 	= lRegistro.get(1).toString();
		String limite 	= lRegistro.get(2).toString();
		String acum 	= lRegistro.get(3).toString();
		String mensajeter 	= lRegistro.get(4).toString();
		String fecha 	= lRegistro.get(5).toString();
		String icusuario 	= lRegistro.get(6).toString();
		String nombre 	= lRegistro.get(7).toString();
		String moneda 	= lRegistro.get(8).toString();
		String limitedl 	= lRegistro.get(9).toString();
		String acumdl 	= lRegistro.get(10).toString();
		String nombreMoneda = (moneda.equals("1") || moneda.equals("0"))?"MONEDA NACIONAL":"DOLARES";
		hash.put("IC_PYME",pyme);
		hash.put("CG_RAZON_SOCIAL",nomPyme);
		hash.put("MENSAJE",mensajeter);
		hash.put("FECHA",fecha);
		hash.put("ICUSUARIO",icusuario);
		hash.put("NOMBRE",nombre);
		hash.put("FG_LIMITE_PYME",limite);
		hash.put("FG_LIMITE_PYME_ACUM",acum);
		hash.put("FG_LIMITE_PYME_USD",limitedl);
		hash.put("FG_LIMITE_PYME_ACUM_USD",acumdl);
		NuevoReg.add(hash);
	}

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (NuevoReg != null){
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(NuevoReg);
		jsonObj.put("registros", jsObjArray);
	}

	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("Captura")){
	String fg_limite_pyme				=	(request.getParameter("fg_limite_pyme")==null)?"":request.getParameter("fg_limite_pyme");
	String fg_limite_pyme_dl			=	(request.getParameter("fg_limite_pyme_dl")==null)?"":request.getParameter("fg_limite_pyme_dl");
	String cg_mensaje_terminacion		=	(request.getParameter("cg_mensaje_terminacion")==null)?"":request.getParameter("cg_mensaje_terminacion");
	String fechaHoy						=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String horaActual						=	new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
	String pkcs7							=	request.getParameter("pkcs7");
	String externContent					=	request.getParameter("textoFirmado");
	String serial							=	"";
	String folioCert						=	"";
	char getReceipt						=	'Y';
	JSONObject jsonObj					=	new JSONObject();

	// AUTENTIFICACION
	Seguridad	s			=	new Seguridad();
	Acuse			acuse		=	null;
	String		_acuse	=	"";
	AccesoDB con			=	new AccesoDB();
	try{
		con.conexionDB();
		acuse	= new Acuse(Acuse.ACUSE_EPO,"4","com",con);
	}catch(Exception e){
		throw new AppException("Ocurrio un error durante la autentificación");
	}finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
        System.out.println("acuse:"+acuse);

	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		folioCert = acuse.toString();		
                System.out.println("folioCert:"+folioCert);
                if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
                    _acuse = s.getAcuse();	
                    System.out.println("_acuse:"+_acuse);
                    BeanParametros.setLimitePyme(ic_epo, ic_pyme, cg_num_distribuidor, fg_limite_pyme, cg_mensaje_terminacion, strNombreUsuario, strLogin, acuse.toString(), _acuse, fg_limite_pyme_dl);
                } else {
                    throw new NafinException("GRAL0021");
                }
	}
	String usuario = strLogin+" - "+strNombreUsuario;
	jsonObj.put("acuse", acuse);
	jsonObj.put("fechaHoy", fechaHoy);
	jsonObj.put("horaActual", horaActual);
	jsonObj.put("usuario", usuario);
	jsonObj.put("_acuse", _acuse);

	String nombreArchivo	= Comunes.cadenaAleatoria(16)+".pdf";
	ComunesPDF pdfDoc		= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual		= fechaActual.substring(0,2);
	String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual		= fechaActual.substring(6,10);
	String horaActPdf		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
	if(!"NAFIN".equals(strTipoUsuario)) {
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	}

	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActPdf,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);
	pdfDoc.setTable(2,50);
	pdfDoc.setCell("Recibo: "+_acuse,"celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de Acuse:","formas");
	pdfDoc.setCell(acuse.toString(),"formas");
	pdfDoc.setCell("Fecha:","formas");
	pdfDoc.setCell(fechaHoy,"formas");
	pdfDoc.setCell("Hora:","formas");
	pdfDoc.setCell(horaActual,"formas");
	pdfDoc.setCell("Usuario:","formas");
	pdfDoc.setCell(usuario,"formas");
	pdfDoc.addTable();
	pdfDoc.endDocument();
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	
	infoRegresar = jsonObj.toString();

}
%>
<%=infoRegresar%>
