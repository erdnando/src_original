<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		com.netro.parametrosgrales.*,
		netropology.utilerias.*,		
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	ParametrosRequest req = null;
	String itemArchivo	="",  rutaArchivo ="",   error_tam =""; 
	String PATH_FILE	=	strDirectorioTemp;
	String nombreArchivo= "";
	System.out.println("ServletFileUpload.isMultipartContent(request)=="+ServletFileUpload.isMultipartContent(request));
	boolean codificacionValida = true;
	String codificacionArchivo = "";
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
	
		req = new ParametrosRequest(upload.parseRequest(request));
			 
		FileItem fItem = (FileItem)req.getFiles().get(0);
		itemArchivo		= (String)fItem.getName();
		InputStream archivo = fItem.getInputStream();
		rutaArchivo = PATH_FILE+itemArchivo;
		int tamanio			= (int)fItem.getSize();
		
		nombreArchivo= Comunes.cadenaAleatoria(16)+ ".txt";
		String rutaArchivoTemporal = PATH_FILE + "/" + nombreArchivo;
		fItem.write(new File(rutaArchivoTemporal));
		
		ParametrosGrales paramGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
		boolean continuar = true;
		if(paramGrales.validaCodificacionArchivoHabilitado()){
			String codificacionDetectada[] = {""};
			CodificacionArchivo codificaArchivo = new CodificacionArchivo();
			codificaArchivo.setProcessTxt(true);
			codificaArchivo.setProcessZip(true);
			
			if(codificaArchivo.esCharsetNoSoportado(rutaArchivoTemporal,codificacionDetectada)){
				codificacionArchivo = codificacionDetectada[0];
				codificacionValida = false;
				continuar = false;
			}
		}
			
		
		if(continuar && tamanio>2097152){
			error_tam ="El Archivo es muy Grande, excede el L�mite que es de 2 MB.";
		}		
	}
	if(!error_tam.equals("")){
		nombreArchivo="";
	}
	%>
{
	"success": true,
	"archivo":	'<%=nombreArchivo%>',
	"error_tam":	'<%=error_tam%>',
	"codificacionValida": <%=(codificacionValida)?"true":"false"%>,
	"codificacionArchivo": '<%=codificacionArchivo%>'
}



