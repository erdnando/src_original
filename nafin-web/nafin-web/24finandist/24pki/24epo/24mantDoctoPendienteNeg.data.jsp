<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,java.math.*,
	java.sql.*,
	netropology.utilerias.*,netropology.utilerias.usuarios.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="/24finandist/24pki/certificado.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
	int start= 0, limit =0;   
	String infoRegresar ="";
	String auxPerfil = strPerfil;
	JSONObject 	resultado	= new JSONObject();
try{
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	
	com.netro.distribuidores.ConsMantDoctoPendienteNeg paginador = new com.netro.distribuidores.ConsMantDoctoPendienteNeg();
	JSONObject jsonObj = new JSONObject();
	paginador.setPerfil(strPerfil);
	Vector nombresCampo = new Vector(5);
	String campo_adicional_1="", campo_adicional_2 ="",campo_adicional_3 ="",campo_adicional_4 ="", campo_adicional_5 ="";	
	nombresCampo = cargaDocto.getCamposAdicionales(iNoCliente,false);
	int hayCamposAdicionales = nombresCampo.size();
	int numeroCampos=  nombresCampo.size();
	if(hayCamposAdicionales>0){
		for(int i = 0; i < hayCamposAdicionales; i++){
			List campos =(List)nombresCampo.get(i);
			if(i==0) 	campo_adicional_1= campos.get(0).toString();
			if(i==1) 	campo_adicional_2= campos.get(0).toString();
			if(i==2) 	campo_adicional_3= campos.get(0).toString();
			if(i==3) 	campo_adicional_4= campos.get(0).toString();
			if(i==4) 	campo_adicional_5= campos.get(0).toString();			
		}
	}
	String parTipoCredito = cargaDocto.getTipoCreditoMantPendNeg(iNoCliente);
	
	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	String  meseSinIntereses = BeanParametros.DesAutomaticoEpo(iNoCliente,"CS_MESES_SIN_INTERESES"); 
	
	
	if (informacion.equals("CatalogoIF")  ) {	
		JSONArray registrosCat = new JSONArray();	
		HashMap info = new HashMap();
		Vector vecFilas = cargaDocto.getLineas(iNoCliente);  //ya existe
		info.put("clave", "0");
		info.put("descripcion", "Seleccione un IF");
		registrosCat.add(info);
		for(int i=0; i<vecFilas.size(); i++){
			Vector	vecColumnas = (Vector)vecFilas.get(i);
			String clave = vecColumnas.get(0).toString(); 			
			String descripcion = vecColumnas.get(1).toString(); 		
			info = new HashMap();			
			info.put("clave", clave);
			info.put("descripcion", descripcion);	
			registrosCat.add(info);
		}
		infoRegresar =  "{\"success\": true, \"total\": \"" + registrosCat.size() + "\", \"registros\": " + registrosCat.toString()+"}";
	} else  if (informacion.equals("Consultar") ||  informacion.equals("ResumenTotales") ||  informacion.equals("Individual") ||  informacion.equals("ArchivoPDF")  ) {
		
		String numAcuse = (request.getParameter("cc_acuse") != null) ? request.getParameter("cc_acuse") : "";
		String fechaCargaIni = (request.getParameter("fechaCargaIni") != null) ? request.getParameter("fechaCargaIni") : "";
		String fechaCargaFin = (request.getParameter("fechaCargaFin") != null) ? request.getParameter("fechaCargaFin") : "";
		String numUsuario = (request.getParameter("ic_usuario") != null) ? request.getParameter("ic_usuario") : "";
		String tipoConsulta = (request.getParameter("tipoConsulta") != null) ? request.getParameter("tipoConsulta") : "";
		HashMap datos = new HashMap();
		JSONArray registros = new JSONArray();
		HashMap registrosTot = new HashMap();	
		JSONArray registrosTotales = new JSONArray();
		double	montoTotalMN		=	0;
		double	montoTotalDL		=	0;
		double	montoTotalValMN		=	0;
		double	montoTotalValDL		=	0;
		double	montoTotalDescMN	=	0;
		double	montoTotalDescDL	=	0;
		int  docMN=0, docDL=0;
		
		UtilUsr utilUsr = new UtilUsr();
		String consulta  ="";
		String numDoctos = "";
		
		paginador.setTipoConsulta(tipoConsulta);
		paginador.setNumAcuse(numAcuse);
		paginador.setIc_epo(iNoCliente);
		paginador.setFechaCargaIni(fechaCargaIni);
		paginador.setFechaCargaFin(fechaCargaFin);
		paginador.setNumUsuario(numUsuario);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		if (informacion.equals("Consultar") ) {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			try {
				if(operacion.equals("Generar")){
					queryHelper.executePKQuery(request);
				}
				Registros registros1 = queryHelper.getPageResultSet(request,start,limit);
				while(registros1.next()){
					int doctosMN = 0;
					int doctosDL = 0;
					if(!registros1.getString("IN_TOTAL_DOCTO_MN").equals(null)){//in_total_docto_dl
						doctosMN = registros1.getString("IN_TOTAL_DOCTO_MN")!=null?Integer.parseInt(registros1.getString("IN_TOTAL_DOCTO_MN").toString()):doctosMN;
					}
					if(!registros1.getString("IN_TOTAL_DOCTO_DL").equals(null)){//in_total_docto_mn
						doctosDL = registros1.getString("IN_TOTAL_DOCTO_DL")!=null?Integer.parseInt(registros1.getString("IN_TOTAL_DOCTO_DL").toString()):doctosDL;
					}
					String sUsuario = registros1.getString("IC_USUARIO").toString();
					Usuario usuario = utilUsr.getUsuario(sUsuario);
					strNombreUsuario = usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
					numDoctos	= String.valueOf(doctosMN+doctosDL);
					registros1.setObject("IC_USUARIO",sUsuario+" "+strNombreUsuario);
					registros1.setObject("IG_NUMERO_DOCTO",numDoctos);
				}
				consulta	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros1.getJSONData()+"}";
				resultado = JSONObject.fromObject(consulta);
				resultado.put("meseSinIntereses", meseSinIntereses);
				infoRegresar = resultado.toString();
				} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
				}
	} else if(informacion.equals("ArchivoPDF")){
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			JSONObject jsonObj3 = new JSONObject();
			jsonObj3.put("success", new Boolean(true));
			jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj3.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}	
	}else if (informacion.equals("Individual") ) {	
		String cargaEPO = cargaDocto.lineaCreditoEpo(iNoCliente,numAcuse);   //
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if(operacion.equals("Generar")){
				queryHelper.executePKQuery(request);
			}
			Registros registros1 = queryHelper.getPageResultSet(request,start,limit);
			consulta	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros1.getJSONData()+"}";
			resultado = JSONObject.fromObject(consulta);
			resultado.put("tipoCredito",parTipoCredito );
			resultado.put("cargaEPO",cargaEPO );
			resultado.put("indiceCamposAdicionales",String.valueOf(hayCamposAdicionales));	 //numero de campos Adicionales
			resultado.put("CAMPO_ADICIONAL_1",campo_adicional_1);
			resultado.put("CAMPO_ADICIONAL_2",campo_adicional_2);
			resultado.put("CAMPO_ADICIONAL_3",campo_adicional_3);
			resultado.put("CAMPO_ADICIONAL_4",campo_adicional_4);
			resultado.put("CAMPO_ADICIONAL_5",campo_adicional_5);
			resultado.put("meseSinIntereses", meseSinIntereses);
			infoRegresar = resultado.toString();
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
	}
	
	}else if(	informacion.equals("detalleConfirma")	){

	String num_acuse = (request.getParameter("num_acuse") == null)?"":request.getParameter("num_acuse");
	String cadDocSelec = (request.getParameter("cadDocSelec") == null)?"":request.getParameter("cadDocSelec");
	String strPreNegociable = request.getParameter("strPreNegociable");
	StringBuffer seguridad	=	new StringBuffer("");
	StringBuffer textoF_a	=	new StringBuffer("");
	StringBuffer textoF_b	=	new StringBuffer("");
	String cargaEPO = cargaDocto.lineaCreditoEpo(iNoCliente,num_acuse);
	List regsNego = new ArrayList();
	List regstTotales = new ArrayList();
	int regP=0;
	int regPN=0;
	String icEstatusP="";
	boolean bOperaFactorajeVencido = false,	bOperaFactorajeDistribuido = false;
	if(auxPerfil.equals("EPO MAN1 DISTR")){
			icEstatusP = "30";
			
	}else if(auxPerfil.equals("EPO MAN2 DISTR")){
			icEstatusP = "28";
	} 
	Vector vDoctosCargados = cargaDocto.mostrarDocumentosPendientes(iNoCliente,icEstatusP, num_acuse, cadDocSelec);
	String sNumDistribuidor = "", sRfc="", sPymeRazonSoc="", sNumeroDocto="", sFechaEmision="", sFechaVenc="", sPlazoDocto="", sMoneda = "";
	String sPlazoDesc="", sCdNombre="",	sIcMoneda="", sFnMonto="", sCgCategoria="", sFnPorcDesc="", sMontoDesc="";
	String sModoPlazo="", sCdFechaVencCred="", sCdReferencia="" , sEstatus="",sCgCampo1="",sCgCampo2="",sCgCampo3="",sCgCampo4="",sCgCampo5="";
	String sIgPlazoCred = "", sDfFechaVenCred = "" ;
	BigDecimal bdTotalDol=new BigDecimal("0.0"); 	
	BigDecimal bdTotalMN=new BigDecimal("0.0");	
	BigDecimal bdTotalDolNeg=new BigDecimal("0.0");
	BigDecimal bdTotalMNNeg=new BigDecimal("0.0");
	BigDecimal bdTotalDolNoNeg=new BigDecimal("0.0"); BigDecimal bdTotalDescDolNoNeg=new BigDecimal("0.0");
	BigDecimal bdTotalMNNoNeg=new BigDecimal("0.0"); BigDecimal bdTotalDescMNNoNeg=new BigDecimal("0.0");
	int iNumMoneda=0, iNoTotalDtosDol=0, iNoTotalDtosMN=0;
	for(int i=0; i<vDoctosCargados.size(); i++) {
		Vector vd 		= (Vector)vDoctosCargados.get(i);
		HashMap hash = new HashMap();
		sNumDistribuidor = vd.get(0).toString();
		sPymeRazonSoc 	= vd.get(1).toString();
		sNumeroDocto 	= vd.get(2).toString();
		sFechaEmision 	= vd.get(4).toString();
		sFechaVenc 	= vd.get(5).toString();
		sPlazoDocto 	= vd.get(12).toString();
		sMoneda 	= vd.get(13).toString();
		sFnMonto 	= vd.get(14).toString();
		sCgCategoria 	= vd.get(16).toString();
		sPlazoDesc 	= vd.get(18).toString();
		sFnPorcDesc 	= vd.get(20).toString();
		sMontoDesc 	= vd.get(25).toString();
		sModoPlazo 	= vd.get(23).toString();
		sCdReferencia 	= vd.get(3).toString();
		sCgCampo1 	= vd.get(6).toString();
		sCgCampo2 	= vd.get(7).toString();
		sCgCampo3 	= vd.get(8).toString();
		sCgCampo4 	= vd.get(9).toString();
		sCgCampo5 	= vd.get(10).toString();
		sIgPlazoCred 	= vd.get(19).toString();
		sDfFechaVenCred 	= vd.get(26).toString();
		sEstatus 	= vd.get(28).toString();
		sIcMoneda = vd.get(15).toString();
		
		hash.put("CG_NUM_DISTRIBUIDOR", vd.get(0).toString());
		hash.put("NOMBRE_DIST", vd.get(1).toString());
		hash.put("IG_NUMERO_DOCTO", vd.get(2).toString());
		hash.put("CT_REFERENCIA", vd.get(3).toString());
		hash.put("DF_FECHA_EMISION", vd.get(4).toString());
		hash.put("DF_FECHA_VENC", vd.get(5).toString());
		hash.put("CG_CAMPO1", vd.get(6).toString());
		hash.put("CG_CAMPO2", vd.get(7).toString());
		hash.put("CG_CAMPO3", vd.get(8).toString());
		hash.put("CG_CAMPO4", vd.get(9).toString());
		hash.put("CG_CAMPO5", vd.get(10).toString());
		hash.put("CG_TIPO_CREDITO", vd.get(11).toString());
		hash.put("IG_PLAZO_DOCTO", vd.get(12).toString());
		hash.put("MONEDA", vd.get(13).toString());
		hash.put("FN_MONTO", vd.get(14).toString());
		hash.put("IC_MONEDA", vd.get(15).toString());
		hash.put("CATEGORIA", vd.get(16).toString());
		hash.put("IC_CATEGORIA", vd.get(17).toString());
		hash.put("IG_PLAZO_DESCUENTO", vd.get(18).toString());
		hash.put("IG_PLAZO_CREDITO", vd.get(19).toString());
		hash.put("FN_PORC_DESCUENTO", vd.get(20).toString());
		hash.put("ACUSE", vd.get(21).toString());
		hash.put("IC_ESTATUS", vd.get(22).toString());
		hash.put("MODO_PLAZO", vd.get(23).toString());
		hash.put("TIPO_CREDITO", vd.get(24).toString());
		hash.put("MONTO_DESCONTAR", vd.get(25).toString());
		hash.put("DF_FECHA_VENC_CREDITO", vd.get(26).toString());
		hash.put("CG_RFC", vd.get(27).toString());
		hash.put("ESTATUS", vd.get(28).toString());
		hash.put("TIPOPAGO", vd.get(29).toString()); 
		
		regsNego.add(hash);
		textoF_b.append("Número de Distribuidor|Distribuidor|Número de Documento Inicial|Fecha de Emisión|Fecha de Vencimiento|Plazo|Moneda|Monto|Categoría|Plazo de Descuento en días|% de Descuento|Monto % Descuento|Modalidad de Plazo|Comentarios|Campos Adicionales|Plazo|Fecha de Vencimiento|Estatus|");
		textoF_b.append("\n");
		textoF_b.append(sNumDistribuidor.replace('"',' ')+"|"+sPymeRazonSoc+"|"+sNumeroDocto+"|"+sFechaEmision+"|"+sFechaVenc+"|");
		textoF_b.append(sPlazoDocto+"|"+sMoneda+"|"+sFnMonto+"%"+"|"+sCgCategoria+"|"+sPlazoDesc+"|"+sFnPorcDesc+"|"+sMontoDesc+"|"+sModoPlazo+"|"+sCdReferencia+"|"+sCgCampo1+"|"+sCgCampo2+"|"+sCgCampo3+"|"+sCgCampo4+"|"+sCgCampo5+"|"+sIgPlazoCred+"|"+sDfFechaVenCred+"|"+sEstatus+"|");
		textoF_b.append("\n");
		if(sFnMonto.equals("")){sFnMonto = "0";}
		if(sMontoDesc.equals("")){sMontoDesc = "0";}
		if (sIcMoneda.equals("54")) {
			bdTotalDol = bdTotalDol.add(new BigDecimal(sFnMonto));
			bdTotalDolNeg = bdTotalDolNeg.add(new BigDecimal(sFnMonto));
		} else if (sIcMoneda.equals("1")) {
			bdTotalMN = bdTotalMN.add(new BigDecimal(sFnMonto));
			bdTotalMNNeg = bdTotalMNNeg.add(new BigDecimal(sFnMonto));
			iNoTotalDtosMN++;
		}
		regP++;
	}//Fin-For
	HashMap total = new HashMap();
	total.put("MONEDA","MONEDA NACIONAL");
	total.put("TOTAL_DOC",Integer.toString(iNoTotalDtosMN));
	total.put("TOTAL_MONTO",bdTotalMN.toPlainString());
	regstTotales.add(total);
	HashMap total1 = new HashMap();
	total1.put("MONEDA","MONEDA USD");
	total1.put("TOTAL_DOC",Integer.toString(iNoTotalDtosDol));
	total1.put("TOTAL_MONTO",bdTotalDol.toPlainString());
	regstTotales.add(total1);
	if (regsNego != null){
		JSONArray jaNego = new JSONArray();
		jaNego = JSONArray.fromObject(regsNego);
		jsonObj.put("regsNego", jaNego);
	}
	if (regstTotales != null){
		JSONArray datosTotales = new JSONArray();
		datosTotales = JSONArray.fromObject(regstTotales);
		jsonObj.put("datosTotales", datosTotales);
	}

	jsonObj.put("bdTotalMN", bdTotalMN.toPlainString());
	jsonObj.put("bdTotalDol", bdTotalDol.toPlainString());

	jsonObj.put("bdTotalMNNeg", bdTotalMNNeg.toPlainString());
	jsonObj.put("bdTotalDolNeg", bdTotalDolNeg.toPlainString());
	jsonObj.put("iNoTotalDtosMN", Integer.toString(iNoTotalDtosMN));
	jsonObj.put("iNoTotalDtosDol", Integer.toString(iNoTotalDtosDol));
	String txtLegalNC = "Al transmitir este MENSAJE DE DATOS, usted está bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesión de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisión tendrá validez para todos los efectos legales. En caso de transmitir NOTAS DE CREDITO, usted está en el entendido de que aplicarán a los DOCUMENTOS una vez que a la MIPYME que correspondan decida efectuar el descuento.";
	String txtLegal = "Al transmitir este MENSAJE DE DATOS, usted está bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesión de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisión tendrá validez para todos los efectos legales. En caso de transmitir NOTAS DE CRÉDITO, usted está en el entendido de que aplicarán a los DOCUMENTOS una vez que a la MIPYME que correspondan decida efectuar el descuento.";
	textoF_a.append(
				"Moneda Nacional\tNo. total de documentos Pendientes Negociables cargados\t"+iNoTotalDtosMN+" \t" +
				"Dolares\t\tNo. total de documentos Pendientes Negociables cargados\t"+iNoTotalDtosDol+" \t " +
				"Moneda Nacional\tMonto total de los documentos Pendientes Negociables cargados\t"+bdTotalMNNeg.toPlainString()+" \t" +
				"Dolares\t\tMonto total de los documentos Pendientes Negociables cargados\t"+bdTotalDolNeg.toPlainString()+" \n" +
				txtLegal+"|\n"	);
	seguridad.append(textoF_a.toString() + textoF_b.toString());
	jsonObj.put("txtFirma", seguridad.toString());
	jsonObj.put("tipoCredito",parTipoCredito );
	jsonObj.put("cargaEPO",cargaEPO );
	jsonObj.put("indiceCamposAdicionales",String.valueOf(hayCamposAdicionales));	 //numero de campos Adicionales
	jsonObj.put("CAMPO_ADICIONAL_1",campo_adicional_1);
	jsonObj.put("CAMPO_ADICIONAL_2",campo_adicional_2);
	jsonObj.put("CAMPO_ADICIONAL_3",campo_adicional_3);
	jsonObj.put("CAMPO_ADICIONAL_4",campo_adicional_4);
	jsonObj.put("CAMPO_ADICIONAL_5",campo_adicional_5);
	jsonObj.put("meseSinIntereses", meseSinIntereses);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("Confirma")){
	String num_acuse		=	(request.getParameter("num_acuse") == null)?"":request.getParameter("num_acuse");
	String cadDocSelec	=	(request.getParameter("cadDocSelec") == null)?"":request.getParameter("cadDocSelec");
	String regPreneg		=	(request.getParameter("regPrenegociables")==null)?"":request.getParameter("regPrenegociables");
	String usuario = iNoUsuario+" "+strNombre;
	boolean errorUsuario = true;
	String	pkcs7	= (request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
	String 	serial = "";
	String 	folio= "";  
	String 	externContent = (request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
	char 	getReceipt = 'Y';
	String recibo_e = "500";
	Acuse no_acuse=new Acuse(Acuse.ACUSE_EPO,"1");
	String acuse = no_acuse.formatear();
	String selecIF = (request.getParameter("selecIF") == null)?"":request.getParameter("selecIF");
	String[] ic_documento = request.getParameterValues("ic_documento");
	String icEstatusP = "";
	if(auxPerfil.equals("EPO MAN1 DISTR")){
			icEstatusP = "30";
			
	}else if(auxPerfil.equals("EPO MAN2 DISTR")){    
			icEstatusP = "28";
	} 
	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {   
		folio = no_acuse.toString();   
		Seguridad s = new Seguridad();   
		if (s.autenticar(folio, _serial, pkcs7, externContent, getReceipt)) {
			recibo_e = s.getAcuse();	
			if("Eliminar".equals(operacion)){
				cargaDocto.eliminaPendientes(iNoCliente, num_acuse,icEstatusP);
				jsonObj.put("success", new Boolean(true));
			}
			if("Generar".equals(operacion)) {
				boolean bOkActualiza = true;
				try {
					if(Integer.parseInt(regPreneg)>0) {
						bOkActualiza = cargaDocto.bactualizaEstatusPendiente(iNoCliente, num_acuse, icEstatusP,auxPerfil ,cadDocSelec);
						if(!selecIF.equals("")&&ic_documento.length!=0){
							cargaDocto.actualizaLineaCredito(selecIF,ic_documento,num_acuse,iNoCliente);
						}
						
					}
					jsonObj.put("success", new Boolean(true));
				} catch(Exception e) {
					bOkActualiza = false;
					String _error = s.mostrarError();
					jsonObj.put("success", new Boolean(false));
					jsonObj.put("msg", "Error al actualizar. " + _error + ".<br>Proceso CANCELADO");
				}
			}//fin-generar		 
		}  //if (La autenticacion)
		else { //autenticación fallida 
			String _error = s.mostrarError();
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
		}
		jsonObj.put("operacion", operacion);
	}
	jsonObj.put("meseSinIntereses", meseSinIntereses);
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("MonitorLineasCredito")){	
	HashMap datos = new HashMap();
	JSONArray registrosT = new JSONArray();		
	int		numLineasMN		= 0;
	int		numLineasUSD	= 0;
	boolean	seleccionarIF	= true; 
	Vector vecFilas = cargaDocto.monitorLineas(iNoCliente);
	int hidNumLineas1 =vecFilas.size();
	for(int i=0;i<vecFilas.size();i++){
		
		Vector 	vecColumnas = (Vector)vecFilas.get(i);
		String ic_linea = (String)vecColumnas.get(0);
		String moneda_linea = (String)vecColumnas.get(4);
		String linea_credito = (String)vecColumnas.get(1);
		String saldo_inicial = (String)vecColumnas.get(2);  
		String saldo_financiado = (String)vecColumnas.get(5);  
		String monto_proceso = (String)vecColumnas.get(6);  
		String saldo_disponible_linea = (String)vecColumnas.get(7);  
		String monto_negociable = (String)vecColumnas.get(8);  	
		String montoSeleccionado ="0";
		String numDoctos ="0"; 
		String saldoDisponible = vecColumnas.get(9).toString();
	 
		datos = new HashMap();
		datos.put("IC_LINEA", ic_linea);			
		datos.put("MONEDA_LINEA", moneda_linea);	
		datos.put("LINEA_CREDITO", linea_credito);
		datos.put("SALDO_INICIAL", saldo_inicial);			
		datos.put("SALDO_FINANCIADO",saldo_financiado);			
		datos.put("MONTO_PROCESO", monto_proceso);			
		datos.put("SALDO_DISPONIBLE_LINEA", saldo_disponible_linea);			
		datos.put("MONTO_NEGOCIABLE",monto_negociable);			
		datos.put("MONTO_SELECCIONADO", montoSeleccionado);			
		datos.put("NUM_DOCTOS", numDoctos);			
		datos.put("SALDO_DISPONIBLE", saldoDisponible);			
		registrosT.add(datos);
			
	}

	String consulta =  "{\"success\": true, \"total\": \"" + registrosT.size() + "\", \"registros\": " + registrosT.toString()+"}";
	resultado = JSONObject.fromObject(consulta);
	resultado.put("meseSinIntereses", meseSinIntereses);
	infoRegresar = resultado.toString();
/************************************************************************************/
// Confirmar Detalle de las Lineas de Credito
/***********************************************************************************/
}
}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	out.println("Error: "+ e);
}

%>
<%=infoRegresar%>



