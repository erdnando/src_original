
Ext.onReady(function() {
	
	//Variables globales
	var ventaCartera  = Ext.getDom('ventaCartera').value;
	var validaLinea  = Ext.getDom('validaLinea').value;
	var lineaCredito = Ext.getDom('lineaCredito').value;
	var descuentoAutomatico = Ext.getDom('descuentoAutomatico').value;
	var limiteLineaCredito = Ext.getDom('limiteLineaCredito').value;
	var tipoCredito = Ext.getDom('tipoCredito').value;
	var NOnegociable = Ext.getDom('NOnegociable').value;	
	var hidFechaActual = Ext.getDom('hidFechaActual').value; //fecha hoy
	var hid_fecha_porc_desc = Ext.getDom('hid_fecha_porc_desc').value;
	var txttotdoc = Ext.getDom('txttotdoc').value;
	var txtmtodoc = Ext.getDom('txtmtodoc').value;
	var txtmtomindoc = Ext.getDom('txtmtomindoc').value;
	var txtmtomaxdoc = Ext.getDom('txtmtomaxdoc').value;
	var txttotdocdo = Ext.getDom('txttotdocdo').value;
	var txtmtodocdo = Ext.getDom('txtmtodocdo').value;
	var txtmtomindocdo = Ext.getDom('txtmtomindocdo').value;
	var txtmtomaxdocdo = Ext.getDom('txtmtomaxdocdo').value;
	var hidCifrasControl = Ext.getDom('hidCifrasControl').value;
	var campos = Ext.getDom('campos').value;
        var operaContrato = Ext.getDom('operaContrato').value;
	var proceso  = Ext.getDom('proceso').value;
	var accionDetalle = Ext.getDom('accionDetalle').value;
	var mensajeEPO =  Ext.getDom('mensajeEPO').value;	
	var tipoCarga = Ext.getDom('tipoCarga').value;
	var producto = Ext.getDom('producto').value;
	
	var fechaLimite; 	
	var hidID;
	var numElementos =0;
	var lineaReg	= [];
	var plazo = [];
	var fecha_vto	= [];
	var igNumeroDocto = [];
	var tamanio =150;
	
		 
	var procesarDetalleLineas = function(store, arrRegistros, opts) 	{
		var parametros = "pantalla=Detalle"+"&proceso="+proceso+"&tipoCarga="+tipoCarga;					
		document.location.href = "24forma01DetalleExt.jsp?"+parametros;		
	}
	
	var procesarCancelar = function(store, arrRegistros, opts) 	{
		var parametros;
		if(tipoCarga=='I') { 		parametros= "24forma01Ext.jsp";			 	}
		if(tipoCarga=='M' && producto =='FactoRecur') { 	parametros= "24forma02FExt.jsp";			}
		if(tipoCarga=='M' && producto =='') { 		parametros= "24forma02Ext.jsp";		}		
		document.location.href = parametros+"?&txttotdoc=0&txtmtodoc=0&txtmtomindoc=0&txtmtomaxdoc=0&txttotdocdo=0&txtmtodocdo=0&txtmtomindocdo=0&txtmtomaxdocdo=0";
		
		}
	

	var procesarSuccessFailureAcuse =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){
				if(jsondeAcuse.acuse!='') {
					var acuse  = jsondeAcuse.acuse;
					var fecha = jsondeAcuse.fecha;
					var hora  = jsondeAcuse.hora;
					var usuario = jsondeAcuse.usuario;
					var mensajeEPO = jsondeAcuse.mensajeEPO;
					var proceso = jsondeAcuse.proceso;
				
					var parametros = "pantalla=Acuse"+"&proceso="+proceso+"&acuse="+	acuse+"&fecha="+fecha+"&hora="+hora+"&usuario="+usuario+"&mensajeEPO="+mensajeEPO+"&tipoCarga="+tipoCarga+"&producto="+producto; 
					document.location.href = "24forma01AcuseExt.jsp?"+parametros;	
		
				} else 	if(jsondeAcuse.acuse=='') {
					Ext.MessageBox.alert("Mensaje",jsondeAcuse.mensajeError); 				
					gridCaptura.hide();
					gridDetallesTotales.hide();
					fpPreAcuse.hide();
				}
				
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var confirmarAcuse = function(pkcs7, textoFirmar, vproceso, totalDocMN , totalMontoMN , totalDocUSD , totalMontoUSD   ){
	
		
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnConfirmaAcuse").enable();	
			return;	//Error en la firma. Termina...
		}else  {
			Ext.getCmp("btnConfirmaAcuse").disable();	
			Ext.Ajax.request({
				url : '24forma01Ext.data.jsp',
				params : {
					pkcs7: pkcs7,
					textoFirmado: textoFirmar,
					informacion: 'ConfirmaCarga',
					proceso:vproceso,
					totalDocMN :totalDocMN,
					totalMontoMN :totalMontoMN,
					totalDocUSD :totalDocUSD,
					totalMontoUSD :totalMontoUSD
				},
				callback: procesarSuccessFailureAcuse
			});	
		}
	
	
	}
	
	
	
	var procesarConfirmaAcuse =  function(opts, success, response) {
		var  gridDetallesTotales = Ext.getCmp('gridDetallesTotales');
		
		var totalDocDM= 0;
		var totalDocCC=0;
		var  montoTotalDM =0;
		var  montoTotalCC =0;
		var totalDocDMUSD=0;
		var totalDocCCUSD =0
		var montoTotalDMUSD =0;
		var montoTotalCCUSD =0;
		
		var totalDocMN =0;
		var totalMontoMN=0;
		var totalDocUSD=0;
		var totalMontoUSD =0;
		
		var store = gridDetallesTotales.getStore();
			store.each(function(record) {
				totalDocDM = record.data['TOTALDOCDM'];
				totalDocCC = record.data['TOTALDOCC'];
				montoTotalDM  = record.data['MONTOTOTALDM'];
				montoTotalCC = record.data['MONTOTOTALCC'];
				totalDocDMUSD = record.data['TOTALDOCDMUSD'];
				totalDocCCUSD = record.data['TOTALDOCCUSD'];
				montoTotalDMUSD= record.data['MONTOTOTALDMUSD'];
				montoTotalCCUSD = record.data['MONTOTOTALCCUSD'];
				
		});
		
		totalDocMN = parseFloat(totalDocDM) + parseFloat(totalDocCC);
		totalMontoMN = parseFloat(montoTotalDM) + parseFloat(montoTotalCC);
		totalDocUSD = parseFloat(totalDocDMUSD) + parseFloat(totalDocCCUSD);
		 totalMontoUSD =parseFloat(montoTotalDMUSD) + parseFloat(montoTotalCCUSD);
			
		if(totalDocMN ==0  &&  totalMontoMN==0 && totalDocUSD==0 &&  totalMontoUSD ==0 )  {
			var  gridCaptura = Ext.getCmp('gridCaptura');
			var store = gridCaptura.getStore();
				store.each(function(record) {							
					if( record.data['IC_MONEDA']==1 )  {
						totalDocMN++;
						totalMontoMN += parseFloat(record.data['MONTO']);  ;				
					}else if( record.data['IC_MONEDA']==54 )  {
						totalDocUSD++;
						totalMontoUSD += parseFloat(record.data['MONTO']);  ;				
					}								
			});		
		}
				
		var textoFirmar = "Moneda nacional "+
				" \n No. total de documentos cargados "+totalDocMN+
				" \n Monto total de los documentos cargados "+ parseFloat(totalMontoMN)  +
				" \n D�lares \n"+" \n No. total de documentos cargados "+totalDocUSD+
				" \n Monto total de los documentos cargados "+parseFloat(totalMontoUSD) +"\n";
                if(operaContrato == 'S'){
                    textoFirmar = textoFirmar + "Al transmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, est� publicando el(los) DOCUMENTO(S) INICIAL(ES) que se detallan en esta pantalla y que pueden ser objeto de financiamiento, \n"+
                             "otorgando su consentimiento para que en caso de que se den las condiciones se�aladas en el Contrato de Financiamiento a Clientes y Distribuidores, el (los) DOCUMENTO(S) INICIAL(ES) ser�n sustituidos por el (los) DOCUMENTOS(S) FINAL(ES) que deber�n \m"+
                             "ser cobrados por el INTERMEDIARIO FINANCIERO.\n";
		}else{
                    textoFirmar = textoFirmar + " Para todos los efectos legales al transmitir este 	MENSAJE DE DATOS, me obligo a no modificar ni cancelar o restringir el (los) DOCUMENTO(S) que aqui se menciona(n) cuando este(os) haya(n) sido seleccionado(s) por la MIPYME. \n";
		}
		
		
		NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar, proceso, totalDocMN , totalMontoMN , totalDocUSD , totalMontoUSD   );
				
		
	}
	
	
	var procesarConsultaDetalle = function(store, arrRegistros, opts) 	{
								
		if (arrRegistros != null) {
			if (!gridDetallesTotales.isVisible()) {
				gridDetallesTotales.show();
			}		
			var jsonData = store.reader.jsonData;
			var tipoCambio = jsonData.tipoCambio;
			var tipo_Credito = jsonData.tipo_Credito;
			var obtieneTipoCredito =  jsonData.obtieneTipoCredito;
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridDetallesTotales.getColumnModel();	
						
			var btnCancelarP = Ext.getCmp('btnCancelarP');	
			var btnConfirmaAcuse = Ext.getCmp('btnConfirmaAcuse');
			var btnConfirmaAcuse = Ext.getCmp('btnConfirmaAcuse');	
			
			var btnDetalle = Ext.getCmp('btnDetalle');
			var btnDetalleV1 = Ext.getCmp('btnDetalleV1');
			
			var btnDetalleCC = Ext.getCmp('btnDetalleCC');
			var btnDetalleDOC = Ext.getCmp('btnDetalleDOC');
			
			
				if(tipoCambio==''){
						gridDetallesTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_DOCTOS_MNDL'), true);	
						gridDetallesTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_TOTAL_MNDL'), true);	
						gridDetallesTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_CREDITO_MNDL'), true);	
				}		
		
		
			btnCancelarP.show();
			//para habilitar los botones  de  Confirmar Detalle
			if(jsonData.botDetalleVenta =='SI' ){ 	btnDetalleV1.show();   	}
			if(jsonData.botDetalleFacto =='SI' ){ 	btnDetalle.show();   	}
			if(jsonData.botCCC =='SI' ){ 	btnDetalleCC.show();   	}
			if(jsonData.botDocto =='SI' ){ 	btnDetalleDOC.show();   	}
			//para los botones de confirmar		
								
			if(jsonData.habConfirma =='SI' ){ 	btnConfirmaAcuse.show();   	}
					
				
			var el = gridDetallesTotales.getGridEl();		
			if(store.getTotalCount() > 0) {
				el.unmask();	
			} else {								
				el.mask('Para ver los totales favor de capturar detalle de Descuento y/o Factoraje', 'x-mask');				
			}			
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		if (arrRegistros != null) {
			if (!gridCaptura.isVisible()) {
				gridCaptura.show();
			}		
		
			var jsonData = store.reader.jsonData;		
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  campo1 = jsonData.Campo0;
			var  campo2 = jsonData.Campo1;
			var  campo3 = jsonData.Campo2;
			var  campo4 = jsonData.Campo3;
			var  campo5 = jsonData.Campo4;
			var operaConversion = jsonData.operaConversion;
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridCaptura.getColumnModel();
				
			if(operaConversion=='false'){
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);	
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);	
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('MONTO_VALUADO'), true);								
			}
							
			if(campo1 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),campo1);
			}
			if(campo2 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),campo2);
			}
			if(campo3 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),campo3);
			}
			if(campo4 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),campo4);
			}
			if(campo5 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),campo5);
			}
		
			gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('PLAZO'), false);
			gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VEN'), false);
			
			
			if(hayCamposAdicionales=='0'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='1'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='2'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);			
			}else  	if(hayCamposAdicionales=='3'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='4'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='5'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);	
			}
				
			if(jsonData.strPerfil!='ADMIN EPO' && jsonData.strPerfil!='ADMIN EPO GRUPO' &&  jsonData.strPerfil!='ADMIN EPO FON'  ){
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('NEGOCIABLE'), true);
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('ESTATUS'), false);
			}else {
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('NEGOCIABLE'), false);
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('ESTATUS'), true);
			}
			
			//F09-2015
			if(jsonData.meseSinIntereses=='S' && jsonData.obtieneTipoCredito=='C'){
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), false);			
			}else  {
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), true);		 
			}
			
			var el = gridCaptura.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
		//para los totales del grid Detalles 
	var totalesDetallesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotalesDetalle'	
		},								
		fields: [			
			{name: 'TIPO_DOCUMENTO'},
			{name: 'TOTALDOCDM'},
			{name: 'MONTOTOTALDM'},
			{name: 'MONTOTMN'},
			{name: 'TOTALDOCDMUSD'},
			{name: 'MONTOTOTALDMUSD'},
			{name: 'MONTOTDL'},			
			{name: 'TOTALDOCCONVDM'},
			{name: 'MONTODOCTO'},
			{name: 'MONTOCREDITO'},			
			{name: 'TOTALDOCC'},
			{name: 'MONTOTOTALCC'},
			{name: 'MONTOMNCC'},
			{name: 'TOTALDOCCUSD'},
			{name: 'MONTOTOTALCCUSD'},
			{name: 'MONTODLCC'},			
			{name: 'TOTALDOCCONVCC'},
			{name: 'MONTODOCTOC'},
			{name: 'MONTOCREDITOC'},	
			//
			{name: 'TOTAL_DOCTOS_MN'},
			{name: 'MONTO_TOTAL_MN'},
			{name: 'MONTO_CREDITO_MN'},			
			{name: 'TOTAL_DOCTOS_DL'},
			{name: 'MONTO_TOTAL_DL'},
			{name: 'MONTO_CREDITO_DL'},			
			{name: 'TOTAL_DOCTOS_MNDL'},
			{name: 'MONTO_TOTAL_MNDL'},
			{name: 'MONTO_CREDITO_MNDL'}						
		],
		totalProperty : 'total',
		autoLoad: false,	
		listeners: {
			load: procesarConsultaDetalle,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConsultaDetalle(null, null, null);						
				}
			}
		}			
	});
	
	//esto esta en duda como manejarlo
	var gruposM = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 1, align: 'center'},
				{header: 'Moneda nacional', colspan: 3, align: 'center'},	
				{header: 'D�lares', colspan: 3, align: 'center'},
				{header: 'Doctos en DLL financiados en M.N.', colspan: 3, align: 'center'}	
			]
		]
	});
	
	var gridDetallesTotales = new Ext.grid.EditorGridPanel({	
  	id: 'gridDetallesTotales',		
		store: totalesDetallesData,
		plugins: gruposM,
		hidden: true,		
		columns: [	
			{
				header: 'Tipo de documento inicial',
				dataIndex: 'TIPO_DOCUMENTO',
				width: 150,
				align: 'left'				
			},
			{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTAL_DOCTOS_MN',
				width: 150,
				align: 'center'			
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'MONTO_TOTAL_MN',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto del cr�dito',
				dataIndex: 'MONTO_CREDITO_MN',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
				//DOLARES		
			{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTAL_DOCTOS_DL',
				width: 150,
				align: 'center'			
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'MONTO_TOTAL_DL',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto del cr�dito',
				dataIndex: 'MONTO_CREDITO_DL',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			//Doctos en DLL financiados en M.N.			
			{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTAL_DOCTOS_MNDL',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'MONTO_TOTAL_MNDL',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto del cr�dito',
				dataIndex: 'MONTO_CREDITO_MNDL',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			}		
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 150,
		width: 943,	
		style: 'margin:0 auto;',
		frame: true
	});	
	


	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'CONSECUTIVO'},		
			{name: 'NUM_DISTRIBUIDOR'},
			{name: 'NOMBRE_DISTRIBUIDOR'},
			{name: 'NUN_DOCTOINICIAL'},			
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'CATEGORIA'},
			{name: 'PLAZO_DESC'},
			{name: 'PORCE_DESC'},
			{name: 'MONTO_DESC'},				
			{name: 'TIPO_CONVERSION'},	
			{name: 'TIPO_CAMBIO'},	
			{name: 'MONTO_VALUADO'},
			{name: 'MODALIDAD'},
			{name: 'COMENTARIOS'},
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'},
			{name: 'CAMPO4'},
			{name: 'CAMPO5'},
			{name: 'PLAZO'},
			{name: 'FECHA_VEN'},		
			{name: 'NEGOCIABLE'},
			{name: 'LINEA_REGISTRO'},
			{name: 'RESPONSABLE_INTERES'},
			{name: 'DIAS_MINIMO'},
			{name: 'DIAS_MAXIMO'},		
			{name: 'IC_MONEDA'},	
			{name: 'SELECCIONAR_IFS'},
			{name: 'AUXICLINEA_MN'},
			{name: 'AUXICLINEA_USD'},
			{name: 'LINEA_REGISTRO_AUXILIAR'},
			{name: 'PARTIPOCAMBIO'},
			{name: 'HID_NUMLINEAS'},			
			{name: 'CAPTURA_LINEA'},
			{name: 'CAPTURA_PLAZO'},
			{name: 'CAPTURA_FECHAVEN'},
			{name: 'TIPO_CREDITO'},
			{name: 'ESTATUS'},
			{name: 'IC_MONEDA'},
			{name: 'TIPOPAGO'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
				
	var gridCaptura = new Ext.grid.EditorGridPanel({
		id: 'gridCaptura',
		title: 'Carga de Documentos',		
		clicksToEdit: 1,	
		style: 'margin:0 auto;',
		columns: [
		{							
			header : 'N�mero de distribuidor',
			tooltip: 'N�mero de distribuidor',
			dataIndex : 'NUM_DISTRIBUIDOR',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Distribuidor',
			tooltip: 'Distribuidor',
			dataIndex : 'NOMBRE_DISTRIBUIDOR',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'N�mero documento inicial',
			tooltip: 'N�mero documento inicial',
			dataIndex : 'NUN_DOCTOINICIAL',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Fecha de emisi�n',
			tooltip: 'Fecha de emisi�n',
			dataIndex : 'FECHA_EMISION',
			width : 150,
			align: 'center',
			sortable : false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{							
			header : 'Fecha vto. docto.',
			tooltip: 'Fecha vto. docto.',
			dataIndex : 'FECHA_VENCIMIENTO',
			width : 150,
			align: 'center',
			sortable : false
			//,renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{							
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO_DOCTO',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			align: 'left',
			sortable : false			
		},
		{							
			header : 'Monto. docto.',
			tooltip: 'Monto. docto.',
			dataIndex : 'MONTO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Categor�a',
			tooltip: 'Categor�a',
			dataIndex : 'CATEGORIA',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Plazo para descuento en dias',
			tooltip: 'Plazo para descuento en dias',
			dataIndex : 'PLAZO_DESC',
			width : 150,
			align: 'center',
			sortable : false,
			renderer:function(value,metadata,registro){
				if(registro.data['TIPO_CREDITO']!='F') {	
					return value;
				}else  {
					return 'NA';
				}
			}		
		},
		{							
			header : '% descuento',
			tooltip: '% descuento',
			dataIndex : 'PORCE_DESC',
			width : 150,
			align: 'center',
			sortable : false,
			renderer:function(value,metadata,registro){
				if(registro.data['TIPO_CREDITO']!='F') {	
					return Ext.util.Format.number(value,'0.0000%');
				}else  {
					return 'NA';
				}
			}					
		},
		{							
			header : 'Monto % de descuento',
			tooltip: 'Monto % de descuento',
			dataIndex : 'MONTO_DESC',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00') 
		},
		{							
			header : 'Tipo conversi�n',
			tooltip: 'Tipo conversi�n',
			dataIndex : 'TIPO_CONVERSION',
			width : 150,
			align: 'right',
			sortable : false		 
		},
		{							
			header : 'Tipo cambio ',
			tooltip: 'Tipo cambio ',
			dataIndex : 'TIPO_CAMBIO',
			width : 150,
			align: 'right',
			sortable : false		 
		},
		{							
			header : 'Monto valuado en Pesos',
			tooltip: 'Monto valuado en Pesos',
			dataIndex : 'MONTO_VALUADO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00') 
		},	
		{							
			header : 'Modalidad de Plazo',
			tooltip: 'Modalidad de Plazo',
			dataIndex : 'MODALIDAD',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Tipo de Pago',
			tooltip: 'Tipo de Pago',
			dataIndex : 'TIPOPAGO',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Comentarios',
			tooltip: 'Comentarios',
			dataIndex : 'COMENTARIOS',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo1',
			tooltip: 'Campo1',
			dataIndex : 'CAMPO1',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo2',
			tooltip: 'Campo2',
			dataIndex : 'CAMPO2',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Campo3',
			tooltip: 'Campo3',
			dataIndex : 'CAMPO3',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo4',
			tooltip: 'Campo4',
			dataIndex : 'CAMPO4',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo5',
			tooltip: 'Campo5',
			dataIndex : 'CAMPO5',
			width : 150,
			align: 'left',
			sortable : false
		},		
		{							
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO',
			width : 150,
			align: 'center',
			sortable : false
		},			
		{							
			header : 'Fecha vto',
			tooltip: 'Fecha vto',
			dataIndex : 'FECHA_VEN',
			width : 150,
			align: 'center',
			sortable : false		
		},			
		{							
			header : 'Negociable',
			tooltip: 'Negociable',
			dataIndex : 'NEGOCIABLE',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Estatus',
			tooltip: 'Estatus',
			dataIndex : 'ESTATUS',
			width : 150,
			align: 'center',
			sortable : false
		}	
		],
		displayInfo: true,
		store: consultaData,
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		frame: false
		});
		
	
	//Contenedor para el PreAcuse
	var fpPreAcuse = new Ext.Container({
		layout: 'table',
		id: 'fpPreAcuse',
		//hidden: true,
		layoutConfig: {
			columns: 20
		},
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [	
		{
			xtype: 'button',
			text: 'Detalle de Descuento y/o Factoraje',
			id: 'btnDetalle',
			iconCls: 'icoAceptar',
			handler: procesarDetalleLineas,	
			hidden: true
		},	
		{
			xtype: 'button',
			text: 'Detalle de Descuento y/o Factoraje Venta de Cartera',
			id: 'btnDetalleV1',
			iconCls: 'icoAceptar',
			handler: procesarDetalleLineas,	
			hidden: true
		},	
		{
			xtype: 'button',
			text: 'Detalle Cr�dito en Cuenta Corriente',
			id: 'btnDetalleCC',
			iconCls: 'icoAceptar',
			handler: procesarDetalleLineas,	
			hidden: true
		},	
		{
			xtype: 'button',
			text: 'Detalle de los Documentos',
			id: 'btnDetalleDOC',
			iconCls: 'icoAceptar',
			handler: procesarDetalleLineas,	
			hidden: true
		},		
		
		{
			xtype: 'button',
			text: 'Confirma',
			id: 'btnConfirmaAcuse',
			iconCls: 'icoAceptar',
			handler: procesarConfirmaAcuse,	
			hidden: true
		},	
		{
			xtype: 'button',
			text: 'Cancelar',			
			iconCls: 'icoLimpiar',
			id: 'btnCancelarP',
			hidden: true,
			handler:procesarCancelar
		}		
	]
	});
	

	var fpCarga = new Ext.form.FormPanel({
		id: 'forma',
		hidden: true,
		width: 885,
		title: 'Criterios de Captura',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},				
		monitorValid: true	
	})
	
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fpCarga,
			NE.util.getEspaciador(20),
			gridDetallesTotales,	
			NE.util.getEspaciador(20),
			fpPreAcuse,			
			NE.util.getEspaciador(20),				
			gridCaptura,			
			NE.util.getEspaciador(20)		
			
		]
	});
		
	//Cargando PreAcuse 
	if(proceso!==''){
	
	totalesDetallesData.load({ 		
			params: { 		
				informacion: 'ResumenTotalesDetalle', 
				proceso:proceso,
				accionDetalle:accionDetalle
			} 		
		});
		
		consultaData.load({  	
			params: { 		
				informacion: 'Consulta', 		
				proceso:proceso,
				pantalla: 'Captura'
			}  		
		});						
	
	}
	
	
} );