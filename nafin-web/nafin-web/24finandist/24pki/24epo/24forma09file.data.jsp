<%@ page
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		com.netro.parametrosgrales.*,
		netropology.utilerias.*,		
		net.sf.json.JSONArray,net.sf.json.JSONObject"
		contentType="text/html; charset=UTF-8"
		errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	ParametrosRequest req = null;
	String itemArchivo	="",  rutaArchivo ="",   error_tam =""; 
	String PATH_FILE	=	strDirectorioTemp;
	String nombreArchivo= "";
	String estadoSiguiente = "";
	String rutaArchivoTemporal="";
	System.out.println("ServletFileUpload.isMultipartContent(request)=="+ServletFileUpload.isMultipartContent(request));
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
	
		req = new ParametrosRequest(upload.parseRequest(request));
			 
		FileItem fItem = (FileItem)req.getFiles().get(0);
		itemArchivo		= (String)fItem.getName();
		InputStream archivo = fItem.getInputStream();
		rutaArchivo = PATH_FILE+itemArchivo;
		int tamanio			= (int)fItem.getSize();
		
		nombreArchivo= Comunes.cadenaAleatoria(16)+ ".txt";
		rutaArchivoTemporal = PATH_FILE + "/" + nombreArchivo;
		fItem.write(new File(rutaArchivoTemporal));
		
		if(tamanio>2097152){
			error_tam ="El Archivo es muy Grande, excede el Límite que es de 2 MB.";
		}		
	}
	if(!error_tam.equals("")){
		nombreArchivo="";
	}
	ParametrosGrales paramGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
	boolean continua = true;
	String codificacionArchivo ="";
	//boolean resul_cod
	if(paramGrales.validaCodificacionArchivoHabilitado()){
		String[]  	CODIFICACION	= {""};
		CodificacionArchivo  valida_codificacion= new CodificacionArchivo();
		valida_codificacion.setProcessTxt(true);
			//codificaArchivo.setProcessZip(true);
		if(valida_codificacion.esCharsetNoSoportado(rutaArchivoTemporal,CODIFICACION)){
				codificacionArchivo = CODIFICACION[0];
				continua = false;
			
		}
	}
	if(continua==true){
		estadoSiguiente = "VALIDA_CARACTERES_CONTROL";
	}else{
		estadoSiguiente = "MENSAJE_CODIFICACION";
	}
	%>
{
	"success": true,
	"nombreArchivo":	'<%=nombreArchivo%>',
	"error_tam":	'<%=error_tam%>',
	"estadoSiguiente":	'<%=estadoSiguiente%>'
}



