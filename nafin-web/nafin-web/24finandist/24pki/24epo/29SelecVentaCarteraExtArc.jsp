<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.anticipos.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	java.text.SimpleDateFormat,
	java.util.Date,
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String ic_pyme 	= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String ic_moneda 	= (request.getParameter("ic_moneda")==null)?"0":request.getParameter("ic_moneda");
String ig_numero_docto 	= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
String fn_monto_de 	= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
String fn_monto_a 	= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
String cc_acuse 	= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
String mto_descuento 	= (request.getParameter("mto_descuento")==null)?"":request.getParameter("mto_descuento");
String df_fecha_emision_de 	= (request.getParameter("df_fecha_emision_de")==null)?"":request.getParameter("df_fecha_emision_de");
String df_fecha_emision_a 	= (request.getParameter("df_fecha_emision_a")==null)?"":request.getParameter("df_fecha_emision_a");
String doctos_cambio 	= (request.getParameter("doctos_cambio")==null)?"":request.getParameter("doctos_cambio");
String df_fecha_venc_a 	= (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
String df_fecha_venc_de 	= (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
String fecha_publicacion_de 	= (request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
String fecha_publicacion_a 	= (request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");

String infoRegresar	=	"";
String fechaHoy		= "";
if(ic_moneda.equals("") ) ic_moneda = "0";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}

	JSONObject jsonObj = new JSONObject();
	


if(informacion.equals("GenerarArchivo")  ) {
	
try {
	

	String tipoArchivo = (request.getParameter("tipoArchivo") != null) ? request.getParameter("tipoArchivo") : "";
		
	if(tipoArchivo.equals("PDF")){
		
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";	
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	
		pdfDoc.addText("Mexico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Selección de Documentos Venta de Cartera ","formas",ComunesPDF.CENTER);
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
		
		CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);
		
		AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
	
		ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

	
		String	descuentoAutomatico = BeanParametro.DesAutomaticoEpo(iNoEPO,"PUB_EPO_DESC_AUTOMATICO"); 
		String	ventaCartera =  BeanParametro.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); 
		String	tipoCredito= BeanParametro.obtieneTipoCredito (iNoEPO);

	
		Vector vecFilas = aceptPyme.consultaDoctosVentaCartera(iNoCliente,ic_pyme,ic_moneda,ig_numero_docto,fn_monto_de,fn_monto_a, cc_acuse,mto_descuento,df_fecha_emision_de,df_fecha_emision_a,doctos_cambio,df_fecha_venc_de,df_fecha_venc_a,fecha_publicacion_de,fecha_publicacion_a,null);
	
		int i =0, totalDoctosMN	=	0, 	totalDoctosUSD = 0, menosdias =0,  no_dia_semana = 0, menosdias2 =0, 
		menosdias3 =0,  totaldPlazo =0,	elementos  = 0,	totalCreditosMN		=0, totalCreditosUSD	=	0;
		
		double	totalMontoMN	=	0, totalMontoUSD =0,	totalMontstrFacultad =0,	totalMontoDescMN	=	0,totalMontoDescUSD	=	0,
		totalInteresMN =	0,	totalInteresUSD	=	0,	totalMontoValuadoMN	=	0,	totalMontoValuadoUSD=	0,totalMontoCreditoMN	=	0,
		totalMontoCreditoUSD=	0, montoCredito	=	0, tasaInteres = 0, montoTasaInt	=	0, valorTasaInt	=	0,  fnPuntos  = 0,
		montoValuadoLinea  = 0;
		Vector	vecColumnas	= null;
		if(vecFilas.size()>0){
			pdfDoc.setTable(13, 100);
			pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("PYME","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. docto. inicial","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. acuse","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de emisión","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha vencimiento","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de publicación","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo docto.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Tipo conv.","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Tipo cambio","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Monto valuado en pesos","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Núm. de documento final","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de operación","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Referencia tasa de interés","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Valor tasa de interés","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto de intereses","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Total de Capital menos Interés","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
		}
					
		for(i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);
			String nombrePyme			=	(String)vecColumnas.get(0);
			String igNumeroDocto		=	(String)vecColumnas.get(1);
			String ccAcuse 			=	(String)vecColumnas.get(2);
			String dfFechaEmision		=	(String)vecColumnas.get(3);
			String dfFechaVencimiento	=	(String)vecColumnas.get(4);
			String dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
			String igPlazoDocto		=	(String)vecColumnas.get(6);
			String moneda 				=	(String)vecColumnas.get(7);
			double fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			String cgTipoConv			=	(String)vecColumnas.get(9);
			String tipoCambio			=   (String)vecColumnas.get(10);
			double montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			double montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			String igPlazoDescuento	= 	(String)vecColumnas.get(12);
			String fnPorcDescuento		= 	(String)vecColumnas.get(13);
			String modoPlazo			= 	(String)vecColumnas.get(14);
			String estatus 			=	(String)vecColumnas.get(15);
			String cambios				= 	(String)vecColumnas.get(16);
			//creditos por concepto de intereses
			String numeroCredito		= 	(String)vecColumnas.get(17);
			String nombreIf			= 	(String)vecColumnas.get(18);
			String tipoLinea			=	(String)vecColumnas.get(19);
			String fechaOperacion		=	(String)vecColumnas.get(20);
			String plazoCredito		=	(String)vecColumnas.get(22);
			String fechaVencCredito	=	(String)vecColumnas.get(23);
			String relMat				= 	(String)vecColumnas.get(25);
			String tipoCobroInt		=	(String)vecColumnas.get(27);
			String referencia			=	(String)vecColumnas.get(29);
			String tipoPiso			= 	(String)vecColumnas.get(30);
			String icTipoCobroInt		= 	(String)vecColumnas.get(31);
			String icMoneda			= 	(String)vecColumnas.get(32);
			String icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			String monedaLinea			= 	(String)vecColumnas.get(35);
			String dias_minimo			= 	(String)vecColumnas.get(36);
			String dias_maximo			= 	(String)vecColumnas.get(37);
			String icLineaCredito		= 	(String)vecColumnas.get(38);
			String noIc_if				= 	(String)vecColumnas.get(39);
			String nombreMLinea		=  	(String)vecColumnas.get(40);		
 			String plazo_valido =	(String)vecColumnas.get(42)==null?"0":(String)vecColumnas.get(42); 
      String fechaVencimiento =	(String)vecColumnas.get(47)==null?"":(String)vecColumnas.get(47); 
      String epo = (String)vecColumnas.get(48)==null?"":(String)vecColumnas.get(48); 
      String plazopyme = (String)vecColumnas.get(49)==null?"0":(String)vecColumnas.get(49); 
      String plazoepo = (String)vecColumnas.get(50)==null?"0":(String)vecColumnas.get(50);
      String plazonafin = (String)vecColumnas.get(51)==null?"0":(String)vecColumnas.get(51); 
      plazo_valido = plazopyme;
			if (plazopyme.equals("0") && ( plazo_valido.equals("0") || plazo_valido.equals("")) ){
				plazo_valido = plazoepo;        
			}
			if (plazoepo.equals("0") && ( plazo_valido.equals("0") || plazo_valido.equals("") )){
				plazo_valido = plazonafin;
			}
			if(icMoneda.equals(monedaLinea)){
				cgTipoConv		= "";
				tipoCambio		= "";
				montoValuado	= fnMonto;
			}
			if("1".equals(icTipoFinanciamiento)){			
				fechaVencCredito = dfFechaVencimiento;
			}
			if("+".equals(relMat)) {
				valorTasaInt += fnPuntos;
			}else if("-".equals(relMat)){
				valorTasaInt -= fnPuntos;
			}else if("*".equals(relMat)){
				valorTasaInt *= fnPuntos;
			}else if("/".equals(relMat)){
				valorTasaInt /= fnPuntos;
			}
			montoValuadoLinea = fnMonto;
			plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
			if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento)){
			 montoCredito = fnMonto-montoDescuento;
			}else if("2".equals(icTipoFinanciamiento)){
			 	montoCredito = fnMonto;
			}
			//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
				if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento)){
					montoCredito = montoValuado;
				}else if("2".equals(icTipoFinanciamiento)){
					montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
				}
			}								
			if("1".equals(tipoPiso)){
				montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
			}else{
				montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
			}
      String  plazofinanciamiento =(String)vecColumnas.get(44);
			String  interes =(String)vecColumnas.get(45);
			String credito = (String)vecColumnas.get(46);
			
			menosdias= Fecha.restaFechas(dfFechaEmision,dfFechaVencimiento); 
			
			SimpleDateFormat formatEntrada = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaVenci = formatEntrada.parse(dfFechaVencimiento);
			Date fechaSistema = new Date();
								
			menosdias2= Fecha.restaFechas(fechaHoy, dfFechaVencimiento); 			
			totaldPlazo = menosdias - menosdias2;
			plazo_valido=String.valueOf(menosdias2);
	
			pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(ccAcuse,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaEmision,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaVencimiento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(dfFechaPublicacion,"formas",ComunesPDF.CENTER);	
			pdfDoc.setCell(igPlazoDocto,"formas",ComunesPDF.CENTER);	
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMonto,2,false),"formas",ComunesPDF.RIGHT);
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){	
				pdfDoc.setCell(cgTipoConv,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipoCambio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoValuado,2,false),"formas",ComunesPDF.RIGHT);									
			}else{				
				pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+"0.0","formas",ComunesPDF.RIGHT);								
			}		
			pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numeroCredito,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombreMLinea,"formas",ComunesPDF.CENTER);
			montoCredito = ((double)Math.round(montoCredito*100)/100);  //DUDA CON ESTO
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoCredito,2,false),"formas",ComunesPDF.RIGHT);	
			if("1".equals(icTipoFinanciamiento)){				
				pdfDoc.setCell(("0".equals(plazoCredito)?"":plazoCredito),"formas",ComunesPDF.CENTER);		
			}else{
				if ("3".equals(plazofinanciamiento) && (descuentoAutomatico.equals("N")|| descuentoAutomatico.equals("S"))){  					
					pdfDoc.setCell("0","formas",ComunesPDF.CENTER);	
				} else if (descuentoAutomatico.equals("N")|| descuentoAutomatico.equals("")){				
					pdfDoc.setCell(("0".equals(plazo_valido)?"":plazo_valido),"formas",ComunesPDF.CENTER);	
				}else if (descuentoAutomatico.equals("S") ) {					
					pdfDoc.setCell(("0".equals(plazo_valido)?"":plazo_valido),"formas",ComunesPDF.CENTER);	
				}     
			}
			
			pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell((("S".equals(icTipoCobroInt))?"":""+valorTasaInt)+"%","formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+ ( ("S".equals(icTipoCobroInt))?"":""+((double)Math.round(montoTasaInt*Double.parseDouble(plazoCredito)*100)/100)),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTasaInt,2,false),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+"0.0","formas",ComunesPDF.RIGHT);
			
			elementos++;
			if("1".equals(icMoneda)){
				totalDoctosMN++;
				totalMontoMN += fnMonto;
				totalMontoDescMN += montoDescuento;
				totalInteresMN += valorTasaInt;
			}else{
				totalDoctosUSD++;
				totalMontoUSD += fnMonto;
				totalMontoDescUSD += montoDescuento;
				totalInteresUSD += valorTasaInt;
				if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
					totalMontoValuadoUSD += montoValuado;
				}
			}
			if("1".equals(monedaLinea)){
				totalCreditosMN++;
				totalMontoCreditoMN += montoCredito;
			}else if("54".equals(monedaLinea)&&"54".equals(icMoneda)){
				totalCreditosUSD++;
				totalMontoCreditoUSD += montoCredito;
			}

			
		}//for
					
		if(totalDoctosMN>0){ 	
			pdfDoc.setCell("Total M.N.","celda01",ComunesPDF.CENTER );
			pdfDoc.setCell(String.valueOf(totalDoctosMN)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,7);			
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoMN),2)  ,"formas",ComunesPDF.RIGHT);			
			pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);	
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoDescMN),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("Total Financiamientos M.N.","formas",ComunesPDF.CENTER,2);				
			pdfDoc.setCell(String.valueOf(totalCreditosMN) ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);	
			pdfDoc.setCell("$ 0.0","formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$ 0.0","formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);	
		}
		if(totalDoctosUSD>0){ 		
			pdfDoc.setCell("Total USD.","celda01",ComunesPDF.CENTER );
			pdfDoc.setCell(String.valueOf(totalDoctosUSD)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,7);		
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoUSD),2)  ,"formas",ComunesPDF.RIGHT);			
			pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);		
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoDescUSD),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("Total Financiamientos M.N.","formas",ComunesPDF.CENTER,2);	
			pdfDoc.setCell( String.valueOf(totalCreditosUSD) ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);	
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);	
			pdfDoc.setCell("$ 0.0","formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$ 0.0","formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);	
		}
			 
			
		pdfDoc.addTable();
		pdfDoc.endDocument();		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	
	}

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
		
}
	
%>
<%=jsonObj%>