var OperaVentaCartera= ['La modalidad de operaci�n de esta cadena no contempla Venta de Cartera'];

var texto = ['NOTA:  las columnas con subIndice 1 son de Datos del Documento   y subIndice 2 son de Datos del Financiamiento '];

Ext.onReady(function() {

	//DECLARACION DE LAS VARIABLES A USAR
	var fechaHoy = Ext.getDom('fechaHoy').value;	
	var diasInhabiles  = Ext.getDom('diasInhabiles').value;	
	var parTipoCambio = Ext.getDom('parTipoCambio').value;
	var ventaCartera = Ext.getDom('ventaCartera').value;
	var descuentoAutomatico = Ext.getDom('descuentoAutomatico').value;
	var tipoCredito = Ext.getDom('tipoCredito').value;
	var lineaCredito = Ext.getDom('lineaCredito').value;
	var hidNumLineas=0;
	var noElementos=0; 
	var hidNumTasas;
	var totalInteresMN =0;
	var totalInteresUSD =0;
	var totalCapitalInteresMN =0;
	var totalCapitalInteresUSD =0;
	var Auxiliar;
	var monitorLinea;
	var modificado = [];
	var ic_documento = [];
	var ic_tasa = [];
	var cg_rel_mat = [];
	var fn_puntos = [];
	var fn_valor_tasa = [];
	var fn_importe_interes = [];
	var fn_importe_recibir = [];
	var plazo_credito = [];
	var ic_linea_credito = [];
	var fecha_vto = [];
	var monto=[];
	var ic_moneda_linea=[];
	var ic_moneda_docto=[];
	var ventaCartera;
	var mensaje;
	

	var cancelar =  function() {  
		modificado = [];	  
		ic_documento = [];
		ic_tasa = [];
		cg_rel_mat = [];
		fn_puntos = [];
		fn_valor_tasa = [];
		fn_importe_interes = [];
		fn_importe_recibir = [];
		plazo_credito = [];
		ic_linea_credito = [];	
		fecha_vto =[];
		monto=[];
		ic_moneda_linea=[];
		ic_moneda_docto=[];
	}
	
	////////////////////////VALIDADCIONES DE 
	
	var revisaEntero1 =  function( campo) { 
		var numero=campo;
		if (!isdigit(numero)) {
			Ext.MessageBox.alert('Mensaje','El valor no es un n�mero v�lido.');
			return false;
		}	else{
			return true;
		}
	}
	
	var Tmes = new Array();
	var sCadena;
	Tmes[1] = "31"; Tmes[2] ="28"; Tmes[3]="31"; Tmes[4]="30"; Tmes[5]="31"; 
	Tmes[6]="30";
	Tmes[7] = "31"; Tmes[8] ="31"; Tmes[9]="30"; Tmes[10]="31";Tmes[11]="30"; 
	Tmes[12]="31";
	
	var fnDesentrama =  function( Cadena) { 
   var Dato;
   sCadena      = Cadena
   tamano       = sCadena.length;
   posicion     = sCadena.indexOf("/");
   Dato         = sCadena.substr(0,posicion);
   cadenanueva  = sCadena.substring(posicion + 1,tamano);
   sCadena      = cadenanueva;
   return Dato;
}


	var mtdCalculaDias =  function( sFecha,sFecha2) {
		var iResAno,DiaMes,DiasDif=0,iCont=0,iMes,DiaMesA,iBan=0;
    var DiasPar,Dato,VencDia,VencMes,AltaMes,AltaDia;
    sCadena   = sFecha
    AltaDia   = fnDesentrama(sCadena);
    AltaMes  = fnDesentrama(sCadena);
    AltaAno   = sCadena;
    sCadena     = sFecha2
    VencDia     = fnDesentrama(sCadena);
    VencMes     = fnDesentrama(sCadena);
    VencAno     = sCadena;
    Dato  = AltaMes.substr(0,1);
    if (Dato ==0){
			AltaMes = AltaMes.substr(1,2)
		}
    Dato  = AltaDia.substr(0,1);
    if (Dato ==0){
			AltaDia = AltaDia.substr(1,2)
    }
    Dato  = VencDia.substr(0,1);
    if (Dato ==0){
			VencDia = VencDia.substr(1,2)
    }
    Dato  = VencMes.substr(0,1);
		if (Dato ==0){
			VencMes = VencMes.substr(1,2)
    }
    while (VencAno != AltaAno || VencMes != AltaMes || VencDia != AltaDia){
			iCont++;
      while(AltaMes != VencMes || VencAno != AltaAno){
				iBan=1;
			  iResAno = AltaAno % 4;
			  if (iResAno==0){
					Tmes[2] =29;
			  }else{
					Tmes[2] =28;
			  }
			  if (iCont == 1){
					DiaMesA = Tmes[AltaMes];
			    DiasDif = DiaMesA - AltaDia;
			  } else{
					DiaMesA  = Tmes[AltaMes];
			    DiasDif += parseInt(DiaMesA);
			  }
			  if (AltaMes==12){
					AltaAno++;
			    AltaMes=1;
			  }else{
					AltaMes++;
			  }
			  iCont++;
			} // fin de while
      if (AltaMes == VencMes && VencAno == AltaAno){
				if (iBan==0){
					DiasDif = parseInt(VencDia) - parseInt(AltaDia);
					AltaDia = VencDia;
        } else if (iBan=1){
					DiasDif += parseInt(VencDia);
          AltaDia = VencDia;
        }
      }
		}
		return DiasDif;
	}
	
	//Inicializa validaciones
	var inicializaTasasXPlazo =  function( valor, metadata, registro, info) { 
	
		var plazo= registro.data['PLAZO_FINAL'];	
		if(plazo!='0') {	
			return obtenTasaXPlazoIni( valor, metadata, registro, info);
		}else {
			return '0';
		}	
	}
	
	var obtenTasaXPlazoIni =  function( valor, metadata, registro, info) { 
		var i=0;
		var auxIcIf;
		var auxReferenciaTasa;
		var auxIcTasa;
		var auxValorTasa;
		var auxIcMoneda;
		var auxPlazoDias;
		var iPlazoCredito = registro.data['PLAZO_FINAL'];
		var iPlazoTasa;
		var iPlazoTasaAux = 9999999999;
		var ok = false;
		var referenciaTasa = registro.data['REFERENCIA'];
		var valorTasa			= registro.data['VALOR_TASA']; 
		var icMoneda			= registro.data['IC_MONEDA_LINEA'];
		var icIf				  = registro.data['IC_IF']; 
		var icTasa				= registro.data['IC_TASA'];  
		var cgRelMat			= registro.data['CG_REL_MAT'];  
		var fnPuntos			= registro.data['FN_PUNTOS']; 
		var tipoPiso			= registro.data['TIPO_PISO'];  
		var montoAuxInt		= registro.data['MONTO_AUX_INT'];
		var montoCredito	= registro.data['MONTO_CREDITO']; 
		var montoInt			= registro.data['MONTO_TASA_INTERES'];  
		var montoCapitalInt = registro.data['MONTO_TOTAL_CAPITAL']; 
		var valorTasaPuntos = registro.data['VALOR_TASA_PUNTOS'];
		var plazo= registro.data['PLAZO_FINAL'];	
			
		if(hidNumTasas==1){		
			auxIcIf			= eval('Auxiliar.auxIcIf0');   
			auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa0'); 
			auxIcTasa			= eval('Auxiliar.auxIcTasa0'); 
			auxCgRelMat		= eval('Auxiliar.auxCgRelMat0'); 
			auxFnPuntos		= eval('Auxiliar.auxFnPuntos0'); 
			auxValorTasa		= eval('Auxiliar.auxValorTasa0'); 
			auxIcMoneda		= eval('Auxiliar.auxIcMoneda0'); 
			auxPlazoDias		= eval('Auxiliar.auxPlazoDias0'); 
			if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){		
				iPlazoTasaAux	= iPlazoTasa;
				referenciaTasa	= auxReferenciaTasa;		 	
				valorTasa	= auxValorTasa; 
				icTasa	= auxIcTasa;
				cgRelMat	= auxCgRelMat;
				fnPuntos	= auxFnPuntos;
				if(cgRelMat=='+')
					valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
				else if(cgRelMat=='-')
					valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
				else if(cgRelMat=='*')
					valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
				else if(cgRelMat=='/')
					valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);	
				ok = true;
				if(tipoPiso=="1"){
					montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360,2)*100)/100;
				}else{
					montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
				}
				montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
				montoCapitalInt = parseFloat(montoCredito)-parseFloat(montoInt);			
			}		
		}else {
			for(i=0;i<hidNumTasas;i++){
				auxIcIf			= eval('Auxiliar.auxIcIf'+i);   
				auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa'+i); 
				auxIcTasa			= eval('Auxiliar.auxIcTasa'+i); 
				auxCgRelMat		= eval('Auxiliar.auxCgRelMat'+i); 
				auxFnPuntos		= eval('Auxiliar.auxFnPuntos'+i); 
				auxValorTasa		= eval('Auxiliar.auxValorTasa'+i); 
				auxIcMoneda		= eval('Auxiliar.auxIcMoneda'+i); 
				auxPlazoDias		= eval('Auxiliar.auxPlazoDias'+i); 
			  iPlazoTasa			= parseInt(auxPlazoDias);			
				if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
					iPlazoTasaAux 			= iPlazoTasa;
					referenciaTasa	= auxReferenciaTasa;		 	
					valorTasa 		= auxValorTasa; 			
					icTasa			= auxIcTasa;
					cgRelMat			= auxCgRelMat;
					fnPuntos			= auxFnPuntos;
					if(cgRelMat=='+')
						valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
					else if(cgRelMat=='-')
						valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
					else if(cgRelMat=='*')
						valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
					else if(cgRelMat=='/')
						valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);								
					ok = true;	
					if(tipoPiso=="1"){
						montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360)*100,2)/100;
					}else{
						montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
					}
					montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);
					montoCapitalInt = parseFloat(montoCredito)-parseFloat(montoInt);
				}
			}//for 
		}
			
		if(info =='ValorTasaInteres' ) { ok = valorTasaPuntos;    }
		if(info =='MontodeIntereses' ) { ok = montoInt; 	}
		if(info =='MontoCapitalInteres' ) { ok = montoCapitalInt; }
		if(info =='ReferenciaTasa' ) { ok = referenciaTasa; }		
		if(info =='ic_tasa') { ok = icTasa; }		
		if(info =='montoAuxi' ) { ok = montoAuxInt; }	
		if(info =='cg_rel_mat' ) { ok = cgRelMat; }	
		if(info =='fnPuntos' ) { ok = fnPuntos; }	
	
		
	return ok;
	}
	
	
	var validaplazo =  function( opts, success, response, registro) { 
		var plazoIF = registro.data['PLAZO_VALIDO'];  
		var dias_minimo = registro.data['DIAS_MINIMO'];  
		var dias_maximo = registro.data['DIAS_MAXIMO']; 	
		var PlazoPyme= registro.data['PLAZO_FINAL'];							
		var  mensajetxt = "";
		if(plazoIF!="") {
			plazoIF = parseInt(plazoIF);
		}		
		if (PlazoPyme > plazoIF){    
			Ext.MessageBox.alert("Mensaje","El plazo capturado "+PlazoPyme+ " no puede ser mayor a "+plazoIF); 
			return false;
		}else {
			mensajetxt =sumaFecha( opts, success, response, registro);
			if(mensajetxt!=""){
				Ext.MessageBox.alert("Mensaje"," "+mensajetxt);	
				return false;
			}
			inicializaTasasXPlazo2(opts, success, response, registro);			
		   return true;
		}
	}//termina funcion


	var objeto 	= 0;	
	var objeto2 = 0;
	var objeto3 = 0;

	var sumaFecha =  function( opts, success, response, registro) {
		
		var mensaje="";
		var dias_minimo = registro.data['DIAS_MINIMO'];  
		var dias_maximo = registro.data['DIAS_MAXIMO']; 		
  	var	txtPlazo; 
		txtPlazo= registro.data['PLAZO_FINAL'];	
			
		var	fechaVto				= Ext.util.Format.date(registro.data['FECHA_VENC_FINAL'],'d/m/Y'); 
		var	fechaEmision		= Ext.util.Format.date(registro.data['FECHA_EMISION'],'d/m/Y');  
		var fechaVenc				= Ext.util.Format.date(registro.data['FECHA_VENCIMIENTO'],'d/m/Y');   
		var	montoInt				= registro.data['MONTO_TASA_INTERES'];   
		var montoCapitalInt	= registro.data['MONTO_TOTAL_CAPITAL']; 		
		var	montoAuxInt			= registro.data['MONTO_AUX_INT']; 	
		var	monto						= registro.data['MONTO']; 	
		var	montoCredito		= registro.data['MONTO_CREDITO']; 
		var	montoDocto			= registro.data['MONTO_VALUADO']; 
		var	igPlazoDocto 		= registro.data['PLAZO_DOCTO']; 			 
		var	referenciaTasa	= registro.data['REFERENCIA'];  
		var	monedaLinea			= registro.data['IC_MONEDA_LINEA'];  
		var	icIf						= registro.data['IC_IF'];  		
		var	valorTasa				= registro.data['VALOR_TASA'];  
		var tipoPiso				= registro.data['TIPO_PISO'];  
		var	icTasa					= registro.data['IC_TASA'];   
		var	cgRelMat				= registro.data['CG_REL_MAT'];  
		var	fnPuntos				= registro.data['FN_PUNTOS'];  
		var	valorTasaPuntos	= registro.data['VALOR_TASA_PUNTOS'];        
		
		var fec_venc_value = fechaVenc;
		var fec_venc_array = fec_venc_value.split("/");
		var TDate = new Date(fec_venc_array[2],fec_venc_array[1]-1,fec_venc_array[0]);
				
		diaMes = TDate.getDate();  		
		if(objeto3==0)
			objeto3 = 1;
		//alert(txtPlazo+'--'+referenciaTasa+'--'+valorTasa+'--'+monedaLinea+'--'+icIf+'--'+icTasa+'--'+cgRelMat+'--'+fnPuntos+'--'+tipoPiso+'--'+montoAuxInt+'--'+montoCredito+'--'+montoInt+'--'+montoCapitalInt+'--'+valorTasaPuntos);
    if(txtPlazo!="0") {
			if(!obtenTasaXPlazo(opts, success, response, registro)) {
      	if(objeto3==1){
				mensaje = "El documento no puede ser seleccionado con ese plazo porque no se encontro la tasa correspondiente \n ";				
			}			
		}else{
			objeto3=0;
		}		
    }//if(txtPlazo!="") 
		var AddDays = 0;
    if(txtPlazo!=""){
    	if(revisaEntero1(txtPlazo)){      
				AddDays = txtPlazo;
			}else{
       mensaje =   "El valor no es un n�mero v�lido.";
      }
			
		}
			
  if(txtPlazo!="0"){
		diaMes = parseInt(diaMes)+ parseInt(AddDays);
		TDate.setDate(diaMes);
		var CurYear = TDate.getYear();
		var CurDay = TDate.getDate();
		var CurMonth = TDate.getMonth()+1;
		if(CurDay<10)
			CurDay = '0'+CurDay;
		if(CurMonth<10)
			CurMonth = '0'+CurMonth;
		TheDate = CurDay +'/'+CurMonth+'/'+CurYear;
				
		fechaVto = Ext.util.Format.date(TDate,'d/m/Y'); 		
		registro.data['FECHA_VENC_FINAL']=fechaVto; 
		registro.commit();
		
		montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(txtPlazo),2);		
		plazo = parseInt(txtPlazo);		
		dias_maximo = dias_maximo - mtdCalculaDias(fechaEmision,fechaHoy)
		//dias_maximo = 150;
		
		if(objeto==0)
			objeto = 1;
		if(plazo<dias_minimo){
			if(objeto==1){				
				mensaje += "Plazo minimo del financiamiento = "+dias_minimo+" \n";			
			}			
		}else if(plazo>dias_maximo){
			if(objeto==1){
				mensaje += "Plazo maximo del financiamiento = "+dias_maximo+" \n";
			}
		}else{
			objeto=0;
		}
		if(objeto2==0)
			objeto2 = 1;
		var diasInhabiles2 = diasInhabiles;
		var diaMes = CurDay +'/'+CurMonth;
		
		if(diasInhabiles2.indexOf(diaMes)>0||TDate.getDay()==0||TDate.getDay()==6){
			if(objeto2==1){
				mensaje +="La Fecha de Vencimiento del Credito es un dia inhabil";				
			}
		}else{
			objeto2 = 0;
		}
  }
		return mensaje;
	}	
		
	
	var obtenTasaXPlazo =  function( opts, success, response, registro) { 
		var i = 0;
		var plazo= registro.data['PLAZO_FINAL'];	
		var referenciaTasa = registro.data['REFERENCIA'];
		var valorTasa			= registro.data['VALOR_TASA']; 
		var icMoneda			= registro.data['IC_MONEDA_LINEA'];
		var icIf				  = registro.data['IC_IF']; 
		var icTasa				= registro.data['IC_TASA'];  
		var cgRelMat			= registro.data['CG_REL_MAT'];  
		var fnPuntos			= registro.data['FN_PUNTOS']; 
		var tipoPiso			= registro.data['TIPO_PISO'];  
		var montoAuxInt		= registro.data['MONTO_AUX_INT'];;
		var montoCredito	= registro.data['MONTO_CREDITO']; 
		var montoInt			= registro.data['MONTO_TASA_INTERES'];  
		var montoCapitalInt = registro.data['MONTO_TOTAL_CAPITAL']; 
		var valorTasaPuntos = registro.data['VALOR_TASA_PUNTOS'];		
		
		var	numTasas = hidNumTasas;		
		var i=0;
		var auxIcIf;
		var auxReferenciaTasa;
		var auxIcTasa;
		var auxValorTasa;
		var auxIcMoneda;
		var auxPlazoDias;
		var iPlazoCredito = parseInt(plazo);
		var iPlazoTasa;
		var iPlazoTasaAux = 9999999999;
		var ok = false;
		if(numTasas==1){
			auxIcIf			= eval('Auxiliar.auxIcIf0');   
			auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa0'); 
			auxIcTasa			= eval('Auxiliar.auxIcTasa0'); 
			auxCgRelMat		= eval('Auxiliar.auxCgRelMat0'); 
			auxFnPuntos		= eval('Auxiliar.auxFnPuntos0'); 
			auxValorTasa		= eval('Auxiliar.auxValorTasa0'); 
			auxIcMoneda		= eval('Auxiliar.auxIcMoneda0'); 
			auxPlazoDias		= eval('Auxiliar.auxPlazoDias0'); 
			iPlazoTasa			= parseInt(auxPlazoDias);
			if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
				iPlazoTasaAux			= iPlazoTasa;
				referenciaTasa	= auxReferenciaTasa;		 	
				valorTasa			= auxValorTasa; 
				icTasa			= auxIcTasa;
				cgRelMat			= auxCgRelMat;
				fnPuntos			= auxFnPuntos;
				if(cgRelMat=='+')
					valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
				else if(cgRelMat=='-')
					valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
				else if(cgRelMat=='*')
					valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
				else if(cgRelMat=='/')
					valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);
					ok = true;
				if(tipoPiso=="1"){
					montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360,2)*100)/100;
				}else{
					montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
				}
				montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);				
				montoCapitalInt = parseFloat(montoInt)+parseFloat(montoCredito);
			}
		}else{
			for(i=0;i<numTasas;i++){
				auxIcIf			= eval('Auxiliar.auxIcIf'+i);   
				auxReferenciaTasa	= eval('Auxiliar.auxReferenciaTasa'+i); 
				auxIcTasa			= eval('Auxiliar.auxIcTasa'+i); 
				auxCgRelMat		= eval('Auxiliar.auxCgRelMat'+i); 
				auxFnPuntos		= eval('Auxiliar.auxFnPuntos'+i); 
				auxValorTasa		= eval('Auxiliar.auxValorTasa'+i); 
				auxIcMoneda		= eval('Auxiliar.auxIcMoneda'+i); 
				auxPlazoDias		= eval('Auxiliar.auxPlazoDias'+i); 
				iPlazoTasa			= parseInt(auxPlazoDias);
				//alert("iteracion "+ i + " plazo credito = "+iPlazoCredito+" Plazo Tasa = "+iPlazoTasa +" plazoTasaAux ="+iPlazoTasaAux);
				if(icMoneda==auxIcMoneda&&iPlazoCredito<=iPlazoTasa&&iPlazoTasa<=iPlazoTasaAux&&auxIcIf==icIf){
					//alert("Si entro en el if "+iPlazoTasa + "iplazotasaaux = "+iPlazoTasaAux+" i:: "+i);
					iPlazoTasaAux 			= iPlazoTasa;
					referenciaTasa	= auxReferenciaTasa;		 	
					valorTasa 		= auxValorTasa; 			
					icTasa			= auxIcTasa;
					cgRelMat			= auxCgRelMat;
					fnPuntos			= auxFnPuntos;
					if(cgRelMat=='+')
						valorTasaPuntos = parseFloat(valorTasa)+parseFloat(fnPuntos);
					else if(cgRelMat=='-')
						valorTasaPuntos = parseFloat(valorTasa)-parseFloat(fnPuntos);				
					else if(cgRelMat=='*')
						valorTasaPuntos = parseFloat(valorTasa)*parseFloat(fnPuntos);				
					else if(cgRelMat=='/')
						valorTasaPuntos = parseFloat(valorTasa)/parseFloat(fnPuntos);	
						ok = true;
					if(tipoPiso=="1"){
						montoAuxInt = roundOff((montoCredito *(valorTasaPuntos/100)/360)*100,2)/100;						
					}else{
						montoAuxInt = (montoCredito*(valorTasaPuntos/100)/360);
					}
					montoInt = roundOff(parseFloat(montoAuxInt)*parseFloat(plazo),2);					
					montoCapitalInt = parseFloat(montoCredito)-parseFloat(montoInt);	
				}
			}//for   
		}					
		registro.data['MONTO_TASA_INTERES']=montoInt;
		registro.data['MONTO_TOTAL_CAPITAL']=montoCapitalInt;
		registro.data['VALOR_TASA_PUNTOS']=valorTasaPuntos;
		registro.data['REFERENCIA']=referenciaTasa;		
		registro.commit();			
		return ok;
}

		
	var inicializaTasasXPlazo2 =  function( opts, success, response, registro) { 
		var plazo=  registro.data['PLAZO_FINAL'];				
		if(plazo!='0') {	
			return obtenTasaXPlazo( opts, success, response, registro);
		}else {
			return '0';
		}	
	}	
	
	var sumaTotalesIntereses =  function(opts, success, response, registro) {	
		var totalMN 		= 0.00;
		var totalUSD		= 0.00;
		var totalCapitalMN 	= 0.00;
		var totalCapitalUSD = 0.00;	
		var montoTasaInt;
		var montoCapitalInt;
		var icMoneda;
		var monedaLinea; 
		var monto_tasa_int = registro.data['MONTO_TASA_INTERES']; 
		var monto_capital_int =registro.data['MONTO_TOTAL_CAPITAL'];
	
		montoTasaInt	= parseFloat(monto_tasa_int);
		montoCapitalInt	= parseFloat(monto_capital_int);
		icMoneda 	= registro.data['IC_MONEDA_DOCTO'];
		monedaLinea	= registro.data['IC_MONEDA_LINEA']; 
		
		if(monedaLinea=="1"){
			totalMN += montoTasaInt;
			totalCapitalMN	+= montoCapitalInt;
		}else if(icMoneda=="54"&&monedaLinea=="54"){
			totalUSD += montoTasaInt;
			totalCapitalUSD	+= montoCapitalInt;
		}				
		totalInteresMN = roundOff(totalMN,2); //estos valores son los  que se muestran al seleccionar los registros 
		totalInteresUSD = roundOff(totalUSD,2);
		totalCapitalInteresMN = roundOff(totalCapitalMN,2);
		totalCapitalInteresUSD = roundOff(totalCapitalUSD,2);					
			
	}
	
	var seleccionaDocto =  function(opts, success, response, record) {
	
		var fechaEmision	=  Ext.util.Format.date( record.data['FECHA_EMISION'],'d/m/Y'); 
		var fechaVenc	=  Ext.util.Format.date( record.data['FECHA_VENCIMIENTO'],'d/m/Y'); 	
		var txtPlazo =record.data['PLAZO_FINAL'];
		var montoInt = record.data['MONTO_TASA_INTERES']; 
		var icTasa = record.data['IC_TASA'];	
		var plazo_unico=  record.data['PLAZO_VALIDO'];
		
		if (record.data['PYME_BLOQ_X_IF']=='S' )  {
			Ext.MessageBox.alert("Mensaje","Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado por el Intermediario Financiero. ");
			//return false;	
		}else  {
		
			if(txtPlazo=="0"){
				Ext.MessageBox.alert("Mensaje","Debe capturar los plazos del credito en los documentos a aceptar");
				return false;
			}else {
				return true;
			}
			
			plazoIF = parseInt(plazo_unico); 
			PlazoPyme = parseInt(txtPlazo);  
				
			if (PlazoPyme > plazoIF){
			 Ext.MessageBox.alert("Mensaje","El plazo capturado "+PlazoPyme+ " no puede ser mayor a "+plazoIF);
				return false;
			}else {
				return true;
			}
			if(datecomp(fechaHoy, fechaEmision)==2){
				Ext.MessageBox.alert("Mensaje","Los Documentos no pueden seleccionarse antes de su d�a de emisi�n");		
				return;
			}	
			
			if(datecomp(fechaHoy,fechaVenc)==1){
				Ext.MessageBox.alert("Mensaje","Los Documentos no pueden seleccionarse despues de su fecha de Vencimiento");		
				return false;
			}else {
				return true;
			}
		}
					
	}	

	var calculaMontos =  function(opts, success, response, registro) {	
	
		var numLineas = hidNumLineas
		var tipoCambio = parTipoCambio;
		var valorTipoCambio = 0;
		var auxMonto		= 0; 
		var monto_credito		= registro.data['MONTO_CREDITO']; 
		var ic_linea_credito = registro.data['IC_LINEA_CREDITO']; 
		var auxi 			= 0;	
		var montoSeleccionado=0;
		var saldoDisponible=0;
		if(tipoCambio!=''){
			valorTipoCambio = parseFloat(tipoCambio);
		}				
		
		var montoDisSubLimite	= registro.data['MONTO_DIS_SUB_LIMITE']; 
		
		if(monto_credito>montoDisSubLimite) {
			Ext.MessageBox.alert("Mensaje","Por el momento alguno(s) de los documento(s) no puede ser seleccionado. "+
			"<br>Favor de comunicarse al centro de atenci�n a clientes NAFIN�.");
			return false ;		
		}
		
		
		if(numLineas==1){
			montoSeleccionado = "0.00";
			numDoctos = "0";	
			saldoDisponible			= eval('monitorLinea.hidSaldoDisponible0');  
			montoSeleccionado			= eval('monitorLinea.montoSeleccionado0'); 
			auxMonto = parseFloat(monto_credito);
			if(auxMonto>0){					
					montoSeleccionado = roundOff(parseFloat(montoSeleccionado)+auxMonto,2);				
					saldoDisponible = roundOff(parseFloat(saldoDisponible)-auxMonto,2);	
					//return true;
				}else{
					return false;
				}
				if(parseInt(saldoDisponible)<0){
					Ext.MessageBox.alert("Mensaje","El saldo de la l�nea es insuficiente para el documento");
					return false ;
				}				
				
				return true;
				
		}else if(numLineas>1){	
			for(var i=0;i<numLineas;i++){
				montoSeleccionado	= eval('monitorLinea.montoSeleccionado'+i); 
				saldoDisponible		= eval('monitorLinea.hidSaldoDisponible'+i); 

				var ic_linea	= eval('monitorLinea.ic_linea'+i); 				
					if(ic_linea_credito==ic_linea){					
					auxMonto = monto_credito;
					if(auxMonto>0){
						montoSeleccionado = roundOff(parseFloat(montoSeleccionado)+auxMonto,2);				
						saldoDisponible = roundOff(parseFloat(saldoDisponible)-auxMonto,2);	
						//return true;
					}else{
						return false;
					}					
					if(parseInt(saldoDisponible)<0){
						Ext.MessageBox.alert("Mensaje","El saldo de la l�nea es insuficiente para el documento");
						return false;
					}
				}
			}
			//return true;
		}
		
		return true
		/*if(parseInt(monto)>parseInt(montoLineaCredito) ){
			confirm("El monto publicado rebasa el saldo disponible de su l�nea. �Desea continuar?");
			return false;
		}else {
			return true;
		}*/
						
	}
	
	
	
	var validaciones =  function(opts, success, response, record) {	
		gridEditable = Ext.getCmp('gridEditable');		
		
		if (record.data['PYME_BLOQ_X_IF']=='S' )  {
			Ext.MessageBox.alert("Mensaje","Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado por el Intermediario Financiero. ");
			return false;			
		}else  {		
	
			var montoInt = inicializaTasasXPlazo( '', '', record,'MontodeIntereses');
			record.data['MONTO_TASA_INTERES']=montoInt;			
									
			var montoCap = inicializaTasasXPlazo( '', '', record,'MontoCapitalInteres');
			record.data['MONTO_TOTAL_CAPITAL']=montoCap;								
				
			var varTasa = inicializaTasasXPlazo( '', '', record,'ValorTasaInteres');
			record.data['VALOR_TASA_PUNTOS']=varTasa;				
		
			var referencia = inicializaTasasXPlazo( '', '', record,'ReferenciaTasa');		
			record.data['REFERENCIA']=referencia;	
			
			var montoAuxi = inicializaTasasXPlazo( '', '', record,'montoAuxi');
			record.data['MONTO_AUX_INT']=montoAuxi;	
								
			var tasa =  inicializaTasasXPlazo( '', '', record,'ic_tasa');
			record.data['IC_TASA']=tasa;	
			
			var relMat =  inicializaTasasXPlazo( '', '', record,'cg_rel_mat');
			record.data['CG_REL_MAT']=relMat;				
		
			var fnPuntos =  inicializaTasasXPlazo( '', '', record,'fnPuntos');
			record.data['FN_PUNTOS']=fnPuntos;		
					
			record.commit();
			return true;
			}
		
	}
	
	
	///////////////////CONCLUYEN VALIDACIONES
	if(ventaCartera=='S')  {
		mensaje = '<table width="900" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td class="formas" colspan="5">'+
		'Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesi�n de Derechos de Cobro del (los) Documento (s) final (es) descrito (s) en el mismo derivado del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO seg�n corresponda. Dicha aceptaci�n tendr� plena validez para todos los efectos legales a que haya lugar. En este mismo acto se genera el aviso de notificaci�n a la EMPRESA DE PRIMER ORDEN.  En t�rminos de los art�culos 32 C del C�digo Fiscal y 427 de la Ley General de T�tulos y Operaciones de Cr�dito, para los efectos legales conducentes, el INTERMEDIARIO FINANCIERO se obliga a notificar por sus propios medios al CLIENTE Y/O DISTRIBUIDOR la transmisi�n de los derechos y/o cuentas por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO. No obstante, en t�rminos de los citados art�culos, en caso de existir COBRANZA DELEGADA, el INTERMEDIARIO FINANCIERO no estar� obligado a notificar al CLIENTE Y/O DISTRIBUIDOR la transmisi�n de los derechos y/o cuentas por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO. '+
		'</td></tr>'+ 
		'</table>';
	}else  {
		mensaje = '<table width="900" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td class="formas" colspan="5">'+
		'Al trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, est� solicitando el DESCUENTO y/o FACTORAJE del (los) DOCUMENTOS (S) INICIAL (ES) que se detallan en est� pantalla y que ha publicado electr�nicamente, otorgando su consentimiento para que en caso de que se d�n las condiciones se�aladas en el Contrato de Financiamiento a Clientes y Distribuidores, el (LOS) DOCUMENTOS (S) INICIAL (ES) 	ser�n sustituidos por el (los) DOCUMENTO (S) FINAL (ES), cediendo los derechos de cobro a su favor al INTERMEDIARIO FINANCIERO que los opere'+
		'</td></tr>'+ 
		'</table>';
	}
	
	
	var leyenda = new Ext.Container({
		layout: 'table',
		id: 'leyenda',
		hidden: true,
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		mensaje, 
				cls:		'x-form-item', 
				style: { 
					width:   		'700%', 
					textAlign: 		'left'
				} 
			}			
		]
	});
	
	
	// muestra la pantalla de acuse 
	var  procesarSuccessFailureAcuse =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			var acuse;				
			if (jsondeAcuse != null){
				acuse= jsondeAcuse.acuse;						
			}
		
				var Comfirmacionclave = Ext.getCmp('Comfirmacionclave');
					if (Comfirmacionclave) {		
						Comfirmacionclave.destroy();		
					}									
			
				var acuseCifras = [
						['N�mero de Acuse', jsondeAcuse.acuse],
						['Fecha de Carga', jsondeAcuse.fecCarga],
						['Hora de Carga', jsondeAcuse.horaCarga],
						['Usuario de Captura', jsondeAcuse.captUser]
					];
					
					storeCifrasData.loadData(acuseCifras);				
				
					var totalDoctosMN3 =0;
					var totalMontoMN3 = 0;
					var totalMontoSIntMN3 = 0;
					var totalInteresMN3 = 0;
					
					var totalDoctosUSD3 = 0;
					var totalMontoUSD3 = 0;
					var totalMontoSInUSD3 = 0;
					var totalInteresUSD3 = 0;
					
					var totalDoctosConv3 = 0;
					var totalMontosConv3 = 0;
					var totalMontoSinConv3 = 0;
					var totalInteresConv3 = 0;
					
				var  gridMontos = Ext.getCmp('gridMontos');
				var store = gridMontos.getStore();	
				store.each(function(record) {
					 totalDoctosMN3 = record.data['totalDoctosMN'];
					 totalMontoMN3 = record.data['totalMontoMN'];
					 totalMontoSIntMN3 = record.data['totalMontoSIntMN'];
					 totalInteresMN3 = record.data['totalInteresMN'];
					
					 totalDoctosUSD3 = record.data['totalDoctosUSD'];
					 totalMontoUSD3 = record.data['totalMontoUSD'];
					 totalMontoSInUSD3 = record.data['totalMontoSInUSD'];
					 totalInteresUSD3 = record.data['totalInteresUSD'];
					
					 totalDoctosConv3 = record.data['totalDoctosConv'];
					 totalMontosConv3 = record.data['totalMontosConv'];
					 totalMontoSinConv3 = record.data['totalMontoSinConv'];
					 totalInteresConv3 = record.data['totalInteresConv'];										
				});
			
			
	

			var  gridPreAcuse = Ext.getCmp('gridPreAcuse');
			var store = gridPreAcuse.getStore();	
			//se recorre grid principal para detectar los documentos seleccionados			
			store.each(function(record) {
				record.data['ESTATUS'] ='Seleccionado Pyme';
				record.commit();
			});
		
				
				//para el boton de PDF de Acuse 
			var btnGenerarAc = Ext.getCmp('btnGenerarPDFAcuse');
			btnGenerarAc.setHandler(function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');								
				Ext.Ajax.request({
					url: '29SelecVentaCarteraArcAcuse.jsp',
					params: Ext.apply(fp.getForm().getValues(),{														
						acuse:acuse,								
						informacion: 'GenerarArchivo',
						tipoArchivo:'PDF',
						totalDoctosMN3:totalDoctosMN3, 
						totalMontoMN3:totalMontoMN3,
						totalMontoDescMN3:totalMontoSIntMN3, 
						totalInteresMN3:totalInteresMN3, 						
						totalDoctosUSD3:totalDoctosUSD3, 
						totalMontoUSD3:totalMontoUSD3, 
						totalMontoDescUSD3:totalMontoSInUSD3, 
						totalInteresUSD3:totalInteresUSD3,						
						totalDoctosConv3:totalDoctosConv3, 
						totalMontosConv3:totalMontosConv3, 
						totalMontoDescConv3:totalMontoSinConv3, 
						totalInteresConv3:totalInteresConv3						
					}),
					callback: procesarSuccessFailurePDFAcuse
				});
			});
			
			//para el boton de CSV de Acuse 
			var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
			btnGenerarCSVAcuse.setHandler(function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');								
					Ext.Ajax.request({
						url: '29SelecVentaCarteraArcAcuse.jsp',
						params: Ext.apply(fp.getForm().getValues(),{														
							acuse:acuse,							
							tipoArchivo:'CSV',
							informacion: 'GenerarArchivo'						
						}),
						callback: procesarSuccessFailureCSVAcuse
					});
				});
				
		}
	}
	//confirmacion de clave de cesion
	function confirmacionClavesCesion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
		var bConfirma;
		jsondeConfirma = Ext.util.JSON.decode(response.responseText);
			if (jsondeConfirma != null){
				bConfirma= jsondeConfirma.bConfirma;									
			}		
		if(bConfirma ==false) {
			Ext.MessageBox.alert('Mensaje','La confirmaci�n es incorrecta, intente de nuevo');
			
		} else  if(bConfirma ==true) {		
			gridMontos.show();
			gridEditable.hide();
			ctexto.hide();
			fp.hide();
			leyenda.show();
			gridPreAcuse.show();
			gridCifrasControl.show();	
					
			var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
			var btnBajarCSVAcuse = Ext.getCmp('btnBajarCSVAcuse');
			var btnConfirmarPre = Ext.getCmp('btnConfirmarPre');
			var btnCancelarPre = Ext.getCmp('btnCancelarPre');
			var btnSalirPre = Ext.getCmp('btnSalirPre');
			
			btnGenerarPDFAcuse.show();
			btnBajarPDFAcuse.hide();
			btnGenerarCSVAcuse.show();
			btnBajarCSVAcuse.hide();
			btnCancelarPre.hide();
			btnConfirmarPre.hide();
			btnSalirPre.show();
			
			Ext.Ajax.request({
				url : '29SelecVentaCarteraExt.data.jsp',
				params : {
					informacion: 'GeneraAcuse',
						ic_documento:ic_documento,
						ic_tasa:ic_tasa,
						cg_rel_mat:cg_rel_mat,
						fn_puntos:fn_puntos,
						fn_valor_tasa:fn_valor_tasa,
						fn_importe_interes:fn_importe_interes,
						fn_importe_recibir:fn_importe_recibir,
						plazo_credito:plazo_credito,
						fecha_vto:fecha_vto,
						ic_linea_credito:ic_linea_credito,
						monto:monto,
						ic_moneda_docto:ic_moneda_docto,
						ic_moneda_linea:ic_moneda_linea
					},
				callback: procesarSuccessFailureAcuse
			});
			}//bConfirma
		}//if
	}
	
	var procesarConfirmar = function() {
		var  gridEditable = Ext.getCmp('gridEditable');
		var store = gridEditable.getStore();	
		cancelar(); //limpia las variables 
		var registrosPreAcu = []; //para agregar los registros al Preacuse  y acuse 		
		//se recorre grid principal para detectar los documentos seleccionados			
		store.each(function(record) {		
			if(record.data['SELECCION']=='S'){	
				modificado.push(record.data['NO_DOCTO_FINAL']);
				ic_documento.push(record.data['IC_DOCUMENTO']);
				ic_tasa.push(record.data['IC_TASA']);
				cg_rel_mat.push(record.data['CG_REL_MAT']);
				fn_puntos.push(record.data['FN_PUNTOS']);
				fn_valor_tasa.push(record.data['VALOR_TASA_PUNTOS']);		
				fn_importe_interes.push(record.data['MONTO_TASA_INTERES']);
				fn_importe_recibir.push(record.data['MONTO_CREDITO']);
				plazo_credito.push(record.data['PLAZO_FINAL']);
				ic_linea_credito.push(record.data['IC_LINEA_CREDITO_D']);	
				fecha_vto.push( Ext.util.Format.date(record.data['FECHA_VENCIMIENTO'],'d/m/Y'));	
				monto.push(record.data['MONTO']);
				ic_moneda_docto.push(record.data['IC_MONEDA_DOCTO']);
				ic_moneda_linea.push(record.data['IC_MONEDA_LINEA']);
				registrosPreAcu.push(record);
			}
		});
				
		//se le carga informacion al grid de preacuse y al grid de totales
		consultaPreAcuseData.add(registrosPreAcu);
			if(modificado =='') {
				Ext.MessageBox.alert('Mensaje','Debe seleccionar por lo menos un documento para continuar');
				return false;	
			}			
			gridMontos.show();
			gridEditable.hide();
			ctexto.hide();
			fp.hide();
			leyenda.show();
			gridPreAcuse.show();
			gridTotales.hide();
			var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
			var btnBajarCSVAcuse = Ext.getCmp('btnBajarCSVAcuse');
			var btnCancelarPre = Ext.getCmp('btnCancelarPre');
			var btnSalirPre = Ext.getCmp('btnSalirPre');
			
			btnGenerarPDFAcuse.hide();
			btnBajarPDFAcuse.hide();
			btnGenerarCSVAcuse.hide();
			btnBajarCSVAcuse.hide();
			btnSalirPre.hide();
		
		
			//para el Preacuse 	
			var btnConfirmarPre = Ext.getCmp('btnConfirmarPre');			
			btnConfirmarPre.setHandler(function(boton, evento) {	
			var  fLogCesion = new  NE.cesion.FormLoginCesion();
			fLogCesion.setHandlerBtnAceptar(function(){
				Ext.Ajax.request({
					url: '29SelecVentaCarteraExt.data.jsp',
					params: Ext.apply(fLogCesion.getForm().getValues(),{
						informacion: 'ConfirmacionClaves'
					}),
					callback: confirmacionClavesCesion
				});
			});
			
			//se crea ventana de Confirmacion de Clave 
			var	ventanaConfir =  Ext.getCmp('Comfirmacionclave');	
			if (ventanaConfir) {
				ventanaConfir.show();
			} else {							
				new Ext.Window({
					layout: 'fit',
					width: 400,
					height: 200,			
					id: 'Comfirmacionclave',
					modal:true,
					closeAction: 'hide',
					items: [					
						fLogCesion
					],
					title: ''				
				}).show();
			}
		});
	}
	
	
	var procesarCambiosDocumentos = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var ic_documento = registro.get('IC_DOCUMENTO');
		cambioDoctosData.load({params:	{ic_documento:	ic_documento}});
		var ventana = Ext.getCmp('cambioDoctos');
		if (ventana) {
			ventana.destroy();
		}
		new Ext.Window({
			layout: 'fit',
			width: 800,
			height: 200,
			id: 'cambioDoctos',
			closeAction: 'hide',
			items: [
				gridCambiosDoctos
			],
			title: 'Documento'
			}).show();
	}



	var procesarSuccessFailureCSVAcuse = function (opts, success, response) {
		var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
		btnGenerarCSVAcuse.setIconClass('');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSVAcuse = Ext.getCmp('btnBajarCSVAcuse');
			btnBajarCSVAcuse.show();
			btnBajarCSVAcuse.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});			
			btnBajarCSVAcuse.setHandler(function (boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else {
			btnGenerarCSVAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	
	}
	
	//GENERAR ARCHIVO  PDF
	var procesarSuccessFailurePDFAcuse =  function(opts, success, response) {
		var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
		btnGenerarPDFAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			btnBajarPDFAcuse.show();
			btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFAcuse.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	/*
	//GENERAR ARCHIVO  PDF
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	*/
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;		
		Noelementos = jsonData.ElEMENTOS; 
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridEditable.isVisible()) {
				gridEditable.show();
			}	
		}
		
		var btnConfirmar = Ext.getCmp('btnConfirmar');
		//var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		//var btnBajarPDF = Ext.getCmp('btnBajarPDF');
		var btnCancelar = Ext.getCmp('btnCancelar');
	 	
		var el = gridEditable.getGridEl();		
			if(store.getTotalCount() > 0) {
				el.unmask();					
				btnConfirmar.enable();
				//btnGenerarPDF.enable();				
				//btnBajarPDF.hide();
				btnCancelar.enable();		
				ctexto.show();
				gridTotales.show();
			} else {					
				btnConfirmar.disable();
				//btnGenerarPDF.disable();				
				//btnBajarPDF.hide();
				btnCancelar.disable();
				ctexto.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}	
	}
	
		//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	//Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		align: 'center',
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 150,
			sortable : true
		},
		{
			header : 'Informacion',			
			dataIndex : 'informacion',
			width : 350,
			sortable : true,
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 500,
	style: 'margin:0 auto;',
	autoHeight : true,
	title: 'Cifras de Control',
	frame: true
	});
	
	
	
	var procesarCambioDoctosData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var gridCambios = Ext.getCmp('gridCambios');
			var el = gridCambios.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ningun cambio para este documento', 'x-mask');
			}
		}
	}
	
	var cambioDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma03Ext.data.jsp',
		baseParams: {informacion: 'obtenCambioDoctos'},
		fields: [
			{name: 'FECH_CAMBIO', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CD_DESCRIPCION'},
			{name: 'FECH_EMI_ANT', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECH_EMI_NEW', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_MONTO_ANTERIOR',	type: 'float'},
			{name: 'FN_MONTO_NUEVO', 		type: 'float'},
			{name: 'FECH_VENC_ANT', 		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECH_VENC_NEW', 		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'MODO_PLAZO_ANTERIOR'},
			{name: 'MODO_PLAZO_NUEVO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCambioDoctosData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarCambioDoctosData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});
	
	
		var gridCambiosDoctos = {
		xtype: 'grid',
		store: cambioDoctosData,
		id: 'gridCambios',
		columns: [
			{
				header: 'Fecha del Cambio',
				dataIndex: 'FECH_CAMBIO',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Cambio Estatus',
				dataIndex: 'CD_DESCRIPCION',		//CT_CAMBIO_MOTIVO
				align: 'left',	width: 200
			},{
				header: 'Fecha Emision Anterior',
				dataIndex: 'FECH_EMI_ANT',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Fecha Emision Nueva',
				dataIndex: 'FECH_EMI_NEW',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Monto Anterior',
				dataIndex: 'FN_MONTO_ANTERIOR',
				align: 'right',	width: 120,	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto Nuevo',
				dataIndex: 'FN_MONTO_NUEVO',
				align: 'right',	width: 120,	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Fecha Vencimiento Anterior',
				dataIndex: 'FECH_VENC_ANT',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Fecha Vencimiento Nuevo',
				dataIndex: 'FECH_VENC_NEW',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Modalidad de Plazo Anterior',
				dataIndex: 'MODO_PLAZO_ANTERIOR',
				align: 'left',	width: 150
			},{
				header: 'Modalidad de Plazo Nuevo',
				dataIndex: 'MODO_PLAZO_NUEVO',
				align: 'left',	width: 150
			}
		],
		height: 300,
		title: '',
		frame: false,
		loadMask: true
	};
	
	
	
	
		//para mostrar totales  en el PreAcuse y acuse 
	var storeMontosData = new Ext.data.ArrayStore({
		fields: [
			{name: 'totalDoctosMN', type: 'float'},
			{name: 'totalMontoMN', type: 'float'},
			{name: 'totalMontoSIntMN', type: 'float'},
			{name: 'totalInteresMN', type: 'float'},	
					
			{name: 'totalDoctosUSD', type: 'float'},			
			{name: 'totalMontoUSD', type: 'float'},
			{name: 'totalMontoSInUSD', type: 'float'},
			{name: 'totalInteresUSD', type: 'float'},
			
			{name: 'totalDoctosConv', type: 'float'},
			{name: 'totalMontosConv', type: 'float'},
			{name: 'totalMontoSinConv', type: 'float'},
			{name: 'totalInteresConv', type: 'float'}
		  ]
	 });
	
	//esto esta en duda como manejarlo
	var gruposMontos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Moneda nacional', colspan: 4, align: 'center'},
				{header: 'D�lares Americanos', colspan: 4, align: 'center'}	,				
				{header: 'Doctos. en DLL financiados en M.N.', colspan: 4, align: 'center'}					
			]
		]
	});
	
	//Grid del Preacuse y Acuse  
	var gridMontos = new Ext.grid.EditorGridPanel({	
		id: 'gridMontos',		
		store: storeMontosData,
		plugins: gruposMontos,
		hidden: true,
		columns: [		
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosMN',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos.',
				tooltip: 'Monto total de doctos.',
				dataIndex : 'totalMontoMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{							
				header : 'Monto del Financiamiento',
				tooltip: 'Monto del Financiamiento',
				dataIndex : 'totalMontoSIntMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{							
				header : 'Monto intereses',
				tooltip: 'Monto intereses',
				dataIndex : 'totalInteresMN',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},								
			//USD		
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosUSD',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos.',
				tooltip: 'Monto total de doctos.',
				dataIndex : 'totalMontoUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto del Financiamiento',
				tooltip: 'Monto del Financiamiento',
				dataIndex : 'totalMontoSInUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto intereses finales',
				tooltip: 'Monto intereses finales',
				dataIndex : 'totalInteresUSD',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
		//Doctos. en DLL financiados en M.N.
			{							
				header : 'Num. total de doctos.',
				tooltip: 'Num. total de doctos.',
				dataIndex : 'totalDoctosConv',
				width : 150,
				align: 'center',
				hidden: false,	
				sortable : false
			},
			{							
				header : 'Monto total de doctos.',
				tooltip: 'Monto total de doctos.',
				dataIndex : 'totalMontosConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto del Financiamiento',
				tooltip: 'Monto del Financiamiento',
				dataIndex : 'totalMontoSinConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto intereses',
				tooltip: 'Monto intereses',
				dataIndex : 'totalInteresConv',
				width : 150,
				align: 'right',
				hidden: false,	
				sortable : false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 150,
		width: 943,		
		frame: true
	
	});
	
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '29SelecVentaCarteraExt.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'SELECCION'},
			{name: 'NOMBRE_PYME'},
			{name: 'NO_DOCTO_INICAL'},
			{name: 'IC_DOCUMENTO'},			
			{name: 'ACUSE'},
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENCIMIENTO',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'IC_MONEDA_DOCTO'},
			{name: 'IC_MONEDA_LINEA'},
			{name: 'IC_IF'},
			{name: 'TIPO_PISO'},				
			{name: 'MONTO'},
			{name: 'TIPO_CONV'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VALUADO'},
			{name: 'ESTATUS'},
			{name: 'NO_DOCTO_FINAL'},
			{name: 'MONEDA_FINAL'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'MONTO_CREDITO'},			
			{name: 'PLAZO_FINAL'},
			{name: 'FECHA_HOY', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENCREDITO', type: 'date', dateFormat: 'd/m/Y'},			
			{name: 'NOMBRE_IF'},
			{name: 'IC_LINEA_CREDITO'},	
			{name: 'IC_LINEA_CREDITO_D'},			
			{name: 'REFERENCIA'},
			{name: 'IC_TASA'},
			{name: 'CG_REL_MAT'},
			{name: 'FN_PUNTOS'},
			{name: 'VALOR_TASA'},
			{name: 'VALOR_TASA_PUNTOS'},
			{name: 'MONTO_TASA_INTERES'},
			{name: 'MONTO_AUX_INT'},			
			{name: 'MONTO_CAPITAL_INTERES'},
			{name: 'IC_TIPO_FINANCIAMIENTO'},
			{name: 'PLAZO_FINANCIAMIENTO'},
			{name: 'DESCUENTO_AUTOMATICO'},
			{name: 'DIAS_MINIMO'},
			{name: 'DIAS_MAXIMO'},
			{name: 'PLAZO_VALIDO'},
			{name: 'PYME_BLOQ_X_IF'},
			{name: 'DESCUENTO_AFORO'},
			{name: 'MONTO_DESCONTAR'},			
			{name: 'CG_LINEA_CREDITO'},
			{name: 'FECHA_VENC_LINEA',type: 'date', dateFormat: 'd/m/Y'}
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	



	var consultaPreAcuseData = new Ext.data.ArrayStore({
			fields: [
			{name: 'SELECCION'},
			{name: 'NOMBRE_PYME'},
			{name: 'NO_DOCTO_INICAL'},
			{name: 'IC_DOCUMENTO'},			
			{name: 'ACUSE'},
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENCIMIENTO',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'IC_MONEDA_DOCTO'},
			{name: 'IC_MONEDA_LINEA'},
			{name: 'IC_IF'},
			{name: 'TIPO_PISO'},				
			{name: 'MONTO'},
			{name: 'TIPO_CONV'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MONTO_VALUADO'},
			{name: 'ESTATUS'},
			{name: 'NO_DOCTO_FINAL'},
			{name: 'MONEDA_FINAL'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'MONTO_CREDITO'},			
			{name: 'PLAZO_FINAL'},
			{name: 'FECHA_HOY', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENCREDITO', type: 'date', dateFormat: 'd/m/Y'},			
			{name: 'NOMBRE_IF'},
			{name: 'IC_LINEA_CREDITO'},
			{name: 'IC_LINEA_CREDITO_D'},	
			{name: 'REFERENCIA'},
			{name: 'IC_TASA'},
			{name: 'CG_REL_MAT'},
			{name: 'FN_PUNTOS'},
			{name: 'VALOR_TASA'},
			{name: 'VALOR_TASA_PUNTOS'},
			{name: 'MONTO_TASA_INTERES'},
			{name: 'MONTO_AUX_INT'},			
			{name: 'MONTO_CAPITAL_INTERES'},
			{name: 'IC_TIPO_FINANCIAMIENTO'},
			{name: 'PLAZO_FINANCIAMIENTO'},
			{name: 'DESCUENTO_AUTOMATICO'},
			{name: 'DIAS_MINIMO'},
			{name: 'DIAS_MAXIMO'},
			{name: 'PLAZO_VALIDO'},
			{name: 'PYME_BLOQ_X_IF'},
			{name: 'DESCUENTO_AFORO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'MONTO_DIS_SUB_LIMITE'}
		]
	});	
	
	function procesaValoresIniciales(opts, success, response) {			
		var fp = Ext.getCmp('forma');		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);					
			if (jsonValoresIniciales != null){
				Auxiliar = jsonValoresIniciales.Auxiliar;										
				hidNumTasas = jsonValoresIniciales.hidNumTasas;				
				hidNumLineas =jsonValoresIniciales.hidNumLineas;
				monitorLinea = jsonValoresIniciales.monitorLinea;	
				ventaCartera = jsonValoresIniciales.ventaCartera;					
			}	
				fp.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29SelecVentaCarteraExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29SelecVentaCarteraExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var numDoctos =0;	
	var montoSeleccionado =0;
	var saldoDisponible =0;
	var totalDoctosMN =0;
	var totalMontoMN =0;
	var totalMontoDescMN =0;
	var totalInteresMN2=0;
	var totalMontoCreditoMN =0;	
	var totalDoctosUSD=0;
	var totalMontoUSD=0;
	var totalMontoImpUSD=0;
	var totalMontoDescUSD =0;
	var totalInteresUSD2=0;
	var totalMontoCreditoUSD =0;	
	var totalDoctosConv=0;
	var totalMontosConv=0;
	var totalMontoImpConv=0;
	var totalMontoDescConv=0;
	var totalMontoIntConv=0;
	var totalMontoCreditoConv =0;
	var totalInteresConv =0;
	var moneda; 
	var totalDoctos =0; 
	var totalMonto  =0;
	var totalDoctosF  =0;
	var totalMontoF  =0;
	var totalInteresF  =0;
	var monedaUS;
	var totalDoctosUSD  =0;
	var totalMontoUSD   =0;
	var totalDoctosFUSD   =0; 
	var totalMontoFUSD  =0;
	var totalInteresFUSD  =0;
	var totalMontoInteresF =0;
	var totalInteresF  =0;
	var totalConvPesos =0;
	var totalMontoInteresFUSD =0;
		
	//F020-2015	
	var validaFechaLineaCredito =  function(opts, success, response, record) {
		var cg_lineaC 				= record.data['CG_LINEA_CREDITO'];
		var fechaVenLinea 				= Ext.util.Format.date(record.data['FECHA_VENC_LINEA'],'d/m/Y');  	
		var fechaVenDocto =Ext.util.Format.date(record.data['FECHA_VENCIMIENTO'],'d/m/Y'); 
	
		if( cg_lineaC=='S'  &&  datecomp(fechaVenDocto,fechaVenLinea)==1 ) {
			Ext.MessageBox.alert('Mensaje','El documento no puede vencer el d�a '+fechaVenDocto+' por que la L�nea de Cr�dito que tiene con el Intermediario vence el d�a '+fechaVenLinea);			
			return false;
		}
	}
	
	
	var selectModel = new Ext.grid.CheckboxSelectionModel( {
		checkOnly: true,
		listeners: {
		//Para quitar lo selecccionado en el documentos ******************************************
			rowdeselect: function(selectModel, rowIndex, record) {	 
			
				record.data['SELECCION']='N';
				record.commit();	
				
				var monto_credito = record.data['MONTO_CREDITO'];
				var monto =  record.data['MONTO']; 
				var auxMonto = parseFloat(monto_credito);				
													
					if( record.data['IC_MONEDA_DOCTO']=='1') {							
						totalDoctosMN --;
						totalMontoMN		-= parseFloat(record.data['MONTO']);
						var montoSinInteres  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
						totalMontoDescMN	-= montoSinInteres
						totalInteresMN2		-= parseFloat(record.data['MONTO_TASA_INTERES']);				
						totalMontoCreditoMN	-= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
					}				
					if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='54' ){				
						totalDoctosUSD --;
						totalMontoUSD		-= parseFloat(record.data['MONTO']);
						var montoSinInteresUSD  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
						totalMontoDescUSD	-= montoSinInteresUSD;
						totalInteresUSD2		-= parseFloat(record.data['MONTO_TASA_INTERES']);	
						totalMontoCreditoUSD -=parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
					}
					if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){			
						totalDoctosConv --;
						totalMontosConv			-= parseFloat(record.data['MONTO']);
						var montoSinInteresConv  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']);					
						totalMontoDescConv 	-= montoSinInteresConv
						totalInteresConv		-= parseFloat(record.data['MONTO_TASA_INTERES']);
						totalConvPesos			-= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
						totalMontoCreditoConv	-= parseFloat(record.data['MONTO_DESCUENTO']);
					}		
				
						var dataTotales = [	
						[
						totalDoctosMN, totalMontoMN, totalMontoMN, totalInteresMN2, 
						totalDoctosUSD, totalMontoUSD, totalMontoUSD, totalInteresUSD2,
						totalDoctosConv, totalMontosConv, totalMontosConv, totalInteresConv
						]	];
						
					storeMontosData.loadData(dataTotales);
					
					if(record.data['IC_MONEDA_DOCTO']=='1'){								
						 totalMontoInteresF  =  totalInteresMN2; 
						 totalInteresF  =  totalMontoCreditoMN; 				
						}
						if(record.data['IC_MONEDA_DOCTO']=='54'){							 						 
							 totalMontoInteresFUSD =  totalInteresUSD2; 
							 totalInteresFUSD  =  totalMontoCreditoUSD; 
					}	
					
					if(record.data['IC_MONEDA_DOCTO']=='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){							 						 
						totalMontoInteresF =  totalInteresConv; 
						totalInteresF  =  totalConvPesos; 
					}	


					//esto es para obtener los datos del Gid de gridTotalesE 
					var store = gridTotales.getStore();			
					store.each(function(record) {		
						if(record.data['ICMONEDA']=='1'){
						 record.data['MONTOINTERES'] = totalMontoInteresF;
						 record.data['MONTOCAPITALINTERES'] = totalInteresF;
						}
						if(record.data['ICMONEDA']=='54'){	
						 record.data['MONTOINTERES'] = totalMontoInteresFUSD;
						 record.data['MONTOCAPITALINTERES'] = totalInteresFUSD;
						}	
						record.commit();	
					});					
				
							
			},	
			//Para selecccionar los documentos ******************************************
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //Se activa cuando una fila est� seleccionada, return false para cancelar.
							
				if(seleccionaDocto (selectModel, rowIndex, keepExisting, record)) {					
					record.data['SELECCION']= 'S';
					record.commit();	
				}else {					
					record.data['SELECCION']= 'N';
					record.commit();	
				}				
				
				if(validaFechaLineaCredito('', '', '', record)==false) { //F020-2015						
							record.data['SELECCION']= 'N';
							record.commit();	
							return false;		
					}else  {
						record.data['SELECCION']= 'S';
						record.commit();	
					}
					
				if(validaciones(selectModel, rowIndex, keepExisting, record) ) {
					var monto_credito = record.data['MONTO_CREDITO'];
					var monto =  record.data['MONTO']; 
					var auxMonto = parseFloat(monto_credito);				
					
					if(!calculaMontos(selectModel, rowIndex, keepExisting, record)){
						record.data['SELECCION']= 'N';
						record.commit();	
						return false;
					};
					
					sumaTotalesIntereses(selectModel, rowIndex, keepExisting, record);	
		
					if( record.data['IC_MONEDA_DOCTO']=='1') {							
						totalDoctosMN ++;
						totalMontoMN		+= parseFloat(record.data['MONTO']);
						var montoSinInteres  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
						totalMontoDescMN	+= montoSinInteres
						totalInteresMN2		+= parseFloat(record.data['MONTO_TASA_INTERES']);				
						totalMontoCreditoMN	+= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
					}				
					if(record.data['IC_MONEDA_DOCTO'] =='54'  &&  record.data['IC_MONEDA_LINEA'] =='54'){				
						totalDoctosUSD ++;
						totalMontoUSD		+= parseFloat(record.data['MONTO']);
						var montoSinInteresUSD  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
						totalMontoDescUSD	+= montoSinInteresUSD;
						totalInteresUSD2		+= parseFloat(record.data['MONTO_TASA_INTERES']);	
						totalMontoCreditoUSD+=parseFloat(record.data['MONTO_TOTAL_CAPITAL']);  
					}
					if(record.data['IC_MONEDA_DOCTO'] =='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){			
						totalDoctosConv ++;
						totalMontosConv			+= parseFloat(record.data['MONTO']);
					  var montoSinInteresConv  = parseFloat(record.data['MONTO_TOTAL_CAPITAL']) - parseFloat(record.data['MONTO_TASA_INTERES']); 
						totalMontoDescConv 		+= montoSinInteresConv;				
						totalInteresConv		+= parseFloat(record.data['MONTO_TASA_INTERES']);
						totalConvPesos			+= parseFloat(record.data['MONTO_TOTAL_CAPITAL']);   
						totalMontoCreditoConv	+= parseFloat(record.data['MONTO_DESCUENTO']);
					}
						
					var dataTotales = [	
					[
					totalDoctosMN, totalMontoMN, totalMontoMN, totalInteresMN2, 
					totalDoctosUSD, totalMontoUSD, totalMontoUSD, totalInteresUSD2,
					totalDoctosConv, totalMontosConv, totalMontosConv, totalInteresConv
					]	];
					
					storeMontosData.loadData(dataTotales);
					
					
					if(record.data['IC_MONEDA_DOCTO']=='1'){								
						 totalMontoInteresF  =  totalInteresMN2; 
						 totalInteresF  =  totalMontoCreditoMN; 				
						}
						if(record.data['IC_MONEDA_DOCTO']=='54'){							 						 
							 totalMontoInteresFUSD =  totalInteresUSD2; 
							 totalInteresFUSD  =  totalMontoCreditoUSD; 
						}	
						if(record.data['IC_MONEDA_DOCTO']=='54' &&  record.data['IC_MONEDA_LINEA'] =='1'){							 						 
							 totalMontoInteresF =  totalInteresConv; 
							 totalInteresF  =  totalConvPesos; 
						}	
						//esto es para obtener los datos del Gid de gridTotalesE 
						var store = gridTotales.getStore();			
						store.each(function(record) {						
							if(record.data['MONEDA']=='MONEDA NACIONAL'){
							 record.data['MONTOINTERES'] = totalMontoInteresF;
							 record.data['MONTOCAPITALINTERES'] = totalInteresF;
							}
							if(record.data['MONEDA']=='MONEDA USD'){	
							 record.data['MONTOINTERES'] = totalMontoInteresFUSD;
							 record.data['MONTOCAPITALINTERES'] = totalInteresFUSD;
							}	
							record.commit();	
						});
										
					record.data['SELECCION']='S';
					record.commit();
					return true;
				}else {							
					record.data['SELECCION']='N';
					record.commit();	
					return false;
					alert("paso --g-");
				}
			}
		}
	});
	
	
		var gridPreAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridPreAcuse',
		title: 'Selecci�n de Documentos Venta de Cartera',
		hidden: true,
		store: consultaPreAcuseData,
		clicksToEdit: 1,		
		columns: [			
			{
				header: 'FECHA_VENC_FINAL',
				tooltip: 'FECHA_VENC_FINAL',
				dataIndex: 'FECHA_VENC_FINAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'SELECCION',
				tooltip: 'SELECCION',
				dataIndex: 'SELECCION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: '<sup>1</sup> PYME',
				tooltip: 'PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup> Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex: 'NO_DOCTO_INICAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup> Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex: 'ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup> Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>1</sup> Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>1</sup>Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>1</sup>Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: '<sup>1</sup>Moneda.',
				tooltip: 'Moneda.',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: '<sup>1</sup>Monto.',
				tooltip: 'Monto.',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<sup>1</sup>% Descuento Aforo',
				tooltip: '% Descuento Aforo',
				dataIndex: 'DESCUENTO_AFORO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: '<sup>1</sup>Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			
			{
				header: '<sup>1</sup>Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONV',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'			
			},
			{
				header: '<sup>1</sup>Tipo cambio.',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'			
			},
			{
				header: '<sup>1</sup>Monto valuado en pesos.',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
				header: '<sup>1</sup>Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: '<sup>2</sup>N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex: 'NO_DOCTO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'				
			},			
			{
				header: '<sup>2</sup>Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: '<sup>2</sup>Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: '<sup>2</sup>Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,		
				align: 'center'				
			},
			{
				header: '<sup>2</sup>Fecha de operaci�n',
				tooltip: 'Fecha de operaci�n',
				dataIndex: 'FECHA_HOY',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{
				header: '<sup>2</sup>IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,		
				align: 'left'					
			},
			{			
				header: '<sup>2</sup>Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'			
			},
			{
				header: '<sup>2</sup>Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_PUNTOS',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},			
			{
				header: '<sup>2</sup>Monto de intereses',
				tooltip: 'Monto de intereses',
				dataIndex: 'MONTO_TASA_INTERES',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: '<sup>2</sup>Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTO_TOTAL_CAPITAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}									
		]	,
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',	
				{
					xtype: 'button',
					id: 'btnConfirmarPre',
					text: 'Confirmar',
					handler: procesarConfirmar					
				},			
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFAcuse'			
				},
				// Botones para el Acuse
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFAcuse',
					hidden: true
				},				
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVAcuse'			
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSVAcuse',
					hidden: true
				},				
				{
					text: 'Cancelar',
					xtype: 'button',
					id: 'btnCancelarPre',
					handler: function() {						
						window.location = '29SelecVentaCarteraExt.jsp';
					}
				},
				{
					text: 'Salir',
					xtype: 'button',
					id: 'btnSalirPre',
					handler: function() {						
						window.location = '29SelecVentaCarteraExt.jsp';
					}
				}
			]
		}
	});
	
	
	var totalesData = new Ext.data.JsonStore({			
		root : 'registrosT',
		url : '29SelecVentaCarteraExt.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
			/*ic_epo: Ext.getCmp("ic_epo"),
			ic_moneda: Ext.getCmp("ic_moneda"),
			mto_descuento: Ext.getCmp("mto_descuento"),
			cc_acuse: Ext.getCmp("cc_acuse"),
			fn_monto_de: Ext.getCmp("fn_monto_de"),
			fn_monto_a: Ext.getCmp("fn_monto_a"),
			df_fecha_emision_de: Ext.getCmp("df_fecha_emision_de"),
			df_fecha_emision_a: Ext.getCmp("df_fecha_emision_a"),
			ig_numero_docto: Ext.getCmp("ig_numero_docto"),
			df_fecha_venc_de: Ext.getCmp("df_fecha_venc_de"),
			df_fecha_venc_a: Ext.getCmp("df_fecha_venc_a"),
			fecha_publicacion_de: Ext.getCmp("fecha_publicacion_de"),
			fecha_publicacion_a: Ext.getCmp("fecha_publicacion_a"),
			doctos_cambio: Ext.getCmp("doctos_cambio"),
			lineaTodos: Ext.getCmp("lineaTodos")
			*/
		},								
		fields: [			
			{name: 'MONEDA' , mapping: 'MONEDA'},
			{name: 'ICMONEDA' , mapping: 'ICMONEDA'},
			{name: 'TOTAL', mapping: 'TOTAL'},
			{name: 'MONTO', mapping: 'MONTO' },			
			{name: 'TOTAL_MONTO_VALUADO',  mapping: 'TOTAL_MONTO_VALUADO'},	
			{name: 'TOTAL_CREDITOS',  mapping: 'TOTAL_CREDITOS'},	
			{name: 'TOTAL_MONTO_CREDITOS',  mapping: 'TOTAL_MONTO_CREDITOS'},				
			{name: 'MONTOINTERES',  mapping: 'MONTOINTERES'},			
			{name: 'MONTOCAPITALINTERES',  mapping: 'MONTOCAPITALINTERES'},
			{name: 'MONTO_DESCONTAR',  mapping: 'MONTO_DESCONTAR'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});	
	
	//esto esta en duda como manejarlo
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Datos del Documento', colspan: 5, align: 'center'},
					{header: 'Datos del Financiamiento', colspan: 4, align: 'center'}					
				]
			]
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		id: 'gridTotales',
		store: totalesData,
		plugins: grupos,
		margins: '20 0 0 0',
		align: 'center',
		title: 'Totales',		
		hidden: true,
		columns: [	
		{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'			
		},	
		{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTAL',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'MONTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},	
			{
				header: 'Monto a descontar.',
				dataIndex: 'MONTO_DESCONTAR',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},	
			{
				header: 'Monto total valuado en pesos',
				dataIndex: 'TOTAL_MONTO_VALUADO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
		//Finales
			{							
				header : 'Num. total de doctos.',
				dataIndex : 'TOTAL_CREDITOS',
				width : 150,
				align: 'center'
			},
			{							
				header : 'Monto total de doctos.',			
				dataIndex : 'TOTAL_MONTO_CREDITOS',
				width : 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			{							
				header : 'Monto de Intereses',			
				dataIndex : 'MONTOINTERES',
				width : 150,
				align: 'right',				
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto Total de Capital e Inter�s',				
				dataIndex : 'MONTOCAPITALINTERES',
				width : 150,
				align: 'right',				
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
			],
			height: 150,	
			width : 943,
			frame: false
		});
		
	var gridEditable = new Ext.grid.EditorGridPanel({
		id: 'gridEditable',
		title: 'Selecci�n de Documentos Venta de Cartera',
		hidden: true,
		store: consultaData,
		clicksToEdit: 1,
		sm:selectModel,
		columns: [
			selectModel,			
			{
				header: 'SELECCION',
				tooltip: 'SELECCION',
				dataIndex: 'SELECCION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: '<sup>1</sup> PYME',
				tooltip: 'PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup> Num. docto. inicial',
				tooltip: 'Num. docto. inicial',
				dataIndex: 'NO_DOCTO_INICAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup> Num. acuse',
				tooltip: 'Num. acuse',
				dataIndex: 'ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: '<sup>1</sup> Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>1</sup> Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>1</sup>Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: '<sup>1</sup>Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: '<sup>1</sup>Moneda.',
				tooltip: 'Moneda.',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'			
			},
			{
				header: '<sup>1</sup>Monto.',
				tooltip: 'Monto.',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<sup>1</sup>% Descuento Aforo',
				tooltip: '% Descuento Aforo',
				dataIndex: 'DESCUENTO_AFORO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: '<sup>1</sup>Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			
			{
				header: '<sup>1</sup>Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONV',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'			
			},
			{
				header: '<sup>1</sup>Tipo cambio.',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'			
			},
			{
				header: '<sup>1</sup>Monto valuado en pesos.',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
				header: '<sup>1</sup>Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: '<sup>2</sup>N�m. de documento final',
				tooltip: 'N�m. de documento final',
				dataIndex: 'NO_DOCTO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'				
			},			
			{
				header: '<sup>2</sup>Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: '<sup>2</sup>Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: '<sup>2</sup>Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO_FINAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,		
				align: 'center',
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: false
				}		
			},
			{
				header: '<sup>2</sup>Fecha de operaci�n',
				tooltip: 'Fecha de operaci�n',
				dataIndex: 'FECHA_HOY',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{
				header: '<sup>2</sup>IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,		
				align: 'left'					
			},
			{			
				header: '<sup>2</sup>Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left',
				renderer:function(value,metadata,registro){		
					var select = registro.data['SELECCION'];						
					var refe = inicializaTasasXPlazo( value, metadata, registro,'ReferenciaTasa');
					if(select=='S'){
						refe = (value=='')?'':value;
					}
					return refe;
				}	
			},
			{
				header: '<sup>2</sup>Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_PUNTOS',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];	
					var valort = Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'ValorTasaInteres'),'0.0000%');
					if(select=='S'){
						valort =  Ext.util.Format.number((value=='0')?'':value, '0.0000%');					
					}
					return valort;					
				}
			},
			{
				header: '<sup>2</sup>Monto de intereses',
				tooltip: 'Monto de intereses',
				dataIndex: 'MONTO_TASA_INTERES',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];					
					var monto = Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'MontodeIntereses'), '$0,0.00');	
					if(select=='S'){
						monto =  Ext.util.Format.number((value=='0')?'':value, '$0,0.00');
					}
					return monto;
				}
			},		
			{
				header: '<sup>2</sup>Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTO_TOTAL_CAPITAL',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer:function(value,metadata,registro){
					var monto = Ext.util.Format.number( inicializaTasasXPlazo( value, metadata, registro,'MontoCapitalInteres'), '$0,0.00');
					var select = registro.data['SELECCION'];
					if(select=='S'){
						monto =Ext.util.Format.number((value=='0')?'':value, '$0,0.00');	
					}
					return monto;
				}
			},			
			//estos campos deben de ser ocultos 
			{				
				header : 'IC_TASA',  //editable
				tooltip: 'IC_TASA',
				dataIndex : 'IC_TASA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center',
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var tasa = inicializaTasasXPlazo( value, metadata, registro,'ic_tasa');
					if(select=='S'){
						tasa  = (value=='')?'':value;
					}
					return tasa	
				}					
			},
			{				
				header : 'MONTO_AUX_INT',  //editable
				tooltip: 'MONTO_AUX_INT',
				dataIndex : 'MONTO_AUX_INT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var montoAuxi = inicializaTasasXPlazo( value, metadata, registro,'montoAuxi');
					if(select=='S'){
						montoAuxi  = (value=='')?'':value;
					}
						return  Ext.util.Format.number(montoAuxi, '$0,0.00');	
				}	
			},
			{				
				header : 'CG_REL_MAT',  //editable
				tooltip: 'CG_REL_MAT',
				dataIndex : 'CG_REL_MAT',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'right',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var relMat =  inicializaTasasXPlazo( value, metadata, registro,'cg_rel_mat');
					if(select=='S'){
						relMat  = (value=='')?'':value;
					}
					return relMat;	
				}	
			},
			{				
				header : 'FN_PUNTOS',  //editable
				tooltip: 'FN_PUNTOS',
				dataIndex : 'FN_PUNTOS',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center',		
				renderer:function(value,metadata,registro){
					var select = registro.data['SELECCION'];
					var fnPuntos = inicializaTasasXPlazo( value, metadata, registro,'fnPuntos');
					if(select=='S'){
						fnPuntos  = (value=='')?'':value;
					}
					return fnPuntos;	
				}	
			},			
			{				
				header : 'IC_LINEA_CREDITO',  //editable
				tooltip: 'IC_LINEA_CREDITO',
				dataIndex : 'IC_LINEA_CREDITO',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'				
			}	,			
			{				
				header : 'VALOR_TASA',  
				tooltip: 'VALOR_TASA',
				dataIndex : 'VALOR_TASA',
				width : 150,
				sortable : false,
				hidden: true,				
				align: 'center'				
			}	,				
			
			
			{
	       xtype: 'actioncolumn',
				 header: '<sup>2</sup>Cambio Documentos',
				 tooltip: 'Cambio Documentos',
				 dataIndex: '',
         width: 130,
				 align: 'center',
				  items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {								
								this.items[0].tooltip = 'Ver';								
								return 'iconoLupa';											
						},
						handler: procesarCambiosDocumentos
					}
				]	
			}									
		]	,
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		align: 'center',
		frame: false,
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridEditable = e.gridEditable; 
				var campo= e.field;
				if(campo == 'PLAZO_FINAL'){
					var financiamiento=record.data['IC_TIPO_FINANCIAMIENTO'];
					var descuento=record.data['DESCUENTO_AUTOMATICO'];						
					if(financiamiento=='1' ){						
						return false; // no es editable 						
					}else {		
						if(financiamiento=='3'  &&  (descuento=='N'  ||  descuento=='S')){							
							return true; // es editable 						
						} else if (descuento=='N' || ( descuento=='S' || descuentoo=='' ) ){
							return false; // no es editable 					
						}
					}
				}
			},
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
				var record = e.record;
				var gridEditable = e.gridEditable; 
				var campo= e.field;
				 
				var montoInt = inicializaTasasXPlazo( '', '', record,'MontodeIntereses');
				record.data['MONTO_TASA_INTERES']=montoInt;			
								
				var montoCap = inicializaTasasXPlazo( '', '', record,'MontoCapitalInteres');
				record.data['MONTO_TOTAL_CAPITAL']=montoCap;								
				
				var varTasa = inicializaTasasXPlazo( '', '', record,'ValorTasaInteres');
				record.data['VALOR_TASA_PUNTOS']=varTasa;				
	
				var referencia = inicializaTasasXPlazo( '', '', record,'ReferenciaTasa');
				record.data['REFERENCIA']=referencia;	
				
				var montoAuxi = inicializaTasasXPlazo( '', '', record,'montoAuxi');
				record.data['MONTO_AUX_INT']=montoAuxi;	
								
				var tasa =  inicializaTasasXPlazo( '', '', record,'ic_tasa');
				record.data['IC_TASA']=tasa;
					
				var relMat =  inicializaTasasXPlazo( '', '', record,'cg_rel_mat');
				record.data['CG_REL_MAT']=relMat;				
				
				var fnPuntos =  inicializaTasasXPlazo( '', '', record,'fnPuntos');
				record.data['FN_PUNTOS']=fnPuntos;						
								
				if(campo=='PLAZO_FINAL') { 	 		validaplazo('', '', '', record); 			} //cuando se captura el plazo 
				
				record.commit();		
			
			}
		},
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',
				'-',				
				{
					xtype: 'button',
					id: 'btnConfirmar',
					text: 'Confirmar',
					handler: procesarConfirmar					
				},
				/*'-',				
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '29SelecVentaCarteraExtArc.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GenerarArchivo',
								tipoArchivo:'PDF'
							}),
							callback: procesarSuccessFailurePDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				*/
				'-',				
				{
					text: 'Cancelar',
					xtype: 'button',
					id: 'btnCancelar',
					handler: function() {						
						window.location = '29SelecVentaCarteraExt.jsp';
					}
				}
				
			]
		}
	});
			
	
	var elementosForma = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Pyme',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
			{
				xtype: 'combo',
				name: 'ic_pyme',
				id: 'ic_pyme1',
				fieldLabel: 'Distribuidor',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'ic_pyme',
				emptyText: 'Seleccione...',
				forceSelection : true,
				triggerAction : 'all',
				width: 250,
				typeAhead: true,
				minChars : 1,
				store : catalogoPYMEData,
				tpl:'<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}</div>'+
				'</tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
				'</div></tpl></tpl>',
				setNewEmptyText: function(emptyTextMsg){
					this.emptyText = emptyTextMsg;
					this.setValue('');
					this.applyEmptyText();
					this.clearInvalid();								
				},								
				listeners: {
					select: {
						fn: function(combo) {							
							procesaIniciales(combo.getValue());									
						}
					}	
				}			
			},
			{
					xtype: 'displayfield',
					value: 'Moneda:',
					width: 50
				},
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 250,
					minChars : 1,
					allowBlank: true,
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. documento inicial',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
				xtype: 'textfield',
				name: 'ig_numero_docto',
				id: 'ig_numero_docto1',
				fieldLabel: 'Num. documento inicial',
				allowBlank: true,
				hidden: false,
				maxLength: 15,	
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'Monto documento',
					width: 120
				},
				{
					xtype: 'numberfield',
					name: 'fn_monto_de',
					id: 'fn_monto_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',				
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'numberfield',
					name: 'fn_monto_a',
					id: 'fn_monto_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',					
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. acuse de carga',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'cc_acuse',
					id: 'cc_acuse1',
					fieldLabel: 'Num. acuse de carga',
					allowBlank: true,
					hidden: false,
					maxLength: 15,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				},				
				{
					xtype: 'checkbox',
					name: 'mto_descuento',
					id: 'mto_descuento1',
					fieldLabel: 'Monto % de descuento',
					allowBlank: true,
					hidden: false,					
					msgTarget: 'side',
					margins: '0 20 0 0',
					align: 'center'
				},
				{
					xtype: 'displayfield',
					value: 'Monto % de descuento',
					width: 200,
					align: 'left'
				}			
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha emisi�n docto',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_emision_de',
					id: 'df_fecha_emision_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_emision_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_emision_a',
					id: 'df_fecha_emision_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_emision_de1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'S�lo documentos con cambio',
					width:120
				},
				{
					xtype: 'checkbox',
					name: 'doctos_cambio',
					id: 'doctos_cambio1',
					fieldLabel: 'S�lo documentos con cambio',
					allowBlank: true,
					hidden: false,					
					msgTarget: 'side',
					margins: '0 20 0 0',
					value:	'S',
					align: 'center'
				}
				]
			},
					{
			xtype: 'compositefield',
			fieldLabel: 'Fecha vencimiento docto',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_de',
					id: 'df_fecha_venc_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_venc_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_a',
					id: 'df_fecha_venc_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_venc_de1',
					margins: '0 20 0 0'  
				}										
				]
			},
			{
			xtype: 'compositefield',
			fieldLabel: 'Fecha publicaci�n de',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_de',
					id: 'fecha_publicacion_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_publicacion_a1',					
					margins: '0 20 0 0',			
					value:fechaHoy
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_a',
					id: 'fecha_publicacion_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_publicacion_de1',
					margins: '0 20 0 0' ,
					value:fechaHoy
				}
				]
			}			
	];
	
	
	var fp = new Ext.form.FormPanel({
			id: 'forma',
			width: 885,
			title: 'Criterios de B�squeda',
			frame: true,
			collapsible: true,
			titleCollapse: false,
			style: 'margin:0 auto;',
			bodyStyle: 'padding: 6px',
			labelWidth: 170,
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			items: elementosForma,			
			monitorValid: true,
			buttons: [
				{
					text: 'Buscar',
					iconCls: 'icoBuscar',
					formBind: true,
					handler: function(boton, evento) {
						var ic_pyme = Ext.getCmp("ic_pyme1");
						if (Ext.isEmpty(ic_pyme.getValue()) ) {
							ic_pyme.markInvalid('Por favor, especifique la Pyme');
							return;
						}		
						
						var ventanaDoctos = Ext.getCmp('cambioDoctos');
						if (ventanaDoctos) {
							ventanaDoctos.destroy();
						}
						
						fp.el.mask('Enviando...', 'x-mask-loading');					
						
					//consulta para los registros con 
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consulta'
						})
					});
					
					totalesData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ResumenTotales'
						})
					});	
					
					
					}
				},
				{
					text: 'Limpiar',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '29SelecVentaCarteraExt.jsp';
					}
				}
			]			
		});
		
		
	var OperaVenta = new Ext.form.FormPanel({		
		id: 'OperaVenta',
		width: 600,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',	
		style: 'margin:0 auto;',
		defaultType: 'textfield',
		html: OperaVentaCartera.join('')			
	})
	
	var ctexto = new Ext.form.FormPanel({		
		id: 'forma3',
		width: 900,			
		frame: true,		
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		html: texto.join('')			
	});
	
	
	if(ventaCartera=='N'){
		OperaVenta.show();
		fp.hide();
	}else if(ventaCartera=='S'){
		fp.show();
		OperaVenta.hide();
	}
	
		//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			applyTo: 'areaContenido',
			style: 'margin:0 auto;',
			width: 949,
			items: [
				fp,
				OperaVenta,
				NE.util.getEspaciador(20),
				gridEditable,
				gridMontos,
				leyenda,
				NE.util.getEspaciador(20),
				gridTotales,
				gridPreAcuse,				
				gridCifrasControl, 
				NE.util.getEspaciador(20),
				gridCifrasControl,
				ctexto,
				NE.util.getEspaciador(20)				
			]
		});
	
	
	catalogoPYMEData.load();
	catalogoMonedaData.load();
	
	var procesaIniciales =  function(ic_pyme) {	
	fp.el.mask('Cargando Valores Iniciales...', 'x-mask-loading');	
		Ext.Ajax.request({
			url: '29SelecVentaCarteraExt.data.jsp',
			params: {
				informacion: "SeleccionPYME",
				ic_pyme: ic_pyme
			},			
			callback: procesaValoresIniciales				
		});	
	}
	
	
	
} );