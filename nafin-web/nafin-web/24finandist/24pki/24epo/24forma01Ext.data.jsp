<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
String mod_categoria = (request.getParameter("mod_categoria") != null) ? request.getParameter("mod_categoria") : "";
String ic_tipo_financiamiento	= (request.getParameter("ic_tipo_financiamiento")==null)?"":request.getParameter("ic_tipo_financiamiento");
String cg_num_distribuidor	= (request.getParameter("cg_num_distribuidor")==null)?"":request.getParameter("cg_num_distribuidor");
String cmb_categoria	= (request.getParameter("cmb_categoria")==null)?"0":request.getParameter("cmb_categoria");
String ig_plazo_descuento	= (request.getParameter("ig_plazo_descuento")==null)?"":request.getParameter("ig_plazo_descuento");
String ig_numero_docto	= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
String fn_porc_descuento	= (request.getParameter("fn_porc_descuento")==null)?"":request.getParameter("fn_porc_descuento");
String df_fecha_emision	= (request.getParameter("df_fecha_emision")==null)?"":request.getParameter("df_fecha_emision");
String ct_referencia	= (request.getParameter("ct_referencia")==null)?"":request.getParameter("ct_referencia");
String df_fecha_venc	= (request.getParameter("df_fecha_venc")==null)?"":request.getParameter("df_fecha_venc");
String ic_moneda	= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
String fn_monto	= (request.getParameter("fn_monto")==null)?"":request.getParameter("fn_monto");
String txt_fecha_limite_dsc	= (request.getParameter("txt_fecha_limite_dsc")==null)?"":request.getParameter("txt_fecha_limite_dsc");
String negociable	= (request.getParameter("negociable")==null)?"":request.getParameter("negociable");
String proceso	= (request.getParameter("proceso")==null)?"":request.getParameter("proceso");
String accionDetalle	= (request.getParameter("accionDetalle")==null)?"N":request.getParameter("accionDetalle");
String hidID	= (request.getParameter("hidID")==null)?"N":request.getParameter("hidID");
String pantalla	= (request.getParameter("pantalla")==null)?"":request.getParameter("pantalla");
//esta parte es para la pantalla de las linea  de Credito
String seleccionarIFs	= (request.getParameter("seleccionarIFs")==null)?"":request.getParameter("seleccionarIFs");
String auxIcLineaMN	= (request.getParameter("auxIcLineaMN")==null)?"":request.getParameter("auxIcLineaMN");
String auxDescLineaMN	= (request.getParameter("auxDescLineaMN")==null)?"":request.getParameter("auxDescLineaMN");
String auxIcLineaUSD	= (request.getParameter("auxIcLineaUSD")==null)?"":request.getParameter("auxIcLineaUSD");
String auxDescLineaUSD	= (request.getParameter("auxDescLineaUSD")==null)?"":request.getParameter("auxDescLineaUSD");
String hidNumLineas	= (request.getParameter("hidNumLineas")==null)?"":request.getParameter("hidNumLineas");
String acuse	= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String ic_tipo_Pago	= (request.getParameter("ic_tipo_Pago")==null)?"":request.getParameter("ic_tipo_Pago");
String ic_pediodo	= (request.getParameter("ic_pediodo")==null)?"":request.getParameter("ic_pediodo");


	if("".equals(cg_num_distribuidor)) 	cg_num_distribuidor = ic_pyme;
if("EPO MAN0 DISTR".equals(strPerfil)) {
	negociable = "30";   // Pendiente Negociable    F032-2014  
}else if("EPO MAN1 DISTR".equals(strPerfil)) {
	negociable = "28";   // Pre Negociable  F032-2014
}else  {
	if(negociable.equals("S")) negociable ="2";
	if(negociable.equals("N")) negociable ="1";
}
String ses_ic_epo =  iNoCliente;
Calendar fechaEmisPlazo = new GregorianCalendar();

String mod_ig_plazo_descuento ="", 	mod_fn_porc_descuento= "", hid_plazo_descto ="", 	hid_porc_descto ="", 	hid_tipo_param ="", 
				hid_plazo_descto_max ="", hid_porc_descto_max	= "", hid_tipo_param_max ="", txt_fechaEmisPlazo ="", mod_ic_pyme ="",
				mod_ig_numero_docto	="",  mod_df_fecha_emision 	="", 	mod_df_fecha_venc	="", 	mod_ic_moneda	="", 
				mod_fn_monto ="", 	mod_ic_tipo_financiamiento	="", mod_ct_referencia	="", mod_fecha_limite_dsc ="",
				msgError ="", infoRegresar ="", operaVentaCartera ="", DescripcionEstatus ="", bandeVentaCartera ="";
	
Vector cg_campo = new Vector (5);
cg_campo.addElement((request.getParameter("cg_campo1")==null)?"":request.getParameter("cg_campo1"));
cg_campo.addElement((request.getParameter("cg_campo2")==null)?"":request.getParameter("cg_campo2"));
cg_campo.addElement((request.getParameter("cg_campo3")==null)?"":request.getParameter("cg_campo3"));
cg_campo.addElement((request.getParameter("cg_campo4")==null)?"":request.getParameter("cg_campo4"));
cg_campo.addElement((request.getParameter("cg_campo5")==null)?"":request.getParameter("cg_campo5"));
	double montoTotalMN 	= 0;
	double montoTotalDL 	= 0;
	double montoDescuentoMN = 0;
	double montoDescuentoDL = 0;
	int numRegistrosMN = 0;
	int numRegistrosDL = 0;
	double montoValuado = 0;
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();	
	
	HashMap  registrosTot = new HashMap();	
	JSONArray registrosTotales = new JSONArray();	
	JSONObject 	resultado	= new JSONObject();
	
CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
String descuentoAutomatico =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_DESC_AUTOMATICO");
String limiteLineaCredito =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_LIMITE_LINEA_CREDITO");//Fodea 029-2010 Distribuodores Fase III
String obtieneTipoCredito =  BeanParametros.obtieneTipoCredito (iNoCliente); //Fodea 029-2010 Distribuodores Fase III
String NOnegociable =  BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_NO_NEGOCIABLES"); //Fodea 029-2010 Distribuodores Fase III
String ventaCartera =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); //Fodea 029-2010 Distribuodores Fase III
String  hid_fecha_porc_desc = cargaDocto.getParamFechaPorc(iNoCliente);
String parTipoCambio = cargaDocto.getTipoCambio(ses_ic_epo);
boolean operaConversion = cargaDocto.operaConversion(iNoCliente);
String obtieneTipoCredito2 ="";
if(ventaCartera.equals("S")) {  	operaVentaCartera = "S";   }
if (obtieneTipoCredito.equals("A")){ 	obtieneTipoCredito2 = "C";   }
if (obtieneTipoCredito.equals("D")){ 	obtieneTipoCredito2 = "D";   }
if (obtieneTipoCredito.equals("C")){ 	obtieneTipoCredito2 = "C";   }
if (obtieneTipoCredito.equals("F")){ 	obtieneTipoCredito2 = "F";   }

String tipoCredito1 =  BeanParametros.obtieneTipoCredito (iNoCliente); //Fodea 029-2010 Distribuodores Fase III
Vector nombresCampo = new Vector(5);
nombresCampo = cargaDocto.getCamposAdicionales(iNoCliente,false);
int hayCamposAdicionales = nombresCampo.size();
int numeroCampos=  nombresCampo.size();

String  meseSinIntereses = BeanParametros.DesAutomaticoEpo(iNoCliente,"CS_MESES_SIN_INTERESES");
if (meseSinIntereses.equals("N") || !obtieneTipoCredito.equals("C")  ){  ic_tipo_Pago = "1";  }   


/*System.out.println(" -*************************  ");
System.out.println(" parTipoCambio  "+parTipoCambio);
System.out.println(" ic_tipo_financiamiento  "+ic_tipo_financiamiento);
System.out.println(" ic_pyme  "+ic_pyme);
System.out.println(" mod_categoria  "+mod_categoria);
System.out.println(" cg_num_distribuidor  "+cg_num_distribuidor);
System.out.println(" cmb_categoria  "+cmb_categoria);
System.out.println(" ig_plazo_descuento  "+ig_plazo_descuento);
System.out.println(" ig_numero_docto  "+ig_numero_docto);
System.out.println(" fn_porc_descuento  "+fn_porc_descuento);
System.out.println(" df_fecha_emision  "+df_fecha_emision);
System.out.println(" df_fecha_venc  "+df_fecha_venc);
System.out.println(" ct_referencia  "+ct_referencia);
System.out.println(" ic_moneda  "+ic_moneda);
System.out.println(" fn_monto  "+fn_monto);
System.out.println(" txt_fecha_limite_dsc  "+txt_fecha_limite_dsc);
System.out.println(" negociable  "+negociable);
System.out.println(" proceso  "+proceso);
System.out.println(" hidID  "+hidID);
System.out.println(" seleccionarIFs "+seleccionarIFs);
System.out.println(" accionDetalle "+accionDetalle);
System.out.println(" auxIcLineaMN "+auxIcLineaMN);
System.out.println(" auxDescLineaMN "+auxDescLineaMN);
System.out.println(" auxIcLineaUSD "+auxIcLineaUSD);
System.out.println(" auxDescLineaUSD "+auxDescLineaUSD);
System.out.println(" informacion  "+informacion);
System.out.println(" pantalla "+pantalla);
System.out.println("acuse "+acuse);
System.out.println(" -*************************  ");
*/
/************************************************************************************/
// Catalogos  que se Usan 
/***********************************************************************************/
if(informacion.equals("CatalogoCategoria") && !ic_pyme.equals("")  ) {
		
	HashMap info = new HashMap();
	Vector vecFilas = cargaDocto.getCategoriasXEpo(iNoCliente,ic_pyme,"4",mod_categoria);
	
	info = new HashMap();
	info.put("clave", "0");
	info.put("descripcion", "Sin categoria");	
	registros.add(info);
	for(int i=0; i<vecFilas.size(); i++){
		Vector	vecColumnas = (Vector)vecFilas.get(i);
		String clave = vecColumnas.get(0).toString(); 			
		String descripcion = vecColumnas.get(1).toString(); 			
		info = new HashMap();			
		info.put("clave", clave);
		info.put("descripcion", descripcion);	
		registros.add(info);
	}
	
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
}else  if(informacion.equals("CatalogoPYME")  ) {

	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setCampoClave("cpe.cg_num_distribuidor");	
	cat.setCampoDescripcion("cp.cg_razon_social"); 
	cat.setClaveEpo(iNoCliente);	
	cat.setOrden("cp.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
} else  if (informacion.equals("CatalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	


} else  if (informacion.equals("CatalogoModalidad")   ) {	
	
	HashMap info = new HashMap();		
	Vector vecFilas = cargaDocto.getTipoFinanciamientoXEpo(iNoCliente,ic_pyme,ic_tipo_financiamiento);
	for(int i=0;i<vecFilas.size();i++){					
		Vector vecColumnas = (Vector)vecFilas.get(i);
		String sel = vecColumnas.get(2).toString();
		String clave =  vecColumnas.get(0).toString();
		String descripcion = vecColumnas.get(1).toString();
		info = new HashMap();
		info.put("clave", clave);
		info.put("descripcion", descripcion);	
		registros.add(info);
	}
		
	String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consulta);
	resultado.put("ic_tipo_financiamiento", ic_tipo_financiamiento);
	infoRegresar = resultado.toString();

} else  if (informacion.equals("CatalogoIF")  ) {	

	HashMap info = new HashMap();
	
	Vector vecFilas = cargaDocto.getLineas(ses_ic_epo);
		info.put("clave", "0");
		info.put("descripcion", "Seleccione un IF");
		registros.add(info);
	for(int i=0; i<vecFilas.size(); i++){
		Vector	vecColumnas = (Vector)vecFilas.get(i);
		String clave = vecColumnas.get(0).toString(); 			
		String descripcion = vecColumnas.get(1).toString(); 		
		info = new HashMap();			
		info.put("clave", clave);
		info.put("descripcion", descripcion);	
		registros.add(info);
	}
		
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";


/************************************************************************************/
//   Para Obtener los campos parametrizados  de la EPO
/***********************************************************************************/
} else  if (informacion.equals("catalogoPediodoData")  ) {

	Vector  vecParametros    = BeanParametros.getParamEpoa(ses_ic_epo);
   String cmbMeses  = (vecParametros.get(16)==null)?"": vecParametros.get(16).toString().trim(); // Fodea 09-2015
	
	CatalogoPeriodo catalogo = new CatalogoPeriodo();
	catalogo.setCampoClave("IC_PERIODO");
	catalogo.setCampoDescripcion("CD_DESCRIPCIOM");		
	catalogo.setOrden("IC_PERIODO");
	catalogo.setMeses(cmbMeses);
	infoRegresar = catalogo.getJSONElementos();	
	


} else  if (informacion.equals("valoresIniciales")  ) {
	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("numeroCampos",String.valueOf(numeroCampos));
	jsonObj.put("meseSinIntereses", meseSinIntereses);
	jsonObj.put("obtieneTipoCredito", obtieneTipoCredito);

	for(int i=0;i<nombresCampo.size();i++){
		Vector detalleCampo = (Vector) nombresCampo.get(i);
		String nombreCampo = (String) detalleCampo.get(0);
		int numCampo = Integer.parseInt((String)detalleCampo.get(1));
		String tipoDato = (String)detalleCampo.get(2);
		int longitudDato = Integer.parseInt((String)detalleCampo.get(3));
	
		String tiposdDato ="";
		if(tipoDato.equals("A"))  {  tiposdDato = "textfield"; }
		if(tipoDato.equals("N"))  {  tiposdDato = "numberfield"; }
		if(tipoDato.equals("N"))  {  tiposdDato = "numberfield"; }
		String idCampo = "cg_campo"+numCampo;
		
		
		String  campos ="	{ "+
			" xtype: 'compositefield', "+
			" fieldLabel: '"+nombreCampo+"',"+
			"	combineErrors: false,"+
			"	msgTarget: 'side',"+
			" width: 500,"+
			"	id: '"+idCampo+"',"+
			"	items: ["+
			"		{ "+
					"	xtype: '"+tiposdDato+"',"+
					"	name: '"+idCampo+"',"+
					"	id: '"+idCampo+"',"+
					"	fieldLabel: '"+nombreCampo+"',"+
					"	allowBlank: true, "+								
					"	maxLength: "+longitudDato+","+
					"	width: 150,"+
					"	msgTarget: 'side',"+							
					"	margins: '0 20 0 0'  "+
					" },	"+
					" { "+
					" xtype: 'displayfield',"+
					" value: '',"+
					" width: 150 "+
				  " }	"+		
			" ]"+
		"} ";
		
		jsonObj.put(idCampo,campos.toString());					
	
	} //fin del for

	infoRegresar = jsonObj.toString();	
	

/************************************************************************************/
//   Para Obtener los  datos al seleccionar la Categoria
//  Plazo para descuento ,  Porcentaje de descuento, Fecha límite  para tener Descuento:
/***********************************************************************************/

}else  if (informacion.equals("DatosCategoria")  ) {
		
	if(!"".equals(ic_pyme) && !"".equals(df_fecha_emision) 	&&  ("E".equals(hid_fecha_porc_desc) ||"P".equals(hid_fecha_porc_desc)) 	) {
		
		Vector 	vectoraux = cargaDocto.getParametrosClasific(ses_ic_epo,ic_pyme,df_fecha_emision,hid_fecha_porc_desc,cmb_categoria);
		
    if(!"0".equals(cmb_categoria)) {
			mod_ig_plazo_descuento		= (String)vectoraux.get(0);		
			mod_fn_porc_descuento		= (String)vectoraux.get(1);
			hid_plazo_descto			= mod_ig_plazo_descuento;
			hid_porc_descto				= mod_fn_porc_descuento;
			hid_tipo_param				= (String)vectoraux.get(2);
			if("E".equals(hid_fecha_porc_desc) && vectoraux.size()>3) {
				hid_plazo_descto_max		= (String)vectoraux.get(3);
				hid_porc_descto_max			= (String)vectoraux.get(4);
				hid_tipo_param_max			= (String)vectoraux.get(3);
			}
			String dia_		= df_fecha_emision.substring(0, 2);
			String mes_		= df_fecha_emision.substring(3, 5);
			String anyo_	= df_fecha_emision.substring(6, 10);
			fechaEmisPlazo.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));
			if("".equals(hid_plazo_descto_max))
			hid_plazo_descto_max = mod_ig_plazo_descuento;
			fechaEmisPlazo.add(Calendar.DATE, Integer.parseInt(hid_plazo_descto_max));
			txt_fechaEmisPlazo = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaEmisPlazo.getTime());

		}	else {
			mod_ig_plazo_descuento = "0";
			mod_fn_porc_descuento = "0";
			txt_fecha_limite_dsc = df_fecha_emision;			
		}
		
		mod_ic_pyme					= ic_pyme;
		mod_ig_numero_docto			= ig_numero_docto;
		mod_categoria				= cmb_categoria;
		mod_df_fecha_emision 		= df_fecha_emision;
		mod_df_fecha_venc			= df_fecha_venc;
		mod_ic_moneda				= ic_moneda;
		mod_fn_monto				= fn_monto;
		mod_ic_tipo_financiamiento	= ic_tipo_financiamiento;
		mod_ct_referencia			= ct_referencia;
		mod_fecha_limite_dsc 		= txt_fecha_limite_dsc;
		
	}
	
		JSONObject 	jsonObj	= new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("msgError",msgError);	
		jsonObj.put("mod_ig_plazo_descuento",mod_ig_plazo_descuento);	
		jsonObj.put("mod_fn_porc_descuento",mod_fn_porc_descuento);	
		jsonObj.put("mod_fecha_limite_dsc",mod_fecha_limite_dsc);			
		infoRegresar = jsonObj.toString();

	
/************************************************************************************/
//  Validaciones antes de la captura
/***********************************************************************************/	
}else  if (informacion.equals("AntesDECaptura")  ) {
		
	if(ventaCartera.equals("S") ) { // se quita la condición de validar Descuento Automatica determinado por Edgar ya que no se usa
		int no_dia_semana = 0;
		String fechaDeHoy = null;
		Calendar gcDiaFecha = new GregorianCalendar();
		java.util.Date fechaEmisionNvo = Comunes.parseDate(df_fecha_emision);
		gcDiaFecha.setTime(fechaEmisionNvo);
		no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);						
				
		String inhabilEpo = cargaDocto.getDiasInhabilesDes(ses_ic_epo,df_fecha_emision);
		fechaDeHoy 	= Fecha.getFechaActual();
		 
		SimpleDateFormat formatEntrada = new SimpleDateFormat("dd/MM/yyyy");
		Date df_fecha_emision2 = formatEntrada.parse(df_fecha_emision);
		Date fechaDeHoy2 = new Date();
		
		if (  fechaDeHoy2.compareTo(df_fecha_emision2)<0  )  {   
			msgError = "La Fecha de Emisión debe ser igual o menor al día de hoy ";	
		}//venta de Cartera	
		 
	}	
		JSONObject 	jsonObj	= new JSONObject();	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("msgError",msgError);			
		infoRegresar = jsonObj.toString();
		
/************************************************************************************/
//  Captura en la tabla temporal 
/***********************************************************************************/	

} else  if (informacion.equals("Captura")  ) {	
	
		String 	ic_proc_docto ="";
		
		if(proceso.equals("")){
				ic_proc_docto = cargaDocto.getClaveDoctoTmp();
		}else {
			ic_proc_docto = proceso;
		}
	
	String hid_plazo_user ="S";
	if(ic_tipo_financiamiento.equals("2")){
		hid_plazo_user ="N";
	}else {
		if(!ig_plazo_descuento.equals(hid_plazo_descto))
			hid_plazo_user="N";
		else {
			if(!fn_porc_descuento.equals(hid_porc_descto))
				hid_plazo_user ="N";	
			else {
				if(hid_tipo_param.equals("G") || hid_tipo_param.equals("U"))
					hid_plazo_user="N";
				else
					if(cmb_categoria.equals("0") || cmb_categoria.equals(""))
						hid_plazo_user="N";
			}
		}
	}

	if(ventaCartera.equals("S") ) {  // se quita la condición de validar Descuento Automatica determinado por Edgar ya que no se usa
		int no_dia_semana = 0;
		String fechaDeHoy = null;
		Calendar gcDiaFecha = new GregorianCalendar();
		java.util.Date fechaEmisionNvo = Comunes.parseDate(df_fecha_emision);
		gcDiaFecha.setTime(fechaEmisionNvo);
		no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);						
			
		String inhabilEpo = cargaDocto.getDiasInhabilesDes(ses_ic_epo,df_fecha_emision);
		fechaDeHoy 	= Fecha.getFechaActual();
		 
		SimpleDateFormat formatEntrada = new SimpleDateFormat("dd/MM/yyyy");
		Date df_fecha_emision2 = formatEntrada.parse(df_fecha_emision);
		Date fechaDeHoy2 = new Date();
		if (  fechaDeHoy2.compareTo(df_fecha_emision2)<0  )  {   
			msgError = "La Fecha de Emisión debe ser igual o menor al día de hoy ";	
		}//venta de Cartera	
		
	}//venta de Cartera
			
	if(tipoCredito1.equals("F")){
		ic_tipo_financiamiento="";
	}
	 
	try {
	if(msgError.equals("")){
		
		
		HashMap hmResult = cargaDocto.tempIndividual(df_fecha_venc,df_fecha_emision,ses_ic_epo,ic_proc_docto,ig_numero_docto,cg_num_distribuidor,
			ic_moneda,fn_monto,ct_referencia,cg_campo,ic_tipo_financiamiento,ig_plazo_descuento,fn_porc_descuento,hid_plazo_user,cmb_categoria,
			hid_fecha_porc_desc,negociable,operaVentaCartera, ic_tipo_Pago,ic_pediodo  );
	
		//if(cargaDocto.tempIndividual(df_fecha_venc,df_fecha_emision,ses_ic_epo,ic_proc_docto,ig_numero_docto,cg_num_distribuidor,ic_moneda,fn_monto,ct_referencia,cg_campo,ic_tipo_financiamiento,ig_plazo_descuento,fn_porc_descuento,hid_plazo_user,cmb_categoria,hid_fecha_porc_desc,negociable,operaVentaCartera)){		
		if(((Boolean)hmResult.get("RESULTADO")).booleanValue()){
			System.out.println("--------------------------------------------------valido");
		}else {
			//msgError = cargaDocto.getMsgError();
			msgError = ((String)hmResult.get("MSGERROR"));
			System.out.println("--------------------------------------------------invalido = "+msgError);
			if(msgError.equals("")){
				//msgError="El numero de documento ya existe";
			}
		}		
	}
	}catch(NafinException ne){
		ne.printStackTrace();	
	}catch(Exception e){
		e.printStackTrace();		
	}
	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("msgError",msgError);	
	jsonObj.put("proceso",ic_proc_docto);		
	infoRegresar = jsonObj.toString();	

/************************************************************************************/
//  Consulta despues de la Captura, PreAcuse y Acuse 
/***********************************************************************************/	

} else  if (  
		(informacion.equals("Consulta") || informacion.equals("ResumenTotales")) 
		&&  ( pantalla.equals("PreAcuseDespuesdeDetalle") ||  pantalla.equals("Captura") )  
		) {	
	
	Vector	vecFilas  = cargaDocto.getDoctoInsertado(proceso,ses_ic_epo,null,tipoCredito1);	
	int numElementos = vecFilas.size();	
	for(int i=0;i<vecFilas.size();i++){
		Vector vecColumnas = (Vector)vecFilas.get(i);
		
			String nombrePyme 			= (String)vecColumnas.get(1);
			String clavePyme 			= (String)vecColumnas.get(9);
			String claveDistribuidor 	= (String)vecColumnas.get(0);
			String numeroDocumento 	= (String)vecColumnas.get(2);
			String fechaEmision 		= (String)vecColumnas.get(3);
			String fechaVencimiento 	= (String)vecColumnas.get(4);
			String plazoDocto 			= (String)vecColumnas.get(21);
			String monedaNombre 		= (String)vecColumnas.get(5);
			String monto 				= (String)vecColumnas.get(6);
			String tipoCredito 		= (String)vecColumnas.get(25);
			String respPagoInteres 	= (String)vecColumnas.get(23);
			String modoPlazo 			= (String)vecColumnas.get(22);
			String plazoDescuento 		= (String)vecColumnas.get(13);
			String porcDescuento		= (String)vecColumnas.get(12);
			String montoDescuento 		= (String)vecColumnas.get(26);
			String tipoConversion 		= (String)vecColumnas.get(14);
			String tipoCambio 			= (String)vecColumnas.get(15);
			String referencia 			= (String)vecColumnas.get(7);
			String consecutivo 		= (String)vecColumnas.get(10);
			String icMoneda			= (String)vecColumnas.get(8);
			int numCampos = Integer.parseInt(vecColumnas.get(30).toString());
			String categoria			= (String)vecColumnas.get(31);
      String estatus			= (String)vecColumnas.get(33);
			String plazo_credito		= (String)vecColumnas.get(19);
			String fecha_vto_credito	= (String)vecColumnas.get(20);
			String responsable_interes	= (String)vecColumnas.get(16);
			String dias_minimo			= (String)vecColumnas.get(28);
			String dias_maximo			= (String)vecColumnas.get(29);
			String desEstatus			= (String)vecColumnas.get(35); //F032-2014
		   String desTipoPago			= (String)vecColumnas.get(36); //F09-2015
			
      if (estatus.equals("1")){ //No Negociable Fodea 029-2010 Distribuidores
      DescripcionEstatus = "NO";
      }else if (estatus.equals("2")){ //Negociable
        DescripcionEstatus ="SI";
      }
			bandeVentaCartera			= (String)vecColumnas.get(34);
			if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
			}		
			
			if(montoDescuento==null||"".equals(montoDescuento))
				montoDescuento = "0";
			if ("1".equals(icMoneda)){
				numRegistrosMN++;
				montoValuado = Double.parseDouble(monto);
				if (monto!=null) montoTotalMN += Double.parseDouble(monto);
				if (montoDescuento!=null) montoDescuentoMN += Double.parseDouble(montoDescuento);
			}
			else {//IC_MONEDA=54
				numRegistrosDL++;
				if(operaConversion)
				montoValuado = Double.parseDouble(monto)*Double.parseDouble(tipoCambio);
				if (monto!=null) montoTotalDL += Double.parseDouble(monto);
				if (montoDescuento!=null) montoDescuentoDL += Double.parseDouble(montoDescuento);
			}
			
			if(tipoCredito1.equals("F")){
				categoria ="NA";
				plazoDescuento ="NA";
				porcDescuento="NA";
			}			
			
			datos = new HashMap();
			datos.put("TIPO_CREDITO",tipoCredito1);
			datos.put("CONSECUTIVO",consecutivo);
			datos.put("NUM_DISTRIBUIDOR",claveDistribuidor);
			datos.put("NOMBRE_DISTRIBUIDOR",nombrePyme);
			datos.put("NUN_DOCTOINICIAL",numeroDocumento);
			datos.put("FECHA_EMISION",fechaEmision);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("PLAZO_DOCTO",plazoDocto);
			datos.put("MONEDA",monedaNombre);
			datos.put("MONTO",monto);
			datos.put("CATEGORIA",categoria);
			datos.put("PLAZO_DESC",plazoDescuento);
			datos.put("PORCE_DESC",porcDescuento);
			datos.put("MONTO_DESC",montoDescuento);					
			datos.put("TIPO_CONVERSION",("54".equals(icMoneda))?tipoConversion:"");	
			datos.put("TIPO_CAMBIO",("54".equals(icMoneda))?tipoCambio:"");	
			datos.put("MONTO_VALUADO",("54".equals(icMoneda))?Double.toString(montoValuado):"");	
			datos.put("MODALIDAD",modoPlazo);			
			datos.put("COMENTARIOS",referencia);
			if(hayCamposAdicionales>0) {
				for(int j=0;j<=hayCamposAdicionales;j++){	
					if(j==1) { datos.put("CAMPO1",(String)vecColumnas.get(numCampos+1));  }
					if(j==2) {  datos.put("CAMPO2",(String)vecColumnas.get(numCampos+2)); }
					if(j==3) {  datos.put("CAMPO3",(String)vecColumnas.get(numCampos+3));  }
					if(j==4) { datos.put("CAMPO4",(String)vecColumnas.get(numCampos+4)); }
					if(j==5) {  datos.put("CAMPO5",(String)vecColumnas.get(numCampos+5)); }
				}
			}
			if(!obtieneTipoCredito.equals("D") ){	
			datos.put("PLAZO",plazo_credito);
			datos.put("FECHA_VEN",fecha_vto_credito);	
			}
			if(!"EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("N") ){	
				datos.put("PLAZO",plazo_credito);
			datos.put("FECHA_VEN",fecha_vto_credito);	
		}
		
		if("EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("N") ){	
			datos.put("PLAZO",plazo_credito);
			datos.put("FECHA_VEN",fecha_vto_credito);	
		}

		if("EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("S") ){	
			datos.put("PLAZO","N/A");
			datos.put("FECHA_VEN","N/A");	
		}		

			datos.put("NEGOCIABLE",DescripcionEstatus);					
			datos.put("RESPONSABLE_INTERES",responsable_interes);
			datos.put("DIAS_MINIMO",dias_minimo);
			datos.put("DIAS_MAXIMO",dias_maximo);			
			datos.put("IC_MONEDA",icMoneda);
			datos.put("SELECCIONAR_IFS",seleccionarIFs);
							
			//esta parte es para el Detalle de las Lineas de Credito
			if(seleccionarIFs.equals("S")){
				datos.put("LINEA_REGISTRO","");	//este es para el combo				
			}else if( seleccionarIFs.equals("N")  && "1".equals(icMoneda)){
				datos.put("LINEA_REGISTRO",auxIcLineaMN);	
				datos.put("LINEA_REGISTRO_AUXILIAR",auxDescLineaMN);				
			}else if( seleccionarIFs.equals("N")  &&  "54".equals(icMoneda)){
				datos.put("LINEA_REGISTRO",auxIcLineaUSD);						
			}
			datos.put("CAPTURA_LINEA","N");		
			datos.put("LINEA_REGISTRO_ANTERIOR","0");	
			datos.put("PARTIPOCAMBIO",parTipoCambio);
			datos.put("HID_NUMLINEAS",hidNumLineas);			
			datos.put("CAPTURA_PLAZO","N");
			datos.put("CAPTURA_FECHAVEN","N");
			datos.put("ESTATUS",desEstatus); //F032-2014
			datos.put("IC_MONEDA",icMoneda); //F032-2014
			datos.put("TIPOPAGO",desTipoPago); //F09-2015 
			
						
			if(datos.size()>0){
				registros.add(datos);	
			}			
	} //for
				
	for(int t =0; t<2; t++) {		
		registrosTot = new HashMap();		
			if(t==0){ 				
			registrosTot.put("MONEDA", "MONEDA NACIONAL");			
			registrosTot.put("TOTAL", Double.toString(numRegistrosMN) );	
			registrosTot.put("TOTAL_MONTO", Double.toString(montoTotalMN));	
			registrosTot.put("TOTAL_MONTO_DESC", Double.toString(montoDescuentoMN)  );			
		}
		if(t==1){ 			
			registrosTot.put("MONEDA", "MONEDA USD");
			registrosTot.put("TOTAL", Double.toString(numRegistrosDL) );	
			registrosTot.put("TOTAL_MONTO", Double.toString(montoTotalDL));	
			registrosTot.put("TOTAL_MONTO_DESC", Double.toString(montoDescuentoDL)  );			
		}
		registrosTotales.add(registrosTot);
	}
	
	
	if (informacion.equals("Consulta")) {
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
		resultado = JSONObject.fromObject(consulta);
		for(int i=0;i<nombresCampo.size();i++){
			Vector detalleCampo = (Vector) nombresCampo.get(i);
			String nombreCampo = (String) detalleCampo.get(0);
			String numCampo = ("Campo"+i);				
			resultado.put(numCampo, nombreCampo);				
		}		
		resultado.put("operaConversion", String.valueOf(operaConversion));
		resultado.put("hayCamposAdicionales", String.valueOf(hayCamposAdicionales));
		resultado.put("numRegistrosMN", String.valueOf(numRegistrosMN));
		resultado.put("numRegistrosDL", String.valueOf(numRegistrosDL));
		resultado.put("hidMontoTotalMN", String.valueOf(montoTotalMN));
		resultado.put("hidMontoTotalDL", String.valueOf(montoTotalDL));
		resultado.put("numElementos", String.valueOf(numElementos));
		resultado.put("pantalla", pantalla);	
		resultado.put("strPerfil", strPerfil);
		resultado.put("meseSinIntereses", meseSinIntereses);
		resultado.put("obtieneTipoCredito", obtieneTipoCredito);
		infoRegresar = resultado.toString();
	
	
	}else if(informacion.equals("ResumenTotales") ) {
		
		 infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registrosT\": " + registrosTotales.toString()+"}";
	
	}
	
/************************************************************************************/
//  Consulta cuando la EPO Opera Credito Cuenta Corriente
/***********************************************************************************/		
	
} else  if (  (informacion.equals("Consulta") || informacion.equals("ResumenTotales")) &&  pantalla.equals("CreditoCuentaCorriente") ) {	
		
	String num_cliente ="", df_fecha_vto ="", moneda ="", tipo_conversion ="",   tipo_cambio="", fn_porc_desc ="", 
	ic_linea_credito_dm ="", 	plazo_credito	="", fecha_vto_credito ="", ig_plazo_docto ="", modo_plazo ="",
	dias_minimo	="", 	dias_maximo	="";
	int numCampos = 0, num_doctos_usd =0, num_doctos_mn =0;
	double monto_valuado =0, monto_descuento =0, 	fn_monto_total_mn =0, monto_valuado_total_mn =0,
				fn_monto_total_usd =0, monto_valuado_total_usd =0,  fn_monto2 =0;		
		
	Vector	vecFilas = cargaDocto.getDoctoInsertado(proceso,ses_ic_epo,null,obtieneTipoCredito2);
	for(int i=0;i<vecFilas.size();i++){
		Vector vecColumnas = (Vector)vecFilas.get(i);
		num_cliente			= (String)vecColumnas.get(0);
		ig_numero_docto		= (String)vecColumnas.get(2);
		df_fecha_emision	= (String)vecColumnas.get(3);
		df_fecha_vto		= (String)vecColumnas.get(4);
		moneda				= (String)vecColumnas.get(5);
		ic_moneda 			= (String)vecColumnas.get(8);
		fn_monto2			= Double.parseDouble(vecColumnas.get(6).toString());
		tipo_conversion		= (String)vecColumnas.get(14);
		tipo_cambio			= "1".equals(ic_moneda)?"":(String)vecColumnas.get(15);
		monto_valuado		= "".equals(tipo_cambio)?fn_monto2:(Double.parseDouble(tipo_cambio)*fn_monto2);
		ig_plazo_descuento	= (String)vecColumnas.get(13);
		fn_porc_desc		= ("".equals((String)vecColumnas.get(12))||(String)vecColumnas.get(12)==null)?"0":(String)vecColumnas.get(12);
		monto_descuento		= fn_monto2 * (Double.parseDouble(fn_porc_desc)/100);
	//	responsable_interes	= (String)vecColumnas.get(16);
	//	tipo_cobranza		= (String)vecColumnas.get(17);
		ct_referencia		= (String)vecColumnas.get(7);
		ic_linea_credito_dm	= (String)vecColumnas.get(18);
		plazo_credito		= (String)vecColumnas.get(19);
		fecha_vto_credito	= (String)vecColumnas.get(20);
		ig_plazo_docto		= (String)vecColumnas.get(21);
		modo_plazo			= (String)vecColumnas.get(22);
		dias_minimo			= (String)vecColumnas.get(28);
		dias_maximo			= (String)vecColumnas.get(29);
		numCampos = Integer.parseInt(vecColumnas.get(30).toString());
		String desEstatus			= (String)vecColumnas.get(35); //F032-2014
		String desTipoPago			= (String)vecColumnas.get(36); //F09-2015
		
		if("1".equals(ic_moneda)){
			num_doctos_mn++;
			fn_monto_total_mn += fn_monto2;
			monto_valuado_total_mn += monto_descuento;
		}else{
			num_doctos_usd++;
			fn_monto_total_usd += fn_monto2;
			monto_valuado_total_usd += monto_descuento;
		}
	
		datos = new HashMap();
		datos.put("TIPO_CREDITO",tipoCredito1);
		datos.put("CLIENTE",num_cliente);
		datos.put("NUMERO_DOCTO",ig_numero_docto);
		datos.put("FECHA_EMISION",df_fecha_emision);
		datos.put("FECHA_VENCIMIENTO",df_fecha_vto);
		datos.put("PLAZO_DOCTO",ig_plazo_docto);
		datos.put("MONEDA",moneda);
		datos.put("MONTO",Double.toString(fn_monto2) );
		datos.put("PLAZO_DESC",ig_plazo_descuento);
		datos.put("PORCENTAJE",fn_porc_desc);
		datos.put("MONTO_DESC",Double.toString(monto_descuento) );
		datos.put("COMENTARIOS",ct_referencia );
		datos.put("MODALIDAD",modo_plazo );
		datos.put("ESTATUS",desEstatus); //F032-2014
		datos.put("TIPOPAGO",desTipoPago); //F09-2015 
		if(hayCamposAdicionales>0) {
			for(int j=0;j<=hayCamposAdicionales;j++){	
				if(j==1) { datos.put("CAMPO1",(String)vecColumnas.get(numCampos+1));  }
				if(j==2) {  datos.put("CAMPO2",(String)vecColumnas.get(numCampos+2)); }
				if(j==3) {  datos.put("CAMPO3",(String)vecColumnas.get(numCampos+3));  }
				if(j==4) { datos.put("CAMPO4",(String)vecColumnas.get(numCampos+4)); }
				if(j==5) {  datos.put("CAMPO5",(String)vecColumnas.get(numCampos+5)); }
			}
		}
		
		if(datos.size()>0){
			registros.add(datos);	
		}	
	}//FOR
	
	for(int t =0; t<2; t++) {		
		registrosTot = new HashMap();		
			if(t==0){ 				
			registrosTot.put("MONEDA", "MONEDA NACIONAL");			
			registrosTot.put("TOTAL", Double.toString(num_doctos_mn) );	
			registrosTot.put("TOTAL_MONTO", Double.toString(fn_monto_total_mn));	
			registrosTot.put("TOTAL_MONTO_DESC", Double.toString(monto_valuado_total_mn)  );			
		}
		if(t==1){ 			
			registrosTot.put("MONEDA", "MONEDA USD");
			registrosTot.put("TOTAL", Double.toString(num_doctos_usd) );	
			registrosTot.put("TOTAL_MONTO", Double.toString(fn_monto_total_usd));	
			registrosTot.put("TOTAL_MONTO_DESC", Double.toString(monto_valuado_total_usd)  );			
		}
		registrosTotales.add(registrosTot);
	}
	
	
		if (informacion.equals("Consulta")) {
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
		resultado = JSONObject.fromObject(consulta);
		for(int i=0;i<nombresCampo.size();i++){
			Vector detalleCampo = (Vector) nombresCampo.get(i);
			String nombreCampo = (String) detalleCampo.get(0);
			String numCampo = ("Campo"+i);				
			resultado.put(numCampo, nombreCampo);				
		}		
		resultado.put("operaConversion", String.valueOf(operaConversion));
		resultado.put("hayCamposAdicionales", String.valueOf(hayCamposAdicionales));
		resultado.put("strPerfil", strPerfil);
		resultado.put("meseSinIntereses", meseSinIntereses);
		resultado.put("obtieneTipoCredito", obtieneTipoCredito);
		infoRegresar = resultado.toString();
	
	
	}else if(informacion.equals("ResumenTotales") ) {
		
		 infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registrosT\": " + registrosTotales.toString()+"}";
	
	}
	
		
		
/************************************************************************************/
//  Eliminación  de la tabla temporal
/***********************************************************************************/		
}else if (informacion.equals("Eliminar")) {	


	cargaDocto.cancelaCarga(proceso,hidID,ses_ic_epo);

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("proceso", proceso);
	infoRegresar = jsonObj.toString();

/************************************************************************************/
//  Modificación  de la tabla temporal
/***********************************************************************************/	
}else if (informacion.equals("Modificar")) {	
	
	JSONObject 	jsonObj	= new JSONObject();
	Vector 	vecFilas = cargaDocto.getDoctoInsertado(proceso,ses_ic_epo,hidID,tipoCredito1); 
  jsonObj.put("success", new Boolean(true));
  
  if (vecFilas.size()>0){
	Vector vecColumnas = (Vector)vecFilas.get(0);
 	
		mod_ic_pyme					= (String)vecColumnas.get(0);
		mod_ig_numero_docto			= (String)vecColumnas.get(2);
		mod_df_fecha_emision		= (String)vecColumnas.get(3);
		mod_df_fecha_venc			= (String)vecColumnas.get(4);
		mod_ic_moneda				= (String)vecColumnas.get(8);
		mod_fn_monto				= (String)vecColumnas.get(6);
		mod_ct_referencia			= (String)vecColumnas.get(7);
		mod_ig_plazo_descuento		= (String)vecColumnas.get(13);
		mod_fn_porc_descuento		= (String)vecColumnas.get(12);
		mod_ic_tipo_financiamiento	= (String)vecColumnas.get(27);
		mod_categoria				= (String)vecColumnas.get(32);
		int numCampos = Integer.parseInt(vecColumnas.get(30).toString());
		
		
		jsonObj.put("mod_ic_pyme", mod_ic_pyme);
		jsonObj.put("mod_ig_numero_docto", mod_ig_numero_docto);
		jsonObj.put("mod_df_fecha_emision", mod_df_fecha_emision);
		jsonObj.put("mod_df_fecha_venc", mod_df_fecha_venc);
		jsonObj.put("mod_ic_moneda", mod_ic_moneda);
		jsonObj.put("mod_fn_monto", mod_fn_monto);
		jsonObj.put("mod_ct_referencia", mod_ct_referencia);
		jsonObj.put("mod_ig_plazo_descuento", mod_ig_plazo_descuento);
		jsonObj.put("mod_fn_porc_descuento", mod_fn_porc_descuento);		
		jsonObj.put("mod_ic_tipo_financiamiento", mod_ic_tipo_financiamiento);
		jsonObj.put("mod_categoria", mod_categoria);
		jsonObj.put("hidID", hidID);		
		jsonObj.put("proceso", proceso);
		
		for (int j=numCampos+1;j<vecColumnas.size();j++){
			String valorCampos = (String)vecColumnas.get(j); 
			String idCampo = "cg_campo"+j;
			
			jsonObj.put("idCampo", valorCampos);		
		}		
	}
		
	cargaDocto.cancelaCarga(proceso,hidID,ses_ic_epo);
		
	infoRegresar = jsonObj.toString();	
	
/************************************************************************************/
// Resumen de los totales de la pantalla de Preacuse
/***********************************************************************************/
} else  if (informacion.equals("ResumenTotalesDetalle")  ) {	
	 //esto es para el PreAcuse
	 
	int		auxDM	= 0, 	totalDocDM 	= 0, 	totalDocConvDM	= 0,	totalDocDMUSD		= 0, 	totalDocCC = 0, 	totalDocConvCC = 0, 	totalDocCCUSD		= 0;
	double	montoTotalDM		= 0 , 	montoTotalConvDM	= 0, 	montoCDescDM		= 0, 	montoCDescConvDM	= 0,  montoCDescDMUSD		= 0,   montoCDescCC		= 0,
	montoCDescConvCC	= 0, montoCDescCCUSD		= 0, montoTotalDMUSD		= 0, montoTotalCC		= 0, 	montoTotalConvCC	= 0, 	montoTotalCCUSD 	= 0,
	interesTotalDM 		= 0, 	interesTotalDMUSD 	= 0, interesTotalCC 		= 0, interesTotalCCUSD 	= 0;
	String tipoCambio =  cargaDocto.getTipoCambio(ses_ic_epo);	
	
	JSONArray registrosT = new JSONArray();	
	String tipo_Credito ="";
	Vector 	vecFilas = cargaDocto.getDoctoInsertado(proceso,ses_ic_epo,null,tipoCredito1);	
		
	for(int i=0;i<vecFilas.size();i++){
		Vector vecColumnas = (Vector)vecFilas.get(i);
		double fnPorcAux = "".equals(vecColumnas.get(12).toString())?0:Double.parseDouble(vecColumnas.get(12).toString());
		String monedaLinea = (String)vecColumnas.get(24);
		if("D".equals(vecColumnas.get(11).toString())){
			auxDM++;
			if("1".equals(vecColumnas.get(8).toString())){
				totalDocDM ++;
				montoTotalDM += Double.parseDouble(vecColumnas.get(6).toString());
				montoCDescDM += Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
			}
			if("54".equals(vecColumnas.get(8).toString())&&"54".equals(monedaLinea)){
				totalDocDMUSD ++;
				montoTotalDMUSD += Double.parseDouble(vecColumnas.get(6).toString());
				montoCDescDMUSD += Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
			}
			if("54".equals(vecColumnas.get(8).toString())&&"1".equals(monedaLinea)){
				totalDocConvDM ++;
				montoTotalConvDM += Double.parseDouble(vecColumnas.get(6).toString());
				montoCDescConvDM += Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
			}												
		}
		if("C".equals(vecColumnas.get(11).toString())){
			if("1".equals(vecColumnas.get(8).toString())){
				totalDocCC ++;
				montoTotalCC += Double.parseDouble(vecColumnas.get(6).toString());
				montoCDescCC += Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
			}
			if("54".equals(vecColumnas.get(8).toString())){
				totalDocCCUSD ++;
				montoTotalCCUSD += Double.parseDouble(vecColumnas.get(6).toString());
				montoCDescCCUSD +=Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
			}
		}   
		
		//Factoraje con recuerso Fodea 019-2012
		if("F".equals(vecColumnas.get(11).toString())){
			if("1".equals(vecColumnas.get(8).toString())){
				totalDocCC ++;
				montoTotalCC += Double.parseDouble(vecColumnas.get(6).toString());
				montoCDescCC += Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
			}
			if("54".equals(vecColumnas.get(8).toString())){
				totalDocCCUSD ++;
				montoTotalCCUSD += Double.parseDouble(vecColumnas.get(6).toString());
				montoCDescCCUSD +=Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
			}
		}    
	}
			
	if(totalDocDM+totalDocDMUSD+totalDocConvDM>0  &&"S".equals(accionDetalle) ){	
	
		double montoTMN = montoTotalDM-montoCDescDM; 
		double montoTDL =montoTotalDMUSD-montoCDescDMUSD;
		double montoDocto =0, montoCredito =0; 
		if(!"".equals(tipoCambio)){
			montoDocto= montoTotalConvDM*Double.parseDouble(tipoCambio);
			montoCredito = (montoTotalConvDM-montoCDescConvDM)*Double.parseDouble(tipoCambio);
		}
		tipo_Credito ="Descuento";
		datos.put("TIPO_DOCUMENTO", "Descuento");			
		datos.put("TOTALDOCDM", String.valueOf(totalDocDM));	
		datos.put("MONTOTOTALDM", Double.toString(montoTotalDM) );
		datos.put("MONTOTMN", Double.toString(montoTMN));		
		datos.put("TOTALDOCDMUSD",  String.valueOf(totalDocDMUSD));		
		datos.put("MONTOTOTALDMUSD",  Double.toString(montoTotalDMUSD));		
		datos.put("MONTOTDL",  Double.toString(montoTDL));		
		datos.put("TOTALDOCCONVDM",  String.valueOf(totalDocConvDM));			
		datos.put("MONTODOCTO",  Double.toString(montoDocto));	
		datos.put("MONTOCREDITO", Double.toString(montoCredito));	
		
		
		datos.put("TOTAL_DOCTOS_MN", String.valueOf(totalDocDM));	
		datos.put("MONTO_TOTAL_MN", Double.toString(montoTotalDM) );
		datos.put("MONTO_CREDITO_MN", Double.toString(montoTMN));	
		
		datos.put("TOTAL_DOCTOS_DL",  String.valueOf(totalDocDMUSD));		
		datos.put("MONTO_TOTAL_DL",  Double.toString(montoTotalDMUSD));		
		datos.put("MONTO_CREDITO_DL",  Double.toString(montoTDL));		
		
		datos.put("TOTAL_DOCTOS_MNDL",  String.valueOf(totalDocConvDM));			
		datos.put("MONTO_TOTAL_MNDL",  Double.toString(montoDocto));	
		datos.put("MONTO_CREDITO_MNDL", Double.toString(montoCredito));	
		
		//EStos son los de credito Cuenta Corriente
		double montoMNCC= montoTotalCC-montoCDescCC;
		double montoDLCC= montoTotalCCUSD-montoCDescCCUSD;
		double montoDoctoC=0,  montoCreditoC=0;		
		if(!"".equals(tipoCambio)){
		 montoDoctoC =  montoTotalConvCC*Double.parseDouble(tipoCambio);
		 montoCreditoC = (montoTotalConvCC-montoCDescConvCC)*Double.parseDouble(tipoCambio); 
		}
		
		datos.put("TOTALDOCC", String.valueOf(totalDocCC));	
		datos.put("MONTOTOTALCC", Double.toString(montoTotalCC));	
		datos.put("MONTOMNCC", Double.toString(montoMNCC));			
		datos.put("TOTALDOCCUSD", String.valueOf(totalDocCCUSD));			
		datos.put("MONTOTOTALCCUSD",  Double.toString(montoTotalCCUSD));	
		datos.put("MONTODLCC", Double.toString(montoDLCC));		
		datos.put("TOTALDOCCONVCC",  String.valueOf(totalDocConvCC));			
		datos.put("MONTODOCTOC",  Double.toString(montoDoctoC));	
		datos.put("MONTOCREDITOC", Double.toString(montoCreditoC));		
		
	}
	
	if(totalDocCC+totalDocCCUSD+totalDocConvCC>0){
		double montoMNCC= montoTotalCC-montoCDescCC;
		double montoDLCC= montoTotalCCUSD-montoCDescCCUSD;
		double montoDoctoC=0,  montoCreditoC=0;		
		if(!"".equals(tipoCambio)){
		 montoDoctoC =  montoTotalConvCC*Double.parseDouble(tipoCambio);
		 montoCreditoC = (montoTotalConvCC-montoCDescConvCC)*Double.parseDouble(tipoCambio); 
		}

		tipo_Credito ="CCC";
		datos.put("TIPO_DOCUMENTO", "Crédito cuenta corriente");			
		datos.put("TOTALDOCC", String.valueOf(totalDocCC));	
		datos.put("MONTOTOTALCC", Double.toString(montoTotalCC));	
		datos.put("MONTOMNCC", Double.toString(montoMNCC));			
		datos.put("TOTALDOCCUSD", String.valueOf(totalDocCCUSD));			
		datos.put("MONTOTOTALCCUSD",  Double.toString(montoTotalCCUSD));	
		datos.put("MONTODLCC", Double.toString(montoDLCC));		
		datos.put("TOTALDOCCONVCC",  String.valueOf(totalDocConvCC));			
		datos.put("MONTODOCTOC",  Double.toString(montoDoctoC));	
		datos.put("MONTOCREDITOC", Double.toString(montoCreditoC));	
	
		datos.put("TOTAL_DOCTOS_MN", String.valueOf(totalDocCC));	
		datos.put("MONTO_TOTAL_MN", Double.toString(montoTotalCC));	
		datos.put("MONTO_CREDITO_MN", Double.toString(montoMNCC));		
		
		datos.put("TOTAL_DOCTOS_DL", String.valueOf(totalDocCCUSD));			
		datos.put("MONTO_TOTAL_DL",  Double.toString(montoTotalCCUSD));	
		datos.put("MONTO_CREDITO_DL", Double.toString(montoDLCC));		
		
		datos.put("TOTAL_DOCTOS_MNDL",  String.valueOf(totalDocConvCC));			
		datos.put("MONTO_TOTAL_MNDL",  Double.toString(montoDoctoC));	
		datos.put("MONTO_CREDITO_MNDL", Double.toString(montoCreditoC));
		
		//estos son los de Descuento
		double montoTMN = montoTotalDM-montoCDescDM; 
		double montoTDL =montoTotalDMUSD-montoCDescDMUSD;
		double montoDocto =0, montoCredito =0; 
		if(!"".equals(tipoCambio)){
			montoDocto= montoTotalConvDM*Double.parseDouble(tipoCambio);
			montoCredito = (montoTotalConvDM-montoCDescConvDM)*Double.parseDouble(tipoCambio);
		}

		datos.put("TOTALDOCDM", String.valueOf(totalDocDM));	
		datos.put("MONTOTOTALDM", Double.toString(montoTotalDM) );
		datos.put("MONTOTMN", Double.toString(montoTMN));		
		datos.put("TOTALDOCDMUSD",  String.valueOf(totalDocDMUSD));		
		datos.put("MONTOTOTALDMUSD",  Double.toString(montoTotalDMUSD));		
		datos.put("MONTOTDL",  Double.toString(montoTDL));		
		datos.put("TOTALDOCCONVDM",  String.valueOf(totalDocConvDM));			
		datos.put("MONTODOCTO",  Double.toString(montoDocto));	
		datos.put("MONTOCREDITO", Double.toString(montoCredito));	
		
		
	}
	
	if(totalDocCC+totalDocCCUSD+totalDocConvCC>0){
		double montoMNCC= montoTotalCC-montoCDescCC;
		double montoDLCC= montoTotalCCUSD-montoCDescCCUSD;
		double montoDoctoC=0,  montoCreditoC=0;		
		if(!"".equals(tipoCambio)){
		 montoDoctoC =  montoTotalConvCC*Double.parseDouble(tipoCambio);
		 montoCreditoC = (montoTotalConvCC-montoCDescConvCC)*Double.parseDouble(tipoCambio); 
		}

		tipo_Credito ="F";
		datos.put("TIPO_DOCUMENTO", "Factoraje con Recurso");			
		datos.put("TOTALDOCC", String.valueOf(totalDocCC));	
		datos.put("MONTOTOTALCC", Double.toString(montoTotalCC));	
		datos.put("MONTOMNCC", Double.toString(montoMNCC));			
		datos.put("TOTALDOCCUSD", String.valueOf(totalDocCCUSD));			
		datos.put("MONTOTOTALCCUSD",  Double.toString(montoTotalCCUSD));	
		datos.put("MONTODLCC", Double.toString(montoDLCC));		
		datos.put("TOTALDOCCONVCC",  String.valueOf(totalDocConvCC));			
		datos.put("MONTODOCTOC",  Double.toString(montoDoctoC));	
		datos.put("MONTOCREDITOC", Double.toString(montoCreditoC));	
	
		datos.put("TOTAL_DOCTOS_MN", String.valueOf(totalDocCC));	
		datos.put("MONTO_TOTAL_MN", Double.toString(montoTotalCC));	
		datos.put("MONTO_CREDITO_MN", Double.toString(montoMNCC));		
		
		datos.put("TOTAL_DOCTOS_DL", String.valueOf(totalDocCCUSD));			
		datos.put("MONTO_TOTAL_DL",  Double.toString(montoTotalCCUSD));	
		datos.put("MONTO_CREDITO_DL", Double.toString(montoDLCC));		
		
		datos.put("TOTAL_DOCTOS_MNDL",  String.valueOf(totalDocConvCC));			
		datos.put("MONTO_TOTAL_MNDL",  Double.toString(montoDoctoC));	
		datos.put("MONTO_CREDITO_MNDL", Double.toString(montoCreditoC));
		
		//estos son los de Descuento
		double montoTMN = montoTotalDM-montoCDescDM; 
		double montoTDL =montoTotalDMUSD-montoCDescDMUSD;
		double montoDocto =0, montoCredito =0; 
		if(!"".equals(tipoCambio)){
			montoDocto= montoTotalConvDM*Double.parseDouble(tipoCambio);
			montoCredito = (montoTotalConvDM-montoCDescConvDM)*Double.parseDouble(tipoCambio);
		}

		datos.put("TOTALDOCDM", String.valueOf(totalDocDM));	
		datos.put("MONTOTOTALDM", Double.toString(montoTotalDM) );
		datos.put("MONTOTMN", Double.toString(montoTMN));		
		datos.put("TOTALDOCDMUSD",  String.valueOf(totalDocDMUSD));		
		datos.put("MONTOTOTALDMUSD",  Double.toString(montoTotalDMUSD));		
		datos.put("MONTOTDL",  Double.toString(montoTDL));		
		datos.put("TOTALDOCCONVDM",  String.valueOf(totalDocConvDM));			
		datos.put("MONTODOCTO",  Double.toString(montoDocto));	
		datos.put("MONTOCREDITO", Double.toString(montoCredito));	
	}
	
	if(datos.size()>0){
		registrosT.add(datos);
	}
	
	//esto es para saber si se habilita la el boton de confirmar  en el preacuse
	String habConfirma  = "NO", botDetalleVenta = "NO",  botDetalleFacto = "NO", botCCC = "NO",  botDocto = "NO";
	
	if (obtieneTipoCredito.equals("D") && ventaCartera.equals("N")) {
		if(auxDM>0){	
			botDetalleFacto ="SI";	
		}
	}else if( obtieneTipoCredito.equals("D") && ventaCartera.equals("S") )  {
		botDetalleVenta  ="SI";
	}
	
	if (obtieneTipoCredito.equals("C")) {
		if(totalDocCC+totalDocCCUSD>0){
			botCCC = "SI";
		}
	}
	
	if (obtieneTipoCredito.equals("A")) {
		if(totalDocCC+totalDocCCUSD>0){
			botDocto = "SI";
		}
	}

	
	if((("S".equals(accionDetalle)&&auxDM>0)||(totalDocCC+totalDocCCUSD>0&&auxDM==0)||(totalDocCC+totalDocCCUSD>0&&"S".equals(accionDetalle))) ){
		habConfirma= "SI";
	}else if("D".equals(obtieneTipoCredito) && ventaCartera.equals("S")){
		habConfirma= "SI";
	}
	
	
	
	String consulta =  "{\"success\": true, \"total\": \"" + registrosT.size() + "\", \"registros\": " + registrosT.toString()+"}";
	resultado = JSONObject.fromObject(consulta);
	resultado.put("tipoCambio", tipoCambio);
	resultado.put("tipo_Credito", tipoCredito1);	
	resultado.put("obtieneTipoCredito", obtieneTipoCredito);	
	resultado.put("habConfirma", habConfirma);
	resultado.put("botDetalleVenta", botDetalleVenta);
	resultado.put("botDetalleFacto", botDetalleFacto);
	resultado.put("botCCC", botCCC);
	resultado.put("botDocto", botDocto);
	
	infoRegresar = resultado.toString();


/************************************************************************************/
// Monitor de las Lineas de Credito
/***********************************************************************************/

} else  if (informacion.equals("MonitorLineasCredito")  ) {	
//esto es para la seleccion de la linea de credito
	JSONArray registrosT = new JSONArray();		
	int		numLineasMN		= 0;
	int		numLineasUSD	= 0;
	boolean	seleccionarIF	= true; 
		
	Vector vecFilas = cargaDocto.monitorLineas(ses_ic_epo);
	int hidNumLineas1 =vecFilas.size();
	
	for(int i=0;i<vecFilas.size();i++){
		
		Vector 	vecColumnas = (Vector)vecFilas.get(i);
		String ic_linea = (String)vecColumnas.get(0);
		String moneda_linea = (String)vecColumnas.get(4);
		String linea_credito = (String)vecColumnas.get(1);
		String saldo_inicial = (String)vecColumnas.get(2);  
		String saldo_financiado = (String)vecColumnas.get(5);  
		String monto_proceso = (String)vecColumnas.get(6);  
		String saldo_disponible_linea = (String)vecColumnas.get(7);  
		String monto_negociable = (String)vecColumnas.get(8);  	
		String montoSeleccionado ="0";
		String numDoctos ="0"; 
		String saldoDisponible = vecColumnas.get(9).toString();
	 
		if("1".equals(vecColumnas.get(4).toString())){
			numLineasMN ++;
			auxIcLineaMN	= vecColumnas.get(0).toString();
			auxDescLineaMN	= vecColumnas.get(1).toString();
		}
		if("54".equals(vecColumnas.get(4).toString())){
			numLineasUSD ++;
			auxIcLineaUSD	= vecColumnas.get(0).toString();
			auxDescLineaUSD	= vecColumnas.get(1).toString();
		}
		datos = new HashMap();
		datos.put("IC_LINEA", ic_linea);			
		datos.put("MONEDA_LINEA", moneda_linea);	
		datos.put("LINEA_CREDITO", linea_credito);
		datos.put("SALDO_INICIAL", saldo_inicial);			
		datos.put("SALDO_FINANCIADO",saldo_financiado);			
		datos.put("MONTO_PROCESO", monto_proceso);			
		datos.put("SALDO_DISPONIBLE_LINEA", saldo_disponible_linea);			
		datos.put("MONTO_NEGOCIABLE",monto_negociable);			
		datos.put("MONTO_SELECCIONADO", montoSeleccionado);			
		datos.put("NUM_DOCTOS", numDoctos);			
		datos.put("SALDO_DISPONIBLE", saldoDisponible);			
		registrosT.add(datos);
			
	}//for
	
	//determinamos si se va a permitir seleccionar linea de credito o si se selecciona automaticamente
		if(numLineasMN==1&&numLineasUSD==1&&"".equals(parTipoCambio)){
		seleccionarIF	= false;
	}else if(numLineasMN==1&&numLineasUSD==0&&!"".equals(parTipoCambio)){
		seleccionarIF	= false;
		auxIcLineaUSD	= auxIcLineaMN;
		auxDescLineaUSD = auxDescLineaMN;
	}else if(numLineasMN==1&&numLineasUSD==0&&"".equals(parTipoCambio)){
		seleccionarIF	= false;
		auxIcLineaUSD	= "";
		auxDescLineaUSD = "";
	}else if(numLineasMN==0&&numLineasUSD==1){
		seleccionarIF	= false;
		auxIcLineaMN	= "";
		auxDescLineaMN	= "";
	}else{
		seleccionarIF = true;
	}
		
	if(seleccionarIF ==false) seleccionarIFs ="N";
	if(seleccionarIF ==true) seleccionarIFs ="S";

	String consulta =  "{\"success\": true, \"total\": \"" + registrosT.size() + "\", \"registros\": " + registrosT.toString()+"}";
	resultado = JSONObject.fromObject(consulta);
	resultado.put("hidNumLineas", String.valueOf(hidNumLineas1));
	resultado.put("seleccionarIFs", String.valueOf(seleccionarIFs));
	resultado.put("auxIcLineaMN", auxIcLineaMN);
	resultado.put("auxDescLineaMN", auxDescLineaMN);
	resultado.put("auxIcLineaUSD", auxIcLineaUSD);
	resultado.put("auxDescLineaUSD", auxDescLineaUSD);
		
	resultado.put("proceso", proceso);
	infoRegresar = resultado.toString();

/************************************************************************************/
// Confirmar Detalle de las Lineas de Credito
/***********************************************************************************/

} else  if (informacion.equals("ConfirmaDetalle")  ) {
	
	String lineaTodos 	= request.getParameter("lineaTodos");
	String chkTodos		= (request.getParameter("chkTodasLineas")==null)?"N":request.getParameter("chkTodasLineas");
	String	lineaReg[]			=	request.getParameterValues("lineaReg");
	String	plazo[]			=	request.getParameterValues("plazo");
	String	fecha_vto[]			=	request.getParameterValues("fecha_vto");
	String	igNumeroDocto[]			=	request.getParameterValues("igNumeroDocto");
		
	cargaDocto.actualizaDetalle(ses_ic_epo,proceso,lineaTodos,chkTodos,lineaReg,plazo,fecha_vto,igNumeroDocto);
	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("proceso",proceso);
	jsonObj.put("accionDetalle","S");	
	infoRegresar = jsonObj.toString();	

/************************************************************************************/
// Confirmo la Carga de mis documetos  para genetar Acuse
/***********************************************************************************/

} else  if (informacion.equals("ConfirmaCarga")  ) {

	Seguridad s = new Seguridad();
	
	String		_acuse	= "";
	String folioCert = "";
	char getReceipt = 'Y';
	AccesoDB con = new AccesoDB();
		
	String externContent = (request.getParameter("textoFirmado") != null) ? request.getParameter("textoFirmado") : "";
	String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
	String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
	String usuario = 	strLogin+" - "+strNombreUsuario;

	String	totalDocMN = (request.getParameter("totalDocMN")==null)?"":request.getParameter("totalDocMN");
	String	totalMontoMN = (request.getParameter("totalMontoMN")==null)?"":request.getParameter("totalMontoMN");
	String	totalDocUSD = (request.getParameter("totalDocUSD")==null)?"":request.getParameter("totalDocUSD");
	String	totalMontoUSD = (request.getParameter("totalMontoUSD")==null)?"":request.getParameter("totalMontoUSD");
	String tipoCarga = (request.getParameter("tipoCarga")==null)?"M":request.getParameter("tipoCarga");  //M Masiva //I Individual
	
		
	String  mensajeError ="", mensajeEPO  ="EPO";
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		folioCert = "FDIST"+ses_ic_epo;
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
			_acuse = s.getAcuse();				
			acuse = cargaDocto.confirmaCarga(ses_ic_epo,proceso,iNoUsuario,totalDocMN,totalDocUSD,totalMontoMN,totalMontoUSD,_acuse, limiteLineaCredito);
		}
	}			
	if(acuse.equals("")){
		mensajeError = "La autentificación no se llevo acabo con éxito"+s.mostrarError();
	}
	
	if(application.getInitParameter("EpoCemex").equals(iNoCliente)){
		mensajeEPO ="EpoCemex";
	}
	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("acuse",acuse);	
	jsonObj.put("fecha",fechaHoy);	
	jsonObj.put("hora",horaActual);	
	jsonObj.put("usuario",usuario);
	jsonObj.put("proceso",proceso);	
	jsonObj.put("mensajeError",mensajeError);
	jsonObj.put("mensajeEPO",mensajeEPO);	
	infoRegresar = jsonObj.toString();	
	
} else  if (  informacion.equals("Consulta")   && pantalla.equals("Acuse")  ) {	

	String    num_cliente ="",   df_fecha_vto="",  moneda ="",  tipo_conversion ="",  tipo_cambio ="", 
	monto_valuado ="",  fn_porc_desc ="",   responsable_interes="",  ic_linea_credito_dm	= "",
	plazo_credito	="", fecha_vto_credito	="", 	ig_plazo_docto ="", modo_plazo ="", categoria = "", monedaLinea ="",
	estatus ="", cliente ="", desEstatus ="",  desTipoPago ="";
	
	double fn_monto_total =0, monto_valuado_total =0, fnPorcAux =0, fn_monto2 =0, monto_valuado2 =0, monto_descuento =0;
	int numCampos =0;
	Vector vecFilas = cargaDocto.getDoctoInsertado(acuse,tipoCredito1);
	
	int numElementos = vecFilas.size();
	for(int i=0;i<vecFilas.size();i++){
		Vector	vecColumnas = (Vector)vecFilas.get(i);					
		fnPorcAux = "".equals(vecColumnas.get(12).toString())?0:Double.parseDouble(vecColumnas.get(12).toString());
		num_cliente			= (String)vecColumnas.get(0);
		cliente				= (String)vecColumnas.get(1);
		ig_numero_docto		= (String)vecColumnas.get(2);
		df_fecha_emision	= (String)vecColumnas.get(3);
		df_fecha_vto		= (String)vecColumnas.get(4);
		moneda				= (String)vecColumnas.get(5);
		ic_moneda 			= (String)vecColumnas.get(8);
		fn_monto2			= Double.parseDouble(vecColumnas.get(6).toString());
		tipo_conversion		= (String)vecColumnas.get(14);
		tipo_cambio			= "1".equals(ic_moneda)?"":(String)vecColumnas.get(15);
		monto_valuado2		= "".equals(tipo_cambio)?fn_monto2:(Double.parseDouble(tipo_cambio)*fn_monto2);
		ig_plazo_descuento	= (String)vecColumnas.get(13);
		fn_porc_desc		= ("".equals((String)vecColumnas.get(12))||(String)vecColumnas.get(12)==null)?"0":(String)vecColumnas.get(12);
		monto_descuento		= fn_monto2 * (Double.parseDouble(fn_porc_desc)/100);	
		ct_referencia		= (String)vecColumnas.get(7);
		ic_linea_credito_dm	= (String)vecColumnas.get(18);
		plazo_credito		= (String)vecColumnas.get(19);
		fecha_vto_credito	= (String)vecColumnas.get(20);
		ig_plazo_docto		= (String)vecColumnas.get(26);
		modo_plazo 			= (String)vecColumnas.get(27);
		fn_monto_total += fn_monto2;
		monto_valuado_total += monto_valuado2;
		monedaLinea = (String)vecColumnas.get(28);
		categoria = (String)vecColumnas.get(29);
		desEstatus = (String)vecColumnas.get(32);
		desTipoPago = (String)vecColumnas.get(33);
		
    numCampos = 33;    
		for (int r=numCampos+1;r<vecColumnas.size();r++){
    // mod_cg_campo.addElement(vecColumnas.get(r));
    }
	  estatus			= (String)vecColumnas.get(30);
    if (estatus.equals("1")){ //No Negociable
			DescripcionEstatus = "NO";
    }else if (estatus.equals("2")){ //Negociable
			DescripcionEstatus ="SI";
    }
    	
		bandeVentaCartera			= (String)vecColumnas.get(31);
		if (bandeVentaCartera.equals("S") ) {
		modo_plazo ="";
		}
		
		if(tipoCredito1.equals("F")){
			categoria ="NA";
			ig_plazo_descuento ="NA";
			fn_porc_desc="NA";
		}	
		
		datos = new HashMap();
		datos.put("TIPO_CREDITO",tipoCredito1);
		datos.put("NUM_DISTRIBUIDOR",num_cliente);
		datos.put("NOMBRE_DISTRIBUIDOR",cliente);
		datos.put("NUN_DOCTOINICIAL",ig_numero_docto);
		datos.put("FECHA_EMISION",df_fecha_emision);
		datos.put("FECHA_VENCIMIENTO",df_fecha_vto);
		datos.put("PLAZO_DOCTO",ig_plazo_docto);
		datos.put("MONEDA",moneda);
		datos.put("MONTO",Double.toString (fn_monto2));
		datos.put("CATEGORIA",categoria);
		datos.put("PLAZO_DESC",ig_plazo_descuento);
		datos.put("PORCE_DESC",fn_porc_desc);
		datos.put("MONTO_DESC",Double.toString (monto_descuento) );	
		
		datos.put("TIPO_CONVERSION",("54".equals(ic_moneda))?tipo_conversion:"");	
		datos.put("TIPO_CAMBIO",("54".equals(ic_moneda))?tipo_cambio:"");	
		datos.put("MONTO_VALUADO",("54".equals(ic_moneda))?monto_valuado:"");	
		datos.put("MODALIDAD",modo_plazo);			
		datos.put("COMENTARIOS",ct_referencia);		
		
		if(hayCamposAdicionales>0) {
			for(int j=0;j<=hayCamposAdicionales;j++){	
				if(j==1) { datos.put("CAMPO1",(String)vecColumnas.get(numCampos+1));  }
				if(j==2) {  datos.put("CAMPO2",(String)vecColumnas.get(numCampos+2)); }
				if(j==3) {  datos.put("CAMPO3",(String)vecColumnas.get(numCampos+3));  }
				if(j==4) { datos.put("CAMPO4",(String)vecColumnas.get(numCampos+4)); }
				if(j==5) {  datos.put("CAMPO5",(String)vecColumnas.get(numCampos+5)); }
			}
		}	
		
		if(!obtieneTipoCredito.equals("D") ){	
			datos.put("PLAZO",plazo_credito);
			datos.put("FECHA_VEN",fecha_vto_credito);	
		}
		if(!"EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("N") ){	
			datos.put("PLAZO",plazo_credito);
			datos.put("FECHA_VEN",fecha_vto_credito);	
		}
		
		if("EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("N") ){	
			datos.put("PLAZO",plazo_credito);
			datos.put("FECHA_VEN",fecha_vto_credito);	
		}

		if("EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("S") ){	
			datos.put("PLAZO","N/A");
			datos.put("FECHA_VEN","N/A");	
		}		

			datos.put("NEGOCIABLE",DescripcionEstatus);					
			datos.put("RESPONSABLE_INTERES","");
			datos.put("DIAS_MINIMO","");
			datos.put("DIAS_MAXIMO","");			
			datos.put("IC_MONEDA","");
			datos.put("SELECCIONAR_IFS","");				
			datos.put("LINEA_REGISTRO","");		
			datos.put("LINEA_REGISTRO_AUXILIAR","");						
			datos.put("AUXICLINEA_MN","");
			datos.put("AUXICLINEA_USD","");
			datos.put("PARTIPOCAMBIO","");
			datos.put("HID_NUMLINEAS","");
			datos.put("CAPTURA_LINEA","");
			datos.put("CAPTURA_PLAZO","");
			datos.put("CAPTURA_FECHAVEN","");
			datos.put("CONSECUTIVO","");
			datos.put("ESTATUS",desEstatus);
			datos.put("TIPOPAGO",desTipoPago);
			registros.add(datos);	
		} //for
		
		
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
		resultado = JSONObject.fromObject(consulta);
		
		for(int i=0;i<nombresCampo.size();i++){
			Vector detalleCampo = (Vector) nombresCampo.get(i);
			String nombreCampo = (String) detalleCampo.get(0);
			String numCampo = ("Campo"+i);				
			resultado.put(numCampo, nombreCampo);				
		}		
		resultado.put("operaConversion", String.valueOf(operaConversion));
		resultado.put("hayCamposAdicionales", String.valueOf(hayCamposAdicionales));		
		resultado.put("pantalla", pantalla);	
		resultado.put("strPerfil", strPerfil);
		resultado.put("meseSinIntereses",meseSinIntereses); 
		resultado.put("obtieneTipoCredito", obtieneTipoCredito);
		infoRegresar = resultado.toString();		

}else if (informacion.equals("validaPymeBloqXNumDist")) {

	boolean bBloqueada = cargaDocto.validaPymeXEpoBloqXNumProv(cg_num_distribuidor, ses_ic_epo, "4"); 
	
	resultado.put("success", new Boolean(true));
	resultado.put("pymeBloqueada", new Boolean(bBloqueada));
	infoRegresar = resultado.toString();
	
}else if (informacion.equals("validaPymeBloqueada")) {

	boolean bBloqueada = cargaDocto.validaPymeXEpoBloqueda(ses_ic_epo, ic_pyme, "4"); 
	
	resultado.put("success", new Boolean(true));
	resultado.put("pymeBloqueada", new Boolean(bBloqueada));
	infoRegresar = resultado.toString();

}


%>
<%=infoRegresar%>



