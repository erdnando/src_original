<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.sql.*, java.math.*,
	com.netro.distribuidores.*,com.netro.exception.*, javax.naming.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj			=	new JSONObject();
CreaArchivo archivo			=	new CreaArchivo();
String informacion   		=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String ic_pyme					=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String cg_num_distribuidor	=	(request.getParameter("cg_num_distribuidor")==null)?"":request.getParameter("cg_num_distribuidor");
String montoLimitedl			=	(request.getParameter("montoLimitedl")==null)?"":request.getParameter("montoLimitedl");
if (montoLimitedl.equals("")){
	montoLimitedl = "0";
}
try {
	if(informacion.equals("ArchivoPDF")){

		ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
		String nombreArchivo	= Comunes.cadenaAleatoria(16)+".pdf";
		ComunesPDF pdfDoc		= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual		= fechaActual.substring(0,2);
		String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual		= fechaActual.substring(6,10);
		String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		if(!"NAFIN".equals(strTipoUsuario)) {
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		}else  {
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			"",
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			
			
		}

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

		List lDatos = BeanParametros.getLimitePyme(ic_epo, ic_pyme, cg_num_distribuidor);
		for(int i=0;i<lDatos.size();i++) {
			List lRegistro 	= (ArrayList)lDatos.get(i);
			String pyme 		= lRegistro.get(0).toString();
			String nomPyme 	= lRegistro.get(1).toString();
			String limite 		= lRegistro.get(2).toString();
			String acum 		= lRegistro.get(3).toString();
			String mensajeter = lRegistro.get(4).toString();
			String fecha 		= lRegistro.get(5).toString();
			String icusuario 	= lRegistro.get(6).toString();
			String nombre 		= lRegistro.get(7).toString();
			String moneda 		= lRegistro.get(8).toString();
			String limitedl 	= lRegistro.get(9).toString();
			String acumdl 		= lRegistro.get(10).toString();
			String nombreMoneda=(moneda.equals("1") || moneda.equals("0"))?"MONEDA NACIONAL":"DOLARES";
			double disp 		= Double.parseDouble(limite)-Double.parseDouble(acum);
			double porUtil 	= "0".equals(limite)?0:(Double.parseDouble(acum)*100)/Double.parseDouble(limite);
			double dispdl 		= 0;
			double porUtildl 	= 0;
			if(!limitedl.equals("")){
				dispdl			= Double.parseDouble(limitedl)-Double.parseDouble(acumdl);
				porUtildl		= "0".equals(limitedl)?0:(Double.parseDouble(acumdl)*100)/Double.parseDouble(limitedl);
			}
			if(i==0) {
				int numCols = (!"0".equals(montoLimitedl)?10:7);

				float widths[];
				if(!"".equals(montoLimitedl)){
					widths = new float[]{6.0f,6.5f,5.5f,6.0f,5.5f,5.5f,5.5f,5.5f,5.5f,5.5f};
				}else{
					widths = new float[]{6.6f,6.6f,6.6f,6.6f,6.6f,6.6f,6.6f};
				}
				pdfDoc.setTable(numCols,100,widths);
				pdfDoc.setCell("Limite de Línea por PyME","celda01",ComunesPDF.CENTER,numCols);
				pdfDoc.setCell("Distribuidor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Mensaje de terminación de límite de línea","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha/Hora","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Usuario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Límite M.N.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Utilización M.N.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Disponible M.N.","celda01",ComunesPDF.CENTER);
				if(!montoLimitedl.equals("0")){
					pdfDoc.setCell("Monto Límite DLLS.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Utilización DL.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Disponible DL.","celda01",ComunesPDF.CENTER);
				}
			}
			pdfDoc.setCell(nomPyme.replace(',',' '),"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(mensajeter,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(fecha,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(icusuario+" "+nombre,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(limite, 2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoDecimal(porUtil, 2)+" %","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(disp, 2),"formas",ComunesPDF.RIGHT);
			if(!montoLimitedl.equals("0")){
				pdfDoc.setCell(((!limitedl.equals(""))?"$ "+Comunes.formatoDecimal(limitedl, 2):""),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(((!limitedl.equals(""))?Comunes.formatoDecimal(porUtildl, 2)+" %":""),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(((!limitedl.equals(""))?"$ "+Comunes.formatoDecimal(dispdl, 2):""),"formas",ComunesPDF.RIGHT);
			}
		}
		if(lDatos.size()==0) {
			pdfDoc.addText(" ","formas",ComunesPDF.LEFT);
			pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
		}else{
			pdfDoc.addTable();
		}
		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

	}

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>