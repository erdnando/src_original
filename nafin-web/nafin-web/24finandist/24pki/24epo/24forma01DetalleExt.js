
Ext.onReady(function() {
	
	//Variables globales
	var ventaCartera  = Ext.getDom('ventaCartera').value;
	var validaLinea  = Ext.getDom('validaLinea').value;
	var lineaCredito = Ext.getDom('lineaCredito').value;
	var descuentoAutomatico = Ext.getDom('descuentoAutomatico').value;
	var limiteLineaCredito = Ext.getDom('limiteLineaCredito').value;
	var tipoCredito = Ext.getDom('tipoCredito').value;
	var NOnegociable = Ext.getDom('NOnegociable').value;	
	var hidFechaActual = Ext.getDom('hidFechaActual').value; //fecha hoy
	var hid_fecha_porc_desc = Ext.getDom('hid_fecha_porc_desc').value;
	var txttotdoc = Ext.getDom('txttotdoc').value;
	var txtmtodoc = Ext.getDom('txtmtodoc').value;
	var txtmtomindoc = Ext.getDom('txtmtomindoc').value;
	var txtmtomaxdoc = Ext.getDom('txtmtomaxdoc').value;
	var txttotdocdo = Ext.getDom('txttotdocdo').value;
	var txtmtodocdo = Ext.getDom('txtmtodocdo').value;
	var txtmtomindocdo = Ext.getDom('txtmtomindocdo').value;
	var txtmtomaxdocdo = Ext.getDom('txtmtomaxdocdo').value;
	var hidCifrasControl = Ext.getDom('hidCifrasControl').value;
	var campos = Ext.getDom('campos').value;
	var proceso= Ext.getDom('proceso').value;	
	var mensajeEPO =  Ext.getDom('mensajeEPO').value;	
	var parTipoCambio =  Ext.getDom('parTipoCambio').value;
	var tipoCarga = Ext.getDom('tipoCarga').value;
        var operaContrato  = Ext.getDom('operaContrato').value;
        
	var fechaLimite; 	
	var hidID;
	var numElementos =0;
	var numLineas =0;	
	var lineaReg	= [];
	var plazo = [];
	var fecha_vto	= [];
	var igNumeroDocto = [];
	var tamanio =150;
	var mensajes;
	var mensajesIntereses;
	
	var cancelar =  function() { 
		lineaReg	= [];
		plazo = [];
		fecha_vto	= [];
		igNumeroDocto = [];
	}
	

//////////////INICIA VALIDACIONES JAVA SCRIPT //////////////////////////

	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
	
	var objeto = 0;	
	var mensaje = true;

	var revisaEntero =  function( campo) { 
		var numero=campo;
		if (!isdigit(numero)) {
			Ext.MessageBox.alert('Mensaje','El valor no es un n�mero v�lido.');
			return false;
		}	else{
			return true;
		}
	}

	var Tmes = new Array();
	var sCadena;
	Tmes[1] = "31"; Tmes[2] ="28"; Tmes[3]="31"; Tmes[4]="30"; Tmes[5]="31"; 
	Tmes[6]="30";
	Tmes[7] = "31"; Tmes[8] ="31"; Tmes[9]="30"; Tmes[10]="31";Tmes[11]="30"; 
	Tmes[12]="31";

	var fnDesentrama =  function( Cadena) { 
   var Dato;
   sCadena      = Cadena
   tamano       = sCadena.length;
   posicion     = sCadena.indexOf("/");
   Dato         = sCadena.substr(0,posicion);
   cadenanueva  = sCadena.substring(posicion + 1,tamano);
   sCadena      = cadenanueva;
   return Dato;
	}


	var mtdCalculaDias =  function( sFecha,sFecha2) {
		var iResAno,DiaMes,DiasDif=0,iCont=0,iMes,DiaMesA,iBan=0;
    var DiasPar,Dato,VencDia,VencMes,AltaMes,AltaDia;
    sCadena   = sFecha
    AltaDia   = fnDesentrama(sCadena);
    AltaMes  = fnDesentrama(sCadena);
    AltaAno   = sCadena;
    sCadena     = sFecha2
    VencDia     = fnDesentrama(sCadena);
    VencMes     = fnDesentrama(sCadena);
    VencAno     = sCadena;
    Dato  = AltaMes.substr(0,1);
    if (Dato ==0){
			AltaMes = AltaMes.substr(1,2)
		}
    Dato  = AltaDia.substr(0,1);
    if (Dato ==0){
			AltaDia = AltaDia.substr(1,2)
    }
    Dato  = VencDia.substr(0,1);
    if (Dato ==0){
			VencDia = VencDia.substr(1,2)
    }
    Dato  = VencMes.substr(0,1);
		if (Dato ==0){
			VencMes = VencMes.substr(1,2)
    }
    while (VencAno != AltaAno || VencMes != AltaMes || VencDia != AltaDia){
			iCont++;
      while(AltaMes != VencMes || VencAno != AltaAno){
				iBan=1;
			  iResAno = AltaAno % 4;
			  if (iResAno==0){
					Tmes[2] =29;
			  }else{
					Tmes[2] =28;
			  }
			  if (iCont == 1){
					DiaMesA = Tmes[AltaMes];
			    DiasDif = DiaMesA - AltaDia;
			  } else{
					DiaMesA  = Tmes[AltaMes];
			    DiasDif += parseInt(DiaMesA);
			  }
			  if (AltaMes==12){
					AltaAno++;
			    AltaMes=1;
			  }else{
					AltaMes++;
			  }
			  iCont++;
			} // fin de while
      if (AltaMes == VencMes && VencAno == AltaAno){
				if (iBan==0){
					DiasDif = parseInt(VencDia) - parseInt(AltaDia);
					AltaDia = VencDia;
        } else if (iBan=1){
					DiasDif += parseInt(VencDia);
          AltaDia = VencDia;
        }
      }
		}
		return DiasDif;
	}


	var confirma = true;	
	
	function calculaMontos(){
	
		var valorTipoCambio = 0;
		var auxMonto		= 0;		
		var ic_moneda;			 
		var lineaReg; 				
		var hidMonto; 			
		var moneda_linea;	
		var numDoctos;	
		var montoSeleccionado;
		var saldoDisponible;
		var ic_lineaM; 
		var lineaReg_Ante;			
		var  gridDetalle = Ext.getCmp('gridDetalle');
		var  gridMonitor = Ext.getCmp('gridMonitor');		
		var storeD = gridDetalle.getStore();
		var storeM = gridMonitor.getStore();	
				
		if(parTipoCambio!=''){
			valorTipoCambio = parseFloat(parTipoCambio);
		}else{
			parametrizado = false;
		}
		
		storeD.each(function(record) {
			
			ic_moneda = record.data['IC_MONEDA']; 				 
			lineaReg =  record.data['LINEA_REGISTRO']; 				
			lineaReg_Ante = record.data['LINEA_REGISTRO_ANTERIOR'];
			hidMonto = record.data['MONTO']; 
						
			if(record.data['CAPTURA_LINEA']=='N'){
				storeM.each(function(recordM) {
				
					 moneda_linea = recordM.data['MONEDA_LINEA'];	
					 numDoctos = recordM.data['NUM_DOCTOS'];	
					 montoSeleccionado = recordM.data['MONTO_SELECCIONADO'];
					 saldoDisponible = recordM.data['SALDO_DISPONIBLE'];
					 ic_lineaM = recordM.data['IC_LINEA']; 		

					//caso en que ambas lineas y monedas son iguales
						if(lineaReg == ic_lineaM) {
							if(ic_moneda ==moneda_linea  ) {
							auxMonto = hidMonto;					
							}
						}
										
						if(lineaReg == ic_lineaM) {
							if(ic_moneda !=moneda_linea  ) {
								//SEGUNDO CASO: LINEA EN M.N. Y DOCUMENTO EN U.S.D.
								if(moneda_linea=='1'&& ic_moneda=='54'){
									if(parametrizado){
										return monto_docto*valorTipoCambio;
									}else{
										if(mensaje){
											Ext.MessageBox.alert("Mensaje","La EPO no esta parametrizada para conversion de tipo de cambio");
											record.data['CAPTURA_LINEA']='N';	
											record.data['LINEA_REGISTRO'] ='0';
											record.data['LINEA_REGISTRO_ANTERIOR'] ='0';
											mensaje = false;
										}
										auxMonto=  0;
									}			
								}		
																		
								//TERCER CASO: LINEA EN USD Y DOCUMENTO EN M.N.
								if(moneda_linea=='54'&& ic_moneda=='1'){
									if(mensaje){
										Ext.MessageBox.alert("Mensaje","No se pueden seleccionar documentos en M.N. para una l�nea en d�lares");
										record.data['CAPTURA_LINEA']='N';
										record.data['LINEA_REGISTRO'] ='0';
										record.data['LINEA_REGISTRO_ANTERIOR'] ='0';
										mensaje = false;
									}
									auxMonto =0;							
								}
								
								
								//CUARTO CASO: LINEA EN USD Y DOCUMENTO EN M.N.
								if(moneda_linea=='1'&& ic_moneda=='54'){
									if(mensaje){
										Ext.MessageBox.alert("Mensaje","No se pueden seleccionar documentos en M.N. para una l�nea en d�lares");
										record.data['CAPTURA_LINEA']='N';		
										record.data['LINEA_REGISTRO'] ='0';
										record.data['LINEA_REGISTRO_ANTERIOR'] ='0';
										mensaje = false;
									}
									auxMonto =0;							
								}
								
							}
						}
					
					//alert(lineaReg +"=="+ ic_lineaM +"&&"  +moneda_linea+"=="+ic_moneda +"Monto "+auxMonto);
					
						if(lineaReg == ic_lineaM  && moneda_linea==ic_moneda){	
							if(parseFloat(auxMonto)>0){						
								record.data['CAPTURA_LINEA']='S'
								record.data['LINEA_REGISTRO'] =lineaReg;
								record.data['LINEA_REGISTRO_ANTERIOR'] =lineaReg;								
								recordM.data['NUM_DOCTOS']= parseInt(numDoctos)+1;						
								recordM.data['MONTO_SELECCIONADO'] = roundOff(parseFloat(montoSeleccionado)+ parseFloat(auxMonto),2);
								recordM.data['SALDO_DISPONIBLE'] = roundOff(parseFloat(saldoDisponible)- parseFloat(auxMonto),2);							
							}	
						}
													
						if(parseInt(saldoDisponible)<0&&confirma){
							if(confirm("El monto publicado rebasa el saldo disponible de su l�nea. �Desea continuar?")){
								record.data['CAPTURA_LINEA']='N';
								record.data['LINEA_REGISTRO'] ='0';
								record.data['LINEA_REGISTRO_ANTERIOR'] ='0';
								confirma = false;
								mensaje = true;
							}else{
								Ext.MessageBox.alert("Deber� realizar nuevamente la carga de los documentos que desea descontar");
								var pantalla;
								if(tipoCarga =='M'){  pantalla ='24form2Ext.jsp' 		} 
								if(tipoCarga =='I'){  pantalla ='24form1Ext.jsp' 		}								
								document.location.href = pantalla;	
							}
						}		
											
						//alert(lineaReg_Ante+"  !="+ lineaReg+"----"+ic_lineaM+" == "+lineaReg_Ante +"mensaje "+mensaje);
						if(lineaReg_Ante !='0') {
							if(lineaReg!=lineaReg_Ante){

									if(ic_lineaM ==lineaReg_Ante) {
										auxMonto = hidMonto;	
										recordM.data['NUM_DOCTOS']= parseInt(numDoctos)-1;						
										recordM.data['MONTO_SELECCIONADO'] = roundOff(parseFloat(montoSeleccionado)- parseFloat(auxMonto),2);
										recordM.data['SALDO_DISPONIBLE'] = roundOff(parseFloat(saldoDisponible)+ parseFloat(auxMonto),2);	
										record.data['CAPTURA_LINEA']='N';
										record.data['LINEA_REGISTRO'] ='0';
										record.data['LINEA_REGISTRO_ANTERIOR'] ='0';
									}		
							}
						}
					
						recordM.commit();	
						record.commit();
										
				});
				
			}	//if(record.data['CAPTURA_LINEA']=='N'){
			
		});		
		
	}
	
	//cuando no se muestra  el combo de IF
	function calculaMontosNOIF(){
	
		var valorTipoCambio = 0;
		var auxMonto		= 0;
		var ic_moneda;			 
		var lineaReg; 				
		var hidMonto; 			
		var moneda_linea;	
		var numDoctos;	
		var montoSeleccionado;
		var saldoDisponible;
		var ic_lineaM; 
		var lineaReg_Ante;	
		
		var  gridDetalle = Ext.getCmp('gridDetalle');
		var  gridMonitor = Ext.getCmp('gridMonitor');		
		var storeD = gridDetalle.getStore();
		var storeM = gridMonitor.getStore();	
			
		storeD.each(function(record) {
			
			ic_moneda = record.data['IC_MONEDA']; 				 
			lineaReg =  record.data['LINEA_REGISTRO'];			
			hidMonto = record.data['MONTO']; 
						
			if(record.data['CAPTURA_LINEA']=='N'){
				storeM.each(function(recordM) {
				
					 moneda_linea = recordM.data['MONEDA_LINEA'];	
					 numDoctos = recordM.data['NUM_DOCTOS'];	
					 montoSeleccionado = recordM.data['MONTO_SELECCIONADO'];
					 saldoDisponible = recordM.data['SALDO_DISPONIBLE'];
					 ic_lineaM = recordM.data['IC_LINEA']; 		

					//caso en que ambas lineas y monedas son iguales
						if(lineaReg == ic_lineaM) {
							if(ic_moneda ==moneda_linea  ) {
							auxMonto = hidMonto;					
							}
						}							
					
						if(lineaReg == ic_lineaM  && moneda_linea==ic_moneda){	
							if(parseFloat(auxMonto)>0){					
								record.data['CAPTURA_LINEA']='S'			
								recordM.data['NUM_DOCTOS']= parseInt(numDoctos)+1;						
								recordM.data['MONTO_SELECCIONADO'] = roundOff(parseFloat(montoSeleccionado)+ parseFloat(auxMonto),2);
								recordM.data['SALDO_DISPONIBLE'] = roundOff(parseFloat(saldoDisponible)- parseFloat(auxMonto),2);							
							}	
						}					
						recordM.commit();											
				});				
			}	//if(record.data['CAPTURA_LINEA']=='N'){			
		});			
	}
	
	var restaFecha =  function( opts, success, response, registro) {
		var dias_minimo = registro.data['DIAS_MINIMO'];  
		var dias_maximo = registro.data['DIAS_MAXIMO']; 
		var	txtPlazo 		= registro.data['PLAZO']; 
		var	igPlazoDocto 	= registro.data['PLAZO_DOCTO']; 
		var	fechaVto 		= Ext.util.Format.date(registro.data['FECHA_VEN'],'d/m/Y');  ;
		var montoTasaInt;
		var montAuxInt;
		var montoCredito;
		var montoDocto;
		var plazo;
		dias_minimo = parseInt(dias_minimo);
		dias_maximo = parseInt(dias_maximo);

		txtPlazo = mtdCalculaDias(hidFechaActual,fechaVto);
		plazo = parseInt(txtPlazo);
		
		if(objeto==0)
			objeto = 2;
		if(plazo<dias_minimo||plazo>dias_maximo||plazo<parseInt(igPlazoDocto)){
			if(objeto==2){
				Ext.MessageBox.alert("Mensaje","El plazo esta fuera de los rangos permitidos, verifique que no exceda los parametros de\ndias minimos y maximos y que sea mayor que el plazo del documento.\n\t\tDias minimos:\t\t"+dias_minimo+"\n\t\tDias maximos:\t\t"+dias_maximo+"\n\t\tPlazo del documento:\t"+igPlazoDocto);
				registro.data['FECHA_VEN']=''; 
				registro.data['PLAZO']=''; 
				registro.data['CAPTURA_PLAZO']='N'; 
				registro.data['CAPTURA_FECHAVEN']='N';
				return false;
			}else
				return false;
		}else{
			objeto = 0;
			registro.data['FECHA_VEN']=fechaVto; 
			registro.data['PLAZO']=plazo; 
			registro.data['CAPTURA_FECHAVEN']='S'; 
			registro.data['CAPTURA_PLAZO']='S'; 				
		}		
	}

	var sumaFecha =  function( opts, success, response, registro) {
		var dias_minimo = registro.data['DIAS_MINIMO'];  
		var dias_maximo = registro.data['DIAS_MAXIMO']; 
		
		var TDate = new Date();
		var diaMes = TDate.getDate();
		var comboPlazo = registro.data['PLAZO']; 
		var igPlazoDocto =  registro.data['PLAZO_DOCTO']; 
		var fechaVto = registro.data['FECHA_VEN'];
		
		
		
		var plazo;	
		dias_minimo = parseInt(dias_minimo);
		dias_maximo = parseInt(dias_maximo);	
		AddDays = 0;
		if(comboPlazo !="") 	{
			if(revisaEntero(comboPlazo))
				AddDays = comboPlazo;
		}
		diaMes = parseInt(diaMes)+ parseInt(AddDays);
		TDate.setDate(diaMes);
		var CurYear = TDate.getYear();
		var CurDay = TDate.getDate();
		var CurMonth = TDate.getMonth()+1;
		if(CurDay<10)
			CurDay = '0'+CurDay;
		if(CurMonth<10)
			CurMonth = '0'+CurMonth;
		TheDate = CurDay +'/'+CurMonth+'/'+CurYear;
		fechaVto =TheDate;
		plazo = parseInt(comboPlazo);
			
		if(objeto==0)
			objeto = 1;
		if(plazo<dias_minimo||plazo>dias_maximo||plazo<parseInt(igPlazoDocto)){
			if(objeto==1){
				Ext.MessageBox.alert("Mensaje", "El plazo esta fuera de los rangos permitidos, verifique que no exceda los parametros de\ndias minimos y maximos y que sea mayor que el plazo del documento.\n\t\tDias minimos:\t\t"+dias_minimo+"\n\t\tDias maximos:\t\t"+dias_maximo+"\n\t\tPlazo del documento:\t"+igPlazoDocto);		
				registro.data['FECHA_VEN']=''; 
				registro.data['PLAZO']=''; 
				registro.data['CAPTURA_PLAZO']='N'; 
				registro.data['CAPTURA_FECHAVEN']='N'; 
				return false;
			}else
				return false;
		}else{
			objeto=0;
			registro.data['FECHA_VEN']=fechaVto; 
			registro.data['PLAZO']=plazo; 
			registro.data['CAPTURA_PLAZO']='S'; 
			registro.data['CAPTURA_FECHAVEN']='S'; 			
		}			
	}
			
	function calculaFechaLimite(fechaAuto){
		
		var ig_plazo_descuento = Ext.getCmp("ig_plazo_descuento1");
		var txt_fecha_limite_dsc = Ext.getCmp("txt_fecha_limite_dsc1");
		var df_fecha_emision = Ext.getCmp("df_fecha_emision1");
	 
		var fechaAuto1 = Ext.util.Format.date(fechaAuto,'d/m/Y'); 	
				
		if(Ext.isEmpty(ig_plazo_descuento.getValue())) {
			ig_plazo_descuento.markInvalid("Para poder calcular la Fecha Limite para tener Descuento\nes necesario que introduzca el Plazo para Descuento");
			return;
		}
		if(hid_fecha_porc_desc=="P")
			fechaAuto1 = hidFechaActual;			
		var TDate = new Date();
		var dia_aux = parseInt(fechaAuto1.substring(0,2),10);
		TDate.setDate(dia_aux);
		TDate.setMonth(parseInt(fechaAuto1.substring(3,5),10)-1);
		TDate.setYear(parseInt(fechaAuto1.substring(6,10),10));
		TDate.setDate(TDate.getDate()+parseInt(ig_plazo_descuento.getValue(),10));
		var CurYear = TDate.getYear();
		var CurDay = TDate.getDate();
		var CurMonth = TDate.getMonth()+1;
		if(CurDay<10)
			CurDay = '0'+CurDay;
		if(CurMonth<10)
			CurMonth = '0'+CurMonth;
		TheDate = CurDay +'/'+CurMonth+'/'+CurYear;
		
		if (TheDate=="NaN/NaN/NaN"){
			fechaLimite=hidFechaActual;
		}else {
			fechaLimite = TheDate;
		}						
			Ext.getCmp("txt_fecha_limite_dsc1").setValue(fechaLimite);
	}
	
	//esta funcion es para cuando se selecciona la Linea de Credito
	var actualizaCombos =  function( lineaTodos) {
	
		var  gridMonitor = Ext.getCmp('gridMonitor');
		var storeM = gridMonitor.getStore();	
		storeM.each(function(record) {
			var ic_linea = record.data['IC_LINEA'];
			var moneda_linea = record.data['MONEDA_LINEA'];
						
			var  gridDetalle = Ext.getCmp('gridDetalle');
			var store = gridDetalle.getStore();
			store.each(function(record) {
				if(ic_linea ==lineaTodos){
					var moneda_docto = record.data['IC_MONEDA'];
					if(moneda_linea ==moneda_docto) {
						record.data['LINEA_REGISTRO'] = lineaTodos;																					
						record.data['LINEA_REGISTRO_ANTERIOR'] = lineaTodos;	
					}else {
						record.data['LINEA_REGISTRO'] = '0';	
						record.data['LINEA_REGISTRO'] = '0';	
					}
				}
				record.commit();			
				});
		});	
		
		calculaMontos();
		
	}
		
	//////////////TERMINA VALIDACIONES JAVA SCRIPT //////////////////////////
	
	
	function procesarTerminaCaptura(opts, success, response) {	
	var parametros = "pantalla= PreAcuse"+"&proceso="+proceso+"&tipoCarga="+tipoCarga;			
	document.location.href = "24forma01PreAExt.jsp?"+parametros;	
	
	}
	var mensajes2;
	if(tipoCredito=='D' &&  ventaCartera =='N') {
		 mensajes = '<table width="900" >'+
			'<tr><td class="formas" colspan="3">Para los siguientes documentos a publicar para PyME�s quedando la modalidad Descuento, se deber� seleccionar el IF y, si aplica, el plazo del cr�dito.</td></tr>'+ 
			'</table>';
	} else if(  (tipoCredito=='D' &&  ventaCartera =='S') ) {	
			 mensajes = '<table width="900" >'+
			'<tr><td class="formas" colspan="3">Se muestran los siguientes documentos a publicar para las PyME�s que operan bajo la modalidad Descuento y/o Factoraje</td></tr>'+ 
			'</table>';
	} else if( tipoCredito=='C'  ||  tipoCredito=='A') {	
			 mensajes = '<table width="900">'+
			'<tr><td class="formas" colspan="3">Se muestran los siguientes documentos a publicar para las PyME�s que operan bajo la modalidad Cr�dito en Cuenta Corriente</td></tr>'+ 
			'</table>';
	} 
	
	if( tipoCredito=='C' ) {
		mensajesIntereses = '<table width="900">'+
			'<tr><td class="formas" width="900">Los par�metros de esta cadena toman como base el compromiso acordado en la CARTA DE CONSENTIMIENTO, por lo que es una cadena que opera bajo el concepto de Tasa 0% para el SUJETO DE APOYO, donde usted se compromete a cubrir la COMISION POR SERVICIOS FINANCIEROS al INTERMEDIARIO FINANCIERO, de acuerdo a lo pactado en dicho documento y por la(s) operaci�n(es) aqu� plasmadas.</td></tr>'+ 
			'</table>';
	}
	
	/*
	if(mensajeEPO=='EpoCemex'){
		 mensajes2 = '<table width="900">'+
			'<tr><td class="formas" colspan="4">Al transmitir este mensaje de datos para todos los efectos legales, acepto bajo mi responsabilidad, que los datos capturados en la orden de compra son correctos y reales.</td></tr>'+ 
			'</table>';
	}else if(mensajeEPO!='EpoCemex'){
		if(ventaCartera =='S' ) {	
		
			mensajes2 = '<table width="900">'+
				'<tr><td class="formas" width="900">'+
				'Al trasmitir este mensaje de datos y para todos los efectos legales, Usted bajo su responsabilidad, '+
        'est� aceptando se realice el DESCUENTO y/o FACTORAJE CON RECURSO del (los) Documento (s) final (es) que se detallan en est� pantalla, otorgando'+
        'su consentimiento para que en caso de que se den la condiciones se�aladas en el Contrato para Realizar Operaciones de Descuento y/o Factoraje con Recurso, '+
        'se realice la cesi�n de los derechos de cobro a favor del INTERMEDIARIO FINANCIERO que los opere. '+
				'</table>';		
		}else  {
			 mensajes2 = '<table width="900">'+
				'<tr><td class="formas" width="900">'+
				'Al trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, est� solicitando el DESCUENTO y/o FACTORAJE del (los) DOCUMENTOS (S) INICIAL (ES) que se detallan en est� pantalla y que ha publicado electr�nicamente, '+
				'otorgando su consentimiento para que en caso de que se d�n las condiciones se�aladas en el Contrato de Financiamiento a Clientes y Distribuidores, el (LOS) DOCUMENTOS (S) INICIAL (ES) ser�n sustituidos por el (los) DOCUMENTO (S) FINAL (ES), cediendo los '+
				'derechos de cobro a su favor al INTERMEDIARIO FINANCIERO que los opere. '+
				'</table>';	
		}
	}
  */
   
   mensajes2 = '<table width="900">'+
			'<tr><td class="formas" width="900">'+
      'Al trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, est� de acuerdo en que el (los) Documento (s) inicial (es) que se '+
      'detalla (n) en est� pantalla puedan ser operados indistintamente a trav�s de  las siguientes modalidades, en t�rminos de los pactado en el �Contrato de Financiamiento a Clientes y '+
      'Distribuidores� (para conocer las modalidades convenidas favor de revisar su contrato): <br><br> '+
      'a) DESCUENTO y/o FACTORAJE. <br> '+
      'De operarse en esta modalidad y para todos los efectos legales,  bajo su responsabilidad, est� solicitando el DESCUENTO y/o FACTORAJE del (los) Documento (s) inicial (es) que se detallan '+
      'en est� pantalla y que ha publicado electr�nicamente, otorgando su consentimiento para que en caso  de que se den la condiciones se�aladas en el �Contrato de Financiamiento a Clientes y '+
      'Distribuidores�, el (los) Documento (s) inicial (es)  ser� (n) sustituido (s) por el (los) DOCUMENTO (S) FINAL (ES), cediendo los derechos de cobro a su favor al INTERMEDIARIO FINANCIERO que los opere.'+
      '<br><br> '+
      'b) Pago del CLIENTE o DISTRIBUIDOR mediante Tarjeta de Cr�dito. <br>'+
      'De operarse en esta modalidad y para todos los efectos legales, otorga su consentimiento para que sea cubierto el (los) Documento (s) inicial (es) que se detalla (n) en est� pantalla '+
      'y que ha publicado electr�nicamente, en t�rminos del �Contrato de Financiamiento a Clientes y Distribuidores�, mediante la utilizaci�n de una Tarjeta de Cr�dito asociada al CLIENTE o DISTRIBUIDOR.'+
      '<br/><br/>'+
      'Asimismo, en este acto manifiesto bajo protesta de decir verdad, que s� he emitido o emitir� a mi CLIENTE o DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, ' +
      'seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.'+
      '</td></table>';	

   
   if(operaContrato=='S'){
       mensajes2 = '<table width="900">'+
			'<tr><td class="formas" width="900">'+
      'Al transmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, est� publicando el(los) DOCUMENTO(S) INICIAL(ES) que se detallan '+
      'en esta pantalla y que pueden ser objeto de financiamiento, otorgando su consentimiento para que en caso de que se den las condiciones se�aladas en el Contrato de Financiamiento a Clientes y '+
      'Distribuidores, el (los) DOCUMENTO(S) INICIAL(ES) ser�n sustituidos por el (los) DOCUMENTOS(S) FINAL(ES) que deber�n ser cobrados por el INTERMEDIARIO FINANCIERO.'+
      '</td></table>';	
   }
   	
	var mensajesIntereses_1 = new Ext.Container({
		layout: 'table',
		id: 'mensajesIntereses_1',		
		layoutConfig: {
			columns: 100
		},
		
		width:	'943',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden:true,
		items: [			
			{ 
				xtype:   'label',  
				html:		mensajesIntereses, 
				cls:		'x-form-item', 
				style: { 
					width:   		'943%', 
					textAlign: 		'left'
				} 
			}			
		]
	});
	
	
	var mensaje1 = new Ext.Container({
		layout: 'table',
		id: 'mensaje1',		
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		mensajes, 
				cls:		'x-form-item', 
				style: { 
					width:   		'700%', 
					textAlign: 		'left'
				} 
			}			
		]
	});
	
	var mensaje3 = new Ext.Container({
		layout: 'table',
		id: 'mensaje3',		
		layoutConfig: {
			columns: 100
		},
		
		width:	'943',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden:true,
		items: [			
			{ 
				xtype:   'label',  
				html:		mensajes2, 
				cls:		'x-form-item', 
				style: { 
					width:   		'943%', 
					textAlign: 		'left'
				} 
			}			
		]
	});
	
	var procesaRegresaPreacuse =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var datos = Ext.util.JSON.decode(response.responseText);
			var proceso =datos.proceso;		
			var accionDetalle =datos.accionDetalle;		
			var fp = Ext.getCmp('forma');
			var parametros = "pantalla= PreAcuse"+"&proceso="+proceso+"&accionDetalle="+accionDetalle+"&tipoCarga="+tipoCarga;			
			document.location.href = "24forma01PreAExt.jsp?"+parametros;	
			
		}
	}
	
	
	//confirmacion del detalle 
	function procesarConfirmaDetalle(opts, success, response) {
		var  gridDetalle = Ext.getCmp('gridDetalle');
		var store = gridDetalle.getStore();
		cancelar(); //limpia las variables 
		var hidResponsable;
		var totalElementos =0;
		var totalElementosP =0;
		
		store.each(function(record) {
			hidResponsable =record.data['RESPONSABLE_INTERES'];
			
			if(record.data['CAPTURA_LINEA']=='S'){	
				lineaReg.push(record.data['LINEA_REGISTRO']);				
				fecha_vto.push(record.data['FECHA_VEN']);
				igNumeroDocto.push(record.data['NUN_DOCTOINICIAL']);	
				totalElementos ++;
			} 				
				plazo.push(record.data['PLAZO']);
				totalElementosP++;
			
		});
		
		if(numElementos !=totalElementos){
			Ext.MessageBox.alert('Mensaje','No se puede confirmar detalle porque no todos los documentos tienen asignada una linea de credito');
			return false;	
		}
		
		if(hidResponsable=='EPO'){
			if(numElementos !=totalElementosP ){
				Ext.MessageBox.alert('Mensaje','Favor de completar los campos IF o Plazo para cada uno de los registros desplegados en pantalla');
				return false;	
			}		
		}
		
		var lineaTodos = Ext.getCmp("lineaTodos1").getValue();		
		Ext.Ajax.request({
			url: '24forma01Ext.data.jsp',
			params: {
				informacion: "ConfirmaDetalle",
				lineaReg:lineaReg,			
				fecha_vto:fecha_vto,
				igNumeroDocto:igNumeroDocto,
				plazo:plazo,
				lineaTodos:lineaTodos,
				proceso:proceso				
			},
			callback: procesaRegresaPreacuse				
		});						
	}

	//GENERA EL  ARCHIVO PDF DEL DETALLE
	var procesarSuccessFailureGenerarDetalle =  function(opts, success, response) {
		var btnImprimirDetalle = Ext.getCmp('btnImprimirDetalle');
		btnImprimirDetalle.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarDetalle = Ext.getCmp('btnBajarDetalle');
			btnBajarDetalle.show();
			btnBajarDetalle.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			
			btnBajarDetalle.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
			
		} else {
			btnImprimirDetalle.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//COMBO DE IF PARA EL DETALLE DE LINEAS DE CREDITO
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var comboIF = new Ext.Container({
		layout: 'table',
		id: 'comboIF',
		hidden: true,		
		width:	'auto',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [	
			{ 
				xtype:   'label',  
				html:		'Seleccionar todos los documentos con el siguiente IF:', 
				cls:		'x-form-item', 
				style: { 
					width:   		'100', 
					textAlign: 		'left'
				} 
			},							
			{
				xtype: 'combo',
				name: 'lineaTodos',
				id: 'lineaTodos1',
				fieldLabel: 'Seleccionar todos los documentos con el siguiente IF',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'lineaTodos',
				emptyText: 'Seleccione...',
				forceSelection : true,
				triggerAction : 'all',
				width: 500,
				typeAhead: true,
				minChars : 1,
				store : catalogoIFData,
				tpl : NE.util.templateMensajeCargaCombo,
				listeners: {
					select: {
						fn: function(combo) {
							var lineaTodos = Ext.getCmp('lineaTodos1');							
							actualizaCombos(lineaTodos.getValue());
						}
					}
				}
			}		
		]
	});



	var procesarConsultaDetalleCCCData = function(store, arrRegistros, opts) 	{
			
		if (arrRegistros != null) {
			if (!gridCCC.isVisible()) {
				gridCCC.show();
			}	
			var jsonData = store.reader.jsonData;	
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  campo1 = jsonData.Campo0;
			var  campo2 = jsonData.Campo1;
			var  campo3 = jsonData.Campo2;
			var  campo4 = jsonData.Campo3;
			var  campo5 = jsonData.Campo4;
			
		
			
			if(jsonData.strPerfil=='ADMIN EPO')  {
				Ext.getCmp('mensaje3').show(); 
			}
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridCCC.getColumnModel();
			if(campo1 !=''){					
				gridCCC.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),campo1);
			}
			if(campo2 !=''){					
				gridCCC.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),campo2);
			}
			if(campo3 !=''){					
				gridCCC.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),campo3);
			}
			if(campo4 !=''){					
				gridCCC.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),campo4);
			}
			if(campo5 !=''){					
				gridCCC.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),campo5);
			}
				
			if(jsonData.meseSinIntereses=='S' && jsonData.obtieneTipoCredito=='C' ) {
				Ext.getCmp('mensajesIntereses_1').show(); 
			}
					
			if(hayCamposAdicionales=='0'){
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='1'){
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='2'){
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);			
			}else  	if(hayCamposAdicionales=='3'){
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='4'){
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='5'){
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCCC.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);	
			}
			if(jsonData.strPerfil!='ADMIN EPO'){			
				gridCCC.getColumnModel().setHidden(cm.findColumnIndex('ESTATUS'), false);
			}else {		
				gridCCC.getColumnModel().setHidden(cm.findColumnIndex('ESTATUS'), true);
			}
			
			//F09-2015
			if(jsonData.meseSinIntereses=='S'){
				gridCCC.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), false);			
			}else  {
				gridCCC.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), true);		 
			}
			
			var el = gridCCC.getGridEl();	
			
			
			
			if(store.getTotalCount() > 0) {
				el.unmask();					
					gridTotales.show();							
			} else {					
				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
		

	var procesarConsultaMonitor = function(store, arrRegistros, opts) 	{
			
		if (arrRegistros != null) {
			if (!gridMonitor.isVisible()) {
				gridMonitor.show();
			}					
		
			var jsonData = store.reader.jsonData;	
				if(jsonData.seleccionarIFs=='N'){
					comboIF.hide();
				}else {	
					comboIF.show();
				}
	
				numLineas =jsonData.hidNumLineas
				
			consultaDetalleData.load({ 
				params: { 
					informacion: 'Consulta', 
					pantalla:'Captura',
					proceso:jsonData.proceso,					
					seleccionarIFs:jsonData.seleccionarIFs, 	
					auxIcLineaMN:jsonData.auxIcLineaMN,
					auxDescLineaMN:jsonData.auxDescLineaMN,
					auxIcLineaUSD:jsonData.auxIcLineaUSD,
					auxDescLineaUSD:jsonData.auxDescLineaUSD,
					hidNumLineas:jsonData.hidNumLineas					
				}  		
			});	
					
			var el = gridMonitor.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();													
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}			
		}	
	}		
	
	
	var procesarConsultaDetalleData = function(store, arrRegistros, opts) 	{
				
		if (arrRegistros != null) {
			if (!gridDetalle.isVisible()) {
				gridDetalle.show();
			}	
			var jsonData = store.reader.jsonData;	
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  campo1 = jsonData.Campo0;
			var  campo2 = jsonData.Campo1;
			var  campo3 = jsonData.Campo2;
			var  campo4 = jsonData.Campo3;
			var  campo5 = jsonData.Campo4;
			var operaConversion = jsonData.operaConversion;			
			numElementos = jsonData.numElementos;
				
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridDetalle.getColumnModel();
			
			if(jsonData.strPerfil=='ADMIN EPO')  {
				Ext.getCmp('mensaje3').show(); 
			}
							
			if(operaConversion=='false'){
				gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);	
				gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);	
				gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('MONTO_VALUADO'), true);								
			}
								
			if(campo1 !=''){					
				gridDetalle.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),campo1);
			}
			if(campo2 !=''){					
				gridDetalle.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),campo2);
			}
			if(campo3 !=''){					
				gridDetalle.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),campo3);
			}
			if(campo4 !=''){					
				gridDetalle.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),campo4);
			}
			if(campo5 !=''){					
				gridDetalle.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),campo5);
			}
					
			if(hayCamposAdicionales=='0'){
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='1'){
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='2'){
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);			
			}else  	if(hayCamposAdicionales=='3'){
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='4'){
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='5'){
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);	
			}
			if(jsonData.strPerfil!='ADMIN EPO'){			
				gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('ESTATUS'), false);
			}else {		
				gridDetalle.getColumnModel().setHidden(cm.findColumnIndex('ESTATUS'), true);
			}
			
		
			var el = gridDetalle.getGridEl();			
			
			if(store.getTotalCount() > 0) {
				el.unmask();					
					gridTotales.show();						
			} else {					
				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
			
	//para los totales del grid Detalles 
	var consultaMonitorData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'MonitorLineasCredito'	
		},								
		fields: [			
			{name: 'IC_LINEA' , mapping: 'IC_LINEA'},
			{name: 'MONEDA_LINEA' , mapping: 'MONEDA_LINEA'},	
			{name: 'LINEA_CREDITO' , mapping: 'LINEA_CREDITO'},			
			{name: 'SALDO_INICIAL' , mapping: 'SALDO_INICIAL'},
			{name: 'SALDO_FINANCIADO' , mapping: 'SALDO_FINANCIADO'},
			{name: 'MONTO_PROCESO' , mapping: 'MONTO_PROCESO'},
			{name: 'SALDO_DISPONIBLE_LINEA' , mapping: 'SALDO_DISPONIBLE_LINEA'},
			{name: 'MONTO_NEGOCIABLE' , mapping: 'MONTO_NEGOCIABLE'},
			{name: 'MONTO_SELECCIONADO' , mapping: 'MONTO_SELECCIONADO'},
			{name: 'NUM_DOCTOS' , mapping: 'NUM_DOCTOS'},
			{name: 'SALDO_DISPONIBLE' , mapping: 'SALDO_DISPONIBLE'}
		],
		totalProperty : 'total',
		autoLoad: false,	
		listeners: {
			load: procesarConsultaMonitor,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConsultaMonitor(null, null, null);						
				}
			}
		}			
	});
	
	var gridMonitor = new Ext.grid.EditorGridPanel({
		id: 'gridMonitor',
		title: 'Monitor de l�neas de cr�dito',
		hidden: true,
		clicksToEdit: 1,			
		columns: [		
		{							
			header : 'L�nea de Cr�dito IF',
			tooltip: 'L�nea de Cr�dito IF',
			dataIndex : 'LINEA_CREDITO',
			width : 400,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Saldo inicial de la l�nea',
			tooltip: 'Saldo inicial de la l�nea',
			dataIndex : 'SALDO_INICIAL',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Saldo actual de doctos financiados',
			tooltip: 'Saldo actual de doctos financiados',
			dataIndex : 'SALDO_FINANCIADO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Monto de Doctos en proceso de autorizaci�n',
			tooltip: 'Monto de Doctos en proceso de autorizaci�n',
			dataIndex : 'MONTO_PROCESO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Saldo disponible de la L�nea',
			tooltip: 'Saldo disponible de la L�nea',
			dataIndex : 'SALDO_DISPONIBLE_LINEA',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{	header : 'Monto de documentos negociables',
			tooltip: 'Monto de documentos negociables',
			dataIndex : 'MONTO_NEGOCIABLE',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{	header : 'Monto de documentos publicados',
			tooltip: 'Monto de documentos publicados',
			dataIndex : 'MONTO_SELECCIONADO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{	header : 'Num. doctos. publicados',
			tooltip: 'Num. doctos. publicados',
			dataIndex : 'NUM_DOCTOS',
			width : 150,
			align: 'center',
			sortable : false		
		},
		{	header : 'Saldo potencial disponible de la l�nea',
			tooltip: 'Saldo potencial disponible de la l�nea',
			dataIndex : 'SALDO_DISPONIBLE',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
		],
		displayInfo: true,
		store: consultaMonitorData,
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		frame: false
	});
		


	

	//para los totales del grid Nomal 
	var totalesData = new Ext.data.JsonStore({			
		root : 'registrosT',
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'	
		},								
		fields: [			
			{name: 'MONEDA' , mapping: 'MONEDA'},
			{name: 'TOTAL', type: 'float', mapping: 'TOTAL'},
			{name: 'TOTAL_MONTO' , type: 'float', mapping: 'TOTAL_MONTO' },
			{name: 'TOTAL_MONTO_DESC',type: 'float',   mapping: 'TOTAL_MONTO_DESC'}				
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		id: 'gridTotales',
		store: totalesData,
		margins: '20 0 0 0',
		align: 'center',
		title: '',	
		hidden: true,
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'				
			},
			{
				header: 'Total de Documentos ',
				dataIndex: 'TOTAL',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Total Monto Descuento',
				dataIndex: 'TOTAL_MONTO_DESC',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: '',
				dataIndex: '',
				width: 334,
				align: 'right'			
			}					
			],
			height: 100,
			width: 943,
			frame: false
	});	
		
/////////////////////////////////////////////////////////////////////////////////

	var consultaDetalleCCCData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},			
		fields: [
			{name: 'CLIENTE'},		
			{name: 'NUMERO_DOCTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},			
			{name: 'PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PLAZO_DESC'},
			{name: 'PORCENTAJE'},
			{name: 'MONTO_DESC'},
			{name: 'COMENTARIOS'},	
			{name: 'MODALIDAD'},			
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'},
			{name: 'CAMPO4'},
			{name: 'CAMPO5'},
			{name: 'TIPO_CREDITO'},
			{name: 'ESTATUS'},
			{name: 'TIPOPAGO'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDetalleCCCData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaDetalleCCCData(null, null, null);						
					}
				}
			}			
		});
		
		var gridCCC = new Ext.grid.EditorGridPanel({
		id: 'gridCCC',
		title: 'Carga de Documentos',
		hidden: true,
		clicksToEdit: 1,				
		columns: [
		{							
			header : 'Cliente',
			tooltip: 'Cliente',
			dataIndex : 'CLIENTE',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'N�mero docto.',
			tooltip: 'N�mero docto.',
			dataIndex : 'NUMERO_DOCTO',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Fecha de emisi�n',
			tooltip: 'Fecha de emisi�n',
			dataIndex : 'FECHA_EMISION',
			width : 150,
			align: 'center',
			sortable : false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{							
			header : 'Fecha vto. docto.',
			tooltip: 'Fecha vto. docto.',
			dataIndex : 'FECHA_VENCIMIENTO',
			width : 150,
			align: 'center',
			sortable : false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{							
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO_DOCTO',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			align: 'left',
			sortable : false			
		},
		{							
			header : 'Monto. docto.',
			tooltip: 'Monto. docto.',
			dataIndex : 'MONTO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},		
		{							
			header : 'Plazo para descuento en dias',
			tooltip: 'Plazo para descuento en dias',
			dataIndex : 'PLAZO_DESC',
			width : 150,
			align: 'center',
			sortable : false,
			renderer:function(value,metadata,registro){
				if(registro.data['TIPO_CREDITO']!='F') {	
					return value;
				}else  {
					return 'NA';
				}
			}		
		},
		{							
			header : '% descuento',
			tooltip: '% descuento',
			dataIndex : 'PORCENTAJE',
			width : 150,
			align: 'center',
			sortable : false,
			renderer:function(value,metadata,registro){
				if(registro.data['TIPO_CREDITO']!='F') {	
					return Ext.util.Format.number(value,'0.0000%');
				}else  {
					return 'NA';
				}
			}	
		},
		{							
			header : 'Monto % de descuento',
			tooltip: 'Monto % de descuento',
			dataIndex : 'MONTO_DESC',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00') 
		},
		{							
			header : 'Modalidad de Plazo',
			tooltip: 'Modalidad de Plazo',
			dataIndex : 'MODALIDAD',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Tipo de Pago',
			tooltip: 'Tipo de Pago',
			dataIndex : 'TIPOPAGO',
			width : 150,
			align: 'center',
			sortable : false
		},	
		{							
			header : 'Comentarios',
			tooltip: 'Comentarios',
			dataIndex : 'COMENTARIOS',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo1',
			tooltip: 'Campo1',
			dataIndex : 'CAMPO1',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo2',
			tooltip: 'Campo2',
			dataIndex : 'CAMPO2',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Campo3',
			tooltip: 'Campo3',
			dataIndex : 'CAMPO3',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo4',
			tooltip: 'Campo4',
			dataIndex : 'CAMPO4',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo5',
			tooltip: 'Campo5',
			dataIndex : 'CAMPO5',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Estatus',
			tooltip: 'Estatus',
			dataIndex : 'ESTATUS',
			width : 150,
			align: 'center',
			sortable : false			
		}
		],
		displayInfo: true,
		store: consultaDetalleCCCData,
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		frame: false
	});
		

///ES
	var consultaDetalleData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'CONSECUTIVO'},		
			{name: 'NUM_DISTRIBUIDOR'},
			{name: 'NOMBRE_DISTRIBUIDOR'},
			{name: 'NUN_DOCTOINICIAL'},			
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'CATEGORIA'},
			{name: 'PLAZO_DESC'},
			{name: 'PORCE_DESC'},
			{name: 'MONTO_DESC'},				
			{name: 'TIPO_CONVERSION'},	
			{name: 'TIPO_CAMBIO'},	
			{name: 'MONTO_VALUADO'},
			{name: 'MODALIDAD'},
			{name: 'COMENTARIOS'},
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'},
			{name: 'CAMPO4'},
			{name: 'CAMPO5'},
			{name: 'PLAZO'},
			{name: 'FECHA_VEN'},		
			{name: 'NEGOCIABLE'},
			{name: 'LINEA_REGISTRO'},
			{name: 'LINEA_REGISTRO_ANTERIOR'},			
			{name: 'RESPONSABLE_INTERES'},
			{name: 'DIAS_MINIMO'},
			{name: 'DIAS_MAXIMO'},		
			{name: 'IC_MONEDA'},	
			{name: 'SELECCIONAR_IFS'},
			{name: 'PARTIPOCAMBIO'},
			{name: 'HID_NUMLINEAS'},			
			{name: 'CAPTURA_LINEA'},
			{name: 'CAPTURA_PLAZO'},
			{name: 'CAPTURA_FECHAVEN'}	,
			{name: 'ESTATUS'}		
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDetalleData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaDetalleData(null, null, null);						
					}
				}
			}			
		});
		
	
		//para la creacion del combo en el grid
	var comboLineas = new Ext.form.ComboBox({  
    triggerAction : 'all',  
    displayField : 'descripcion',  
    valueField : 'clave', 
		typeAhead: true,
		width: 250,
		minChars : 1,
		allowBlank: true,
    store :catalogoIFData 
	});
	
	Ext.util.Format.comboRenderer = function(comboLineas){
			return function(value,metadata,registro,rowIndex,colIndex,store){				
					if(value!=''){
						var record = comboLineas.findRecord(comboLineas.valueField, value);
						return record ? record.get(comboLineas.displayField) : comboLineas.valueNotFoundText;
					}				
			}
	}
	
	
	var gridDetalle = new Ext.grid.EditorGridPanel({
		id: 'gridDetalle',
		title: 'Carga de Documentos',
		hidden: true,
		store: consultaDetalleData,
		clicksToEdit: 1,	
		style: 'margin:0 auto;',
		columns: [
		
		{							
			header : 'IC_MONEDA',
			tooltip: 'IC_MONEDA',
			dataIndex : 'IC_MONEDA',
			width : 150,
			align: 'left',
			hidden: true,
			sortable : false
		},
		{							
			header : 'N�mero de distribuidor',
			tooltip: 'N�mero de distribuidor',
			dataIndex : 'NUM_DISTRIBUIDOR',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Distribuidor',
			tooltip: 'Distribuidor',
			dataIndex : 'NOMBRE_DISTRIBUIDOR',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'N�mero documento inicial',
			tooltip: 'N�mero documento inicial',
			dataIndex : 'NUN_DOCTOINICIAL',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Fecha de emisi�n',
			tooltip: 'Fecha de emisi�n',
			dataIndex : 'FECHA_EMISION',
			width : 150,
			align: 'center',
			sortable : false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{							
			header : 'Fecha vto. docto.',
			tooltip: 'Fecha vto. docto.',
			dataIndex : 'FECHA_VENCIMIENTO',
			width : 150,
			align: 'center',
			sortable : false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{							
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO_DOCTO',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			align: 'left',
			sortable : false			
		},
		{							
			header : 'Monto. docto.',
			tooltip: 'Monto. docto.',
			dataIndex : 'MONTO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Categor�a',
			tooltip: 'Categor�a',
			dataIndex : 'CATEGORIA',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Plazo para descuento en dias',
			tooltip: 'Plazo para descuento en dias',
			dataIndex : 'PLAZO_DESC',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : '% descuento',
			tooltip: '% descuento',
			dataIndex : 'PORCE_DESC',
			width : 150,
			align: 'center',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('0.0000%')
		},
		{							
			header : 'Monto % de descuento',
			tooltip: 'Monto % de descuento',
			dataIndex : 'MONTO_DESC',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00') 
		},
		{							
			header : 'Tipo conversi�n',
			tooltip: 'Tipo conversi�n',
			dataIndex : 'TIPO_CONVERSION',
			width : 150,
			align: 'right',
			sortable : false		 
		},
		{							
			header : 'Tipo cambio ',
			tooltip: 'Tipo cambio ',
			dataIndex : 'TIPO_CAMBIO',
			width : 150,
			align: 'right',
			sortable : false		 
		},
		{							
			header : 'Monto valuado en Pesos',
			tooltip: 'Monto valuado en Pesos',
			dataIndex : 'MONTO_VALUADO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00') 
		},	
		{							
			header : 'Modalidad de Plazo',
			tooltip: 'Modalidad de Plazo',
			dataIndex : 'MODALIDAD',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Comentarios',
			tooltip: 'Comentarios',
			dataIndex : 'COMENTARIOS',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo1',
			tooltip: 'Campo1',
			dataIndex : 'CAMPO1',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo2',
			tooltip: 'Campo2',
			dataIndex : 'CAMPO2',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Campo3',
			tooltip: 'Campo3',
			dataIndex : 'CAMPO3',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo4',
			tooltip: 'Campo4',
			dataIndex : 'CAMPO4',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo5',
			tooltip: 'Campo5',
			dataIndex : 'CAMPO5',
			width : 150,
			align: 'left',
			sortable : false
		},		
		{
			header: 'IF', //combo para la linea de Credito
			tooltip: 'IF',
			dataIndex: 'LINEA_REGISTRO',
			sortable: true,
			resizable: true,
			width: 400,
			hidden: false,		
			align: 'left',
			editor:comboLineas,
			renderer: Ext.util.Format.comboRenderer(comboLineas)
		},
		{
			header: 'LINEA_REGISTRO_ANTERIOR', 
			tooltip: 'LINEA_REGISTRO_ANTERIOR',
			dataIndex: 'LINEA_REGISTRO_ANTERIOR',
			sortable: true,
			resizable: true,
			width: 400,
			hidden: true,		
			align: 'left',
			renderer:function(value,metadata,registro){		
				var select = registro.data['SELECCIONAR_IFS'];									
				if(select=='N'){
					calculaMontosNOIF();
				}
				return value;
			}	
		},			
		{							
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO',
			width : 150,
			align: 'center',
			sortable : false,
			editor: {
				xtype : 'numberfield',
				maxValue : 999999999999.99,
				allowBlank: false
			},
			renderer: function(value, metadata, record, rowindex, colindex, store) {
				var jsonData = store.reader.jsonData;	
				var responsable = record.get('RESPONSABLE_INTERES');
				if(responsable !='EPO'){
					return 'N/A';	
				}else {
					return value;
				}
			}
		},			
		{							
			header : 'Fecha vto',
			tooltip: 'Fecha vto',
			dataIndex : 'FECHA_VEN',
			width : 150,
			align: 'center',
			sortable : false,
			editor: campoFechas,
			renderer: function(value, metadata, record, rowindex, colindex, store) {
				var jsonData = store.reader.jsonData;	
				var responsable = record.get('RESPONSABLE_INTERES');
				if( responsable !='EPO'){
					return 'N/A';	
				}		else {
					return value;
				}			
			}
		},
		{							
			header : 'Estatus',
			tooltip: 'Estatus',
			dataIndex : 'ESTATUS',
			width : 150,
			align: 'center',
			sortable : false			
		}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		frame: false,		
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridDetalle = e.gridDetalle; 
				var campo= e.field;
				
				var selec_if = record.get('SELECCIONAR_IFS');
				var responsable = record.get('RESPONSABLE_INTERES');
				if(record.get('PLAZO')) {					
					record.data['CAPTURA_PLAZO']='S';				
				}
				if(record.get('FECHA_VEN')) {		
					record.data['CAPTURA_FECHAVEN']='S'; 
				}
					
						
				if(campo == 'LINEA_REGISTRO'){
					if(selec_if=='S' ){						
						return true; //  es editable 		  				
					}else {		
						return false; // no es editable 	
					}
				}
				record.commit();				
			
				
				if(campo == 'PLAZO'){
					if( responsable =='EPO'){						
						return true; //  es editable 		  				
					}else {		
						return false; // no es editable 	
					}
				}
								
				if(campo == 'FECHA_VEN'){
					if(responsable =='EPO'){					
						return true; //  es editable 		  				
					}else {		
						return false; // no es editable 	
					}
				}
								
			}	,	
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
		
				var record = e.record;
				var gridDetalle = e.gridDetalle; 
				var campo= e.field;
		
				if(campo == 'PLAZO'){				
					sumaFecha('', '', '', record); 
				}
				
				if(campo == 'FECHA_VEN'){				
					restaFecha('', '', '', record); 
				}
						
				if(campo == 'LINEA_REGISTRO'){	
					var noLinea = record.data['LINEA_REGISTRO'];
					var lineAnterior = record.data['LINEA_REGISTRO_ANTERIOR'];
					if(noLinea==0 || lineAnterior !=noLinea){
						record.data['CAPTURA_LINEA']='N';
					}
					record.data['LINEA_REGISTRO'] = noLinea;
					calculaMontos(); 
				}			
				
				record.commit();
				
			}
		}
	});
		

	
	//contenedor de la forma de detalle para lineas de Credito
	var fpDetalle = new Ext.Container({
		layout: 'table',
		id: 'fpDetalle',
		//hidden: true,
		layoutConfig: {
			columns: 50
		},
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{
				xtype: 'button',
				text: 'Regresar',
				id: 'btnRegresar',
				iconCls: 'icoLimpiar',
				handler: procesarTerminaCaptura
			},		
			{
				xtype: 'button',
				text: 'Generar PDF',
				iconCls: 'icoPdf',
				id: 'btnImprimirDetalle',		
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
					url: '24forma01ArcExt.jsp',
						params:{
						informacion: 'ImprimirDetalle',
						proceso:proceso,
						tipoArchivo:'PDF'
						},
						callback: procesarSuccessFailureGenerarDetalle
					});
				}
			},			
			{
				xtype: 'button',
				text: 'Bajar PDF',
				iconCls: 'icoPdf',
				id: 'btnBajarDetalle',
				hidden: true
			},		
			{
				xtype: 'button',
				text: 'Confirmar Detalle',
				id: 'btnConfirmar',
				iconCls: 'icoAceptar',
				hidden: true,
				handler: procesarConfirmaDetalle		
			}			
		]
	});
	
	 

	
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			mensaje1,		
			NE.util.getEspaciador(20),		
			gridMonitor,
			NE.util.getEspaciador(20),
			comboIF,	
			NE.util.getEspaciador(20),
			gridDetalle,
			gridCCC,
			gridTotales,	
			NE.util.getEspaciador(20),
			mensaje3,
			mensajesIntereses_1,
			NE.util.getEspaciador(20),
			fpDetalle,
			NE.util.getEspaciador(20)	
		]
	});
	
	
	if(proceso!==''){
		var btnConfirmar = Ext.getCmp('btnConfirmar');
		if(tipoCredito=='D' &&  ventaCartera =='N') {	
			consultaMonitorData.load({ 	
				params: { 		
					informacion: 'MonitorLineasCredito', 
					proceso:proceso 	
				} 		
			});
			totalesData.load({ 		
				params: { 		
					informacion: 'ResumenTotales', 
					proceso:proceso,
					pantalla:'Captura'
				} 		
			});
			gridCCC.hide();
			btnConfirmar.show();
			catalogoIFData.load();
			
		} else if(    (tipoCredito=='D' &&  ventaCartera =='S')  || tipoCredito=='C' || tipoCredito=='A'  ) {	
		
			comboIF.hide();
			gridMonitor.hide();
			gridDetalle.hide();
			btnConfirmar.hide();		
					
			consultaDetalleCCCData.load({ 
				params: { 
					informacion: 'Consulta', 
					pantalla:'CreditoCuentaCorriente',
					proceso:proceso								
				}  		
			});
			totalesData.load({ 		
				params: { 		
					informacion: 'ResumenTotales', 
					proceso:proceso,
					pantalla:'CreditoCuentaCorriente'
				} 		
			});		
		}	
	}
	


} );