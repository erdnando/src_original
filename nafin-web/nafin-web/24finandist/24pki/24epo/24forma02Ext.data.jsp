<%@ page contentType="application/json;charset=UTF-8"
	import="
    org.apache.commons.logging.Log,
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	java.text.SimpleDateFormat,
	java.util.Date,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	java.io.*,	
	java.text.*, 
	java.math.*,
	net.sf.json.*"	
	errorPage="/00utils/error_extjs.jsp, /00utils/error_extjs_fileupload.jsp"		
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String hidCifrasControl  = (request.getParameter("hidCifrasControl") != null) ? request.getParameter("hidCifrasControl") : "N";
	

String infoRegresar ="";
String rutaArchivoTemporal = null;
ParametrosRequest req = null;
StringBuffer contenidoArchivo=new StringBuffer("");
JSONObject 	resultado	= new JSONObject();
//DECLARACION DE VARIABLES
int numFiles=0, i=0, nd=1, comp_fecha=0, no_pyme=0, pipesporlinea=0, ic_proc_docto=0,
lineadocs=1, totdoc_mn=0, totdoc_dolar=0, no_dia_semana=0,
negociable=2,	//Este es el estatus del documento, el 2 es Negociable.	
rtxttotdoc =0,  rtxttotdocdol = 0,	si_detalle = 0;
boolean ok=true, ok_enc=true,	ok_fechas=true;
String ln	=	"",  cg_num_distribuidor	=	"",  numero_docto	=	"", fecha_emision	=	"", fecha_venc =	"",
fecha_venc_adm	=	"", ic_moneda	=	"", fn_monto	=	"", ic_tipo_financiamiento =	"", categoria	=	"", 
ig_plazo_descuento	=	"", fn_porc_descuento	=	"", ic_linea_credito_dm	=	"", fecha_lim_dscto		=	"", 
ct_referencia		=	"", dscto_especial =	"", campo_ad1="", campo_ad2="", campo_ad3="", campo_ad4="", 
campo_ad5="",  dm_inhabil="",  nombre_pyme = "", NOnegociable = "" , estatusNegociable = "",
operaVentaCartera ="", smonto_mn_ok = "", smonto_dolar_ok="", smonto_mn_er="", smonto_dolar_er="",
arma_error="", tipo_Pago ="", pediodo ="";
String ses_cg_razon_social=strNombre;
Vector vecFilas 	= null;
Vector vecColumnas 		= null;
Vector vecFilasSE		= null;
Vector vecColumnasSE	= null;
Vector vecFilasCE 		= new Vector();
Vector vecColumnasCE	= null;
VectorTokenizer vt=null; 
Vector vecdat=null; 
Vector ig_nums_doctos=new Vector(); 
String ses_ic_epo=iNoCliente;
double monto_mn_ok = 0,  monto_dolar_ok=0,  monto_mn_er=0,  monto_dolar_er=0;	
BigDecimal monto_mn=new BigDecimal("0.0");	BigDecimal monto_dolar=new BigDecimal("0.0");
StringBuffer error=new StringBuffer();  /* documentos Malos */
StringBuffer bueno=new StringBuffer(); 	/* documentos buenos */
StringBuffer error_enc=new StringBuffer();  /* Enc Malos */
StringBuffer seguridad=new StringBuffer("");
StringBuffer bueno_det=new StringBuffer();  /* Detalle Bueno */	
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

try {
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
	if(informacion.equals("ValidaCargaArchivo")) {
		ic_proc_docto=0;
		//parametrizaciones
		String	descuentoAutomatico =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_DESC_AUTOMATICO" );
		String limiteLineaCredito =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_LIMITE_LINEA_CREDITO");//Fodea 029-2010 Distribuodores Fase III
		String TipoCredito =  BeanParametros.obtieneTipoCredito (iNoCliente); //Fodea 029-2010 Distribuodores Fase III
		String ventaCartera =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); //Fodea 029-2010 Distribuodores Fase III
		String  meseSinIntereses = BeanParametros.DesAutomaticoEpo(iNoCliente,"CS_MESES_SIN_INTERESES");
		Vector  vecParametros    = BeanParametros.getParamEpoa(ses_ic_epo); // Fodea 09-2015
      String cmbMeses  = (vecParametros.get(16)==null)?"": vecParametros.get(16).toString().trim(); // Fodea 09-2015
	
		if(ventaCartera.equals("S")) {
			operaVentaCartera = "S";
		}
		String lineaCredito = cargaDocto.validaMontolineaCredito(iNoCliente); //Fodea 029-2010 Distribuidores Fase III
		String hid_fecha_porc_desc = cargaDocto.getParamFechaPorc(iNoCliente);
		double LineaCredito2 =0;
		
		//obtengo parametros de Cifras de Control  
			
		String txttotdoc  = (request.getParameter("txttotdoc") != null) ? request.getParameter("txttotdoc") : "0";
		String txtmtodoc  = (request.getParameter("txtmtodoc") != null) ? request.getParameter("txtmtodoc") : "0";
		String txtmtomindoc  = (request.getParameter("txtmtomindoc") != null) ? request.getParameter("txtmtomindoc") : "0";
		String txtmtomaxdoc  = (request.getParameter("txtmtomaxdoc") != null) ? request.getParameter("txtmtomaxdoc") : "0";
	
		String txttotdocdo  = (request.getParameter("txttotdocdo") != null) ? request.getParameter("txttotdocdo") : "0";
		String txtmtodocdo  = (request.getParameter("txtmtodocdo") != null) ? request.getParameter("txtmtodocdo") : "0";
		String txtmtomindocdo  = (request.getParameter("txtmtomindocdo") != null) ? request.getParameter("txtmtomindocdo") : "0";
		String txtmtomaxdocdo  = (request.getParameter("txtmtomaxdocdo") != null) ? request.getParameter("txtmtomaxdocdo") : "0";
		
		if(txttotdoc.equals("")) txttotdoc="0";
		if(txtmtodoc.equals("")) txtmtodoc="0";
		if(txtmtomindoc.equals("")) txtmtomindoc="0";
		if(txtmtomaxdoc.equals("")) txtmtomaxdoc="0";
		
		if(txttotdocdo.equals("")) txttotdocdo="0";
		if(txtmtodocdo.equals("")) txtmtodocdo="0";
		if(txtmtomindocdo.equals("")) txtmtomindocdo="0";
		if(txtmtomaxdocdo.equals("")) txtmtomaxdocdo="0"; 
		
		/*
		System.out.println(" hidCifrasControl--->:"+hidCifrasControl);
		System.out.println(" txttotdoc--->:"+txttotdoc);
		System.out.println(" txtmtodoc--->:"+txtmtodoc);
		System.out.println(" txtmtomindoc--->:"+txtmtomindoc);
		System.out.println(" txtmtomaxdoc--->:"+txtmtomaxdoc);
		
		System.out.println(" txttotdocdo--->:"+txttotdocdo);
		System.out.println(" txtmtodocdo--->:"+txtmtodocdo);
		System.out.println(" txtmtomindocdo--->:"+txtmtomindocdo);
		System.out.println(" txtmtomaxdocdo--->:"+txtmtomaxdocdo);
		*/
			
		int otxttotdoc=0;
		BigDecimal otxtmtodoc=new BigDecimal(0); 
		BigDecimal txtmtomindocdol=new BigDecimal(0);
		BigDecimal txtmtomaxdocdol=new BigDecimal(0);
		int otxttotdocdol=0;;
		BigDecimal otxtmtodocdol=new BigDecimal(0);	
		BigDecimal txtmtomindoc1=new BigDecimal(0);
		BigDecimal txtmtomaxdoc1=new BigDecimal(0);
			
		
		if("S".equals(hidCifrasControl)){
			otxttotdoc=Integer.parseInt(txttotdoc); 
			otxtmtodoc=new BigDecimal(txtmtodoc); 
			txtmtomindocdol=new BigDecimal(txtmtomindocdo);
			txtmtomaxdocdol=new BigDecimal(txtmtomaxdocdo);
		
			otxttotdocdol=Integer.parseInt(txttotdocdo);
			otxtmtodocdol=new BigDecimal(txtmtodocdo);	
			txtmtomindoc1=new BigDecimal(txtmtomindoc);
			txtmtomaxdoc1=new BigDecimal(txtmtomaxdoc);
		}
					
		if (strAforo == null) strAforo = "0";
		BigDecimal porcentaje=new BigDecimal(strAforo);	/* strAforo viene de Varible de Session. */
		BigDecimal cien=new BigDecimal("100.0");
		BigDecimal fn_aforo=cien.multiply(porcentaje);
		int diavMax=0, diavMin=0;
		String query=""; ResultSet rs=null; Vector diames_inhabil=new Vector();
		java.util.Date FechaHoy=new java.util.Date();	Calendar dia_fv = new GregorianCalendar();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy"); 
		java.util.Date fechaDocto=null;                 java.util.Date fechaVenc=null;
		java.util.Date fechaContr=null;
		int total_campos=0, c1=0, c2=0, c3=0, c4=0, c5=0; boolean sin_campos=false;
		int noCampo[]=new int[6];  String tipoDato[]=new String[6];
		int longitud[]=new int[6]; String nombreCampo[]=new String[6]; 

		/* Obtiene los campos dinamicos definidos por la epo. */
		vecFilas = cargaDocto.getCamposAdicionales(ses_ic_epo,false);
		total_campos = vecFilas.size();
		int v=1; String los_campos="", los_valores="";
			
		for(i=0;i<vecFilas.size();i++) { 
			vecColumnas = (Vector)vecFilas.get(i);
			noCampo[v] = Integer.parseInt((String)vecColumnas.get(1));
			tipoDato[v] =(String)vecColumnas.get(2);	
			longitud[v] =Integer.parseInt((String)vecColumnas.get(3));
			nombreCampo[v]=(String)vecColumnas.get(0);
			//out.println(noCampo[v]+"-"+nombreCampo[v]+"-"+tipoDato[v]+"-"+longitud[v]+"<br>");
			if(noCampo[v]==1) { los_campos+=",CG_CAMPO1"; c1=1; }
			if(noCampo[v]==2) { los_campos+=",CG_CAMPO2"; c2=2; }
			if(noCampo[v]==3) { los_campos+=",CG_CAMPO3"; c3=3; }
			if(noCampo[v]==4) { los_campos+=",CG_CAMPO4"; c4=4; }
			if(noCampo[v]==5) { los_campos+=",CG_CAMPO5"; c5=5; }
				v++;
		}//termina for de Campos Adicionales
			   
	 	//Obtiene el maximo de ic_proc_docto.
		ic_proc_docto = Integer.parseInt(cargaDocto.getClaveDoctoTmp());
    
		//recupero el nombre del archivo
		String archivo  = (request.getParameter("archivo") != null) ? request.getParameter("archivo") : "";
		String rutaArchivo = strDirectorioTemp +archivo; 		
		java.io.File ft = new java.io.File(rutaArchivo);
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
		VectorTokenizer vtd = null;
		Vector vecdet = null;
		String linea=	"";
		BigDecimal montoMN=new BigDecimal("0.0");
		BigDecimal montoDL=new BigDecimal("0.0");
		int        cantMN=0;
		int        cantDL=0;			
			
		String csCarcaterEspecial = "N";
			
		while((linea=br.readLine())!=null){
			csCarcaterEspecial = "N";
			vtd		= new VectorTokenizer(linea,"|");
			vecdet	= vtd.getValuesVector();		
			arma_error = "";
			ok=true;
			ok_fechas=true;			
      pipesporlinea=0; linea=linea.trim();
			if(linea.length()>0) {
				for(int n=0; n<linea.length(); n++) {
					ln=linea.substring(n,n+1);
	        if(ln.equals("|"))
						pipesporlinea++;
	        }
			  
				if(Comunes.tieneCaracterControlNoPermitidoEnXML1(linea)){
					csCarcaterEspecial = "S";
					arma_error = "Se detectaron caracteres de control no permitidos (excluyendo tabulador (\\t) salto de línea (\\n) y retorno de carro (\\r))";	
					ok = false;
				}
				
	        try {
						
	          vt=new VectorTokenizer(linea,"|");
	          vecdat=vt.getValuesVector();					
						cg_num_distribuidor 	= (vecdat.size()>=1)?(String)vecdat.get(0):"";
						numero_docto 			= (vecdat.size()>=2)?(String)vecdat.get(1):"";
	          fecha_emision 			= (vecdat.size()>=3)?(String)vecdat.get(2):"";
	          fecha_venc 				= (vecdat.size()>=4)?(String)vecdat.get(3):"";
	          ic_moneda 				= (vecdat.size()>=5)?(String)vecdat.get(4):"";
	          fn_monto 				= (vecdat.size()>=6)?(String)vecdat.get(5):"";
						ic_tipo_financiamiento	= (vecdat.size()>=7)?(String)vecdat.get(6):"";
						categoria				= (vecdat.size()>=8)?(String)vecdat.get(7):"";
						if("1".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento)){
							ig_plazo_descuento	= (vecdat.size()>=9)?(String)vecdat.get(8):"";
							fecha_lim_dscto		= (vecdat.size()>=10)?(String)vecdat.get(9):"";
							fn_porc_descuento	= (vecdat.size()>=11)?(String)vecdat.get(10):"";
						}	
						else
							ic_linea_credito_dm = (vecdat.size()>=12)?(String)vecdat.get(11):"";
							ct_referencia			= (vecdat.size()>=13)?(String)vecdat.get(12):"";
							campo_ad1 				= (vecdat.size()>=14)?(String)vecdat.get(13):"";
							campo_ad2 				= (vecdat.size()>=15)?(String)vecdat.get(14):"";
							campo_ad3 				= (vecdat.size()>=16)?(String)vecdat.get(15):"";
							campo_ad4 				= (vecdat.size()>=17)?(String)vecdat.get(16):"";
							campo_ad5 				= (vecdat.size()>=18)?(String)vecdat.get(17):"";
						
						if(strPerfil.equals("ADMIN EPO") || strPerfil.equals("ADMIN EPO GRUPO") || strPerfil.equals("ADMIN EPO FON")  || strPerfil.equals("ADMIN EPO DISTR") )  {
						
								NOnegociable 				= (vecdat.size()>=19)?(String)vecdat.get(18):"";
								
								if ( meseSinIntereses.equals("S"))  {
									tipo_Pago 				= (vecdat.size()>=20)?(String)vecdat.get(19):"";
									pediodo 				= (vecdat.size()>=21)?(String)vecdat.get(20):"";
								}else if ( meseSinIntereses.equals("N"))  {
									tipo_Pago = "1"; 
								}
								
						}else if(!strPerfil.equals("ADMIN EPO") &&  !strPerfil.equals("ADMIN EPO GRUPO") &&  !strPerfil.equals("ADMIN EPO FON")  &&  !strPerfil.equals("ADMIN EPO DISTR") )  {    // cuando la epo Opera firma mancomunada
							 
							if ( meseSinIntereses.equals("S"))  {
									tipo_Pago 				= (vecdat.size()>=19)?(String)vecdat.get(18):"";
									pediodo 				= (vecdat.size()>=20)?(String)vecdat.get(19):"";
								}else if ( meseSinIntereses.equals("N"))  {
									tipo_Pago = "1"; 
								}
							}
							
							
							
						vecdat.removeAllElements();
						cg_num_distribuidor	= Comunes.quitaComitasSimples(cg_num_distribuidor);
						numero_docto 	= Comunes.quitaComitasSimples(numero_docto);
						fecha_emision = Comunes.quitaComitasSimples(fecha_emision);
						fecha_venc	= Comunes.quitaComitasSimples(fecha_venc);
						ic_tipo_financiamiento 	= Comunes.quitaComitasSimples(ic_tipo_financiamiento);
						categoria = Comunes.quitaComitasSimples(categoria);
						ig_plazo_descuento = Comunes.quitaComitasSimples(ig_plazo_descuento);
						fecha_lim_dscto = Comunes.quitaComitasSimples(fecha_lim_dscto);
						fn_porc_descuento	= Comunes.quitaComitasSimples(fn_porc_descuento);
						ic_moneda = Comunes.quitaComitasSimples(ic_moneda);
						fn_monto 	= Comunes.quitaComitasSimples(fn_monto);
						ic_linea_credito_dm 	= Comunes.quitaComitasSimples(ic_linea_credito_dm);
                        ct_referencia = Comunes.quitaComitasSimples(ct_referencia);
                        campo_ad1 	= Comunes.quitaComitasSimples(campo_ad1);
                        campo_ad2 	= Comunes.quitaComitasSimples(campo_ad2);
                        campo_ad3 	= Comunes.quitaComitasSimples(campo_ad3);
                        campo_ad4 	= Comunes.quitaComitasSimples(campo_ad4);
                        campo_ad5 	= Comunes.quitaComitasSimples(campo_ad5);
                        campo_ad1		= campo_ad1.trim(); 
                        campo_ad2=campo_ad2.trim(); 
                        campo_ad3=campo_ad3.trim(); 
                        campo_ad4=campo_ad4.trim(); 
                        campo_ad5=campo_ad5.trim();
						
						log.trace(" Numero de Distribuidor:"+cg_num_distribuidor);
						log.trace(" No de Documento:"+numero_docto);
						log.trace(" Fecha de Emisión del docto:"+fecha_emision);
						log.trace(" Fecha de Vencimiento del docto:"+fecha_venc);
						log.trace(" Moneda:"+ic_moneda);
						log.trace(" Monto del docto:"+fn_monto);						
						log.trace(" Tipo Financiamiento:"+ic_tipo_financiamiento);
						log.trace(" categoria:"+categoria);
						log.trace(" Plazo para Descuento:"+ig_plazo_descuento);
						log.trace(" Fecha de Límite para Descuento:"+fecha_lim_dscto);
						log.trace(" % del Descuento:"+fn_porc_descuento);			
						log.trace(" Linea de Crédito:"+ic_linea_credito_dm);
						log.trace(" Referencia o Comentario:"+ct_referencia);
						log.trace(" Campo Adicional 1:"+campo_ad1);
						log.trace(" Campo Adicional 2:"+campo_ad2);
						log.trace(" Campo Adicional 3:"+campo_ad3);
						log.trace(" Campo Adicional 4:"+campo_ad4);
						log.trace(" Campo Adicional 5:"+campo_ad5);
						log.trace(" negociable: "+NOnegociable);
						log.trace(" tipo_Pago: "+tipo_Pago);
						log.trace(" pediodo: "+pediodo);
						
						nombre_pyme = "";
						vecFilas = new Vector();
						vecFilas.addElement(campo_ad1);
						vecFilas.addElement(campo_ad2);
						vecFilas.addElement(campo_ad3);
						vecFilas.addElement(campo_ad4);
						vecFilas.addElement(campo_ad5);
                        
	        } catch(ArrayIndexOutOfBoundsException aioobe) {
                log.error("Error  aioobe  "+aioobe);            
                arma_error = "El layout es incorrecto";	
                ok = false;
            } catch(Exception e){
                log.error("Error  "+e);
                arma_error = "El layout es incorrecto";						
                ok = false;
			}				
				
				System.out.println("arma_error   "+arma_error);  
				
				//Realiza el proceso del archivo desde el EJB
			  int moneda=0;
				
				
				if("EPO MAN0 DISTR".equals(strPerfil)) {
					estatusNegociable = "30";   // Pendiente Negociable    F032-2014  
				}else if("EPO MAN1 DISTR".equals(strPerfil)) {
					estatusNegociable = "28";   // Pre Negociable  F032-2014
				}else  {
				//fodea 029-2010	
					if (NOnegociable.equalsIgnoreCase("S") ) {
					estatusNegociable = "2";
					}else  if (NOnegociable.equalsIgnoreCase("N")) {
					 estatusNegociable = "1";
					} 
					if (NOnegociable.equalsIgnoreCase("") ) {
						NOnegociable="NS";
					}
				}
				
     
				try{
				  
				  moneda=Integer.parseInt(ic_moneda);
				  
					if("".equals(arma_error)){
						arma_error =  cargaDocto.validacionesMasiva(ses_ic_epo,fecha_venc,fecha_emision,numero_docto,ic_moneda,fn_monto,txtmtomindoc1,txtmtomaxdoc1,txtmtomindocdol,txtmtomaxdocdol,lineadocs,vecFilas,pipesporlinea,cg_num_distribuidor,hidCifrasControl,ic_tipo_financiamiento,fecha_lim_dscto,categoria,ig_plazo_descuento,fn_porc_descuento,descuentoAutomatico,TipoCredito, limiteLineaCredito,NOnegociable,ventaCartera, strPerfil, tipo_Pago, pediodo, meseSinIntereses, cmbMeses );
					}
					if(!"".equals(arma_error)){						
						ok = false;
					}else  {					
					  if(!cargaDocto.tempMasiva(fecha_venc,fecha_emision,ses_ic_epo,ic_proc_docto+"",numero_docto,cg_num_distribuidor,ic_moneda,fn_monto,ct_referencia,vecFilas,ic_tipo_financiamiento,ig_plazo_descuento,fn_porc_descuento,ic_linea_credito_dm,fecha_lim_dscto,hid_fecha_porc_desc,categoria,estatusNegociable,operaVentaCartera, tipo_Pago, pediodo)){						
							ok = false;
					  }
					}
					// Totales 
					if(ok)  {
						double fn_monto2 =Double.parseDouble((String)fn_monto); 
						if(moneda==1)  {
							montoMN = montoMN.add(new BigDecimal(fn_monto));
							cantMN++;							
						}else  if(moneda==54)  {
							montoDL = montoDL.add(new BigDecimal(fn_monto));
							cantDL++;				
						}		
					}
					
				  
				} catch(NafinException ne) {
                    log.error("NafinException:"+ne);
                    ne.printStackTrace();
					arma_error = ne.getMsgError();
					ok = false;
				} catch(NumberFormatException nfe) {
                    log.error("NumberFormatException:"+nfe);
                    //nfe.printStackTrace();
					arma_error = "Se espera un valor numérico, "+nfe.getCause();
					ok = false;
				} catch(Exception e) {
                    log.error("Exception:"+e);
                    e.printStackTrace();
					arma_error = "El layout es incorrecto, "+e.getCause();
					ok = false;
				}
        ig_nums_doctos.addElement(numero_docto);
			  lineadocs++;
				if (ok==false) {
					if(Comunes.esDecimal(fn_monto)){
						if(moneda == 1) {	//Si es Moneda Nacional
							monto_mn_er += Double.parseDouble(fn_monto);
	          }
	          if(moneda == 54) {	//Si son Dolares.
							monto_dolar_er += Double.parseDouble(fn_monto);
	          }
					}
					vecColumnasCE = new Vector();
					vecColumnasCE.add(numero_docto);
					vecColumnasCE.add(arma_error);
					vecFilasCE.add(vecColumnasCE);
									
					String addError = "";
					if("S".equals(csCarcaterEspecial)){
						addError = ","+arma_error;
					}
					contenidoArchivo.append(cg_num_distribuidor+","+numero_docto+","+fecha_emision+","+fecha_venc +","+ic_moneda +","+fn_monto +","+ic_tipo_financiamiento+","+ig_plazo_descuento +","+fn_porc_descuento +","+ct_referencia +","+campo_ad1 +","+campo_ad2 +","+campo_ad3+","+campo_ad4+","+campo_ad5+ ","+tipo_Pago + ","+ pediodo + ","+addError+"\n");

				} else {
					if(moneda == 1) {	//Si es Moneda Nacional
						rtxttotdoc ++;
						monto_mn_ok += Double.parseDouble(fn_monto);
          }
          if(moneda == 54) {	//Si son Dolares.
						rtxttotdocdol ++;
						monto_dolar_ok += Double.parseDouble(fn_monto);
          }
				}
			} //if de linea > 0
			
		} //while fin de archivo !=null
		
		
		lineadocs=0;
		monto_mn = montoMN;
		monto_dolar =montoDL;
		totdoc_mn = cantMN;
		totdoc_dolar = cantDL;
	
		
		if("S".equals(hidCifrasControl)){
			if(totdoc_mn != otxttotdoc) {
				error_enc.append(" El Total de Documentos de entrada en M.N. "+otxttotdoc+" que se introdujo no coincide con el Total de Documentos en M.N. del Archivo que es de: "+totdoc_mn+". \n");
				ok_enc=false;
			}
			if(totdoc_dolar != otxttotdocdol) {
				error_enc.append(" El Total de Documentos de entrada en Dolares "+otxttotdocdol+" que se introdujo no coincide con el Total de Documentos en Dolares del Archivo que es de: "+totdoc_dolar+". \n");
				ok_enc=false;
			}
	       
			if((monto_mn.compareTo(otxtmtodoc)< 0) || (monto_mn.compareTo(otxtmtodoc)> 0) ) {
				error_enc.append(" El Monto Total de Documentos de entrada en M.N. "+otxtmtodoc.toPlainString()+" no coincide con el Monto del Archivo de Carga que es de: "+monto_mn.toPlainString()+". \n");
				ok_enc=false;
	    }
									
			if((monto_dolar.compareTo(otxtmtodocdol)< 0) || (monto_dolar.compareTo(otxtmtodocdol)> 0) ) {
				error_enc.append(" El Monto Total de Documentos de entrada en Dolares "+otxtmtodocdol.toPlainString()+" no coincide con el Monto del Archivo de Carga que es de: "+monto_dolar.toPlainString()+". \n");
				ok_enc=false;
	    }
		}
		
		vecFilas = cargaDocto.getDoctoInsertado(String.valueOf(ic_proc_docto),ses_ic_epo,null,null);
		vecFilasSE = new Vector();
		String referencia="";
		BigDecimal total_dolares=new BigDecimal("0.0"); BigDecimal total_desc_dol=new BigDecimal("0.0");
		BigDecimal total_mn=new BigDecimal("0.0");      BigDecimal total_desc_mn=new BigDecimal("0.0");
		BigDecimal mtodesc=new BigDecimal("0.0");
		int total_doctos_dol=0, total_doctos_mn=0;
		
		for(i=0;i<vecFilas.size();i++)	{
			vecColumnas = (Vector)vecFilas.get(i);
			referencia= (String)vecColumnas.get(7);
			BigDecimal mtodoc=new BigDecimal((String)vecColumnas.get(6));
			mtodesc=porcentaje.multiply(mtodoc); 
			vecFilasSE.add(vecColumnas);
			/* El Tipo de Moneda */
			if ("54".equals((String)vecColumnas.get(8))) {
				total_dolares = total_dolares.add(new BigDecimal((String)vecColumnas.get(6)));
				total_desc_dol = total_desc_dol.add(mtodesc);
				total_doctos_dol++;
			} else if ("1".equals((String)vecColumnas.get(8))) {
				total_mn = total_mn.add(new BigDecimal((String)vecColumnas.get(6)));
				total_desc_mn = total_desc_mn.add(mtodesc);
				total_doctos_mn++;
			}
			
		} // for
		smonto_mn_ok = String.valueOf(monto_mn_ok);
		smonto_dolar_ok=String.valueOf(monto_dolar_ok);
		smonto_mn_er=String.valueOf(monto_mn_er);
		smonto_dolar_er=String.valueOf(monto_dolar_er);
		
		
		double porLineaCredito=0;
		double montoDocto =0;
		double totalPorLineaCredito =0;
		String numDoctoCE	= "";
		String errorCE	 	= "";
		String numDoctoSE	= "";
		String pymeSE	 	= "";
		String montoSE		= "";
			
		for(int x=0;x<vecFilasSE.size()||x<vecFilasCE.size();x++){
			numDoctoSE	= "";
			pymeSE	 	= "";
			montoSE		= "";			
			numDoctoCE		= "";
			errorCE	   		= "";
				
			if(x<vecFilasSE.size()){
				vecColumnasSE	= (Vector)vecFilasSE.get(x);
				numDoctoSE		= (String)vecColumnasSE.get(2);
				pymeSE	   		= (String)vecColumnasSE.get(0);
				montoSE			= (String)vecColumnasSE.get(6);
			}
			if(!montoSE.equals("") && limiteLineaCredito.equals("S") ){
				LineaCredito2=Double.parseDouble((String)lineaCredito);
				montoDocto = Double.parseDouble((String)montoSE);
				porLineaCredito =  (montoDocto * 100)/LineaCredito2;
				totalPorLineaCredito = 100-porLineaCredito;
			}
			
			if(x<vecFilasCE.size()){
				vecColumnasCE	= (Vector)vecFilasCE.get(x);
				numDoctoCE		= (String)vecColumnasCE.get(0);
				errorCE	   		= (String)vecColumnasCE.get(1);
			}			
			
			datos.put("DESCRIP_SERROR",numDoctoSE);	
			datos.put("PYME_SERROR",pymeSE);	
			datos.put("MONTO_SERROR",montoSE);
			if(limiteLineaCredito.equals("S")){
				datos.put("DES_PORCENTAJE", Double.toString (totalPorLineaCredito));
			}else {					
				datos.put("DES_PORCENTAJE", "0");
			}
			datos.put("NUDOCTO_CERROR",numDoctoCE);					
			datos.put("CERROR",errorCE);				
			registros.add(datos);				
		}//FOR			
			
		String 	btnConfirmaCarga  ="N";
		if ((ok_enc) && (rtxttotdoc > 0 || rtxttotdocdol >0)) {
			btnConfirmaCarga ="S";
		}	
			
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		resultado = JSONObject.fromObject(consulta);	
		resultado.put("NUMERO_PROCESO", String.valueOf(ic_proc_docto));	
		resultado.put("TOTALDOCS",String.valueOf(vecFilasSE.size()));	
		resultado.put("MONTO_TOTAL_MNS", total_mn.toPlainString());	
		resultado.put("MONTO_TOTAL_DLS", total_dolares.toPlainString());	
		resultado.put("TOTALDOCE", String.valueOf(vecFilasCE.size()));
		resultado.put("MONTO_TOTAL_MNE", smonto_mn_er);	
		resultado.put("MONTO_TOTAL_DLE", smonto_dolar_er);	
		resultado.put("ERROR_ENC", error_enc.toString());	
		resultado.put("ARCHIVO_ERRORES", contenidoArchivo.toString());	
		resultado.put("limiteLineaCredito", limiteLineaCredito);
		resultado.put("btnConfirmaCarga", btnConfirmaCarga);
		
		infoRegresar = resultado.toString();


	}


%>
<%
} catch(Exception ex) {
	System.out.println("La Exception: "+ex.toString()); 
}

%>
<%=infoRegresar%>