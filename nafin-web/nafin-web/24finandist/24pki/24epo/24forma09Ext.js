
Ext.onReady(function() {
	
	var textoFirmar; 
	var totaldoc;
	var totaldocdl;
	var montodoc;
	var montodocdl;
	var lineabien;
	var lineaerror;
	var linea;
	var icDoctos;
	var strPreacuse;
	var table;	
	var fpError = null;		
	//para mostrar los archivos con errores
	var procesarSuccessFailureArchivoError =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	function mostrarArchivoTXT(opts, success, response) {
		//Ext.getCmp('btnGenerarArchivoCSV').enable();
		//var btnImprimirCSV = Ext.getCmp('btnGenerarArchivoCSV');
		//btnImprimirCSV.setIconClass('icoXls');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//valida la carga  Masiva de Documentos 
	var procesarSuccessValidaCarga = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;		 
		var error_cifras = jsonData.ERROR_CIFRAS;		
		var lineas_error = jsonData.LINEAS_ERROR;	
		var strErrores = jsonData.STRERRORES;	
		var errores = jsonData.ERRORES;	
		var bien = jsonData.BIEN;	
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var fp = Ext.getCmp('formaCarga');		
		fp.el.unmask();
		
		var table = '<table>'+
								'<tr>'+
									'<td class="formas" align="center">'+
										'Documentos sin Errores:'+
									'</td>'+
									'<td class="formas" align="center">'+
										'Detalle de Documentos con Errores:'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td class="formas">'+
										'<textarea name="sinerrores" cols="80" wrap="hard" rows="5" class="formas">'+bien+'</textarea>'+
									'</td>'+
									'<td class="formas">'+
										'<textarea name="conerroresE" cols="80" wrap="hard" rows="5" class="formas">'+errores+'</textarea>'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td class="formas" align="center">'+
										'Errores Vs. Cifras de Control'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td class="formas">'+
										'<textarea name="conerrores" cols="80" wrap="hard" rows="5" class="formas">'+error_cifras+'</textarea>'+
									'</td>'+
								'</tr>'+
							'</table>';
							
		
			 fpError = new Ext.Container({
				layout: 'table',
				id: 'fpError',				
				width:	'auto',
				heigth:	'auto',
				hidden: true,
				style: 'margin:0 auto;',
				items: [		
					{ 
					xtype:   'label',  
					html:		table, 
					cls:		'x-form-item'
					}
				]
			});
			
			
			
			//boton para mostrar los errores de la carga 
			var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
			btnGenerarCSV.setHandler(
				function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');								
					Ext.Ajax.request({
						url: '24forma09ExtArchi.jsp',
						params: Ext.apply(fp.getForm().getValues(),{														
							archivo: strErrores,											
							informacion:'GenerarArchivoError'
						}),
						callback: procesarSuccessFailureArchivoError
					});
				}
			);
			contenedorPrincipalCmp.remove(fpError);
					
			if(strErrores!='' || errores!='' || error_cifras!=''){
				fpError.show();
				contenedorPrincipalCmp.add(fpError);
				contenedorPrincipalCmp.add(fpGenerarC);
				btnGenerarCSV.show();
			}else {						
				btnGenerarCSV.hide();				
			}
		
		if (arrRegistros != null) {	
			//el grid de  Arriba
			var cifras = [
				['No. total de documentos cargados', jsonData.N0_MONEDA_NACIONAL, jsonData.NO_MONEDA_DOLAR],
				['Monto total de los documentos cargados',Ext.util.Format.number(jsonData.MON_MONEDA_NACIONAL.replace(/[\$]/g, ''),'$0,00.00') , Ext.util.Format.number(jsonData.MON_MONEDA_DOLAR.replace(/[\$]/g, ''),'$0,00.00')]				
			];	
			
			if (!gridResulCarga.isVisible()) {				
				gridResulCarga.show();				
			}		
			
			var el = gridResulCarga.getGridEl();			
			if(store.getTotalCount() > 0) {	
				fpCarga.hide();		
				gridLayout.hide();	
				fpError.hide();
				btnGenerarCSV.hide();
			
				//se llena el grid de Totales 
				var CifrasTotales = [
					['Total Moneda Nacional ', Ext.util.Format.number(jsonData.TOTAL_N.replace(/[\$]/g, ''),'$0,00.00') ],
					['Total Dolares' , Ext.util.Format.number(jsonData.TOTAL_D.replace(/[\$]/g, ''),'$0,00.00') ],
					['Total Documentos en Moneda Nacional', jsonData.TOTAL_DN],
					['Total Documentos en Dolares', jsonData.TOTAL_DD]
				];		
		
			
			//boton para continuar con el proceso
			var btnContinuarP = Ext.getCmp('btnContinuarP');
			var btnConfirma = Ext.getCmp('btnConfirma');
			var btnCancelar = Ext.getCmp('btnCancelar');	
			if(error_cifras==''){
				btnGenerarCSV.hide();
				btnContinuarP.hide();				
				totalesData.loadData(cifras);	
				gridTotales.show();
				gridResumTotales.show();
				fpConfirmar.show();					
				btnConfirma.show();
				btnCancelar.show();					
				resumenTotalesData.loadData(CifrasTotales);	
				gridResumTotales.show();														
			}else {										
				gridTotales.hide();				
				btnContinuarP.hide();
			}	
			
			
			//valores para enviar al preacuse 
			textoFirmar = "Total Moneda Nacional: "+  Ext.util.Format.number(jsonData.TOTAL_N.replace(/[\$]/g, ''),'$0,00.00')+"\n" +
			"Total Dolares: "+Ext.util.Format.number(jsonData.TOTAL_D.replace(/[\$]/g, ''),'$0,00.00')+"\n"+
			"Total Documentos en Moneda Nacional: "+jsonData.TOTAL_DN+"\n"+
			"Total Documentos en Dolares: "+jsonData.TOTAL_DD;
			
			totaldoc = jsonData.TOTAL_DN;
			totaldocdl  = jsonData.TOTAL_DD;
			montodoc  = jsonData.TOTAL_N;
			montodocdl = jsonData.TOTAL_D;				
			lineabien = jsonData.LINEAS_BIEN;
			lineaerror = jsonData.LINEAS_ERROR;
			linea = jsonData.LINEAS;
			icDoctos = jsonData.IC_DOCTOS;
			strPreacuse = jsonData.STRPREACUSE;
				
			el.unmask();
		} else {
			el.mask('No se encontr� ning�n registro', 'x-mask');
			gridResulCarga.hide();	
		}		
		}
		contenedorPrincipalCmp.doLayout();
		
	}
	
	var procesarSuccessFailureGenerarPDFAcuse =  function(opts, success, response) {
		var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
		btnGenerarPDFAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			btnBajarPDFAcuse.show();
			btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPDFAcuse.show();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		//para el boton de CSV de acuse 
	var procesarSuccessFailureGenerarCSVAcuse =  function(opts, success, response) {
		var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
		btnGenerarCSVAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSVAcuse = Ext.getCmp('btnBajarCSVAcuse');
			btnBajarCSVAcuse.show();
			btnBajarCSVAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSVAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarCSVAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	
	}
	
	
	//guarda los ajustes y genera acuse 
	var procesarSuccessFailureGuardar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){
				
				var btnConfirma = Ext.getCmp('btnConfirma');
				var btnCancelar = Ext.getCmp('btnCancelar');
				btnConfirma.hide();
				btnCancelar.hide();
				
				var acuseCifras = [
					['N�mero de Acuse', jsondeAcuse.acuse],
					['Fecha ', jsondeAcuse.fecha],
					['Hora ', jsondeAcuse.hora],
					['Usuario ', jsondeAcuse.usuario]
				];				
				storeCifrasData.loadData(acuseCifras);	
				gridCifrasControl.show();
				
				//botones de Acuse 
				fpConfirmar.show();
				var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
				var btnGenerarCSVAcuse = Ext.getCmp('btnGenerarCSVAcuse');
				var btnSalir = Ext.getCmp('btnSalir');
				btnGenerarPDFAcuse.show();
				btnGenerarCSVAcuse.show();
				btnSalir.show();
											
				btnGenerarPDFAcuse.setHandler(function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');								
				Ext.Ajax.request({
					url: '24forma09ExtArchi.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						acuse:jsondeAcuse.acuse,
						fecha:jsondeAcuse.fecha,
						hora:jsondeAcuse.hora,
						usuario:jsondeAcuse.usuario,
						recibo:jsondeAcuse.recibo,
						informacion:'ACUSEPDF',
						tipoArchivo:'PDF',
						totaldoc: totaldoc,
						totaldocdl: totaldocdl,
						montodoc: montodoc,
						montodocdl: montodocdl,			
						lineabien:lineabien,
						lineaerror:lineaerror,
						linea: linea,
						icDoctos:icDoctos,
						strPreacuse:strPreacuse
					}),
					callback: procesarSuccessFailureGenerarPDFAcuse
				});
			});
			
			
			//csv
			btnGenerarCSVAcuse.setHandler(function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');								
				Ext.Ajax.request({
					url: '24forma09ExtArchi.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						acuse:jsondeAcuse.acuse,
						fecha:jsondeAcuse.fecha,
						hora:jsondeAcuse.hora,
						usuario:jsondeAcuse.usuario,
						recibo:jsondeAcuse.recibo,
						informacion:'ACUSECSV',
						tipoArchivo:'CSV',
						totaldoc: totaldoc,
						totaldocdl: totaldocdl,
						montodoc: montodoc,
						montodocdl: montodocdl,			
						lineabien:lineabien,
						lineaerror:lineaerror,
						linea: linea,
						icDoctos:icDoctos,
						strPreacuse:strPreacuse
					}),
					callback: procesarSuccessFailureGenerarCSVAcuse
				});
			});
			
					
			}						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var confirmarAcuse = function(pkcs7, vtextoFirmar){
	
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
			
		}else  {
				
			Ext.Ajax.request({
				url : '24forma09Ext.data.jsp',
				params : {
					pkcs7: pkcs7,
					textoFirmado: vtextoFirmar,
					informacion: 'ACUSE',
					totaldoc: totaldocdl,
					totaldocdl: totaldocdl,
					montodoc: montodoc,
					montodocdl: montodocdl,			
					lineabien:lineabien,
					lineaerror:lineaerror,
					linea: linea,
					icDoctos:icDoctos,
					strPreacuse:strPreacuse
				},
				callback: procesarSuccessFailureGuardar
			});					
		}	
	}
	
	
	function procesarConfirmar(opts, success, response) {
		
		NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar);
	
		
	}
	var procesaCargaMasiva = function(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaMasiva(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaMasiva(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	var cargaMasiva = function(estado, respuesta ){
	if(			estado == "SUBIR_ARCHIVO"					){
			
		fpCarga.getForm().submit({
			url: '24forma09file.data.jsp',									
			waitMsg: 'Enviando datos...',
				success: function(form, action) {								
					procesaCargaMasiva(null,  true,  action.response );
				}
				,failure: NE.util.mostrarSubmitError
			})	
			
	} else if(	estado == "VALIDA_CARACTERES_CONTROL"				){
			var nombreArchivo	= respuesta.nombreArchivo;
		 if(nombreArchivo!='') {	
				Ext.getCmp('nombArchivo').setValue(nombreArchivo);
				Ext.Ajax.request({
					url: 		'24forma09Ext.data.jsp',
					params: 	{
						informacion:				'validaCaracteresControl',
						nombreArchivo: 	nombreArchivo
					},
					callback: 				procesaCargaMasiva
				});
			}	else{
				Ext.MessageBox.alert("Mensaje","El Archivo es muy Grande, excede el L�mite que es de 2 MB.");
				return;
			}
		} else if(	estado == "MOSTRAR_AVANCE_THREAD"				){
			Ext.Ajax.request({
				url: 		'24forma09Ext.data.jsp',
				params: 	{
					informacion:				'avanceThreadCaracteres'
				},
				callback: 				procesaCargaMasiva
			});
		
		}else if(	estado == "HAY_CARACTERES_CONTROL"				){
				var mensaje	= respuesta.mns;
				var nombreArchivoCaractEsp	= respuesta.nombreArchivoCaractEsp;
				var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
				Ext.getCmp('nombArchivoDetalle').setValue(nombreArchivoCaractEsp);
				fpCarga.hide();
				if (gridLayout.isVisible()) {
					gridLayout.hide();
				}
				if(fpError!=null){
					fpError.hide();
					Ext.getCmp('btnGenerarCSV').hide();
				}
				caracteresEspeciales.show();
				caracteresEspeciales.setMensaje(mensaje);
		}else if(	estado == "ERROR_THREAD_CARACTERES"				){
			var mensaje	= respuesta.mns;
			Ext.Msg.alert("Error", mensaje );
			return;
		}else if(	estado == "VALIDA_CARGA_DOCTO"				){
			var mArchivo =Ext.getCmp('nombArchivo').getValue();
			if(mArchivo!='') {							
				resultadosCargaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ValidaCargaArchivo',
						archivo:mArchivo																									
					})
				});															
			}	else{
				Ext.MessageBox.alert("Mensaje","El Archivo es muy Grande, excede el L�mite que es de 2 MB.");
				return;
			}
		}else if(	estado == "DESCARGAR_ARCHIVO"				){
			var archivo = Ext.getCmp('nombArchivoDetalle').getValue();
			Ext.Ajax.request({
				url: 		'24forma09Ext.data.jsp',
				params: 	{
					informacion:				'descargaArchivoDetalle',
					archivo : archivo
				},
				callback: 				mostrarArchivoTXT
			});
		}else if(	estado == "MENSAJE_CODIFICACION"								){
			new NE.cespcial.AvisoCodificacionArchivo({codificacion:respuesta.codificacionArchivo}).show();
 
		}
		return;
	}

	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
 //Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
	id: 'gridCifrasControl',
	store: storeCifrasData,
	margins: '20 0 0 0',
	style: 'margin:0 auto;',
	hideHeaders : true,
	hidden: true,
	align: 'center',
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 200,
			sortable : true
		},
		{
			header : 'Informacion',			
			dataIndex : 'informacion',
			width : 350,
			sortable : true,
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 560,
	style: 'margin:0 auto;',
	autoHeight : true,
	//title: '',
	frame: true
	});
	
	var totalesData = new Ext.data.ArrayStore({
		  fields: [
				{name: 'DESCRIPCION'},
			  {name: 'MONEDA_NACIONAL'},
			  {name: 'MONEDA_DOLAR'}							
		  ]
	 })
	 
	
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',
		store: totalesData,	
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		align: 'center',
		title: '',		
		hidden: true,
		columns: [	
			{
				header: '',
				dataIndex: 'DESCRIPCION',
				width: 250,
				align: 'left'			
			},	
			{
				header: 'Moneda Nacional',
				dataIndex: 'MONEDA_NACIONAL',
				width: 150,
				align: 'right'				
			},
			{
				header: 'Dolares',
				dataIndex: 'MONEDA_DOLAR',
				width: 150,
				align: 'right'						
			}					
		],
		height: 100,	
		width : 560,
		frame: false
	});
	
	
				//para mostrar el Acuse 
	var resumenTotalesData = new Ext.data.ArrayStore({
		fields: [
			{name: 'etiqueta'},
			{name: 'informacion'}
		  ]
	 });
	 
	 
	 	//Grid del Acuse 
	var gridResumTotales = new Ext.grid.GridPanel({
		id: 'gridResumTotales',
		store: resumenTotalesData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		align: 'center',
		columns: [
		{
			header : '',
			dataIndex : 'etiqueta',
			width : 300,
			sortable : true
		},
		{
			header : '',			
			dataIndex : 'informacion',
			width : 150,
			sortable : true		
		},
		{
			header : '',			
			dataIndex : '',
			width : 493,
			sortable : true		
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 943,
	height: 300,
	style: 'margin:0 auto;',
	autoHeight : true,
	title: '',
	frame: true
	});
	
	
	//Resultados del proceso de carga
		var resultadosCargaData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24forma09Ext.data.jsp',
		baseParams: {
			informacion: 'ValidaCargaArchivo'		
		},								
		fields: [			
			{name: 'PROVEEDOR', mapping: 'PROVEEDOR'},
			{name: 'NO_DOCTO', mapping: 'NO_DOCTO'},
			{name: 'MONTO', mapping: 'MONTO'},
			{name: 'FECHA_VENCIMIENTO', mapping: 'FECHA_VENCIMIENTO'},
			{name: 'MODALIDAD_ANTE', mapping: 'MODALIDAD_ANTE'},
			{name: 'MODALIDAD_NUEVA', mapping: 'MODALIDAD_NUEVA'},
			{name: 'CAUSA', mapping: 'CAUSA'}					
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarSuccessValidaCarga,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarSuccessValidaCarga(null, null, null);						
				}
			}
		}
	});	
		
	
	var gridResulCarga = new Ext.grid.EditorGridPanel({	
		id: 'gridResulCarga',
		store: resultadosCargaData,	
		margins: '20 0 0 0',
		align: 'center',
		style: 'margin:0 auto;',			
		title: '',		
		hidden: true,
		columns: [					
			{
				header: 'Clave del proveedor',
				dataIndex: 'PROVEEDOR',
				width: 150,
			
				resizable: true	,
				align: 'center'				
			},
			{
				header: 'N�mero de Documento',
				dataIndex: 'NO_DOCTO',
				width: 150,				
				resizable: true	,
				align: 'center'				
			},
			{
				header: 'Monto',
				dataIndex: 'MONTO',
				width: 150,				
				resizable: true	,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				width: 150,				
				resizable: true	,
				align: 'center'				
			},
			{
				header: 'Modalidad de Plazo Anterior',
				dataIndex: 'MODALIDAD_ANTE',
				width: 150,				
				resizable: true	,
				align: 'center'				
			},
			{
				header: 'Modalidad de Plazo Nuevo',
				dataIndex: 'MODALIDAD_NUEVA',
				width: 150,
				height: 200,
				resizable: true	,
				align: 'center'				
			},
			{
				header: 'Causa',
				dataIndex: 'CAUSA',
				width: 150,				
				resizable: true	,
				align: 'center'				
			} 	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		align: 'center',
		frame: false
	});
	

	
		var myValidFn = function(v) {
			var myRegex = /^.+\.([tT][xX][tT])$/;
			return myRegex.test(v);
		}
		Ext.apply(Ext.form.VTypes, {
			archivotxt 		: myValidFn,
			archivotxtText 	: 'El formato del archivo de origen no es el correcto para el tipo de archivo seleccionado'
		});
		
	var fpGenerarC = new Ext.Container({
		layout: 'table',
		id: 'fpGenerarC',
		hidden: false,
		layoutConfig: {
			columns: 6
		},
		width:	'100',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
		{
			xtype: 'button',
			text: 'Descargar archivo',
			id: 'btnGenerarCSV',
			hidden: true
		}, 
		{
			xtype: 'button',
			text: ' Archivo',
			id: 'btnBajarCSV',
			hidden: true
		}	,	
		{
			xtype: 'button',
			text: 'Procesar',
			id: 'btnContinuarP',			
			hidden: true
		}	
		
	]
	});
		
	var fpConfirmar = new Ext.Container({
		layout: 'table',
		id: 'fpConfirmar',
		hidden: true,
		layoutConfig: {
			columns: 20
		},
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
		{
			xtype: 'button',
			text: 'Confirmar documentos Negociables',
			id: 'btnConfirma',
			iconCls: 'icoAceptar',
			handler: procesarConfirmar,	
			hidden: true
		},		
		{
			xtype: 'button',
			text: 'Cancelar',			
			iconCls: 'icoLimpiar',
			id: 'btnCancelar',
			hidden: true,
			handler: function() {
			window.location = '24forma09Ext.jsp';
			}
		},
		//estos son para el acuse
		{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnGenerarPDFAcuse',
				hidden: true
			},			
			{
				xtype: 'button',
				text: 'Bajar PDF',
				id: 'btnBajarPDFAcuse',
				hidden: true
			},
			//'-',
			{
				xtype: 'button',
				text: 'Generar Archivo',
				id: 'btnGenerarCSVAcuse',
				hidden: true
			},			
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				id: 'btnBajarCSVAcuse',
				hidden: true
			},
			//'-',
			
			{
				text: 'Salir',
				xtype: 'button',
				id: 'btnSalir',
				hidden: true,
				handler: function() {						
					window.location = '24forma09Ext.jsp';
				}
			}
			
	]
	})
	
	

	//STORES////////////////////////////////////////////////////////////////////////
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMCAMPO'},
		  {name: 'DESC'},
		  {name: 'TIPODATO'},
		  {name: 'LONGITUD'},
		  {name: 'OBLIGATORIO'}
	  ]
	});
 
 	//HANDLERS PARA OBJETOS BUTTON/////////////////////////////////////////////////
	var infoLayout = [
		['1','Numero de Distribuidor','Num�rico','8','Si'],
		['2','No de Documento','AlfaNum�rico','','Si'],		
		['3','Fecha de Emisi�n del docto.','Fecha','dd/mm/aa','Si'],				
		['4','Clave de la Moneda','Num�rico','1','Si'],	
		['5','Modalidad de Plazo','Alfanum�rico','','Si'],		
		['6','Tipo de Financiamiento','Num�rico','3','Si'],		
		['7','Tipo de Modalidad','N�merico','','Si'],
		['8','Causa del Mantenimiento ','Alfanum�rico','','Si']
	];

	storeLayoutData.loadData(infoLayout);
 
	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'No. de Campo',
				dataIndex : 'NUMCAMPO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Descripci�n',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'DESC',
				width : 200,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Tipo de Dato',
				dataIndex : 'TIPODATO',
				width : 100,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Longitud',
				dataIndex : 'LONGITUD',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 100,
				sortable : true,
				align: 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 630,
		height: 230,
		frame: true
	});
	
	
	var elementosFormaCarga = [	
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){						
					
					if (!gridLayout.isVisible()) {
							gridLayout.show();
						}else{
							gridLayout.hide();
						}
					
					}
				},
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth:.95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 70,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									emptyText: 'Nombre del Archivo',
									fieldLabel: 'Nombre del Archivo',
									name: 'archivoFormalizacion',   
									buttonText: '',
									width: 340,	  
									buttonCfg: {
										iconCls: 'upload-icon'
									},
									anchor: '95%',
									vtype: 'archivotxt'
								}						
							]
						}
					]
				},
				{  
				  xtype:	'hidden',  
				  name:	'nombArchivo',
				  id: 	'nombArchivo'
				},
				{  
				  xtype:	'hidden',  
				  name:	'nombArchivoDetalle',
				  id: 	'nombArchivoDetalle'
				}	
			]
		}	
	];
	var fpCarga = new Ext.form.FormPanel({
	  id: 'formaCarga',
		width: 500,
		title: 'Ubicaci�n del archivo de datos',
		frame: true,
		//hidden: true,
		collapsible: true,
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		
		items: elementosFormaCarga,			
		monitorValid: true,
		buttons: [
			{
				text: 'Continuar',
				id: 'continuar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:  function(){
					var archivo = Ext.getCmp("archivo");
					if (Ext.isEmpty(archivo.getValue()) ) {
						archivo.markInvalid('El valor de la Ruta es requerida.');	
						return;
					}else {						
						 cargaMasiva("SUBIR_ARCHIVO", null);
						 
					}	
				}
			}
		]
	});
	

//****************************************CIFRAS DE CONTROL***************************************

	var enviar = function() {
		
		var txttotdoc = Ext.getCmp("txttotdoc1");
		var txtmtodoc = Ext.getCmp("txtmtodoc1");
		var txtmtomindoc = Ext.getCmp("txtmtomindoc1");
		var txtmtomaxdoc  = Ext.getCmp("txtmtomaxdoc1");
		
		var txttotdocdo = Ext.getCmp("txttotdocdol");
		var txtmtodocdo = Ext.getCmp("txtmtodocdol");
		var txtmtomindocdo = Ext.getCmp("txtmtomindocdo2");
		var txtmtomaxdocdo  = Ext.getCmp("txtmtomaxdocdo2");
		
		Ext.getCmp("txttotdoc1").setValue(eliminaFormatoFlotante(txttotdoc.getValue()));
		Ext.getCmp("txtmtodoc1").setValue(eliminaFormatoFlotante(txtmtodoc.getValue()));
		Ext.getCmp("txtmtomindoc1").setValue(eliminaFormatoFlotante(txtmtomindoc.getValue()));
		Ext.getCmp("txtmtomaxdoc1").setValue(eliminaFormatoFlotante(txtmtomaxdoc.getValue()));
		
		Ext.getCmp("txttotdocdol").setValue(eliminaFormatoFlotante(txttotdocdo.getValue()));
		Ext.getCmp("txtmtodocdol").setValue(eliminaFormatoFlotante(txtmtodocdo.getValue()));
		Ext.getCmp("txtmtomindocdo2").setValue(eliminaFormatoFlotante(txtmtomindocdo.getValue()));
		Ext.getCmp("txtmtomaxdocdo2").setValue(eliminaFormatoFlotante(txtmtomaxdocdo.getValue()));
		
		var continuar  = Ext.getCmp("continuar1");		
		
		var continua = true;
				
		if (Ext.isEmpty(txttotdoc.getValue())   && Ext.isEmpty(txtmtodoc.getValue())  
		&&  Ext.isEmpty(txttotdocdo.getValue()) &&  Ext.isEmpty(txtmtodocdo.getValue()) 
		) {
			Ext.MessageBox.alert("Mensaje","Por favor escriba el  n�mero total documentos en Moneda Nacional y/o Dolares");
			continua = false;
			return;
		}		
		//moneda Nacional
		if(!Ext.isEmpty(txttotdoc.getValue())  ) {
		
			if(Ext.isEmpty(txtmtodoc.getValue()) || txtmtodoc.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomindoc.getValue()) || txtmtomindoc.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomaxdoc.getValue()) || txtmtomaxdoc.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
				continua = false;
				return;
			}
			
			if(txtmtomaxdoc.getValue()<txtmtomindoc.getValue() ){
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Moneda Nacional)");
				continua = false;
				return;
			
			}
			
			if(txtmtodoc.getValue()<txtmtomaxdoc.getValue() ){
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Moneda Nacional)");
				continua = false;
				return;
			}
		
		}
		//Dolares
		if(!Ext.isEmpty(txttotdocdo.getValue())  ) {	
		
			if(Ext.isEmpty(txtmtodocdo.getValue()) || txtmtodocdo.getValue()==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomindocdo.getValue()) || txtmtomindocdo.getValue()==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomaxdocdo.getValue()) || txtmtomaxdocdo.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
				continua = false;
				return;
			}
			
			if(txtmtomaxdocdo.getValue()<txtmtomindocdo.getValue() ){
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Dolares)");
				continua = false;
				return;
			
			}
			
			if(txtmtodocdo.getValue()<txtmtomaxdocdo.getValue() ){
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Dolares)");
				continua = false;
				return;
			}
		}	
		
			
		
		
		
		
		if(continua ==true) {
			fpCarga.show();
			fp.hide();	
		}	
	}
	 	
	var elementosForma = [	
		{
				xtype: 'textfield',
				name: 'continuar',
				id: 'continuar1',
				fieldLabel: 'continuar',
				allowBlank: true,
				startDay: 0,
				maxLength: 7,	//ver el tama�o maximo del numero en BD para colocar este igual
				width: 200,
				msgTarget: 'side',						
				margins: '0 20 0 0',				
				value:	'S',
				hidden: true
		},				
		{ 
				xtype:   'label',  
				html:		'Moneda Nacional', 
				cls:		'x-form-item', 
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				} 
			},		
			{
				xtype: 'numberfield',
				name: 'txttotdoc',
				id: 'txttotdoc1',
				fieldLabel: 'No. total de documento',
				allowBlank: true,
				startDay: 0,
				maxLength: 7,	//ver el tama�o maximo del numero en BD para colocar este igual
				width: 200,
				msgTarget: 'side',						
				margins: '0 20 0 0'		
		},		
		{
			xtype: 'textfield',
			name: 'txtmtodoc',
			id: 'txtmtodoc1',
			fieldLabel: 'Monto total de documento',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',			
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomindoc',
			id: 'txtmtomindoc1',
			fieldLabel: 'Monto m�nimo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomaxdoc',
			id: 'txtmtomaxdoc1',
			fieldLabel: 'Monto m�ximo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		}	,
		{ 
			xtype:   'label',  
			html:		'D�lares', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 'numberfield',
			name: 'txttotdocdo',
			id: 'txttotdocdol',
			fieldLabel: 'No. total de documento',
			allowBlank: true,
			startDay: 0,
			maxLength: 7,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0'		
		}	,
		{
			xtype: 'textfield',
			name: 'txtmtodocdo',
			id: 'txtmtodocdol',
			fieldLabel: 'Monto total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		}	,
		{
			xtype: 'textfield',
			name: 'txtmtomindocdo',
			id: 'txtmtomindocdo2',
			fieldLabel: 'Monto m�nimo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		}	,
		{
			xtype: 'textfield',
			name: 'txtmtomaxdocdo',
			id: 'txtmtomaxdocdo2',
			fieldLabel: 'Monto m�ximo de los documentos:',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		}			
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: '',
		frame: true,
		//collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Enviar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: enviar
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24forma09Ext.jsp';
				}
			}
		]
	});
	var caracteresEspeciales = new NE.cespcial.CaracteresEspeciales(
		{
		hidden: true
		}
	);
	
	caracteresEspeciales.setHandlerAceptar(function(){
		caracteresEspeciales.hide(),
		fpCarga.show();
	});
	
	caracteresEspeciales.setHandlerDescargarArchivo(function(){
		 cargaMasiva("DESCARGAR_ARCHIVO", null);
	});
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fp,			
			fpCarga,
			gridTotales,	
			gridCifrasControl,
			NE.util.getEspaciador(20),
			fpConfirmar,
			NE.util.getEspaciador(20),
			gridResulCarga,						
			gridResumTotales,						
			gridLayout,
			caracteresEspeciales		
		]
	});
	
	fpCarga.hide();

	
} );