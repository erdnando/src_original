<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
	String sistemaMenu = (request.getParameter("tipoMenu") != null) ? request.getParameter("tipoMenu") : "";
%>
<html>
<head>
<title>.:: N@fin Electr?nico :: Financiamiento a Distribuidores ::.</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%@ include file="/00utils/componente_firma.jspf" %>
<script type="text/javascript" src="24forma08ext.js?<%=session.getId()%>"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<% if("EPO".equals(strTipoUsuario)) { %>

	<%@ include file="/01principal/01epo/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">
				<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
				<div id="areaContenido"><div style="height:230px"></div></div>
			</div>
		</div>
		
		<%@ include file="/01principal/01epo/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
		
	<form id='formParametros' name="formParametros">
		<input type="hidden" id="sistemaMenu" name="sistemaMenu" value="<%=sistemaMenu%>"/>	
	</form>


<% } else   if("NAFIN".equals(strTipoUsuario)) { 
	sistemaMenu = "Consultar";
%>
	<%if(esEsquemaExtJS)  {%>
		
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
				<div id="areaContenido"><div style="height:230px"></div></div>
			</div>
		</div>
		
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
	<%}else  {%>
		
		<div id="Contcentral">
			<div id="areaContenido"><div style="height:230px"></div></div>
		</div>
		</div>
		<form id='formAux' name="formAux" target='_new'></form>				
	<%}%>
	
	<form id='formParametros' name="formParametros">
		<input type="hidden" id="sistemaMenu" name="sistemaMenu" value="<%=sistemaMenu%>"/>	
	</form>
<% } %> 

</body>
</html>