<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.anticipos.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	java.text.SimpleDateFormat,
	java.util.Date,
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String tipoArchivo = (request.getParameter("tipoArchivo") != null) ? request.getParameter("tipoArchivo") : "";
CreaArchivo archivo = new CreaArchivo();
String nombreArchivo = "";	
ComunesPDF pdfDoc = new ComunesPDF();
StringBuffer contenidoArchivo = new StringBuffer("");
JSONObject jsonObj = new JSONObject();
String fechaHoy		= "";
Vector	vecColumnas	= null;
int totalDoctosMN	=	0, 	totalDoctosUSD = 0,  totaldPlazo =0,	elementos  = 0,	totalCreditosMN		=0, totalCreditosUSD	=	0;
double	totalMontoMN	=	0, totalMontoUSD =0,	totalMontstrFacultad =0,	totalMontoDescMN	=	0,totalMontoDescUSD	=	0,
totalInteresMN =	0,	totalInteresUSD	=	0,	totalMontoValuadoMN	=	0,	totalMontoValuadoUSD=	0,totalMontoCreditoMN	=	0,
totalMontoCreditoUSD=	0,  tasaInteres = 0, montoValuadoLinea  = 0, totalMontoMNDesA =0, totalMontoUSDDesA =0; 
		
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}



	
try {
	if(informacion.equals("GenerarArchivo")) {
		AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
		
		String acuse =  (request.getParameter("acuse") != null) ? request.getParameter("acuse") : "";
		String fechaCarga = "",  horaCarga = "";
		
		//Fodea 020-2014
		ParametrosDist  BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
		String	ventaCartera =  BeanParametro.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); 

	
		
		Vector vecFilas = null;
		if (!acuse.equals("") || acuse!=null ) {
			vecFilas = aceptPyme.consDoctosVCartera(acuse);	
		}
		
		if(tipoArchivo.equals("PDF")){		
			 archivo = new CreaArchivo();
			 nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";	
			 pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
		
			pdfDoc.addText("Mexico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("Selección de Documentos Venta de Cartera ","formas",ComunesPDF.CENTER);
			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
			pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
			String mensaje="";
			if(ventaCartera.equals("S") ) {
				mensaje = "Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del (los) Documento (s) final (es) descrito (s) en el mismo derivado del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO según corresponda. Dicha aceptación tendrá plena validez para todos los efectos legales a que haya lugar. En este mismo acto se genera el aviso de notificación a la EMPRESA DE PRIMER ORDEN.  En términos de los artículos 32 C del Código Fiscal y 427 de la Ley General de Títulos y Operaciones de Crédito, para los efectos legales conducentes, el INTERMEDIARIO FINANCIERO se obliga a notificar por sus propios medios al CLIENTE Y/O DISTRIBUIDOR la transmisión de los derechos y/o cuentas por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO. No obstante, en términos de los citados artículos, en caso de existir COBRANZA DELEGADA, el INTERMEDIARIO FINANCIERO no estará obligado a notificar al CLIENTE Y/O DISTRIBUIDOR la transmisión de los derechos y/o cuentas por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO";
			}else  {
				mensaje ="Al trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, está solicitando el DESCUENTO y/o "+
                "FACTORAJE del (los) DOCUMENTOS (S) INICIAL (ES) que se detallan en está pantalla y que ha publicado electrónicamente, otorgando su consentimiento para que "+
                "en caso de que se dén las condiciones señaladas en el Contrato de Financiamiento a Clientes y Distribuidores, el 	(LOS) DOCUMENTOS (S) INICIAL (ES) 	"+
                "serán sustituidos por el (los) DOCUMENTO (S) FINAL (ES), cediendo los derechos de cobro a su favor al INTERMEDIARIO FINANCIERO que los opere";
			
			}
			pdfDoc.setTable(12, 100);
			pdfDoc.setCell("Moneda nacional","celda01",ComunesPDF.CENTER,4);
			pdfDoc.setCell("Dólares","celda01",ComunesPDF.CENTER,4);
			pdfDoc.setCell("Doctos. en DLL financiados en M.N.","celda01",ComunesPDF.CENTER,4);
	
			pdfDoc.setCell("Num. total de doctos","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto del financiamiento","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
			
			pdfDoc.setCell("Num. total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto del financiamiento","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
			
			pdfDoc.setCell("Num. total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de doctos.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto del financiamiento","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto intereses","celda01",ComunesPDF.CENTER);
		
			String totalDoctosMN3 = (request.getParameter("totalDoctosMN3") != null) ? request.getParameter("totalDoctosMN3") : "0";
			String totalMontoMN3 = (request.getParameter("totalMontoMN3") != null) ? request.getParameter("totalMontoMN3") : "0";
			String totalMontoDescMN3 = (request.getParameter("totalMontoDescMN3") != null) ? request.getParameter("totalMontoDescMN3") : "0";
			String totalInteresMN3 = (request.getParameter("totalInteresMN3") != null) ? request.getParameter("totalInteresMN3") : "0";
			
			String totalDoctosUSD3 = (request.getParameter("totalDoctosUSD3") != null) ? request.getParameter("totalDoctosUSD3") : "0";
			String totalMontoUSD3= (request.getParameter("totalMontoUSD3") != null) ? request.getParameter("totalMontoUSD3") : "0";
			String totalMontoDescUSD3 = (request.getParameter("totalMontoDescUSD3") != null) ? request.getParameter("totalMontoDescUSD3") : "0";
			String totalInteresUSD3 = (request.getParameter("totalInteresUSD3") != null) ? request.getParameter("totalInteresUSD3") : "0";
			
			String totalDoctosConv3 = (request.getParameter("totalDoctosConv3") != null) ? request.getParameter("totalDoctosConv3") : "0";
			String totalMontosConv3 = (request.getParameter("totalMontosConv3") != null) ? request.getParameter("totalMontosConv3") : "0";
			String totalMontoDescConv3 = (request.getParameter("totalMontoDescConv3") != null) ? request.getParameter("totalMontoDescConv3") : "0";
			String totalInteresConv3 = (request.getParameter("totalInteresConv3") != null) ? request.getParameter("totalInteresConv3") : "0";
				 
			pdfDoc.setCell(totalDoctosMN3,"formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(Comunes.formatoDecimal(totalMontoMN3,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(totalMontoDescMN3,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(totalInteresMN3,2),"formas",ComunesPDF.CENTER);
			
			pdfDoc.setCell(totalDoctosUSD3,"formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(Comunes.formatoDecimal(totalMontoUSD3,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(totalMontoDescUSD3,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(totalInteresUSD3,2),"formas",ComunesPDF.CENTER);

		
			pdfDoc.setCell(totalDoctosConv3,"formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(Comunes.formatoDecimal(totalMontosConv3,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(totalMontoDescConv3,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(totalInteresConv3,2),"formas",ComunesPDF.CENTER);
			
			pdfDoc.setCell(mensaje,"formas",ComunesPDF.JUSTIFIED,12);
			
			pdfDoc.addTable();
		
			
			
			if(vecFilas.size()>0){
				pdfDoc.setTable(15, 100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Num. docto. inicial","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Num. acuse","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de emisión","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de publicación","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo docto.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("% Descuento Aforo","celda01",ComunesPDF.CENTER);	//F05-2014
			   pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);	//F05-2014				
				pdfDoc.setCell("Tipo conv.","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Tipo cambio","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto valuado en pesos","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Núm. de documento final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de operación","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia tasa de interés","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor tasa de interés","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto de intereses","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Total de Capital menos Interés","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 3);
			}
		}
		
		if( tipoArchivo.equals("CSV")){		
		
			contenidoArchivo.append(" PYME, Num. docto. inicial,Num. acuse,Fecha de emisión,Fecha vencimiento,Fecha de publicación,Plazo docto.,"+
			 "Moneda,Monto,% Descuento Aforo,  Monto a Descontar, Tipo conv.,Tipo cambio,Monto valuado en pesos,Estatus,Núm. de documento final,Moneda,Monto,Plazo,Fecha de operación,"+
			 "IF, Referencia tasa de interés,Valor tasa de interés,Monto de intereses,Monto Total de Capital menos Interés \n");
			 
		}
		
			for(int i=0;i<vecFilas.size();i++){
				vecColumnas         = (Vector)vecFilas.get(i);
				String nombreEPO           = (String)vecColumnas.get(0);
				String	igNumeroDocto		    =	(String)vecColumnas.get(1);
				String ccAcuse 			      =	(String)vecColumnas.get(2);
				String dfFechaEmision		  =	(String)vecColumnas.get(3);
				String dfFechaVencimiento	=	(String)vecColumnas.get(4);
				String dfFechaPublicacion 	=	(String)vecColumnas.get(5);
				String igPlazoDocto		    =	(String)vecColumnas.get(6);
				String moneda 				      =	(String)vecColumnas.get(7);
				double fnMonto 			      =	Double.parseDouble(vecColumnas.get(8).toString());
				String cgTipoConv			    =	(String)vecColumnas.get(9);
				String tipoCambio			    = (String)vecColumnas.get(10);
				double montoValuado 		    =	"".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
				String igPlazoDescuento	  =	(String)vecColumnas.get(12);
				String fnPorcDescuento		  =	(String)vecColumnas.get(13);
				double montoDescuento 		  =	Double.parseDouble(vecColumnas.get(28).toString());
				String modoPlazo			      =	(String)vecColumnas.get(14);
				String estatus 			      =	(String)vecColumnas.get(15);
				String cambios				      =	(String)vecColumnas.get(16);
				//creditos por concepto de intereses
				String numeroCredito		    =	(String)vecColumnas.get(17);
				String nombreIf			      =	(String)vecColumnas.get(18);
				String tipoLinea			      =	(String)vecColumnas.get(19);
				String fechaOperacion		  =	(String)vecColumnas.get(20);
				String plazoCredito		    =	(String)vecColumnas.get(22);
				String fechaVencCredito	  =	(String)vecColumnas.get(23);
				double valorTasaInt		    =	Double.parseDouble(vecColumnas.get(24).toString());
				String relMat				      =	(String)vecColumnas.get(25);
				double fnPuntos			      =	Double.parseDouble(vecColumnas.get(26).toString());
				String tipoCobroInt		    =	(String)vecColumnas.get(27);
				String referencia			    =	(String)vecColumnas.get(29);
				String tipoPiso			      = (String)vecColumnas.get(30);
				String icTipoCobroInt		  = (String)vecColumnas.get(31);
				String icMoneda			      = (String)vecColumnas.get(32);
				String icTasa				      = (String)vecColumnas.get(33);
				String monedaLinea			    = (String)vecColumnas.get(35);
				String icLineaCredito		  = (String)vecColumnas.get(38);
				double montoCredito		    = Double.parseDouble(vecColumnas.get(21).toString());
				double montoTasaInt		    =	Double.parseDouble(vecColumnas.get(39).toString());
				String acuseFormateado 	  = (String)vecColumnas.get(40);
				fechaCarga			    = (String)vecColumnas.get(41);
				horaCarga			      = (String)vecColumnas.get(42);
				String usuario				      = (String)vecColumnas.get(43);
				String nombreMLinea		    =	(String)vecColumnas.get(44);
				String nombrePyme			    =	(String)vecColumnas.get(45);
				String   descuentoAforo1 = 	(String)vecColumnas.get(46)==null?"0":(String)vecColumnas.get(46); //F05-2014
				if (descuentoAforo1.equals("0") || descuentoAforo1.equals("") )  { descuentoAforo1 = "100";}  //F05-2014
				double  descuentoAforo= Double.parseDouble(descuentoAforo1);  //F05-2014				
				double fnMontoDescuento 			= (double)Math.round( fnMonto*(descuentoAforo/100));   //F05-2014
				
				/*
				if("+".equals(relMat))
					valorTasaInt += fnPuntos;
				else if("-".equals(relMat))
					valorTasaInt -= fnPuntos;
				else if("*".equals(relMat))
					valorTasaInt *= fnPuntos;
				else if("/".equals(relMat))
					valorTasaInt /= fnPuntos;*/
					
				plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
				double montoCapitalInt =  montoCredito-montoTasaInt;

				Calendar gcDiaFecha = new GregorianCalendar();
				java.util.Date fechaVencNvo = Comunes.parseDate(fechaVencCredito);
				gcDiaFecha.setTime(fechaVencNvo);
				int no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);      
				String  diahabil ="";
     
				int menosdias =0;                     
				if(no_dia_semana==Calendar.SATURDAY ) { // 7 Sabado
					menosdias = -1;
				}
				if(no_dia_semana==Calendar.SUNDAY) { //domingo
					menosdias = -2;
				}     
		    if(no_dia_semana==Calendar.SATURDAY  ||  no_dia_semana==Calendar.SUNDAY ){ 
					diahabil  =  Fecha.sumaFechaDias(fechaVencCredito, menosdias);  
				} else {
					diahabil = fechaVencCredito;
				}
				
				elementos++;
				if("1".equals(icMoneda)){
					totalDoctosMN++;
					totalMontoMN += fnMonto;
					totalMontoDescMN += montoDescuento;
					totalInteresMN += valorTasaInt;
					totalMontoMNDesA +=fnMontoDescuento; //F05-2014
					
				}else{
					totalDoctosUSD++;
					totalMontoUSD += fnMonto;
					totalMontoDescUSD += montoDescuento;
					totalInteresUSD += valorTasaInt;
					totalMontoUSDDesA +=fnMontoDescuento;  //F05-2014
					if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
						totalMontoValuadoUSD += montoValuado;
					}
				}
				if("1".equals(monedaLinea)){
					totalCreditosMN++;
					totalMontoCreditoMN += montoCredito;
				}else if("54".equals(monedaLinea)&&"54".equals(icMoneda)){
					totalCreditosUSD++;
					totalMontoCreditoUSD += montoCredito;
				}
				
			
			if(tipoArchivo.equals("PDF")){	
				pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ccAcuse,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(dfFechaEmision,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(dfFechaVencimiento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(dfFechaPublicacion,"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell(igPlazoDocto,"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMonto,2,false),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(descuentoAforo+"%","formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(fnMontoDescuento,2,false),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(cgTipoConv,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipoCambio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoValuado,2,false),"formas",ComunesPDF.RIGHT);		
				pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numeroCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombreMLinea,"formas",ComunesPDF.CENTER);			
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoCredito,2,false),"formas",ComunesPDF.RIGHT);				
				pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);			
				pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.CENTER);			
				pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(referencia.replace(',',' ')+" "+relMat.replace(',',' ')+" "+fnPuntos,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(String.valueOf(valorTasaInt),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTasaInt,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoCapitalInt,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 3);
			}
				
				if(tipoArchivo.equals("CSV")){					
					contenidoArchivo.append(nombrePyme.replace(',',' ')+",");
					contenidoArchivo.append(igNumeroDocto.replace(',',' ')+",");
					contenidoArchivo.append(ccAcuse.replace(',',' ')+",");
					contenidoArchivo.append(dfFechaEmision+",");
					contenidoArchivo.append(dfFechaVencimiento+",");
					contenidoArchivo.append(dfFechaPublicacion+",");
					contenidoArchivo.append(igPlazoDocto.replace(',',' ')+",");
					contenidoArchivo.append(moneda.replace(',',' ')+",");
					contenidoArchivo.append("$"+Comunes.formatoDecimal(fnMonto,2,false)+",");
					contenidoArchivo.append(descuentoAforo+","); //F05-2014
					contenidoArchivo.append("$"+Comunes.formatoDecimal(fnMontoDescuento,2,false)+",");//F05-2014 
					contenidoArchivo.append(cgTipoConv.replace(',',' ')+",");
					contenidoArchivo.append(tipoCambio.replace(',',' ')+",");
					contenidoArchivo.append("$"+Comunes.formatoDecimal(montoValuado,2,false)+",");	
					contenidoArchivo.append(estatus.replace(',',' ')+",");
					contenidoArchivo.append(numeroCredito.replace(',',' ')+",");
					contenidoArchivo.append(nombreMLinea.replace(',',' ')+",");
					contenidoArchivo.append("$"+Comunes.formatoDecimal(montoCredito,2,false)+",");			
					contenidoArchivo.append(plazoCredito.replace(',',' ')+",");		
					contenidoArchivo.append(fechaHoy+",");	
					contenidoArchivo.append(nombreIf.replace(',',' ')+",");	
					contenidoArchivo.append(referencia.replace(',',' ')+" "+relMat.replace(',',' ')+" "+fnPuntos+",");
					contenidoArchivo.append(valorTasaInt+",");
					contenidoArchivo.append("$"+Comunes.formatoDecimal(montoTasaInt,2,false)+",");
					contenidoArchivo.append("$"+Comunes.formatoDecimal(montoCapitalInt,2,false)+"\n");
				}
					
			}//for
			
						
			if(tipoArchivo.equals("PDF")){		
			if(totalDoctosMN>0){ 	
			pdfDoc.setCell("Total M.N.","celda01",ComunesPDF.CENTER );
			pdfDoc.setCell(String.valueOf(totalDoctosMN)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,7);			
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoMN),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoMNDesA),2)  ,"formas",ComunesPDF.RIGHT);	
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);	
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);				
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoDescMN),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("Total Financiamientos M.N.","formas",ComunesPDF.CENTER,2);				
			pdfDoc.setCell(String.valueOf(totalCreditosMN) ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);	
			pdfDoc.setCell("$ 0.0","formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$ 0.0","formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 3);	
		}
		if(totalDoctosUSD>0){ 		
			pdfDoc.setCell("Total USD.","celda01",ComunesPDF.CENTER );
			pdfDoc.setCell(String.valueOf(totalDoctosUSD)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,7);		
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoUSD),2)  ,"formas",ComunesPDF.RIGHT);	
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoUSDDesA),2)  ,"formas",ComunesPDF.RIGHT);				
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(totalMontoDescUSD),2)  ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("Total Financiamientos M.N.","formas",ComunesPDF.CENTER,2);	
			pdfDoc.setCell( String.valueOf(totalCreditosUSD) ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);	
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);	
			pdfDoc.setCell("$ 0.0","formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$ 0.0","formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 3);	
		}					
		pdfDoc.addTable();
				
				
				
			pdfDoc.setTable(2, 40);
			pdfDoc.setCell("Datos de cifras de control", "celda02", ComunesPDF.CENTER, 2);
			pdfDoc.setCell("Num. acuse", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(acuse, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(fechaCarga, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("Hora", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(horaCarga, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre y número de usuario", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(iNoUsuario+" "+strNombreUsuario, "formas", ComunesPDF.CENTER);
			pdfDoc.addTable();
			
				pdfDoc.endDocument();		
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
			}
			
			if(tipoArchivo.equals("CSV")){	
					
				contenidoArchivo.append(" "+"\n");
				contenidoArchivo.append("Datos de cifras de control"+"\n");
				contenidoArchivo.append("Num. acuse"+",");
				contenidoArchivo.append(acuse+"\n");
				contenidoArchivo.append("Fecha"+",");
				contenidoArchivo.append(fechaCarga+"\n");
				contenidoArchivo.append("Hora"+",");
				contenidoArchivo.append(horaCarga+"\n");
				contenidoArchivo.append("Nombre y número de usuario"+",");
				contenidoArchivo.append(iNoUsuario+" "+strNombreUsuario.replace(',',' ')+",");
			
			
				if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
					jsonObj.put("success", new Boolean(false));
					jsonObj.put("msg", "Error al generar el archivo CSV ");
				} else {
					nombreArchivo = archivo.nombre;		
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				}
			}
		
		}

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}

	
%>
<%=jsonObj%>