<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.math.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	java.text.SimpleDateFormat,
	java.util.Date,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String ic_pyme 	= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String ic_moneda 	= (request.getParameter("ic_moneda")==null)?"0":request.getParameter("ic_moneda");
String ig_numero_docto 	= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
String fn_monto_de 	= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
String fn_monto_a 	= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
String cc_acuse 	= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
String mto_descuento 	= (request.getParameter("mto_descuento")==null)?"":request.getParameter("mto_descuento");
String df_fecha_emision_de 	= (request.getParameter("df_fecha_emision_de")==null)?"":request.getParameter("df_fecha_emision_de");
String df_fecha_emision_a 	= (request.getParameter("df_fecha_emision_a")==null)?"":request.getParameter("df_fecha_emision_a");
String doctos_cambio 	= (request.getParameter("doctos_cambio")==null)?"":request.getParameter("doctos_cambio");
String df_fecha_venc_a 	= (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
String df_fecha_venc_de 	= (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
String fecha_publicacion_de 	= (request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
String fecha_publicacion_a 	= (request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
String infoRegresar	=	"";
if(ic_moneda.equals("") ) ic_moneda = "0";

if(doctos_cambio.equals("on") ) { 
	doctos_cambio = "S";
}else {
	doctos_cambio = "N"; 
}

String	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String horaCarga = new java.text.SimpleDateFormat("HH:mm").format(new java.util.Date());

	Vector	vecFilas		= null;
	Vector	vecFilas1		= null;
	Vector	vecFilas2		= null;
	Vector	vecColumnas	= null;
	String ic_if ="", hidNumTasas ="";
	int numTasas=0, i=0;
	String auxIcIf ="", auxReferenciaTasa = "", auxIcTasa ="", auxCgRelMat ="", auxFnPuntos ="", 	auxValorTasa = "",
	auxIcMoneda = "", auxPlazoDias ="";
	JSONObject 	resultado	= new JSONObject();
	HashMap	datosAuxiliar = new HashMap();
	HashMap	monitor = new HashMap();

	CapturaTasas BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);
	
	AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);

	ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
if (informacion.equals("CatalogoPYME") ) {	
	
	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setCampoClave("cp.ic_pyme");
	cat.setCampoDescripcion("cp.cg_razon_social"); 
	cat.setClaveEpo(iNoCliente);	
	cat.setOrden("cp.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
} else  if (informacion.equals("CatalogoMoneda") ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	

} else  if (informacion.equals("SeleccionPYME") ) {

	//Fodea 020-2014
	String	ventaCartera =  BeanParametro.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); 

	//OBTENEMOS LAS TASAS POR EPO E IF Y LAS METEMOS EN HIDDENS AUXILIARES

	vecFilas2 = BeanTasas.esquemaParticular(iNoCliente,4,"",ic_pyme,"D"); // obtiene las EPO e IF que tienen una LC vigente	
	int conteo =0;
	
	for (int j=0; j<vecFilas2.size(); j++) {
		Vector lovDatosTas = (Vector)vecFilas2.get(j);
		boolean esTasaGeneral = false;
		ic_if = lovDatosTas.get(0).toString();
		int totalTasasMN = 0;
		int totalTasasUSD = 0;
		String in = "";
		vecFilas1 = BeanTasas.ovgetTasasxEpo(4,iNoCliente,ic_if, "1, 54");
					
		for( i=0;i<vecFilas1.size();i++){
			vecColumnas = (Vector)vecFilas1.get(i);
			auxIcIf = ic_if;
			auxReferenciaTasa = (String)vecColumnas.get(1)+" "+(String)vecColumnas.get(5)+" "+(String)vecColumnas.get(6);
			auxIcTasa =(String)vecColumnas.get(2);
			auxCgRelMat =(String)vecColumnas.get(5);
			auxFnPuntos =(String)vecColumnas.get(6);
			auxValorTasa = (String)vecColumnas.get(4);
			auxIcMoneda = (String)vecColumnas.get(9);
			auxPlazoDias = (String)vecColumnas.get(10);
			if("1".equals(vecColumnas.get(9).toString()))
				totalTasasMN ++;
			else if("54".equals(vecColumnas.get(9).toString()))
				totalTasasUSD ++;
				numTasas++;							
			datosAuxiliar.put("auxIcIf"+conteo,auxIcIf);
			datosAuxiliar.put("auxReferenciaTasa"+conteo,auxReferenciaTasa);
			datosAuxiliar.put("auxIcTasa"+conteo,auxIcTasa);
			datosAuxiliar.put("auxCgRelMat"+conteo,auxCgRelMat);
			datosAuxiliar.put("auxFnPuntos"+conteo,auxFnPuntos);
			datosAuxiliar.put("auxValorTasa"+conteo,auxValorTasa);
			datosAuxiliar.put("auxIcMoneda"+conteo,auxIcMoneda);
			datosAuxiliar.put("auxPlazoDias"+conteo,auxPlazoDias);	
			conteo++;
		}	
						
		if(totalTasasMN==0||totalTasasUSD==0){
			if(totalTasasMN==0)
				in = "1";
			if(totalTasasUSD==0){
				if(!"".equals(in))
					in += ",";
				in += "54";
			}
				
			vecFilas1  = BeanTasas.ovgetTasas(4,in);			
			esTasaGeneral = true;
		}else{
			vecFilas1 = new Vector();
		}
		
		
		for( i=0;i<vecFilas1.size();i++){
			vecColumnas = (Vector)vecFilas1.get(i);	
			auxIcIf =ic_if;
			auxReferenciaTasa = (String)vecColumnas.get(1)+" "+(String)vecColumnas.get(5)+" "+(String)vecColumnas.get(6);
			auxIcTasa = (String)vecColumnas.get(2);
			auxCgRelMat =(String)vecColumnas.get(5);
			auxFnPuntos =(String)vecColumnas.get(6);
			auxValorTasa  =(String)vecColumnas.get(4);
			auxIcMoneda =(String)vecColumnas.get(10);
			auxPlazoDias = (String)vecColumnas.get(11);
			numTasas++;
		
			datosAuxiliar.put("auxIcIf"+conteo,auxIcIf);
			datosAuxiliar.put("auxReferenciaTasa"+conteo,auxReferenciaTasa);
			datosAuxiliar.put("auxIcTasa"+conteo,auxIcTasa);
			datosAuxiliar.put("auxCgRelMat"+conteo,auxCgRelMat);
			datosAuxiliar.put("auxFnPuntos"+conteo,auxFnPuntos);
			datosAuxiliar.put("auxValorTasa"+conteo,auxValorTasa);
			datosAuxiliar.put("auxIcMoneda"+conteo,auxIcMoneda);
			datosAuxiliar.put("auxPlazoDias"+conteo,auxPlazoDias);	
			conteo++;			
		}			
		
	}//for (int j=0; j<vecFilas2.size(); j++)	

	

	hidNumTasas = String.valueOf(numTasas);  //numero de tasas
	
	//*********************Monitor de Lineas de Credito*******************************************
	String ic_linea  ="", 	moneda_linea ="", saldo_inicial  ="",  montoSeleccionado = "",  numDocto ="",
	saldoDisponible ="",  hidSaldoDisponible = "";
	int hidNumLineas =0;
 
	vecFilas = aceptPyme.monitorLineasdm(iNoCliente,"","");

	for(i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector)vecFilas.get(i);
		ic_linea  = (String)vecColumnas.get(0);
		moneda_linea = (String)vecColumnas.get(4);
		saldo_inicial  = Comunes.formatoDecimal(vecColumnas.get(2).toString(),2,false);
		montoSeleccionado = "0.00";
		numDocto ="0";
		saldoDisponible =Comunes.formatoDecimal(vecColumnas.get(3).toString(),2,false);
		hidSaldoDisponible = Comunes.formatoDecimal(vecColumnas.get(5).toString(),2,false);
		hidNumLineas = vecFilas.size();	
		
		monitor.put("ic_linea"+i,ic_linea);
		monitor.put("moneda_linea"+i,moneda_linea);
		monitor.put("saldo_inicial"+i,saldo_inicial);
		monitor.put("montoSeleccionado"+i,montoSeleccionado);
		monitor.put("numDocto"+i,numDocto);
		monitor.put("saldoDisponible"+i,saldoDisponible);
		monitor.put("hidSaldoDisponible"+i,hidSaldoDisponible);		
	}

		
		resultado.put("success", new Boolean(true));
		resultado.put("Auxiliar", datosAuxiliar);
		resultado.put("monitorLinea", monitor);
		resultado.put("hidNumTasas", hidNumTasas);
		resultado.put("hidNumLineas",  String.valueOf(hidNumLineas));
		resultado.put("ventaCartera",  ventaCartera);
		
		infoRegresar = resultado.toString();

	} else  if (informacion.equals("Consulta") ||  informacion.equals("ResumenTotales")  ) {

	//*********************Consulta de Registros *******************************************
	
	String	descuentoAutomatico = BeanParametro.DesAutomaticoEpo(iNoEPO,"PUB_EPO_DESC_AUTOMATICO"); 
	String	ventaCartera =  BeanParametro.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); 
	String	tipoCredito= BeanParametro.obtieneTipoCredito (iNoEPO);

	
	vecFilas = aceptPyme.consultaDoctosVentaCartera(iNoCliente,ic_pyme,ic_moneda,ig_numero_docto,fn_monto_de,fn_monto_a, cc_acuse,mto_descuento,df_fecha_emision_de,df_fecha_emision_a,doctos_cambio,df_fecha_venc_de,df_fecha_venc_a,fecha_publicacion_de,fecha_publicacion_a,null);
	
	int totalDoctosMN	=	0, 	totalDoctosUSD = 0, menosdias =0,  no_dia_semana = 0, menosdias2 =0, 
	menosdias3 =0,  totaldPlazo =0,	elementos  = 0,	totalCreditosMN		=0, totalCreditosUSD	=	0;
	
	double	totalMontoMN	=	0, totalMontoUSD =0,	totalMontstrFacultad =0,	totalMontoDescMN	=	0,totalMontoDescUSD	=	0,
	totalInteresMN =	0,	totalInteresUSD	=	0,	totalMontoValuadoMN	=	0,	totalMontoValuadoUSD=	0,totalMontoCreditoMN	=	0,
	totalMontoCreditoUSD=	0, montoCredito	=	0, tasaInteres = 0, montoTasaInt	=	0, valorTasaInt	=	0,  fnPuntos  = 0,
	montoValuadoLinea  = 0,  totalMontoMNDesA =0,  totalMontoUSDDesA =0;	
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	HashMap registrosTot = new HashMap();	
	JSONArray registrosTotales = new JSONArray();
	String operalineaCreditoIF = "", fechaVencimientoLinea = "";
	
	for(i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);
			
			String numeroCredito		= 	(String)vecColumnas.get(17);
			String nombrePyme			=	(String)vecColumnas.get(0);
			String igNumeroDocto		=	(String)vecColumnas.get(1);
			String ccAcuse 			=	(String)vecColumnas.get(2);
			String dfFechaEmision		=	(String)vecColumnas.get(3);
			String dfFechaVencimiento	=	(String)vecColumnas.get(4);
			String dfFechaPublicacion 	= 	(String)vecColumnas.get(5);
			String igPlazoDocto		=	(String)vecColumnas.get(6);
			String moneda 				=	(String)vecColumnas.get(7);
			double fnMonto 			= 	Double.parseDouble(vecColumnas.get(8).toString());
			String cgTipoConv			=	(String)vecColumnas.get(9);
			String tipoCambio			=   (String)vecColumnas.get(10);
			double montoDescuento 		=	Double.parseDouble(vecColumnas.get(28).toString());
			double montoValuado 		=	"".equals(tipoCambio)?0:(Double.parseDouble(tipoCambio)*(fnMonto-montoDescuento));
			String igPlazoDescuento	= 	(String)vecColumnas.get(12);
			String fnPorcDescuento		= 	(String)vecColumnas.get(13);
			String modoPlazo			= 	(String)vecColumnas.get(14);
			String estatus 			=	(String)vecColumnas.get(15);
			String cambios				= 	(String)vecColumnas.get(16);
			//creditos por concepto de intereses
			
			String nombreIf			= 	(String)vecColumnas.get(18);
			String tipoLinea			=	(String)vecColumnas.get(19);
			String fechaOperacion		=	(String)vecColumnas.get(20);
			String plazoCredito		=	(String)vecColumnas.get(22);
			
			String fechaVencCredito	=	(String)vecColumnas.get(23);
			String relMat				= 	(String)vecColumnas.get(25);
			String tipoCobroInt		=	(String)vecColumnas.get(27);
			String referencia			=	(String)vecColumnas.get(29);
			String tipoPiso			= 	(String)vecColumnas.get(30);
			String icTipoCobroInt		= 	(String)vecColumnas.get(31);
			String icMoneda			= 	(String)vecColumnas.get(32);
			String icTipoFinanciamiento= 	(String)vecColumnas.get(34);
			String monedaLinea			= 	(String)vecColumnas.get(35);
			String dias_minimo			= 	(String)vecColumnas.get(36);
			String dias_maximo			= 	(String)vecColumnas.get(37);
			String icLineaCredito		= 	(String)vecColumnas.get(38);
			String noIc_if				= 	(String)vecColumnas.get(39);
			String nombreMLinea		=  	(String)vecColumnas.get(40);		
 			String plazo_valido =	(String)vecColumnas.get(42)==null?"0":(String)vecColumnas.get(42); 
      String fechaVencimiento =	(String)vecColumnas.get(47)==null?"":(String)vecColumnas.get(47); 
      String epo = (String)vecColumnas.get(48)==null?"":(String)vecColumnas.get(48); 
      String plazopyme = (String)vecColumnas.get(49)==null?"0":(String)vecColumnas.get(49); 
      String plazoepo = (String)vecColumnas.get(50)==null?"0":(String)vecColumnas.get(50);
      String plazonafin = (String)vecColumnas.get(51)==null?"0":(String)vecColumnas.get(51); 
		String  pymeBloqueadaDisxIF = (String)vecColumnas.get(52)==null?"0":(String)vecColumnas.get(52);//F05-2014
		String   descuentoAforo1 = 	(String)vecColumnas.get(53)==null?"0":(String)vecColumnas.get(53);//F05-2014
		double  descuentoAforo= Double.parseDouble(vecColumnas.get(53).toString()); 
		
		operalineaCreditoIF = (String)vecColumnas.get(56); //F20-2015
		fechaVencimientoLinea =  (String)vecColumnas.get(57); //F20-2015		
			
		BigDecimal  porSubLimite= new BigDecimal(vecColumnas.get(54).toString());  //F05-2014 // % LineaSublimite	
		int valor = porSubLimite.compareTo(new BigDecimal("0"));		
		if(valor==0)  {
			porSubLimite=new BigDecimal("100");
			System.out.println(" La Pyme no tiene parametrizado el  Sub-límites Distribuidor y se le asignara 100% ");
		}
		
		BigDecimal  montoLinea= new BigDecimal(vecColumnas.get(55).toString());  //F05-2014 // Monto Linea de Credito		
	
		BigDecimal   MontoSubLimiteDistribuidor  = montoLinea.multiply(porSubLimite).divide(new BigDecimal("100"),2);//F05-2014
		
		System.out.println(" MontoSubLimiteDistribuidor   "+MontoSubLimiteDistribuidor);		
		
		 
		String  montoDocutosT =  aceptPyme.getMontoTotalMocumentos(iNoCliente, icMoneda,  noIc_if  , "1", "" , icLineaCredito) ; //F05-2014  // Monto Total de doctos en estatus  3, 4, 24 		
		
		BigDecimal  MontoTotalDocumentos= new BigDecimal(montoDocutosT);  //F05-2014 // Monto Linea de Credito
		BigDecimal  MontoDispoSubLimiteDistribuidor  = MontoSubLimiteDistribuidor.subtract(MontoTotalDocumentos);//F05-2014
		if (descuentoAforo1.equals("0"))  {		descuentoAforo = 100; }//F05-2014
		double fnMontoDescuento 			= (double)Math.round( fnMonto*(descuentoAforo/100));   //F05-2014
		
	/*	System.out.println("montoLinea  "+montoLinea);
		System.out.println("porSubLimite  "+porSubLimite);
		System.out.println("MontoTotalDocumentos  "+MontoTotalDocumentos);		
		System.out.println("MontoSubLimiteDistribuidor  "+MontoSubLimiteDistribuidor);		
		System.out.println("MontoDispoSubLimiteDistribuidor  "+MontoDispoSubLimiteDistribuidor);    
		*/
		
      plazo_valido = plazopyme;
			if (plazopyme.equals("0") && ( plazo_valido.equals("0") || plazo_valido.equals("")) ){
				plazo_valido = plazoepo;        
			}
			if (plazoepo.equals("0") && ( plazo_valido.equals("0") || plazo_valido.equals("") )){
				plazo_valido = plazonafin;
			}
			if(icMoneda.equals(monedaLinea)){
				cgTipoConv		= "";
				tipoCambio		= "";
				montoValuado	= fnMonto;
			}
			if("1".equals(icTipoFinanciamiento)){			
				fechaVencCredito = dfFechaVencimiento;
			}
			if("+".equals(relMat)) {
				valorTasaInt += fnPuntos;
			}else if("-".equals(relMat)){
				valorTasaInt -= fnPuntos;
			}else if("*".equals(relMat)){
				valorTasaInt *= fnPuntos;
			}else if("/".equals(relMat)){
				valorTasaInt /= fnPuntos;
			}
			montoValuadoLinea = fnMonto;
			plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;
			if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento)){
			 montoCredito = fnMonto-montoDescuento;
			}else if("2".equals(icTipoFinanciamiento)){
			 	montoCredito = fnMonto;
			}
			//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
				if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento)){
					montoCredito = montoValuado;
				}else if("2".equals(icTipoFinanciamiento)){
					montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
				}
			}	
			montoCredito = ((double)Math.round(montoCredito*100)/100);  //DUDA CON ESTO
			if (!descuentoAforo1.equals("0"))  {    		montoCredito=  fnMontoDescuento; 			}  //F05-2014
			
			if("1".equals(tipoPiso)){
				montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
			}else{
				montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
			}
			String  plazofinanciamiento =(String)vecColumnas.get(44);
			String  interes =(String)vecColumnas.get(45);
			String credito = (String)vecColumnas.get(46);
			
			menosdias= Fecha.restaFechas(dfFechaEmision,dfFechaVencimiento); 
			
			SimpleDateFormat formatEntrada = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaVenci = formatEntrada.parse(dfFechaVencimiento);
			Date fechaSistema = new Date();
								
			menosdias2= Fecha.restaFechas(fechaHoy, dfFechaVencimiento); 			
			totaldPlazo = menosdias - menosdias2;
			plazo_valido=String.valueOf(menosdias2);
	
			//esta parte es para obtener la linea de credito de la tabla 
			/*
			Vector vecFilasL = aceptPyme.consDoctosVentaCartera(iNoCliente,"",numeroCredito);
			Vector	vecColumnasL = (Vector)vecFilasL.get(0);
			String icLineaCreditod		= 	(String)vecColumnasL.get(38);	
			*/
			String icLineaCreditod	=  icLineaCredito;
			
				
			datos = new HashMap();
			datos.put("SELECCION", "N");
			datos.put("NOMBRE_PYME",nombrePyme);
			datos.put("NO_DOCTO_INICAL",igNumeroDocto);
			datos.put("IC_DOCUMENTO",numeroCredito);			
			datos.put("ACUSE",ccAcuse);
			datos.put("FECHA_EMISION",dfFechaEmision);
			datos.put("FECHA_VENCIMIENTO",dfFechaVencimiento);
			datos.put("FECHA_PUBLICACION",dfFechaPublicacion);
			datos.put("PLAZO_DOCTO",igPlazoDocto);
			datos.put("MONEDA",moneda);			
			datos.put("IC_MONEDA_DOCTO",icMoneda);
			datos.put("IC_MONEDA_LINEA",monedaLinea);
			datos.put("IC_IF",noIc_if);
			datos.put("TIPO_PISO",tipoPiso);			
			datos.put("MONTO",Double.toString (fnMonto));			
			if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){	
				datos.put("TIPO_CONV",cgTipoConv);
				datos.put("TIPO_CAMBIO",tipoCambio);
				datos.put("MONTO_VALUADO",Double.toString (montoValuado) );				
			}else{	
				datos.put("TIPO_CONV","");
				datos.put("TIPO_CAMBIO","");
				datos.put("MONTO_VALUADO","0");				
			}		
			datos.put("ESTATUS",estatus);
			datos.put("NO_DOCTO_FINAL",numeroCredito);
			datos.put("MONEDA_FINAL",nombreMLinea);
			datos.put("MONTO_DESCUENTO",Double.toString (montoDescuento));
			datos.put("MONTO_CREDITO",Double.toString (montoCredito));			
			if("1".equals(icTipoFinanciamiento)){
				datos.put("PLAZO_FINAL","0".equals(plazoCredito)?"0":plazoCredito);
			}else{
				if ("3".equals(plazofinanciamiento) && (descuentoAutomatico.equals("N")|| descuentoAutomatico.equals("S"))){  
					datos.put("PLAZO_FINAL","0");	 //dua con respecto a este parte
				} else if (descuentoAutomatico.equals("N")|| descuentoAutomatico.equals("")){ 
					datos.put("PLAZO_FINAL","0".equals(plazo_valido)?"0":plazo_valido);	
				}else if (descuentoAutomatico.equals("S") ) {
					datos.put("PLAZO_FINAL","0".equals(plazo_valido)?"0":plazo_valido);	
				}     
			}	
					
			datos.put("PLAZO_VALIDO",plazo_valido);	
			datos.put("FECHA_HOY",fechaHoy);
			datos.put("FECHA_VENCREDITO",fechaVencCredito);
			datos.put("NOMBRE_IF",nombreIf);
			datos.put("IC_LINEA_CREDITO",icLineaCredito);		
			datos.put("IC_LINEA_CREDITO_D",icLineaCreditod);		
			datos.put("REFERENCIA","");
			datos.put("IC_TASA","");
			datos.put("CG_REL_MAT","");
			datos.put("FN_PUNTOS","");
			datos.put("VALOR_TASA","");
			datos.put("VALOR_TASA_PUNTOS",("S".equals(icTipoCobroInt))?"":""+valorTasaInt);
			datos.put("MONTO_TASA_INTERES",("S".equals(icTipoCobroInt))?"":""+((double)Math.round(montoTasaInt*Double.parseDouble(plazoCredito)*100)/100));
			datos.put("MONTO_AUX_INT",Double.toString (montoTasaInt) );
			datos.put("MONTO_CAPITAL_INTERES","0.0");
			datos.put("IC_TIPO_FINANCIAMIENTO",icTipoFinanciamiento); 
			datos.put("PLAZO_FINANCIAMIENTO",plazofinanciamiento);
			datos.put("DESCUENTO_AUTOMATICO",descuentoAutomatico);	
			datos.put("DIAS_MINIMO",dias_minimo); 
			datos.put("DIAS_MAXIMO",dias_maximo); 
			datos.put("DIAS_MAXIMO",dias_maximo); 
			datos.put("PYME_BLOQ_X_IF", pymeBloqueadaDisxIF);
			datos.put("DESCUENTO_AFORO", Double.toString (descuentoAforo)  );
			datos.put("MONTO_DESCONTAR", Double.toString (fnMontoDescuento)  );
			datos.put("MONTO_DIS_SUB_LIMITE", MontoDispoSubLimiteDistribuidor.toString()  );
			datos.put("CG_LINEA_CREDITO", operalineaCreditoIF.trim()  );	 //F20-2015
			datos.put("FECHA_VENC_LINEA", fechaVencimientoLinea  );	//F20-2015
			registros.add(datos);	
				
			elementos++;
			if("1".equals(icMoneda)){
				totalDoctosMN++;
				totalMontoMN += fnMonto;
				totalMontoDescMN += montoDescuento;
				totalInteresMN += valorTasaInt;
				totalMontoMNDesA +=fnMontoDescuento; //F05-2014
			}else{
				totalDoctosUSD++;
				totalMontoUSD += fnMonto;
				totalMontoDescUSD += montoDescuento;
				totalInteresUSD += valorTasaInt;
				if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
					totalMontoValuadoUSD += montoValuado;
				}
				totalMontoUSDDesA +=fnMontoDescuento;  //F05-2014
			}
			if("1".equals(monedaLinea)){
				totalCreditosMN++;
				totalMontoCreditoMN += montoCredito;
			}else if("54".equals(monedaLinea)&&"54".equals(icMoneda)){
				totalCreditosUSD++;
				totalMontoCreditoUSD += montoCredito;  
			}
		}//for
		
			
	
			
		for(int t =0; t<2; t++) {		
		registrosTot = new HashMap();		
			if(t==0){ 				
			registrosTot.put("MONEDA", "MONEDA NACIONAL");
			registrosTot.put("ICMONEDA", "1");
			registrosTot.put("TOTAL", String.valueOf(totalDoctosMN));	
			registrosTot.put("MONTO", Double.toString(totalMontoMN));				
			registrosTot.put("MONTO_DESCONTAR", Double.toString(totalMontoMNDesA)); //F05-2014			
			registrosTot.put("TOTAL_MONTO_VALUADO", Double.toString(totalMontoValuadoMN)  );
			registrosTot.put("TOTAL_CREDITOS",  String.valueOf(totalCreditosMN) );
			registrosTot.put("TOTAL_MONTO_CREDITOS", Double.toString(totalMontoCreditoMN)  );			
			registrosTot.put("MONTOINTERES", "0.0" );	
			registrosTot.put("MONTOCAPITALINTERES", "0.0" );
		}
		if(t==1){ 			
			registrosTot.put("MONEDA", "MONEDA USD");
			registrosTot.put("ICMONEDA", "54");
			registrosTot.put("TOTAL", String.valueOf(totalDoctosUSD) );	
			registrosTot.put("MONTO", Double.toString(totalMontoUSD)  );			
			registrosTot.put("TOTAL_MONTO_VALUADO", Double.toString(totalMontoValuadoUSD)  );	
			registrosTot.put("TOTAL_CREDITOS", String.valueOf(totalCreditosUSD) );
			registrosTot.put("TOTAL_MONTO_CREDITOS", Double.toString(totalMontoCreditoUSD)  );
			registrosTot.put("MONTOINTERES", "0.0" );	
			registrosTot.put("MONTOCAPITALINTERES", "0.0" );	
			registrosTot.put("MONTO_DESCONTAR", Double.toString(totalMontoUSDDesA)); //F05-2014
			
		}
		registrosTotales.add(registrosTot);
	}
	
		
		if (informacion.equals("Consulta") ) {
			String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
			resultado = JSONObject.fromObject(consulta);		
			resultado.put("ElEMENTOS", String.valueOf(elementos));			
			infoRegresar = resultado.toString();
		
		}
		
		if (informacion.equals("ResumenTotales") ) {
				 infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registrosT\": " + registrosTotales.toString()+"}";
		}

} else  if (informacion.equals("ConfirmacionClaves") ) {

	String cesionUser 	= (request.getParameter("cesionUser")==null)?"":request.getParameter("cesionUser");
	String cesionPassword 	= (request.getParameter("cesionPassword")==null)?"":request.getParameter("cesionPassword");
	boolean		bConfirma = false;		
	
	if(!cesionUser.equals("") && !cesionPassword.equals("")) {
	try{
			aceptPyme.validarUsuarioEPO("", cesionUser , cesionPassword, (String) request.getRemoteAddr(), (String) request.getRemoteHost(), "DCAPT" );

			bConfirma = true;
		}catch(Exception e){
			bConfirma = false;
		}		 
	}	
	bConfirma = true;
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("bConfirma", new Boolean(bConfirma));	
	infoRegresar = jsonObj.toString();	
		
}else  if (informacion.equals("GeneraAcuse") ) {
 
	String 		_acuse = "500";

	String	ic_documento[]			=	request.getParameterValues("ic_documento");
	String	ic_tasa[]				=	request.getParameterValues("ic_tasa");
	String	cg_rel_mat[]			=	request.getParameterValues("cg_rel_mat");
	String	fn_puntos[]				=	request.getParameterValues("fn_puntos");
	String	fn_valor_tasa[]			=	request.getParameterValues("fn_valor_tasa");
	String	fn_importe_interes[]	=	request.getParameterValues("fn_importe_interes");
	String	fn_importe_recibir[]	=	request.getParameterValues("fn_importe_recibir");
	String	plazo_credito[]			=	request.getParameterValues("plazo_credito");
	String	fecha_vto[]				=	request.getParameterValues("fecha_vto");
	String	ic_linea_credito[]		=	request.getParameterValues("ic_linea_credito");
	String	monto[]		=	request.getParameterValues("monto");
	String	ic_moneda_docto[]		=	request.getParameterValues("ic_moneda_docto");
	String	ic_moneda_linea[]		=	request.getParameterValues("ic_moneda_linea");
	


	double totalMontoMN =0;
	double totalInteresMN =0;
	double totalMontoImpMN =0;
	double totalMontoMNDesA =0;
	
	
	double totalMontoUSD =0;
	double totalInteresUSD =0;
	double totalMontoImpUSD =0;
	double totalMontoUSDDesA =0;

	
			for(int x=0;x<ic_documento.length;x++){
		
				if("1".equals(ic_moneda_docto[x])){
				System.out.println("monto[x] "+monto[x]);	
					totalMontoMN		+= Double.parseDouble(monto[x]);					
					totalInteresMN		+= Double.parseDouble("".equals(fn_importe_interes[x])?"0":fn_importe_interes[x]);				
					totalMontoImpMN+= Double.parseDouble(fn_importe_recibir[x]);
				}
				if("54".equals(ic_moneda_docto[x])&&"54".equals(ic_moneda_linea[x])){
				
					totalMontoUSD		+= Double.parseDouble(monto[x]);
					totalMontoImpUSD	+= Double.parseDouble(fn_importe_recibir[x]);
					totalInteresUSD		+= Double.parseDouble("".equals(fn_importe_interes[x])?"0":fn_importe_interes[x]);
					
				}		
							
		} //for
	
	
	String 	acuse = aceptPyme.confirmaDoctoCCC(Double.toString (totalMontoMN), 	Double.toString (totalInteresMN),	Double.toString (totalMontoImpMN), 	Double.toString (totalMontoUSD),
																			 Double.toString (totalInteresUSD), 	Double.toString (totalMontoImpUSD),iNoUsuario,_acuse,	ic_documento,ic_tasa,cg_rel_mat,fn_puntos,fn_valor_tasa,fn_importe_interes,fn_importe_recibir,plazo_credito,fecha_vto,ic_linea_credito);

	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("acuse",acuse);	 
	jsonObj.put("fecCarga",fechaHoy);	
	jsonObj.put("horaCarga",horaCarga);	
	jsonObj.put("captUser",iNoUsuario+" "+strNombreUsuario);
	
	infoRegresar = jsonObj.toString();		
	
}else if (informacion.equals("obtenCambioDoctos")) {

	String ic_documento = (request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");

	if(ic_documento != null && !ic_documento.equals("")) {
		DetalleCambiosDoctos dataCambios = new DetalleCambiosDoctos();
		dataCambios.setIcDocumento(ic_documento);
		Registros registros = dataCambios.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}
%>
<%=infoRegresar%>

 