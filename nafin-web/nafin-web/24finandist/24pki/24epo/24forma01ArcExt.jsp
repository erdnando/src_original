<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*, 
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	com.netro.pdf.*,	
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<% 
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
String mod_categoria = (request.getParameter("mod_categoria") != null) ? request.getParameter("mod_categoria") : "";
String ic_tipo_financiamiento	= (request.getParameter("ic_tipo_financiamiento")==null)?"6":request.getParameter("ic_tipo_financiamiento");
String cg_num_distribuidor	= (request.getParameter("cg_num_distribuidor")==null)?"":request.getParameter("cg_num_distribuidor");
String cmb_categoria	= (request.getParameter("cmb_categoria")==null)?"0":request.getParameter("cmb_categoria");
String ig_plazo_descuento	= (request.getParameter("ig_plazo_descuento")==null)?"":request.getParameter("ig_plazo_descuento");
String ig_numero_docto	= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
String fn_porc_descuento	= (request.getParameter("fn_porc_descuento")==null)?"":request.getParameter("fn_porc_descuento");
String df_fecha_emision	= (request.getParameter("df_fecha_emision")==null)?"":request.getParameter("df_fecha_emision");
String ct_referencia	= (request.getParameter("ct_referencia")==null)?"":request.getParameter("ct_referencia");
String df_fecha_venc	= (request.getParameter("df_fecha_venc")==null)?"":request.getParameter("df_fecha_venc");
String ic_moneda	= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
String fn_monto	= (request.getParameter("fn_monto")==null)?"":request.getParameter("fn_monto");
String txt_fecha_limite_dsc	= (request.getParameter("txt_fecha_limite_dsc")==null)?"":request.getParameter("txt_fecha_limite_dsc");
String negociable	= (request.getParameter("negociable")==null)?"":request.getParameter("negociable");
String proceso	= (request.getParameter("proceso")==null)?"":request.getParameter("proceso");
String accionDetalle	= (request.getParameter("accionDetalle")==null)?"N":request.getParameter("accionDetalle");
String hidID	= (request.getParameter("hidID")==null)?"N":request.getParameter("hidID");
String tipoCredito	= (request.getParameter("tipoCredito")==null)?"":request.getParameter("tipoCredito");
String acuse	= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String fechaAcuse	= (request.getParameter("fechaAcuse")==null)?fechaHoy:request.getParameter("fechaAcuse");
String horaAcuse	= (request.getParameter("horaAcuse")==null)?"":request.getParameter("horaAcuse");
String tipoArchivo	= (request.getParameter("tipoArchivo")==null)?"":request.getParameter("tipoArchivo");
String accion = (request.getParameter("accion") != null) ? request.getParameter("accion") : "";


String usuario = 	strLogin+" - "+strNombreUsuario;
JSONObject jsonObj = new JSONObject();
StringBuffer contenidoArchivo = new StringBuffer();

if("".equals(cg_num_distribuidor)) 	cg_num_distribuidor = ic_pyme;
if(negociable.equals("S")) negociable ="2";
if(negociable.equals("N")) negociable ="1";


String ses_ic_epo =  iNoCliente;
Calendar fechaEmisPlazo = new GregorianCalendar();
String mod_ig_plazo_descuento ="", 	mod_fn_porc_descuento= "", hid_plazo_descto ="", 	hid_porc_descto ="", 	hid_tipo_param ="", 
				hid_plazo_descto_max ="", hid_porc_descto_max	= "", hid_tipo_param_max ="", txt_fechaEmisPlazo ="", mod_ic_pyme ="",
				mod_ig_numero_docto	="",  mod_df_fecha_emision 	="", 	mod_df_fecha_venc	="", 	mod_ic_moneda	="", 	mod_fn_monto ="", 
				mod_ic_tipo_financiamiento	="", mod_ct_referencia	="", mod_fecha_limite_dsc ="",	dias_minimo ="", 	dias_maximo	="", 
				desEstatus ="", mensajesIntereses ="", desTipoPago =""; 
								
Vector cg_campo = new Vector (5);
cg_campo.addElement((request.getParameter("cg_campo1")==null)?"":request.getParameter("cg_campo1"));
cg_campo.addElement((request.getParameter("cg_campo2")==null)?"":request.getParameter("cg_campo2"));
cg_campo.addElement((request.getParameter("cg_campo3")==null)?"":request.getParameter("cg_campo3"));
cg_campo.addElement((request.getParameter("cg_campo4")==null)?"":request.getParameter("cg_campo4"));
cg_campo.addElement((request.getParameter("cg_campo5")==null)?"":request.getParameter("cg_campo5"));

String msgError =""; 
String infoRegresar ="";


CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB",CargaDocDist.class);

ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class);


String descuentoAutomatico =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_DESC_AUTOMATICO");
String limiteLineaCredito =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_LIMITE_LINEA_CREDITO");//Fodea 029-2010 Distribuodores Fase III
String obtieneTipoCredito =  BeanParametros.obtieneTipoCredito (iNoCliente); //Fodea 029-2010 Distribuodores Fase III
String NOnegociable =  BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_NO_NEGOCIABLES"); //Fodea 029-2010 Distribuodores Fase III
String ventaCartera =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); //Fodea 029-2010 Distribuodores Fase III
String  hid_fecha_porc_desc = cargaDocto.getParamFechaPorc(iNoCliente);
boolean operaConversion = cargaDocto.operaConversion(iNoCliente);
String operaContrato = BeanParametros.obtieneOperaSinCesion(iNoCliente);  
Vector nombresCampo = new Vector(5);
nombresCampo = cargaDocto.getCamposAdicionales(ses_ic_epo,false);
	
String operaVentaCartera ="";
if(ventaCartera.equals("S")) {
	operaVentaCartera = "S";
}
 if (obtieneTipoCredito.equals("A")){
 obtieneTipoCredito = "C";
 }
 
String  meseSinIntereses = BeanParametros.DesAutomaticoEpo(iNoCliente,"CS_MESES_SIN_INTERESES");
	
	
String nombrePyme ="", clavePyme ="", claveDistribuidor="", numeroDocumento ="", fechaEmision ="",
fechaVencimiento ="", plazoDocto ="", monedaNombre ="", monto ="",  respPagoInteres = "", 
modoPlazo ="", plazoDescuento ="", porcDescuento ="", montoDescuento ="", tipoConversion ="", 
tipoCambio ="", categoria	="", estatus= "", plazo_credito	 ="", fecha_vto_credito	="", responsable_interes	= "",
DescripcionEstatus ="", referencia  ="", consecutivo ="",  icMoneda	="", bandeVentaCartera	 ="",
num_cliente ="", df_fecha_vto= "",  tipo_conversion = "", tipo_cambio  = "", fn_porc_desc , monto_descuento = "", 
tipo_cobranza = "", ic_linea_credito_dm = "", moneda ="" , ig_plazo_docto  ="", modo_plazo ="",  moneda_linea="",
strMontoValuado = "",  cliente = "", monedaLinea ="";

int 	num_doctos_mn = 0, 	num_doctos_usd = 0, numCampos = 0,
totalDocDM =0,  totalDocDMUSD =0,  totalDocConvDM =0, totalDocCC =0, 
totalDocCCUSD =0, totalDocConvCC =0;


double	fn_monto_total_mn	= 0, 	fn_monto_total_usd	= 0, monto_valuado_total_mn 	
= 0, monto_valuado_total_usd =0, ffn_monto =0,  monto_valuado	=0, fn_monto2 =0,  
monto_descuento2 =0, fnPorcAux =0, fn_monto_total =0, monto_valuado_total =0, 
montoTotalDM =0,  montoCDescDM =0,  interesTotalDM =0, montoTotalDMUSD =0,  
montoCDescDMUSD =0, interesTotalDMUSD =0,  montoTotalConvDM =0,  montoCDescConvDM =0,  
montoTotalCC =0,  montoCDescCC =0, interesTotalCC =0,  montoTotalCCUSD =0, 
montoCDescCCUSD =0, montoTotalConvCC =0, 	 montoCDescConvCC =0;
String cadEpo = "";						
String cadEpoCemex =
"Al transmitir este mensaje de datos para todos los efectos legales, acepto bajo mi responsabilidad, que los datos capturados en la orden de compra son correctos y reales.";
		
if(ventaCartera.equals("S")) {
	 cadEpo = 
	" Al trasmitir este mensaje de datos y para todos los efectos legales, Usted bajo su responsabilidad, está aceptando se realice el DESCUENTO y/o FACTORAJE CON RECURSO del (los) "+
    "Documento (s) final (es) que se detallan en está pantalla, otorgando su consentimiento para que en caso de que se den la condiciones señaladas en el Contrato para Realizar "+
    "Operaciones de Descuento y/o Factoraje con Recurso, se realice la cesión de los derechos de cobro a favor del INTERMEDIARIO FINANCIERO que los opere.";

}	else  {
	cadEpo = 
	" Al trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, está solicitando el DESCUENTO y/o FACTORAJE del (los) DOCUMENTOS (S) INICIAL (ES) que se detallan en está pantalla y que ha publicado electrónicamente, "+
	" otorgando su consentimiento para que en caso de que se dén las condiciones señaladas en el Contrato de Financiamiento a Clientes y Distribuidores, el (LOS) DOCUMENTOS (S) INICIAL (ES) serán sustituidos por el (los) DOCUMENTO (S) FINAL (ES), cediendo los "+
	" derechos de cobro a su favor al INTERMEDIARIO FINANCIERO que los opere.";
}

cadEpoCemex = cadEpo = "Al trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, está de acuerdo en que el (los) Documento (s) inicial (es) que se detalla (n) en "+
    "está pantalla puedan ser operados indistintamente a través de  las siguientes modalidades, en términos de los pactado en el “Contrato de Financiamiento a Clientes y Distribuidores” (para conocer las "+
    "modalidades convenidas favor de revisar su contrato): \n\n"+
    "a) DESCUENTO y/o FACTORAJE.\n"+
    "De operarse en esta modalidad y para todos los efectos legales,  bajo su responsabilidad, está solicitando el DESCUENTO y/o FACTORAJE del (los) Documento (s) inicial (es) que se detallan en está "+
    "pantalla y que ha publicado electrónicamente, otorgando su consentimiento para que en caso  de que se den la condiciones señaladas en el “Contrato de Financiamiento a Clientes y Distribuidores”, "+
    "el (los) Documento (s) inicial (es)  será (n) sustituido (s) por el (los) DOCUMENTO (S) FINAL (ES), cediendo los derechos de cobro a su favor al INTERMEDIARIO FINANCIERO que los opere. \n\n"+
    "b) Pago del CLIENTE o DISTRIBUIDOR mediante Tarjeta de Crédito. \n"+
    "De operarse en esta modalidad y para todos los efectos legales, otorga su consentimiento para que sea cubierto el (los) Documento (s) inicial (es) que se detalla (n) en está pantalla y que ha "+
    "publicado electrónicamente, en términos del “Contrato de Financiamiento a Clientes y Distribuidores”, mediante la utilización de una Tarjeta de Crédito asociada al CLIENTE o DISTRIBUIDOR.\n\n "+
    "Asimismo, en este acto manifiesto bajo protesta de decir verdad, que sí he emitido o emitiré a mi CLIENTE o DISTRIBUIDOR el CFDI por la operación comercial que le dio origen a esta transacción, "+
    "según sea el caso y conforme establezcan las disposiciones fiscales vigentes.";

 if(operaContrato.equals("S")){
      cadEpoCemex =  cadEpo = "Al transmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, está publicando el(los) DOCUMENTO(S) INICIAL(ES) que se "+
      "detallan en esta pantalla y que pueden ser objeto de financiamiento, otorgando su consentimiento para que en caso de que se den las condiciones señaladas en el Contrato de Financiamiento "+
      "a Clientes y Distribuidores, el (los) DOCUMENTO(S) INICIAL(ES) serán sustituidos por el (los) DOCUMENTOS(S) FINAL(ES) que deberán ser cobrados por el INTERMEDIARIO FINANCIERO.";
  } 

	if(obtieneTipoCredito.equals("C")   &&  meseSinIntereses.equals("S") ) {	
		cadEpoCemex += " \n \n Los parámetros de esta cadena toman como base el compromiso acordado en la CARTA DE CONSENTIMIENTO, por lo que es una cadena que opera bajo el concepto de Tasa 0% para el SUJETO DE APOYO, donde usted se compromete a cubrir la COMISION POR SERVICIOS FINANCIEROS al INTERMEDIARIO FINANCIERO, de acuerdo a lo pactado en dicho documento y por la(s) operación(es) aquí plasmadas.";				 
		cadEpo += "\n \n Los parámetros de esta cadena toman como base el compromiso acordado en la CARTA DE CONSENTIMIENTO, por lo que es una cadena que opera bajo el concepto de Tasa 0% para el SUJETO DE APOYO, donde usted se compromete a cubrir la COMISION POR SERVICIOS FINANCIEROS al INTERMEDIARIO FINANCIERO, de acuerdo a lo pactado en dicho documento y por la(s) operación(es) aquí plasmadas.";				 	
	}
		
				
String contentType = "text/html;charset=ISO-8859-1";
String 		nombreArchivo 	= null;	
CreaArchivo archivo 				= null;
archivo  = new CreaArchivo();

ComunesPDF pdfDoc = new ComunesPDF();
	
if(tipoArchivo.equals("PDF")){
	archivo 			    = new CreaArchivo();
	nombreArchivo 	  = archivo.nombreArchivo()+".pdf";
	pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
}	
	String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
	String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
	String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
	String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));


/************************************************************************************/
// boton de  Imprimir pantalla detalle
/***********************************************************************************/

CrearArchivosAcuses  generaAch =  new CrearArchivosAcuses();


System.out.println("obtieneTipoCredito "+obtieneTipoCredito);  
System.out.println("============================================= ====");  
System.out.println("informacion ===="+informacion);
System.out.println("============================================= ===="); 

if (informacion.equals("ImprimirDetalle") && tipoArchivo.equals("PDF") ) {	

					
	pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String) application.getAttribute("strDirectorioPublicacion"), "");
	String  mensaje =      
        "Al trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, está de acuerdo en que el (los) Documento (s) inicial (es) que se "+
      "detalla (n) en está pantalla puedan ser operados indistintamente a través de  las siguientes modalidades, en términos de los pactado en el “Contrato de Financiamiento a Clientes y "+
      "Distribuidores” (para conocer las modalidades convenidas favor de revisar su contrato): \n\n"+
      "a) DESCUENTO y/o FACTORAJE. \n "+
      "De operarse en esta modalidad y para todos los efectos legales,  bajo su responsabilidad, está solicitando el DESCUENTO y/o FACTORAJE del (los) Documento (s) inicial (es) que se detallan "+
      "en está pantalla y que ha publicado electrónicamente, otorgando su consentimiento para que en caso  de que se den la condiciones señaladas en el “Contrato de Financiamiento a Clientes y "+
      "Distribuidores”, el (los) Documento (s) inicial (es)  será (n) sustituido (s) por el (los) DOCUMENTO (S) FINAL (ES), cediendo los derechos de cobro a su favor al INTERMEDIARIO FINANCIERO que los opere."+
      "\n\n "+
      "b) Pago del CLIENTE o DISTRIBUIDOR mediante Tarjeta de Crédito. \n"+
      "De operarse en esta modalidad y para todos los efectos legales, otorga su consentimiento para que sea cubierto el (los) Documento (s) inicial (es) que se detalla (n) en está pantalla "+
      "y que ha publicado electrónicamente, en términos del “Contrato de Financiamiento a Clientes y Distribuidores”, mediante la utilización de una Tarjeta de Crédito asociada al CLIENTE o DISTRIBUIDOR."+
      "\n\n"+
      "Asimismo, en este acto manifiesto bajo protesta de decir verdad, que sí he emitido o emitiré a mi CLIENTE o DISTRIBUIDOR el CFDI por la operación comercial que le dio origen a esta transacción, " +
      "según sea el caso y conforme establezcan las disposiciones fiscales vigentes.\n";
    
    
    
	if(obtieneTipoCredito.equals("D")) {
	 mensaje = mensaje + "Para los siguientes documentos a publicar para PyME’s quedando la modalidad Descuento, se deberá seleccionar el IF y, si aplica, el plazo del crédito. ";			
	}else if(obtieneTipoCredito.equals("C")) {
		 mensaje = mensaje + "Se muestran los siguientes documentos a publicar para las PyME’s que operan bajo la modalidad Crédito en Cuenta Corriente";		
	}
	
    mensaje +=
          "\nAl trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, está de acuerdo en que el (los) Documento (s) inicial (es) que se "+
      "detalla (n) en está pantalla puedan ser operados indistintamente a través de  las siguientes modalidades, en términos de los pactado en el “Contrato de Financiamiento a Clientes y "+
      "Distribuidores” (para conocer las modalidades convenidas favor de revisar su contrato): \n\n "+
      "a) DESCUENTO y/o FACTORAJE. \n "+
      "De operarse en esta modalidad y para todos los efectos legales,  bajo su responsabilidad, está solicitando el DESCUENTO y/o FACTORAJE del (los) Documento (s) inicial (es) que se detallan "+
      "en está pantalla y que ha publicado electrónicamente, otorgando su consentimiento para que en caso  de que se den la condiciones señaladas en el “Contrato de Financiamiento a Clientes y "+
      "Distribuidores”, el (los) Documento (s) inicial (es)  será (n) sustituido (s) por el (los) DOCUMENTO (S) FINAL (ES), cediendo los derechos de cobro a su favor al INTERMEDIARIO FINANCIERO que los opere."+
      "\n\n "+
      "b) Pago del CLIENTE o DISTRIBUIDOR mediante Tarjeta de Crédito. \n"+
      "De operarse en esta modalidad y para todos los efectos legales, otorga su consentimiento para que sea cubierto el (los) Documento (s) inicial (es) que se detalla (n) en está pantalla "+
      "y que ha publicado electrónicamente, en términos del “Contrato de Financiamiento a Clientes y Distribuidores”, mediante la utilización de una Tarjeta de Crédito asociada al CLIENTE o DISTRIBUIDOR."+
      "\n\n"+
      "Asimismo, en este acto manifiesto bajo protesta de decir verdad, que sí he emitido o emitiré a mi CLIENTE o DISTRIBUIDOR el CFDI por la operación comercial que le dio origen a esta transacción, " +
      "según sea el caso y conforme establezcan las disposiciones fiscales vigentes.";
    
	pdfDoc.addText(mensaje,"formas",ComunesPDF.LEFT);	
	pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);	
		
	//Monitor de Lineas de Credito
	if(obtieneTipoCredito.equals("D") &&  ventaCartera.equals("N")) {
		pdfDoc.setTable(9, 100);
		pdfDoc.setCell("Monitor de líneas de crédito", "celda02", ComunesPDF.CENTER,9);
		pdfDoc.setCell("Línea de Crédito IF", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Saldo inicial de la línea ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Saldo actual de doctos financiados  ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto de Doctos en proceso de autorización  ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Saldo disponible de la Línea 	 ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto de documentos negociables ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto de documentos publicados ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Num. doctos. publicados  ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Saldo potencial disponible de la línea ", "celda02", ComunesPDF.CENTER);
	
		Vector vecFilas = cargaDocto.monitorLineas(ses_ic_epo);
		
		for(int i=0;i<vecFilas.size();i++){		
			Vector 	vecColumnas = (Vector)vecFilas.get(i);
			String ic_linea = (String)vecColumnas.get(0);
			 moneda_linea = (String)vecColumnas.get(4)+" "+(String)vecColumnas.get(1);
			String saldo_inicial = (String)vecColumnas.get(2);  
			String saldo_financiado = (String)vecColumnas.get(5);  
			String monto_proceso = (String)vecColumnas.get(6);  
			String saldo_disponible_linea = (String)vecColumnas.get(7);  
			String monto_negociable = (String)vecColumnas.get(8);  	
			String montoSeleccionado ="";
			String numDoctos =""; 
			String saldoDisponible = vecColumnas.get(9).toString();
			
			
			pdfDoc.setCell(ic_linea  ,"formas", ComunesPDF.CENTER);		
			pdfDoc.setCell("$"+Comunes.formatoDecimal(saldo_inicial,2) ,"formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(saldo_financiado,2) ,"formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_proceso,2)   ,"formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(saldo_disponible_linea,2)  ,"formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_negociable,2)  ,"formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoSeleccionado,2)   ,"formas", ComunesPDF.RIGHT);
			pdfDoc.setCell( numDoctos  ,"formas", ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(saldoDisponible,2)   ,"formas", ComunesPDF.RIGHT);
			
		}//for
		pdfDoc.addTable();
	
			//consulta de los datos
		double columnas =17;
		int columnas1 =17;
		if(operaConversion){ columnas +=3; columnas1 +=3; }
		if(nombresCampo.size()>0){ columnas +=nombresCampo.size(); columnas1 +=nombresCampo.size(); } 
		double  columnasT = columnas/2;
		int columnasT1 = columnas1/2;
		String  Atotales = Double.toString (columnasT) ; 
		columnasT = Math.round(columnasT);	
		String  totales = Double.toString (columnasT) ;
		
		if(!Atotales.equals(totales)){
			columnasT1+=1;
		}
		
		pdfDoc.setTable(columnasT1, 100);		
		pdfDoc.setCell("Distribuidor", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Número docto. inicial", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de emisión", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha vto. docto.", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Categoria ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo para descuento en dias ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("% descuento ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a descontar ", "celda02", ComunesPDF.CENTER);	
		if(operaConversion){
			pdfDoc.setCell("Tipo conversión", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo cambio", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto valuado en Pesos", "celda02", ComunesPDF.CENTER);
		}		
		pdfDoc.setCell("Modalidad plazo", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Comentarios", "celda02", ComunesPDF.CENTER);
		for (int j=0;j<nombresCampo.size();j++){
				Vector detalleCampo = (Vector) nombresCampo.get(j);
				pdfDoc.setCell((String)detalleCampo.get(0), "celda02", ComunesPDF.CENTER);			
		}
	
		pdfDoc.setCell("IF ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha vto. ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Estatus ", "celda02", ComunesPDF.CENTER);
		if(!Atotales.equals(totales)){
			pdfDoc.setCell("  ", "celda02", ComunesPDF.CENTER);
		}
		
		
	
		Vector vecFilasC = cargaDocto.getDoctoInsertado(proceso,iNoCliente,null,obtieneTipoCredito);
		for(int i=0;i<vecFilasC.size();i++){
			Vector vecColumnas = (Vector)vecFilasC.get(i);
			 nombrePyme 			= (String)vecColumnas.get(1);
			 clavePyme 			= (String)vecColumnas.get(9);
			 claveDistribuidor 	= (String)vecColumnas.get(0);
			 numeroDocumento 	= (String)vecColumnas.get(2);
			 fechaEmision 		= (String)vecColumnas.get(3);
			 fechaVencimiento 	= (String)vecColumnas.get(4);
			 plazoDocto 			= (String)vecColumnas.get(21);
			 monedaNombre 		= (String)vecColumnas.get(5);
			 monto 				= (String)vecColumnas.get(6);
			 tipoCredito 		= (String)vecColumnas.get(25);
			 respPagoInteres 	= (String)vecColumnas.get(23);
			 modoPlazo 			= (String)vecColumnas.get(22);
			 plazoDescuento 		= (String)vecColumnas.get(13);
			 porcDescuento		= (String)vecColumnas.get(12);
			 montoDescuento 		= (String)vecColumnas.get(26);
			 tipoConversion 		= (String)vecColumnas.get(14);
			 tipoCambio 			= (String)vecColumnas.get(15);
			 ffn_monto			= Double.parseDouble(vecColumnas.get(6).toString());		
			 monto_valuado		= "".equals(tipoCambio)?ffn_monto:(Double.parseDouble(tipoCambio)*ffn_monto);
			 referencia 			= (String)vecColumnas.get(7);
			 consecutivo 		= (String)vecColumnas.get(10);
			 icMoneda			= (String)vecColumnas.get(8);
			 numCampos = Integer.parseInt(vecColumnas.get(30).toString());
			 categoria			= (String)vecColumnas.get(31);
			 estatus			= (String)vecColumnas.get(33);
			 plazo_credito=  (String)vecColumnas.get(19)==null?"N/A":(String)vecColumnas.get(19); 
			 fecha_vto_credito =  (String)vecColumnas.get(20)==null?"N/A":(String)vecColumnas.get(20); 
			 responsable_interes	= (String)vecColumnas.get(16);
			 numCampos = Integer.parseInt(vecColumnas.get(30).toString());
			 desEstatus			= (String)vecColumnas.get(35); //F032-2014
			
			if (estatus.equals("1")){ //No Negociable Fodea 029-2010 Distribuidores
				DescripcionEstatus = "NO";
			}else if (estatus.equals("2")){ //Negociable
				DescripcionEstatus ="SI";
			}
			bandeVentaCartera			= (String)vecColumnas.get(34);
			if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
			}					
			if(montoDescuento==null||"".equals(montoDescuento))
				montoDescuento = "0";
		
		if("1".equals(icMoneda)){
			num_doctos_mn++;
			fn_monto_total_mn += Double.parseDouble(monto);
			monto_valuado_total_mn += Double.parseDouble(montoDescuento);
		}else{
			num_doctos_usd++;
			fn_monto_total_usd +=  Double.parseDouble(monto);
			monto_valuado_total_usd += Double.parseDouble(montoDescuento);
		}
		
		
			pdfDoc.setCell(claveDistribuidor ,"formas", ComunesPDF.CENTER);
			pdfDoc.setCell(numeroDocumento ,"formas", ComunesPDF.CENTER);		
			pdfDoc.setCell(fechaEmision ,"formas", ComunesPDF.CENTER);		
			pdfDoc.setCell(fechaVencimiento ,"formas", ComunesPDF.CENTER);		
			pdfDoc.setCell(plazoDocto ,"formas", ComunesPDF.CENTER);		
			pdfDoc.setCell(monedaNombre,"formas", ComunesPDF.LEFT);		
			pdfDoc.setCell("$"+Comunes.formatoDecimal( monto,2) ,"formas", ComunesPDF.RIGHT);		
			pdfDoc.setCell(categoria+" " ,"formas", ComunesPDF.RIGHT);		
			pdfDoc.setCell(plazoDescuento,"formas", ComunesPDF.CENTER);		
			pdfDoc.setCell(porcDescuento+"%","formas", ComunesPDF.CENTER);		
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2) ,"formas", ComunesPDF.RIGHT);	
			
			if(operaConversion){
				pdfDoc.setCell(tipoConversion,"formas", ComunesPDF.CENTER);		
				pdfDoc.setCell(tipoCambio,"formas", ComunesPDF.CENTER);		
				pdfDoc.setCell("$"+Comunes.formatoDecimal( Double.toString(monto_valuado),2)  ,"formas", ComunesPDF.RIGHT);				
			}
			pdfDoc.setCell(modoPlazo,"formas", ComunesPDF.LEFT);		
			pdfDoc.setCell(referencia,"formas", ComunesPDF.LEFT);		
			
			
			for (int j=0;j<=nombresCampo.size();j++){				
				if(j==1) { pdfDoc.setCell((String)vecColumnas.get(numCampos+1) ,"formas", ComunesPDF.LEFT);	  }
				if(j==2) { pdfDoc.setCell((String)vecColumnas.get(numCampos+2) ,"formas", ComunesPDF.LEFT);	  }
				if(j==3) { pdfDoc.setCell((String)vecColumnas.get(numCampos+3) ,"formas", ComunesPDF.LEFT);	  }
				if(j==4) { pdfDoc.setCell((String)vecColumnas.get(numCampos+4) ,"formas", ComunesPDF.LEFT);	  }
				if(j==5) { pdfDoc.setCell((String)vecColumnas.get(numCampos+5) ,"formas", ComunesPDF.LEFT);	  }
			}
					
			pdfDoc.setCell(" " ,"formas", ComunesPDF.RIGHT);	 //IF
			
			
			if("EPO".equals(responsable_interes)  ){	
				pdfDoc.setCell(plazo_credito,"formas", ComunesPDF.CENTER);		
				pdfDoc.setCell(fecha_vto_credito ,"formas", ComunesPDF.CENTER);	
			}else{	
				pdfDoc.setCell("N/A" ,"formas", ComunesPDF.CENTER);		
				pdfDoc.setCell("N/A" ,"formas", ComunesPDF.CENTER);		
			}
			
			pdfDoc.setCell(desEstatus, "formas", ComunesPDF.CENTER);	
			if(!Atotales.equals(totales)){
				pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
			}	
						
					
		} //concluye consulta
		 
					
			int valor = 2+nombresCampo.size();
		
			pdfDoc.setCell("Total M.N.:","formas",ComunesPDF.RIGHT,2);
			pdfDoc.setCell(String.valueOf(num_doctos_mn),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT,3);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto_total_mn,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT,3);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_valuado_total_mn,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
				
			pdfDoc.setCell("Total Dolares:","formas",ComunesPDF.RIGHT,2);
			pdfDoc.setCell(String.valueOf(num_doctos_usd),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT,3);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto_total_usd,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT,3);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_valuado_total_usd,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			
			pdfDoc.addTable();
		
		
	}else if(  (obtieneTipoCredito.equals("D") && ventaCartera.equals("S") )   || obtieneTipoCredito.equals("C") ||  obtieneTipoCredito.equals("A")  ){
	
		Vector	vecFilas = cargaDocto.getDoctoInsertado(proceso,ses_ic_epo,null,obtieneTipoCredito);
		
		int campos = 13+nombresCampo.size();
		if(meseSinIntereses.equals("S") && obtieneTipoCredito.equals("C")   ) { campos =campos+1;  }
		
		if(vecFilas.size()>0){
			
				pdfDoc.setTable(campos, 100);		
				pdfDoc.setCell("Cliente", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Número docto.", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de emisión", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha vto. docto.", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en dias", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("% descuento", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto % de descuento", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Modalidad plazo", "celda02", ComunesPDF.CENTER);
				if(meseSinIntereses.equals("S")  && obtieneTipoCredito.equals("C") ) { 
				pdfDoc.setCell("Tipo de Pago", "celda02", ComunesPDF.CENTER); 
				}
				pdfDoc.setCell("Comentarios", "celda02", ComunesPDF.CENTER);
				for (int j=0;j<nombresCampo.size();j++){
					Vector detalleCampo = (Vector) nombresCampo.get(j);
					pdfDoc.setCell((String)detalleCampo.get(0), "celda02", ComunesPDF.CENTER);
				}			
				pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER);
				
				
				for(int i=0;i<vecFilas.size();i++){
					Vector  vecColumnas = (Vector)vecFilas.get(i);
					num_cliente			= (String)vecColumnas.get(0);
					ig_numero_docto		= (String)vecColumnas.get(2);
					df_fecha_emision	= (String)vecColumnas.get(3);
					df_fecha_vto		= (String)vecColumnas.get(4);
					moneda				= (String)vecColumnas.get(5);
					ic_moneda 			= (String)vecColumnas.get(8);
					fn_monto2			= Double.parseDouble(vecColumnas.get(6).toString());
					tipo_conversion		= (String)vecColumnas.get(14);
					tipo_cambio			= "1".equals(ic_moneda)?"":(String)vecColumnas.get(15);
					monto_valuado		= "".equals(tipo_cambio)?fn_monto2:(Double.parseDouble(tipo_cambio)*fn_monto2);
					ig_plazo_descuento	= (String)vecColumnas.get(13);
					fn_porc_desc		= ("".equals((String)vecColumnas.get(12))||(String)vecColumnas.get(12)==null)?"0":(String)vecColumnas.get(12);
					monto_descuento2		= fn_monto2 * (Double.parseDouble(fn_porc_desc)/100);
				//	responsable_interes	= (String)vecColumnas.get(16);
				//	tipo_cobranza		= (String)vecColumnas.get(17);
					ct_referencia		= (String)vecColumnas.get(7);
					ic_linea_credito_dm	= (String)vecColumnas.get(18);
					plazo_credito		= (String)vecColumnas.get(19);
					fecha_vto_credito	= (String)vecColumnas.get(20);
					ig_plazo_docto		= (String)vecColumnas.get(21);
					modo_plazo			= (String)vecColumnas.get(22);
					dias_minimo			= (String)vecColumnas.get(28);
					dias_maximo			= (String)vecColumnas.get(29);
					numCampos = Integer.parseInt(vecColumnas.get(30).toString());
					desEstatus			= (String)vecColumnas.get(35); //F032-2014
					desTipoPago			= (String)vecColumnas.get(36); //F09-2015
					
					if("1".equals(ic_moneda)){
						num_doctos_mn++;
						fn_monto_total_mn += fn_monto2;
						monto_valuado_total_mn += monto_descuento2;
					}else{
						num_doctos_usd++;
						fn_monto_total_usd += fn_monto2;
						monto_valuado_total_usd += monto_descuento2;
					}
			
					pdfDoc.setCell(num_cliente ,"formas", ComunesPDF.CENTER);	
					pdfDoc.setCell(ig_numero_docto ,"formas", ComunesPDF.CENTER);	
					pdfDoc.setCell(df_fecha_emision ,"formas", ComunesPDF.CENTER);	
					pdfDoc.setCell(df_fecha_vto ,"formas", ComunesPDF.CENTER);	
					pdfDoc.setCell(ig_plazo_docto ,"formas", ComunesPDF.CENTER);	
					pdfDoc.setCell(moneda ,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto2,2) ,"formas", ComunesPDF.RIGHT);
					pdfDoc.setCell(ig_plazo_descuento ,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(fn_porc_desc+(("".equals(fn_porc_desc))?"":" %") ,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_descuento2,2) ,"formas", ComunesPDF.RIGHT);
					pdfDoc.setCell(modo_plazo ,"formas", ComunesPDF.CENTER);
					if(meseSinIntereses.equals("S") && obtieneTipoCredito.equals("C") ) { 
						pdfDoc.setCell(desTipoPago ,"formas", ComunesPDF.CENTER);
					}
					pdfDoc.setCell(ct_referencia ,"formas", ComunesPDF.CENTER);
					
					for (int j=0;j<=nombresCampo.size();j++){		
						if(j==1) { pdfDoc.setCell((String)vecColumnas.get(numCampos+1) ,"formas", ComunesPDF.LEFT);	  }
						if(j==2) { pdfDoc.setCell((String)vecColumnas.get(numCampos+2) ,"formas", ComunesPDF.LEFT);	  }
						if(j==3) { pdfDoc.setCell((String)vecColumnas.get(numCampos+3) ,"formas", ComunesPDF.LEFT);	  }
						if(j==4) { pdfDoc.setCell((String)vecColumnas.get(numCampos+4) ,"formas", ComunesPDF.LEFT);	  }
						if(j==5) { pdfDoc.setCell((String)vecColumnas.get(numCampos+5) ,"formas", ComunesPDF.LEFT);	  }
					}	
					pdfDoc.setCell(desEstatus ,"formas", ComunesPDF.CENTER);
					
			}//for
				int valor = 3+nombresCampo.size();
				if(meseSinIntereses.equals("S")  && obtieneTipoCredito.equals("C") ) { valor =valor+1;  }

				pdfDoc.setCell("Total M.N.:","formas",ComunesPDF.RIGHT,2);
				pdfDoc.setCell(String.valueOf(num_doctos_mn),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("","formas",ComunesPDF.LEFT,3);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto_total_mn,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("","formas",ComunesPDF.LEFT,2);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_valuado_total_mn,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("","formas",ComunesPDF.LEFT, valor);
					
				pdfDoc.setCell("Total Dolares:","formas",ComunesPDF.RIGHT,2);
				pdfDoc.setCell(String.valueOf(num_doctos_usd),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("","formas",ComunesPDF.LEFT,3);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto_total_usd,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("","formas",ComunesPDF.LEFT,2);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_valuado_total_usd,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("","formas",ComunesPDF.LEFT,valor);
				
				if(strPerfil.equals("ADMIN EPO")  ||  strPerfil.equals("ADMIN EPO GRUPO")   ||  strPerfil.equals("ADMIN EPO FON") )  {
					if(application.getInitParameter("EpoCemex").equals(iNoCliente)){
						pdfDoc.setCell(cadEpoCemex,"formas",ComunesPDF.JUSTIFIED,campos);
					}else {
						pdfDoc.setCell(cadEpo,"formas",ComunesPDF.JUSTIFIED,campos);
					}				
				}
				pdfDoc.addTable();
		}				
	}

	
	pdfDoc.endDocument();		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	

/************************************************************************************/
// boton de btndetalleDM Imprimir detalle documentos Descuento y/o Factoraje
/***********************************************************************************/
}else if ( ( informacion.equals("btndetalleDM") || informacion.equals("btndetalleCCC") )   && tipoArchivo.equals("PDF") ) {
	
	Vector mod_cg_campo			= new Vector(5);
	Vector vecFilas = cargaDocto.getDoctoInsertado(acuse,obtieneTipoCredito);
	int colum = 14;
	int colum2 = 2;
	if(vecFilas.size()>0){
		pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String) application.getAttribute("strDirectorioPublicacion"), "");
		
		
		if(meseSinIntereses.equals("S") && obtieneTipoCredito.equals("C") ) {  colum = colum+1;   colum2 = colum2+1;  }
		
		pdfDoc.setTable(colum, 100);		
		pdfDoc.setCell("A", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Cliente", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Número docto.", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de emisión ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha vto. docto. ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Categoria ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo  para descuento en dias", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("% descuento", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a Descontar", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("  ", "celda02", ComunesPDF.CENTER,colum2);
		

		pdfDoc.setCell("B", "celda02", ComunesPDF.CENTER);
		if(operaConversion){
			pdfDoc.setCell("Tipo conversión", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo cambio", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto valuado en Pesos", "celda02", ComunesPDF.CENTER);
		}		
		pdfDoc.setCell("Modalidad plazo", "celda02", ComunesPDF.CENTER);
		if(meseSinIntereses.equals("S")  && obtieneTipoCredito.equals("C") ) {
			pdfDoc.setCell("Tipo de Pago", "celda02", ComunesPDF.CENTER);
		}
		pdfDoc.setCell("Comentarios", "celda02", ComunesPDF.CENTER);
		
		for (int j=0;j<nombresCampo.size();j++){
			Vector detalleCampo = (Vector) nombresCampo.get(j);
			pdfDoc.setCell((String)detalleCampo.get(0), "celda02", ComunesPDF.CENTER);			
		}
		
		if("D".equals(tipoCredito)){
			pdfDoc.setCell("Plazo crédito", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha vto crédito", "celda02", ComunesPDF.CENTER);
			if("ADMIN EPO".equals(strPerfil) || "ADMIN EPO GRUPO".equals(strPerfil) ||  strPerfil.equals("ADMIN EPO FON") ) {
				pdfDoc.setCell("Negociable", "celda02", ComunesPDF.CENTER);
			}else if("EPO MAN0 DISTR".equals(strPerfil) || "EPO MAN1 DISTR".equals(strPerfil)  || "ADMIN EPO DISTR".equals(strPerfil)) {
				pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER);
			}			
		}else if ("C".equals(tipoCredito)){
			pdfDoc.setCell("Plazo crédito", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha vto crédito", "celda02", ComunesPDF.CENTER);
			if("ADMIN EPO".equals(strPerfil) || "ADMIN EPO GRUPO".equals(strPerfil)  ||  strPerfil.equals("ADMIN EPO FON") ) {
				pdfDoc.setCell("Negociable", "celda02", ComunesPDF.CENTER);
			}else if("EPO MAN0 DISTR".equals(strPerfil)  || "EPO MAN1 DISTR".equals(strPerfil) || "ADMIN EPO DISTR".equals(strPerfil) ) {
				pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER);
			}
		}else if ("A".equals(tipoCredito)){
			if("ADMIN EPO".equals(strPerfil) || "ADMIN EPO GRUPO".equals(strPerfil) ||  strPerfil.equals("ADMIN EPO FON") ) {
				pdfDoc.setCell("Negociable", "celda02", ComunesPDF.CENTER);
			}else if("EPO MAN0 DISTR".equals(strPerfil)  || "EPO MAN1 DISTR".equals(strPerfil) || "ADMIN EPO DISTR".equals(strPerfil) ) {
				pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER);
			}
			pdfDoc.setCell(" ", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(" ", "celda02", ComunesPDF.CENTER);
		}
		if(!operaConversion){
			pdfDoc.setCell("  ", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("  ", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("  ", "celda02", ComunesPDF.CENTER);
		}
		
		int cam = 5-nombresCampo.size();
		for (int j=0;j<cam;j++){
			pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);			
		}
	
	}
	
	for(int i=0;i<vecFilas.size();i++){
		Vector vecColumnas = (Vector)vecFilas.get(i);
		num_cliente			= (String)vecColumnas.get(0);
		ig_numero_docto		= (String)vecColumnas.get(2);
		df_fecha_emision	= (String)vecColumnas.get(3);
		df_fecha_vto		= (String)vecColumnas.get(4);
		moneda				= (String)vecColumnas.get(5);
		ic_moneda 			= (String)vecColumnas.get(8);
		fn_monto2			= Double.parseDouble(vecColumnas.get(6).toString());
		tipo_conversion		= (String)vecColumnas.get(14);
		tipo_cambio			= "1".equals(ic_moneda)?"":vecColumnas.get(15).toString().replace(',','.');
		fn_porc_desc		= ("".equals((String)vecColumnas.get(12))||(String)vecColumnas.get(12)==null)?"0":(String)vecColumnas.get(12);
		monto_descuento2		= fn_monto2 * (Double.parseDouble(fn_porc_desc)/100);
		monto_valuado		= "".equals(tipo_cambio)?fn_monto2:(Double.parseDouble(tipo_cambio)*fn_monto2);
		ig_plazo_descuento	= (String)vecColumnas.get(13);
		responsable_interes	= (String)vecColumnas.get(16);
		tipo_cobranza		= (String)vecColumnas.get(17);
		ct_referencia		= (String)vecColumnas.get(7);
		ic_linea_credito_dm	= (String)vecColumnas.get(18);
		plazo_credito		= (String)vecColumnas.get(19);
		fecha_vto_credito	= (String)vecColumnas.get(20);	
		ig_plazo_docto		= (String)vecColumnas.get(26);
		modo_plazo			= (String)vecColumnas.get(27);
		moneda_linea		= (String)vecColumnas.get(28);
		categoria			= (String)vecColumnas.get(29);
		desEstatus = (String)vecColumnas.get(32);
		desTipoPago = (String)vecColumnas.get(33);
		
		if(!"EPO".equals(responsable_interes)){
			//plazo_credito = "N/A";
			//fecha_vto_credito = "N/A";
		}
		if(ic_moneda.equals(moneda_linea)||"".equals(moneda_linea)){
			tipo_cambio = "";
			tipo_conversion = "";
			strMontoValuado = "";
		}else{
			strMontoValuado = "$"+ Comunes.formatoDecimal(monto_valuado,2);
		}
		if("1".equals(ic_moneda)){
			num_doctos_mn++;
			fn_monto_total_mn += fn_monto2;
			monto_valuado_total_mn += monto_valuado;
		}else{
			num_doctos_usd++;
			fn_monto_total_usd += fn_monto2;
			monto_valuado_total_usd += monto_valuado;
		}
    
    numCampos = 33;       
    for (int u=numCampos+1;u<vecColumnas.size();u++){
      mod_cg_campo.addElement(vecColumnas.get(u));
    }
		
    estatus			= (String)vecColumnas.get(30);
    if (estatus.equals("1")){ //No Negociable
      DescripcionEstatus = "NO";
    }else if (estatus.equals("2")){ //Negociable
      DescripcionEstatus ="SI";
    }
    bandeVentaCartera			= (String)vecColumnas.get(31);
	if (bandeVentaCartera.equals("S") ) {
			modo_plazo ="";
    }


		pdfDoc.setCell("A", "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(num_cliente, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(ig_numero_docto, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(df_fecha_emision, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(df_fecha_vto, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(ig_plazo_docto, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(moneda, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto2,2), "formas", ComunesPDF.RIGHT);
		pdfDoc.setCell(categoria, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(ig_plazo_descuento, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(fn_porc_desc+"%", "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_descuento2,2), "formas", ComunesPDF.RIGHT);
		pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER,colum2);	
				
		pdfDoc.setCell("B", "formas", ComunesPDF.CENTER);	
		if(operaConversion){
			pdfDoc.setCell(tipo_conversion, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell(tipo_cambio, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(strMontoValuado,2), "formas", ComunesPDF.RIGHT);
		}
		
		pdfDoc.setCell(modo_plazo, "formas", ComunesPDF.CENTER);  
		
		if(meseSinIntereses.equals("S") && obtieneTipoCredito.equals("C")  ) {
			pdfDoc.setCell(desTipoPago, "formas", ComunesPDF.CENTER);
		}
		pdfDoc.setCell(ct_referencia, "formas", ComunesPDF.LEFT);
		
		for (int j=0;j<=nombresCampo.size();j++){		
			if(j==1) { pdfDoc.setCell((String)vecColumnas.get(numCampos+1) ,"formas", ComunesPDF.RIGHT);	  }
			if(j==2) { pdfDoc.setCell((String)vecColumnas.get(numCampos+2) ,"formas", ComunesPDF.RIGHT);	  }
			if(j==3) { pdfDoc.setCell((String)vecColumnas.get(numCampos+3) ,"formas", ComunesPDF.RIGHT);	  }
			if(j==4) { pdfDoc.setCell((String)vecColumnas.get(numCampos+4) ,"formas", ComunesPDF.RIGHT);	  }
			if(j==5) { pdfDoc.setCell((String)vecColumnas.get(numCampos+5) ,"formas", ComunesPDF.RIGHT);	  }
		}		

				
		if("D".equals(tipoCredito) && ventaCartera.equals("S")){	
			pdfDoc.setCell("N/A", "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("N/A", "formas", ComunesPDF.CENTER);
			if("ADMIN EPO".equals(strPerfil) || "ADMIN EPO GRUPO".equals(strPerfil)  ||  strPerfil.equals("ADMIN EPO FON")) {
				pdfDoc.setCell(DescripcionEstatus, "formas", ComunesPDF.CENTER);	
			}else if("EPO MAN0 DISTR".equals(strPerfil) || "EPO MAN1 DISTR".equals(strPerfil)  || "ADMIN EPO DISTR".equals(strPerfil)) {
				pdfDoc.setCell(desEstatus, "formas", ComunesPDF.CENTER);	
			}	
		}
		if("D".equals(tipoCredito) &&  ventaCartera.equals("N")){	
			pdfDoc.setCell(plazo_credito, "formas", ComunesPDF.CENTER);		
			pdfDoc.setCell(fecha_vto_credito, "formas", ComunesPDF.CENTER);					
			if("ADMIN EPO".equals(strPerfil) || "ADMIN EPO GRUPO".equals(strPerfil)  ||  strPerfil.equals("ADMIN EPO FON") ) {
				pdfDoc.setCell(DescripcionEstatus, "formas", ComunesPDF.CENTER);	
			}else if("EPO MAN0 DISTR".equals(strPerfil) || "EPO MAN1 DISTR".equals(strPerfil)  || "ADMIN EPO DISTR".equals(strPerfil)) {
				pdfDoc.setCell(desEstatus, "formas", ComunesPDF.CENTER);	
			}
			
		}	else if("C".equals(tipoCredito)){
			pdfDoc.setCell(" ", "formas", ComunesPDF.CENTER);
			pdfDoc.setCell(" ", "formas", ComunesPDF.CENTER);
			if("ADMIN EPO".equals(strPerfil) || "ADMIN EPO GRUPO".equals(strPerfil) ||  strPerfil.equals("ADMIN EPO FON") ) {
				pdfDoc.setCell(DescripcionEstatus, "formas", ComunesPDF.CENTER);	
			}else if("EPO MAN0 DISTR".equals(strPerfil) || "EPO MAN1 DISTR".equals(strPerfil) || "ADMIN EPO DISTR".equals(strPerfil) ) {
				pdfDoc.setCell(desEstatus, "formas", ComunesPDF.CENTER);	
			}	
		}else if("A".equals(tipoCredito)){
			pdfDoc.setCell(DescripcionEstatus, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell(" ", "formas", ComunesPDF.CENTER);
			pdfDoc.setCell(" ", "formas", ComunesPDF.CENTER);
		}
		if(!operaConversion){
				pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
			}	
			
			int cam = 5-nombresCampo.size();
			for (int j=0;j<cam;j++){
				pdfDoc.setCell("", "formas", ComunesPDF.CENTER);			
			}			
			
	}//for
	int total = 6;
	if(meseSinIntereses.equals("S") && obtieneTipoCredito.equals("C")  ) { total=total+1; }
	
	pdfDoc.setCell("Total M.N.:","formas",ComunesPDF.RIGHT,2);
	pdfDoc.setCell(String.valueOf(num_doctos_mn),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("","formas",ComunesPDF.LEFT,4);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto_total_mn,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("","formas",ComunesPDF.LEFT,total);
	
	pdfDoc.setCell("Total Dolares:","formas",ComunesPDF.RIGHT,2);
	pdfDoc.setCell(String.valueOf(num_doctos_usd),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("","formas",ComunesPDF.LEFT,4);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto_total_usd,2),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("","formas",ComunesPDF.LEFT,total);
	
	if(strPerfil.equals("ADMIN EPO") || "ADMIN EPO GRUPO".equals(strPerfil)  ||  strPerfil.equals("ADMIN EPO FON") )  {
		if(application.getInitParameter("EpoCemex").equals(iNoCliente)){
			pdfDoc.setCell(cadEpoCemex , "formas", ComunesPDF.JUSTIFIED, colum);
		}else {
			pdfDoc.setCell(cadEpo , "formas", ComunesPDF.JUSTIFIED, colum);
		}		
	}
	
	pdfDoc.addTable();
		
		
	pdfDoc.endDocument();		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	





/************************************************************************************/
// boton de btnPrintAcuse Imprimir Acuse
/***********************************************************************************/

}else if ( informacion.equals("btnPrintAcuse") || informacion.equals("btnGenerarDetalleDF") ||  informacion.equals("btnGenerarDetalleCCC")  ) {	
	
	generaAch.setAcuse(acuse);
	generaAch.setTipoArchivo(tipoArchivo);	
	generaAch.setUsuario(iNoUsuario);
	generaAch.setVersion("PANTALLA");	
	
	int existeArch= 	generaAch.existerArcAcuse(); //verifica si existe 
	
	try {	
	
	if( existeArch ==0 ) {
	
		Vector vecFilas = cargaDocto.getDoctoInsertado(acuse,tipoCredito);	
		
		int numCamposAdicionales=nombresCampo.size();
		int camposAdi =  5-numCamposAdicionales;
		if(!operaConversion){	 	camposAdi+=3;   }	
		camposAdi+=1;
		int numElementos = vecFilas.size();
		int columna  =7;
		if(!"".equals(tipoCambio)){
			columna+=3;
		}

	
 // esta parte es para los totales 
	if(tipoArchivo.equals("PDF")) {
		
		pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String) application.getAttribute("strDirectorioPublicacion"), "");
	
		pdfDoc.setTable(columna, 80);	
		pdfDoc.setCell("Tipo de Documento inicial", "celda02", ComunesPDF.CENTER);	
		pdfDoc.setCell("Moneda Nacional", "celda02", ComunesPDF.CENTER,3);	
		pdfDoc.setCell("Dólares", "celda02", ComunesPDF.CENTER,3);
		if(!"".equals(tipoCambio)){
			pdfDoc.setCell("Doctos en DLL financiados en M.N.", "celda02", ComunesPDF.CENTER,3);
		}
		pdfDoc.setCell(" ", "celda02", ComunesPDF.CENTER);
	
		pdfDoc.setCell("Num. total de doctos.", "celda02", ComunesPDF.CENTER);	
		pdfDoc.setCell("Monto total de doctos.", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto del crédito", "celda02", ComunesPDF.CENTER);
		
		
		pdfDoc.setCell("Num. total de doctos.", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto total de doctos.", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto del crédito", "celda02", ComunesPDF.CENTER);
		
	
		if(!"".equals(tipoCambio)){		
			pdfDoc.setCell("Num. total de doctos.", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto total de doctos.", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto del crédito", "celda02", ComunesPDF.CENTER);			
		}
		
		for(int i=0;i<vecFilas.size();i++){
			Vector	vecColumnas = (Vector)vecFilas.get(i);	
			monedaLinea = (String)vecColumnas.get(28);
			fnPorcAux = "".equals(vecColumnas.get(12).toString())?0:Double.parseDouble(vecColumnas.get(12).toString());	
			
			if("D".equals(vecColumnas.get(11).toString())){
				if("1".equals(vecColumnas.get(8).toString())){
					totalDocDM ++;
					montoTotalDM += Double.parseDouble(vecColumnas.get(6).toString());
					montoCDescDM += Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
					interesTotalDM 	= 0;
				}
				if("54".equals(vecColumnas.get(8).toString())&&"54".equals(monedaLinea)){
					totalDocDMUSD ++;
					montoTotalDMUSD += Double.parseDouble(vecColumnas.get(6).toString());
					montoCDescDMUSD += Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
					interesTotalDMUSD = 0;
				}		
				if("54".equals(vecColumnas.get(8).toString())&&"1".equals(monedaLinea)){
					totalDocConvDM ++;
					montoTotalConvDM += Double.parseDouble(vecColumnas.get(6).toString());
					montoCDescConvDM += Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
				}																						
			}
			
			if("C".equals(vecColumnas.get(11).toString())){
				if("1".equals(vecColumnas.get(8).toString())){
					totalDocCC ++;
						montoTotalCC += Double.parseDouble(vecColumnas.get(6).toString());
						montoCDescCC += Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
						interesTotalCC 	= 0;
				}
				if("54".equals(vecColumnas.get(8).toString())){
					totalDocCCUSD ++;
					montoTotalCCUSD += Double.parseDouble(vecColumnas.get(6).toString());
					montoCDescCCUSD +=Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
				}
			}
			
			if("F".equals(vecColumnas.get(11).toString())){
				if("1".equals(vecColumnas.get(8).toString())){
					totalDocCC ++;
						montoTotalCC += Double.parseDouble(vecColumnas.get(6).toString());
						montoCDescCC += Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
						interesTotalCC 	= 0;
				}
				if("54".equals(vecColumnas.get(8).toString())){
					totalDocCCUSD ++;
					montoTotalCCUSD += Double.parseDouble(vecColumnas.get(6).toString());
					montoCDescCCUSD +=Double.parseDouble(vecColumnas.get(6).toString())*(fnPorcAux/100);
				}
			}
		} //for
	
			if(totalDocDM+totalDocDMUSD+totalDocConvDM>0){
				pdfDoc.setCell("Descuento", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell( String.valueOf(totalDocDM) , "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDM,2), "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDM-montoCDescDM,2), "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell( String.valueOf(totalDocDMUSD) , "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDMUSD,2), "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDMUSD-montoCDescDMUSD,2), "formas", ComunesPDF.RIGHT);
				if(!"".equals(tipoCambio)){	
					pdfDoc.setCell( String.valueOf(totalDocConvDM) , "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalConvDM*Double.parseDouble(tipoCambio),2), "formas", ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((montoTotalConvDM-montoCDescConvDM)*Double.parseDouble(tipoCambio),2), "formas", ComunesPDF.RIGHT);
				}
			}
			if(totalDocCC+totalDocCCUSD+totalDocConvCC>0 && tipoCredito.equals("C")){
				pdfDoc.setCell("Crédito cuenta corriente", "formas", ComunesPDF.CENTER);		
				pdfDoc.setCell( String.valueOf(totalDocCC) , "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoTotalCC,2) , "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalCC-montoCDescCC,2) , "formas", ComunesPDF.RIGHT);
				
				pdfDoc.setCell( String.valueOf(totalDocCCUSD) , "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoTotalCCUSD,2) , "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoTotalCCUSD-montoCDescCCUSD,2) , "formas", ComunesPDF.RIGHT);
	
				if(!"".equals(tipoCambio)){				
					pdfDoc.setCell( String.valueOf(totalDocConvCC) , "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoTotalConvCC*Double.parseDouble(tipoCambio),2) , "formas", ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+ Comunes.formatoDecimal((montoTotalConvCC-montoCDescConvCC)*Double.parseDouble(tipoCambio),2) , "formas", ComunesPDF.RIGHT);
				}			
			}	
			
			if(totalDocCC+totalDocCCUSD+totalDocConvCC>0 &&  tipoCredito.equals("F")){
				pdfDoc.setCell("Factoraje con Recuerso", "formas", ComunesPDF.CENTER);		
				pdfDoc.setCell( String.valueOf(totalDocCC) , "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoTotalCC,2) , "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalCC-montoCDescCC,2) , "formas", ComunesPDF.RIGHT);
				
				pdfDoc.setCell( String.valueOf(totalDocCCUSD) , "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoTotalCCUSD,2) , "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoTotalCCUSD-montoCDescCCUSD,2) , "formas", ComunesPDF.RIGHT);
	
				if(!"".equals(tipoCambio)){				
					pdfDoc.setCell( String.valueOf(totalDocConvCC) , "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoTotalConvCC*Double.parseDouble(tipoCambio),2) , "formas", ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+ Comunes.formatoDecimal((montoTotalConvCC-montoCDescConvCC)*Double.parseDouble(tipoCambio),2) , "formas", ComunesPDF.RIGHT);
				}			
			}			
			if(strPerfil.equals("ADMIN EPO") || "ADMIN EPO GRUPO".equals(strPerfil)  ||  strPerfil.equals("ADMIN EPO FON"))  {
				if(application.getInitParameter("EpoCemex").equals(iNoCliente)){
					pdfDoc.setCell(cadEpoCemex , "formas", ComunesPDF.JUSTIFIED, columna);
				}else {
					pdfDoc.setCell(cadEpo , "formas", ComunesPDF.JUSTIFIED, columna);
				}			
			}
			pdfDoc.addTable();
		
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);		 
 		
		//Cifras de Control 
		pdfDoc.setTable(2, 50);
		pdfDoc.setCell("Datos cifras de control", "celda02", ComunesPDF.CENTER,2);
		pdfDoc.setCell("Num. acuse", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell(acuse, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell(fechaHoy, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("Hora ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell(horaAcuse, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("Usuario ", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell(usuario, "formas", ComunesPDF.CENTER);
		pdfDoc.addTable();
		
				
		//documentos Cargados 
		if(numElementos>0) {	
			int  colum = 13;
			if(meseSinIntereses.equals("S")  && obtieneTipoCredito.equals("C") ) {colum =colum+1; }
			
			pdfDoc.setTable(colum, 100);
			pdfDoc.setCell("Número de Distribuidor", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Distribuidor", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Número docto. inicial", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de emisión", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha vto. docto.", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Categoria", "celda02", ComunesPDF.CENTER);	
			pdfDoc.setCell("Plazo para descuento en dias", "celda02", ComunesPDF.CENTER);	
			pdfDoc.setCell("% descuento", "celda02", ComunesPDF.CENTER);	
			pdfDoc.setCell("Monto % de descuento", "celda02", ComunesPDF.CENTER);
			if(operaConversion){
				pdfDoc.setCell("Tipo conversión", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo cambio", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto valuado en Pesos", "celda02", ComunesPDF.CENTER);
			}			 
			pdfDoc.setCell("Modalidad plazo", "celda02", ComunesPDF.CENTER); 
			if(meseSinIntereses.equals("S") && obtieneTipoCredito.equals("C")  ) {
				pdfDoc.setCell("Tipo de Pago ", "celda02", ComunesPDF.CENTER);		
			}
			pdfDoc.setCell("Comentarios", "celda02", ComunesPDF.CENTER);			
			for (int v=0;v<numCamposAdicionales;v++){
				Vector detalleCampo = (Vector) nombresCampo.get(v);
				pdfDoc.setCell((String)detalleCampo.get(0), "celda02", ComunesPDF.CENTER);
			}
			pdfDoc.setCell("Plazo", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha vto.", "celda02", ComunesPDF.CENTER);
			if("ADMIN EPO".equals(strPerfil) || "ADMIN EPO GRUPO".equals(strPerfil) ||  strPerfil.equals("ADMIN EPO FON")) {
				pdfDoc.setCell("Negociable", "celda02", ComunesPDF.CENTER);
			}else if("EPO MAN0 DISTR".equals(strPerfil) || "EPO MAN1 DISTR".equals(strPerfil) || "ADMIN EPO DISTR".equals(strPerfil) ) {
				pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER);
			}
			
			if(!operaConversion){
				pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
			}	
			int cam = 5-numCamposAdicionales;
			for (int j=0;j<cam;j++){
				pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);			
			}		
			pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
			if(meseSinIntereses.equals("S") && obtieneTipoCredito.equals("C")  ) {  
				pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);  
			}
		}
	}
	
	if(tipoArchivo.equals("CSV")) {
		contenidoArchivo.append("Número de Distribuidor,  Distribuidor, Número docto. inicial, Fecha de emisión,"+
		" Fecha vto. docto. , Plazo, Moneda, Monto, Categoria, Plazo para descuento en dias, % descuento, Monto % de descuento");
		if(operaConversion){
			contenidoArchivo.append(",  Tipo conversión , Tipo cambio, Monto valuado en Pesos");
		}
		contenidoArchivo.append(", Modalidad plazo ");
		if(meseSinIntereses.equals("S") && obtieneTipoCredito.equals("C")  ) {
			contenidoArchivo.append(", Tipo de Pago ");
		}
		
		contenidoArchivo.append(", Comentarios ");
			
		for (int v=0;v<numCamposAdicionales;v++){
			Vector detalleCampo = (Vector) nombresCampo.get(v);
			contenidoArchivo.append(","+(String)detalleCampo.get(0)+"");
		}
		contenidoArchivo.append(", Plazo, Fecha vto ");
		
		if("ADMIN EPO".equals(strPerfil) || "ADMIN EPO GRUPO".equals(strPerfil)  ||  strPerfil.equals("ADMIN EPO FON")) {
			contenidoArchivo.append(", Negociable ");
		}else if("EPO MAN0 DISTR".equals(strPerfil) || "EPO MAN1 DISTR".equals(strPerfil) || "ADMIN EPO DISTR".equals(strPerfil) ) {
			contenidoArchivo.append(", Estatus "); 
		}
		contenidoArchivo.append(" \n"); 
	}
			
			
	for(int i=0;i<vecFilas.size();i++){
			Vector	vecColumnas = (Vector)vecFilas.get(i);					
			fnPorcAux = "".equals(vecColumnas.get(12).toString())?0:Double.parseDouble(vecColumnas.get(12).toString());
			num_cliente			= (String)vecColumnas.get(0);
			cliente				= (String)vecColumnas.get(1);
			ig_numero_docto		= (String)vecColumnas.get(2);
			df_fecha_emision	= (String)vecColumnas.get(3);
			df_fecha_vto		= (String)vecColumnas.get(4);
			moneda				= (String)vecColumnas.get(5);
			ic_moneda 			= (String)vecColumnas.get(8);
			fn_monto2			= Double.parseDouble(vecColumnas.get(6).toString());
			tipo_conversion		= (String)vecColumnas.get(14);
			tipo_cambio			= "1".equals(ic_moneda)?"":(String)vecColumnas.get(15);
			monto_valuado		= "".equals(tipo_cambio)?fn_monto2:(Double.parseDouble(tipo_cambio)*fn_monto2);
			ig_plazo_descuento	= (String)vecColumnas.get(13);
			fn_porc_desc		= ("".equals((String)vecColumnas.get(12))||(String)vecColumnas.get(12)==null)?"0":(String)vecColumnas.get(12);
			monto_descuento2		= fn_monto2 * (Double.parseDouble(fn_porc_desc)/100);	
			ct_referencia		= (String)vecColumnas.get(7);
			ic_linea_credito_dm	= (String)vecColumnas.get(18);
			plazo_credito		= (String)vecColumnas.get(19);
			fecha_vto_credito	= (String)vecColumnas.get(20);
			ig_plazo_docto		= (String)vecColumnas.get(26);
			modo_plazo 			= (String)vecColumnas.get(27);
			fn_monto_total += fn_monto2;
			monto_valuado_total += monto_valuado;
			monedaLinea = (String)vecColumnas.get(28);
			categoria = (String)vecColumnas.get(29);
			desEstatus = (String)vecColumnas.get(32);
			desTipoPago = (String)vecColumnas.get(33);
			
			numCampos = 33;    
			for (int r=numCampos+1;r<vecColumnas.size();r++){
			// mod_cg_campo.addElement(vecColumnas.get(r));
			}
			estatus			= (String)vecColumnas.get(30);
			if (estatus.equals("1")){ //No Negociable
				DescripcionEstatus = "NO";
			}else if (estatus.equals("2")){ //Negociable
				DescripcionEstatus ="SI";
			}
				
			bandeVentaCartera			= (String)vecColumnas.get(31);
			if (bandeVentaCartera.equals("S") ) {
			modo_plazo ="";
			}
			String valor1  = ("54".equals(ic_moneda))?Comunes.formatoDecimal(tipo_cambio,2):"";
			String valor2 = ("54".equals(ic_moneda))?Comunes.formatoDecimal(monto_valuado,2):"";
			if(tipoCredito.equals("F")) {
				categoria ="NA";
				ig_plazo_descuento="NA";
				fn_porc_desc ="NA";
				modo_plazo="NA";
			}
			if(tipoArchivo.equals("PDF")) {
				pdfDoc.setCell(num_cliente,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cliente,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_emision,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_vto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ig_plazo_docto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto2,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(categoria,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(ig_plazo_descuento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fn_porc_desc+(("".equals(fn_porc_desc))?"":" %"),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_descuento2,2),"formas",ComunesPDF.RIGHT);						
				if(operaConversion){					
					pdfDoc.setCell(("54".equals(ic_moneda))?tipo_conversion:"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+valor1,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+valor2,"formas",ComunesPDF.RIGHT);					
				}
				pdfDoc.setCell(modo_plazo,"formas",ComunesPDF.CENTER);
				if(meseSinIntereses.equals("S")  && obtieneTipoCredito.equals("C") ) {  
					pdfDoc.setCell(desTipoPago,"formas",ComunesPDF.CENTER);
				}
				
				pdfDoc.setCell(ct_referencia,"formas",ComunesPDF.CENTER);			
				for (int j=0;j<=nombresCampo.size();j++){		
					if(j==1) { pdfDoc.setCell((String)vecColumnas.get(numCampos+1) ,"formas", ComunesPDF.RIGHT);	  }
					if(j==2) { pdfDoc.setCell((String)vecColumnas.get(numCampos+2) ,"formas", ComunesPDF.RIGHT);	  }
					if(j==3) { pdfDoc.setCell((String)vecColumnas.get(numCampos+3) ,"formas", ComunesPDF.RIGHT);	  }
					if(j==4) { pdfDoc.setCell((String)vecColumnas.get(numCampos+4) ,"formas", ComunesPDF.RIGHT);	  }
					if(j==5) { pdfDoc.setCell((String)vecColumnas.get(numCampos+5) ,"formas", ComunesPDF.RIGHT);	  }
				}		

				/*if(!obtieneTipoCredito.equals("D")){	
					pdfDoc.setCell(plazo_credito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_vto_credito,"formas",ComunesPDF.CENTER);				
				}
				if("EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("S")){
					pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(DescripcionEstatus,"formas",ComunesPDF.CENTER);				
				
				}else 	if(!"EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("N")){	
					pdfDoc.setCell(plazo_credito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_vto_credito,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(DescripcionEstatus,"formas",ComunesPDF.CENTER);				
				
				//} else 	if("EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("N") ){
				*/
					pdfDoc.setCell(plazo_credito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_vto_credito,"formas",ComunesPDF.CENTER);
					
					if("ADMIN EPO".equals(strPerfil) || "ADMIN EPO GRUPO".equals(strPerfil) ||  strPerfil.equals("ADMIN EPO FON") ) {
					pdfDoc.setCell(DescripcionEstatus, "formas", ComunesPDF.CENTER);	
					}else if("EPO MAN0 DISTR".equals(strPerfil) || "EPO MAN1 DISTR".equals(strPerfil) || "ADMIN EPO DISTR".equals(strPerfil) ) {
						pdfDoc.setCell(desEstatus, "formas", ComunesPDF.CENTER);	
					}
									
				if(!operaConversion){
					pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
				}	
				if(meseSinIntereses.equals("S") && obtieneTipoCredito.equals("C")  ) { 
					pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
				}
				int cam = 5-numCamposAdicionales;
				for (int j=0;j<cam;j++){
					pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);			
				}	
				pdfDoc.setCell("  ", "formas", ComunesPDF.CENTER);
			}
			
			if(tipoArchivo.equals("CSV")) {
				contenidoArchivo.append(num_cliente+",");
				contenidoArchivo.append(cliente.replace(',',' ')+",");
				contenidoArchivo.append(ig_numero_docto.replace(',',' ')+",");
				contenidoArchivo.append(df_fecha_emision+",");
				contenidoArchivo.append(df_fecha_vto+",");
				contenidoArchivo.append(ig_plazo_docto+",");;
				contenidoArchivo.append(moneda+",");
				contenidoArchivo.append("\"$"+Comunes.formatoDecimal(fn_monto2,2,true)+"\",");
				contenidoArchivo.append(categoria.replace(',',' ')+",");
				contenidoArchivo.append(ig_plazo_descuento+",");
				contenidoArchivo.append(fn_porc_desc+(("".equals(fn_porc_desc))?"":" %")+",");
				contenidoArchivo.append("\"$"+Comunes.formatoDecimal(monto_descuento2,2,true)+"\",");
				if(operaConversion){		
					if("54".equals(ic_moneda)){	
						contenidoArchivo.append(tipo_conversion+",");						
					}else  {
						contenidoArchivo.append(",");
					}					
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(valor1,2,true)+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(valor2,2,true)+"\",");								
				}
				contenidoArchivo.append(modo_plazo+",");
				
				if(meseSinIntereses.equals("S") && obtieneTipoCredito.equals("C")  ) {
					contenidoArchivo.append(desTipoPago+",");
				}
				contenidoArchivo.append(ct_referencia+",");	
				
				
				for (int e=1;e<=nombresCampo.size();e++){	
					String campo = (String)vecColumnas.get(numCampos+e);
					contenidoArchivo.append(campo.replace(',',' ')+",");
				}
			 
			
				
				if(!obtieneTipoCredito.equals("D")){	
					contenidoArchivo.append(plazo_credito+",");
					contenidoArchivo.append(fecha_vto_credito+",");				
				}
				if("EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("S")){
					contenidoArchivo.append("N/A"+",");
					contenidoArchivo.append("N/A"+",");				
				} else if(!"EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("S")){
					contenidoArchivo.append("N/A"+",");
					contenidoArchivo.append("N/A"+",");		
				}
				if(!"EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("N")){	
					contenidoArchivo.append(plazo_credito+",");
					contenidoArchivo.append(fecha_vto_credito+",");
				}else  if("EPO".equals(responsable_interes) && obtieneTipoCredito.equals("D") && ventaCartera.equals("N") ){
					contenidoArchivo.append(plazo_credito+",");
					contenidoArchivo.append(fecha_vto_credito+",");
				}	
				if("ADMIN EPO".equals(strPerfil) || "ADMIN EPO GRUPO".equals(strPerfil)  ||  strPerfil.equals("ADMIN EPO FON")) {
					contenidoArchivo.append(DescripcionEstatus);				
				}else if("EPO MAN0 DISTR".equals(strPerfil) || "EPO MAN1 DISTR".equals(strPerfil)  || "ADMIN EPO DISTR".equals(strPerfil)) {
					contenidoArchivo.append(desEstatus);		
				}
				contenidoArchivo.append("\n");	 
			}				
						
		}//for 
	 
	if(tipoArchivo.equals("CSV")){
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			
			generaAch.setRutaArchivo(strDirectorioTemp+nombreArchivo);
			generaAch.guardarArchivo() ; //Guardar	
	
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			jsonObj.put("accion",accion);
		}
	}
	
	if(tipoArchivo.equals("PDF")){
		pdfDoc.addTable();
		pdfDoc.endDocument();	
		
	
		generaAch.setRutaArchivo(strDirectorioTemp+nombreArchivo);
		generaAch.guardarArchivo() ; //Guardar	
	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		jsonObj.put("accion",accion);
	}
	

	}else 	if(existeArch>0)  {
	
	generaAch.setStrDirectorioTemp(strDirectorioTemp);
	nombreArchivo = generaAch.desArchAcuse() ; //Descargar
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("accion",accion);

	}
	} catch (Exception exception) {
		
		java.io.StringWriter outSW = new java.io.StringWriter();
		exception.printStackTrace(new java.io.PrintWriter(outSW));
		String stackTrace = outSW.toString();
			
		generaAch.setDesError(stackTrace);	
		
		generaAch.guardaBitacoraArch();	
	
	throw new NafinException("SIST0001");
	
} 
    
	
} 	


%>
<%=jsonObj%>





