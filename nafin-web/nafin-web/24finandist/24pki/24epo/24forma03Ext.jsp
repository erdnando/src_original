<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>
<%
	MantenimientoDist mantDist = ServiceLocator.getInstance().lookup("MantenimientoDistEJB", MantenimientoDist.class);

	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	String NOnegociable =  BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_NO_NEGOCIABLES"); 
	String ventaCartera =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); 
	String descuentoAutomatico = BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_DESC_AUTOMATICO");
	String tipoCredito =  BeanParametros.obtieneTipoCredito (iNoCliente); 
	String dias_amp_fecha_venc = mantDist.getDiasAmpFechaVen(iNoCliente);
	String fechaHoy		= "";
	try{
		fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	}catch(Exception e){
		fechaHoy = "";
	}

%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js"></script>
<script type="text/javascript" src="/nafin/00utils/NEcesionDerechos.js"></script>
<script type="text/javascript" src="24forma03Ext.js"></script>
<%@ include file="/00utils/componente_firma.jspf" %>

<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
<form id='formParametros' name="formParametros">
	<input type="hidden" id="descuentoAutomatico" name="descuentoAutomatico" value="<%=descuentoAutomatico%>"/>
	<input type="hidden" id="ventaCartera" name="ventaCartera" value="<%=ventaCartera%>"/>
	<input type="hidden" id="tipoCredito" name="tipoCredito" value="<%=tipoCredito%>"/>
	<input type="hidden" id="NOnegociable" name="NOnegociable" value="<%=NOnegociable%>"/>
	<input type="hidden" id="fechaHoy" name="fechaHoy" value="<%=fechaHoy%>"/>
	<input type="hidden" id="dias_amp_fecha_venc" name="dias_amp_fecha_venc" value="<%=dias_amp_fecha_venc%>"/>
</form>
</body>
</html>