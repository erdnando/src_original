
Ext.onReady(function() {
	
	var hidCifrasControl = Ext.getDom('hidCifrasControl').value;
	var tipoCarga = Ext.getDom('tipoCarga').value;
	var columna= 4;
	var txttotdoc = Ext.getDom('txttotdoc').value;
	var txtmtodoc = Ext.getDom('txtmtodoc').value;
	var txtmtomindoc = Ext.getDom('txtmtomindoc').value;
	var txtmtomaxdoc = Ext.getDom('txtmtomaxdoc').value;
	var txttotdocdo = Ext.getDom('txttotdocdo').value;
	var txtmtodocdo = Ext.getDom('txtmtodocdo').value;
	var txtmtomindocdo = Ext.getDom('txtmtomindocdo').value;
	var txtmtomaxdocdo = Ext.getDom('txtmtomaxdocdo').value;
	
	
		//para mostrar los archivos con errores
	var procesarSuccessFailureArchivoError =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarSuccessValidaCarga = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;		
		var proceso = jsonData.NUMERO_PROCESO; 
		var error_enc = jsonData.ERROR_ENC; 
		var archivo = jsonData.ARCHIVO_ERRORES;		
		var limiteLineaCredito  = jsonData.limiteLineaCredito;	
		var btnConfirmaCarga = jsonData.btnConfirmaCarga;	
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var fp = Ext.getCmp('formaCarga');		
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridResulCarga.isVisible()) {
				gridResulCarga.show();
			}
			
			var el = gridResulCarga.getGridEl();			
			if(store.getTotalCount() > 0) {	
				fpCarga.show();				
				gridTotales.show();
				gridLayout.hide();
				
				
				var cm = gridResulCarga.getColumnModel();
				if(limiteLineaCredito=='N') {
					gridResulCarga.getColumnModel().setHidden(cm.findColumnIndex('DES_PORCENTAJE'), true);				
					columna =3;				
				}	
				
					
				//se llena el grid de Totales 
				var cifras = [
					[jsonData.TOTALDOCS, 
					jsonData.MONTO_TOTAL_MNS,
					jsonData.MONTO_TOTAL_DLS,
					jsonData.TOTALDOCE,
					jsonData.MONTO_TOTAL_MNE,
					jsonData.MONTO_TOTAL_DLE]			
				];										
				resumenTotalesData.loadData(cifras);
			
			//se muestra el mensaje bajo los totales en caso de haber			
			var table ='<table align="left" width="700" class="formas"  cellspacing="0" cellpadding="3">'+
									'<tr><td width="275" colspan="4" >Errores vs cifras de control:</td> </tr>'+									
									'<tr>'+
									'<td valign="middle" align="left" class="formas" width="275" colspan="4">'+
										'<textarea name="conerrores" cols="100" wrap="hard" rows="6" class="formas">'+error_enc+'</textarea>'+
									'</td>'+
									'</tr>'+
									'	</table>';			
				var fpError = new Ext.Container({
					layout: 'table',
					id: 'fpError',				
					width:	'auto',
					heigth:	'auto',
					style: 'margin:0 auto;',
					items: [		
					{ 
						xtype:   'label',  
						html:		table, 
						cls:		'x-form-item', 
						style: { 
							width:   		'100%', 
							textAlign: 		'left'
						} 
					}				
				]
				});
				
				contenedorPrincipalCmp.remove(fpError);
				//contenedorPrincipalCmp.remove(fpGenerarC);
				
				if(error_enc!='') {
					contenedorPrincipalCmp.add(fpError);						
				}
				contenedorPrincipalCmp.add(NE.util.getEspaciador(20));	
				contenedorPrincipalCmp.add(fpGenerarC);
				contenedorPrincipalCmp.add(NE.util.getEspaciador(20));				
				contenedorPrincipalCmp.doLayout();
				
				//boton para mostrar los errores de la carga 
				var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
				
	
				if(archivo!=''){	
					btnGenerarCSV.show();					
				}else  if(archivo==''){
					btnGenerarCSV.hide();	
				}
				
				btnGenerarCSV.setHandler(
					function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');								
						Ext.Ajax.request({
							url: '24forma02ExtArchi.jsp',
							params: Ext.apply(fp.getForm().getValues(),{														
								archivo: archivo,											
								informacion:'GenerarArchivoError'
							}),
							callback: procesarSuccessFailureArchivoError
						});
					}
				);
	
				//boton para continuar con el proceso
				var btnContinuarP = Ext.getCmp('btnContinuarP');				
				if(btnConfirmaCarga=='S'){					
					btnContinuarP.show();
				}else if(btnConfirmaCarga=='N'){					
					btnContinuarP.hide();
				}	
				
				btnContinuarP.setHandler(
					function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');								
					var parametros = "pantalla=PreAcuse"+"&proceso="+proceso+"&tipoCarga="+tipoCarga+"&producto=FactoRecur";		
					document.location.href = "24forma01PreAExt.jsp?"+parametros;
					}
				);
								
				//el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
		//para mostrar TOTALES 
	var resumenTotalesData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'TOTALDOCS'},
			  {name: 'MONTO_TOTAL_MNS'},
				{name: 'MONTO_TOTAL_DLS'},
				{name: 'TOTALDOCE'},
				{name: 'MONTO_TOTAL_MNE'},
				{name: 'MONTO_TOTAL_DLE'}
		  ]
	 });
	 
	 	var gruposT = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Documentos sin errores', colspan: 3, align: 'center'},
				{header: 'Documentos con errores', colspan: 3, align: 'center'}					
			]
		]
	});
	
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',
		store: resumenTotalesData,	
		margins: '20 0 0 0',
		align: 'center',
		title: '',
		plugins: gruposT,	
		hidden: true,
		columns: [	
			{
				header: 'Total documentos',
				dataIndex: 'TOTALDOCS',
				width: 157,
				align: 'center'			
			},	
			{
				header: 'Monto Total MN',
				dataIndex: 'MONTO_TOTAL_MNS',
				width: 157,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto Total DL',
				dataIndex: 'MONTO_TOTAL_DLS',
				width: 157,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 		
			},	
			{
				header: 'Total documentos',
				dataIndex: 'TOTALDOCE',
				width: 157,
				align: 'center'			
			},	
			{
				header: 'Monto Total MN',
				dataIndex: 'MONTO_TOTAL_MNE',
				width: 157,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 			
			},
			{
				header: 'Monto Total DL',
				dataIndex: 'MONTO_TOTAL_DLE',
				width: 157,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 	
			}	
		],
		height: 100,	
		width : 943,
		frame: false
	});
	
	
	//Resultados del proceso de carga
		var resultadosCargaData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24forma02FExt.data.jsp',
		baseParams: {
			informacion: 'ValidaCargaArchivo'		
		},								
		fields: [			
			{name: 'DESCRIP_SERROR' , mapping: 'DESCRIP_SERROR'},			
			{name: 'PYME_SERROR', mapping: 'PYME_SERROR'},
			{name: 'MONTO_SERROR', mapping: 'MONTO_SERROR' },	
			{name: 'DES_PORCENTAJE', mapping: 'DES_PORCENTAJE' },			
			{name: 'NUDOCTO_CERROR',  mapping: 'NUDOCTO_CERROR'},			
			{name: 'CERROR',  mapping: 'CERROR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarSuccessValidaCarga,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarSuccessValidaCarga(null, null, null);						
				}
			}
		}
	});
	
	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Documentos sin errores', colspan: columna, align: 'center'},
				{header: 'Documentos con errores', colspan: 2, align: 'center'}					
			]
		]
	});
	
	
	var gridResulCarga = new Ext.grid.EditorGridPanel({	
		id: 'gridResulCarga',
		store: resultadosCargaData,	
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		align: 'center',
		plugins: grupos,	
		title: 'Resultados del proceso de carga',		
		hidden: true,
		columns: [	
			{
				header: 'Numero de documento',
				dataIndex: 'DESCRIP_SERROR',
				width: 117,
				align: 'left'	,
				resizable: true	
			},	
			{
				header: 'PYME',
				dataIndex: 'PYME_SERROR',
				width: 117,
				resizable: true	,
				align: 'center'				
			},			
			{
				header: 'Monto',
				dataIndex: 'MONTO_SERROR',
				width: 117,
				align: 'right',
				resizable: true	,
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},				
			{
				header: '% Porcentaje disponible de la linea de cr�dito',
				dataIndex: 'DES_PORCENTAJE',
				width: 117,
				align: 'right',
				resizable: true	,
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			
			//Documentos con errores
			{							
				header : 'Numero de documento',			
				dataIndex : 'NUDOCTO_CERROR',
				width : 235,
				resizable: true	,
				align: 'center'				
			}	,
			{
				header: 'Error',
				dataIndex: 'CERROR',
				width: 352,
				resizable: true	,
				align: 'left'			
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		align: 'center',
		frame: false
	});
	
		//STORES////////////////////////////////////////////////////////////////////////
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMCAMPO'},
		  {name: 'DESC'},
		  {name: 'TIPODATO'},
		  {name: 'LONGITUD'},
		  {name: 'OBLIGATORIO'}
	  ]
	});
	
	//HANDLERS PARA OBJETOS BUTTON/////////////////////////////////////////////////
	var infoLayout = [
		['1','Numero de Distribuidor','Num�rico','8','Si'],
		['2','No de Documento','Num�rico','15','Si'],
		['3','Fecha de Emisi�n del docto.','Fecha','dd/mm/aaaa','Si'],
		['4','Fecha de Vencimiento del docto','Fecha','dd/mm/aaaa','Si'],
		['5','Moneda','Num�rico','3','Si'],
		['6','Monto del docto','Num�rico','14,2','Si'],
		['7','Modalidad de Plazo','Alfanum�rico','3','Si'],	
		['8','Referencia o Comentario','Alfanum�rico','260','No'],
		['9','Campo Adicional 1','Seg�n lo parametrizado por la EPO','Seg�n lo parametrizado por la EPO','No'],
		['10','Campo Adicional 2','Seg�n lo parametrizado por la EPO','Seg�n lo parametrizado por la EPO','No'],
		['11','Campo Adicional 3','Seg�n lo parametrizado por la EPO','Seg�n lo parametrizado por la EPO','No'],
		['12','Campo Adicional 4','Seg�n lo parametrizado por la EPO','Seg�n lo parametrizado por la EPO','No'],
		['13','Campo Adicional 5','Seg�n lo parametrizado por la EPO','Seg�n lo parametrizado por la EPO','No'],
		['14','Negociable ','Alfanum�rico','1','Si']
	];

	storeLayoutData.loadData(infoLayout);
 
	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'No. de Campo',
				dataIndex : 'NUMCAMPO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Descripci�n',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'DESC',
				width : 200,
				sortable : true
			},
			{
				header : 'Tipo de Dato',
				dataIndex : 'TIPODATO',
				width : 200,
				sortable : true
			},
			{
				header : 'Longitud',
				dataIndex : 'LONGITUD',
				width : 200,
				sortable : true
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 100,
				sortable : true,
				align: 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 830,
		height: 360,
		frame: true
	});
	
	
	
		var fpGenerarC = new Ext.Container({
		layout: 'table',
		id: 'fpGenerarC',
		hidden: false,
		layoutConfig: {
			columns: 6
		},
		width:	'100',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
		{
			xtype: 'button',
			text: 'Generar archivo de documentos con error',
			id: 'btnGenerarCSV',
			iconCls: 'icoXls',
			hidden: true
		}, 
		{
			xtype: 'button',
			text: ' Bajar Archivo',
			id: 'btnBajarCSV',
			iconCls: 'icoXls',
			hidden: true
		}	,	
		{
			xtype: 'button',
			text: 'Continuar Carga',
			iconCls: 'icoAceptar',
			id: 'btnContinuarP',			
			hidden: true
		}	,
		{
			xtype: 'button',
			iconCls: 'icoLimpiar',
			text: 'Cancelar',
			id: 'btnCancelar',			
			handler: function() {
				window.location = '24forma02FExt.jsp';
			}
		}	
		
	]
	});
	
	var myValidFn = function(v) {
		var myRegex = /^.+\.([tT][xX][tT])$/;
			return myRegex.test(v);
		}
		Ext.apply(Ext.form.VTypes, {
			archivotxt 		: myValidFn,
			archivotxtText 	: 'El formato del archivo de origen no es el correcto para el tipo de archivo seleccionado'
		});
		
	var elementosFormaCarga = [	
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){						
					
					if (!gridLayout.isVisible()) {
							gridLayout.show();
						}else{
							gridLayout.hide();
						}
					
					}
				},
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 100,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									emptyText: 'Nombre del Archivo',
									fieldLabel: 'Nombre del Archivo',
									name: 'archivoFormalizacion',   
									buttonText: 'Examinar...',
									width: 300,	  
									buttonCfg: {
										iconCls: 'upload-icon'
									},
									anchor: '100%',
									vtype: 'archivotxt'
								}						
							]
						}
					]
				}	
			]
		}	
	];
	
	var fpCarga = new Ext.form.FormPanel({
	  id: 'formaCarga',
		width: 550,
		title: 'Ubicaci�n del archivo de datos',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},		
		items: elementosFormaCarga,			
		monitorValid: true,
		buttons: [
			{
				text: 'Continuar',
				id: 'continuar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:  function(){
					var archivo = Ext.getCmp("archivo");
					if (Ext.isEmpty(archivo.getValue()) ) {
						archivo.markInvalid('El valor de la Ruta es requerida.');	
						return;
					}else {							 
						fpCarga.getForm().submit({
						url: '24forma02file.data.jsp',									
						waitMsg: 'Enviando datos...',
						waitTitle :'Por favor, espere',		
							success: function(form, action) {								
								var resp = action.result;
								var mArchivo =resp.archivo;
								var error_tam = resp.error_tam;																		
								if(mArchivo!='') {	
								
								fpCarga.el.mask('Procesando...', 'x-mask-loading');	
									resultadosCargaData.load({
										params: {
											informacion: 'ValidaCargaArchivo',
											archivo:mArchivo,
											hidCifrasControl:hidCifrasControl,											
											txttotdoc:txttotdoc,
											txtmtodoc:txtmtodoc,
											txtmtomindoc:txtmtomindoc,
											txtmtomaxdoc:txtmtomaxdoc, 											
											txttotdocdo:txttotdocdo,											
											txtmtodocdo:txtmtodocdo, 
											txtmtomindocdo:txtmtomindocdo,
											txtmtomaxdocdo:txtmtomaxdocdo		
										}
									});															
								}	else{
									Ext.MessageBox.alert("Mensaje","El Archivo es muy Grande, excede el L�mite que es de 2 MB.");
									return;
								}
							}
							,failure: NE.util.mostrarSubmitError
						})												
					}	
				}
			}
		]
	});
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [			
			NE.util.getEspaciador(20),
			fpCarga,
			NE.util.getEspaciador(20),
			gridResulCarga,			
			gridTotales,
			gridLayout,
			NE.util.getEspaciador(20)
		]
	});
	
	
	
	
} );