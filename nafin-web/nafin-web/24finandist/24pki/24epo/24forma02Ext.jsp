<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>
<%
try{
	String tipoCarga = (request.getParameter("tipoCarga")!=null)?request.getParameter("tipoCarga"):"M";

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	cargaDocto.operaDistribuidores(iNoCliente);
	String hidCifrasControl ="S";
	try{
		cargaDocto.operaCifrasControl(iNoCliente);
	}catch(NafinException ne){
		hidCifrasControl ="N";	
	}
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>

<% if(hidCifrasControl.equals("S")){ %>
<script type="text/javascript" src="24forma02Ext.js?<%=session.getId()%>"></script>
<%}else if(hidCifrasControl.equals("N")){  %>
<% response.sendRedirect("24forma02aExt.jsp?hidCifrasControl=N&tipoCarga=M"); %>
<%} %>
<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="../certificado.jspf" %>
<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>

<form id='formParametros' name="formParametros">
	<input type="hidden" id="hidCifrasControl" name="hidCifrasControl" value="<%=hidCifrasControl%>"/>	
	<input type="hidden" id="tipoCarga" name="tipoCarga" value="<%=tipoCarga%>"/>	
		
</form>

</body>
</html>
<%
}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	out.println(" Error: "+e);
}
%>