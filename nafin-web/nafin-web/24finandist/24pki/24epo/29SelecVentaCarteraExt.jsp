<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>
<%

	//INSTANCIACION DE EJB'S
	AceptacionPyme aceptPyme = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);

	ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

	aceptPyme.cambiaTipoFinanciamiento(iNoEPO);
	String parTipoCambio = aceptPyme.getTipoCambio(iNoEPO);
	String  diasInhabiles = aceptPyme.getDiasInhabiles("",iNoEPO);

	String	descuentoAutomatico = BeanParametro.DesAutomaticoEpo(iNoEPO,"PUB_EPO_DESC_AUTOMATICO"); 
	String	ventaCartera =  BeanParametro.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); 
	String	tipoCredito= BeanParametro.obtieneTipoCredito (iNoEPO);
	String	lineaCredito = cargaDocto.validaMontolineaCredito(iNoCliente);


	String fechaHoy		= "";
	try{
		fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	}catch(Exception e){
		fechaHoy = "";
	}
	%> 
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/00utils/NEcesionDerechos.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="29SelecVentaCarteraExt.js?<%=session.getId()%>"></script>
<%-- El jsp que contiene el dise?o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
<form id='formParametros' name="formParametros">
	<input type="hidden" id="fechaHoy" name="fechaHoy" value="<%=fechaHoy%>"/>
	<input type="hidden" id="diasInhabiles" name="diasInhabiles" value="<%=diasInhabiles%>"/>	
	<input type="hidden" id="parTipoCambio" name="parTipoCambio" value="<%=parTipoCambio%>"/>
	<input type="hidden" id="descuentoAutomatico" name="descuentoAutomatico" value="<%=descuentoAutomatico%>"/>
	<input type="hidden" id="ventaCartera" name="ventaCartera" value="<%=ventaCartera%>"/>
	<input type="hidden" id="tipoCredito" name="tipoCredito" value="<%=tipoCredito%>"/>
	<input type="hidden" id="lineaCredito" name="lineaCredito" value="<%=lineaCredito%>"/>
	
</form>
<form id='formAux' name="formAux" target='_new'></form>

</body>
</html>