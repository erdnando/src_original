<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>
<%

	String txttotdoc  = (request.getParameter("txttotdoc") != null) ? request.getParameter("txttotdoc") : "0";
	String txtmtodoc  = (request.getParameter("txtmtodoc") != null) ? request.getParameter("txtmtodoc") : "0";
	String txtmtomindoc  = (request.getParameter("txtmtomindoc") != null) ? request.getParameter("txtmtomindoc") : "0";
	String txtmtomaxdoc  = (request.getParameter("txtmtomaxdoc") != null) ? request.getParameter("txtmtomaxdoc") : "0";
	
	String txttotdocdo  = (request.getParameter("txttotdocdo") != null) ? request.getParameter("txttotdocdo") : "0";
	String txtmtodocdo  = (request.getParameter("txtmtodocdo") != null) ? request.getParameter("txtmtodocdo") : "0";
	String txtmtomindocdo  = (request.getParameter("txtmtomindocdo") != null) ? request.getParameter("txtmtomindocdo") : "0";
	String txtmtomaxdocdo  = (request.getParameter("txtmtomaxdocdo") != null) ? request.getParameter("txtmtomaxdocdo") : "0";
	
	String hidCifrasControl = (request.getParameter("hidCifrasControl")!=null)?request.getParameter("hidCifrasControl"):"";
	String tipoCarga = (request.getParameter("tipoCarga")!=null)?request.getParameter("tipoCarga"):"";
 	String pantalla = (request.getParameter("pantalla")!=null)?request.getParameter("pantalla"):"";
	String proceso = (request.getParameter("proceso")!=null)?request.getParameter("proceso"):"";
	
try{

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	cargaDocto.operaDistribuidores(iNoCliente);

	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
	String descuentoAutomatico =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_DESC_AUTOMATICO");
	String limiteLineaCredito =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_LIMITE_LINEA_CREDITO");//Fodea 029-2010 Distribuodores Fase III
	String tipoCredito =  BeanParametros.obtieneTipoCredito (iNoCliente); //Fodea 029-2010 Distribuodores Fase III
	String NOnegociable =  BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_NO_NEGOCIABLES"); //Fodea 029-2010 Distribuodores Fase III
	String ventaCartera =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); //Fodea 029-2010 Distribuodores Fase III
	String operaVentaCartera ="", lineaCredito="";
	if(ventaCartera.equals("S")) {  	operaVentaCartera = "S";    }
	String  validaLinea = cargaDocto.validaLineCredito(iNoCliente);
	
	
	if(!validaLinea.equals("")){
	lineaCredito = cargaDocto.montoLineaCreCaptura(iNoCliente); //Fodea 029-2010 Distribuidores Fase III
	}
	
	String hidFechaActual=(new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
	String hid_fecha_porc_desc = cargaDocto.getParamFechaPorc(iNoCliente);
	
	//para ver si hay campos adicionales 
	Vector nombresCampo = cargaDocto.getCamposAdicionales(iNoCliente,false);
	String campos ="N", noCampos ="0";
	if(nombresCampo.size()>0){
		campos ="S";
		noCampos = String.valueOf(nombresCampo.size());	
	}
	
	String  mensajeEPO  ="EPO";
	if(application.getInitParameter("EpoCemex").equals(iNoCliente)){
		mensajeEPO ="EpoCemex";
	}

%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>

<script type="text/javascript" src="24forma02FaExt.js?<%=session.getId()%>"></script>

<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="../certificado.jspf" %>

<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
<form id='formParametros' name="formParametros">	
	<input type="hidden" id="ventaCartera" name="ventaCartera" value="<%=ventaCartera%>"/>	
	<input type="hidden" id="validaLinea" name="validaLinea" value="<%=validaLinea%>"/>
	<input type="hidden" id="lineaCredito" name="lineaCredito" value="<%=lineaCredito%>"/>
	
	<input type="hidden" id="descuentoAutomatico" name="descuentoAutomatico" value="<%=descuentoAutomatico%>"/>
	<input type="hidden" id="limiteLineaCredito" name="limiteLineaCredito" value="<%=limiteLineaCredito%>"/>
	<input type="hidden" id="tipoCredito" name="tipoCredito" value="<%=tipoCredito%>"/>
	<input type="hidden" id="NOnegociable" name="NOnegociable" value="<%=NOnegociable%>"/>
	<input type="hidden" id="ventaCartera" name="ventaCartera" value="<%=ventaCartera%>"/>
	
	<input type="hidden" id="txttotdoc" name="txttotdoc" value="<%=txttotdoc%>"/>
	<input type="hidden" id="txtmtodoc" name="txtmtodoc" value="<%=txtmtodoc%>"/>
	<input type="hidden" id="txtmtomindoc" name="txtmtomindoc" value="<%=txtmtomindoc%>"/>
	<input type="hidden" id="txtmtomaxdoc" name="txtmtomaxdoc" value="<%=txtmtomaxdoc%>"/>
	
	<input type="hidden" id="txttotdocdo" name="txttotdocdo" value="<%=txttotdocdo%>"/>
	<input type="hidden" id="txtmtodocdo" name="txtmtodocdo" value="<%=txtmtodocdo%>"/>
	<input type="hidden" id="txtmtomindocdo" name="txtmtomindocdo" value="<%=txtmtomindocdo%>"/>
	<input type="hidden" id="txtmtomaxdocdo" name="txtmtomaxdocdo" value="<%=txtmtomaxdocdo%>"/>
	
	<input type="hidden" id="hidCifrasControl" name="hidCifrasControl" value="<%=hidCifrasControl%>"/>
	<input type="hidden" id="hidFechaActual" name="hidFechaActual" value="<%=hidFechaActual%>"/>
	<input type="hidden" id="hid_fecha_porc_desc" name="hid_fecha_porc_desc" value="<%=hid_fecha_porc_desc%>"/>
	<input type="hidden" id="campos" name="campos" value="<%=campos%>"/>
	<input type="hidden" id="noCampos" name="noCampos" value="<%=noCampos%>"/>
		
	<input type="hidden" id="proceso" name="proceso" value="<%=proceso%>"/>
	<input type="hidden" id="mensajeEPO" name="mensajeEPO" value="<%=mensajeEPO%>"/>
	<input type="hidden" id="tipoCarga" name="tipoCarga" value="<%=tipoCarga%>"/>
	
</form>

</body>
</html>
<%
}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	out.println(" Error: "+e);
}
%>