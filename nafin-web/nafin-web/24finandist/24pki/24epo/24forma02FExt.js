
Ext.onReady(function() {
	
		var tipoCarga = Ext.getDom('tipoCarga').value;
	
		var enviar = function() {
		
		var txttotdoc = Ext.getCmp("txttotdoc1");
		var txtmtodoc = Ext.getCmp("txtmtodoc1");
		var txtmtomindoc = Ext.getCmp("txtmtomindoc1");
		var txtmtomaxdoc  = Ext.getCmp("txtmtomaxdoc1");
		
		var txttotdocdo = Ext.getCmp("txttotdocdol");
		var txtmtodocdo = Ext.getCmp("txtmtodocdol");
		var txtmtomindocdo = Ext.getCmp("txtmtomindocdo2");
		var txtmtomaxdocdo  = Ext.getCmp("txtmtomaxdocdo2");
		
		Ext.getCmp("txttotdoc1").setValue(eliminaFormatoFlotante(txttotdoc.getValue()));
		Ext.getCmp("txtmtodoc1").setValue(eliminaFormatoFlotante(txtmtodoc.getValue()));
		Ext.getCmp("txtmtomindoc1").setValue(eliminaFormatoFlotante(txtmtomindoc.getValue()));
		Ext.getCmp("txtmtomaxdoc1").setValue(eliminaFormatoFlotante(txtmtomaxdoc.getValue()));
		
		Ext.getCmp("txttotdocdol").setValue(eliminaFormatoFlotante(txttotdocdo.getValue()));
		Ext.getCmp("txtmtodocdol").setValue(eliminaFormatoFlotante(txtmtodocdo.getValue()));
		Ext.getCmp("txtmtomindocdo2").setValue(eliminaFormatoFlotante(txtmtomindocdo.getValue()));
		Ext.getCmp("txtmtomaxdocdo2").setValue(eliminaFormatoFlotante(txtmtomaxdocdo.getValue()));
	
				
		var continua = true;
				
		if (Ext.isEmpty(txttotdoc.getValue())   && Ext.isEmpty(txtmtodoc.getValue())  
		&&  Ext.isEmpty(txttotdocdo.getValue()) &&  Ext.isEmpty(txtmtodocdo.getValue()) 
		) {
			Ext.MessageBox.alert("Mensaje","Por favor escriba el  n�mero total documentos en Moneda Nacional y/o D�lares");
			continua = false;
			return;
		}		
		//moneda Nacional
		if(!Ext.isEmpty(txttotdoc.getValue())  ) {
		
			if(Ext.isEmpty(txtmtodoc.getValue()) || txtmtodoc.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomindoc.getValue()) || txtmtomindoc.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomaxdoc.getValue()) || txtmtomaxdoc.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
				continua = false;
				return;
			}
			
			if((parseFloat(txtmtodoc.getValue()) < parseFloat(txtmtomaxdoc.getValue())) || (parseFloat(txtmtomaxdoc.getValue()) < parseFloat(txtmtomindoc.getValue())) || (parseFloat(txtmtodoc.getValue()) < parseFloat(txtmtomindoc.getValue()))) {
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Moneda Nacional)");
				continua = false;
				return;			
			}
		
		}
		//Dolares
		if(!Ext.isEmpty(txttotdocdo.getValue())  ) {	
		
			if(Ext.isEmpty(txtmtodocdo.getValue()) || txtmtodocdo.getValue()==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomindocdo.getValue()) || txtmtomindocdo.getValue()==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomaxdocdo.getValue()) || txtmtomaxdocdo.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
				continua = false;
				return;
			}
			
			if((parseFloat(txtmtodocdo.getValue()) < parseFloat(txtmtomaxdocdo.getValue())) || (parseFloat(txtmtomaxdocdo.getValue()) < parseFloat(txtmtomindocdo.getValue())) || (parseFloat(txtmtodocdo.getValue()) < parseFloat(txtmtomindocdo.getValue()))) {
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (D�lares)");
				continua = false;
				return;
			}
		}	
		
		var hidCifrasControl ='S';		
		
		if (Ext.isEmpty(txttotdoc.getValue()) ) {  	Ext.getCmp("txttotdoc1").setValue(0);  }
		if (Ext.isEmpty(txtmtodoc.getValue()) ) {  	Ext.getCmp("txtmtodoc1").setValue(0); }
		if (Ext.isEmpty(txtmtomindoc.getValue()) ) {  	Ext.getCmp("txtmtomindoc1").setValue(0); }
		if (Ext.isEmpty(txtmtomaxdoc.getValue()) ) {  	Ext.getCmp("txtmtomaxdoc1").setValue(0); }
		
		if (Ext.isEmpty(txttotdocdo.getValue()) ) {  	Ext.getCmp("txttotdocdol").setValue(0); }
		if (Ext.isEmpty(txtmtodocdo.getValue()) ) {  	Ext.getCmp("txtmtodocdol").setValue(0); }
		if (Ext.isEmpty(txtmtomindocdo.getValue()) ) {  	Ext.getCmp("txtmtomindocdo2").setValue(0); }
		if (Ext.isEmpty(txtmtomaxdocdo.getValue()) ) {  	Ext.getCmp("txtmtomaxdocdo2").setValue(0); }
		
		var parametros = "txttotdoc="+txttotdoc.getValue()+"&txtmtodoc="+txtmtodoc.getValue()+"&txtmtomindoc="+txtmtomindoc.getValue()+"&txtmtomaxdoc="+txtmtomaxdoc.getValue()+"&txttotdocdo="+txttotdocdo.getValue()+"&txtmtodocdo="+txtmtodocdo.getValue()+"&txtmtomindocdo="+txtmtomindocdo.getValue()+"&txtmtomaxdocdo="+txtmtomaxdocdo.getValue()+"&hidCifrasControl="+hidCifrasControl+"&tipoCarga="+tipoCarga;
			
		if(continua ==true) {
			document.location.href  = "24forma02FaExt.jsp?"+parametros;	
		}
	}
	
	var elementosForma = [	
		{ 
				xtype:   'label',  
				html:		'Moneda Nacional', 
				cls:		'x-form-item', 
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				} 
			},		
			{
				xtype: 'numberfield',
				name: 'txttotdoc',
				id: 'txttotdoc1',
				fieldLabel: 'No. total de documento',
				allowBlank: true,
				startDay: 0,
				maxLength: 7,	//ver el tama�o maximo del numero en BD para colocar este igual
				width: 200,
				msgTarget: 'side',						
				margins: '0 20 0 0'		
		},
		{
			xtype: 'textfield',
			name: 'txtmtodoc',
			id: 'txtmtodoc1',
			fieldLabel: 'Monto total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomindoc',
			id: 'txtmtomindoc1',
			fieldLabel: 'Monto m�nimo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomaxdoc',
			id: 'txtmtomaxdoc1',
			fieldLabel: 'Monto m�ximo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		}	,
		{ 
			xtype:   'label',  
			html:		'D�lares', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 'numberfield',
			name: 'txttotdocdo',
			id: 'txttotdocdol',
			fieldLabel: 'No. total de documento',
			allowBlank: true,
			startDay: 0,
			maxLength: 7,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0'		
		}	,
		{
			xtype: 'textfield',
			name: 'txtmtodocdo',
			id: 'txtmtodocdol',
			fieldLabel: 'Monto total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		}	,
		{
			xtype: 'textfield',
			name: 'txtmtomindocdo',
			id: 'txtmtomindocdo2',
			fieldLabel: 'Monto m�nimo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		}	,
		{
			xtype: 'textfield',
			name: 'txtmtomaxdocdo',
			id: 'txtmtomaxdocdo2',
			fieldLabel: 'Monto m�ximo de los documentos:',
			allowBlank: true,
			startDay: 0,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		}			
	];
	var fpCifras = new Ext.form.FormPanel({
		id: 'fpCifras',
		width: 500,
		title: '',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Enviar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: enviar
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24forma02FExt.jsp';
				}
			}
		]
	})

	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fpCifras				
		]
	});
	
	

	
	
} );