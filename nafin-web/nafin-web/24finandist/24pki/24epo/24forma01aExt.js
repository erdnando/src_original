
Ext.onReady(function() {
	
	//Variables globales
	var ventaCartera  = Ext.getDom('ventaCartera').value;
	var validaLinea  = Ext.getDom('validaLinea').value;
	var lineaCredito = Ext.getDom('lineaCredito').value;
	var descuentoAutomatico = Ext.getDom('descuentoAutomatico').value;
	var limiteLineaCredito = Ext.getDom('limiteLineaCredito').value;
	var tipoCredito = Ext.getDom('tipoCredito').value;
	var NOnegociable = Ext.getDom('NOnegociable').value;	
	var hidFechaActual = Ext.getDom('hidFechaActual').value; //fecha hoy
	var hid_fecha_porc_desc = Ext.getDom('hid_fecha_porc_desc').value;
	var txttotdoc = Ext.getDom('txttotdoc').value;
	var txtmtodoc = Ext.getDom('txtmtodoc').value;
	var txtmtomindoc = Ext.getDom('txtmtomindoc').value;
	var txtmtomaxdoc = Ext.getDom('txtmtomaxdoc').value;
	var txttotdocdo = Ext.getDom('txttotdocdo').value;
	var txtmtodocdo = Ext.getDom('txtmtodocdo').value;
	var txtmtomindocdo = Ext.getDom('txtmtomindocdo').value;
	var txtmtomaxdocdo = Ext.getDom('txtmtomaxdocdo').value;
	var hidCifrasControl = Ext.getDom('hidCifrasControl').value;
	var campos = Ext.getDom('campos').value;
	var proceso = Ext.getDom('proceso').value;
	var noCampos = Ext.getDom('noCampos').value;
	var tipoCarga = Ext.getDom('tipoCarga').value;
	var strPerfil = Ext.getDom('strPerfil').value;
	 
	var fechaLimite; 
	var	numRegistrosMN=0;
	var	numRegistrosDL=0;
	var hidMontoTotalMN=0;
	var hidMontoTotalDL=0;
	var hidID;
	var numElementos =0;
		
	if(ventaCartera=='S' && validaLinea==''){
		Ext.MessageBox.alert("Mensaje","La Epo no tiene linea de Credito");
	}

	//valida la fecha Limite al insertar el plazo_descuento
	function calculaFechaLimite(){
		var ig_plazo_descuento = Ext.getCmp("ig_plazo_descuento1");
		if(ig_plazo_descuento.getValue()==''  ){
			ig_plazo_descuento.markInvalid('Para poder calcular la Fecha Limite para tener Descuento\nes necesario que introduzca el Plazo para Descuento');
			return;
		}	
		var df_fecha_emision = Ext.getCmp("df_fecha_emision1");
		var fechaAuto = Ext.util.Format.date(df_fecha_emision.getValue(),'d/m/Y');	
		
		if(hid_fecha_porc_desc=="P"){
			fechaAuto = hidFechaActual;
		}		
		var TDate = new Date();
		//alert(fechaAuto);
		var dia_aux = parseInt(fechaAuto.substring(0,2),10);
		TDate.setDate(dia_aux);
		TDate.setMonth(parseInt(fechaAuto.substring(3,5),10)-1);
		TDate.setYear(parseInt(fechaAuto.substring(6,10),10));
		TDate.setDate(TDate.getDate()+parseInt(ig_plazo_descuento.getValue(),10));		
		
		var CurYear = TDate.getYear();
		var CurDay = TDate.getDate();
		var CurMonth = TDate.getMonth()+1;
		if(CurDay<10)
			CurDay = '0'+CurDay;
		if(CurMonth<10)
			CurMonth = '0'+CurMonth;
		TheDate = CurDay +'/'+CurMonth+'/'+CurYear;
		
		if (TheDate=="NaN/NaN/NaN"){    
			Ext.getCmp("txt_fecha_limite_dsc1").setValue(hidFechaActual);		
		}else {						
			Ext.getCmp("txt_fecha_limite_dsc1").setValue(TDate);
		}		
	}
	
	
	//valida la fecha limite de descuento al introducirla y calcula el Plazo para descuento
	function validaFechaLim(){
		var f = document.frmContenido;
		var fechaAuto = "";
		
		var df_fecha_emision = Ext.getCmp("df_fecha_emision1");
		var txt_fecha_limite_dsc = Ext.getCmp("txt_fecha_limite_dsc1");
		var ig_plazo_descuento = Ext.getCmp("ig_plazo_descuento1");
		var fEmision = Ext.util.Format.date(df_fecha_emision.getValue(),'d/m/Y');
		var flimitedsc = Ext.util.Format.date(txt_fecha_limite_dsc.getValue(),'d/m/Y');
							
		if(hid_fecha_porc_desc=="E")
			fechaAuto = fEmision;  
		else
			if(hid_fecha_porc_desc=="P")
				fechaAuto = hidFechaActual;
		
			if(df_fecha_emision.getValue()!= ''){
				if(datecomp( fEmision , flimitedsc)==2){
					var TDateEmi = new Date();
					var TDateLim = new Date();
					TDateEmi.setDate(parseInt(fechaAuto.substring(0,2),10));
					TDateEmi.setMonth(parseInt(fechaAuto.substring(3,5),10)-1);
					TDateEmi.setYear(parseInt(fechaAuto.substring(6,10),10));
					TDateLim.setDate(parseInt(flimitedsc.substring(0,2),10));
					TDateLim.setMonth(parseInt(flimitedsc.substring(3,5),10)-1);
					TDateLim.setYear(parseInt(flimitedsc.substring(6,10),10));
					var resta = (TDateLim.getTime()-TDateEmi.getTime())/86400000;										
					Ext.getCmp("ig_plazo_descuento1").setValue(Math.round(parseFloat(resta)));	
				}	else {						
						if(datecomp(fEmision , flimitedsc)==0){
						Ext.getCmp("ig_plazo_descuento1").setValue(1);
					}	else {
							txt_fecha_limite_dsc.markInvalid('Revise la Fecha Limite para Descuento\nya que debe ser mayor o igual a hoy.');
						return;
					}
				}
			}	
			
			if(df_fecha_emision.getValue()== '' && txt_fecha_limite_dsc.getValue()!= ''){
				txt_fecha_limite_dsc.markInvalid('Para poder calcular la Fecha Limite para tener Descuento\nes necesario que introduzca la Fecha de Emisi�n');
				Ext.getCmp("txt_fecha_limite_dsc1").setValue('');
				return;
			}	
		
	}
	
	//esta es para calcular la fecja de Limite de descuento 
	//cuando el plazo ya esta captura  y la fehca Emision
	function validaFechaEmision(){
		var ig_plazo_descuento = Ext.getCmp("ig_plazo_descuento1");
		var df_fecha_emision = Ext.getCmp("df_fecha_emision1");
		var fEmision = Ext.util.Format.date(df_fecha_emision.getValue(),'d/m/Y');
		
		var ig_plazo_descuento = Ext.getCmp("ig_plazo_descuento1");
		if(ig_plazo_descuento.getValue()!='') {
				calculaFechaLimite();		
			}
	}

	function validaPlazo(){
		var df_fecha_emision = Ext.getCmp("df_fecha_emision1");
		var fEmision = Ext.util.Format.date(df_fecha_emision.getValue(),'d/m/Y');	
		var ig_plazo_descuento = Ext.getCmp("ig_plazo_descuento1");
		if(ig_plazo_descuento.getValue()!='') {
			calculaFechaLimite();
		}
	}

	function validaPorcentaje()	{
		var fn_porc_descuento = Ext.getCmp("fn_porc_descuento1");
		if(parseFloat(fn_porc_descuento.getValue())<0){ 
			fn_porc_descuento.markInvalid("El porcentaje debe ser mayor o igual a cero");		
			return;
		}
		if(parseFloat(fn_porc_descuento.getValue())>100){ 		
			fn_porc_descuento.markInvalid("El porcentaje debe ser menor a cien");			
			return;
		}	
	}
	
	function validaMonto()	{
		var fn_monto = Ext.getCmp("fn_monto1");
	
		 if(parseFloat(fn_monto.getValue())<=0){
        fn_monto.markInvalid("El monto debe ser mayor a cero");        
        return;
      }  
			var porcentaje = (fn_monto.getValue() *100)/lineaCredito;
			var restaporcentaje = 100-porcentaje;
			var valor =  parseFloat(porcentaje).toFixed(2);
			var valor2 =   parseFloat(restaporcentaje).toFixed(2);
			
      if ((descuentoAutomatico=="S" &&  limiteLineaCredito=="S" &&  tipoCredito =="C" ) || (descuentoAutomatico=="S" &&  limiteLineaCredito=="S" &&  tipoCredito =="D" && ventaCartera =="S")) {       
			  if (parseFloat(fn_monto.getValue()) > lineaCredito){
           fn_monto.markInvalid("El monto capturado rebasa el l�mite de su l�nea de cr�dito. Porcentaje Utilizado "+valor+" %");
           return ;    
        }else{
				   fn_monto.markInvalid("Porcentaje disponible de la linea de cr�dito  "+valor2+ " %");        
				}
    }   
}

	
//////////////INICIA VALIDACIONES JAVA SCRIPT //////////////////////////

	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
	
	var procesaPymeBloqXNumProv = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);	
			if(resp.pymeBloqueada){
				Ext.getCmp("cg_num_distribuidor1").markInvalid('Pyme Bloqueada');
				Ext.getCmp('numDistValidHid1').setValue('N');
			}
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
		
	var procesaPymeBloqueada = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);	
			if(resp.pymeBloqueada){
				Ext.getCmp("ic_pyme1").markInvalid('Pyme Bloqueada');
				Ext.getCmp('cvePymeValidHid1').setValue('N');
			}
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//termina proceso de captura 
	function procesarTerminaCaptura(opts, success, response) {	
		var  gridCaptura = Ext.getCmp('gridCaptura');
		var store = gridCaptura.getStore();
		var ic_moneda;
		var hidNumDoctosActualesMN =0;
		var hidNumDoctosActualesDL =0; 
		var hidMontoTotalMN = 0;
		var hidMontoTotalDL =0;
		var hidCtrlNumdocumentosMN = txttotdoc;
		var hidCtrlNumdocumentosDL =txttotdocdo;
		var hidCtrlMontoTotalMN =txtmtodoc;
		var hidCtrlMontoTotalDL =txtmtodocdo;
		
		store.each(function(record) {
		 if(record.data['IC_MONEDA']==1){
			hidNumDoctosActualesMN ++;
			hidMontoTotalMN +=parseFloat(record.data['MONTO'],10);			
		 }
		 if(record.data['IC_MONEDA']==54){
			hidNumDoctosActualesDL ++;
			hidMontoTotalDL +=parseFloat(record.data['MONTO'],10);
		}
	
	});
	
	if( 'S'==hidCifrasControl){		
		if(!(parseInt(hidNumDoctosActualesMN,10)==parseInt(hidCtrlNumdocumentosMN,10)
			&& parseInt(hidNumDoctosActualesDL,10)==parseInt(hidCtrlNumdocumentosDL,10)
			&& parseFloat(hidMontoTotalMN,10)==parseFloat(hidCtrlMontoTotalMN,10)
			&& parseFloat(hidMontoTotalDL,10)==parseFloat(hidCtrlMontoTotalDL,10))){
			Ext.MessageBox.alert("Mensaje","Los datos capturados no concuerdan con los de control."+"\nNumero Documentos M.N.: "+hidNumDoctosActualesMN+ " debe ser "+ hidCtrlNumdocumentosMN+	"\nNumero Documentos D�lares: "+hidNumDoctosActualesDL+ " debe ser "+hidCtrlNumdocumentosDL+"\nMonto Total M.N.: "+hidMontoTotalMN+ " debe ser "+hidCtrlMontoTotalMN+	"\nMonto Total D�lares.: "+hidMontoTotalDL+ " debe ser "+hidCtrlMontoTotalDL);
				return;
		}		
	}
	
		var parametros = "pantalla=PreAcuse"+"&proceso="+proceso+"&tipoCarga="+tipoCarga;			
		document.location.href = "24forma01PreAExt.jsp?"+parametros;	
	
	}
	//cancelar la captura de documentos
	function procesarCancelar(opts, success, response) {
		if(!confirm("�Est� seguro de querer cancelar la operaci�n?")) {
			return;
		}else {
			document.location.href  = "24forma01Ext.jsp";	
		}	
	}

	
	//Elimina el registro 
	var procesarSuccessFailureBorrar =  function(opts, success, response) {		
		var gridCaptura = Ext.getCmp('gridCaptura');
		gridCaptura.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			gridCaptura.getStore().commitChanges();
			Ext.MessageBox.alert('Mensaje','Registro borrado exitosamente.');
			
			fpCarga.el.mask('Cargando Consulta ...', 'x-mask-loading');	
			
			consultaData.load({  	
				params: { 		
					informacion: 'Consulta', 		
					proceso:proceso,
					pantalla:'Captura'
				}  		
			});	
				
			totalesData.load({ 		
				params: { 		
					informacion: 'ResumenTotales', 
					proceso:proceso,
					pantalla:'Captura'
				} 		
			});
				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Elimina el registro 
	var procesaAccionBorrar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var hidID = registro.get('CONSECUTIVO');
		var gridCaptura = Ext.getCmp('gridCaptura');
		var store = gridCaptura.getStore();
	
		if(!confirm("�Esta seguro de querer eliminar el registro?")) {
			return;
		}else {
			gridCaptura.el.mask('Actualizando...', 'x-mask-loading');
			gridCaptura.stopEditing();
			Ext.Ajax.request({
				url : '24forma01Ext.data.jsp',
				params : {
					informacion: 'Eliminar',
					proceso:proceso,
					hidID:hidID	
				},
				callback: procesarSuccessFailureBorrar
			});	
		}
	}
	
	var procesarSuccessFailureModificar =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var datos = Ext.util.JSON.decode(response.responseText);
			var mod_ic_pyme =datos.mod_ic_pyme;
			var mod_ig_numero_docto =datos.mod_ig_numero_docto;			
			var mod_df_fecha_emision =datos.mod_df_fecha_emision;	
			var mod_df_fecha_venc =datos.mod_df_fecha_venc;	
			var mod_ic_moneda =datos.mod_ic_moneda;	
			var mod_fn_monto =datos.mod_fn_monto;	
			var mod_ct_referencia =datos.mod_ct_referencia;	
			var mod_ig_plazo_descuento =datos.mod_ig_plazo_descuento;	
			var mod_fn_porc_descuento =datos.mod_fn_porc_descuento;				
			var mod_ic_tipo_financiamiento =datos.mod_ic_tipo_financiamiento;	
			var mod_categoria =datos.mod_categoria;		
			var hidID =datos.hidID;					
			proceso = datos.proceso;	
			
			Ext.getCmp("fn_monto1").setValue(mod_fn_monto);
			Ext.getCmp("cg_num_distribuidor1").setValue(mod_ic_pyme);
			Ext.getCmp("ig_numero_docto1").setValue(mod_ig_numero_docto);
			Ext.getCmp("df_fecha_emision1").setValue(mod_df_fecha_emision);
			Ext.getCmp("df_fecha_venc1").setValue(mod_df_fecha_venc);		
			Ext.getCmp("fn_porc_descuento1").setValue(mod_fn_porc_descuento);
			Ext.getCmp("ig_plazo_descuento1").setValue(mod_ig_plazo_descuento);	
			
			Ext.getCmp("cmb_categoria1").setValue(mod_categoria);	
			Ext.getCmp("ic_pyme1").setValue(mod_ic_pyme);	
			Ext.getCmp("ic_tipo_financiamiento1").setValue(mod_ic_tipo_financiamiento);	
			Ext.getCmp("ic_moneda1").setValue(mod_ic_moneda);	
			Ext.getCmp("ct_referencia1").setValue(mod_ct_referencia);	
							
		}
		consultaData.load({  	
				params: { 		
					informacion: 'Consulta', 		
					proceso:proceso,
					pantalla:'Captura'
				}  		
			});	
			
	}
	
	var procesaAccionModificar = function(grid, rowIndex, colIndex, item, event) {
	
		var registro = grid.getStore().getAt(rowIndex);
		var hidID = registro.get('CONSECUTIVO');
		
		Ext.Ajax.request({
			url: '24forma01Ext.data.jsp',
			params: {
				informacion: "Modificar",
				proceso:proceso,
				hidID:hidID	
			},
			callback: procesarSuccessFailureModificar
		});
			
	}
	
	var procesarSuccessFailureGuardar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var datos = Ext.util.JSON.decode(response.responseText);
			proceso =datos.proceso;
			var msgError =datos.msgError;				
			
			var fp = Ext.getCmp('forma');		
			var forma = fp.getForm();
			
			if(msgError!=''){
				Ext.MessageBox.alert("Mensaje",msgError);	
			}else if(	msgError==''){
				
				Ext.getCmp("ic_tipo_financiamiento1").setValue(''); 
				Ext.getCmp("ic_tipo_Pago1").setValue('');
				Ext.getCmp("ic_pediodo1").setValue('');				
				
				
				fp.el.mask('Cargando Consulta ...', 'x-mask-loading');	
				
				consultaData.load({ 	
					params: {  	
						informacion: 'Consulta',		
						proceso:proceso,
						pantalla:'Captura'
					} 	
				});
					
				totalesData.load({  	
					params: { 	
						informacion: 'ResumenTotales', 	
						proceso:proceso,
						pantalla:'Captura'
					} 	
				});
				
				var btnTerminaCap = Ext.getCmp('btnTerminaCap');	
				var btnCancelar = Ext.getCmp('btnCancelar');
				gridTotales.show();					
				fpConfirmar.show();
				btnTerminaCap.show();
				btnCancelar.show();
				
				
			//esta parte es para limpiar los componentes de la forma cuando se agregan 
				Ext.getCmp("fn_monto1").setValue('');
				Ext.getCmp("cg_num_distribuidor1").setValue('');
				Ext.getCmp("ig_numero_docto1").setValue('');
				Ext.getCmp("df_fecha_emision1").setValue('');
				Ext.getCmp("df_fecha_venc1").setValue('');		
				Ext.getCmp("fn_porc_descuento1").setValue('');
				Ext.getCmp("ig_plazo_descuento1").setValue('');	
					Ext.getCmp("cg_num_distribuidor1").setValue('');	
				Ext.getCmp("cmb_categoria1").setValue('');	
				Ext.getCmp("ic_pyme1").setValue('');	
				Ext.getCmp("ic_tipo_financiamiento1").setValue('');	
				Ext.getCmp("ic_moneda1").setValue('');	
				Ext.getCmp("ct_referencia1").setValue('');	
				Ext.getCmp("txt_fecha_limite_dsc1").setValue('');	
				if(campos=='S'){
					for(var i= 1; i<=noCampos; i++) {
						if(i==1){		Ext.getCmp("cg_campo1").setValue('');	}	
						if(i==2){		Ext.getCmp("cg_campo2").setValue('');	}
						if(i==3){		Ext.getCmp("cg_campo3").setValue('');	}
						if(i==4){		Ext.getCmp("cg_campo4").setValue('');	}
						if(i==5){		Ext.getCmp("cg_campo5").setValue('');	}
					}
				}			
			
			}	
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarSuccessFailureValida =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var datos = Ext.util.JSON.decode(response.responseText);
			var msgError =datos.msgError;		
			var fp = Ext.getCmp('forma');		
			if(msgError==''){
				Ext.Ajax.request({
					url : '24forma01Ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion:'Captura',
						proceso:proceso,						
						hidID:hidID					
					}),
					callback: procesarSuccessFailureGuardar
				});		
			
			}else {
				Ext.MessageBox.alert("Mensaje",msgError);				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
		

	
	//combo de categoria
	function procesaValoresCategoria(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			var plazo = jsonValoresIniciales.mod_ig_plazo_descuento 
			var porce = jsonValoresIniciales.mod_fn_porc_descuento 
			var fecha = jsonValoresIniciales.mod_fecha_limite_dsc 
		
			Ext.getCmp("ig_plazo_descuento1").setValue(plazo);
			Ext.getCmp("fn_porc_descuento1").setValue(porce);
			Ext.getCmp("txt_fecha_limite_dsc1").setValue(fecha);
			
			/*if(fecha==''){
				var  df_fecha_emision = Ext.getCmp("df_fecha_emision1");
				var fechaAuto1 ="";//Ext.util.Format.date(df_fecha_emision,'d/m/Y'); 
				var fechaLim; 
		
				
				if(hid_fecha_porc_desc=="P"){
				 fechaAuto1 = hidFechaActual;	
				 }
				var TDate = new Date();
				var dia_aux = parseInt(fechaAuto1.substring(0,2),10);
				TDate.setDate(dia_aux);
				TDate.setMonth(parseInt(fechaAuto1.substring(3,5),10)-1);
				TDate.setYear(parseInt(fechaAuto1.substring(6,10),10));
				TDate.setDate(TDate.getDate()+parseInt(plazo,10));
				var CurYear = TDate.getYear();
				var CurDay = TDate.getDate();
				var CurMonth = TDate.getMonth()+1;
				if(CurDay<10)
					CurDay = '0'+CurDay;
				if(CurMonth<10)
					CurMonth = '0'+CurMonth;
				TheDate = CurDay +'/'+CurMonth+'/'+CurYear;			
				if (TheDate=="NaN/NaN/NaN"){
					fechaLim=hidFechaActual;
				}else {
					fechaLim = TheDate;
				}		
				alert(fechaLim);
				
				Ext.getCmp("txt_fecha_limite_dsc1").setValue(fechaLim);
			}	
			*/
			fpCarga.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

			
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;			
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
					
		if (arrRegistros != null) {
			if (!gridCaptura.isVisible()) {
				gridCaptura.show();
			}				
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  campo1 = jsonData.Campo0;
			var  campo2 = jsonData.Campo1;
			var  campo3 = jsonData.Campo2;
			var  campo4 = jsonData.Campo3;
			var  campo5 = jsonData.Campo4;
			var operaConversion = jsonData.operaConversion;
			numRegistrosMN = jsonData.numRegistrosMN;
			numRegistrosDL = jsonData.numRegistrosDL;
			hidMontoTotalMN = jsonData.hidMontoTotalMN;
			hidMontoTotalDL = jsonData.hidMontoTotalDL;
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridCaptura.getColumnModel();
				
			if(operaConversion=='false'){
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);	
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);	
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('MONTO_VALUADO'), true);								
			}
							
			if(campo1 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),campo1);
			}
			if(campo2 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),campo2);
			}
			if(campo3 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),campo3);
			}
			if(campo4 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),campo4);
			}
			if(campo5 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),campo5);
			}
		
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('PLAZO'), true);
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VEN'), true);
			
			if(hayCamposAdicionales=='0'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='1'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='2'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);			
			}else  	if(hayCamposAdicionales=='3'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='4'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='5'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);	
			}
			
			if(jsonData.strPerfil!='ADMIN EPO' && strPerfil!='ADMIN EPO GRUPO'  &&  strPerfil!='ADMIN EPO FON'  &&  strPerfil!='ADMIN EPO DISTR' ){
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('NEGOCIABLE'), true);
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('ESTATUS'), false);
			}else {
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('NEGOCIABLE'), false);
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('ESTATUS'), true);
			}
			
				//F09-2015
			if(jsonData.meseSinIntereses=='S' && jsonData.obtieneTipoCredito=='C' ){
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), false);			
			}else  {
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), true);		
			}
			
			var el = gridCaptura.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var agregar = function() {
	
		var ic_pyme = Ext.getCmp("ic_pyme1");
		var cg_num_distribuidor = Ext.getCmp("cg_num_distribuidor1");		
		var fn_monto = Ext.getCmp("fn_monto1");		
		var ig_numero_docto = Ext.getCmp("ig_numero_docto1");
		var df_fecha_emision = Ext.getCmp("df_fecha_emision1");
		var df_fecha_venc = Ext.getCmp("df_fecha_venc1");
		var ic_moneda = Ext.getCmp("ic_moneda1");
		var ic_tipo_financiamiento  = Ext.getCmp("ic_tipo_financiamiento1");
		var fn_porc_descuento  = Ext.getCmp("fn_porc_descuento1");
	   var ig_plazo_descuento = Ext.getCmp("ig_plazo_descuento1");
		var fechaV	=  Ext.util.Format.date( '01/01/2001','d/m/Y');		
		var txt_fecha_limite_dsc = Ext.getCmp("txt_fecha_limite_dsc1");
		
		
		if(Ext.getCmp('numDistValidHid1').getValue()=='N' || Ext.getCmp('cvePymeValidHid1').getValue()=='N'){
			return;
		}
		
		if (!Ext.isEmpty(df_fecha_emision.getValue()) ){	
			var fecha =  Ext.util.Format.date(df_fecha_emision.getValue(),'d/m/Y'); 			
			if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
				df_fecha_emision.markInvalid('La fecha emisi�n no puede ser menor al a�o 2001');
				return;
			}
		}
		
		if (!Ext.isEmpty(df_fecha_venc.getValue()) ){	
			var fecha =  Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y'); 			
			if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
				df_fecha_venc.markInvalid('La fecha vencimiento no puede ser menor al a�o 2001');
				return;
			}
		}
		if (!Ext.isEmpty(txt_fecha_limite_dsc.getValue()) ){	
			var fecha =  Ext.util.Format.date(txt_fecha_limite_dsc.getValue(),'d/m/Y'); 			
			if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
				txt_fecha_limite_dsc.markInvalid('La fecha l�mite para tener Descuento no puede ser menor al a�o 2001');
				return;
			}
		}
		
		if (Ext.isEmpty(ic_pyme.getValue()) &&   Ext.isEmpty(cg_num_distribuidor.getValue()) ){
			ic_pyme.markInvalid('Por favor elija un distribuidor.');
			return;
		}		
		
		if (Ext.isEmpty(ig_numero_docto.getValue()) ){
			ig_numero_docto.markInvalid('No debe dejar el campo en blanco. Por favor capt�relo.');
			return;
		}
		
		if (Ext.isEmpty(df_fecha_emision.getValue()) ){
			df_fecha_emision.markInvalid('Por favor capture la Fecha.');
			return;
		}
		
		if (Ext.isEmpty(df_fecha_venc.getValue()) ){
			df_fecha_venc.markInvalid('Por favor capture la Fecha.');
			return;
		}
		
				 
		var resultado = datecomp( Ext.util.Format.date(df_fecha_emision.getValue(),'d/m/Y'), Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y') );
		//  0 si son iguales.
		//  1 si la primera es mayor que la segunda
		//  2 si la segunda mayor que la primera
		// -1 si son fechas invalidas		
		if (resultado!=2) {
			df_fecha_venc.markInvalid("La fecha de vencimiento del documento debe ser posterior a la de emisi�n");			
			return;
		}
	
		var resultado = datecomp(hidFechaActual, Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y'));
		if (resultado==1) 	{
			df_fecha_venc.markInvalid("La fecha de vencimiento no puede ser anterior a la fecha actual");			
			return;
		}	
						
		if (Ext.isEmpty(fn_monto.getValue()) ){
			fn_monto.markInvalid('Por favor capture el monto del documento');
			return;
		}
		
		if (!Ext.isEmpty(fn_monto.getValue()) ){
			if(parseFloat(fn_monto.getValue())<=0){
				fn_monto.markInvalid("El monto debe ser mayor a cero");
				return false;
			}  				
			var porcentaje = (fn_monto.getValue() *100)/lineaCredito;
			var restaporcentaje = 100-porcentaje;
			var valor =  parseFloat(porcentaje).toFixed(2);
			var valor2 =   parseFloat(restaporcentaje).toFixed(2);
			if ((descuentoAutomatico=="S" &&  limiteLineaCredito=="S" &&  tipoCredito=="C" ) || (descuentoAutomatico=="S" &&  limiteLineaCredito=="S" &&  tipoCredito =="D" && ventaCartera =="S")) {       
				if (parseFloat(fn_monto.getValue()) > lineaCredito){
					fn_monto.markInvalid("El monto capturado rebasa el l�mite de su l�nea de cr�dito. Porcentaje Utilizado "+valor+" %");
          return ;    
        }else{
					Ext.MessageBox.alert("Mensaje","Porcentaje disponible de la linea de cr�dito  "+valor2+ " %");        
				}
			}
		}
				
		if (Ext.isEmpty(ic_moneda.getValue()) ){
			ic_moneda.markInvalid('Por favor elija una Moneda.');
			return;
		}
		
		if(tipoCredito!='F') {
			if (Ext.isEmpty(ic_tipo_financiamiento.getValue()) ){
				ic_tipo_financiamiento.markInvalid('Por favor elija una Modalidad de Plazo.');
				return;
			}
		}
	
		//para las cifras de control 
		if (ic_moneda.getValue() == 1 && 'S'==hidCifrasControl)  {  //moneda nacional
		
			if (parseFloat(fn_monto.getValue(),10) > parseFloat(txtmtomaxdoc,10) || parseFloat(fn_monto.getValue(),10) < parseFloat(txtmtomindoc,10)) 	{
				fn_monto.markInvalid("Verifique el monto.\nCifras de Control:\n Monto M�ximo M.N.: " +txtmtomaxdoc+"\nMonto M�nimo M.N.: "+txtmtomindoc);
				return;
			}
			
			if(roundOff(parseFloat(hidMontoTotalMN,10)+parseFloat(fn_monto.getValue(),10),2)>parseFloat(txtmtodoc,10)) 	{
					fn_monto.markInvalid(" Monto total excedido.\nCifras de Control:\n Monto Total M.N.: " +txtmtodoc);			
				return;
			}
			if(parseInt(numRegistrosMN,10)+1 > parseInt(txttotdoc,10))
			{
				ic_pyme.markInvalid("Cantidad de Documentos en M.N. Excedida.\nCifras de Control:\n Numero de Documentos M.N.: " +txttotdoc);				
				return;
			}
		} 
	
		if (ic_moneda.getValue() == 54 && 'S'==hidCifrasControl)  {  //moneda 54  D�lares		
			
			if (parseFloat(fn_monto.getValue(),10) > parseFloat(txtmtomaxdocdo,10) || parseFloat(fn_monto.getValue(),10) < parseFloat(txtmtomindocdo,10))			{
				fn_monto.markInvalid("Verifique el monto.\nCifras de Control:\n Monto M�ximo D�lares: " +txtmtomaxdocdo+"\nMonto M�nimo D�lares: "+txtmtomindocdo);
				return;
			}

			if(roundOff(parseFloat(hidMontoTotalDL,10)+parseFloat(fn_monto.getValue(),10),2)>parseFloat(txtmtodocdo,10))	{
				fn_monto.markInvalid("Monto total excedido.\nCifras de Control:\n Monto Total D�lares.: " +txtmtodocdo);				
				return;
			}

			if(parseInt(numRegistrosDL,10)+1>parseInt(txttotdocdo,10))		{
				ic_pyme.markInvalid("Cantidad de Documentos en D�lares Excedida.\nCifras de Control:\n Numero de Documentos D�lares: " +txttotdocdo);
				return;
			}			
		}
				
		//para el plazo 	
		if (!Ext.isEmpty(fn_porc_descuento.getValue()) ){
				
			if(parseFloat(fn_porc_descuento.getValue()) <0){ 			
				fn_porc_descuento.markInvalid('El porcentaje debe ser mayor o igual a cero');
				return;
			}
			if( parseFloat(fn_porc_descuento.getValue()) >100){			
				fn_porc_descuento.markInvalid('El porcentaje debe ser menor a cien');				
				return false;
			}		
		}	
	
		if(!Ext.isEmpty(ig_plazo_descuento.getValue())  && Ext.isEmpty(fn_porc_descuento.getValue())  ) {			
			fn_porc_descuento.markInvalid('Si captura un Plazo de Descuento debe de capturar el Porcentaje de Descuento.');				
			return;
		}
		
		//F09-2015	
		if(tipoCredito=='C') {
			if( Ext.getCmp("meseSinIntereses").getValue()=='S'  &&  Ext.isEmpty(Ext.getCmp("ic_tipo_Pago1").getValue()) ){
				Ext.getCmp("ic_tipo_Pago1").markInvalid('Por favor elija un Tipo de Pago.');
				return;
			}
			
			if( Ext.getCmp("meseSinIntereses").getValue()=='S'  &&  Ext.getCmp("ic_tipo_Pago1").getValue()==2 && Ext.isEmpty(Ext.getCmp("ic_pediodo1").getValue()) ){
				Ext.getCmp("ic_pediodo1").markInvalid('Por favor elija un Periodo.');
				return;
			}
		}
				
		
		var fp = Ext.getCmp('forma');				
			
		Ext.Ajax.request({
				url : '24forma01Ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion:'AntesDECaptura',
					proceso:proceso,
					hidID:hidID					
				}),
				callback: procesarSuccessFailureValida
			});			
	}
	
	var procesarCatalogoModalidadData= function(store, records, oprion){
			if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
				
				var jsonData = store.reader.jsonData;
				var ic_tipo_financiamiento = Ext.getCmp('ic_tipo_financiamiento1');
				if(jsonData.ic_tipo_financiamiento!='') {			
					ic_tipo_financiamiento.setValue('2');
				}
			}
	  }
	
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoModalidadData = new Ext.data.JsonStore({
		id: 'catalogoModalidadDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoModalidad'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatalogoModalidadData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoCategoriaData = new Ext.data.JsonStore({
		id: 'catalogoPYMEDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCategoria'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	})
	

	
	var catalogoPediodoData = new Ext.data.JsonStore({
		id: 'catalogoPediodoData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'catalogoPediodoData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	})
	
	
	  var dataTipoPago = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['','Seleccionar'],
			['1','Financiamiento con intereses '],
			['2','Meses sin intereses ']
		]		
	 });
	 
	 
	
	//para los totales del grid Nomal 
	var totalesData = new Ext.data.JsonStore({			
		root : 'registrosT',
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'	
		},								
		fields: [			
			{name: 'MONEDA' , mapping: 'MONEDA'},
			{name: 'TOTAL', type: 'float', mapping: 'TOTAL'},
			{name: 'TOTAL_MONTO' , type: 'float', mapping: 'TOTAL_MONTO' },
			{name: 'TOTAL_MONTO_DESC',type: 'float',   mapping: 'TOTAL_MONTO_DESC'}				
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		id: 'gridTotales',
		store: totalesData,
		margins: '20 0 0 0',
		align: 'center',
		title: '',	
		hidden: true,
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'				
			},
			{
				header: 'Total de Documentos ',
				dataIndex: 'TOTAL',
				width: 150,
				align: 'center'				 
			},
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Total Monto Descuento',
				dataIndex: 'TOTAL_MONTO_DESC',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: '',
				dataIndex: '',
				width: 334,
				align: 'right'			
			}		
			],
			height: 100,
			width: 943,
			frame: false
	});	
		
/////////////////////////////////////////////////////////////////////////////////

	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CONSECUTIVO'},		
			{name: 'NUM_DISTRIBUIDOR'},
			{name: 'NOMBRE_DISTRIBUIDOR'},
			{name: 'NUN_DOCTOINICIAL'},			
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'CATEGORIA'},
			{name: 'PLAZO_DESC'},
			{name: 'PORCE_DESC'},
			{name: 'MONTO_DESC'},				
			{name: 'TIPO_CONVERSION'},	
			{name: 'TIPO_CAMBIO'},	
			{name: 'MONTO_VALUADO'},
			{name: 'MODALIDAD'},
			{name: 'COMENTARIOS'},
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'},
			{name: 'CAMPO4'},
			{name: 'CAMPO5'},
			{name: 'PLAZO'},
			{name: 'FECHA_VEN'},		
			{name: 'NEGOCIABLE'},
			{name: 'LINEA_REGISTRO'},
			{name: 'RESPONSABLE_INTERES'},
			{name: 'DIAS_MINIMO'},
			{name: 'DIAS_MAXIMO'},		
			{name: 'IC_MONEDA'},	
			{name: 'SELECCIONAR_IFS'},
			{name: 'AUXICLINEA_MN'},
			{name: 'AUXICLINEA_USD'},
			{name: 'LINEA_REGISTRO_AUXILIAR'},
			{name: 'PARTIPOCAMBIO'},
			{name: 'HID_NUMLINEAS'},			
			{name: 'CAPTURA_LINEA'},
			{name: 'CAPTURA_PLAZO'},
			{name: 'CAPTURA_FECHAVEN'},
			{name: 'TIPO_CREDITO'},
			{name: 'ESTATUS'},			
			{name: 'TIPOPAGO'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
				
	var gridCaptura = new Ext.grid.EditorGridPanel({
		id: 'gridCaptura',
		title: 'Carga de Documentos',
		hidden: true,
		clicksToEdit: 1,				
		columns: [
		{							
			header : 'N�mero de distribuidor',
			tooltip: 'N�mero de distribuidor',
			dataIndex : 'NUM_DISTRIBUIDOR',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Distribuidor',
			tooltip: 'Distribuidor',
			dataIndex : 'NOMBRE_DISTRIBUIDOR',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'N�mero documento inicial',
			tooltip: 'N�mero documento inicial',
			dataIndex : 'NUN_DOCTOINICIAL',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Fecha de emisi�n',
			tooltip: 'Fecha de emisi�n',
			dataIndex : 'FECHA_EMISION',
			width : 150,
			align: 'center',
			sortable : false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{							
			header : 'Fecha vto. docto.',
			tooltip: 'Fecha vto. docto.',
			dataIndex : 'FECHA_VENCIMIENTO',
			width : 150,
			align: 'center',
			sortable : false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{							
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO_DOCTO',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			align: 'left',
			sortable : false			
		},
		{							
			header : 'Monto. docto.',
			tooltip: 'Monto. docto.',
			dataIndex : 'MONTO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Categor�a',
			tooltip: 'Categor�a',
			dataIndex : 'CATEGORIA',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Plazo para descuento en dias',
			tooltip: 'Plazo para descuento en dias',
			dataIndex : 'PLAZO_DESC',
			width : 150,
			align: 'center',
			sortable : false,
			renderer:function(value,metadata,registro){
				if(registro.data['TIPO_CREDITO']!='F') {	
					return value;
				}else  {
					return 'NA';
				}
			}			
		},
		{							
			header : '% descuento',
			tooltip: '% descuento',
			dataIndex : 'PORCE_DESC',
			width : 150,
			align: 'center',
			sortable : false,
			renderer:function(value,metadata,registro){
				if(registro.data['TIPO_CREDITO']!='F') {	
					return Ext.util.Format.number(value,'0.0000%');
				}else  {
					return 'NA';
				}
			}
		},
		{							
			header : 'Monto % de descuento',
			tooltip: 'Monto % de descuento',
			dataIndex : 'MONTO_DESC',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00') 
		},
		{							
			header : 'Tipo conversi�n',
			tooltip: 'Tipo conversi�n',
			dataIndex : 'TIPO_CONVERSION',
			width : 150,
			align: 'right',
			sortable : false		 
		},
		{							
			header : 'Tipo cambio ',
			tooltip: 'Tipo cambio ',
			dataIndex : 'TIPO_CAMBIO',
			width : 150,
			align: 'right',
			sortable : false		 
		},
		{							
			header : 'Monto valuado en Pesos',
			tooltip: 'Monto valuado en Pesos',
			dataIndex : 'MONTO_VALUADO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00') 
		},	
		{							
			header : 'Modalidad de Plazo',
			tooltip: 'Modalidad de Plazo',
			dataIndex : 'MODALIDAD',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Tipo de Pago',
			tooltip: 'Tipo de Pago',
			dataIndex : 'TIPOPAGO',
			width : 150,
			align: 'center',
			sortable : false
		},		
		{							
			header : 'Comentarios',
			tooltip: 'Comentarios',
			dataIndex : 'COMENTARIOS',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo1',
			tooltip: 'Campo1',
			dataIndex : 'CAMPO1',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo2',
			tooltip: 'Campo2',
			dataIndex : 'CAMPO2',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Campo3',
			tooltip: 'Campo3',
			dataIndex : 'CAMPO3',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo4',
			tooltip: 'Campo4',
			dataIndex : 'CAMPO4',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo5',
			tooltip: 'Campo5',
			dataIndex : 'CAMPO5',
			width : 150,
			align: 'left',
			sortable : false
		},			
		{							
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO',
			width : 150,
			align: 'center',
			sortable : false
		},			
		{							
			header : 'Fecha vto',
			tooltip: 'Fecha vto',
			dataIndex : 'FECHA_VEN',
			width : 150,
			align: 'center',
			sortable : false		
		},			
		{							
			header : 'Negociable',
			tooltip: 'Negociable',
			dataIndex : 'NEGOCIABLE',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Estatus',
			tooltip: 'Estatus',
			dataIndex : 'ESTATUS',
			width : 150,
			align: 'center',
			sortable : false
		},		
		{
		  xtype: 'actioncolumn',
				header: 'Eliminar',
				tooltip: 'Eliminar',
				dataIndex : 'ELIMINAR',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Eliminar';
							return 'borrar';										
						},
						handler: procesaAccionBorrar
					}
				]				
		}	,		
		{
		  xtype: 'actioncolumn',
				header: 'Modificar',
				tooltip: 'Modificar',
				dataIndex : 'MODIFICAR',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Modificar';
							return 'modificar';										
						},
						handler: procesaAccionModificar
					}
				]				
		}	
		],
		displayInfo: true,
		store: consultaData,
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		frame: false
		});
		
	var elementosFormaCarga = [	
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Distribuidor',
			combineErrors: false,
			msgTarget: 'side',
			width: 600,
			items: [
				{	
					xtype: 'hidden',
					name: 'numDistValidHid',
					id: 'numDistValidHid1'
				},
				{
				xtype: 'textfield',
				name: 'cg_num_distribuidor',
				id: 'cg_num_distribuidor1',
				fieldLabel: 'N�mero de Distribuidor:',
				allowBlank: true,
				hidden: false,
				maxLength: 25,	
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0',
				listeners: {
						blur: {
							fn: function(objTxt) {
								Ext.getCmp('numDistValidHid1').setValue('S');
								if(objTxt.getValue()!='') {
									Ext.Ajax.request({
										url: '24forma01Ext.data.jsp',
										params: {
											informacion: "validaPymeBloqXNumDist",
											cg_num_distribuidor: objTxt.getValue()
										},
										callback: procesaPymeBloqXNumProv
									});
								}							
							}
						}
					}
				},				
				{
					xtype: 'displayfield',
					value: 'Categor�a:',
					width: 150
				},
				{
					xtype: 'combo',
					name: 'cmb_categoria',
					id: 'cmb_categoria1',
					fieldLabel: 'Categoria',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'cmb_categoria',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',
					store : catalogoCategoriaData,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 300,
					listeners: {
						select: {
							fn: function(combo) {	
								procesaCategoria(combo.getValue());			
							}
						}	
					}					
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* Distribuidor',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{	
					xtype: 'hidden',
					name: 'cvePymeValidHid',
					id: 'cvePymeValidHid1'
				},
				{
					xtype: 'combo',
					name: 'ic_pyme',
					id: 'ic_pyme1',
					fieldLabel: 'Distribuidor',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_pyme',
					emptyText: 'Seleccione...',
					forceSelection : true,
					triggerAction : 'all',
					width: 300,
					typeAhead: true,
					minChars : 1,					
					msgTarget: 'side',					
					store : catalogoPYMEData,
					tpl:'<tpl for=".">' +
					'<tpl if="!Ext.isEmpty(loadMsg)">'+
					'<div class="loading-indicator">{loadMsg}</div>'+
					'</tpl>'+
					'<tpl if="Ext.isEmpty(loadMsg)">'+
					'<div class="x-combo-list-item">' +
					'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
					'</div></tpl></tpl>',
					setNewEmptyText: function(emptyTextMsg){
						this.emptyText = emptyTextMsg;
						this.setValue('');
						this.applyEmptyText();
						this.clearInvalid();					
					}, 
					listeners: {
						select: {
							fn: function(combo) {							
								//para el combo de cmb_categoria1	
								var categoria = Ext.getCmp('cmb_categoria1');
								categoria.setValue('');
								categoria.setDisabled(false);
								categoria.store.load({
									params: {
										ic_pyme:combo.getValue()
									}
								});	
								if( ( ventaCartera=='S' && descuentoAutomatico=='S') || tipoCredito=='F' ){									
									categoria.disable();								
								}								
							}
						},
						change: {
							fn: function(combo, newVal, oldVal) {	
								Ext.getCmp('cvePymeValidHid1').setValue('S');
								if(newVal!='') {
									Ext.getCmp("cg_num_distribuidor1").setValue("");
									Ext.getCmp('numDistValidHid1').setValue('S');
									
									Ext.Ajax.request({
										url: '24forma01Ext.data.jsp',
										params: {
											informacion: "validaPymeBloqXNumDist",
											ic_pyme: newVal
										},
										callback: procesaPymeBloqueada
									});
								}							
							}
						}
					}
				},
				{
					xtype: 'displayfield',
					value: 'Plazo para descuento: ',
					width: 150
				},				
				{
					xtype: 'numberfield',
					name: 'ig_plazo_descuento',
					id: 'ig_plazo_descuento1',
					fieldLabel: 'Plazo para descuento',
					allowBlank: true,
					hidden: false,
					maxLength: 3,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																									
							if(field.getValue() != '') {
								validaPlazo();
							}
						}
					}	
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* N�m. documento inicial',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'textfield',
					name: 'ig_numero_docto',
					id: 'ig_numero_docto1',
					fieldLabel: 'N�m. documento inicial',
					allowBlank: true,
					hidden: false,
					maxLength: 15,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'Porcentaje de descuento:',
					width: 150
				},				
				{
					xtype: 'numberfield',
					name: 'fn_porc_descuento',
					id: 'fn_porc_descuento1',
					fieldLabel: 'Porcentaje de descuento',
					allowBlank: true,
					hidden: false,
					maxLength: 5,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																									
							if(field.getValue() != '') {
								validaPorcentaje();
							}
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '*Fecha de emisi�n',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_emision',
					id: 'df_fecha_emision1',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha',					
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																										
							if(field.getValue() != '') {
								var fecha	=  Ext.util.Format.date(field.getValue(),'d/m/Y'); 
								var fechaV	=  Ext.util.Format.date( '01/01/2001','d/m/Y');		
								var df_fecha_emision1 = Ext.getCmp("df_fecha_emision1");
								if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
									df_fecha_emision1.markInvalid('La fecha emisi�n no puede ser menor al a�o 2001');
									return;
								}				
								validaFechaEmision();
							}
						}
					}					
				},				
				{
					xtype: 'displayfield',
					value: 'Comentarios:',
					width: 150
				},				
				{
					xtype: 'textarea',
					name: 'ct_referencia',
					id: 'ct_referencia1',
					fieldLabel: 'Comentarios:',
					allowBlank: true,
					hidden: false,
					maxLength: 260,	
					width: 300,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '*Fecha de vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_venc',
					id: 'df_fecha_venc1',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha',					
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																										
							if(field.getValue() != '') {
								var fecha	=  Ext.util.Format.date(field.getValue(),'d/m/Y'); 
								var fechaV	=  Ext.util.Format.date( '01/01/2001','d/m/Y');		
								var df_fecha_venc1 = Ext.getCmp("df_fecha_venc1");
								if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
									df_fecha_venc1.markInvalid('La fecha vencimiento no puede ser menor al a�o 2001');
									return;
								}								
							}
						}
					}
				},			
				{
					xtype: 'displayfield',
					value: 'Fecha l�mite para tener Descuento:',
					width: 150
				},
				{
					xtype: 'datefield',
					name: 'txt_fecha_limite_dsc',
					id: 'txt_fecha_limite_dsc1',
					fieldLabel: '* Fecha l�mite para tener Descuento',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha',					
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 
							var  df_fecha_emision = Ext.getCmp("df_fecha_emision1");							
								validaFechaLim();							
							
						}			
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '*Moneda',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					fieldLabel: '* Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 300,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 150
				}			
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '*Monto',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'numberfield',
					name: 'fn_monto',
					id: 'fn_monto1',
					fieldLabel: 'Monto',
					allowBlank: true,
					hidden: false,
					maxLength: 25,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																									
							if(field.getValue() != '') {
								validaMonto();
							}
						}
					}
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 150
				}			
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '*Modalidad de Plazo',
			id: 'ic_tipo_financiamiento2',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'combo',
					name: 'ic_tipo_financiamiento',
					id: 'ic_tipo_financiamiento1',
					fieldLabel: '* Modalidad de Plazo',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_tipo_financiamiento',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 300,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',
					store : catalogoModalidadData,
					tpl : NE.util.templateMensajeCargaCombo
				}							
			]			
		},	
			{
			xtype: 'compositefield',
			fieldLabel: '*Tipo de Pago',
			id: 'ic_tipo_Pago2',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			hidden:true,
			items: [
				{
					xtype: 'combo',
					name: 'ic_tipo_Pago',
					id: 'ic_tipo_Pago1',
					fieldLabel: '* Tipo de Pago',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_tipo_Pago',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 300,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',					
					store : dataTipoPago,
					listeners:{
						select:	function(combo){
						
							var ic_tipo_financiamiento = Ext.getCmp('ic_tipo_financiamiento1');
													 
							if(combo.getValue() == '2' ){
								
								Ext.getCmp('ic_pediodo2').show();	
								var  ic_pediodo1=  Ext.getCmp('ic_pediodo1')								
								ic_pediodo1.store.load();
								ic_tipo_financiamiento.store.load({
									params: {
										ic_tipo_financiamiento:'2'
									}
								});								
								
							}else{
								Ext.getCmp('ic_pediodo2').hide();
								
								ic_tipo_financiamiento.store.load({ }); 
							}
						}
					}
				}							
			]			
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Periodo',
			id: 'ic_pediodo2',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			hidden:true,
			items: [
				{
					xtype: 'combo',
					name: 'ic_pediodo',
					id: 'ic_pediodo1',
					fieldLabel: 'Periodo',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_pediodo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 300,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',
					store : catalogoPediodoData,
					tpl : NE.util.templateMensajeCargaCombo
				}							
			]			
		},
		
		{
			xtype: 'textfield',
			name: 'xxxxx',
			id: 'xxxxx1',
			allowBlank: true,
			startDay: 0,
			hidden: true,
			width: 150,
			msgTarget: 'side',								
			margins: '0 20 0 0'
		}	, 				
		{
			xtype: 'compositefield',
			fieldLabel: 'Negociable',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			id: 'idnegociables', 
			items: [
				{  
					xtype:  'radiogroup',   
					fieldLabel: "Negociable",    
					name: 'negociable',   
					id: 'negociable1',  
					value: 'S',  	
					width: 150,
					columns: 2,			
					items:         
						[         
							{ 
								boxLabel:    "SI",             
								name:        'negociable',   
								//checked: true,            
								inputValue:  "S" ,
								width: 20
							},         
							{           
								boxLabel: "NO",             
								name: 'negociable',             
								inputValue:  "N",  
								width: 20	
							}   
						],   
					style: {      
						paddingLeft: '10px'   
					}		
				},				
				{
					xtype: 'displayfield',
					value: ' *Campos obligatorios',
					id: 'camposObliga1',
					width: 150
				}				
			]	
		}
		,{
			xtype: 'displayfield',
			value: ' *Campos obligatorios',
			id: 'camposObliga2',
			hidden: true,
			width: 150
		},
		{ 	xtype: 'textfield',  hidden: true,  id: 'meseSinIntereses', 	value: '' }
	];
	
	var fpCarga = new Ext.form.FormPanel({
		id: 'forma',
		width: 885,
		title: 'Criterios de Captura',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaCarga,			
		monitorValid: true,				
		buttons: [
			{
				text: 'Agregar',
				id: 'btnAgregar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: agregar
			},			
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24forma01Ext.jsp';
				}
			}
		]
	})

//Contenedor para la pantalla de Carga Inicial
	var fpConfirmar = new Ext.Container({
		layout: 'table',
		id: 'fpConfirmar',
		hidden: true,
		layoutConfig: {
			columns: 20
		},
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
		{
			xtype: 'button',
			text: 'Cancelar',			
			iconCls: 'icoLimpiar',
			id: 'btnCancelar',
			hidden: true,
			handler: procesarCancelar
		},
		{
			xtype: 'button',
			text: 'Terminar captura',
			id: 'btnTerminaCap',
			iconCls: 'icoAceptar',
			handler: procesarTerminaCaptura,	
			hidden: true
		}
	]
	})
	
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			fpCarga,	
			NE.util.getEspaciador(20),
			gridCaptura,			
			gridTotales,
			NE.util.getEspaciador(20),
			fpConfirmar,
			NE.util.getEspaciador(20)			
		]
	});
		
	catalogoMonedaData.load();
	catalogoPYMEData.load();
	catalogoModalidadData.load();
	
	
	if(tipoCredito=='F'){
		Ext.getCmp('ic_tipo_financiamiento2').disable();
		Ext.getCmp('cmb_categoria1').disable();
		Ext.getCmp('ig_plazo_descuento1').disable();
		Ext.getCmp('fn_porc_descuento1').disable();
		Ext.getCmp('txt_fecha_limite_dsc1').disable();
	}

	if(ventaCartera=='S' || descuentoAutomatico=='S'){
		var cmb_categoria = Ext.getCmp('cmb_categoria1');	
		var ig_plazo_descuento = Ext.getCmp('ig_plazo_descuento1');	
		var fn_porc_descuento = Ext.getCmp('fn_porc_descuento1');
		var txt_fecha_limite_dsc = Ext.getCmp('txt_fecha_limite_dsc1');
		
		cmb_categoria.disable();
		ig_plazo_descuento.disable();
		fn_porc_descuento.disable();
		txt_fecha_limite_dsc.disable();
		
	}

	//esto es para mostrar los campos adicionales
	function procesaValoresIniciales(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
			
				Ext.getCmp('meseSinIntereses').setValue(jsonValoresIniciales.meseSinIntereses);
				
				if(jsonValoresIniciales.meseSinIntereses=='S' && jsonValoresIniciales.obtieneTipoCredito =='C') {
					Ext.getCmp('ic_tipo_Pago2').show();
				}else  {
					Ext.getCmp('ic_tipo_Pago2').hide(); 
				}
				
				
				var  numeroCampos = jsonValoresIniciales.numeroCampos;	
				for(var i= 1; i<=numeroCampos; i++) {			
					if(i==1){		
						var cg_campo1 =  Ext.decode(jsonValoresIniciales.cg_campo1);						
						var indice1 = fpCarga.items.indexOfKey('xxxxx1')+1;
						fpCarga.insert(indice1,cg_campo1);
						fpCarga.doLayout();
					
					}		
					if(i==2){		
						var cg_campo2 =  Ext.decode(jsonValoresIniciales.cg_campo2);						
						var indice1 = fpCarga.items.indexOfKey('cg_campo1')+1;
						fpCarga.insert(indice1,cg_campo2);
						fpCarga.doLayout();						
					}	
					if(i==3){		
						var cg_campo3 =  Ext.decode(jsonValoresIniciales.cg_campo3);						
						var indice1 = fpCarga.items.indexOfKey('cg_campo2')+1;
						fpCarga.insert(indice1,cg_campo3);
						fpCarga.doLayout();						
					}		
					if(i==4){		
						var cg_campo4 =  Ext.decode(jsonValoresIniciales.cg_campo4);						
						var indice1 = fpCarga.items.indexOfKey('cg_campo3')+1;
						fpCarga.insert(indice1,cg_campo4);
						fpCarga.doLayout();						
					}	
					if(i==5){		
						var cg_campo5 =  Ext.decode(jsonValoresIniciales.cg_campo5);						
						var indice1 = fpCarga.items.indexOfKey('cg_campo4')+1;
						fpCarga.insert(indice1,cg_campo5);
						fpCarga.doLayout();						
					}	
										
				}//for				
			}
			fpCarga.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	if(strPerfil!='ADMIN EPO' && strPerfil!='ADMIN EPO GRUPO'  &&  strPerfil!='ADMIN EPO FON'   &&  strPerfil!='ADMIN EPO DISTR'   ){
		Ext.getCmp("idnegociables").hide();
		Ext.getCmp("camposObliga2").show();
		
	}
	
	//Peticion para obtener valores de los campos adicionales
	//if(campos=='S'){
		fpCarga.el.mask('Cargando Campos Adicionales...', 'x-mask-loading');	
		Ext.Ajax.request({
			url: '24forma01Ext.data.jsp',
			params: {
				informacion: "valoresIniciales"
			},
			callback: procesaValoresIniciales
		});
	//}
	//esto es para cuando selecciona la categoria
	var procesaCategoria =  function(cmb_categoria) {	
	
		var ic_pyme = Ext.getCmp("ic_pyme1");
		var  df_fecha_emision = Ext.getCmp("df_fecha_emision1");
		/*if (Ext.isEmpty(df_fecha_emision.getValue()) ){
			df_fecha_emision.markInvalid('Por favor capture la Fecha.');
			return;
		}
		*/				
		fpCarga.el.mask('Cargando Datos de Categoria ...', 'x-mask-loading');	
		Ext.Ajax.request({
			url: '24forma01Ext.data.jsp',
			params: {
				informacion: "DatosCategoria",
				cmb_categoria: cmb_categoria,
				ic_pyme:ic_pyme.getValue(),
				df_fecha_emision: Ext.util.Format.date(df_fecha_emision.getValue(),'d/m/Y'),
				hid_fecha_porc_desc:hid_fecha_porc_desc
			},
			callback: procesaValoresCategoria				
		});	
	}
	
	
	
} );