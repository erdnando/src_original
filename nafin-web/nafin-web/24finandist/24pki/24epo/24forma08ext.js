Ext.onReady(function() {

	var jsonValoresIniciales= null;
	var jsonValorMonto 		= null;

	function procesaValidarNumDist(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var valida = Ext.util.JSON.decode(response.responseText);
			if (valida.distribuidor != undefined && !valida.distribuidor){
				var numDist = Ext.getCmp('numDist');
				numDist.markInvalid('El distribuidor no existe para esta EPO');
				numDist.focus();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaValorMonto(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValorMonto = Ext.util.JSON.decode(response.responseText);
			Ext.Ajax.request({
				url: '24forma08ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{informacion: 'Consulta'}),
				callback: procesaValoresConsulta
			});
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function procesaValoresLimite(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var  jsonData = Ext.util.JSON.decode(response.responseText);
			
			var montoLimiteMN =  parseFloat(Ext.getCmp('montoLimiteMN').getValue());
			var montoLimiteDL =  parseFloat(Ext.getCmp('montoLimiteDL').getValue());			
			var  validaMontoLimite = parseFloat(jsonData.validaMontoLimite);
			if(montoLimiteMN!='NaN')  {			
				if(montoLimiteMN <validaMontoLimite)  {			
					Ext.getCmp('montoLimiteMN').markInvalid("El nuevo monto no puede ser menor a $ "+validaMontoLimite);						
				}
				
			}else  if(montoLimiteDL!='NaN')  {
				if(montoLimiteDL <validaMontoLimite)  {								
					Ext.getCmp('montoLimiteDL').markInvalid("El nuevo monto no puede ser menor a $ "+validaMontoLimite);	
				}
			
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	

	function procesaValoresIniciales(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			//if (jsonValoresIniciales != null){
			var cboEpo = Ext.getCmp('cboEpo');
			var cmbDis = Ext.getCmp('cmbDis');
			var numDist = Ext.getCmp('numDist');
			var montoLimiteMN = Ext.getCmp('montoLimiteMN');
			var montoLimiteDL = Ext.getCmp('montoLimiteDL');
			var disMsj1 = Ext.getCmp('disMsj1');
			var btnAccion = Ext.getCmp('btnAccion');
			var tipoSistema = Ext.get('sistemaMenu').getValue();
			
			btnAccion.setText('Consultar');
			btnAccion.setIconClass('icoBuscar');			
				
			if(jsonValoresIniciales.DCAPT != undefined && jsonValoresIniciales.DCAPT){
				if (jsonValoresIniciales.obtieneTipoCredito != undefined && jsonValoresIniciales.obtieneTipoCredito != 'C'){
					//cmbDis.allowBlank = false;
					numDist.show();
					montoLimiteMN.show();
					disMsj1.show();
					if (jsonValoresIniciales.montoLimitedl != undefined && jsonValoresIniciales.montoLimitedl != 0){
						montoLimiteDL.show();
					}
					btnAccion.setText('Aceptar');
					btnAccion.setIconClass('aceptar');
				}	else if (jsonValoresIniciales.obtieneTipoCredito != undefined && jsonValoresIniciales.obtieneTipoCredito == 'C'){
					fp.hide();
					Ext.Msg.alert('','Este men� est� disponible s�lo para Descuento Mercantil');
				}
			}else{
				btnAccion.setText('Consultar');
				btnAccion.setIconClass('icoBuscar');					
			}
			if (jsonValoresIniciales.strTipoUsuario=='NAFIN'){
				cboEpo.show();
			}else{
				cboEpo.hide();
				Ext.Ajax.request({
					url: '24forma08ext.data.jsp',
					params: {informacion: 'Consulta', ic_epo: jsonValoresIniciales.ses_ic_epo},
					callback: procesaValoresConsulta
				});
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaValoresConsulta(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.registros != undefined){
				if (!grid.isVisible()){
					grid.show();
				}
				consultaData.loadData(infoR.registros);
				Ext.getCmp('btnGenerarPDF').enable();				
				var cm = grid.getColumnModel();
				cm.setHidden(5, true);
				cm.setHidden(6, true);
				cm.setHidden(7, true);
				cm.setHidden(8, true);
				cm.setHidden(9, true);
				cm.setHidden(10, true);
				if(jsonValoresIniciales.DCAPT == undefined){
					cm.setHidden(5, false);
					cm.setHidden(6, false);
				}				
				
				if (jsonValoresIniciales.ses_ic_epo != undefined){
					if (jsonValoresIniciales.montoLimitedl != undefined && jsonValoresIniciales.montoLimitedl != 0){
						cm.setHidden(7, false);
						if(jsonValoresIniciales.DCAPT == undefined){
							cm.setHidden(8, false);
							cm.setHidden(9, false);
						}
					}
				}else{				
					if (jsonValorMonto.montoLimitedl != undefined && jsonValorMonto.montoLimitedl != 0){
						cm.setHidden(7, false);
						if(jsonValoresIniciales.DCAPT == undefined){
							cm.setHidden(8, false);
							cm.setHidden(9, false);
						}
					}
				}
				if(jsonValoresIniciales.DCAPT != undefined && jsonValoresIniciales.DCAPT){
					cm.setHidden(10, false);
				}
				if (Ext.isEmpty(infoR.registros)){
					grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					Ext.getCmp('btnGenerarPDF').disable();
					Ext.getCmp('cmbDis').setRawValue('');
					Ext.getCmp('cmbDis').setValue();
				}else{
					grid.getGridEl().unmask();
				}
				if(grid.isVisible() && jsonValoresIniciales.DCAPT != undefined && jsonValoresIniciales.DCAPT){
					Ext.getCmp('barGrid').hide();
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarSuccessFailureGenerarPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			btnGenerarPDF.setIconClass('icoPdf');
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var procesaModificar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var dataDis = catalogoDisData.reader.jsonData;
		Ext.each(dataDis.registros, function(item, index, arrItems){
			if (registro.get('IC_PYME') == item.clave){
				Ext.getCmp('cmbDis').setValue(registro.get('IC_PYME'))
				Ext.getCmp('cmbDis').forceSelection = false;
			}
		});
		Ext.getCmp('montoLimiteMN').setValue(registro.get('FG_LIMITE_PYME'));
		Ext.getCmp('montoLimiteDL').setValue(registro.get('FG_LIMITE_PYME_USD'));
		Ext.getCmp('disMsj1').setValue(registro.get('MENSAJE'));
		if (grid.isVisible()){grid.hide();}
	}

	var procesaEliminar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var dataDis = catalogoDisData.reader.jsonData;
		Ext.each(dataDis.registros, function(item, index, arrItems){
			if (registro.get('IC_PYME') == item.clave){
				Ext.getCmp('cmbDis').setValue(registro.get('IC_PYME'))
				var dataGrid = consultaData.reader.jsonData;
				Ext.each(dataGrid, function(itemData, indexData, arrItemsData){
					if (itemData.IC_PYME == item.clave){
						var newData = [];
						newData.push(itemData);
						consultaData.loadData(newData);
						return false;
					}
				});
			}
		});
	}

	function procesaCaptura(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.Ajax.request({
				url: '24forma08ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{informacion: 'Consulta',ic_epo: jsonValoresIniciales.ses_ic_epo}),
				callback: procesaValoresConsulta
			});
			Ext.getCmp('formaAcuse').setTitle('<div align="left"><center>Recibo: '+infoR._acuse+'</center></div>');
			Ext.getCmp('disAcuse').setValue(infoR.acuse.acuse);
			Ext.getCmp('disFecha').setValue(infoR.fechaHoy);
			Ext.getCmp('disHora').setValue(infoR.horaActual);
			Ext.getCmp('disUsuario').setValue(infoR.usuario);
			Ext.getCmp('btnAbrirPDF').setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = infoR.urlArchivo;
				forma.submit();
			});
			var winAcuse = Ext.getCmp('winAcuse');
			if (winAcuse){
				winAcuse.show();
			}else{
				new Ext.Window({
					modal: true,
					resizable: false,
					layout: 'form',
					width: 415,
					height: 205,
					x:400,
					y:50,
					id: 'winAcuse',
					closeAction: 'hide',
					items: [fpAcuse],
					title: 'Acuse'
				}).show().el.dom.scrollIntoView();
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var catalogoEPODistData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '24forma08ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPODist'
		},
		totalProperty: 'total',
		autoload: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoDisData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma08ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoDistribuidor'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name: 'IC_PYME'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'MENSAJE'},
			{name: 'FECHA'},
			{name: 'ICUSUARIO'},
			{name: 'NOMBRE'},
			{name: 'FG_LIMITE_PYME'},
			{name: 'FG_LIMITE_PYME_ACUM'},
			{name: 'FG_LIMITE_PYME_USD'},
			{name: 'FG_LIMITE_PYME_ACUM_USD'}
		],
		data:[{'IC_PYME':'','CG_RAZON_SOCIAL':'','MENSAJE':'','FECHA':'','ICUSUARIO':'','NOMBRE':'','FG_LIMITE_PYME':'','FG_LIMITE_PYME_ACUM':'','FG_LIMITE_PYME_USD':'','FG_LIMITE_PYME_ACUM_USD':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{//0
				header: 'Distribuidor', tooltip: 'Distribuidor',
				dataIndex: 'CG_RAZON_SOCIAL',
				align: 'left',
				sortable: true,	
				resizable: true,	
				width: 150,	
				hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{//1
				header : 'Mensaje de terminaci�n de l�mite de l�nea', 
				tooltip: 'Mensaje de terminaci�n de l�mite de l�nea',
				dataIndex : 'MENSAJE', 
				align: 'left',
				sortable : true,
				resizable: true, 
				width : 250,	
				hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{//2
				header: 'Fecha/Hora', 
				tooltip: 'Fecha/Hora',
				dataIndex: 'FECHA', 
				align: 'center',
				sortable: true,	
				width: 150,	
				resizable: true, 
				hideable : true, 
				hidden: false
			},{//3
				header : 'Usuario',
				tooltip: 'Usuario',
				dataIndex : 'ICUSUARIO',
				sortable : true,	
				width : 220, 
				hidden: false, 
				resizable: true,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value + ' ' + record.get('NOMBRE')) + '"';
					return value + ' ' + record.get('NOMBRE');
				}				
			},{//4
				header : 'Monto L�mite M.N.', 
				tooltip: 'Monto L�mite M.N.',
				dataIndex : 'FG_LIMITE_PYME',
				hideable: false, 
				align: 'right',
				sortable : true,	
				width : 120, 
				hidden: false,	
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},
			{//5
				header : '% Utilizaci�n M.N.',
				tooltip: '% de Utilizaci�n M.N.',
				dataIndex : '', 
				hideable: false,
				sortable : true,	
				width : 120, 
				hidden: false, 
				align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					value = 0;
					if (record.get('FG_LIMITE_PYME') != 0){
						value = (record.get('FG_LIMITE_PYME_ACUM')*100)/record.get('FG_LIMITE_PYME')
					}
					return Ext.util.Format.number(value, '0,0.00 %');
				}
			},{//6
				header : 'Monto Disponible M.N.', 
				tooltip: 'Monto Disponible M.N.',
				dataIndex : '', 
				hideable: false, 
				sortable : true, 
				align: 'right',	
				width : 130,	
				hidden: false, 
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					value = '';
					value = record.get('FG_LIMITE_PYME')-record.get('FG_LIMITE_PYME_ACUM');
					return Ext.util.Format.number(value, '$ 0,0.00');
				}
			},
			{//7
				header : 'Monto L�mite DLLS.', 
				tooltip: 'Monto L�mite DLLS.',
				dataIndex : 'FG_LIMITE_PYME_USD', 
				hideable: false, 
				align: 'right',
				sortable : true,	
				width : 120,	
				hidden: false, 
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{//8
				header : '% Utilizaci�n DL.', 
				tooltip: '% de Utilizaci�n DL.',	
				dataIndex : '', 
				hideable: false,
				sortable : true,	
				width : 120,	
				align: 'center',
				hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					value = 0;
					if (record.get('FG_LIMITE_PYME_USD') != 0){
						value = (record.get('FG_LIMITE_PYME_ACUM_USD')*100)/record.get('FG_LIMITE_PYME_USD')
					}
					return Ext.util.Format.number(value, '0,0.00 %');
				}
			},
			{//9
				header : 'Monto Disponible DL.', 
				tooltip: 'Monto Disponible DL.',
				dataIndex : '',
				sortable : true,	
				width : 130,	
				hidden: false, 
				align: 'right',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					value = '';
					if (record.get('FG_LIMITE_PYME_USD') != ""){
						value = record.get('FG_LIMITE_PYME_USD')-record.get('FG_LIMITE_PYME_ACUM_USD');
					}
					return Ext.util.Format.number(value, '$ 0,0.00');
				}
			},
			{//10
				xtype:	'actioncolumn',
				header : 'Seleccionar',
				tooltip: 'Seleccionar',
				dataIndex : '',
				align: 'center',
				sortable : true,	
				width : 80, 
				hidden: false,	//renderer: renderButton
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
								this.items[0].tooltip = 'Modificar';
								return 'modificar';
						},
						handler:	procesaModificar
					},{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
								this.items[1].tooltip = 'Eliminar';
								return 'borrar';
						},
						handler:	procesaEliminar
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		//title: '',
		frame: true,
		bbar: {
			id:	'barGrid',
			items:[
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
					
						boton.setIconClass('loading-indicator');
						var montoLimitedl = null;
						if (jsonValoresIniciales.NAFIN != undefined && jsonValoresIniciales.NAFIN){
							montoLimitedl = jsonValorMonto.montoLimitedl;
							Ext.Ajax.request({
								url: '24forma08extpdf.jsp',
								params: Ext.apply(fp.getForm().getValues(),{informacion: 'ArchivoPDF',montoLimitedl: jsonValorMonto.montoLimitedl}),
								callback: procesarSuccessFailureGenerarPDF
							});
						}else{
							Ext.Ajax.request({
								url: '24forma08extpdf.jsp',
								params: Ext.apply(fp.getForm().getValues(),{informacion: 'ArchivoPDF',montoLimitedl: jsonValoresIniciales.montoLimitedl, ic_epo: jsonValoresIniciales.ses_ic_epo}),
								callback: procesarSuccessFailureGenerarPDF
							});
						}												
					}
				}
			]
		}
	});


	var elementosForma = [	
		/*{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'cboEpo',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_epo',
			emptyText: 'Seleccione EPO',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			hidden:true,
			allowBlank: true,	
			store: catalogoEPODistData,
			tpl: NE.util.templateMensajeCargaCombo,*/
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la EPO',
			id: 'cboEpo',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,
			width: 200,	
			store: catalogoEPODistData,			
			listeners:{
				select:{ 
					fn:function (combo) {
						
						var tipoSistema = Ext.get('sistemaMenu').getValue();	
						
							Ext.Ajax.request({
							url: '24forma08ext.data.jsp',
							params: {
								informacion: "valoresIniciales",
								sistemaMenu:tipoSistema,
								ic_epo: combo.getValue()
							},
							callback: procesaValoresIniciales
						});						
		
						fp.el.mask('Enviando...', 'x-mask-loading');
							catalogoDisData.load({	params: {
								ic_epo: combo.getValue() 
							}	
						});
						
						Ext.Ajax.request({
							url: '24forma08ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ConsultaParametroMonto',
								ic_epo: combo.getValue() 
							}),
							callback: procesaValorMonto
						});
					}	
				}
			}
		},{
			xtype: 'textfield',
			name: 'cg_num_distribuidor',
			id: 'numDist',
			anchor:'65%',
			hidden: true,
			maxLength: 25,
			fieldLabel: 'No. Distribuidor',
			listeners:{
				blur: function(field){
					if (!Ext.isEmpty(field.getValue())){
						fp.el.mask('Procesando...', 'x-mask-loading');
						var claveEpo = "";
						if (jsonValoresIniciales.ses_ic_epo != undefined){
							claveEpo = jsonValoresIniciales.ses_ic_epo;
						}
						Ext.Ajax.request({
							url: '24forma08ext.data.jsp',
							params: {
								informacion: "validarNumDist",
								ic_epo: claveEpo,
								cg_num_distribuidor: field.getValue()
							},
							callback: procesaValidarNumDist
						});
					}
				}
			}
		},{
			xtype: 'combo',
			name: 'ic_pyme',
			id: 'cmbDis',
			allowBlank: true,
			fieldLabel: 'Nombre del Distribuidor',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_pyme',
			emptyText: 'Seleccione distribuidor',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoDisData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'numberfield',
			name: 'fg_limite_pyme',
			id: 'montoLimiteMN',
			allowBlank: false,
			anchor:'65%',
			hidden: true,
			maxLength: 15,
			fieldLabel: 'Monto Limite M.N.',
			listeners:{
				blur: function(field){ 								
					if(field.getValue() != '') {						
						Ext.Ajax.request({
							url: '24forma08ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'validarMontoLimite'
							}),
							callback: procesaValoresLimite
						});			
					}
				}
			}			
		},		
		{
			xtype: 'numberfield',
			name: 'fg_limite_pyme_dl',
			id: 'montoLimiteDL',
			anchor:'65%',
			hidden: true,
			maxLength: 15,
			fieldLabel: 'Monto Limite DLLS.',
			listeners:{
				blur: function(field){ 								
					if(field.getValue() != '') {						
						Ext.Ajax.request({
							url: '24forma08ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'validarMontoLimite'
							}),
							callback: procesaValoresLimite
						});			
					}
				}
			}
		},{
			xtype: 'textfield',
			name: 'cg_mensaje_terminacion',
			id: 'disMsj1',
			allowBlank:	false,
			hidden: true,
			maxLength: 2000,
			fieldLabel: 'Mensaje de terminaci�n<br>de l�mite de l�nea'
		}
	];

	function verificaPanel(){
		var myPanel = Ext.getCmp('forma');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.isVisible())	{
				if (!panelItem.isValid())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}

	var elementosAcuse = [
		{xtype: 'displayfield',	id: 'disAcuse',	name:	'disAcuse',	fieldLabel: 'N�mero de Acuse',value:'',anchor: '100%'},
		{xtype: 'displayfield',	id: 'disFecha',	name:	'disFecha',	fieldLabel: 'Fecha', 			value:'',	anchor: '100%'},
		{xtype: 'displayfield',	id: 'disHora',		name:	'disHora',	fieldLabel: 'Hora', 				value:'',	anchor: '100%'},
		{xtype: 'displayfield',	id: 'disUsuario',	name:	'disUsuario',fieldLabel:'Usuario', 			value:'',		anchor: '100%'}
	];

	var fpAcuse = new Ext.form.FormPanel({
		id: 'formaAcuse',
		width: 400,
		frame: false,
		border: true,
		//style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		monitorValid: false,
		items: elementosAcuse,
		title: 'Recibo: ',
		buttonAlign: 'center',
		buttons: [
			{
				text:'Abrir PDF',	id: 'btnAbrirPDF',iconCls: 'icoPdf'//,// hidden: true
				/*handler: function() {
					var forma = Ext.getDom('formAux');
					forma.action = infoR.urlArchivo;
					forma.submit();
				}*/
			},{
				text:'Regresar',	id: 'btnRegresar',iconCls: 'icoLimpiar',
				handler: function() {
					var tipoSistema = Ext.get('sistemaMenu').getValue();
					if(tipoSistema){
						window.location = '24formaCap08ext.jsp';
					}else{
						window.location = '24forma08ext.jsp';
					}
				}
			}
		]
	});
	
	var confirmarAcuse = function(pkcs7, textoFirmado){
	
		if (Ext.isEmpty(pkcs7) || pkcs7 == 'error:noMatchingCert') {
			Ext.Msg.alert('Firmar','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;	//Error en la firma. Termina...
		
		}else  {
			
			Ext.getCmp('disAcuse').setValue('');
			Ext.getCmp('disFecha').setValue('');
			Ext.getCmp('disHora').setValue('');
			Ext.getCmp('disUsuario').setValue('');
			fp.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url : '24forma08ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Captura',
					pkcs7: pkcs7, 
					textoFirmado: textoFirmado,
					ic_epo: jsonValoresIniciales.ses_ic_epo
				}),
				callback: procesaCaptura
			});
		}
						
	
	}
	

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				id: 'btnAccion',
				text: 'Consultar',
				handler: function(boton, evento) {
					grid.hide();
					if(!verificaPanel()) {
						return;
					}								
									
					if (boton.text != 'Consultar') {
					
						var numeroDist = Ext.getCmp('numDist').getValue();
						var nombreDist = Ext.getCmp('cmbDis').getRawValue();
						var montoLimiteMN = Ext.getCmp('montoLimiteMN');
						var montoLimiteDL = Ext.getCmp('montoLimiteDL');
						var disMsj1 = Ext.getCmp('disMsj1');
											
						if(montoLimiteMN.getValue() <= 0)	{
							montoLimiteMN.markInvalid('Debe capturar un monto mayor a cero');
							montoLimiteMN.focus();
							return;
						}else if ( jsonValoresIniciales.montoLimiteMN != undefined ){
							if ( montoLimiteMN.getValue() > jsonValoresIniciales.montoLimiteMN ){
								montoLimiteMN.markInvalid('El monto excede el Limite de la epo');
								montoLimiteMN.focus();
								return;
							}
						}
						if (!Ext.isEmpty(montoLimiteDL.getValue())){
							if (jsonValoresIniciales.montoLimiteDL != undefined){
								if ( montoLimiteDL.getValue() > jsonValoresIniciales.montoLimiteDL ){
									montoLimiteDL.markInvalid('El monto excede el Limite de la epo');
									montoLimiteDL.focus();
									return;
								}
							}
						}
						var textoFirmado = "No. de distribuidor: "+numeroDist+"\nNombre del distribuidor: "+nombreDist+"\nMonto limite M.N.: "+
												montoLimiteMN.getValue()+"\nMonto limite DLLS.: "+montoLimiteDL.getValue()+"\nMensaje de terminaci�n de limite de linea: "+disMsj1.getValue();
						
						
						NE.util.obtenerPKCS7(confirmarAcuse, textoFirmado );
						
						
						
					}else{
					
						var cboEpo = Ext.getCmp('cboEpo');					
						if(jsonValoresIniciales.strTipoUsuario == 'NAFIN'  && Ext.isEmpty(cboEpo.getValue()) ){
							cboEpo.markInvalid('Debe seleccionar una EPO');
							cboEpo.focus();
							return;
						}
						
						fp.el.mask('Enviando...', 'x-mask-loading');
						if (jsonValoresIniciales.NAFIN != undefined && jsonValoresIniciales.NAFIN){
							Ext.Ajax.request({
								url: '24forma08ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{informacion: 'ConsultaParametroMonto'}),
								callback: procesaValorMonto
							});
						}else{
							Ext.Ajax.request({
								url: '24forma08ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{informacion: 'Consulta',ic_epo: jsonValoresIniciales.ses_ic_epo}),
								callback: procesaValoresConsulta
							});
						}
					}
					
				} //fin handler
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					var tipoSistema = Ext.get('sistemaMenu').getValue();
					if(tipoSistema){
						window.location = '24formaCap08ext.jsp';
					}else{
						window.location = '24forma08ext.jsp';
					}
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			grid
		]
	});

	catalogoDisData.load();
	catalogoEPODistData.load();
	
	var tipoSistema = Ext.get('sistemaMenu').getValue();
	fp.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24forma08ext.data.jsp',
		params: {
			informacion: "valoresIniciales",
			sistemaMenu:tipoSistema
		},
		callback: procesaValoresIniciales
	});
	
	
});