<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	java.text.SimpleDateFormat,
	java.util.Date,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
int start= 0, limit =0;
String infoRegresar ="";
JSONObject 	resultado	= new JSONObject();

try{

	MantenimientoDist mantDist = ServiceLocator.getInstance().lookup("MantenimientoDistEJB", MantenimientoDist.class);

	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

  String NOnegociable =  BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_NO_NEGOCIABLES"); 
	String ventaCartera =BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_VENTA_CARTERA"); 
	String descuentoAutomatico = BeanParametros.DesAutomaticoEpo(iNoCliente,"PUB_EPO_DESC_AUTOMATICO");
  String tipoCredito =  BeanParametros.obtieneTipoCredito (iNoCliente); 
	

	if (informacion.equals("CatalogoPYME") ) {	
		
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cp.cg_razon_social"); 
		cat.setClaveEpo(iNoCliente);	
		cat.setOrden("cp.cg_razon_social");
		infoRegresar = cat.getJSONElementos();
		
	} else  if (informacion.equals("CatalogoMoneda") ) {
	
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setTabla("comcat_moneda");		
		catalogo.setOrden("ic_moneda");
		catalogo.setValoresCondicionIn("1,54", Integer.class);
		infoRegresar = catalogo.getJSONElementos();	
	
	} else  if (informacion.equals("CatalogoEstatus") ) {
	
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus_docto");
		catalogo.setCampoDescripcion("cd_descripcion");
		catalogo.setTabla("comcat_estatus_docto");		
		catalogo.setOrden("ic_estatus_docto");
		catalogo.setValoresCondicionIn("2,9,1", Integer.class);
		infoRegresar = catalogo.getJSONElementos();	
	
		
	
	} else  if (informacion.equals("CatalogoModalidad") ) {  // este es para el combo de la Forma 
	
		JSONArray registros = new JSONArray();
		HashMap info = new HashMap();
		
		Vector combo = mantDist.getVecModalidad(iNoCliente,"0");	
			
		for(int i=0; i<combo.size(); i++){
			List dato = (List)combo.get(i);
			String clave = dato.get(0).toString(); 
			String descripcion = dato.get(1).toString(); 	
			info = new HashMap();
			info.put("clave", clave);
			info.put("descripcion", descripcion);	
			registros.add(info);
		}
		
		infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
	} else  if (informacion.equals("CatalogoModalidadGrid") ) { // este es para el combo del Grid 
	
		JSONArray registros = new JSONArray();
		HashMap info = new HashMap();
		String tipoFinanciamiento = (request.getParameter("tipoFinanciamiento") != null) ? request.getParameter("tipoFinanciamiento") : "";
		String  tipoFinanciamiento2 ="1,2,3,4";
		if(tipoFinanciamiento.equals("1")) tipoFinanciamiento2 ="2,3,4";
		if(tipoFinanciamiento.equals("2")) tipoFinanciamiento2 ="1,3,4";
		if(tipoFinanciamiento.equals("3")) tipoFinanciamiento2 ="1,2,4";
		if(tipoFinanciamiento.equals("4")) tipoFinanciamiento2 ="1,2,3";
		
		Vector combo = mantDist.getVecModalidad(iNoCliente,"0", tipoFinanciamiento2);	
			
		for(int i=0; i<combo.size(); i++){
			List dato = (List)combo.get(i);
			String clave = dato.get(0).toString(); 
			String descripcion = dato.get(1).toString(); 	
			info = new HashMap();
			info.put("clave", clave);
			info.put("descripcion", descripcion);	
			registros.add(info);
		}
		
		infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

	} else  if (informacion.equals("Consultar") ||  informacion.equals("ResumenTotales")  ) {
		
		String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
		String ic_moneda = (request.getParameter("ic_moneda") != null) ? request.getParameter("ic_moneda") : "";
		String modo_plazo = (request.getParameter("modalidad_plazo") != null) ? request.getParameter("modalidad_plazo") : "";
		String ic_estatus_docto = (request.getParameter("ic_estatus_docto") != null) ? request.getParameter("ic_estatus_docto") : "";
		String ig_numero_docto = (request.getParameter("ig_numero_docto") != null) ? request.getParameter("ig_numero_docto") : "";
		String cc_acuse = (request.getParameter("cc_acuse") != null) ? request.getParameter("cc_acuse") : "";
		String fecha_emision_de = (request.getParameter("fecha_emision_de") != null) ? request.getParameter("fecha_emision_de") : "";
		String fecha_emision_a = (request.getParameter("fecha_emision_a") != null) ? request.getParameter("fecha_emision_a") : "";
		String fecha_vto_de = (request.getParameter("fecha_vto_de") != null) ? request.getParameter("fecha_vto_de") : "";
		String fecha_vto_a = (request.getParameter("fecha_vto_a") != null) ? request.getParameter("fecha_vto_a") : "";
		String fecha_publicacion_de = (request.getParameter("fecha_publicacion_de") != null) ? request.getParameter("fecha_publicacion_de") : "";
		String fecha_publicacion_a = (request.getParameter("fecha_publicacion_a") != null) ? request.getParameter("fecha_publicacion_a") : "";
		String fn_monto_de = (request.getParameter("fn_monto_de") != null) ? request.getParameter("fn_monto_de") : "";
		String fn_monto_a = (request.getParameter("fn_monto_a") != null) ? request.getParameter("fn_monto_a") : "";
		String monto_con_descuento = (request.getParameter("monto_con_descuento") != null) ? request.getParameter("monto_con_descuento") : "";
		String solo_cambio = (request.getParameter("solo_cambio") != null) ? request.getParameter("solo_cambio") : "";
		HashMap datos = new HashMap();
		JSONArray registros = new JSONArray();
		 HashMap registrosTot = new HashMap();	
		JSONArray registrosTotales = new JSONArray();
		double	montoTotalMN		=	0;
		double	montoTotalDL		=	0;
		double	montoTotalValMN		=	0;
		double	montoTotalValDL		=	0;
		double	montoTotalDescMN	=	0;
		double	montoTotalDescDL	=	0;
		int  docMN=0, docDL=0;
	
		String consulta  ="";
		
		com.netro.distribuidores.ConsMantenimientoDocto paginador = new com.netro.distribuidores.ConsMantenimientoDocto();
		paginador.setIc_pyme(ic_pyme);
		paginador.setIc_moneda(ic_moneda);
		paginador.setModo_plazo(modo_plazo);
		paginador.setIc_estatus_docto(ic_estatus_docto);
		paginador.setIg_numero_docto(ig_numero_docto);
		paginador.setCc_acuse(cc_acuse);
		paginador.setFecha_emision_de(fecha_emision_de);
		paginador.setFecha_emision_a(fecha_emision_a);
		paginador.setFecha_vto_de(fecha_vto_de);
		paginador.setFecha_vto_a(fecha_vto_a);
		paginador.setFecha_publicacion_de(fecha_publicacion_de);
		paginador.setFecha_publicacion_a(fecha_publicacion_a);
		paginador.setFn_monto_de(fn_monto_de);
		paginador.setFn_monto_de(fn_monto_a);
		paginador.setMonto_con_descuento(monto_con_descuento);
		paginador.setSolo_cambio(solo_cambio);
		paginador.setNOnegociable(NOnegociable);
		paginador.setIc_epo(iNoCliente);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if (informacion.equals("Consultar") ) {	
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);															   		
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		
				resultado = JSONObject.fromObject(consulta);
				infoRegresar = resultado.toString();
			
	}	else if (informacion.equals("ResumenTotales") ) {
		
		queryHelper = new CQueryHelperRegExtJS(paginador); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion
		
	}
	
	} else  if (informacion.equals("Guardar") )  {
	
		String error = ""; 
		String ic_documento []	=	request.getParameterValues("ic_documento");
		String nueva_fecha_docto  []	=	request.getParameterValues("nueva_fecha_docto");
		String nueva_fecha_vto  []	=	request.getParameterValues("nueva_fecha_vto"); 
		String nuevo_monto  []	=	request.getParameterValues("nuevo_monto");
		String nuevo_modo_plazo  []	=	request.getParameterValues("nuevo_modo_plazo");
		String fecha_lim_desc  []	=	request.getParameterValues("fecha_lim_desc");		
		String rs_estatus_docto  []	=	request.getParameterValues("rs_estatus_docto");
		String anterior_monto  []	=	request.getParameterValues("anterior_monto");
		String ses_ic_epo  = iNoCliente;
		String cg_fecha_porc_desc = cargaDocto.getParamFechaPorc(iNoCliente);
		String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
		String textoFirmado = (request.getParameter("textoFirmado") != null) ? request.getParameter("textoFirmado") : "";
	

		String in=""; 
		for( int i=0;i<ic_documento.length;i++){
			if(!"".equals(nueva_fecha_docto[i])||!"".equals(nueva_fecha_vto[i])||!"".equals(nuevo_monto[i])||!"".equals(nuevo_modo_plazo[i])||!"".equals(fecha_lim_desc[i])){
			if(!"".equals(in))
				in+=",";
				in += ic_documento[i];
			}
		}	
	
	try{
	
		mantDist.guardaCambios(ic_documento, nueva_fecha_docto,	nueva_fecha_vto, nuevo_monto, 	nuevo_modo_plazo, 	
														fecha_lim_desc,	cg_fecha_porc_desc, rs_estatus_docto,	anterior_monto,	ses_ic_epo, 
														tipoCredito, ventaCartera, 	descuentoAutomatico);
														
	}catch(NafinException ne){	
		error = ne.getMsgError();
	}catch(Exception e){
		e.printStackTrace();
		error	=e.toString();
	}
	
		JSONObject 	jsonObj	= new JSONObject();
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("documentos",in);	
		jsonObj.put("error",error);
		infoRegresar = jsonObj.toString();	
	
	
} else  if (informacion.equals("ConsultaAcuse") )  {

	String documentos = (request.getParameter("documentos") != null) ? request.getParameter("documentos") : "";
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	
	Vector vecFilas = mantDist.consultaDoctos(iNoCliente,"","","","","","","","","","","","","","","","",documentos,true,NOnegociable);
		for(int i=0;i<vecFilas.size();i++){
			Vector vecColumnas = (Vector)vecFilas.get(i);
			String icDocumento			=	(String)vecColumnas.get(0);
			String distribuidor		=	(String)vecColumnas.get(1);
			String numDocto			=	(String)vecColumnas.get(2);
			String numAcuse			=	(String)vecColumnas.get(3);
			String fechaEmision		=	(String)vecColumnas.get(4);
			String fechaPublicacion	=	(String)vecColumnas.get(5);
			String fechaVencimiento	=	(String)vecColumnas.get(6);
			String plazoDocto			=	(String)vecColumnas.get(7);
			String icMoneda			=	(String)vecColumnas.get(8);
			String moneda				=	(String)vecColumnas.get(9);
			double monto				=	Double.parseDouble(vecColumnas.get(10).toString());
			String tipoConversion		=	(String)vecColumnas.get(11);
			double tipoCambio			=	Double.parseDouble(vecColumnas.get(12).toString());
			String plazoDescuento		=	(String)vecColumnas.get(13);
			String porcDescuento		=	(String)vecColumnas.get(14);
			double montoDescontar		=	Double.parseDouble(vecColumnas.get(15).toString());
			String icTipoFinanciamiento=	(String)vecColumnas.get(16);
			String modoPlazo			=	(String)vecColumnas.get(17);
			String estatus				=	(String)vecColumnas.get(18);
			String fechaModificacion	=	(String)vecColumnas.get(19);
			double montoValuado 		= 	(monto-montoDescontar)*tipoCambio;
			String antFechaDocto		=	(String)vecColumnas.get(20);
			String antFechaVto			= 	(String)vecColumnas.get(22);
			String antMonto 			=	(String)vecColumnas.get(24);
			String antModoPlazo		=	(String)vecColumnas.get(26);
			String bandeVentaCartera = (String)vecColumnas.get(30);
			if (bandeVentaCartera.equals("S") ) {
				modoPlazo ="";
			}
			
				datos.put("NOMBRE_PYME",distribuidor);	
				datos.put("NO_DOCUMENTO",numDocto);	
				datos.put("IC_DOCUMENTO",icDocumento);
				datos.put("ACUSE",numAcuse);
				datos.put("FECHA_EMISION",fechaEmision);
				datos.put("FECHA_PUBLICACION",fechaPublicacion);
				datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
				datos.put("PLAZO_DOCTO",plazoDocto);
				datos.put("MONEDA",moneda);
				datos.put("MONTO",Double.toString (monto));				
				datos.put("PLAZO_DESC",plazoDescuento);
				datos.put("PORC_DESC",porcDescuento);
				datos.put("MONTO_DESC",Double.toString (montoDescontar));
				datos.put("MODA_PLAZO",modoPlazo);
				datos.put("ESTATUS",estatus);
				datos.put("FECHA_MODIFICACION",fechaModificacion);
				datos.put("FECHA_ANTERIOR_EMISION",antFechaDocto);
				datos.put("FECHA_ANTERIOR_VENCIMIENTO",antFechaVto);
				datos.put("ANTERIOR_MONTO",antMonto);
				datos.put("FECHA_LIMITE_DESC","");			
				datos.put("ANTERIOR_MODO_PLAZO",antModoPlazo);				
				datos.put("MONTO_ANTERIOR",Double.toString (monto));
				datos.put("RS_ESTATUS_DOCTO","");
				datos.put("TIPO_FINANCIAMIENTO",icTipoFinanciamiento);
				datos.put("TIPO_CONVERSION",tipoConversion);
				datos.put("VENTA_CARTERTA",bandeVentaCartera);				
				registros.add(datos);
				
		}//for

		
			String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
			resultado = JSONObject.fromObject(consulta);
			infoRegresar = resultado.toString();

	
}else if (informacion.equals("obtenCambioDoctos")) {

	String ic_documento = (request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");

	if(ic_documento != null && !ic_documento.equals("")) {
		DetalleCambiosDoctos dataCambios = new DetalleCambiosDoctos();
		dataCambios.setIcDocumento(ic_documento);
		Registros registros = dataCambios.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}
}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	out.println("Error: "+ e);
}

%>
<%=infoRegresar%>



