
Ext.onReady(function() {

	var ic_documento  = [];
	var nueva_fecha_docto  = [];
	var nueva_fecha_vto  = [];
	var nuevo_monto  = [];
	var nuevo_modo_plazo  = [];
	var fecha_lim_desc  = [];
	var cg_fecha_porc_desc;
	var rs_estatus_docto  = [];
	var anterior_monto  = [];
	
	var cancelar =  function() { 
	 ic_documento  = [];
	 nueva_fecha_docto  = [];
	 nueva_fecha_vto  = [];
	 nuevo_monto  = [];
	 nuevo_modo_plazo  = [];
	 fecha_lim_desc  = [];
	 cg_fecha_porc_desc;
	 rs_estatus_docto  = [];
	 anterior_monto  = [];	
	}
		

	var NOnegociable =  Ext.getDom('NOnegociable').value; 
	var ventaCartera =Ext.getDom('ventaCartera').value;
	var descuentoAutomatico = Ext.getDom('descuentoAutomatico').value;
  var tipoCredito =  Ext.getDom('tipoCredito').value;
	var fechaHoy =  Ext.getDom('fechaHoy').value;
	var dias_amp_fecha_venc =  Ext.getDom('dias_amp_fecha_venc').value;
	
	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
		
	
	//esta funcion  validad la captura de la Nueva fecha emisi�n
	var validaFechaEmi =  function( opts, success, response, registro) { 	
		var nueva_fecha_docto = Ext.util.Format.date(registro.data['FECHA_NUEVA_EMISION'],'d/m/Y'); 		
		if (descuentoAutomatico == 'S' &&  ventaCartera=='S') 	{
			var resultado = datecomp(nueva_fecha_docto ,fechaHoy);						
			if (resultado == 2 || resultado==0) 	{
				Ext.MessageBox.alert("Mensaje","La  Nueva Fecha de Emision del documento debe ser posterior al dia de hoy");					
				return false ;
			}	
		}
	}
	
	//es esta funcion es para validar la Nueva fecha vencimiento 
	var validaFechaVenc =  function( opts, success, response, registro) { 	
		var nueva_fecha_Venc =  Ext.util.Format.date(registro.data['FECHA_NUEVA_VENCIMIENTO'],'d/m/Y');   
		var fechaVencAnt  = Ext.util.Format.date(registro.data['FECHA_VENCIMIENTO'],'d/m/Y');  
		if(nueva_fecha_Venc!="") {		
			if(dias_amp_fecha_venc=="") {
				Ext.MessageBox.alert("Mensaje","No esta parametrizado los D�as M�ximos para ampliar la Fecha de Vencimiento.");
				return false ;
			}
			//Funcion que compara dos fechas (dd/mm/aa)
			//Regresa:
			//  0 si son iguales.
			//  1 si la primera es mayor que la segunda
			//  2 si la segunda mayor que la primera
			// -1 si son fechas invalidas
			var resultado = datecomp(nueva_fecha_Venc ,fechaHoy);
			if(resultado == 2 || resultado == 0) {
				Ext.MessageBox.alert("Mensaje","La nueva fecha de vencimiento debe ser mayor al dia de hoy");
				return false;
			}
			var TDate = new Date();
			TDate.setDate(parseInt(fechaVencAnt.substring(0,2),10));
			TDate.setMonth(parseInt(fechaVencAnt.substring(3,5),10)-1);
			TDate.setYear(parseInt(fechaVencAnt.substring(6,10),10));							
			TDate.setDate(TDate.getDate()+parseInt(dias_amp_fecha_venc,10));
			
			var CurYear = TDate.getYear();
			var CurDay = TDate.getDate();
			var CurMonth = TDate.getMonth()+1;
			if(CurDay<10)
				CurDay = '0'+CurDay;
			if(CurMonth<10)
				CurMonth = '0'+CurMonth;
				TheDate = CurDay +'/'+CurMonth+'/'+CurYear;
				resultado = datecomp(nueva_fecha_Venc ,TheDate);
			if(resultado == 1) {
				Ext.MessageBox.alert("Mensaje","La fecha de vencimiento definida, excede al par�metro D�as m�ximos para ampliar Fecha de Vencimiento "+dias_amp_fecha_venc);
				return false ;
			}			
		}
	}
	
	var validaNuevoModaplazo =  function( opts, success, response, registro) { 		
		var ModalidadPlazoAnterior  = registro.data['TIPO_FINANCIAMIENTO'];
		var ModalidadPlazoNuevo  = registro.data['NUEVO_MODO_PLAZO'];
		if(ModalidadPlazoAnterior ==ModalidadPlazoNuevo){
			Ext.MessageBox.alert("Mensaje","No puede elegir la misma Modalidad de Plazo ");			
			return false; 
		}

	}
	
	//es esta funcion es para validar la Fecha l�mite de descuento
	var validaFechaLim =  function( opts, success, response, registro) { 		
		var fecemi  = Ext.util.Format.date(registro.data['FECHA_EMISION'],'d/m/Y');  
		var feclim =  Ext.util.Format.date(registro.data['FECHA_LIMITE_DESC'],'d/m/Y');  
		var fhoy = new Date();
		var femi = new Date();
		var flim = new Date();
		femi.setDate(parseInt(fecemi.substring(0,2),10)); 
		femi.setMonth(parseInt(fecemi.substring(3,5),10)-1); 
		femi.setYear(parseInt(fecemi.substring(6,10),10));
		flim.setDate(parseInt(feclim.substring(0,2),10)); 
		flim.setMonth(parseInt(feclim.substring(3,5),10)-1); 
		flim.setYear(parseInt(feclim.substring(6,10),10));
		
		if(femi >= flim){
			Ext.MessageBox.alert("Mensaje","La nueva fecha de descuento debe ser mayor a la fecha de emisi�n");			
			return false ;
		} else if(fhoy >= flim){
			Ext.MessageBox.alert("Mensaje","La nueva fecha de descuento debe ser mayor a la fecha actual");
			return false;
		} else {
			if(feclim!=''){
				Ext.MessageBox.alert("Mensaje","Al cambiar la fecha l�mite para tener descuento, no se modifcar� el porcentaje de descuento, seguir�n siendo los definidos en la captura inicial");			
			}
		}
	}
	
	var procesarSuccessFailureGuardar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				jsondeAcuse = Ext.util.JSON.decode(response.responseText);
				var documentos;	
				var error;
				if (jsondeAcuse != null){
					documentos= jsondeAcuse.documentos;	
					error= jsondeAcuse.error;	
					
					if(documentos !=null & error =='') {
						//consulta para los registros con 
						consultaAcuseData.load({
							params: {
							informacion: 'ConsultaAcuse',
							documentos:documentos	
							}
						});			
					}else {
						Ext.MessageBox.alert("Mensaje",error);	
					}
					
				}						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var confirmarAcuse = function(pkcs7, textoFirmar, vic_documento , vnueva_fecha_docto ,   vnueva_fecha_vto , vnuevo_monto , vnuevo_modo_plazo , vfecha_lim_desc , vrs_estatus_docto , vanterior_monto ){
	
			
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
			
		}else  {
			
			Ext.Ajax.request({
				url : '24forma03Ext.data.jsp',
				params : {
					pkcs7: pkcs7,
					textoFirmado: textoFirmar,
					informacion: 'Guardar',
					ic_documento: vic_documento,
					nueva_fecha_docto: vnueva_fecha_docto,
					nueva_fecha_vto: vnueva_fecha_vto,
					nuevo_monto: vnuevo_monto,
					nuevo_modo_plazo: vnuevo_modo_plazo,
					fecha_lim_desc: vfecha_lim_desc	,	
					rs_estatus_docto: vrs_estatus_docto,
					anterior_monto: vanterior_monto
				},
				callback: procesarSuccessFailureGuardar
			});	
		}				
	
	}
	
	
	
	
	var guardarCambios = function() {
	
		var  gridEditable = Ext.getCmp('gridEditable');
		var store = gridEditable.getStore();	
		var modificados = store.getModifiedRecords();
		var columnModelGrid = gridEditable.getColumnModel();
		var confirmacion = false;
		var nohayModificados = false;
		cancelar(); // limpia las variable 
		
		if (modificados.length ==0){
			Ext.MessageBox.alert("Mensaje","Debe capturar por lo menos un cambio para continuar");
			return false;
		}else if (modificados.length > 0) {
			//validacion de registros modificados...
			Ext.each(modificados, function(record) {
				var nueva_fecha_docto1 = Ext.util.Format.date(record.data['FECHA_NUEVA_EMISION'],'d/m/Y');  
				var nueva_fecha_vto1 = Ext.util.Format.date(record.data['FECHA_NUEVA_VENCIMIENTO'],'d/m/Y'); 			
				var nuevo_monto1 = record.data['NUEVO_MONTO'];		
				var nuevo_modo_plazo1 = record.data['NUEVO_MODO_PLAZO'];		
				var fechalim = Ext.util.Format.date(record.data['FECHA_LIMITE_DESC'],'d/m/Y');			
				var rs_estatus_docto1 = record.data['IC_ESTATUS'];	
				var icTipoFinanciamiento = record.data['IC_TIPO_FINANCIAMIENTO'];
				if(nuevo_monto1==0){ nuevo_monto1=''; }
			
				if(nueva_fecha_docto1=='' && nueva_fecha_vto1==''&& nuevo_monto1=='' && nuevo_modo_plazo1==''	&& fechalim==''){
					nohayModificados= true;						
					Ext.MessageBox.alert("Mensaje"," Debe capturar por lo menos un cambio para continuar");
					return false;					
				}
				
				if(nueva_fecha_vto1!='' && rs_estatus_docto1== 9 && (icTipoFinanciamiento ==1|| icTipoFinanciamiento ==4) && !(nuevo_modo_plazo1==2||nuevo_modo_plazo1==3)) {
					nohayModificados= true;						
					Ext.MessageBox.alert("Mensaje","Debe modificar la Modalidad de Plazo a Mixto o Ampliado");
					return false;					
				}				
				if (descuentoAutomatico == 'S' &&  ventaCartera=='S') 	{
					var resultado = datecomp(nueva_fecha_docto1 ,fechaHoy);
					if (resultado == 2 || resultado==0) 	{
						nohayModificados= true;
						Ext.MessageBox.alert("Mensaje","La fecha de Nueva de Emision del documento debe ser posterior al dia de hoy");					
						return false;						
					}	
				}		
				
				var ModalidadPlazoAnterior  = record.data['TIPO_FINANCIAMIENTO'];
				var ModalidadPlazoNuevo  = record.data['NUEVO_MODO_PLAZO'];
				if(ModalidadPlazoAnterior ==ModalidadPlazoNuevo){
					Ext.MessageBox.alert("Mensaje","No puede elegir la misma Modalidad de Plazo ");	
					nohayModificados= true;
					return false; 
				}
		
				ic_documento.push(record.data['IC_DOCUMENTO']);
				nueva_fecha_docto.push(Ext.util.Format.date(record.data['FECHA_NUEVA_EMISION'],'d/m/Y'));  
				nueva_fecha_vto.push(Ext.util.Format.date(record.data['FECHA_NUEVA_VENCIMIENTO'],'d/m/Y')); 
				if(record.data['NUEVO_MONTO'] !=0){
					nuevo_monto.push(record.data['NUEVO_MONTO']);
				}
				nuevo_modo_plazo.push(record.data['NUEVO_MODO_PLAZO']);
				fecha_lim_desc.push(Ext.util.Format.date(record.data['FECHA_LIMITE_DESC'],'d/m/Y'));		
				rs_estatus_docto.push(record.data['IC_ESTATUS']);
				anterior_monto.push(record.data['MONTO']);
			
			});
			
			var textoFirmar = "Numero de documentos a modificar:"+modificados.length+"\n";
								
			if(nohayModificados==false) {
				if(confirm("�Esta seguro de querer realizar los cambios?")) {
					confirmacion =true;
				}else{
					confirmacion =false;
					return false 
				}
			}
			if(confirmacion==true) {
			
							
				NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar, ic_documento , nueva_fecha_docto ,   nueva_fecha_vto , nuevo_monto , nuevo_modo_plazo , fecha_lim_desc , rs_estatus_docto , anterior_monto    );
				
			}
		}				
	}
	

	var procesarCambiosDocumentos = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var ic_documento = registro.get('IC_DOCUMENTO');
		cambioDoctosData.load({params:	{ic_documento:	ic_documento}});
		var ventana = Ext.getCmp('cambioDoctos');
		if (ventana) {
			ventana.destroy();
		}
		new Ext.Window({
			layout: 'fit',
			width: 800,
			height: 200,
			id: 'cambioDoctos',
			closeAction: 'hide',
			items: [
				gridCambiosDoctos
			],
			title: 'Documento'
			}).show();
	}
		
	var procesarResumenTotalesData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}			
			var el = gridTotales.getGridEl();			
			if(store.getTotalCount() > 0) {				
				el.unmask();
			
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
		
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridEditable.isVisible()) {
				gridEditable.show();
			}
						
			var el = gridEditable.getGridEl();			
			if(store.getTotalCount() > 0) {				
					resumenTotalesData.load();						
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarConsultaAcuseData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridAcuse.isVisible()) {
				gridAcuse.show();
			}
			var el = gridAcuse.getGridEl();			
			if(store.getTotalCount() > 0) {	
				gridEditable.hide();
				gridTotales.hide();
				fp.hide();
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}	
	}
	
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma03Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma03Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoEstatusData = new Ext.data.JsonStore({
		id: 'catalogoEstatusDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma03Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var catalogoModalidadData = new Ext.data.JsonStore({
		id: 'catalogoModalidadDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma03Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoModalidad'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var catalogoModalidadGridData = new Ext.data.JsonStore({
		id: 'catalogoModalidadDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma03Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoModalidadGrid'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var procesarCambioDoctosData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var gridCambios = Ext.getCmp('gridCambios');
			var el = gridCambios.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ningun cambio para este documento', 'x-mask');
			}
		}
	}
	
	var cambioDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma03Ext.data.jsp',
		baseParams: {informacion: 'obtenCambioDoctos'},
		fields: [
			{name: 'FECH_CAMBIO', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CD_DESCRIPCION'},
			{name: 'FECH_EMI_ANT', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECH_EMI_NEW', 			type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_MONTO_ANTERIOR',	type: 'float'},
			{name: 'FN_MONTO_NUEVO', 		type: 'float'},
			{name: 'FECH_VENC_ANT', 		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECH_VENC_NEW', 		type: 'date', dateFormat: 'd/m/Y'},
			{name: 'MODO_PLAZO_ANTERIOR'},
			{name: 'MODO_PLAZO_NUEVO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCambioDoctosData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarCambioDoctosData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});
	
	
		var gridCambiosDoctos = {
		xtype: 'grid',
		store: cambioDoctosData,
		id: 'gridCambios',
		columns: [
			{
				header: 'Fecha del Cambio',
				dataIndex: 'FECH_CAMBIO',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Cambio Estatus',
				dataIndex: 'CD_DESCRIPCION',		//CT_CAMBIO_MOTIVO
				align: 'left',	width: 200
			},{
				header: 'Fecha Emision Anterior',
				dataIndex: 'FECH_EMI_ANT',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Fecha Emision Nueva',
				dataIndex: 'FECH_EMI_NEW',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Monto Anterior',
				dataIndex: 'FN_MONTO_ANTERIOR',
				align: 'right',	width: 120,	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto Nuevo',
				dataIndex: 'FN_MONTO_NUEVO',
				align: 'right',	width: 120,	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Fecha Vencimiento Anterior',
				dataIndex: 'FECH_VENC_ANT',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Fecha Vencimiento Nuevo',
				dataIndex: 'FECH_VENC_NEW',
				align: 'left',	width: 100, renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header: 'Modalidad de Plazo Anterior',
				dataIndex: 'MODO_PLAZO_ANTERIOR',
				align: 'left',	width: 150
			},{
				header: 'Modalidad de Plazo Nuevo',
				dataIndex: 'MODO_PLAZO_NUEVO',
				align: 'left',	width: 150
			}
		],
		height: 300,
		title: '',
		frame: false,
		loadMask: true
	};
	
	
	
	var resumenTotalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24forma03Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'		
		},								
		fields: [			
			{name: 'IC_MONEDA'},		
			{name: 'MONEDA' },			
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONT0' },			
			{name: 'TOTAL_MONTO_VALUADO'},	
			{name: 'TOTAL_MONTO_DESCUENTO'}		
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
		load: procesarResumenTotalesData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarResumenTotalesData(null, null, null);						
				}
			}
		}		
	});	
	
	var gridTotales = new Ext.grid.EditorGridPanel({
		id: 'gridTotales',
		store: resumenTotalesData,	
		margins: '20 0 0 0',
		align: 'center',
		title: 'Totales',		
		hidden: true,
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 190,
				align: 'left'			
			},	
			{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTAL_REGISTROS',
				width: 190,
				align: 'center'				
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'TOTAL_MONT0',
				width: 190,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			{							
				header : 'Monto total Monto % de descuento.',			
				dataIndex : 'TOTAL_MONTO_DESCUENTO',
				width : 190,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			}	,
			{
				header: 'Monto total valuado en pesos',
				dataIndex: 'TOTAL_MONTO_VALUADO',
				width: 190,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			}						
		],
		height: 120,	
		width : 943,
		frame: false
	});
		
		
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma03Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_PYME'},
			{name: 'NO_DOCUMENTO'},
			{name: 'IC_DOCUMENTO'},			
			{name: 'ACUSE'},
			{name: 'FECHA_EMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENCIMIENTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO_DOCTO'},					
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'CATEGORIA'},
			{name: 'PLAZO_DESC'},
			{name: 'PORC_DESC'},
			{name: 'MONTO_DESC'},
			{name: 'MODA_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'FECHA_MODIFICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_NUEVA_EMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_NUEVA_VENCIMIENTO'},
			{name: 'NUEVO_MONTO'},
			{name: 'FECHA_LIMITE_DESC', type: 'date', dateFormat: 'd/m/Y'},		
			{name: 'NUEVO_MODO_PLAZO'},	
			{name: 'MONTO_ANTERIOR'},		
			{name: 'IC_ESTATUS'},
			{name: 'TIPO_FINANCIAMIENTO'},
			{name: 'IC_TIPO_FINANCIAMIENTO'},
			{name: 'TIPO_CONVERSION'},
			{name: 'VENTA_CARTETA'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	
	
	
	var consultaAcuseData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma03Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaAcuse'
		},
		fields: [
			{name: 'NOMBRE_PYME'},
			{name: 'NO_DOCUMENTO'},
			{name: 'IC_DOCUMENTO'},
			{name: 'ACUSE'},
			{name: 'FECHA_EMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_PUBLICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENCIMIENTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PLAZO_DESC'},
			{name: 'PORC_DESC'},
			{name: 'MONTO_DESC'},
			{name: 'MODA_PLAZO'},
			{name: 'ESTATUS'},
			{name: 'FECHA_MODIFICACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_ANTERIOR_EMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_ANTERIOR_VENCIMIENTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'ANTERIOR_MONTO'},
			{name: 'FECHA_LIMITE_DESC', type: 'date', dateFormat: 'd/m/Y'},		
			{name: 'ANTERIOR_MODO_PLAZO'},	
			{name: 'MONTO_ANTERIOR'},		
			{name: 'IC_ESTATUS'},
			{name: 'TIPO_FINANCIAMIENTO'},
			{name: 'IC_TIPO_FINANCIAMIENTO'},			
			{name: 'TIPO_CONVERSION'},
			{name: 'VENTA_CARTETA'}				
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaAcuseData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaAcuseData(null, null, null);						
				}
			}
		}
	});	
	
	
	var gridAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridAcuse',
		title: 'Mantenimiento Documentos',
		hidden: true,
		store: consultaAcuseData,
		clicksToEdit: 1,		
		columns: [			
			{
				header: 'Distribuidor', 
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			}	, 
			{
				header: 'N�mero de documento inicial', 
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			}	, 
			{
				header: 'Num. acuse carga',
				tooltip: 'Num. acuse carga',
				dataIndex: 'ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			}	, 
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	, 
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	, 
			{
				header: 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	, 
			{
				header: 'Plazo docto',
				tooltip: 'Plazo docto',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			}	, 
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			}	, 
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	, 
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'PLAZO_DESC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			}	, 
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'PORC_DESC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			}	, 
			{
				header: 'Monto a descontar',
				tooltip: 'Monto a descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			}	, 
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODA_PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			}	, 
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			}	, 	
			{
				header: 'Fecha de modificacion',
				tooltip: 'Fecha de modificacion',
				dataIndex: 'FECHA_MODIFICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	,
			{				
				header : 'Fecha emisi�n anterior', 
				tooltip: 'Fecha emisi�n anterior',
				dataIndex : 'FECHA_ANTERIOR_EMISION',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',					
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Fecha vencimiento anterior', 
				tooltip: ' Fecha vencimiento anterior',
				dataIndex : 'FECHA_ANTERIOR_VENCIMIENTO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',					
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Monto anterior',
				tooltip: 'Monto anterior',
				dataIndex: 'ANTERIOR_MONTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,		
				align: 'center',			
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},				
			{
				header: 'Modalidad de Plazo anterior',
				tooltip: 'Modalidad de Plazo anterior',
				dataIndex: 'ANTERIOR_MODO_PLAZO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,		
				align: 'center'	
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		align: 'center',
		frame: false,
			bbar: {
			xtype: 'toolbar',
			items: [								
				'->',
				'-',			
				{
					text: 'Regresar',
					xtype: 'button',
					id: 'btnRegresarr',
					handler: function() {						
						window.location = '24forma03Ext.jsp';
					}
				}
				]
			}
	});

		
	//para la creacion del combo en el grid
	var comboModalidad = new Ext.form.ComboBox({  
    triggerAction : 'all',  
    displayField : 'descripcion',  
    valueField : 'clave', 
		typeAhead: true,
		width: 250,
		minChars : 1,
		allowBlank: true,
    store :catalogoModalidadGridData 
});

	var gridEditable = new Ext.grid.EditorGridPanel({
		id: 'gridEditable',
		title: 'Mantenimiento Documentos',
		hidden: true,
		store: consultaData,
		clicksToEdit: 1,		
		columns: [			
			{
				header: 'Distribuidor', 
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			}	, 
			{
				header: 'N�mero de documento inicial', 
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			}	, 
			{
				header: 'Num. acuse carga',
				tooltip: 'Num. acuse carga',
				dataIndex: 'ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			}	, 
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	, 
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	, 
			{
				header: 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	, 
			{
				header: 'Plazo docto',
				tooltip: 'Plazo docto',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			}	, 
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				
			}	, 
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	, 
			{
				header: 'Categoria',
				tooltip: 'Categoria',
				dataIndex: 'CATEGORIA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'			
			}	, 
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'PLAZO_DESC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			}	, 
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'PORC_DESC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			}	, 
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			}	, 			
			{
				header:'Modalidad de plazo', 
				tooltip: 'Modalidad de plazo',
				dataIndex:'TIPO_FINANCIAMIENTO', 
				sortable: true,
				resizable: true	,
				width: 130,
				sortable: true			
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			
			}	, 
			{
				xtype: 'actioncolumn',
				header: 'Cambios del documento',
				tooltip: 'Cambios del documento',
				dataIndex: '',
        width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {								
							this.items[0].tooltip = 'Ver';								
							return 'iconoLupa';											
						},
						handler: procesarCambiosDocumentos
					}
				]	
			},
			{
				header: 'Fecha de modificacion',
				tooltip: 'Fecha de modificacion',
				dataIndex: 'FECHA_MODIFICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	,
			{				
				header : 'Nueva fecha emisi�n', 
				tooltip: 'Nueva fecha emisi�n',
				dataIndex : 'FECHA_NUEVA_EMISION',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: campoFechas,		
				renderer:function(value,metadata,registro){
					 return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);
				}
			},
			{				
				header : 'Nueva fecha vencimiento', 
				tooltip: 'Nueva fecha vencimienton',
				dataIndex : 'FECHA_NUEVA_VENCIMIENTO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: campoFechas,	
				renderer:function(value,metadata,registro){
					 return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);
				}				
			},
			{
				header: 'Nuevo monto',
				tooltip: 'Nuevo monto',
				dataIndex: 'NUEVO_MONTO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,		
				align: 'center',
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: true
				},				
				renderer:function(value,metadata,registro){					
					 return NE.util.colorCampoEdit(Ext.util.Format.number(value,'$0,0.00'),metadata,registro);
				}    				
			},				
			{
				header: 'Modalidad de Plazo',
				tooltip: 'Modalidad de Plazo',
				dataIndex: 'NUEVO_MODO_PLAZO',
				sortable: true,
				resizable: true,
				width: 200,
				hidden: false,		
				align: 'center',
				editor:comboModalidad,
				renderer: function(value, metadata, record, rowindex, colindex, store) { // esto es para colocar la descripcion del combo y no el id
					var valor = value -1;
					if(value!=''){																											
							var record = catalogoModalidadGridData.getAt(valor);  
							return record.get('descripcion'); 					
						}else {
							return value;  
						}
				},
				scope: this
			},			 
			{				
				header : 'Fecha l�mite de descuento', 
				tooltip: 'Fecha l�mite de descuento',
				dataIndex : 'FECHA_LIMITE_DESC',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: campoFechas,		
				renderer:function(value,metadata,registro){
					 return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);
				} 
			}		
		],		
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridEditable = e.gridEditable; 
				var campo= e.field;
				if(campo == 'FECHA_NUEVA_EMISION'){
					var rs_estatus_docto =record.data['IC_ESTATUS'];										
					if(rs_estatus_docto=='9' ){						
						return false; // no es editable 		 false 				
					}else {		
						return true; // es editable 					true
					}
				}
				var tipoFinanciamiento = record.data['IC_TIPO_FINANCIAMIENTO'] ;
				
				if(campo == 'NUEVO_MODO_PLAZO'){
					var banderaVentacartera =record.data['VENTA_CARTETA'];										
					if(ventaCartera=='S'  || banderaVentacartera=='S' ){						
						return false; // no es editable 						
					}else {	
					
					catalogoModalidadGridData.load({
						params: {  tipoFinanciamiento: tipoFinanciamiento 	}
					});
										
						return true; // es editable 					
					}
				}
				
			}	,	
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
				var record = e.record;
				var gridEditable = e.gridEditable; 
				var campo= e.field;
				var rs_estatus_docto =record.data['IC_ESTATUS'];
			
				//cuando se captura el FECHA_NUEVA_EMISION 
				if(campo=='FECHA_NUEVA_EMISION') { 
					validaFechaEmi('', '', '', record); 				
				} 
				
				//cuando se captura el FECHA_NUEVA_VENCIMIENTO 
				if(campo=='FECHA_NUEVA_VENCIMIENTO') {  
					if(rs_estatus_docto=='9' ){		
						validaFechaVenc('', '', '', record); 						
					} 
				}
				
				//cuando se capruta la FECHA_LIMITE_DESC
				if(campo=='FECHA_LIMITE_DESC') {  					
					validaFechaLim('', '', '', record);																			
				}
				
				//valida que no sea seleccionado la misma modalidad de plazo con la que ya cuenta el documento
				if(campo=='NUEVO_MODO_PLAZO') {  					
					validaNuevoModaplazo('', '', '', record);																			
				}
				
									
			}
		},
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,	
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",					
			items: [								
				'->',
				'-',				
				{
					xtype: 'button',
					id: 'btnGuardar',
					text: 'Guardar Cambios',
					handler: guardarCambios					
				},
				'-',	
				{
					text: 'Cancelar',
					xtype: 'button',
					id: 'btnCancelar',
					handler: function() {						
						window.location = '24forma03Ext.jsp';
					}
				}
				]
			}
	});
	
	
	var elementosForma = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Distribuidor',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'combo',
					name: 'ic_pyme',
					id: 'ic_pyme1',
					fieldLabel: 'Distribuidor',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_pyme',
					emptyText: 'Seleccione...',
					forceSelection : true,
					triggerAction : 'all',
					width: 250,
					typeAhead: true,
					minChars : 1,
					store : catalogoPYMEData,
					tpl:'<tpl for=".">' +
					'<tpl if="!Ext.isEmpty(loadMsg)">'+
					'<div class="loading-indicator">{loadMsg}</div>'+
					'</tpl>'+
					'<tpl if="Ext.isEmpty(loadMsg)">'+
					'<div class="x-combo-list-item">' +
					'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
					'</div></tpl></tpl>'				
				},
				{
					xtype: 'displayfield',
					value: 'Moneda:',
					width: 100
				},
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 245,
					minChars : 1,
					allowBlank: true,
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Estatus',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'ic_estatus_docto',
					id: 'ic_estatus_docto1',
					fieldLabel: 'Estatus',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_estatus_docto',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 250,
					minChars : 1,
					allowBlank: true,
					store : catalogoEstatusData,
					tpl : NE.util.templateMensajeCargaCombo
				},
				{
					xtype: 'displayfield',
					value: 'Monto documento',
					width: 100
				},
				{
					xtype: 'numberfield',
					name: 'fn_monto_de',
					id: 'fn_monto_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',				
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'numberfield',
					name: 'fn_monto_a',
					id: 'fn_monto_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',					
					margins: '0 20 0 0'  
				}
			]
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. documento inicial',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'ig_numero_docto',
					id: 'ig_numero_docto1',
					fieldLabel: 'Num. documento inicial',
					allowBlank: true,
					hidden: false,
					maxLength: 15,	
					width: 250,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'checkbox',
					name: 'monto_con_descuento',
					id: 'monto_con_descuento1',
					fieldLabel: 'Monto % de descuento',
					allowBlank: true,
					hidden: false,					
					msgTarget: 'side',
					margins: '0 20 0 0',
					align: 'center'
				},
				{
					xtype: 'displayfield',
					value: 'Monto % de descuento',
					width: 200,
					align: 'left'
				}				
			]
		},			
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. acuse de carga',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'cc_acuse',
					id: 'cc_acuse1',
					fieldLabel: 'Num. acuse de carga',
					allowBlank: true,
					hidden: false,
					maxLength: 15,	
					width: 250,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				},				
				{
					xtype: 'checkbox',
					name: 'solo_cambio',
					id: 'solo_cambio1',
					fieldLabel: 'Solo documentos con cambios',
					allowBlank: true,
					hidden: false,					
					msgTarget: 'side',
					margins: '0 20 0 0',
					align: 'center'
				},
				{
					xtype: 'displayfield',
					value: 'Solo documentos con cambios',
					width: 200,
					align: 'left'
				}			
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha emisi�n docto',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_emision_de',
					id: 'fecha_emision_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_emision_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_emision_a',
					id: 'fecha_emision_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_emision_de1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'Modalidad de plazo:',
					width: 100
				},
				
				{
					xtype: 'combo',
					name: 'modalidad_plazo',
					id: 'modalidad_plazo1',
					fieldLabel: 'Estatus',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'modalidad_plazo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 250,
					minChars : 1,
					allowBlank: true,
					store : catalogoModalidadData,
					tpl : NE.util.templateMensajeCargaCombo
				}			
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha vencimiento docto',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_vto_de',
					id: 'fecha_vto_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_vto_a1',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_vto_a',
					id: 'fecha_vto_a',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_vto_de1',
					margins: '0 20 0 0'  
				}										
				]
			},
			{
			xtype: 'compositefield',
			fieldLabel: 'Fecha publicaci�n de',
			combineErrors: false,
			msgTarget: 'side',
			width: 200,
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_de',
					id: 'fecha_publicacion_de1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_publicacion_a1',					
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_a',
					id: 'fecha_publicacion_a1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_publicacion_de1',
					margins: '0 20 0 0' 					
				}
			]
		}			
	];
	
	
	var fp = new Ext.form.FormPanel({
			id: 'forma',
			width: 885,
			title: 'Criterios de B�squeda',
			frame: true,
			collapsible: true,
			titleCollapse: false,
			style: 'margin:0 auto;',
			bodyStyle: 'padding: 6px',
			labelWidth: 170,
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			items: elementosForma,			
			monitorValid: true,
			buttons: [
				{
					text: 'Buscar',
					iconCls: 'icoBuscar',
					formBind: true,
					handler: function(boton, evento) {
					
						var fecha_emision_de = Ext.getCmp("fecha_emision_de1");
						var fecha_emision_a = Ext.getCmp("fecha_emision_a1");					
						if (!Ext.isEmpty(fecha_emision_de.getValue())   &&  Ext.isEmpty(fecha_emision_a.getValue()) ) {
								fecha_emision_de.markInvalid('Debe capturar ambas fechas de emision o dejarlas en blanco');	
								fecha_emision_a.markInvalid('Debe capturar ambas fechas de emision o dejarlas en blanco');	
								return;
						}else if (Ext.isEmpty(fecha_emision_de.getValue())   &&  !Ext.isEmpty(fecha_emision_a.getValue()) ) {
								fecha_emision_de.markInvalid('Debe capturar ambas fechas de emision o dejarlas en blanco');	
								fecha_emision_a.markInvalid('Debe capturar ambas fechas de emision o dejarlas en blanco');	
								return;
						}
					
						
						var fecha_publicacion_de = Ext.getCmp("fecha_publicacion_de1");
						var fecha_publicacion_a = Ext.getCmp("fecha_publicacion_a1");					
						if (!Ext.isEmpty(fecha_publicacion_de.getValue())   &&  Ext.isEmpty(fecha_publicacion_a.getValue()) ) {
								fecha_publicacion_de.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');	
								fecha_publicacion_a.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');	
								return;
						}else if (Ext.isEmpty(fecha_publicacion_de.getValue())  && !Ext.isEmpty(fecha_publicacion_a.getValue()) ) {
								fecha_publicacion_de.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');	
								fecha_publicacion_a.markInvalid('Debe capturar ambas fechas de publicacion o dejarlas en blanco');	
								return;
						}
			
						var fn_monto_de = Ext.getCmp("fn_monto_de1");
						var fn_monto_a = Ext.getCmp("fn_monto_a1");					
						if (!Ext.isEmpty(fn_monto_de.getValue())   &&   Ext.isEmpty(fn_monto_a.getValue()) ) {
								fn_monto_de.markInvalid('Debe capturar ambos montos o dejarlos en blanco');	
								fn_monto_a.markInvalid('Debe capturar ambos montos o dejarlos en blanco');	
								return;
						}else if (Ext.isEmpty(fn_monto_de.getValue())   &&  !Ext.isEmpty(fn_monto_a.getValue()) ) {
								fn_monto_de.markInvalid('Debe capturar ambos montos o dejarlos en blanco');	
								fn_monto_a.markInvalid('Debe capturar ambos montos o dejarlos en blanco');	
								return;
						}
						
						if (!Ext.isEmpty(fn_monto_de.getValue())   &&  !Ext.isEmpty(fn_monto_a.getValue()) ) {
							if( parseFloat(fn_monto_de.getValue()) > parseFloat(fn_monto_a.getValue()) ){
									Ext.MessageBox.alert("Mensaje","El monto  a no puede ser mayor que el monto de");
								return false ;
							}
						}
												
						var ic_estatus_docto = Ext.getCmp("ic_estatus_docto1");
						if (!Ext.isEmpty(ic_estatus_docto.getValue()) ) {
							if (ic_estatus_docto.getValue() ==1 && NOnegociable=='N') {
									Ext.MessageBox.alert("Mensaje","La EPO no se encuentra parametrizada para publicar documentos No Negociables");								
								return false ;
							}
						}
					
						var modalidad_plazo = Ext.getCmp("modalidad_plazo1");
						var modo = modalidad_plazo.getValue();
						
						var ventanaDoctos = Ext.getCmp('cambioDoctos');
						if (ventanaDoctos) {
							ventanaDoctos.destroy();
						}
					
						fp.el.mask('Enviando...', 'x-mask-loading');						
						//consulta para los registros con 
					consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{																
									modalidad_plazo:modo,
									operacion: 'Generar', //Generar datos para la consulta
									start: 0,
									limit: 15
							})
						});
						
					}
				},
				{
					text: 'Limpiar',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '24forma03Ext.jsp';
					}
				}
			]			
		});
		
		

	
		//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			applyTo: 'areaContenido',
			style: 'margin:0 auto;',
			width: 949,
			items: [
				fp,
				NE.util.getEspaciador(20),
				gridEditable,
				NE.util.getEspaciador(20),
				gridTotales,
				gridAcuse,
				NE.util.getEspaciador(20)
			]
		});
	
	
	catalogoPYMEData.load();
	catalogoMonedaData.load();
	catalogoEstatusData.load();
	catalogoModalidadData.load();	
	 
	

	
} );