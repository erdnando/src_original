
Ext.onReady(function() {
	
	//Variables globales
	var ventaCartera  = Ext.getDom('ventaCartera').value;
	var validaLinea  = Ext.getDom('validaLinea').value;
	var lineaCredito = Ext.getDom('lineaCredito').value;
	var descuentoAutomatico = Ext.getDom('descuentoAutomatico').value;
	var limiteLineaCredito = Ext.getDom('limiteLineaCredito').value;
	var tipoCredito = Ext.getDom('tipoCredito').value;
	var NOnegociable = Ext.getDom('NOnegociable').value;	
	var hidFechaActual = Ext.getDom('hidFechaActual').value; //fecha hoy
	var hid_fecha_porc_desc = Ext.getDom('hid_fecha_porc_desc').value;
	var txttotdoc = Ext.getDom('txttotdoc').value;
	var txtmtodoc = Ext.getDom('txtmtodoc').value;
	var txtmtomindoc = Ext.getDom('txtmtomindoc').value;
	var txtmtomaxdoc = Ext.getDom('txtmtomaxdoc').value;
	var txttotdocdo = Ext.getDom('txttotdocdo').value;
	var txtmtodocdo = Ext.getDom('txtmtodocdo').value;
	var txtmtomindocdo = Ext.getDom('txtmtomindocdo').value;
	var txtmtomaxdocdo = Ext.getDom('txtmtomaxdocdo').value;
	var hidCifrasControl = Ext.getDom('hidCifrasControl').value;
	var campos = Ext.getDom('campos').value;
	var proceso  = Ext.getDom('proceso').value;
	var acuse  = Ext.getDom('acuse').value;
	var fecha  = Ext.getDom('fecha').value;
	var hora  = Ext.getDom('hora').value;
	var usuario  = Ext.getDom('usuario').value;
	var mensajeEPO  = Ext.getDom('mensajeEPO').value;	
	var tipoCarga = Ext.getDom('tipoCarga').value;
	var producto = Ext.getDom('producto').value;
	var strPerfil = Ext.getDom('strPerfil').value;
	var operaContrato  = Ext.getDom('operaContrato').value;
        
	var fechaLimite; 	
	var hidID;
	var numElementos =0;
	var lineaReg	= [];
	var plazo = [];
	var fecha_vto	= [];
	var igNumeroDocto = [];
	var tamanio =150;
	var mensajesIntereses;
	
	if(proceso!=='' ){
		var acuseCifras = [
			['N�mero de Acuse', acuse],
			['Fecha ', hidFechaActual],
			['Hora ', hora],
			['Usuario ', usuario]
		];
	}
	
	var cancelar =  function() { 
		lineaReg	= [];
		plazo = [];
		fecha_vto	= [];
		igNumeroDocto = [];
	}
	
	if(ventaCartera=='S' && validaLinea==''){
		Ext.MessageBox.alert("Mensaje","La Epo no tiene linea de Credito");
	}

	var procesarSalir = function(store, arrRegistros, opts) 	{
		var parametros;
		if(tipoCarga=='I') { 		parametros= "24forma01Ext.jsp";			 	}
		if(tipoCarga=='M' && producto =='FactoRecur') { 	parametros= "24forma02FExt.jsp";			}
		if(tipoCarga=='M' && producto =='') { 		parametros= "24forma02Ext.jsp";		}	
				
	
		document.location.href = parametros+"?&txttotdoc=0&txtmtodoc=0&txtmtomindoc=0&txtmtomaxdoc=0&txttotdocdo=0&txtmtodocdo=0&txtmtomindocdo=0&txtmtomaxdocdo=0";
			
	}
	

	//var  cadEpoCemex ="Al transmitir este mensaje de datos para todos los efectos legales, acepto bajo mi responsabilidad, que los datos capturados en la orden de compra son correctos y reales.";

	var cadEpo = " ";
	/*
	if(ventaCartera=='S')  {
		 cadEpo = " Al trasmitir este mensaje de datos y para todos los efectos legales, Usted bajo su responsabilidad, est� aceptando se realice el DESCUENTO y/o FACTORAJE CON RECURSO del (los) Documento (s) final (es) que se detallan en est� pantalla, otorgando su consentimiento para que en caso de que se den la condiciones se�aladas en el Contrato para Realizar Operaciones de Descuento y/o Factoraje con Recurso, se realice la cesi�n de los derechos de cobro a favor del INTERMEDIARIO FINANCIERO que los opere. ";
	}else  {
		cadEpo = " Al trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, est� solicitando el DESCUENTO y/o FACTORAJE del (los) DOCUMENTOS (S) INICIAL (ES) que se detallan en est� pantalla y que ha publicado electr�nicamente, "+
		" otorgando su consentimiento para que en caso de que se d�n las condiciones se�aladas en el Contrato de Financiamiento a Clientes y Distribuidores, el (LOS) DOCUMENTOS (S) INICIAL (ES) ser�n sustituidos por el (los) DOCUMENTO (S) FINAL (ES), cediendo los "+
		" derechos de cobro a su favor al INTERMEDIARIO FINANCIERO que los opere.";
	}
  */
  var cadEpoCemex = cadEpo = 'Al trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, est� de acuerdo en que el (los) Documento (s) inicial (es) que se '+
      'detalla (n) en est� pantalla puedan ser operados indistintamente a trav�s de  las siguientes modalidades, en t�rminos de los pactado en el �Contrato de Financiamiento a Clientes y '+
      'Distribuidores� (para conocer las modalidades convenidas favor de revisar su contrato): <br><br> '+
      'a) DESCUENTO y/o FACTORAJE. <br> '+
      'De operarse en esta modalidad y para todos los efectos legales,  bajo su responsabilidad, est� solicitando el DESCUENTO y/o FACTORAJE del (los) Documento (s) inicial (es) que se detallan '+
      'en est� pantalla y que ha publicado electr�nicamente, otorgando su consentimiento para que en caso  de que se den la condiciones se�aladas en el �Contrato de Financiamiento a Clientes y '+
      'Distribuidores�, el (los) Documento (s) inicial (es)  ser� (n) sustituido (s) por el (los) DOCUMENTO (S) FINAL (ES), cediendo los derechos de cobro a su favor al INTERMEDIARIO FINANCIERO que los opere.'+
      '<br><br> '+
      'b) Pago del CLIENTE o DISTRIBUIDOR mediante Tarjeta de Cr�dito. <br>'+
      'De operarse en esta modalidad y para todos los efectos legales, otorga su consentimiento para que sea cubierto el (los) Documento (s) inicial (es) que se detalla (n) en est� pantalla '+
      'y que ha publicado electr�nicamente, en t�rminos del �Contrato de Financiamiento a Clientes y Distribuidores�, mediante la utilizaci�n de una Tarjeta de Cr�dito asociada al CLIENTE o DISTRIBUIDOR.<br/>'+
      'Asimismo, en este acto manifiesto bajo protesta de decir verdad, que s� he emitido o emitir� a mi CLIENTE o DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, ' +
      'seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.';
      
  
  if(operaContrato == 'S'){
      cadEpoCemex =  cadEpo = 'Al transmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, est� publicando el(los) DOCUMENTO(S) INICIAL(ES) que se '+
      'detallan en esta pantalla y que pueden ser objeto de financiamiento, otorgando su consentimiento para que en caso de que se den las condiciones se�aladas en el Contrato de Financiamiento '+
      'a Clientes y Distribuidores, el (los) DOCUMENTO(S) INICIAL(ES) ser�n sustituidos por el (los) DOCUMENTOS(S) FINAL(ES) que deber�n ser cobrados por el INTERMEDIARIO FINANCIERO.';
  } 
	var tableCemex = '<table width="900" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td class="formas" colspan="3">'+cadEpoCemex+'</td></tr>'+ 
		'</table>';
		
	if( tipoCredito=='C' ) {
		mensajesIntereses = '<table width="900" cellpadding="3" cellspacing="1" border="0">'+
			'<tr><td class="formas" colspan="3">Los par�metros de esta cadena toman como base el compromiso acordado en la CARTA DE CONSENTIMIENTO, por lo que es una cadena que opera bajo el concepto de Tasa 0% para el SUJETO DE APOYO, donde usted se compromete a cubrir la COMISION POR SERVICIOS FINANCIEROS al INTERMEDIARIO FINANCIERO, de acuerdo a lo pactado en dicho documento y por la(s) operaci�n(es) aqu� plasmadas.</td></tr>'+ 
			'</table>';
	}
	
	
		function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			Ext.getCmp('btnArchivoCons').setIconClass('icoXls');		
			
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var mensajesIntereses_1 = new Ext.Container({
		layout: 'table',
		id: 'mensajesIntereses_1',
		hidden: true,		
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		mensajesIntereses, 
				cls:		'x-form-item', 
				style: { 
					width:   		'700%', 
					textAlign: 		'left'
				} 
			}			
		]
	});
	
	var cadEpoCemex1 = new Ext.Container({
		layout: 'table',
		id: 'cadEpoCemex1',
		hidden: true,		
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		tableCemex, 
				cls:		'x-form-item', 
				style: { 
					width:   		'700%', 
					textAlign: 		'left'
				} 
			}			
		]
	});
		
	var tablecadEpo = '<table width="900" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td class="formas" colspan="3">'+cadEpo+'</td></tr>'+ 
		'</table>';
	var cadcadEpo1 = new Ext.Container({
		layout: 'table',
		id: 'cadcadEpo1',
		hidden: true,		
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		tablecadEpo, 
				cls:		'x-form-item', 
				style: { 
					width:   		'700%', 
					textAlign: 		'left'
				} 
			}			
		]
	});
	
	//boton de btndetalleCCC Imprimir CCC	
	var procesarSuccessFailureDetalleCCCF =  function(opts, success, response) {
		var btnGenerarDetalleCCC = Ext.getCmp('btnGenerarDetalleCCC');
		btnGenerarDetalleCCC.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonData = Ext.util.JSON.decode(response.responseText);	
			if(jsonData.accion=='button')  {
			
				var btnBajarGenerarDetalleCCC = Ext.getCmp('btnBajarGenerarDetalleCCC');
				btnBajarGenerarDetalleCCC.show();
				btnBajarGenerarDetalleCCC.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarGenerarDetalleCCC.setHandler( 
					function(boton, evento) {
						var forma = Ext.getDom('formAux');
						forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
						forma.submit();
					}
				);
			}
			
		} else {
			btnGenerarDetalleCCC.enable();
			Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			//NE.util.mostrarConnError(response,opts);
		}
	}
	
		//boton de btndetalleCCC Imprimir detalle documentos CCC
	var procesarSuccessFailureDetalleCCC =  function(opts, success, response) {
		var btndetalleCCC = Ext.getCmp('btndetalleCCC');
		btndetalleCCC.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajardetalleCCC = Ext.getCmp('btnBajardetalleCCC');
			btnBajardetalleCCC.show();
			btnBajardetalleCCC.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajardetalleCCC.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btndetalleCCC.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//boton de Generar archivo de detalle de documentos Descuento y/o Factoraje
	var procesarSuccessFailureDetalleDF =  function(opts, success, response) {
		var btnGenerarDetalleDF = Ext.getCmp('btnGenerarDetalleDF');
		btnGenerarDetalleDF.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var jsonData = Ext.util.JSON.decode(response.responseText);	
			if(jsonData.accion=='button')  {
				var btnBajarGenerarDetalleDF = Ext.getCmp('btnBajarGenerarDetalleDF');
				btnBajarGenerarDetalleDF.show();
				btnBajarGenerarDetalleDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarGenerarDetalleDF.setHandler( 
					function(boton, evento) {
						var forma = Ext.getDom('formAux');
						forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
						forma.submit();
					}
				);
			}
		} else {
			btnGenerarDetalleDF.enable();
			Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			//NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	//boton de btndetalleDM Imprimir Acuse
	var procesarSuccessFailurePrintAcuse =  function(opts, success, response) {
		var btnPrintAcuse = Ext.getCmp('btnPrintAcuse');
		btnPrintAcuse.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
		var jsonData = Ext.util.JSON.decode(response.responseText);	
		
			if(jsonData.accion=='Acuse')  {
			
			
				if(jsonData.tipoCredito=='C')  {
				
					Ext.Ajax.request({
						url: '24forma01ArcExt.jsp',
						params: {														
							tipoArchivo: 'CSV',											
							informacion:'btnGenerarDetalleCCC',
							tipoCredito:tipoCredito,
							acuse:acuse,
							accion:'button'
						},
						callback: procesarSuccessFailureDetalleCCCF
					});
				
				
				}else  {
				
					Ext.Ajax.request({
						url: '24forma01ArcExt.jsp',
						params: {														
							tipoArchivo: 'CSV',											
							informacion:'btnGenerarDetalleDF',
							tipoCredito:tipoCredito,
							acuse:acuse
						},
						callback: procesarSuccessFailureDetalleDF
					});
				
				}
			
			
			}else  {
				var btnBajarPrintAcuse = Ext.getCmp('btnBajarPrintAcuse');
				btnBajarPrintAcuse.show();
				btnBajarPrintAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarPrintAcuse.setHandler( 
					function(boton, evento) {
						var forma = Ext.getDom('formAux');
						forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
						forma.submit();
					}
				);
			
			}
			
		} else {
			Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			btnPrintAcuse.enable();
			//NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//boton de btndetalleDM Imprimir detalle documentos Descuento y/o Factoraje
	var procesarSuccessFailureDetalleDM =  function(opts, success, response) {
		var btndetalleDM = Ext.getCmp('btndetalleDM');
		btndetalleDM.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajardetalleDM = Ext.getCmp('btnBajardetalleDM');
			btnBajardetalleDM.show();
			btnBajardetalleDM.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajardetalleDM.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btndetalleDM.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
		
	
	var procesarConsultaDetalle = function(store, arrRegistros, opts) 	{
			
		if (arrRegistros != null) {
			if (!gridDetallesTotales.isVisible()) {
				gridDetallesTotales.show();
			}		
			var jsonData = store.reader.jsonData;	
			var tipoCambio = jsonData.tipoCambio;
			var tipo_Credito = jsonData.tipo_Credito;
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridDetallesTotales.getColumnModel();	
		
						
				if(tipoCambio==''){
					gridDetallesTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_DOCTOS_MNDL'), true);	
					gridDetallesTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_TOTAL_MNDL'), true);	
					gridDetallesTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_CREDITO_MNDL'), true);	
				}				
		
				
		var btndetalleDM = Ext.getCmp('btndetalleDM');
		var btndetalleCCC = Ext.getCmp('btndetalleCCC');
		var btnPrintAcuse = Ext.getCmp('btnPrintAcuse');
		var btnGenerarDetalleDF = Ext.getCmp('btnGenerarDetalleDF');
		var btnGenerarDetalleCCC = Ext.getCmp('btnGenerarDetalleCCC');
		var btnSalir = Ext.getCmp('btnSalir');
						
		btnSalir.show();
		btnPrintAcuse.show();
		var botonActivar1 ="";
		var botonActivar2 ="";
		
		//validaciones para mostrar  los botones de Imprimir en el Acuse 
		
		var store = gridDetallesTotales.getStore();
		store.each(function(record) {
			if( parseFloat(record.data['TOTALDOCDM']) +  parseFloat(record.data['TOTALDOCDMUSD'])   +  parseFloat(record.data['TOTALDOCCONVDM'])  >0){
				botonActivar1 ="btndetalleDM";
			}							
			if( parseFloat(record.data['TOTALDOCC']) +  parseFloat(record.data['TOTALDOCCUSD'])   +  parseFloat(record.data['TOTALDOCCONVCC'])  >0){
				botonActivar1 ="btndetalleCCC";
			}						
			if( parseFloat(record.data['TOTALDOCDM']) +  parseFloat(record.data['TOTALDOCDMUSD'])   +  parseFloat(record.data['TOTALDOCCONVDM'])  >0){
				botonActivar2 ="btnGenerarDetalleDF";
			}				
			if( parseFloat(record.data['TOTALDOCC']) +  parseFloat(record.data['TOTALDOCCUSD'])   +  parseFloat(record.data['TOTALDOCCONVCC'])  >0){
				botonActivar2 ="btnGenerarDetalleCCC"; 
			}
		});		
		
		if(tipo_Credito!='F'){
			if(	botonActivar1 =="btndetalleDM")  { 			btndetalleDM.show(); 			}
			if(	botonActivar1 =="btndetalleCCC") { 			btndetalleCCC.show(); 		}
			if(	botonActivar2 =="btnGenerarDetalleDF") { 			btnGenerarDetalleDF.show(); 		}
			if(	botonActivar2 =="btnGenerarDetalleCCC") { 			btnGenerarDetalleCCC.show(); 		}
		}
				
		Ext.Ajax.request({
			url: '24forma01ArcExt.jsp',
			params: {														
				tipoArchivo: 'PDF',											
				informacion:'btnPrintAcuse',
				tipoCredito:tipoCredito,
				acuse:acuse,
				fechaAcuse:fecha,
				horaAcuse:hora,
				accion:'Acuse'
			},
			callback: procesarSuccessFailurePrintAcuse
		});
		
		//Botones de Imprimir detalle documentos Descuento y/o Factoraje 				
		btndetalleDM.setHandler(function(boton, evento) {
			boton.disable();
			boton.setIconClass('loading-indicator');								
			Ext.Ajax.request({
				url: '24forma01ArcExt.jsp',
				params: {														
					tipoArchivo: 'PDF',											
					informacion:'btndetalleDM',
					tipoCredito:tipoCredito,
					acuse:acuse
				},
				callback: procesarSuccessFailureDetalleDM
			});
		});
					
		//Imprimir Acuse					
		btnPrintAcuse.setHandler(	function(boton, evento) {
			boton.disable();
			boton.setIconClass('loading-indicator');								
			Ext.Ajax.request({
				url: '24forma01ArcExt.jsp',
				params: {														
					tipoArchivo: 'PDF',											
					informacion:'btnPrintAcuse',
					tipoCredito:tipoCredito,
					acuse:acuse,
					fechaAcuse:fecha,
					horaAcuse:hora,
					accion:'button'
				},
				callback: procesarSuccessFailurePrintAcuse
			});
		});
			
		//Generar archivo de detalle de documentos Descuento y/o Factoraje 
		btnGenerarDetalleDF.setHandler(function(boton, evento) {
			boton.disable();
			boton.setIconClass('loading-indicator');								
			Ext.Ajax.request({
				url: '24forma01ArcExt.jsp',
				params: {														
					tipoArchivo: 'CSV',											
					informacion:'btnGenerarDetalleDF',
					tipoCredito:tipoCredito,
					acuse:acuse,
					accion:'button'
				},
				callback: procesarSuccessFailureDetalleDF
			});
		});
		
			////Credito Cuenta Corriente
			//Botones de Imprimir detalle documentos CCC	
		btndetalleCCC.setHandler(function(boton, evento) {
			boton.disable();
			boton.setIconClass('loading-indicator');								
			Ext.Ajax.request({
				url: '24forma01ArcExt.jsp',
				params: {														
					tipoArchivo: 'PDF',											
					informacion:'btndetalleCCC',
					tipoCredito:tipoCredito,
					acuse:acuse,
					accion:'button'
				},
				callback: procesarSuccessFailureDetalleCCC
			});
		});
			
			
		
			//Botones de Imprimir detalle documentos CCC	
		btnGenerarDetalleCCC.setHandler(function(boton, evento) {
			boton.disable();
			boton.setIconClass('loading-indicator');								
			Ext.Ajax.request({
				url: '24forma01ArcExt.jsp',
				params: {														
					tipoArchivo: 'CSV',											
					informacion:'btnGenerarDetalleCCC',
					tipoCredito:tipoCredito,
					acuse:acuse,
					accion:'button'
				},
				callback: procesarSuccessFailureDetalleCCCF
			});
		});
		
		var el = gridDetallesTotales.getGridEl();					
			
			if(store.getTotalCount() > 0) {
				//el.unmask();	
			} else {								
				el.mask('Para ver los totales favor de capturar detalle de Descuento y/o Factoraje', 'x-mask');				
			}			
		}
	}
	
	
			
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
							
		if (arrRegistros != null) {
			if (!gridCaptura.isVisible()) {
				gridCaptura.show();
			}		
			var jsonData = store.reader.jsonData;		
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  campo1 = jsonData.Campo0;
			var  campo2 = jsonData.Campo1;
			var  campo3 = jsonData.Campo2;
			var  campo4 = jsonData.Campo3;
			var  campo5 = jsonData.Campo4;
			var operaConversion = jsonData.operaConversion;
					
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridCaptura.getColumnModel();
				
			if(jsonData.meseSinIntereses=='S'  && jsonData.obtieneTipoCredito=='C' ) {
				Ext.getCmp('mensajesIntereses_1').show(); 
			}	
				
			if(operaConversion=='false'){
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CONVERSION'), true);	
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);	
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('MONTO_VALUADO'), true);								
			}
							
			if(campo1 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),campo1);
			}
			if(campo2 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),campo2);
			}
			if(campo3 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),campo3);
			}
			if(campo4 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),campo4);
			}
			if(campo5 !=''){					
				gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),campo5);
			}
		
			if(hayCamposAdicionales=='0'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='1'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='2'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);			
			}else  	if(hayCamposAdicionales=='3'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='4'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);	
			}else  	if(hayCamposAdicionales=='5'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);	
			}
			
			if(jsonData.strPerfil!='ADMIN EPO' && jsonData.strPerfil!='ADMIN EPO GRUPO' &&   jsonData.strPerfil!='ADMIN EPO FON'){
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('NEGOCIABLE'), true);
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('ESTATUS'), false);
			}else {
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('NEGOCIABLE'), false);
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('ESTATUS'), true);
			}
			
			//F09-2015
			if(jsonData.meseSinIntereses=='S'  && jsonData.obtieneTipoCredito=='C' ){
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), false);			
			}else  {
				gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), true);		 
			}
					
			var el = gridCaptura.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
 //Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 150,
		style: 'margin:0 auto;',		
		frame: true
	});
	

	

		//para los totales del grid Detalles 
	var totalesDetallesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotalesDetalle'	
		},								
		fields: [			
			{name: 'TIPO_DOCUMENTO'},
			{name: 'TOTALDOCDM'},
			{name: 'MONTOTOTALDM'},
			{name: 'MONTOTMN'},
			{name: 'TOTALDOCDMUSD'},
			{name: 'MONTOTOTALDMUSD'},
			{name: 'MONTOTDL'},			
			{name: 'TOTALDOCCONVDM'},
			{name: 'MONTODOCTO'},
			{name: 'MONTOCREDITO'},			
			{name: 'TOTALDOCC'},
			{name: 'MONTOTOTALCC'},
			{name: 'MONTOMNCC'},
			{name: 'TOTALDOCCUSD'},
			{name: 'MONTOTOTALCCUSD'},
			{name: 'MONTODLCC'},			
			{name: 'TOTALDOCCONVCC'},
			{name: 'MONTODOCTOC'},
			{name: 'MONTOCREDITOC'},
			//
			{name: 'TOTAL_DOCTOS_MN'},
			{name: 'MONTO_TOTAL_MN'},
			{name: 'MONTO_CREDITO_MN'},			
			{name: 'TOTAL_DOCTOS_DL'},
			{name: 'MONTO_TOTAL_DL'},
			{name: 'MONTO_CREDITO_DL'},			
			{name: 'TOTAL_DOCTOS_MNDL'},
			{name: 'MONTO_TOTAL_MNDL'},
			{name: 'MONTO_CREDITO_MNDL'}
		],
		totalProperty : 'total',
		autoLoad: false,	
		listeners: {
			load: procesarConsultaDetalle,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConsultaDetalle(null, null, null);						
				}
			}
		}			
	});
	
	//esto esta en duda como manejarlo
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 1, align: 'center'},
				{header: 'Moneda nacional', colspan: 3, align: 'center'},	
				{header: 'D�lares', colspan: 3, align: 'center'},
				{header: 'Doctos en DLL financiados en M.N.', colspan: 3, align: 'center'}	
			]
		]
	});
	
	var gridDetallesTotales = new Ext.grid.EditorGridPanel({	
		id: 'gridDetallesTotales',
		store: totalesDetallesData,
		margins: '20 0 0 0',
		align: 'center',
		title: '',
		plugins: grupos,	
		hidden: true,
		columns: [	
			{
				header: 'Tipo de documento inicial',
				dataIndex: 'TIPO_DOCUMENTO',
				width: 150,
				align: 'left'				
			},
			{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTAL_DOCTOS_MN',
				width: 150,
				align: 'center'			
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'MONTO_TOTAL_MN',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto del cr�dito',
				dataIndex: 'MONTO_CREDITO_MN',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
				//DOLARES		
			{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTAL_DOCTOS_DL',
				width: 150,
				align: 'center'			
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'MONTO_TOTAL_DL',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto del cr�dito',
				dataIndex: 'MONTO_CREDITO_DL',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			//Doctos en DLL financiados en M.N.			
			{
				header: 'Num. total de doctos.',
				dataIndex: 'TOTAL_DOCTOS_MNDL',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Monto total de doctos.',
				dataIndex: 'MONTO_TOTAL_MNDL',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Monto del cr�dito',
				dataIndex: 'MONTO_CREDITO_MNDL',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			}		
		],
			height: 100,
			width: 943,
			frame: false
	});	
	




	
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'CONSECUTIVO'},		
			{name: 'NUM_DISTRIBUIDOR'},
			{name: 'NOMBRE_DISTRIBUIDOR'},
			{name: 'NUN_DOCTOINICIAL'},			
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'CATEGORIA'},
			{name: 'PLAZO_DESC'},
			{name: 'PORCE_DESC'},
			{name: 'MONTO_DESC'},				
			{name: 'TIPO_CONVERSION'},	
			{name: 'TIPO_CAMBIO'},	
			{name: 'MONTO_VALUADO'},
			{name: 'MODALIDAD'},
			{name: 'COMENTARIOS'},
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'},
			{name: 'CAMPO4'},
			{name: 'CAMPO5'},
			{name: 'PLAZO'},
			{name: 'FECHA_VEN'},		
			{name: 'NEGOCIABLE'},
			{name: 'LINEA_REGISTRO'},
			{name: 'RESPONSABLE_INTERES'},
			{name: 'DIAS_MINIMO'},
			{name: 'DIAS_MAXIMO'},		
			{name: 'IC_MONEDA'},	
			{name: 'SELECCIONAR_IFS'},
			{name: 'AUXICLINEA_MN'},
			{name: 'AUXICLINEA_USD'},
			{name: 'LINEA_REGISTRO_AUXILIAR'},
			{name: 'PARTIPOCAMBIO'},
			{name: 'HID_NUMLINEAS'},			
			{name: 'CAPTURA_LINEA'},
			{name: 'CAPTURA_PLAZO'},
			{name: 'CAPTURA_FECHAVEN'},
			{name: 'TIPO_CREDITO'},
			{name: 'ESTATUS'},
			{name: 'TIPOPAGO'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
				
	var gridCaptura = new Ext.grid.EditorGridPanel({
		id: 'gridCaptura',
		title: 'Carga de Documentos',
		hidden: true,
		clicksToEdit: 1,				
		columns: [
		{							
			header : 'N�mero de distribuidor',
			tooltip: 'N�mero de distribuidor',
			dataIndex : 'NUM_DISTRIBUIDOR',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Distribuidor',
			tooltip: 'Distribuidor',
			dataIndex : 'NOMBRE_DISTRIBUIDOR',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'N�mero documento inicial',
			tooltip: 'N�mero documento inicial',
			dataIndex : 'NUN_DOCTOINICIAL',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Fecha de emisi�n',
			tooltip: 'Fecha de emisi�n',
			dataIndex : 'FECHA_EMISION',
			width : 150,
			align: 'center',
			sortable : false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{							
			header : 'Fecha vto. docto.',
			tooltip: 'Fecha vto. docto.',
			dataIndex : 'FECHA_VENCIMIENTO',
			width : 150,
			align: 'center',
			sortable : false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{							
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO_DOCTO',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			align: 'left',
			sortable : false			
		},
		{							
			header : 'Monto. docto.',
			tooltip: 'Monto. docto.',
			dataIndex : 'MONTO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Categor�a',
			tooltip: 'Categor�a',
			dataIndex : 'CATEGORIA',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Plazo para descuento en dias',
			tooltip: 'Plazo para descuento en dias',
			dataIndex : 'PLAZO_DESC',
			width : 150,
			align: 'center',
			sortable : false,
			renderer:function(value,metadata,registro){
				if(registro.data['TIPO_CREDITO']!='F') {	
					return value;
				}else  {
					return 'NA';
				}
			}
		},
		{							
			header : '% descuento',
			tooltip: '% descuento',
			dataIndex : 'PORCE_DESC',
			width : 150,
			align: 'center',
			sortable : false,
			renderer:function(value,metadata,registro){
				if(registro.data['TIPO_CREDITO']!='F') {	
					return Ext.util.Format.number(value,'0.0000%');
				}else  {
					return 'NA';
				}
			}		
		},
		{							
			header : 'Monto % de descuento',
			tooltip: 'Monto % de descuento',
			dataIndex : 'MONTO_DESC',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00') 
		},
		{							
			header : 'Tipo conversi�n',
			tooltip: 'Tipo conversi�n',
			dataIndex : 'TIPO_CONVERSION',
			width : 150,
			align: 'right',
			sortable : false		 
		},
		{							
			header : 'Tipo cambio ',
			tooltip: 'Tipo cambio ',
			dataIndex : 'TIPO_CAMBIO',
			width : 150,
			align: 'right',
			sortable : false		 
		},
		{							
			header : 'Monto valuado en Pesos',
			tooltip: 'Monto valuado en Pesos',
			dataIndex : 'MONTO_VALUADO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00') 
		},	
		{							
			header : 'Modalidad de Plazo',
			tooltip: 'Modalidad de Plazo',
			dataIndex : 'MODALIDAD',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Tipo de Pago',
			tooltip: 'Tipo de Pago',
			dataIndex : 'TIPOPAGO',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Comentarios',
			tooltip: 'Comentarios',
			dataIndex : 'COMENTARIOS',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo1',
			tooltip: 'Campo1',
			dataIndex : 'CAMPO1',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo2',
			tooltip: 'Campo2',
			dataIndex : 'CAMPO2',
			width : 150,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Campo3',
			tooltip: 'Campo3',
			dataIndex : 'CAMPO3',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo4',
			tooltip: 'Campo4',
			dataIndex : 'CAMPO4',
			width : 150,
			align: 'left',
			sortable : false
		},	
		{							
			header : 'Campo5',
			tooltip: 'Campo5',
			dataIndex : 'CAMPO5',
			width : 150,
			align: 'left',
			sortable : false
		},		
		{							
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO',
			width : 150,
			align: 'center',
			sortable : false
		},			
		{							
			header : 'Fecha vto',
			tooltip: 'Fecha vto',
			dataIndex : 'FECHA_VEN',
			width : 150,
			align: 'center',
			sortable : false		
		},			
		{							
			header : 'Negociable',
			tooltip: 'Negociable',
			dataIndex : 'NEGOCIABLE',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Estatus',
			tooltip: 'Estatus',
			dataIndex : 'ESTATUS',
			width : 150,
			align: 'center',
			sortable : false
		}
		],
		displayInfo: true,
		store: consultaData,
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		frame: false
		});
	
	
	var fpImprimirAcuse = new Ext.Container({
		layout: 'table',
		id: 'fpImprimirAcuse',			
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [					
			{
				xtype: 'button',
				text: '<center>Imprimir detalle documentos <br> Descuento y/o Factoraje</center>', //aqui falta un mensaje 
				id: 'btndetalleDM',
				iconCls: 'icoPdf',			
				hidden: true
			},		
			{
				xtype: 'button',
				text: 'Bajar PDF',				
				id: 'btnBajardetalleDM',
				iconCls: 'icoPdf',
				hidden: true				
			},	
			{
				xtype: 'button',				
				text: 'Imprimir detalle <br>documentos C.C.C.',
				id: 'btndetalleCCC',
				iconCls: 'icoPdf'	,
				hidden: true
			},	
			{
				xtype: 'button',				
				text: 'Bajar PDF',
				iconCls: 'icoPdf',
				id: 'btnBajardetalleCCC',
				hidden: true			
			},
			{
				xtype: 'button',				
				text: 'Imprimir Acuse',
				id: 'btnPrintAcuse',
				iconCls: 'icoPdf',
				hidden: true				
			},	
			{
				xtype: 'button',				
				text: 'Bajar PDF',
				id: 'btnBajarPrintAcuse',
				iconCls: 'icoPdf',
				hidden: true				
			},			
			{
				xtype: 'button',
				text: 'Generar archivo de detalle <br>de documentos Descuento y/o Factoraje',
				id: 'btnGenerarDetalleDF',
				iconCls: 'icoXls',
				hidden: true
				
			},
			{
				xtype: 'button',				
				text: 'Bajar Archivo',
				id: 'btnBajarGenerarDetalleDF',
				iconCls: 'icoXls',
				hidden: true				
			},
			{
				xtype: 'button',				
				text: 'Generar archivo de  detalle<br> de documentos C.C.C. ',
				id: 'btnGenerarDetalleCCC',
				iconCls: 'icoXls',
				hidden: true				
			},
			{
				xtype: 'button',				
				text: 'Bajar Archivo',
				id: 'btnBajarGenerarDetalleCCC',
				iconCls: 'icoXls',
				hidden: true				
			},			
			{
				xtype: 'button',				
				text: 'Salir',			
				iconCls: 'icoLimpiar',
				id: 'btnSalir',
				hidden: true,											
				handler: procesarSalir
			}				
		
		]
	});
		
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			gridDetallesTotales,
			NE.util.getEspaciador(20),
			cadcadEpo1, 
			cadEpoCemex1,
			mensajesIntereses_1,
			NE.util.getEspaciador(20),
			gridCifrasControl,	
			NE.util.getEspaciador(20),
			gridCaptura,		
			NE.util.getEspaciador(20),			
			fpImprimirAcuse,
			NE.util.getEspaciador(20)
		]
	});
		
	// cuando se genera el Acuse 
	if(proceso!=='' ){
	
		storeCifrasData.loadData(acuseCifras);	
		gridCifrasControl.show();
		
		if(strPerfil=='ADMIN EPO' || strPerfil=='ADMIN EPO GRUPO'  &&  strPerfil!='ADMIN EPO FON' )  {				
			if(mensajeEPO=='EpoCemex'){
				cadcadEpo1.hide()
				cadEpoCemex1.show();					
			}else {
				cadcadEpo1.show();
				cadEpoCemex1.hide();	
			}
		}
		
		consultaData.load({  	
			params: { 		
				informacion: 'Consulta', 		
				acuse:acuse, 
				pantalla:'Acuse'	
			}  		
		});	
		
		totalesDetallesData.load({ 		
			params: { 		
				informacion: 'ResumenTotalesDetalle', 
				proceso:proceso,  
				accionDetalle:'S'	
			} 		
		});	
		
		//fpImprimirAcuse.show();
					
		var btnSalir = Ext.getCmp('btnSalir');
		var btnPrintAcuse = Ext.getCmp('btnPrintAcuse');			
		btnSalir.show();
		btnPrintAcuse.show();
	
						
	}
	
	
} );