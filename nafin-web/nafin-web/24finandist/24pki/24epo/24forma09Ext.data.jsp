<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,	
	netropology.utilerias.caracterescontrol.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	java.text.SimpleDateFormat,
	java.util.Date,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	java.io.*,	
	java.text.*, 
	java.math.*,
	net.sf.json.*"	
	errorPage="/00utils/error_extjs.jsp, /00utils/error_extjs_fileupload.jsp"		
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar ="";
String rutaArchivoTemporal = null;
ParametrosRequest req = null;
StringBuffer contenidoArchivo=new StringBuffer("");
JSONObject 	resultado	= new JSONObject();
String correcto ="N";
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

try {
	MantenimientoDist mantDist = ServiceLocator.getInstance().lookup("MantenimientoDistEJB", MantenimientoDist.class); 
	
	if(informacion.equals("ValidaCargaArchivo")) {
		
		String txttotdoc  = (request.getParameter("txttotdoc") != null) ? request.getParameter("txttotdoc") : "0";
		String txtmtodoc  = (request.getParameter("txtmtodoc") != null) ? request.getParameter("txtmtodoc") : "0";
		String txtmtomindoc1  = (request.getParameter("txtmtomindoc") != null) ? request.getParameter("txtmtomindoc") : "0";
		String txtmtomaxdoc1  = (request.getParameter("txtmtomaxdoc") != null) ? request.getParameter("txtmtomaxdoc") : "0";
	
		String txttotdocdo  = (request.getParameter("txttotdocdo") != null) ? request.getParameter("txttotdocdo") : "0";
		String txtmtodocdo  = (request.getParameter("txtmtodocdo") != null) ? request.getParameter("txtmtodocdo") : "0";
		String txtmtomindocdo  = (request.getParameter("txtmtomindocdo") != null) ? request.getParameter("txtmtomindocdo") : "0";
		String txtmtomaxdocdo  = (request.getParameter("txtmtomaxdocdo") != null) ? request.getParameter("txtmtomaxdocdo") : "0";
		

		System.out.println(" txttotdoc"+txttotdoc);
		System.out.println(" txtmtodoc"+txtmtodoc);
		System.out.println(" txtmtomindoc1"+txtmtomindoc1);
		System.out.println(" txtmtomaxdoc1"+txtmtomaxdoc1);
		
		
		System.out.println(" txttotdocdo"+txttotdocdo);
		System.out.println(" txtmtodocdo"+txtmtodocdo);
		System.out.println(" txtmtomindocdo"+txtmtomindocdo);
		System.out.println(" txtmtomaxdocdo"+txtmtomaxdocdo);
	
		
		if(txttotdoc.equals("")) txttotdoc="0";
		if(txtmtodoc.equals("")) txtmtodoc="0";
		if(txtmtomindoc1.equals("")) txtmtomindoc1="0";
		if(txtmtomaxdoc1.equals("")) txtmtomaxdoc1="0";
		
		if(txttotdocdo.equals("")) txttotdocdo="0";
		if(txtmtodocdo.equals("")) txtmtodocdo="0";
		if(txtmtomindocdo.equals("")) txtmtomindocdo="0";
		if(txtmtomaxdocdo.equals("")) txtmtomaxdocdo="0";
		
		String error = "", bien = "", lineabien = "", lineaerror = "", errorcifras = "", icDoctos = "", strPreacuse = "", strErrores = "";
		int iTotalPymes = 0, iTotalReg = 0, iTotaldoctos = 0, iTotalbienmn = 0, iTotalbiendl = 0;
		double smonto = 0, montomx = 0, montomn = 0, smontodl = 0, montomxdl = 0, montomndl = 0;
		
		String archivo  = (request.getParameter("archivo") != null) ? request.getParameter("archivo") : "";
				
		String rutaArchivo = strDirectorioTemp +archivo; 		
		java.io.File ft = new java.io.File(rutaArchivo);
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
		VectorTokenizer vtd = null;
		Vector vecdet = null;
		String linea=	"", sregistro ="";
		while ((sregistro=br.readLine())!=null){
			linea += sregistro +"\n";
		}	
		HashMap maparesultados = mantDist.validarMantenimientoMasivo(linea,iNoCliente);
		
		error = maparesultados.get("errores").toString();
		bien = maparesultados.get("correctos").toString();
		lineabien = maparesultados.get("lineasb").toString();
		lineaerror = maparesultados.get("linease").toString();
		
		iTotaldoctos = Integer.parseInt(maparesultados.get("totaldoctos").toString());
		smonto = ((Double)maparesultados.get("sumamonto")).doubleValue();
		montomx = ((Double)maparesultados.get("montomax")).doubleValue();
		montomn = ((Double)maparesultados.get("montomin")).doubleValue();
		smontodl = ((Double)maparesultados.get("sumamontodl")).doubleValue();
		montomxdl = ((Double)maparesultados.get("montomaxdl")).doubleValue();
		montomndl = ((Double)maparesultados.get("montomindl")).doubleValue();

		iTotalbienmn = Integer.parseInt(maparesultados.get("totalbienmn").toString());
		iTotalbiendl = Integer.parseInt(maparesultados.get("totalbiendl").toString());
		
		icDoctos = maparesultados.get("icDoctos").toString();
		strPreacuse = maparesultados.get("strPreacuse").toString();
		strErrores = maparesultados.get("strErrores").toString();
		
		if(iTotaldoctos != (Integer.parseInt(txttotdoc) + Integer.parseInt(txttotdocdo))){//numero de documentos
			errorcifras += "El número de documentos no corresponde al número total de documentos  capturado\n";
		}
		if(smonto != (new Double(txtmtodoc)).doubleValue()){//monto total para MN
			errorcifras += "El monto no corresponde al monto total capturado para Moneda Nacional\n";
		}
		if(smontodl != (new Double(txtmtodocdo)).doubleValue()){//monto total para DL
			errorcifras += "El monto no corresponde al monto total capturado para Dolares\n";
		}
		if(montomn != (new Double(txtmtomindoc1)).doubleValue()){//monto minimo para MN
			errorcifras += "El monto mínimo no corresponde a lo capturado para Moneda Nacional\n";
		}
		if(montomndl != (new Double(txtmtomindocdo)).doubleValue()){//monto minimo para DL
			errorcifras += "El monto mínimo no corresponde a lo capturado para Dolares\n";
		}
		if(montomx != (new Double(txtmtomaxdoc1)).doubleValue()){//monto maximo para MN
			errorcifras += "El monto máximo no corresponde a lo capturado para Moneda Nacional\n";
		}
		System.out.println(montomx+" ==="+txtmtomaxdocdo);
		
		if(montomxdl != (new Double(txtmtomaxdocdo)).doubleValue()){//monto maximo para DL
			errorcifras += "El monto máximo no corresponde a lo capturado para Dolares\n";
		}
			
		
		if(!strPreacuse.equals("") &&  error.equals("") && errorcifras.equals("") ){
			correcto="S";
			StringTokenizer st = new StringTokenizer(strPreacuse, "|");
			StringTokenizer biens = new StringTokenizer(bien, "Linea");
					
			while(st.hasMoreTokens()){
				String slinea = st.nextToken();
				VectorTokenizer vt = new VectorTokenizer(slinea, ",");
				Vector vlinea = vt.getValuesVector();
				
				String slineaBien = biens.nextToken();
				VectorTokenizer vtBien = new VectorTokenizer(slineaBien, ",");
				Vector vlineaBien = vtBien.getValuesVector();
				String Lineas2 =  vlineaBien.get(0).toString();
								
				String proveedor =  vlinea.get(0).toString();
				String noDocumento   =vlinea.get(1).toString();
				String monto = 	 vlinea.get(2).toString();
				String fechaVencimiento = vlinea.get(3).toString();
				String modalidadAnterior = "Plazo Mixto";
				String modalidadNueva  ="Plazo Ampliado";
				String causa =  vlinea.get(4).toString();
				
				datos.put("PROVEEDOR",proveedor);					
				datos.put("NO_DOCTO",noDocumento);	
				datos.put("MONTO",monto);
				datos.put("FECHA_VENCIMIENTO",fechaVencimiento);	
				datos.put("MODALIDAD_ANTE",modalidadAnterior);	
				datos.put("MODALIDAD_NUEVA",modalidadNueva);	
				datos.put("CAUSA",causa);				
				registros.add(datos);							
			}		
		}						
		
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		resultado = JSONObject.fromObject(consulta);	
		
		//ESTAS LAS USO PARA EL PREACUSE
		
		resultado.put("STRERRORES", strErrores.toString());
		resultado.put("ERROR_CIFRAS", errorcifras.toString());
		resultado.put("ERRORES", error.toString());
		resultado.put("BIEN", bien.toString());		
		resultado.put("LINEAS_ERROR", lineaerror.toString());
		resultado.put("LINEAS_BIEN", lineabien.toString());
		resultado.put("LINEAS", linea.toString());
		resultado.put("IC_DOCTOS", icDoctos.toString());
		resultado.put("STRPREACUSE", strPreacuse.toString());
		
		//ESTAS LAS USO PARA EL PREACUSE
		resultado.put("TOTAL_N",Double.toString (smonto));
		resultado.put("TOTAL_D", Double.toString (smontodl));
		resultado.put("TOTAL_DN", String.valueOf(iTotalbienmn));
		resultado.put("TOTAL_DD", String.valueOf(iTotalbiendl));
		resultado.put("N0_MONEDA_NACIONAL",  String.valueOf(iTotalbienmn));   
		resultado.put("NO_MONEDA_DOLAR",   String.valueOf(iTotalbiendl));
		resultado.put("MON_MONEDA_NACIONAL", Double.toString (smonto));
		resultado.put("MON_MONEDA_DOLAR",  Double.toString (smontodl));
		infoRegresar = resultado.toString();
	
	

	
	
}else if(informacion.equals("ACUSE")) {		
	
	Seguridad s = new Seguridad();
	Acuse		acuse	= null;
	String		_acuse	= "";
	String folioCert = "";
	char getReceipt = 'Y';
	AccesoDB con = new AccesoDB();
	try{
		con.conexionDB();
		acuse	= new Acuse(Acuse.ACUSE_EPO,"4","com",con);
	}catch(Exception e){
		throw new NafinException("SIST001");
	}finally{
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}

	String externContent = (request.getParameter("textoFirmado") != null) ? request.getParameter("textoFirmado") : "";
	String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
	String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
	String totaldoc 		= (request.getParameter("totaldoc")==null)?"0":request.getParameter("totaldoc").trim();
	String totaldocdl 		= (request.getParameter("totaldocdl")==null)?"0":request.getParameter("totaldocdl").trim();
	String montodoc 		= (request.getParameter("montodoc")==null)?"0":request.getParameter("montodoc").trim();
	String montodocdl 		= (request.getParameter("montodocdl")==null)?"0":request.getParameter("montodocdl").trim();
	String lineabien 		= (request.getParameter("lineabien")==null)?"0":request.getParameter("lineabien").trim();
	String lineaerror 		= (request.getParameter("lineaerror")==null)?"0":request.getParameter("lineaerror").trim();
	String linea	 		= (request.getParameter("linea")==null)?"":request.getParameter("linea").trim();
	String icDoctos	 		= (request.getParameter("icDoctos")==null)?"":request.getParameter("icDoctos").trim();
	String strPreacuse	 		= (request.getParameter("strPreacuse")==null)?"":request.getParameter("strPreacuse").trim();
	String usuario = 	strLogin+" - "+strNombreUsuario;
		
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		folioCert = acuse.toString();	
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
			_acuse = s.getAcuse();			
			
			//actualizar los documentos correctos
			mantDist.guardaCambiosMasivo(strPreacuse, totaldoc, totaldocdl, montodoc, montodocdl, acuse.toString(), _acuse, strLogin);
		}
	
		resultado.put("success", new Boolean(true));
		resultado.put("acuse",acuse.formatear().toString());
		resultado.put("fecha",fechaHoy);	
		resultado.put("hora",horaActual);	
		resultado.put("usuario",usuario.toString());
		resultado.put("recibo",acuse.toString());
		infoRegresar = resultado.toString();	
	
	}
}else if (informacion.equals("validaCaracteresControl")) {

		resultado	= new JSONObject();
		String nombreArchivo 	= (request.getParameter("nombreArchivo")	== null)?"":request.getParameter("nombreArchivo");
		System.out.println("ANTES DEL HILO");
		session.removeAttribute("ValidaCaracteresControl");
		BuscaCaracteresControlThread ValidaCaractCon= new BuscaCaracteresControlThread();
		
		ResumenBusqueda resCaractCon = new ResumenBusqueda();
		System.out.println("NOMBRE "+strDirectorioTemp +nombreArchivo );
		ValidaCaractCon.setRutaArchivo(strDirectorioTemp+nombreArchivo);
		ValidaCaractCon.setRegistrador(resCaractCon);
		ValidaCaractCon.setCreaArchivoDetalle(true);
		ValidaCaractCon.setDirectorioTemporal(strDirectorioTemp);
		ValidaCaractCon.setSeparadorCampo("|");
		new Thread(ValidaCaractCon).start();
		session.setAttribute("ValidaCaracteresControl", ValidaCaractCon);
	//ValidaCaractCon.getMensaje();
		resultado.put("success",new Boolean(true));
		resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_THREAD"		);
		infoRegresar = resultado.toString();
		System.out.println("TERMINO Y EMPEZO EL THREAD ");
	
	}else if (    informacion.equals("avanceThreadCaracteres") 				)	{
		resultado	= new JSONObject();
		BuscaCaracteresControlThread ValidaCaractCon = (BuscaCaracteresControlThread)session.getAttribute("ValidaCaracteresControl");
		String[] mensa = {""};
		ResumenBusqueda resCaractCon = new ResumenBusqueda(); 
		int estatusThread = ValidaCaractCon.getStatus(mensa);
		
		System.out.println("ESTATUS "+estatusThread);
		if(estatusThread==200){
			resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_THREAD");	
		}else if(estatusThread==300 ){
			resCaractCon = (ResumenBusqueda)ValidaCaractCon.getRegistrador();
			boolean hayCaracteres = resCaractCon.hayCaracteresControl();
			if(hayCaracteres==true){
				resultado.put("estadoSiguiente", "HAY_CARACTERES_CONTROL");
				resultado.put("mns",resCaractCon.getMensaje());
				resultado.put("nombreArchivoCaractEsp",resCaractCon.getNombreArchivoDetalle());
				
			}else{
				resultado.put("estadoSiguiente", "VALIDA_CARGA_DOCTO");
			}
		}else if(estatusThread==400 ){
			resultado.put("estadoSiguiente", "ERROR_THREAD_CARACTERES");	
			resultado.put("mns", "Error en el archivo, favor de verificarlo");	
		}
		resultado.put("success",new Boolean(true));
		infoRegresar = resultado.toString();
		System.out.println("TERMINO Y EMPEZO EL THREAD "+infoRegresar);
	
	
	}else if (informacion.equals("descargaArchivoDetalle")	){
		String nombreArchivo 	= (request.getParameter("archivo")	== null)?"":request.getParameter("archivo");
		JSONObject jsonObj3 = new JSONObject();
		jsonObj3.put("success", new Boolean(true));
		jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj3.toString();
	}

%>
<%
} catch(Exception ex) {
System.out.println("La Exception: "+ex.toString()); 
	out.println("La Exception: "+ex.toString() ); 
}

System.out.println("infoRegresar: "+infoRegresar); 
%>
<%=infoRegresar%>