<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>
<%
try{

	String tipoCarga = (request.getParameter("tipoCarga")!=null)?request.getParameter("tipoCarga"):"I";

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	cargaDocto.operaDistribuidores(iNoCliente);
	String hidCifrasControl ="S";
	try{
		cargaDocto.operaCifrasControl(iNoCliente);
	}catch(NafinException ne){
		hidCifrasControl ="N";	
	}
		
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<% if(hidCifrasControl.equals("S")){ %>
<script type="text/javascript" src="24forma01Ext.js?<%=session.getId()%>"></script>
<%}else if(hidCifrasControl.equals("N")){  %>
<% response.sendRedirect("24forma01aExt.jsp?hidCifrasControl=N&tipoCarga=I"); %>
<%} %>
<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="../certificado.jspf" %>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>

<form id='formParametros' name="formParametros">
	<input type="hidden" id="hidCifrasControl" name="hidCifrasControl" value="<%=hidCifrasControl%>"/>	
	<input type="hidden" id="tipoCarga" name="tipoCarga" value="<%=tipoCarga%>"/>	
	
	
</form>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
<%
}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	out.println(" Error: "+e);
}
%>