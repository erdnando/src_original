Ext.onReady(function() {
	var strPerfil = Ext.getDom('auxStrPerfil').value;
	var form = {hayCheck:false,	num_acuse:null, regP:0, regPN:0, cadDocSelec:null, textoFirmar:null, pkcs7:null}
	var selecIF = "";
	var selecIFMasiva = "";
	var ic_documento = [];
	var individualSub = "";
	var masivoSub = "";
	var storeIf = "null";
	//----------------------------------- Funciones a utilizar ----------------------------------
	function regresar(pagina){
		window.location = pagina;
	}
	function processResult(res){        
		if (res.toLowerCase() == 'ok' || res.toLowerCase() == 'yes'){
			regresar('24mantDoctoPendienteNeg.jsp');
		}
	}
	var procesarCatalogoIF = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			var jsonData = store.data.items;
			Ext.each(jsonData, function(item,inx,arrItem){ 
				if(item.data.clave == storeIf ){
					Ext.getCmp('lineaTodos1').setValue(storeIf);
					Ext.getCmp('lineaTodos1Masivo').setValue(storeIf);
				}
			});
		}
		
	}
	function processResultElimina(res){
		if (res.toLowerCase() == 'ok' || res.toLowerCase() == 'yes'){
			pnl.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url : '24mantDoctoPendienteNeg.data.jsp',
				params : {
					informacion:			'Confirma',
					num_acuse:				form.num_acuse,
					cadDocSelec:			form.cadDocSelec,
					regPrenegociables:	form.regP,
					regPrenonegociables:	form.regPN,
					operacion:				Ext.getCmp('hidOperacion').getValue(),
					Pkcs7:					form.pkcs7,
					TextoFirmado:			form.textoFirmar,
					selecIF :selecIF,
					ic_documento :ic_documento
				},
				callback: procesaConfirma
			});
		}
	}
	//----------------------------------- Handlers ------------------------------
	function procesaConfirma(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			if (resp.operacion != undefined && resp.operacion == "Eliminar"){
				Ext.Msg.show({
					title:	'Confirmar',
					msg:		'Se eliminaron los registros seleccionados de manera satisfactoria.',
					buttons:	Ext.Msg.OK,
					fn: processResult,
					closable:false,
					icon: Ext.MessageBox.INFO
				});
			}else if (resp.operacion != undefined && resp.operacion == "Generar") {
				Ext.Msg.show({
					title:	'Confirmar',
					msg:		'Se ha  cambiado el estatus satisfactoriamente a los documentos seleccionados.',
					buttons:	Ext.Msg.OK,
					fn: processResult,
					closable:false,
					icon: Ext.MessageBox.INFO
				});
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	var Automatizacion = function(grid, rowIndex, colIndex, item, event) {
		form.cadDocSelec="";
		form.textoFirmar = "";
		Ext.getCmp('hidOperacion').setValue('');
		var registro = grid.getStore().getAt(rowIndex);
		form.num_acuse = registro.get('CC_ACUSE');
		fp.hide();
		grid.hide();
		if (	colIndex == 4	){	//	Individual
			Ext.getCmp('disAcuse').body.update('<div align="center">'+form.num_acuse+'</div>');
			Ext.getCmp('disFecha').body.update('<div align="center">'+registro.get('FECHA_CARGA')+'</div>');
			Ext.getCmp('disUsuario').body.update('<div align="center">'+registro.get('IC_USUARIO')+'</div>');
			Ext.getCmp('disDoctos').body.update('<div align="center">'+registro.get('IG_NUMERO_DOCTO')+'</div>');
			individualSub = "";
			individualSub = "true";
			pnlIndividual.show();
			individualData.load({
				params: Ext.apply({
					operacion: 'Generar',
					start: 0,
					limit: 15,
					tipoConsulta:'Individual',
					cc_acuse:form.num_acuse
				})
			});
		}else if (	colIndex == 5 || colIndex == 6	){	//Masiva y/o eliminar
			if (colIndex == 6){
				Ext.getCmp('hidOperacion').setValue('Eliminar');
			}else{
				masivoSub = "";
				masivoSub = "true";
				Ext.getCmp('hidOperacion').setValue('Generar');
			}
			Ext.Ajax.request({
				url : '24mantDoctoPendienteNeg.data.jsp',
				params : {
					informacion:'detalleConfirma',
					num_acuse:form.num_acuse
				},
				callback: procesarDetalleConfirma
			});
		}
	}
	var procesarConsultaMonitor = function(store, arrRegistros, opts) 	{
		if (arrRegistros != null) {
			if (!gridMonitor.isVisible()) {
				gridMonitor.show();
			}					
			var jsonData = store.reader.jsonData;
			var el = gridMonitor.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();													
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}			
		}	
	}	
	
	function mostrarArchivoPDF(opts, success, response) {
		Ext.getCmp('btnGenerarPDF').enable();
		var btnImprimirPDF = Ext.getCmp('btnGenerarPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarIndividualData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;	
			var  indiceCamposAdicionales = jsonData.indiceCamposAdicionales;
			var grid = Ext.getCmp('gridIndividual');
			var cm = grid.getColumnModel();
			storeIf = jsonData.cargaEPO;
			if(jsonData.tipoCredito=="S"&&individualSub=="true"){
				catalogoIFData.load();
				consultaMonitorData.load();
				comboIF.show();
				individualSub = "";
			}else{
				comboIF.hide();
				Ext.getCmp('gridMonitor').hide();
			}
			if (!gridIndividual.isVisible()) {
				gridIndividual.show();
			}
			if(indiceCamposAdicionales=='0'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
			}
			if(indiceCamposAdicionales=='1'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO1'),jsonData.CAMPO_ADICIONAL_1);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
			}
			if(indiceCamposAdicionales=='2'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO1'),jsonData.CAMPO_ADICIONAL_1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO2'),jsonData.CAMPO_ADICIONAL_2);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
			}
			if(indiceCamposAdicionales=='3'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO1'),jsonData.CAMPO_ADICIONAL_1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO2'),jsonData.CAMPO_ADICIONAL_2);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO3'),jsonData.CAMPO_ADICIONAL_3);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
			}
			if(indiceCamposAdicionales=='4'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO1'),jsonData.CAMPO_ADICIONAL_1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO2'),jsonData.CAMPO_ADICIONAL_2);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO3'),jsonData.CAMPO_ADICIONAL_3);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO4'),jsonData.CAMPO_ADICIONAL_4);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
			}
			if(indiceCamposAdicionales=='5'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO1'),jsonData.CAMPO_ADICIONAL_1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO2'),jsonData.CAMPO_ADICIONAL_2);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO3'),jsonData.CAMPO_ADICIONAL_3);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO4'),jsonData.CAMPO_ADICIONAL_4);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO5'),jsonData.CAMPO_ADICIONAL_5);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), false);
			}
			
			
				//F09-2015
			if(jsonData.meseSinIntereses=='S'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), false);			
			}else  {
				grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), true);		
			}
			
			var el = gridIndividual.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnConfirma').enable();
				el.unmask();
			}else{
				Ext.getCmp('btnConfirma').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	function procesaConsulta(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				consultaData.loadData('');
				if (!grid.isVisible()){
					grid.show();
				}
				if (infoR.registros != undefined){
					consultaData.loadData(infoR.registros);
					Ext.getCmp('btnImprimirPDF').enable();
					Ext.getCmp('btnImprimirPDF').setHandler( function(boton, evento) {
						var forma = Ext.getDom('formAux');
						forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
						forma.submit();
					});
					grid.getGridEl().unmask();
				}else{
					grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					Ext.getCmp('btnImprimirPDF').disable();
				}
			}			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarDetalleConfirma(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			pnlIndividual.hide();
			if ( Ext.getCmp('hidOperacion').getValue() == 'Eliminar'){
				Ext.getCmp('botonConfirmar').setText('Eliminar');
			}
			pnlConfirma.show();
			Ext.getCmp('nMn').body.update('<div align="center">0</div>');
			Ext.getCmp('nDl').body.update('<div align="center">0</div>');
			Ext.getCmp('totMnNe').body.update('<div align="right">$0.00</div>');
			Ext.getCmp('totDlNe').body.update('<div align="right">$0.00</div>');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				gridConfirmaPendNegoData.loadData('');
				if (!gridConfirmaPendNego.isVisible()){
					gridConfirmaPendNego.show();
				}
				if(infoR.tipoCredito=="S"&&masivoSub=="true"){
					storeIf =jsonData.cargaEPO;
					catalogoIFData.load();
					consultaMonitorData.load();
					gridMonitorMasivo.show();
					comboIFMasivo.show();
				}else{
					gridMonitorMasivo.hide();
					comboIFMasivo.hide();
				}
				if (infoR.regsNego != undefined && infoR.regsNego.length > 0){	
					Ext.getCmp('totMnNe').body.update('<div align="right">'+Ext.util.Format.number(infoR.bdTotalMNNeg, '$0,0.00')+'</div>');
					Ext.getCmp('totDlNe').body.update('<div align="right">'+Ext.util.Format.number(infoR.bdTotalDolNeg, '$0,0.00')+'</div>');
					form.regP = infoR.regsNego.length;
					gridConfirmaPendNegoData.loadData(infoR.regsNego);
					gridConfirmaPendNego.getGridEl().unmask();
					//*************  PARA LOS CAMPOS ADICIONALES ***************//
					var grid = Ext.getCmp('gridConfirmaPendNego');
					var cm = grid.getColumnModel();
					var  indiceCamposAdicionales = infoR.indiceCamposAdicionales;
					if(indiceCamposAdicionales=='0'){
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), true);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), true);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), true);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), true);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
					}
					if(indiceCamposAdicionales=='1'){
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO1'),infoR.CAMPO_ADICIONAL_1);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), true);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), true);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), true);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
					}
					if(indiceCamposAdicionales=='2'){
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO1'),infoR.CAMPO_ADICIONAL_1);	
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO2'),infoR.CAMPO_ADICIONAL_2);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), true);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), true);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
					}
					if(indiceCamposAdicionales=='3'){
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO1'),infoR.CAMPO_ADICIONAL_1);	
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO2'),infoR.CAMPO_ADICIONAL_2);	
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO3'),infoR.CAMPO_ADICIONAL_3);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), true);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
					}
					if(indiceCamposAdicionales=='4'){
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO1'),infoR.CAMPO_ADICIONAL_1);	
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO2'),infoR.CAMPO_ADICIONAL_2);	
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO3'),infoR.CAMPO_ADICIONAL_3);	
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO4'),infoR.CAMPO_ADICIONAL_4);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
					}
					if(indiceCamposAdicionales=='5'){
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO1'),infoR.CAMPO_ADICIONAL_1);	
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO2'),infoR.CAMPO_ADICIONAL_2);	
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO3'),infoR.CAMPO_ADICIONAL_3);	
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO4'),infoR.CAMPO_ADICIONAL_4);	
						grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_CAMPO5'),infoR.CAMPO_ADICIONAL_5);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO1'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO3'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO4'), false);	
						grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CAMPO5'), false);
					}
					
						//F09-2015
					if(jsonData.meseSinIntereses=='S'){
						grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), false);			
					}else  {
						grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOPAGO'), true);		
					}
			
				}else{
					form.regP = 0;
					gridConfirmaPendNego.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				}
				resumenTotalesData.loadData('');
				if (infoR.datosTotales != undefined && infoR.datosTotales.length > 0){
					resumenTotalesData
					var gridTotales=Ext.getCmp('gridTotales');
					if (!gridTotales.isVisible()){
						resumenTotalesData.loadData(infoR.datosTotales);
						gridTotales.show();
					}
				}else{
					gridTotales.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				}
				if ( infoR.iNoTotalDtosMN != undefined && parseFloat(infoR.iNoTotalDtosMN) > 0 ){
					Ext.getCmp('nMn').body.update('<div align="center">'+infoR.iNoTotalDtosMN+'</div>');
				}
				if ( infoR.iNoTotalDtosDol != undefined && parseFloat(infoR.iNoTotalDtosDol) > 0 ){
					Ext.getCmp('nDl').body.update('<div align="center">'+infoR.iNoTotalDtosDol+'</div>');
				}
				if(strPerfil == "EPO MAN2 DISTR"){
					Ext.getCmp('nLeyenda').show();
				}else{
					Ext.getCmp('nLeyenda').hide();
				}
				
			}
			if ( infoR.txtFirma != undefined ){
				form.textoFirmar = infoR.txtFirma;
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaData = function(store, arrRegistros, opts) {
		pnl.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();			
			if(store.getTotalCount() > 0) {				
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24mantDoctoPendienteNeg.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'CC_ACUSE'},
			{name: 'FECHA_CARGA'},
			{name: 'IC_USUARIO'},
			{name: 'IG_NUMERO_DOCTO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	

	var individualData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24mantDoctoPendienteNeg.data.jsp',
		baseParams: {
			informacion: 'Individual'
		},
		fields: [
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'IC_DOCUMENTO'},
			{name: 'DF_FECHA_EMISION'},
			{name: 'DF_FECHA_VENC'},
			{name: 'FN_MONTO'},
			{name: 'CT_REFERENCIA'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'IC_MONEDA', type: 'int'},
			{name: 'MONEDA'},
			{name: 'IC_PYME'},
			{name: 'CG_CAMPO1'},
			{name: 'CG_CAMPO2'},
			{name: 'CG_CAMPO3'},
			{name: 'CG_CAMPO4'},
			{name: 'CG_CAMPO5'},
			{name: 'CG_TIPO_CREDITO'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'DF_FECHA_VENC_CREDITO'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MODO_PLAZO'},
			{name: 'TIPO_CREDITO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'IC_TIPO_FINANCIAMIENTO'},
			{name: 'CATEGORIA'},
			{name: 'IC_CATEGORIA'},
			{name: 'ESTATUS'},
			{name: 'IC_ESTATUS'},
			{name: 'ACUSE'},
			{name: 'CG_RFC'},
			{name: 'CG_NUM_DISTRIBUIDOR'},
			{name: 'NOMBRE_DIST'},
			{name: 'TIPOPAGO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
										Ext.apply(options.params, {num_acuse: form.num_acuse});
									}
							},
			load: procesarIndividualData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarIndividualData(null, null, null);
				}
			}
		}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'MONEDA'},
			{name: 'TOTAL_DOC'},
			{name: 'TOTAL_MONTO'}
		],
		data:[
			{'MONEDA':'',	'TOTAL_DOC':'',	'TOTAL_MONTO':''}
		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
	var gridConfirmaPendNegoData = new Ext.data.JsonStore({
		fields: [
			{name: 'CG_RFC'},
			{name: 'NOMBRE_DIST'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'DF_FECHA_EMISION'},
			{name: 'DF_FECHA_VENC'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'MONEDA'},
			{name: 'IC_MONEDA'},
			{name: 'FN_MONTO'},
			{name: 'CATEGORIA'},
			{name: 'FN_PORC_DESCUENTO'},
			{name: 'MONTO_DESCONTAR'},//CD_DESCRIPCION
			{name: 'MODO_PLAZO'},
			{name: 'CG_CAMPO1'},
			{name: 'CG_CAMPO2'},
			{name: 'CG_CAMPO3'},
			{name: 'CG_CAMPO4'},
			{name: 'CG_CAMPO5'},
			{name: 'DF_FECHA_VENC_CREDITO'},
			{name: 'ESTATUS'},
			{name: 'CG_NUM_DISTRIBUIDOR'},
			{name: 'ACUSE'},
			{name: 'IC_ESTATUS'},
			{name: 'IG_PLAZO_CREDITO'},
			{name: 'IC_CATEGORIA'},
			{name: 'CG_TIPO_CREDITO'},
			{name: 'CT_REFERENCIA'},
			{name: 'TIPOPAGO'}
		],
		data:[
			{'CG_RFC':'',	'NOMBRE_DIST':'',	'IG_NUMERO_DOCTO':'',	'DF_FECHA_EMISION':'',	'DF_FECHA_VENC':'',	'IG_PLAZO_DESCUENTO':'',	'IG_PLAZO_DOCTO':'',
			'MONEDA':'',	'IC_MONEDA':'',	'FN_MONTO':'',	'CATEGORIA':'','FN_PORC_DESCUENTO':'','MONTO_DESCONTAR':'',	'MODO_PLAZO':'',
			'CG_CAMPO1':'',	'CG_CAMPO2':'',	'CG_CAMPO3':'',	'CG_CAMPO4':'','CG_CAMPO5':'','DF_FECHA_VENC_CREDITO':'',	'ESTATUS':'',
			'CG_NUM_DISTRIBUIDOR':'',	'ACUSE':'',	'IC_ESTATUS':'',	'IG_PLAZO_CREDITO':'','IC_CATEGORIA':'','CG_TIPO_CREDITO':'',	'CT_REFERENCIA':'', 'TIPOPAGO':''
			}
		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
	var consultaMonitorData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24mantDoctoPendienteNeg.data.jsp',
		baseParams: {
			informacion: 'MonitorLineasCredito'	
		},								
		fields: [			
			{name: 'IC_LINEA' , mapping: 'IC_LINEA'},
			{name: 'MONEDA_LINEA' , mapping: 'MONEDA_LINEA'},	
			{name: 'LINEA_CREDITO' , mapping: 'LINEA_CREDITO'},			
			{name: 'SALDO_INICIAL' , mapping: 'SALDO_INICIAL'},
			{name: 'SALDO_FINANCIADO' , mapping: 'SALDO_FINANCIADO'},
			{name: 'MONTO_PROCESO' , mapping: 'MONTO_PROCESO'},
			{name: 'SALDO_DISPONIBLE_LINEA' , mapping: 'SALDO_DISPONIBLE_LINEA'},
			{name: 'MONTO_NEGOCIABLE' , mapping: 'MONTO_NEGOCIABLE'},
			{name: 'MONTO_SELECCIONADO' , mapping: 'MONTO_SELECCIONADO'},
			{name: 'NUM_DOCTOS' , mapping: 'NUM_DOCTOS'},
			{name: 'SALDO_DISPONIBLE' , mapping: 'SALDO_DISPONIBLE'}
		],
		totalProperty : 'total',
		autoLoad: false,	
		listeners: {
			load: procesarConsultaMonitor,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConsultaMonitor(null, null, null);						
				}
			}
		}			
	});
	//COMBO DE IF PARA EL DETALLE DE LINEAS DE CREDITO
	var catalogoIFData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24mantDoctoPendienteNeg.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
	//	totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load:procesarCatalogoIF,
			exception: NE.util.mostrarDataProxyError
		}
	});
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 4, align: 'center'},
				{header: 'Autorizaci�n', colspan: 3, align: 'center'}
			]
		]
	});
	var selectModel = new Ext.grid.CheckboxSelectionModel( {
	  	listeners: {
			
			rowdeselect: function(selectModel, rowIndex, record) {	// cuando se quita la selecci�n
				record.data['SELECCIONAR']='N';
				record.commit();				
					  
			},
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //Se activa cuando una fila est� seleccionada, return false para cancelar.		
				record.data['SELECCIONAR']='S';	
				record.commit();
			}		
		}
	});

	var grid = new Ext.grid.GridPanel({
		store: 	consultaData,
		id:		'gridDoctosPNegociables',
		hidden: 	true,
		autoExpandColumn: 'Usuario',
		plugins: grupos,
		columns: [
			{
				header: 		'N�mero de Acuse',	tooltip:'N�mero de Acuse',	dataIndex:'CC_ACUSE',
				sortable: 	true,	resizable:true,	width:150,	hidden:false,	align:'center'
			},{
				header: 		'Fecha',	tooltip: 	'Fecha',	dataIndex:'FECHA_CARGA',
				sortable: 	true,	resizable:true,	width:90,	hidden:false,	align:'center'
			},{
				header: 		'Usuario',	id: 'Usuario',	tooltip:'Usuario',dataIndex: 	'IC_USUARIO',
				sortable: 	true,	resizable:true,	hidden:false,	align:'center'
			},{
				header: 		'N�mero Documentos',	tooltip:'N�mero Documentos',	dataIndex:'IG_NUMERO_DOCTO',
				sortable: 	true,	resizable: 	true,	width:120,	hidden:false,	align:'center'
			},{
            xtype: 		'actioncolumn',
				header:'Individual', tooltip:'Individual',	width:60,	hidden:false,	align:'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('CC_ACUSE') != '')	{
								this.items[0].tooltip = 'Ver';
								return 'modificar';
							}else	{
								return value;
							}
						},
						handler:	Automatizacion
					}
				]
			},{
            xtype: 		'actioncolumn',
				header:'Masiva', tooltip:'Masiva',	width:60,	hidden:false,	align:'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('CC_ACUSE') != '')	{
								this.items[0].tooltip = 'Ver';
								return 'modificar';
							}else	{
								return value;
							}
						},
						handler:	Automatizacion
					}
				]
			},{
            xtype: 		'actioncolumn',
				header:'Eliminar', tooltip:'Eliminar',	width:60,	hidden:false,	align:'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('CC_ACUSE') != '')	{
								this.items[0].tooltip = 'Ver';
								return 'rechazar';
							}else	{
								return value;
							}
						},
						handler:	Automatizacion
					}
				]
         }
		],
		stripeRows: true,
		loadMask: 	true,
		height: 		400,
		width: 		943,		
		frame: 		true,
		columnLines: true,
		style: 'margin:0 auto;',
		bbar: {
			items: [
				'->',
				{
			xtype: 'button',
			text: 'Generar PDF',
			iconCls:	'icoPdf',
			id: 'btnGenerarPDF',
			handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '24mantDoctoPendienteNeg.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ArchivoPDF'
					}),
					callback: mostrarArchivoPDF
				});
			}
			}
			]
		}
	});

	var gridTotales = new Ext.grid.GridPanel({
		store: resumenTotalesData,
		id: 'gridTotales',
		hidden:true,
		columns: [
			{
				header: '<center>MONEDA</center>',
				dataIndex: 'MONEDA',
				align: 'left',
				width: 200,
				resizable: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header: '<center>Total de Documentos</center>',
				dataIndex: 'TOTAL_DOC',
				width: 200,	
				align: 'center', 
				resizable: false
			},{
				header: '<center>Total Monto</center>', 
				tooltip:'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 200,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				resizable: false
			}
		],
		view: new Ext.grid.GridView({markDirty: false}),	
		width: 940,
		height: 120,
		columnLines: true,
		style: 'margin:0 auto;',
		loadMask: true,
		title: 'Totales',
		frame: false
	});

	var gridMonitor = new Ext.grid.EditorGridPanel({
		id: 'gridMonitor',
		title: 'Monitor de l�neas de cr�dito',
		hidden: true,
		clicksToEdit: 1,			
		columns: [		
		{							
			header : 'L�nea de Cr�dito IF',
			tooltip: 'L�nea de Cr�dito IF',
			dataIndex : 'LINEA_CREDITO',
			width : 400,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Saldo inicial de la l�nea',
			tooltip: 'Saldo inicial de la l�nea',
			dataIndex : 'SALDO_INICIAL',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Saldo actual de doctos financiados',
			tooltip: 'Saldo actual de doctos financiados',
			dataIndex : 'SALDO_FINANCIADO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Monto de Doctos en proceso de autorizaci�n',
			tooltip: 'Monto de Doctos en proceso de autorizaci�n',
			dataIndex : 'MONTO_PROCESO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Saldo disponible de la L�nea',
			tooltip: 'Saldo disponible de la L�nea',
			dataIndex : 'SALDO_DISPONIBLE_LINEA',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{	header : 'Monto de documentos negociables',
			tooltip: 'Monto de documentos negociables',
			dataIndex : 'MONTO_NEGOCIABLE',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{	header : 'Monto de documentos publicados',
			tooltip: 'Monto de documentos publicados',
			dataIndex : 'MONTO_SELECCIONADO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{	header : 'Num. doctos. publicados',
			tooltip: 'Num. doctos. publicados',
			dataIndex : 'NUM_DOCTOS',
			width : 150,
			align: 'center',
			sortable : false		
		},
		{	header : 'Saldo potencial disponible de la l�nea',
			tooltip: 'Saldo potencial disponible de la l�nea',
			dataIndex : 'SALDO_DISPONIBLE',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
		],
		displayInfo: true,
		store: consultaMonitorData,
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		frame: false
	});
	
	var gridMonitorMasivo = new Ext.grid.EditorGridPanel({
		id: 'gridMonitorMasivo',
		title: 'Monitor de l�neas de cr�dito',
		hidden: true,
		clicksToEdit: 1,			
		columns: [		
		{							
			header : 'L�nea de Cr�dito IF',
			tooltip: 'L�nea de Cr�dito IF',
			dataIndex : 'LINEA_CREDITO',
			width : 400,
			align: 'left',
			sortable : false
		},
		{							
			header : 'Saldo inicial de la l�nea',
			tooltip: 'Saldo inicial de la l�nea',
			dataIndex : 'SALDO_INICIAL',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Saldo actual de doctos financiados',
			tooltip: 'Saldo actual de doctos financiados',
			dataIndex : 'SALDO_FINANCIADO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Monto de Doctos en proceso de autorizaci�n',
			tooltip: 'Monto de Doctos en proceso de autorizaci�n',
			dataIndex : 'MONTO_PROCESO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{							
			header : 'Saldo disponible de la L�nea',
			tooltip: 'Saldo disponible de la L�nea',
			dataIndex : 'SALDO_DISPONIBLE_LINEA',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{	header : 'Monto de documentos negociables',
			tooltip: 'Monto de documentos negociables',
			dataIndex : 'MONTO_NEGOCIABLE',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{	header : 'Monto de documentos publicados',
			tooltip: 'Monto de documentos publicados',
			dataIndex : 'MONTO_SELECCIONADO',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{	header : 'Num. doctos. publicados',
			tooltip: 'Num. doctos. publicados',
			dataIndex : 'NUM_DOCTOS',
			width : 150,
			align: 'center',
			sortable : false		
		},
		{	header : 'Saldo potencial disponible de la l�nea',
			tooltip: 'Saldo potencial disponible de la l�nea',
			dataIndex : 'SALDO_DISPONIBLE',
			width : 150,
			align: 'right',
			sortable : false,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
		],
		displayInfo: true,
		store: consultaMonitorData,
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		frame: false   
	});

	var gridConfirmaPendNego = new Ext.grid.GridPanel({
		title:'Pendientes Negociables',
		store: gridConfirmaPendNegoData,
		id:'gridConfirmaPendNego',
		columns: [
			{
				header: 'N�mero de Distribuidor',
				tooltip: 'N�mero de Distribuidor',
				dataIndex: 'CG_NUM_DISTRIBUIDOR',
				sortable: true, 
				width: 150,
				resizable: true,
				align: 'center'
			},{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false,
				align: 'center'
			},{
				header : 'N�mero de Documento Inicial', 
				tooltip: 'N�mero de Documento Inicial',	
				dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true,
				width : 110, 
				align: 'center'
			},{
				header : 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex : 'DF_FECHA_EMISION',
				sortable : true,
				width : 110, 
				align: 'center', 
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				hidden: false
			},
			{
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_FECHA_VENC',
				sortable : true,
				width : 110, 
				align: 'center', 
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				hidden: false
			},{
				header : 'Plazo', 
				tooltip: 'Plazo',
				dataIndex : 'IG_PLAZO_DOCTO',
				sortable : true,
				width : 150,
				hidden: false,
				align: 'center'
			},{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false,
				align: 'center'
			},{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'FN_MONTO',
				sortable : true, 
				width : 110,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),
				hidden: false
			},{
				header : 'Categor�a', 
				tooltip: 'Categor�a',
				dataIndex : 'CATEGORIA',
				sortable : true,
				width : 110, 
				align: 'center'
			},{
				header : 'Plazo de Descuento en d�as',
				tooltip: 'Plazo de Descuento en d�as',	
				dataIndex : 'IG_PLAZO_DESCUENTO',
				sortable : true,
				width : 110,
				align: 'center'
			},{
				header : '% de Descuento',
				tooltip: '% de Descuento',
				dataIndex : 'FN_PORC_DESCUENTO',
				sortable : true,
				width : 150,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0 % ')
			},{
				header : 'Monto % Descuento',
				tooltip: 'Monto % Descuento',
				dataIndex : 'MONTO_DESCONTAR',
				sortable : true,
				width : 150,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header : 'Modalidad de Plazo', 
				tooltip: 'Modalidad de Plazo',
				dataIndex : 'MODO_PLAZO',
				sortable : true,
				width : 150,
				align: 'center'
			},
			{
				header: 'Tipo Pago',
				tooltip: 'Tipo Pago',	
				dataIndex: 'TIPOPAGO',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: false,
				align: 'center'
			},			
			{
				header : 'Comentarios',
				tooltip: 'Comentarios',	
				dataIndex : 'CT_REFERENCIA',
				sortable : true, 
				width : 110, 
				align: 'center'
			},
			{
				header: 'Campo 1',
				tooltip: 'Campo 1',	
				dataIndex: 'CG_CAMPO1',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Campo 2',
				tooltip: 'Campo 2',	
				dataIndex: 'CG_CAMPO2',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Campo 3',
				tooltip: 'Campo 3',	
				dataIndex: 'CG_CAMPO3',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Campo 4',
				tooltip: 'Campo 4',	
				dataIndex: 'CG_CAMPO4',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Campo 5',
				tooltip: 'Campo 5',	
				dataIndex: 'CG_CAMPO5',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: true,
				align: 'center'
			},
			{
				header : '<center>Plazo</center>',
				tooltip: 'Plazo',
				dataIndex : 'IG_PLAZO_CREDITO',
				sortable : true,
				width : 110,
				align: 'right'
			},
			{
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_FECHA_VENC_CREDITO',
				sortable : true,
				width : 110,
				align: 'center'
			},
			{
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable : true,
				width : 110,
				align: 'center'
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 180,
		width: 940,
		columnLines: true,
		style: 'margin:0 auto;'
	});
	var comboIF = new Ext.Container({
		layout: 'table',
		id: 'comboIF',
		hidden: true,		
		width:	'auto',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [	
			{ 
				xtype:   'label',  
				html:		'Seleccionar todos los documentos con el siguiente IF:', 
				cls:		'x-form-item', 
				style: { 
					width:   		'100', 
					textAlign: 		'left'
				} 
			},							
			{
				xtype: 'combo',
				name: 'lineaTodos',
				id: 'lineaTodos1',
				fieldLabel: 'Seleccionar todos los documentos con el siguiente IF',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'lineaTodos',
				emptyText: 'Seleccione...',
				forceSelection : true,
				triggerAction : 'all',
				width: 500,
				typeAhead: true,
				minChars : 1,
				store : catalogoIFData
			}		
		]
	});
	
	var comboIFMasivo = new Ext.Container({
		layout: 'table',
		id: 'comboIFMasivo',
		hidden: true,		
		width:	'auto',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [	
			{ 
				xtype:   'label',  
				html:		'Seleccionar todos los documentos con el siguiente IF:', 
				cls:		'x-form-item', 
				style: { 
					width:   		'100', 
					textAlign: 		'left'
				} 
			},							
			{
				xtype: 'combo',
				name: 'lineaTodosMasivo',
				id: 'lineaTodos1Masivo',
				fieldLabel: 'Seleccionar todos los documentos con el siguiente IF',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'lineaTodosMasivo',
				emptyText: 'Seleccione...',
				forceSelection : true,
				triggerAction : 'all',
				width: 500,
				typeAhead: true,
				minChars : 1,
				store : catalogoIFData
			}		
		]
	});


	var elementosConfirma = [
		{
			xtype: 'panel',layout:'table',	width:550,	border:true,	layoutConfig:{ columns: 3 },
			defaults: {frame:false, border: true,width:165, height: 35,bodyStyle:'padding:2px'},
			items:[
				{	width:230,	frame:true,	border:false,	html:'&nbsp;'	},
				{	border:false,	frame:true,	html:'<div align="center">Moneda Nacional</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">D�lares</div>'	},
				{	width:230,	id:'ntNd',html:'<div align="right">No. Total de documentos Pendientes<br>Negociables cargados</div>'	},
				{	id:'nMn',	html: '&nbsp;'	},
				{	id:'nDl',	html:'&nbsp;'	},
				{	width:230,id:'nMtd',	html:'<div align="right">Monto total de los documentos Pendientes Negociables cargados</div>'	},
				{	id:'totMnNe',	html: '&nbsp;'	},
				{	id:'totDlNe',	html: '&nbsp;'	},
				{	width:550,id:'nLeyenda', hidden:true,	height: 310,	colspan:3,	
                    html:'<div align="left">' +
                        'Al trasmitir este mensaje de datos y para todos los efectos legales Usted, bajo su responsabilidad, ' +
                        'est� de acuerdo en que el (los) Documento (s) inicial (es) que se detalla (n) en est� pantalla puedan ser ' +
                        'operados indistintamente a trav�s de las siguientes modalidades, en t�rminos de los pactado en el �Contrato de ' +
                        'Financiamiento a Clientes y Distribuidores� (para conocer las modalidades convenidas favor de revisar su contrato):<br><br>  ' +
                        'a) DESCUENTO y/o FACTORAJE. De operarse en esta modalidad y para todos los efectos legales, bajo su responsabilidad,est� solicitando ' +
                        'el DESCUENTO y/o FACTORAJE del (los) Documento (s) inicial (es) que se detallan en est� pantalla y que ha publicado electr�nicamente, ' +
                        'otorgando su consentimiento para que en caso de que se den la condiciones se�aladas en el �Contrato de Financiamiento a Clientes y Distribuidores�, ' +
                        'el (los) Documento (s) inicial (es) ser� (n) sustituido (s) por el (los) DOCUMENTO(S) FINAL (ES), cediendo los derechos de cobro a su favor al ' +
                        'INTERMEDIARIO FINANCIERO que los opere.<br><br> ' +
                        'b) Pago del CLIENTE o DISTRIBUIDOR mediante Tarjeta de Cr�dito. De operarse en esta modalidad y para todos los efectos legales, ' +
                        'otorga su consentimiento para que sea cubierto el (los) Documento (s) inicial (es) que se detalla (n) en est� pantalla y que ha publicado electr�nicamente, ' +
                        'en t�rminos del �Contrato de Financiamiento a Clientes y Distribuidores�, mediante la utilizaci�n de una Tarjeta de Cr�dito asociada al CLIENTE o DISTRIBUIDOR.<br/><br/>' +
                        'Asimismo, en este acto manifiesto bajo protesta de decir verdad, que s� he emitido o emitir� a mi CLIENTE o DISTRIBUIDOR el CFDI por la operaci�n comercial ' +
                        'que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.'+
                        '</div>' 
                    }
			]
		},{
			xtype: 'hidden', id:'hidOperacion',	value:''
		}
	];

	var fpConfirma	=	new Ext.FormPanel({style: 'margin:0 auto;',	autoHeight:true,	bodyStyle:'padding:2px',	width:556,	border:true,	frame:false,	items:elementosConfirma,	hidden:false,
		buttons: [
			{
				id:		'botonConfirmar',
				text: 	'Confirmar',
				iconCls: 'correcto',
				handler:	function(btn) {
					NE.util.obtenerPKCS7(confirmar, form.textoFirmar, btn);
				}
			},{
				text: 	'Cancelar',
				iconCls: 'rechazar',
				handler: function() {
					Ext.Msg.show({
						msg: '�Est� usted seguro de cancelar la operaci�n?',
							buttons: Ext.Msg.OKCANCEL,
							fn: processResult,
							icon: Ext.MessageBox.INFO
						});
				}
			}
		]
	});

	var confirmar = function(pkcs7, textoFirmar, btn) {
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}
		form.pkcs7 = pkcs7;
		if (btn.text == 'Eliminar'){
			Ext.Msg.show({
				msg: '�Est� usted seguro de eliminar los registros seleccionados?',
				buttons: Ext.Msg.OKCANCEL,
				fn: processResultElimina,
				icon: Ext.MessageBox.INFO
			});
		}else{
			pnl.el.mask('Procesando...', 'x-mask-loading');
			if(masivoSub == "true"){
				ic_documento[0]="T";
				selecIF= Ext.getCmp('lineaTodos1Masivo').getValue();
				if(storeIf ==selecIF ){
					selecIF = "";
					ic_documento =[];
				}
			}
			if(individualSub == "true"){
				if(storeIf ==selecIF ){
					selecIF = "";
					ic_documento =[];
				}
			}
			Ext.Ajax.request({
				url : '24mantDoctoPendienteNeg.data.jsp',
				params : {
					informacion:			'Confirma',
					num_acuse:				form.num_acuse,
					cadDocSelec:			form.cadDocSelec,
					regPrenegociables:	form.regP,
					regPrenonegociables:	form.regPN,
					operacion:				Ext.getCmp('hidOperacion').getValue(),
					Pkcs7:					form.pkcs7,
					TextoFirmado:			form.textoFirmar,
					selecIF : selecIF,
					ic_documento:ic_documento
				},
				callback: procesaConfirma
			});
		}
	}

	var gridIndividual = new Ext.grid.GridPanel({
		store: individualData,
		id:'gridIndividual',
		columns: [
			{
				header: '<center>N�mero de distribuidor</center>',
				tooltip: '<center>N�mero de distribuidor </center>',
				dataIndex: 'CG_NUM_DISTRIBUIDOR',
				sortable: true,
				width: 150, 
				resizable:true,
				align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false,
				align: 'center'
			},{
				header : 'N�mero documento inicial',
				tooltip: 'N�mero documento inicial',
				dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true,
				width : 150,
				align: 'center'
			},{
				header : 'Fecha de Emisi�n', 
				tooltip: 'Fecha de Emisi�n',
				dataIndex : 'DF_FECHA_EMISION',
				sortable : true,
				width : 100, 
				align: 'center', 
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Fecha de Vencimiento', 
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_FECHA_VENC',
				sortable : true,
				width : 100, 
				align: 'center', 
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},{
				header : 'Plazo',
				tooltip: 'plazo',
				dataIndex : 'IG_PLAZO_DOCTO',
				sortable : true,
				width : 100,
				align: 'center'
			},{
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable : true,
				width : 150,
				hidden: false,
				align: 'center'
			},{
				header : '<center>Monto</center>',
				tooltip: 'Monto',
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Categor�a',
				tooltip: 'Categor�a',	
				dataIndex: 'CATEGORIA',
				sortable: true,
				width: 100, 
				resizable: true,
				hidden: false,
				align: 'center'
			},{
				header: 'Plazo de descuento en d�as ',
				tooltip: 'Plazo de desuento en d�as',	
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				width: 100, 
				resizable: true,
				hidden: false,
				align: 'center'
			},{
				header: '% de Descuento ',
				tooltip: '% de Descuento',	
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				width: 100, 
				resizable: true,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0 %')
			},{
				header: 'Monto  % Descuento',
				tooltip: 'Monto % Descuento',	
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				width: 100, 
				resizable: true,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header: 'Modalidad de Plazo',
				tooltip: 'Modalidad de Plazo',	
				dataIndex: 'MODO_PLAZO',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Tipo Pago',
				tooltip: 'Tipo Pago',	
				dataIndex: 'TIPOPAGO',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: false,
				align: 'center'
			},			
			{
				header: 'Comentarios',
				tooltip: 'Comentarios',	
				dataIndex: 'CT_REFERENCIA',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Campo 1',
				tooltip: 'Campo 1',	
				dataIndex: 'CG_CAMPO1',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Campo 2',
				tooltip: 'Campo 2',	
				dataIndex: 'CG_CAMPO2',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Campo 3',
				tooltip: 'Campo 3',	
				dataIndex: 'CG_CAMPO3',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Campo 4',
				tooltip: 'Campo 4',	
				dataIndex: 'CG_CAMPO4',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Campo 5',
				tooltip: 'Campo 5',	
				dataIndex: 'CG_CAMPO5',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',	
				dataIndex: 'IG_PLAZO_CREDITO',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',	
				dataIndex: 'DF_FECHA_VENC_CREDITO',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',	
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 150, 
				resizable: true,
				hidden: false,
				align: 'center'
			},
			selectModel
		],
		stripeRows: true,
		loadMask: true,//
		height: 400,
		width: 943,
		columnLines: true,
		sm:selectModel,
		style: 'margin:0 auto;',
		bbar:{
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'bbarIndividual',
			displayInfo: true,
			store: individualData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: ['->','-',
				{
					xtype: 'button',
					text: 'Teminar Captura',
					id: 'btnConfirma',
					disabled: true,
					handler: function(boton, evento) {
						var conf = false;
						var ds = individualData;
						var jsonData = ds.reader.jsonData;
						var countChecksE =0;
						var  gridConsulta = Ext.getCmp('gridIndividual');
						var store = gridConsulta.getStore();		
						store.each(function(record) {
							if(record.data['SELECCIONAR']=='S')  {
								countChecksE++;
							}
						});
						if (countChecksE==0){
							Ext.Msg.alert(boton.text,'Seleccione al menos un documento');
								return;
							}
						var selecDoc = [];
						var cadDocSelec = "";
						ic_documento = [];
						var i=0;
						store.each(function(record) {
							if (record.data['SELECCIONAR']=='S'){
								if(i==0){
									cadDocSelec += "'" + record.data['IG_NUMERO_DOCTO'] + "'";
									ic_documento[i]=record.data['IC_DOCUMENTO'] ;
								}else{
									cadDocSelec += ",'" + record.data['IG_NUMERO_DOCTO'] + "'";
									ic_documento[i]=record.data['IC_DOCUMENTO'] ;
								}
								i++;
							}
						});
						selecIF= Ext.getCmp('lineaTodos1').getValue();
						form.cadDocSelec = cadDocSelec;
						Ext.getCmp('hidOperacion').setValue("Generar");
						pnl.el.mask('Procesando...', 'x-mask-loading');
						Ext.Ajax.request({
							url : '24mantDoctoPendienteNeg.data.jsp',
							params : {
								informacion:'detalleConfirma',
								cadDocSelec:form.cadDocSelec,
								num_acuse:form.num_acuse
							},
							callback: procesarDetalleConfirma
						});
					}
				},'-',{
					xtype: 'button',
					text: 'Regresar',
					id: 'btnRegInd',
					handler: function(boton, evento) {
						regresar('24mantDoctoPendienteNeg.jsp');
					}
				}
			]
		}
	});

	var elementosIndividual = [
		{
			xtype: 'panel',layout:'table',	width:700,	border:true,	layoutConfig:{ columns: 4 },
			defaults: {frame:false, border: true,width:150, height: 25,bodyStyle:'padding:5px', align:'center'},
			items:[
				{	frame:true,	border:false,	html:'<div align="center">N�mero de Acuse</div>'	},
				{	frame:true,	border:false,	html:'<div align="center">Fecha</div>'	},
				{	frame:true,	border:false,	width:300,	html:'<div align="center">Usuario</div>'	},
				{	frame:true,	border:false,	width:100,	html:'<div align="center">N�mero Doctos</div>'	},
				{	id:'disAcuse',	html:'&nbsp;'	},
				{	id:'disFecha',	html:'&nbsp;'	},
				{	id:'disUsuario',	width:300,	html:'&nbsp;'	},
				{	id:'disDoctos',	width:100,	html:'&nbsp;'	}
			]
		}
	];
	var fpIndividual=new Ext.FormPanel({style:'margin: 0 auto',	autoHeight:true,	bodyStyle:'padding:2px',	width:706,	border:true,	frame:false,	items:elementosIndividual,	hidden:false 	});
	var elementosFormaDoctosPNegociables = [
		
		{
			xtype: 		'textfield',
			name: 		'cc_acuse',
			id: 			'numeroAcuse',
			fieldLabel: 'N�mero de Acuse',
			allowBlank: true,
			regex : /^[-_\w|\s]*$/i,
			hidden: 		false,
			maxLength: 	15,	
			msgTarget: 	'side',
			anchor:		'65%'
		},
		{
			xtype: 						'compositefield',
			fieldLabel: 				'Fecha de Carga',
			combineErrors: 			false,
			msgTarget: 					'side',
			items: [
				{
					xtype: 				'datefield',
					name: 				'fechaCargaIni',
					id: 					'fechaCargaIni',
					allowBlank: 		true,
					startDay: 			0,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoFinFecha: 	'fechaCargaFin',
					margins: 			'0 20 0 0'  
				},{
					xtype: 				'displayfield',
					value: 				'a',
					width: 				20
				},{
					xtype: 				'datefield',
					name: 				'fechaCargaFin',
					id: 					'fechaCargaFin',
					allowBlank: 		true,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoInicioFecha: 'fechaCargaIni',
					margins: 			'0 20 0 0'  
				}
			]
		},
		{ 
			xtype: 		'textfield',
			name: 		'ic_usuario',
			id: 			'numeroUsuario',
			fieldLabel: 'Usuario',
			regex : /^[\d]*$/i,
			allowBlank: true,
			hidden: 		false,
			maxLength: 	15,	
			msgTarget: 	'side',
			anchor:		'70%'
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 				'forma',
		width: 			420,
		title: 			'Consulta de Doctos Pendientes',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	115,
		defaultType: 	'textfield',
		items: 			elementosFormaDoctosPNegociables,
		monitorValid: 	false,
		buttons: [
			{
				id:				'botonConsultar',
				text: 			'Consultar',
				errorActivo:	false,
				iconCls: 		'icoBuscar',
				handler: 		function() {
					form.hayCheck = false;
					pnl.el.mask('Procesando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							start: 0,
							limit: 15,
							operacion:'Generar',
							tipoConsulta :'Consultar'
						})
					});
				}
			}
			,
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	function() {
					regresar('24mantDoctoPendienteNeg.jsp');
				}
			}
		]
	});

	var pnlConfirma = new Ext.Container({
		id: 		'contenedorConfirma',
		width: 	949,
		height: 	'auto',
		hidden: true,
		items: 	[
			
			NE.util.getEspaciador(10),
			fpConfirma,
			NE.util.getEspaciador(5),
			gridMonitorMasivo,
			NE.util.getEspaciador(10),
			comboIFMasivo,
			NE.util.getEspaciador(20),
			gridConfirmaPendNego,
			NE.util.getEspaciador(10),
			gridTotales
		]
	});

	var pnlIndividual = new Ext.Container({
		id: 		'contenedorIndividual',
		width: 	949,
		height: 	'auto',
		hidden: true,
		items: 	[
			gridMonitor,
			NE.util.getEspaciador(20),
			comboIF,
			NE.util.getEspaciador(20),
			fpIndividual,
			NE.util.getEspaciador(20),
			gridIndividual
			
		]
	});

	//-------------------------------- FORMA PRINCIPAL ------------------------------
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			grid,
			pnlIndividual,
			pnlConfirma
		]
	});
	if(strPerfil=="EPO MAN1 DISTR"){
		Ext.getCmp("gridConfirmaPendNego").setTitle("Pendientes Negociables");//forma
		Ext.getCmp("forma").setTitle("Consulta de Doctos Pendientes ");
		
		Ext.getCmp('ntNd').body.update('<div align="center">'+"No. Total de documentos Pendientes<br>Negociables cargados"+'</div>');
		Ext.getCmp('nMtd').body.update('<div align="center">'+"Monto total de los documentos Pendientes Negociables cargados"+'</div>');
	}else{
		Ext.getCmp("gridConfirmaPendNego").setTitle("Pre-Negociables");
		Ext.getCmp("forma").setTitle("Consulta de Doctos Pre-Negociables");
		
		Ext.getCmp('ntNd').body.update('<div align="center">'+"No. Total de documentos<br>Pre-Negociables cargados"+'</div>');
		Ext.getCmp('nMtd').body.update('<div align="center">'+"Monto total de los documentos<br>Pre-Negociables cargados"+'</div>');
	 }
	
	
});