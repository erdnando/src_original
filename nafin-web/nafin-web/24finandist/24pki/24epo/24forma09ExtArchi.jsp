<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.anticipos.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
CreaArchivo archivo = new CreaArchivo();		
String nombreArchivo=null;
JSONObject jsonObj = new JSONObject();
	
if(informacion.equals("GenerarArchivoError")  ) {	
	
	String archivoErrores  = (request.getParameter("archivo") != null) ? request.getParameter("archivo") : "";
	if (!archivo.make(archivoErrores, strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo CSV ");
	} else {
		nombreArchivo = archivo.nombre;		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}

}else if(informacion.equals("ACUSECSV")  ||   informacion.equals("ACUSEPDF") ) {	

	String tipoArchivo = (request.getParameter("tipoArchivo") != null) ? request.getParameter("tipoArchivo") : "";
	String acuse = (request.getParameter("acuse") != null) ? request.getParameter("acuse") : "";
	String fecha = (request.getParameter("fecha") != null) ? request.getParameter("fecha") : "";
	String hora = (request.getParameter("hora") != null) ? request.getParameter("hora") : "";
	String usuario = (request.getParameter("usuario") != null) ? request.getParameter("usuario") : "";
	String recibo	= (request.getParameter("recibo")==null)?"":request.getParameter("recibo").trim();
	String totaldoc = (request.getParameter("totaldoc")==null)?"0":request.getParameter("totaldoc").trim();
	String totaldocdl = (request.getParameter("totaldocdl")==null)?"0":request.getParameter("totaldocdl").trim();
	String montodoc = (request.getParameter("montodoc")==null)?"0":request.getParameter("montodoc").trim();
	String montodocdl = (request.getParameter("montodocdl")==null)?"0":request.getParameter("montodocdl").trim();
	String lineabien 	= (request.getParameter("lineabien")==null)?"0":request.getParameter("lineabien").trim();
	String lineaerror = (request.getParameter("lineaerror")==null)?"0":request.getParameter("lineaerror").trim();
	String linea	 = (request.getParameter("linea")==null)?"":request.getParameter("linea").trim();
	String icDoctos	= (request.getParameter("icDoctos")==null)?"":request.getParameter("icDoctos").trim();
	String strPreacuse	= (request.getParameter("strPreacuse")==null)?"":request.getParameter("strPreacuse").trim();
	
	StringBuffer contenidoArchivo = new StringBuffer();
	
	ComunesPDF pdfDoc		= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual		= fechaActual.substring(0,2);
	String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual		= fechaActual.substring(6,10);
	String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	if(tipoArchivo.equals("CSV")){
		contenidoArchivo.append("La autentificación se llevó a cabo con éxito\nRecibo: "+recibo+"\n");
		contenidoArchivo.append(",Moneda Nacional, Dólares\n");
		contenidoArchivo.append("No. total de documentos cargados,"+totaldoc+","+totaldocdl+"\n");
		contenidoArchivo.append("Monto total de los documentos cargados, $"+montodoc+", $"+montodocdl+"\n");
		contenidoArchivo.append("Número de Acuse,"+acuse+"\n");
		contenidoArchivo.append("Fecha,"+fecha+"\n");
		contenidoArchivo.append("Hora,"+hora+"\n");
		contenidoArchivo.append("Usuario,"+usuario+"\n");
		contenidoArchivo.append("Clave del proveedor,Número de Documento,Monto,Fecha de Vencimiento,Modalidad de Plazo,Causa\n");
	}
	
	if(tipoArchivo.equals("PDF")){
		
		archivo = new CreaArchivo();
		nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";	
		pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);			 
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);
		
		pdfDoc.addText(" Mantenimiento Masivo de Documentos ","formas",ComunesPDF.CENTER);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);

		pdfDoc.setTable(3,50);
		pdfDoc.setCell("La autentificación se llevó a cabo con éxito Recibo    "+recibo+"","celda01rep",ComunesPDF.CENTER,3);	
		pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda Nacional","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setCell("Dólares","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setCell("No. total de documentos cargados","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setCell(totaldoc,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(totaldocdl,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Acuse","celda01rep",ComunesPDF.LEFT);
		pdfDoc.setCell(acuse,"formas",ComunesPDF.LEFT,2);
		pdfDoc.setCell("Fecha","celda01rep",ComunesPDF.LEFT);
		pdfDoc.setCell(fecha,"formas",ComunesPDF.LEFT,2);
		pdfDoc.setCell("Hora","celda01rep",ComunesPDF.LEFT);
		pdfDoc.setCell(hora,"formas",ComunesPDF.LEFT,2);
		pdfDoc.setCell("Usuario","celda01rep",ComunesPDF.LEFT);
		pdfDoc.setCell(usuario,"formas",ComunesPDF.LEFT,2);
		pdfDoc.addTable();

		pdfDoc.setTable(6,100);
		pdfDoc.setCell("Clave del proveedor ","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Documentor ","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto ","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Vencimiento ","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setCell("Modalidad de Plazo ","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setCell("Causa ","celda01rep",ComunesPDF.CENTER);
	}
	
	StringTokenizer st = new StringTokenizer(strPreacuse, "|");
	while(st.hasMoreTokens()){
		String slinea = st.nextToken();
		VectorTokenizer vt = new VectorTokenizer(slinea, ",");
		Vector vlinea = vt.getValuesVector();
		
		if(tipoArchivo.equals("CSV")){
		contenidoArchivo.append(vlinea.get(0)+","+vlinea.get(1)+",$"+vlinea.get(2)+","+vlinea.get(3)+",Plazo Ampliado,"+vlinea.get(4)+"\n");
		}
		
		if(tipoArchivo.equals("PDF")){
		pdfDoc.setCell(vlinea.get(0).toString(),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(vlinea.get(1).toString(),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(vlinea.get(2).toString(),2)   ,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(vlinea.get(3).toString(),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo Ampliado","formas",ComunesPDF.CENTER);
		pdfDoc.setCell(vlinea.get(4).toString(),"formas",ComunesPDF.CENTER);	
		}
	}//se termina el while
		
	if(tipoArchivo.equals("CSV")){
		contenidoArchivo.append("Total Moneda Nacional:, $"+montodoc+"\n");
		contenidoArchivo.append("Total Dólares:, $"+montodocdl+"\n");
		contenidoArchivo.append("Total Documentos en Moneda Nacional:,"+totaldoc+"\n");
		contenidoArchivo.append("Total Documentos en Dólares:,"+totaldocdl+"\n");
	}
	
	pdfDoc.setCell("Total Moneda Nacional:","celda01rep",ComunesPDF.RIGHT,2);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(montodoc,2) ,"formas",ComunesPDF.LEFT,4);
	pdfDoc.setCell("Total Dólares:","celda01rep",ComunesPDF.RIGHT,2);
	pdfDoc.setCell("$"+Comunes.formatoDecimal(montodocdl,2) ,"formas",ComunesPDF.LEFT,4);
	pdfDoc.setCell("Total Documentos en Moneda Nacional:","celda01rep",ComunesPDF.RIGHT,2);
	pdfDoc.setCell(totaldoc,"formas",ComunesPDF.LEFT,4);
	pdfDoc.setCell("Total Documentos en Dólares:","celda01rep",ComunesPDF.RIGHT,2);
	pdfDoc.setCell(totaldocdl,"formas",ComunesPDF.LEFT,4);
		
	if(tipoArchivo.equals("CSV")){
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
	
	
	if(tipoArchivo.equals("PDF")){	
		pdfDoc.addTable();
		pdfDoc.endDocument();		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
	}

	
}

	
%>
<%=jsonObj%>

<%System.out.println("jsonObj-----"+jsonObj);%>
