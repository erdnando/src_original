<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	com.netro.anticipos.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.CatalogoEPODistribuidores,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	CreaArchivo archivo = new CreaArchivo();		
	String nombreArchivo=null;
	JSONObject jsonObj = new JSONObject();
	
if(informacion.equals("GenerarArchivoError")  ) {	

	String archivoErrores  = (request.getParameter("archivo") != null) ? request.getParameter("archivo") : "";
	
	if (!archivo.make(archivoErrores, strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo CSV ");
	} else {
		nombreArchivo = archivo.nombre;		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}

}
	
%>
<%=jsonObj%>