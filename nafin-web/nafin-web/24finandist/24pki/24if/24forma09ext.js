Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

	form = {iClient:null, usuarioLogin:null,	actionBoton:"Capt",	textoF:null,	ic_epo:null,	ic_pyme:null}

	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			form.iClient = Ext.util.JSON.decode(response.responseText).iCliente;
			form.usuarioLogin = Ext.util.JSON.decode(response.responseText).usuario;
			pnl.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDF_acuse =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfAcuse');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarAcuse');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarConfirmar(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fp.hide();
			gridCaptura.hide();
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('disTitle').body.update('<div align="center"><b>'+infoR.msg+'<br>Recibo N�mero:  '+infoR._acuse+'</b></div>');
			Ext.getCmp('disAcuse').body.update(infoR.acuse);
			Ext.getCmp('disFechaCarga').body.update(infoR.fechaCarga);
			Ext.getCmp('disHoraCarga').body.update(infoR.horaCarga);
			Ext.getCmp('disUsuario').body.update('<div class="formas" >'+infoR.usuario+'<div>');

			fpAcuse.show();

			fpAcuse.el.mask('Enviando...', 'x-mask-loading');
			consultaData.load({	params: Ext.apply({	operacion: 'Generar',	start: 0,	limit: 15	})	});

			Ext.getCmp('btnPdfAcuse').setHandler(
				function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '24forma09pdf_ext.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
									_acuse: infoR._acuse,
									acuse: infoR.acuse,
									fechaHoy:infoR.fechaCarga,
									horaActual:infoR.horaCarga,
									mensaje:infoR.msg
						}),
						callback: procesarSuccessFailureGenerarPDF_acuse
					});
				}
			);

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		pnl.el.unmask();
		if (arrRegistros != null) {
			if(form.actionBoton === "Capt"){
				if(fpAcuse.isVisible()){
					fpAcuse.el.unmask();
					if (!gridConsulta.isVisible()) {
						gridConsulta.show();
					}
					var el = gridConsulta.getGridEl();
					Ext.getCmp('btnGenerarPDF').hide();
					Ext.getCmp('btnBajarPDF').hide();
					Ext.getCmp('btnBajarAcuse').hide();
					Ext.getCmp('btnRegresar').show();
					if(store.getTotalCount() > 0) {
						gridConsulta.setTitle('<div align="center">Los cambios se realizaron con �xito</div>');
						Ext.getCmp('btnPdfAcuse').enable();
						el.unmask();
					}else{
						Ext.getCmp('btnPdfAcuse').disable();
						el.mask('No se encontr� ning�n registro', 'x-mask');
					}
				}else{
					if (!gridCaptura.isVisible()) {
						gridCaptura.show();
					}
					var el = gridCaptura.getGridEl();
					if(store.getTotalCount() > 0) {
						Ext.getCmp('btnConfirmar').enable();
						Ext.getCmp('btnCancelar').enable();
						var jsonData = store.reader.jsonData.registros;
						form.textoF = "N@E,PyME,RFC,Bloqueado,Fecha/HoraBloqueo/Desbloqueo,IFBloqueo/Desbloqueo,Causa\n";
						Ext.each(jsonData, function(item,index,arrItem){
							if(item.BLOQUEADO==item.AUXBLOQUEADO){
									form.textoF += item.NE+","+item.RZ+","+item.RFC+","+item.BLOQUEADO+","+item.FECHA+",,"+item.CAUSA+"\n";
							}else{
									form.textoF += item.NE+","+item.RZ+","+item.RFC+","+item.BLOQUEADO+","+item.FECHA+","+item.LOG_USUARIO+","+item.CAUSA+"\n";
							}
						});
						el.unmask();
					}else{
						Ext.getCmp('btnConfirmar').disable();
						Ext.getCmp('btnCancelar').disable();
						el.mask('No se encontr� ning�n registro', 'x-mask');
					}				
				}

			}else{
				if (!gridConsulta.isVisible()) {
					gridConsulta.show();
				}
				var el = gridConsulta.getGridEl();
				Ext.getCmp('btnBajarPDF').hide();
				Ext.getCmp('btnPdfAcuse').hide();
				Ext.getCmp('btnBajarAcuse').hide();
				Ext.getCmp('btnRegresar').hide();
				if(store.getTotalCount() > 0) {
					Ext.getCmp('btnGenerarPDF').enable();
					el.unmask();
				}else{
					Ext.getCmp('btnGenerarPDF').disable();
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	}

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma09ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma09ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDistribuidor'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '24forma09ext.data.jsp',
		baseParams: {	informacion: 'Consulta'	},
		fields: [
			{name:'ICPYME'},
			{name:'ICEPO'},
			{name:'NE'},
			{name:'RZ'},
			{name:'RFC'},
			{name:'FECHA'},
			{name:'BLOQUEADO', convert: NE.util.string2boolean},
			{name:'AUXBLOQUEADO', convert: NE.util.string2boolean},
			{name:'CAUSA'},
			{name:'LOG_USUARIO'}
			
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
				beforeLoad:	{fn: function(store, options){
								Ext.apply(options.params, {
										ic_epo:form.ic_epo,
										ic_pyme:form.ic_pyme
								})}},
				load: procesarConsultaData,
				exception: {
							fn: function(proxy,type,action,optionsRequest,response,args){
									NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
									procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
							}
				}
		}
	});

	var elementosAcuse = [
		{
			xtype: 'panel',layout:'table',	width:500,	border:true,	layoutConfig:{ columns: 2 },
			defaults: {frame:false, border: true,width:160, height: 35,bodyStyle:'padding:4px'},
			items:[
				{	id:'disTitle',	width:500,	colspan:2,	border:false,	height:45,	frame:true,	html:'<div align="center">Recibo N�mero<br>Recibo N�mero:</div>'	},
				{	height: 33,	border:false,	frame:true,	html:'&nbsp;'	},
				{	height: 33,	width:340,	border:false,	frame:true,		html:'<div class="formas" align="center">Moneda Nacional</div>'	},
				{	html:'<div class="formas" align="left">N�mero de Acuse</div>'	},
				{	width:340,id:'disAcuse',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Fecha</div>'	},
				{	width:340,id:'disFechaCarga',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Hora</div>'	},
				{	width:340,id:'disHoraCarga',	html:'&nbsp;'},
				{	html:'<div class="formas" align="left">Usuario</div>'	},
				{	width:340,id:'disUsuario',html: '&nbsp;'	}
			]
		}
	];

	var fpAcuse=new Ext.FormPanel({	style: 'margin:0 auto;',	height:'auto',	width:502,	border:true,	frame:false,	items:elementosAcuse,	hidden:true 	});

	var confirmaAcuse = function(pkcs7, textoFirma, info  ){
	
		if (Ext.isEmpty(pkcs7)) {
			Ext.Msg.alert('Firmar','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;	//Error en la firma. Termina...
		}else  {
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url : '24forma09ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),	{
					informacion:'Confirmar', 
					cadena:info,
					Pkcs7:pkcs7,
					TextoFirmado:textoFirma
				}),
				callback: procesarConfirmar
			});
		}													
	}	
	
	var gridCaptura = new Ext.grid.EditorGridPanel({
		id:'gridCaptura',store: consultaData,	clicksToEdit:1,	columLines:true, viewConfig: {forceFit: true}, hidden:true,
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
		columns: [
			{
				header:'N@E',	tooltip:'N@E',	dataIndex:'NE',	sortable:true,	width:120,	align:'center'
			},{
				header:'PyME',	tooltip:'PyME',	dataIndex:'RZ',	sortable:true,	width:200,	align:'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'RFC',	tooltip:'RFC',	dataIndex:'RFC',	sortable:true,	width:150,	align:'center'
			},{
				xtype: 'checkcolumn',
				header:'Bloqueado',	tooltip:'Bloqueado',	dataIndex:'BLOQUEADO',	sortable:false,	width:90,	align:'center',hideable: false
			},{
				header:'Fecha/Hora-Bloqueo/Desbloqueo',	tooltip:'Fecha/Hora<br>Bloqueo/Desbloqueo',	dataIndex:'FECHA',	sortable:true,	width:150,	align:'center'
			},{
				header:'IF Bloqueo/Desbloqueo',	tooltip:'IF Bloqueo/Desbloqueo',	dataIndex:'LOG_USUARIO',	sortable:true,	width:200,	align:'left'
			},{
				header : 'Causa', tooltip: 'Causa',
				dataIndex : 'CAUSA', fixed:true,
				sortable : false,	width : 250,	hiddeable: false,
				editor:{
					xtype:	'textarea',
					id: 'txtArea',
					maxLength: 260,
					enableKeyEvents: true,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 260){
								field.setValue((field.getValue()).substring(0,259));
								Ext.Msg.alert('Observaciones','La causa del cambio de estatus no debe sobrepasar los 260 caracteres.');
								field.focus();
							}
						}
					}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			}
		],
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',	text: 'Confirmar',	id: 'btnConfirmar',	iconCls:'icoAceptar', disabled:true,
						handler: function(boton, evento) {
										var flag=false;
										var jsonData = consultaData.data.items;
										var chk = "N";
										var info = "";
										Ext.each(jsonData, function(item,index,arrItem){
											
											if (item.data.BLOQUEADO){
												if (	Ext.isEmpty(item.data.CAUSA)	){
													Ext.Msg.alert(boton.text,
														"Favor de escribir la causa de bloqueo",
														function() {
															var gridEl = Ext.getCmp('gridCaptura').getGridEl();
															var col = Ext.getCmp('gridCaptura').getColumnModel().findColumnIndex('CAUSA');
															Ext.getCmp('gridCaptura').startEditing(index, col);
														}
													);
													flag=true;
													return false;
												}else{
													chk = "S";
												}
											}
											if(item.data.BLOQUEADO==item.data.AUXBLOQUEADO){
												info += form.iClient+"|"+item.data.ICPYME+"|"+item.data.ICEPO+"|"+chk+"|"+item.data.CAUSA+"|"+""+"@";
											}else{
												info += form.iClient+"|"+item.data.ICPYME+"|"+item.data.ICEPO+"|"+chk+"|"+item.data.CAUSA+"|"+form.usuarioLogin+"@";
											}
											chk = "N";
										});
										if(flag){return;}
										Ext.Msg.confirm(boton.text, 
											"Esta seguro de querer enviar su informaci�n",
											function(botonConf) {
													
												if (botonConf == 'ok' || botonConf == 'yes') {
													
													NE.util.obtenerPKCS7(confirmaAcuse, form.textoF, info);	
																										
												}
											}
										);
									}
				},{
					xtype: 'tbspacer', width: 5
				},'-',{
					xtype: 'button',	text: 'Cancelar',	id: 'btnCancelar',	iconCls:'icoRechazar',	disabled:true,
					handler: function(boton, evento) {
								fp.getForm().reset();
								Ext.getCmp('_ic_pyme').setValue('');
								Ext.getCmp('_ic_pyme').store.removeAll();
								gridCaptura.hide();
								gridConsulta.hide();
					}
				}
			]
		}
	});

	var gridConsulta = new Ext.grid.GridPanel({
		id:'gridConsulta',store: consultaData,	columLines:true, viewConfig: {forceFit: true}, hidden:true,
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false, header:true,
		columns: [
			{
				header:'N@E',	tooltip:'N@E',	dataIndex:'NE',	sortable:true,	width:120,	align:'center'
			},{
				header:'PyME',	tooltip:'PyME',	dataIndex:'RZ',	sortable:true,	width:200,	align:'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'RFC',	tooltip:'RFC',	dataIndex:'RFC',	sortable:true,	width:150,	align:'center'
			},{
				header:'Bloqueado',	tooltip:'Bloqueado',	dataIndex:'BLOQUEADO',	sortable:false,	width:90,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value){
									value = 'Si';
								}else{
									value = 'No';
								}
								return value;
							}
			},{
				header:'Fecha/Hora  Bloqueo/Desbloqueo',	tooltip:'Fecha/Hora<br>Bloqueo/Desbloqueo',	dataIndex:'FECHA',	sortable:true,	width:150,	align:'center'
			},{
				header:'IF Bloqueo/Desbloqueo',	tooltip:'IF Bloqueo/Desbloqueo',	dataIndex:'LOG_USUARIO',	sortable:true,	width:200,	align:'left'
			},{
				header:'Causa',	tooltip:'Causa',	dataIndex:'CAUSA',	sortable:true,	width:200,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			}
		],
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacionCons',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',	text: 'Generar PDF',	id: 'btnGenerarPDF',
					handler: function(boton, evento) {
									var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
									boton.disable();
									boton.setIconClass('loading-indicator');
									Ext.Ajax.request({
										url : '24forma09ext.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
											informacion: 'ArchivoPDF',
											start: cmpBarraPaginacion.cursor,
											limit: cmpBarraPaginacion.pageSize
										}),
										callback: procesarSuccessFailureGenerarPDF
									});
								}
				},{
					xtype: 'button',	text: 'Bajar PDF',	id: 'btnBajarPDF'
				},{
					xtype: 'button',	text: 'Generar PDF Acuse',	id: 'btnPdfAcuse'
				},{
					xtype: 'button',	text: 'Bajar PDF',	id: 'btnBajarAcuse'
				},{
					xtype: 'button',	text: 'Regresar',	id: 'btnRegresar', hidden:true,
					handler: function(boton, evento) {location.reload();}
				}
			]
		}
	});

	var elementosForma = [
		{
			xtype: 'combo',
			id:	'_ic_epo',
			name: 'ic_epo',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,	height:300,
			store: catalogoEpoDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:	{fn:function(combo){
									if (	!Ext.isEmpty(combo.getValue())	) {
										gridCaptura.hide();
										gridConsulta.hide();
										var comboPyme = Ext.getCmp('_ic_pyme');
										comboPyme.setValue('');
										comboPyme.store.removeAll();
										comboPyme.store.reload({	params: {ic_epo: combo.getValue() }	});
									}
							}
				}
			}
		},{
			xtype: 'combo',
			id:	'_ic_pyme',
			name: 'ic_pyme',
			hiddenName : 'ic_pyme',
			fieldLabel: 'PYME',
			emptyText: 'Seleccionar . . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoPymeDistData,	tpl:NE.util.templateMensajeCargaCombo
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 550,
		style: ' margin:0 auto;',
		title:	'<div align="center">Bloquear al Distribuidor por EPO</div>',
		frame: true,
		labelWidth:135,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: true,
		tbar: {
			xtype: 'buttongroup',	style: ' margin:0 auto;',columns: 2,bodyStyle: 'padding: 5px',frame:false,
			items: [{
				text: 'Captura',	id:'btnCaptura',	iconCls: 'modificar',disabled:true,
				handler: function(boton, evento) {
								boton.disable();
								fp.getForm().reset();
								Ext.getCmp('_ic_pyme').setValue('');
								Ext.getCmp('_ic_pyme').store.removeAll();
								form.actionBoton = "Capt";
								Ext.getCmp('btnConsulta').enable();
								gridCaptura.hide();
								gridConsulta.hide();
							}
			},{
				text: 'Consuta',	id:'btnConsulta',	iconCls: 'icoBuscar',
				handler: function(boton, evento) {
								boton.disable();
								fp.getForm().reset();
								Ext.getCmp('_ic_pyme').setValue('');
								Ext.getCmp('_ic_pyme').store.removeAll();
								form.actionBoton = "Cons";
								Ext.getCmp('btnCaptura').enable();
								gridCaptura.hide();
								gridConsulta.hide();
							}
			}]
		},
		buttons: [
			{
				text: 'Buscar',	id: 'btnBuscar',	iconCls: 'icoBuscar',	formBind: true,
				handler: function(boton, evento) {
								gridCaptura.hide();
								gridConsulta.hide();
								form.ic_epo = Ext.getCmp('_ic_epo').getValue();
								form.ic_pyme = Ext.getCmp('_ic_pyme').getValue();
								pnl.el.mask('Enviando...', 'x-mask-loading');
								consultaData.load({	params: Ext.apply({	operacion: 'Generar',	start: 0,	limit: 15	})	});
				}
			},{
				text:'Cancelar', iconCls:'icoRechazar',
				handler: function(boton, evento) {
								fp.getForm().reset();
								Ext.getCmp('_ic_pyme').setValue('');
								Ext.getCmp('_ic_pyme').store.removeAll();
								gridCaptura.hide();
								gridConsulta.hide();
							}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	fpAcuse,	NE.util.getEspaciador(10),	gridCaptura,gridConsulta,	NE.util.getEspaciador(10)
		]
	});

	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24forma09ext.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});
	catalogoEpoDistData.load();

});