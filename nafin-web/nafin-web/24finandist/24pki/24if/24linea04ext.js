Ext.onReady(function() { 

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });

	var auxLink;
	var limpiar=false;
	var mensajeVal="";
	var formularios=new Array();
	var modifica=new Array();
	var minVence;
	var form;
	var tSolicI=0;
	var strPerfil;

	function makeTimeoutFunc(param) {
		return function() {
			if (strPerfil == "IF FACT RECURSO"){
				Ext.Msg.alert("Mensaje de p�gina web","Ahora que ha capturado las L�neas de Cr�dito, <br>debe capturar las Tasas por PYME correspondientes<br>Presione el boton Salir para continuar.");
			}else{
				if (tSolicI == 1){
					Ext.Msg.alert("Mensaje de p�gina web","Ahora que ha capturado la L�nea de Cr�dito Inicial, \ndebe capturar las Tasas por PYME correspondientes\nPresione el boton Salir para continuar.");
				}else if(tSolicI > 1){
					Ext.Msg.alert("Mensaje de p�gina web","Ahora que ha capturado las L�neas de Cr�dito Iniciales, <br>debe capturar las Tasas por PYME correspondientes<br>Presione el boton Salir para continuar.");
				}
			}
		}
	}

	function timeMsg(){
		setTimeout(makeTimeoutFunc(), 3000);
	}

	function Limpiar() {
		Ext.getCmp('ic_epo').reset();
		Ext.getCmp('_ic_pyme').reset();
		Ext.getCmp('_tipoSol').reset();
		Ext.getCmp('_ic_moneda').reset();
		Ext.getCmp('mtoAuto').reset();
		Ext.getCmp('vencimiento').reset();
		if (Ext.getCmp('nCtaDist').readOnly == false){
			Ext.getCmp('nCtaDist').reset();
		}
		if (Ext.getCmp('nCtaIf').readOnly == false){
			Ext.getCmp('nCtaIf').reset();
		}else{
			//Ext.getCmp('btnHelp').hide();
		}
		if (Ext.getCmp('_ifBanco').readOnly == false){
			Ext.getCmp('_ifBanco').reset();
		}
		Ext.getCmp('_tipoInt').reset();
		Ext.getCmp('plazoMax').reset();
		Ext.getCmp('ctaBancaria').reset();
	}

	function Limpiar2() {
		Ext.getCmp('ic_epo').reset();
		Ext.getCmp('_ic_pyme').reset();
		Ext.getCmp('_tipoSol').reset();
		Ext.getCmp('_ic_moneda').reset();
		Ext.getCmp('mtoAuto').reset();		
		Ext.getCmp('vencimiento').reset();
		Ext.getCmp('vencimiento').setReadOnly(false);
		Ext.getCmp('nCtaDist').reset();
		Ext.getCmp('nCtaDist').setReadOnly(false);
		Ext.getCmp('nCtaDist').hide();
		//Ext.getCmp('nCtaDist').reset();
		Ext.getCmp('nCtaIf').reset();
		Ext.getCmp('nCtaIf').setReadOnly(false);
		Ext.getCmp('nCtaIf').hide();
		Ext.getCmp('_ifBanco').reset();
		Ext.getCmp('_ifBanco').setReadOnly(false);
		Ext.getCmp('_ifBanco').hide();
		//Ext.getCmp('btnHelp').hide();
		Ext.getCmp('layout').hide();
		Ext.getCmp('disDetalle').hide();
		Ext.getCmp('disMsgDetalle').hide();
		Ext.getCmp('disMsgDetalleNo').hide();
		Ext.getCmp('_tipoInt').hide();
		Ext.getCmp('_tipoInt').reset();
		Ext.getCmp('_folioSol').hide();
		Ext.getCmp('_folioSol').reset();
		Ext.getCmp('plazoMax').reset();
		Ext.getCmp('ctaBancaria').reset();
		Ext.getCmp('btnAgregar').hide();
		Ext.getCmp('btnLimpiar').hide();
	}

	function recargar(aux)	{
		if ( Ext.isEmpty(Ext.getCmp('ic_epo').getValue()) && aux==1)
			Limpiar();
		if ( Ext.isEmpty(Ext.getCmp('ic_epo').getValue()) && aux == 2){
			Ext.getCmp('_tipoSol').reset();
			Ext.getCmp('ic_epo').markInvalid("Seleccione primero una EPO");
			return;
		}
	}

	function verificaPanel(myPanel){
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype === 'panel')	{
				verificaPanel(panelItem);
			}else{
				if (panelItem.xtype != 'button' && panelItem.xtype != undefined)	{
					if (!panelItem.isValid() && panelItem.isVisible())	{
						valid = false;
						return false;
					}
				}
			}
		});
		return valid;
	}

	function validaFecha(objeto,minimo){
		if(!Ext.isEmpty(objeto.getValue())){
			if(!revisaFecha(objeto))
				return false;
			else if(minimo!=""){
				var comp = datecomp(minimo,objeto.value);
				if(comp==1||comp==0){
					Ext.Msg.alert("","La fecha de vencimiento debe ser mayor a "+minimo);
					objeto.selectText();
					return false;
				}
			}
			return true;	
		}else{
			return true;
		}	
	}


	function validaFechaAmRed(objeto){
	
		var hoy = new Date();
		var dia = hoy.getDate();
		var mes = hoy.getMonth() + 1;  
		if(navigator.appName =='Microsoft Internet Explorer'){
			var anio = hoy.getYear();
		}else{
			var anio = hoy.getYear() + 1900;
		}		
		var fechahoy = dia+"/"+mes+"/"+anio;
		
	
		if(!Ext.isEmpty(objeto.getValue())){
			if(!revisaFecha(objeto)) {
				return false;
			} else {
				var comp = datecomp(fechahoy,objeto.value);
				if(comp==1 || comp==0){
					Ext.Msg.alert("","La fecha de vencimiento debe ser mayor al dia actual ");
					objeto.selectText();
					Ext.getCmp('vencimiento').setValue(''); 
					return false;
				}
			}
			return true;	
		}else{
			return true;
		}	
	}
	

	function showHide(epo){
		Ext.getCmp('mtoAuto').reset();
		Ext.getCmp('vencimiento').reset();
		//Ext.getCmp('vencimiento').setReadOnly(false);
		Ext.getCmp('nCtaDist').reset();
		Ext.getCmp('nCtaDist').setReadOnly(false);
		Ext.getCmp('nCtaDist').hide();
		Ext.getCmp('nCtaIf').reset();
		Ext.getCmp('nCtaIf').setReadOnly(false);
		Ext.getCmp('nCtaIf').hide();
		Ext.getCmp('_ifBanco').reset();
		Ext.getCmp('_ifBanco').setReadOnly(false);
		Ext.getCmp('_ifBanco').hide();
		Ext.getCmp('layout').hide();
		//Ext.getCmp('btnHelp').hide();
		Ext.getCmp('disMsgDetalle').hide();
		Ext.getCmp('disMsgDetalleNo').hide();
		if (auxLink === "A") {
			if (auxLink === "A" && ( Ext.isEmpty(Ext.getCmp('_tipoSol').getValue()) ||  Ext.getCmp('_tipoSol').getValue() === "I") ) {
				Ext.getCmp('nCtaDist').show();
				Ext.getCmp('nCtaDist').label.update('EPO - Num. cuenta: ');
			}else{
				if(Ext.isEmpty(epo)){
					if ( !Ext.isEmpty(Ext.getCmp('_folioSol').getValue()) ) {
						fp.el.mask('Enviando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '24linea04ext.data.jsp',
							params: {
										ic_epo:Ext.getCmp('ic_epo').getValue(),
										solic:Ext.getCmp('_tipoSol').getValue(),
										peticion:"epo",
										informacion:'detalleCuenta',
										folio:Ext.getCmp('_folioSol').getValue(),
										ic_pyme:Ext.getCmp('_ic_pyme').getValue(),
										ic_moneda:Ext.getCmp('_ic_moneda').getValue()
										
							},
							callback: procesaDetalleCuenta
						});
					}else{
						Ext.getCmp('disMsgDetalle').show();
					}
				}
			}
		}else{
			if (	auxLink === "I" && (Ext.isEmpty(Ext.getCmp('_tipoSol').getValue()) ||  Ext.getCmp('_tipoSol').getValue() === "I")	){
				Ext.getCmp('nCtaIf').show();
				Ext.getCmp('_ifBanco').show();
				//Ext.getCmp('btnHelp').show();
			}else{
				if(Ext.isEmpty(epo)){
					if ( !Ext.isEmpty(Ext.getCmp('_folioSol').getValue()) &&  !Ext.isEmpty(Ext.getCmp('_ic_pyme').getValue())) {
						fp.el.mask('Enviando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '24linea04ext.data.jsp',
							params: {
										ic_epo:Ext.getCmp('ic_epo').getValue(),
										solic:Ext.getCmp('_tipoSol').getValue(),
										peticion:"if",
										informacion:'detalleCuenta',
										folio:Ext.getCmp('_folioSol').getValue(),
										ic_pyme:Ext.getCmp('_ic_pyme').getValue(),
										ic_moneda:Ext.getCmp('_ic_moneda').getValue()
							},
							callback: procesaDetalleCuenta
						});
					}else{
						Ext.getCmp('disMsgDetalle').show();
					}
				}
			}
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdf');
			toolbar.setWidth(toolbar.getWidth()+90);
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			toolbar.setWidth(toolbar.getWidth()+90);
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarConfirmar(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			fp.hide();
			Ext.getCmp('barraGrid').hide();
			var cm = grid.getColumnModel();
			cm.setHidden(cm.findColumnIndex(''), true);
			fpCoins.show();
			Ext.getCmp('disAcuse').body.update(infoR._acuse);
			Ext.getCmp('disFechaCarga').body.update(infoR.fechaCarga);
			Ext.getCmp('disHoraCarga').body.update(infoR.horaCarga);
			Ext.getCmp('disUsuario').body.update('<div class="formas" >'+infoR.usuario+'<div>');
			Ext.getCmp('btnGenerarArchivo').setHandler( function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url : '24linea04csv_ext.jsp',
					params: Ext.apply(opts.params,{
									_acuse: infoR._acuse,
									fechaHoy:infoR.fechaCarga,
									horaActual:infoR.horaCarga,
									usuario:infoR.usuario
								}),
					callback: procesarSuccessFailureGenerarArchivo
				});
			});
			Ext.getCmp('btnGenerarPDF').setHandler( function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url : '24linea04pdf_ext.jsp',
					params: Ext.apply(opts.params,{
									_acuse: infoR._acuse,
									fechaHoy:infoR.fechaCarga,
									horaActual:infoR.horaCarga,
									usuario:infoR.usuario
								}),
					callback: procesarSuccessFailureGenerarPDF
				});
			});

			fpAcuse.show();
			toolbar.show();
			timeMsg();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaCapturaLinea(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var newReg = Ext.data.Record.create(['NONAFINDIST','NOMBRE_PYME','IC_PYME','EPO','CLAVE_EPO','TIPO_SOL','IC_SOL','FOLIO_SOL','MONEDA','MONTO_AUTO','TIPO_COBRO','CLAVE_COBINT'
															,'FECHA_VTO','CUENTA_IF','CUENTA_DIST','IC_MONEDA','IFBANCO','CLAVE_BANCO','PLAZO_MAX','CTA_BANCARIA']);
			consultaData.add(new newReg({	'NONAFINDIST':infoR.noNafinDist,
													'NOMBRE_PYME':infoR.nomPyme,
													'IC_PYME':form.ic_pyme,
													'EPO':infoR.ic_epo,
													'CLAVE_EPO':form.epoDM,
													'TIPO_SOL':infoR.solic,
													'IC_SOL':form.tipoSol,
													'FOLIO_SOL':form.folioSol,
													'MONEDA':infoR.nomMoneda,
													'MONTO_AUTO':form.mtoAuto,
													'TIPO_COBRO':infoR.cobint,
													'CLAVE_COBINT':form.tipoInt,
													'FECHA_VTO':form.vencimiento,
													'CUENTA_IF':form.nCtaIf,
													'CUENTA_DIST':form.nCtaDist,
													'IC_MONEDA':form.ic_moneda,
													'CLAVE_BANCO':form.ifBanco,
													'IFBANCO':infoR.ifBanco,
													'PLAZO_MAX':Ext.getCmp('plazoMax').getValue(),
													'CTA_BANCARIA':Ext.getCmp('ctaBancaria').getValue()
												}) );
			var sumMontoMN=0;
			var sumMontoDL=0;
			var countMN = 0;
			var countDL = 0;
			consultaData.each(function(registro){
				if (registro.get('IC_MONEDA') === "1"){
					countMN++;
					sumMontoMN += parseFloat(registro.get('MONTO_AUTO'));
				}else if(registro.get('IC_MONEDA') === "54"){
					countDL++;
					sumMontoDL += parseFloat(registro.get('MONTO_AUTO'));
				}
			});
			if (countMN > 0){
				var regMN = totalesData.getAt(0);
				regMN.set('NOMMONEDA','Moneda Nacional');
				regMN.set('TOTAL_REGISTROS',countMN);
				regMN.set('TOTAL_MONTO_AUTORIZADO',sumMontoMN);

				if(countDL > 0){
					var regDL = totalesData.getAt(1);
					regDL.set('NOMMONEDA','Total Dolares');
					regDL.set('TOTAL_REGISTROS',countDL);
					regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
				}
			}else if(countDL > 0){
				var regDL = totalesData.getAt(0);
				regDL.set('NOMMONEDA','Total Dolares');
				regDL.set('TOTAL_REGISTROS',countDL);
				regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
			}
			Ext.getCmp('totMn').body.update('<div align="left">'+countMN+'</div>');
			Ext.getCmp('montoMn').body.update('<div align="left">'+Ext.util.Format.number(sumMontoMN, '$ 0,0.00')+'</div>');
			Ext.getCmp('totDl').body.update('<div align="left">'+countDL+'</div>');
			Ext.getCmp('montoDl').body.update('<div align="left">'+Ext.util.Format.number(sumMontoDL, '$ 0,0.00')+'</div>');
			grid.show();
			Ext.getCmp('barraGrid').show();
			var totalesCmp = Ext.getCmp('gridTotales');
			if (!totalesCmp.isVisible()) {
				totalesCmp.show();
			}
			formularios[formularios.length]=fp.getForm().getValues();

			var jsonData = consultaData.data.items;
			var noNafinEs_aux	=[];
			var eposDM_aux		=[];
			var ic_epos_aux	=[];
			var ic_pymes_aux	=[];
			var nomPymes		=[];
			var solics_aux		=[];
			var tipoSol_aux	=[];
			var folios_aux		=[];
			var monedas_aux	=[];
			var nomMoneda		=[];
			var montoAutos_aux=[];
			var plazs_aux		=[];
			var cobints_aux	=[];
			var tipoCobro_aux	=[];
			var vencimientos_aux=[];
			var ifBancos		=[];
			var ifBancos_aux	=[];
			var nCtaIfs_aux	=[];
			var nCtaDist_aux	=[];
			var nPlazoMax		=[];
			var nCtaBancaria	=[];
			Ext.each(jsonData, function(item,index,arrItem){
				noNafinEs_aux.push(item.data.NONAFINDIST);
				eposDM_aux.push(item.data.CLAVE_EPO);
				ic_epos_aux.push(item.data.EPO);
				ic_pymes_aux.push(item.data.IC_PYME);
				nomPymes.push(item.data.NOMBRE_PYME);
				solics_aux.push(item.data.IC_SOL);
				tipoSol_aux.push(item.data.TIPO_SOL);
				folios_aux.push(item.data.FOLIO_SOL);
				monedas_aux.push(item.data.IC_MONEDA);
				nomMoneda.push(item.data.MONEDA);
				montoAutos_aux.push(item.data.MONTO_AUTO);
				cobints_aux.push(item.data.CLAVE_COBINT);
				tipoCobro_aux.push(item.data.TIPO_COBRO);
				vencimientos_aux.push(item.data.FECHA_VTO);
				ifBancos.push(item.data.IFBANCO);
				ifBancos_aux.push(item.data.CLAVE_BANCO);
				nCtaIfs_aux.push(item.data.CUENTA_IF);
				nCtaDist_aux.push(item.data.CUENTA_DIST);
				nPlazoMax.push(item.data.PLAZO_MAX);
				nCtaBancaria.push(item.data.CTA_BANCARIA);
				if( item.data.IC_SOL == "I" ){
					tSolicI++;
				}
			});

			if (tSolicI >= 1 || strPerfil == "IF FACT RECURSO"){
				Ext.getCmp('btnSalir').setHandler( function(boton, evento) {
					boton.setIconClass('loading-indicator');
					window.location = '/nafin/24finandist/24if/24forma05ext.jsp';
				});
			}else{
				Ext.getCmp('btnSalir').setHandler( function(boton, evento) {
					boton.setIconClass('loading-indicator');
					window.location = '24linea02ext.jsp';
				});
			}

			Ext.getCmp('btnConfirmar').setHandler( function(boton, evento) {
					pnl.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24linea04ext.data.jsp',
						params:	{	informacion: "Confirma",
										totDocs:			countMN,
										totMtoAuto:		sumMontoMN,
										totDocsDol:		countDL,
										totMtoAutoDol:	sumMontoDL,
										noNafinEs_aux:	noNafinEs_aux,
										eposDM_aux:		eposDM_aux,
										ic_epos_aux:	ic_epos_aux,
										ic_pymes_aux:	ic_pymes_aux,
										nomPymes:		nomPymes,
										solics_aux:		solics_aux,
										tipoSol_aux:	tipoSol_aux,
										folios_aux:		folios_aux,
										monedas_aux:	monedas_aux,
										nomMoneda:		nomMoneda,
										montoAutos_aux:montoAutos_aux,
										cobints_aux:	cobints_aux,
										tipoCobro_aux:	tipoCobro_aux,
										vencimientos_aux:vencimientos_aux,
										nCtaDist_aux:	nCtaDist_aux,
										ifBancos:		ifBancos,
										ifBancos_aux:	ifBancos_aux,
										nCtaIfs_aux:	nCtaIfs_aux,
										nPlazoMax:		nPlazoMax,
										nCtaBancaria:	nCtaBancaria
									},
						callback: procesarConfirmar
					});
			});

			Limpiar();
			fp.el.unmask();
			return;
		} else {
			NE.util.mostrarConnError(response,opts);
			fp.el.unmask();
		}
	}

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			strPerfil = infoR.strPerfil;
			var cm = grid.getColumnModel();
			if(infoR.strPerfil == "IF FACT RECURSO"){
				Ext.getCmp('_ic_pyme').label.update('Proveedor:');
				Ext.getCmp('_ic_pyme').emptyText = "Seleccione proveedor. . ."
				Ext.getCmp('_ic_pyme').reset();
				Ext.getCmp('_tipoSol').setVisible(false);
				Ext.getCmp('plazoMax').setVisible(true);
				Ext.getCmp('ctaBancaria').setVisible(true);
				cm.setHidden(cm.findColumnIndex('PLAZO_MAX'),	false);
				cm.setHidden(cm.findColumnIndex('CTA_BANCARIA'),false);
				cm.setColumnHeader(cm.findColumnIndex('NONAFINDIST'),"N�mero Proveedor");
				cm.setColumnTooltip(cm.findColumnIndex('NONAFINDIST'),"N�mero Proveedor");
				cm.setColumnHeader(cm.findColumnIndex('NOMBRE_PYME'),"Nombre Proveedor");
				cm.setColumnTooltip(cm.findColumnIndex('NOMBRE_PYME'),"Nombre Proveedor");
			}else{
				auxLink = infoR.auxLink;
				cm.setHidden(cm.findColumnIndex('TIPO_SOL'),		false);
				cm.setHidden(cm.findColumnIndex('FOLIO_SOL'),	false);
				cm.setHidden(cm.findColumnIndex('CUENTA_DIST'),	false);
				cm.setHidden(cm.findColumnIndex('TIPO_COBRO'),	false);
				cm.setHidden(cm.findColumnIndex('IFBANCO'),		false);
				cm.setHidden(cm.findColumnIndex('CUENTA_IF'),	false);
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaDetalleCuenta(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.auxCad != undefined){
				if (auxLink === "A" && !(Ext.getCmp('_tipoSol').getValue() === "I")) {
					Ext.getCmp('nCtaDist').label.update('Distribuidor - Num. cuenta: ');
					Ext.getCmp('nCtaDist').setValue(infoR.auxCad);
					Ext.getCmp('nCtaDist').setReadOnly(true);
					Ext.getCmp('nCtaDist').show();
				}
				if (auxLink === "I" && !(Ext.getCmp('_tipoSol').getValue() === "I")) {
					Ext.getCmp('nCtaIf').setValue(infoR.auxCad);
					Ext.getCmp('nCtaIf').setReadOnly(true);
					Ext.getCmp('nCtaIf').show();
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
						var cuenta =Ext.getCmp('nCtaIf');
						var comboT = Ext.getCmp('_tipoSol');
						if( cuenta.isVisible()){
							if(	comboT.getValue() == "I" || comboT.getValue() == "A" ){//FODEA-039-2014 (MOdalidad 2?)
								Ext.getCmp('leyenda').show();
							}
						}else {
							Ext.getCmp('leyenda').hide();
						}
					//Ext.getCmp('btnHelp').show();
//------------------------------------------------------------------------------					
//------------------------------------------------------------------------------					
					Ext.getCmp('_ifBanco').setReadOnly(true);
					Ext.getCmp('_ifBanco').show();
					var dato = catalogoBanco.findExact("clave", infoR.auxCad2);
					if (dato != -1 ) {
						var reg = catalogoBanco.getAt(dato);
						value = reg.get('descripcion');
					}
					Ext.getCmp('_ifBanco').setValue(infoR.auxCad2);
				}
			}else{
				Ext.getCmp('disMsgDetalleNo').show();
			}
			if(Ext.getCmp('_tipoSol').getValue() != "I"){
				Ext.getCmp('mtoAuto').setMinValue(parseFloat(infoR.minimo)+1);
				Ext.getCmp('mtoAuto').minText = "El nuevo monto no puede ser menor a $ "+roundOff(infoR.minimo,2);
				if (infoR.auxLink != undefined){
					Ext.getCmp('mtoAuto').setValue(infoR.auxLink);
				}
			}
			if (opts.params.solic === "R"){
				Ext.getCmp('vencimiento').setReadOnly(false);
				minVence = infoR.minVence;
			}else if (opts.params.solic === "A"){
				//Ext.getCmp('vencimiento').setReadOnly(true);
				Ext.getCmp('vencimiento').setValue(infoR.vencimiento);
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaValidaLinea(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.limpiar){
				Ext.Msg.alert('',infoR.mensajeVal);
				Limpiar();
				return;
			}else{
				fp.el.mask('Enviando...', 'x-mask-loading');
				Ext.Ajax.request({
					url: '24linea04ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{informacion: "capturaLinea"}),
					callback: procesaCapturaLinea
				});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesaModificar = function(grid, rowIndex, colIndex, item, event) {
		Ext.getCmp('barraGrid').hide();
		consultaData.removeAt(rowIndex);
		var modifica = formularios[rowIndex];
		formularios.remove(formularios[rowIndex]);
		Ext.getCmp('ic_epo').setValue(modifica.epoDM);
		Ext.getCmp('_ic_pyme').setValue(modifica.ic_pyme);
		Ext.getCmp('_tipoSol').setValue(modifica.tipoSol);

		if(Ext.getCmp('_tipoSol').getValue() === "I"){
			Ext.getCmp('vencimiento').setValue(modifica.vencimiento);
			Ext.getCmp('_folioSol').setVisible(false);
			Ext.getCmp('_folioSol').setValue("");
			Ext.getCmp('nCtaIf').setValue(modifica.nCtaIf);
			Ext.getCmp('nCtaDist').setValue(modifica.nCtaDist);
		}else{
			Ext.getCmp('_folioSol').setValue(modifica.folioSol);
			Ext.getCmp('_folioSol').setVisible(true);
		}
		Ext.getCmp('_ic_moneda').setValue(modifica.ic_moneda);
		Ext.getCmp('_tipoInt').setValue(modifica.tipoInt);
		Ext.getCmp('plazoMax').setValue(modifica.plazoMax);
		Ext.getCmp('ctaBancaria').setValue(modifica.ctaBancaria);

		if (auxLink === "A") {
			if (auxLink === "A" && ( Ext.isEmpty(Ext.getCmp('_tipoSol').getValue()) ||  Ext.getCmp('_tipoSol').getValue() === "I") ) {
				Ext.getCmp('nCtaDist').label.update('EPO - Num. cuenta: ');
			}else{
				if ( !Ext.isEmpty(Ext.getCmp('_folioSol').getValue()) ) {
					fp.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24linea04ext.data.jsp',
						params: {
									ic_epo:Ext.getCmp('ic_epo').getValue(),
									solic:Ext.getCmp('_tipoSol').getValue(),
									peticion:"epo",
									informacion:'detalleCuenta',
									folio:Ext.getCmp('_folioSol').getValue(),
									ic_pyme:Ext.getCmp('_ic_pyme').getValue(),
									ic_moneda:Ext.getCmp('_ic_moneda').getValue()
						},
						callback: procesaDetalleCuenta
					});
				}else{
					Ext.getCmp('disMsgDetalle').show();
				}
			}
		}else{
			if (	auxLink === "I" && (Ext.isEmpty(Ext.getCmp('_tipoSol').getValue()) ||  Ext.getCmp('_tipoSol').getValue() === "I")	){
				Ext.getCmp('nCtaIf').show();
				Ext.getCmp('_ifBanco').show();
				//Ext.getCmp('btnHelp').show();
			}else{
				if ( !Ext.isEmpty(Ext.getCmp('_folioSol').getValue()) && !Ext.isEmpty(Ext.getCmp('_ic_pyme').getValue())) {
					fp.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '24linea04ext.data.jsp',
						params: {
									ic_epo:Ext.getCmp('ic_epo').getValue(),
									solic:Ext.getCmp('_tipoSol').getValue(),
									peticion:"if",
									informacion:'detalleCuenta',
									folio:Ext.getCmp('_folioSol').getValue(),
									ic_pyme:Ext.getCmp('_ic_pyme').getValue(),
									ic_moneda:Ext.getCmp('_ic_moneda').getValue()
						},
						callback: procesaDetalleCuenta
					});
				}else{
					if(strPerfil != "IF FACT RECURSO"){
						Ext.getCmp('disMsgDetalle').show();
					}
				}
			}
		}

		if (consultaData.data.length <= 0){
			if (grid.isVisible()){
				grid.hide();
				formularios=new Array();
			}
			if (Ext.getCmp('gridTotales').isVisible()){
				Ext.getCmp('gridTotales').hide();
			}
		}else{
			var sumMontoMN=0;
			var sumMontoDL=0;
			var countMN = 0;
			var countDL = 0;
			consultaData.each(function(registro){
				if (registro.get('IC_MONEDA') === "1"){
					countMN++;
					sumMontoMN += parseFloat(registro.get('MONTO_AUTO'));
				}else if(registro.get('IC_MONEDA') === "54"){
					countDL++;
					sumMontoDL += parseFloat(registro.get('MONTO_AUTO'));
				}
			});
			if (countMN > 0){
				var regMN = totalesData.getAt(0);
				regMN.set('NOMMONEDA','Moneda Nacional');
				regMN.set('TOTAL_REGISTROS',countMN);
				regMN.set('TOTAL_MONTO_AUTORIZADO',sumMontoMN);

				if(countDL > 0){
					var regDL = totalesData.getAt(1);
					regDL.set('NOMMONEDA','Total Dolares');
					regDL.set('TOTAL_REGISTROS',countDL);
					regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
				}
			}else if(countDL > 0){
				var regDL = totalesData.getAt(0);
				regDL.set('NOMMONEDA','Total Dolares');
				regDL.set('TOTAL_REGISTROS',countDL);
				regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
			}
			Ext.getCmp('totMn').body.update('<div align="left">'+countMN+'</div>');
			Ext.getCmp('montoMn').body.update('<div align="left">'+Ext.util.Format.number(sumMontoMN, '$ 0,0.00')+'</div>');
			Ext.getCmp('totDl').body.update('<div align="left">'+countDL+'</div>');
			Ext.getCmp('montoDl').body.update('<div align="left">'+Ext.util.Format.number(sumMontoDL, '$ 0,0.00')+'</div>');
			grid.show();
			var totalesCmp = Ext.getCmp('gridTotales');
			if (!totalesCmp.isVisible()) {
				totalesCmp.show();
			}
		}
	}

	var procesaEliminar = function(grid, rowIndex, colIndex, item, event) {
		consultaData.removeAt(rowIndex);
		formularios.remove(formularios[rowIndex]);
		if (consultaData.data.length <= 0){
			if (grid.isVisible()){
				grid.hide();
				formularios=new Array();
			}
			if (Ext.getCmp('gridTotales').isVisible()){
				Ext.getCmp('gridTotales').hide();
			}
		}else{
			var sumMontoMN=0;
			var sumMontoDL=0;
			var countMN = 0;
			var countDL = 0;
			consultaData.each(function(registro){
				if (registro.get('IC_MONEDA') === "1"){
					countMN++;
					sumMontoMN += parseFloat(registro.get('MONTO_AUTO'));
				}else if(registro.get('IC_MONEDA') === "54"){
					countDL++;
					sumMontoDL += parseFloat(registro.get('MONTO_AUTO'));
				}
			});
			if (countMN > 0){
				var regMN = totalesData.getAt(0);
				regMN.set('NOMMONEDA','Moneda Nacional');
				regMN.set('TOTAL_REGISTROS',countMN);
				regMN.set('TOTAL_MONTO_AUTORIZADO',sumMontoMN);

				if(countDL > 0){
					var regDL = totalesData.getAt(1);
					regDL.set('NOMMONEDA','Total Dolares');
					regDL.set('TOTAL_REGISTROS',countDL);
					regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
				}
			}else if(countDL > 0){
				var regDL = totalesData.getAt(0);
				regDL.set('NOMMONEDA','Total Dolares');
				regDL.set('TOTAL_REGISTROS',countDL);
				regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
			}
			Ext.getCmp('totMn').body.update('<div align="left">'+countMN+'</div>');
			Ext.getCmp('montoMn').body.update('<div align="left">'+Ext.util.Format.number(sumMontoMN, '$ 0,0.00')+'</div>');
			Ext.getCmp('totDl').body.update('<div align="left">'+countDL+'</div>');
			Ext.getCmp('montoDl').body.update('<div align="left">'+Ext.util.Format.number(sumMontoDL, '$ 0,0.00')+'</div>');
			grid.show();
			var totalesCmp = Ext.getCmp('gridTotales');
			if (!totalesCmp.isVisible()) {
				totalesCmp.show();
			}
		}
		Limpiar2();
	}

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDistribuidor'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var tipoSolData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['I','INICIAL'],
			['A','AMPLIACI�N/REDUCCI�N'],
			['R','RENOVACI�N']
		 ]
	});

	var procesarTipoFolio = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('_folioSol').valueNotFoundText = '';
		}else{
			Ext.getCmp('_folioSol').valueNotFoundText = 'No existen folios relacionados';
		}
		Ext.getCmp('_folioSol').reset();
	}

	var catalogoFolioSolData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea04ext.data.jsp',
		baseParams: {	informacion: 'TipoFolio'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load: procesarTipoFolio,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoMonedaDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoTipoCobroData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTipoCobroDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoBanco = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea04ext.data.jsp',
		baseParams: {	informacion: 'CatalogoBanco'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var totalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO_AUTORIZADO'}
		],
		data:[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_AUTORIZADO':''},
				{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_AUTORIZADO':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name:'NONAFINDIST'},
			{name:'NOMBRE_PYME'},
			{name:'CLAVE_EPO'},
			{name:'IC_PYME'},
			{name:'EPO'},
			{name:'TIPO_SOL'},
			{name:'IC_SOL'},
			{name:'FOLIO_SOL'},
			{name:'IC_MONEDA'},
			{name:'MONEDA'},
			{name:'MONTO_AUTO'},
			{name:'TIPO_COBRO'},
			{name:'CLAVE_COBINT'},
			{name:'FECHA_VTO'},
			{name:'CUENTA_IF'},
			{name:'CUENTA_DIST'},
			{name:'IFBANCO'},
			{name:'CLAVE_BANCO'},
			{name:'PLAZO_MAX'},
			{name:'CTA_BANCARIA'}
		],
		totalProperty : 'total',
		data:	[{	'NONAFINDIST':'','NOMBRE_PYME':'','IC_PYME':'','CLAVE_EPO':'','EPO':'','TIPO_SOL':'','IC_SOL':'','FOLIO_SOL':'','IC_MONEDA':'','MONEDA':'','MONTO_AUTO':'',
					'TIPO_COBRO':'','CLAVE_COBINT':'','FECHA_VTO':'','CUENTA_IF':'','CUENTA_DIST':'','IFBANCO':'','CLAVE_BANCO':'','PLAZO_MAX':'','CTA_BANCARIA':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridTotales = {
		xtype:'grid',	store:totalesData,	id:'gridTotales',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:940,	height:70,	hidden:true,frame: false,
		columns: [
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 400,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),menuDisabled:true
			},{
				header: 'Monto Autorizado',	dataIndex: 'TOTAL_MONTO_AUTORIZADO',	width:400,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		]
	};

	var grid = new Ext.grid.GridPanel({
		id:'grid',store: consultaData,	columnLines:true, hidden:true,
		stripeRows:true,	loadMask:true,	width:938,	height:250,	style:'margin:0 auto;',	frame:false,
		columns: [
			{
				header:'EPO',	tooltip:'EPO',	dataIndex:'EPO',	sortable:false,	width:150,	align:'center',	hideable:false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'N�mero Distribuidor',	tooltip:'N�mero Distribuidor',	dataIndex:'NONAFINDIST',	sortable:true,	width:140,	align:'center'
			},{
				header:'Nombre Distribuidor',	tooltip:'Nombre Distribuidor',	dataIndex:'NOMBRE_PYME',	sortable:true,	width:140,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Tipo de solicitud',	tooltip:'Tipo de solicitud',	dataIndex:'TIPO_SOL',	sortable:false,	width:100,	align:'center', hidden:true
			},{
				header:'Folio solicitud relacionada',	tooltip:'Folio solicitud relacionada',	dataIndex:'FOLIO_SOL',	sortable:false,	width:140,	align:'center', hidden:true
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:false,	width:150,	align:'center'
			},{
				header:'Monto autorizado',	tooltip:'Monto autorizado',	dataIndex:'MONTO_AUTO',	sortable:false,	width:120,	align:'right',renderer: Ext.util.Format.numberRenderer('$ 0,000.00')
			},{
				header:'Fecha de vencimiento',	tooltip:'Fecha de vencimiento',	dataIndex:'FECHA_VTO',	sortable:false,	width:120,	align:'center'
			},{
				header:'Tipo de cobro de intereses',	tooltip:'Tipo de cobro de intereses',	dataIndex:'TIPO_COBRO',	sortable:false,	width:150,	align:'center', hidden:true
			},{
				header:'No. Cuenta Distribuidor',	tooltip:'No. Cuenta Distribuidor',	dataIndex:'CUENTA_DIST',	sortable:false,	width:110,	align:'center', hidden:true
			},{
				header:'IF Banco de Servicio',	tooltip:'IF Banco de Servicio',	dataIndex:'IFBANCO',	sortable:false,	width:110,	align:'center', hidden:true,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'No. Cuenta IF',	tooltip:'No. Cuenta IF',	dataIndex:'CUENTA_IF',	sortable:false,	width:110,	align:'center', hidden:true
			},{
				header:'Plazo m�x. Ampliaci�n descuento',	tooltip:'Plazo m�x. Ampliaci�n descuento',	dataIndex:'PLAZO_MAX',	sortable:false,	width:110,	align:'center', hidden:true
			},{
				header:'Cuenta Bancaria',	tooltip:'Cuenta Bancaria',	dataIndex:'CTA_BANCARIA',	sortable:false,	width:110,	align:'center', hidden:true
			},{
					xtype:	'actioncolumn',
					header : 'Seleccionar', tooltip: 'Seleccionar',
					dataIndex : '',align: 'center', hideable: false,
					sortable : true,	width : 100, hidden: false,
					items: [
						{
							getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
											this.items[0].tooltip = 'Modificar';
											return 'modificar';
							},
							handler:	procesaModificar
						},{
							getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
											this.items[1].tooltip = 'Eliminar';
											return 'borrar';
							},
							handler:	procesaEliminar
						}
					]
			}
		],
		bbar: {
			id:'barraGrid',
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',	text: 'Confirmar',	id: 'btnConfirmar',	iconCls:'autorizar'
				},{
					xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5
				},{
					xtype: 'button',	text: 'Cancelar',	id: 'btnCancelar',	iconCls:'icoRechazar',	handler: function(boton, evento) {Limpiar2();}
				}
			]
		}
	});

	var toolbar = new Ext.Toolbar({
		id:'barBotones',
		style: 'margin:0 auto;',
		autoScroll : true,
		hidden:false,
		width: 310,
		height: 'auto',
		 items: [
			'->','-',
			{
				text:'Generar Archivo',	id:'btnGenerarArchivo',	iconCls:'icoXls', scale:'medium'
			},{
				text:'Bajar Archivo',	id:'btnBajarArchivo',	hidden:true, scale:'medium'
			},'-',{
				xtype: 'tbspacer', width: 2
			},{
				text:'Generar Pdf',	id:'btnGenerarPDF',	iconCls:'icoPdf',scale:'medium'
			},{
				text:'Bajar Pdf',	id:'btnBajarPdf',	hidden:true, scale:'medium'
			},'-',{
				xtype: 'tbspacer', width: 2
			},{
				text:'Salir',id:'btnSalir',iconCls:'icoLimpiar',scale:'medium'
			},'-',{
				xtype: 'tbspacer', width: 2
			}
		]
	});

	var elementosCoins = [
		{
			xtype: 'panel',layout:'table',	width:820,	border:true,	layoutConfig:{ columns: 4 },
			defaults: {frame:false, border:true,width:205, height: 35,bodyStyle:'padding:8px'},
			items:[
				{	width:410,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Moneda Nacional</div>'	},
				{	width:410,	colspan:2,	border:false,	frame:true,	html:'<div align="center">D�lares</div>'	},
				{	border:false,	frame:true,	html:'<div class="formas" align="center">N�mero de solicitudes capturadas</div>'	},
				{	border:false,	frame:true,	html:'<div class="formas" align="center">Monto de solicitudes capturadas</div>'	},
				{	border:false,	frame:true,	html:'<div class="formas" align="center">N�mero de solicitudes capturadas</div>'	},
				{	border:false,	frame:true,	html:'<div class="formas" align="center">Monto de solicitudes capturadas</div>'	},
				{	id:'totMn',		html:'&nbsp;'	},
				{	id:'montoMn',	html:'&nbsp;'	},
				{	id:'totDl',		html:'&nbsp;'	},
				{	id:'montoDl',	html:'&nbsp;'	}//,
				//{	height: 25,	colspan:4,	width:820,	html:'<div class="formas" align="center">Leyenda legal</div>'	}
			]
		}
	];

	var fpCoins=new Ext.FormPanel({	style: 'margin:0 auto;',	height:'auto',	width:820,	border:true,	frame:false,	items:elementosCoins,	hidden:true 	});

	var elementosAcuse = [
		{
			xtype: 'panel',layout:'table',	width:699,	border:false,	layoutConfig:{ columns: 2 },	style: 'margin:0 auto;',
			defaults: {frame:false, border: true,width:170	, height: 35,	bodyStyle:'padding:6px'},
			items:[
				{	width:698,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Datos de cifras de control</div>'	},
				{	html:'<div class="formas" align="left">N�m. acuse</div>'	},
				{	width:535,id:'disAcuse',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Fecha</div>'	},
				{	width:535,id:'disFechaCarga',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Hora</div>'	},
				{	width:535,id:'disHoraCarga',	html:'&nbsp;'},
				{	html:'<div class="formas" align="left">Nombre y n�mero de usuario</div>'	},
				{	width:535,id:'disUsuario',html: '&nbsp;'	}
			]
		}
	];

	var fpAcuse=new Ext.FormPanel({	style: 'margin:0 auto;',	height:'auto',	width:700,	border:true,	frame:false,	items:elementosAcuse,	hidden:true 	});

	var elementosForma = [
		{
			xtype: 'combo',
			id:	'ic_epo',
			name: 'epoDM',
			hiddenName : 'epoDM',
			fieldLabel: 'EPO',
			emptyText: 'Seleccionar EPO. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			allowBlank: false,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEpoDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:function(combo){
							var comboPyme = Ext.getCmp('_ic_pyme');
							comboPyme.setValue('');
							comboPyme.store.removeAll();
							comboPyme.store.reload({	params: {ic_epo: combo.getValue() }	});
							comboPyme.reset();
							Ext.getCmp('_ic_moneda').reset();
							if(strPerfil != "IF FACT RECURSO"){
								var comboTipo = Ext.getCmp('_tipoInt');
								comboTipo.setValue('');
								comboTipo.store.removeAll();
								comboTipo.store.reload({	params: {epoDM: combo.getValue() }	});
								comboTipo.setVisible(true);
								comboTipo.reset();
								Ext.getCmp('_folioSol').setValue('');
								Ext.getCmp('disDetalle').show();
								Ext.getCmp('btnAgregar').show();
								Ext.getCmp('btnLimpiar').show();
								Ext.getCmp('_folioSol').hide();
								Ext.getCmp('_tipoSol').reset();
								Ext.getCmp('_ic_moneda').reset();
								showHide("epo");
							}else{
								Ext.getCmp('btnAgregar').show();
								Ext.getCmp('btnLimpiar').show();
							}
				},
				change:function(combo, ewVal, oldVal) {recargar(1);}
			}
		},{
			xtype: 'combo',
			id:	'_ic_pyme',
			name: 'ic_pyme',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Distribuidor',
			emptyText: 'Seleccionar distribuidor. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			allowBlank: false,
			typeAhead: true,
			minChars : 1,
			store: catalogoPymeDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:{fn:function(combo){
								if (!Ext.isEmpty(Ext.getCmp('ic_epo').getValue()) && !Ext.isEmpty(Ext.getCmp('_ic_moneda').getValue()) && !Ext.isEmpty(Ext.getCmp('_tipoSol').getValue()) ){
									if (Ext.getCmp('_tipoSol').getValue() != "I"){
										var cmbFol = Ext.getCmp('_folioSol');
										cmbFol.setValue('');
										cmbFol.store.removeAll();
										cmbFol.store.reload({	params: {epoDM: Ext.getCmp('ic_epo').getValue(),ic_pyme:combo.getValue(),
																					solic:Ext.getCmp('_tipoSol').getValue(), moneda:Ext.getCmp('_ic_moneda').getValue()}	});
										cmbFol.setVisible(true);
									}else{
										cmbFol.setVisible(false);
									}
								}
							}
						},
				change:function(combo, ewVal, oldVal) {recargar(1);}
			}
		},{
			xtype:'panel', layout:'table',	width:690,	border:false, fram:true,	layoutConfig:{ columns: 2 }, anchor:'100%',
			defaults: {width:313, autoHeight: true},
			items: [
				{
					xtype:'panel', layout:'form',	labelWidth:140,defaults: {	msgTarget: 'side',	anchor: '-20'	},width:345, 
					items:[
						{
							xtype: 'combo',
							id:	'_tipoSol',
							name: 'tipoSol',
							hiddenName : 'tipoSol',
							emptyText: 'Seleccionar. . .',
							fieldLabel: 'Tipo de solicitud',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							forceSelection : true,
							triggerAction : 'all',
							autoLoad: false,
							//allowBlank: false,
							//lazyRender:true,
							typeAhead: true,
							//minChars : 1,
							 width:240,
							msgTarget: 'side',
							margins: '0 20 0 0',	
							store: tipoSolData,
							listeners:{
								select:	{fn:function(combo){
												if (!Ext.isEmpty(combo.getValue()) && !Ext.isEmpty(Ext.getCmp('ic_epo').getValue()) && !Ext.isEmpty(Ext.getCmp('_ic_pyme').getValue()) ){
														var cmbFol = Ext.getCmp('_folioSol');
														if (	combo.getValue() != "I" && !Ext.isEmpty(Ext.getCmp('_ic_moneda').getValue())	) {
															cmbFol.setValue('');
															cmbFol.store.removeAll();
															cmbFol.store.reload({	params: {epoDM: Ext.getCmp('ic_epo').getValue(),ic_pyme:Ext.getCmp('_ic_pyme').getValue(),
																										solic:combo.getValue(), moneda:Ext.getCmp('_ic_moneda').getValue()}	});
															cmbFol.setVisible(true);
														}else{
															minVence="";
															Ext.getCmp('mtoAuto').setMinValue(0);
															cmbFol.setVisible(false);
														}
													showHide("");
												}

												var cuenta =Ext.getCmp('nCtaIf');
												if( cuenta.isVisible()){
													if(	combo.getValue() == "I" || combo.getValue() == "A" ){//FODEA-039-2014 (MOdalidad 2?)
														Ext.getCmp('leyenda').show();
													}
													
												}else {
													Ext.getCmp('leyenda').hide();
												}
											}
											
											
								},
								change:function(combo, ewVal, oldVal) {recargar(2);}
							}
						}
					]
				},{
					xtype:'panel', layout:'form',	defaults: {	msgTarget: 'side',	anchor: '-20'	},width:290, labelWidth:120,
					items:[
						{
							xtype: 'combo',
							id:'_folioSol',
							name: 'folioSol',
							hiddenName:'folioSol',
							fieldLabel:'Folio de solicitud relacionada',
							emptyText: 'Seleccione una solicitud relacionada',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							allowBlank: false,
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoFolioSolData,
							tpl : NE.util.templateMensajeCargaCombo,
							hidden:true,
							listeners:{
								change:function(combo, ewVal, oldVal) {recargar(2);},
								select:function(combo) {
											showHide("");
										},
								change:function(combo, ewVal, oldVal) {recargar(2);}
								
							}
						}
					]
				}
			]
		},{
			xtype: 'combo',
			id:	'_ic_moneda',
			name: 'ic_moneda',
			hiddenName : 'ic_moneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccionar . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			allowBlank: false,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo, anchor:'49%',
			listeners:{
				select:	{fn:function(combo){
								Ext.getCmp('mtoAuto').reset();
								if (!Ext.isEmpty(Ext.getCmp('ic_epo').getValue()) && !Ext.isEmpty(Ext.getCmp('_tipoSol').getValue()) ){
									var cmbFol = Ext.getCmp('_folioSol');
									if (	Ext.getCmp('_tipoSol').getValue() != "I" && !Ext.isEmpty(combo.getValue())	) {
										cmbFol.setValue('');
										cmbFol.store.removeAll();
										cmbFol.store.reload({	params: {epoDM: Ext.getCmp('ic_epo').getValue(),
																					moneda:combo.getValue(), solic:Ext.getCmp('_tipoSol').getValue(),
																					ic_pyme:Ext.getCmp('_ic_pyme').getValue()
																					}	});
										cmbFol.setVisible(true);
									}else{
										cmbFol.setVisible(false);
									}
								}
							}
				},
				change:function(combo, ewVal, oldVal) {recargar(2);}
			}
		},{
			xtype: 'numberfield',	name: 'mtoAuto',	id: 	'mtoAuto',	fieldLabel: 'Monto autorizado',	allowBlank:false,	maxLength: 10,	anchor:'38%', minValue:0, decimalPrecision:9
		},{
			xtype: 'datefield',
			fieldLabel: 'Fecha de vencimiento',	name: 'vencimiento',	id: 'vencimiento',	allowBlank: false,	startDay: 0,	anchor:'38%',
			listeners:{
				focus:function(dt){
							if (dt.readOnly == true){
								Ext.getCmp('_tipoInt').focus();
							}
				},
				change:function(dt) {
							if(Ext.getCmp('_tipoSol').getValue()==="R"){
								validaFecha(dt,minVence);
							}
							if(Ext.getCmp('_tipoSol').getValue()==="A"){
								validaFechaAmRed(dt); 
							}
							
				}
			}
		},{
			xtype: 'numberfield',	name:'plazoMax',	id:'plazoMax',	fieldLabel: 'Plazo m�ximo ampliaci�n descuento',	allowBlank:false,	maxLength: 3,	anchor:'38%', minValue:0, hidden:true
		},{
			xtype: 'textfield',	name:'ctaBancaria',	id:'ctaBancaria',	fieldLabel: 'Cuenta Bancaria',	allowBlank:false,	maxLength: 6,	anchor:'38%', minValue:0, hidden:true, maskRe:/[0-9]/
		},{
			xtype: 'combo',
			id:	'_tipoInt',
			name: 'tipoInt',
			hiddenName : 'tipoInt',
			fieldLabel: 'Tipo de cobro de inter�s',
			emptyText: 'Seleccione un tipo',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			allowBlank: false,
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1, anchor:'49%',
			store: catalogoTipoCobroData, hidden:true,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype:'displayfield', id:'disDetalle',value:'<div class="titulos" align="center">Detalle de cuentas</div>', hidden:true
		},{
			xtype:'displayfield', id:'disMsgDetalle',value:'<div class="formas" align="center">Seleccione el folio de solicitud relacionada para mostrar la informaci�n</div>', hidden:true
		},{
			xtype:'displayfield', id:'disMsgDetalleNo',value:'<div class="formas" align="center">No existen Detalles</div>', hidden:true
		},{
			xtype: 'textfield',
			name: 'nCtaDist',
			id: 	'nCtaDist',
			fieldLabel: 'EPO - Num. cuenta',
			vtype:'alphanum',
			allowBlank:false,	maxLength: 9,	anchor:'40%',	hidden:true
		},{
			xtype: 'combo',
			id:	'_ifBanco',
			name: 'ifBanco',
			hiddenName : 'ifBanco',
			fieldLabel: 'If - Banco servicio',
			emptyText: 'Seleccione un Banco',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1, width:500,
			store: catalogoBanco, hidden:true,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				focus:function(combo){
							if (combo.readOnly == true){
								Ext.getCmp('nCtaIf').focus();
							}
				}
			}
		},{
			xtype:'panel', layout:'table',	width:690,	layoutConfig:{ columns: 2 }, anchor:'100%',
			defaults: {autoHeight: true},
			items: [
				{
					xtype:'panel', layout:'form',bodyStyle: 'padding: 6px',defaults: {	msgTarget: 'side',	anchor: '-20'	}, width:250, 
					items:[
						{
							xtype: 'textfield',
							name: 'nCtaIf',
							id: 	'nCtaIf',
							fieldLabel: 'IF - Num. cuenta',
							vtype:'alphanum',
							allowBlank:true,	maxLength: 9,	hidden:true,
							listeners:{
								focus:function(field){
											if (field.readOnly == true){
												Ext.getCmp('btnAgregar').focus();
											}
								}
							}
						}
					]
				},
				{
					xtype :	'displayfield',
					id		:	'leyenda',
					value :	'El dato es obligatorio s�lo para la cadena PEMEX GAS y PETROQUIMICA BASICA',
					hidden:true,
					autoWidth: true
				},{},
				/*{
					xtype: 'button',	id:'btnHelp',	autoHeight: true,	iconCls: 'icoAyuda', autoWidth: true, hidden:true,
					handler: function() {
						Ext.getCmp('layout').show();
					}
				},*/{
					width:500
				},{
					xtype:'panel', layout:'table', style: ' margin:0 auto;',layoutConfig:{ columns: 2 }, id:'layout', hidden:true, colspan:3, width:690, labelWidth:150,
					items:[
						{
							html:'El dato es obligatorio s�lo para la cadena PEMEX GAS y PETROQUIMICA BASICA'
						},{
							xtype: 'button',	autoWidth: true,	autoHeight: true,	iconCls: 'icoRechazar',
							handler: function() {
								Ext.getCmp('layout').hide();
							}
						}
					]
				}
			]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title:'<div align="center">Campos de Captura</div>',
		width: 690,
		frame: true,
		collapsible: true,
		titleCollapse: true,
		labelWidth:140,
		labelAlign:'right',
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Agregar',	id: 'btnAgregar',	iconCls: 'aceptar',	hidden:true, formBind:true,
				handler: function(boton, evento) {
								if(!verificaPanel(Ext.getCmp('forma'))) {
									return;
								}
								if (strPerfil === "IF FACT RECURSO"){
									Ext.getCmp('_tipoSol').setValue("I");
									Ext.getCmp('_tipoInt').setValue("1");
								}
								form = fp.getForm().getValues();
								var folio = Ext.getCmp('_folioSol');
								if (folio.isVisible() && Ext.isEmpty(folio.getValue())){
									folio.markInvalid('El campo no puede estar vacio');
									return;
								}
								var dt = new Date();
								var res = datecomp(form.vencimiento,dt.format('d/m/Y'));
								if(res==2 || res == 0 || res == -1){
									Ext.getCmp('vencimiento').markInvalid("La fecha de vencimiento no es valida.");
									return;
								}
								
								var tipoInt = Ext.getCmp('_tipoInt');
								if (tipoInt.isVisible() && Ext.isEmpty(tipoInt.getValue())){
									tipoInt.markInvalid('El campo es obligatorio');
									return;
								}
								
								var ic_epo = form.epoDM;
								var ic_pyme = form.ic_pyme;
								var solic = form.tipoSol;
								var moneda = form.ic_moneda;
								var bandera=false;
								limpiar = false;
								mensajeVal="";
								
								if(formularios.length > 0) {
									for(var i=0;i<formularios.length;i++){
										if (ic_epo === formularios[i].epoDM && ic_pyme === formularios[i].ic_pyme && solic === formularios[i].tipoSol && moneda === formularios[i].ic_moneda){
											bandera=true;
											limpiar=true;
											mensajeVal = "Ya se capturo una linea inicial para esa PyME y moneda";
											break;
										}/*else{
											if (ic_epo === formularios[i].epoDM && solic === "R" && solic === formularios[i].tipoSol){
												bandera=true;
												limpiar=true;
												mensajeVal = "Ya se capturo una renovacion para esta EPO y Moneda";
												break;
											}
										}*/
									}
								}
								if ((solic === "I" || solic === "R") && !bandera){
										fp.el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: '24linea04ext.data.jsp',
											params: {
														moneda:moneda,
														ic_pyme:ic_pyme,
														epoDM:ic_epo,
														vencimiento:form.vencimiento,
														solic:solic,
														informacion:"validaLinea"
											},
											callback: procesaValidaLinea
										});
								}else{
									if(limpiar){
										Ext.Msg.alert("",mensajeVal);
										Limpiar();
										return;
									}
									fp.el.mask('Enviando...', 'x-mask-loading');
									Ext.Ajax.request({
										url: '24linea04ext.data.jsp',
										params: Ext.apply(form,{informacion: "capturaLinea"}),
										callback: procesaCapturaLinea
									});
								}
				} //fin handler
			},{
				text: 'Limpiar',	id:'btnLimpiar',	iconCls:'icoLimpiar',	hidden:true,
				handler: function() {
					Limpiar();
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,	fpCoins,	NE.util.getEspaciador(10),
			grid,	gridTotales,	NE.util.getEspaciador(10),	fpAcuse,
			NE.util.getEspaciador(10),	toolbar
		]
	});

	catalogoMonedaData.load();
	catalogoEpoDistData.load();
	consultaData.loadData('');
	catalogoBanco.load();
	toolbar.hide();
	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24linea04ext.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});

});