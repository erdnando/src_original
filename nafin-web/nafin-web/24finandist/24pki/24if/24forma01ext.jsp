<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>   
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>
<%@ include file="../certificado.jspf" %>

<%
try{
ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
String operaContrato = BeanParametros.obtieneOperaSinCesion(iNoEPO); //PARAMETRO NUEVO JAGE 14012017

%>

<html>
<head>
<title>.:: N@fin Electrónico :: Financiamiento a Distribuidores ::.</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>

<script type="text/javascript" src="24forma01ext.js?<%=session.getId()%>"></script>
<%@ include file="/00utils/componente_firma.jspf" %>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01if/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01if/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>

	<form id='formParametros' name="formParametros">
		<input type="hidden" id="_strPerfil" name="_strPerfil" value="<%=strPerfil%>"/>	
                <input type="hidden" id="operaContrato" name="operaContrato" value="<%=operaContrato%>"/>
	</form>

</body>
</html>
<%
}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	out.println(" Error: "+e);
}
%>