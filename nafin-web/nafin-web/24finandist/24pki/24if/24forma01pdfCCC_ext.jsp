<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.sql.*, java.math.*, com.netro.descuento.*,
	com.netro.distribuidores.*,com.netro.exception.*, javax.naming.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %> 
<%

JSONObject jsonObj = new JSONObject();
boolean SIN_COMAS = false;
String fechaHoy	= "";
String fechaActual	= "";
String HoraActual	= "";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	HoraActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}
String 	ic_epo							= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String 	numero_credito					= (request.getParameter("numero_credito")==null)?"":request.getParameter("numero_credito");
String 	df_fecha_seleccion_de		= (request.getParameter("df_fecha_seleccion_de")==null)?fechaHoy:request.getParameter("df_fecha_seleccion_de");
String 	df_fecha_seleccion_a			= (request.getParameter("df_fecha_seleccion_a")==null)?fechaHoy:request.getParameter("df_fecha_seleccion_a");
String	tipoArchivo						= (request.getParameter("tipoArchivo")==null)?"":request.getParameter("tipoArchivo");
String 	ig_numero_docto				= (request.getParameter("ig_numero_docto")!=null)?request.getParameter("ig_numero_docto"):"";
String 	ic_pyme							= (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
boolean  hayMasDeUnaEpoSeleccionada = "true".equals(request.getParameter("hayMasDeUnaEpoSeleccionada"))?true:false;

try {
	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
	
	AutorizacionSolic autSolic = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
        
        ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); //PARAMETRO NUEVO JAGE 15012017
        String operaContrato = BeanParametros.obtieneOperaSinCesion(ic_epo); //PARAMETRO NUEVO JAGE 15012017
	
	Vector nombresCampo = new Vector(5);
	Vector detalleCampo = null;
	String campo ="", campo0="", campo1="",campo2="",campo3="",campo4="",campo5="";

	StringBuffer texto = new StringBuffer();
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

	if(application.getInitParameter("EpoCemex").equals(ic_epo)){
		texto.append(
			new String(" Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del ")+
			new String(" (los) Documento (s) final (es) descrito (s) en el mismo. Dicha aceptación tendrá plena validez para ")+
			new String(" todos los efectos legales. En este mismo acto se genera el aviso de notificación a la ")+
			new String(" EMPRESA DE PRIMER ORDEN y al DISTRIBUIDOR, por lo que tiene la validez de acuse de recibo, en términos ")+
			new String(" de los artículos 32 C del Código Fiscal o  2038 y 2041 de Código Civil Federal , según corresponda para ")+
			new String(" los efectos legales conducentes. "));
               if(operaContrato.equals("S")){
                    texto.setLength(0);
                    texto.append(
			new String(" Al transmitir este mensaje de datos bajo mi responsabilidad acepto el financiamiento del(los) ")+
			new String(" DOCUMENTO(S) FINAL(ES) descrito(s) en el mismo y de conformidad con el Contrato de Financiamiento ")+
			new String(" a Clientes y Distribuidores. Dicha aceptación tendrá plena validez para todos los efectos legales. "));
			
               }        
	}else{
		texto.append(
			new String(" Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del ")+
			new String(" (los) Documento (s) final (es) descrito (s) en el mismo. Dicha aceptación tendrá plena validez para ")+
			new String(" todos los efectos legales. En este mismo acto se genera el aviso de notificación a la ")+
			new String(" EMPRESA DE PRIMER ORDEN y al DISTRIBUIDOR, por lo que tiene la validez de acuse de recibo, en términos ")+
			new String(" de los artículos 32 C del Código Fiscal o  2038 y 2041 de Código Civil Federal , según corresponda para ")+
			new String(" los efectos legales conducentes. "));
                if(operaContrato.equals("S")){
                    texto.setLength(0);
                    texto.append(
			new String(" Al transmitir este mensaje de datos bajo mi responsabilidad acepto el financiamiento del(los) ")+
			new String(" DOCUMENTO(S) FINAL(ES) descrito(s) en el mismo y de conformidad con el Contrato de Financiamiento ")+
			new String(" a Clientes y Distribuidores. Dicha aceptación tendrá plena validez para todos los efectos legales. "));
               }       
                        
	}
	pdfDoc.setTable(1, 100);
	pdfDoc.setCell(texto.toString(),"formas",ComunesPDF.LEFT);
	pdfDoc.addTable();

	ArrayList listaClavesEpo = null;
	int i = 0;
	int elementos = 0;
	if(hayMasDeUnaEpoSeleccionada){

		listaClavesEpo = Comunes.stringToArrayList(ic_epo, ",");

		// Validar el horario de servicio de las epos seleccionadas, si alguna de estas no se encuentra en el horario de servicio
		// suprimirlas de la lista de epos validas
		for(int k=0;k<listaClavesEpo.size();k++){
			try {
				String claveEpo = (String) listaClavesEpo.get(k);
				Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
			}catch(Exception e){
				listaClavesEpo.remove(k);
				k--;
				/*
					if(e instanceof NafinException){
						NafinException e = (NafinException) e;
						if(e.getCause().equals("SIST0001")){
							throw e;
						}
					}
				*/
			}
		}

		// Validar que haya al menos una EPO 
		if(listaClavesEpo.size() == 0){
			throw new Exception("No se encontró ninguna EPO con horario válido");
		} else if (listaClavesEpo.size() == 1){
			ic_epo = (String) listaClavesEpo.get(0);
			hayMasDeUnaEpoSeleccionada = false;
		}
	}

	if(!hayMasDeUnaEpoSeleccionada){
		Horario.validarHorario(4, strTipoUsuario, ic_epo, iNoCliente);
		listaClavesEpo = new ArrayList();
		listaClavesEpo.add(ic_epo);
	}

	if (!ic_epo.equals("")){
	  // F029 - 2011: Si selecciono mas de una EPO usar nombres de campos genericos
	  if(hayMasDeUnaEpoSeleccionada){
		  
		  Vector fila = null;
		  
		  nombresCampo = new Vector();
		  
		  fila = new Vector();
		  fila.add("CG CAMPO 1");
		  nombresCampo.add(fila);
		  
		  fila = new Vector();
		  fila.add("CG CAMPO 2");
		  nombresCampo.add(fila);
		  
		  fila = new Vector();
		  fila.add("CG CAMPO 3");
		  nombresCampo.add(fila);
		  
		  fila = new Vector();
		  fila.add("CG CAMPO 4");
		  nombresCampo.add(fila);
		  
		  fila = new Vector();
		  fila.add("CG CAMPO 5");
		  nombresCampo.add(fila);
 
	  }else{
		  nombresCampo = cargaDocto.getCamposAdicionales(ic_epo,false);
	  }
	}

	// Obtener HashMap con los Parametros de Fondeo
	HashMap catalogoTipoFondeo = hayMasDeUnaEpoSeleccionada?BeanAutDescuento.getTipoFondeo(iNoCliente, listaClavesEpo, "4"):null;
	Vector vecFilas = null;
	Vector vecColumnas = null;
	boolean mostrarCabecera = true;
	pdfDoc.setTable(7, 100);
	for(int k=0;k<listaClavesEpo.size();k++){

		String claveEpo 	= (String) listaClavesEpo.get(k);

		if(!strPerfil.equals("IF FACT RECURSO")){
			vecFilas	=	autSolic.getLineasConDoctosCCC(claveEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a);
		}else{
			vecFilas	=	autSolic.getLineasConDoctosFF(claveEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a);
		}

		for(i=0;i<vecFilas.size();i++){
			
			vecColumnas 			= 	(Vector)vecFilas.get(i);
			String nombreAcreditado	=	(String)vecColumnas.get(0);
			String numLineaCredito	=	(String)vecColumnas.get(2);
			String moneda				=	(String)vecColumnas.get(3);
			String saldoInicial		=	(String)vecColumnas.get(5);
			String montoSeleccionado=	(String)vecColumnas.get(6);
			String numDoctos			=	(String)vecColumnas.get(7);
			String saldoDisponible	=	(String)vecColumnas.get(8);
			
			if(mostrarCabecera){
				pdfDoc.setCell("Nombre del acreditado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. línea de crédito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Saldo inicial","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto operado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. doctos. operados","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Saldo disponible","celda01",ComunesPDF.CENTER);
				mostrarCabecera = false;
			}
			pdfDoc.setCell(nombreAcreditado,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numLineaCredito,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(saldoInicial,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoSeleccionado,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numDoctos,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(saldoDisponible,2),"formas",ComunesPDF.CENTER);
		} // for
	}
	pdfDoc.addTable();

	mostrarCabecera = true;
	String	moneda	=	"",	icMoneda	=	"",	icDocumento	=	"",	tipoFinan = "", numeroCuenta="",	nombre_epo="";
	String	igNumeroDocto	=	"", fechaSolicitud = "",	distribuidor = "",	tipoConversion = "",	referencia = "",	plazo = "",	fechaVto = "";
	String	tipoPagoInteres=	"",	rs_referencia = "", tipoPago ="";
	double 	montoInteres = 0,	tasaInteres = 0,	monto = 0,	tipoCambio = 0,	montoValuado = 0;

	//totales
	int totalDoctosMN= 0,	totalDoctosUSD = 0;
	double totalMontoMN = 0,	totalMontoUSD = 0,	totalMontoValuadoMN = 0,	totalMontoValuadoUSD	= 0,	totalMontoIntMN = 0,totalMontoIntUSD = 0;
	//int columnas = 12;
	List lEncabezados = new ArrayList();
	//if(hayMasDeUnaEpoSeleccionada){ columnas +=1;}
	//pdfDoc.setTable(columnas, 100);
	for(int k=0;k<listaClavesEpo.size();k++){

		String claveDeLaEpo 	= (String) listaClavesEpo.get(k);

		if(!strPerfil.equals("IF FACT RECURSO")){
			vecFilas	=	autSolic.getDoctosSeleccionadosCCC(claveDeLaEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a,null);
		}else{
			vecFilas	=	autSolic.getDoctosSeleccionadosFF(claveDeLaEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a,null);
		}

		int numeroCamposAdicionales = nombresCampo.size();
		if(hayMasDeUnaEpoSeleccionada){
		 numeroCamposAdicionales = cargaDocto.getCamposAdicionales(claveDeLaEpo,false).size();
		}

		for(i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);

			igNumeroDocto		=	(String)vecColumnas.get(0);
			fechaSolicitud		=	(String)vecColumnas.get(1);
			distribuidor		=	(String)vecColumnas.get(2);
			moneda				=	(String)vecColumnas.get(4);
			icMoneda				=	(String)vecColumnas.get(5);
			monto					=	Double.parseDouble(vecColumnas.get(20).toString());
			tipoConversion		=	(String)vecColumnas.get(7);
			tipoCambio			=	Double.parseDouble(vecColumnas.get(8).toString());
			montoValuado		=	monto*tipoCambio;
			referencia			=	(String)vecColumnas.get(9);
			tasaInteres			=	Double.parseDouble(vecColumnas.get(21).toString());
			plazo					=	(String)vecColumnas.get(11);
			fechaVto				=	(String)vecColumnas.get(12);
			tipoPagoInteres	=	(String)vecColumnas.get(13);
			montoInteres 		=	Double.parseDouble(vecColumnas.get(15).toString());
			icDocumento			=	(String)vecColumnas.get(16);
			nombre_epo 			=	(String)vecColumnas.get(17);
			campo1 				=	(String)vecColumnas.get(22);
			campo2 				=	(String)vecColumnas.get(23);
			campo3 				=	(String)vecColumnas.get(24);
			campo4 				=	(String)vecColumnas.get(25);
			campo5 				=	(String)vecColumnas.get(26);
			numeroCuenta		=	(String)vecColumnas.get(27);
			tipoPago =  (String)vecColumnas.get(29); //F09-2015 
			if(numeroCuenta.equals("0")){
				numeroCuenta= "";
			}

			if(hayMasDeUnaEpoSeleccionada){
				tipoFinan = BeanAutDescuento.getDescripcionTipoFondeo(catalogoTipoFondeo,claveDeLaEpo);
			}
			if("1".equals(icMoneda)){
				totalDoctosMN 			++;
				totalMontoMN 			+= monto;
				totalMontoValuadoMN 	+= montoValuado;
				totalMontoIntMN		+= montoInteres;
			}else if("54".equals(icMoneda)){
				totalDoctosUSD			++;
				totalMontoUSD 			+= monto;
				totalMontoValuadoUSD	+= montoValuado;
				totalMontoIntUSD		+= montoInteres;
			}

			if(mostrarCabecera){
				lEncabezados.add("No. de documento Final");
				lEncabezados.add("Fecha solicitud");
				lEncabezados.add("EPO relacionada");
				lEncabezados.add("Distribuidor");
				lEncabezados.add("Moneda");
				lEncabezados.add("Monto");
				lEncabezados.add("Referencia tasa de interés");
				lEncabezados.add("Valor tasa de interés");
				lEncabezados.add("Plazo");
				lEncabezados.add("Fecha de vencimiento");
				lEncabezados.add("Monto interés");
				lEncabezados.add("Tipo de pago intereses");
				if (nombresCampo !=null ) {
					for (int e=0;e<nombresCampo.size();e++){
						detalleCampo = (Vector) nombresCampo.get(e);
						lEncabezados.add((String)detalleCampo.get(0));
					}
				}
				//lEncabezados.add("Cuenta Bancaria Distribuidor");
				if(hayMasDeUnaEpoSeleccionada){
					lEncabezados.add("Tipo de Financiamiento");
				}
				pdfDoc.setTable(lEncabezados, "celda01", 100);
				mostrarCabecera = false;
			} //if
			pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaSolicitud,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
			if(!tipoPago.equals("2")) {
			pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
			}else  {
				pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
			} 
			pdfDoc.setCell(Comunes.formatoDecimal(tasaInteres,2)+"%","formas",ComunesPDF.CENTER);
			if(!tipoPago.equals("2")) {
				pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
			}else { 
				pdfDoc.setCell(plazo+" (M)","formas",ComunesPDF.CENTER);
			}
			pdfDoc.setCell(fechaVto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(tipoPagoInteres,"formas",ComunesPDF.CENTER);
			if (nombresCampo !=null ) {
				for (int j=1;j<=nombresCampo.size();j++){
					if(j>numeroCamposAdicionales){
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						continue;
					}
					if (j==1) {
						pdfDoc.setCell(campo1,"formas",ComunesPDF.CENTER);
					} else if (j==2) {
						pdfDoc.setCell(campo2,"formas",ComunesPDF.CENTER);
					} else if (j==3) { 
						pdfDoc.setCell(campo3,"formas",ComunesPDF.CENTER);
					} else if (j==4) {
						pdfDoc.setCell(campo4,"formas",ComunesPDF.CENTER);
					} else if (j==5) { 
						pdfDoc.setCell(campo5,"formas",ComunesPDF.CENTER);
					}
				}       
			}
			//pdfDoc.setCell(numeroCuenta,"formas",ComunesPDF.CENTER);
			if(hayMasDeUnaEpoSeleccionada){
				pdfDoc.setCell(tipoFinan,"formas",ComunesPDF.CENTER);
			}
		}// for
	}
	if(totalDoctosMN>0){
		pdfDoc.setCell("Total M.N.","formas",ComunesPDF.CENTER);
		pdfDoc.setCell(Integer.toString(totalDoctosMN),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoIntMN,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		if (nombresCampo !=null ) {
			for (int e=0;e<nombresCampo.size();e++){
				detalleCampo = (Vector) nombresCampo.get(e);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			}
		}
		if(hayMasDeUnaEpoSeleccionada){
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		}
	}
	if(totalDoctosUSD>0){
		pdfDoc.setCell("Total Dolares","formas",ComunesPDF.CENTER);
		pdfDoc.setCell(Integer.toString(totalDoctosUSD),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoIntUSD,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		if (nombresCampo !=null ) {
			for (int e=0;e<nombresCampo.size();e++){
				detalleCampo = (Vector) nombresCampo.get(e);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			}
		}
		if(hayMasDeUnaEpoSeleccionada){
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		}
	}
	pdfDoc.addTable();
		
	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>