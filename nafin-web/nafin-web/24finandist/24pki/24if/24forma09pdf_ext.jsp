<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.math.*, com.netro.distribuidores.*,
	com.netro.exception.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%

JSONObject jsonObj = new JSONObject();

String _acuse		= (request.getParameter("_acuse")==null)?"":request.getParameter("_acuse");
String acuse		= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String fechaHoy	= (request.getParameter("fechaHoy")==null)?"":request.getParameter("fechaHoy");
String horaCarga	= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");
String ic_epo		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String mensaje_a	= (request.getParameter("mensaje")==null)?"":request.getParameter("mensaje");

try {
	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

	pdfDoc.setTable(2, 50);
	pdfDoc.setCell(mensaje_a,"celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Recibo Número:"+_acuse,"celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Moneda Nacional","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Acuse","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(acuse,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Fecha","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Hora","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(horaCarga,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Usuario","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(strLogin+" - "+strNombreUsuario,"formas",ComunesPDF.LEFT);
	pdfDoc.addTable();

	List lregistros = BeanParametros.getPymesBloqueo(iNoCliente, ic_epo, ic_pyme);
	if(lregistros.size()>0){
		pdfDoc.setTable(7, 100);
		pdfDoc.setCell("Los cambios se realizaron con éxito","celda01",ComunesPDF.CENTER,7);
		pdfDoc.setCell("N@E","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("PyME","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Bloqueado","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha/Hora\nBloqueo/Desbloqueo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("IF Bloqueo/Desbloqueo","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER);
		
		for(int i=0;i<lregistros.size();i++){
			List lfila	= (List)lregistros.get(i);
			String bloq	= lfila.get(5).toString().equals("S")?"Si":"No";
			String ne	= (String)lfila.get(0);
			String pyme	= (String)lfila.get(1);
			String rfc	= (String)lfila.get(2);
			String fecha= (String)lfila.get(3);
			String causa= (String)lfila.get(4);
			String usuario1 = (String)lfila.get(8);
			pdfDoc.setCell(ne,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(pyme,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(bloq,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fecha,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(usuario1, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>