Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

	function leeRespuesta(){
		window.location = '24forma06ext.jsp';
	}

	form = {nRow:null, textoF:null, _acuse:null, acuse:null, fechaC:null,horaC:null}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarSave(opts, success, response) {
		pnl.el.unmask();
		Ext.getCmp('btnGuardar').setIconClass('icoGuardar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fp.hide();
			grid.hide();
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.Msg.alert('',infoR.msg);
			Ext.getCmp('disTitle').body.update('<div align="center"><b>Recibo N�mero:  '+infoR._acuse+'</b></div>');
			Ext.getCmp('disAcuse').body.update(infoR.acuse);
			Ext.getCmp('disFechaCarga').body.update(infoR.fechaCarga);
			Ext.getCmp('disHoraCarga').body.update(infoR.horaCarga);
			Ext.getCmp('disUsuario').body.update('<div class="formas" >'+infoR.usuario+'<div>');

			fpAcuse.show();
			if (infoR.registros != undefined && infoR.registros.length > 0){
				consultaDataAcuse.loadData(infoR.registros);
				gridAcuse.show();
			}

			Ext.getCmp('btnGenerarPDF').setHandler(
				function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '24forma06pdf_ext.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
									_acuse: infoR._acuse,
									acuse: infoR.acuse,
									fechaHoy:infoR.fechaCarga,
									horaActual:infoR.horaCarga
						}),
						callback: procesarSuccessFailureGenerarPDF
					});
				}
			);
			Ext.getCmp('btnGenerarArchivo').setHandler(
				function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '24forma06arch_ext.jsp',
						params:	Ext.apply(fp.getForm().getValues(),{
									_acuse: infoR._acuse,
									acuse: infoR.acuse,
									fechaHoy:infoR.fechaCarga,
									horaActual:infoR.horaCarga
						}),
						callback: procesarSuccessFailureGenerarArchivo
					});
				}
			);


		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaProcesar(opts, success, response) {
		consultaData.loadData('');
		form.textoF = "";
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			
			//FODEA-005-2014-VALIDACION
			var tipoCredito=Ext.getCmp('tipo_credito').getValue();
			var tipoTasa=Ext.getCmp('tipo_tasa').getValue();
			if(tipoCredito=='C' && (tipoTasa=='P'||tipoTasa=='N')){
				grid.getColumnModel().setHidden(2,false);	
				gridAcuse.getColumnModel().setHidden(1,false);	
			}else{
				grid.getColumnModel().setHidden(2,true);
				gridAcuse.getColumnModel().setHidden(1,true);	
			}
			
			
			
			//
			if(infoR.lsTexto != undefined){
				form.textoF = infoR.lsTexto;
			}
			if (	!grid.isVisible()	){
				grid.show();
			}
			var cm = grid.getColumnModel();
			cm.setHidden(cm.findColumnIndex('REL_MAT'), true);
			cm.setHidden(cm.findColumnIndex('PUNTOS_ADD'), true);
			cm.setHidden(cm.findColumnIndex('FECHA'), true);
			if (infoR.regs != undefined && infoR.regs.length > 0){
				consultaData.loadData(infoR.regs);
				if (	Ext.getCmp('tipo_tasa').getValue() === 'P' ){
					cm.setHidden(cm.findColumnIndex('REL_MAT'), false);
					cm.setHidden(cm.findColumnIndex('PUNTOS_ADD'), false);
				}else{
					cm.setHidden(cm.findColumnIndex('FECHA'), false);
				}
				Ext.getCmp('btnGuardar').enable();
				Ext.getCmp('btnCancelar').enable();
				grid.getGridEl().unmask();
			}else{
				Ext.getCmp('btnGuardar').disable();
				Ext.getCmp('btnCancelar').disable();
				grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
		pnl.el.unmask();
	}

	var tipoCreditoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [	['D','Modalidad 1 (Riesgo Empresa de Primer Orden )'],['C','Modalidad 2 (Riesgo Distribuidor)']	]			 
	});

	var catalogoTasaData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['P','PREFERENCIAL'],
			['N','NEGOCIADA']
		 ]
	});

	var procesarCatalogoMoneda = function(store, arrRegistros, opts) {
		var dato = catalogoMonedaData.findExact("clave", "1");
		if (dato != -1 ) {
			Ext.getCmp('ic_moneda').setValue('1');
		}
	}

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma06ext.data.jsp',
		baseParams: {	informacion: 'CatalogoMonedaDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesarCatalogoMoneda,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma06ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma06ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDistribuidor'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoPlazoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma06ext.data.jsp',
		baseParams: {	informacion: 'CatalogoPlazo'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var consultaDataAcuse = new Ext.data.JsonStore({
		fields: [ {name:'NOMBRE_EPO'},{name:'NOMBRE_PYME'},{name:'TASA_INTERES'},{name:'REFERENCIA'},{name:'PLAZO'},{name:'VALOR'},{name:'TASA_APLICAR'} ],
		data:	[{	'NOMBRE_EPO':'',	'NOMBRE_PYME':'',	'TASA_INTERES':'',	'REFERENCIA':'',	'PLAZO':'',	'VALOR':'',	'TASA_APLICAR':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});


	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name:'CLAVE_TASA'},{name:'NOMBRE_EPO'},{name:'TIPO_CREDITO'},{name:'TASA_INTERES'},
			{name:'PLAZO'},{name:'VALOR'},{name:'FECHA'},{name:'REL_MAT'},{name:'PUNTOS_ADD'},{name:'TASA_APLICAR'},{name:'NOMBRE_PYME'}
		],
		data:	[{	'CLAVE_TASA':'','TIPO_CREDITO':'','NOMBRE_EPO':'',	'TASA_INTERES':'',	'PLAZO':'',	'VALOR':'',	'FECHA':'',	'REL_MAT':'',	'PUNTOS_ADD':'',	'TASA_APLICAR':'','NOMBRE_PYME':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridAcuse = new Ext.grid.GridPanel({
		id:'gridAcuse',	store:consultaDataAcuse,	columLines:true, 
		//viewConfig: {forceFit: true}, 
		hidden:true,autoScroll:true,
		stripeRows:true,	loadMask:true,	height:200,	width:940,	style:'margin:0 auto;',	frame:true,
		columns: [
			{
				header:'EPO Relacionada',	tooltip:'EPO Relacionada',	dataIndex:'NOMBRE_EPO',	sortable:true,	width:200,	align:'center'
			},{
				header:'Nombre de la PYME',	tooltip:'Nombre de la PYME',	dataIndex:'NOMBRE_PYME',	sortable:true,	width:200,	align:'center',hidden:true
			},{
				header:'Referencia',	tooltip:'Referencia',	dataIndex:'REFERENCIA',	sortable:true,	width:150,	align:'center'
			},{
				header:'Tipo Tasa',	tooltip:'Tipo Tasa',	dataIndex:'TASA_INTERES',	sortable:true,	width:150,	align:'center'
			},{
				header:'Plazo',	tooltip:'Plazo',	dataIndex:'PLAZO',	sortable:true,	width:100,	align:'center'
			},{
				header:'Valor',	tooltip:'Valor',	dataIndex:'VALOR',	sortable:true,	width:100,	align:'right',renderer: Ext.util.Format.numberRenderer('0.00')
			},{
				header:'Valor Tasa',	tooltip:'Valor Tasa',	dataIndex:'TASA_APLICAR',	sortable:true,	width:100,	hideable:false,	align:'right'
			}
		],
		bbar: {
			id:'barraGridB',
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',	text: 'Generar PDF',	id: 'btnGenerarPDF',iconCls: 'icoPdf'
				},{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',

					hidden: true
				},{
					xtype: 'tbspacer', width: 5
				},'-',{
					xtype: 'button',	text: 'Generar Archivo',	id: 'btnGenerarArchivo',iconCls: 'icoXls'
				},{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				}
			]
		}
	});
	

	var elementosAcuse = [
		{
			xtype: 'panel',layout:'table',	width:500,	border:true,	layoutConfig:{ columns: 2 },
			defaults: {frame:false, border: true,width:160, height: 35,bodyStyle:'padding:6px'},
			items:[
				{	id:'disTitle',	width:500,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Recibo N�mero:</div>'	},
				{	html:'<div class="formas" align="left">N�mero de Acuse</div>'	},
				{	id:'disAcuse', width:340,	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Fecha de Autorizaci�n</div>'	},
				{	width:340,id:'disFechaCarga',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Hora de Autorizaci�n</div>'	},
				{	width:340,id:'disHoraCarga',	html:'&nbsp;'},
				{	html:'<div class="formas" align="left">Usuario de Captura</div>'	},
				{	width:340,id:'disUsuario',html: '&nbsp;'	},
				{	width:500,	colspan:2,	html: '<div align="center"></div>'	}
			]
		}
	];

	var fpAcuse=new Ext.FormPanel({	style: 'margin:0 auto;',	height:'auto',	width:502,	border:true,	frame:false,	items:elementosAcuse,	hidden:true 	});


	var confirma = function(pkcs7, textoFirma, puntos, tasa_aplicar, ic_tasa  ){
	
		if (Ext.isEmpty(pkcs7)) {
		
			Ext.Msg.alert('Firmar','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;	//Error en la firma. Termina...
		}else  {
		
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url : '24forma06ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion:'Save',
					razonSocial:Ext.getCmp('ic_epo').lastSelectionText,
					Pkcs7:pkcs7,
					TextoFirmado:textoFirma,
					puntos:puntos,
					valor_tasa_neg:tasa_aplicar,
					ic_tasa:	ic_tasa	
				}),
				callback: procesarSave
			});			
									
		}
	}
	
	

	var grid = new Ext.grid.EditorGridPanel({
		id:'grid',store: consultaData,	clicksToEdit:1,	columLines:true, viewConfig: {forceFit: true}, hidden:true,
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
		columns: [
			{
				header:'Tipo de Cr�dito',	tooltip:'Tipo de Cr�dito',	dataIndex:'TIPO_CREDITO',	sortable:true,	width:150,	align:'center'
			},{
				header:'EPO Relacionada',	tooltip:'EPO Relacionada',	dataIndex:'NOMBRE_EPO',	sortable:true,	width:150,	align:'center'
			},{
				header:'Nombre de la Pyme',	tooltip:'Nombre de la Pyme',	dataIndex:'NOMBRE_PYME',	sortable:true,	width:150,	align:'center',hidden:true
			},{
				header:'Tipo Tasa',	tooltip:'Tipo Tasa',	dataIndex:'TASA_INTERES',	sortable:true,	width:150,	align:'center'
			},{
				header:'Valor',	tooltip:'Valor',	dataIndex:'VALOR',	sortable:true,	width:120,	align:'right',renderer: Ext.util.Format.numberRenderer('0.00')
			},{
				header:'Rel. Mat.',	tooltip:'Rel. Mat.',	dataIndex:'REL_MAT',	sortable:true,	width:100,	align:'center', hidden:true, hideable:false
			},{
				header:'Puntos',	tooltip:'Puntos',	dataIndex:'PUNTOS_ADD',	sortable:true,	width:120,	hidden:true, hideable:false,	align:'right',
				editor: {xtype : 'numberfield', id: 'puntos', name:'puntos',	maxValue:998,	minValue:0, maxLength:9},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(Ext.util.Format.number(value,'0.00'),metadata,registro);
							}
			},{
				header:'Fecha',	tooltip:'Fecha',	dataIndex:'FECHA',	sortable:true,	width:110,	align:'center',hidden:true, hideable:false
			},{
				header:'Valor de la Tasa',	tooltip:'Valor de la Tasa',	dataIndex:'TASA_APLICAR',	sortable:true,	width:120,	hideable:false,	align:'right',
				editor: {xtype : 'numberfield', id:'tasa_aplicar', name:'tasa_aplicar',	maxValue:999,	minValue:0, decimalPrecision:10,
							maxText:'El n�mero es muy grande. Por favor escriba un n�mero menor o igual a 3 enteros  y 5 decimales',
							minText:'La tasa debe ser mayor o igual a cero',
							listeners:{ blur:{ fn:	function(field){
																var numero = (field.getValue()).toString();
																if (numero.indexOf('.') == -1) numero += ".";
																	dectext = numero.substring(numero.indexOf('.')+1, numero.length);
																if (dectext.length > 5) {
																	Ext.Msg.alert('Error de Validaci�n', 
																		"El n�mero es muy grande. Por favor escriba un n�mero menor o igual a 3 enteros  y 5 decimales",
																		function() {
																			var col = grid.getColumnModel().findColumnIndex('TASA_APLICAR');
																			Ext.getCmp('grid').startEditing(form.nRow, col);
																		});
																}
															}
											}
							}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								var numero = value.toString();								
								if (	Ext.getCmp('tipo_tasa').getValue() === 'P' &&  registro.data['PUNTOS_ADD']!='') {
									 numero = roundOff(parseFloat(registro.data['VALOR']) - parseFloat(registro.data['PUNTOS_ADD']),2);
								}																							
								var dectext="";
								if (numero.indexOf('.') == -1) numero += ".";
									dectext = numero.substring(numero.indexOf('.')+1, numero.length);
								switch(dectext.length) {
									case 1:
										value = Ext.util.Format.number(numero,'0.0');
										break;
									case 2:
										value = Ext.util.Format.number(numero,'0.00');
										break;
									case 3:
										value = Ext.util.Format.number(numero,'0.000');
										break;
									case 4:
										value = Ext.util.Format.number(numero,'0.0000');
										break;
									case 5:
										value = Ext.util.Format.number(numero,'0.00000');
										break;
									default:
										if (	Ext.getCmp('tipo_tasa').getValue() === 'N' ){
											value = Ext.util.Format.number(value,'0.00');
										}
								}
								return NE.util.colorCampoEdit(value,metadata,registro);
							}
			}
		],
		bbar: {
			id:'barraGrid',
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					xtype: 'button',	text: 'Cancelar',	id: 'btnCancelar',	iconCls:'icoRechazar',	disabled:true,
					handler: function(boton, evento) {
									leeRespuesta();
					}
				},{
					xtype: 'tbspacer', width: 5
				},'-',{
					xtype: 'button',	text: 'Guardar',	id: 'btnGuardar',	iconCls:'icoGuardar', disabled:true,
					handler: function(boton, evento) {
									var jsonData = consultaData.data.items;
									var puntos = Ext.getCmp('puntos').getValue();
									var tasa_aplicar = Ext.getCmp('tasa_aplicar').getValue();
									var ic_tasa = "";
									if (	Ext.getCmp('tipo_tasa').getValue() === 'N' ){
											if ( Ext.isEmpty(tasa_aplicar) ){
												Ext.Msg.alert(boton.text,"Debe capturar el valor de la tasa");
												return;
											}
											Ext.each(jsonData, function(item,index,arrItem){
												ic_tasa = item.data.CLAVE_TASA;
											});
									}else{
										if ( Ext.isEmpty(puntos) ){
											Ext.Msg.alert(boton.text,"Los puntos deben ser capturados",
											function(){
												var col = grid.getColumnModel().findColumnIndex('PUNTOS_ADD');
												Ext.getCmp('grid').startEditing(form.nRow, col);
												return;
											});
											return;
										}
										if (tasa_aplicar<0){
											Ext.Msg.alert(boton.text,"La tasa debe ser mayor o igual a cero",
											function(){
												var col = grid.getColumnModel().findColumnIndex('PUNTOS_ADD');
												Ext.getCmp('grid').startEditing(form.nRow, col);
												return;
											});
											return;
										}
										if (tasa_aplicar>=999){
											Ext.Msg.alert(boton.text,"El n�mero es muy grande. Por favor escriba un n�mero menor o igual a 3 enteros  y 5 decimales",function(){
												var col = grid.getColumnModel().findColumnIndex('PUNTOS_ADD');
												Ext.getCmp('grid').startEditing(form.nRow, col);
												return;											
											});
											return;
										}
									}
									
									NE.util.obtenerPKCS7(confirma, form.textoF, puntos, tasa_aplicar, ic_tasa    );
									
								}
				}
			]
		},
		listeners: {
			beforeedit: {
				fn: function(obj) {
					if (obj.field === 'TASA_APLICAR') {
						if (	Ext.getCmp('tipo_tasa').getValue() === 'P' ){
							obj.cancel = true;
							return;
						}
					form.nRow = obj.row;

					}else if (obj.field === 'PUNTOS_ADD') {
						form.nRow = obj.row;
					}
				}
			},
			validateedit: {
				fn: function(obj) {
					if (obj.field == 'PUNTOS_ADD') {
						var resultado = parseFloat(obj.record.data['VALOR']) - parseFloat(obj.value);
						if (resultado < 0	) {
							//obj.cancel = true;
							Ext.Msg.alert('Error de Validaci�n', 
								"La tasa debe ser mayor o igual a cero",
								function() {
									obj.grid.startEditing(obj.row, obj.column);
									return;
								});
							obj.value = "";
							obj.record.data['TASA_APLICAR'] = "";
							return;
						}
						obj.record.data['TASA_APLICAR'] = Ext.util.Format.number(resultado,'0.00');
					}else if(obj.field == 'TASA_APLICAR'){
						var resultado = parseFloat(obj.record.data['VALOR']) - parseFloat(obj.value);
						if (resultado < 0	) {
							//obj.cancel = true;
							Ext.Msg.alert('Error de Validaci�n', 
								"El valor de la tasa debe ser menor o igual a "+parseFloat(obj.record.data['VALOR']),
								function() {
									obj.grid.startEditing(obj.row, obj.column);
									return;
								});
							obj.value = "";
							//obj.record.data['TASA_APLICAR'] = "";
							return;
						}
					}
				}
			}
		}
	});

	var elementosForma = [
		{
			xtype: 'combo',
			id:	'tipo_credito',
			name: '_tipo_credito',
			hiddenName : '_tipo_credito',
			fieldLabel: 'Tipo de cr�dito',
			emptyText: 'Seleccionar Credito',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			allowBlank: false,
			lazyRender:true,
			typeAhead: true,
			minChars : 1,
			listeners: {
				select:{
					fn:function(combo){
						catalogoEpoDistData.load({
							params:{
								tipoCredito:combo.value
							}
						});
						
					}
				}
				
			},
			store: tipoCreditoData//,
			//value:'D'
		},{
			xtype: 'combo',
			name: '_tipo_tasa',
			id:	'tipo_tasa',
			hiddenName : '_tipo_tasa',
			fieldLabel: 'Tipo de Tasa',
			emptyText: 'Seleccionar . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoTasaData, value:'P',
			listeners:{
				select:	{fn:function(combo){
									if (	!Ext.isEmpty(combo.getValue())	) {	
										grid.hide();
										var comboPlazo = Ext.getCmp('plazo');
										comboPlazo.setValue('');
										comboPlazo.store.removeAll();
										comboPlazo.store.reload({	params: {tipo_tasa: combo.getValue(), 
																						ic_epo:Ext.getCmp('ic_epo').getValue(),
																						ic_moneda: Ext.getCmp('ic_moneda').getValue()
																						}	});
									}
							}
				}
			}
		},{
			xtype: 'combo',
			id:	'ic_epo',
			name: '_ic_epo',
			hiddenName : '_ic_epo',
			fieldLabel: 'EPO',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			allowBlank:false,
			typeAhead: true,
			minChars : 1,	height:300,
			store: catalogoEpoDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:	{fn:function(combo){
					var tipoCredito =Ext.getCmp('tipo_credito').getValue();
									if (	!Ext.isEmpty(combo.getValue())	) {
										grid.hide();
										var comboPyme = Ext.getCmp('ic_pyme');
										var comboPlazo = Ext.getCmp('plazo');
										comboPyme.setValue('');
										comboPyme.store.removeAll();
										comboPyme.store.reload({	
											params: {
												ic_epo: combo.getValue() ,
												tipoCredito:tipoCredito
											}	
										});
										comboPyme.reset();
										if(tipoCredito=='D'){
											comboPlazo.setValue('');
											comboPlazo.store.removeAll();
											comboPlazo.store.reload({	params: {ic_epo: combo.getValue(), 
																							ic_moneda:Ext.getCmp('ic_moneda').getValue(),
																							tipo_tasa: Ext.getCmp('tipo_tasa').getValue(),
																							tipo_credito :Ext.getCmp('tipo_credito').getValue()
																							}	});
										}
									}
							}
				}
			}
		},{
			xtype: 'combo',
			name: '_ic_moneda',
			id:	'ic_moneda',
			hiddenName : '_ic_moneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccionar . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			allowBlank:false,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:	{fn:function(combo){
									if (	!Ext.isEmpty(combo.getValue())	) {
										grid.hide();
										var comboPlazo = Ext.getCmp('plazo');
										comboPlazo.setValue('');
										comboPlazo.store.removeAll();
										comboPlazo.store.reload({	params: {ic_moneda: combo.getValue(), 
																						ic_epo:Ext.getCmp('ic_epo').getValue(),
																						tipo_tasa: Ext.getCmp('tipo_tasa').getValue()
																						}	});
									}
							}
				}
			}
		},{
			xtype: 'combo',
			id:	'ic_pyme',
			name: '_ic_pyme',
			hiddenName : '_ic_pyme',
			fieldLabel: 'Nombre del Distribuidor',
			emptyText: 'Seleccionar . . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			allowBlank:false,
			typeAhead: true,
			minChars : 1,
			store: catalogoPymeDistData,	
			tpl:NE.util.templateMensajeCargaCombo,
			listeners:{
				select:	{fn:function(combo){
					var tipoCredito =Ext.getCmp('tipo_credito').getValue();
									if (	!Ext.isEmpty(combo.getValue())	) {
										grid.hide();
										var comboPyme = Ext.getCmp('ic_pyme');
										var comboPlazo = Ext.getCmp('plazo');
										if(tipoCredito=='C'){
											comboPlazo.setValue('');
											comboPlazo.store.removeAll();
											comboPlazo.store.reload({	params: {ic_pyme: comboPyme.getValue(), 
																							ic_moneda:Ext.getCmp('ic_moneda').getValue(),
																							tipo_tasa: Ext.getCmp('tipo_tasa').getValue(),
																							tipo_credito :Ext.getCmp('tipo_credito').getValue(),
																							ic_epo: Ext.getCmp('ic_epo').getValue()
																							}	});
										}
									}
							}
				}
			}
		},{
			xtype: 'combo',
			name: 'plazo',
			id:	'plazo',
			hiddenName : '_plazo',
			fieldLabel: 'Plazo',
			emptyText: 'Seleccionar . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			allowBlank:false,
			typeAhead: true,
			minChars : 1,
			store: catalogoPlazoData,
			tpl : NE.util.templateMensajeCargaCombo
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 550,
		style: ' margin:0 auto;',
		title:	'<div align="center">Tasas Preferenciales/Negociadas</div>',
		frame: true,
		labelWidth:135,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Procesar',
				id: 'btnProcesar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton, evento) {
								form.nRow = 0;
								grid.hide();
								
								pnl.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '24forma06ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
										informacion:	'Procesar',
										tipo_credito:Ext.getCmp('tipo_credito').getValue(),//MOD +(ALL)
										valorTipoCredito:Ext.getCmp('tipo_credito').getRawValue(),//MOD +(ALL)
										razonSocial:	Ext.getCmp('ic_epo').lastSelectionText	}),
									callback: procesaProcesar
								});
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	fpAcuse,	NE.util.getEspaciador(10),	grid,	NE.util.getEspaciador(10),	gridAcuse,	NE.util.getEspaciador(10)
		]
	});

	catalogoMonedaData.load();
	//catalogoEpoDistData.load();
	catalogoPlazoData.load();

});