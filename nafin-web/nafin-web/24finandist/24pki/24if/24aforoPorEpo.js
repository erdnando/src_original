Ext.onReady(function() {
	var aforo = [];
	var aforoAnt = [];
	var moneda = [];
	var ic_epo = [];
	var cancelar =  function() {  
		aforo = [];
		aforoAnt = [];
		moneda = [];
		ic_epo = [];
	}
//- - - - - - -  - - - - - HANDLER�S - - - - - - - - - - - - - - - - - - - - - -
	function procesaRecargaResultado(opts, success, response) {
		var fpDatos = Ext.getCmp('gridConsulta');
		fpDatos.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var grid1 = Ext.getCmp('gridConsulta');
			grid1.el.unmask();
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var mensaje = jsonData.mensaje;
				if(jsonData.accion=='G') {
					Ext.MessageBox.alert('Mensaje',mensaje);	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
				}
			} 
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24aforoPorEpo.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});

	var procesarConsultaData = function(store,arrRegistros,opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0) {
				gridConsulta.show();	
				el.unmask();
				Ext.getCmp('btnGuardar').enable();
			} else {		
				gridConsulta.show();
				Ext.getCmp('btnGuardar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var consultaData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '24aforoPorEpo.data.jsp',
		fields : ['clave', 'descripcion','loadMsg'],
		baseParams: {
			informacion: 'Consultar'
		},
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
				{name: 'IC_IF'},
				{name: 'IC_EPO'},
				{name: 'EPO'},
				{name: 'IC_MONEDA'},
				{name: 'CD_NOMBRE'},
				{name: 'FN_VALOR_AFORO'},
				{name: 'FN_VALOR_AFORO_AUX'},
				{name: 'CG_USUARIO_ALTA'},
				{name: 'DF_ALTA'},
				{name: 'CG_USUARIO_MODIFICA'},
				{name: 'DF_MODIFICACION'}
			]
		}),
		groupField: 'EPO',
		sortInfo:{field: 'EPO', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});
	var grid = new Ext.grid.EditorGridPanel({
		store: consultaData,
		columnLines:false,
		id: 'gridConsulta',
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		title: ' ',
		hidden: true,
		titleCollapse: false,
		collapsible: true,
		frame:true,
	   height: 400,
		width:920,
		stripeRows:true,
		clicksToEdit:1,
		loadMask: true,
		columns: [
			{
				header:' ',
				tooltip:' ',
				dataIndex:'CD_NOMBRE',
				sortable: true,
				resizable: true,
				hidden: false,
				width: 200,
				minChars : 1,
				align: 'center'
			},
			{
				header:'EPO ',
				tooltip:'EPO',
				dataIndex:'EPO',
				sortable: true,
				resizable: true,
				hidden: false,
				width: 200,
				minChars : 1,
				align: 'center'
			},
			{
				header: '% Aforo',
				tooltip: '% Aforo',
				dataIndex: 'FN_VALOR_AFORO',
				sortable: true,
				resizable: true,
				hidden: false,
				width: 100,
				minChars : 1,
				align: 'center',
				editor: {
					xtype: 'numberfield',
					maxValue : 999999999999.99
				},
				renderer:function(value,metadata,registro){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			},
			{
				header:'Usuario Alta',
				tooltip: 'Usuario Alta',
				dataIndex:'CG_USUARIO_ALTA',
				sortable: true,
				resizable: true,
				hidden: false,
				width: 200,
				minChars : 1,
				align: 'center'
			},
			{
				header:'Fecha Alta',
				tooltip: 'Fecha Alta',
				dataIndex:'DF_ALTA',
				sortable: true,
				resizable: true,
				hidden: false,
				width: 100,
				minChars : 1,
				align: 'center'
			},
			{
				header:'Usuario �ltima Modificaci�n',
				tooltip: 'Usuario �ltima Modificaci�n',
				dataIndex:'CG_USUARIO_MODIFICA',
				sortable: true,
				resizable: true,
				hidden: false,
				width: 200,
				minChars : 1,
				align: 'center'
			},
			{
				header:'Fecha �ltima Modificaci�n',
				tooltip: 'Fecha �ltima Modificaci�n',
				dataIndex:'DF_MODIFICACION',
				sortable: true,
				resizable: true,
				hidden: false,
				width: 100,
				minChars : 1,
				align: 'center'
			}
		],
		view: new Ext.grid.GroupingView({  
		forceFit:true,
			groupTextTpl:'{text} '//			groupTextTpl:'{text}  {[ values.rs[0].data["IC_MONEDA"] == "" ? "La EPO no tiene l�neas de Cr�dito parametrizadas, favor de verificar " : " " ]}'

			}),
		bbar: {
			xtype: 'toolbar',
			items: [
				'-',
				'->',
				{
					xtype: 'button',
					text: 'Guardar',
					id: 'btnGuardar',
					iconCls: 'icoGuardar',
					handler: function(boton, evento) {
						var i=0;
						var  gridConsulta = Ext.getCmp('gridConsulta');  
						var store = gridConsulta.getStore();
						var jsonData = store.data.items;
						cancelar();
						Ext.each(jsonData, function(item,inx,arrItem){
							aforo.push(item.data.FN_VALOR_AFORO);	
							ic_epo.push(item.data.IC_EPO);
							moneda.push(item.data.IC_MONEDA);
							aforoAnt.push(item.data.FN_VALOR_AFORO_AUX);
							i++;
						});
						grid.el.mask('Guardando....', 'x-mask-loading');
						Ext.Ajax.request({
							url: '24aforoPorEpo.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'Guardar_Datos',
								aforo: aforo,
								moneda : moneda,
								aforoAnt : aforoAnt,
								ic_epo : ic_epo,
								numeroRegistros : i
							}),
							callback:procesaRecargaResultado
						});
					}
				}		
			]
		}	
	});
	var elementosFormaConsulta = [
		{
			xtype: 'combo',
			name: 'epo',
			id: 'cmbEpo',
			hiddenName : 'epo',
			fieldLabel: '&nbsp;&nbsp;EPO',
			mode: 'local', 
			autoLoad: false,
			msgTarget: 'side',
			valueField : 'clave',
			displayField : 'descripcion',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,	
			width: 200,
			store : catalogoIF,
			tpl:  '<tpl for=".">' +
                '<tpl if="!Ext.isEmpty(loadMsg)">'+
                '<div class="loading-indicator">{loadMsg}</div>'+
                '</tpl>'+
                '<tpl if="Ext.isEmpty(loadMsg)">'+
                '<div class="x-combo-list-item">' +
                '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                '</div></tpl></tpl>'
		}
	];
	var fp = new Ext.form.FormPanel({
		id				 :'forma',
		width			 :400,
		style			 :'margin:0 auto;',
		frame			 :true,
		bodyStyle	 :'padding: 6px',
		labelWidth	 :100,
		title:' ',
		titleCollapse: false,
		collapsible: true,
		defaults		 : {
			msgTarget	: 'side',
			anchor		: '-20'
		},
		items			 :elementosFormaConsulta,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					fp.el.mask('Consultando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function(boton, evento) {
					Ext.getCmp('forma').getForm().reset();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridConsulta').reset();
					
					
				}
			}

		]
	});
//----------------------------------- CONTENEDOR -------------------------------------
	var contenedorPrincipal = new Ext.Container({
		applyTo: 'areaContenido',
		width: 	949,
		height: 'auto',
		style: 'margin:0 auto;',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	});
	catalogoIF.load();
});