<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.distribuidores.*,
		com.netro.anticipos.*,
		net.sf.json.JSONArray,	net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

System.out.println("informacion -----------> "+informacion );

if (informacion.equals("valoresIniciales"))	{

	JSONObject jsonObj = new JSONObject();
	Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);

	String auxLink = lineadm.getDetalle(iNoCliente);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("auxLink", auxLink);
	jsonObj.put("strPerfil", strPerfil);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("detalleCuenta"))	{

	String ic_epo=	(request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	String solic=	(request.getParameter("solic")!=null)?request.getParameter("solic"):"";
	String folio =	(request.getParameter("folio")!=null)?request.getParameter("folio"):"";
	String peticion =	(request.getParameter("peticion")!=null)?request.getParameter("peticion"):"";
	String ic_pyme=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String moneda	=	(request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):""; 
	JSONObject jsonObj = new JSONObject();

	//Fodea 05-2014 
	AceptacionPyme acepPymeHome = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
	String montoMinimo =  acepPymeHome.getMontoTotalMocumentos  (ic_epo,moneda,  iNoCliente , "2", ic_pyme, folio );	


	if (!"".equals(folio)) {

		LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

		String auxCad = "";
		String auxCad2 = "";
		String auxCad3 = "";

		Vector vecFilas2 = null, vecColumnas = null;

		vecFilas2 = lineaCredito.getDetalleInicial(folio);
		if (vecFilas2.size()>0){
			vecColumnas = (Vector)vecFilas2.get(0);
			if(peticion.equals("epo")){
				auxCad = (String)vecColumnas.get(3);
				jsonObj.put("auxCad", auxCad);
			}
			if(peticion.equals("if")){
				auxCad = (String)vecColumnas.get(0);
				auxCad2 = (String)vecColumnas.get(1);
				auxCad3 = (String)vecColumnas.get(2);
				jsonObj.put("auxCad", auxCad);
				jsonObj.put("auxCad2", auxCad2);
				jsonObj.put("auxCad3", auxCad3);
			}
		}
		Vector vecFilas = lineaCredito.getMontoAutFechaVto(iNoCliente,ic_epo,ic_pyme,folio);
		String auxLink	= "",	minimo = "0", vencimiento="";
		if ("R".equals(solic)){
			auxLink = (String)vecFilas.get(0);
			String minVence = (String)vecFilas.get(1);
			jsonObj.put("auxLink", auxLink);
			jsonObj.put("minVence", minVence);
		}else if("A".equals(solic)){
			minimo 		= (String)vecFilas.get(0);
			vencimiento = (String)vecFilas.get(1);
			jsonObj.put("minimo", montoMinimo);
			jsonObj.put("vencimiento", vencimiento);
		}
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoEPODist") ) {

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveIf(iNoCliente);
	if(strPerfil.equals("IF FACT RECURSO")){
		cat.setTipoCredito("F");
	}else{
		cat.setTipoCredito("C");
	}
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoDistribuidor")){

	String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");

	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		if(!strPerfil.equals("IF FACT RECURSO")){
			cat.setTipoCredito("C");
		}
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}

}else if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoTipoCobroDist")){

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

	String ic_epo	=	(request.getParameter("epoDM")!=null)?request.getParameter("epoDM"):"";

	String rs_epo_tipo_cobro_int	=	"1,2";

	if(!"".equals(ic_epo))	{
		String valor = cargaDocto.getEpoTipoCobroInt_b(ic_epo);
		if(valor.equals("E")){
			rs_epo_tipo_cobro_int="1,2,3";
		}
	}

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_cobro_interes");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_tipo_cobro_interes");
	cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoBanco") ) {

	Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);
	Vector vecColumnas = null;
	Vector vecFilas = lineadm.getComboBanco();

	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	for (int i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector)vecFilas.get(i);
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave((String)vecColumnas.get(0));
		elementoCatalogo.setDescripcion((String)vecColumnas.get(1));
		coleccionElementos.add(elementoCatalogo);
	}
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar = "{\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString()+"}";

}else if (informacion.equals("TipoFolio")){

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

	String ic_epo	=	(request.getParameter("epoDM")!=null)?request.getParameter("epoDM"):"";
	String ic_pyme	=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String solic	=	(request.getParameter("solic")!=null)?request.getParameter("solic"):"";
	String moneda	=	(request.getParameter("moneda")!=null)?request.getParameter("moneda"):"";
	Vector vecFilas = null;
	Vector vecColumnas = null;
	String icFin="";
	List coleccionElementos = new ArrayList();
	if(!ic_epo.equals("") && !solic.equals("") && !moneda.equals("")){
		vecFilas = lineaCredito.getComboSolicitudRelacionada(iNoCliente,ic_epo,ic_pyme,solic,moneda,"4");
		if (vecFilas.size() > 0){
			for(int i=0;i<vecFilas.size();i++) {
				vecColumnas = (Vector)vecFilas.get(i);
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				icFin = (String)vecColumnas.get(0);
				elementoCatalogo.setClave(icFin);
				elementoCatalogo.setDescripcion(icFin);
				coleccionElementos.add(elementoCatalogo);
			}
			Iterator it = coleccionElementos.iterator();
			JSONArray jsonArr = new JSONArray();
			while(it.hasNext()) {
				Object obj = it.next();
				if (obj instanceof netropology.utilerias.ElementoCatalogo) {
					ElementoCatalogo ec = (ElementoCatalogo)obj;
					jsonArr.add(JSONObject.fromObject(ec));
				}
			}
			infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
		}else{
			infoRegresar = "{\"success\": true, \"total\": 0, \"registros\": [] }";
		}
	}

}else if (informacion.equals("validaLinea")){

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

	String ic_epo	=	(request.getParameter("epoDM")!=null)?request.getParameter("epoDM"):"";
	String ic_pyme	=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String solic	=	(request.getParameter("solic")!=null)?request.getParameter("solic"):"";
	String moneda	=	(request.getParameter("moneda")!=null)?request.getParameter("moneda"):"";
	String vencimiento	=	(request.getParameter("vencimiento")!=null)?request.getParameter("vencimiento"):"";

	JSONObject jsonObj = new JSONObject();
	boolean limpiar = false;
	try{
		if(strPerfil.equals("IF FACT RECURSO")){			
			lineaCredito.validaLinea(moneda,iNoCliente,ic_epo,ic_pyme,vencimiento,solic);
		}else{
			lineaCredito.validaLinea(moneda,iNoCliente,ic_epo,ic_pyme,vencimiento,solic);
		}
	}catch(NafinException ne){
		limpiar = true;
		String cad = ne.getMsgError();
		if(strPerfil.equals("IF FACT RECURSO")){
			if("Ya existe una línea para esa EPO".equals(cad) || "Ya existe una linea para esa EPO".equals(cad)){
				cad = "Ya existe una línea de crédito para la relación PYME-EPO";
			}
		}
		jsonObj.put("mensajeVal", cad);
	}
	jsonObj.put("limpiar", new Boolean(limpiar));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("capturaLinea")){

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

	JSONObject jsonObj = new JSONObject();
	String ic_epo	=	(request.getParameter("epoDM")!=null)?request.getParameter("epoDM"):"";
	String ic_pyme	=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String solic	=	(request.getParameter("tipoSol")!=null)?request.getParameter("tipoSol"):"";
	String moneda	=	(request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
	String vencimiento	=	(request.getParameter("vencimiento")!=null)?request.getParameter("vencimiento"):"";
	String ifBanco	=	(request.getParameter("ifBanco")!=null)?request.getParameter("ifBanco"):"";
	String tipo_cobro_int	=	(request.getParameter("tipoInt")!=null)?request.getParameter("tipoInt"):"";
	if(tipo_cobro_int.equals("")){tipo_cobro_int = "1";}
	
	Vector vecFilasval = null;
	Vector vecColumnas = null;

	vecFilasval = lineaCredito.getValores(ic_epo,ic_pyme,solic,moneda,tipo_cobro_int,ifBanco);
		
	String noNafinDist="",	nomPyme="",	nomMoneda="",	cobint="";
	if(vecFilasval != null && vecFilasval.size() > 0){
		vecColumnas = (Vector)vecFilasval.get(0);
		
		ic_epo = (String)vecColumnas.get(0);
		noNafinDist = (String)vecColumnas.get(1);
		nomPyme = (String)vecColumnas.get(2);
		solic = (String)vecColumnas.get(3);
		nomMoneda =(String)vecColumnas.get(4);
		cobint = (String)vecColumnas.get(5);
		ifBanco = (String)vecColumnas.get(6);
	}

	jsonObj.put("ic_epo", ic_epo);
	jsonObj.put("noNafinDist", noNafinDist);
	jsonObj.put("nomPyme", nomPyme);
	jsonObj.put("solic", solic);
	jsonObj.put("nomMoneda", nomMoneda);
	jsonObj.put("ic_moneda", moneda);
	jsonObj.put("cobint", cobint);
	jsonObj.put("ifBanco", ifBanco);

	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Confirma")){

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

	JSONObject jsonObj = new JSONObject();
	Vector vecFilas = null;
	Vector vecColumnas = null;

	String totMtoAuto	  	= (request.getParameter("totMtoAuto")==null)?"":request.getParameter("totMtoAuto");
	String totDocs	  		= (request.getParameter("totDocs")==null)?"":request.getParameter("totDocs");
	String totMtoAutoDol = (request.getParameter("totMtoAutoDol")==null)?"":request.getParameter("totMtoAutoDol");
	String totDocsDol	  	= (request.getParameter("totDocsDol")==null)?"":request.getParameter("totDocsDol");

	String noNafinDists[]			= request.getParameterValues("noNafinEs_aux");
	String folios[]					= request.getParameterValues("folios_aux");
	String montoAutos[] 				= request.getParameterValues("montoAutos_aux");
	String vencimientos[]			= request.getParameterValues("vencimientos_aux");
	String nCtaDists[] 				= request.getParameterValues("nCtaDist_aux");
	String nCtaIfs[]					= request.getParameterValues("nCtaIfs_aux");
	String ifBancos[] 				= request.getParameterValues("ifBancos");

	String ic_epos_aux[]				= request.getParameterValues("eposDM_aux");
	String ic_pymes_aux[] 			= request.getParameterValues("ic_pymes_aux");
	String solics_aux[] 				= request.getParameterValues("solics_aux");
	String monedas_aux[] 			= request.getParameterValues("monedas_aux");
	String tipo_cobro_ints_aux[]	= request.getParameterValues("cobints_aux");
	String ifBancos_aux[] 			= request.getParameterValues("ifBancos_aux");
	String nPlazoMax[]				= request.getParameterValues("nPlazoMax");
	String nCtaBancaria[]			= request.getParameterValues("nCtaBancaria");

	if(strPerfil.equals("IF FACT RECURSO")){	//	Se sobrecarga erl metodo para guardar lineas con este perfil de factoraje con recurso
		vecFilas = lineaCredito.setLineasCredito(iNoUsuario,strNombreUsuario, iNoCliente,totDocs,totMtoAuto,totDocsDol,totMtoAutoDol,ic_epos_aux,ic_pymes_aux,solics_aux,monedas_aux,montoAutos,vencimientos,tipo_cobro_ints_aux,nPlazoMax, nCtaBancaria);
	}else{
		vecFilas = lineaCredito.setLineasCredito(iNoUsuario,iNoCliente,totDocs,totMtoAuto,totDocsDol,totMtoAutoDol,ic_epos_aux,ic_pymes_aux,solics_aux,folios,monedas_aux,montoAutos,vencimientos,tipo_cobro_ints_aux,nCtaDists,ifBancos_aux,nCtaIfs);
	}

	vecColumnas = (Vector)vecFilas.get(0);
	String cc_acuse 	= (String)vecColumnas.get(0);
	String acuseForm 	= (String)vecColumnas.get(1);
	String fecha 		= (String)vecColumnas.get(2);
	String hora 		= (String)vecColumnas.get(3);
	String usuario		= iNoUsuario + " " + strNombreUsuario;

	jsonObj.put("_acuse", acuseForm);
	jsonObj.put("fechaCarga", fecha);
	jsonObj.put("horaCarga", hora);
	jsonObj.put("usuario", usuario);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>
