<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*, java.io.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%

JSONObject jsonObj = new JSONObject();

try {

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;

	String distribuidor 		= null;
	String icDocumento 		= null;
	String fechaEmision 		= null;
	String fechaVencCredito = null;
	String epo 					= null;
	double valorTasaInt 		= 0;
	String plazoCredito 		= null;
	String moneda 				= null;
	double monto 				= 0;
	double montoCredito 		= 0;
	double montoTasaInt 		= 0;
	String fechaSeleccion 	= null;

	contenidoArchivo.append(
		"Nombre EPO,"  +
		"Número de documento, "  +
		"Fecha Documento,"  +
		"Fecha de vencimiento,  "  +
		"Nombre Cliente,"  +
		"Tasa,"  +
		"Plazo(días),"  +
		"Moneda,"  +
		"Tipo de Factoraje,"  +
		"Monto Documento,"  +
		"Porcentaje de Descuento,  "  +
		"Recursos en Garantía,"  +
		"Monto a Descontar, "  +
		"Monto Interés,"  +
		"Monto a Operar,"  +
		"Beneficiario,"  +
		"Banco Beneficiario,"  +
		"Sucursal Beneficiario,"  +
		"Cuenta Beneficiaria,"  +
		"% Beneficiario,"  +
		"Importe a Recibir Beneficiario,"  +
		"Neto a recibir PyME,"  +
		"Fecha Registro operación\n" 
	);
	Registros rs = new Registros();
	if(!strPerfil.equals("IF FACT RECURSO")){
		rs = cargaDocto.getLayoutFactorajeVencimiento(iNoCliente);
	}else{
		rs = cargaDocto.getLayoutFactorajeVencimiento(iNoCliente, "F");
	}

	while(rs != null && rs.next()){
		distribuidor 		= (rs.getString("NOMBRE_DIST")			== null)?"":rs.getString("NOMBRE_DIST");
		icDocumento			= (rs.getString("IC_DOCUMENTO")			== null)?"":rs.getString("IC_DOCUMENTO");
		fechaEmision		= (rs.getString("DF_FECHA_EMISION")		== null)?"":rs.getString("DF_FECHA_EMISION");
		fechaVencCredito	= (rs.getString("FECHA_VENC_CREDITO")	== null)?"":rs.getString("FECHA_VENC_CREDITO");
		epo			 		= (rs.getString("NOMBRE_EPO")				== null)?"":rs.getString("NOMBRE_EPO");
		valorTasaInt		= Double.parseDouble(rs.getString("VALOR_TASA_INT"));
		plazoCredito		= (rs.getString("IG_PLAZO_CREDITO")		== null)?"":rs.getString("IG_PLAZO_CREDITO");
		moneda 				= (rs.getString("MONEDA")					== null)?"":rs.getString("MONEDA");
		monto 				= Double.parseDouble(rs.getString("FN_MONTO"));
		montoCredito		= Double.parseDouble(rs.getString("MONTO_CREDITO"));
		montoTasaInt		= Double.parseDouble(rs.getString("MONTO_TASA_INT"));
		fechaSeleccion		= (rs.getString("FECHA_SELECCION")		== null)?"":rs.getString("FECHA_SELECCION");

		// Suprimir comas
		epo					= epo.replaceAll(",","");
		distribuidor		= distribuidor.replaceAll(",","");
		moneda				= moneda.replaceAll(",","");

		// Suprimir comillas dobles
		epo					= epo.replaceAll("\"","");
		distribuidor		= distribuidor.replaceAll("\"","");
		moneda				= moneda.replaceAll("\"","");

		 // 1. Nombre EPO -> Distribuidor
		 contenidoArchivo.append(distribuidor+",");
		 // 2. Número de documento -> Numero de documento Final
		 contenidoArchivo.append(icDocumento+",");
		 // 3. Fecha Documento -> Fecha de Emisión
		 contenidoArchivo.append(fechaEmision+",");
		 // 4. Fecha de vencimiento -> Fecha de Vencimiento (la 2da. Fecha de vencimiento Mostrada en el layout)
		 contenidoArchivo.append(fechaVencCredito+",");
		 // 5. Nombre Cliente -> EPO
		 contenidoArchivo.append(epo+",");
		 // 6. Tasa -> Valor tasa de Interés
		 contenidoArchivo.append(valorTasaInt+"%,");
		 // 7. Plazo(días) -> Plazo
		 contenidoArchivo.append(plazoCredito+",");
		 // 8. Moneda -> Moneda
		 contenidoArchivo.append(moneda+",");
		 // 9. Tipo de Factoraje -> El dato contenido en este campo será fijo, siempre se mostrará como "Vencido"
		 contenidoArchivo.append("Vencido,");
		 // 10. Monto Documento -> Monto
		 contenidoArchivo.append(monto+",");
		 // 11. Porcentaje de Descuento -> El dato contenido en este campo será fijo, siempre se mostrará el valor "100%"
		 contenidoArchivo.append("100%,");
		 // 12. Recursos en Garantía -> El dato contenido en este campo será fijo, siempre se mostrará el valor  "0".
		 contenidoArchivo.append("0,");
		 // 13. Monto a Descontar -> Monto (2do Monto mostrado en el en el layout)
		 contenidoArchivo.append(montoCredito+",");
		 // 14. Monto Interés -> Monto de intereses
		 contenidoArchivo.append(montoTasaInt+",");
		 // 15. Monto a Operar -> Monto total de capital e interés
		 contenidoArchivo.append(montoCredito+montoTasaInt+",");
		 // 16. Beneficiario -> El dato contenido en este campo será fijo, siempre se mostrará  el valor "0".
		 contenidoArchivo.append("0,");
		 // 17. Banco Beneficiario -> El dato contenido en este campo será fijo, siempre se mostrará  el valor "0".
		 contenidoArchivo.append("0,");
		 // 18. Sucursal Beneficiario -> El dato contenido en este campo será fijo, siempre se mostrará  el valor "0".
		 contenidoArchivo.append("0,");
		 // 19. Cuenta Beneficiaria -> El dato contenido en este campo será fijo, siempre se mostrará  el valor "0".
		 contenidoArchivo.append("0,");
		 // 20. % Beneficiario -> El dato contenido en este campo será fijo, siempre se mostrará  el valor "0".
		 contenidoArchivo.append("0,");
		 // 21. Importe a Recibir Beneficiario -> El dato contenido en este campo será fijo, siempre se mostrará  el valor "0".
		 contenidoArchivo.append("0,");
		 // 22. Neto a recibir PyME -> El dato contenido en este campo será fijo, siempre se mostrará  el valor "0".
		 contenidoArchivo.append("0,");
		 // 23. Fecha Registro operación -> Fecha de operación distribuidor
		 contenidoArchivo.append(fechaSeleccion+",");

		 contenidoArchivo.append("\n");

	}

	if (!escribeArchivo(contenidoArchivo.toString(), strDirectorioTemp+"Layout Factoraje Vencimiento.csv")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivo = "Layout Factoraje Vencimiento.csv";
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>

<%!
		private boolean escribeArchivo(String contenido, String path) {
		String linea="";
		try {
			DataOutputStream dos=new DataOutputStream(new FileOutputStream(new File(path)));
			StringTokenizer st=new StringTokenizer(contenido,"\n");
			if(st.countTokens() >=0) {
				while(st.hasMoreTokens()) {
					linea=st.nextToken().trim();
					dos.writeBytes(linea +"\r\n");
					//System.out.println(linea);
				}
			} else {
				dos.writeBytes(contenido);
			}
			dos.close();
			return true;
		} //try
		catch(IOException ioe) {
			System.out.println("Error de IO: "+ioe); return false;}
		}
%>