<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.math.*, com.netro.anticipos.*,
	com.netro.exception.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%

JSONObject jsonObj = new JSONObject();

String _acuse		= (request.getParameter("_acuse")==null)?"":request.getParameter("_acuse");
String acuse		= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String fechaHoy	= (request.getParameter("fechaHoy")==null)?"":request.getParameter("fechaHoy");
String horaCarga	= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");

String ic_epo		= (request.getParameter("_ic_epo")==null)?"":request.getParameter("_ic_epo");
String ic_pyme		= (request.getParameter("_ic_pyme")==null)?"":request.getParameter("_ic_pyme");
String ic_moneda	= (request.getParameter("_ic_moneda")==null)?"1":request.getParameter("_ic_moneda");
String tipo_tasa	= (request.getParameter("_tipo_tasa")==null)?"P":request.getParameter("_tipo_tasa");
String plazo		= (request.getParameter("_plazo")==null)?"":request.getParameter("_plazo");
String tipoCredito			= (request.getParameter("_tipo_credito")==null)?"":request.getParameter("_tipo_credito");

try {

	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
	
	CapturaTasas beanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

	pdfDoc.setTable(2, 50);
	pdfDoc.setCell("Recibo Número:"+_acuse,"celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de Acuse","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(acuse,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Fecha de Autorización","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Hora de Autorización","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(horaCarga,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Usuario de Captura","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(strNombreUsuario,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("\n","formas",ComunesPDF.CENTER,2);
	pdfDoc.addTable();

	Vector lovTasas = beanTasas.ovgetTasasPreferNego("4",tipo_tasa,iNoCliente,ic_epo,ic_moneda,ic_pyme, plazo, tipoCredito);
	for (int i=0; i<lovTasas.size(); i++) {
			Vector lovDatosTasa = (Vector)lovTasas.get(i);
			String lsMoneda	= lovDatosTasa.get(0)==null?"":lovDatosTasa.get(0).toString();
			String lsTipoTasa	= lovDatosTasa.get(1)==null?"":lovDatosTasa.get(1).toString();
			String lsCveTasa	= lovDatosTasa.get(2)==null?"":lovDatosTasa.get(2).toString();
			String lsPlazo		= lovDatosTasa.get(3)==null?"":lovDatosTasa.get(3).toString();
			String lsValor		= lovDatosTasa.get(4)==null?"":lovDatosTasa.get(4).toString();
			String lsRelMat	= lovDatosTasa.get(5)==null?"":lovDatosTasa.get(5).toString();
			String lsPuntos	= lovDatosTasa.get(6)==null?"":lovDatosTasa.get(6).toString();
			String lsTasaPiso	= lovDatosTasa.get(7)==null?"":lovDatosTasa.get(7).toString();
			String lsFecha		= lovDatosTasa.get(8)==null?"":lovDatosTasa.get(8).toString();
			String lsEpo		= lovDatosTasa.get(9)==null?"":lovDatosTasa.get(9).toString();
			String lsPyme		= lovDatosTasa.get(10)==null?"":lovDatosTasa.get(10).toString();
			String lsTasaFinal= lovDatosTasa.get(11)==null?"":lovDatosTasa.get(11).toString();
			String lsFechaNego= lovDatosTasa.get(12)==null?"":lovDatosTasa.get(12).toString();

			String lsPuntosPref	= lovDatosTasa.get(15)==null?"":lovDatosTasa.get(15).toString();
			String lsTasaNego = "";
			String lsReferencia = "";
			
			lsTipoTasa		= lsTipoTasa+" ("+lsValor+") "+lsRelMat+" "+lsPuntos;
			double tasafinal = 0;
			if("P".equals(tipo_tasa)) {
				lsReferencia	= "Preferencial";
				lsTipoTasa = lsTipoTasa +" - "+lsPuntosPref;
				lsTasaNego 		= Comunes.formatoDecimal(lsValor,4);//?????????????????????????????????????????
			}
			else if("N".equals(tipo_tasa)) {
				lsReferencia	= "Negociada";
				lsTasaNego 		= Comunes.formatoDecimal(lsTasaFinal,4);
				lsTasaFinal		= lsTasaPiso;

			} else {
				lsReferencia	= "Base";
				lsTasaFinal		= lsTasaPiso;
			}
		if(i==0) {
			pdfDoc.setTable(7, 100);
			pdfDoc.setCell("EPO relacionada","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre de la PyME","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Tasa","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Valor","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Valor Tasa","celda01",ComunesPDF.CENTER);
		}
		pdfDoc.setCell(lsEpo,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsPyme,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsReferencia,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsTipoTasa,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsPlazo,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(Comunes.formatoDecimal(lsTasaFinal,4,false),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsTasaNego,"formas",ComunesPDF.CENTER);
	}
	pdfDoc.addTable();
	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>