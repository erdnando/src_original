<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.distribuidores.*,
		com.netro.descuento.*,
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	JSONObject resultado = new JSONObject();

if (informacion.equals("valoresIniciales")  ) {

	String fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	resultado.put("success", new Boolean(true));	
	resultado.put("fechaHoy", fechaHoy);
	resultado.put("strTipoUsuario", strTipoUsuario);
	infoRegresar = resultado.toString();	

}else if (informacion.equals("catalogoEpo")  ) {
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
   cat.setCampoClave("ic_epo");
   cat.setCampoDescripcion("cg_razon_social"); 
   cat.setOrden("2"); 
	infoRegresar = cat.getJSONElementos();	
	
}else if (informacion.equals("catalogoDist")  ) {
	String ic_epo = request.getParameter("ic_epo")==null?"":request.getParameter("ic_epo");

	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
   cat.setCampoClave("cp.ic_pyme");
   cat.setCampoDescripcion("cp.cg_razon_social"); 
	cat.setClaveEpo(ic_epo);
   cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();	
	
}else if (informacion.equals("consultaDatos") || informacion.equals("ArchivoCSV") || informacion.equals("ConsultarPreAcuse") || informacion.equals("ArchivoPDF")) {
	String tit = "";
	String consulta = "";
	StringBuffer texto = new StringBuffer();
	StringBuffer texto2 = new StringBuffer();
	String operacion = request.getParameter("operacion")==null?"":request.getParameter("operacion"),
			 pantalla = request.getParameter("pantalla")==null?"":request.getParameter("pantalla"),
			 ic_epo = request.getParameter("ic_epo")==null?"":request.getParameter("ic_epo"),
			 nae = request.getParameter("ic_nae_dis")==null?"":request.getParameter("ic_nae_dis"),
			 fec_ini = request.getParameter("fechaIni")==null?"":request.getParameter("fechaIni"),
			 fec_fin = request.getParameter("fechaFin")==null?"":request.getParameter("fechaFin");
			 String pant = (request.getParameter("pantalla") != null) ? request.getParameter("pantalla") : "",
			 cve_if =iNoCliente;
			 String docs = (request.getParameter("doctos") != null) ? request.getParameter("doctos") : "";
			 String cmp_ref = (request.getParameter("ref") != null) ? request.getParameter("ref") : "";
	
	LiberaLimNaf paginador = new LiberaLimNaf();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
	
	paginador.setCve_epo(ic_epo);
	paginador.setNo_nae(nae);
	paginador.setCve_if(cve_if);
	paginador.setFec_ven_ini(fec_ini);
	paginador.setFec_ven_fin(fec_fin);
	paginador.set_Pantalla(pantalla);
	paginador.setDocumentos(docs);
	paginador.setReferencia(cmp_ref);
	
	if(informacion.equals("consultaDatos") || informacion.equals("ConsultarPreAcuse") ){
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			
			ParametrosDist paramDist = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
			
			tit = paramDist.getTiposCredito(nae, ic_epo);
			
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
		
		if (pant.equals("PantallaAcuse") )
			texto.append("<b> Se realizó la liberación de los vencimientos  </b>");
			texto2.append("<b> Confirme la aplicación de la liberación de los vencimientos </b>");
		
		resultado = JSONObject.fromObject(consulta);	
		resultado.put("mensaje", texto.toString());
		resultado.put("mensaje2", texto2.toString());
		resultado.put("pantalla", pant);
		resultado.put("titulo", tit);
		resultado.put("strTipoUsuario", strTipoUsuario);
		infoRegresar = resultado.toString();
		
	}else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	

			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = resultado.toString();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if( informacion.equals("ArchivoPDF") ){
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = resultado.toString();
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}	
		
	}
	
}else if (informacion.equals("Totales")  )  {
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	String consulta = "";
	for(int t =0; t<2; t++) {		
		datos = new HashMap();		
		if(t==0){ 	
			datos.put("IC_MONEDA", "1");
			datos.put("TOT_MONEDA", "Nacional");		
			datos.put("MONTO_TOT", "0" );													
		}		
		if(t==1){ 		
			datos.put("IC_MONEDA", "54");
			datos.put("TOT_MONEDA", "Dólares ");		
			datos.put("MONTO_TOT", "0" );											
		}		
		registros.add(datos);
	}
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consulta);
	infoRegresar = resultado.toString();	

}else if (informacion.equals("LiberarVencimiento")  )  {

	String folioCert = "",  _acuse = "", horaHoy ="", mensajeAutentificacion ="";
	String mensaje2 = "";
	String pkcs7 = request.getParameter("pkcs7");	
	String [] documentos		= request.getParameterValues("documentos");
	String [] valores		= request.getParameterValues("valores");
	String [] monto		= request.getParameterValues("monto");
	String externContent = request.getParameter("textoFirmar");
	String doctoSelec = (request.getParameter("doctoSelec") != null) ? request.getParameter("doctoSelec") : "";
	String fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String fechaIni = (request.getParameter("fechaIni") != null) ? request.getParameter("fechaIni") : "";
	String fechaFin = (request.getParameter("fechaFin") != null) ? request.getParameter("fechaFin") : "";
	StringBuffer lisDocumentos 	= new StringBuffer();
	
	if(documentos !=null){
		for (int i=0; i<=documentos.length-1; i++) {
			lisDocumentos.append(documentos[i]+",");
		}
		lisDocumentos.deleteCharAt(lisDocumentos.length()-1);	
		doctoSelec =lisDocumentos.toString();
	}
	
	LibLimDistribuidores limitesDist = new LibLimDistribuidores();
	
	char getReceipt = 'Y';
	Seguridad s = new Seguridad();
	Acuse acuse = null;		
	horaHoy= (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
	// convertimos array a String 
	StringBuffer  valoresT = new StringBuffer();
	for(int i=0;i<valores.length;i++) {			
		valoresT.append(valores[i]);			
	}	
	
	try{	
		if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			acuse = new Acuse(Acuse.ACUSE_EPO,"1");
			folioCert = acuse.toString();		
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
				_acuse = s.getAcuse();
				
				//  liberar documentos 
				if(limitesDist.liberarLimitesDist(documentos, monto, iNoUsuario+"-"+strNombreUsuario, _acuse )){
					mensajeAutentificacion = "<b>La autentificación se llevo a cabo con exito  <b>Recibo:"+_acuse+"</b>";
					mensaje2 = "<b>Se realizó la liberación de los vencimientos  </b>";
				}
				else{
					mensajeAutentificacion = "<b>La autentificación no se llevo a cabo </b>";
					_acuse="";
				}
				
			
			}else { //autenticación fallida
				mensajeAutentificacion = "<b>La autentificación no se llevo a cabo </b>";
			}
		} else { //autenticación fallida
			mensajeAutentificacion = "<b>La autentificación no se llevo a cabo </b>";
		}	
		
		resultado.put("success", new Boolean(true));
		resultado.put("mensajeAutentificacion",mensajeAutentificacion);
		resultado.put("msjleyenda",mensaje2);
		if(acuse !=null)resultado.put("acuse",acuse.formatear());	
		if(acuse ==null)resultado.put("acuse",acuse);	
		resultado.put("recibo",_acuse);
		if(acuse ==null)resultado.put("acuse2",acuse.toString());
		resultado.put("fecha",fechaHoy);
		resultado.put("hora",horaHoy);
		resultado.put("usuario",iNoUsuario+" - "+strNombreUsuario);
		resultado.put("doctoSelec",doctoSelec);	
		infoRegresar  = resultado.toString();
		
	}catch(Exception e){
		out.println("Error en jsp");
		System.out.println("error:::"+e);
		e.printStackTrace();
		resultado.put("mensajeAutentificacion",mensajeAutentificacion);
	}/*finally{
		System.out.println(" liberar documentos  :::");
	}*/
	
			
}

%>
<%=infoRegresar%>
