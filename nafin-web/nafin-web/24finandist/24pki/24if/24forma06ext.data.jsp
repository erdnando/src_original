 <%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.sql.*, com.netro.exception.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.anticipos.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoEPODist") ) {
	
		String tipoCredito = (request.getParameter("tipoCredito")==null)?"":request.getParameter("tipoCredito");//MOD +(ALL)

		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClaveIf(iNoCliente);
		cat.setTipoCredito(tipoCredito);// MOD+(cat.setTipoCredito(tipoCredito)) -(cat.setTipoCredito("D"))
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoDistribuidor")){

	String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String tipoCredito = (request.getParameter("tipoCredito")==null)?"":request.getParameter("tipoCredito");//MOD +(ALL)

	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		//cat.setTipoCredito(tipoCredito);// MOD+(cat.setTipoCredito(tipoCredito)) -(cat.setTipoCredito("D"))
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}else{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
	
}else if (informacion.equals("CatalogoPlazo")){

	CapturaTasas beanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	String ic_epo		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String ic_moneda	= (request.getParameter("ic_moneda")==null)?"1":request.getParameter("ic_moneda");
	String tipo_tasa	= (request.getParameter("tipo_tasa")==null)?"P":request.getParameter("tipo_tasa");
	String tipo_credito= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");

	if(!"".equals(ic_epo)||!"".equals(ic_pyme)){
		List coleccionElementos = new ArrayList();
		Vector vecPlazo = new Vector();
		
		if("C".equals(tipo_credito) ){//modalidad 2 getCatalogoPlazoPyme
			if("P".equals(tipo_tasa)){
				vecPlazo = beanTasas.getCatalogoPlazoPyme(4,ic_pyme,iNoCliente,ic_moneda, "");
		
				for(int i=0;i<vecPlazo.size();i++){
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					Vector vecColumnas=	(Vector)vecPlazo.get(i);
					String icPlazo		=	(String)vecColumnas.get(1);
					String descPlazo	=	(String)vecColumnas.get(0);
					elementoCatalogo.setClave(icPlazo);
					elementoCatalogo.setDescripcion(descPlazo);
					coleccionElementos.add(elementoCatalogo);
				}
			}else{
	
				vecPlazo = beanTasas.getComboPlazo(4,ic_epo);
				for(int i=0;i<vecPlazo.size();i++){
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					Vector vecColumnas= (Vector)vecPlazo.get(i);
					String icPlazo		= (String)vecColumnas.get(0);
					String descPlazo	= (String)vecColumnas.get(1);
					elementoCatalogo.setClave(icPlazo);
					elementoCatalogo.setDescripcion(descPlazo);
					coleccionElementos.add(elementoCatalogo);
				}
			}
			
		}else{
			if("P".equals(tipo_tasa)){
				vecPlazo = beanTasas.getCatalogoPlazo(4,ic_epo,iNoCliente,ic_moneda, "");
		
				for(int i=0;i<vecPlazo.size();i++){
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					Vector vecColumnas=	(Vector)vecPlazo.get(i);
					String icPlazo		=	(String)vecColumnas.get(1);
					String descPlazo	=	(String)vecColumnas.get(0);
					elementoCatalogo.setClave(icPlazo);
					elementoCatalogo.setDescripcion(descPlazo);
					coleccionElementos.add(elementoCatalogo);
				}
			}else{
	
				vecPlazo = beanTasas.getComboPlazo(4,ic_epo);
				for(int i=0;i<vecPlazo.size();i++){
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					Vector vecColumnas= (Vector)vecPlazo.get(i);
					String icPlazo		= (String)vecColumnas.get(0);
					String descPlazo	= (String)vecColumnas.get(1);
					elementoCatalogo.setClave(icPlazo);
					elementoCatalogo.setDescripcion(descPlazo);
					coleccionElementos.add(elementoCatalogo);
				}
			}
		}
		

		Iterator it = coleccionElementos.iterator();
		JSONArray jsonArr = new JSONArray();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}else{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("Procesar")){

	CapturaTasas beanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	String ic_epo		= (request.getParameter("_ic_epo")==null)?"":request.getParameter("_ic_epo");
	String ic_pyme		= (request.getParameter("_ic_pyme")==null)?"":request.getParameter("_ic_pyme");
	String ic_moneda	= (request.getParameter("_ic_moneda")==null)?"1":request.getParameter("_ic_moneda");
	String tipo_tasa	= (request.getParameter("_tipo_tasa")==null)?"P":request.getParameter("_tipo_tasa");
	String plazo		= (request.getParameter("_plazo")==null)?"":request.getParameter("_plazo");
	String razonSocial= (request.getParameter("razonSocial")==null)?"":request.getParameter("razonSocial");
	
	String tipoCredito= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
	String valorTipoCredito = (request.getParameter("valorTipoCredito")==null)?"":request.getParameter("valorTipoCredito");

	String lsMoneda="", lsTipoTasa="", lsCveTasa="", lsPlazo="", lsValor="",	lsRelMat="", lsPuntos="", lsTasaPiso="",	lsTipoCred="",lsValorTasaNeg="";
	String nombrePyme="", puntospref ="", tasaAplicar_1 ="";///MOD FODEA-005-2014 nuevo registro
	StringBuffer lsTexto = new StringBuffer();
	double tasaAplicar = 0;
	JSONObject jsonObj = new JSONObject();
	Vector lovTasas = null;
	List regs  = new ArrayList();
	String valoresAnteriores="Tipo= PREFERENCIAL, IC_EPO="+ic_epo+" , IC_PYME="+ic_pyme+", Tasa Negociada=10";
	if("P".equals(tipo_tasa)){
		lsTexto.append("Tipo de Credito|EPO Relacionada|Tipo de Tasa|Valor|Rel Mat");
		lovTasas = beanTasas.ovgetTasasxEpo1(4,ic_epo,iNoCliente,ic_moneda, plazo,ic_pyme,tipoCredito);
		for(int i=0;i<lovTasas.size();i++){
			Vector lovDatosTasa = (Vector)lovTasas.get(i);
			HashMap hash = new HashMap();
			lsCveTasa	= lovDatosTasa.get(2).toString();
			lsMoneda	= lovDatosTasa.get(0).toString();
			lsTipoTasa	= lovDatosTasa.get(1).toString();
			lsPlazo = lovDatosTasa.get(3).toString();
			lsValor = lovDatosTasa.get(4).toString();
			lsRelMat = lovDatosTasa.get(5).toString();
			lsPuntos = lovDatosTasa.get(6).toString();
			lsTasaPiso = lovDatosTasa.get(7).toString();
			nombrePyme = lovDatosTasa.get(11).toString();//FODEA-005-2014 registro nuevo
			puntospref = lovDatosTasa.get(12).toString();//FODEA-005-2014 registro nuevo
			lsTipoCred = valorTipoCredito;//MOD +(lsTipoCred = valorTipoCredito) -(lsTipoCred = "Modalidad 1 (Riesgo Empresa de Primer Orden )")
			hash.put("CLAVE_TASA",lsCveTasa);
			hash.put("TIPO_CREDITO",lsTipoCred);
			hash.put("NOMBRE_EPO",razonSocial);
			hash.put("TASA_INTERES",lsTipoTasa);
			hash.put("PLAZO",lsPlazo);
			hash.put("VALOR",lsTasaPiso); 
			//hash.put("REL_MAT",lsRelMat);
			hash.put("REL_MAT","-");
			hash.put("PUNTOS_ADD","");
			hash.put("TASA_APLICAR","");
			hash.put("NOMBRE_PYME",nombrePyme);
			hash.put("PUNTOS_ADD",puntospref );
			
			
			regs.add(hash);
			lsTexto.append("\n"+lsTipoCred+"|"+razonSocial+"|"+lsTipoTasa+"|"+lsTasaPiso+"|-");
		}
	}else if("N".equals(tipo_tasa)){
		lsTexto.append("Tipo de Credito|EPO Relacionada|Tipo de Tasa|Valor|Fecha");
		lovTasas	=	beanTasas.ovgetTasasxEpo(4,ic_epo,iNoCliente,ic_moneda,plazo,ic_pyme,tipoCredito);
		String fechaHoy	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

		for(int i=0;i<lovTasas.size();i++){
			Vector lovDatosTasa = (Vector)lovTasas.get(i);
			HashMap hash = new HashMap();
			lsCveTasa	= lovDatosTasa.get(2).toString();
			lsMoneda	= lovDatosTasa.get(0).toString();
			lsTipoTasa	= lovDatosTasa.get(1).toString();
			lsPlazo = lovDatosTasa.get(3).toString();
			lsValor = lovDatosTasa.get(4).toString();
			lsRelMat = lovDatosTasa.get(5).toString();
			lsPuntos = lovDatosTasa.get(6).toString();
			lsTasaPiso = lovDatosTasa.get(7).toString();
			lsValorTasaNeg = lovDatosTasa.get(9).toString();
			nombrePyme = lovDatosTasa.get(10).toString();//FODEA-005-2014 registro nuevo
			lsTipoCred = valorTipoCredito;//MOD +(lsTipoCred = valorTipoCredito) -(lsTipoCred = "Modalidad 1 (Riesgo Empresa de Primer Orden )")
			tasaAplicar_1 = lovDatosTasa.get(11).toString();//FODEA-005-2014 registro nuevo
			hash.put("CLAVE_TASA",lsCveTasa);
			hash.put("TIPO_CREDITO",lsTipoCred);
			hash.put("NOMBRE_EPO",razonSocial);
			hash.put("TASA_INTERES",lsTipoTasa);
			hash.put("PLAZO","");
			hash.put("VALOR",lsTasaPiso);
			hash.put("FECHA",fechaHoy);
			hash.put("REL_MAT","");
			hash.put("PUNTOS_ADD","");
			hash.put("TASA_APLICAR",tasaAplicar_1);			
			hash.put("NOMBRE_PYME",nombrePyme);			
			regs.add(hash);
			lsTexto.append("\n"+lsTipoCred+"|"+razonSocial+"|"+lsTipoTasa+"|"+lsTasaPiso+"|"+fechaHoy);
		}
	}
	jsonObj.put("regs", regs);
	jsonObj.put("lsTexto", lsTexto.toString());
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Save")){

	JSONObject jsonObj = new JSONObject();
	CapturaTasas beanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	String ic_epo		= (request.getParameter("_ic_epo")==null)?"":request.getParameter("_ic_epo");
	String ic_pyme		= (request.getParameter("_ic_pyme")==null)?"":request.getParameter("_ic_pyme");
	String ic_moneda	= (request.getParameter("_ic_moneda")==null)?"1":request.getParameter("_ic_moneda");
	String tipo_tasa	= (request.getParameter("_tipo_tasa")==null)?"P":request.getParameter("_tipo_tasa");
	String plazo		= (request.getParameter("_plazo")==null)?"":request.getParameter("_plazo");
	String puntos			= (request.getParameter("puntos")==null)?"":request.getParameter("puntos");
	String valor_tasa_neg= (request.getParameter("valor_tasa_neg")==null)?"":request.getParameter("valor_tasa_neg");
	String ic_tasa			= (request.getParameter("ic_tasa")==null)?"":request.getParameter("ic_tasa");
	
	String tipoCredito			= (request.getParameter("_tipo_credito")==null)?"":request.getParameter("_tipo_credito");

	String pkcs7 = request.getParameter("Pkcs7");
	String externContent = request.getParameter("TextoFirmado");
	char getReceipt = 'Y';
	String folioCert = "";
	String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
	Seguridad s = new Seguridad();
	Acuse		acuse	= null;
	String	_acuse	= "";
	AccesoDB con = new AccesoDB();
	
	try{
		con.conexionDB();
		acuse	= new Acuse(Acuse.ACUSE_IF,"4","com",con);
	}catch(Exception e){
		throw new NafinException("SIST001");
	}finally{
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}

	boolean tranza = true;

	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		folioCert = acuse.toString();	
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
			_acuse = s.getAcuse();

			if("P".equals(tipo_tasa)){
				if(tipoCredito.equals("D")){/*Guardar en Bitacora*/
				}
				beanTasas.guardaTasaPreferencial(ic_epo,ic_pyme,iNoCliente,ic_moneda,tipo_tasa,puntos,4, acuse.toString(), iNoUsuario, _acuse,plazo,tipoCredito);
				
				
			}else if("N".equals(tipo_tasa)){
				beanTasas.guardaTasaNegociada(ic_epo,ic_pyme,iNoCliente,ic_moneda,tipo_tasa,valor_tasa_neg,ic_tasa,4, acuse.toString(), iNoUsuario, _acuse,tipoCredito);
			}

			jsonObj.put("msg", "La autentificación se llevo a cabo con éxito");
			jsonObj.put("_acuse", _acuse);
			jsonObj.put("acuse", acuse.formatear());
			jsonObj.put("fechaCarga", fechaHoy);
			jsonObj.put("horaCarga", horaActual);
			jsonObj.put("usuario", strNombreUsuario);

			List registros  = new ArrayList();
			Vector lovTasas = beanTasas.ovgetTasasPreferNego("4",tipo_tasa,iNoCliente,ic_epo,ic_moneda,ic_pyme, plazo,tipoCredito);
			for (int i=0; i<lovTasas.size(); i++) {
				HashMap hash = new HashMap();
					Vector lovDatosTasa = (Vector)lovTasas.get(i);
					String lsMoneda		= lovDatosTasa.get(0)==null?"":lovDatosTasa.get(0).toString();
					String lsTipoTasa	= lovDatosTasa.get(1)==null?"":lovDatosTasa.get(1).toString();
					String lsCveTasa	= lovDatosTasa.get(2)==null?"":lovDatosTasa.get(2).toString();
					String lsPlazo		= lovDatosTasa.get(3)==null?"":lovDatosTasa.get(3).toString();
					String lsValor		= lovDatosTasa.get(4)==null?"":lovDatosTasa.get(4).toString();
					String lsRelMat		= lovDatosTasa.get(5)==null?"":lovDatosTasa.get(5).toString();
					String lsPuntos		= lovDatosTasa.get(6)==null?"":lovDatosTasa.get(6).toString();
					String lsTasaPiso	= lovDatosTasa.get(7)==null?"":lovDatosTasa.get(7).toString();
					
					String lsFecha		= lovDatosTasa.get(8)==null?"":lovDatosTasa.get(8).toString();
					String lsEpo		= lovDatosTasa.get(9)==null?"":lovDatosTasa.get(9).toString();
					String lsPyme		= lovDatosTasa.get(10)==null?"":lovDatosTasa.get(10).toString();
					String lsTasaFinal 	= lovDatosTasa.get(11)==null?"":lovDatosTasa.get(11).toString();
					String lsFechaNego 	= lovDatosTasa.get(12)==null?"":lovDatosTasa.get(12).toString();
					
					String lsPuntosPref	= lovDatosTasa.get(15)==null?"":lovDatosTasa.get(15).toString();
					String lsTasaNego = "";
					String lsReferencia = "";
					
					lsTipoTasa		= lsTipoTasa+" ("+lsValor+") "+lsRelMat+" "+lsPuntos;
					double tasafinal = 0;
					if("P".equals(tipo_tasa)) {
						lsReferencia	= "Preferencial";
						lsTipoTasa = lsTipoTasa +" - "+lsPuntosPref;
						lsTasaNego 		= Comunes.formatoDecimal(lsValor,4);//??????????????????????????????????????????
					}
					else if("N".equals(tipo_tasa)) {
						lsReferencia	= "Negociada";
						lsTasaNego 		= Comunes.formatoDecimal(lsTasaFinal,4);
						lsTasaFinal		= lsTasaPiso;
		
					} else {
						lsReferencia	= "Base";
						lsTasaFinal		= lsTasaPiso;
					}
					hash.put("NOMBRE_EPO",lsEpo);
					hash.put("NOMBRE_PYME",lsPyme);
					hash.put("REFERENCIA",lsReferencia);
					hash.put("TASA_INTERES",lsTipoTasa);
					hash.put("PLAZO",lsPlazo);
					hash.put("VALOR",lsTasaFinal);
					hash.put("TASA_APLICAR",lsTasaNego);
					registros.add(hash);
			}
			jsonObj.put("registros", registros);
		}else{
			tranza = false;
			jsonObj.put("msg", "La autentificación no se llevó a cabo.<br>Proceso CANCELADO");
		}
	}else{
		tranza = false;
		jsonObj.put("msg", "La autentificación no se llevó a cabo.<br>Proceso CANCELADO");	
	}

	jsonObj.put("success", new Boolean(tranza));
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>