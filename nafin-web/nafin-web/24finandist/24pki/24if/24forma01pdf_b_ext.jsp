<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.sql.*, java.math.*, com.netro.descuento.*,
   netropology.utilerias.negocio.*,	com.netro.distribuidores.*,com.netro.exception.*, javax.naming.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%

JSONObject jsonObj = new JSONObject();
boolean SIN_COMAS = false;
String fechaHoy	= "";
String fechaActual	= "";
String HoraActual	= "";
String fechaCarga = "",	horaCarga =	"";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	HoraActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}

String	tipoArchivo			= (request.getParameter("tipoArchivo")==null)?"":request.getParameter("tipoArchivo");
String	ic_documentos[]	= request.getParameterValues("selecciona");
String	[]doctosPorEpo		= request.getParameterValues("doctosPorEpo");
String 	aux_acuse			= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String 	ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String 	hidTipoCredito		= (request.getParameter("hidTipoCredito")==null)?"":request.getParameter("hidTipoCredito");
String 	cadEpo				= (request.getParameter("cadEpo")==null)?"":request.getParameter("cadEpo");
String accion = (request.getParameter("accion") != null) ? request.getParameter("accion") : "";

//VARIABLES DE USO LOCAL
int i = 0;
Vector vecFilas		= null;
Vector vecColumnas	= null;

//DE DESPLIEGUE
//segunda tabla
String acuse="",	nombreEPO = "",	moneda = "",	icMoneda = "",	icDocumento = "",	igNumeroDocto = "",	fechaSolicitud = "",	distribuidor = "",	referencia = "";
String tipoConversion = "",	plazo = "",	fechaVto = "",	tipoPagoInteres = "",	monedaLinea = "";
double	monto					=	0;
double	tipoCambio			=	0;
double	montoValuado		=	0;
double	tasaInteres			=	0;
double	montoInteres		=	0;
double	porcDescuento		=	0;
double	montoDescuento		=	0;
double	totalMontoIntConv =	0;
//tercera tabla
String rs_numDist = "",	rs_cveTasa = "",	rs_tipoConver = "",	rs_tipoCamb = "",	rs_respInt = "",csTipoFondeo = "",tipoFinan = "";
String rs_tipoCob = "",	rs_referencia = "",	rs_campo1 = "",	rs_campo2 = "",	rs_campo3 = "",	rs_campo4 = "",	rs_campo5 = "",	
numerocuenta ="",tipoPago ="";

//totales
int		totalDoctosMN				= 0;
int 		totalDoctosUSD				= 0;
int 		totalDoctosUSDBot			= 0;
double	totalMontoMN				= 0;
double	totalMontoUSD				= 0;
double	totalMontoUSDBot			= 0;
double 	totalMontoValuadoMN		= 0;
double 	totalMontoValuadoUSD		= 0;
double 	totalMontoValuadoUSDBot	= 0;
double	totalMontoIntMN			= 0;
double	totalMontoIntUSD			= 0;
double	totalMontoIntUSDBot		= 0;
double	totalMontoDescMN			= 0;
double	totalMontoDescUSD			= 0;
double	totalMontoDescUSDBot		= 0;
int		totalDoctosConv			= 0;
double	totalMontoConvMN			= 0;
double	totalMontoConv				= 0;


	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
	 
	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
	
	AutorizacionSolic autSolic2 = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);
	
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

	CrearArchivosAcuses  generaAch =  new CrearArchivosAcuses();
	generaAch.setAcuse(aux_acuse);
	generaAch.setTipoArchivo("PDF");		
	generaAch.setStrDirectorioTemp(strDirectorioTemp);
	generaAch.setUsuario(iNoUsuario);
	generaAch.setVersion("PANTALLA");	
		 
	int existeArch= 	generaAch.existerArcAcuse( );  //verifica si existe 
	
	System.out.println("existeArch ===="+existeArch);

try {

	if( existeArch ==0 ) {
	
	StringBuffer texto = new StringBuffer();
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
	
	// Obtener la lista de las Epos Seleccionadas
	HashMap 			doctosAgrupados 	= getDoctosAgrupadosPorEpo(ic_documentos, doctosPorEpo);

	ArrayList 		listaClavesEpos	= (ArrayList) doctosAgrupados.get("LISTA_CLAVES_EPOS");
	if(listaClavesEpos.size()== 0){
		throw new Exception("No se ha seleccionado ningún documento válido.");
	}
	boolean	hayMasDeUnaEpoSeleccionada = listaClavesEpos.size()>1?true:false;

	if(!hayMasDeUnaEpoSeleccionada){
		
		String claveEpo = (String) listaClavesEpos.get(0);
		Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
		HashMap ohDatosFondeo = BeanAutDescuento.getDatosFondeo(iNoCliente, claveEpo, "4");
		csTipoFondeo = ohDatosFondeo.get("TIPO_FONDEO").toString();
	}else{

		for(int k=0;k<listaClavesEpos.size();k++){
   		String claveEpo = (String) listaClavesEpos.get(k);
   		Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
   	}
	}

	// Obtener HashMap con los Parametros de Fondeo
	HashMap catalogoTipoFondeo = hayMasDeUnaEpoSeleccionada?BeanAutDescuento.getTipoFondeo(iNoCliente, listaClavesEpos, "4"):null;
	/*if (!hayMasDeUnaEpoSeleccionada && "P".equals(csTipoFondeo)){
		pdfDoc.addText("Las siguientes solicitudes de distribuidores se realizaron con Fondeo Propio","formas",ComunesPDF.CENTER);
	}*/

	Vector nombresCampo = new Vector(5);
	Vector detalleCampo = null;

	// F029 - 2011: Si selecciono mas de una EPO usar nombres de campos genericos
	if(hayMasDeUnaEpoSeleccionada){

		Vector fila = null;
		nombresCampo = new Vector();
		fila = new Vector();
		fila.add("CG CAMPO 1");
		nombresCampo.add(fila);

		fila = new Vector();
		fila.add("CG CAMPO 2");
		nombresCampo.add(fila);

		fila = new Vector();
		fila.add("CG CAMPO 3");
		nombresCampo.add(fila);

		fila = new Vector();
		fila.add("CG CAMPO 4");
		nombresCampo.add(fila);

		fila = new Vector();
		fila.add("CG CAMPO 5");
		nombresCampo.add(fila);
	}else{
		String claveEpo = (String) listaClavesEpos.get(0);
		nombresCampo 	= cargaDocto.getCamposAdicionales(claveEpo,false);
	}

	vecFilas = autSolic2.getDoctosSeleccionadosIF(aux_acuse,hidTipoCredito);
	for(i=0;i<vecFilas.size();i++){

		vecColumnas 		=  (Vector)vecFilas.get(i);
		icMoneda				=	(String)vecColumnas.get(5);
		monto					=	Double.parseDouble(vecColumnas.get(24).toString());
		tipoCambio			=	Double.parseDouble(vecColumnas.get(8).toString());
		montoValuado		=	monto*tipoCambio;
		montoInteres 		=	Double.parseDouble(vecColumnas.get(15).toString());
		porcDescuento		=	Double.parseDouble(vecColumnas.get(18).toString());
		montoDescuento		= 	montoValuado * (porcDescuento/100);
		monedaLinea			= 	(String)vecColumnas.get(23);

		if("1".equals(icMoneda)){
			totalDoctosMN 			++;
			totalMontoMN 			+= monto;
			totalMontoValuadoMN 	+= montoValuado;
			totalMontoIntMN		+= montoInteres;
		}
		
		if("54".equals(icMoneda)&&"54".equals(monedaLinea)){
			totalDoctosUSD			++;
			totalMontoUSD 			+= monto;
			totalMontoValuadoUSD	+= montoValuado;
			totalMontoIntUSD		+= montoInteres;
		}

		if("54".equals(icMoneda)&&"1".equals(monedaLinea)){
			totalDoctosConv		++;
			totalMontoConv			+= monto;
			totalMontoConvMN		+= montoValuado;
			totalMontoIntConv		+= montoInteres;
		}
	}

	pdfDoc.setTable(6, 100);
	if (!hayMasDeUnaEpoSeleccionada && "P".equals(csTipoFondeo)){
		pdfDoc.setCell("Las siguientes solicitudes de distribuidores se realizaron con Fondeo Propio","formas",ComunesPDF.CENTER,6);
	}
	pdfDoc.setCell("Moneda Nacional","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Dólares","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Documentos en DLL financiados en M.N.","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de solicitudes procesadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto de solicitudes procesadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de solicitudes procesadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto de solicitudes procesadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de solicitudes procesadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto de solicitudes procesadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell(String.valueOf(totalDoctosUSD),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell(String.valueOf(totalDoctosConv),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoConvMN,2),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell(cadEpo,"formas",ComunesPDF.LEFT,6);
	pdfDoc.addTable();

	vecFilas = autSolic2.getDoctosSeleccionadosIF(aux_acuse,hidTipoCredito);
	String claveDeLaEpo = null;
	List lEncabezados = new ArrayList();
	int numeroCamposAdicionales = nombresCampo.size();
	for(i=0;i<vecFilas.size();i++){
		
		vecColumnas 		=  (Vector)vecFilas.get(i);
		igNumeroDocto		=	(String)vecColumnas.get(0);
		fechaSolicitud		=	(String)vecColumnas.get(1);
		distribuidor		=	(String)vecColumnas.get(2);
		moneda				=	(String)vecColumnas.get(4);
		icMoneda				=	(String)vecColumnas.get(23);
		monto					=	Double.parseDouble(vecColumnas.get(24).toString());
		tipoConversion		=	(String)vecColumnas.get(7);
		tipoCambio			=	Double.parseDouble(vecColumnas.get(8).toString());
		montoValuado		=	monto*tipoCambio;
		referencia			=	(String)vecColumnas.get(9);
		tasaInteres			=	Double.parseDouble(vecColumnas.get(10).toString());
		plazo					=	(String)vecColumnas.get(11);
		fechaVto				=	(String)vecColumnas.get(12);
		tipoPagoInteres	=	(String)vecColumnas.get(13);
		montoInteres 		=	Double.parseDouble(vecColumnas.get(15).toString());
		icDocumento			=	(String)vecColumnas.get(16);
		nombreEPO			=	(String)vecColumnas.get(17);
		acuse 				=	(String)vecColumnas.get(19);
		//fechaCarga			=	(String)vecColumnas.get(1);
		fechaCarga			=	(String)vecColumnas.get(20);
		horaCarga			=	(String)vecColumnas.get(21);
		if(hayMasDeUnaEpoSeleccionada){
			tipoFinan = BeanAutDescuento.getDescripcionTipoFondeo(catalogoTipoFondeo,claveDeLaEpo);
		}
		if(hidTipoCredito.equals("D")) {
			rs_numDist			=	(String)vecColumnas.get(25);
			rs_cveTasa			=	(String)vecColumnas.get(26);
			rs_tipoConver		=	(String)vecColumnas.get(27);
			rs_tipoCamb			=	(String)vecColumnas.get(28);
			rs_respInt			=	(String)vecColumnas.get(29);
			rs_tipoCob			=	(String)vecColumnas.get(30);
			rs_referencia 		=	(String)vecColumnas.get(31);
			rs_campo1			=	(String)vecColumnas.get(32);
			rs_campo2			=	(String)vecColumnas.get(33);
			rs_campo3			=	(String)vecColumnas.get(34);
			rs_campo4			=	(String)vecColumnas.get(35);
			rs_campo5 			=	(String)vecColumnas.get(36);
			tasaInteres			=	Double.parseDouble(vecColumnas.get(37).toString());
			numerocuenta		=	(String)vecColumnas.get(38);
			if(numerocuenta.equals("0")){
				numerocuenta	= "";
			}
			claveDeLaEpo 		=	(String)vecColumnas.get(39);
		}else {
			rs_campo1			=	(String)vecColumnas.get(25);
			rs_campo2			=	(String)vecColumnas.get(26);
			rs_campo3			=	(String)vecColumnas.get(27);
			rs_campo4			=	(String)vecColumnas.get(28);
			rs_campo5 			=	(String)vecColumnas.get(29);
			tasaInteres			=	Double.parseDouble(vecColumnas.get(30).toString());
			numerocuenta		=	(String)vecColumnas.get(31);
			claveDeLaEpo 		=	(String)vecColumnas.get(32);
			tipoPago 		=	(String)vecColumnas.get(40);
			
		}
		if(i==0){
			lEncabezados.add("No. de documento Final");
			lEncabezados.add("EPO Relacionada");
			lEncabezados.add("Moneda");
			lEncabezados.add("Monto");
			lEncabezados.add("Tipo Conv");
			lEncabezados.add("Tipo Cambio");
			lEncabezados.add("Monto Valuado");
			lEncabezados.add("Referencia tasa de interés");
			lEncabezados.add("Tasa interés");
			lEncabezados.add("Plazo");
			lEncabezados.add("Fecha de vencimiento");
			lEncabezados.add("Monto de Intereses");
			lEncabezados.add("Tipo de Cobro de Intereses");
			if (nombresCampo !=null ) {
				for (int e=0;e<nombresCampo.size();e++){
					detalleCampo = (Vector) nombresCampo.get(e);
					lEncabezados.add((String)detalleCampo.get(0));
				}
			}
			/*if(hidTipoCredito.equals("D")) {
				lEncabezados.add(" Número de Cuenta ");
			}*/
			if(hayMasDeUnaEpoSeleccionada){
				lEncabezados.add("Tipo de Financiamiento ");
			}
			pdfDoc.setTable(lEncabezados, "celda01", 100);
		}
		pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(nombreEPO,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(tipoConversion,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(String.valueOf(tipoCambio),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(String.valueOf(montoValuado),"formas",ComunesPDF.CENTER);
		if(!tipoPago.equals("2")) {
			pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
		}else  {
			pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
		}
		pdfDoc.setCell(String.valueOf(tasaInteres),"formas",ComunesPDF.CENTER);
		if(!tipoPago.equals("2")) {
			pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
		}else {
			pdfDoc.setCell(plazo+ " (M)","formas",ComunesPDF.CENTER);
		}
		pdfDoc.setCell(fechaVto,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(tipoPagoInteres,"formas",ComunesPDF.CENTER);
		if (nombresCampo !=null ) {
			for (int j=1;j<=nombresCampo.size();j++){
				if(j>numeroCamposAdicionales){
					pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
					continue;
				}
				if (j==1) {
					pdfDoc.setCell(rs_campo1,"formas",ComunesPDF.CENTER);
				} else if (j==2) {
					pdfDoc.setCell(rs_campo2,"formas",ComunesPDF.CENTER);
				} else if (j==3) { 
					pdfDoc.setCell(rs_campo3,"formas",ComunesPDF.CENTER);
				} else if (j==4) {
					pdfDoc.setCell(rs_campo4,"formas",ComunesPDF.CENTER);
				} else if (j==5) { 
					pdfDoc.setCell(rs_campo5,"formas",ComunesPDF.CENTER);
				}
			}       
		}
		/*if(hidTipoCredito.equals("D")) {
			pdfDoc.setCell(numerocuenta,"formas",ComunesPDF.CENTER);
		}*/
		if(hayMasDeUnaEpoSeleccionada){
			pdfDoc.setCell(tipoFinan,"formas",ComunesPDF.CENTER);
		}
	}
	if(lEncabezados.size() > 0){
		pdfDoc.addTable();
	}
	/*
	if(totalDoctosMN+totalDoctosConv>0){
		pdfDoc.setCell("Total M.N.","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(String.valueOf(totalDoctosMN+totalDoctosConv),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoMN+totalMontoConv,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoIntMN+totalMontoIntConv,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		if (nombresCampo !=null ) {
			for (int j=1;j<=nombresCampo.size();j++){
				if (j==1) {
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				} else if (j==2) {
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				} else if (j==3) { 
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				} else if (j==4) {
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				} else if (j==5) { 
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				}
			}
		}
		if(hayMasDeUnaEpoSeleccionada){
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		}
	}
	if(totalDoctosUSD>0){
		pdfDoc.setCell("Total Dolares","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(String.valueOf(totalDoctosUSD),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoIntUSD,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		if (nombresCampo !=null ) {
			for (int j=1;j<=nombresCampo.size();j++){
				if (j==1) {
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				} else if (j==2) {
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				} else if (j==3) { 
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				} else if (j==4) {
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				} else if (j==5) { 
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				}
			}
		}
		if(hayMasDeUnaEpoSeleccionada){
			pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		}
	}*/

	pdfDoc.setTable(2, 50);
	pdfDoc.setCell("Datos de cifras de control","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Núm. acuse","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(fechaCarga,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Hora","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(horaCarga,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Clave y número de usuario","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(iNoUsuario+" "+strNombreUsuario,"formas",ComunesPDF.CENTER);
	pdfDoc.addTable();
		
	pdfDoc.endDocument();
	
	
	generaAch.setRutaArchivo(strDirectorioTemp+nombreArchivo);
	
	generaAch.guardarArchivo() ; //Guardar	
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("accion",accion);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

}else 	if(existeArch>0)  {
	
	nombreArchivo = generaAch.desArchAcuse( ) ;

	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("accion",accion); 

}

} catch (Exception exception) {
		
		java.io.StringWriter outSW = new java.io.StringWriter();
		exception.printStackTrace(new java.io.PrintWriter(outSW));
		String stackTrace = outSW.toString();
			
		generaAch.setDesError(stackTrace);	
		
		generaAch.guardaBitacoraArch();	
	
	throw new NafinException("Error al generar el archivo");
	
} 

%>
<%!
	public HashMap getDoctosAgrupadosPorEpo(String[] doctosSeleccionados, String[] doctosPorEpo){
		HashMap 			doctosAgrupados 	= new HashMap();
		LinkedHashSet	listaClaves			= new LinkedHashSet();
		ArrayList		listaClavesEpos	= new ArrayList();
		HashMap			resultado			= new HashMap();

		for(int i=0;i<doctosPorEpo.length;i++){
			String[] doctoEpo =  doctosPorEpo[i].split(",");
			String 	docto 	=	doctoEpo[0];
			String 	claveEpo =	doctoEpo[1];

			if(contains(doctosSeleccionados,docto)){
				ArrayList lista = (ArrayList) doctosAgrupados.get(claveEpo);
				if(lista == null){
					lista = new ArrayList();
					doctosAgrupados.put(claveEpo,lista);
				}
				lista.add(docto);
				listaClaves.add(claveEpo);
			}
		}

		Iterator itr = listaClaves.iterator();
		while(itr.hasNext()){
			listaClavesEpos.add(itr.next());
		}

		resultado.put("DOCTOS_AGRUPADOS",	doctosAgrupados);
		resultado.put("LISTA_CLAVES_EPOS",	listaClavesEpos);

		return resultado;
	}
	
	public boolean contains(String []array, String element){
		if(array == null || array.length == 0 ) return false;
		for(int i=0;i<array.length;i++){
			String tmp = array[i];
			if( tmp == null && element == null ){
				return true;
			}
			if(tmp != null && tmp.equals(element)){
				return true;
			}
		}
		return false;
	}
%>
<%=jsonObj%>