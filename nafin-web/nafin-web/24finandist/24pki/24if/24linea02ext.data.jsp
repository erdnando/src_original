 <%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.sql.*,com.netro.exception.*,
		netropology.utilerias.*, com.netro.anticipos.*,
		com.netro.distribuidores.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%

String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("valoresIniciales"))	{

	Lineadm lineadm = ServiceLocator.getInstance().lookup("LineadmEJB", Lineadm.class);
	JSONObject jsonObj = new JSONObject();

	String tipo_liq = lineadm.getDetalle(iNoCliente);
	jsonObj.put("tipo_liq", tipo_liq);
	jsonObj.put("strPerfil", strPerfil);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoEPODist") ) {

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveIf(iNoCliente);
	if(strPerfil.equals("IF FACT RECURSO")){
		cat.setTipoCredito("F");
	}else{
		cat.setTipoCredito("C");
	}
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoDistribuidor")){

	String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");

	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		if(!strPerfil.equals("IF FACT RECURSO")){
			cat.setTipoCredito("C");
		}
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();
	}else{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("CatalogoMonedaMn")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setValoresCondicionIn("1", Integer.class);
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoMonedaDl")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setValoresCondicionIn("54", Integer.class);
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoMonedaTodo")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setValoresCondicionIn("1,54", Integer.class);
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoEstatusAsignar")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_linea");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_linea");
	cat.setValoresCondicionIn("3,4", Integer.class);
	//cat.setOrden("ic_estatus_linea");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoTipoCobroDist")){

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	
	String ic_epo	=	(request.getParameter("aux_icepo")!=null)?request.getParameter("aux_icepo"):"";
	
	String rs_epo_tipo_cobro_int	=	"1,2";
	
	if(!"".equals(ic_epo))	{
		String valor = cargaDocto.getEpoTipoCobroInt_b(ic_epo);
		if(valor.equals("E")){
			rs_epo_tipo_cobro_int="1,2,3";
		}
	}
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_cobro_interes");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_tipo_cobro_interes");
	cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
	cat.setOrden("ic_tipo_cobro_interes");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoTipoCobroGral")){

	String rs_epo_tipo_cobro_int	=	"1,2,3";

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_cobro_interes");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_tipo_cobro_interes");
	cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
	cat.setOrden("ic_tipo_cobro_interes");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoBanco") ) {

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);
	Vector vecColumnas = null;
	Vector vecFilas = lineaCredito.getVecBancos();
	
	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	for (int i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector)vecFilas.get(i);
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave((String)vecColumnas.get(0));
		elementoCatalogo.setDescripcion((String)vecColumnas.get(1));
		coleccionElementos.add(elementoCatalogo);
	}
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar = "{\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString()+"}";

}else if (informacion.equals("Consulta")){

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);
	
	JSONObject jsonObj = new JSONObject();

	String ic_epo	=	(request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	String ic_pyme	=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String ic_linea=	(request.getParameter("ic_linea")!=null)?request.getParameter("ic_linea"):"";

	Vector	vecFilas 	= null;
	Vector	vecColumnas = null;

	String 	rs_ic_linea_credito_padre 	= "";
	String 	folioSolicitud 	= "";
	String 	numCliente 		= "";
	String 	nombreCliente 	= "";
	String 	plaza 			= "";
	String 	direccion 		= "";
	String 	telefono 		= "";
	String 	tipoSolicitud 	= "";
	String 	fechaSolicitud 	= "";
	String 	estatusActual 	= "";
	String 	tipo			= "";
	String 	fechaHoy 		= "";
	String 	icEstatusActual	= "";
	String 	banco			= "";
	String 	cuenta			= "";
	String 	auxIcPyme 		= "";
	String	auxIcEpo		= "";
	String 	folioArmado 	= "";
	String 	lineasIF		= "0.00";
	String 	productoNafin	= "";
	String 	ic_productoNafin= "";
	String  Moneda			= "";
	String	plazoMinimo		= "";

	List regs = new ArrayList();
	
	String ic_producto="4";
	vecFilas = lineaCredito.consultaIF(ic_epo,iNoCliente,ic_pyme,ic_linea,ic_producto);
	for(int i=0;i<vecFilas.size();i++)	{
		HashMap hash = new HashMap();
		vecColumnas		= (Vector)vecFilas.get(i);
		folioSolicitud	= (String)vecColumnas.get(0);
		numCliente		= (String)vecColumnas.get(1);
		nombreCliente	= (String)vecColumnas.get(2);
		plaza 			= (String)vecColumnas.get(3);
		direccion 		= (String)vecColumnas.get(4);
		telefono 		= (String)vecColumnas.get(5);
		tipoSolicitud	= (String)vecColumnas.get(6);
		estatusActual	= (String)vecColumnas.get(8);
		fechaHoy 		= (String)vecColumnas.get(9);
		icEstatusActual= (String)vecColumnas.get(11);
		tipo 			= (String)vecColumnas.get(12);
		banco			= (String)vecColumnas.get(15);
		cuenta			= (String)vecColumnas.get(16);
		auxIcPyme 		= (String)vecColumnas.get(21);
		auxIcEpo		= (String)vecColumnas.get(22);
		folioArmado  	= (String)vecColumnas.get(23);
		lineasIF		= (String)vecColumnas.get(28);
		productoNafin   = (String)vecColumnas.get(30);
		Moneda   		= (String)vecColumnas.get(31);
		ic_productoNafin= (String)vecColumnas.get(32);
		fechaSolicitud	= (String)vecColumnas.get(33);
		rs_ic_linea_credito_padre = (String)vecColumnas.get(34);
		plazoMinimo		= (String)vecColumnas.get(39);
		hash.put("FOLIOSOLICITUD",folioSolicitud);
		hash.put("NUMDIST",numCliente);
		hash.put("NOMBREPYME",nombreCliente);
		hash.put("PLAZA",plaza);
		hash.put("DIRECCION",direccion);
		hash.put("TELEFONO",telefono);
		hash.put("TIPOSOLICITUD",tipoSolicitud);
		hash.put("ESTATUSACTUAL",estatusActual);
		hash.put("FECHAHOY",fechaHoy);
		hash.put("IC_ESTATUSACTUAL",icEstatusActual);
		hash.put("TIPO",tipo);
		hash.put("BANCO",banco);
		hash.put("CUENTA",cuenta);
		hash.put("AUX_ICPYME",auxIcPyme);
		hash.put("AUX_ICEPO",auxIcEpo);
		hash.put("FOLIOARMADO",folioArmado);
		hash.put("LINEASIF",lineasIF);
		hash.put("PRODUCTONAFIN",productoNafin);
		hash.put("MONEDA",Moneda);
		hash.put("IC_PRODUCTONAFIN",ic_productoNafin);
		hash.put("FECHASOLICITUD",fechaSolicitud);
		hash.put("IC_LINEACREDITOPADRE",rs_ic_linea_credito_padre);
		hash.put("PLAZOMINIMO",plazoMinimo);
		hash.put("CAUSA_RECHAZO","");
		hash.put("TIPO_COBRO_INT","");
		hash.put("FECHA","");
		regs.add(hash);
	}
	jsonObj.put("registros", regs);
	jsonObj.put("success", new Boolean(true));	
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Termina")){

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

	JSONObject jsonObj = new JSONObject();

	int numRegistrosMN 		= 0;
	int numRegistrosDL 		= 0;
	double montoAutoMN 		= 0.0;
	double montoAutoDL 		= 0.0;
	String aFolio [] 		= request.getParameterValues("folio");
	String aFolioArm [] 	= request.getParameterValues("rs_folioArmado");
	String aNumCliente [] 	= request.getParameterValues("rs_numCliente");
	String aNomCliente [] 	= request.getParameterValues("rs_nomPyme");
	String aFolioRel [] 	= request.getParameterValues("rs_folioRel");
	String aFechaSol [] 	= request.getParameterValues("rs_fechaSol");
	String aMoneda[]		= request.getParameterValues("moneda");
	String aMonto [] 		= request.getParameterValues("monto");
	String aEstatus [] 		= request.getParameterValues("estatus");
	String aTipoCobro [] 	= request.getParameterValues("tipo_cobro_int");
	String aCausa [] 		= request.getParameterValues("causa_rechazo");
	String aMontoAuto [] 	= request.getParameterValues("monto_auto");
	String aFechaVto [] 	= request.getParameterValues("fecha_vto");
	String aBanco [] 		= request.getParameterValues("banco");
	String aCuenta [] 		= request.getParameterValues("cuenta");
	String aTipo[]			= request.getParameterValues("tipo");
	String atipoSolicitud[]	= request.getParameterValues("tipoSolicitud");
	String tipo_liq			= request.getParameter("tipo_liq");
	Vector vecFilas = null;
	Vector vecColumnas = null;
	String cc_acuse 	= "";
	String acuseForm 	= "";
	String fecha 		= "";
	String hora 		= "";
	String usuario 	= "";

	String totMtoAuto	  	= (request.getParameter("totMtoAuto")==null)?"0":request.getParameter("totMtoAuto");
	String totDocs	  		= (request.getParameter("totDocs")==null)?"0":request.getParameter("totDocs");
	String totMtoAutoDol = (request.getParameter("totMtoAutoDol")==null)?"0":request.getParameter("totMtoAutoDol");
	String totDocsDol	  	= (request.getParameter("totDocsDol")==null)?"0":request.getParameter("totDocsDol");
	numRegistrosMN 		= Integer.parseInt(totDocs);
	numRegistrosDL 		= Integer.parseInt(totDocsDol);
	montoAutoMN 			= Double.parseDouble(totMtoAuto);
	montoAutoDL 			= Double.parseDouble(totMtoAutoDol);

	String msj = "";
	try {
		vecFilas = lineaCredito.AutorizacionIF(iNoUsuario,numRegistrosMN,montoAutoMN,numRegistrosDL,montoAutoDL,aTipo,tipo_liq,aFolio,aMoneda,aMonto,aEstatus,aTipoCobro,aCausa,aMontoAuto,aFechaVto,aBanco,aCuenta,numRegistrosMN,montoAutoMN,numRegistrosDL,montoAutoDL);
		vecColumnas = (Vector)vecFilas.get(0);
		cc_acuse 	= (String)vecColumnas.get(0);
		acuseForm 	= (String)vecColumnas.get(1);
		fecha 		= (String)vecColumnas.get(2);
		hora 			= (String)vecColumnas.get(3);	
		usuario 	= iNoUsuario+" "+strNombreUsuario;
		msj = "La autentificación se llevó a cabo con éxito<br>Los cambios de estatus fueron realizados con éxito";

		jsonObj.put("msg", msj);
		jsonObj.put("_acuse", acuseForm);
		jsonObj.put("fechaCarga", fecha);
		jsonObj.put("horaCarga", hora);
		jsonObj.put("usuario", usuario);
		jsonObj.put("success", new Boolean(true));

	} catch(Exception e) {
		msj=e.toString();
	}

	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>