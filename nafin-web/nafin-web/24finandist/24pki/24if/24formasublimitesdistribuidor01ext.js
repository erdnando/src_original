/*
*Distribuidores / Capturas / Sub-l�mites distribuidor 
*/
Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------

//VARIABLES GLOBALES
var porcentajeLineaOriginal=[];

var operacion=[];//almacena  un valor booleano por cada fila del grid, este 
//booleano es para identificar si se inserta un regisro nuevo (true) o 
//se actualiza uno existente (false)

//--------------------------------HANDLER's-------------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

							
		var gridConsulta = Ext.getCmp('gridConsulta');//fpGridModifica	
		var fpGridModifica = Ext.getCmp('formaGrid');//fpGridModifica	
		fpGridModifica.setTitle(Ext.getCmp('cboEPO').getRawValue());
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
				if (!fpGridModifica.isVisible()) {
									fpGridModifica.show();
								}
			var jsonData = store.reader.jsonData;	
			Ext.each(jsonData.registros,function(record){
				porcentajeLineaOriginal.push(Ext.apply({id:record.IC_MONEDA,porc:record.PORCENTEJE_LINEA}))
				
			});
				
			if(store.getTotalCount() > 0) {//////////Verifica que existan registros
				//Validacio � tiene porcentaje capturado?
				var datos= gridConsulta.getStore();
				Ext.each(arrRegistros, function(record) { //step 2
					Ext.Ajax.request({ 		// step 5
								url:'24formasublimitesdistribuidor01ext.data.jsp',
								params :Ext.apply(fp.getForm().getValues(),{
									ic_moneda:record.data.IC_MONEDA,
									informacion:'porcenteje.capturado'
								}),
								scope:this,
								success : function(response) { //step 6
								if (!fpGridModifica.isVisible()) {
									fpGridModifica.show();
								}
									var respuesta =Ext.util.JSON.decode(response.responseText);
									operacion.push(respuesta);
									
									//datos.insert(0,respuesta)		;
									//gridConsulta.getStore().commitChanges();

								}
							});
				});
				
			////////////////////////////
				el.unmask();					
				Ext.getCmp('btnGuardar').enable().show();
			} else {	
				Ext.getCmp('btnGuardar').disable().hide();
				//el.mask('No se encontr� ning�n registro', 'x-mask');				
				el.mask('La EPO no tiene l�neas de Cr�dito parametrizadas, favor de verificar', 'x-mask');				
			}
		}
	}

	
	
//--------------------------------STORE's---------------------------------------
 // shared reader
    var fields = new Ext.data.JsonReader({
		root:'registros'
	 }, [
		{name: 'IC_PYME'},
       {name: 'DISTRIBUIDOR'},
		 {name: 'MONEDA'},
       {name: 'PORCENTEJE_LINEA',id:'porcLin'/*, type: 'float'*/},
       {name: 'MONTO_SUB_LIMITE'/*, type: 'float'*/},
       {name: 'USUARIO_ALTA'},
       {name: 'FECHA_ALTA'/*, type: 'date', dateFormat: 'n/j h:ia'*/},
       {name: 'USUARIO_ULTIMA_MODIFICACION'},
       {name: 'FECHA_ULTIMA_MODIFICACION' /*, dateFormat: 'n/j h:ia'*/},
		 {name: 'FN_MONTO_AUTORIZADO',type: 'float'},
		 {name: 'IC_MONEDA'}
    ]);
	var consulta = new Ext.data.GroupingStore({
		url:'24formasublimitesdistribuidor01ext.data.jsp',		
		reader: fields,
		//baseParams:{informacion:'Consulta'},	
		sortInfo:{field: 'DISTRIBUIDOR', direction: 'DESC'},
		groupField: 'DISTRIBUIDOR',
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var catalogoEPO = new Ext.data.JsonStore({
		root:'registros',
		fields:['clave', 'descripcion', 'loadMsg'],
		url:'24formasublimitesdistribuidor01ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPO'	},
		totalProperty:'total',	autoLoad:false,	listeners:{exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo}
	});
	
	var catalogoDistribuidores = new Ext.data.JsonStore({
		root:'registros',
		fields:['clave', 'descripcion', 'loadMsg'],
		url:'24formasublimitesdistribuidor01ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDist'	},
		totalProperty:'total',	autoLoad:false,	listeners:{exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo}
	});

//------------------------------ELEMENTOS---------------------------------------	

	var elementosForma = [
		{
			xtype:'combo',
			name:	'catalogoEpo',
			id:	'cboEPO',
			hiddenName:	'catalogoEpo',
			fieldLabel:	'EPO',
			emptyText:	'Seleccione . . .',
			allowBlank:false,
			forceSelection:true,	
			triggerAction:'all',	
			typeAhead: true,	
			minChars:1,	
			mode: 'local',	
			displayField:'descripcion',	
			valueField:'clave',
			store:catalogoEPO,
			tpl:NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						Ext.getCmp('cboDist').reset();
							cveEstatus = combo.value;
								catalogoDistribuidores.load({params:	{epo:cveEstatus}});
							}
					}
				}
			},
			{
				xtype:'combo',
				name:	'catalogoDist',
				id:	'cboDist',
				hiddenName:	'catalogoDist',
				fieldLabel:	'Distibuidor',
				emptyText:	'Seleccione . . .',
				allowBlank:false,
				forceSelection:true,	
				triggerAction:'all',	
				typeAhead: true,	
				minChars:1,	
				mode: 'local',	
				displayField:'descripcion',	
				valueField:'clave',
				store:catalogoDistribuidores,	
				tpl:NE.util.templateMensajeCargaCombo
			}
		
	];


		var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consulta,//consultaData,
		// plugins	: summary,  //step 3
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		//title: 'Consulta',
		clicksToEdit: 1,
		//hidden: true,
		columns: [	
			{
				header: '',
				tooltip: 'Distribuidor',
				dataIndex: 'DISTRIBUIDOR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: '',
				tooltip: '',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{
				header: '% L�nea',
				tooltip: '% L�nea',
				dataIndex: 'PORCENTEJE_LINEA',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				editor: {
                xtype: 'numberfield',
                allowBlank: false
            }
			},
			{
				header: 'Monto Sub-l�mite.',
				tooltip: 'Monto Sub-l�mite.',
				dataIndex: 'MONTO_SUB_LIMITE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				
				renderer:function(v,params,record){
							return '<div align=right>'+ Ext.util.Format.usMoney((record.data.PORCENTEJE_LINEA * record.data.FN_MONTO_AUTORIZADO)/100)+'</div>';
							 
				 }
			},
			{
				header: 'Usuario Alta',
				tooltip: 'Usuario Alta',
				dataIndex: 'USUARIO_ALTA',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
							return '<div align="left">'+ value+'</div>';
				 }
			},
			{
				header: 'Fecha Alta',
				tooltip: 'Fecha Alta',
				dataIndex: 'FECHA_ALTA',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
							return '<div align="left">'+ value+'</div>';
				 }	
			},
			{
				header: 'Usuario �ltima<BR>Modificaci�n',
				tooltip: 'Usuario �ltima Modificaci�n',
				dataIndex: 'USUARIO_ULTIMA_MODIFICACION',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'/*,
				renderer:function(value){
							return '<div align="left">'+ value+'</div>';
				 }*/
			},
			{
				header: 'Fecha �ltima<BR>Modificaci�n',
				tooltip: 'Fecha �ltima<BR>Modificaci�n',
				dataIndex: 'FECHA_ULTIMA_MODIFICACION',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
							return '<div align="left">'+ value+'</div>';
				 }
			}															
		],
	view:new Ext.grid.GroupingView({
		//forceFit 			: true,
		ShowGroupName		: true,
		enableNoGroup		: false,
		enableGropingMenu	: false,
		hideGroupedColumn	: true,
		groupTextTpl: '{text}'//,, ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
	}),
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		shadow : false,
		frame: false
	});
	var elementosFormaGrid=[
		gridConsulta
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		frame:true,
		labelWidth: 60,
		style: ' margin:0 auto;',
		title: 'Sub-l�mites Distribuidor',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items:	elementosForma,
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,	
				handler: function(boton, evento) {
				fp.el.mask('Procesando...', 'x-mask-loading');
				fpGridModifica.hide();
					consulta.load({
						params: Ext.apply(fp.getForm().getValues(),{
								operacion : 'Generar',
								informacion : 'Consultar',
								start:0,
								limit:1000					

						})
					}); 
				
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				tabIndex			: 14,
				handler: function() {
					window.location = '24formasublimitesdistribuidor01ext.jsp';					
				}
			}
		]
	});
	
	var fpGridModifica=new Ext.form.FormPanel({
		id: 'formaGrid',
		collapsible:true,
		hidden: true,
		width: 800,
		frame:true,
		labelWidth: 60,
		style: ' margin:0 auto;',
		title: Ext.getCmp('cboEPO').getRawValue()+' Sub-l�mites Distribuidor',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items:	elementosFormaGrid,
		monitorValid: true,
		buttons: [		
			{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls: 'icoGuardar',
				formBind: true,	
				handler: function(boton, evento,grid) {					
					
					var datos= gridConsulta.getStore();//.getModifiedRecords(); //paso 1
					
					if(!Ext.isEmpty(datos.getModifiedRecords())){
						var recordsToSave=[] ;
						var indice=0;
						Ext.each(datos.getModifiedRecords(), function(record) { //step 2
							
							var icPyme=record.data.IC_PYME;
							var distribuidor=(record.data.DISTRIBUIDOR);
							var porcentaleLinea=(record.data.PORCENTEJE_LINEA);
							var montoSublimite=((record.data.PORCENTEJE_LINEA*record.data.MONTO_LINEA_EPO)/100);
							var icMoneda=(record.data.IC_MONEDA);
							////////////////////////////////////////////
							gridConsulta.el.mask('Procesando?', 'x-mask-loading'); //step 3
							//se hace una peticion por cada fila modificada
							
							Ext.Ajax.request({ 		// step 5
								url:'24formasublimitesdistribuidor01ext.data.jsp',
								params :Ext.apply(fp.getForm().getValues(),{
									//records : recordsToSave,
									//icPyme:icPyme,
									guardar:operacion[indice].guardar	,						

									//distribuidor:distribuidor,
									porcentaleLinea:porcentaleLinea,
									//montoSublimite:montoSublimite,
									icMoneda:icMoneda,
									//pocentajeOriginal:porcentajeLineaOriginal[indice].porc,
									informacion:'Guarbar_Cambios'
								}),
								scope:this,
								success : function(response) { //step 6
									gridConsulta.el.unmask();
									//gridConsulta.getStore().commitChanges();
									consulta.reload();
									gridConsulta.getStore().commitChanges();
									indice++;
									Ext.Msg.alert(boton.getText(),'La operaci�n se realiz� con �xito');
								}
							});	
							
							//////////////////////////////////////////////
							
							//recordsToSave.push(Ext.apply({id:record.IC_PYME},record.data));
							recordsToSave.push(Ext.apply({id:record.IC_PYME},record.data));
							
						});
						recordsToSave = Ext.encode(recordsToSave); //step 4
						
						
						
					}else{
						Ext.Msg.alert(boton.getText(),'Favor de ingresar el % de L�nea correspondiente');
					}
					
					
				}
			}
		]
	});
	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(5),
			fpGridModifica
			
		]
	});

	catalogoEPO.load();

});//Fin de funcion principal