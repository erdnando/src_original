<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	netropology.utilerias.*,
	com.netro.anticipos.*,
	com.netro.exception.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
//String informacion= (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

String _acuse		= (request.getParameter("_acuse")==null)?"":request.getParameter("_acuse");
String acuse		= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String fechaHoy	= (request.getParameter("fechaHoy")==null)?"":request.getParameter("fechaHoy");
String horaActual	= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");

String ic_epo		= (request.getParameter("_ic_epo")==null)?"":request.getParameter("_ic_epo");
String ic_pyme		= (request.getParameter("_ic_pyme")==null)?"":request.getParameter("_ic_pyme");
String ic_moneda	= (request.getParameter("_ic_moneda")==null)?"1":request.getParameter("_ic_moneda");
String tipo_tasa	= (request.getParameter("_tipo_tasa")==null)?"P":request.getParameter("_tipo_tasa");
String plazo		= (request.getParameter("_plazo")==null)?"":request.getParameter("_plazo");

String tipoCredito			= (request.getParameter("_tipo_credito")==null)?"":request.getParameter("_tipo_credito");

try {

	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;

	CapturaTasas beanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

	contenidoArchivo.append(
		"Recibo Numero:,"+_acuse+
		"\nNumero de Acuse,"+acuse+
		"\nFecha de Autorizacion,"+fechaHoy+
		"\nHora de Autorizacion,"+horaActual+
		"\nUsuario de Captura,"+strNombreUsuario.replace(',',' ')+
		"\n,\n,\n");

	Vector lovTasas = beanTasas.ovgetTasasPreferNego("4",tipo_tasa,iNoCliente,ic_epo,ic_moneda,ic_pyme, plazo, tipoCredito);
	for (int i=0; i<lovTasas.size(); i++) {
			Vector lovDatosTasa = (Vector)lovTasas.get(i);
			String lsMoneda	= lovDatosTasa.get(0)==null?"":lovDatosTasa.get(0).toString();
			String lsTipoTasa	= lovDatosTasa.get(1)==null?"":lovDatosTasa.get(1).toString();
			String lsCveTasa	= lovDatosTasa.get(2)==null?"":lovDatosTasa.get(2).toString();
			String lsPlazo		= lovDatosTasa.get(3)==null?"":lovDatosTasa.get(3).toString();
			String lsValor		= lovDatosTasa.get(4)==null?"":lovDatosTasa.get(4).toString();
			String lsRelMat	= lovDatosTasa.get(5)==null?"":lovDatosTasa.get(5).toString();
			String lsPuntos	= lovDatosTasa.get(6)==null?"":lovDatosTasa.get(6).toString();
			String lsTasaPiso	= lovDatosTasa.get(7)==null?"":lovDatosTasa.get(7).toString();
			String lsFecha		= lovDatosTasa.get(8)==null?"":lovDatosTasa.get(8).toString();
			String lsEpo		= lovDatosTasa.get(9)==null?"":lovDatosTasa.get(9).toString();
			String lsPyme		= lovDatosTasa.get(10)==null?"":lovDatosTasa.get(10).toString();
			String lsTasaFinal= lovDatosTasa.get(11)==null?"":lovDatosTasa.get(11).toString();
			String lsFechaNego= lovDatosTasa.get(12)==null?"":lovDatosTasa.get(12).toString();

			String lsPuntosPref	= lovDatosTasa.get(15)==null?"":lovDatosTasa.get(15).toString();
			String lsTasaNego = "";
			String lsReferencia = "";

			lsTipoTasa		= lsTipoTasa+" ("+lsValor+") "+lsRelMat+" "+lsPuntos;
			double tasafinal = 0;
			if("P".equals(tipo_tasa)) {
				lsReferencia	= "Preferencial";
				lsTipoTasa = lsTipoTasa +" - "+lsPuntosPref;
				lsTasaNego 		= (lsValor);//?????????????????????????????????????????
			}
			else if("N".equals(tipo_tasa)) {
				lsReferencia	= "Negociada";
				lsTasaNego 		= Comunes.formatoDecimal(lsTasaFinal,4);
				lsTasaFinal		= lsTasaPiso;

			} else {
				lsReferencia	= "Base";
				lsTasaFinal		= lsTasaPiso;
			}
		if(i==0) {
			contenidoArchivo.append("\nEpo relacionada,Nombre de la PyME,Referencia,Tipo Tasa,Plazo,Valor,Valor Tasa");
		}
		contenidoArchivo.append("\n"+lsEpo.replace(',',' ')+","+lsPyme.replace(',',' ')+","+lsReferencia+","+lsTipoTasa+",  "+lsPlazo+","+Comunes.formatoDecimal(lsTasaFinal,4,false)+","+lsTasaNego);
	}

	if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivo = archivo.nombre;
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>