Ext.onReady(function() {

	var strPerfil = Ext.get('_strPerfil').getValue();
	var dt = Ext.util.Format.date(Date(),'d/m/Y');
	var form = {hayMasDeUnaEpoSeleccionada:null,	totales:null, cadEpo:null, textoF:null, doctosPorEpo:null,selecciona:null, acuse:null,  cg_responsable_int:null,   cg_tipo_cobranza:null,  cg_tipo_cobro_int:null, cadMeses1:null, cadMeses2:null }
        
        var operaContrato = Ext.getDom('operaContrato').value;
        
	function verificaPanel(){
		var myPanel = Ext.getCmp('formCampos');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype != 'label')	{	//El metodo isValid() no aplica para los tipos de objetos label.
				if (!panelItem.isValid())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('ventaCartera').setValue(info.ventaCartera);
			
			if( info.cadEpo === 'EpoCemex'){
				if(info.ventaCartera=='S')  {				
					form.cadEpo = 'Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesi�n de Derechos de Cobro del '+
						'(los) Documento (s) final (es) descrito (s) en el mismo derivado del DESCUENTO Y/O FACTORAJE CON RECURSO o del '+
						' DESCUENTO Y/O FACTORAJE SIN RECURSO seg�n corresponda. Dicha aceptaci�n tendr� plena validez para todos los efectos '+
						'legales a que haya lugar. En este mismo acto se genera el aviso de notificaci�n a la EMPRESA DE PRIMER ORDEN.  '+
						' En t�rminos de los art�culos 32 C del C�digo Fiscal y 427 de la Ley General de T�tulos y Operaciones de Cr�dito, '+
						'para los efectos legales conducentes, el INTERMEDIARIO FINANCIERO se obliga a notificar por sus propios medios al '+
						'CLIENTE Y/O DISTRIBUIDOR la transmisi�n de los derechos y/o cuentas por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO '+
						'o del DESCUENTO Y/O FACTORAJE SIN RECURSO. No obstante, en t�rminos de los citados art�culos, en caso de existir COBRANZA DELEGADA, '+
						'el INTERMEDIARIO FINANCIERO no estar� obligado a notificar al CLIENTE Y/O DISTRIBUIDOR la transmisi�n de los derechos y/o cuentas '+
						'por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO.<br/>'+
                        'Por otra parte, a partir del 17 de octubre de 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que s� ha emitido o emitir� a su CLIENTE o '+
                        'DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.';
                        
				}else  {		
										
                        form.cadEpo = 
						'Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesi�n de Derechos de Cobro del  '+
						' (los) Documento (s) final (es) descrito (s) en el mismo. Dicha aceptaci�n tendr� plena validez para '+
						' todos los efectos legales. En este mismo acto se genera el aviso de notificaci�n a la  '+
						' EMPRESA DE PRIMER ORDEN y al CLIENTE o DISTRIBUIDOR, por lo que tiene la validez de acuse de recibo, en t�rminos  '+
						' de los art�culos 32 C del C�digo Fiscal o  2038 y 2041 de C�digo Civil Federal , seg�n corresponda para  '+
						' los efectos legales conducentes.<br/>'+
                        'Por otra parte, a partir del 17 de octubre de 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que s� ha emitido o emitir� a su CLIENTE o '+
                        'DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.';                        
                if(info.operaContrato == 'S'){
                                             form.cadEpo = 
						'Al transmitir este mensaje de datos bajo mi responsabilidad acepto el financiamiento del(los) DOCUMENTO(S) FINAL(ES)'+
                                                ' descrito(s) en el mismo y de conformidad con el Contrato de Financiamiento a Clientes y Distribuidores. Dicha aceptaci�n '+
                                                ' tendr� plena validez para todos los efectos legales. ';
                                        }
				}
			}else{
				if(info.ventaCartera=='S')  {				
					form.cadEpo = 'Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesi�n de Derechos de Cobro del '+
						'(los) Documento (s) final (es) descrito (s) en el mismo derivado del DESCUENTO Y/O FACTORAJE CON RECURSO o del '+
						' DESCUENTO Y/O FACTORAJE SIN RECURSO seg�n corresponda. Dicha aceptaci�n tendr� plena validez para todos los efectos '+
						'legales a que haya lugar. En este mismo acto se genera el aviso de notificaci�n a la EMPRESA DE PRIMER ORDEN.  '+
						' En t�rminos de los art�culos 32 C del C�digo Fiscal y 427 de la Ley General de T�tulos y Operaciones de Cr�dito, '+
						'para los efectos legales conducentes, el INTERMEDIARIO FINANCIERO se obliga a notificar por sus propios medios al '+
						'CLIENTE Y/O DISTRIBUIDOR la transmisi�n de los derechos y/o cuentas por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO '+
						'o del DESCUENTO Y/O FACTORAJE SIN RECURSO. No obstante, en t�rminos de los citados art�culos, en caso de existir COBRANZA DELEGADA, '+
						'el INTERMEDIARIO FINANCIERO no estar� obligado a notificar al CLIENTE Y/O DISTRIBUIDOR la transmisi�n de los derechos y/o cuentas '+
						'por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO.';
					
				}else  {			
					form.cadEpo = 
						'Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesi�n de Derechos de Cobro del  '+
						' (los) Documento (s) final (es) descrito (s) en el mismo. Dicha aceptaci�n tendr� plena validez para '+
						' todos los efectos legales. En este mismo acto se genera el aviso de notificaci�n a la  '+
						' EMPRESA DE PRIMER ORDEN y al CLIENTE o DISTRIBUIDOR, por lo que tiene la validez de acuse de recibo, en t�rminos  '+
						' de los art�culos 32 C del C�digo Fiscal o  2038 y 2041 de C�digo Civil Federal , seg�n corresponda para  '+
						' los efectos legales conducentes. '+
                        'Por otra parte, a partir del 17 de octubre de 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que s� ha emitido o emitir� a su CLIENTE o '+
                        'DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.';
                                       if(info.operaContrato == 'S'){
                                             form.cadEpo = 
						'Al transmitir este mensaje de datos bajo mi responsabilidad acepto el financiamiento del(los) DOCUMENTO(S) FINAL(ES)'+
                                                ' descrito(s) en el mismo y de conformidad con el Contrato de Financiamiento a Clientes y Distribuidores. Dicha aceptaci�n '+
                                                ' tendr� plena validez para todos los efectos legales. ';
                                        }                  
                                                
				}
			}				
			
			Ext.getCmp('panMsg').body.update('<div class="formas" align="justify">'+form.cadEpo+'</div>');
							
			if(info.meseSinIntereses=='S')  {
				//Fodea 09-2015
				form.cadMeses1 ='NOTA: Los registros que se muestran en el grid de consulta en color AZUL, son los documentos que fueron financiados a meses sin intereses.';
				form.cadMeses2 ='NOTA: Los registros que se muestran en el grid de consulta en color AZUL, son los documentos que fueron financiados a meses sin intereses.';
				
				Ext.getCmp('fpMsgMeses2').body.update('<div class="formas" align="justify"> <font color="#58D3F7" >'+form.cadMeses2+' </font> </div>');				
				Ext.getCmp('fpMsgMeses2').show();
			}else  {
				Ext.getCmp('fpMsgMeses2').hide();
				
			}
			
			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarConfirmar(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnConfirma').hide();
			Ext.getCmp('btnCancela').hide();
			form.acuse = infoR.acuse;
			Ext.getCmp('disAcuse').body.update(infoR._acuse);
			Ext.getCmp('disFechaCarga').body.update(infoR.fechaCarga);
			Ext.getCmp('disHoraCarga').body.update(infoR.horaCarga);
			Ext.getCmp('disUsuario').body.update('<div class="formas" >'+infoR.usuario+'<div>');
			fpAcuse.show();
			toolbarB.show();
			
			if(infoR.acuse!='')  {
				Ext.Ajax.request({
					url	: '24forma01pdf_b_ext.jsp',
					params:	Ext.apply({	ic_pyme : Ext.getCmp('_ic_pyme').getValue(),
						acuse: infoR.acuse,
						hidTipoCredito:Ext.getCmp('tipoCredito').getValue(),
						cadEpo: form.cadEpo,
						selecciona:form.selecciona,
						doctosPorEpo:form.doctosPorEpo,
						accion:'acuse'
					}),
					callback: procesarSuccessFailureGenerarPDF_B
				});
				
			}
			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarAutorizar(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
                        var leyendaE = 'LEYENDA LEGAL';
                        if(infoR.operaContrato == 'S'){
                            leyendaE = 'Al transmitir este mensaje de datos bajo mi responsabilidad acepto el financiamiento del(los) '+
                            ' DOCUMENTO(S) FINAL(ES) descrito(s) en el mismo y de conformidad con el Contrato de Financiamiento a Clientes y Distribuidores. '+
                            ' Dicha aceptaci�n tendr� plena validez para todos los efectos legales.';
                        }
                         
                         
			fp.hide();
			fpCampos.hide();
			fpMsg.hide();
			gridUno.hide();
			fpB.hide();
			toolbar.hide();
			grid.hide();			
			Ext.getCmp('fpMsgMeses2').show();
			
			Ext.getCmp('gridTotales').hide();
			Ext.getCmp('msgC').body.update('<div class="formas" align="justify">'+form.cadEpo+'</div>');

			if (infoR.totales != undefined && infoR.totales.length > 0 ){
				var tdMN="",tmMN="",tdUSD="",tmUSD="",tdCONV="", tmCONV="";
				Ext.each(infoR.totales, function(item,index){
					tdMN	=	item.totalDoctosMN;
					tmMN	=	Ext.util.Format.number((parseFloat(item.totalMontoMN).toString()).replace(/[\$]/g, ''), '000.00');
					tdUSD	=	item.totalDoctosUSD;
					tmUSD	=	Ext.util.Format.number((parseFloat(item.totalMontoUSD).toString()).replace(/[\$]/g, ''), '000.00');
					tdCONV=	item.totalDoctosConv;
					tmCONV=	Ext.util.Format.number((parseFloat(item.totalMontoConv).toString()).replace(/[\$]/g, ''), '000.00');

					form.textoF =	" Num. Solic M.N. "+item.totalDoctosMN+"\n"+
										" Monto Solic M.N "+item.totalMontoMN+"\n"+
										" Num. Solic Dolares "+item.totalDoctosUSD+"\n"+
										" Monto Solic Dolares "+item.totalMontoUSD+"\n"+
										" Num. Solic en Dolares financiadas en M.N. "+item.totalDoctosConv+"\n"+
										" Monto Solic en Dolares financiadas en M.N. "+item.totalMontoConv+"\n"+
										leyendaE;//" LEYENDA LEGAL";
				});
				Ext.getCmp('tDocMN').body.update('<div class="formas">'+tdMN+'</div>');
				Ext.getCmp('tMonMN').body.update('<div class="formas">$ '+tmMN+'</div>');
				Ext.getCmp('tDocUSD').body.update('<div class="formas">'+tdUSD+'</div>');
				Ext.getCmp('tMonUSD').body.update('<div class="formas">$ '+tmUSD+'</div>');
				Ext.getCmp('tDocConv').body.update('<div class="formas">'+tdCONV+'</div>');
				Ext.getCmp('tMonConv').body.update('<div class="formas">$ '+tmCONV+'</div>');
			}

			var cm = gridB.getColumnModel();
			cm.setHidden(cm.findColumnIndex('NOMBRE_EPO'), true);
			cm.setHidden(cm.findColumnIndex('TIPO_FINANCIAMIENTO'), true);
			cm.setHidden(cm.findColumnIndex('CG_CAMPO1'), true);
			cm.setHidden(cm.findColumnIndex('CG_CAMPO2'), true);
			cm.setHidden(cm.findColumnIndex('CG_CAMPO3'), true);
			cm.setHidden(cm.findColumnIndex('CG_CAMPO4'), true);
			cm.setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
			var count = 1;
			Ext.each(infoR.nombresCampo, function(item, index, arrItems){
						cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO'+count.toString()), item[0]);
						cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO'+count.toString()), item[0]);
						cm.setHidden(cm.findColumnIndex('CG_CAMPO'+count.toString()), false);
					count ++;
			});
			if(form.hayMasDeUnaEpoSeleccionada){
				cm.setHidden(cm.findColumnIndex('TIPO_FINANCIAMIENTO'), false);
			}
			if(Ext.getCmp('tipoCredito').getValue() != 'D'){
				cm.setHidden(cm.findColumnIndex('NOMBRE_EPO'), false);
			}
			fpC.show();
			gridB.show();
			Ext.getCmp('gridTotalesB').show();			
						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarCuentaBancariaData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var gridCuenta = Ext.getCmp('gridCuenta');
			var el = gridCuenta.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontraron datos ', 'x-mask');
			}
		}
	}

	var procesarCambioDoctosData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var gridCambios = Ext.getCmp('gridCambios');
			var el = gridCambios.getGridEl();

			var cm = gridCambios.getColumnModel();

			cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'),true);
			cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), true);
			cm.setHidden(cm.findColumnIndex('MONTO_VAL'), true);
			cm.setHidden(cm.findColumnIndex('PORCENTAJE_AFORO'), true);
			
			if(Ext.getCmp('tipoCredito').getValue() === 'D'){
				cm.setHidden(cm.findColumnIndex('TIPO_CONVERSION'), false);
				cm.setHidden(cm.findColumnIndex('TIPO_CAMBIO'), false);
				cm.setHidden(cm.findColumnIndex('MONTO_VAL'), false);				
			}
				
			if(Ext.getCmp('tipoCredito').getValue() === 'D' &&  Ext.getCmp('ventaCartera').getValue() === 'S' ){
				cm.setHidden(cm.findColumnIndex('PORCENTAJE_AFORO'), false);
			}

			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ningun cambio para este documento', 'x-mask');
			}
		}
	}

	var muestraCuentaBancaria = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var ventana = Ext.getCmp('winBancaria');
		if (ventana) {
			ventana.destroy();
		}
		new Ext.Window({
			layout: 'fit',
			width: 900,
			height: 180,
			//modal: true,
			id: 'winBancaria',
			closeAction: 'hide',
			items: [
				gridCuentaBancaria
			],
			title: 'Cuenta Bancaria',
			bbar: {
				xtype: 'toolbar',buttons: ['->','-',{xtype: 'button',	text: 'Cerrar',id: 'btnCloseCta',handler:	function() {Ext.getCmp('winBancaria').destroy();}}]
			}
			}).show();
			cuentaBancariaData.load({params:	{ic_epo: registro.get('IC_EPO'), ic_pyme:registro.get('IC_PYME'), ic_moneda: registro.get('IC_MONEDA') }});
	}

	var muestraDetalleDocto = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var ic_documento = registro.get('IC_DOCUMENTO');

		var ventana = Ext.getCmp('cambioDoctos');
		if (ventana) {
			ventana.destroy();
		}
		new Ext.Window({
			layout: 'fit',
			width: 900,
			height: 180,
			id: 'cambioDoctos',
			closeAction: 'hide',
			items: [
				gridCambiosDoctos
			],
			title: 'Detalle del Documento',
			bbar: {
				xtype: 'toolbar',buttons: ['->','-',{xtype: 'button',	text: 'Cerrar',id: 'btnClose',handler:	function() {Ext.getCmp('cambioDoctos').destroy();}}]
			}
			}).show();
			cambioDoctosData.load({params:	{ic_docto:	ic_documento, tipo_credito:Ext.getCmp('tipoCredito').getValue() }});
	}

	var procesarSuccessFailureLayoutFactVto = function (opts, success, response) {
		var boton=Ext.getCmp('btnGenerarLayout');
		boton.setIconClass('icoXls');
		boton.enable();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else {
			boton.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo_B = function (opts, success, response) {
		var boton=null;
		var btnBajarArchivo = null;
		if(opts.params.tipoArchivo === 'ARCH'){

			boton = Ext.getCmp('btnGenerarArchivoB');
			btnBajarArchivo = Ext.getCmp('btnBajaCsvB');
			boton.setIconClass('icoXls');
			
		}else if(opts.params.tipoArchivo === 'VAR'){

			boton = Ext.getCmp('btnGeneraVarB');
			btnBajarArchivo = Ext.getCmp('btnBajaVarB');
			boton.setIconClass('icoXls');
			
		}else if(opts.params.tipoArchivo === 'FIJO'){

			boton = Ext.getCmp('btnGeneraFijoB');
			btnBajarArchivo = Ext.getCmp('btnBajaFijoB');
			boton.setIconClass('icoTxt');

		}else if(opts.params.tipoArchivo === 'INTEG'){

			boton = Ext.getCmp('btnGeneraIntegralB');
			btnBajarArchivo = Ext.getCmp('btnBajaIntegralB');
			boton.setIconClass('icoXls');

		}
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var jsonData = Ext.util.JSON.decode(response.responseText);		
			
			if(jsonData.accion=='button')  {
			
				btnBajarArchivo.show();
				btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarArchivo.setHandler ( function (boton,evento) {
					if(opts.params.tipoArchivo === 'FIJO' || opts.params.tipoArchivo ==='F'){
						var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo
						archivo = archivo.replace('/nafin','');
						var params = {nombreArchivo: archivo};
						fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
						fp.getForm().getEl().dom.submit();
					}else{
						var forma = Ext.getDom('formAux');
						forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
						forma.submit();
					}
				});
			}
			
		} else {
			boton.enable();
			if(opts.params.tipoArchivo === 'VAR'){
				Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			}else  {
				NE.util.mostrarConnError(response,opts);
			}
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var boton=null;
		var btnBajarArchivo = null;
		if(opts.params.tipoArchivo === 'ARCH' || opts.params.tipoArchivo ==='G' ){

			boton = Ext.getCmp('btnGenerarArchivo');
			btnBajarArchivo = Ext.getCmp('btnBajaCsv');
			boton.setIconClass('icoXls');
			
		}else if(opts.params.tipoArchivo === 'VAR' || opts.params.tipoArchivo ==='V' ){

			boton = Ext.getCmp('btnGeneraVar');
			btnBajarArchivo = Ext.getCmp('btnBajaVar');
			boton.setIconClass('icoXls');

		}else if(opts.params.tipoArchivo === 'FIJO' || opts.params.tipoArchivo ==='F' ){

			boton = Ext.getCmp('btnGeneraFijo');
			btnBajarArchivo = Ext.getCmp('btnBajaFijo');
			boton.setIconClass('icoTxt');

		}else if(opts.params.tipoArchivo === 'INTEGRAL' || opts.params.tipoArchivo ==='I' ){

			boton = Ext.getCmp('btnGeneraIntegral');
			btnBajarArchivo = Ext.getCmp('btnBajaIntegral');
			boton.setIconClass('icoXls');

		}
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {

				btnBajarArchivo.show();
				btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarArchivo.setHandler ( function (boton,evento) {
					if(opts.params.tipoArchivo === 'FIJO' || opts.params.tipoArchivo ==='F'){
						var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo
						archivo = archivo.replace('/nafin','');
						var params = {nombreArchivo: archivo};
						fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
						fp.getForm().getEl().dom.submit();
					}else{
						var forma = Ext.getDom('formAux');
						forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
						forma.submit();
					}
				});

		} else {
			boton.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDF_B =  function(opts, success, response) {
		Ext.getCmp('btnGenerarPDFB').setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonData = Ext.util.JSON.decode(response.responseText);	
			
			if(jsonData.accion=='button')  {	
			
				var btnBajarPDF = Ext.getCmp('btnBajaPdfB');
				btnBajarPDF.show();
				btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarPDF.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
			}else if(jsonData.accion=='acuse')  {	
			
				Ext.Ajax.request({
					url	: '24forma01arch2_ext.jsp',
					params:	Ext.apply({	
						tipoArchivo: 'VAR',
						acuse: form.acuse,
						ic_pyme : Ext.getCmp('_ic_pyme').getValue(),
						hidTipoCredito:Ext.getCmp('tipoCredito').getValue(),
						selecciona:form.selecciona,
						doctosPorEpo:form.doctosPorEpo,
						accion:'acuse'
					}),
					callback: procesarSuccessFailureGenerarArchivo_B
				});
				
			}
			
		} else {
			Ext.getCmp('btnGenerarPDFB').enable();
			Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			//NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajaPdf');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			Ext.getCmp('btnGenerarPDF').enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaConsulta(opts, success, response) {
		pnl.el.unmask();
		consultaUnoData.loadData('');
		consultaData.loadData('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.hayMasDeUnaEpoSeleccionada != undefined){
				form.hayMasDeUnaEpoSeleccionada = infoR.hayMasDeUnaEpoSeleccionada;
			}
			if (infoR.regsUno != undefined && infoR.regsUno.length > 0){
				consultaUnoData.loadData(infoR.regsUno);
				if (!gridUno.isVisible()){
					gridUno.show();
				}
				var cmU = gridUno.getColumnModel();
				if(Ext.getCmp('_tipo_credito').getValue() === '1'	||	Ext.getCmp('_tipo_credito').getValue() === '2'	){
					cmU.setColumnHeader(cmU.findColumnIndex('NOMBRE_EPO'),"Nombre del acreditado");
					//cmU.setColumnTooltip(cmU.findColumnIndex('NOMBRE_EPO'),"Nombre del acreditado");
				}else{
					cmU.setColumnHeader(cmU.findColumnIndex('NOMBRE_EPO'),"EPO");
					//cmU.setColumnTooltip(cmU.findColumnIndex('NOMBRE_EPO'),"EPO");
				}
			}
			if (!grid.isVisible()){
				grid.show();
			}
			if (infoR.regs != undefined && infoR.regs.length > 0){
				if (infoR.csTipoFondeo != undefined && infoR.csTipoFondeo==='P'){
					Ext.getCmp('msgFondeo').show();
				}
				consultaData.loadData(infoR.regs);
				selectModel.selectAll();

				var cm = grid.getColumnModel();

				cm.setHidden(cm.findColumnIndex('NOMBRE_EPO'),true);
				cm.setHidden(cm.findColumnIndex('TIPO_FINANCIAMIENTO'), true);
				cm.setHidden(cm.findColumnIndex('CUENTA_BANCARIA'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO1'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO2'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO3'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO4'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO5'), true);

				if(Ext.getCmp('tipoCredito').getValue() === 'D'){
					cm.setHidden(cm.findColumnIndex('CUENTA_BANCARIA'), false);
					Ext.getCmp('btnGenerarLayout').hide();
					cm.setColumnHeader(cm.findColumnIndex('TASA_INTERES'), "Tasa inter�s");
					cm.setColumnTooltip(cm.findColumnIndex('TASA_INTERES'), "Tasa inter�s");
				}else{
					cm.setHidden(cm.findColumnIndex('NOMBRE_EPO'),false);
					cm.setColumnHeader(cm.findColumnIndex('TASA_INTERES'), "Valor tasa de inter�s");
					cm.setColumnTooltip(cm.findColumnIndex('TASA_INTERES'), "Valor tasa de inter�s");
					if (infoR.nombresCampo!=undefined){
						var cont = 1;
						Ext.each(infoR.nombresCampo, function(item, index, arrItems){
									cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO'+cont), item[0]);
									cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO'+cont), item[0]);
									cm.setHidden(cm.findColumnIndex('CG_CAMPO'+cont), false);
								cont ++;
						});
					}
					Ext.getCmp('btnGenerarLayout').show();
				}
				if(form.hayMasDeUnaEpoSeleccionada){
					cm.setHidden(cm.findColumnIndex('TIPO_FINANCIAMIENTO'), false);
				}
				if (infoR.totales != undefined && infoR.totales.length > 0 ){
					form.totales = infoR.totales;
					Ext.each(infoR.totales, function(item,index){
						Ext.getCmp('totalDoctosMN').setValue(Ext.util.Format.number((parseFloat(item.totalDoctosMN).toString()).replace(/[\$]/g, ''), '000.00'));
						Ext.getCmp('totalMontoMN').setValue(Ext.util.Format.number((parseFloat(item.totalMontoMN).toString()).replace(/[\$]/g, ''), '000.00'));
						Ext.getCmp('totalPorSelMN').setValue(Ext.util.Format.number("0".replace(/[\$]/g, ''), '000.00'));
						Ext.getCmp('montoPorSelMN').setValue(Ext.util.Format.number("0".replace(/[\$]/g, ''), '000.00'));
						Ext.getCmp('totalDoctosUSD').setValue(Ext.util.Format.number((parseFloat(item.totalDoctosUSD).toString()).replace(/[\$]/g, ''), '000.00'));
						Ext.getCmp('totalMontoUSD').setValue(Ext.util.Format.number((parseFloat(item.totalMontoUSD).toString()).replace(/[\$]/g, ''), '000.00'));
						Ext.getCmp('totalPorSelUSD').setValue(Ext.util.Format.number("0".replace(/[\$]/g, ''), '000.00'));
						Ext.getCmp('montoPorSelUSD').setValue(Ext.util.Format.number("0".replace(/[\$]/g, ''), '000.00'));
					});
				}
				fpB.show();
				toolbar.show();
				grid.getGridEl().unmask();
				
				
				if (form.totales != undefined && form.totales.length > 0 ){
					resumenTotalesData.loadData('');
					var reg = Ext.data.Record.create(['TIPO_MONEDA', 'TOTAL_REGISTROS','TOTAL_MONTO','TOTAL_INTERES']);
					Ext.each(form.totales, function(item,index){
						if (parseFloat(item.totalDoctosMN) > 0){
							resumenTotalesData.add(	new reg({TIPO_MONEDA:'Total M.N',TOTAL_REGISTROS: item.totalDoctosMN,TOTAL_MONTO: item.totalMontoMN,TOTAL_INTERES: item.totalMontoIntMN}) );
							if (parseFloat(item.totalDoctosUSD) > 0){
								resumenTotalesData.add(	new reg({TIPO_MONEDA:'Total Dolares',TOTAL_REGISTROS: item.totalDoctosUSD,TOTAL_MONTO: item.totalMontoUSD,TOTAL_INTERES: item.totalMontoIntUSD}) );
							}
						}else{
							if (parseFloat(item.totalDoctosUSD) > 0){
								resumenTotalesData.add(	new reg({TIPO_MONEDA:'Total Dolares',TOTAL_REGISTROS: item.totalDoctosUSD,TOTAL_MONTO: item.totalMontoUSD,TOTAL_INTERES: item.totalMontoIntUSD}) );
							}
						}
					});
				}
				Ext.getCmp('gridTotales').show();
			}else{
				grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('gridTotales').hide();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarCatalogoEpoDist = function(store, arrRegistros, opts) {
			var datos=[];
			store.each(function(record){
				record.fields.each(function(field){
					if ( field.name === 'clave' && !Ext.isEmpty(record.get(field.name)) ){
						datos.push(record.get(field.name));
					}
				});
			},this);
			if (datos.length > 0){
				var claves = "";
				if(datos.length == 1){
					claves = "|"+datos[0];
				}else{
					claves = datos.join(',');
				}
				var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
				store.insert(	0,new reg({	clave: claves,	descripcion: 'Seleccionar Todos',	loadMsg: null	})	);
			}
	}

	var catalogoTipoData;
	if (strPerfil === "IF FACT RECURSO"){
		catalogoTipoData = new Ext.data.ArrayStore({
			 fields: ['clave', 'descripcion'],
			 data : [
				['2','Factoraje con recurso']
			 ]
		});
	}else{
		catalogoTipoData = new Ext.data.ArrayStore({
			fields: ['clave', 'descripcion'],
			data : [	['0','Modalidad 1 (Riesgo Empresa de Primer Orden )'],	['1','Modalidad 2 (Riesgo Distribuidor) ']	]
		}); 
	}

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPODist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoEpoDist,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoDistribuidor'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaUnoData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMBRE_EPO'},{name: 'LINEA_CREDITO'},{name: 'SALDO_INICIAL',type: 'float'},{name: 'MONEDA'},
			{name: 'MONTO_OPERADO',type: 'float'},{name: 'DOCTOS_OPERADOS',type: 'float'},{name: 'SALDO_DISPONIBLE',type: 'float'}
		],
		data:	[{'NOMBRE_EPO':'','LINEA_CREDITO':0,'SALDO_INICIAL':'','MONEDA':'','MONTO_OPERADO':0,'DOCTOS_OPERADOS':0,'SALDO_DISPONIBLE':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name:'IC_EPO'},{name:'DOCTO_FINAL'},{name:'IC_MONEDA'},{name:'IC_PYME'},{name:'TIPO_CONVERSION'},{name:'TIPO_CAMBIO',type: 'float'},
			{name:'FECHA_SOLICITUD'},{name:'NOMBRE_PYME'},{name:'NOMBRE_EPO'},{name:'MONEDA'},{name:'MONTO',type:'float'},{name:'REFERENCIA_TASA'},
			{name:'TASA_INTERES',type:'float'},{name:'PLAZO'},{name:'FECHA_VENCIMIENTO'},{name:'MONTO_INTERES',type:'float'},
			{name:'TIPO_INTERES'},{name:'CUENTA_BANCARIA'},{name:'IC_DOCUMENTO'},{name:'TIPO_FINANCIAMIENTO'},
			{name:'CG_CAMPO1'},{name:'CG_CAMPO2'},{name:'CG_CAMPO3'},{name:'CG_CAMPO4'},{name:'CG_CAMPO5'},
			{name:'IC_TIPO_COBRO_INTERES'}, {name:'CG_TIPO_COBRANZA'}, {name:'CG_RESPONSABLE_INTERES'}, {name: 'TIPOPAGO'} 
		],
		data:	[{'IC_EPO':'','DOCTO_FINAL':'','IC_MONEDA':'','IC_PYME':'','TIPO_CONVERSION':'','TIPO_CAMBIO':0,'FECHA_SOLICITUD':'','NOMBRE_PYME':'','NOMBRE_EPO':'','MONEDA':'','MONTO':0,'REFERENCIA_TASA':'',
					'TASA_INTERES':0,'PLAZO':'','FECHA_VENCIMIENTO':'','MONTO_INTERES':0,'TIPO_INTERES':'','CUENTA_BANCARIA':'','IC_DOCUMENTO':'','TIPO_FINANCIAMIENTO':'',
					'CG_CAMPO1':'','CG_CAMPO2':'','CG_CAMPO3':'','CG_CAMPO4':'','CG_CAMPO5':''},
					{name:'IC_TIPO_COBRO_INTERES'}, {name:'CG_TIPO_COBRANZA'}, {name:'CG_RESPONSABLE_INTERES'}, {name: 'TIPOPAGO'} 
				],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		fields: [{name: 'TIPO_MONEDA'},{name: 'TOTAL_REGISTROS', type: 'float'},{name: 'TOTAL_MONTO', type: 'float'},{name: 'TOTAL_INTERES', type: 'float'}],
		data:	[{'TIPO_MONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO':'','TOTAL_INTERES':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var cuentaBancariaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma01ext.data.jsp',
		baseParams: {informacion: 'obtenCuentaBancaria'},
		fields: [	{name: 'NOMPYME'},{name: 'NEPYME'},{name: 'PYMERFC'},{name: 'NOCUENTA'},{name: 'SUCURSAL'},{name: 'PLAZA'}	],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCuentaBancariaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarCuentaBancariaData(null, null, null);
				}
			}
		}
	});

	var cambioDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma01ext.data.jsp',
		baseParams: {informacion: 'obtenCambioDoctos'},
		fields: [
			{name: 'EPO'},
			{name: 'CC_ACUSE'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'FECHA_PUBLICACION'},
			{name: 'FECHA_EMISION'	},
			{name: 'FECHA_VENC'		},
			{name: 'IG_PLAZO_DOCTO'},
			{name: 'FN_MONTO',	type: 'float'},
			{name: 'MONEDA'},
			{name: 'IG_PLAZO_DESCUENTO'},
			{name: 'FN_PORC_DESCUENTO', type: 'float'},
			{name: 'TIPO_CONVERSION'},
			{name: 'TIPO_CAMBIO'},
			{name: 'MODO_PLAZO'},
			{name: 'MONTO_VAL'},
			{name: 'MONEDA_DOCTO'},
			{name:'IC_TIPO_COBRO_INTERES'}, 
			{name:'CG_TIPO_COBRANZA'}, 
			{name:'CG_RESPONSABLE_INTERES'},
			{name:'PORCENTAJE_AFORO'},
			{name:'TIPOPAGO'}			
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCambioDoctosData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarCambioDoctosData(null, null, null);
				}
			}
		}
	});

///***********--- - - - - -  Componentes  - - - - - - ******************/

	var elementosAcuse = [
		{
			xtype: 'panel',layout:'table',	width:500,	border:true,	layoutConfig:{ columns: 2 },
			defaults: {frame:false, border: true,width:160, height: 35,bodyStyle:'padding:8px'},
			items:[
				{	width:500,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Datos de cifras de control</div>'	},
				{	html:'<div class="formas" align="left">N�m. acuse</div>'	},
				{	width:340,id:'disAcuse',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Fecha</div>'	},
				{	width:340,id:'disFechaCarga',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Hora</div>'	},
				{	width:340,id:'disHoraCarga',	html:'&nbsp;'},
				{	html:'<div class="formas" align="left">Clave y n�mero de usuario</div>'	},
				{	width:340,id:'disUsuario',html: '&nbsp;'	}
			]
		}
	];

	var fpAcuse=new Ext.FormPanel({	style: 'margin:0 auto;',	height:'auto',	width:502,	border:true,	frame:false,	items:elementosAcuse,	hidden:true 	});

	var gridCuentaBancaria = {
		xtype: 'grid',	store: cuentaBancariaData,	id: 'gridCuenta',	height: 300,	title: '',	frame: false,	loadMask: true,
		columns: [
			{
				header:'Distribuidor',	tooltip:'Distribuidor',	dataIndex:'NOMPYME',	sortable:true,	resizable:true,	width:200,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header: 'N�mero electr�nico',dataIndex: 'NEPYME',align: 'center',	width: 150
			},{
				header: 'RFC',dataIndex: 'PYMERFC',align: 'center',	width: 120
			},{
				header: 'Cuenta',	dataIndex: 'NOCUENTA', align: 'center',	width: 150
			},{
				header: 'Sucursal',dataIndex: 'SUCURSAL',align: 'center',	width: 150
			},{
				header: 'Plaza',dataIndex: 'PLAZA',align: 'center',	width: 150
			}
		]
	};

	var gridCambiosDoctos = {
		xtype: 'grid',	store: cambioDoctosData,	id: 'gridCambios',	height: 300,	title: '',	frame: false,	loadMask: true,
		columns: [
			{
				header:'Epo',	tooltip:'Epo',	dataIndex:'EPO',	sortable:true,	resizable:true,	width:130,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header: 'N�mero de Acuse de Carga',tooltip:'N�mero de Acuse de Carga',dataIndex: 'CC_ACUSE',align: 'center',	width: 120
			},{
				header: 'N�mero de Documento inicial',tooltip:'N�mero de Documento inicial',dataIndex: 'IG_NUMERO_DOCTO',align: 'center',	width: 120
			},{
				header: 'Fecha de publicaci�n',	tooltip:'Fecha de publicaci�n',dataIndex: 'FECHA_PUBLICACION', align: 'center',	width: 100
			},{
				header: 'Fecha de Emisi�n',	tooltip:'Fecha de Emisi�n',dataIndex: 'FECHA_EMISION', align: 'center',	width: 100
			},{
				header: 'Fecha de Vencimiento',	tooltip:'Fecha de Vencimiento',dataIndex: 'FECHA_VENC', align: 'center',	width: 100
			},{
				header: 'Plazo del documento',tooltip:'Plazo del documento',dataIndex: 'IG_PLAZO_DOCTO',align: 'center',	width: 120
			},{
				header: 'Monto',tooltip:'Monto',dataIndex: 'FN_MONTO',align: 'right',	width: 120,renderer: Ext.util.Format.numberRenderer('$0,00.00')
			},{
				header: 'Moneda',tooltip:'Moneda',dataIndex: 'MONEDA',align: 'center',	width: 120
			},{
				header: 'Plazo para descuento',tooltip:'Plazo para descuento',dataIndex: 'IG_PLAZO_DESCUENTO',align: 'center',	width: 120
			},{
				header: 'Monto % de descuento',tooltip:'Monto % de descuento',dataIndex: 'FN_PORC_DESCUENTO',align: 'right',	width: 120, renderer: Ext.util.Format.numberRenderer('$0,00.00')
			},{
				header: 'Tipo de Conversi�n',tooltip:'Tipo de Conversi�n',dataIndex: 'TIPO_CONVERSION',align: 'center',	width: 120
			},{
				header: 'Tipo de cambio',tooltip:'Tipo de cambio',dataIndex: 'TIPO_CAMBIO',align: 'center',	width: 120
			},{
				header: 'Monto Valuado',tooltip:'Monto Valuado',dataIndex: 'MONTO_VAL',align: 'right',	width: 120
			},{
				header: 'Modalidad de Plazo',	tooltip:'Modalidad de Plazo',dataIndex: 'MODO_PLAZO',	align: 'center',	width: 150
			},
			{
				header: '% de Aforo',	tooltip:'% de Aforo',dataIndex: 'PORCENTAJE_AFORO',	align: 'center',	width: 150, renderer: Ext.util.Format.numberRenderer('0.00%')
			}
		]
	};

	var elementosC = [
		{
			xtype: 'panel',layout:'table',	width:600,	border:true,	layoutConfig:{ columns: 6 },
			defaults: {frame:false, border: true,width:100, height: '30',bodyStyle:'padding:1px'},
			items:[
				{	colspan:2,	width:200,frame:true,	border:false,	html:'<div align="center">Moneda Nacional<br>&nbsp;</div>'	},
				{	colspan:2,	width:200,frame:true,	border:false,	html:'<div align="center">D�lares<br>&nbsp;</div>'	},
				{	colspan:2,	width:200,frame:true,	border:false,	html:'<div align="center">Documentos en DLL <br>financiados en M.N.</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">N�mero de solicitudes procesadas</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">Monto de solicitudes procesadas</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">N�mero de solicitudes procesadas</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">Monto de solicitudes procesadas</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">N�mero de solicitudes procesadas</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">Monto de solicitudes procesadas</div>'	},
				{	id:'tDocMN',html:''	},
				{	id:'tMonMN',html:''	},
				{	id:'tDocUSD',html:''	},
				{	id:'tMonUSD',html:''	},
				{	id:'tDocConv',html:''	},
				{	id:'tMonConv',html:''	},
				{	colspan:6,	width:600,	id:'msgC',	html:''	}
			]
		}
	];

	var confirmaAcuse = function(pkcs7, textoFirma){
	
	if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnConfirma").enable();	
			Ext.Msg.alert('Firmar','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;
		}else  {			
			Ext.getCmp("btnConfirma").disable();
			
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url : '24forma01ext.data.jsp',
				params: Ext.apply({	informacion: 'Confirmar',
					pkcs7: pkcs7, 
					textoFirmado: textoFirma,
					ic_pyme : Ext.getCmp('_ic_pyme').getValue(),
					hidTipoCredito:Ext.getCmp('tipoCredito').getValue(),
					selecciona:form.selecciona,
					doctosPorEpo:form.doctosPorEpo,
					cg_responsable_int:form.cg_responsable_int,
					cg_tipo_cobranza:form.cg_tipo_cobranza,
					cg_tipo_cobro_int:form.cg_tipo_cobro_int,
					ic_epo: Ext.getCmp('_ic_epo').getValue()																
				}),
				callback: procesarConfirmar
			});			
		}
	}
	var fpC	=	new Ext.FormPanel({
		style: 'margin:0 auto;',	autoHeight:true,	bodyStyle:'padding:1px',	width:604,	border:true,	frame:false,	items:elementosC,	hidden:true,
		buttons:[
			{
				text:'Confirmar',	id:'btnConfirma',	iconCls:'icoAceptar',
				handler:	function(){
				
				
						NE.util.obtenerPKCS7(confirmaAcuse, form.textoF);
								
				}
			},{
				text:'Cancelar',	id:'btnCancela',	iconCls:'icoRechazar',
				handler:	function(boton){
								window.location = '24forma01ext.jsp';
							}
			}
		]
	});

	var fpFondeo	=	new Ext.Panel({id:'msgFondeo',	style: 'margin:0 auto;',	autoHeight:true,	bodyStyle:'padding:5px',	width:940,	border:false,	frame:false, hidden:true,
										html:'<div align="center" class="formas"><b><font color="red">Las siguientes solicitudes de distribuidores se realizar&aacute;n con Fondeo Propio</font></b></div>'});
	var elementosB = [
		{
			xtype: 'panel',layout:'table',	width:940,	border:true,	layoutConfig:{ columns: 5 },
			defaults: {frame:false, border: true,width:200, height: 30,bodyStyle:'padding:2px'},
			items:[
				{	width:140,frame:true,	border:false,	html:'<div align="center">&nbsp;</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">N�mero de solicitudes seleccionadas</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">Monto de solicitudes seleccionadas</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">N�mero de solicitudes por seleccionar</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">Monto de solicitudes por seleccionar</div>'	},
				{	width:140,	frame:true,	height: 33,html:'<div align="center">Moneda nacional</div>'	},
				{	bodyStyle:'padding:8px', layout:'form',labelWidth:20, height: 40,
					items:[{xtype:'numberfield', id:'totalDoctosMN',	name:'totalDoctosMN',	readOnly:true, value:0}]},
				{	bodyStyle:'padding:8px', layout:'form',labelWidth:20, height: 40,
					items:[{xtype:'textfield', id:'totalMontoMN',	name:'totalMontoMN',	readOnly:true,allowPureDecimal:true,value:0,
						listeners:{
							afterrender: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  },
							change: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  }
						}
					}]	
				},
				{	bodyStyle:'padding:8px', layout:'form',labelWidth:20,height: 40,items:[{xtype:'numberfield', id:'totalPorSelMN',	name:'totalPorSelMN',	readOnly:true, value:0}]	},
				{	bodyStyle:'padding:8px', layout:'form',labelWidth:20, height: 40,
					items:[{xtype:'textfield', id:'montoPorSelMN',	name:'montoPorSelMN',	readOnly:true, value:0,
						listeners:{
							afterrender: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  },
							change: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  }
						}
					}]	
				},
				{	width:140,height: 33,	frame:true,	html:'<div align="center">D�lares</div>'	},
				{	bodyStyle:'padding:8px', layout:'form',labelWidth:20,height: 40,items:[{xtype:'numberfield', id:'totalDoctosUSD',	name:'totalDoctosUSD',	readOnly:true, value:0}]	},
				{	bodyStyle:'padding:8px' ,layout:'form',labelWidth:20,height: 40,
					items:[{xtype:'textfield', id:'totalMontoUSD',	name:'totalMontoUSD',	readOnly:true, value:0,
						listeners:{
							afterrender: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  },
							change: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  }
						}
					}]	
				},
				{	bodyStyle:'padding:8px', layout:'form',labelWidth:20,height: 40,items:[{xtype:'numberfield', id:'totalPorSelUSD',	name:'totalPorSelUSD',	readOnly:true, value:0}]	},
				{	bodyStyle:'padding:8px', layout:'form',labelWidth:20,height: 40,
					items:[{xtype:'textfield', id:'montoPorSelUSD',	name:'montoPorSelUSD',	readOnly:true, value:0,
						listeners:{
							afterrender: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  },
							change: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  }
						}
					}]
				}
			]
		}
	];

	var fpB	=	new Ext.FormPanel({style: 'margin:0 auto;',	autoHeight:true,	bodyStyle:'padding:1px',	width:944,	border:true,	frame:false,	items:elementosB,	hidden:true});

	var selectModel = new Ext.grid.CheckboxSelectionModel({
		header:'',
		dataIndex:'SELECCION',
		width:	29,
		singleSelect: false,
      checkOnly: true,
		listeners: {
			rowdeselect:function(selectModel, rowIndex, record) {
								if(record.data['IC_MONEDA'] === '1'){
									var totalDoctosMN	= parseFloat(Ext.getCmp('totalDoctosMN').getValue());
									var totalMontoMN	= parseFloat(Ext.getCmp('totalMontoMN').getValue());
									var totalPorSelMN	= parseFloat(Ext.getCmp('totalPorSelMN').getValue());
									var montoPorSelMN	= parseFloat(Ext.getCmp('montoPorSelMN').getValue());
									Ext.getCmp('totalDoctosMN').setValue(Ext.util.Format.number(((totalDoctosMN-1).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('totalMontoMN').setValue(Ext.util.Format.number(((totalMontoMN-parseFloat(record.data['MONTO'])).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('totalPorSelMN').setValue(Ext.util.Format.number(((totalPorSelMN+1).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('montoPorSelMN').setValue(Ext.util.Format.number(((montoPorSelMN+parseFloat(record.data['MONTO'])).toString()).replace(/[\$]/g, ''), '000.00'));
								}else{
									var totalDoctosUSD	= parseFloat(Ext.getCmp('totalDoctosUSD').getValue());
									var totalMontoUSD	= parseFloat(Ext.getCmp('totalMontoUSD').getValue());
									var totalPorSelUSD	= parseFloat(Ext.getCmp('totalPorSelUSD').getValue());
									var montoPorSelUSD	= parseFloat(Ext.getCmp('montoPorSelUSD').getValue());
									Ext.getCmp('totalDoctosUSD').setValue(Ext.util.Format.number(((totalDoctosUSD-1).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('totalMontoUSD').setValue(Ext.util.Format.number(((totalMontoUSD-parseFloat(record.data['MONTO'])).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('totalPorSelUSD').setValue(Ext.util.Format.number(((totalPorSelUSD+1).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('montoPorSelUSD').setValue(Ext.util.Format.number(((montoPorSelUSD+parseFloat(record.data['MONTO'])).toString()).replace(/[\$]/g, ''), '000.00'));
								}
							},
			rowselect:function(selectModel, rowIndex, record) {
								if(record.data['IC_MONEDA'] === '1'){
									var totalDoctosMN	= parseFloat(Ext.getCmp('totalDoctosMN').getValue());
									var totalMontoMN	= parseFloat(Ext.getCmp('totalMontoMN').getValue());
									var totalPorSelMN	= parseFloat(Ext.getCmp('totalPorSelMN').getValue());
									var montoPorSelMN	= parseFloat(Ext.getCmp('montoPorSelMN').getValue());
									Ext.getCmp('totalDoctosMN').setValue(Ext.util.Format.number(((totalDoctosMN+1).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('totalMontoMN').setValue(Ext.util.Format.number(((totalMontoMN+parseFloat(record.data['MONTO'])).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('totalPorSelMN').setValue(Ext.util.Format.number(((totalPorSelMN-1).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('montoPorSelMN').setValue(Ext.util.Format.number(((montoPorSelMN-parseFloat(record.data['MONTO'])).toString()).replace(/[\$]/g, ''), '000.00'));
								}else{
									var totalDoctosUSD= parseFloat(Ext.getCmp('totalDoctosUSD').getValue());
									var totalMontoUSD	= parseFloat(Ext.getCmp('totalMontoUSD').getValue());
									var totalPorSelUSD= parseFloat(Ext.getCmp('totalPorSelUSD').getValue());
									var montoPorSelUSD= parseFloat(Ext.getCmp('montoPorSelUSD').getValue());
									Ext.getCmp('totalDoctosUSD').setValue(Ext.util.Format.number(((totalDoctosUSD+1).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('totalMontoUSD').setValue(Ext.util.Format.number(((totalMontoUSD+parseFloat(record.data['MONTO'])).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('totalPorSelUSD').setValue(Ext.util.Format.number(((totalPorSelUSD-1).toString()).replace(/[\$]/g, ''), '000.00'));
									Ext.getCmp('montoPorSelUSD').setValue(Ext.util.Format.number(((montoPorSelUSD-parseFloat(record.data['MONTO'])).toString()).replace(/[\$]/g, ''), '000.00'));
								}
							}
		}
    });

	var gridB = new Ext.grid.GridPanel({
		id:'gridB',store: consultaData,	columLines:true, clicksToEdit:1, hidden:true,
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
		columns: [
			{
				header:'Num. Documento Final',	
				tooltip:'Num. Documento Final',	
				dataIndex:'IC_DOCUMENTO',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {		
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}				
			},
			{
				header:'EPO Relacionada',	
				tooltip:'EPO Relacionada',	
				dataIndex:'NOMBRE_EPO',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center', 
				hidden:true,
				hideable:false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
							
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}	
				}
			},{
				header:'Moneda',	
				tooltip:'Moneda',	
				dataIndex:'MONEDA',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}	
				}
			},
			{
				header : 'Monto', 
				tooltip: 'Monto',	
				dataIndex : 'MONTO',
				sortable : true, 
				width : 100, 
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00'), 
				hidden: false, 
				hideable: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+Ext.util.Format.number(value, '$0,0.00')+' </font>';
					}else {		
						return Ext.util.Format.number(value, '$0,0.00');
					}	
				}				
			},
			{
				header:'Referencia Tasa de inter�s',	
				tooltip:'Referencia Tasa de inter�s',	
				dataIndex:'REFERENCIA_TASA',	
				sortable:true,
				resizable:true,	
				width:130,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+''+' </font>';
					}else {		
						return value;
					}	
				}
			},
			{
				header:'Tasa inter�s',	
				tooltip:'Tasa inter�s',	
				dataIndex:'TASA_INTERES',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+Ext.util.Format.number(value, '0.0000 %')+' </font>';
					}else {		
						return Ext.util.Format.number(value, '0.0000 %');
					}	
				}
			},
			{
				header:'Plazo',	
				tooltip:'Plazo',	
				dataIndex:'PLAZO',	
				sortable:true,	
				resizable:true,	
				width:80,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' (M) </font>';
					}else {		
						return value;
					}	
				}				
			},
			{
				header : 'Fecha de vencimiento', 
				tooltip: 'Fecha de vencimiento',	
				dataIndex : 'FECHA_VENCIMIENTO',
				sortable : true, 
				width : 100, 
				align: 'center', 
				hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}	
				}
			},
			{
				header : 'Monto inter�s', 
				tooltip: 'Monto inter�s',	
				dataIndex : 'MONTO_INTERES',
				sortable : true, 
				width : 100, 
				align: 'right', 				
				hidden: false, 
				hideable: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+Ext.util.Format.number(value, '$0,0.00')+' </font>';
					}else {		
						return Ext.util.Format.number(value, '$0,0.00');
					}	
				}				
			},
			{
				header:'Tipo de cobro de intereses',	
				tooltip:'Tipo de cobro de intereses',	
				dataIndex:'TIPO_INTERES',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}	
				}
			},
			{
				header : '', 
				tooltip: '',	
				dataIndex : 'CG_CAMPO1',
				width : 120, 
				sortable : true, 
				resizable: true, 
				hidden: true,
				hideable:false,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (	Ext.isEmpty(record.get('CG_CAMPO1')) ){
						value = "N/A";
					}
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}	
				}
			},
			{
				header : '', 
				tooltip: '',	
				dataIndex : 'CG_CAMPO2',
				width : 120, 
				sortable : true, 
				resizable: true, 
				hidden: true,
				hideable: false,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex) {
					if (Ext.isEmpty(record.get('CG_CAMPO2'))){
						value = "N/A";
					}
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}	
				}
			},
			{
				header : '', 
				tooltip: '',	
				dataIndex : 'CG_CAMPO3',
				width : 120, 
				sortable : true, 
				resizable: true, 
				hidden: true,
				hideable: false,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex) {
					if (Ext.isEmpty(record.get('CG_CAMPO3')))	{
						value = "N/A";
					}
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			},
			{
				header : '', 
				tooltip: '',	
				dataIndex : 'CG_CAMPO4',
				width : 120, 
				sortable : true, 
				resizable: true,
				hidden: true,
				hideable: false,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex) {
					if (Ext.isEmpty(record.get('CG_CAMPO4'))){
						value = "N/A";
					}
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			},
			{
				header : '', 
				tooltip: '',	
				dataIndex : 'CG_CAMPO5',
				width : 120, 
				sortable : true, 
				resizable: true,
				hidden: true,
				hideable: false,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex) {
					if (Ext.isEmpty(record.get('CG_CAMPO5'))){
						value = "N/A";
					}
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			},
			{
				header:'Tipo de Financiamiento',	
				tooltip:'Tipo de Financiamiento',	
				dataIndex:'TIPO_FINANCIAMIENTO',	
				sortable:true,	
				resizable:true,	
				width:150,	
				align:'center',
				hidden:true,
				hideable:false,
				renderer:function(value, metadata, record, rowindex, colindex) {
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			}
		]	
	});

	var gridTotalesB = {
		xtype:'grid',	id: 'gridTotalesB',	store: resumenTotalesData,	margins: '20 0 0 0',	title: 'Totales',	hidden: true,	height: 100,	width : 940,	frame: false,
		view: new Ext.grid.GridView({markDirty: false, forceFit:true}),
		columns: [
			{
				header: 'Totales',	dataIndex: 'TIPO_MONEDA',	width: 200,	align: 'left'
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 200,	align: 'center'
			},{
				header: 'Monto total de documentos',	dataIndex: 'TOTAL_MONTO',	width: 200,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto total de intereses',	dataIndex: 'TOTAL_INTERES',width: 200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		]
	};

	var grid = new Ext.grid.EditorGridPanel({
		id:'gridIni',store: consultaData,	columLines:true, clicksToEdit:1, sm: selectModel, hidden:true,
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			selectModel,
			{
				xtype:'actioncolumn',
				header:'Num. Documento Final',	
				tooltip:'Num. Documento Final',	
				dataIndex:'IC_DOCUMENTO',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {				
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+(record.get('IC_DOCUMENTO'))+' </font>';
					}else {
						return (record.get('IC_DOCUMENTO'));
					}
				}, 
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	handler: muestraDetalleDocto
					}
				]
			},
			{
				header : 'Fecha solicitud', 
				tooltip: 'Fecha solicitud',	
				dataIndex : 'FECHA_SOLICITUD',
				sortable : true, 
				width : 100, 
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if( record.get('TIPOPAGO') ==2)  {
					return ' <font color="#58D3F7" >'+value+' </font>';
					}else {
						return value;
					}
				}
			},{
				header:'EPO Relacionada',	
				tooltip:'EPO Relacionada',	
				dataIndex:'NOMBRE_EPO',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center', 
				hidden:true,
				hideable:false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {		
				
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			},{
				header: 'Distribuidor',	
				tooltip: 'Distribuidor',	
				dataIndex:'NOMBRE_PYME',	
				sortable:true,	
				resizable:true,	
				width:200,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {						
						return value;
					}
				
				}
			},{
				header:'Moneda',	
				tooltip:'Moneda',	
				dataIndex:'MONEDA',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {		
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			},
			{
				header : 'Monto', 
				tooltip: 'Monto',	
				dataIndex : 'MONTO',
				sortable : true, 
				width : 100, 
				align: 'right', 				
				hidden: false, 
				hideable: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {		
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+Ext.util.Format.number(value, '$0,0.00')+' </font>';
					}else {		
						return Ext.util.Format.number(value, '$0,0.00');
					}
				}				
			},
			{
				header:'Referencia Tasa de inter�s',	
				tooltip:'Referencia Tasa de inter�s',	
				dataIndex:'REFERENCIA_TASA',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {		
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+''+' </font>';
					}else {		
						return value;
					}
				}				
			},
			{
				header:'Tasa inter�s',	
				tooltip:'Tasa inter�s',	
				dataIndex:'TASA_INTERES',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center',				
				renderer:function(value, metadata, record, rowindex, colindex, store) {		
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+Ext.util.Format.number(value, '0.0000 %')+' </font>';
					}else {		
						return Ext.util.Format.number(value, '0.0000 %');
					}
				}				
			},{
				header:'Plazo',	
				tooltip:'Plazo',	
				dataIndex:'PLAZO',	
				sortable:true,	
				resizable:true,	
				width:80,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {		
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+'(M) </font>';
					}else {		
						return value;
					}
				}					
			},{
				header : 'Fecha de vencimiento', 
				tooltip: 'Fecha de vencimiento',	
				dataIndex : 'FECHA_VENCIMIENTO',
				sortable : true, 
				width : 100, 
				align: 'center', 
				hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {		
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			},
			{
				header : 'Monto inter�s', 
				tooltip: 'Monto inter�s',	
				dataIndex : 'MONTO_INTERES',
				sortable : true, 
				width : 100,
				align: 'right', 				
				hidden: false, 
				hideable: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {		
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+Ext.util.Format.number(value, '$0,0.00')+' </font>';
					}else {		
						return Ext.util.Format.number(value, '$0,0.00');
					}
				}
				
			},
			{
				header:'Tipo de pago intereses',	
				tooltip:'Tipo de pago intereses',	
				dataIndex:'TIPO_INTERES',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {		
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}				
			},
			{
				xtype:'actioncolumn',
				header:'Cuenta Bancaria Distribuidor',	
				tooltip:'Cuenta Bancaria Distribuidor',	
				dataIndex:'CUENTA_BANCARIA',	
				sortable:true,	
				resizable:true,	
				width:130,	
				align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					return (record.get('CUENTA_BANCARIA'));
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							
						},	handler: muestraCuentaBancaria
					}
				]
			},{
				header : '', 
				tooltip: '',	
				dataIndex : 'CG_CAMPO1',
				width : 100, 
				sortable : true, 
				resizable: true, 
				hidden: true,
				hideable: false,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (	Ext.isEmpty(record.get('CG_CAMPO1')) ){
						value = "N/A";
					}
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}					
				}
			},
			{
				header : '', 
				tooltip: '',	
				dataIndex : 'CG_CAMPO2',
				width : 100, 
				sortable : true, 
				resizable: true, 
				hidden: true,
				hideable:false,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex) {
					if (Ext.isEmpty(record.get('CG_CAMPO2'))){
						value = "N/A";
					}
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			},{
				header : '', 
				tooltip: '',	
				dataIndex : 'CG_CAMPO3',
				width : 100, 
				sortable : true, 
				resizable: true, 
				hidden: true,
				hideable: false,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex) {
					if (Ext.isEmpty(record.get('CG_CAMPO3')))	{
						value = "N/A";
					}
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			},{
				header : '', 
				tooltip: '',	
				dataIndex : 'CG_CAMPO4',
				width : 100, 
				sortable : true, 
				resizable: true, 
				hidden: true,
				hideable: false,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex) {
					if (Ext.isEmpty(record.get('CG_CAMPO4'))){
						value = "N/A";
					}
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			},{
				header : '', 
				tooltip: '',	
				dataIndex : 'CG_CAMPO5',
				width : 100, 
				sortable : true, 
				resizable: true, 
				hidden: true,
				hideable: false,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex) {
					if (Ext.isEmpty(record.get('CG_CAMPO5'))){
						value = "N/A";
					}
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			},
			{
				header:'Tipo de Financiamiento',	
				tooltip:'Tipo de Financiamiento',	
				dataIndex:'TIPO_FINANCIAMIENTO',	
				sortable:true,	
				resizable:true,	
				width:150,	
				align:'center',
				hidden:true,
				hideable:false,
				renderer:function(value, metadata, record, rowindex, colindex) {
					
					if( record.get('TIPOPAGO') ==2)  {
						return ' <font color="#58D3F7" >'+value+' </font>';
					}else {		
						return value;
					}
				}
			}
		]
	});

	var gridTotales = {
		xtype:'grid',	id: 'gridTotales',	store: resumenTotalesData,	margins: '20 0 0 0',	title: 'Totales',	hidden: true,	height: 100,	width : 940,	frame: false,
		view: new Ext.grid.GridView({markDirty: false, forceFit:true}),		
		columns: [
			{
				header: 'Totales',	dataIndex: 'TIPO_MONEDA',	width: 200,	align: 'left'
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 200,	align: 'center'
			},{
				header: 'Monto total de documentos',	dataIndex: 'TOTAL_MONTO',	width: 200,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto total de intereses',	dataIndex: 'TOTAL_INTERES',width: 200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		]
	};

	var gridUno = new Ext.grid.GridPanel({
		id:'gridUno',store: consultaUnoData,	columLines:true, viewConfig: {forceFit: true}, hidden:true,
		stripeRows:true,	loadMask:true,	height:100,	width:940,	style:'margin:0 auto;',	frame:false,	
		columns: [
			{
				header:'EPO',	tooltip:'',	dataIndex:'NOMBRE_EPO',	sortable:true,	resizable:true,	width:130,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header : 'Num. L�nea de cr�dito', tooltip: 'Num. L�nea de cr�dito',	dataIndex : 'LINEA_CREDITO',sortable : true, width : 130, align: 'center'
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header : 'Saldo Inicial', tooltip: 'Saldo Inicial',	dataIndex : 'SALDO_INICIAL',sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Monto Operado', tooltip: 'Monto Operado',	dataIndex : 'MONTO_OPERADO',
				sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header:'Num. Doctos. operados',	tooltip:'Num. Doctos. operados',	dataIndex:'DOCTOS_OPERADOS',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header : 'Saldo Disponible', tooltip: 'Saldo Disponible',	dataIndex : 'SALDO_DISPONIBLE',sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		]
	});

	var elementosForma =[
		{
			xtype:'panel',
			layout:'form',
			frame:true,
			bodyStyle: 'padding: 5px 70px 10px 100px',
			defaults : {
				anchor : '-20'
			},
			items:[
				{
					xtype: 'combo',
					id:	'_tipo_credito',
					name: 'tipo_credito',
					hiddenName : 'tipo_credito',
					fieldLabel: 'Tipo de Cr�dito',
					emptyText: 'Seleccionar Tipo de Cr�dito. . .',
					mode: 'local',
					lazyRender:true,
					displayField: 'descripcion',
					valueField: 'clave',
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoTipoData,
					listeners:{
						'select':function(cbo){
										if (fpFondeo.isVisible()){fpFondeo.hide();}
										if (gridUno.isVisible()){gridUno.hide();}
										if (grid.isVisible()){grid.hide();}
										if (fpB.isVisible()){fpB.hide();}
										if (toolbar.isVisible()){toolbar.hide();}
										Ext.getCmp('gridTotales').hide();
										Ext.getCmp('btnBajaCsv').hide();
										Ext.getCmp('btnBajaVar').hide();
										Ext.getCmp('btnBajaFijo').hide();
										Ext.getCmp('btnBajaPdf').hide();
										Ext.getCmp('numero_credito').hide();
										Ext.getCmp('ig_numero_docto').hide();
										Ext.getCmp('_ic_pyme').hide();
										var comboEpo = Ext.getCmp('_ic_epo');
										Ext.getCmp('compo').reset();
										if(cbo.getValue() == '0'){
												Ext.getCmp('tipoCredito').setValue('D');
												Ext.getCmp('numero_credito').show();
												Ext.getCmp('btnAutoriza').setText('Autorizar');
												comboEpo.setValue('');
												comboEpo.store.removeAll();
												comboEpo.store.reload({	params: {tipo_credito: 'D'}	});
										}else if (cbo.getValue() == '1'){
												Ext.getCmp('tipoCredito').setValue('C');
												var comboPyme = Ext.getCmp('_ic_pyme');
												Ext.getCmp('ig_numero_docto').show();
												Ext.getCmp('btnAutoriza').setText('Confirmar');
												Ext.getCmp('_ic_pyme').show();
												comboPyme.setValue('');
												comboPyme.store.removeAll();
												comboEpo.setValue('');
												comboEpo.store.removeAll();
												comboEpo.store.reload({	params: {tipo_credito: 'C'}	});
										}else if (cbo.getValue() == '2'){
											Ext.getCmp('tipoCredito').setValue('F');
											var comboPyme = Ext.getCmp('_ic_pyme');
											Ext.getCmp('ig_numero_docto').show();
											Ext.getCmp('btnAutoriza').setText('Confirmar');
											Ext.getCmp('_ic_pyme').show();
											comboPyme.setValue('');
											comboPyme.store.removeAll();
											comboEpo.setValue('');
											comboEpo.store.removeAll();
											comboEpo.store.reload({	params: {tipo_credito: 'F'}	});
										}
									}
					}
				}
			]
		}
	];

	var elementosFormaCampos =[
		{
			xtype: 'combo',
			id:	'_ic_epo',
			name: 'ic_epo',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO',
			emptyText: 'Seleccione una EPO. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			allowBlank: false,
			typeAhead: true,
			minChars : 1,
			store: catalogoEpoDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				'select':function(cbo){
								if(Ext.getCmp('_tipo_credito').getValue() === '1'	||	Ext.getCmp('_tipo_credito').getValue() === '2'	){
									var valCombo = cbo.getValue();
									if (valCombo.indexOf("|") != -1){
										valCombo = valCombo.replace("|","");
									}
									var comboPyme = Ext.getCmp('_ic_pyme');
									comboPyme.setValue('');
									comboPyme.store.removeAll();
									comboPyme.store.reload({	params: {ic_epo: valCombo}	});
								}
																
								Ext.Ajax.request({	url : '24forma01ext.data.jsp',	
									params:{informacion: "valoresIniciales",
										ic_epo: Ext.getCmp('_ic_epo').getValue()
									},	callback:procesaValoresIniciales	
								});


							}
			}
		},{
			xtype: 'combo',
			id:	'_ic_pyme',
			name: 'ic_pyme',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Distribuidor autorizado',
			emptyText: 'Seleccione distribuidor. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			minChars : 1,
			store: catalogoPymeDistData
		},{
			xtype: 'numberfield',
			name: 'numero_credito',
			id: 'numero_credito',
			fieldLabel: 'N�mero de documento final',
			anchor:'50%',
			maxLength: 10
		},{
			xtype: 'compositefield',
			id:'compo',
			fieldLabel: 'Fecha de solicitud',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_seleccion_de',
					id: 'df_fecha_seleccion_de',
					allowBlank: true,
					startDay: 0,
					width: 100,
					value: dt,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_seleccion_a',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 24
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_seleccion_a',
					id: 'df_fecha_seleccion_a',
					allowBlank: true,
					startDay: 1,
					width: 100,
					value: dt,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_seleccion_de',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},{
			xtype: 'textfield',
			name: 'ig_numero_docto',
			id: 'ig_numero_docto',
			fieldLabel: 'N�mero de Cr�dito',
			allowBlank: true,
			maxLength: 9,
			anchor:'50%',
			hidden:true
		},{
			xtype:'hidden',
			name:	'tipoCredito',
			id:	'tipoCredito',
			value:'D'
		},
		{ 	xtype:'hidden',name:	'ventaCartera',	id: 'ventaCartera',	value:'N' }
	];

	var toolbarB = new Ext.Toolbar({
		id:'barBotonesB',
		style: 'margin:0 auto;',
		autoScroll : true,
		hidden:true,
		height:55,
		 items: [
			'->','-',
			{
				text:'Generar Archivo',	id:'btnGenerarArchivoB',	iconCls:'icoXls',
				handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url	: '24forma01arch2_ext.jsp',
									params:	Ext.apply({	tipoArchivo: 'ARCH',
																acuse: form.acuse,
																ic_pyme : Ext.getCmp('_ic_pyme').getValue(),
																hidTipoCredito:Ext.getCmp('tipoCredito').getValue(),
																selecciona:form.selecciona,
																doctosPorEpo:form.doctosPorEpo,
                                                                                                                                accion:'button'
                                                                                                                                }),
									callback: procesarSuccessFailureGenerarArchivo_B
								});
						}
			},{
				text:'Bajar Archivo',	id:'btnBajaCsvB',	hidden:true
			},'-',{
				text:'Exportar Interfase Variable',id:'btnGeneraVarB',	iconCls:'icoXls', //iconAlign: 'top',
				handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url	: '24forma01arch2_ext.jsp',
									params:	Ext.apply({	tipoArchivo: 'VAR',
																acuse: form.acuse,
																ic_pyme : Ext.getCmp('_ic_pyme').getValue(),
																hidTipoCredito:Ext.getCmp('tipoCredito').getValue(),
																selecciona:form.selecciona,
																doctosPorEpo:form.doctosPorEpo,
																accion:'button'
																}),
									callback: procesarSuccessFailureGenerarArchivo_B
								});
						}
			},{
				text:'Bajar <br>Interfase Variable',	id:'btnBajaVarB',	hidden:true                                
			},'-',{
				text:'Exportar Interfase Fijo',id:'btnGeneraFijoB',iconCls:'icoTxt', //iconAlign: 'top',
				handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url	: '24forma01arch2_ext.jsp',
									params:	Ext.apply({	tipoArchivo: 'FIJO',
																acuse: form.acuse,
																ic_pyme : Ext.getCmp('_ic_pyme').getValue(),
																hidTipoCredito:Ext.getCmp('tipoCredito').getValue(),
																selecciona:form.selecciona,
																doctosPorEpo:form.doctosPorEpo,
                                                                                                                                accion:'button'
                                                                                                                                }),
									callback: procesarSuccessFailureGenerarArchivo_B
								});
						}
			},{
				text:'Bajar <br>Interfase Fijo',	id:'btnBajaFijoB',	hidden:true
			},'-',{
				text:'Generar Archivo Integral',id:'btnGeneraIntegralB',iconCls:'icoXls',
				handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url	: '24forma01arch2_ext.jsp',
									params:	Ext.apply({	tipoArchivo: 'INTEG',
																acuse: form.acuse,
																ic_pyme : Ext.getCmp('_ic_pyme').getValue(),
																hidTipoCredito:Ext.getCmp('tipoCredito').getValue(),
																selecciona:form.selecciona,
																doctosPorEpo:form.doctosPorEpo,
                                                                                                                                accion:'button'
                                                                                                                                }),
									callback: procesarSuccessFailureGenerarArchivo_B
								});
						}
			},{
				text:'Bajar <br>Integral',	id:'btnBajaIntegralB',	hidden:true
			},'-',{
				text:'Generar Pdf',	id:'btnGenerarPDFB',	iconCls:'icoPdf', //iconAlign: 'top',
				handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url	: '24forma01pdf_b_ext.jsp',
									params:	Ext.apply({	ic_pyme : Ext.getCmp('_ic_pyme').getValue(),
																acuse: form.acuse,
																hidTipoCredito:Ext.getCmp('tipoCredito').getValue(),
																cadEpo: form.cadEpo,
																selecciona:form.selecciona,
																doctosPorEpo:form.doctosPorEpo,
																accion:'button'
																}),
									callback: procesarSuccessFailureGenerarPDF_B
								});
						}
			},{
				text:'Bajar Pdf',	id:'btnBajaPdfB',	hidden:true
			},'-',{
				text:'&nbsp;&nbsp;&nbsp;Salir&nbsp;&nbsp;&nbsp;',iconCls:'icoLimpiar', //iconAlign: 'top',
				handler:function(){
								window.location = '24forma01ext.jsp';
							}
			}
		]
	});

	var toolbar = new Ext.Toolbar({
		id:'barBotones',	style: 'margin:0 auto;',	autoScroll:true,	hidden:true,	height:60,
		 items: [  
			'->','-',
			{
				text:'Generar Layout <br>Factoraje al Vencimiento',	id:'btnGenerarLayout',	hidden:true,	iconCls:'icoXls', scale:'large',//iconAlign: 'top',
				handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '24forma01LayFactVto_ext.jsp',
									params:	{informacion:'FactorajeVencimiento'},
									callback: procesarSuccessFailureLayoutFactVto
								});
						}
			},'-',{
				text:'Generar Archivo',	id:'btnGenerarArchivo',	iconCls:'icoXls', //iconAlign: 'top',
				handler: function(boton, evento) {
								var tipoAr;
								var url;
								if(Ext.getCmp('tipoCredito').getValue() === 'D'){
									tipoAr= 'ARCH';
									url	= '24forma01arch_ext.jsp';
								}else{
									tipoAr= 'G';
									url	= '24forma01DesArchvo_ext.jsp';
								}
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: url,
									params:	Ext.apply(fp.getForm().getValues(),fpCampos.getForm().getValues(),{tipoArchivo: tipoAr,	hayMasDeUnaEpoSeleccionada: form.hayMasDeUnaEpoSeleccionada, accion:'button'}),
									callback: procesarSuccessFailureGenerarArchivo
								});
						}
			},{
				text:'Bajar Archivo',	id:'btnBajaCsv',	hidden:true
			},'-',{
				text:'Exportar Interfase Variable',id:'btnGeneraVar',	iconCls:'icoXls', //iconAlign: 'top',
				handler: function(boton, evento) {
								var tipoAr;
								var url;
								if(Ext.getCmp('tipoCredito').getValue() === 'D'){
									tipoAr= 'VAR';
									url	= '24forma01arch_ext.jsp';
								}else{
									tipoAr= 'V';
									url	= '24forma01DesArchvo_ext.jsp';
								}
								//boton.disable();
								//boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: url,
									params:	Ext.apply(fp.getForm().getValues(),fpCampos.getForm().getValues(),{
									tipoArchivo: tipoAr,
									hayMasDeUnaEpoSeleccionada:form.hayMasDeUnaEpoSeleccionada,
									acuse: form.acuse,
									accion:'button'
									}),
									callback: procesarSuccessFailureGenerarArchivo
								});
						}
			},{
				text:'Bajar <br>Interfase Variable',	id:'btnBajaVar',	hidden:true
			},'-',{
				text:'Exportar Interfase Fijo',id:'btnGeneraFijo',iconCls:'icoTxt', //iconAlign: 'top',
				handler: function(boton, evento) {
								var tipoAr;
								var url;
								if(Ext.getCmp('tipoCredito').getValue() === 'D'){
									tipoAr= 'FIJO';
									url	= '24forma01arch_ext.jsp';
								}else{
									tipoAr= 'F';
									url	= '24forma01DesArchvo_ext.jsp';
								}
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: url,
									params:	Ext.apply(fp.getForm().getValues(),fpCampos.getForm().getValues(),{tipoArchivo: tipoAr,	hayMasDeUnaEpoSeleccionada:form.hayMasDeUnaEpoSeleccionada}),
									callback: procesarSuccessFailureGenerarArchivo
								});
						}
			},{
				text:'Bajar <br>Interfase Fijo',	id:'btnBajaFijo',	hidden:true
			},'-',{
				text:'Generar Archivo Integral',id:'btnGeneraIntegral',iconCls:'icoXls',
				handler: function(boton, evento) {
								var tipoAr;
								var url;
								if(Ext.getCmp('tipoCredito').getValue() === 'D'){
									tipoAr= 'INTEGRAL';
									url	= '24forma01arch_ext.jsp';
								}else{
									tipoAr= 'I';
									url	= '24forma01DesArchvo_ext.jsp';
								}
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: url,
									params:	Ext.apply(fp.getForm().getValues(),fpCampos.getForm().getValues(),{tipoArchivo: tipoAr,	hayMasDeUnaEpoSeleccionada:form.hayMasDeUnaEpoSeleccionada}),
									callback: procesarSuccessFailureGenerarArchivo
								});
						}
			},{
				text:'Bajar <br>Integral',	id:'btnBajaIntegral',	hidden:true
			},'-',{
				text:'Generar Pdf',	id:'btnGenerarPDF',	iconCls:'icoPdf', //iconAlign: 'top',
				handler: function(boton, evento) {
								var url;
								if(Ext.getCmp('tipoCredito').getValue() === 'D'){
									url	= '24forma01pdf_ext.jsp';
								}else{
									url	= '24forma01pdfCCC_ext.jsp';
								}
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: url,
									params:	Ext.apply(fp.getForm().getValues(),fpCampos.getForm().getValues(),{hayMasDeUnaEpoSeleccionada:form.hayMasDeUnaEpoSeleccionada}),
									callback: procesarSuccessFailureGenerarPDF
								});
						}
			},{
				text:'Bajar Pdf',	id:'btnBajaPdf',	hidden:true
			},'-',{
				text:'Autorizar',	id:'btnAutoriza',	iconCls:'icoAceptar', //iconAlign: 'top',
				handler:	function(boton){
								var sm = grid.getSelectionModel().getSelections();
								if (!Ext.isEmpty(sm)){
									var datos = consultaData.data.items;
									form.doctosPorEpo = [];
									form.cg_responsable_int = [];
									form.cg_tipo_cobranza = [];
									form.cg_tipo_cobro_int = [];
									
									Ext.each(datos, function(item, index, arrItems){
										Ext.each(datos[index].data, function(it, inx, aItems){
											form.doctosPorEpo.push(it.IC_DOCUMENTO + "," + it.IC_EPO);
											form.cg_responsable_int.push(it.CG_RESPONSABLE_INTERES);
											form.cg_tipo_cobranza.push(it.CG_TIPO_COBRANZA);
											form.cg_tipo_cobro_int.push(it.IC_TIPO_COBRO_INTERES);
										
										});
									});
									consultaData.loadData('');
									resumenTotalesData.loadData('');
									consultaData.add(sm);
									consultaData.commitChanges();
									var reg = Ext.data.Record.create(['TIPO_MONEDA', 'TOTAL_REGISTROS','TOTAL_MONTO','TOTAL_INTERES']);

									form.selecciona = [];
									for (i=0; i<=sm.length-1; i++) {
										form.selecciona.push(sm[i].get('IC_DOCUMENTO'));										
									}

									var jsonData = consultaData.data.items;
									var countMN = 0;
									var countUSD = 0;
									var montoMN=0;
									var montoUSD=0;
									var montoIntMN=0;
									var montoIntUSD=0;
									Ext.each(jsonData, function(registro, index, arrItems){
										Ext.each(jsonData[index].data, function(reg, inx, aItems){
											if(reg.IC_MONEDA === '1'){
												countMN++;
												montoMN += parseFloat(reg.MONTO);
												montoIntMN += parseFloat(reg.MONTO_INTERES);
											}else{
												countUSD++;
												montoUSD += parseFloat(reg.MONTO);
												montoIntUSD += parseFloat(reg.MONTO_INTERES);
											}
										});
									});
									if(countMN>0){
										resumenTotalesData.add(	new reg({TIPO_MONEDA:'Total M.N',TOTAL_REGISTROS: countMN,TOTAL_MONTO: montoMN,	TOTAL_INTERES: montoIntMN}) );
									}
									if(countUSD>0){
										resumenTotalesData.add(	new reg({TIPO_MONEDA:'Total Dolares',TOTAL_REGISTROS: countUSD,TOTAL_MONTO: montoUSD,TOTAL_INTERES: montoIntUSD}) );
									}

									Ext.getCmp('disAcuse').body.update('&nbsp');
									Ext.getCmp('disFechaCarga').body.update('&nbsp');
									Ext.getCmp('disHoraCarga').body.update('&nbsp');
									Ext.getCmp('disUsuario').body.update('&nbsp');

									pnl.el.mask('Enviando...', 'x-mask-loading');
									Ext.Ajax.request({
										url : '24forma01ext.data.jsp',
										params:	Ext.apply({	informacion:'autoriza',
																	ic_pyme : Ext.getCmp('_ic_pyme').getValue(),
																	hidTipoCredito:Ext.getCmp('tipoCredito').getValue(),
																	selecciona:form.selecciona,
																	doctosPorEpo:form.doctosPorEpo,
																	cg_responsable_int:form.cg_responsable_int,
																	cg_tipo_cobranza:form.cg_tipo_cobranza,
																	cg_tipo_cobro_int:form.cg_tipo_cobro_int,
																	ic_epo: Ext.getCmp('_ic_epo').getValue()
                                                                                                                                        }),
										callback: procesarAutorizar
									});
								}else{
									Ext.Msg.alert(boton.text,'Debe seleccionar al menos un documento para continuar');
								}
							}
			},'-',{
				text:'Cancelar',iconCls:'icoRechazar', //iconAlign: 'top',
				handler:function(){
								window.location = '24forma01ext.jsp';
							}
			}
		]
	});

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 620,
		frame: false,
		style: ' margin:0 auto;',
		defaults : {	anchor:'-1'	},
		items: elementosForma
	});

	var fpCampos = new Ext.form.FormPanel({
		id: 'formCampos',
		labelWidth:160,
		width: 620,
		frame: true,
		style: ' margin:0 auto;',
		defaults : {	msgTarget: 'side',	anchor : '-20'	},
		bodyStyle: 'padding: 10px',
		title:'<div align="center">Criterios de B�squeda</div>',
		titleCollapse:true,
		collapsible: true,
		items: elementosFormaCampos,
		buttons:[
			{
				text:'Consultar',
				iconCls:'icoBuscar',
				handler: function(){
								var ventana = Ext.getCmp('winBancaria');
								if (ventana) {
									ventana.destroy();
								}
								var ventana = Ext.getCmp('cambioDoctos');
								if (ventana) {
									ventana.destroy();
								}
								if (fpFondeo.isVisible()){fpFondeo.hide();}
								if (gridUno.isVisible()){gridUno.hide();}
								if (grid.isVisible()){grid.hide();}
								if (fpB.isVisible()){fpB.hide();}
								if (toolbar.isVisible()){toolbar.hide();}
								Ext.getCmp('gridTotales').hide();
								Ext.getCmp('btnGenerarArchivo').enable();
								Ext.getCmp('btnGeneraVar').enable();
								Ext.getCmp('btnGeneraFijo').enable();
								Ext.getCmp('btnGenerarPDF').enable();
								Ext.getCmp('btnBajaCsv').hide();
								Ext.getCmp('btnBajaVar').hide();
								Ext.getCmp('btnBajaFijo').hide();
								Ext.getCmp('btnBajaPdf').hide();
								Ext.getCmp('btnGeneraIntegral').enable();
								Ext.getCmp('btnBajaIntegral').hide();
								
								if(!verificaPanel()) {
									return;
								}
								var fechaSolMin = Ext.getCmp('df_fecha_seleccion_de');
								var fechaSolMax = Ext.getCmp('df_fecha_seleccion_a');
								if (!Ext.isEmpty(fechaSolMin.getValue()) || !Ext.isEmpty(fechaSolMax.getValue()) ) {
									if(Ext.isEmpty(fechaSolMin.getValue()))	{
										fechaSolMin.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
										fechaSolMin.focus();
										return;
									}else if (Ext.isEmpty(fechaSolMax.getValue())){
										fechaSolMax.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
										fechaSolMax.focus();
										return;
									}
								}
								pnl.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '24forma01ext.data.jsp',
									params: Ext.apply(Ext.getCmp('formCampos').getForm().getValues(),{informacion: "Consulta"}),
									callback: procesaConsulta
								});
				}
			},{
				text:'Limpiar',
				iconCls:'icoLimpiar',
				handler:function(){
								window.location = '24forma01ext.jsp';
							}
			}
		]
	});

	var fpMsg = new Ext.Panel({
			id:'panMsg',
			bodyStyle: 'padding: 5px',
			width: 620,
			frame: true,
			style: ' margin:0 auto;',
			border:true,
			html:'&nbsp;'
	});

	var fpMsgMeses2 = new Ext.Panel({
			id:'fpMsgMeses2',
			bodyStyle: 'padding: 5px',
			width: 620,
			frame: false,
			style: ' margin:0 auto;',
			border:false,
			hidden:true, 
			html:'&nbsp;'
	});
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 945,
		height: 'auto',
		items: [	fp,fpCampos,fpMsg, NE.util.getEspaciador(10) , NE.util.getEspaciador(10), fpFondeo,gridUno, 
					NE.util.getEspaciador(10),	grid,gridTotales,	NE.util.getEspaciador(10),fpB,NE.util.getEspaciador(10),toolbar,
					fpC, fpMsgMeses2,  NE.util.getEspaciador(10),gridB,gridTotalesB,NE.util.getEspaciador(10),fpAcuse,NE.util.getEspaciador(10),toolbarB	]
	});
	
	if (strPerfil != "IF FACT RECURSO"){
		Ext.getCmp('_tipo_credito').setValue('0');
	}
	
	
	catalogoEpoDistData.load();
	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({	url : '24forma01ext.data.jsp',	params:{informacion: "valoresIniciales"},	callback:procesaValoresIniciales	});

});