<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, com.netro.anticipos.*,
	netropology.utilerias.*,
	com.netro.exception.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();

Vector vecFilas	=null;
Vector vecColumnas=null;
String _acuse		= (request.getParameter("_acuse")==null)?"":request.getParameter("_acuse");
String fechaHoy	= (request.getParameter("fechaHoy")==null)?"":request.getParameter("fechaHoy");
String horaActual	= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");
String usuario		= (request.getParameter("usuario")==null)?"":request.getParameter("usuario");

String totMtoAuto	  	= (request.getParameter("totMtoAuto")==null)?"0":request.getParameter("totMtoAuto");
String totDocs	  		= (request.getParameter("totDocs")==null)?"0":request.getParameter("totDocs");
String totMtoAutoDol = (request.getParameter("totMtoAutoDol")==null)?"0":request.getParameter("totMtoAutoDol");
String totDocsDol	  	= (request.getParameter("totDocsDol")==null)?"0":request.getParameter("totDocsDol");

String aFolio [] 		= request.getParameterValues("folio");
String aFolioArm [] 	= request.getParameterValues("rs_folioArmado");
String aNumCliente [] 	= request.getParameterValues("rs_numCliente");
String aNomCliente [] 	= request.getParameterValues("rs_nomPyme");
String aFolioRel [] 	= request.getParameterValues("rs_folioRel");
String aFechaSol [] 	= request.getParameterValues("rs_fechaSol");
String aMoneda[]		= request.getParameterValues("moneda");
String aMonto [] 		= request.getParameterValues("monto");
String aEstatus [] 		= request.getParameterValues("estatus");
String aTipoCobro [] 	= request.getParameterValues("tipo_cobro_int");
String aCausa [] 		= request.getParameterValues("causa_rechazo");
String aMontoAuto [] 	= request.getParameterValues("monto_auto");
String aFechaVto [] 	= request.getParameterValues("fecha_vto");
String aBanco [] 		= request.getParameterValues("banco");
String aCuenta [] 		= request.getParameterValues("cuenta");
String aTipo[]			= request.getParameterValues("tipo");
String atipoSolicitud[]	= request.getParameterValues("tipoSolicitud");
String tipo_liq			= request.getParameter("tipo_liq");

try {

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;

	contenidoArchivo.append("Moneda Nacional,,Dolares");
	contenidoArchivo.append("\nNúmero de solicitudes procesadas, Monto de Solicitudes procesadas, Número de Solicitudes procesadas, Monto de Solicitudes procesadas");
	contenidoArchivo.append("\n"+totDocs+","+totMtoAuto+","+totDocsDol+","+totMtoAutoDol+"\nLeyenda legal");
	contenidoArchivo.append("\n \nFolio de solicitud,No. de distribuidor,Distribuidor,Tipo solicitud,Num. línea relacionada,Fecha solicitud,Estatus asignado,Causa de rechazo,Moneda,Monto,Fecha vencimiento");

	if (aFolio.length > 0){
		for (int i=0;i<aFolio.length;i++){
			if(!"".equals(aEstatus[i])) {
				vecFilas = lineaCredito.getDescripcion(aMoneda[i],aEstatus[i],aTipoCobro[i]);
				vecColumnas = (Vector)vecFilas.get(0);
				String nomMoneda	= (String)vecColumnas.get(0);
				String nomEstatus	= (String)vecColumnas.get(1);
				String nomTipoCobro	= (String)vecColumnas.get(2);

				contenidoArchivo.append("\n"+aFolioArm[i]+","+aNumCliente[i]+","+aNomCliente[i]+","+atipoSolicitud[i]+","+aFolioRel[i]+","+aFechaSol[i]+","+nomEstatus+","+aCausa[i]+","+nomMoneda+","+aMontoAuto[i]+","+aFechaVto[i]);
			}
		}
	}

	if (!("0".equals(totDocs))){
		contenidoArchivo.append("\nTotales MN,"+totDocs+",,,"+totMtoAuto);
	}
	if (!("0".equals(totDocsDol))){
		contenidoArchivo.append("\nTotales Dolares,"+totDocsDol+",,,"+totMtoAutoDol);
	}
	contenidoArchivo.append("\n \n \nDatos de cifras de control");
	contenidoArchivo.append("\nNum. acuse,"+_acuse);
	contenidoArchivo.append("\nFecha,"+fechaHoy);
	contenidoArchivo.append("\nHora,"+horaActual);
	contenidoArchivo.append("\nNombre y número de usuario,"+usuario);

	if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivo = archivo.nombre;
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>