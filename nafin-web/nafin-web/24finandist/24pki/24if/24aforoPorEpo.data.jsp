<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		netropology.utilerias.*,
		com.netro.distribuidores.*,
		com.netro.model.catalogos.*,
		javax.naming.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String epo = (request.getParameter("epo")!=null)?request.getParameter("epo"):"";
String producto = (request.getParameter("producto")!=null)?request.getParameter("producto"):"";
String infoRegresar	=	"", mensaje = "", consulta ="";
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
JSONObject jsonObjG = new JSONObject();
JSONObject jsonObj = new JSONObject();

ParametrosDist actualizacionCatalogos = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

List aux = actualizacionCatalogos.getParametrosEpo("","");
ConsAforePorEpo paginador = new ConsAforePorEpo();

if (informacion.equals("catalogoIF") ) {
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveIf(iNoCliente);   
   cat.setTipoCredito("D"); 
	cat.setPantalla("AFOROEPO");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
}else if(informacion.equals("Guardar_Datos")){

	String numeroRegistros  =(request.getParameter("numeroRegistros")!=null) ? request.getParameter("numeroRegistros"):"";
	boolean resultado = false;
	JSONObject jsonObj1 = new JSONObject();
	int i_numReg = Integer.parseInt(numeroRegistros);
		for(int i = 0; i< i_numReg; i++){  
			String ic_epo[] = request.getParameterValues("ic_epo");
			String aforo[] = request.getParameterValues("aforo");
			String moneda[] = request.getParameterValues("moneda");
			String aforoAnt[] = request.getParameterValues("aforoAnt");
			if(!aforoAnt[i].equals(aforo[i])){
				resultado =actualizacionCatalogos.guardaAforoPorEPO(iNoCliente,ic_epo[i],moneda[i], aforo[i],strLogin, iNoNafinElectronico,strNombreUsuario);//quitar 32 y colocar iNoCliente Y EN 43 COLOCAR "EPO"
				jsonObj1.put("accion", "G");
				if(resultado ==true){
					mensaje = "La operación se realizo con éxito.";
				}
			}
		}
		jsonObj1.put("success", new Boolean(true));
		jsonObj1.put("mensaje", mensaje);
		infoRegresar = jsonObj1.toString();
}else if(informacion.equals("Consultar")){
		paginador.setClaveIF(iNoCliente);
		paginador.setClaveEPO(epo);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
		try{
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString(); 
		}	catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}	
} 
%>
<%=infoRegresar%>


