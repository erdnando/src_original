<%@ page contentType="application/json;charset=UTF-8"
	import=" 
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		com.netro.descuento.*,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ page import="com.netro.pdf.*"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>

<%

String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";


ParametrosDist  BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
JSONObject jsonObj = new JSONObject();

if (informacion.equals("valoresIniciales"))	{
	 
	 jsonObj = new JSONObject();	
	if(application.getInitParameter("EpoCemex").equals(iNoCliente)){
		jsonObj.put("cadEpo", "EpoCemex");
	}else{
		jsonObj.put("cadEpo", "Epo");
	}	
	
	//Fodea 020-2014 
	String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	if(!ic_epo.equals("") )  {
		String	ventaCartera =  BeanParametro.DesAutomaticoEpo(ic_epo,"PUB_EPO_VENTA_CARTERA");
		String  meseSinIntereses = BeanParametro.DesAutomaticoEpo(ic_epo,"CS_MESES_SIN_INTERESES");
		String operaContrato = BeanParametro.obtieneOperaSinCesion(ic_epo); //PARAMETRO NUEVO JAGE 14012017

		jsonObj.put("ventaCartera", ventaCartera);	
		jsonObj.put("meseSinIntereses", meseSinIntereses);
                jsonObj.put("operaContrato", operaContrato);
                
                
	}
	jsonObj.put("success", new Boolean(true));	
	infoRegresar = jsonObj.toString();

	System.out.println("infoRegresar  "+infoRegresar);

}else if (informacion.equals("CatalogoEPODist") ) {

		String tipo_credito = (request.getParameter("tipo_credito")==null)?"D":request.getParameter("tipo_credito");
	
		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClaveIf(iNoCliente);
		if (tipo_credito.equals("D")){
			cat.setTipoCredito("D");
		}else if (tipo_credito.equals("C")){
			cat.setTipoCredito("C");
		}else if (tipo_credito.equals("F")){
			cat.setTipoCredito("F");
		}
		cat.setCdoctos("S");
		cat.setEstatusDocto("3");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoDistribuidor")){

	String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setClasifica("flag");
		if(!strPerfil.equals("IF FACT RECURSO")){
			cat.setTipoCredito("C");
		}
		cat.setEstatusDocto("3");
		cat.setCdoctos("S");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}

}else if (informacion.equals("Consulta")){

	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
	
	AutorizacionSolic autSolic = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	Vector nombresCampo = new Vector(5);
	Vector detalleCampo = null;
	String campo ="", campo0="", campo1="",campo2="",campo3="",campo4="",campo5="";
	Vector mod_cg_campo			= new Vector(5);

	String fechaHoy	= "";
	String fechaActual= "";
	String HoraActual	= "";
	try{
		fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		HoraActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
	}catch(Exception e){
		fechaHoy = "";
	}

	jsonObj = new JSONObject();
	String ic_epo			=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String numero_credito			= (request.getParameter("numero_credito")==null)?"":request.getParameter("numero_credito");
	String df_fecha_seleccion_de	= (request.getParameter("df_fecha_seleccion_de")==null)?fechaHoy:request.getParameter("df_fecha_seleccion_de");
	String df_fecha_seleccion_a	= (request.getParameter("df_fecha_seleccion_a")==null)?fechaHoy:request.getParameter("df_fecha_seleccion_a");
	String ic_pyme						= (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String ig_numero_docto			= (request.getParameter("ig_numero_docto")!=null)?request.getParameter("ig_numero_docto"):"";
	String tipoCredito				= (request.getParameter("tipoCredito")!=null)?request.getParameter("tipoCredito"):"D";
	String csTipoFondeo = "";
	if(ic_epo.indexOf("|") != -1){
		ic_epo = ic_epo.substring(1,ic_epo.length());
	}
	boolean 		hayMasDeUnaEpoSeleccionada = false;
	ArrayList 	listaClavesEpo 				= null; 

	hayMasDeUnaEpoSeleccionada = Comunes.esUnaLista(ic_epo);
	jsonObj.put("hayMasDeUnaEpoSeleccionada", new Boolean(hayMasDeUnaEpoSeleccionada));
	listaClavesEpo 				= null;
	int i = 0;
	try{
		if(hayMasDeUnaEpoSeleccionada){
			
			listaClavesEpo = Comunes.stringToArrayList(ic_epo, ",");
			
			// Validar el horario de servicio de las epos seleccionadas, si alguna de estas no se encuentra en el horario de servicio
			// suprimirlas de la lista de epos validas
			for(int k=0;k<listaClavesEpo.size();k++){
				try {
					String claveEpo = (String) listaClavesEpo.get(k);
					Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
				}catch(Exception e){
					listaClavesEpo.remove(k);
					k--;
					/*
						if(e instanceof NafinException){
							NafinException e = (NafinException) e;
							if(e.getCause().equals("SIST0001")){
								throw e;
							}
						}
					*/
				}
			}
			
			// Validar que haya al menos una EPO 
			if(listaClavesEpo.size() == 0){
				throw new Exception("No se encontró ninguna EPO con horario válido");
			}else if(listaClavesEpo.size() == 1){
				ic_epo = (String) listaClavesEpo.get(0);	
				hayMasDeUnaEpoSeleccionada = false;
			}
			
		}
		if(!hayMasDeUnaEpoSeleccionada){
			
			Horario.validarHorario(4, strTipoUsuario, ic_epo, iNoCliente);
			HashMap ohDatosFondeo = BeanAutDescuento.getDatosFondeo(iNoCliente, ic_epo, "4");
			csTipoFondeo = ohDatosFondeo.get("TIPO_FONDEO").toString();
			if ("P".equals(csTipoFondeo)){
				jsonObj.put("csTipoFondeo", csTipoFondeo);
			}

			listaClavesEpo = new ArrayList();
			listaClavesEpo.add(ic_epo);
		}

		if (tipoCredito.equals("C") || tipoCredito.equals("F")){
			if (!ic_epo.equals("")){
			  // F029 - 2011: Si selecciono mas de una EPO usar nombres de campos genericos
			  if(hayMasDeUnaEpoSeleccionada){
				  
				  Vector fila = null;
				  
				  nombresCampo = new Vector();
				  
				  fila = new Vector();
				  fila.add("CG CAMPO 1");
				  nombresCampo.add(fila);
				  
				  fila = new Vector();
				  fila.add("CG CAMPO 2");
				  nombresCampo.add(fila);
				  
				  fila = new Vector();
				  fila.add("CG CAMPO 3");
				  nombresCampo.add(fila);
				  
				  fila = new Vector();
				  fila.add("CG CAMPO 4");
				  nombresCampo.add(fila);
				  
				  fila = new Vector();
				  fila.add("CG CAMPO 5");
				  nombresCampo.add(fila);
		 
			  }else{
				  nombresCampo = cargaDocto.getCamposAdicionales(ic_epo,false);
			  }
			  jsonObj.put("nombresCampo", nombresCampo);
			}
		}

		HashMap catalogoTipoFondeo = hayMasDeUnaEpoSeleccionada?BeanAutDescuento.getTipoFondeo(iNoCliente, listaClavesEpo, "4"):null;

		List NuevoReg = new ArrayList();
		Vector vecFilas = null;
		Vector vecColumnas = null;
		
		System.out.println("tipoCredito  "+tipoCredito);
		
		for(int k=0;k<listaClavesEpo.size();k++){
			String claveEpo = (String) listaClavesEpo.get(k);
			if (tipoCredito.equals("D")){
				vecFilas	=	autSolic.getLineasConDoctos(claveEpo,iNoCliente,numero_credito,df_fecha_seleccion_de,df_fecha_seleccion_a);
			}else if (tipoCredito.equals("C")){
				vecFilas	=	autSolic.getLineasConDoctosCCC(claveEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a);
			}else if (tipoCredito.equals("F")){
				vecFilas	=	autSolic.getLineasConDoctosFF(claveEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a);
			}

			for(i=0;i<vecFilas.size();i++){
				HashMap hash = new HashMap();
				vecColumnas 		= (Vector)vecFilas.get(i);
				String nombreAcreditado	=	(String)vecColumnas.get(0);
				String numLineaCredito	=	(String)vecColumnas.get(2);
				String moneda				=	(String)vecColumnas.get(3);
				String saldoInicial		=	(String)vecColumnas.get(5);
				String montoSeleccionado=	(String)vecColumnas.get(6);
				String numDoctos			=	(String)vecColumnas.get(7);
				String saldoDisponible	=	(String)vecColumnas.get(8);
				hash.put("NOMBRE_EPO",nombreAcreditado);
				hash.put("LINEA_CREDITO",numLineaCredito);
				hash.put("MONEDA",moneda);
				hash.put("SALDO_INICIAL",saldoInicial);
				hash.put("MONTO_OPERADO",montoSeleccionado);
				hash.put("DOCTOS_OPERADOS",numDoctos);
				hash.put("SALDO_DISPONIBLE",saldoDisponible);
				NuevoReg.add(hash);
			}
		}
		jsonObj.put("regsUno", NuevoReg);

		String	moneda	=	"",	icMoneda	=	"",	icDocumento	=	"",ic_pyme_b	=	"", nombre_epo="";;
		String	igNumeroDocto	=	"", fechaSolicitud = "",	distribuidor = "",	tipoConversion = "",	referencia = "",	plazo = "",	fechaVto = "";
		String	tipoPagoInteres=	"",	rs_referencia = "",	rs_campo1 = "",	rs_campo2 = "",	rs_campo3 = "",	rs_campo4 = "",	rs_campo5 = "",
		tipoPago = "";
		double 	montoInteres = 0,	tasaInteres = 0,	monto = 0,	tipoCambio = 0,	montoValuado = 0;

		//totales
		int totalDoctosMN= 0,	totalDoctosUSD = 0;
		double totalMontoMN = 0,	totalMontoUSD = 0,	totalMontoValuadoMN = 0,	totalMontoValuadoUSD	= 0,	totalMontoIntMN = 0,totalMontoIntUSD = 0;

		NuevoReg = new ArrayList();
		List totalReg = new ArrayList();
		for(int k=0;k<listaClavesEpo.size();k++){
			String claveEpo 	= (String) listaClavesEpo.get(k);
			if (tipoCredito.equals("D")){
				vecFilas	=	autSolic.getDoctosSeleccionados(claveEpo,iNoCliente,numero_credito,df_fecha_seleccion_de,df_fecha_seleccion_a,null);
			}else if (tipoCredito.equals("C")){
				vecFilas	=	autSolic.getDoctosSeleccionadosCCC(claveEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a,null);
			}if (tipoCredito.equals("F")){
				vecFilas	=	autSolic.getDoctosSeleccionadosFF(claveEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a,null);
			}
			

			for(i=0;i<vecFilas.size();i++){
				HashMap hash = new HashMap();
				vecColumnas = (Vector)vecFilas.get(i);
				
				igNumeroDocto			=	(String)vecColumnas.get(0);
				fechaSolicitud			=	(String)vecColumnas.get(1);
				distribuidor			=	(String)vecColumnas.get(2);
				ic_pyme_b				=	(String)vecColumnas.get(3);
				moneda					=	(String)vecColumnas.get(4);
				monto						=	Double.parseDouble(vecColumnas.get(20).toString());
				tipoConversion			=	(String)vecColumnas.get(7);
				tipoCambio				=	Double.parseDouble(vecColumnas.get(8).toString());
				montoValuado			=	monto*tipoCambio;
				referencia				=	(String)vecColumnas.get(9);
				plazo						=	(String)vecColumnas.get(11);
				fechaVto					=	(String)vecColumnas.get(12);
				tipoPagoInteres		=	(String)vecColumnas.get(13);
				montoInteres 			=	Double.parseDouble(vecColumnas.get(15).toString());
				icDocumento				=	(String)vecColumnas.get(16);
				nombre_epo 				=	(String)vecColumnas.get(17);
				icMoneda					=	(String)vecColumnas.get(19);
				if (tipoCredito.equals("D")){
					//icMoneda					=	(String)vecColumnas.get(19);
					tasaInteres				=	Double.parseDouble(vecColumnas.get(33).toString());
				}else{
					//icMoneda				=	(String)vecColumnas.get(5);
					tasaInteres			=	Double.parseDouble(vecColumnas.get(21).toString());
					/*campo1 				=	(String)vecColumnas.get(22);
					campo2 				=	(String)vecColumnas.get(23);
					campo3 				=	(String)vecColumnas.get(24);
					campo4 				=	(String)vecColumnas.get(25);
					campo5 				=	(String)vecColumnas.get(26);*/				
				}
				campo1 				=	(String)vecColumnas.get(22);
				campo2 				=	(String)vecColumnas.get(23);
				campo3 				=	(String)vecColumnas.get(24);
				campo4 				=	(String)vecColumnas.get(25);
				campo5 				=	(String)vecColumnas.get(26);
				String cg_numero_cuenta = autSolic.getCuentaAutorizada(claveEpo,iNoCliente,ic_pyme_b,icMoneda);
				String tipoFinan = "";
				if(hayMasDeUnaEpoSeleccionada){
					tipoFinan = BeanAutDescuento.getDescripcionTipoFondeo(catalogoTipoFondeo,claveEpo);
				}

				String ic_tipo_cobro_interes = (String)vecColumnas.get(14); 
				String tipo_cobranza = (String)vecColumnas.get(26); 
				String cg_responsable_interes = (String)vecColumnas.get(25); 
				
				if (tipoCredito.equals("C")){  //F09-2015
					tipoPago =  (String)vecColumnas.get(29); 
				}
				
				
				hash.put("IC_EPO",claveEpo);
				hash.put("IC_MONEDA",icMoneda);
				hash.put("IC_PYME",ic_pyme_b);
				hash.put("TIPO_CONVERSION",tipoConversion);
				hash.put("TIPO_CAMBIO",Double.toString(tipoCambio));
				hash.put("DOCTO_FINAL",igNumeroDocto);
				hash.put("FECHA_SOLICITUD",fechaSolicitud);
				hash.put("NOMBRE_EPO",nombre_epo);
				hash.put("NOMBRE_PYME",distribuidor);
				hash.put("MONEDA",moneda);
				hash.put("MONTO",Double.toString(monto));
				hash.put("REFERENCIA_TASA",referencia);
				hash.put("TASA_INTERES",Double.toString(tasaInteres));
				hash.put("PLAZO",plazo);
				hash.put("FECHA_VENCIMIENTO",fechaVto);
				hash.put("MONTO_INTERES",Double.toString(montoInteres));
				hash.put("TIPO_INTERES",tipoPagoInteres);
				hash.put("CUENTA_BANCARIA",cg_numero_cuenta);
				hash.put("IC_DOCUMENTO",icDocumento);
				hash.put("CG_CAMPO1",campo1);
				hash.put("CG_CAMPO2",campo2);
				hash.put("CG_CAMPO3",campo3);
				hash.put("CG_CAMPO4",campo4);
				hash.put("CG_CAMPO5",campo5);
				hash.put("TIPO_FINANCIAMIENTO",tipoFinan);
				
				hash.put("IC_TIPO_COBRO_INTERES",ic_tipo_cobro_interes);
				hash.put("CG_TIPO_COBRANZA",tipo_cobranza);
				hash.put("CG_RESPONSABLE_INTERES",cg_responsable_interes);				
				hash.put("TIPOPAGO",tipoPago);	 
				
				
				if("1".equals(icMoneda)){
					totalDoctosMN 			++;
					totalMontoMN 			+= monto;
					totalMontoValuadoMN 	+= montoValuado;
					totalMontoIntMN		+= montoInteres;
				}else if("54".equals(icMoneda)){
					totalDoctosUSD			++;
					totalMontoUSD 			+= monto;
					totalMontoValuadoUSD	+= montoValuado;
					totalMontoIntUSD		+= montoInteres;
				}
				NuevoReg.add(hash);
			}
		}
		jsonObj.put("regs", NuevoReg);
		HashMap hashTot = new HashMap();
		hashTot.put("totalDoctosMN",Integer.toString(totalDoctosMN));
		hashTot.put("totalMontoMN",Double.toString(totalMontoMN));
		hashTot.put("totalMontoValuadoMN",Double.toString(totalMontoValuadoMN));
		hashTot.put("totalMontoIntMN",Double.toString(totalMontoIntMN));
		hashTot.put("totalDoctosUSD",Integer.toString(totalDoctosUSD));
		hashTot.put("totalMontoUSD",Double.toString(totalMontoUSD));
		hashTot.put("totalMontoValuadoUSD",Double.toString(totalMontoValuadoUSD));
		hashTot.put("totalMontoIntUSD",Double.toString(totalMontoIntUSD));
		totalReg.add(hashTot);
		jsonObj.put("totales", totalReg);

		jsonObj.put("success", new Boolean(true));

	}catch(Exception e){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", e);
	}

	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenCambioDoctos")) {

	String ic_documento = (request.getParameter("ic_docto")==null)?"":request.getParameter("ic_docto");
	String tipo_credito = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");

	if(ic_documento != null && !ic_documento.equals("")) {
		DetalleCambiosDoctosIf dataCambios = new DetalleCambiosDoctosIf();
		dataCambios.setTipoCredito(tipo_credito);
		dataCambios.setIcDocumento(ic_documento);
		Registros registros = dataCambios.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("obtenCuentaBancaria")) {

	AutorizacionSolic autSolic = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);

	String 	ic_epo	= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String 	ic_pyme	= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String 	ic_moneda	= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
	Vector vecFilas		= null;
	Vector vecColumnas	= null;

	String	rs_Distribuidor = "",	rs_NEPyme = "",	rs_RFC = "",	rs_Cuenta = "",	rs_Sucursal = "",	rs_Plaza = "";

	vecFilas = autSolic.getDatosCuentaAutorizada(ic_epo,iNoCliente,ic_pyme,ic_moneda);

	if(vecFilas.size()>0){
		List regs = new ArrayList();
		vecColumnas = (Vector)vecFilas.get(0);	
		HashMap hash = new HashMap();
		rs_Distribuidor=	(String)vecColumnas.get(0);
		rs_NEPyme		=	(String)vecColumnas.get(1);
		rs_RFC 			=	(String)vecColumnas.get(2);
		rs_Cuenta 		=	(String)vecColumnas.get(3);
		rs_Sucursal 	=	(String)vecColumnas.get(4);
		rs_Plaza 		=	(String)vecColumnas.get(5);
		hash.put("NOMPYME",rs_Distribuidor);
		hash.put("NEPYME",rs_NEPyme);
		hash.put("PYMERFC",rs_RFC);
		hash.put("NOCUENTA",rs_Cuenta);
		hash.put("SUCURSAL",rs_Sucursal);
		hash.put("PLAZA",rs_Plaza);
		regs.add(hash);
		if (regs != null){
			JSONArray jsObjArray = new JSONArray();
			jsObjArray = JSONArray.fromObject(regs);
			infoRegresar = "{\"success\": true, \"total\": "+vecFilas.size()+", \"registros\": " + jsObjArray.toString() + "}";
		}
	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("autoriza")) {
        
        String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
        
	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);

	AutorizacionSolic autSolic = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);
        
        String operaContrato = BeanParametro.obtieneOperaSinCesion(ic_epo); //PARAMETRO NUEVO JAGE 14012017

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

	String	ic_documentos[]	= request.getParameterValues("selecciona");
	String	[]doctosPorEpo		= request.getParameterValues("doctosPorEpo");
	String 	ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String 	hidTipoCredito		= (request.getParameter("hidTipoCredito")==null)?"":request.getParameter("hidTipoCredito");
	String	cg_responsable_int[]	= request.getParameterValues("cg_responsable_int");
	String	cg_tipo_cobranza[]	= request.getParameterValues("cg_tipo_cobranza");
	String	cg_tipo_cobro_int[]	= request.getParameterValues("cg_tipo_cobro_int");

	jsonObj = new JSONObject();
	// Obtener la lista de las Epos Seleccionadas
	HashMap 			doctosAgrupados 	= getDoctosAgrupadosPorEpo(ic_documentos, doctosPorEpo, cg_responsable_int, cg_tipo_cobranza , cg_tipo_cobro_int );
	
	ArrayList 		listaClavesEpos	= (ArrayList) doctosAgrupados.get("LISTA_CLAVES_EPOS");
	if(listaClavesEpos.size()== 0){
		throw new Exception("No se ha seleccionado ningún documento válido.");
	}
	boolean	hayMasDeUnaEpoSeleccionada = listaClavesEpos.size()>1?true:false;

	if(!hayMasDeUnaEpoSeleccionada){
		String claveEpo = (String) listaClavesEpos.get(0);
		Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
		HashMap ohDatosFondeo = BeanAutDescuento.getDatosFondeo(iNoCliente, claveEpo, "4");
	}else{
		for(int k=0;k<listaClavesEpos.size();k++){
			String claveEpo = (String) listaClavesEpos.get(k);
			Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
		}
	}

	Vector	nombresCampo = new Vector(5);

	if(hayMasDeUnaEpoSeleccionada){
		Vector fila = null;
		nombresCampo = new Vector();

		fila = new Vector();
		fila.add("CG CAMPO 1");
		nombresCampo.add(fila);

		fila = new Vector();
		fila.add("CG CAMPO 2");
		nombresCampo.add(fila);

		fila = new Vector();
		fila.add("CG CAMPO 3");
		nombresCampo.add(fila);

		fila = new Vector();
		fila.add("CG CAMPO 4");
		nombresCampo.add(fila);

		fila = new Vector();
		fila.add("CG CAMPO 5");
		nombresCampo.add(fila);
	}else{
		String claveEpo = (String) listaClavesEpos.get(0);
		nombresCampo 	= cargaDocto.getCamposAdicionales(claveEpo,false);
	}
	jsonObj.put("nombresCampo", nombresCampo);

	Vector vecFilas		= null;
	Vector vecColumnas	= null;
	String in = "",	icMoneda = "",	monedaLinea = "";
	double	monto						= 0;
	double	tipoCambio				= 0;
	double	montoValuado			= 0;
	double	montoInteres			= 0;
	int		totalDoctosMN			= 0;
	int 		totalDoctosUSD			= 0;
	double	totalMontoMN			= 0;
	double	totalMontoUSD			= 0;
	double 	totalMontoValuadoMN	= 0;
	double 	totalMontoValuadoUSD	= 0;
	double	totalMontoIntMN		= 0;
	double	totalMontoIntUSD		= 0;
	int		totalDoctosConv		= 0;
	double	totalMontoConvMN		= 0;
	double	totalMontoConv			= 0;
	double	totalMontoIntConv 	= 0;

	int i=0;
	for(int k=0;k<listaClavesEpos.size();k++){

		String 	claveEpo 		= (String) listaClavesEpos.get(k);
		HashMap 	doctos 	      = (HashMap) doctosAgrupados.get("DOCTOS_AGRUPADOS");
		in = Comunes.arrayListToString((ArrayList)doctos.get(claveEpo),",");

		vecFilas = autSolic.getDoctosSeleccionados(claveEpo,iNoCliente,ic_pyme,"","","",in,hidTipoCredito);

		for(i=0;i<vecFilas.size();i++){
			vecColumnas 		= 	(Vector)vecFilas.get(i);
			icMoneda				=	(String)vecColumnas.get(5);
			monto					=	Double.parseDouble(vecColumnas.get(20).toString());
			tipoCambio			=	Double.parseDouble(vecColumnas.get(8).toString());
			montoValuado		=	monto*tipoCambio;
			montoInteres 		=	Double.parseDouble(vecColumnas.get(15).toString());
			monedaLinea			= 	(String)vecColumnas.get(19);

			if("1".equals(icMoneda)){
				totalDoctosMN 			++;
				totalMontoMN 			+= monto;
				totalMontoValuadoMN 	+= montoValuado;
				totalMontoIntMN		+= montoInteres;
			}

			if("54".equals(icMoneda)&&"54".equals(monedaLinea)){
				totalDoctosUSD			++;
				totalMontoUSD 			+= monto;
				totalMontoValuadoUSD	+= montoValuado;
				totalMontoIntUSD		+= montoInteres;
			}

			if("54".equals(icMoneda)&&"1".equals(monedaLinea)){
				totalDoctosConv		++;
				totalMontoConv			+= monto;
				totalMontoConvMN		+= montoValuado;
				totalMontoIntConv		+= montoInteres;
			}
		}
	}
	List totalReg = new ArrayList();
	HashMap hashTot = new HashMap();
	hashTot.put("totalDoctosMN",Integer.toString(totalDoctosMN));
	hashTot.put("totalMontoMN",Double.toString(totalMontoMN));
	hashTot.put("totalMontoValuadoMN",Double.toString(totalMontoValuadoMN));
	hashTot.put("totalMontoIntMN",Double.toString(totalMontoIntMN));
	hashTot.put("totalDoctosUSD",Integer.toString(totalDoctosUSD));
	hashTot.put("totalMontoUSD",Double.toString(totalMontoUSD));
	hashTot.put("totalMontoValuadoUSD",Double.toString(totalMontoValuadoUSD));
	hashTot.put("totalMontoIntUSD",Double.toString(totalMontoIntUSD));
	hashTot.put("totalDoctosConv",Integer.toString(totalDoctosConv));
	hashTot.put("totalMontoConv",Double.toString(totalMontoConv));
	hashTot.put("totalMontoValuadoConv",Double.toString(totalMontoConvMN));
	hashTot.put("totalMontoIntConv",Double.toString(totalMontoIntConv));
	totalReg.add(hashTot);

	jsonObj.put("totales", totalReg);
        jsonObj.put("operaContrato",operaContrato);

	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Confirmar")) {

	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);

	AutorizacionSolic autSolic = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);
        
        String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
        
        String operaContrato = BeanParametro.obtieneOperaSinCesion(ic_epo); //PARAMETRO NUEVO JAGE 14012017

	String	ic_documentos[]	= request.getParameterValues("selecciona");
	String	[]doctosPorEpo		= request.getParameterValues("doctosPorEpo");
	String 	ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String 	hidTipoCredito		= (request.getParameter("hidTipoCredito")==null)?"":request.getParameter("hidTipoCredito");
	String	cg_responsable_int[]	= request.getParameterValues("cg_responsable_int");
	String	cg_tipo_cobranza[]	= request.getParameterValues("cg_tipo_cobranza");
	String	cg_tipo_cobro_int[]	= request.getParameterValues("cg_tipo_cobro_int");
	
	jsonObj = new JSONObject();
	String _acuse = "",	acuse = "";
	String pkcs7							=	request.getParameter("pkcs7");
	String externContent					=	request.getParameter("textoFirmado");
	//String serial							=	"";
	String folioCert						=	"";
	char getReceipt						=	'Y';
	try {
	 
		if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
			folioCert = "FDIST"+iNoCliente;//acuse.toString();
			Seguridad s = new Seguridad();
	
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
				_acuse = s.getAcuse();

				HashMap 			doctosAgrupados 	= getDoctosAgrupadosPorEpo(ic_documentos, doctosPorEpo , cg_responsable_int, cg_tipo_cobranza , cg_tipo_cobro_int );
				
				ArrayList 		listaClavesEpos	= (ArrayList) doctosAgrupados.get("LISTA_CLAVES_EPOS");
				if(listaClavesEpos.size()== 0){
					throw new Exception("No se ha seleccionado ningún documento válido.");
				}
				boolean	hayMasDeUnaEpoSeleccionada = listaClavesEpos.size()>1?true:false;

				if(!hayMasDeUnaEpoSeleccionada){
					String claveEpo = (String) listaClavesEpos.get(0);
					Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
					HashMap ohDatosFondeo = BeanAutDescuento.getDatosFondeo(iNoCliente, claveEpo, "4");
				}else{
					for(int k=0;k<listaClavesEpos.size();k++){
						String claveEpo = (String) listaClavesEpos.get(k);
						Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
					}
				}

				// Obtener HashMap con los Parametros de Fondeo
				HashMap catalogoTipoFondeo = BeanAutDescuento.getTipoFondeo(iNoCliente, listaClavesEpos, "4");

				Vector vecFilas		= null;
				Vector vecColumnas	= null;
				String in = "",	icMoneda = "",	monedaLinea = "";
				double	monto						= 0;
				double	tipoCambio				= 0;
				double	montoValuado			= 0;
				double	montoInteres			= 0;
				int		totalDoctosMN			= 0;
				int 		totalDoctosUSD			= 0;
				double	totalMontoMN			= 0;
				double	totalMontoUSD			= 0;
				double 	totalMontoValuadoMN	= 0;
				double 	totalMontoValuadoUSD	= 0;
				double	totalMontoIntMN		= 0;
				double	totalMontoIntUSD		= 0;
				int		totalDoctosConv		= 0;
				double	totalMontoDescMN		= 0;
				double	totalMontoDescUSD		= 0;
				double	totalMontoConvMN		= 0;
				double	totalMontoConv			= 0;
				double	totalMontoIntConv 	= 0;

				int i=0;
				for(int k=0;k<listaClavesEpos.size();k++){

					String 	claveEpo 		= (String) listaClavesEpos.get(k);
					HashMap 	doctos 	      = (HashMap) doctosAgrupados.get("DOCTOS_AGRUPADOS");
					in = Comunes.arrayListToString((ArrayList)doctos.get(claveEpo),",");

					vecFilas = autSolic.getDoctosSeleccionados(claveEpo,iNoCliente,ic_pyme,"","","",in,hidTipoCredito);

					for(i=0;i<vecFilas.size();i++){
						vecColumnas 		= 	(Vector)vecFilas.get(i);
						icMoneda				=	(String)vecColumnas.get(5);
						monto					=	Double.parseDouble(vecColumnas.get(20).toString());
						tipoCambio			=	Double.parseDouble(vecColumnas.get(8).toString());
						montoValuado		=	monto*tipoCambio;
						montoInteres 		=	Double.parseDouble(vecColumnas.get(15).toString());
						monedaLinea			= 	(String)vecColumnas.get(19);

						if("1".equals(icMoneda)){
							totalDoctosMN 			++;
							totalMontoMN 			+= monto;
							totalMontoValuadoMN 	+= montoValuado;
							totalMontoIntMN		+= montoInteres;
						}

						if("54".equals(icMoneda)&&"54".equals(monedaLinea)){
							totalDoctosUSD			++;
							totalMontoUSD 			+= monto;
							totalMontoValuadoUSD	+= montoValuado;
							totalMontoIntUSD		+= montoInteres;
						}

						if("54".equals(icMoneda)&&"1".equals(monedaLinea)){
							totalDoctosConv		++;
							totalMontoConv			+= monto;
							totalMontoConvMN		+= montoValuado;
							totalMontoIntConv		+= montoInteres;
						}
					}
				}

		ArrayList 		listaResp_int	= (ArrayList) doctosAgrupados.get("LISTA_RESP_INT");
		ArrayList 		listaTipoCobranza	= (ArrayList) doctosAgrupados.get("LISTA_TIPO_COBRANZA");
		ArrayList 		listaCobroInt	= (ArrayList) doctosAgrupados.get("LISTA_COBRO_INT");

				acuse = autSolic.confirmaSolic(
					String.valueOf(totalMontoMN),
					String.valueOf(totalMontoIntMN),
					String.valueOf(totalMontoDescMN),
					String.valueOf(totalMontoUSD),
					String.valueOf(totalMontoIntUSD),
					String.valueOf(totalMontoDescUSD),
					iNoUsuario,
					_acuse,
					listaClavesEpos,
					hidTipoCredito,
					(HashMap) doctosAgrupados.get("DOCTOS_AGRUPADOS"),
					catalogoTipoFondeo,
					listaResp_int,
					listaTipoCobranza,
					listaCobroInt				
				);
				String fechaCarga="",horaCarga="", aux_acuse="";
				aux_acuse = acuse;
				vecFilas = autSolic.getDoctosSeleccionadosIF(aux_acuse,hidTipoCredito);
				String claveDeLaEpo = null;
				for(i=0;i<vecFilas.size();i++){
					vecColumnas 		=  (Vector)vecFilas.get(i);
					acuse 				=	(String)vecColumnas.get(19);
					fechaCarga			=	(String)vecColumnas.get(20);
					horaCarga			=	(String)vecColumnas.get(21);
					break;
				}

				String usuario = iNoUsuario+" "+strNombreUsuario;
				jsonObj.put("acuse", aux_acuse);
				jsonObj.put("_acuse", acuse);
				jsonObj.put("fechaCarga", fechaCarga);
				jsonObj.put("horaCarga", horaCarga);
				jsonObj.put("usuario", usuario);
                                jsonObj.put("operaContrato",operaContrato);
				jsonObj.put("success", new Boolean(true));

			} else{
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "La autentificación no se llevó a cabo.<br>Proceso CANCELADO");
			}
		}
	} catch(Exception e) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error en el proceso. "+e+" <br>Proceso CANCELADO");
	}
	infoRegresar = jsonObj.toString();
}

%>
<%!
	public HashMap getDoctosAgrupadosPorEpo(String[] doctosSeleccionados, String[] doctosPorEpo, 
	 String[] cg_responsable_int,  String[] cg_tipo_cobranza ,  String[] cg_tipo_cobro_int  ){
		
		HashMap 			doctosAgrupados 	= new HashMap();
		LinkedHashSet	listaClaves			= new LinkedHashSet();
		ArrayList		listaClavesEpos	= new ArrayList();
		HashMap			resultado			= new HashMap();
		
		ArrayList		li_responsable_int	= new ArrayList();
		ArrayList		li_tipo_cobranza	= new ArrayList();
		ArrayList		li_tipo_cobro_int	= new ArrayList();
		
		for(int i=0;i<doctosPorEpo.length;i++){
			String[] doctoEpo =  doctosPorEpo[i].split(",");
			String 	docto 	=	doctoEpo[0];
			String 	claveEpo =	doctoEpo[1];
			
			if(contains(doctosSeleccionados,docto)){
				ArrayList lista = (ArrayList) doctosAgrupados.get(claveEpo);
				if(lista == null){
					lista = new ArrayList();
					doctosAgrupados.put(claveEpo,lista);
				}
				lista.add(docto);
				listaClaves.add(claveEpo);
			}
			
			li_responsable_int.add(cg_responsable_int[i]); 
			li_tipo_cobranza.add(cg_tipo_cobranza[i]);
			li_tipo_cobro_int.add(cg_tipo_cobro_int[i]);
		}

		Iterator itr = listaClaves.iterator();
		while(itr.hasNext()){
			listaClavesEpos.add(itr.next());
		}

		resultado.put("DOCTOS_AGRUPADOS",	doctosAgrupados);
		resultado.put("LISTA_CLAVES_EPOS",	listaClavesEpos);
		resultado.put("LISTA_RESP_INT",	li_responsable_int);
		resultado.put("LISTA_TIPO_COBRANZA",	li_tipo_cobranza);
		resultado.put("LISTA_COBRO_INT",	li_tipo_cobro_int);
		
		

		return resultado;
	}
	
	public boolean contains(String []array, String element){
		if(array == null || array.length == 0 ) return false;
		for(int i=0;i<array.length;i++){
			String tmp = array[i];
			if( tmp == null && element == null ){
				return true;
			}
			if(tmp != null && tmp.equals(element)){
				return true;
			}
		}
		return false;
	}

%>

<%=infoRegresar%>
