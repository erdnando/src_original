<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, java.math.*, com.netro.descuento.*,
	com.netro.distribuidores.*,com.netro.exception.*, javax.naming.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%

JSONObject jsonObj = new JSONObject();
boolean SIN_COMAS = false;
String fechaHoy	= "";
String fechaActual	= "";
String HoraActual	= "";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	HoraActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}
String 	ic_epo							= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String 	numero_credito					= (request.getParameter("numero_credito")==null)?"":request.getParameter("numero_credito");
String 	df_fecha_seleccion_de		= (request.getParameter("df_fecha_seleccion_de")==null)?fechaHoy:request.getParameter("df_fecha_seleccion_de");
String 	df_fecha_seleccion_a			= (request.getParameter("df_fecha_seleccion_a")==null)?fechaHoy:request.getParameter("df_fecha_seleccion_a");
String	tipoArchivo						= (request.getParameter("tipoArchivo")==null)?"":request.getParameter("tipoArchivo");
boolean  hayMasDeUnaEpoSeleccionada = "true".equals(request.getParameter("hayMasDeUnaEpoSeleccionada"))?true:false;

//Fodea 020-2014
ParametrosDist  BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
String	ventaCartera =  BeanParametro.DesAutomaticoEpo(ic_epo,"PUB_EPO_VENTA_CARTERA"); 

	
try {
	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
	
	AutorizacionSolic autSolic = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);
	StringBuffer texto = new StringBuffer();
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

	if(application.getInitParameter("EpoCemex").equals(ic_epo)){
		if(ventaCartera.equals("S") )  {		
			texto.append( 
			new String( "Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del ")+
			new String("(los) Documento (s) final (es) descrito (s) en el mismo derivado del DESCUENTO Y/O FACTORAJE CON RECURSO o del ")+
			new String(" DESCUENTO Y/O FACTORAJE SIN RECURSO según corresponda. Dicha aceptación tendrá plena validez para todos los efectos ")+
			new String("legales a que haya lugar. En este mismo acto se genera el aviso de notificación a la EMPRESA DE PRIMER ORDEN.  ")+
			new String(" En términos de los artículos 32 C del Código Fiscal y 427 de la Ley General de Títulos y Operaciones de Crédito, ")+
			new String("para los efectos legales conducentes, el INTERMEDIARIO FINANCIERO se obliga a notificar por sus propios medios al ")+
			new String("CLIENTE Y/O DISTRIBUIDOR la transmisión de los derechos y/o cuentas por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO ")+
			new String("o del DESCUENTO Y/O FACTORAJE SIN RECURSO. No obstante, en términos de los citados artículos, en caso de existir COBRANZA DELEGADA, ")+
			new String("el INTERMEDIARIO FINANCIERO no estará obligado a notificar al CLIENTE Y/O DISTRIBUIDOR la transmisión de los derechos y/o cuentas ")+
			new String("por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO."));
			
		}else  {
			texto.append(
				new String(" Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del  ")+
				new String(" (los) Documento (s) final (es) descrito (s) en el mismo. Dicha aceptación tendrá plena validez para ")+
				new String(" todos los efectos legales. En este mismo acto se genera el aviso de notificación a la  ")+
				new String(" EMPRESA DE PRIMER ORDEN y al DISTRIBUIDOR, por lo que tiene la validez de acuse de recibo, en términos  ")+
				new String(" de los artículos 32 C del Código Fiscal o  2038 y 2041 de Código Civil Federal , según corresponda para  ")+
				new String(" los efectos legales conducentes. "));
		}
	}else{
		if(ventaCartera.equals("S") )  {
			texto.append( 
				new String( "Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del ")+
				new String("(los) Documento (s) final (es) descrito (s) en el mismo derivado del DESCUENTO Y/O FACTORAJE CON RECURSO o del ")+
				new String(" DESCUENTO Y/O FACTORAJE SIN RECURSO según corresponda. Dicha aceptación tendrá plena validez para todos los efectos ")+
				new String("legales a que haya lugar. En este mismo acto se genera el aviso de notificación a la EMPRESA DE PRIMER ORDEN.  ")+
				new String(" En términos de los artículos 32 C del Código Fiscal y 427 de la Ley General de Títulos y Operaciones de Crédito, ")+
				new String("para los efectos legales conducentes, el INTERMEDIARIO FINANCIERO se obliga a notificar por sus propios medios al ")+
				new String("CLIENTE Y/O DISTRIBUIDOR la transmisión de los derechos y/o cuentas por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO ")+
				new String("o del DESCUENTO Y/O FACTORAJE SIN RECURSO. No obstante, en términos de los citados artículos, en caso de existir COBRANZA DELEGADA, ")+
				new String("el INTERMEDIARIO FINANCIERO no estará obligado a notificar al CLIENTE Y/O DISTRIBUIDOR la transmisión de los derechos y/o cuentas ")+
				new String("por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO."));
				
		}else  {
			texto.append(
				new String(" Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del  ")+
				new String(" (los) Documento (s) final (es) descrito (s) en el mismo. Dicha aceptación tendrá plena validez para ")+
				new String(" todos los efectos legales. En este mismo acto se genera el aviso de notificación a la  ")+
				new String(" EMPRESA DE PRIMER ORDEN y al DISTRIBUIDOR, por lo que tiene la validez de acuse de recibo, en términos  ")+
				new String(" de los artículos 32 C del Código Fiscal o  2038 y 2041 de Código Civil Federal , según corresponda para  ")+
				new String(" los efectos legales conducentes. "));
		}
        texto.append("\n\nPor otra parte, a partir del 17 de octubre de 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que sí ha emitido o emitirá "+
        "a su CLIENTE o DISTRIBUIDOR el CFDI por la operación comercial que le dio origen a esta transacción, según sea el caso y conforme establezcan las disposiciones fiscales vigentes.");
	}
	pdfDoc.setTable(1, 100);
	pdfDoc.setCell(texto.toString(),"formas",ComunesPDF.LEFT);
	pdfDoc.addTable();

	ArrayList listaClavesEpo = null;
	int i = 0;
	int elementos = 0;
	if(hayMasDeUnaEpoSeleccionada){

		listaClavesEpo = Comunes.stringToArrayList(ic_epo, ",");

		// Validar el horario de servicio de las epos seleccionadas, si alguna de estas no se encuentra en el horario de servicio
		// suprimirlas de la lista de epos validas
		for(int k=0;k<listaClavesEpo.size();k++){
			try {
				String claveEpo = (String) listaClavesEpo.get(k);
				Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
			}catch(Exception e){
				listaClavesEpo.remove(k);
				k--;
				/*
					if(e instanceof NafinException){
						NafinException e = (NafinException) e;
						if(e.getCause().equals("SIST0001")){
							throw e;
						}
					}
				*/
			}
		}

		// Validar que haya al menos una EPO 
		if(listaClavesEpo.size() == 0){
			throw new Exception("No se encontró ninguna EPO con horario válido");
		} else if (listaClavesEpo.size() == 1){
			ic_epo = (String) listaClavesEpo.get(0);
			hayMasDeUnaEpoSeleccionada = false;
		}
	}

	if(!hayMasDeUnaEpoSeleccionada){
		Horario.validarHorario(4, strTipoUsuario, ic_epo, iNoCliente);
		listaClavesEpo = new ArrayList();
		listaClavesEpo.add(ic_epo);
	}

	// Obtener HashMap con los Parametros de Fondeo
	HashMap catalogoTipoFondeo = hayMasDeUnaEpoSeleccionada?BeanAutDescuento.getTipoFondeo(iNoCliente, listaClavesEpo, "4"):null;
	Vector vecFilas = null;
	Vector vecColumnas = null;
	boolean mostrarCabecera = true;
	pdfDoc.setTable(7, 100);
	for(int k=0;k<listaClavesEpo.size();k++){

		String claveEpo 	= (String) listaClavesEpo.get(k);
		vecFilas 			= autSolic.getLineasConDoctos(claveEpo,iNoCliente,numero_credito,df_fecha_seleccion_de,df_fecha_seleccion_a);
		
		for(i=0;i<vecFilas.size();i++){
			
			vecColumnas 			= 	(Vector)vecFilas.get(i);
			String nombreAcreditado	=	(String)vecColumnas.get(0);
			String numLineaCredito	=	(String)vecColumnas.get(2);
			String moneda				=	(String)vecColumnas.get(3);
			String saldoInicial		=	(String)vecColumnas.get(5);
			String montoSeleccionado=	(String)vecColumnas.get(6);
			String numDoctos			=	(String)vecColumnas.get(7);
			String saldoDisponible	=	(String)vecColumnas.get(8);
			
			if(mostrarCabecera){
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. línea de crédito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Saldo inicial","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto operado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. doctos. operados","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Saldo disponible","celda01",ComunesPDF.CENTER);
				mostrarCabecera = false;
			}
			pdfDoc.setCell(nombreAcreditado,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numLineaCredito,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(saldoInicial,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoSeleccionado,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numDoctos,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(saldoDisponible,2),"formas",ComunesPDF.CENTER);
		} // for
	}
	pdfDoc.addTable();

	mostrarCabecera = true;
	String	moneda	=	"",	icMoneda	=	"",	icDocumento	=	"",ic_pyme	=	"",	tipoFinan = "";
	String	igNumeroDocto	=	"", fechaSolicitud = "",	distribuidor = "",	tipoConversion = "",	referencia = "",	plazo = "",	fechaVto = "";
	String	tipoPagoInteres=	"",	rs_referencia = "",	rs_campo1 = "",	rs_campo2 = "",	rs_campo3 = "",	rs_campo4 = "",	rs_campo5 = "";
	double 	montoInteres = 0,	tasaInteres = 0,	monto = 0,	tipoCambio = 0,	montoValuado = 0;

	//totales
	int totalDoctosMN= 0,	totalDoctosUSD = 0;
	double totalMontoMN = 0,	totalMontoUSD = 0,	totalMontoValuadoMN = 0,	totalMontoValuadoUSD	= 0,	totalMontoIntMN = 0,totalMontoIntUSD = 0;
	int columnas = 12;
	if(hayMasDeUnaEpoSeleccionada){ columnas +=1;}
	pdfDoc.setTable(columnas, 100);
	for(int k=0;k<listaClavesEpo.size();k++){

		String claveEpo 	= (String) listaClavesEpo.get(k); 
		vecFilas 			= autSolic.getDoctosSeleccionados(claveEpo,iNoCliente,numero_credito,df_fecha_seleccion_de,df_fecha_seleccion_a,null);

		for(i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);

			igNumeroDocto			=	(String)vecColumnas.get(0);
			fechaSolicitud			=	(String)vecColumnas.get(1);
			distribuidor			=	(String)vecColumnas.get(2);
			ic_pyme					=	(String)vecColumnas.get(3);
			moneda					=	(String)vecColumnas.get(4);
			icMoneda					=	(String)vecColumnas.get(19);
			monto						=	Double.parseDouble(vecColumnas.get(20).toString());
			tipoConversion			=	(String)vecColumnas.get(7);
			tipoCambio				=	Double.parseDouble(vecColumnas.get(8).toString());
			montoValuado			=	monto*tipoCambio;
			referencia				=	(String)vecColumnas.get(9);
			tasaInteres				=	Double.parseDouble(vecColumnas.get(33).toString());
			plazo						=	(String)vecColumnas.get(11);
			fechaVto					=	(String)vecColumnas.get(12);
			tipoPagoInteres		=	(String)vecColumnas.get(13);
			montoInteres 			=	Double.parseDouble(vecColumnas.get(15).toString());
			icDocumento				=	(String)vecColumnas.get(16);

			String rs_nomEPO		=	(String)vecColumnas.get(17);
			String rs_numDist		=	(String)vecColumnas.get(21);
			String rs_cveTasa		=	(String)vecColumnas.get(22);
			String rs_tipoConver	=	(String)vecColumnas.get(23);
			String rs_tipoCamb	=	(String)vecColumnas.get(24);
			String rs_respInt		=	(String)vecColumnas.get(25);
			String rs_tipoCob		=	(String)vecColumnas.get(26);
			rs_referencia 			=	(String)vecColumnas.get(27);
			rs_campo1				=	(String)vecColumnas.get(28);
			rs_campo2				=	(String)vecColumnas.get(29);
			rs_campo3				=	(String)vecColumnas.get(30);
			rs_campo4				=	(String)vecColumnas.get(31);
			rs_campo5 				=	(String)vecColumnas.get(32);
			String cg_numero_cuenta = autSolic.getCuentaAutorizada(claveEpo,iNoCliente,ic_pyme,icMoneda);
			if(hayMasDeUnaEpoSeleccionada){
				tipoFinan = BeanAutDescuento.getDescripcionTipoFondeo(catalogoTipoFondeo,claveEpo);
			}
			if("1".equals(icMoneda)){
				totalDoctosMN 			++;
				totalMontoMN 			+= monto;
				totalMontoValuadoMN 	+= montoValuado;
				totalMontoIntMN		+= montoInteres;
			}else if("54".equals(icMoneda)){
				totalDoctosUSD			++;
				totalMontoUSD 			+= monto;
				totalMontoValuadoUSD	+= montoValuado;
				totalMontoIntUSD		+= montoInteres;
			}
			
			if(mostrarCabecera){
				pdfDoc.setCell("No. de documento Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Distribuidor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia tasa de interés","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa interés","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto interés","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de pago intereses","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta Bancaria Distribuidor","celda01",ComunesPDF.CENTER);
				if(hayMasDeUnaEpoSeleccionada){
					pdfDoc.setCell("Tipo de Financiamiento","celda01",ComunesPDF.CENTER);
				}
				mostrarCabecera = false;
			} //if
				pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaSolicitud,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(tasaInteres,2)+"%","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipoPagoInteres,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cg_numero_cuenta,"formas",ComunesPDF.CENTER);
				if(hayMasDeUnaEpoSeleccionada){
					pdfDoc.setCell(tipoFinan,"formas",ComunesPDF.CENTER);
				}
		}// for
	}
	pdfDoc.addTable();
		
	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>