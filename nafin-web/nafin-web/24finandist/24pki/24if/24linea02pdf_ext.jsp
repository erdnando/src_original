<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*, com.netro.anticipos.*,
	com.netro.exception.*, 
	netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

JSONObject jsonObj = new JSONObject();

Vector vecFilas	=null;
Vector vecColumnas=null;
String _acuse		= (request.getParameter("_acuse")==null)?"":request.getParameter("_acuse");
String fechaHoy	= (request.getParameter("fechaHoy")==null)?"":request.getParameter("fechaHoy");
String horaCarga	= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");
String usuario		= (request.getParameter("usuario")==null)?"":request.getParameter("usuario");

String totMtoAuto	  	= (request.getParameter("totMtoAuto")==null)?"":request.getParameter("totMtoAuto");
String totDocs	  		= (request.getParameter("totDocs")==null)?"":request.getParameter("totDocs");
String totMtoAutoDol = (request.getParameter("totMtoAutoDol")==null)?"":request.getParameter("totMtoAutoDol");
String totDocsDol	  	= (request.getParameter("totDocsDol")==null)?"":request.getParameter("totDocsDol");

String aFolio [] 		= request.getParameterValues("folio");
String aFolioArm [] 	= request.getParameterValues("rs_folioArmado");
String aNumCliente [] 	= request.getParameterValues("rs_numCliente");
String aNomCliente [] 	= request.getParameterValues("rs_nomPyme");
String aFolioRel [] 	= request.getParameterValues("rs_folioRel");
String aFechaSol [] 	= request.getParameterValues("rs_fechaSol");
String aMoneda[]		= request.getParameterValues("moneda");
String aMonto [] 		= request.getParameterValues("monto");
String aEstatus [] 		= request.getParameterValues("estatus");
String aTipoCobro [] 	= request.getParameterValues("tipo_cobro_int");
String aCausa [] 		= request.getParameterValues("causa_rechazo");
String aMontoAuto [] 	= request.getParameterValues("monto_auto");
String aFechaVto [] 	= request.getParameterValues("fecha_vto");
String aBanco [] 		= request.getParameterValues("banco");
String aCuenta [] 		= request.getParameterValues("cuenta");
String aTipo[]			= request.getParameterValues("tipo");
String atipoSolicitud[]	= request.getParameterValues("tipoSolicitud");
String tipo_liq			= request.getParameter("tipo_liq");

try {

	LineaCredito lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class);

	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

	pdfDoc.setTable(4, 50);
	pdfDoc.setCell("Moneda Nacional","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Dólares","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de solicitudes procesadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto de Solicitudes procesadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de solicitudes procesadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto de Solicitudes procesadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(totDocs,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAuto,2),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell(totDocsDol,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAutoDol,2),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Leyenda legal","formas",ComunesPDF.CENTER,4);
	pdfDoc.addTable();

	pdfDoc.setTable(12, 100);
	pdfDoc.setCell("Folio de solicitud","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("No. de distribuidor","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Distribuidor","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo solicitud","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Núm. línea relacionada","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha solicitud","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Estatus asignado","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Núm. clientes afiliados a la EPO","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo Cobro de Interes","celda01",ComunesPDF.CENTER);

	if (aFolio.length > 0){
		for (int i=0;i<aFolio.length;i++){
			if(!"".equals(aEstatus[i])) {
				vecFilas = lineaCredito.getDescripcion(aMoneda[i],aEstatus[i],aTipoCobro[i]);
				vecColumnas = (Vector)vecFilas.get(0);
				String nomMoneda	= (String)vecColumnas.get(0);
				String nomEstatus	= (String)vecColumnas.get(1);
				String nomTipoCobro	= (String)vecColumnas.get(2);

				pdfDoc.setCell(aFolioArm[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(aNumCliente[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(aNomCliente[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(atipoSolicitud[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(aFolioRel[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(aFechaSol[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nomEstatus,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(aCausa[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nomMoneda,"formas",ComunesPDF.CENTER);
				if(!"".equals(aMontoAuto[i])){
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(aMontoAuto[i],2),"formas",ComunesPDF.CENTER);
				}else {
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				}
				pdfDoc.setCell(aFechaVto[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nomTipoCobro,"formas",ComunesPDF.CENTER);
			}
		}
	}

	if (!("0".equals(totDocs))){
		pdfDoc.setCell("Totales MN","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(totDocs,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAuto,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
	}
	if (!("0".equals(totDocsDol))){
		pdfDoc.setCell("Totales Dolares","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(totDocsDol,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAutoDol,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		pdfDoc.setCell("","formas",ComunesPDF.LEFT);
	}
	pdfDoc.addTable();
	
	pdfDoc.setTable(2, 50);
	pdfDoc.setCell("Datos de cifras de control","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de Acuse","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(_acuse,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Fecha","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Hora","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(horaCarga,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Nombre y número de usuario","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(usuario,"formas",ComunesPDF.LEFT);
	pdfDoc.addTable();

	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>