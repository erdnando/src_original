<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	netropology.utilerias.*,
	com.netro.exception.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();

String _acuse			= (request.getParameter("_acuse")==null)?"":request.getParameter("_acuse");
String fechaHoy		= (request.getParameter("fechaHoy")==null)?"":request.getParameter("fechaHoy");
String horaActual		= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");
String usuario			= (request.getParameter("usuario")==null)?"":request.getParameter("usuario");
String totMtoAuto	  	= (request.getParameter("totMtoAuto")==null)?"":request.getParameter("totMtoAuto");
String totDocs	  		= (request.getParameter("totDocs")==null)?"":request.getParameter("totDocs");
String totMtoAutoDol = (request.getParameter("totMtoAutoDol")==null)?"":request.getParameter("totMtoAutoDol");
String totDocsDol	  	= (request.getParameter("totDocsDol")==null)?"":request.getParameter("totDocsDol");

String amoneda[] 				= request.getParameterValues("nomMoneda");
String noNafinDists[]		= request.getParameterValues("noNafinEs_aux");
String folios[]				= request.getParameterValues("folios_aux");
String montoAutos[] 			= request.getParameterValues("montoAutos_aux");
String vencimientos[]		= request.getParameterValues("vencimientos_aux");
String nCtaDists[] 			= request.getParameterValues("nCtaDist_aux");
String nCtaIfs[]				= request.getParameterValues("nCtaIfs_aux");
String ic_epos[]				= request.getParameterValues("ic_epos_aux");
String nomPymes[] 			= request.getParameterValues("nomPymes");
String tipoSol_aux[] 		= request.getParameterValues("tipoSol_aux");
String monedas_aux[] 		= request.getParameterValues("monedas_aux");
String tipo_cobro_ints[]	= request.getParameterValues("tipoCobro_aux");
String ifBancos[] 			= request.getParameterValues("ifBancos");
String ifBancos_aux[] 		= request.getParameterValues("ifBancos_aux");
String nPlazoMax[]			= request.getParameterValues("nPlazoMax");
String nCtaBancaria[]		= request.getParameterValues("nCtaBancaria");

try {

	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;

	contenidoArchivo.append("Moneda Nacional,,Dolares");
	contenidoArchivo.append("\nNúmero de solicitudes capturadas, Monto de Solicitudes capturadas, Número de Solicitudes capturadas, Monto de Solicitudes capturadas");
	contenidoArchivo.append("\n"+totDocs+","+totMtoAuto+","+totDocsDol+","+totMtoAutoDol);

	if(!strPerfil.equals("IF FACT RECURSO")){
		contenidoArchivo.append("\nLeyenda legal");
		contenidoArchivo.append("\n\nEPO,Número Distribuidor,Nombre Distribuidor,Tipo de solicitud,Folio solicitud relacionada,Moneda,Monto autorizado,Fecha de vencimiento,Tipo de cobro de intereses,No. Cuenta Distribuidor,IF Banco de Servicio,No. Cuenta IF");
		if (ic_epos.length > 0){
			for(int i=0;i<ic_epos.length;i++){
				contenidoArchivo.append("\n"+ic_epos[i].replace(',',' ')+","+noNafinDists[i]+","+nomPymes[i].replace(',',' ')+","+tipoSol_aux[i]+","+folios[i]+","+amoneda[i]+","+montoAutos[i]+","+vencimientos[i]+","+tipo_cobro_ints[i]+","+nCtaDists[i]+","+ifBancos[i].replace(',',' ')+","+nCtaIfs[i]);
			}
		}

		if (!("0".equals(totDocs))){
			contenidoArchivo.append("\nTotales MN,"+totDocs+",,,,,"+totMtoAuto);
		}
		if (!("0".equals(totDocsDol))){
			contenidoArchivo.append("\nTotales Dolares,"+totDocsDol+",,,"+totMtoAutoDol);
		}	
	}else{
		contenidoArchivo.append("\n\nEPO,Número Distribuidor,Nombre Distribuidor,Moneda,Monto autorizado,Fecha de vencimiento,Plazo máximo ampliación descuento,Cuenta Bancaria");
		if (ic_epos.length > 0){
			for(int i=0;i<ic_epos.length;i++){
				contenidoArchivo.append("\n"+ic_epos[i].replace(',',' ')+","+noNafinDists[i]+","+nomPymes[i].replace(',',' ')+","+amoneda[i]+","+montoAutos[i]+","+vencimientos[i]+","+nPlazoMax[i]+","+nCtaBancaria[i]);
			}
		}
		if (!("0".equals(totDocs))){
			contenidoArchivo.append("\nTotales Dolares,"+totDocs+",,,"+totMtoAuto);
		}
		if (!("0".equals(totDocsDol))){
			contenidoArchivo.append("\nTotales Dolares,"+totDocsDol+",,,"+totMtoAutoDol);
		}
	}

	contenidoArchivo.append("\n \n \nDatos de cifras de control");
	contenidoArchivo.append("\nNum. acuse,"+_acuse);
	contenidoArchivo.append("\nFecha,"+fechaHoy);
	contenidoArchivo.append("\nHora,"+horaActual);
	contenidoArchivo.append("\nNombre y número de usuario,"+usuario.replace(',',' '));

	if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivo = archivo.nombre;
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>