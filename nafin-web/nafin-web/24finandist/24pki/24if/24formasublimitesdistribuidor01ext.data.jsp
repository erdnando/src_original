<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	netropology.utilerias.*,
	netropology.utilerias.usuarios.*,
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	com.netro.pdf.*,
	net.sf.json.*,	
	net.sf.json.JSONArray,	
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="/appComun.jspf" %>
<%//PARAMETROS

String claveEpo = (request.getParameter("catalogoEpo") == null) ? "" : request.getParameter("catalogoEpo");
String clavePyme = (request.getParameter("catalogoDist") == null) ? "" : request.getParameter("catalogoDist");

String informacion = (request.getParameter("informacion") == null) ? ""   : request.getParameter("informacion");
String operacion = (request.getParameter("operacion") == null) ? ""   : request.getParameter("operacion");

int  start= 0, limit =0;
%>
<%
String infoRegresar = "", consulta ="";
HashMap datos = new HashMap();
JSONArray registros = new JSONArray(); 
JSONObject jsonObj = new JSONObject();
%>
<%


 if(informacion.equals("CatalogoEPO")) {
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveIf(iNoCliente);
	
	/*if (tipo_credito.equals("D")){
		cat.setTipoCredito("D");
	}else if (tipo_credito.equals("C")){
		cat.setTipoCredito("C");
	}else if (tipo_credito.equals("C")){
		cat.setTipoCredito("F");
	}*/
	//cat.setOrden("2");
	//
	infoRegresar = cat.getJSONElementos();	
	System.err.println(infoRegresar);
	/*	
	SubLimitesDistribuidor clase = new SubLimitesDistribuidor();	 
	
	clase.setIcIf(iNoCliente);
	clase.setModalidad("D");
	Registros catalogo = clase.getDataCatalogoEPO();
	//List cat=clase.getDataCatalogoEPOList();
	consulta =  "{\"success\": true, \"total\": \"" + catalogo.getNumeroRegistros() + "\", \"registros\": " + catalogo.getJSONData()+"}";
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("total",new Integer(catalogo.getNumeroRegistros()));
	jsonObj.put("registros",(catalogo.getJSONData()));
	
   jsonObj = JSONObject.fromObject(consulta);	
	
	infoRegresar = jsonObj.toString();
	*/
	
}else if( informacion.equals("CatalogoDist") ) {
	String ic_epo = (request.getParameter("epo")==null)?"":request.getParameter("epo");
	
	
	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}
	
	

}else if(informacion.equals("catalogoMoneda")) {

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("CD_NOMBRE");
	catalogo.setTabla("comcat_moneda");	
	catalogo.setValoresCondicionIn("1,54", Integer.class);	

	infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("Consultar")   ) {
	
	SubLimitesDistribuidor paginador = new SubLimitesDistribuidor();	
			paginador.setIcEPO(claveEpo);
			paginador.setIcPyme(clavePyme);
			paginador.setIcIf(iNoCliente);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
										
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			if(informacion.equals("Consultar") ){
				try {
					if (operacion.equals("Generar")) {	//Nueva consulta
						queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
					}
						consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
				} catch(Exception e) {
					throw new AppException("Error en la paginacion", e);
				}
					//Thread.sleep(5000);
					jsonObj = JSONObject.fromObject(consulta);
				
			}
			
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("porcenteje.capturado")){
	String ic_moneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
	SubLimitesDistribuidor paginador = new SubLimitesDistribuidor();	
			paginador.setIcEPO(claveEpo);
			paginador.setIcPyme(clavePyme);
			paginador.setIcIf(iNoCliente);
			paginador.setIcMoneda(ic_moneda);
			HashMap hash = new HashMap();
			if(!paginador.tienePocentajeCapturado()){
				
				Registros data=paginador.getDataGuardada();
				
				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("data", data.getJSONData());	
			jsonObj.put("guardar", new Boolean(false));	
				
			}else{
				jsonObj.put("guardar", new Boolean(true));	
			}
			
		infoRegresar = jsonObj.toString();	
			

}else if (informacion.equals("Guarbar_Cambios")){

	String loginUsuario = (String)request.getSession().getAttribute("Clave_usuario");
	String nombreUsuario = (String)request.getSession().getAttribute("strNombreUsuario");
 
				
	String porcentaleLinea = (request.getParameter("porcentaleLinea")==null)?"":request.getParameter("porcentaleLinea");
	String icMoneda = (request.getParameter("icMoneda")==null)?"":request.getParameter("icMoneda");
	String pocentajeLineaOriginal = (request.getParameter("pocentajeOriginal")==null)?"":request.getParameter("pocentajeOriginal");
	String guardar = (request.getParameter("guardar")==null)?"":request.getParameter("guardar");
	
	SubLimitesDistribuidor paginador = new SubLimitesDistribuidor();	
		paginador.setIcEPO(claveEpo);
		paginador.setIcPyme(clavePyme);
		paginador.setIcIf(iNoCliente);
		paginador.setIcMoneda(icMoneda);
		paginador.setPorcentajeLinea(porcentaleLinea);
		
		if(!paginador.tienePocentajeCapturado()){
			paginador.setUsuarioAlta(nombreUsuario);
			if(paginador.guardarDatos()){
				jsonObj.put("msj", new String("La operación se realizó con éxito"));
			}else{
					jsonObj.put("msj", new String("Ocurrio un error al insertar el nuevo registro"));
			}
		}else{
			paginador.setUsuarioModifico(nombreUsuario);
			if(paginador.actualizaDatos()){
				jsonObj.put("msj", new String("La operación se realizó con éxito"));
			}else{
					jsonObj.put("msj", new String("Ocurrio un error al insertar el nuevo registro"));
			}
		}
		if("true".equals(guardar)){
			//es la primera vez que se captura % de la linea 
			
		}else{
		}
		
	infoRegresar = jsonObj.toString();	
	
}
%>
<%=infoRegresar%>

