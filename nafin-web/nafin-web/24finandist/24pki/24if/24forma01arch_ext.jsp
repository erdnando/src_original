<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*, java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String informacion   			=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
boolean SIN_COMAS = false;
String fechaHoy	= "";
String fechaActual	= "";
String HoraActual	= "";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	HoraActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}
//PARAMETROS QUE PROVINEN DE LA PAGINA ACTUAL
String 	ic_epo							= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String 	numero_credito					= (request.getParameter("numero_credito")==null)?"":request.getParameter("numero_credito");
String 	df_fecha_seleccion_de		= (request.getParameter("df_fecha_seleccion_de")==null)?fechaHoy:request.getParameter("df_fecha_seleccion_de");
String 	df_fecha_seleccion_a			= (request.getParameter("df_fecha_seleccion_a")==null)?fechaHoy:request.getParameter("df_fecha_seleccion_a");
String	tipoArchivo						= (request.getParameter("tipoArchivo")==null)?"":request.getParameter("tipoArchivo");
boolean  hayMasDeUnaEpoSeleccionada = "true".equals(request.getParameter("hayMasDeUnaEpoSeleccionada"))?true:false;

try {
	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
	
	AutorizacionSolic autSolic = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);

	//Fodea 020-2014
	ParametrosDist  BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	String	ventaCartera =  BeanParametro.DesAutomaticoEpo(ic_epo,"PUB_EPO_VENTA_CARTERA"); 


	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	StringBuffer contenidoArchVar = new StringBuffer(2000);
	StringBuffer contenidoArchFijo = new StringBuffer(2000);
	StringBuffer contenidoArchIntegral = new StringBuffer(2000);
	
	String nombreArchivo = null;
	String nombreArchivoVar = "";
	StringBuffer ArchivoVariable = new StringBuffer(2000);
	String nombreArchivoFijo = "";
	StringBuffer ArchivoFijo = new StringBuffer(2000);
	ArrayList listaClavesEpo = null;
	String		nombreArchivoIntegral = "";
	StringBuffer ArchivoIntegral = new StringBuffer(2000);
	

	int i = 0;
	int elementos = 0;
	if(application.getInitParameter("EpoCemex").equals(ic_epo)){
	
		if(ventaCartera.equals("S") )  {		
			contenidoArchivo.append( 
			new String( "Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del (los) Documento (s) final (es) descrito (s) en el mismo derivado del DESCUENTO Y/O FACTORAJE CON RECURSO o del   \n ").replace(',',' ')+
			new String(" DESCUENTO Y/O FACTORAJE SIN RECURSO según corresponda. Dicha aceptación tendrá plena validez para todos los efectos legales a que haya lugar. En este mismo acto se genera el aviso de notificación a la EMPRESA DE PRIMER ORDEN.  \n ").replace(',',' ')+
			new String(" En términos de los artículos 32 C del Código Fiscal y 427 de la Ley General de Títulos y Operaciones de Crédito, para los efectos legales conducentes, el INTERMEDIARIO FINANCIERO se obliga a notificar por sus propios medios \n ").replace(',',' ')+
			new String(" CLIENTE Y/O DISTRIBUIDOR la transmisión de los derechos y/o cuentas por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO. No obstante, en términos de los citados artículos, en caso de existir COBRANZA DELEGADA,  \n ").replace(',',' ')+
			new String(" el INTERMEDIARIO FINANCIERO no estará obligado a notificar al CLIENTE Y/O DISTRIBUIDOR la transmisión de los derechos y/o cuentas  por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO.  \n ").replace(',',' '));
			
		}else  {	
			contenidoArchivo.append(
				new String("Al transmitir este mensaje de datos, bajo mi responsabilidad manifiesto para todos los efectos legales a que haya lugar, que he depositado a la EPO \n ").replace(',',' ')+
				new String("la cantidad total del monto señalado en la orden de compra, atendiendo con esto la instrucción del distribuidor para realizar las disposiciones de su línea de crédito. \n ").replace(',',' '));
		}
	}else{
		if(ventaCartera.equals("S") )  {		
			contenidoArchivo.append( 
			new String( "Al trasmitir este mensaje de datos bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del (los) Documento (s) final (es) descrito (s) en el mismo derivado del DESCUENTO Y/O FACTORAJE CON RECURSO o del   \n ").replace(',',' ')+
			new String(" DESCUENTO Y/O FACTORAJE SIN RECURSO según corresponda. Dicha aceptación tendrá plena validez para todos los efectos legales a que haya lugar. En este mismo acto se genera el aviso de notificación a la EMPRESA DE PRIMER ORDEN.  \n ").replace(',',' ')+
			new String(" En términos de los artículos 32 C del Código Fiscal y 427 de la Ley General de Títulos y Operaciones de Crédito, para los efectos legales conducentes, el INTERMEDIARIO FINANCIERO se obliga a notificar por sus propios medios \n ").replace(',',' ')+
			new String(" CLIENTE Y/O DISTRIBUIDOR la transmisión de los derechos y/o cuentas por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO. No obstante, en términos de los citados artículos, en caso de existir COBRANZA DELEGADA,  \n ").replace(',',' ')+
			new String(" el INTERMEDIARIO FINANCIERO no estará obligado a notificar al CLIENTE Y/O DISTRIBUIDOR la transmisión de los derechos y/o cuentas  por cobrar objeto del DESCUENTO Y/O FACTORAJE CON RECURSO o del DESCUENTO Y/O FACTORAJE SIN RECURSO.  \n ").replace(',',' '));
			
		}else  {	
			contenidoArchivo.append(
						new String("Al transmitir este mensaje de datos, bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del (los) Documento(s) final(es) descrito(s) en el  mismo. Dicha aceptación tendrá plena validez para todos los efectos legales. \n ").replace(',',' ')+
						new String("En este mismo acto se genera el aviso de notificación a la EMPRESA DE PRIMER ORDEN y al DISTRIBUIDOR, por lo que tiene la validez de acuse de recibo. \n ").replace(',',' ')+
						new String("Las operaciones que aparecen en esta página fueron notificadas a la EMPRESA DE PRIMER ORDEN y al DISTRIBUIDOR de conformidad y para los efectos de los artículos 45 K de la Ley General de Organizaciones y Actividades Auxiliares \n ").replace(',',' ')+
						new String("del Crédito y 32 C del Código Fiscal o del artículo 2038 del Código Civil Federal según corresponda para los efectos legales conducentes. \n ").replace(',',' '));
			}
		}
		if(hayMasDeUnaEpoSeleccionada){

			listaClavesEpo = Comunes.stringToArrayList(ic_epo, ",");

			// Validar el horario de servicio de las epos seleccionadas, si alguna de estas no se encuentra en el horario de servicio
			// suprimirlas de la lista de epos validas
			for(int k=0;k<listaClavesEpo.size();k++){
				try {
					String claveEpo = (String) listaClavesEpo.get(k);
					Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
				}catch(Exception e){
					listaClavesEpo.remove(k);
					k--;
					/*
						if(e instanceof NafinException){
							NafinException e = (NafinException) e;
							if(e.getCause().equals("SIST0001")){
								throw e;
							}
						}
					*/
				}
			}
			
			// Validar que haya al menos una EPO 
			if(listaClavesEpo.size() == 0){
				throw new Exception("No se encontró ninguna EPO con horario válido");
			} else if (listaClavesEpo.size() == 1){
				ic_epo = (String) listaClavesEpo.get(0);
				hayMasDeUnaEpoSeleccionada = false;
			}
		}

		if(!hayMasDeUnaEpoSeleccionada){
			Horario.validarHorario(4, strTipoUsuario, ic_epo, iNoCliente);
			listaClavesEpo = new ArrayList();
			listaClavesEpo.add(ic_epo);
		}

		// Obtener HashMap con los Parametros de Fondeo
		HashMap catalogoTipoFondeo = hayMasDeUnaEpoSeleccionada?BeanAutDescuento.getTipoFondeo(iNoCliente, listaClavesEpo, "4"):null;
		Vector vecFilas = null;
		Vector vecColumnas = null;
		boolean mostrarCabecera = true;
		for(int k=0;k<listaClavesEpo.size();k++){

			String claveEpo 	= (String) listaClavesEpo.get(k);
			vecFilas 			= autSolic.getLineasConDoctos(claveEpo,iNoCliente,numero_credito,df_fecha_seleccion_de,df_fecha_seleccion_a);
			
			for(i=0;i<vecFilas.size();i++){
				
				vecColumnas 			= 	(Vector)vecFilas.get(i);
				String nombreAcreditado	=	(String)vecColumnas.get(0);
				String numLineaCredito	=	(String)vecColumnas.get(2);
				String moneda				=	(String)vecColumnas.get(3);
				String saldoInicial		=	(String)vecColumnas.get(5);
				String montoSeleccionado=	(String)vecColumnas.get(6);
				String numDoctos			=	(String)vecColumnas.get(7);
				String saldoDisponible	=	(String)vecColumnas.get(8);
				
				if(mostrarCabecera){
					contenidoArchivo.append("\n \nEPO,No. línea de crédito,Moneda,Saldo inicial,Monto operado,No. doctos. operados,Saldo disponible");
					mostrarCabecera = false;
				}
				contenidoArchivo.append(
									"\n"+nombreAcreditado+","+
									numLineaCredito+","+
									moneda+","+
									saldoInicial+","+
									montoSeleccionado+","+
									numDoctos+","+
									saldoDisponible);
			} // for
		}
	
		mostrarCabecera = true;
		String	moneda	=	"",	icMoneda	=	"",	icDocumento	=	"",ic_pyme	=	"";
		String	igNumeroDocto	=	"", fechaSolicitud = "",	distribuidor = "",	tipoConversion = "",	referencia = "",	plazo = "",	fechaVto = "";
		String	tipoPagoInteres=	"",	rs_referencia = "",	rs_campo1 = "",	rs_campo2 = "",	rs_campo3 = "",	rs_campo4 = "",	rs_campo5 = "";
		double 	montoInteres = 0,	tasaInteres = 0,	monto = 0,	tipoCambio = 0,	montoValuado = 0;

		//totales
		int totalDoctosMN= 0,	totalDoctosUSD = 0;
		double totalMontoMN = 0,	totalMontoUSD = 0,	totalMontoValuadoMN = 0,	totalMontoValuadoUSD	= 0,	totalMontoIntMN = 0,totalMontoIntUSD = 0;
		for(int k=0;k<listaClavesEpo.size();k++){
			
			String claveEpo 	= (String) listaClavesEpo.get(k); 
			vecFilas 			= autSolic.getDoctosSeleccionados(claveEpo,iNoCliente,numero_credito,df_fecha_seleccion_de,df_fecha_seleccion_a,null);
			
			System.out.println("<br>vecFilas:----------------->"+vecFilas );
			for(i=0;i<vecFilas.size();i++){
				vecColumnas = (Vector)vecFilas.get(i);
				//out.println("<br>vecColumnas: "+vecColumnas);
				igNumeroDocto			=	(String)vecColumnas.get(0);
				fechaSolicitud			=	(String)vecColumnas.get(1);
				distribuidor			=	(String)vecColumnas.get(2);
				ic_pyme					=	(String)vecColumnas.get(3);
				moneda					=	(String)vecColumnas.get(4);
				icMoneda					=	(String)vecColumnas.get(19);
				monto						=	Double.parseDouble(vecColumnas.get(20).toString());
				tipoConversion			=	(String)vecColumnas.get(7);
				tipoCambio				=	Double.parseDouble(vecColumnas.get(8).toString());
				montoValuado			=	monto*tipoCambio;
				referencia				=	(String)vecColumnas.get(9);
				tasaInteres				=	Double.parseDouble(vecColumnas.get(33).toString());
				plazo						=	(String)vecColumnas.get(11);
				fechaVto					=	(String)vecColumnas.get(12);
				tipoPagoInteres		=	(String)vecColumnas.get(13);
				montoInteres 			=	Double.parseDouble(vecColumnas.get(15).toString());
				icDocumento				=	(String)vecColumnas.get(16);

				String rs_nomEPO		=	(String)vecColumnas.get(17);
				String rs_numDist		=	(String)vecColumnas.get(21);
				String rs_cveTasa		=	(String)vecColumnas.get(22);
				String rs_tipoConver	=	(String)vecColumnas.get(23);
				String rs_tipoCamb	=	(String)vecColumnas.get(24);
				String rs_respInt		=	(String)vecColumnas.get(25);
				String rs_tipoCob		=	(String)vecColumnas.get(26);
				rs_referencia 			=	(String)vecColumnas.get(27);
				rs_campo1				=	(String)vecColumnas.get(28);
				rs_campo2				=	(String)vecColumnas.get(29);
				rs_campo3				=	(String)vecColumnas.get(30);
				rs_campo4				=	(String)vecColumnas.get(31);
				rs_campo5 				=	(String)vecColumnas.get(32);
				String cg_numero_cuenta = autSolic.getCuentaAutorizada(claveEpo,iNoCliente,ic_pyme,icMoneda);
				
				if("1".equals(icMoneda)){
					totalDoctosMN 			++;
					totalMontoMN 			+= monto;
					totalMontoValuadoMN 	+= montoValuado;
					totalMontoIntMN		+= montoInteres;
				}else if("54".equals(icMoneda)){
					totalDoctosUSD			++;
					totalMontoUSD 			+= monto;
					totalMontoValuadoUSD	+= montoValuado;
					totalMontoIntUSD		+= montoInteres;
				}
				
				if(mostrarCabecera){
					
					contenidoArchivo.append(
							"\n \nNo. de documento Final,"+
							"Fecha solicitud,"+
							"Distribuidor,"+
							"Moneda,"+
							"Monto,"+
							"Referencia tasa de interés,"+
							"Tasa interés,"+
							"Plazo,"+
							"Fecha de vencimiento,"+
							"Monto interés,"+
							"Tipo de pago intereses,"+
							"Cuenta Bancaria Distribuidor"+
							(hayMasDeUnaEpoSeleccionada?",Tipo de Financiamiento":""));
					
					contenidoArchVar.append(
							"\nD,No. Documento final,"+
							"No. Distribuidor,"+
							"Nombre Distribuidor,"+
							"Clave Epo,"+
							"Nombre EPO,"+
							"Fecha de operación,"+
							"Clave de moneda,"+
							"Moneda,"+
							"Monto del crédito,"+
							"Plazo crédito,"+
							"Fecha vencimiento crédito,"+
							"Monto Interes,"+
							"Clave Tasa interes,"+
							"Tasa interes,"+
							"Tipo de conversión,"+
							"Monto docto.inicial dls.,"+
							"Tipo de cambio,"+
							"Responsable pago de interés,"+
							"Tipo cobranza,"+
							"Referencia,"+
							"CG campo1,"+ 
							"CG campo2,"+
							"CG campo3,"+
							"CG campo4,"+
							"CG campo5");
						
							contenidoArchIntegral.append( 
							"\nNo. Documento final,"+
							"No. Distribuidor,"+
							"Nombre Distribuidor,"+
							"Cuenta Bancaria Distribuidor,"+
							"Clave Epo,"+
							"Nombre EPO,"+
							"Fecha solicitud,"+
							"Fecha de operación,"+
							"Clave de moneda,"+
							"Moneda,"+
							"Monto del crédito,"+
							"Plazo crédito,"+
							"Fecha vencimiento crédito,"+
							"Monto Interes,"+
							"Tipo de cobro interes,"+ ///??
							"Referencia tasa de interés,"+
							"Clave Tasa interes,"+
							"Tasa interes,"+
							"Tipo de conversión,"+
							"Monto docto.inicial dls.,"+
							"Tipo de cambio,"+
							"Monto valuado," +////?
							"Responsable pago de interés,"+
							"Tipo cobranza,"+
							"Referencia,"+
							"CG campo1,"+ 
							"CG campo2,"+
							"CG campo3,"+
							"CG campo4,"+
							"CG campo5");
							
						mostrarCabecera = false;
							
					} //if
					
					// Contenido Archivo
					contenidoArchivo.append("\n"+
						icDocumento+","+
						fechaSolicitud+","+
						distribuidor.replace(',',' ')+","+
						moneda+","+
						monto+","+
						referencia+","+
						tasaInteres+","+
						plazo+","+
						fechaVto+","+
						montoInteres+","+
						tipoPagoInteres+","+
						cg_numero_cuenta+
						(hayMasDeUnaEpoSeleccionada?","+BeanAutDescuento.getDescripcionTipoFondeo(catalogoTipoFondeo,claveEpo):""));
						
					//	Contenido Archivo Interfase Variable
					contenidoArchVar.append("\nD,"+
						icDocumento.trim()+","+
						rs_numDist.trim()+","+
						distribuidor.replace(',',' ')+","+
						claveEpo.trim()+","+
						rs_nomEPO.replace(',',' ')+","+
						fechaSolicitud.trim()+","+
						icMoneda.trim()+","+
						moneda.trim()+","+
						Comunes.formatoDecimal(monto,2,SIN_COMAS)+","+
						plazo.trim()+","+
						fechaVto.trim()+","+
						Comunes.formatoDecimal(montoInteres,2,SIN_COMAS)+","+
						rs_cveTasa.trim()+","+
						Comunes.formatoDecimal(tasaInteres,2,SIN_COMAS)+",");
					if(!"1".equals(rs_tipoCamb.trim())){
						contenidoArchVar.append(
							rs_tipoConver.trim()+","+
							Comunes.formatoDecimal(montoValuado,2,SIN_COMAS)+","+
							rs_tipoCamb.trim()+",");
					}
					else {
						contenidoArchVar.append("N,0.00,0.00,");
					}
					contenidoArchVar.append(
						rs_respInt.trim()+","+
						rs_tipoCob.trim()+","+
						rs_referencia.trim()+","+
						rs_campo1.trim()+","+
						rs_campo2.trim()+","+
						rs_campo3.trim()+","+
						rs_campo4.trim()+","+
						rs_campo5.trim());

				// Contenido Archivo Interfase Fija
				contenidoArchFijo.append("\nD"+
					Comunes.formatoFijo(Comunes.formatoDecimal(icDocumento,0,SIN_COMAS),10,"0","") + 
					Comunes.formatoFijo(rs_numDist,25," ","A") +
					Comunes.formatoFijo(distribuidor.replace(',',' '),100," ","A") +
					Comunes.formatoFijo(Comunes.formatoDecimal(claveEpo,0,SIN_COMAS),3,"0","") + 
					Comunes.formatoFijo(rs_nomEPO.replace(',',' '),100," ","A") +
					Comunes.formatoFijo(fechaSolicitud,10," ","") + 
					Comunes.formatoFijo(Comunes.formatoDecimal(icMoneda,0,SIN_COMAS),3,"0","") + 
					Comunes.formatoFijo(moneda,30," ","A") +
					Comunes.formatoFijo(Comunes.formatoDecimal(monto,2,SIN_COMAS),15,"0","") + 
					Comunes.formatoFijo(Comunes.formatoDecimal(plazo,0,SIN_COMAS),3,"0","") + 
					Comunes.formatoFijo(fechaVto,10," ","") + 
					Comunes.formatoFijo(Comunes.formatoDecimal(montoInteres,2,SIN_COMAS),15,"0","") + 
					Comunes.formatoFijo(Comunes.formatoDecimal(rs_cveTasa,0,SIN_COMAS),3,"0","") + 
					Comunes.formatoFijo(Comunes.formatoDecimal(tasaInteres,5,SIN_COMAS),8,"0",""));
				if(!"1".equals(rs_tipoCamb.trim())){
				contenidoArchFijo.append(
					Comunes.formatoFijo(rs_tipoConver,1," ","A") +
					Comunes.formatoFijo(Comunes.formatoDecimal(montoValuado,2,SIN_COMAS),15,"0","") + 
					Comunes.formatoFijo(Comunes.formatoDecimal(rs_tipoCamb,2,SIN_COMAS),15,"0",""));
				}
				else {
				contenidoArchFijo.append(
					Comunes.formatoFijo("N",1," ","A") +
					Comunes.formatoFijo(Comunes.formatoDecimal("0",2,SIN_COMAS),15,"0","") + 
					Comunes.formatoFijo(Comunes.formatoDecimal("0",2,SIN_COMAS),15,"0",""));
				}
				contenidoArchFijo.append(
					Comunes.formatoFijo(rs_respInt,1," ","A") +
					Comunes.formatoFijo(rs_tipoCob,1," ","A")+
					Comunes.formatoFijo(rs_referencia,100," ","A") +
					Comunes.formatoFijo(rs_campo1,100," ","A") +
					Comunes.formatoFijo(rs_campo2,100," ","A") +
					Comunes.formatoFijo(rs_campo3,100," ","A") +
					Comunes.formatoFijo(rs_campo4,100," ","A") +
					Comunes.formatoFijo(rs_campo5,100," ","A")+".");
					
					
						//	Contenido Archivo Integral
					contenidoArchIntegral.append("\n"+
						icDocumento.trim()+","+
						rs_numDist.trim()+","+
						distribuidor.replace(',',' ')+","+
						cg_numero_cuenta+","+
						claveEpo.trim()+","+
						rs_nomEPO.replace(',',' ')+","+
						fechaSolicitud.trim()+","+//fecha operacion???
						fechaSolicitud.trim()+","+
						icMoneda.trim()+","+
						moneda.trim()+","+
						Comunes.formatoDecimal(monto,2,SIN_COMAS)+","+
						plazo.trim()+","+
						fechaVto.trim()+","+
						Comunes.formatoDecimal(montoInteres,2,SIN_COMAS)+","+
						tipoPagoInteres+","+//tipo cobro interes
						referencia+","+//referencia tasa de interes 
						rs_cveTasa.trim()+","+
						Comunes.formatoDecimal(tasaInteres,2,SIN_COMAS)+",");
					if(!"1".equals(rs_tipoCamb.trim())){
						contenidoArchIntegral.append(
							rs_tipoConver.trim()+",,"+
							//Comunes.formatoDecimal(monto,2,SIN_COMAS)+","+//monto docto inicial dls no se cual es ???
							Comunes.formatoDecimal(montoValuado,2,SIN_COMAS)+","+
							rs_tipoCamb.trim()+",");
					}else {
						contenidoArchIntegral.append("Sin Conversion,,0.00, 0,");
					}
					contenidoArchIntegral.append(
						rs_respInt.trim()+","+
						rs_tipoCob.trim()+","+
						rs_referencia.trim()+","+
						rs_campo1.trim()+","+
						rs_campo2.trim()+","+
						rs_campo3.trim()+","+
						rs_campo4.trim()+","+
						rs_campo5.trim());
				elementos++;
				
			}	// for
			
		}
	
		if(elementos>0){

			// Archivo de Interfase Variable.
			ArchivoVariable.append(
				"H," +fechaActual+","+HoraActual+",Documentos por Autorizar" +
				contenidoArchVar.toString()+
				"\nT,Total registros M.N.,"+totalDoctosMN+
				",Monto Total Créditos M.N.,,,,,,"+Comunes.formatoDecimal(totalMontoMN,2,SIN_COMAS)+
				",,Total Monto Interés M.N.,"+Comunes.formatoDecimal(totalMontoIntMN,2,SIN_COMAS)+
				",\nT,Total registros D.L.,"+totalDoctosUSD+
				",Monto Total Créditos D.L.,,,,,,"+Comunes.formatoDecimal(totalMontoUSD,2,SIN_COMAS)+
				",,Total Monto Interés D.L.,"+Comunes.formatoDecimal(totalMontoIntUSD,2,SIN_COMAS));
			
			// Archivo de Interfase Fija
			ArchivoFijo.append(
				"H" +fechaActual+HoraActual+"  Documentos por Autorizar" +
				contenidoArchFijo.toString()+
				"\nT"+
				Comunes.formatoFijo(Comunes.formatoDecimal(totalDoctosMN,0,SIN_COMAS),14,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(totalMontoMN,2,SIN_COMAS),15,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(totalMontoIntMN,2,SIN_COMAS),15,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(totalDoctosUSD,0,SIN_COMAS),14,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(totalMontoUSD,2,SIN_COMAS),15,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(totalMontoIntUSD,2,SIN_COMAS),15,"0",""));
			
			// Archivo Integral
			ArchivoIntegral.append(
				contenidoArchIntegral.append(
				"\nTotal registros M.N.,"+totalDoctosMN+
				",Monto Total Créditos M.N.,"+Comunes.formatoDecimal(totalMontoMN,2,SIN_COMAS)+
				",Total Monto Interés M.N.,"+Comunes.formatoDecimal(totalMontoIntMN,2,SIN_COMAS)+
				",\nTotal registros D.L.,"+totalDoctosUSD+
				",Monto Total Créditos D.L.,"+Comunes.formatoDecimal(totalMontoUSD,2,SIN_COMAS)+
				",Total Monto Interés D.L.,"+Comunes.formatoDecimal(totalMontoIntUSD,2,SIN_COMAS)));


			// Crear Archivos
			// 1. Archivo
			if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "Error al generar el archivo");
			}else{
				nombreArchivo = archivo.nombre;
				jsonObj.put("success", new Boolean(true));
			}
			// 2. Archivo de Interfase Variable
			if (!archivo.make(ArchivoVariable.toString(), strDirectorioTemp, ".csv")){
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "Error al generar el archivo");
			}else{
				nombreArchivoVar = archivo.nombre;
				jsonObj.put("success", new Boolean(true));
			}
			// 3. Archivo Interfase Fija
			if (!archivo.make(ArchivoFijo.toString(), strDirectorioTemp, ".txt")){
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "Error al generar el archivo");
			}else{
				nombreArchivoFijo = archivo.nombre;
				jsonObj.put("success", new Boolean(true));
			}
			
			// 4. Archivo Integral
			if (!archivo.make(ArchivoIntegral.toString(), strDirectorioTemp, ".csv")){
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "Error al generar el archivo");
			}else{
				nombreArchivoIntegral = archivo.nombre;
				jsonObj.put("success", new Boolean(true));
			}
			
			// Definir archivo a descargar
			//String auxArchivo = "";
			if(tipoArchivo.equals("ARCH")){
				//auxArchivo = strDirecVirtualTemp+nombreArchivo;
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			}else if(tipoArchivo.equals("VAR")){
				//auxArchivo = strDirecVirtualTemp+nombreArchivoVar;
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoVar);
			}else if(tipoArchivo.equals("FIJO")){
				//auxArchivo = strDirecVirtualTemp+nombreArchivoFijo;
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoFijo);
			}else if(tipoArchivo.equals("INTEGRAL")) {				
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoIntegral);
			}
		}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>