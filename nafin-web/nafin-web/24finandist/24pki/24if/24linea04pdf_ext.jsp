<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	com.netro.exception.*, 
	netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

JSONObject jsonObj = new JSONObject();

String _acuse			= (request.getParameter("_acuse")==null)?"":request.getParameter("_acuse");
String fechaHoy		= (request.getParameter("fechaHoy")==null)?"":request.getParameter("fechaHoy");
String horaCarga		= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");
String usuario			= (request.getParameter("usuario")==null)?"":request.getParameter("usuario");
String totMtoAuto	  	= (request.getParameter("totMtoAuto")==null)?"":request.getParameter("totMtoAuto");
String totDocs	  		= (request.getParameter("totDocs")==null)?"":request.getParameter("totDocs");
String totMtoAutoDol = (request.getParameter("totMtoAutoDol")==null)?"":request.getParameter("totMtoAutoDol");
String totDocsDol	  	= (request.getParameter("totDocsDol")==null)?"":request.getParameter("totDocsDol");

String amoneda[] 				= request.getParameterValues("nomMoneda");
String noNafinDists[]		= request.getParameterValues("noNafinEs_aux");
String folios[]				= request.getParameterValues("folios_aux");
String montoAutos[] 			= request.getParameterValues("montoAutos_aux");
String vencimientos[]		= request.getParameterValues("vencimientos_aux");
String nCtaDists[] 			= request.getParameterValues("nCtaDist_aux");
String nCtaIfs[]				= request.getParameterValues("nCtaIfs_aux");
String ic_epos[]				= request.getParameterValues("ic_epos_aux");
String nomPymes[] 			= request.getParameterValues("nomPymes");
String tipoSol_aux[] 		= request.getParameterValues("tipoSol_aux");
String monedas_aux[] 		= request.getParameterValues("monedas_aux");
String tipo_cobro_ints[]	= request.getParameterValues("tipoCobro_aux");
String ifBancos[] 			= request.getParameterValues("ifBancos");
String ifBancos_aux[] 		= request.getParameterValues("ifBancos_aux");
String nPlazoMax[]			= request.getParameterValues("nPlazoMax");
String nCtaBancaria[]		= request.getParameterValues("nCtaBancaria");

try {

	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

	pdfDoc.setTable(4, 50);
	pdfDoc.setCell("Moneda Nacional","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Dólares","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de solicitudes capturadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto de Solicitudes capturadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de solicitudes capturadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto de Solicitudes capturadas","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(totDocs,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAuto,2),"formas",ComunesPDF.LEFT);
	pdfDoc.setCell(totDocsDol,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAutoDol,2),"formas",ComunesPDF.LEFT);
	pdfDoc.addTable();

	if(!strPerfil.equals("IF FACT RECURSO")){
		pdfDoc.setTable(12, 100);
		pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Número Distribuidor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre Distribuidor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo de solicitud","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Folio solicitud relacionada","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto autorizado","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo de cobro de intereses","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("No. Cuenta Distribuidor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("IF Banco de Servicio","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("No. Cuenta IF","celda01",ComunesPDF.CENTER);
	
		if (ic_epos.length > 0){
			for(int i=0;i<ic_epos.length;i++){
				pdfDoc.setCell(ic_epos[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(noNafinDists[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nomPymes[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipoSol_aux[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(folios[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(amoneda[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoAutos[i],2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(vencimientos[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipo_cobro_ints[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nCtaDists[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ifBancos[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nCtaIfs[i],"formas",ComunesPDF.CENTER);
			}
		}
		if (!("0".equals(totDocs))){
			pdfDoc.setCell("Totales MN","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(totDocs,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAuto,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		}
		if (!("0".equals(totDocsDol))){
			pdfDoc.setCell("Totales Dolares","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(totDocsDol,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAutoDol,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		}
		pdfDoc.addTable();	
	}else{
		pdfDoc.setTable(8, 100);
		pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Número Proveedor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER);
		//pdfDoc.setCell("Tipo de solicitud","celda01",ComunesPDF.CENTER);
		//pdfDoc.setCell("Folio solicitud relacionada","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto autorizado","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
		//pdfDoc.setCell("Tipo de cobro de intereses","celda01",ComunesPDF.CENTER);
		//pdfDoc.setCell("No. Cuenta Distribuidor","celda01",ComunesPDF.CENTER);
		//pdfDoc.setCell("IF Banco de Servicio","celda01",ComunesPDF.CENTER);
		//pdfDoc.setCell("No. Cuenta IF","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo máximo ampliación descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Cuenta Bancaria","celda01",ComunesPDF.CENTER);

		if (ic_epos.length > 0){
			for(int i=0;i<ic_epos.length;i++){
				pdfDoc.setCell(ic_epos[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(noNafinDists[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nomPymes[i],"formas",ComunesPDF.CENTER);
				//pdfDoc.setCell(tipoSol_aux[i],"formas",ComunesPDF.CENTER);
				//pdfDoc.setCell(folios[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(amoneda[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoAutos[i],2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(vencimientos[i],"formas",ComunesPDF.CENTER);
				//pdfDoc.setCell(tipo_cobro_ints[i],"formas",ComunesPDF.CENTER);
				//pdfDoc.setCell(nCtaDists[i],"formas",ComunesPDF.CENTER);
				//pdfDoc.setCell(ifBancos[i],"formas",ComunesPDF.CENTER);
				//pdfDoc.setCell(nCtaIfs[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nPlazoMax[i],"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nCtaBancaria[i],"formas",ComunesPDF.CENTER);
			}
		}
		if (!("0".equals(totDocs))){
			pdfDoc.setCell("Totales MN","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(totDocs,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAuto,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		}
		if (!("0".equals(totDocsDol))){
			pdfDoc.setCell("Totales Dolares","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(totDocsDol,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMtoAutoDol,2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
		}
		pdfDoc.addTable();
	}
	
	pdfDoc.setTable(2, 50);
	pdfDoc.setCell("Datos de cifras de control","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de Acuse","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(_acuse,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Fecha","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Hora","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(horaCarga,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Nombre y número de usuario","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(usuario,"formas",ComunesPDF.LEFT);
	pdfDoc.addTable();

	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>