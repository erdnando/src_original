<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*, java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String informacion   			=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
boolean SIN_COMAS = false;
String fechaHoy	= "";
String fechaActual	= "";
String HoraActual	= "";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	HoraActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}
//PARAMETROS QUE PROVINEN DE LA PAGINA ACTUAL
String 	ic_epo							= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String 	numero_credito					= (request.getParameter("numero_credito")==null)?"":request.getParameter("numero_credito");
String 	df_fecha_seleccion_de		= (request.getParameter("df_fecha_seleccion_de")==null)?fechaHoy:request.getParameter("df_fecha_seleccion_de");
String 	df_fecha_seleccion_a			= (request.getParameter("df_fecha_seleccion_a")==null)?fechaHoy:request.getParameter("df_fecha_seleccion_a");
String	tipoArchivo						= (request.getParameter("tipoArchivo")==null)?"":request.getParameter("tipoArchivo");
String 	ic_pyme							= (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
String 	ig_numero_docto				= (request.getParameter("ig_numero_docto")!=null)?request.getParameter("ig_numero_docto"):"";
boolean  hayMasDeUnaEpoSeleccionada = "true".equals(request.getParameter("hayMasDeUnaEpoSeleccionada"))?true:false;
	
try {
	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
	
	AutorizacionSolic autSolic = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
        
        ParametrosDist  BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	StringBuffer contenidoArchivoIntegral = new StringBuffer(2000);
	StringBuffer contenidoArchivoVariable = new StringBuffer(2000);
	StringBuffer contenidoArchivoFijo = new StringBuffer(2000);
	StringBuffer contenidoArchivoGeneral = new StringBuffer();
	Vector vecFilasVF		= null;
	Vector vecColumnasVF	= null;
	Vector vecFilas		= null;
	Vector vecColumnas	= null;
	Vector vecFilasF		= null;
	Vector vecColumnasF	= null;

	String nombreArchivo = null;
	String nombreArchivoVar = "";
	StringBuffer ArchivoVariable = new StringBuffer(2000);
	String nombreArchivoFijo = "";
	StringBuffer ArchivoFijo = new StringBuffer(2000);
	ArrayList listaClavesEpo = null;
	int i = 0;
	Vector nombresCampo = new Vector(5);
	Vector detalleCampo = null;
	String campo ="", campo0="", campo1="",campo2="",campo3="",campo4="",campo5="";
	String igNumeroDoctoV =	"", NoDistribuidorV ="", nombreDistribuidorV = "", claveEpoV = "",
	nombreEpoV = "", fechaOperacionV = "", claveMonedaV = "", monedaV = "", plazoCreditoV = "",
	fechaVencimientoV = "", claveTasaV = "", tipoConversionV = "", MontoDocumentoV = "", 
	responsableV = "", referenciaV = "", campo1V = "", campo2V = "",
	campo3V = "",  campo4V = "", campo5V = "";
	
	double montoCreditoV = 0; 
	double montoInteresV = 0;
	double tasaInteresV =0;
	double tipoCambioV =0;
	int	 totalDoctosMNV			= 0;
	int	 totalDoctosUSDV			= 0;
	double totalMontoMNV				= 0;
	double totalMontoUSDV			= 0;
	double totalMontoValuadoMNV	= 0;
	double totalMontoValuadoUSDV	= 0;
	double totalMontoIntMNV			= 0;
	double totalMontoIntUSDV		= 0;

	double montoCreditoI = 0;
	double montoInteresI = 0;
	double tasaInteresI =0;
	double tipoCambioI =0;
	int		totalDoctosMNI			= 0;
	int 		totalDoctosUSDI			= 0;
	double	totalMontoMNI			= 0;
	double	totalMontoUSDI			= 0;
	double 	totalMontoValuadoMNI		= 0;
	double 	totalMontoValuadoUSDI	= 0;
	double	totalMontoIntMNI			= 0;
	double	totalMontoIntUSDI		= 0;

	String igNumeroDoctoF =	"", NoDistribuidorF ="", nombreDistribuidorF = "", claveEpoF = "",
	nombreEpoF = "", fechaOperacionF = "", claveMonedaF = "", monedaF = "", plazoCreditoF = "",
	fechaVencimientoF = "", claveTasaF = "", tipoConversionF = "", MontoDocumentoF = "", 
	responsableF = "", referenciaF = "", campo1F = "", campo2F = "",
	campo3F = "",  campo4F = "", campo5F = "";
	
	double montoCreditoF = 0; 
	double montoInteresF = 0;
	double tasaInteresF =0;
	double tipoCambioF =0;
	int	 totalDoctosMNF			= 0;
	int 	 totalDoctosUSDF			= 0;
	double totalMontoMNF				= 0;
	double totalMontoUSDF			= 0;
	double totalMontoValuadoMNF	= 0;
	double totalMontoValuadoUSDF	= 0;
	double totalMontoIntMNF			= 0;
	double totalMontoIntUSDF		= 0;
	String numeroCuenta ="";

	String tipoCobroInteres ="";
	String ctReferencia ="";
	String tipoCobranza ="";

	String cadEpoCemex =
	"Al transmitir este mensaje de datos, bajo mi responsabilidad manifiesto para todos los efectos legales a que haya lugar, que "+
	"he depositado a la EPO la cantidad total del monto señalado en la orden de compra, atendiendo con esto la instrucción del "+  
	"distribuidor para realizar las disposiciones de su línea de crédito.";
	
	String cadEpo = 
	"Por este acto acepto la solicitud de financiamiento  de las órdenes de compra emitidos por la empresa de primer orden "+
	"al amparo de la línea de crédito del distribuidor, manifestando que por esta misma fecha realizaré a la empresa de 1° orden "+
	"el depósito del 100% del importe de los documentos.";

        String operaContrato = BeanParametro.obtieneOperaSinCesion(ic_epo); //PARAMETRO NUEVO JAGE 14012017
        
        if(operaContrato.equals("S")){
            cadEpo =
            "Al transmitir este mensaje de datos bajo mi responsabilidad acepto el financiamiento del(los) DOCUMENTO(S) FINAL(ES) descrito(s) en el mismo "+
            "y de conformidad con el Contrato de Financiamiento a Clientes y Distribuidores. Dicha aceptación tendrá plena validez para todos los efectos legales. ";
        }
        
        
	/*if(application.getInitParameter("EpoCemex").equals(ic_epo)){
	contenidoArchivo.append(
				new String("Al transmitir este mensaje de datos, bajo mi responsabilidad manifiesto para todos los efectos legales a que haya lugar, que he depositado a la EPO \n ").replace(',',' ')+
				new String("la cantidad total del monto señalado en la orden de compra, atendiendo con esto la instrucción del distribuidor para realizar las disposiciones de su línea de crédito. \n ").replace(',',' '));
	}else{
	contenidoArchivo.append(
				new String("Al transmitir este mensaje de datos, bajo mi responsabilidad acepto la Cesión de Derechos de Cobro del (los) Documento(s) final(es) descrito(s) en el  mismo. Dicha aceptación tendrá plena validez para todos los efectos legales. \n ").replace(',',' ')+
				new String("En este mismo acto se genera el aviso de notificación a la EMPRESA DE PRIMER ORDEN y al DISTRIBUIDOR, por lo que tiene la validez de acuse de recibo. \n ").replace(',',' ')+
				new String("Las operaciones que aparecen en esta página fueron notificadas a la EMPRESA DE PRIMER ORDEN y al DISTRIBUIDOR de conformidad y para los efectos de los artículos 45 K de la Ley General de Organizaciones y Actividades Auxiliares \n ").replace(',',' ')+
				new String("del Crédito y 32 C del Código Fiscal o del artículo 2038 del Código Civil Federal según corresponda para los efectos legales conducentes. \n ").replace(',',' '));
	}*/
	if(hayMasDeUnaEpoSeleccionada){

		listaClavesEpo = Comunes.stringToArrayList(ic_epo, ",");
	
		// Validar el horario de servicio de las epos seleccionadas, si alguna de estas no se encuentra en el horario de servicio
		// suprimirlas de la lista de epos validas
		for(int k=0;k<listaClavesEpo.size();k++){
			try {
				String claveEpo = (String) listaClavesEpo.get(k);
				Horario.validarHorario(4, strTipoUsuario, claveEpo, iNoCliente);
			}catch(Exception e){
				listaClavesEpo.remove(k);
				k--;
				/*
					if(e instanceof NafinException){
						NafinException e = (NafinException) e;
						if(e.getCause().equals("SIST0001")){
							throw e;
						}
					}
				*/
			}
		}
		
		// Validar que haya al menos una EPO 
		if(listaClavesEpo.size() == 0){
			throw new Exception("No se encontró ninguna EPO con horario válido");
		} else if (listaClavesEpo.size() == 1){
			ic_epo = (String) listaClavesEpo.get(0);
			hayMasDeUnaEpoSeleccionada = false;
		}
	}

	if(!hayMasDeUnaEpoSeleccionada){
		Horario.validarHorario(4, strTipoUsuario, ic_epo, iNoCliente);
		listaClavesEpo = new ArrayList();
		listaClavesEpo.add(ic_epo);
	}

	if (!ic_epo.equals("")){
	  // F029 - 2011: Si selecciono mas de una EPO usar nombres de campos genericos
	  if(hayMasDeUnaEpoSeleccionada){
		  
		  Vector fila = null;
		  
		  nombresCampo = new Vector();
		  
		  fila = new Vector();
		  fila.add("CG CAMPO 1");
		  nombresCampo.add(fila);
		  
		  fila = new Vector();
		  fila.add("CG CAMPO 2");
		  nombresCampo.add(fila);
		  
		  fila = new Vector();
		  fila.add("CG CAMPO 3");
		  nombresCampo.add(fila);
		  
		  fila = new Vector();
		  fila.add("CG CAMPO 4");
		  nombresCampo.add(fila);
		  
		  fila = new Vector();
		  fila.add("CG CAMPO 5");
		  nombresCampo.add(fila);
 
	  }else{
		  nombresCampo = cargaDocto.getCamposAdicionales(ic_epo,false);
	  }
	}

	// Obtener HashMap con los Parametros de Fondeo
	HashMap catalogoTipoFondeo = hayMasDeUnaEpoSeleccionada?BeanAutDescuento.getTipoFondeo(iNoCliente, listaClavesEpo, "4"):null;

	/* PARA GENERAR LA DESCARGA DE ARCHIVO*/ 

	if ( tipoArchivo.equals("G")) {
		 if(application.getInitParameter("EpoCemex").equals(ic_epo)){
				contenidoArchivo.append(cadEpoCemex.replace(',',' ')+","+"\n");	
			  }else{
				contenidoArchivo.append(cadEpo.replace(',',' ')+","+"\n");	
		 }
	}

	boolean mostrarCabecera = true;
	for(int k=0;k<listaClavesEpo.size();k++){

		String claveEpo 	= (String) listaClavesEpo.get(k);

		if(!strPerfil.equals("IF FACT RECURSO")){
			vecFilas	=	autSolic.getLineasConDoctosCCC(claveEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a);
		}else{
			vecFilas	=	autSolic.getLineasConDoctosFF(claveEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a);
		}

		for(i=0;i<vecFilas.size();i++){
			vecColumnas 			= 	(Vector)vecFilas.get(i);
			String nombreAcreditado	=	(String)vecColumnas.get(0);
			String numLineaCredito	=	(String)vecColumnas.get(2);
			String moneda				=	(String)vecColumnas.get(3);
			String saldoInicial		=	(String)vecColumnas.get(5);
			String montoSeleccionado=	(String)vecColumnas.get(6);
			String numDoctos			=	(String)vecColumnas.get(7);
			String saldoDisponible	=	(String)vecColumnas.get(8);
			
			if(mostrarCabecera){
				contenidoArchivo.append("\n \nNombre del acreditado,No. línea de crédito,Moneda,Saldo inicial,Monto operado,No. doctos. operados,Saldo disponible");
				mostrarCabecera = false;
			}
			contenidoArchivo.append(
								"\n"+nombreAcreditado+","+
								numLineaCredito+","+
								moneda+","+
								saldoInicial+","+
								montoSeleccionado+","+
								numDoctos+","+
								saldoDisponible);
		} // for
	}
	
	mostrarCabecera = true;
	String	moneda	=	"",	icMoneda	=	"",	icDocumento	=	"",	nombre_epo="";
	String	igNumeroDocto	=	"", fechaSolicitud = "",	distribuidor = "",	tipoConversion = "",	referencia = "",	plazo = "",	fechaVto = "";
	String	tipoPagoInteres=	"", tipoPago ="", tipoCredito ="";
	double 	montoInteres = 0,	tasaInteres = 0,	monto = 0,	tipoCambio = 0,	montoValuado = 0;

	//totales
	int totalDoctosMN= 0,	totalDoctosUSD = 0;
	double totalMontoMN = 0,	totalMontoUSD = 0,	totalMontoValuadoMN = 0,	totalMontoValuadoUSD	= 0,	totalMontoIntMN = 0,totalMontoIntUSD = 0;
	for(int k=0;k<listaClavesEpo.size();k++){
	
		String claveDeLaEpo 	= (String) listaClavesEpo.get(k);

		if(!strPerfil.equals("IF FACT RECURSO")){
			vecFilas	=	autSolic.getDoctosSeleccionadosCCC(claveDeLaEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a,null);
		}else{
			vecFilas	=	autSolic.getDoctosSeleccionadosFF(claveDeLaEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a,null);
		}

		int numeroCamposAdicionales = nombresCampo.size();
		if(hayMasDeUnaEpoSeleccionada){
		 numeroCamposAdicionales = cargaDocto.getCamposAdicionales(claveDeLaEpo,false).size();
		}

		for(i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);
			igNumeroDocto		=	(String)vecColumnas.get(0);
			fechaSolicitud		=	(String)vecColumnas.get(1);
			distribuidor		=	(String)vecColumnas.get(2);
			moneda				=	(String)vecColumnas.get(4);
			icMoneda				=	(String)vecColumnas.get(5);
			monto					=	Double.parseDouble(vecColumnas.get(20).toString());
			tipoConversion		=	(String)vecColumnas.get(7);
			tipoCambio			=	Double.parseDouble(vecColumnas.get(8).toString());
			montoValuado		=	monto*tipoCambio;
			referencia			=	(String)vecColumnas.get(9);
			tasaInteres			=	Double.parseDouble(vecColumnas.get(21).toString());
			plazo					=	(String)vecColumnas.get(11);
			fechaVto				=	(String)vecColumnas.get(12);
			tipoPagoInteres	=	(String)vecColumnas.get(13);
			montoInteres 		=	Double.parseDouble(vecColumnas.get(15).toString());
			icDocumento			=	(String)vecColumnas.get(16);
			nombre_epo 			=	(String)vecColumnas.get(17);
			campo1 				=	(String)vecColumnas.get(22);
			campo2 				=	(String)vecColumnas.get(23);
			campo3 				=	(String)vecColumnas.get(24);
			campo4 				=	(String)vecColumnas.get(25);
			campo5 				=	(String)vecColumnas.get(26);
			numeroCuenta		=	(String)vecColumnas.get(27);
			tipoPago =  (String)vecColumnas.get(29); //F09-2015 
			
				
			if(numeroCuenta.equals("0")){
				numeroCuenta= "";
			}
			if("1".equals(icMoneda)){
				totalDoctosMN 			++;
				totalMontoMN 			+= monto;
				totalMontoValuadoMN 	+= montoValuado;
				totalMontoIntMN		+= montoInteres;
			}else if("54".equals(icMoneda)){
				totalDoctosUSD			++;
				totalMontoUSD 			+= monto;
				totalMontoValuadoUSD	+= montoValuado;
				totalMontoIntUSD		+= montoInteres;
			}
			
			if(mostrarCabecera){
				contenidoArchivo.append(
						"\n \nDocto Final,"+
						"Fecha solicitud,"+
						"EPO relacionada"+","+
						"Distribuidor,"+
						"Moneda,"+
						"Monto,"+
						"Referencia tasa de interés,"+
						"Valor tasa intereses,"+
						"Plazo,"+
						"Fecha de vencimiento,"+
						"Monto intereses,"+
						"Tipo de cobro de intereses"+",");
		
				if (nombresCampo !=null ) {
					for (int e=0;e<nombresCampo.size();e++){
						detalleCampo = (Vector) nombresCampo.get(e);
						contenidoArchivo.append((String)detalleCampo.get(0)+",");
					}
				}
		
				contenidoArchivo.append(" Número de Cuenta ");
					 
				if(hayMasDeUnaEpoSeleccionada){
				 contenidoArchivo.append(",Tipo de Financiamiento");
				}
				 
				contenidoArchivo.append("\n");
				mostrarCabecera = false;

				} //if
		
				contenidoArchivo.append(icDocumento.replace(',',' ')+","+
										 fechaSolicitud.replace(',',' ')+","+
										 nombre_epo.replace(',',' ')+","+
										 distribuidor.replace(',',' ')+","+
										 moneda.replace(',',' ')+","+
										 Comunes.formatoDecimal(monto,2,SIN_COMAS)+",");
										 if(!tipoPago.equals("2")) {
										 	contenidoArchivo.append(referencia.replace(',',' ')+",");
										 }else {
										 	contenidoArchivo.append(""+",");
										 }
										 
										contenidoArchivo.append( Comunes.formatoDecimal(tasaInteres,2,SIN_COMAS)+",");
										 
										if(!tipoPago.equals("2")) {
										 contenidoArchivo.append(plazo.replace(',',' ')+",");
										}else { 
										 contenidoArchivo.append(plazo.replace(',',' ')+"(M),");
										}
										 
										 contenidoArchivo.append( fechaVto.replace(',',' ')+","+
										 Comunes.formatoDecimal(montoInteres,2,SIN_COMAS)+","+
										 tipoPagoInteres.replace(',',' ')+",");
		
				if (nombresCampo !=null ) {
					for (int j=1;j<=nombresCampo.size();j++){
						if(j>numeroCamposAdicionales){
						 contenidoArchivo.append("N/A,");
							continue;
						}
					 
						if (j==1) {
							contenidoArchivo.append(campo1.replace(',',' ')+",");
						} else if (j==2) {
							contenidoArchivo.append(campo2.replace(',',' ')+",");    
						} else if (j==3) { 
							contenidoArchivo.append(campo3.replace(',',' ')+",");
						} else if (j==4) {
							contenidoArchivo.append(campo4.replace(',',' ')+",");
						} else if (j==5) { 
							contenidoArchivo.append(campo5.replace(',',' ')+",");
						}
					}       
				}

				contenidoArchivo.append(numeroCuenta.replace(',',' ')+",");

				if(hayMasDeUnaEpoSeleccionada){
					contenidoArchivo.append(BeanAutDescuento.getDescripcionTipoFondeo(catalogoTipoFondeo,claveDeLaEpo).replace(',',' ')+",");
				}
				contenidoArchivo.append("\n");
		}	// for
	}
	if(totalDoctosMN>0){
		contenidoArchivo.append("Total M.N."+","+",");
		contenidoArchivo.append(totalDoctosMN+","+","+",");
		contenidoArchivo.append(Comunes.formatoDecimal(totalMontoMN,2,SIN_COMAS)+","+","+","+","+",");
		contenidoArchivo.append(Comunes.formatoDecimal(totalMontoIntMN,2,SIN_COMAS)+","+",");
		contenidoArchivo.append("\n");
	}
  
	if(totalDoctosUSD>0){
		contenidoArchivo.append("Total Dolares"+","+",");
		contenidoArchivo.append(totalDoctosUSD+","+","+",");
		contenidoArchivo.append(Comunes.formatoDecimal(totalMontoUSD,2,SIN_COMAS)+","+","+","+","+",");
		contenidoArchivo.append(Comunes.formatoDecimal(totalMontoIntUSD,2,SIN_COMAS)+","+",");
		contenidoArchivo.append("\n");
	}

	/****************************************************************/
	//PARA GENERAR Exportar Interfase Variable
	/*****************************************************************/
	
	// 1. Agregar Cabecera
	contenidoArchivoVariable.append("H"+","+fechaHoy+","+HoraActual+","+"Documentos por Autorizar"+"\n");
	contenidoArchivoVariable.append(
					"D"+","+"No. Documento final,"+
					"No. Distribuidor,"+
					"Nombre Distribuidor,"+
					"Clave Epo,"+
					"Nombre EPO,"+
					"Fecha de operación,"+
					"Clave de moneda,"+
					"Moneda,"+
					"Monto del crédito,"+
					"Plazo crédito,"+
					"Fecha vencimiento crédito,"+
					"Monto Interes,"+
					"Clave Tasa interes,"+
					"Tasa interes,"+
					"Tipo de conversión,"+
					"Monto docto.inicial dls.,"+
					"Tipo de cambio,"+
					"Responsable pago de interés,"+
					"Tipo cobranza,"+
					"Referencia,");
	for (int e=0;e<nombresCampo.size();e++){
		 detalleCampo = (Vector) nombresCampo.get(e);
		 contenidoArchivoVariable.append((String)detalleCampo.get(0)+",");        
	}	
	contenidoArchivoVariable.append(" Número de Cuenta ");
	contenidoArchivoVariable.append("\n");
	
	// 2. Agregar contenido
	for(int k=0;k<listaClavesEpo.size();k++){
		String claveDeLaEpo 	= (String) listaClavesEpo.get(k);
		vecFilasVF 				= autSolic.getDoctosSelCCCVariableFijos(claveDeLaEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a);
		
		int numeroCamposAdicionales = nombresCampo.size();
		if(hayMasDeUnaEpoSeleccionada){
			numeroCamposAdicionales = cargaDocto.getCamposAdicionales(claveDeLaEpo,false).size();
		}

		for(i=0;i<vecFilasVF.size();i++){
			vecColumnasVF 			= (Vector)vecFilasVF.get(i);
			igNumeroDoctoV			= (String)vecColumnasVF.get(0);
			NoDistribuidorV 		= (String)vecColumnasVF.get(1);
			nombreDistribuidorV 	= (String)vecColumnasVF.get(2);
			claveEpoV 				= (String)vecColumnasVF.get(3);
			nombreEpoV 				= (String)vecColumnasVF.get(4);
			fechaOperacionV 		= (String)vecColumnasVF.get(5);
			claveMonedaV 			= (String)vecColumnasVF.get(6);
			monedaV 					= (String)vecColumnasVF.get(7);
			montoCreditoV 			= Double.parseDouble(vecColumnasVF.get(8).toString());
			plazoCreditoV 			= (String)vecColumnasVF.get(9);
			fechaVencimientoV 	= (String)vecColumnasVF.get(10);
			montoInteresV 			= Double.parseDouble(vecColumnasVF.get(11).toString());          
			claveTasaV 				= (String)vecColumnasVF.get(12);
			tasaInteresV 			= Double.parseDouble(vecColumnasVF.get(13).toString());
			tipoConversionV 		= (String)vecColumnasVF.get(14);
			MontoDocumentoV 		= (String)vecColumnasVF.get(15);
			tipoCambioV 			= Double.parseDouble(vecColumnasVF.get(16).toString());
			responsableV 			= (String)vecColumnasVF.get(17);
			referenciaV 			= (String)vecColumnasVF.get(18);
			campo1V 					= (String)vecColumnasVF.get(19);
			campo2V 					= (String)vecColumnasVF.get(20);
			campo3V 					= (String)vecColumnasVF.get(21);
			campo4V 					= (String)vecColumnasVF.get(22);
			campo5V 					= (String)vecColumnasVF.get(23);
			numeroCuenta 			= (String)vecColumnasVF.get(25);
			if(numeroCuenta.equals("0")){
				numeroCuenta= "";
			}
			tipoPago =  (String)vecColumnasVF.get(32); //F09-2015  

			contenidoArchivoVariable.append("D"+","+igNumeroDoctoV.replace(',',' ')+","+
									 NoDistribuidorV.replace(',',' ')+","+
									 nombreDistribuidorV.replace(',',' ')+","+
									 claveEpoV.replace(',',' ')+","+
									 nombreEpoV.replace(',',' ')+","+
									 fechaOperacionV.replace(',',' ')+","+
									 claveMonedaV.replace(',',' ')+","+
									 monedaV.replace(',',' ')+","+
									 Comunes.formatoDecimal(montoCreditoV,2,SIN_COMAS)+",");
									 
									 if(!tipoPago.equals("2")) {
										 contenidoArchivoVariable.append(plazoCreditoV.replace(',',' ')+",");
										}else { 
										 contenidoArchivoVariable.append(plazoCreditoV.replace(',',' ')+"(M),");
									}
							
									 contenidoArchivoVariable.append(fechaVencimientoV.replace(',',' ')+","+
									 Comunes.formatoDecimal(montoInteresV,2,SIN_COMAS)+",");
									 
									if(!tipoPago.equals("2")) {
										 contenidoArchivoVariable.append( claveTasaV.replace(',',' ')+",");
									}else {
										contenidoArchivoVariable.append(",");
									}
									
									contenidoArchivoVariable.append( Comunes.formatoDecimal(tasaInteresV,0,SIN_COMAS)+","+
									 tipoConversionV.replace(',',' ')+","+
									 MontoDocumentoV.replace(',',' ')+","+
									 Comunes.formatoDecimal(tipoCambioV,0,SIN_COMAS)+","+
									 responsableV.replace(',',' ')+",");
									 
									  if(!tipoPago.equals("2")) {
										contenidoArchivo.append(referenciaV.replace(',',' ')+","+",");
									  }else  {
										contenidoArchivo.append(""+","+",");
									  }
			
			if (nombresCampo !=null ) {
				
				for (int j=1;j<=nombresCampo.size();j++){	
					
					if(j>numeroCamposAdicionales){
						contenidoArchivoVariable.append("N/A,");
						continue;
					}
					if (j==1) {
						contenidoArchivoVariable.append(campo1V.replace(',',' ')+",");
					} else if (j==2) {
						contenidoArchivoVariable.append(campo2V.replace(',',' ')+",");    
					} else if (j==3) { 
						contenidoArchivoVariable.append(campo3V.replace(',',' ')+",");
					} else if (j==4) {
						contenidoArchivoVariable.append(campo4V.replace(',',' ')+",");
					} else if (j==5) { 
						contenidoArchivoVariable.append(campo5V.replace(',',' ')+",");
					}
				}
			}

			if("1".equals(claveMonedaV)){
				totalDoctosMNV 		++;
				totalMontoMNV 			+= montoCreditoV;		
				totalMontoIntMNV		+= montoInteresV;
			}else if("54".equals(claveMonedaV)){
				totalDoctosUSDV		++;
				totalMontoUSDV 		+= montoCreditoV;			
				totalMontoIntUSDV		+= montoInteresV;
			}		
			contenidoArchivoVariable.append(numeroCuenta.replace(',',' ')+",");
			contenidoArchivoVariable.append("\n");
		}//for
	}

   if(totalDoctosMNV>0){
		contenidoArchivoVariable.append("T"+",");
		contenidoArchivoVariable.append("Total M.N."+",");
		contenidoArchivoVariable.append(totalDoctosMNV+","+","+","+","+","+","+",");
		contenidoArchivoVariable.append(Comunes.formatoDecimal(totalMontoMNV,2,SIN_COMAS)+","+","+",");
		contenidoArchivoVariable.append(Comunes.formatoDecimal(totalMontoIntMNV,2,SIN_COMAS)+","+",");
		contenidoArchivoVariable.append("\n");
	}

   if(totalDoctosUSDV>0){
		contenidoArchivoVariable.append("T"+",");
		contenidoArchivoVariable.append("Total Dolares"+",");
		contenidoArchivoVariable.append(totalDoctosUSDV+","+","+","+","+","+","+",");
		contenidoArchivoVariable.append(Comunes.formatoDecimal(totalMontoUSDV,2,SIN_COMAS)+","+","+",");
		contenidoArchivoVariable.append(Comunes.formatoDecimal(totalMontoIntUSDV,2,SIN_COMAS)+","+",");
		contenidoArchivoVariable.append("\n");
	}

	/****************************************************************/
	/*PARA GENERAR Exportar Interfase Fijo*/
	/****************************************************************/

	contenidoArchivoFijo.append("H"+","+fechaHoy+","+HoraActual+","+"Documentos por Autorizar"+"\n");

	for(int k=0;k<listaClavesEpo.size();k++){
		String claveDeLaEpo 	= (String) listaClavesEpo.get(k);
		vecFilasF 					= autSolic.getDoctosSelCCCVariableFijos(claveDeLaEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a);

		for(i=0;i<vecFilasF.size();i++){
			vecColumnasF 			= (Vector)vecFilasF.get(i);
			igNumeroDoctoF			= (String)vecColumnasF.get(0);
			NoDistribuidorF 		= (String)vecColumnasF.get(1);
			nombreDistribuidorF 	= (String)vecColumnasF.get(2);
			claveEpoF 				= (String)vecColumnasF.get(3);
			nombreEpoF 				= (String)vecColumnasF.get(4);
			fechaOperacionF 		= (String)vecColumnasF.get(5);
			claveMonedaF 			= (String)vecColumnasF.get(6);
			monedaF 					= (String)vecColumnasF.get(7);
			montoCreditoF 			= Double.parseDouble(vecColumnasF.get(8).toString());
			plazoCreditoF 			= (String)vecColumnasF.get(9);
			fechaVencimientoF 	= (String)vecColumnasF.get(10);
			montoInteresF 			= Double.parseDouble(vecColumnasF.get(11).toString());          
			claveTasaF 				= (String)vecColumnasF.get(12);
			tasaInteresF 			= Double.parseDouble(vecColumnasF.get(13).toString());
			tipoConversionF 		= (String)vecColumnasF.get(14);
			MontoDocumentoF 		= (String)vecColumnasF.get(15);
			tipoCambioF 			= Double.parseDouble(vecColumnasF.get(16).toString());
			responsableF 			= (String)vecColumnasF.get(17);
			referenciaF 			= (String)vecColumnasF.get(18);
			campo1F 					= (String)vecColumnasF.get(19);
			campo2F 					= (String)vecColumnasF.get(20);
			campo3F 					= (String)vecColumnasF.get(21);
			campo4F 					= (String)vecColumnasF.get(22);
			campo5F 					= (String)vecColumnasF.get(23);    
			numeroCuenta 			= (String)vecColumnasF.get(25);
			if(numeroCuenta.equals("0")){
				numeroCuenta		= "";
			}
			tipoPago =  (String)vecColumnasF.get(32); //F09-2015  
			
			//System.out.println("numeroCuenta--->"+numeroCuenta);
			if("1".equals(claveMonedaF)){
				totalDoctosMNF 		++;
				totalMontoMNF 			+= montoCreditoF;		
				totalMontoIntMNF		+= montoInteresF;
			}else if("54".equals(claveMonedaF)){
				totalDoctosUSDF		++;
				totalMontoUSDF 		+= montoCreditoF;			
				totalMontoIntUSDF		+= montoInteresF;
			}		
 
			// NOTA: La siguiente linea se comento debido a que no tiene ningun uso en esta seccion de codigo
			// contenidoArchivoVariable.append("\n"); 
     
			contenidoArchivoFijo.append( Comunes.formatoFijo("D",5," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(igNumeroDoctoF.replace(',',' '),15," ","A"));
			//contenidoArchivoFijo.append( Comunes.formatoFijo(NoDistribuidorF.replace(',',' '),15," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(nombreDistribuidorF.replace(',',' '),100," ","A"));
			 
			contenidoArchivoFijo.append( Comunes.formatoFijo(claveEpoF.replace(',',' '),5," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(nombreEpoF.replace(',',' '),100," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(fechaOperacionF.replace(',',' '),20," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(claveMonedaF.replace(',',' '),5," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(monedaF.replace(',',' '),30," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(Comunes.formatoDecimal(montoCreditoF,2,SIN_COMAS),20," ","A"));
			if(!tipoPago.equals("2")) {
			contenidoArchivoFijo.append( Comunes.formatoFijo(plazoCreditoF.replace(',',' '),20," ","A"));
			}else {
			contenidoArchivoFijo.append( Comunes.formatoFijo(plazoCreditoF.replace(',',' ')+"(M)",20," ","A"));
			}			
			contenidoArchivoFijo.append( Comunes.formatoFijo(fechaVencimientoF.replace(',',' '),15," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(Comunes.formatoDecimal(montoInteresF,2,SIN_COMAS),15," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(claveTasaF.replace(',',' '),10," ","A"));
			contenidoArchivoFijo.append( Comunes.formatoFijo(Comunes.formatoDecimal(tasaInteresF,0,SIN_COMAS),15," ","A"));
			//contenidoArchivoFijo.append( Comunes.formatoFijo(tipoConversionF.replace(',',' '),30," ","A"));
			//contenidoArchivoFijo.append( Comunes.formatoFijo(MontoDocumentoF.replace(',',' '),10," ","A"));
			//contenidoArchivoFijo.append( Comunes.formatoFijo(Comunes.formatoDecimal(tipoCambioF,0).replace(',',' '),10," ","A"));
			//contenidoArchivoFijo.append( Comunes.formatoFijo(responsableF.replace(',',' '),10," ","A"));
			//contenidoArchivoFijo.append( Comunes.formatoFijo(referenciaF.replace(',',' '),100," ","A"));

			/*
			if (nombresCampo !=null ) {	
					for (int j=1;j<=nombresCampo.size();j++){	
					 if (j==1) {
						contenidoArchivoFijo.append(Comunes.formatoFijo(campo1F.replace(',',' '),100," ","A"));
					 } if (j==2) {
						contenidoArchivoFijo.append(Comunes.formatoFijo(campo2F.replace(',',' '),100," ","A"));
					 } if (j==3) { 
						contenidoArchivoFijo.append(Comunes.formatoFijo(campo3F.replace(',',' '),100," ","A"));
					 } if (j==4) {
						contenidoArchivoFijo.append(Comunes.formatoFijo(campo4F.replace(',',' '),100," ","A"));
					  } if (j==5) { 
						contenidoArchivoFijo.append(Comunes.formatoFijo(campo5F.replace(',',' '),100," ","A"));
						}
				  }       
			 }
			 */

			contenidoArchivoFijo.append( Comunes.formatoFijo(numeroCuenta.replace(',',' '),25," ","")); 	           
			contenidoArchivoFijo.append("\n");
		 } //for
	}

	if(totalDoctosMNF>0){
		contenidoArchivoFijo.append("T"+"0"+"0");
		contenidoArchivoFijo.append(totalDoctosMNF+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0");
		contenidoArchivoFijo.append(Comunes.formatoDecimal(totalMontoMNF,2,SIN_COMAS)+"0"+"0"+"0"+"0");
		contenidoArchivoFijo.append(Comunes.formatoDecimal(totalMontoIntMNF,2,SIN_COMAS)+"0"+"0");
		contenidoArchivoFijo.append("\n");
	}

   if(totalDoctosUSDF>0){
		contenidoArchivoFijo.append("T"+"0"+"0");
		contenidoArchivoFijo.append(totalDoctosUSDF+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0"+"0");
		contenidoArchivoFijo.append(Comunes.formatoDecimal(totalMontoUSDF,2,SIN_COMAS)+"0"+"0"+"0"+"0");
		contenidoArchivoFijo.append(Comunes.formatoDecimal(totalMontoIntUSDF,2,SIN_COMAS)+"0"+"0");
		contenidoArchivoFijo.append("\n");
	}

	/****************************************************************/
	//PARA GENERAR ARCHIVO INTEGRAL
	/*****************************************************************/

	// 3. Agregar Cabecera
	//contenidoArchivoIntegral.append(fechaHoy+","+HoraActual+","+"Documentos por Autorizar"+"\n");
	contenidoArchivoIntegral.append( 
					"\nNo. Documento final,"+
							"No. Distribuidor,"+
							"Nombre Distribuidor,"+
							"Cuenta Bancaria Distribuidor,"+
							"Clave Epo,"+
							"Nombre EPO,"+
							"Fecha solicitud,"+
							"Fecha de operación,"+
							"Clave de moneda,"+
							"Moneda,"+
							"Monto del crédito,"+
							"Plazo crédito,"+
							"Fecha vencimiento crédito,"+
							"Monto Interes,"+
							"Tipo de cobro intereses,"+ ///??
							"Referencia tasa de interés,"+
							"Clave Tasa interes,"+
							"Tasa interes,"+
							"Tipo de conversión,"+
							"Monto docto.inicial dls.,"+
							"Tipo de cambio,"+
							"Monto valuado," +////?
							"Responsable pago de interés,"+
							"Tipo cobranza,"+
							"Referencia,"+
							"CG campo1,"+ 
							"CG campo2,"+
							"CG campo3,"+
							"CG campo4,"+
							"CG campo5");
	for (int e=0;e<nombresCampo.size();e++){
		 detalleCampo = (Vector) nombresCampo.get(e);
		 contenidoArchivoIntegral.append((String)detalleCampo.get(0)+",");        
	}	
	//contenidoArchivoIntegral.append(" Número de Cuenta ");
	contenidoArchivoIntegral.append("\n");
	
	// 2. Agregar contenido
	for(int k=0;k<listaClavesEpo.size();k++){
		 
		 String claveDeLaEpo 	= (String) listaClavesEpo.get(k);
		 vecFilasVF 				= autSolic.getDoctosSelCCCVariableFijos(claveDeLaEpo,ic_pyme,iNoCliente,ig_numero_docto,df_fecha_seleccion_de,df_fecha_seleccion_a);
 
		 int numeroCamposAdicionales = nombresCampo.size();
		 if(hayMasDeUnaEpoSeleccionada){
			numeroCamposAdicionales = cargaDocto.getCamposAdicionales(claveDeLaEpo,false).size();
		 }
			
		 for(i=0;i<vecFilasVF.size();i++){
			 
			vecColumnasVF 			= (Vector)vecFilasVF.get(i);
			igNumeroDoctoV			= (String)vecColumnasVF.get(0);
			NoDistribuidorV 		= (String)vecColumnasVF.get(31);
			nombreDistribuidorV 	= (String)vecColumnasVF.get(2);
			claveEpoV 				= (String)vecColumnasVF.get(3);
			nombreEpoV 				= (String)vecColumnasVF.get(4);
			fechaOperacionV 		= (String)vecColumnasVF.get(5);
			claveMonedaV 			= (String)vecColumnasVF.get(6);
			monedaV 					= (String)vecColumnasVF.get(7);
			montoCreditoV 			= Double.parseDouble(vecColumnasVF.get(8).toString());
			plazoCreditoV 			= (String)vecColumnasVF.get(9);
			fechaVencimientoV 	= (String)vecColumnasVF.get(10);
			montoInteresV 			= Double.parseDouble(vecColumnasVF.get(11).toString());          
			claveTasaV 				= (String)vecColumnasVF.get(12);
			tasaInteresV 			= Double.parseDouble(vecColumnasVF.get(13).toString());
			tipoConversionV 		= (String)vecColumnasVF.get(14);
			MontoDocumentoV 		= (String)vecColumnasVF.get(15);
			tipoCambioV 			= Double.parseDouble(vecColumnasVF.get(16).toString());
			responsableV 			= (String)vecColumnasVF.get(17);
			referenciaV 			= (String)vecColumnasVF.get(18);
			campo1V 					= (String)vecColumnasVF.get(19);
			campo2V 					= (String)vecColumnasVF.get(20);
			campo3V 					= (String)vecColumnasVF.get(21);
			campo4V 					= (String)vecColumnasVF.get(22);
			campo5V 					= (String)vecColumnasVF.get(23);
			numeroCuenta 			= (String)vecColumnasVF.get(25);
			tipoCobroInteres		= (String)vecColumnasVF.get(27);
			tipoCobranza			= (String)vecColumnasVF.get(29);
			ctReferencia 			= (String)vecColumnasVF.get(30);
			montoValuado			=	Double.parseDouble(MontoDocumentoV)*tipoCambioV;
			if(numeroCuenta.equals("0")){
				numeroCuenta= "";
			}
		tipoPago =  (String)vecColumnasVF.get(32); //F09-2015  
 
			contenidoArchivoIntegral.append(igNumeroDoctoV.replace(',',' ')+","+
                            NoDistribuidorV.replace(',',' ')+","+
                            nombreDistribuidorV.replace(',',' ')+","+
									 numeroCuenta.replace(',',' ')+","+
                            claveEpoV.replace(',',' ')+","+
                            nombreEpoV.replace(',',' ')+","+
                            fechaOperacionV.replace(',',' ')+","+//fecha operacion???
									 fechaOperacionV.replace(',',' ')+","+
                            claveMonedaV.replace(',',' ')+","+
                            monedaV.replace(',',' ')+","+
                            Comunes.formatoDecimal(montoCreditoV,2,SIN_COMAS)+",");
                         
								   if(!tipoPago.equals("2")) {		 							
										contenidoArchivoIntegral.append( plazoCreditoV.replace(',',' ')+",");									 
									}else {	
										contenidoArchivoIntegral.append( plazoCreditoV.replace(',',' ')+" ( M ) ,");
									}
									
									
			contenidoArchivoIntegral.append(fechaVencimientoV.replace(',',' ')+","+
                            Comunes.formatoDecimal(montoInteresV,2,SIN_COMAS)+","+
                            tipoCobroInteres.replace(',',' ')+",");
									
									 if(!tipoPago.equals("2")) {									 
									 contenidoArchivoIntegral.append(referenciaV.replace(',',' ')+",");
										contenidoArchivoIntegral.append(claveTasaV.replace(',',' ')+",");
									}else  {
										contenidoArchivoIntegral.append(" "+",");
										contenidoArchivoIntegral.append(" "+",");
									}
				contenidoArchivoIntegral.append( Comunes.formatoDecimal(tasaInteresV,0,SIN_COMAS)+","+
									 tipoConversionV.replace(',',' ')+","+
                            MontoDocumentoV.replace(',',' ')+","+
                            Comunes.formatoDecimal(tipoCambioV,0,SIN_COMAS)+","+
									 Comunes.formatoDecimal(montoValuado,2,SIN_COMAS)+","+
                            responsableV.replace(',',' ')+","+
									 tipoCobranza.replace(',',' ')+","+
                            ctReferencia.replace(',',' ')+",");
         
			if (nombresCampo !=null ) {
				
				for (int j=1;j<=nombresCampo.size();j++){	
					
					if(j>numeroCamposAdicionales){
						contenidoArchivoIntegral.append("N/A,");
						continue;
				 	}
					
					if (j==1) {
						contenidoArchivoIntegral.append(campo1V.replace(',',' ')+",");
					} else if (j==2) {
						contenidoArchivoIntegral.append(campo2V.replace(',',' ')+",");    
					} else if (j==3) { 
						contenidoArchivoIntegral.append(campo3V.replace(',',' ')+",");
					} else if (j==4) {
						contenidoArchivoIntegral.append(campo4V.replace(',',' ')+",");
					} else if (j==5) { 
						contenidoArchivoIntegral.append(campo5V.replace(',',' ')+",");
					}
					
				}
				
			}
 
			if("1".equals(claveMonedaV)){
				totalDoctosMNI 		++;
				totalMontoMNI 			+= montoCreditoV;
				totalMontoIntMNI		+= montoInteresV;
			}else if("54".equals(claveMonedaV)){
				totalDoctosUSDI		++;
				totalMontoUSDI 		+= montoCreditoV;			
				totalMontoIntUSDI		+= montoInteresV;
			}		
		
			//contenidoArchivoVariable.append(numeroCuenta.replace(',',' ')+",");
			contenidoArchivoIntegral.append("\n");
			
		}//for
 
	}
	
   if(totalDoctosMNI>0){
		//contenidoArchivoIntegral.append("T"+",");
		contenidoArchivoIntegral.append("Total M.N."+",");
		contenidoArchivoIntegral.append(totalDoctosMNI+",");
		contenidoArchivoIntegral.append("Monto Total Créditos M.N.,"+Comunes.formatoDecimal(totalMontoMNI,2,SIN_COMAS)+",");
		contenidoArchivoIntegral.append("Total Monto Interés M.N.,"+Comunes.formatoDecimal(totalMontoIntMNI,2,SIN_COMAS)+",");
		contenidoArchivoIntegral.append("\n");  
	}
  
   if(totalDoctosUSDI>0){

		//contenidoArchivoIntegral.append("T"+",");
		contenidoArchivoIntegral.append("Total Dolares"+",");
		contenidoArchivoIntegral.append(totalDoctosUSDI+",");
		contenidoArchivoIntegral.append("Monto Total Créditos D.L.,"+Comunes.formatoDecimal(totalMontoUSDI,2,SIN_COMAS)+",");
		contenidoArchivoIntegral.append("Total Monto Interés D.L.,"+Comunes.formatoDecimal(totalMontoIntUSDI,2,SIN_COMAS)+",");
		contenidoArchivoIntegral.append("\n");
	}

	// Generar Archivo Solicitado
	// 1. Archivo
	if(tipoArchivo.equals("G")){
		contenidoArchivoGeneral = contenidoArchivo;
	}
	// 2. Archivo de Interfase Variable
	if(tipoArchivo.equals("V")){
		contenidoArchivoGeneral = contenidoArchivoVariable;
	}
	// 4. Archivo Integral
	if(tipoArchivo.equals("I")){      
		contenidoArchivoGeneral = contenidoArchivoIntegral; 
	}
	if(!archivo.make(contenidoArchivoGeneral.toString(), strDirectorioTemp, ".csv")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivo = archivo.nombre;
		jsonObj.put("success", new Boolean(true));
	}

	// 3. Archivo de Interfase Fija
	if(tipoArchivo.equals("F")){
		if(!archivo.make(contenidoArchivoFijo.toString(), strDirectorioTemp, ".txt")){
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		} else{
			 nombreArchivo = archivo.nombre;
		}
	}
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
}	finally {
	}
%>
<%=jsonObj%>