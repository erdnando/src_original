 <%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.sql.*,com.netro.exception.*,
		netropology.utilerias.*, com.netro.distribuidores.*,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeDistribuidores,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%

String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "", consulta = "";

if (informacion.equals("valoresIniciales"))	{

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("iCliente", iNoCliente);
	jsonObj.put("usuario", strLogin+" - "+strNombre);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoEPODist") ) {

		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClaveIf(iNoCliente);
		cat.setTipoCredito("D");
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoDistribuidor")){

	String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");

	if(!"".equals(ic_epo)){
		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setClaveEpo(ic_epo);
		cat.setTipoCredito("D");
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();
	}else{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
}else if (informacion.equals("Consulta")	||	informacion.equals("ArchivoPDF")){

	String ic_epo	= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String ic_pyme	= (request.getParameter("ic_pyme")==null)?"1":request.getParameter("ic_pyme");
	int start = 0;
	int limit = 0;
	com.netro.distribuidores.BloqueaIfDistEpo pag = new com.netro.distribuidores.BloqueaIfDistEpo();
	pag.setIc_if(iNoCliente);
	pag.setIc_epo(ic_epo);
	pag.setIc_pyme(ic_pyme);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pag);

	if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		JSONObject jsonObjG = new JSONObject();
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) {	
				queryHelper.executePKQuery(request);				
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);
			
			jsonObjG = JSONObject.fromObject(consulta);
			jsonObjG.put("success",new Boolean(true));
			infoRegresar = jsonObjG.toString();
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("ArchivoPDF")) {
		try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}	

}else if (informacion.equals("Confirmar")){
	System.out.println("cadena cadena *************************************");
	JSONObject jsonObj = new JSONObject();
	String ic_epo	= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String ic_pyme	= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String cadena	= (request.getParameter("cadena")==null)?"":request.getParameter("cadena");
	System.out.println("cadena cadena "+cadena);
	try{
		AccesoDB con = new AccesoDB();
		ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 

		String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());

		String pkcs7 = request.getParameter("Pkcs7");
		String serial = "";
		String folioCert = "";
		String externContent = request.getParameter("TextoFirmado");
		char getReceipt = 'Y';
		boolean tranza = true;
		Seguridad s = new Seguridad();
		Acuse		acuse	= null;
		String	_acuse	= "";
		
		try{
			con.conexionDB();
			acuse	= new Acuse(Acuse.ACUSE_IF,"4","com",con);
		}catch(Exception e){
			throw new NafinException("SIST001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
  
		if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			folioCert = acuse.toString();	
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
				_acuse = s.getAcuse(); 
				BeanParametros.setPymesBloqueoDistEpoIf(cadena, strLogin, acuse.toString(),  _acuse);
				jsonObj.put("msg", "La autentificación se llevo a cabo con éxito");
				jsonObj.put("_acuse", _acuse);
				jsonObj.put("acuse", acuse.formatear());
				jsonObj.put("fechaCarga", fechaHoy);
				jsonObj.put("horaCarga", horaActual);
				jsonObj.put("usuario", strNombreUsuario);
				
			}else{
				tranza = false;
				jsonObj.put("msg", "La autentificación no se llevó a cabo.<br>Proceso CANCELADO");
			}
		}else{
			tranza = false;
			jsonObj.put("msg", "La autentificación no se llevó a cabo.<br>Proceso CANCELADO");
		}
		jsonObj.put("success", new Boolean(tranza));
	}catch(Exception e){
		e.printStackTrace();
	}
	
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>