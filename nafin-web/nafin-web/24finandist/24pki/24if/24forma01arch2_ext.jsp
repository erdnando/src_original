<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,	java.util.*, java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	netropology.utilerias.negocio.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%> 
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String fechaHoy	= "";
String fechaActual	= "";
String HoraActual	= "";
String fechaCarga = "",	horaCarga =	"";
try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	HoraActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
}catch(Exception e){
	fechaHoy = "";
}

JSONObject jsonObj = new JSONObject();
String	ic_documentos[]	= request.getParameterValues("selecciona");
String	[]doctosPorEpo		= request.getParameterValues("doctosPorEpo");
String 	aux_acuse			= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String 	ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String 	hidTipoCredito		= (request.getParameter("hidTipoCredito")==null)?"":request.getParameter("hidTipoCredito");
String 	tipoArchivo			= (request.getParameter("tipoArchivo")		==null)?"":request.getParameter("tipoArchivo");
String 	accion			= (request.getParameter("accion")		==null)?"":request.getParameter("accion");
//System.out.println("accion:"+accion);

//VARIABLES DE USO LOCAL
boolean SIN_COMAS = false;
int i = 0;
Vector vecFilas		= null;
Vector vecColumnas	= null;

CreaArchivo	archivo = new CreaArchivo();

StringBuffer contenidoArchivo = new StringBuffer(2000);
StringBuffer contenidoArchVar = new StringBuffer(2000);
StringBuffer contenidoArchFijo = new StringBuffer(2000);
StringBuffer contenidoArchIntegral = new StringBuffer(2000);

String nombreArchivo = null;
String nombreArchivoVar = null;
String nombreArchivoFijo = null;
String nombreArchivoIntegral = null;

StringBuffer ArchivoVariable = new StringBuffer(2000);
StringBuffer ArchivoFijo = new StringBuffer(2000);
StringBuffer ArchivoIntegral = new StringBuffer(2000);
//DE DESPLIEGUE
//segunda tabla
String acuse = "",	nombreEPO = "",	moneda = "",	icMoneda = "",	icDocumento = "",	igNumeroDocto = "",	fechaSolicitud = "",	distribuidor = "",	referencia = "";
String tipoConversion = "",	plazo = "",	fechaVto = "",	tipoPagoInteres = "",	monedaLinea = "";
double	monto					=	0;
double	tipoCambio			=	0;
double	montoValuado		=	0;
double	tasaInteres			=	0;
double	montoInteres		=	0;
double	porcDescuento		=	0;
double	montoDescuento		=	0;
double	totalMontoIntConv =	0;
//tercera tabla
String rs_numDist = "",	rs_cveTasa = "",	rs_tipoConver = "",	rs_tipoCamb = "",	rs_respInt = "";
String rs_tipoCob = "",	rs_referencia = "",	rs_campo1 = "",	rs_campo2 = "",	rs_campo3 = "",	
rs_campo4 = "",	rs_campo5 = "",	numerocuenta ="",  tipoPago ="";

//totales
int		totalDoctosMN				= 0;
int 		totalDoctosUSD				= 0;
int 		totalDoctosUSDBot			= 0;
double	totalMontoMN				= 0;
double	totalMontoUSD				= 0;
double	totalMontoUSDBot			= 0;
double 	totalMontoValuadoMN		= 0;
double 	totalMontoValuadoUSD		= 0;
double 	totalMontoValuadoUSDBot	= 0;
double	totalMontoIntMN			= 0;
double	totalMontoIntUSD			= 0;
double	totalMontoIntUSDBot		= 0;
double	totalMontoDescMN			= 0;
double	totalMontoDescUSD			= 0;
double	totalMontoDescUSDBot		= 0;
int		totalDoctosConv			= 0;
double	totalMontoConvMN			= 0;
double	totalMontoConv				= 0;

CrearArchivosAcuses  generaAch =  new CrearArchivosAcuses(); 
	
	int existeArch=0;
	if(tipoArchivo.equals("VAR") )  {
		
		generaAch.setAcuse(aux_acuse);
		generaAch.setTipoArchivo("CSV");	
		generaAch.setStrDirectorioTemp(strDirectorioTemp);
		generaAch.setUsuario(iNoUsuario);
		generaAch.setVersion("PANTALLA");	
		existeArch= 	generaAch.existerArcAcuse();
	}
	System.out.println("tipoArchivo ===="+tipoArchivo +"             existeArch ===="+existeArch); 
	
try {
	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);

	AutorizacionSolic autSolic2 = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);

	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);

	// Obtener la lista de las Epos Seleccionadas
	HashMap 			doctosAgrupados 	= getDoctosAgrupadosPorEpo(ic_documentos, doctosPorEpo);

	ArrayList 		listaClavesEpos	= (ArrayList) doctosAgrupados.get("LISTA_CLAVES_EPOS");
	if(listaClavesEpos.size()== 0){
		throw new Exception("No se ha seleccionado ningún documento válido.");
	}
	boolean	hayMasDeUnaEpoSeleccionada = listaClavesEpos.size()>1?true:false;

	// Obtener HashMap con los Parametros de Fondeo
	HashMap catalogoTipoFondeo = hayMasDeUnaEpoSeleccionada?BeanAutDescuento.getTipoFondeo(iNoCliente, listaClavesEpos, "4"):null;

	Vector nombresCampo = new Vector(5);
	Vector detalleCampo = null;

	// F029 - 2011: Si selecciono mas de una EPO usar nombres de campos genericos
	if(hayMasDeUnaEpoSeleccionada){

		Vector fila = null;
		nombresCampo = new Vector();
		fila = new Vector();
		fila.add("CG CAMPO 1");
		nombresCampo.add(fila);
			  
		fila = new Vector();
		fila.add("CG CAMPO 2");
		nombresCampo.add(fila);
			  
		fila = new Vector();
		fila.add("CG CAMPO 3");
		nombresCampo.add(fila);
			  
		fila = new Vector();
		fila.add("CG CAMPO 4");
		nombresCampo.add(fila);
			  
		fila = new Vector();
		fila.add("CG CAMPO 5");
		nombresCampo.add(fila);
	}else{
		String claveEpo = (String) listaClavesEpos.get(0);
		nombresCampo 	= cargaDocto.getCamposAdicionales(claveEpo,false);
	}

	vecFilas = autSolic2.getDoctosSeleccionadosIF(aux_acuse,hidTipoCredito);
	for(i=0;i<vecFilas.size();i++){
		
		vecColumnas 		=  (Vector)vecFilas.get(i);
		icMoneda				=	(String)vecColumnas.get(5);
		monto					=	Double.parseDouble(vecColumnas.get(24).toString());
		tipoCambio			=	Double.parseDouble(vecColumnas.get(8).toString());
		montoValuado		=	monto*tipoCambio;
		montoInteres 		=	Double.parseDouble(vecColumnas.get(15).toString());
		porcDescuento		=	Double.parseDouble(vecColumnas.get(18).toString());
		montoDescuento		= 	montoValuado * (porcDescuento/100);
		monedaLinea			= 	(String)vecColumnas.get(23);
		
		if("1".equals(icMoneda)){
			totalDoctosMN 			++;
			totalMontoMN 			+= monto;
			totalMontoValuadoMN 	+= montoValuado;
			totalMontoIntMN		+= montoInteres;
		}
		
		if("54".equals(icMoneda)&&"54".equals(monedaLinea)){
			totalDoctosUSD			++;
			totalMontoUSD 			+= monto;
			totalMontoValuadoUSD	+= montoValuado;
			totalMontoIntUSD		+= montoInteres;
		}
		
		if("54".equals(icMoneda)&&"1".equals(monedaLinea)){
			totalDoctosConv		++;
			totalMontoConv			+= monto;
			totalMontoConvMN		+= montoValuado;
			totalMontoIntConv		+= montoInteres;
		}
	}

	contenidoArchivo.append("Moneda Nacional,,Dolares,,Documentos en DLL financiados en M.N.");
	contenidoArchivo.append("\nNúmero de solicitudes seleccionadas, Monto de Solicitudes seleccionadas, Número de Solicitudes seleccionadas, Monto de Solicitudes Seleccionadas, Número de Solicitudes seleccionadas, Monto de Solicitudes Seleccionadas");
	contenidoArchivo.append("\n"+totalDoctosMN+","+totalMontoMN+","+totalDoctosUSD+","+totalMontoUSD+","+totalDoctosConv+","+totalMontoConvMN);

	vecFilas = autSolic2.getDoctosSeleccionadosIF(aux_acuse,hidTipoCredito);
	String claveDeLaEpo = null;
	for(i=0;i<vecFilas.size();i++){

		vecColumnas 		=  (Vector)vecFilas.get(i);
		igNumeroDocto		=	(String)vecColumnas.get(0);
		fechaSolicitud		=	(String)vecColumnas.get(1);
		distribuidor		=	(String)vecColumnas.get(2);
		moneda				=	(String)vecColumnas.get(4);
		icMoneda				=	(String)vecColumnas.get(23);
		monto					=	Double.parseDouble(vecColumnas.get(24).toString());
		tipoConversion		=	(String)vecColumnas.get(7);
		tipoCambio			=	Double.parseDouble(vecColumnas.get(8).toString());
		montoValuado		=	monto*tipoCambio;
		referencia			=	(String)vecColumnas.get(9);
		tasaInteres			=	Double.parseDouble(vecColumnas.get(10).toString());
		plazo					=	(String)vecColumnas.get(11);
		fechaVto				=	(String)vecColumnas.get(12);
		tipoPagoInteres	=	(String)vecColumnas.get(13);
		montoInteres 		=	Double.parseDouble(vecColumnas.get(15).toString());
		icDocumento			=	(String)vecColumnas.get(16);
		nombreEPO			=	(String)vecColumnas.get(17);
		acuse 				=	(String)vecColumnas.get(19);
		fechaCarga			=	(String)vecColumnas.get(20);
		horaCarga			=	(String)vecColumnas.get(21);

		if(hidTipoCredito.equals("D")) {
			rs_numDist			=	(String)vecColumnas.get(25);
			rs_cveTasa			=	(String)vecColumnas.get(26);
			rs_tipoConver		=	(String)vecColumnas.get(27);
			rs_tipoCamb			=	(String)vecColumnas.get(28);
			rs_respInt			=	(String)vecColumnas.get(29);
			rs_tipoCob			=	(String)vecColumnas.get(30);
			rs_referencia 		=	(String)vecColumnas.get(31);
			rs_campo1			=	(String)vecColumnas.get(32);
			rs_campo2			=	(String)vecColumnas.get(33);
			rs_campo3			=	(String)vecColumnas.get(34);
			rs_campo4			=	(String)vecColumnas.get(35);
			rs_campo5 			=	(String)vecColumnas.get(36);
			tasaInteres			=	Double.parseDouble(vecColumnas.get(37).toString());
			numerocuenta		=	(String)vecColumnas.get(38);
			if(numerocuenta.equals("0")){
				numerocuenta	= "";
			}
			claveDeLaEpo 		=	(String)vecColumnas.get(39);
		}else {
			rs_campo1			=	(String)vecColumnas.get(25);
			rs_campo2			=	(String)vecColumnas.get(26);
			rs_campo3			=	(String)vecColumnas.get(27);
			rs_campo4			=	(String)vecColumnas.get(28);
			rs_campo5 			=	(String)vecColumnas.get(29);
			tasaInteres			=	Double.parseDouble(vecColumnas.get(30).toString());
			numerocuenta		=	(String)vecColumnas.get(31);
			claveDeLaEpo 		=	(String)vecColumnas.get(32);
			tipoPago 		=	(String)vecColumnas.get(40);
			rs_cveTasa			=	(String)vecColumnas.get(34);
		}

		if(i==0){

			contenidoArchivo.append("\n\nNo. de documento final,EPO Relacionada,Moneda,Monto,Tipo Conv,Tipo Cambio,Monto Valuado,Referencia tasa de inters,Tasa de Interes,Plazo,Fecha de Vencimiento,Monto de Interes,Tipo de Cobro de Interes,");
			for (int e=0;e<nombresCampo.size();e++){
				detalleCampo 		= 	(Vector) nombresCampo.get(e);
				contenidoArchivo.append((String) detalleCampo.get(0)+",");
			}
			contenidoArchivo.append(" Número de Cuenta ");
			if(hayMasDeUnaEpoSeleccionada){
				contenidoArchivo.append(",Tipo de Financiamiento ");
			}
			if(existeArch==0)  {
				contenidoArchVar.append(
					"\nD,No. Documento final,"+
					"No. Distribuidor,"+
					"Nombre Distribuidor,"+
					"Clave Epo,"+
					"Nombre EPO,"+
					"Fecha de operación,"+
					"Clave de moneda,"+
					"Moneda,"+
					"Monto del crédito,"+
					"Plazo crédito,"+
					"Fecha vencimiento crédito,"+
					"Monto Interes,"+
					"Clave Tasa interes,"+
					"Tasa interes,"+
					"Tipo de conversión,"+
					"Monto docto.inicial dls.,"+
					"Tipo de cambio,"+
					"Responsable pago de interés,"+
					"Tipo cobranza,"+
					"Referencia,");
				for (int e=0;e<nombresCampo.size();e++){
					detalleCampo 		= (Vector) nombresCampo.get(e);
					contenidoArchVar.append((String)detalleCampo.get(0)+",");
				}
				contenidoArchVar.append(" Número de Cuenta ");
			}
			
			contenidoArchIntegral.append(
							"\nNo. Documento final,"+
							"No. Distribuidor,"+
							"Nombre Distribuidor,"+
							"Cuenta Bancaria Distribuidor,"+
							"Clave Epo,"+
							"Nombre EPO,"+
							"Fecha solicitud,"+
							"Fecha de operación,"+
							"Clave de moneda,"+
							"Moneda,"+
							"Monto del crédito,"+
							"Plazo crédito,"+
							"Fecha vencimiento crédito,"+
							"Monto Interes,"+
							"Tipo de cobro intereses,"+ ///??
							"Referencia tasa de interés,"+
							"Clave Tasa interes,"+
							"Tasa interes,"+
							"Tipo de conversión,"+
							"Monto docto.inicial dls.,"+
							"Tipo de cambio,"+
							"Monto valuado," +////?
							"Responsable pago de interés,"+
							"Tipo cobranza,"+
							"Referencia,"+
							"CG campo1,"+ 
							"CG campo2,"+
							"CG campo3,"+
							"CG campo4,"+
							"CG campo5");

		}	//if

		// 1. Agregar contenido al archivo
		contenidoArchivo.append("\n"+icDocumento+","+nombreEPO.replace(',',' ')+","+moneda+","+monto+","+tipoConversion+","+((tipoCambio==1)?"":tipoCambio+"")+","+montoValuado);
		if(!tipoPago.equals("2")) {
			contenidoArchivo.append(","+referencia);
		}else  {
			contenidoArchivo.append(","+" "); 
		}
		contenidoArchivo.append(","+tasaInteres);
		
		if(!tipoPago.equals("2")) {
			contenidoArchivo.append(","+plazo);
		}else {
			contenidoArchivo.append(","+plazo+" (M)");
		}
		
		
		contenidoArchivo.append(","+fechaVto+","+montoInteres+","+tipoPagoInteres+","+	
      	rs_campo1.trim()+","+
			rs_campo2.trim()+","+
			rs_campo3.trim()+","+
			rs_campo4.trim()+","+
			rs_campo5.trim()+","+
			numerocuenta.trim()+
			(hayMasDeUnaEpoSeleccionada?BeanAutDescuento.getDescripcionTipoFondeo(catalogoTipoFondeo,claveDeLaEpo).replaceAll(",",""):""));
 
		// 2. Agregar contenido al archivo variable
		if(existeArch==0)  {
			contenidoArchVar.append("\nD,"+
			icDocumento.trim()+","+
			rs_numDist.trim()+","+
			distribuidor.replace(',',' ')+","+
			claveDeLaEpo.trim()+","+
			nombreEPO.replace(',',' ')+","+
			fechaSolicitud.trim()+","+
			icMoneda.trim()+","+
			moneda.trim()+","+
			Comunes.formatoDecimal(monto,2,SIN_COMAS)+",");
			
			if(!tipoPago.equals("2")) {
				contenidoArchVar.append(plazo.trim()+",");
			}else {
				contenidoArchVar.append(plazo.trim()+" (M) ,");
			}			
			
			contenidoArchVar.append(fechaVto.trim()+","+
			Comunes.formatoDecimal(montoInteres,2,SIN_COMAS)+",");
			
			if(!tipoPago.equals("2")) {
			contenidoArchVar.append(rs_cveTasa.trim()+",");
			}else  {
				contenidoArchVar.append(",");
			}
			
			contenidoArchVar.append(Comunes.formatoDecimal(tasaInteres,5,SIN_COMAS)+",");
			
			if(!"1".equals(rs_tipoCamb.trim())){
					contenidoArchVar.append(
					rs_tipoConver.trim()+","+
					Comunes.formatoDecimal(montoValuado,2,SIN_COMAS)+","+
					rs_tipoCamb.trim()+",");
			}
			else {
				contenidoArchVar.append(
					"N,0.00,0.00,");
			}
		contenidoArchVar.append(
			rs_respInt.trim()+","+
			rs_tipoCob.trim()+",");
		//if(hidTipoCredito.equals("D")) {
		if(!tipoPago.equals("2")) {
			contenidoArchVar.append( rs_referencia.trim()+",");
		}else  {
			contenidoArchVar.append( " ".trim()+",");
		}
		contenidoArchVar.append(
			//rs_referencia.trim()+","+
			rs_campo1.trim()+","+
			rs_campo2.trim()+","+
			rs_campo3.trim()+","+
			rs_campo4.trim()+","+
			rs_campo5.trim()+","+
			numerocuenta.trim());
		//}
		}
      // 3. Agregar contenido al archivo fijo
		contenidoArchFijo.append("\nD"+
			Comunes.formatoFijo(Comunes.formatoDecimal(icDocumento,0,SIN_COMAS),10,"0","") + 
			Comunes.formatoFijo(rs_numDist,25," ","A") +
			Comunes.formatoFijo(distribuidor.replace(',',' '),100," ","A") +
			Comunes.formatoFijo(Comunes.formatoDecimal(claveDeLaEpo,0,SIN_COMAS),3,"0","") + 
			Comunes.formatoFijo(nombreEPO.replace(',',' '),100," ","A") +
			Comunes.formatoFijo(fechaSolicitud,10," ","") + 
			Comunes.formatoFijo(Comunes.formatoDecimal(icMoneda,0,SIN_COMAS),3,"0","") + 
			Comunes.formatoFijo(moneda,30," ","A") +
			Comunes.formatoFijo(Comunes.formatoDecimal(monto,2,SIN_COMAS),15,"0","") );
			
			if(!tipoPago.equals("2")) {				
				contenidoArchFijo.append( Comunes.formatoFijo(Comunes.formatoDecimal(plazo,0,SIN_COMAS),3,"0","") );			
			}else {	
				contenidoArchFijo.append(Comunes.formatoFijo(Comunes.formatoDecimal(plazo,0,SIN_COMAS),3,"0","" )+" (M)" );
			}
			
			contenidoArchFijo.append( Comunes.formatoFijo(fechaVto,10," ","") + 
			Comunes.formatoFijo(Comunes.formatoDecimal(montoInteres,2,SIN_COMAS),15,"0","") + 
			Comunes.formatoFijo(Comunes.formatoDecimal(rs_cveTasa,0,SIN_COMAS),3,"0","") + 
			Comunes.formatoFijo(Comunes.formatoDecimal(tasaInteres,5,SIN_COMAS),8,"0",""));
		if(!"1".equals(rs_tipoCamb.trim())){
			contenidoArchFijo.append(
				Comunes.formatoFijo(rs_tipoConver,1," ","A") +
				Comunes.formatoFijo(Comunes.formatoDecimal(montoValuado,2,SIN_COMAS),15,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(rs_tipoCamb,2,SIN_COMAS),15,"0",""));
		}
		else {
			contenidoArchFijo.append(
				Comunes.formatoFijo("N",1," ","A") +
				Comunes.formatoFijo(Comunes.formatoDecimal("0",2,SIN_COMAS),15,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal("0",2,SIN_COMAS),15,"0",""));
		}
		contenidoArchFijo.append(
			Comunes.formatoFijo(rs_respInt,1," ","A") +
			Comunes.formatoFijo(rs_tipoCob,1," ","A"));
		if(hidTipoCredito.equals("D")) {
			contenidoArchFijo.append(
				Comunes.formatoFijo(rs_referencia,100," ","A") +
				Comunes.formatoFijo(rs_campo1,100," ","A") +
				Comunes.formatoFijo(rs_campo2,100," ","A") +
				Comunes.formatoFijo(rs_campo3,100," ","A") +
				Comunes.formatoFijo(rs_campo4,100," ","A") +
				Comunes.formatoFijo(rs_campo5,100," ","A")+".");			
		}
		contenidoArchFijo.append(	Comunes.formatoFijo(numerocuenta,25," ","A"));
		
		// 4. Agregar contenido al archivo Integral
		contenidoArchIntegral.append("\n"+
			icDocumento.trim()+","+
			rs_numDist.trim()+","+
			distribuidor.replace(',',' ')+","+
			numerocuenta.trim()+","+
			claveDeLaEpo.trim()+","+
			nombreEPO.replace(',',' ')+","+
			fechaSolicitud.trim()+","+
			fechaSolicitud.trim()+","+//fecha operacion ????
			icMoneda.trim()+","+
			moneda.trim()+","+
			Comunes.formatoDecimal(monto,2,SIN_COMAS)+",");
			
			if(!tipoPago.equals("2")) {	
				contenidoArchIntegral.append(plazo.trim()+",");	
			}else  {	
				contenidoArchIntegral.append(plazo.trim()+" (M) ,");
			}
	
			contenidoArchIntegral.append( fechaVto.trim()+","+
			Comunes.formatoDecimal(montoInteres,2,SIN_COMAS)+","+
			tipoPagoInteres+",");//tipo de cobro de interes
			if(!tipoPago.equals("2")) {	
				contenidoArchIntegral.append(referencia+",");//refrencia de asas de interes
				contenidoArchIntegral.append( rs_cveTasa.trim()+",");
			}else  {
				contenidoArchIntegral.append(" "+",");//refrencia de asas de interes
					contenidoArchIntegral.append(" "+",");//refrencia de asas de interes
			}
			
			contenidoArchIntegral.append( Comunes.formatoDecimal(tasaInteres,5,SIN_COMAS)+"," );
			
			if(!"1".equals(rs_tipoCamb.trim())){
					contenidoArchIntegral.append(
					tipoConversion.trim()+","+
					Comunes.formatoDecimal(montoValuado,2,SIN_COMAS)+","+
					rs_tipoCamb.trim()+","+
					Comunes.formatoDecimal(montoValuado,2,SIN_COMAS)+",");
			}
			else {
				contenidoArchIntegral.append(
					tipoConversion.trim()+","+
					"0.00,"+
					rs_tipoCamb.trim()+","+"0.00,");
			}
		contenidoArchIntegral.append(
			rs_respInt.trim()+","+
			rs_tipoCob.trim()+",");
			
		//if(hidTipoCredito.equals("D")) {
		
		if(!tipoPago.equals("2")) {	
			contenidoArchIntegral.append(rs_referencia.trim()+",");
		}else  {		
			contenidoArchIntegral.append("".trim()+",");
		}
				
		contenidoArchIntegral.append(
			//rs_referencia.trim()+","+
			rs_campo1.trim()+","+
			rs_campo2.trim()+","+
			rs_campo3.trim()+","+
			rs_campo4.trim()+","+
			rs_campo5.trim());
			//numerocuenta.trim();
		
	}	//for

	// Finalizar archivos
	// 1. Construir version final del archivo
	if(totalDoctosMN+totalDoctosConv>0){	
		contenidoArchivo.append("\nTotal M.N.,"+(totalDoctosMN+totalDoctosConv)+",,"+(totalMontoMN+totalMontoConv)+",,,,,,,,"+(totalMontoIntMN+totalMontoIntConv));
	}
	if(totalDoctosUSD>0){
		contenidoArchivo.append("\nTotal Dolares,"+(totalDoctosUSD)+",,"+(totalMontoUSD)+",,,,,,,,"+totalMontoIntUSD);
	}

	// Agregar Acuse
	contenidoArchivo.append("\n\nNo. Acuse,"+acuse+"\nFecha,"+fechaCarga+"\nHora,"+horaCarga+"\nClave y número de usuario,"+iNoUsuario+" "+strNombreUsuario);

	// 2. Construir la version final del archivo variable.
	if(existeArch==0)  {
		ArchivoVariable.append(
		"H," +fechaActual+","+HoraActual+",Documentos Autorizados" +
		contenidoArchVar.toString()+
		"\nT,Total registros M.N.,"+totalDoctosMN+
		",Monto Total Créditos M.N.,"+Comunes.formatoDecimal(totalMontoMN,2,SIN_COMAS)+
		",Total Monto Interés M.N.,"+Comunes.formatoDecimal(totalMontoIntMN,2,SIN_COMAS)+
		",Total registros D.L.,"+totalDoctosUSDBot+
		",Monto Total Créditos D.L.,"+Comunes.formatoDecimal(totalMontoUSDBot,2,SIN_COMAS)+
		",Total Monto Interés D.L.,"+Comunes.formatoDecimal(totalMontoIntUSDBot,2,SIN_COMAS));
	}

	// 3. Construir la version final del archivo fijo.
	ArchivoFijo.append(
		"H" +fechaActual+HoraActual+"  Documentos Autorizados" +
		contenidoArchFijo.toString()+
		"\nT"+
		Comunes.formatoFijo(Comunes.formatoDecimal(totalDoctosMN,0,SIN_COMAS),14,"0","") +
		Comunes.formatoFijo(Comunes.formatoDecimal(totalMontoMN,2,SIN_COMAS),15,"0","") +
		Comunes.formatoFijo(Comunes.formatoDecimal(totalMontoIntMN,2,SIN_COMAS),15,"0","") +
		Comunes.formatoFijo(Comunes.formatoDecimal(totalDoctosUSDBot,0,SIN_COMAS),14,"0","") +
		Comunes.formatoFijo(Comunes.formatoDecimal(totalMontoUSDBot,2,SIN_COMAS),15,"0","") +
		Comunes.formatoFijo(Comunes.formatoDecimal(totalMontoIntUSDBot,2,SIN_COMAS),15,"0",""));

	// 2. Construir la version final del archivo variable.
	ArchivoIntegral.append(
		//"H," +fechaActual+","+HoraActual+",Documentos Autorizados" +
		contenidoArchIntegral.toString()+
		"\nTotal registros M.N.,"+totalDoctosMN+
		",Monto Total Créditos M.N.,"+Comunes.formatoDecimal(totalMontoMN,2,SIN_COMAS)+
		",Total Monto Interés M.N.,"+Comunes.formatoDecimal(totalMontoIntMN,2,SIN_COMAS)+
		"\nTotal registros D.L.,"+totalDoctosUSDBot+
		",Monto Total Créditos D.L.,"+Comunes.formatoDecimal(totalMontoUSDBot,2,SIN_COMAS)+
		",Total Monto Interés D.L.,"+Comunes.formatoDecimal(totalMontoIntUSDBot,2,SIN_COMAS));

	// 1. Archivo
	if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivo = archivo.nombre;
		jsonObj.put("success", new Boolean(true));
	}
	
	if(existeArch==0)  {
		// 2. Archivo de Interfase Variable
		if (!archivo.make(ArchivoVariable.toString(), strDirectorioTemp, ".csv")){
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		}else{
			nombreArchivoVar = archivo.nombre;
			jsonObj.put("success", new Boolean(true));			
		}
	}

	// 4. Archivo Integral
	if (!archivo.make(ArchivoIntegral.toString(), strDirectorioTemp, ".csv")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivoIntegral = archivo.nombre;
		jsonObj.put("success", new Boolean(true));
	}

	// 3. Archivo Interfase Fija
	if (!archivo.make(ArchivoFijo.toString(), strDirectorioTemp, ".txt")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivoFijo = archivo.nombre;
		jsonObj.put("success", new Boolean(true));
	}
	// Definir archivo a descargar
	if(tipoArchivo.equals("ARCH")){
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
                jsonObj.put("accion",accion);
	}else if(tipoArchivo.equals("VAR")){
		
		if(existeArch==0)  {
		
			generaAch.setRutaArchivo(strDirectorioTemp+nombreArchivoVar);
			generaAch.guardarArchivo( ) ;	//Guardar	 
		
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoVar);
			jsonObj.put("accion",accion);
			
		}else if(existeArch>0)  {
	
			nombreArchivoVar = generaAch.desArchAcuse( ) ;
		
			jsonObj.put("success", new Boolean(true));	
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoVar);
			jsonObj.put("accion",accion);
		
		}
		
		
	}else if(tipoArchivo.equals("FIJO")){
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoFijo);
                jsonObj.put("accion",accion);
	}else if(tipoArchivo.equals("INTEG")){
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoIntegral);
                jsonObj.put("accion",accion);
	}
	
} catch (Exception exception) {
		
		java.io.StringWriter outSW = new java.io.StringWriter();
		exception.printStackTrace(new java.io.PrintWriter(outSW));
		String stackTrace = outSW.toString();
			
		generaAch.setDesError(stackTrace);	
		
		generaAch.guardaBitacoraArch();	
	
	throw new NafinException("Error al generar el archivo");
	
} 


%>
<%!
	public HashMap getDoctosAgrupadosPorEpo(String[] doctosSeleccionados, String[] doctosPorEpo){
		HashMap 			doctosAgrupados 	= new HashMap();
		LinkedHashSet	listaClaves			= new LinkedHashSet();
		ArrayList		listaClavesEpos	= new ArrayList();
		HashMap			resultado			= new HashMap();

		for(int i=0;i<doctosPorEpo.length;i++){
			String[] doctoEpo =  doctosPorEpo[i].split(",");
			String 	docto 	=	doctoEpo[0];
			String 	claveEpo =	doctoEpo[1];

			if(contains(doctosSeleccionados,docto)){
				ArrayList lista = (ArrayList) doctosAgrupados.get(claveEpo);
				if(lista == null){
					lista = new ArrayList();
					doctosAgrupados.put(claveEpo,lista);
				}
				lista.add(docto);
				listaClaves.add(claveEpo);
			}
		}

		Iterator itr = listaClaves.iterator();
		while(itr.hasNext()){
			listaClavesEpos.add(itr.next());
		}

		resultado.put("DOCTOS_AGRUPADOS",	doctosAgrupados);
		resultado.put("LISTA_CLAVES_EPOS",	listaClavesEpos);

		return resultado;
	}
	
	public boolean contains(String []array, String element){
		if(array == null || array.length == 0 ) return false;
		for(int i=0;i<array.length;i++){
			String tmp = array[i];
			if( tmp == null && element == null ){
				return true;
			}
			if(tmp != null && tmp.equals(element)){
				return true;
			}
		}
		return false;
	}
%>
<%=jsonObj%>