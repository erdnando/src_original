Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });

	var tipo_liq="";

	function getFechaMinima(plazoMin){
		var TDate = new Date();
		var diaMes = TDate.getDate();
		var AddDays = parseInt(plazoMin);
		diaMes = parseInt(diaMes)+ parseInt(AddDays);
		TDate.setDate(diaMes);
		MonthAux = TDate.getMonth();
		TDate.setMonth(MonthAux);
		var CurYear = TDate.getFullYear();
		var CurDay = TDate.getDate();
		var CurMonth = TDate.getMonth()+1;
		if(CurDay<10){
			CurDay = '0'+CurDay;
		}
		if(CurMonth<10){
			CurMonth = '0'+CurMonth;
		}
		TheDate = CurDay +'/'+CurMonth+'/'+CurYear;
		return TheDate;
	}

	function validaCampos() {
		var elementos = consultaData.getTotalCount();
		var flag = false;
		var valid = true;
		var msg="";
		var nRow;
		var nCol;
		if (elementos>0){
			var jsonData = consultaData.data.items;
			Ext.each(jsonData, function(item,index,arrItem){
				if(!Ext.isEmpty(item.data.ESTATUS_ASIGNA )){
					if(item.data.ESTATUS_ASIGNA == "3"){
						if(Ext.isEmpty(item.data.MONEDA)){
							msg="Elija la moneda que manejara su l�nea de credito";
							flag = true;
							nRow = index;
							nCol = grid.getColumnModel().findColumnIndex('MONEDA');
							return false;
						}
						if(Ext.isEmpty(item.data.MONTOSOLICITADO)){
							msg="Capture el monto solicitado";
							flag = true;
							nRow = index;
							nCol = grid.getColumnModel().findColumnIndex('MONTOSOLICITADO');
							return false;
						}
						if(Ext.isEmpty(item.data.MONTO_AUTO)){
							msg="Capture el monto autorizado";
							flag = true;
							nRow = index;
							nCol = grid.getColumnModel().findColumnIndex('MONTO_AUTO');
							return false;
						}
						if(Ext.isEmpty(item.data.FECHA_VTO)){
							msg="Capture la fecha de vencimiento";
							flag = true;
							nRow = index;
							nCol = grid.getColumnModel().findColumnIndex('FECHA_VTO');
							return false;
						}
						if(Ext.isEmpty(item.data.BANCO)){
							msg="Capture el banco de servicio";
							flag = true;
							nRow = index;
							nCol = grid.getColumnModel().findColumnIndex('BANCO');
							return false;
						}
						if(Ext.isEmpty(item.data.CUENTA)){
							msg="Capture la cuenta en la que se har�n los depositos";
							flag = true;
							nRow = index;
							nCol = grid.getColumnModel().findColumnIndex('CUENTA');
							return false;
						}
						if(Ext.isEmpty(item.data.TIPO_COBRO_INT)){
							msg="Capture el Tipo de cobro de Interes";
							flag = true;
							nRow = index;
							nCol = grid.getColumnModel().findColumnIndex('TIPO_COBRO_INT');
							return false;
						}
					}
					if(item.data.ESTATUS_ASIGNA == "4"){
						if(Ext.isEmpty(item.data.CAUSA_RECHAZO)){
							msg="Capture las causas del rechazo";
							flag = true;
							nRow = index;
							nCol = grid.getColumnModel().findColumnIndex('CAUSA_RECHAZO');
							return false;
						}
					}
					return true;
				}else{
					msg = "Cambie el estatus de la Linea de Cr�dito para poder continuar";
					flag = true;
					nRow = index;
					nCol = grid.getColumnModel().findColumnIndex('ESTATUS_ASIGNA');
					return false;
				}
			});
			if (flag){
				valid = false;
				Ext.Msg.alert("Mensaje de p�gina web",msg,
					function(){
						Ext.getCmp('btnTerminar').setIconClass('icoAceptar');
						var gridEl = Ext.getCmp('grid').getGridEl();
						var rowEl = Ext.getCmp('grid').getView().getCell( nRow, nCol);
						rowEl.scrollIntoView(gridEl,false);
						grid.startEditing(nRow, nCol);
						return;
					}
				);
			}
			return valid;
		}
	}

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(infoR.strPerfil == "IF FACT RECURSO"){
				Ext.getCmp('_ic_pyme').label.update('Nombre del Proveedor:');
			}
			var auxcad="";
			tipo_liq = infoR.tipo_liq;
			if (infoR.tipo_liq === "I"){
				auxcad = "IF - ";
			}else{
				if (infoR.tipo_liq === "A"){
					auxcad = "Distribuidor - ";
				}
			}
			var cm = grid.getColumnModel();
			cm.setColumnHeader(cm.findColumnIndex('BANCO'), auxcad+"Banco de servicio");
			cm.setColumnHeader(cm.findColumnIndex('CUENTA'), auxcad+"Cuenta");
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaConsulta(opts, success, response) {
		pnl.el.unmask();
		consultaData.loadData('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (!grid.isVisible()){
				grid.show();
			}
			if (infoR.registros != undefined && infoR.registros.length > 0){
				consultaData.loadData(infoR.registros);
				grid.getGridEl().unmask();
				Ext.getCmp('btnTerminar').enable();
			}else{
				grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnTerminar').disable();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdf');
			toolbar.setWidth(toolbar.getWidth()+90);
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			toolbar.setWidth(toolbar.getWidth()+90);
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarTerminar(opts, success, response) {
		pnl.el.unmask();
		Ext.getCmp('btnTerminar').setIconClass('icoAceptar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			fp.hide();
			Ext.Msg.alert("Mensaje de p�gina web",infoR.msg);
			fpCoins.show();
			Ext.getCmp('disAcuse').body.update(infoR._acuse);
			Ext.getCmp('disFechaCarga').body.update(infoR.fechaCarga);
			Ext.getCmp('disHoraCarga').body.update(infoR.horaCarga);
			Ext.getCmp('disUsuario').body.update('<div class="formas" >'+infoR.usuario+'<div>');
			Ext.getCmp('btnGenerarArchivo').setHandler( function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url : '24linea02csv_ext.jsp',
					params: Ext.apply(opts.params,{
									_acuse: infoR._acuse,
									fechaHoy:infoR.fechaCarga,
									horaActual:infoR.horaCarga,
									usuario:infoR.usuario
								}),
					callback: procesarSuccessFailureGenerarArchivo
				});
			});
			Ext.getCmp('btnGenerarPDF').setHandler( function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url : '24linea02pdf_ext.jsp',
					params: Ext.apply(opts.params,{
									_acuse: infoR._acuse,
									fechaHoy:infoR.fechaCarga,
									horaActual:infoR.horaCarga,
									usuario:infoR.usuario
								}),
					callback: procesarSuccessFailureGenerarPDF
				});
			});
			
			grid.hide();
			grid_b.show();
			Ext.getCmp('gridTotales').show();
			fpAcuse.show();
			toolbar.show();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesaEpoDistData = function(store, arrRegistros, opts) {
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione EPO',	loadMsg: null	})	);
			Ext.getCmp('_ic_epo').setValue("");
	}

	var catalogoEpoDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPODist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesaEpoDistData,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesaCatalogoPymeDistData = function(store, arrRegistros, opts) {
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "TODOS",	descripcion: 'Todas las PYMEs',	loadMsg: null	})	);
			Ext.getCmp('_ic_pyme').setValue("TODOS");
	}

	var catalogoPymeDistData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDistribuidor'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesaCatalogoPymeDistData,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesaCatalogoMoneda = function(store, arrRegistros, opts) {
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione Moneda',	loadMsg: null	})	);
	}

	var catalogoMoneda_mn = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoMonedaMn'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesaCatalogoMoneda,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoMoneda_dl = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoMonedaDl'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesaCatalogoMoneda,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoMoneda_todo = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoMonedaTodo'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesaCatalogoMoneda,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesaCatalogoEstatusAsignar = function(store, arrRegistros, opts) {
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione Estatus',	loadMsg: null	})	);
	}

	var catalogoEstatusAsignar = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEstatusAsignar'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	load:procesaCatalogoEstatusAsignar,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoTipoCobroData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTipoCobroDist'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoTipoCobroGral = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTipoCobroGral'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoBanco = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24linea02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoBanco'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var totalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO_AUTORIZADO'}
		],
		data:[{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_AUTORIZADO':''},
				{'NOMMONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_AUTORIZADO':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name:'FOLIOSOLICITUD'},
			{name:'NUMDIST'},
			{name:'NOMBREPYME'},
			{name:'PLAZA'},
			{name:'DIRECCION'},
			{name:'TELEFONO'},
			{name:'TIPOSOLICITUD'},
			{name:'ESTATUSACTUAL'},
			{name:'FECHAHOY'},
			{name:'IC_ESTATUSACTUAL'},
			{name:'TIPO'},
			{name:'BANCO'},
			{name:'CUENTA'},
			{name:'AUX_ICPYME'},
			{name:'AUX_ICEPO'},
			{name:'FOLIOARMADO'},
			{name:'LINEASIF'},
			{name:'PRODUCTONAFIN'},
			{name:'MONEDA'},
			{name:'IC_PRODUCTONAFIN'},
			{name:'FECHASOLICITUD'},
			{name:'IC_LINEACREDITOPADRE'},
			{name:'PLAZOMINIMO'},
			{name:'CAUSA_RECHAZO'},
			{name:'TIPO_COBRO_INT'},
			{name:'ESTATUS_ASIGNA'}
		],
		totalProperty : 'total',
		data:	[{	'FOLIOSOLICITUD':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var grid_b = new Ext.grid.GridPanel({
		id:'grid_b',store: consultaData,	columnLines:true, hidden:true,	view:new Ext.grid.GridView({markDirty: false}),
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
		columns:[
			{
				header:'Folio Solicitud',	tooltip:'Folio Solicitud',	dataIndex:'FOLIOSOLICITUD',	sortable:true,	resizable:true,	width:130,	align:'center',	menuDisabled:true
			},{
				header:'N�m. Distribuidor',	tooltip:'N�m. Distribuidor',	dataIndex:'NUMCLIENTE',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header: 'Nombre PYME',	tooltip: 'Nombre PYME',	dataIndex:'NOMBREPYME',	sortable:true,	resizable:true,	width:200,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header:'Tipo Solicitud',	tooltip:'Tipo Solicitud',	dataIndex:'TIPOSOLICITUD',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'N�m. L�nea relacionada',	tooltip:'N�m. L�nea relacionada',	dataIndex:'IC_LINEACREDITOPADRE',	sortable:true,	resizable:true,	width:120,	align:'center'
			},{
				header : 'Fecha Solicitud', tooltip: 'Fecha Solicitud',	dataIndex : 'FECHASOLICITUD',	sortable : true, width : 100, align: 'center'
			},{
				header:'Estatus asignado',	tooltip:'Estatus asignado',	dataIndex:'ESTATUS_ASIGNA',	sortable:true,	resizable:true,	width:130,	align:'center',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(!Ext.isEmpty(value)){
										var dato = catalogoEstatusAsignar.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoEstatusAsignar.getAt(dato);
											value = reg.get('descripcion');
										}
								}else{
									value = "";
								}
                        return value;
							}
			},{
				header : 'Causas de rechazo', tooltip: 'Causas de rechazo',	dataIndex : 'CAUSA_RECHAZO', fixed:true,	sortable : false,	width : 250,	hiddeable: false
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:true,	resizable:true,	width:130,	align:'center',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
							if(!Ext.isEmpty(value)){
								if (value == "1"){
									var dato = catalogoMoneda_mn.findExact("clave", value);
									if (dato != -1 ) {
										var reg = catalogoMoneda_mn.getAt(dato);
										value = reg.get('descripcion');
									}
								}else if (value =="54"){
									var dato = catalogoMoneda_dl.findExact("clave", value);
									if (dato != -1 ) {
										var reg = catalogoMoneda_dl.getAt(dato);
										value = reg.get('descripcion');
									}
								}else{
									var dato = catalogoMoneda_todo.findExact("clave", value);
									if (dato != -1 ) {
										var reg = catalogoMoneda_todo.getAt(dato);
										value = reg.get('descripcion');
									}
								}
							}else{
								value = "";
							}
							return value;
				}
			},{
				header:'Monto autorizado',	tooltip:'Monto autorizado',	dataIndex:'MONTO_AUTO',	sortable:false,	width:120,	align:'right',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return Ext.util.Format.number(value,'$ 0.00');
							}
			},{
				header:'Fecha vencimiento',	tooltip:'Fecha vencimiento',	dataIndex:'FECHA_VTO',	sortable:false,	width:120,	align:'center',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return Ext.util.Format.date(value,'d/m/Y');
				}
			},{
				header:'Tipo de cobro inter�s',	tooltip:'Tipo de cobro inter�s',	dataIndex:'TIPO_COBRO_INT',	sortable:true,	resizable:true,	width:130,	align:'center',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(!Ext.isEmpty(value)){
										var dato = catalogoTipoCobroGral.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoTipoCobroGral.getAt(dato);
											value = reg.get('descripcion');
										}
								}else{
									value = "";
								}
                        return value;
							}
			}
		]
	});

	var colModel = new Ext.grid.ColumnModel({
		columns:[
			{
				header:'Folio Solicitud',	tooltip:'Folio Solicitud',	dataIndex:'FOLIOSOLICITUD',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'N�m. Distribuidor',	tooltip:'N�m. Distribuidor',	dataIndex:'NUMCLIENTE',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header: 'Nombre PYME',	tooltip: 'Nombre PYME',	dataIndex:'NOMBREPYME',	sortable:true,	resizable:true,	width:200,	align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header : 'Estado', tooltip: 'Estado',	dataIndex : 'PLAZA',	sortable : true, width : 100, align: 'center'
			},{
				header:'Direcci�n',	tooltip:'Direcci�n',	dataIndex:'DIRECCION',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Tel�fono',	tooltip:'Tel�fono',	dataIndex:'TELEFONO',	sortable:true,	resizable:true,	width:100,	align:'center'
			},{
				header:'Tipo Solicitud',	tooltip:'Tipo Solicitud',	dataIndex:'TIPOSOLICITUD',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Folio de Solicitud relacionada',	tooltip:'Folio de Solicitud relacionada',	dataIndex:'IC_LINEACREDITOPADRE',	sortable:true,	resizable:true,	width:80,	align:'center'
			},{
				header:'Fecha Solicitud', tooltip:'Fecha Solicitud',	dataIndex : 'FECHASOLICITUD',	sortable : true, width : 100, align: 'center'
			},{
				header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	sortable:true,	resizable:true,	width:130,	align:'center',editor: new Ext.form.TextField({}),
				renderer:function(value,metadata,registro, rowIndex, colIndex){
							if(!Ext.isEmpty(value)){
								if (value == "1"){
									var dato = catalogoMoneda_mn.findExact("clave", value);
									if (dato != -1 ) {
										var reg = catalogoMoneda_mn.getAt(dato);
										value = reg.get('descripcion');
									}
								}else if (value =="54"){
									var dato = catalogoMoneda_dl.findExact("clave", value);
									if (dato != -1 ) {
										var reg = catalogoMoneda_dl.getAt(dato);
										value = reg.get('descripcion');
									}
								}else{
									var dato = catalogoMoneda_todo.findExact("clave", value);
									if (dato != -1 ) {
										var reg = catalogoMoneda_todo.getAt(dato);
										value = reg.get('descripcion');
									}
								}
							}else{
								value = "Seleccione Moneda";
							}
							return NE.util.colorCampoEdit(value,metadata,registro);
				}
			},{
				header:'Monto Solicitado', tooltip:'Monto Solicitado',	dataIndex:'MONTOSOLICITADO',	sortable:true, width:150, align:'right', hidden:false, hideable:false,
				editor: {xtype : 'numberfield', id: 0, name:'monto',	minValue:0, maxLength:15},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(Ext.util.Format.number(value,'0.00'),metadata,registro);
							}
			},{
				header:'Estatus Actual',	tooltip:'Estatus Actual',	dataIndex:'ESTATUSACTUAL',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Estatus por asignar',	tooltip:'Estatus por asignar',	dataIndex:'ESTATUS_ASIGNA',	sortable:true,	resizable:true,	width:130,	align:'center',
				editor:{	xtype:'combo',	id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection:true,	triggerAction:'all',
							fixed:true,	typeAhead: true,	minChars : 1,	emptyText:'Seleccione Estatus',	store:catalogoEstatusAsignar,
							listeners:{
										select:{ fn:function (comboField, record, index) {
															comboField.fireEvent('blur');
														}
												 }
							}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(!Ext.isEmpty(value)){
										var dato = catalogoEstatusAsignar.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoEstatusAsignar.getAt(dato);
											value = reg.get('descripcion');
										}
								}else{
									value = "Seleccione Estatus";
								}
                        return NE.util.colorCampoEdit(value,metadata,registro);
							}
			},{
				header : 'Causas de rechazo', tooltip: 'Causas de rechazo',	dataIndex : 'CAUSA_RECHAZO', fixed:true,	sortable : false,	width : 250,	hiddeable: false,
				editor:{
					xtype:	'textarea',
					id: 'causa_rechazo',
					maxLength: 254,
					enableKeyEvents: true,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 254){
								field.setValue((field.getValue()).substring(0,253));
								field.focus();
							}
						}
					}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			},{
				header:'Fecha de vto.',	tooltip:'Fecha de vto.',	dataIndex:'FECHA_VTO',	sortable:false,	width:120,	align:'center',
				editor:{xtype:'datefield',selectOnFocus:true},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);
				}
			},{
				header:'Tipo de cobro inter�s',	tooltip:'Tipo de cobro inter�s',	dataIndex:'TIPO_COBRO_INT',	sortable:true,	resizable:true,	width:130,	align:'center',
				editor:{	xtype:'combo',	id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection:true,	triggerAction:'all',
							fixed:true,	typeAhead: true,	minChars : 1,	store:catalogoTipoCobroData,
							listeners:{
										select:{ fn:function (comboField, record, index) {
															comboField.fireEvent('blur');
														}
												 }
							}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(!Ext.isEmpty(value)){
										var dato = catalogoTipoCobroData.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoTipoCobroData.getAt(dato);
											value = reg.get('descripcion');
										}
								}else{
									value = "Tipo Cobro de Interes";
								}
                        return NE.util.colorCampoEdit(value,metadata,registro);
							}
			},{
				header:'Monto autorizado',	tooltip:'Monto autorizado',	dataIndex:'MONTO_AUTO',	sortable:false,	width:120,	align:'right',
				editor: {xtype : 'numberfield', id: 0, name:'monto_auto',	minValue:0, maxLength:15},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(Ext.util.Format.number(value,'0.00'),metadata,registro);
							}
			},{
				header:'Banco de servicio',	tooltip:'Banco de servicio',	dataIndex:'BANCO',	sortable:true,	resizable:true,	width:250,	align:'center',
				editor:{},
				//xtype:'combo', id:0, store:catalogoBanco
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(registro.get('TIPO') == "I"){

									if(!Ext.isEmpty(value)){
										var dato = catalogoBanco.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoBanco.getAt(dato);
											value = reg.get('descripcion');
										}
									}else{
											value = "Seleccione banco";
									}
								
								}else{
									if(!Ext.isEmpty(value)){
										var dato = catalogoBanco.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoBanco.getAt(dato);
											value = reg.get('clave') + " - "+ reg.get('descripcion');
										}
									}
								}
                        return NE.util.colorCampoEdit(value,metadata,registro);
							}
			},{
				header:'Cuenta',	tooltip:'Cuenta',	dataIndex:'CUENTA',	sortable:true,	resizable:true,	width:130,	align:'center',
				editor:{xtype:'hidden'},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
							}
			}
		],
		editors: {
			'mn': new Ext.grid.GridEditor(new Ext.form.ComboBox({
						id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection:true,	triggerAction:'all',fixed:true,
						typeAhead: true,	minChars : 1,	emptyText:'Seleccione Moneda',	store:catalogoMoneda_mn, editable:true,
						listeners:{ //focus:{ fn: function (comboField) { comboField.doQuery(comboField.allQuery, true); comboField.expand(); }},
										select:{ fn:function (comboField, record, index) {
															comboField.fireEvent('blur'); //Ext.getCmp('grid').getView().refresh();
														}
												 }
						}
					})),
			'dl': new Ext.grid.GridEditor(new Ext.form.ComboBox({
						id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection:true,	triggerAction:'all',fixed:true,
						typeAhead: true,	minChars : 1,	emptyText:'Seleccione Moneda',	store:catalogoMoneda_dl,	editable:true,
						listeners:{ //focus:{ fn: function (comboField) { comboField.doQuery(comboField.allQuery, true); comboField.expand(); }},
										select:{ fn:function (comboField, record, index) {
															comboField.fireEvent('blur'); //Ext.getCmp('grid').getView().refresh();
														}
												 }
						}
					})),
			'todo': new Ext.grid.GridEditor(new Ext.form.ComboBox({
						id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection : true,	triggerAction : 'all',fixed:true,
						typeAhead: true,	minChars : 1,	emptyText:'Seleccione Moneda',	store:catalogoMoneda_todo,	editable:true,
						listeners:{ //focus:{ fn: function (comboField) { comboField.doQuery(comboField.allQuery, true); comboField.expand(); }},
										select:{ fn:function (comboField, record, index) {
															comboField.fireEvent('blur'); //Ext.getCmp('grid').getView().refresh();
														}
												 }
						}
					})),
			'banco_combo':new Ext.grid.GridEditor(new Ext.form.ComboBox({
						id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection : true,	triggerAction : 'all',fixed:true,
						typeAhead: true,	minChars : 1,	emptyText:'Seleccione banco',	store:catalogoBanco,	editable:true,
						listeners:{
										select:{ fn:function (comboField, record, index) {
															comboField.fireEvent('blur');
														}
												 }
						}
					})),
			'banco_text':new Ext.grid.GridEditor(new Ext.form.TextField({
						id: 0, name:'banco'
					})),
			'cuenta_text':new Ext.grid.GridEditor(new Ext.form.TextField({
						id: 0,	name:'cuenta'
					})),
			'cuenta_hidden':new Ext.grid.GridEditor(new Ext.form.Hidden({
						id: 0,	name:'cuenta'
					}))
		},
		getCellEditor: function(colIndex, rowIndex) {
			var field = this.getDataIndex(colIndex);
			if (field == 'MONEDA') {
				var clave = grid.store.reader.jsonData[rowIndex].MONEDA;
				if (clave == "1"){
					return this.editors['mn'];
				}else if (clave =="54"){
					return this.editors['dl'];
				}else{
					return this.editors['todo'];
				}
			}else if (field == 'BANCO') {
				var tipo = grid.store.reader.jsonData[rowIndex].TIPO;
				if(tipo == "I"){
					return this.editors['banco_combo'];
				}else{
					return this.editors['banco_text'];
				}
			}else if (field == 'CUENTA') {
				var tipo = grid.store.reader.jsonData[rowIndex].TIPO;
				if(tipo == "I"){
					return this.editors['cuenta_text'];
				}else{
					return this.editors['cuenta_hidden'];
				}				
			}
			return Ext.grid.ColumnModel.prototype.getCellEditor.call(this, colIndex, rowIndex);
		}
	});

	var gridTotales = {
		xtype:'grid',	store:totalesData,	id:'gridTotales',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:940,	height:80,	hidden:true,	frame: false,//title:'Totales', 
		columns: [
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 400,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),menuDisabled:true
			},{
				header: 'Monto Autorizado',	dataIndex: 'TOTAL_MONTO_AUTORIZADO',	width:400,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		]/*,
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]*/
	};

	var grid = new Ext.grid.EditorGridPanel({
		id:'grid',store: consultaData,	columnLines:true, clicksToEdit:1, hidden:true,
		stripeRows:true,	loadMask:true,	height:400,	width:940,	style:'margin:0 auto;',	frame:false,
		cm: colModel,
		listeners:{
			beforeedit:{
				fn: function(obj) {
					if (obj.field == 'TIPO_COBRO_INT') {
						catalogoTipoCobroData.removeAll();
						catalogoTipoCobroData.reload({	params: {aux_icepo: obj.record.data['AUX_ICEPO'] }	});
					}
				}
			},
			validateedit: {
				fn: function(obj) {
						if (obj.field == 'FECHA_VTO') {
							var plazoMinimo = parseInt(obj.record.data['PLAZOMINIMO']);
							var fechaMinima = getFechaMinima(plazoMinimo);
							if(datecomp(fechaMinima,Ext.util.Format.date(obj.value,'d/m/Y'))==1){
								obj.cancel = true;
								Ext.Msg.alert("Mensaje de p�gina web"," La fecha de vencimiento debe ser mayor a "+fechaMinima,
												function(){
													Ext.getCmp('btnTerminar').setIconClass('icoAceptar');
													var gridEl = Ext.getCmp('grid').getGridEl();
													var rowEl = Ext.getCmp('grid').getView().getCell( obj.row, obj.column);
													rowEl.scrollIntoView(gridEl,false);
													grid.startEditing(obj.row, obj.column);
													return;
												});
								return;
							}
						}
				}
			}
		},
		bbar: {
			items: [
				'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
				{
					text:'Terminar captura', id:'btnTerminar',	iconCls:'icoAceptar',
					handler: function(boton){
									boton.setIconClass('loading-indicator');
									if(validaCampos()) {
										var sumMontoMN=0;
										var sumMontoDL=0;
										var countMN = 0;
										var countDL = 0;
										consultaData.each(function(registro){
											if (registro.get('MONEDA') === "1"){
												countMN++;
												sumMontoMN += parseFloat(registro.get('MONTO_AUTO'));
											}else if(registro.get('MONEDA') === "54"){
												countDL++;
												sumMontoDL += parseFloat(registro.get('MONTO_AUTO'));
											}
										});
										if (countMN > 0){
											var regMN = totalesData.getAt(0);
											regMN.set('NOMMONEDA','Moneda Nacional');
											regMN.set('TOTAL_REGISTROS',countMN);
											regMN.set('TOTAL_MONTO_AUTORIZADO',sumMontoMN);
							
											if(countDL > 0){
												var regDL = totalesData.getAt(1);
												regDL.set('NOMMONEDA','Total Dolares');
												regDL.set('TOTAL_REGISTROS',countDL);
												regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
											}
										}else if(countDL > 0){
											var regDL = totalesData.getAt(0);
											regDL.set('NOMMONEDA','Total Dolares');
											regDL.set('TOTAL_REGISTROS',countDL);
											regDL.set('TOTAL_MONTO_AUTORIZADO',sumMontoDL);
										}
										Ext.getCmp('totMn').body.update('<div align="left">'+countMN+'</div>');
										Ext.getCmp('montoMn').body.update('<div align="left">'+Ext.util.Format.number(sumMontoMN, '$ 0,0.00')+'</div>');
										Ext.getCmp('totDl').body.update('<div align="left">'+countDL+'</div>');
										Ext.getCmp('montoDl').body.update('<div align="left">'+Ext.util.Format.number(sumMontoDL, '$ 0,0.00')+'</div>');

										var jsonData = consultaData.data.items;
										var aFolio		= [];
										var aFolioArm	= [];
										var aNumCliente= [];
										var aNomCliente= [];
										var aFolioRel	= [];
										var aFechaSol	= [];
										var aMoneda		= [];
										var aMonto		= [];
										var aEstatus	= [];
										var aTipoCobro	= [];
										var aCausa		= [];
										var aMontoAuto	= [];
										var aFechaVto	= [];
										var aBanco		= [];
										var aCuenta		= [];
										var aTipo		= [];
										var atipoSolicitud = [];
										Ext.each(jsonData, function(item,index,arrItem){
											aFolio.push(item.data.FOLIOSOLICITUD);
											aFolioArm.push(item.data.FOLIOARMADO);
											aNumCliente.push(item.data.NUMCLIENTE);
											aNomCliente.push(item.data.NOMBREPYME);
											aFolioRel.push(item.data.IC_LINEACREDITOPADRE);
											aFechaSol.push(item.data.FECHASOLICITUD);
											aMoneda.push(item.data.MONEDA);
											aMonto.push(item.data.MONTOSOLICITADO);
											aEstatus.push(item.data.ESTATUS_ASIGNA);
											aTipoCobro.push(item.data.TIPO_COBRO_INT);
											aCausa.push(item.data.CAUSA_RECHAZO);
											aMontoAuto.push(item.data.MONTO_AUTO);
											aFechaVto.push(Ext.util.Format.date(item.data.FECHA_VTO,'d/m/Y'));
											aBanco.push(item.data.BANCO);
											aCuenta.push(item.data.CUENTA);
											aTipo.push(item.data.TIPO);
											atipoSolicitud.push(item.data.TIPOSOLICITUD);
										});

										pnl.el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: '24linea02ext.data.jsp',
											params:	{	informacion: "Termina",
															totDocs:			countMN,
															totMtoAuto:		sumMontoMN,
															totDocsDol:		countDL,
															totMtoAutoDol:	sumMontoDL,
															folio:aFolio,
															rs_folioArmado:aFolioArm,
															rs_numCliente:aNumCliente,
															rs_nomPyme:aNomCliente,
															rs_folioRel:aFolioRel,
															rs_fechaSol:aFechaSol,
															moneda:aMoneda,
															monto:aMonto,
															estatus:aEstatus,
															tipo_cobro_int:aTipoCobro,
															causa_rechazo:aCausa,
															monto_auto:aMontoAuto,
															fecha_vto:aFechaVto,
															banco:aBanco,
															cuenta:aCuenta,
															tipo:aTipo,
															tipoSolicitud:atipoSolicitud,
															tipo_liq:tipo_liq
														},
											callback: procesarTerminar
										});
								}
					}
				},{
					xtype: 'tbspacer', width: 5
				}
			]
		}
	});

	var toolbar = new Ext.Toolbar({
		id:'barBotones',
		style: 'margin:0 auto;',
		autoScroll : true,
		hidden:false,
		width: 310,
		height: 55,
		 items: [
			'->','-',
			{
				text:'Generar Archivo',	id:'btnGenerarArchivo',	iconCls:'icoXls', scale:'medium'
			},{
				text:'Bajar Archivo',	id:'btnBajarArchivo',	hidden:true, scale:'medium'
			},'-',{
				xtype: 'tbspacer', width: 2
			},{
				text:'Generar Pdf',	id:'btnGenerarPDF',	iconCls:'icoPdf',scale:'medium'
			},{
				text:'Bajar Pdf',	id:'btnBajarPdf',	hidden:true, scale:'medium'
			},'-',{
				xtype: 'tbspacer', width: 2
			},{
				text:'Salir',id:'btnSalir',iconCls:'icoLimpiar',scale:'medium', handler: function() {location.reload();}
			},'-',{
				xtype: 'tbspacer', width: 2
			}
		]
	});
	toolbar.hide();
	var elementosCoins = [
		{
			xtype: 'panel',layout:'table',	width:820,	border:true,	layoutConfig:{ columns: 4 },
			defaults: {frame:false, border:true,	height: 35,bodyStyle:'padding:8px'},
			items:[
				{	width:410,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Moneda nacional</div>'	},
				{	width:410,	colspan:2,	border:false,	frame:true,	html:'<div align="center">D�lares</div>'	},
				{	width:210,	border:false,	frame:true,	html:'<div class="formas" align="center">N�mero de solicitudes procesadas</div>'	},
				{	width:200,	border:false,	frame:true,	html:'<div class="formas" align="center">Monto de solicitudes procesadas</div>'	},
				{	width:210,	border:false,	frame:true,	html:'<div class="formas" align="center">N�mero de solicitudes procesadas</div>'	},
				{	width:200,	border:false,	frame:true,	html:'<div class="formas" align="center">Monto de solicitudes procesadas</div>'	},
				{	width:210,	id:'totMn',		html:'&nbsp;'	},
				{	width:200,	id:'montoMn',	html:'&nbsp;'	},
				{	width:210,	id:'totDl',		html:'&nbsp;'	},
				{	width:200,	id:'montoDl',	html:'&nbsp;'	},
				{	height: 25,	colspan:4,	width:820,	html:'<div class="formas" align="center">Leyenda legal</div>'	}
			]
		}
	];

	var fpCoins=new Ext.Panel({	style: 'margin:0 auto;',	height:'auto',	width:820,	border:true,	frame:false,	items:elementosCoins,	hidden:true 	});

	var elementosAcuse = [
		{
			xtype: 'panel',layout:'table',	width:699,	border:false,	layoutConfig:{ columns: 2 },	style: 'margin:0 auto;',
			defaults: {frame:false, border: true,width:170	, height: 35,	bodyStyle:'padding:6px'},
			items:[
				{	html:'<div class="formas" align="left">N�m. acuse</div>'	},
				{	width:535,id:'disAcuse',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Fecha</div>'	},
				{	width:535,id:'disFechaCarga',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Hora</div>'	},
				{	width:535,id:'disHoraCarga',	html:'&nbsp;'},
				{	html:'<div class="formas" align="left">Nombre y n�mero de usuario</div>'	},
				{	width:535,id:'disUsuario',html: '&nbsp;'	}
			]
		}
	];

	var fpAcuse=new Ext.Panel({	style: 'margin:0 auto;',	height:'auto',	width:700,	border:true,	frame:false,	items:elementosAcuse,	hidden:true 	});

	var elementosForma = [
		{
			xtype: 'textfield',	name: 'ic_linea',	id: 	'ic_linea',	fieldLabel: 'Folio de solicitud',	maxLength: 23,	anchor:'55%'
		},{
			xtype: 'combo',
			id:	'_ic_epo',
			name: 'ic_epo',
			hiddenName : 'ic_epo',
			fieldLabel: 'Nombre de la EPO',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEpoDistData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:function(combo){
							var comboPyme = Ext.getCmp('_ic_pyme');
							comboPyme.setValue("");
							comboPyme.store.removeAll();
							comboPyme.store.reload({	params: {ic_epo: combo.getValue() }	});
				}
			}
		},{
			xtype: 'combo',
			id:	'_ic_pyme',
			name: 'ic_pyme',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Nombre del Distribuidor',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoPymeDistData,
			tpl:NE.util.templateMensajeCargaCombo
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title:'<div align="center">Criterios de B&uacute;squeda</div>',
		width: 600,
		frame: true,
		collapsible: true,
		titleCollapse: true,
		labelWidth:140,
		labelAlign:'right',
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Agregar Lineas',	id: 'btnAgregar',	iconCls: 'aceptar',	formBind:true,
				handler: function(boton, evento) {	window.location = '24linea04ext.jsp';	}
			},{
				text: 'Consultar',	id: 'btnConsultar',	iconCls: 'icoLupa',	formBind:true,
				handler: function(boton, evento) {
								catalogoMoneda_mn.load();
								catalogoMoneda_dl.load();
								catalogoMoneda_todo.load();
								catalogoEstatusAsignar.load();
								catalogoTipoCobroGral.load();
								catalogoBanco.load();
								Ext.getCmp('btnTerminar').setIconClass('icoAceptar');
								grid.hide();
								pnl.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '24linea02ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{informacion: "Consulta"}),
									callback: procesaConsulta
								});
							}
			},{
				text: 'Cancelar',	id:'btnRechazar',	iconCls:'icoRechazar',	handler: function() {location.reload();}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,	fpCoins,	NE.util.getEspaciador(10),
			grid,	grid_b,	gridTotales,	NE.util.getEspaciador(10),	fpAcuse,
			NE.util.getEspaciador(10),	toolbar
		]
	});

	catalogoEpoDistData.load();

	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '24linea02ext.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});

});