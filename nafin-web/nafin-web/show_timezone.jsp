<%@ 
    page errorPage = "/00utils/error.jsp" 
    import = "
        java.text.SimpleDateFormat,
        java.util.Calendar,
        java.util.concurrent.ThreadLocalRandom,
        java.util.concurrent.TimeUnit,
        java.util.Date,
        java.util.TimeZone,
        org.apache.commons.logging.Log,
        netropology.utilerias.Fecha,
        netropology.utilerias.ServiceLocator
        "%>
<html>
<head>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252"/>
<title>TimeZone</title>
</head>
<body>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>
<%
try {
    String fec_actual = Fecha.getFechaActual();
    String hora_actual = Fecha.getHoraActual("HH24:MI");
    log.debug(fec_actual+" "+hora_actual);
    
    // Instantiate a Date object
    Date date = new Date();
    // display time and date using toString()
    log.debug(date.toString());
    
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
    TimeZone tz1 = cal.getTimeZone();
    log.debug("TimeZone:" + tz1.getDisplayName());
    dateFormat.setTimeZone(cal.getTimeZone());
    log.debug(dateFormat.format(cal.getTime())+" "+hourFormat.format(cal.getTime()));
    
    //System.out.println(cal);
    long milliDiff = cal.get(Calendar.ZONE_OFFSET);
    log.debug(milliDiff);
    
    // Got local offset, now loop through available timezone id(s).
    String [] ids = TimeZone.getAvailableIDs();
    String name = null;
    for (String id : ids) {
        TimeZone tz = TimeZone.getTimeZone(id);
        //System.out.println(id);
        if (tz.getRawOffset() == milliDiff) {
            // Found a match.
            name = id;
            break;
        }
    }
    log.debug(name+"<br>");
    

    String[] idsTZ = TimeZone.getAvailableIDs();
    for (String id : ids) {
        log.debug(displayTimeZone(TimeZone.getTimeZone(id)));
    }    
    log.debug("Total TimeZone ID " + ids.length);
    
} catch (Exception e) {
    e.printStackTrace();
} finally {
    
}
%>
<%!
private static String displayTimeZone(TimeZone tz) {

    long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
    long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset()) 
    - TimeUnit.HOURS.toMinutes(hours);
    // avoid -4:-30 issue
    minutes = Math.abs(minutes);
    String result = "";
    if (hours > 0) {
        result = String.format("(GMT+%d:%02d) %s", hours, minutes, tz.getID());
    } else {
        result = String.format("(GMT%d:%02d) %s", hours, minutes, tz.getID());
    }
    return result;
}
%>