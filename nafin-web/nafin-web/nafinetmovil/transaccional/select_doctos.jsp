<jsp:include page="sesion.jsp"/>
<%@ page import="com.nafin.nafinetmovil.utils.UsuarioBasico"%>
<%@ page import="com.nafin.nafinetmovil.utils.Constantes"%>
<%@ page
	import="
		java.lang.*,
		java.util.*,
		javax.naming.*,
		com.netro.descuento.movil.*,
		com.nafin.nafinetmovil.utils.*,
		javax.naming.Context"
	errorPage="error.jsp"%>
<%
String ua = request.getHeader( "User-Agent" );
boolean isBlackberry = ( ua != null && ua.indexOf( "Blackberry" ) != -1 );
boolean isIPad = ( ua != null && ua.indexOf( "iPad" ) != -1 );
boolean isIPhone = ( ua != null && (ua.indexOf( "iPhone" ) != -1 || ua.indexOf( "iPod" ) != -1) );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
boolean isChrome = ( ua != null && ua.indexOf( "Chrome" ) != -1 );
%>

<%
HashMap    respuesta  = null;
JSONObject object 	  = null;


UsuarioBasico usrBasico=(UsuarioBasico)session.getAttribute("user");
String strCboIF=((String)(request.getAttribute("cboIF"))==null?"0":(String)(request.getAttribute("cboIF")));
//out.println("strCboIF: "+strCboIF);
String strPyme=(String)(request.getAttribute("txtPyme"));
//out.println("txtPyme: "+strPyme);
String strEPO=(String)(request.getAttribute("txtEPO"));
//out.println("txtEPO: "+strEPO);
String strMoneda=(String)(request.getAttribute("cboMoneda"));
//out.println("strMoneda: "+strMoneda);

String    strMensaje = "";
Boolean   bHayError = new Boolean(false);
Boolean   bHayMensaje = new Boolean(false);
ArrayList aListaIF = null;
HashMap   hIF  = null;

Boolean   bHayMensajeSelec = new Boolean(false);
ArrayList aMensajeSelec = null;
ArrayList aListaDoctosXPlazo = null;
HashMap   hDoctosXPlazo  = null;
String    strFechaSigDiaHabil = "";

try {
  SeleccionMovilDoctos seleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionMovilDoctosEJB", SeleccionMovilDoctos.class);

  // Pruebas Modulo 2
  //out.print( "<span style=\"color:green;\" ><b>Pruebas Modulo 2</b></span><br>");
  respuesta 	= seleccionDocumento.getListaIntermediariosFinancieros(strEPO,strPyme, strMoneda, "");
  strMensaje = (String)respuesta.get("mensaje");
  bHayError = new Boolean((String)respuesta.get("hayError"));
  bHayMensaje = new Boolean((String)respuesta.get("hayMensaje"));
  aListaIF = (ArrayList)respuesta.get("listaIntermediarios");

  //object 		= JSONObject.fromObject(respuesta);

  // Pruebas Modulo 3
  for(int i=0;i<aListaIF.size();i++){
    hIF=(HashMap)aListaIF.get(i);
    //out.print( "<span style=\"color:green;\" ><b>Pruebas Modulo 3</b></span><br>");
    respuesta 	= seleccionDocumento.getDocumentosNegociablesAgrupadosPorTasa(strEPO,(String)hIF.get("claveIF"), strPyme, strMoneda);
    bHayMensajeSelec = new Boolean((String)respuesta.get("hayMensajeSeleccion"));
    aMensajeSelec = (ArrayList)respuesta.get("mensajeSeleccion");
    strFechaSigDiaHabil = (String)respuesta.get("fechaSiguienteDiaHabil");
   //object 		= JSONObject.fromObject(respuesta);
    //out.print( 	object.toString() +"<br>");
    //out.print( "<br>" );
  }

%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
  "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head profile="http://www.w3.org/2005/10/profile">
  <title><%=Constantes.SITE_TITLE%></title>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <meta name="HandheldFriendly" content="true" />
  <meta name="Description" content="bienvenido a nacional financiera portatil" />
  <meta name="Keywords" content="nafinet,movil,cadenas productivas,nacional financiera banca de desarrollo" />
  <meta name="Copyright" content="Nacional Financiera SNC" />
  <meta name="distribution" content="Global" />
  <meta name="Robots" content="index,follow" />
  <meta name="city" content="Mexico City" />
  <meta name="country" content="Mexico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="NO-CACHE" />
	<meta http-equiv="Expires" content="0" />
	<noscript><meta http-equiv="refresh" content="0; URL=noscript.jsp" /></noscript>
	<link href="css/general<%=(isIPad?"_ipad":"")%>.css" rel="stylesheet" type="text/css" />
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>

<script type="text/javascript">
<!--
function selectIF(){
	var frmCboIF=document.forms["frmDoctos"]["cboIF"].value
	document.forms["frmDoctos"].action="SelectDoctosS";
	document.frmDoctos.submit();
}

function envia(strXHTML){
	document.forms["frmDoctos"].action=strXHTML;
	document.frmEPO.submit();
}

function updateOrientation() {
  if (window.orientation == 90 || window.orientation == -90) {
    document.getElementById('header').src="images/header480.jpg";
    document.getElementById('header').width=screen.height;
  }
  else {
    document.getElementById('header').width=screen.width;
  }
}
// -->
</script>

<body onLoad="backButtonOverride()" onorientationchange="updateOrientation()">
<form method="post" action="PreAcuseS" name="frmDoctos">
<input name="txtEPO" value="<%=strEPO%>" type="hidden"></input>
<input name="txtPyme" value="<%=strPyme%>" type="hidden"></input>
<input name="cboMoneda" value="<%=strMoneda%>" type="hidden"></input>
<input name="txtFechaSigDia" type="hidden" value="<%=strFechaSigDiaHabil%>"></input>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EEEEEE">
	<tr>
		<td>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TablaBlanca">
				<tr>
					<td align="center"><img id="header" src="<%=((isIPad || isFirefox || isMSIE || isChrome)?Constantes.LOGO_NAFINET_IPAD:Constantes.LOGO_NAFINET)%>" "Nafinet M&oacute;vil"/></td>
				</tr>
				<tr>
					<td align="left">
						<table width="100%" border="0">
							<tr>
								<td align="left">
									<table width="98%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="left" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="identificacion">
													<tr>
														<td>Usuario: <%=usrBasico.getNombre()+" "+usrBasico.getApellidoPaterno()+" "+usrBasico.getApellidoMaterno()%><span>&nbsp;</span> </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="right" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="breadcrumbR">
													<tr>
														<td>INICIO/SALDO/<span class="breadcrumbS">SELECCI&Oacute;N DE DOCUMENTOS</span></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table width="50" border="0" class="tablaMarca">
										<tr>
											<!--td align="center"><img src="http://cadenas.nafin.com.mx/nafin/00archivos/15cadenas/15archcadenas/logos/<%=strEPO%>.gif" alt="" height="40" width="140"/></td-->
											<td align="center"><img src="images/epoch.jpg" alt=""/></td>
										</tr>
									</table>
								</td>
							</tr>
 <%
    if(!bHayMensaje.booleanValue()){
%>
							<tr>
								<td align="center">
							        <select name="cboIF" class="submit" onchange="selectIF();">
							          <option value="0"<%=(("0".equals(strCboIF))?" selected":"")%>>Selecciona Intermediario Financiero</option>
<%
           for(int i=0;i<aListaIF.size();i++){
             hIF=(HashMap)aListaIF.get(i);
%>
							          <option value="<%=(String)hIF.get("claveIF")%>"<%=((((String)hIF.get("claveIF")).equals(strCboIF))?" selected":"")%>><%=(String)hIF.get("nombreIF")%></option>
<%
          }
%>
							        </select>
									<br />
								</td>
							</tr>
							<tr>
								<td align="center">
									<table width="100%" border="1" cellpadding="1" cellspacing="1">
<%
         if(!bHayMensajeSelec.booleanValue() || (bHayMensajeSelec.booleanValue() && !"".equals(strFechaSigDiaHabil))){
          for(int i=0;i<aListaIF.size();i++){
            hIF=(HashMap)aListaIF.get(i);
            respuesta 	= seleccionDocumento.getDocumentosNegociablesAgrupadosPorTasa(strEPO,(String)hIF.get("claveIF"), strPyme, strMoneda);
            aListaDoctosXPlazo = (ArrayList)respuesta.get("listaResumenDoctos");
%>
										<tr align="center" class="menu">
											<td colspan="6"><%=(String)hIF.get("nombreIF")%></td>
										</tr>
<%
            if(i==0){
                  if(bHayMensajeSelec.booleanValue() && !"".equals(strFechaSigDiaHabil)){
%>
                    <%for(int j=0;j<aMensajeSelec.size();j++){%>
										<tr align="center">
											<td colspan="6">NOTA:<%=aMensajeSelec.get(j)%>. </td>
										</tr>
										<%}%>
<%
                  }
%>
										<tr align="center" class="subtitulo">
											<td>Plazo d&iacute;as</td>
											<td>Total Doctos.</td>
											<td>Importe</td>
											<td>Importe a Recibir</td>
											<td>Tasa (%)</td>
											<td>Seleccionar</td>
										</tr>
<%
            }
            for(int j=0;j<aListaDoctosXPlazo.size();j++){
              hDoctosXPlazo = (HashMap)aListaDoctosXPlazo.get(j);
%>
										<tr align="center">
											<td><%=(String)hDoctosXPlazo.get("plazo")%></td>
											<td><%=(String)hDoctosXPlazo.get("totalDoctosFormateado")%></td>
											<td><%=(String)hDoctosXPlazo.get("importeFormateado")%></td>
											<td><%=(String)hDoctosXPlazo.get("importeRecibirFormateado")%></td>
											<td><%=(String)hDoctosXPlazo.get("tasa")%></td>
											<td><%if(!"0".equals(strCboIF)){%><input name="chkP" value="<%=(String)hDoctosXPlazo.get("submitString")%>" type="checkbox" onclick="opera(this,<%=(String)hDoctosXPlazo.get("importe")%>,<%=(String)hDoctosXPlazo.get("importeRecibir")%>)"></input><%}else{%>&nbsp;<%}%></td>
										</tr>
<%
          }
        }
      }
      else{
%>
										<tr>
											<td align="center" class="msj">
											<%for(int i=0;i<aMensajeSelec.size();i++){%>
												<%=aMensajeSelec%>
											<%}%>
											</td>
										</tr>
<%
      }
%>
									</table>
								</td>
							</tr>
							<tr align="center">
								<td>
									<table>
										<tr>
											<td><input type="submit" value="Regresar" class="button" onclick="envia('menu.xhtml')"/></input></td>
											<%if(!"0".equals(strCboIF)){%><td><input id="btnContinuar" type="submit" value="Continuar" class="button" onclick="return valida();"/></input></td><%}%>
											<td><input type="submit" value="  Salir  " class="button" onclick="envia('logout.xhtml')"/></input></td>
										</tr>
									</table>
								</td>
							</tr>
<%
    }else{
%>
							<tr>
								<td align="center" class="msj"><%=strMensaje%></td>
							</tr>
							<tr>
								<td align="center">
									<table>
										<tr>
											<td><input type="submit" value=" Regresar " class="button" onclick="envia('menu.xhtml')"/></input></td>
											<td><input type="submit" value="  Salir  " class="button" onclick="envia('logout.xhtml')"/></input></td>
										</tr>
									</table>
								</td>
							</tr>
<%
    }
%>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">
						<br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="right" class="texto1footer">Enlaces del Gobierno Federal</td>
                <td width="50" rowspan="2"><img src="images/footer_r1_c2.gif" alt="" width="49" height="43" /></td>
              </tr>
              <tr>
                <td class="texto1footer2"><%=Constantes.FOOTER%></td>
              </tr>
            </table>
            <br/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
<!--
var importe = 0;
var importeRecibir = 0;

function opera(objChk,fImporte,fImporteRecibir){
  if(objChk.checked){
	importe=importe+parseFloat(fImporte);
	importeRecibir=importeRecibir+parseFloat(fImporteRecibir);
	//alert("Suma Importe: "+fImporte+" ImpRec: "+fImporteRecibir);
	//alert("Acumulado: "+importe+" Imp: "+importeRecibir);
  }
  else{
	importe=importe-parseFloat(fImporte);
	importeRecibir=importeRecibir-parseFloat(fImporteRecibir);
	//alert("Resta Importe: "+fImporte+" ImpRec: "+fImporteRecibir);
	//alert("Acumulado: "+importe+" Imp: "+importeRecibir);
  }
}

function valida(){
  var formulario = document.forms["frmDoctos"];
  var bCheck = false;

  for(var i=0; i<formulario.elements.length; i++) {
    var elemento = formulario.elements[i];
    if(elemento.type == "checkbox") {
      if(elemento.checked)
        bCheck = true;
    }
  }
  if(!bCheck){
    alert("Debes seleccionar documentos para continuar");
	return false;
  }
  else{
	if(confirm("Importe: $"+formatoFlotante(importe,'aplicar')+"\nImporte a recibir: $"+formatoFlotante(importeRecibir,'aplicar'))){
	  formulario.action="PreAcuseS";
    document.getElementById('btnContinuar').disabled=true;
    formulario.submit();
	  return true;
	}
	else
	  return false;
  }
}

function formatoFlotante(num,operacion){
  if(operacion=="aplicar"){
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	  num = "0";
	cents = Math.floor((num*100+0.5)%100);
	num = Math.floor((num*100+0.5)/100).toString();
	if(cents < 10)
	  cents = "0" + cents;
	for(var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	  num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));

	return (num + "." + cents);
  }
  else{ //operacion="quitar" --> Quitar el formato
	num = num.toString().replace(/\$|\,/g,'');

	return num;
  }
}
function backButtonOverride(){
  setTimeout("backButtonOverrideBody()", 1);

}

function backButtonOverrideBody(){
  try {
  	history.forward();
  } catch (e) {
	// OK to ignore
  }
  setTimeout("backButtonOverrideBody()", 500);
}

if (window.orientation == 90 || window.orientation == -90) {
  document.getElementById('header').src="images/header480.jpg";
  document.getElementById('header').width=screen.height;
}
else {
  document.getElementById('header').width=screen.width;
}
// -->
</script>

<%
}catch(Exception e){
	e.printStackTrace();
}
%>

</body>
</html>