<jsp:include page="sesion.jsp"/>
<%@ page import="com.nafin.nafinetmovil.utils.UsuarioBasico"%>
<%@ page import="com.nafin.nafinetmovil.utils.Constantes"%>
<%@ page 
	import="
		java.lang.*, 
		java.util.*, 
		com.nafin.nafinetmovil.utils.*,
		com.netro.descuento.movil.*" 
	errorPage="error.jsp"%>

<%
String ua = request.getHeader( "User-Agent" );
boolean isBlackberry = ( ua != null && ua.indexOf( "Blackberry" ) != -1 );
boolean isIPad = ( ua != null && ua.indexOf( "iPad" ) != -1 );
boolean isIPhone = ( ua != null && (ua.indexOf( "iPhone" ) != -1 || ua.indexOf( "iPod" ) != -1) );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
boolean isChrome = ( ua != null && ua.indexOf( "Chrome" ) != -1 );
%>

<%
UsuarioBasico usrBasico=(UsuarioBasico)session.getAttribute("user");
String strFechaDe=(request.getParameter("txtFechaDe")==null?"":request.getParameter("txtFechaDe"));
String strFechaA=(request.getParameter("txtFechaA")==null?"":request.getParameter("txtFechaA"));
String strConsulta=(request.getParameter("txtConsulta")==null?"":request.getParameter("txtConsulta"));
String strCveIF=usrBasico.getCveAfiliado();

HashMap    respuesta  = null;
//JSONObject object 	  = null;
HashMap    respuestaUSD  = null;
//JSONObject objectUSD 	  = null;
ArrayList  aListaEpos = null;
ArrayList  aRegistrosMN = null;
ArrayList  aRegistrosUSD = null;
HashMap    hIFMN  = null;
HashMap    hIFUSD  = null;

Boolean    bHayMensajeIFMN = null;
Boolean    bHayMensajeIFUSD = null;
String     strTipoCadena = null;
String     strNumeroDoctosFormateado = null;
String     strMontoTotalFormateado = null;
String     strNumeroPymesFormateado = null;

try {	 
     
  SeleccionMovilDoctos seleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionMovilDoctosEJB", SeleccionMovilDoctos.class);
  
  // Pruebas Modulo 1
  //out.print( "<span style=\"color:green;\" ><b>Pruebas Modulo 1</b></span><br>");
  respuesta 	= seleccionDocumento.getResumenOperacionesRealizadasPorIF(strCveIF,strFechaDe,strFechaA,"1");
  bHayMensajeIFMN = new Boolean((String)respuesta.get("hayMensaje"));

  aRegistrosMN = (ArrayList)respuesta.get("listaOperaciones");
  
  //out.print("<br>aListaEpos.size:"+aRegistrosMN.size()+"<br><br><br>");
  //object 		= JSONObject.fromObject(respuesta);
  //out.print( 	object.toString() +"<br>");
  respuestaUSD 	= seleccionDocumento.getResumenOperacionesRealizadasPorIF(strCveIF,strFechaDe,strFechaA,"54");
  bHayMensajeIFUSD = new Boolean((String)respuestaUSD.get("hayMensaje"));
  aRegistrosUSD = (ArrayList)respuestaUSD.get("listaOperaciones");
  
  //objectUSD 		= JSONObject.fromObject(respuestaUSD);
  //out.print( 	objectUSD.toString() +"<br>");
  //out.print( "<br>" );
%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
  "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head profile="http://www.w3.org/2005/10/profile">
	<title><%=Constantes.SITE_TITLE%></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <meta name="HandheldFriendly" content="true" />
	<meta name="Description" content="bienvenido a nacional financiera portatil" />
	<meta name="Keywords" content="nafinet,movil,cadenas productivas,nacional financiera banca de desarrollo" />
	<meta name="Copyright" content="Nacional Financiera SNC" />
	<meta name="distribution" content="Global" />
	<meta name="Robots" content="index,follow" />
	<meta name="city" content="Mexico City" />
	<meta name="country" content="Mexico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="NO-CACHE" />
	<meta http-equiv="Expires" content="0" />
	<noscript><meta http-equiv="refresh" content="0; URL=noscript.jsp" /></noscript>
	<link href="css/general<%=(isIPad?"_ipad":"")%>.css" rel="stylesheet" type="text/css" />
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>

<script type="text/javascript">
<!--
function envia(strXHTML){
	document.forms["frmIF"].action=strXHTML;
}

function valida(){
	if(document.forms["frmIF"].txtFechaDe.value.length==0){
		alert("Debes escribir la fecha inicio");
		return false;
	}
  else if(document.forms["frmIF"].txtFechaA.value.length==0){ 
		alert("Debes escribir la fecha fin");
		return false;
	}
	else{
		document.forms["frmIF"].action='consultaIF.jsp';
    document.forms["frmIF"].txtConsulta.value=1;
		return true;
	}
}

function updateOrientation() {
  if (window.orientation == 90 || window.orientation == -90) {
    document.getElementById('header').src="images/header480.jpg";
    document.getElementById('header').width=screen.height;
  }
  else {
    document.getElementById('header').width=screen.width;
  }
}
// -->
</script>

<body onorientationchange="updateOrientation()">
<form method="post" action="consultaIF.jsp" name="frmIF">
<input name="txtIF" value="<%=(usrBasico.getCveAfiliado())%>" type="hidden"></input>
<input name="txtConsulta" value="" type="hidden"></input>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EEEEEE">
	<tr>
		<td>
  			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TablaBlanca">
 				<tr>
					<td align="center"><img id="header" src="<%=((isIPad || isFirefox || isMSIE || isChrome)?Constantes.LOGO_NAFINET_IPAD:Constantes.LOGO_NAFINET)%>" "Nafinet M&oacute;vil"/></td>
				</tr>
				<tr>
					<td align="left">
						<table width="100%" border="0">
							<tr>
								<td align="left">
									<table width="98%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="left" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="identificacion">
													<tr>
														<td>Usuario:<%=usrBasico.getNombre()+" "+usrBasico.getApellidoPaterno()+" "+usrBasico.getApellidoMaterno()%><span>&nbsp;</span>          </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="right" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="breadcrumbR">
													<tr>
														<td>INICIO/SALDO/<span class="breadcrumbS">RESUMEN OPERACI&Oacute;N DEL D&Iacute;A</span></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="sub_h2">RESUMEN POR PERIODO</td>
							</tr>
							<tr>
								<td>
                  <table>
                    <tr>
                      <td>De:<input name="txtFechaDe" class="redondear" type="text" size="10" maxlength="10" value=<%=strFechaDe%> /></input></td>
                      <td>a:<input name="txtFechaA" class="redondear" type="text" size="10" maxlength="10" value=<%=strFechaA%> /></input> dd/mm/yyyy</td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"><input type="submit" value="Consultar" class="button" onclick="retirn valida()"/></input></td>
                    </tr>
                  </table>
                </td>
							</tr>
							<tr>
								<td align="center">
									<table border="1">
										<tr align="center" class="menu">
											<td colspan="4">Moneda Nacional</td>
										</tr>
<%
           if(bHayMensajeIFMN.booleanValue()){
%>
										<tr align="center">
											<td colspan="4"><%=respuesta.get("mensaje")%></td>
										</tr>
<%
           
          }else if(aRegistrosMN.size() == 0){
%>
										<tr align="center">
											<td colspan="4">
                        No hay operaci&oacute;n del d&iacute;a de hoy o hasta el momento, 
                        para buscar en otro periodo presiona el bot&oacute;n de Consultar
                      </td>
										</tr>
<%
          }else{
%>
                    <tr align="center" class="subtitulo">
                      <td></td>
                      <td>Pymes</td>
                      <td>Monto</td>
                      <td>Doctos</td>
                    </tr>
<%
             for(int i=0;i<aRegistrosMN.size();i++){
               hIFMN=(HashMap)aRegistrosMN.get(i);
               strTipoCadena = (String)hIFMN.get("tipoCadena");
               strNumeroDoctosFormateado = (String)hIFMN.get("numeroDoctosFormateado");
               strNumeroPymesFormateado = (String)hIFMN.get("numeroPymesFormateado");
               strMontoTotalFormateado = (String)hIFMN.get("montoTotalFormateado");

%>
										<tr align="center">
											<td><%=strTipoCadena%></td>
											<td><%=strNumeroPymesFormateado%></td>
											<td><%=strMontoTotalFormateado%></td>
											<td><%=strNumeroDoctosFormateado%></td>
										</tr>
<%
            }
          }
%>
										<tr align="center" class="menu">
											<td colspan="4">D&oacute;lares</td>
										</tr>
<%
           if(bHayMensajeIFUSD.booleanValue()){
%>
										<tr align="center">
											<td colspan="4"><%=respuestaUSD.get("mensaje")%></td>
										</tr>
<%
           }else if(aRegistrosUSD.size() == 0){
%>
										<tr align="center">
											<td colspan="4">
                        No hay operaci&oacute;n del d&iacute;a de hoy o hasta el momento, 
                        para buscar en otro periodo presiona el bot&oacute;n de Consultar
                      </td>
										</tr>
<%
           }else{
%>
                    <tr align="center" class="subtitulo">
                      <td></td>
                      <td>Pymes</td>
                      <td>Monto</td>
                      <td>Doctos</td>
                    </tr>
<%
             for(int i=0;i<aRegistrosUSD.size();i++){
               hIFUSD=(HashMap)aRegistrosUSD.get(i);
               strTipoCadena = (String)hIFUSD.get("tipoCadena");
               strNumeroDoctosFormateado = (String)hIFUSD.get("numeroDoctosFormateado");
               strNumeroPymesFormateado = (String)hIFUSD.get("numeroPymesFormateado");
               strMontoTotalFormateado = (String)hIFUSD.get("montoTotalFormateado");
            
%>
										<tr align="center">
											<td><%=strTipoCadena%></td>
											<td><%=strNumeroPymesFormateado%></td>
											<td><%=strMontoTotalFormateado%></td>
											<td><%=strNumeroDoctosFormateado%></td>
										</tr>
<%
          }
        }
%>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td align="center">
                  <input type="submit" value="Regresar" class="button" onclick="envia('menuif.xhtml')"/></input>
                  <input type="submit" value="  Salir  " class="button" onclick="envia('logout.xhtml')"/></input>
                </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">
						<br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="right" class="texto1footer">Enlaces del Gobierno Federal</td>
                <td width="50" rowspan="2"><img src="images/footer_r1_c2.gif" alt="" width="49" height="43" /></td>
              </tr>
              <tr>
                <td class="texto1footer2"><%=Constantes.FOOTER%></td>
              </tr>
            </table>
            <br/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
<!--
if (window.orientation == 90 || window.orientation == -90) {
  document.getElementById('header').src="images/header480.jpg";
  document.getElementById('header').width=screen.height;
}
else {
  document.getElementById('header').width=screen.width;
}
// -->
</script>
<%
}catch(Exception e){
	e.printStackTrace();
}
%>
</body>
</html>