<jsp:include page="sesion.jsp"/>
<%@ page import="com.nafin.nafinetmovil.utils.UsuarioBasico"%>
<%@ page import="com.nafin.nafinetmovil.utils.Constantes"%>
<%@ page 
	import="
		java.lang.*, 
		java.util.*,
		com.nafin.nafinetmovil.utils.*,
		com.netro.descuento.movil.*" 
	errorPage="error.jsp"%>

<%
String ua = request.getHeader( "User-Agent" );
boolean isBlackberry = ( ua != null && ua.indexOf( "Blackberry" ) != -1 );
boolean isIPad = ( ua != null && ua.indexOf( "iPad" ) != -1 );
boolean isIPhone = ( ua != null && (ua.indexOf( "iPhone" ) != -1 || ua.indexOf( "iPod" ) != -1) );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
boolean isChrome = ( ua != null && ua.indexOf( "Chrome" ) != -1 );
%>

<%
UsuarioBasico usrBasico=(UsuarioBasico)session.getAttribute("user");
String strCboMoneda=(request.getParameter("cboMoneda")==null?"":request.getParameter("cboMoneda"));
String strCvePyme=usrBasico.getCveAfiliado();

HashMap    respuesta  = null;
//JSONObject object 	  = null;
HashMap    respuestaUSD  = null;
//JSONObject objectUSD 	  = null;
ArrayList  aListaEpos = null;
ArrayList  aListaEposMN = null;
ArrayList  aListaEposUSD = null;
HashMap    hEPO  = null;
HashMap    hEPOMN  = null;
HashMap    hEPOUSD  = null;
Boolean    bHayMensajePymeMN = new Boolean(false);
Boolean    bHayMensajePymeUSD = new Boolean(false);
Boolean    bHayMensajeEpo = new Boolean(false);
String     strMensajeEpo = "";
String     strFechaSigDiaHabil = "";


try {	 
     
  SeleccionMovilDoctos seleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionMovilDoctosEJB", SeleccionMovilDoctos.class);
  
  if("1".equals(strCboMoneda) || "".equals(strCboMoneda) || "0".equals(strCboMoneda)){
    // Pruebas Modulo 1
    //out.print( "<span style=\"color:green;\" ><b>Pruebas Modulo 1</b></span><br>");
    respuesta 	= seleccionDocumento.getResumenDoctosNegociablesPorPyme(strCvePyme,"1");
    bHayMensajePymeMN = new Boolean((String)respuesta.get("hayMensajePyme"));

    aListaEposMN = (ArrayList)respuesta.get("listaResumenPorEPO");
    
    //out.print("<br>aListaEpos.size:"+aListaEposMN.size()+"<br><br><br>");
    //object 		= JSONObject.fromObject(respuesta);
    //out.print( 	object.toString() +"<br>");
  }
  if("54".equals(strCboMoneda) || "".equals(strCboMoneda) || "0".equals(strCboMoneda)){  
    respuestaUSD 	= seleccionDocumento.getResumenDoctosNegociablesPorPyme(strCvePyme,"54");
    bHayMensajePymeUSD = new Boolean((String)respuestaUSD.get("hayMensajePyme"));
    aListaEposUSD = (ArrayList)respuestaUSD.get("listaResumenPorEPO");
    
    //objectUSD 		= JSONObject.fromObject(respuestaUSD);
    //out.print( 	objectUSD.toString() +"<br>");
    //out.print( "<br>" );
   }
%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
  "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head profile="http://www.w3.org/2005/10/profile">
	<title><%=Constantes.SITE_TITLE%></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <meta name="HandheldFriendly" content="true" />
	<meta name="Description" content="bienvenido a nacional financiera portatil" />
	<meta name="Keywords" content="nafinet,movil,cadenas productivas,nacional financiera banca de desarrollo" />
	<meta name="Copyright" content="Nacional Financiera SNC" />
	<meta name="distribution" content="Global" />
	<meta name="Robots" content="index,follow" />
	<meta name="city" content="Mexico City" />
	<meta name="country" content="Mexico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="NO-CACHE" />
	<meta http-equiv="Expires" content="0" />
	<noscript><meta http-equiv="refresh" content="0; URL=noscript.jsp" /></noscript>
	<link href="css/general<%=(isIPad?"_ipad":"")%>.css" rel="stylesheet" type="text/css" />
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>

<script type="text/javascript">
<!--
function envia(strXHTML){
	document.frmEPO.action=strXHTML;
}

function selectMoneda(){
	document.forms["frmEPO"].action='menu.xhtml'; 
	document.frmEPO.submit();
}

function asignaEPO(strEPO){
	if(document.frmEPO.cboMoneda.value==0){
		alert("Debe seleccionar una moneda");
		return false;
	}
	else{
		document.frmEPO.txtEPO.value=strEPO;
		document.frmEPO.action='SelectDoctosS';
		return true;
	}
}

function updateOrientation() {
  if (window.orientation == 90 || window.orientation == -90) {
    document.getElementById('header').src="images/header480.jpg";
    document.getElementById('header').width=screen.height;
  }
  else {
    document.getElementById('header').width=screen.width;
  }
}
// -->
</script>

<body onLoad="backButtonOverride()" onorientationchange="updateOrientation()">
<form method="post" action="" name="frmEPO">
<input name="txtEPO" value="" type="hidden"></input>
<input name="txtPyme" value="<%=(usrBasico.getCveAfiliado())%>" type="hidden"></input>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EEEEEE">
	<tr>
		<td>
  			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TablaBlanca">
 				<tr>
					<td align="center"><img id="header" src="<%=((isIPad || isFirefox || isMSIE || isChrome)?Constantes.LOGO_NAFINET_IPAD:Constantes.LOGO_NAFINET)%>" "Nafinet M&oacute;vil"/></td>
				</tr>
				<tr>
					<td align="left">
						<table width="100%" border="0">
							<tr>
								<td align="left">
									<table width="98%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="left" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="identificacion">
													<tr>
														<td>Bienvenido:<%=usrBasico.getNombre()+" "+usrBasico.getApellidoPaterno()+" "+usrBasico.getApellidoMaterno()%><span>&nbsp;</span>          </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="right" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="breadcrumbR">
													<tr>
														<td><span class="breadcrumbS">INICIO</span>/DOCUMENTOS DISPONIBLES</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center"><input type="submit" value="  Salir  " class="button" onclick="envia('logout.xhtml')"/></input></td>
							</tr>
							<tr>
								<td align="center">
									<select name="cboMoneda" class="submit" onchange="selectMoneda();">
										<option value="0"<%=(("0".equals(strCboMoneda))?"selected":"")%>>Selecciona Moneda</option>
										<option value="1"<%=(("1".equals(strCboMoneda))?"selected":"")%>>Moneda Nacional</option>
										<option value="54"<%=(("54".equals(strCboMoneda))?"selected":"")%>>D&oacute;lares</option>
									</select>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table border="1" width="100%">
<%
         if("1".equals(strCboMoneda) || "".equals(strCboMoneda) || "0".equals(strCboMoneda)){
%>
										<tr align="center" class="menu">
											<td colspan="3">Moneda Nacional</td>
										</tr>
<%
           if(bHayMensajePymeMN.booleanValue()){
%>
										<tr align="center">
											<td colspan="3"><%=respuesta.get("mensajePyme")%></td>
										</tr>
<%
           
           }
           else{
             for(int i=0;i<aListaEposMN.size();i++){
               hEPOMN=(HashMap)aListaEposMN.get(i);
               bHayMensajeEpo = new Boolean((String)hEPOMN.get("hayMensajeEpo"));
               strMensajeEpo = (String)hEPOMN.get("mensajeEpo");
               strFechaSigDiaHabil = (String)hEPOMN.get("fechaSiguienteDiaHabil");

%>
										<tr align="center" class="subtitulo">
											<td colspan="3"><%=hEPOMN.get("nombreEpo")%></td>
										</tr>
<%
             if(bHayMensajeEpo.booleanValue() && "".equals(strFechaSigDiaHabil)){
%>
										<tr align="center">
											<td colspan="3"><%=hEPOMN.get("mensajeEpo")%></td>
										</tr>
<%
           
              }else{
                if(i==0){
                  if(!"".equals(strFechaSigDiaHabil)){
%>
										<tr align="center">
											<td colspan="3">NOTA:<%=hEPOMN.get("mensajeEpo")%></td>
										</tr>
<%
                  }
%>
										<tr align="center">
											<td>Total Doctos.</td>
											<td>Monto Total</td>
											<td>&nbsp;</td>
										</tr>
<%
              }
%>
										<tr align="center">
											<td><%=hEPOMN.get("numeroDocumentosFormateado")%></td>
											<td><%=hEPOMN.get("montoTotalFormateado")%></td>
											<td><input name="login_submit" value="Seleccionar" type="submit" size="26" onclick="return asignaEPO(<%=hEPOMN.get("claveEpo")%>);" class="button"></input></td>
											<input name="txtFechaSigDiaMN" type="hidden" value="<%=strFechaSigDiaHabil%>"></input>
										</tr>
<%
              }
            }
          }
        }if("54".equals(strCboMoneda) || "".equals(strCboMoneda) || "0".equals(strCboMoneda)){
%>
										<tr align="center" class="menu">
											<td colspan="3">D&oacute;lares</td>
										</tr>
<%
           if(bHayMensajePymeUSD.booleanValue()){
%>
										<tr align="center">
											<td colspan="3"><%=respuestaUSD.get("mensajePyme")%></td>
										</tr>
<%
           }
           else{
             for(int i=0;i<aListaEposUSD.size();i++){
               hEPOUSD=(HashMap)aListaEposUSD.get(i);
               bHayMensajeEpo = new Boolean((String)hEPOUSD.get("hayMensajeEpo"));
               strMensajeEpo = (String)hEPOUSD.get("mensajeEpo");
               strFechaSigDiaHabil = (String)hEPOUSD.get("fechaSiguienteDiaHabil");
            
%>
										<tr align="center" class="subtitulo">
											<td colspan="3"><%=hEPOUSD.get("nombreEpo")%></td>
										</tr>
<%
            if(bHayMensajeEpo.booleanValue() && "".equals(strFechaSigDiaHabil)){
%>
										<tr align="center">
											<td colspan="3"><%=hEPOUSD.get("mensajeEpo")%></td>
										</tr>
<%
           
            }else{
              if(i==0){
                  if(!"".equals(strFechaSigDiaHabil)){
%>
										<tr align="center">
											<td colspan="3">NOTA:<%=hEPOUSD.get("mensajeEpo")%></td>
										</tr>
<%
                  }
%>
										<tr align="center">
											<td>Total Doctos.</td>
											<td>Monto Total</td>
											<td>&nbsp;</td>
										</tr>
<%
            }
%>
										<tr align="center">
											<td><%=hEPOUSD.get("numeroDocumentosFormateado")%></td>
											<td><%=hEPOUSD.get("montoTotalFormateado")%></td>
											<td><input name="login_submit" value="Seleccionar" type="submit" size="26" onclick="return asignaEPO(<%=hEPOUSD.get("claveEpo")%>);" class="button"></input></td>
											<input name="txtFechaSigDiaUSD" type="hidden" value="<%=strFechaSigDiaHabil%>"></input>
										</tr>
<%
            }
          }
        }
      }
%>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">
						<br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="right" class="texto1footer">Enlaces del Gobierno Federal</td>
                <td width="50" rowspan="2"><img src="images/footer_r1_c2.gif" alt="" width="49" height="43" /></td>
              </tr>
              <tr>
                <td class="texto1footer2"><%=Constantes.FOOTER%></td>
              </tr>
            </table>
            <br/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
<!--
function backButtonOverride(){
  setTimeout("backButtonOverrideBody()", 1);

}

function backButtonOverrideBody(){
  try {
  	history.forward();
  } catch (e) {
	// OK to ignore
  }
  setTimeout("backButtonOverrideBody()", 500);
}

if (window.orientation == 90 || window.orientation == -90) {
  document.getElementById('header').src="images/header480.jpg";
  document.getElementById('header').width=screen.height;
}
else {
  document.getElementById('header').width=screen.width;
}
// -->
</script>
<%
}catch(Exception e){
	e.printStackTrace();
}
%>
</body>
</html>