<%@ page isErrorPage="true"%>
<%@ page import="com.nafin.nafinetmovil.utils.Constantes"%>

<%
String ua = request.getHeader( "User-Agent" );
boolean isBlackberry = ( ua != null && ua.indexOf( "Blackberry" ) != -1 );
boolean isIPad = ( ua != null && ua.indexOf( "iPad" ) != -1 );
boolean isIPhone = ( ua != null && (ua.indexOf( "iPhone" ) != -1 || ua.indexOf( "iPod" ) != -1) );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
boolean isChrome = ( ua != null && ua.indexOf( "Chrome" ) != -1 );
%>

<%
	if (exception != null) {
		exception.printStackTrace();
	}
%>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
  "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head profile="http://www.w3.org/2005/10/profile">
	<title><%=Constantes.SITE_TITLE%></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <meta name="HandheldFriendly" content="true" />
	<meta name="Description" content="bienvenido a nacional financiera portatil" />
	<meta name="Keywords" content="nafinet,movil,cadenas productivas,nacional financiera banca de desarrollo" />
	<meta name="Copyright" content="Nacional Financiera SNC" />
	<meta name="distribution" content="Global" />
	<meta name="Robots" content="index,follow" />
	<meta name="city" content="Mexico City" />
	<meta name="country" content="Mexico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="NO-CACHE" />
	<meta http-equiv="Expires" content="0" />
	<noscript><meta http-equiv="refresh" content="0; URL=noscript.jsp" /></noscript>
	<link href="css/general<%=(isIPad?"_ipad":"")%>.css" rel="stylesheet" type="text/css" />
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<script type="text/javascript">
<!--
function updateOrientation() {
  if (window.orientation == 90 || window.orientation == -90) {
    document.getElementById('header').width=screen.height;
  }
  else {
    document.getElementById('header').width=screen.width;
  }
}
// -->
</script>
<body onorientationchange="updateOrientation()">
  <div class="header_section">
		<a><img id="header" src="<%=((isIPad || isFirefox || isMSIE || isChrome)?Constantes.LOGO_NAFINET_IPAD:Constantes.LOGO_NAFINET)%>" "Nafinet M&oacute;vil"/></a>
  </div>
  <div class="section_name">
    <a>Nafinet M&oacute;vil</a>
  </div>
  <div class="page_title">
    <a>�Ha ocurrido un error!</a>
  </div>
  <div class="error">
    <div class="center_row">
      <img src="images/error.gif" width="86" height="74" alt="error" />
    </div>
  </div>
  <div class="error_desc">
    <a>Las posibles razones pueden ser:</a>
    <ul>
      <li><a>Estas intentando ver una p&aacute;gina que no existe.</a></li>
      <li><a>No tienes conexi&oacute;n a Internet</a></li>
    </ul>
    <a>Puedes intentar lo siguiente:</a>
    <ul>
      <li><a>Verifica tu conexi&oacute;n a Internet.</a></li>      
      <li><a>Intenta volver a cargar la p&aacute;gina anterior.</a></li>
    </ul>
  </div>
  <div class="back_link">
    <a href="<%=Constantes.HOME_PAGE%>">Ir a Inicio</a>
  </div>
  <div class="footer">
		<br />
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="right" class="texto1footer">Enlaces del Gobierno Federal</td>
        <td width="50" rowspan="2"><img src="images/footer_r1_c2.gif" alt="" width="49" height="43" /></td>
      </tr>
      <tr>
        <td class="texto1footer2"><%=Constantes.FOOTER%></td>
      </tr>
    </table>
    <br/>
  </div>
<script type="text/javascript">
<!--
if (window.orientation == 90 || window.orientation == -90) {
  document.getElementById('header').width=screen.height;
}
else {
  document.getElementById('header').width=screen.width;
}
// -->
</script>
</body>
</html>