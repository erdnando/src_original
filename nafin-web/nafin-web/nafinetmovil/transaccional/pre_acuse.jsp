<jsp:include page="sesion.jsp"/>
<%@ page import="com.nafin.nafinetmovil.utils.UsuarioBasico"%>
<%@ page import="com.nafin.nafinetmovil.utils.Constantes"%>
<%@ page 
	import="
		java.lang.*, 
		java.util.*, 
		javax.naming.*,
		com.nafin.nafinetmovil.utils.*,
		com.netro.descuento.movil.*" 
	errorPage="error.jsp"%>
  
<%
String ua = request.getHeader( "User-Agent" );
boolean isBlackberry = ( ua != null && ua.indexOf( "Blackberry" ) != -1 );
boolean isIPad = ( ua != null && ua.indexOf( "iPad" ) != -1 );
boolean isIPhone = ( ua != null && (ua.indexOf( "iPhone" ) != -1 || ua.indexOf( "iPod" ) != -1) );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
boolean isChrome = ( ua != null && ua.indexOf( "Chrome" ) != -1 );
%>
  
<%
UsuarioBasico usrBasico=(UsuarioBasico)session.getAttribute("user");
String strCboIF=(String)(request.getAttribute("cboIF"));
//System.out.println("pre_acuse.jsp:strCboIF: "+strCboIF);
String strPyme=(String)(request.getAttribute("txtPyme")); 
//System.out.println("pre_acuse.jsp:txtPyme: "+strPyme);
String strEPO=(String)(request.getAttribute("txtEPO")); 
//System.out.println("pre_acuse.jsp:txtEPO: "+strEPO);
String strMoneda=(String)(request.getAttribute("cboMoneda")); 
//System.out.println("pre_acuse.jsp:cboMoneda: "+strMoneda);
String [] strParameterValuesChk=(String [])(request.getAttribute("strParameterValuesChk")); 
for(int i=0;i<strParameterValuesChk.length;i++)
  System.out.println("pre_acuse.jsp:strParameterValuesChk: "+strParameterValuesChk[i]);
String strFechaSigDia=(String)(request.getAttribute("txtFechaSigDia")); 
//System.out.println("pre_acuse.jsp:txtFechaSigDia: "+strFechaSigDia);


HashMap    respuesta   = null;
HashMap    respuesta1  = null;
HashMap    respuesta2  = null;
//JSONObject object 	   = null;
ArrayList  aDoctosDetalle = null;
HashMap    hDoctosDetalle = null;

Boolean bMostrarChkCorreo = null;
Boolean bHayCveSesion = null;
Boolean bHayMensaje = null;
Boolean bHayError = null;
String strMensaje = null;
ArrayList aCajasCve = null;
String strNumCaja = null;


try {	 
     
  SeleccionMovilDoctos seleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionMovilDoctosEJB", SeleccionMovilDoctos.class);
  
  // Pruebas Modulo 4
  // getMensajeResumenDoctosSeleccionados
  //out.print( "<span style=\"color:green;\" ><b>Pruebas Modulo 4</b></span><br>");
  respuesta 	= seleccionDocumento.getMensajeResumenDoctosSeleccionados(strParameterValuesChk);
  //object 		= JSONObject.fromObject(respuesta);
  //out.print( 	object.toString() +"<br>");
  //out.print( "<br>" );

  // Pruebas Modulo 5
  //out.print( "<span style=\"color:green;\" ><b>Pruebas Modulo 5</b></span><br>");
  // getResumenDeLaOperacion
  respuesta1 	= seleccionDocumento.getResumenDeLaOperacion(strEPO,strCboIF,strPyme , strMoneda, "", strParameterValuesChk);
  //object 		= JSONObject.fromObject(respuesta1);
  //out.print( 	object.toString() +"<br>");
  //out.print( "<br>" );

  respuesta2 	= seleccionDocumento.getParametrosClaveConfirmacion(usrBasico.getCn());
  //object 		= JSONObject.fromObject(respuesta2);
  //out.print( 	object.toString() +"<br>");
  //out.print( "<br>" );
  
  bMostrarChkCorreo = new Boolean((String)respuesta2.get("mostrarCheckBoxConfirmacionCorreo"));
  bHayCveSesion = new Boolean((String)respuesta2.get("hayClaveCesion"));
  bHayMensaje = new Boolean((String)respuesta2.get("hayMensaje"));
  bHayError = new Boolean((String)respuesta2.get("hayError"));
  strMensaje = (String)respuesta2.get("mensaje");
  aCajasCve = (ArrayList)respuesta2.get("cajasClaveCesion");
  
%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
  "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head profile="http://www.w3.org/2005/10/profile">
	<title><%=Constantes.SITE_TITLE%></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <meta name="HandheldFriendly" content="true" />
	<meta name="Description" content="bienvenido a nacional financiera portatil" />
	<meta name="Keywords" content="nafinet,movil,cadenas productivas,nacional financiera banca de desarrollo" />
	<meta name="Copyright" content="Nacional Financiera SNC" />
	<meta name="distribution" content="Global" />
	<meta name="Robots" content="index,follow" />
	<meta name="city" content="Mexico City" />
	<meta name="country" content="Mexico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="NO-CACHE" />
	<meta http-equiv="Expires" content="0" />
	<noscript><meta http-equiv="refresh" content="0; URL=noscript.jsp" /></noscript>
	<link href="css/general<%=(isIPad?"_ipad":"")%>.css" rel="stylesheet" type="text/css" />
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<script type="text/javascript">
<!--
function envia(strXHTML){
  document.forms["frmDoctos"].action=strXHTML;
}
function valida(){
  var formulario = document.forms["frmDoctos"];
  var bCheck = false;

  for(var i=0; i<formulario.elements.length; i++) {
    var elemento = formulario.elements[i];
    if(elemento.type == "password") {
      if(elemento.value == ''){
        bCheck = true;
      }
    }
  }
  if(bCheck){
    alert("Debes completar los elementos faltantes de tu clave de confirmacion");
    return false;
  }
  else{
    document.getElementById('btnDescontar').disabled=true;
    formulario.submit();
    return true;
  }
}

function updateOrientation() {
  if (window.orientation == 90 || window.orientation == -90) {
    document.getElementById('header').src="images/header480.jpg";
    document.getElementById('header').width=screen.height;
  }
  else {
    document.getElementById('header').width=screen.width;
  }
}
// -->
</script>

<body onLoad="backButtonOverride()" onorientationchange="updateOrientation()">
<form method="post" action="AcuseS" name="frmDoctos">
<input name="cboIF" value="<%=strCboIF%>" type="hidden"></input>
<input name="cboMoneda" value="<%=strMoneda%>" type="hidden"></input>
<input name="txtEPO" value="<%=strEPO%>" type="hidden"></input>
<input name="txtPyme" value="<%=(usrBasico.getCveAfiliado())%>" type="hidden"></input>
<input name="chkP" value="<%for(int i=0;i<strParameterValuesChk.length;i++){out.print(strParameterValuesChk[i]);}%>" type="hidden"></input>
<input name="txtFechaSigDia" value="<%=strFechaSigDia%>" type="hidden"></input>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EEEEEE">
	<tr>
		<td>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TablaBlanca">
    			<tr>
					<td align="center"><img id="header" src="<%=((isIPad || isFirefox || isMSIE || isChrome)?Constantes.LOGO_NAFINET_IPAD:Constantes.LOGO_NAFINET)%>" "Nafinet M&oacute;vil"/></td>
				</tr>
				<tr>
					<td align="left">
						<table width="100%" border="0">
							<tr>
								<td align="left">
									<table width="98%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="left" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="identificacion">
													<tr>
														<td> Usuario: <%=usrBasico.getNombre()+" "+usrBasico.getApellidoPaterno()+" "+usrBasico.getApellidoMaterno()%><span>&nbsp;</span> </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="right" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="breadcrumbR">
													<tr>
														<td>INICIO/SALDO/<span class="breadcrumbS">PREACUSE OPERACI&Oacute;N</span></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table width="50" border="0" class="tablaMarca">
										<tr>
											<!--td align="center"><img src="http://cadenas.nafin.com.mx/nafin/00archivos/15cadenas/15archcadenas/logos/<%=strEPO%>.gif" alt="" height="40" width="140"/></td-->
											<td align="center"><img src="images/epoch.jpg" alt=""/></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="sub_h2">RESUMEN DE LA OPERACI&Oacute;N </td>
							</tr>
							<tr>
								<td align="center">
									<table border="1" width="100%">
										<tr align="center" class="menu">
											<td colspan="3"><%if("1".equals(strMoneda)){%>Moneda Nacional<%}else if("54".equals(strMoneda)){%>D&oacute;lares<%}%></td>
										</tr>
										<tr align="center" class="subtitulo">
											<td>Total Doctos.</td>
											<td>Importe</td>
											<td>Importe a Recibir</td>
										</tr>
										<tr>
											<td align="center">Total: <%=(String)respuesta1.get("totalDocumentosFormateado")%></td>
											<td align="right">Total: <%=(String)respuesta1.get("importeTotalFormateado")%></td>
											<td align="right">Total: <%=(String)respuesta1.get("importeTotalRecibirFormateado")%></td>
										</tr>
<%
        aDoctosDetalle = (ArrayList)respuesta1.get("detalleDoctosSeleccionados");
        for(int i=0;i<aDoctosDetalle.size();i++){
          hDoctosDetalle=(HashMap)aDoctosDetalle.get(i);
%>
										<tr>
											<td align="center"><%=(String)hDoctosDetalle.get("numeroDocto")%></td>
											<td align="right"><%=(String)hDoctosDetalle.get("importeFormateado")%></td>
											<td align="right"><%=(String)hDoctosDetalle.get("importeRecibirFormateado")%></td>
										</tr>
<%
        }
%>          
										<tr align="center">
											<td colspan="4" class="subtitulo">
                      <%if(aDoctosDetalle.size() == 20 ){%>
                        Nota: Para fines de despliegue, solo se muestran 20 documentos del total a descontar.
                      <%}%>
											  	Para continuar con la operaci&oacute;n es necesario que complete los espacios en
												faltantes de acuerdo a su clave de confirmaci&oacute;n
											</td>
										</tr>
										<tr align="center">
											<td colspan="4" class="subtitulo">
												<table border="1" width="100%">
													<tr>
<%
                boolean bPintaCaja = false;
                int iPosicion = 0;
                Collections.sort(aCajasCve);
                for(int i=1;i<=8;i++){
                  for(int j=0;j<aCajasCve.size();j++){
                    if((new Integer((String)aCajasCve.get(j)).intValue())==i){
                      bPintaCaja=true;
                      iPosicion=i;
                      break;
                    }
                    else
                      bPintaCaja=false;
                  }
%>
														<td width="12.5%"><%if(bPintaCaja){%><input name="txtPos<%=iPosicion%>" type="password" size="2" maxlength="1"></input><%}else{%>*<%}%></td>
<%
                }
%>          
													</tr>
												</table>
											</td>
										</tr>
<%
          if(bMostrarChkCorreo.booleanValue()){
%>
										<tr align="center">
											<td colspan="4" class="subtitulo">
												<table border="0">
													<tr>
														<td><input name="chkMail" type="checkbox" /></input></td>
														<td>
															Desea que se env&iacute;e el acuse al correo: <b><%=usrBasico.getMail()%></b>.
															Si el correo es incorrecto favor de comunicarse al 01800-NAFINSA 
														</td>
													</tr>
												</table>
											</td>
										</tr>
<%
          }
%>
										<tr align="center">
											<td colspan="4" class="leyenda"> 
												&iquest;Olvido su clave de cesi&oacute;n de derechos? Favor de comunicarse al centro de atenci&oacute;n a clientes en la Cd. de M&eacute;xico
                								53-25-6000 o sin costo desde el interior de la rep&uacute;blica al 01800-NAFINSA (01800-623-4672)
                			</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr align="center">
								<td>
									<table>
										<tr>
											<td><input type="submit" value="Descontar" class="button" onclick="return valida();" id="btnDescontar"/></input></td>
											<td><input type="submit" value="Regresar" class="button" onclick="envia('SelectDoctosS')"/></input></td>
											<td><input type="submit" value="  Salir  " class="button" onclick="envia('logout.xhtml')"/></input></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="right" class="texto1footer">Enlaces del Gobierno Federal</td>
                <td width="50" rowspan="2"><img src="images/footer_r1_c2.gif" alt="" width="49" height="43" /></td>
              </tr>
              <tr>
                <td class="texto1footer2"><%=Constantes.FOOTER%></td>
              </tr>
            </table>
            <br/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
<!--
function backButtonOverride(){
  setTimeout("backButtonOverrideBody()", 1);

}

function backButtonOverrideBody(){
  try {
  	history.forward();
  } catch (e) {
	// OK to ignore
  }
  setTimeout("backButtonOverrideBody()", 500);
}

if (window.orientation == 90 || window.orientation == -90) {
  document.getElementById('header').src="images/header480.jpg";
  document.getElementById('header').width=screen.height;
}
else {
  document.getElementById('header').width=screen.width;
}
// -->
</script>
<%
}catch(Exception e){
	e.printStackTrace();
}
%>
</body>
</html>