<jsp:include page="sesion.jsp"/>
<%@ page import="com.nafin.nafinetmovil.utils.UsuarioBasico"%>
<%@ page import="com.nafin.nafinetmovil.utils.Constantes"%>
<%@ page 
	import="
		java.lang.*, 
		java.util.*, 
		com.nafin.nafinetmovil.utils.*,
		com.netro.descuento.movil.*" 
	errorPage="error.jsp"%>
  
<%
String ua = request.getHeader( "User-Agent" );
boolean isBlackberry = ( ua != null && ua.indexOf( "Blackberry" ) != -1 );
boolean isIPad = ( ua != null && ua.indexOf( "iPad" ) != -1 );
boolean isIPhone = ( ua != null && (ua.indexOf( "iPhone" ) != -1 || ua.indexOf( "iPod" ) != -1) );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
boolean isChrome = ( ua != null && ua.indexOf( "Chrome" ) != -1 );
%>

<%
UsuarioBasico usrBasico=(UsuarioBasico)session.getAttribute("user");
String strCboIF=(String)(request.getAttribute("cboIF"));
//System.out.println("acuse.jsp:strCboIF: "+strCboIF);
String strPyme=(String)(request.getAttribute("txtPyme")); 
//System.out.println("acuse.jsp:txtPyme: "+strPyme);
String strEPO=(String)(request.getAttribute("txtEPO")); 
//System.out.println("acuse.jsp:txtEPO: "+strEPO);
String strMoneda=(String)(request.getAttribute("cboMoneda")); 
//System.out.println("acuse.jsp:cboMoneda: "+strMoneda);
String [] strParameterValuesChk=(String [])(request.getAttribute("strParameterValuesChk")); 
//for(int i=0;i<strParameterValuesChk.length;i++)
//  System.out.println("acuse.jsp:strParameterValuesChk: "+strParameterValuesChk[i]);
String strFechaSigDia=(String)(request.getAttribute("txtFechaSigDia")); 
//System.out.println("acuse.jsp:txtFechaSigDia: "+strFechaSigDia);

String strPos1=((String)(request.getAttribute("txtPos1"))==null?"":(String)(request.getAttribute("txtPos1"))); 
//System.out.println("acuse.jsp:strPos1: "+strPos1);
String strPos2=((String)(request.getAttribute("txtPos2"))==null?"":(String)(request.getAttribute("txtPos2"))); 
//System.out.println("acuse.jsp:strPos2: "+strPos2);
String strPos3=((String)(request.getAttribute("txtPos3"))==null?"":(String)(request.getAttribute("txtPos3"))); 
//System.out.println("acuse.jsp:strPos3: "+strPos3);
String strPos4=((String)(request.getAttribute("txtPos4"))==null?"":(String)(request.getAttribute("txtPos4"))); 
//System.out.println("acuse.jsp:strPos4: "+strPos4);
String strPos5=((String)(request.getAttribute("txtPos5"))==null?"":(String)(request.getAttribute("txtPos5"))); 
//System.out.println("acuse.jsp:strPos5: "+strPos5);
String strPos6=((String)(request.getAttribute("txtPos6"))==null?"":(String)(request.getAttribute("txtPos6"))); 
//System.out.println("acuse.jsp:strPos6: "+strPos6);
String strPos7=((String)(request.getAttribute("txtPos7"))==null?"":(String)(request.getAttribute("txtPos7"))); 
//System.out.println("acuse.jsp:strPos7: "+strPos7);
String strPos8=((String)(request.getAttribute("txtPos8"))==null?"":(String)(request.getAttribute("txtPos8"))); 
//System.out.println("acuse.jsp:strPos8: "+strPos8);


HashMap    respuesta  = null;
HashMap    respuesta1  = null;
JSONObject object 	   = null;

Boolean bHayMensaje = null;
Boolean bHayError = null;
String strMensaje = null;
Boolean bEsClaveCorrecta = null;

Boolean bExito = null;
Boolean bHayMensaje1 = null;
Boolean bHayError1 = null;
ArrayList aMensajeSeleccion = null;
String strNombreMoneda = null;
ArrayList  aDoctosDetalle = null;
HashMap    hDoctosDetalle = null;
String strTotalNumDoctosFormateado = null;
String strTotalImpFormateado = null;
String strTotalImpRecFormateado = null;

String strTotalNumDoctos = null;
String strTotalImporte = null;
String strTotalImpRecibir = null;
ArrayList aDetalleDoctosSelec = null;
String strDetalleDoctosSelec = null;
HashMap hCifrasDeControl = null;
String strNumeroDeAcuse = null;
String strFechaDeOperacion = null;
String strHoraDeOperacion = null;
String strIntermediarioFinanciero = null;
String strUsuarioDeCaptura = null;
String strNombreEPO = null;
Boolean bNoSeMuestraDetalleDeTodosLosDoctos = null;
Boolean bFalloPDF = null;
Boolean bFalloCorreo = null;

String lsKey1 = "DIR_TMP";
String lsKey2 = "DIR_PUB";
String directorioTemporal;
String directorioPublicacion;
    
try {	 
     
  SeleccionMovilDoctos seleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionMovilDoctosEJB", SeleccionMovilDoctos.class);
  
  //ResourceBundle bundle = ResourceBundle.getBundle("nafinetmovil", Locale.ENGLISH);
  //directorioTemporal = bundle.getString(lsKey1);
  directorioTemporal = application.getRealPath("/") + "/00tmp/13descuento/";
  //directorioPublicacion = bundle.getString(lsKey2);
  directorioPublicacion = application.getRealPath("/") + "/";
  //System.out.println("key1 = " + lsKey1 + ", " + "value = " + strURL_OID_WS);
  //System.out.println("key2 = " + lsKey2 + ", " + "value = " + strINSTANCE_OID_WS);


  // Pruebas Modulo 6
  //out.print( "<span style=\"color:green;\" ><b>Pruebas Modulo 6</b></span><br>");
  // validaParametrosClaveConfirmacion
  ArrayList 	caracteresClaveCesion 	= new ArrayList();
  HashMap 		registro 					= null;

  if(!"".equals(strPos1)){
    registro = new HashMap();
    registro.put("caracterContrasena", strPos1);// caracter contraseña
    registro.put("caja",					  "1");// posicion del caracter
    caracteresClaveCesion.add(registro);
  }
  if(!"".equals(strPos2)){
    registro = new HashMap();
    registro.put("caracterContrasena", strPos2);// caracter contraseña
    registro.put("caja",					  "2");// posicion del caracter
    caracteresClaveCesion.add(registro);
  }
  if(!"".equals(strPos3)){
    registro = new HashMap();
    registro.put("caracterContrasena", strPos3);// caracter contraseña
    registro.put("caja",					  "3");// posicion del caracter
    caracteresClaveCesion.add(registro);
  }
  if(!"".equals(strPos4)){
    registro = new HashMap();
    registro.put("caracterContrasena", strPos4);// caracter contraseña
    registro.put("caja",					  "4");// posicion del caracter
    caracteresClaveCesion.add(registro);
  }
  if(!"".equals(strPos5)){
    registro = new HashMap();
    registro.put("caracterContrasena", strPos5);// caracter contraseña
    registro.put("caja",					  "5");// posicion del caracter
    caracteresClaveCesion.add(registro);
  }
  if(!"".equals(strPos6)){
    registro = new HashMap();
    registro.put("caracterContrasena", strPos6);// caracter contraseña
    registro.put("caja",					  "6");// posicion del caracter
    caracteresClaveCesion.add(registro);
  }
  if(!"".equals(strPos7)){
    registro = new HashMap();
    registro.put("caracterContrasena", strPos7);// caracter contraseña
    registro.put("caja",					  "7");// posicion del caracter
    caracteresClaveCesion.add(registro);
  }
  if(!"".equals(strPos8)){
    registro = new HashMap();
    registro.put("caracterContrasena", strPos8);// caracter contraseña
    registro.put("caja",					  "8");// posicion del caracter
    caracteresClaveCesion.add(registro);
  }

  respuesta 	= seleccionDocumento.validaParametrosClaveConfirmacion(usrBasico.getCn(), caracteresClaveCesion, false);
  //object 		= JSONObject.fromObject(respuesta);
  //out.print( 	object.toString() +"<br>");
  //out.print( "<br>" );

  bEsClaveCorrecta = new Boolean((String)respuesta.get("esClaveCorrecta"));
  bHayMensaje = new Boolean((String)respuesta.get("hayMensaje"));
  bHayError = new Boolean((String)respuesta.get("hayError"));
  strMensaje = (String)respuesta.get("mensaje");
  
  if(bEsClaveCorrecta.booleanValue() && !bHayMensaje.booleanValue() && !bHayError.booleanValue()){
    // seleccionarDocumentosParaDescuento
    HashMap parametrosUsuario = new HashMap();
    parametrosUsuario.put("loginUsuario",					usrBasico.getCn());
    parametrosUsuario.put("nombreUsuario",				usrBasico.getApellidoPaterno()+" "+usrBasico.getApellidoMaterno()+" "+usrBasico.getNombre());
    parametrosUsuario.put("logo",								  strEPO+".gif");
    respuesta1 	= seleccionDocumento.seleccionarDocumentosParaDescuento(
      strEPO, 
      strCboIF, 
      strPyme, 
      strMoneda, 
      strFechaSigDia, 
      strParameterValuesChk, 
      usrBasico.getCn(), 
      usrBasico.getMail(),
      directorioTemporal,//"/opt/oraias/infra10g/j2ee/OC4J_Nafin/applications/nafin/nafin-web/00tmp/13descuento/",
      directorioPublicacion,//"/opt/oraias/infra10g/j2ee/OC4J_Nafin/applications/nafin/nafin-web/",
      parametrosUsuario
    );
  
    bExito = new Boolean((String)respuesta1.get("exito"));
    bHayMensaje1 = new Boolean((String)respuesta1.get("hayMensaje"));
    bHayError1 = new Boolean((String)respuesta1.get("hayError"));
    aMensajeSeleccion = (ArrayList)respuesta1.get("mensajeSeleccion");
    strNombreMoneda = (String)respuesta1.get("nombreMoneda");
    strTotalNumDoctosFormateado = (String)respuesta1.get("totalNumeroDoctosFormateado");
    strTotalImpFormateado = (String)respuesta1.get("totalImporteFormateado");
    strTotalImpRecFormateado = (String)respuesta1.get("totalImporteRecibirFormateado");
    hCifrasDeControl = (HashMap)respuesta1.get("cifrasDeControl");
    strNumeroDeAcuse = (String)hCifrasDeControl.get("numeroDeAcuse");
    strFechaDeOperacion = (String)hCifrasDeControl.get("fechaDeOperacion");
    strHoraDeOperacion = (String)hCifrasDeControl.get("horaDeOperacion");
    strIntermediarioFinanciero = (String)hCifrasDeControl.get("intermediarioFinanciero");
    strUsuarioDeCaptura = (String)hCifrasDeControl.get("usuarioDeCaptura");
    strNombreEPO = (String)respuesta1.get("nombreEpo");
    strTotalNumDoctos = (String)respuesta1.get("totalNumeroDoctos");
  
    /*object 		= JSONObject.fromObject(respuesta1);
    out.print( 	object.toString() +"<br>");
    out.print( "<br>" );*/
  }
%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
  "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head profile="http://www.w3.org/2005/10/profile">
	<title><%=Constantes.SITE_TITLE%></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <meta name="HandheldFriendly" content="true" />
	<meta name="Description" content="bienvenido a nacional financiera portatil" />
	<meta name="Keywords" content="nafinet,movil,cadenas productivas,nacional financiera banca de desarrollo" />
	<meta name="Copyright" content="Nacional Financiera SNC" />
	<meta name="distribution" content="Global" />
	<meta name="Robots" content="index,follow" />
	<meta name="city" content="Mexico City" />
	<meta name="country" content="Mexico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="NO-CACHE" />
	<meta http-equiv="Expires" content="0" />
	<noscript><meta http-equiv="refresh" content="0; URL=noscript.jsp" /></noscript>
	<link href="css/general<%=(isIPad?"_ipad":"")%>.css" rel="stylesheet" type="text/css" />
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<script type="text/javascript">
<!--
function envia(strXHTML){
  document.forms["frmDoctos"].action=strXHTML;
}

function updateOrientation() {
  if (window.orientation == 90 || window.orientation == -90) {
    document.getElementById('header').width=screen.height;
  }
  else {
    document.getElementById('header').width=screen.width;
  }
}
// -->
</script>

<body onLoad="backButtonOverride()" onorientationchange="updateOrientation()">
<form method="post" action="" name="frmDoctos">
<input name="cboIF" value="<%=strCboIF%>" type="hidden"></input>
<input name="cboMoneda" value="<%=strMoneda%>" type="hidden"></input>
<input name="txtEPO" value="<%=strEPO%>" type="hidden"></input>
<input name="txtPyme" value="<%=(usrBasico.getCveAfiliado())%>" type="hidden"></input>
<input name="chkP" value="<%for(int i=0;i<strParameterValuesChk.length;i++){out.print(strParameterValuesChk[i]);}%>" type="hidden"></input>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EEEEEE">
	<tr>
		<td>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TablaBlanca">
				<tr>
					<td align="center"><img id="header" src="<%=((isIPad || isFirefox || isMSIE || isChrome)?Constantes.LOGO_NAFINET_IPAD:Constantes.LOGO_NAFINET)%>" "Nafinet M&oacute;vil"/></td>
				</tr>
				<tr>
					<td align="left">
						<table width="100%" border="0">
							<tr>
								<td align="left">
									<table width="98%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="left" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="identificacion">
													<tr>
														<td> Usuario: <%=usrBasico.getNombre()+" "+usrBasico.getApellidoPaterno()+" "+usrBasico.getApellidoMaterno()%><span>&nbsp;</span> </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="right" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="breadcrumbR">
													<tr>
														<td>INICIO/SALDO/<span class="breadcrumbS">ACUSE DE LA OPERACI&Oacute;N</span></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table width="50" border="0" class="tablaMarca">
										<tr>
											<!--td align="center"><img src="http://cadenas.nafin.com.mx/nafin/00archivos/15cadenas/15archcadenas/logos/<%=strEPO%>.gif" alt="" height="40" width="140"/></td-->
											<td align="center"><img src="images/epoch.jpg" alt=""/></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="sub_h2">RESUMEN DE LA OPERACI&Oacute;N </td>
							</tr>
<%
  if(!bEsClaveCorrecta.booleanValue() || bHayMensaje.booleanValue() || bHayError.booleanValue()){
%>
    <tr>
      <td class="msj"><%=strMensaje%></td>
    </tr>
<%
  }else if(!bExito.booleanValue() || bHayError1.booleanValue()){
    for(int i=0;i<aMensajeSeleccion.size();i++){
%>
    <tr>
      <td class="msj"><%=(String)aMensajeSeleccion.get(i)%></td>
    </tr>
<%
    }
  }else if(bExito.booleanValue()){
    if(bHayMensaje1.booleanValue()){
      for(int i=0;i<aMensajeSeleccion.size();i++){
%>
    <tr>
      <td class="msj"><%=(String)aMensajeSeleccion.get(i)%></td>
    </tr>
<%
      }
    }
%>
							<tr>
  								<td align="center">
  									<table border="1" width="100%">
										<tr align="center" class="menu">
											<td colspan="4"><%=strNombreEPO%></td>
										</tr>
										<tr align="center" class="menu">
											<td colspan="4"><%=strNombreMoneda%></td>
										</tr>
										<tr align="center" class="subtitulo">
											<td></td>
											<td>Num. Docto.</td>
											<td>Importe</td>
											<td>Importe a Recibir</td>
										</tr>
<%
        aDoctosDetalle = (ArrayList)respuesta1.get("detalleDoctosSeleccionados");
        for(int i=0;i<aDoctosDetalle.size();i++){
          hDoctosDetalle=(HashMap)aDoctosDetalle.get(i);
%>
										<tr align="center">
											<td class="subtitulo"></td>
											<td><%=(String)hDoctosDetalle.get("numeroDocto")%></td>
											<td align="right"><%=(String)hDoctosDetalle.get("importeFormateado")%></td>
											<td align="right"><%=(String)hDoctosDetalle.get("importeRecibirFormateado")%></td>
										</tr>
<%
        }
%>          
										<tr align="center" class="row_odd">
											<td>Total</td>
											<td><%=strTotalNumDoctosFormateado%></td>
											<td><%=strTotalImpFormateado%></td>
											<td><%=strTotalImpRecFormateado%></td>
										</tr>
										<tr align="center">
											<td colspan="4">
                      <%if(aDoctosDetalle.size() == 20 ){%>
                        Nota: Para fines de despliegue, solo se muestran 20 documentos del total a descontar.
                      <%}else{%>
                        &nbsp;
                      <%}%>
                      </td>
										</tr>
										<tr align="center">
											<td colspan="4">
												<table border="1" width="100%">
													<tr align="center" class="menu">
														<td colspan="2">Cifras Control</td>
													</tr>
													<tr align="left">
														<td class="subtitulo">N&uacute;mero de Acuse</td>
														<td><b><%=strNumeroDeAcuse%></b></td>
													</tr>
													<tr align="left">
														<td class="subtitulo">Fecha de Operaci&oacute;n</td>
														<td><b><%=strFechaDeOperacion%></b></td>
													</tr>
													<tr align="left">
														<td class="subtitulo">Hora de Operaci&oacute;n</td>
														<td><b><%=strHoraDeOperacion%></b></td>
													</tr>
													<tr align="left">
														<td class="subtitulo">Intermediario Financiero</td>
														<td><b><%=strIntermediarioFinanciero%></b></td>
													</tr>
													<tr align="left">
														<td class="subtitulo">Usuario de Captura</td>
														<td><b><%=strUsuarioDeCaptura%></b></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr align="center">
											<td colspan="4">
												<table border="0">
													<tr>
														<td align="left" class="leyenda">
															Al transmitir este mensajede datos, bajo mi responsabilidad acepto la propiedad de los documentos
															electr&oacute;nicos que constan descritos en el mismo. Dicha aceptaci&oacute;n tendr&aacute; plena validez
															para todos los efectos legales.
														</td>
													</tr>
													<tr>
														<td align="left" class="leyenda">
															En este mismo acto se genera el aviso de notificaci&oacute;n a la
															Empresa de Primer Orden (EPO) quien es la &uacute;nica posibilitada para recibirla, por lo que tiene validez
															de acuse el recibo.
														</td>
													</tr>
													<tr>
														<td align="left" class="leyenda">
															Las operaciones que aparecen en esta p&aacute;gina fueron notificada a la Empresa
															de Primer Orden de conformidad y para efectos de los art&iacute;culos 45 K de la Ley de General de Organizaciones
															y Actvidades Auxiliares del Cr&eacute;dito y 32 C del C&oacute;digo Fiscal o del art&iacute;culo 2038 del C&oacute;digo Civil Federal
															seg&uacute;n corresponda, para los efectos legales conducentes.<br/>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
<%
  }
%>
							<tr align="center">
								<td>
									<table>
										<tr>
											<td><input type="submit" value="Regresar" class="button" onclick="envia('PreAcuseS')"/></input></td>
											<td><input type="submit" value="Terminar" class="button" onclick="envia('menu.xhtml')"/></input></td>
											<td><input type="submit" value="  Salir  " class="button" onclick="envia('logout.xhtml')"/></input></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="right" class="texto1footer">Enlaces del Gobierno Federal</td>
                <td width="50" rowspan="2"><img src="images/footer_r1_c2.gif" alt="" width="49" height="43" /></td>
              </tr>
              <tr>
                <td class="texto1footer2"><%=Constantes.FOOTER%></td>
              </tr>
            </table>
            <br/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
<!--
function backButtonOverride(){
  setTimeout("backButtonOverrideBody()", 1);

}

function backButtonOverrideBody(){
  try {
  	history.forward();
  } catch (e) {
	// OK to ignore
  }
  setTimeout("backButtonOverrideBody()", 500);
}

if (window.orientation == 90 || window.orientation == -90) {
  document.getElementById('header').src="images/header480.jpg";
  document.getElementById('header').width=screen.height;
}
else {
  document.getElementById('header').width=screen.width;
}
// -->
</script>

<%
}catch(Exception e){
	e.printStackTrace();
}

%>
</body>
</html>