<%@ page import="com.nafin.nafinetmovil.utils.Constantes"%>

<%
String ua = request.getHeader( "User-Agent" );
boolean isBlackberry = ( ua != null && ua.indexOf( "Blackberry" ) != -1 );
boolean isIPad = ( ua != null && ua.indexOf( "iPad" ) != -1 );
boolean isIPhone = ( ua != null && (ua.indexOf( "iPhone" ) != -1 || ua.indexOf( "iPod" ) != -1) );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
boolean isChrome = ( ua != null && ua.indexOf( "Chrome" ) != -1 );
%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
  "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head profile="http://www.w3.org/2005/10/profile">
	<title><%=Constantes.SITE_TITLE%></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <meta name="HandheldFriendly" content="true" />
	<meta name="Description" content="bienvenido a nacional financiera portatil" />
	<meta name="Keywords" content="nafinet,movil,cadenas productivas,nacional financiera banca de desarrollo" />
	<meta name="Copyright" content="Nacional Financiera SNC" />
	<meta name="distribution" content="Global" />
	<meta name="Robots" content="index,follow" />
	<meta name="city" content="Mexico City" />
	<meta name="country" content="Mexico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="NO-CACHE" />
	<meta http-equiv="Expires" content="0" />
	<noscript><meta http-equiv="refresh" content="0; URL=noscript.jsp" /></noscript>
	<link href="css/general<%=((isIPad || isFirefox || isMSIE || isChrome)?"_ipad":"")%>.css" rel="stylesheet" type="text/css" />
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
	
<script type="text/javascript">
function enviar(){
  if(document.forms["frmPyme"].movilUser.value.length == 0 || document.forms["frmPyme"].movilPassword.value.length == 0){
    alert("Favor de escribir user y password");
    return false;
  }
  else{
    document.getElementById('btnEntrar').disabled=true;
    document.forms["frmPyme"].submit();
    return true;
  }
}

function updateOrientation() {
  if (window.orientation == 90 || window.orientation == -90) {
    document.getElementById('tblFrm').className = 'formularioBLan';
    document.getElementById('header').src="images/header480.jpg";
    document.getElementById('header').width=screen.height;
  }
  else {
    document.getElementById('tblFrm').className = 'formularioB';
    document.getElementById('header').width=screen.width;
  }
}
// -->
</script>
 
<body onorientationchange="updateOrientation()">
<form method="post" action="SeguridadS" name="frmPyme">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EEEEEE">
	<tr>
		<td  align="center" valign="middle">
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center"><img id="header" src="<%=((isIPad || isFirefox || isMSIE || isChrome)?Constantes.LOGO_NAFINET_IPAD:Constantes.LOGO_NAFINET)%>" "Nafinet M&oacute;vil"/></td>
				</tr>
<%
        if(request.getAttribute("mensaje")!=null){
%>
				<tr>
				  <td align="center" class="msj" colspan="2" bgcolor="#EEEEEE">
					<%=(String)request.getAttribute("mensaje")%>
				  </td>
				</tr>
<%
        }
%>
				<tr>
					<td id="tblFrm" "left" class="formularioB">
						<table width="147" border="0" align="left" cellpadding="0" cellspacing="0">
							<tr>
								<td width="141" align="left" class="home_titulo"><a>USUARIO</a></td>
							</tr>
							<tr>
								<td align="left"><input name="movilUser" class="redondear" type="text" size="15"></input></td>
							</tr>
							<tr>
								<td align="left" class="home_titulo"><a>PASSWORD</a></td>
							</tr>
							<tr>
								<td align="left"><input name="movilPassword" class="redondear" type="password" size="15"></input></td>
							</tr>
							<tr>
								<td align="right"><input id="btnEntrar" name="login_submit" value="Entrar" type="submit" size="26" class="button" onclick="return enviar();"></input>&nbsp;&nbsp;&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
            <table width="<%=((isIPad || isFirefox || isMSIE || isChrome)?"760":"320")%>" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td valign="top" class="celdaBanner">
                  <table width="<%=((isIPad || isFirefox || isMSIE || isChrome)?"760":"320")%>" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="center" valign="middle">
                        <table width="<%=((isIPad || isFirefox || isMSIE || isChrome)?"760":"320")%>" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td>
                              <span class="TitBanner">Cadenas Productivas</span><br />
                              <span class="subTitBanner">NAFINET M&oacute;vil</span><br />
                              Bienvenidos a Cadenas Productivas, liquidez inmediata en la palma de tu mano.
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <!--td>&nbsp;</td>
                <td align="center" valign="top" class="celdaBanner">
                  <table width="<%=(isIPad?"371":"150")%>" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>
                        <span class="TitBanner">T&Iacute;TULO</span><br />
                        <span class="subTitBanner">Subt&iacute;tulo</span><br />
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                      </td>
                    </tr>
                  </table>
                </td-->
              </tr>
            </table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="right" class="texto1footer">Enlaces del Gobierno Federal</td>
                <td width="50" rowspan="2"><img src="images/footer_r1_c2.gif" alt="" width="49" height="43" /></td>
              </tr>
              <tr>
                <td class="texto1footer2"><%=Constantes.FOOTER%></td>
              </tr>
            </table>
            <br/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
if (window.orientation == 90 || window.orientation == -90) {
  document.getElementById('tblFrm').className = 'formularioBLan';
  document.getElementById('header').src="images/header480.jpg";
  document.getElementById('header').width=screen.height;
}
else {
  document.getElementById('tblFrm').className = 'formularioB';
  document.getElementById('header').width=screen.width;
}
// -->
</script>

</body>
</html>