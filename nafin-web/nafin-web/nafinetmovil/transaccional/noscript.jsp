<%@ page import="com.nafin.nafinetmovil.utils.Constantes"%>
<%
String ua = request.getHeader( "User-Agent" );
boolean isBlackberry = ( ua != null && ua.indexOf( "Blackberry" ) != -1 );
boolean isIPad = ( ua != null && ua.indexOf( "iPad" ) != -1 );
boolean isIPhone = ( ua != null && (ua.indexOf( "iPhone" ) != -1 || ua.indexOf( "iPod" ) != -1) );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
boolean isChrome = ( ua != null && ua.indexOf( "Chrome" ) != -1 );
%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
  "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head profile="http://www.w3.org/2005/10/profile">
	<title><%=Constantes.SITE_TITLE%></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <meta name="HandheldFriendly" content="true" />
	<meta name="Description" content="bienvenido a nacional financiera portatil" />
	<meta name="Keywords" content="nafinet,movil,cadenas productivas,nacional financiera banca de desarrollo" />
	<meta name="Copyright" content="Nacional Financiera SNC" />
	<meta name="distribution" content="Global" />
	<meta name="Robots" content="index,follow" />
	<meta name="city" content="Mexico City" />
	<meta name="country" content="Mexico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="NO-CACHE" />
	<meta http-equiv="Expires" content="0" />
	<link href="css/general<%=(isIPad?"_ipad":"")%>.css" rel="stylesheet" type="text/css" />
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<script type="text/javascript">
<!--
function updateOrientation() {
  if (window.orientation == 90 || window.orientation == -90) {
    document.getElementById('header').src="images/header480.jpg";
    document.getElementById('header').width=screen.height;
  }
  else {
    document.getElementById('header').width=screen.width;
  }
}
// -->
</script>
<body onorientationchange="updateOrientation()">
<form method="post" action="login.xhtml" name="frmEPO">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EEEEEE">
  <tr>
    <td>
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TablaBlanca">
        <tr>
					<td align="center"><img id="header" src="<%=((isIPad || isFirefox || isMSIE || isChrome)?Constantes.LOGO_NAFINET_IPAD:Constantes.LOGO_NAFINET)%>" "Nafinet M&oacute;vil"/></td>
        </tr>
        <tr>
          <td align="left">
            <table width="100%" border="0">
              <tr>
                <td align="left">
                  <table width="98%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>
                        <table border="0" cellpadding="0" cellspacing="0" class="identificacion">
                          <tr>
                            <td>
                             <span>&nbsp;</span>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td align="right" valign="middle">
                        <table border="0" cellpadding="0" cellspacing="0" class="breadcrumbR">
                          <tr>
                            <td>INICIO/<span class="breadcrumbS">AVISO</span></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td class="msj">
                  Usted no tiene habilitado el soporte para javascript, para operar Nafinet M&oacute;vil
                  es necesario habitarlo en su dispositivo m&oacute;vil
                </td>
              </tr>
              <tr>
                <td align="center">
                  <table border="0">
                    <tr align="center" class="menu">
                      <td>Para habilitarlo en IPhone (Safari)</td>
                    </tr>
                    <tr>
                      <td align="left">
                        <ul>
                          <li>Presiona el &iacute;cono de "Ajustes".</li>
                          <li>Buscar "Safari".</li>
                          <li>Buscar la opcion de "JavaScript". Encender la opcion, debe de estar en |</li>
                        </ul>
                      </td>
                    </tr>
                    <tr align="center" class="menu">
                      <td>Para habilitarlo en Blackberry</td>
                    </tr>
                    <tr>
                      <td align="left">
                        <ul>
                          <li>Presiona bot&oacute;n de "Men&uacute;".</li>
                          <li>Presionar "Opciones".</li>
                          <li>Presionar "Configuraci&oacute;n del explorador".</li>
                          <li>Seleccionar "Admitir JavaScript".</li>
                          <li>Presiona bot&oacute;n de "Men&uacute;".</li>
                          <li>Presionar "Guardar Opciones".</li>
                        </ul>
                      </td>
                    </tr>
                    <tr align="center">
                      <td><input name="btnInicio" value="Ir inicio" type="submit" onclick="return validaCve();" class="button" /></input></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
				<tr>
					<td align="center">
						<br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="right" class="texto1footer">Enlaces del Gobierno Federal</td>
                <td width="50" rowspan="2"><img src="images/footer_r1_c2.gif" alt="" width="49" height="43" /></td>
              </tr>
              <tr>
                <td class="texto1footer2"><%=Constantes.FOOTER%></td>
              </tr>
            </table>
            <br/>
					</td>
				</tr>
      </table>
    </td>
  </tr>
</table>
</form>
<script type="text/javascript">
<!--
if (window.orientation == 90 || window.orientation == -90) {
  document.getElementById('header').src="images/header480.jpg";
  document.getElementById('header').width=screen.height;
}
else {
  document.getElementById('header').width=screen.width;
}
// -->
</script>

</body>
</html>