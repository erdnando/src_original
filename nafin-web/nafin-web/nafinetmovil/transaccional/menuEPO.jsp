<jsp:include page="sesion.jsp"/>
<%@ page import="com.nafin.nafinetmovil.utils.UsuarioBasico"%>
<%@ page import="com.nafin.nafinetmovil.utils.Constantes"%>
<%@ page 
	import="
		java.lang.*, 
		java.util.*,
		com.nafin.nafinetmovil.utils.*,
		com.netro.descuento.movil.*" 
	errorPage="error.jsp"%>

<%
String ua = request.getHeader( "User-Agent" );
boolean isBlackberry = ( ua != null && ua.indexOf( "Blackberry" ) != -1 );
boolean isIPad = ( ua != null && ua.indexOf( "iPad" ) != -1 );
boolean isIPhone = ( ua != null && (ua.indexOf( "iPhone" ) != -1 || ua.indexOf( "iPod" ) != -1) );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
boolean isChrome = ( ua != null && ua.indexOf( "Chrome" ) != -1 );
%>

<%
UsuarioBasico usrBasico=(UsuarioBasico)session.getAttribute("user");
String strCboMoneda=(request.getParameter("cboMoneda")==null?"":request.getParameter("cboMoneda"));
String strCveIF=usrBasico.getCveAfiliado();

HashMap    respuesta  = null;
//JSONObject object 	  = null;
HashMap    respuestaUSD  = null;
//JSONObject objectUSD 	  = null;
ArrayList  aListaEpos = null;
ArrayList  aRegistrosMN = null;
ArrayList  aRegistrosUSD = null;
HashMap    hEPOMN  = null;
HashMap    hEPOUSD  = null;

Boolean    bHayMensajeEPOMN = null;
Boolean    bHayMensajeEPOUSD = null;
String     strTipoCadena = null;
String     strNumeroDoctosFormateado = null;
String     strMontoTotalFormateado = null;
String     strNumeroPymesFormateado = null;

try {	 
     
  SeleccionMovilDoctos seleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionMovilDoctosEJB", SeleccionMovilDoctos.class);
  
  if("1".equals(strCboMoneda) || "".equals(strCboMoneda) || "0".equals(strCboMoneda)){
    // Pruebas Modulo 1
    //out.print( "<span style=\"color:green;\" ><b>Pruebas Modulo 1</b></span><br>");
    respuesta 	= seleccionDocumento.getResumenOperacionesRealizadasPorEPO(strCveIF,"","","1");
    bHayMensajeEPOMN = new Boolean((String)respuesta.get("hayMensaje"));

    aRegistrosMN = (ArrayList)respuesta.get("listaOperaciones");
    
    //out.print("<br>aListaEpos.size:"+aRegistrosMN.size()+"<br><br><br>");
    //object 		= JSONObject.fromObject(respuesta);
    //out.print( 	object.toString() +"<br>");
  }
  if("54".equals(strCboMoneda) || "".equals(strCboMoneda) || "0".equals(strCboMoneda)){  
    respuestaUSD 	= seleccionDocumento.getResumenOperacionesRealizadasPorEPO(strCveIF,"","","54");
    bHayMensajeEPOUSD = new Boolean((String)respuestaUSD.get("hayMensaje"));
    aRegistrosUSD = (ArrayList)respuestaUSD.get("listaOperaciones");
    
    //objectUSD 		= JSONObject.fromObject(respuestaUSD);
    //out.print( 	objectUSD.toString() +"<br>");
    //out.print( "<br>" );
   }
%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
  "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head profile="http://www.w3.org/2005/10/profile">
	<title><%=Constantes.SITE_TITLE%></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <meta name="HandheldFriendly" content="true" />
	<meta name="Description" content="bienvenido a nacional financiera portatil" />
	<meta name="Keywords" content="nafinet,movil,cadenas productivas,nacional financiera banca de desarrollo" />
	<meta name="Copyright" content="Nacional Financiera SNC" />
	<meta name="distribution" content="Global" />
	<meta name="Robots" content="index,follow" />
	<meta name="city" content="Mexico City" />
	<meta name="country" content="Mexico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Pragma" content="NO-CACHE" />
	<meta http-equiv="Expires" content="0" />
	<noscript><meta http-equiv="refresh" content="0; URL=noscript.jsp" /></noscript>
	<link href="css/general<%=(isIPad?"_ipad":"")%>.css" rel="stylesheet" type="text/css" />
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>

<script type="text/javascript">
<!--
function envia(strXHTML){
	document.forms["frmEPO"].action=strXHTML;
}

function selectMoneda(){
	document.forms["frmEPO"].action='menu.xhtml'; 
	document.frmEPO.submit();
}

function asignaEPO(strEPO){
	if(document.frmEPO.cboMoneda.value==0){
		alert("Debe seleccionar una moneda");
		return false;
	}
	else{
		document.frmEPO.txtEPO.value=strEPO;
		document.frmEPO.action='SelectDoctosS';
		return true;
	}
}
function updateOrientation() {
  if (window.orientation == 90 || window.orientation == -90) {
    document.getElementById('header').src="images/header480.jpg";
    document.getElementById('header').width=screen.height;
  }
  else {
    document.getElementById('header').width=screen.width;
  }
}
// -->
</script>

<body onorientationchange="updateOrientation()">
<form method="post" action="" name="frmEPO">
<input name="txtEPO" value="" type="hidden"></input>
<input name="txtPyme" value="<%=(usrBasico.getCveAfiliado())%>" type="hidden"></input>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EEEEEE">
	<tr>
		<td>
  			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TablaBlanca">
 				<tr>
					<td align="center"><img id="header" src="<%=((isIPad || isFirefox || isMSIE || isChrome)?Constantes.LOGO_NAFINET_IPAD:Constantes.LOGO_NAFINET)%>" "Nafinet M&oacute;vil"/></td>
				</tr>
				<tr>
					<td align="left">
						<table width="100%" border="0">
							<tr>
								<td align="left">
									<table width="98%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="left" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="identificacion">
													<tr>
														<td>Bienvenido:<%=usrBasico.getNombre()+" "+usrBasico.getApellidoPaterno()+" "+usrBasico.getApellidoMaterno()%><span>&nbsp;</span>          </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="right" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="breadcrumbR">
													<tr>
														<td>INICIO/SALDO/<span class="breadcrumbS">RESUMEN OPERACI&Oacute;N DEL D&Iacute;A</span></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="sub_h2">RESUMEN OPERACI&Oacute;N DEL D&Iacute;A</td>
							</tr>
							<tr>
								<td align="center">
									<table border="1">
<%
         if("1".equals(strCboMoneda) || "".equals(strCboMoneda) || "0".equals(strCboMoneda)){
%>
										<tr align="center" class="menu">
											<td colspan="4">Moneda Nacional</td>
										</tr>
<%
           if(bHayMensajeEPOMN.booleanValue()){
%>
										<tr align="center">
											<td colspan="4"><%=respuesta.get("mensaje")%></td>
										</tr>
<%
           
          }else if(aRegistrosMN.size() == 0){
%>
										<tr align="center">
											<td colspan="4">
                        No hay operaci&oacute;n del d&iacute;a de hoy o hasta el momento, 
                        para buscar en otro periodo presiona el bot&oacute;n de Consultar
                      </td>
										</tr>
<%
          }else{
%>
                    <tr align="center" class="subtitulo">
                      <td></td>
                      <td>Pymes</td>
                      <td>Monto</td>
                      <td>Doctos</td>
                    </tr>
<%
             for(int i=0;i<aRegistrosMN.size();i++){
               hEPOMN=(HashMap)aRegistrosMN.get(i);
               strTipoCadena = (String)hEPOMN.get("nombreIF");
               strNumeroDoctosFormateado = (String)hEPOMN.get("numeroDoctosFormateado");
               strNumeroPymesFormateado = (String)hEPOMN.get("numeroPymesFormateado");
               strMontoTotalFormateado = (String)hEPOMN.get("montoTotalFormateado");

%>
										<tr align="center">
											<td><%=strTipoCadena%></td>
											<td><%=strNumeroPymesFormateado%></td>
											<td><%=strMontoTotalFormateado%></td>
											<td><%=strNumeroDoctosFormateado%></td>
										</tr>
<%
            }
          }
        }if("54".equals(strCboMoneda) || "".equals(strCboMoneda) || "0".equals(strCboMoneda)){
%>
										<tr align="center" class="menu">
											<td colspan="4">D&oacute;lares</td>
										</tr>
<%
           if(bHayMensajeEPOUSD.booleanValue()){
%>
										<tr align="center">
											<td colspan="4"><%=respuestaUSD.get("mensaje")%></td>
										</tr>
<%
           }else if(aRegistrosUSD.size() == 0){
%>
										<tr align="center">
											<td colspan="4">
                        No hay operaci&oacute;n del d&iacute;a de hoy o hasta el momento, 
                        para buscar en otro periodo presiona el bot&oacute;n de Consultar
                      </td>
										</tr>
<%
           }else{
%>
                    <tr align="center" class="subtitulo">
                      <td></td>
                      <td>Pymes</td>
                      <td>Monto</td>
                      <td>Doctos</td>
                    </tr>
<%
             for(int i=0;i<aRegistrosUSD.size();i++){
               hEPOUSD=(HashMap)aRegistrosUSD.get(i);
               strTipoCadena = (String)hEPOUSD.get("tipoCadena");
               strNumeroDoctosFormateado = (String)hEPOUSD.get("numeroDoctosFormateado");
               strNumeroPymesFormateado = (String)hEPOUSD.get("numeroPymesFormateado");
               strMontoTotalFormateado = (String)hEPOUSD.get("montoTotalFormateado");
            
%>
										<tr align="center">
											<td><%=strTipoCadena%></td>
											<td><%=strNumeroPymesFormateado%></td>
											<td><%=strMontoTotalFormateado%></td>
											<td><%=strNumeroDoctosFormateado%></td>
										</tr>
<%
          }
        }
      }
%>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td align="center">
                  <input type="submit" value="Consultar" class="button" onclick="envia('consultaepo.xhtml')"/></input>
                  <input type="submit" value="  Salir  " class="button" onclick="envia('logout.xhtml')"/></input>
                </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">
						<br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="right" class="texto1footer">Enlaces del Gobierno Federal</td>
                <td width="50" rowspan="2"><img src="images/footer_r1_c2.gif" alt="" width="49" height="43" /></td>
              </tr>
              <tr>
                <td class="texto1footer2"><%=Constantes.FOOTER%></td>
              </tr>
            </table>
            <br/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
<!--
if (window.orientation == 90 || window.orientation == -90) {
  document.getElementById('header').src="images/header480.jpg";
  document.getElementById('header').width=screen.height;
}
else {
  document.getElementById('header').width=screen.width;
}
// -->
</script>
<%
}catch(Exception e){
	e.printStackTrace();
}
%>
</body>
</html>