<jsp:include page="sesion.jsp"/>
<%@ page import="com.nafin.nafinetmovil.utils.UsuarioBasico"%>
<%@ page import="com.nafin.nafinetmovil.utils.Constantes"%>
<%@ page 
	import="
		java.lang.*, 
		java.util.*, 
		com.netro.descuento.movil.*" 
	errorPage="error.jsp"%>

<%
String ua = request.getHeader( "User-Agent" );
boolean isBlackberry = ( ua != null && ua.indexOf( "Blackberry" ) != -1 );
boolean isIPad = ( ua != null && ua.indexOf( "iPad" ) != -1 );
boolean isIPhone = ( ua != null && (ua.indexOf( "iPhone" ) != -1 || ua.indexOf( "iPod" ) != -1) );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
boolean isChrome = ( ua != null && ua.indexOf( "Chrome" ) != -1 );
%>

<%
UsuarioBasico usrBasico=(UsuarioBasico)session.getAttribute("user");
String strCboIF=(String)(request.getAttribute("cboIF"));
//System.out.println("cve_confirma.jsp:strCboIF: "+strCboIF);
String strPyme=(String)(request.getAttribute("txtPyme")); 
//System.out.println("cve_confirma.jsp:txtPyme: "+strPyme);
String strEPO=(String)(request.getAttribute("txtEPO")); 
//System.out.println("cve_confirma.jsp:txtEPO: "+strEPO);
String strMoneda=(String)(request.getAttribute("cboMoneda")); 
//System.out.println("cve_confirma.jsp:cboMoneda: "+strMoneda);
String [] strParameterValuesChk=(String [])(request.getAttribute("strParameterValuesChk")); 
String strClave=(String)(request.getAttribute("txtClave1")); 
//System.out.println("cve_confirma.jsp:txtClave1: "+strClave);
String strFechaSigDia=(String)(request.getAttribute("txtFechaSigDia")); 
System.out.println("cve_confirma.jsp:txtFechaSigDia: "+strFechaSigDia);

Boolean bHayMensaje = (request.getAttribute("bHayMensaje")!=null?(Boolean)request.getAttribute("bHayMensaje"):new Boolean(false));
Boolean bHayError = (request.getAttribute("bHayError")!=null?(Boolean)request.getAttribute("bHayError"):new Boolean(false));
String strMensaje = (String)request.getAttribute("strMensaje");
%>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
  "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head profile="http://www.w3.org/2005/10/profile">
	<title><%=Constantes.SITE_TITLE%></title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <meta name="HandheldFriendly" content="true" />
	<meta name="Description" content="bienvenido a nacional financiera portatil" />
	<meta name="Keywords" content="nacional financiera banca de desarrollo" />
	<meta name="Copyright" content="Nacional Financiera SNC" />
	<meta name="distribution" content="Global" />
	<meta name="Robots" content="index,follow" />
	<meta name="city" content="Mexico City" />
	<meta name="country" content="Mexico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/general.css" rel="stylesheet" type="text/css" />
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<script type="text/javascript">
<!--
<%
  if(bHayMensaje.booleanValue() && bHayError.booleanValue()){
%>
    alert('<%=strMensaje%>');
    window.location.href ='PreAcuseS'; 
<%
  }
%>

function envia(strXHTML){
  document.forms["frmEPO"].action=strXHTML;
}

function validaCve(){
  if(document.forms["frmEPO"].txtClave1.value != document.forms["frmEPO"].txtClave2.value){
    alert("No coinciden las claves por favor verificalo");
    return false;
  }
  if(document.forms["frmEPO"].txtClave1.value.length != 8 || document.forms["frmEPO"].txtClave2.value.length != 8){
    alert("Debe de ser de 8 carateres alfanumericos");
    return false;
  }
  else{
    return true;
  }
}

function updateOrientation() {
  if (window.orientation == 90 || window.orientation == -90) {
    document.getElementById('header').src="images/header480.jpg";
    document.getElementById('header').width=screen.height;
  }
  else {
    document.getElementById('header').width=screen.width;
  }
}

// -->
</script>

<body onLoad="backButtonOverride()" onorientationchange="updateOrientation()">
<form method="post" action="CveConfirmaS" name="frmEPO">
<input name="cboIF" value="<%=strCboIF%>" type="hidden"></input>
<input name="cboMoneda" value="<%=strMoneda%>" type="hidden"></input>
<input name="txtEPO" value="<%=strEPO%>" type="hidden"></input>
<input name="txtPyme" value="<%=(usrBasico.getCveAfiliado())%>" type="hidden"></input>
<input name="chkP" value="<%for(int i=0;i<strParameterValuesChk.length;i++){out.print(strParameterValuesChk[i]);}%>" type="hidden"></input>}
<input name="txtFechaSigDia" value="<%=strFechaSigDia%>" type="hidden"></input>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#EEEEEE">
	<tr>
		<td>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TablaBlanca">
				<tr>
					<td align="center"><img id="header" src="<%=((isIPad || isFirefox || isMSIE || isChrome)?Constantes.LOGO_NAFINET_IPAD:Constantes.LOGO_NAFINET)%>" "Nafinet M&oacute;vil"/></td>
				</tr>
				<tr>
					<td align="left">
						<table width="100%" border="0">
							<tr>
								<td align="left">
									<table width="98%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="left" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="identificacion">
													<tr>
														<td>Usuario:<%=usrBasico.getNombre()+" "+usrBasico.getApellidoPaterno()+" "+usrBasico.getApellidoMaterno()%><span>&nbsp;</span> </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="right" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" class="breadcrumbR">
													<tr>
														<td>INICIO/SALDO/<span class="breadcrumbS">CLAVE DE CONFIRMACI&Oacute;N</span></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center">&nbsp;</td>
							</tr>
							<tr>
								<td class="sub_h2">GENERACI&Oacute;N DE CLAVE DE CONFIRMACI&Oacute;N DE OPERACIONES</td>
							</tr>
							<tr>
								<td class="msj">Usted no tiene la clave para la confirmaci&oacute;n de las operaciones de cesi&oacute;n de derechos, favor de capturarla</td>
							</tr>
							<tr>
								<td align="center">
									<table border="0">
										<tr align="center" class="menu">
											<td>Pol&iacute;ticas de la contrase&ntilde;a</td>
										</tr>
										<tr>
											<td>
												<ul>
													<li>Debe contener tanto n&uacute;meros como letras, debe ser igual a 8 posiciones.</li>
													<li>Se permite el uso de may&uacute;sculas y min&uacute;sculas.</li>
													<li>No se permite el uso de letras acentuadas, caracteres especiales como la &ntilde;, $, etc.</li>
													<li>El usuario o parte del usuario no debe de estar contenido en la contrase&ntilde;a</li>
													<li>No debe de contener m&aacute;s de 2 n&uacute;meros iguales consecutivos (ej. 12223445)</li>
													<li>No debe de contener m&aacute;s de 2 letras iguales consecutivos (ej. bbbccd)</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td align="center">
												<table>
													<tr>
														<td class="home_titulo">Clave:</td>
														<td><input name="txtClave1" class="redondear" type="password" size="10" maxlength="8" /></input></td>
													</tr>
													<tr>
														<td class="home_titulo">Confirmar clave:</td>
														<td><input name="txtClave2" class="redondear" type="password" size="10" maxlength="8" /></input></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr align="center">
											<td>
                        <input name="login_submit" value="  Enviar  " type="submit" size="26" onclick="return validaCve();" class="button" /></input>
                        <input name="login_submit" value="  Salir  " type="submit" class="button" onclick="envia('logout.xhtml')"/></input>
                      </td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="right" class="texto1footer">Enlaces del Gobierno Federal</td>
                <td width="50" rowspan="2"><img src="images/footer_r1_c2.gif" alt="" width="49" height="43" /></td>
              </tr>
              <tr>
                <td class="texto1footer2"><%=Constantes.FOOTER%></td>
              </tr>
            </table>
            <br/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
<!--
if (window.orientation == 90 || window.orientation == -90) {
  document.getElementById('header').src="images/header480.jpg";
  document.getElementById('header').width=screen.height;
}
else {
  document.getElementById('header').width=screen.width;
}
// -->
</script>
</body>
</html>