<%@ page language="java" import="oracle.security.sso.enabler.*" errorPage="/00utils/error.jsp"%>
<jsp:useBean id="ssoObj" scope="application" class="com.chermansolutions.oracle.sso.partnerapp.beans.SSOEnablerJspBean" />
<%--
Nota:
Este archivo para los contenedores de versión 10g debe estar en la raiz del directorio web.
Ya que si se deja donde originalmente se encontraba ("00utils/sso") por alguna razón no establece
adecuadamente la cookie y entonces nunca se pueda entrar al sistema.
En conclusión:
- ssosignon.jsp debe estra en raiz del web.
- El registro de la parter application "on success" debe ser la ruta del ssosignon.jsp anterior
- Se debe parar y levantar el "single sign on" (OC4J_SECURITY) para que tomen efecto los cambios. (restart no funciona en ocasiones. Mejor usar stop y start)
--%>
<%
    System.out.println("ssosignon::Establece la cookie");
    try {
		ssoObj.setPartnerAppCookie(request, response);
	} catch(Exception e) {
		System.out.println("ssosignon.jsp::" + e.getMessage());
		throw new Exception("El tiempo para indicar el usuario y contraseña ha sido excedido. Cierre esta ventana e intente nuevamente");
	}
%>
    
