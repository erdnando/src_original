<%@ page language="java" %>
<%@ page import="
	java.util.*,
	com.netro.pdf.*,
	netropology.utilerias.*,
	com.netro.seguridadbean.*,
	com.netro.afiliacion.*,
	com.netro.descuento.*,
	com.netro.garantias.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../29secsession.jspf" %>
<jsp:useBean id="consulta" scope="page" class="com.netro.garantias.ConsSolicIFGAbean" />

<%
Calendar calendario = Calendar.getInstance();
SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
String fechaHoy = formatoFecha.format(calendario.getTime());
String programaC = request.getParameter("Hprograma") == null?"":(String)request.getParameter("Hprograma");
String folioC = request.getParameter("folio") == null?"":(String)request.getParameter("folio");
String fecha_oper_ini = request.getParameter("fecha_oper_ini") == null?fechaHoy:(String)request.getParameter("fecha_oper_ini");
String fecha_oper_fin = request.getParameter("fecha_oper_fin") == null?fechaHoy:(String)request.getParameter("fecha_oper_fin");
String cbo_situacion = request.getParameter("cbo_situacion") == null?"":(String)request.getParameter("cbo_situacion");
String flagConsulta = request.getParameter("flagConsulta") == null?"":(String)request.getParameter("flagConsulta");
String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String resultado="";

if(	informacion.equals("Consulta")	){
	AccesoDB con = new AccesoDB();
   PreparedStatement	ps = null;
   ResultSet	rs = null;
	int numVar=3;
   String qrySentencia	= "";
   int vigencia = 0;
   int i = 0;
   try {
      con.conexionDB();
      consulta.setiTipoOperacion(1);
      consulta.setiIF(Integer.parseInt(iNoCliente));
      consulta.setFechaOperIni(fecha_oper_ini);
      consulta.setFechaOperFin(fecha_oper_fin);
      consulta.setSituacion(cbo_situacion);
		consulta.setPrograma(programaC);
		consulta.setFolio(folioC);
		consulta.setEnviadoSiag("N");
      
		qrySentencia = consulta.getQuerySolicitudes();
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1, 1); //Tipo de Operaci�n
		ps.setInt(2, Integer.parseInt(iNoCliente)); //IF
	  /*FODEA 000 - 2009 garantias*/
	  if(!fecha_oper_ini.equals("") && !fecha_oper_fin.equals("")){
		 ps.setString(3, fecha_oper_ini);
		 ps.setString(4, fecha_oper_fin);
		 numVar++;
		 numVar++;
	  }
	  if(fecha_oper_ini.equals("") && fecha_oper_fin.equals("") && !cbo_situacion.equals("")){ps.setInt(3, Integer.parseInt(cbo_situacion));numVar++;}
	  if(!fecha_oper_ini.equals("") && !fecha_oper_fin.equals("") && !cbo_situacion.equals("")){ps.setInt(5, Integer.parseInt(cbo_situacion));numVar++;}
	  /*FODEA 000 - 2009 garantias*/
	 
	 if(!folioC.equals("")){
		ps.setLong(numVar, Long.parseLong(folioC));
		numVar++;
	  }
	  if(!programaC.equals("")){
		ps.setString(numVar, programaC);
		numVar++;
	  }
	  
	 
	 
	  //Enviado SIAG 'N'
		ps.setString(numVar,"N");
		numVar++;
	  
	  
		rs = ps.executeQuery();
		List reg = new ArrayList();
		HashMap datos;
		while(rs.next()){
			String tipo_envio = (rs.getString("tipo_envio")==null)?"":rs.getString("tipo_envio");/*FODEA 000 - 2009 garantias*/
			String fecha = (rs.getString("fecha")==null)?"":rs.getString("fecha");
			String folio = (rs.getString("ic_folio")==null)?"":rs.getString("ic_folio");
			String situacion	= (rs.getString("situacion")==null)?"":rs.getString("situacion");
			String monto_enviado	= (rs.getString("monto_enviado")==null)?"":rs.getString("monto_enviado");/*FODEA 000 - 2009 garantias*/
			int 	reg_acep	= rs.getInt("in_registros_acep");
			int 	reg_rech	= rs.getInt("in_registros_rech");
			int 	num_reg		= rs.getInt("in_registros");
			int 	ic_situacion= rs.getInt("ic_situacion");
			String programa = (rs.getString("programa")==null)?"":(rs.getString("programa").equals("G")?"Garantías":"Crédito Educativo");
         String decide_archivos_errores;
         if(reg_rech>0 && ic_situacion==5){
            decide_archivos_errores = "true";
         }else{
            decide_archivos_errores = "false";
         }
			datos = new HashMap();
			datos.put("TIPO_ENVIO",tipo_envio.equals("proc_aut")?"AUTOMATICO":"MANUAL");
			datos.put("FECHA",fecha);
			datos.put("FOLIO",folio);
			datos.put("SITUACION",situacion);
			datos.put("IC_SITUACION",Integer.toString(ic_situacion));
			datos.put("MONTO_ENVIADO",monto_enviado);
			datos.put("REG_ACEP",Integer.toString(reg_acep));
			datos.put("REG_RECH",Integer.toString(reg_rech));
			datos.put("NUM_REG",Integer.toString(num_reg));
         datos.put("DECIDE_RESULTADOS",(ic_situacion==5)?"true":"false");
         datos.put("DECIDE_ARCHIVOS_ERRORES",decide_archivos_errores);
			datos.put("PROGRAMA",programa);
			reg.add(datos);
         i++;
		}
		ps.close();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		resultado = jsonObj.toString();	
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}finally{
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}
}

else if(informacion.equals("ArchivoTexto")){
   String folio = request.getParameter("folio");
   String opcion = request.getParameter("opcion");
   String clave_if = request.getParameter("clave_if")!=null?request.getParameter("clave_if"):"";
   
   AccesoDB con = new AccesoDB();
   PreparedStatement	ps = null;
   ResultSet rs = null;
   String qrySentencia	= "";
   String tabla = "";
   String error = "";
   int vigencia = 0;
   int i = 0;
   
   CreaArchivo archivo = new CreaArchivo();
   StringBuffer contenidoArchivo = new StringBuffer("");
   String nombreArchivo = null;
   
   try {
      con.conexionDB();
      if(opcion.equals("origen")){
       tabla = "gti_contenido_arch";
     }else{
       tabla = "gti_arch_rechazo";
     }
     
     if(!"".equals(clave_if))
      iNoCliente = clave_if;
     
      qrySentencia =
         "select cg_contenido"+
         " from "+tabla+" A"+
         " ,comcat_if I"+
         " where A.ic_if_siag = I.ic_if_siag"+
         " and A.ic_folio = ?"+
         " and I.ic_if = ?"+
         " order by A.ic_linea";
      
      ps = con.queryPrecompilado(qrySentencia);
     ps.setLong(1, Long.parseLong(folio));
      ps.setLong(2, Long.parseLong(iNoCliente));
      
      rs = ps.executeQuery();
      while(rs.next()){
         contenidoArchivo.append(rs.getString("cg_contenido")+"\n");
         i++;
      }
      ps.close();
      
      if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt")){
         System.err.println("<--!Error al generar el archivo-->");
         error = "Error al generar el archivo";
      }else
         nombreArchivo = archivo.nombre;

   }catch(Exception e){
      error = e + "";
      e.printStackTrace();
      throw e;
   }finally{
      if(con.hayConexionAbierta())
         con.cierraConexionDB();
   }
   JSONObject jsonObj = new JSONObject();
   jsonObj.put("success", new Boolean(true));
   jsonObj.put("result", error);
   jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
   resultado = jsonObj.toString();
}

else if(informacion.equals("ArchivoPdf")){
   String error = "";
   com.netro.seguridadbean.Seguridad BeanSegFacultad = null;
   try {
      BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
   } catch (Exception e) {
      System.err.println(e);
      error = "EXISTEN DIFICULTADES TECNICAS, INTENTE M�S TARDE";
   }
   
   //String clave_if = request.getParameter("clave_if");
   String clave_if = iNoCliente;
   String folio = request.getParameter("folio");
   String origen = "CONSALTA";//request.getParameter("origen");
   String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);
   AccesoDB con = new AccesoDB();
   PreparedStatement	ps	= null;
   ResultSet rs	= null;
   String qrySentencia	= "";
   String tabla = "";
   CreaArchivo archivo = new CreaArchivo();
   String nombreArchivo = archivo.nombreArchivo()+".pdf";
   int i = 0;
   ComunesPDF pdfDoc = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
   String icIfSiag = "";
   String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
   String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
   String diaActual    = fechaActual.substring(0,2);
   String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
   String anioActual   = fechaActual.substring(6,10);
   String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
   
   pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                                    ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
                            (String)session.getAttribute("sesExterno"),
                            
                                    (String) session.getAttribute("strNombre"),
                                       (String) session.getAttribute("strNombreUsuario"),
                            (String)session.getAttribute("strLogo"),
                            strDirectorioPublicacion);
   
   pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
   
   try{
      con.conexionDB();
      
      
      qrySentencia =
         "select "+
         " ES.ic_folio"+
         " ,I.cg_razon_social as NOMBRE_IF"+
         " ,to_char(df_validacion_siag,'dd/mm/yyyy') as FECHA_PROCESO "+
         " ,to_char(df_autorizacion_siag,'dd/mm/yyyy') as FECHA_AUTORIZA "+
         " ,NVL(ES.in_registros_acep,0) as ACEP"+
         " ,NVL(ES.in_registros_rech,0) as RECH"+
         " ,NVL(ES.in_registros,0) as TOTAL"+
         " ,es.IC_IF_SIAG"+
         " ,es.ic_situacion"+
         " ,es.ig_anio_trim_calif"+
         " ,es.ig_trim_calif"+
         " ,nvl(es.CG_USUARIO_ACTUALIZA, ' ') as CG_USUARIO_ACTUALIZA"+
         " ,cs.cg_descripcion "+
         " from gti_estatus_solic ES"+
         " ,comcat_if I"+
         " ,gticat_situacion cs"+
         " where ES.ic_if_siag = I.ic_if_siag"+
         " and ES.IC_SITUACION = cs.IC_SITUACION"+
         " and ES.IC_TIPO_OPERACION = cs.IC_TIPO_OPERACION"+
         " and ES.ic_folio = ?"+
         " and I.ic_if = ?";
      
      ps = con.queryPrecompilado(qrySentencia);
      ps.setString(1,folio);
      if("4".equals(cvePerf)){
      ps.setString(2,clave_if);
      } else {
      ps.setString(2,iNoCliente);
      }
      rs = ps.executeQuery();
      if(rs.next()){
         int situacion = rs.getInt("ic_situacion");
         icIfSiag = rs.getString("IC_IF_SIAG");
         pdfDoc.setTable(1,100);
         if("CONSALTA".equals(origen)) {
            pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
            pdfDoc.setCell("RESULTADOS DE SOLICITUD DE ALTA DE GARANTIAS","celda04",ComunesPDF.CENTER);
         }else if("CONSSALCOM".equals(origen)){
            pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
            pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE SALDOS Y PAGO DE COMISIONES","celda04",ComunesPDF.CENTER);		
         }else if("CONSCALIF".equals(origen)){
            pdfDoc.addText("\nA�o: "+rs.getString("IG_ANIO_TRIM_CALIF")+"   Trimestre: "+rs.getString("IG_TRIM_CALIF")+"\n","formas",ComunesPDF.CENTER);
            pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE CALIFICACION DE CARTERA","celda04",ComunesPDF.CENTER);		
         }else if("CONSREC".equals(origen)){
            pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
            pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE RECUPERACIONES","celda04",ComunesPDF.CENTER);		
         }else{
            pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
            pdfDoc.setCell("RESULTADOS","celda04",ComunesPDF.CENTER);
         }
         pdfDoc.addTable();
   
         if(!"CONSSALCOM".equals(origen)){
            if("CONSREC".equals(origen)){
               String aux = situacion +" " +rs.getString("cg_descripcion") + "\n Fecha de Operaci�n en Firme:" + ((rs.getString("FECHA_PROCESO")==null)?"":rs.getString("FECHA_PROCESO")) +"\n"; 
               pdfDoc.setTable(2);    
               pdfDoc.setCell("USUARIO: "+rs.getString("CG_USUARIO_ACTUALIZA"),"formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.    
               pdfDoc.setCell(aux,"formas",ComunesPDF.RIGHT,1,1,0);//El parametro 0, es para que la celda no tenga borde.    
               pdfDoc.addTable();
            } else {
               if(rs.getString("FECHA_PROCESO") == null ){
                  pdfDoc.addText("Fecha de Operaci�n en Firme: \n","formas",ComunesPDF.RIGHT);
               }else{
                  pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_PROCESO")+"\n","formas",ComunesPDF.RIGHT);
               }
            }
         }else{
               if(situacion ==5){
                  if(rs.getString("FECHA_AUTORIZA") == null ){
                     pdfDoc.addText("Fecha de Operaci�n en Firme: \n","formas",ComunesPDF.RIGHT);
                  }else{
                     pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_AUTORIZA")+"\n","formas",ComunesPDF.RIGHT);
                  }
               }else{
                  if (rs.getString("FECHA_PROCESO") == null ){
                     pdfDoc.addText("Fecha de C�lculo de Comisiones: \n","formas",ComunesPDF.RIGHT);
                  }else{
                     pdfDoc.addText("Fecha de C�lculo de Comisiones: "+rs.getString("FECHA_PROCESO")+"\n","formas",ComunesPDF.RIGHT);
                  }
              }
         }
         pdfDoc.setTable(5,100);
            pdfDoc.setCell("FOLIO DE LA OPERACION","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("INTERMEDIARIO FINANCIERO","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS","celda01",ComunesPDF.CENTER);
   
            pdfDoc.setCell(rs.getString("IC_FOLIO"),"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(rs.getString("NOMBRE_IF"),"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("ACEP"),0),"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("RECH"),0),"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("TOTAL"),0),"formas",ComunesPDF.CENTER);			
   
         pdfDoc.addTable();
      }
      ps.close();	
   
      pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);	
      if("CONSALTA".equals(origen)) {
         qrySentencia =
            " select ic_programa"   +
            "  ,fn_porc_comision"   +
            "  ,in_registros_acep"   +
            "  ,in_registros_rech"   +
            "  ,in_registros "   +
            "  ,NVL(FN_PORC_GARANTIZADO, 0) FN_PORC_GARANTIZADO "+
            " from gti_det_alta_gtia GD"   +
            " , comcat_if I"   +
            " where GD.ic_if_siag = I.ic_if_siag" +
            " and GD.ic_folio = ?"+
            " and I.ic_if = ?";
         
         ps = con.queryPrecompilado(qrySentencia);
         ps.setString(1,folio);
         if("4".equals(cvePerf)){
         ps.setString(2,clave_if);
         } else {
         ps.setString(2,iNoCliente);
         }		
         rs = ps.executeQuery();
         while(rs.next()){
            if(i==0){
               pdfDoc.setTable(1,100);
               pdfDoc.setCell("DETALLE DE GARANTIAS POR PROGRAMA","celda04",ComunesPDF.CENTER);
               pdfDoc.addTable();
               pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
               pdfDoc.setTable(6,100);
               pdfDoc.setCell("CLAVE DEL PROGRAMA","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("PORCENTAJE DE COMISION","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("PORCENTAJE GARANTIZADO","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS","celda01",ComunesPDF.CENTER);
            }
      
            pdfDoc.setCell(rs.getString("IC_PROGRAMA")+" ","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("FN_PORC_COMISION"),4)+" ","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("FN_PORC_GARANTIZADO"),2)+" ","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS_ACEP"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS_RECH"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS"),0)+" ","formas",ComunesPDF.CENTER);
            i++;
         }	// while
         ps.close();
      }// ALTA
      else if("CONSSALCOM".equals(origen)) {
         
         // FECHA DE OPERACION EN FIRME ( COMISIONES POR MONEDA )
         qrySentencia =
            "  SELECT "   +
            " 		to_char(DF_FECHA_FIRME,'dd/mm/yyyy') AS FECHA_OPERACION "  +
            "  FROM GTI_DETALLE_SALDOS ds"   +
            "   ,comcat_moneda m"   +
            "   WHERE ds.ic_moneda = m.ic_moneda(+)"   +
            "   and ic_if_siag = ? "+
            " AND ic_folio = ? "  +
            " AND ds.cs_comision = 'S' "  +
            " order by decode(m.ic_moneda,1,1,54,2,3)";
         ps = con.queryPrecompilado(qrySentencia);
         ps.setString(1,icIfSiag);
         ps.setString(2,folio);
         rs = ps.executeQuery();
         if(rs.next()){
            if(rs.getString("FECHA_OPERACION") == null ){
               pdfDoc.addText("Fecha de Operaci�n en Firme: ","formas",ComunesPDF.RIGHT);
            }else{
               pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_OPERACION")+"","formas",ComunesPDF.RIGHT);
            }
         }
         rs.close();ps.close();
         
         // DETALLE DE COMISIONES POR MONEDA
         qrySentencia =
            "  SELECT nvl(m.cd_nombre,'NA') as cd_nombre,"   +
            "  		in_aceptados,"   +
            "         in_rechazados,"   +
            " 		in_tot_registros, "   +
            " 		fn_impte_autoriza"   +
            "  FROM GTI_DETALLE_SALDOS ds"   +
            "   ,comcat_moneda m"   +
            "   WHERE ds.ic_moneda = m.ic_moneda(+)"   +
            "   and ic_if_siag = ? "+
            " AND ic_folio = ? "  +
            " AND ds.cs_comision = 'S' "  +
            " order by decode(m.ic_moneda,1,1,54,2,3)";
         ps = con.queryPrecompilado(qrySentencia);
         ps.setString(1,icIfSiag);
         ps.setString(2,folio);
         rs = ps.executeQuery();
         while(rs.next()){
            if(i==0){
               pdfDoc.setTable(1,100);
               pdfDoc.setCell("DETALLE DE COMISIONES POR MONEDA","celda04",ComunesPDF.CENTER);
               pdfDoc.addTable();
               pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
               pdfDoc.setTable(5,100);
               pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
            }
      
            pdfDoc.setCell(rs.getString("cd_nombre")+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
            i++;
         }	// while
         rs.close();ps.close();
         if(i>0){
            pdfDoc.addTable();
         }
         
         // FECHA DE OPERACION EN FIRME ( SIN COMISIONES POR MONEDA )
         qrySentencia =
            "  SELECT "   +
            " 		to_char(DF_FECHA_FIRME,'dd/mm/yyyy') AS FECHA_OPERACION "  +
            "  FROM GTI_DETALLE_SALDOS ds"   +
            "   ,comcat_moneda m"   +
            "   WHERE ds.ic_moneda = m.ic_moneda(+)"   +
            "   and ic_if_siag = ? "+
            " AND ic_folio = ? "  +
            " AND ds.cs_comision = 'N' "  +
            " order by decode(m.ic_moneda,1,1,54,2,3)";
         ps = con.queryPrecompilado(qrySentencia);
         ps.setString(1,icIfSiag);
         ps.setString(2,folio);
         rs = ps.executeQuery();
         if(rs.next()){
            pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);	
            if(rs.getString("FECHA_OPERACION") == null ){
               pdfDoc.addText("Fecha de Operaci�n en Firme: ","formas",ComunesPDF.RIGHT);
            }else{
               pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_OPERACION")+"","formas",ComunesPDF.RIGHT);
            }
         }
         rs.close();ps.close();
         
         // DETALLE SIN COMISIONES POR MONEDA
         i = 0;
         qrySentencia =
            "  SELECT nvl(m.cd_nombre,'NA') as cd_nombre,"   +
            "  		in_aceptados,   "   +
            "         in_rechazados, "   +
            " 		in_tot_registros,  "   +
            " 		fn_impte_autoriza  "   +
            "  FROM GTI_DETALLE_SALDOS ds "   +
            "   ,comcat_moneda m "   +
            "   WHERE ds.ic_moneda = m.ic_moneda(+) "   +
            "   and ic_if_siag = ? "+
            " AND ic_folio = ? "  +
            " AND ds.cs_comision = 'N' "  +
            " order by decode(m.ic_moneda,1,1,54,2,3)";
         ps = con.queryPrecompilado(qrySentencia);
         ps.setString(1,icIfSiag);
         ps.setString(2,folio);
         rs = ps.executeQuery();
         while(rs.next()){
            if(i==0){
               pdfDoc.setTable(1,100);
               pdfDoc.setCell("DETALLE SIN COMISIONES POR MONEDA","celda04",ComunesPDF.CENTER);
               pdfDoc.addTable();
               pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
               pdfDoc.setTable(5,100);
               pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
            }
      
            pdfDoc.setCell(rs.getString("cd_nombre")+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
            i++;
         }	// while
         rs.close();ps.close();
         
      }//CONSSALCOM 
      else if("CONSREC".equals(origen)) {
         qrySentencia =
            "  SELECT nvl(m.cd_nombre,'NA') as cd_nombre,"   +
            "  		SUM(in_aceptados) AS in_aceptados,"   +
            "       SUM(in_rechazados) AS in_rechazados,"   +
            " 		SUM(in_tot_registros) AS in_tot_registros, "   +
            " 		SUM(fn_impte_deposito) AS fn_impte_deposito, "   +
            " 		SUM(fn_impte_calculado) AS fn_impte_calculado, "   +
            " 		SUM(fn_diferencia) AS fn_diferencia, "   +
            " 		SUM(nvl(fn_impte_complemento,0)) AS fn_impte_complemento, "   +
            " 		decode(m.ic_moneda,1,1,54,2,3) "   +
            "  FROM GTI_DETALLE_RECUPERACIONES d "   +
            "   ,comcat_moneda m "   +
            "   WHERE d.ic_moneda = m.ic_moneda(+) "   +
            "   and ic_if_siag = ? "+
            "   AND ic_folio = ?"  +
            "   GROUP BY nvl(m.cd_nombre,'NA'), decode(m.ic_moneda,1,1,54,2,3)"  +
            " order by decode(m.ic_moneda,1,1,54,2,3) ";
         ps = con.queryPrecompilado(qrySentencia);
         ps.setString(1,icIfSiag);
         ps.setString(2,folio);
         rs = ps.executeQuery();
         while(rs.next()){
            if(i==0){
               pdfDoc.setTable(1,100);
               pdfDoc.setCell("DETALLE DE RECUPERACIONES POR MONEDA","celda04",ComunesPDF.CENTER);
               pdfDoc.addTable();
               pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
               pdfDoc.setTable(8,100);
               pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("IMPORTE DEPOSITO BANCO","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("IMPORTE CALCULADO","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("DIFERENCIA","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("DEPOSITO DE LA DIFERENCIA","celda01",ComunesPDF.CENTER);
            }
      
            pdfDoc.setCell(rs.getString("cd_nombre")+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_deposito"),2)+" ","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_calculado"),2)+" ","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_diferencia"),2)+" ","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_complemento"),2)+" ","formas",ComunesPDF.CENTER);
            i++;
         }	// while
         rs.close();ps.close();
      }//CONSREC
      
      if(i>0)
         pdfDoc.addTable();
   
      pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);			
         
      if("CONSREC".equals(origen)) {
      }
         
      qrySentencia =
         "select cg_contenido"+
         " from gti_arch_resultado A"+
         " ,comcat_if I"+
         " where A.ic_if_siag = I.ic_if_siag"+
         " and A.ic_folio = ?"+
         " and I.ic_if = ?"+
         " order by A.ic_linea,A.ic_num_error";
      
      ps = con.queryPrecompilado(qrySentencia);
      ps.setString(1,folio);
      if("4".equals(cvePerf)){
      ps.setString(2,clave_if);
      } else {
      ps.setString(2,iNoCliente);
      }	
      
      rs = ps.executeQuery();
      i=0;
      while(rs.next()){
         if(i==0){
            pdfDoc.setTable(1,100);
            pdfDoc.setCell("ERRORES GENERADOS","celda04",ComunesPDF.CENTER);
            pdfDoc.addTable();		
            
            if("CONSREC".equals(origen)) {
               pdfDoc.addText("Donde:\n","formas",ComunesPDF.LEFT);
               pdfDoc.addText("A: (CAPITAL + INTERESES + MORATORIOS)\n","formas",ComunesPDF.LEFT);			
               pdfDoc.addText("B: (GASTOS DE JUICIO)\n","formas",ComunesPDF.LEFT);			
               pdfDoc.addText("B: (INTERES + PENALIZACION)\n","formas",ComunesPDF.LEFT);			
               //pdfDoc.addText("ERRORES GENERADOS POR EL PROCESO DE CARGA DE RECUPERACIONES AUTOMATICAS\n","formas",ComunesPDF.LEFT);
            }
         }
         pdfDoc.addText(rs.getString("cg_contenido")+"\n","formas",ComunesPDF.LEFT);
         i++;
      }
      ps.close();
   
      
      pdfDoc.endDocument();
      //response.sendRedirect(strDirecVirtualTemp+nombreArchivo);
   }catch(Exception e){
      e.printStackTrace();
      error = e + "";
      throw e;
   }finally{
      if(con.hayConexionAbierta())
         con.cierraConexionDB();
   }
   
   JSONObject jsonObj = new JSONObject();
   jsonObj.put("success", new Boolean(true));
   jsonObj.put("result", error);
   jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
   resultado = jsonObj.toString();
}

%>
<%=resultado%>
