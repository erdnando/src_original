<%@ page contentType="application/json;charset=UTF-8" import="java.util.*, 
java.sql.*, 
netropology.utilerias.*,
com.netro.exception.*,
java.math.*,
java.text.*,
net.sf.json.JSONObject,
javax.naming.*,com.netro.pdf.*"
errorPage="/00utils/error_extjs.jsp" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>

<%
String infoRegresar ="";
String folio          = (request.getParameter("folio")	== null)?"":request.getParameter("folio");
AccesoDB con          = new AccesoDB();
PreparedStatement	ps  = null;
ResultSet rs          = null;
String qrySentencia   = "";
CreaArchivo archivo   = new CreaArchivo();
String nombreArchivo  = archivo.nombreArchivo()+".pdf";
int i = 0;


ComunesPDF pdfDoc     = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
String icIfSiag       = "";
String meses[]        = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
String fechaActual    = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String diaActual      = fechaActual.substring(0,2);
String mesActual      = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
String anioActual     = fechaActual.substring(6,10);
String horaActual     = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());


try{
	con.conexionDB();

	qrySentencia = 
	" SELECT PG.IC_FOLIO, "+
	"        ES.IC_SITUACION, "+
	"        TO_CHAR (PG.DF_FECHA_HORA, 'dd/mm/yyyy hh24:mi') AS FECHA, "+
	"        PG.CG_RFC_ACREDITADO, "+
	"        PG.CG_NOMBRE_ACREDITADO, "+
	"        CM.CD_NOMBRE AS MONEDA, "+
	"        TO_CHAR (PG.DF_VTO, 'dd/mm/yyyy') AS FECHA_VTO, "+
//	"        PG.FN_IMPTE_CAPITAL, "+
//	"        PG.FN_IMPTE_INTERESES, "+
	"        PG.FN_PORC_PARTICIPACION, "+
//	"        PG.FN_IMPTE_PAGO, "+
	"        TO_CHAR (PG.DF_INCUMPLIMIENTO, 'dd/mm/yyyy') AS FECHA_ICTO, "+
	"        PG.CG_CAUSAS_INCUMPLIMIENTO, "+
	"        PG.CG_FUNCIONARIO_IF, "+
	"        PG.CG_TEL_FUNCIONARIO,        "+
	"        PG.CG_EXT_FUNCIONARIO, "+
	"        PG.CG_MAIL_FUNCIONARIO, "+
	"        PG.CG_LOCALIZACION_SUPERVISION, "+
	"        PG.CG_CIUDAD_SUPERVISION, "+
	"        PG.CG_ESTADO_SUPERVISION, "+
	"        CTO.CG_DESCRIPCION AS TIPO_OP, "+
	"        PG.CC_GARANTIA, "+
	"        PG.IG_SIT_CALIFICACION, "+
	"        PG.IG_SIT_PAGO_COMISION, "+
	"        PG.IG_SIT_PAGO_PRORROGA, "+
	"        TO_CHAR (PG.DF_FECHA_HONRADA, 'dd/mm/yyyy') AS DF_FECHA_HONRADA, "+
	"        PG.CG_TIPO_TASA AS TIPO_TASA, "+
	"        PG.FN_TASA, "+
	"        TO_CHAR (PG.DF_FECHA_VTO_PRORROGA, 'dd/mm/yyyy') AS FECHA_VTO_PRORROGA, "+
	"        TO_CHAR (PG.DF_FECHA_ANIV, 'dd/mm/yyyy') AS FECHA_ANIV, "+
	"        PG.FN_PORC_ANIV, "+
	"        PG.FN_COMISION_ANIV, "+
	"        PG.FN_IVA_COMISION_ANIV, "+
	"        PG.FN_COMISION_ANIV + PG.FN_IVA_COMISION_ANIV AS COM, "+
	"        PG.FN_CAPITAL_SIAG, "+
	"        PG.FN_INTERES_SIAG, "+
	"        PG.FN_CAPITAL_SIAG+PG.FN_INTERES_SIAG AS  IMP_PAG_GAN, "+
	"        (PG.FN_CAPITAL_SIAG+PG.FN_INTERES_SIAG)-(PG.FN_COMISION_ANIV + PG.FN_IVA_COMISION_ANIV) AS IMP_NETO_DES, "+
	"        CS.CG_DESCRIPCION AS DESCRIPCION, "+
	"        PG.CG_CAUSAS_RECHAZO, "+
  "        PP.CG_DESCRIPCION AS PERIODICIDAD_PAGO,"+ //FODEA 017 - 2009 ACF
  "        TO_CHAR (PG.DF_FECHA_SEGUNDO_INCUM, 'dd/mm/yyyy') AS DF_FECHA_SEGUNDO_INCUM"+ //FODEA 017 - 2009 ACF
	" FROM GTI_SOLICITUD_PAGO PG, COMCAT_MONEDA CM, GTI_ESTATUS_SOLIC ES, GTICAT_SITUACION CS, GTICAT_TIPOOPERACION CTO, GTICAT_PERIODICIDAD_PAGO PP "+
	" WHERE ES.IC_IF_SIAG         = PG.IC_IF_SIAG "+
	" AND   ES.IC_FOLIO           = PG.IC_FOLIO "+
	" AND   ES.IC_SITUACION       = CS.IC_SITUACION "+
	" AND   ES.IC_TIPO_OPERACION  = CS.IC_TIPO_OPERACION "+
	" AND   ES.IC_TIPO_OPERACION  = CTO.IC_TIPO_OPERACION "+
	" AND   CM.IC_MONEDA(+)       = PG.IC_MONEDA "+
  " AND   PG.IC_PERIODICIDAD_PAGO = PP.IC_PERIODICIDAD_PAGO "+
	" AND   ES.IC_TIPO_OPERACION  = 3 "+
	" AND   PG.IC_FOLIO           = ? ";

	ps = con.queryPrecompilado(qrySentencia);
	ps.setString(1,folio);
	

	
	//System.out.println("Float value::"+Float.parseFloat(""));
	
	rs = ps.executeQuery();
	if(rs.next()){
		i++;

		String sIc_folio                   = (rs.getString("IC_FOLIO") == null)?"":rs.getString("IC_FOLIO") ;
		String sIc_situacion               = (rs.getString("IC_SITUACION") == null)?"":rs.getString("IC_SITUACION") ;
		String sFecha                      = (rs.getString("FECHA") == null)?"":rs.getString("FECHA") ;
		String sCg_rfc_acreditado          = (rs.getString("CG_RFC_ACREDITADO") == null)?"":rs.getString("CG_RFC_ACREDITADO") ;
		String sCg_nombre_acreditado       = (rs.getString("CG_NOMBRE_ACREDITADO") == null)?"":rs.getString("CG_NOMBRE_ACREDITADO") ;
		String sMoneda                     = (rs.getString("MONEDA") == null)?"":rs.getString("MONEDA") ;
		String sFecha_vto                  = (rs.getString("FECHA_VTO") == null)?"":rs.getString("FECHA_VTO") ;
		//String sFn_impte_capital           = (rs.getString("FN_IMPTE_CAPITAL") == null)?"":rs.getString("FN_IMPTE_CAPITAL") ;
		//String sFn_impte_intereses         = (rs.getString("FN_IMPTE_INTERESES") == null)?"":rs.getString("FN_IMPTE_INTERESES") ;
		String sFn_porc_participacion      = (rs.getString("FN_PORC_PARTICIPACION") == null)?"":rs.getString("FN_PORC_PARTICIPACION") ;
		//String sFn_impte_pago              = (rs.getString("FN_IMPTE_PAGO") == null)?"":rs.getString("FN_IMPTE_PAGO") ;
		String sFecha_icto                 = (rs.getString("FECHA_ICTO") == null)?"":rs.getString("FECHA_ICTO") ;
		String sCg_causas_incumplimiento   = (rs.getString("CG_CAUSAS_INCUMPLIMIENTO") == null)?"":rs.getString("CG_CAUSAS_INCUMPLIMIENTO") ;
		String sCg_funcionario_if          = (rs.getString("CG_FUNCIONARIO_IF") == null)?"":rs.getString("CG_FUNCIONARIO_IF") ;
		String sCg_tel_funcionario         = (rs.getString("CG_TEL_FUNCIONARIO") == null)?"":rs.getString("CG_TEL_FUNCIONARIO") ;
		String sCg_ext_funcionario         = (rs.getString("CG_EXT_FUNCIONARIO") == null)?"":rs.getString("CG_EXT_FUNCIONARIO") ;
		String sCg_mail_funcionario        = (rs.getString("CG_MAIL_FUNCIONARIO") == null)?"":rs.getString("CG_MAIL_FUNCIONARIO") ;
		String sCg_localizacion_supervision= (rs.getString("CG_LOCALIZACION_SUPERVISION") == null)?"":rs.getString("CG_LOCALIZACION_SUPERVISION") ;
		String sCg_ciudad_supervision      = (rs.getString("CG_CIUDAD_SUPERVISION") == null)?"":rs.getString("CG_CIUDAD_SUPERVISION") ;
		String sCg_estado_supervision      = (rs.getString("CG_ESTADO_SUPERVISION") == null)?"":rs.getString("CG_ESTADO_SUPERVISION") ;
		String sTipo_op                    = (rs.getString("TIPO_OP") == null)?"":rs.getString("TIPO_OP") ;
		String sCc_garantia                = (rs.getString("CC_GARANTIA") == null)?"":rs.getString("CC_GARANTIA") ;
		String sIg_sit_calificacion        = (rs.getString("IG_SIT_CALIFICACION") == null)?"":rs.getString("IG_SIT_CALIFICACION") ;
		String sIg_sit_pago_comision       = (rs.getString("IG_SIT_PAGO_COMISION") == null)?"":rs.getString("IG_SIT_PAGO_COMISION") ;
		String sIg_sit_pago_prorroga       = (rs.getString("IG_SIT_PAGO_PRORROGA") == null)?"":rs.getString("IG_SIT_PAGO_PRORROGA") ;
		String sDf_fecha_honrada           = (rs.getString("DF_FECHA_HONRADA") == null)?"":rs.getString("DF_FECHA_HONRADA") ;
		String sTipo_tasa                  = (rs.getString("TIPO_TASA") == null)?"":rs.getString("TIPO_TASA") ;
		String sFn_tasa                    = (rs.getString("FN_TASA") == null)?"":rs.getString("FN_TASA") ;
		String sFecha_vto_prorroga         = (rs.getString("FECHA_VTO_PRORROGA") == null)?"":rs.getString("FECHA_VTO_PRORROGA") ;
		String sFecha_aniv                 = (rs.getString("FECHA_ANIV") == null)?"":rs.getString("FECHA_ANIV") ;
		String sFn_porc_aniv               = (rs.getString("FN_PORC_ANIV") == null)?"":rs.getString("FN_PORC_ANIV") ;
		String sFn_comision_aniv           = (rs.getString("FN_COMISION_ANIV") == null)?"":rs.getString("FN_COMISION_ANIV") ;
		String sFn_iva_comision_aniv       = (rs.getString("FN_IVA_COMISION_ANIV") == null)?"":rs.getString("FN_IVA_COMISION_ANIV") ;
		String sCom                        = (rs.getString("COM") == null)?"":rs.getString("COM") ;
		String sFn_capital_siag            = (rs.getString("FN_CAPITAL_SIAG") == null)?"":rs.getString("FN_CAPITAL_SIAG") ;
		String sFn_interes_siag            = (rs.getString("FN_INTERES_SIAG") == null)?"":rs.getString("FN_INTERES_SIAG") ;
		String sImp_pag_gan                = (rs.getString("IMP_PAG_GAN") == null)?"":rs.getString("IMP_PAG_GAN") ;
		String sImp_neto_des               = (rs.getString("IMP_NETO_DES") == null)?"":rs.getString("IMP_NETO_DES") ;
		String sDescripcion                = (rs.getString("DESCRIPCION") == null)?"":rs.getString("DESCRIPCION") ;
		String sCg_causas_rechazo          = (rs.getString("CG_CAUSAS_RECHAZO") == null)?"":rs.getString("CG_CAUSAS_RECHAZO") ;
    String sPeriodicidad_pago          = (rs.getString("PERIODICIDAD_PAGO") == null)?"":rs.getString("PERIODICIDAD_PAGO") ; //FODEA 017 - 2009 ACF
		String sDf_fecha_segundo_incum     = (rs.getString("DF_FECHA_SEGUNDO_INCUM") == null)?"":rs.getString("DF_FECHA_SEGUNDO_INCUM") ; //FODEA 017 - 2009 ACF


		if(sIg_sit_calificacion.equals("01") || sIg_sit_calificacion.equals("1"))
			sIg_sit_calificacion        = "Cumplió" ;
		if(sIg_sit_calificacion.equals("02") || sIg_sit_calificacion.equals("2"))
			sIg_sit_calificacion        = "No Cumplió" ;
			
		if(sIg_sit_pago_comision.equals("01") || sIg_sit_pago_comision.equals("1"))
			sIg_sit_pago_comision       = "Cumplió" ;
		if(sIg_sit_pago_comision.equals("02") || sIg_sit_pago_comision.equals("2"))
			sIg_sit_pago_comision       = "No Cumplió" ;

		if(sIg_sit_pago_prorroga.equals("01") || sIg_sit_pago_prorroga.equals("1"))
			sIg_sit_pago_prorroga       = "Autorizada";			
		if(sIg_sit_pago_prorroga.equals("02") || sIg_sit_pago_prorroga.equals("2"))
			sIg_sit_pago_prorroga       = "Vencida";
		if(sIg_sit_pago_prorroga.equals("03") || sIg_sit_pago_prorroga.equals("3"))
			sIg_sit_pago_prorroga       = "Baja";
		if(sIg_sit_pago_prorroga.equals("04") || sIg_sit_pago_prorroga.equals("4"))
			sIg_sit_pago_prorroga       = "No Autorizada";

		if(sTipo_tasa.equals("1"))
			sTipo_tasa = "Tasa TIIE";
		if(sTipo_tasa.equals("2"))
			sTipo_tasa = "Tasa TIIE menor a 180 días";
		if(sTipo_tasa.equals("3"))
			sTipo_tasa = "Tasa Fija";
		if(sTipo_tasa.equals("4"))
			sTipo_tasa = "Tasa Fija menor a 180 días";

		//begin page 1
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
																 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
																 (String)session.getAttribute("sesExterno"),
																 (String)session.getAttribute("strNombre"),
																 (String)session.getAttribute("strNombreUsuario"),
																 (String)session.getAttribute("strLogo"),
																 strDirectorioPublicacion);

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);	

		pdfDoc.addText("Solicitud de pago de garantía","formasG",ComunesPDF.CENTER);

		float width[] = {25f,25f,25f,25f};
		pdfDoc.setTable(4, 100, width);
		pdfDoc.setCell("Datos de la solicitud","celda01",ComunesPDF.LEFT,4,1,0);
		//row1
		pdfDoc.setCell("Folio","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sIc_folio,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Fecha / Hora de operación","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sFecha,"formasrep",ComunesPDF.LEFT,1,1,0);

		pdfDoc.addTable();

		pdfDoc.setTable(4, 100, width);
		pdfDoc.setCell("Datos de la captura de solicitud","celda01",ComunesPDF.LEFT,4,1,0);
		//row1
		pdfDoc.setCell("RFC Acreditado","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_rfc_acreditado,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Nombre del Acreditado","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_nombre_acreditado,"formasrep",ComunesPDF.LEFT,1,1,0);
		//row2
		pdfDoc.setCell("Moneda","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sMoneda,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Fecha de Vencimiento","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sFecha_vto,"formasrep",ComunesPDF.LEFT,1,1,0);
		//row3
//		pdfDoc.setCell("Importe de Capital Vencido","formasrepB",ComunesPDF.LEFT,1,1,0);
//		pdfDoc.setCell(Comunes.formatoDecimal(sFn_impte_capital,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
//		pdfDoc.setCell("Importe de intereses Vencido","formasrepB",ComunesPDF.LEFT,1,1,0);
//		pdfDoc.setCell(Comunes.formatoDecimal(sFn_impte_intereses,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
		//row4
		pdfDoc.setCell("Porcentaje de participación","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_porc_participacion,2,true) + " %","formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Fecha del primer incumplimiento","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sFecha_icto,"formasrep",ComunesPDF.LEFT,1,1,0);
//		pdfDoc.setCell("Importe del pago solicitado","formasrepB",ComunesPDF.LEFT,1,1,0);
//		pdfDoc.setCell(Comunes.formatoDecimal(sFn_impte_pago,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
		//row5
		pdfDoc.setCell("Causa del Incumplimieto","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_causas_incumplimiento,"formasrep",ComunesPDF.LEFT,1,1,0);
    //FODEA 017 - 2009 ACF (I)
    pdfDoc.setCell("Fecha del segundo incumplimiento","formasrepB",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell(sDf_fecha_segundo_incum,"formasrep",ComunesPDF.LEFT,1,1,0);
    
		pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Periodicidad de pago","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sPeriodicidad_pago,"formasrep",ComunesPDF.LEFT,1,1,0);
    //FODEA 017 - 2009 ACF (F)
		pdfDoc.addTable();

		pdfDoc.setTable(4, 100, width);
		pdfDoc.setCell("Funcionarios Asignado para supervisión","celda01",ComunesPDF.LEFT,4,1,0);
		//row1
		pdfDoc.setCell("Nombre","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_funcionario_if,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Teléfono","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_tel_funcionario,"formasrep",ComunesPDF.LEFT,1,1,0);
		//row2
		pdfDoc.setCell("Ext.:","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_ext_funcionario,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Mail","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_mail_funcionario,"formasrep",ComunesPDF.LEFT,1,1,0);

		pdfDoc.addTable();

		pdfDoc.setTable(4, 100, width);
		pdfDoc.setCell("Datos de la ubicación donde se realiza la supervisión","celda01",ComunesPDF.LEFT,4,1,0);
		//row1
		pdfDoc.setCell("Domicilio","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_localizacion_supervision,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);
		//row2
		pdfDoc.setCell("Ciudad","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_ciudad_supervision,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Estado","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_estado_supervision,"formasrep",ComunesPDF.LEFT,1,1,0);

		pdfDoc.addTable();

		pdfDoc.setTable(4, 100, width);
		pdfDoc.setCell("Detalle de resultados","celda01",ComunesPDF.LEFT,4,1,0);
		//row1
		pdfDoc.setCell("Tipo de Solicitud","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sTipo_op,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Clave de financiamiento","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCc_garantia,"formasrep",ComunesPDF.LEFT,1,1,0);
		//row2
		pdfDoc.setCell("Situación de calificación","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sIg_sit_calificacion,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Situación de pago de comisiones","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sIg_sit_pago_comision,"formasrep",ComunesPDF.LEFT,1,1,0);
		//row3
		pdfDoc.setCell("Situación de Prorroga","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sIg_sit_pago_prorroga,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Fecha Honrada","formasrepB",ComunesPDF.LEFT,1,1,0);
		
		if("8".equals(sIc_situacion)) { //Si la solicitud es rechazada (8). No se coloca la Fecha Honrada aunque se tenga el valor.
			sDf_fecha_honrada = "";
		}

		pdfDoc.setCell(sDf_fecha_honrada,"formasrep",ComunesPDF.LEFT,1,1,0);
		//row4
		pdfDoc.setCell("Tipo de Tasa","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sTipo_tasa,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Tasa","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_tasa,2,true)+" %","formasrep",ComunesPDF.LEFT,1,1,0);
		//row5
		pdfDoc.setCell("Fecha de vencimiento de Prorroga","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sFecha_vto_prorroga,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Fecha de comisión por aniversario","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sFecha_aniv,"formasrep",ComunesPDF.LEFT,1,1,0);
		//row6
		pdfDoc.setCell("Porcentaje de comisión por aniversario","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_porc_aniv,4,true)+" %","formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);

		pdfDoc.addTable();
		//end page 1
		///////////////////////////////////
		//begin page 2
		pdfDoc.newPage();
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
																 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
																 (String)session.getAttribute("sesExterno"),
																 (String)session.getAttribute("strNombre"),
																 (String)session.getAttribute("strNombreUsuario"),
																 (String)session.getAttribute("strLogo"),
																 strDirectorioPublicacion);

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);	

		pdfDoc.setTable(4, 100, width);
		pdfDoc.setCell("Detalle de cargo de comisión por aniversario","celda01",ComunesPDF.LEFT,4,1,0);
		//row1
		pdfDoc.setCell("Importe de comisión por aniversario","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_comision_aniv,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("IVA de comisión por aniversario","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_iva_comision_aniv,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
		//row2
		pdfDoc.setCell("Importe total de comisión por aniversario","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(Comunes.formatoDecimal(sCom,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);

		pdfDoc.addTable();

		pdfDoc.setTable(4, 100, width);
		pdfDoc.setCell("Detalle de importe de pago de garantía","celda01",ComunesPDF.LEFT,4,1,0);
		//row1
		pdfDoc.setCell("Importe de capital a pagar","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_capital_siag,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Importe de intereses a pagar","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_interes_siag,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
		//row2
		pdfDoc.setCell("Importe de pago de garantía","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(Comunes.formatoDecimal(sImp_pag_gan,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);

		pdfDoc.addTable();

		pdfDoc.setTable(4, 100, width);
		pdfDoc.setCell("Importe neto a desembolsar (Pago de garantía menos comisión por aniversario)","celda01",ComunesPDF.LEFT,4,1,0);
		//row1
		pdfDoc.setCell("Importe neto a desembolsar","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(Comunes.formatoDecimal(sImp_pag_gan,2,true)+" - "+Comunes.formatoDecimal(sCom,2,true)+" = "+Comunes.formatoDecimal(sImp_neto_des,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Situación de la solicitud","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sDescripcion,"formasrep",ComunesPDF.LEFT,1,1,0);

		pdfDoc.addTable();


		pdfDoc.setTable(4, 100, width);
		pdfDoc.setCell("Causas de rechazo de la operación","celda01",ComunesPDF.LEFT,4,1,0);
		//row1
		pdfDoc.setCell("Causas de rechazo de la operación","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(sCg_causas_rechazo,"formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);

		pdfDoc.addTable();
		//end page 2
		pdfDoc.endDocument();
		
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar=jsonObj.toString();
	}

%>



<%
}catch(Exception e){
	e.printStackTrace();
	throw e;
}finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
}
%>

<%=infoRegresar%>