<%@ page contentType="application/json;charset=UTF-8" import="java.util.*, 
java.sql.*, 
netropology.utilerias.*,
com.netro.exception.*,
java.math.*,
java.text.*,
net.sf.json.JSONObject,
javax.naming.*,com.netro.pdf.*"
errorPage="/00utils/error_extjs.jsp" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>

<%
String infoRegresar ="";
String folio          = (request.getParameter("folio")	== null)?"":request.getParameter("folio");
String iIcIfSiag      = (request.getParameter("iIcIfSiag")	== null)?"":request.getParameter("iIcIfSiag");
String claveGarantia = (request.getParameter("claveGarantia")	== null)?"":request.getParameter("claveGarantia");

String fiso = (request.getParameter("fiso")	== null)?"":request.getParameter("fiso");

AccesoDB con          = new AccesoDB();
PreparedStatement	ps  = null;
ResultSet rs          = null;
ResultSet rsArchi     = null;
ResultSet rsContrInv  = null;
String qrySentencia   = "";
StringBuffer sArchi = new StringBuffer();
CreaArchivo archivo   = new CreaArchivo();
String nombreArchivo  = archivo.nombreArchivo()+".pdf";
int i = 0;


ComunesPDF pdfDoc     = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
String icIfSiag       = "";
String meses[]        = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
String fechaActual    = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String diaActual      = fechaActual.substring(0,2);
String mesActual      = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
String anioActual     = fechaActual.substring(6,10);
String horaActual     = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());


try{
	con.conexionDB();

	qrySentencia = 
	" SELECT ES.IC_FOLIO, "+
	"        ES.IC_SITUACION, "+
	"        CCI.CG_RAZON_SOCIAL, "+
	"        PG.CC_GARANTIA, "+
	"        PG.FN_CAPITAL_SIAG, "+
	"        PG.FN_INTERES_SIAG, "+
	"        PG.FN_CAPITAL_SIAG+PG.FN_INTERES_SIAG AS DESEMBOLSE, "+
	"        TO_CHAR (PG.DF_FECHA_ANIV, 'dd/mm/yyyy') AS DF_FECHA_ANIV, "+
	"        PG.FN_PORC_ANIV, "+
	"        PG.FN_COMISION_ANIV, "+
	"        PG.FN_IVA_COMISION_ANIV, "+
	"        PG.FN_IVA_COMISION_ANIV + PG.FN_COMISION_ANIV AS COMISION, "+
	"        PG.IG_SIT_CALIFICACION, "+
	"        PG.IG_SIT_PAGO_PRORROGA, "+
	"        TO_CHAR (PG.DF_FECHA_VTO_PRORROGA, 'dd/mm/yyyy') AS DF_FECHA_VTO_PRORROGA, "+
	"        TO_CHAR (PG.DF_FECHA_HONRADA, 'dd/mm/yyyy') AS DF_FECHA_HONRADA, "+
	"        PG.CG_TIPO_TASA AS TIPO_TASA, "+
	"        PG.FN_TASA, "+
	"        (PG.FN_CAPITAL_SIAG+PG.FN_INTERES_SIAG) - (PG.FN_IVA_COMISION_ANIV + PG.FN_COMISION_ANIV) AS NETO, "+
	"        CM.CD_NOMBRE AS MONEDA, "+
	"        CS.CG_DESCRIPCION AS SITUACION, " +
	"        TO_CHAR(ES.DF_AUTORIZACION_SIAG,'dd/mm/yyyy') AS FECHA_PROCESO "+
	"         "+
	" FROM GTI_SOLICITUD_PAGO PG, GTI_ESTATUS_SOLIC ES, COMCAT_MONEDA CM, GTICAT_SITUACION CS, GTICAT_TIPOOPERACION CTO, "+
	"      COMCAT_IF CCI "+
	"  "+
	" WHERE ES.IC_IF_SIAG         = PG.IC_IF_SIAG "+
	" AND   ES.IC_FOLIO           = PG.IC_FOLIO "+
	" AND   ES.IC_SITUACION       = CS.IC_SITUACION "+
	" AND   ES.IC_TIPO_OPERACION  = CS.IC_TIPO_OPERACION "+
	" AND   ES.IC_TIPO_OPERACION  = CTO.IC_TIPO_OPERACION "+
	" AND   CM.IC_MONEDA(+)       = PG.IC_MONEDA "+
	" AND   CCI.IC_IF_SIAG        = PG.IC_IF_SIAG "+
	" AND   ES.IC_TIPO_OPERACION  = ? "+
	" AND   PG.IC_FOLIO           = ? ";

	ps = con.queryPrecompilado(qrySentencia);
	ps.setInt(1, 3); //3 = Pago de Garantias
	ps.setBigDecimal(2,new BigDecimal(folio));
	rs = ps.executeQuery();

	System.out.println("qrySentencia::"+qrySentencia);
	System.out.println("folio::"+folio);
	
	qrySentencia = 
	" SELECT CG_CONTENIDO "+
	" FROM GTI_ARCH_RESULTADO "+
	" WHERE IC_IF_SIAG         = ? "+
	" AND   IC_FOLIO           = ? "+
	" ORDER BY IC_LINEA , IC_NUM_ERROR";

	ps = con.queryPrecompilado(qrySentencia);
	ps.setInt(1,Integer.parseInt(iIcIfSiag));
	ps.setBigDecimal(2,new BigDecimal(folio));
	rsArchi = ps.executeQuery();

	while(rsArchi.next()){
		sArchi.append( ((rsArchi.getString("CG_CONTENIDO") == null)?"":rsArchi.getString("CG_CONTENIDO")+"\n") );
	}
	sArchi.append("\n ");
	rsArchi.close();
	ps.close();

	StringBuffer contratos = new StringBuffer();
	if (fiso != null && !fiso.equals("") && claveGarantia != null && !claveGarantia.equals("")) {
		qrySentencia = 
				" SELECT /*+ use_nl(gar,des,a,b) */ " +
				" 	pp.numero_contrato AS contrato " +
				" FROM gia_garantias gar, " +
				" 	gia_desembolsos_fisos des, " +
				" 	siag_proyecto_producto pp, " +
				" 	siag_contragaranteproyecto cp " +
				" WHERE des.dfi_fiso_contraparte IS NOT NULL " +
				" 	AND nvl(des.dfi_procesado,0) = 0 " +
				" 	AND nvl(des.dfi_recuperacion,' ') = 'D' " +
				" 	AND des.dfi_fiso_clave = gar.gar_fiso " +
				" 	AND des.dfi_uso_contragarantia = gar.gar_uso_contragarantia " +
				" 	AND des.dfi_prg_clave = gar.tpro_clave " +
				" 	AND des.dfi_clave_garant = gar.clave " +
				" 	AND des.dfi_interm_clave = gar.inter_clave " +
				" 	AND pp.pfo_fiso_contraparte = des.dfi_fiso_contraparte " +
				" 	AND pp.clave_tipoprograma_siag = des.dfi_prg_clave " +
				" 	AND cp.uco_clave = decode(des.dfi_tipo_desembolso,'N',2,3) " +
				" 	AND pp.clave_contragarante = cp.clave_contragarante " +
				" 	AND pp.clave_proyecto = cp.clave_proyecto " +
				" 	AND pp.numero_contrato = cp.numero_contrato " +
				" 	AND gar.gar_fiso = ? " +
				" 	AND gar.inter_clave = ? " +
				" 	AND gar.clave = ? " +
				" ORDER BY pp.numero_contrato ";
		System.out.println("29pagog02bpdf.jsp::Query Contrato: " + qrySentencia);
		System.out.println("fiso: " + fiso);
		System.out.println("iIcIfSiag: " + iIcIfSiag);
		System.out.println("claveGarantia: " + claveGarantia);
		//System.out.println("folio: " + folio);
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1, Integer.parseInt(fiso));
		ps.setInt(2, Integer.parseInt(iIcIfSiag));
		ps.setString(3, claveGarantia);
		
		rsContrInv = ps.executeQuery();
		while (rsContrInv.next()) {
			contratos.append(rsContrInv.getString("contrato") + "\n");
		}
		rsContrInv.close();
		ps.close();
		con.terminaTransaccion(true); //Se realiza debido a que se hace consulta a tablas a través de un db link
	}	//fin obtencion de contratos de inversión
	
	if(rs.next()){
		i++;
		
	String sIc_folio              = (rs.getString("IC_FOLIO") == null)?"":rs.getString("IC_FOLIO") ;
	String sIc_situacion          = (rs.getString("IC_SITUACION") == null)?"":rs.getString("IC_SITUACION") ;
	String sCg_razon                = (rs.getString("CG_RAZON_SOCIAL") == null)?"":rs.getString("CG_RAZON_SOCIAL") ;
	String sCc_garantia           = (rs.getString("CC_GARANTIA") == null)?"":rs.getString("CC_GARANTIA") ;
	String sFn_capital_siag       = (rs.getString("FN_CAPITAL_SIAG") == null)?"":rs.getString("FN_CAPITAL_SIAG") ;
	String sFn_interes_siag       = (rs.getString("FN_INTERES_SIAG") == null)?"":rs.getString("FN_INTERES_SIAG") ;
	String sDesembolse            = (rs.getString("DESEMBOLSE") == null)?"":rs.getString("DESEMBOLSE") ;
	String sDf_fecha_aniv         = (rs.getString("DF_FECHA_ANIV") == null)?"":rs.getString("DF_FECHA_ANIV") ;
	String sFn_porc_aniv          = (rs.getString("FN_PORC_ANIV") == null)?"":rs.getString("FN_PORC_ANIV") ;
	String sFn_comision_aniv      = (rs.getString("FN_COMISION_ANIV") == null)?"":rs.getString("FN_COMISION_ANIV") ;
	String sFn_iva_comision_aniv  = (rs.getString("FN_IVA_COMISION_ANIV") == null)?"":rs.getString("FN_IVA_COMISION_ANIV") ;
	String sComision              = (rs.getString("COMISION") == null)?"":rs.getString("COMISION") ;
	String sIg_sit_calificacion   = (rs.getString("IG_SIT_CALIFICACION") == null)?"":rs.getString("IG_SIT_CALIFICACION") ;
	String sIg_sit_pago_prorroga  = (rs.getString("IG_SIT_PAGO_PRORROGA") == null)?"":rs.getString("IG_SIT_PAGO_PRORROGA") ;
	String sDf_fecha_vto_prorroga = (rs.getString("DF_FECHA_VTO_PRORROGA") == null)?"":rs.getString("DF_FECHA_VTO_PRORROGA") ;
	String sDf_fecha_honrada      = (rs.getString("DF_FECHA_HONRADA") == null)?"":rs.getString("DF_FECHA_HONRADA") ;
	String sTipo_tasa             = (rs.getString("TIPO_TASA") == null)?"":rs.getString("TIPO_TASA") ;
	String sFn_tasa               = (rs.getString("FN_TASA") == null)?"":rs.getString("FN_TASA") ;
	String sNeto                  = (rs.getString("NETO") == null)?"":rs.getString("NETO") ;
	String sMoneda                = (rs.getString("MONEDA") == null)?"":rs.getString("MONEDA") ;
	String sSituacionDescripcion = (rs.getString("SITUACION") == null)?"":rs.getString("SITUACION") ;
	String fechaProceso = (rs.getString("FECHA_PROCESO") == null)?"":rs.getString("FECHA_PROCESO");
		
	if(sTipo_tasa.equals("1"))
		sTipo_tasa = "Tasa TIIE";
	if(sTipo_tasa.equals("2"))
		sTipo_tasa = "Tasa TIIE menor a 180 días";
	if(sTipo_tasa.equals("3"))
		sTipo_tasa = "Tasa Fija";
	if(sTipo_tasa.equals("4"))
		sTipo_tasa = "Tasa Fija menor a 180 días";
		
	sSituacionDescripcion = "SOLICITUD " + sSituacionDescripcion;

	if(sIg_sit_calificacion.equals("01") || sIg_sit_calificacion.equals("1"))
		sIg_sit_calificacion        = "Cumplió" ;
	if(sIg_sit_calificacion.equals("02") || sIg_sit_calificacion.equals("2"))
		sIg_sit_calificacion        = "No Cumplió" ;

	if(sIg_sit_pago_prorroga.equals("01") || sIg_sit_pago_prorroga.equals("1"))
		sIg_sit_pago_prorroga       = "Autorizada";			
	if(sIg_sit_pago_prorroga.equals("02") || sIg_sit_pago_prorroga.equals("2"))
		sIg_sit_pago_prorroga       = "Vencida";
	if(sIg_sit_pago_prorroga.equals("03") || sIg_sit_pago_prorroga.equals("3"))
		sIg_sit_pago_prorroga       = "Baja";
	if(sIg_sit_pago_prorroga.equals("04") || sIg_sit_pago_prorroga.equals("4"))
		sIg_sit_pago_prorroga       = "No Autorizada";


		//begin page 1
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
																 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
																 (String)session.getAttribute("sesExterno"),
																 (String)session.getAttribute("strNombre"),
																 (String)session.getAttribute("strNombreUsuario"),
																 (String)session.getAttribute("strLogo"),
																 strDirectorioPublicacion);

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);	

		float widthtb1[] = {100f};
		pdfDoc.setTable(1, 100, widthtb1);
		pdfDoc.setCell("Resultados de solicitudes de pago de garantías","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.addTable();
		pdfDoc.addText("Fecha de operación en firme: "+fechaProceso+"\n","formas",ComunesPDF.RIGHT);
		pdfDoc.addText(sSituacionDescripcion,"formasG",ComunesPDF.CENTER);

		float widthtb2[] = {16.6f,16.6f,16.6f,16.6f,16.6f,16.6f};
		pdfDoc.setTable(6, 100, widthtb2);
		pdfDoc.setCell("Folio de la operación","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Numero de Garantia","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Capital","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Intereses","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Total desembolsado","celda01",ComunesPDF.CENTER,1,1,1);
		//row 1
		pdfDoc.setCell(sIc_folio+"\n ","formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(sCg_razon,"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(sCc_garantia,"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_capital_siag,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_interes_siag,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(Comunes.formatoDecimal(sDesembolse,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
		
		pdfDoc.addTable();

		float widthtb3[] = {13f,13f,12f,12f,12.5f,12.5f,12.5f,12.5f};
		pdfDoc.setTable(8, 100, widthtb3);
		pdfDoc.setCell("Calculo de la comisión por aniversario","celda01",ComunesPDF.CENTER,5,1,1);
		pdfDoc.setCell("Verificación calificación y prorroga","celda01",ComunesPDF.CENTER,3,1,1);
		
		pdfDoc.setCell("Fecha de Aniversario","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("% comisión","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Monto comisión","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Monto IVA","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Monto Total","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Situación calificación","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Situación prorroga","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Fecha vencimiento prorroga","celda01",ComunesPDF.CENTER,1,1,1);
		//row 1
		pdfDoc.setCell(sDf_fecha_aniv+"\n ","formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_porc_aniv,4,true)+" %","formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_comision_aniv,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_iva_comision_aniv,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(Comunes.formatoDecimal(sComision,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(sIg_sit_calificacion,"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(sIg_sit_pago_prorroga,"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(sDf_fecha_vto_prorroga,"formasrep",ComunesPDF.CENTER,1,1,1);
		
		pdfDoc.addTable();

		pdfDoc.setTable(1, 100, widthtb1);
		pdfDoc.setCell("Datos complementarios","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.addTable();

		pdfDoc.setTable(6, 100, widthtb2);
		pdfDoc.setCell("Fecha honrada","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Tipo de tasa","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Importe Neto","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Contrato de Inversión","celda01",ComunesPDF.CENTER,1,1,1);
		//row 1
		
		if("8".equals(sIc_situacion)) { //Si la solicitud es rechazada (8). No se coloca la Fecha Honrada aunque se tenga el valor.
			sDf_fecha_honrada = "";
		}

		pdfDoc.setCell(sDf_fecha_honrada+"\n ","formasrep",ComunesPDF.CENTER,1,1,1);
		
		
		pdfDoc.setCell(sTipo_tasa,"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(Comunes.formatoDecimal(sFn_tasa,2,true) + " %","formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(Comunes.formatoDecimal(sNeto,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(sMoneda,"formasrep",ComunesPDF.CENTER,1,1,1);
		
		if(contratos.length() > 0){
			pdfDoc.setCell(contratos.toString(),"formasrep",ComunesPDF.CENTER,1,1,1);
		}else{
			pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,1,1,1);
		}
		
		
		pdfDoc.addTable();


		pdfDoc.setTable(1, 100, widthtb1);
		pdfDoc.setCell("Errores Generados","celda01",ComunesPDF.CENTER,1,1,1);
		
		pdfDoc.setCell(sArchi.toString(),"formasrep",ComunesPDF.LEFT,1,1,1);
		pdfDoc.addTable();

		pdfDoc.endDocument();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar=jsonObj.toString();
	}
	rs.close();
	ps.close();
	
%>


<%
}catch(Exception e){
	e.printStackTrace();
	throw e;
}finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
}
%>

<%=infoRegresar%>