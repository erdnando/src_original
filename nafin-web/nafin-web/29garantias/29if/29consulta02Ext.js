Ext.onReady(function() {
//var ahora=new Date();
//var mes = (parseInt(ahora.getMonth()) + 1)<10?0+(parseInt(ahora.getMonth()) + 1).toString():(parseInt(ahora.getMonth()) + 1).toString();
//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
var bajarArchivo =  function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var arcName= Ext.util.JSON.decode(response.responseText).urlArchivo;
				
				if(arcName=='NOREG')
				{
					Ext.MessageBox.alert("Error",	"No se encuentra ningun registro");
				}
				else
				{

					var forma    = Ext.getDom('formAux');
					forma.action = NE.appWebContextRoot+'/DescargaArchivo';
					forma.method = 'post';
					forma.target = '_self';
					// Preparar Archivo a Descargar
					var archivo  = arcName;				
					archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
					// Insertar archivo
					var inputNombreArchivo = Ext.DomHelper.insertFirst(
						forma, 
						{ 
							tag: 	'input', 
							type: 'hidden', 
							id: 	'nombreArchivo', 
							name: 'nombreArchivo', 
							value: archivo
						},
						true
					); 
					// Solicitar Archivo Al Servidor
					forma.submit();
					// Remover nodo agregado
					inputNombreArchivo.remove();
					
				}
			
		} else {
			Ext.MessageBox.alert("Error",
			"No se pudo generar el documento");
		}
	}
var bajarPDF =  function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			
		} else {
			Ext.MessageBox.alert("Error",
			"No se pudo generar el documento");
		}
	}
var procesarConsultaData = function(store, arrRegistros, opts) {
	
		var grid = Ext.getCmp('grid');
		
		if (arrRegistros != null) {
			
			if (!grid.isVisible()) {
				grid.show();
			}
		

			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
			
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	/**
* Boolean renderer 
*/
var booleanRenderer = function (value, metadata, record) {
    if (value == '')
       out = '0';
    else 
      out =value;

    return out;

} 
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN

//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT

	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '29consulta02Ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [		
			{name: 'CLAVE_IF'},
			{name: 'NOMBRE_IF'},
			{name: 'TIPO_ENVIO'},			
			{name: 'FECHA'},
			{name: 'IC_FOLIO'},
			{name: 'SITUACION'},
			{name: 'IN_REGISTROS_ACEP'},
			{name: 'IN_REGISTROS_RECH'},
			{name: 'IN_REGISTROS'},
			{name: 'IC_SITUACION'},
			{name: 'MONTO_ENVIADO'},
			{name: 'REPROCESO'}
			
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
		
	});


//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
var grid = {
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		columns: [
			{
				header: 'Fecha y Hora de <br>Operaci�n',
				tooltip: 'Fecha y Hora de Operaci�n',
				dataIndex: 'FECHA',
				width: 100,
				resizable: true,
				sortable: true,
				align: 'center'				
			},{
				header: 'Folio de la <br>Operaci�n',
				tooltip: 'Folio de la Operaci�n',
				dataIndex: 'IC_FOLIO',
				sortable: true,	align: 'center',
				width: 150, resizable: true
				
			},{
				header: 'Situaci�n', tooltip: 'Situaci�n',
				dataIndex: 'SITUACION',
				sortable: true,	align: 'center',
				width: 200, resizable: true
				
			},{
				header: 'N�mero de operaciones<br> aceptadas',
				tooltip: 'N�mero de operaciones aceptadas',
				dataIndex: 'IN_REGISTROS_ACEP',
				sortable: true,	align: 'center',
				width: 150, resizable: true,renderer:booleanRenderer
				
			},{
				header: 'N�mero de operaciones<br>con errores',
				tooltip: 'N�mero de operaciones con errores',
				dataIndex: 'IN_REGISTROS_RECH',
				sortable: true,	align: 'center',
				width: 150, resizable: true,renderer:booleanRenderer
				
			},{
				header: 'Total de<br> Operaciones', 
				tooltip: 'Total de Operaciones',
				dataIndex: 'IN_REGISTROS',
				sortable: true,	align: 'center',
				width: 120, resizable: true
				
			},
			{
				xtype: 'actioncolumn',
				header: 'Archivo<br> Origen', tooltip: 'Archivo Origen',
				dataIndex: '',
				width: 60, resizable: true,
				align: 'center', 
				items: [
					{
						iconCls: 'icoLupa',
					
						handler: function(grid, rowIdx, colIds){
							var reg = grid.getStore().getAt(rowIdx);
								
						Ext.Ajax.request({
							url : '29consulta02Ext.data.jsp',
							params: {
								informacion: 'archivo',
								folio:reg.get('IC_FOLIO'),
								opcion:'origen'
							},
							callback: bajarArchivo						});		
						//FIN AJAX
 						}
					}
				]
			},
			{
			 xtype: 'actioncolumn',
			 dataIndex: '',
			 width: 80,
			 header:'Resultados',tooltip:'Resultados',
			 align: 'center',
			 items: [{
				  getClass: function(v, medaData, record) {						
						if(record.get('IC_SITUACION')=="5")
						return 'icoLupa';return '';
				  },
				  handler: function(grid, rowIndex, colIndex) {
								var reg = grid.getStore().getAt(rowIndex);
								
						Ext.Ajax.request({
							url : '29consulta02Ext.data.jsp',
							params: {
								informacion: 'pdf',
								folio:reg.get('IC_FOLIO'),
								origen:'CONSCALIF',
								clave_if:reg.get('CLAVE_IF')
								
							},
							callback: bajarPDF
						});		
						//FIN AJAX	*/
				  }
			 }]
			},{
			 xtype: 'actioncolumn',
			 dataIndex: '',
			 width: 80,
			 header:'Archivo de<br>Errores',tooltip:'Archivo de Errores',
			 align: 'center',
			 items: [{
				  getClass: function(v, medaData, record) {						
						if(record.get('IC_SITUACION')=="5" &&
						parseInt(record.get('IN_REGISTROS_RECH'))>0)
						return 'icoLupa';//return '';
				  },
				  handler: function(grid, rowIndex, colIndex) {
						var reg = grid.getStore().getAt(rowIndex);
								
						Ext.Ajax.request({
							url : '29consulta02Ext.data.jsp',
							params: {
								informacion: 'archivo',
								folio:reg.get('IC_FOLIO'),
								opcion:'error'
							},
							callback: bajarArchivo						});		
						//FIN AJAX
				  }
			 }]
			}
			
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		colunmWidth: true,
		frame: true,
		emptyMsg: 'No records',
		collapsible: true
	};
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		hidden: true
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [grid,fp]
	});
	
});
