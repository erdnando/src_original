Ext.onReady(function() {

//-----------------------------respuestaGuardar------------------------------
function respuestaGuardar(opts, success, response) {
  if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
      Ext.Msg.show({
          title: 'Guardar',
          msg: 'La informaci�n se almaceno con �xito.',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function (){
               Ext.getCmp('forma').getForm().reset();
               var grid = Ext.getCmp('gridNafin');
               var store = grid.getStore();
                store.each(function(record) {
                store.remove(record);
                store.commitChanges();
               });
               var gridE = Ext.getCmp('gridEpo');
               var storeE = gridE.getStore();
                storeE.each(function(record) {
                storeE.remove(record);
                storeE.commitChanges();
               });
               window.location  = "/nafin/29garantias/29if/paramcorreo01EXT.jsp"; 
              }
          });
          
  } else {
    Ext.Msg.show({
          title: 'Guardar',
          msg: 'Ocurrio un error...',
          modal: true,
          icon: Ext.Msg.ERROR,
          buttons: Ext.Msg.OK,
          fn: function (){
               Ext.getCmp('forma').getForm().reset();
               var grid = Ext.getCmp('gridNafin');
               var store = grid.getStore();
                store.each(function(record) {
                store.remove(record);
                store.commitChanges();
               });
               var gridE = Ext.getCmp('gridEpo');
               var storeE = gridE.getStore();
                storeE.each(function(record) {
                storeE.remove(record);
                storeE.commitChanges();
               });
              }
          });
  }
};
//---------------------------Fin respuestaGuardar----------------------------

//-----------------------------procesarInformacion------------------------------
function procesarInformacion(opts, success, response) {
  if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
    var info = Ext.util.JSON.decode(response.responseText);
    
    if(info.sizeN>0){
      var miData = new Array();
      for ( x =0 ; x < info.sizeN; x++){
        var registro = [];
        registro.push(info.cuentasN[x].descripcion);
        miData.push(registro);
        }
      var grid = Ext.getCmp('gridNafin');
      var store = grid.getStore();
      store.loadData(miData);
    }else {
      var grid = Ext.getCmp('gridNafin');
      var store = grid.getStore();
      store.loadData('');
    } 
//--------------------------------------------
    if(info.sizeIf>0){
      var miData = new Array();
      for ( x =0 ; x < info.sizeIf; x++){
        var registro = [];
        registro.push(info.cuentasIf[x].descripcion);
        miData.push(registro);
        }
      var grid = Ext.getCmp('gridEpo');
      var store = grid.getStore();
      store.loadData(miData);
    }else {
      var grid = Ext.getCmp('gridEpo');
      var store = grid.getStore();
      store.loadData('');
    }
//------------------------------------------
    if(info.sizeEst>0){
      for ( x =0 ; x < info.sizeEst; x++){
		  var inserta=false;
		  var encontrado=false;
        var store = Ext.getCmp('comboEstatus').getStore();
				store.each(function(record){
					if(record.data['clave']==info.estatus[x].clave){
						inserta=false;	
					} else{
						inserta=true;
					}
					if(!inserta){
						encontrado=true;
					}
				});
				if(inserta & !encontrado){
					store.insert([x+1], new Est({ clave: info.estatus[x].clave, 		descripcion:info.estatus[x].descripcion, 		loadMsg: ""}));
				}
            store.commitChanges(); 
        }      
    }
//--------------------------------------------       
  } else {
      NE.util.mostrarConnError(response,opts);			
  }
};
//---------------------------Fin procesarInformacion----------------------------


//*******************************************************  procesarGuardar
var procesarGuardar = function() {
  
  var sN = "";
  var grid = Ext.getCmp('gridNafin');
  var store = grid.getStore();
  
  store.each(function(record) {
    sN = sN + record.data['descripcion']+",";
  });
  
  var sI = "";
  var gridI = Ext.getCmp('gridEpo');
  var storeI = gridI.getStore();
  
  storeI.each(function(record) {
    sI = sI + record.data['descripcion']+",";
  });
  Ext.Ajax.request({
    url: 'paramcorreo01EXT.data.jsp',
    params: {
      informacion: 'guardar',
      intermed: Ext.getCmp('comboIntermediarioFinanciero').getValue(),
      transacc: Ext.getCmp('comboTransaccion').getValue(),
      estatus : Ext.getCmp('comboEstatus').getValue(),
      correoNa: sN,
      correoIf: sI
    },
    callback: respuestaGuardar
    });
  
}
//*******************************************************  procesarGuardar

//-------------------------Store correoEpo---------------------------------
	var correoEpo = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'myStore',
     idIndex: 0, 
		 fields		: [
              {name : 'descripcion'},
              {name : 'clave' }
              ]
		});
//--------------------------Fin Store correoEpo----------------------------

//-------------------------Store correoNafin---------------------------------
	var correoNafin = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'myStore',
     idIndex: 0, 
		 fields		: [
              {name : 'descripcion'},
              {name : 'clave' }
              ]
		});
//--------------------------Fin Store correoNafin----------------------------

//------------------------------------Inter-------------------------------------
var Inter = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin Inter------------------------------------

//------------------------------------Tran-------------------------------------
var Tran = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin Tran------------------------------------

//------------------------------------Est-------------------------------------
var Est = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin Est------------------------------------

//---------------------------- procesarEstatus--------------------------------
	var procesarEstatus = function(store, arrRegistros, opts) {
		store.insert(0,new Est({ 
		clave: "", 
		descripcion: "Seleccionar...", 
		loadMsg: ""})); 		
    Ext.getCmp('comboEstatus').setValue("");
		store.commitChanges(); 
	}	
//----------------------------Fin procesarEstatus--------------------------------

//---------------------------- procesarTransaccion--------------------------------
	var procesarTransaccion = function(store, arrRegistros, opts) {
		store.insert(0,new Tran({ 
		clave: "", 
		descripcion: "TODAS", 
		loadMsg: ""})); 		
    Ext.getCmp('comboTransaccion').setValue("");
		store.commitChanges(); 
	}	
//----------------------------Fin procesarTransaccion--------------------------------


//---------------------------- procesarIntermediario--------------------------------
	var procesarIntermediario = function(store, arrRegistros, opts) {
		store.insert(0,new Inter({ 
		clave: "", 
		descripcion: "Seleccionar...", 
		loadMsg: ""})); 		
    Ext.getCmp('comboIntermediarioFinanciero').setValue("");
		store.commitChanges(); 
	}	
//----------------------------Fin procesarIntermediario--------------------------------


//---------------------------catalogoEstatus-----------------------------
var catalogoEstatus = new Ext.data.JsonStore({
  id					: 'catalogoEstatus',
  root 				: 'registros',
  fields 			: ['clave', 'descripcion', 'loadMsg'],
  url 				: 'paramcorreo01EXT.data.jsp',
  baseParams	: {
              informacion		: 'catalogoest'  
              },
  totalProperty 	: 'total',
  autoLoad			: false,
  listeners		: {
              load:procesarEstatus,
              beforeload: NE.util.initMensajeCargaCombo,
              exception: NE.util.mostrarDataProxyError								
              }
});//-----------------------Fin catalogoEstatus-----------------------------

//---------------------------catalogoTransaccion-----------------------------
var catalogoTransaccion = new Ext.data.JsonStore({
  id					: 'catalogoTransaccion',
  root 				: 'registros',
  fields 			: ['clave', 'descripcion', 'loadMsg'],
  url 				: 'paramcorreo01EXT.data.jsp',
  baseParams	: {
              informacion		: 'catalogotran'  
              },
  totalProperty 	: 'total',
  autoLoad			: false,
  listeners		: {			
              load: procesarTransaccion,
              beforeload: NE.util.initMensajeCargaCombo,
              exception: NE.util.mostrarDataProxyError								
              }
});//-----------------------Fin catalogoTransaccion-----------------------------

//---------------------------Catalogo Intermediario-----------------------------
var catalogoIntermediario = new Ext.data.JsonStore({
  id					: 'catalogoIntermediario',
  root 				: 'registros',
  fields 			: ['clave', 'descripcion', 'loadMsg'],
  url 				: 'paramcorreo01EXT.data.jsp',
  baseParams	: {
              informacion		: 'catalogoint'  
              },
  totalProperty 	: 'total',
  autoLoad			: false,
  listeners		: {			
              load: procesarIntermediario,
              beforeload: NE.util.initMensajeCargaCombo,
              exception: NE.util.mostrarDataProxyError								
              }
});//-----------------------Fin Catalogo Intermediario-----------------------------

//-----------------
var gridEpo = new Ext.grid.GridPanel({
		id: 'gridEpo',
		store: correoEpo,
		height: 100,
		width: 350,
		enableColumnMove: false,
		enableColumnHide: false,
		columns: [
				{
				header: 'Descripci�n',
				tooltip: 'Descripci�n',
				width : 320 ,
				dataIndex: 'descripcion',
				sortable: true,
				resizable: true,
				align: 'left'
			}
		],
    listeners : {
    cellclick: function(grid, rowIndex, columnIndex, e) {
                var record = grid.getStore().getAt(rowIndex);  // Get the Record
                var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                var data = record.get(fieldName);                
                Ext.getCmp('validaEpo').setValue(rowIndex);
            }
    }
	});
//-----------------

//----------------------------------
var gridNafin = new Ext.grid.GridPanel({
		id: 'gridNafin',
		store: correoNafin,
		height: 100,
		width: 350,
		enableColumnMove: false,
		enableColumnHide: false,
		columns: [
				{
				header: 'Descripci�n',
				tooltip: 'Descripci�n',
				width : 320 ,
				dataIndex: 'descripcion',
				sortable: true,
				resizable: true,
				align: 'left'
			}
		],
    listeners : {
    cellclick: function(grid, rowIndex, columnIndex, e) {
                var record = grid.getStore().getAt(rowIndex);  // Get the Record
                var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                var data = record.get(fieldName);
                
                Ext.getCmp('validaNafin').setValue(rowIndex);
            }
    }
	});
//-----------------------

//-------------------------------Elementos Forma--------------------------------	
var elementosForma =  [	
{
xtype: 'compositefield',
 items: [  {
  xtype : 'displayfield',
  value : 'Intermediario Financiero:',
  width : 150
  },{
  xtype				: 'combo',	
  name				: 'comboIntermediarioFinanciero',	
  hiddenName	: 'intermed',	
  id					: 'comboIntermediarioFinanciero',	
  mode				: 'local',
  displayField: 'descripcion',
  valueField 	: 'clave',
  forceSelection : true,	
  triggerAction  : 'all',	
  editable    : true,
  minChars 		: 1,
  typeAhead		: true,
  width				: 395,
  //tpl				  : NE.util.templateMensajeCargaCombo,
  tpl				: NE.util.templateMensajeCargaComboConDescripcionCompleta,
  store 			: catalogoIntermediario,
  listeners   :{
    select    :{
      fn      :function(combo){
     
       var store = Ext.getCmp('comboEstatus').getStore();
     /*   store.insert([x+1], new Est({ clave: info.estatus[x].clave, 		descripcion:info.estatus[x].descripcion, 		loadMsg: ""}));
        store.commitChanges();*/
        store.reload(procesarEstatus);   
        
      Ext.Ajax.request({
        url: 'paramcorreo01EXT.data.jsp',
        params: Ext.apply(fp.getForm().getValues(), {
          informacion: 'carga'
          }),
        callback:procesarInformacion 								
        });
      }//fin fn
     }//fin select
    }//fin listener
  }]
},{
xtype: 'compositefield',
 items: [  {
  xtype : 'displayfield',
  value : 'Transacci�n:',
  width : 150
  },{
  xtype				: 'combo',	
  name				: 'comboTransaccion',	
  hiddenName	: 'transacc',	
  id					: 'comboTransaccion',	
  mode				: 'local',
  displayField: 'descripcion',
  valueField 	: 'clave',
  forceSelection : true,	
  triggerAction  : 'all',	
  editable    : true,
  minChars 		: 1,
  typeAhead		: true,
  width				: 395,
  tpl				  : NE.util.templateMensajeCargaCombo,
  store 			: catalogoTransaccion,
  listeners   :{
    select    :{
      fn      :function(combo){
     
       var store = Ext.getCmp('comboEstatus').getStore();
     /*   store.insert([x+1], new Est({ clave: info.estatus[x].clave, 		descripcion:info.estatus[x].descripcion, 		loadMsg: ""}));
        store.commitChanges();*/
        store.reload(procesarEstatus);
        
      Ext.Ajax.request({
        url: 'paramcorreo01EXT.data.jsp',
        params: Ext.apply(fp.getForm().getValues(), {
          informacion: 'carga'
          }),
        callback:procesarInformacion 								
        });
      }//fin fn
     }//fin select
    }//fin listener
  }]
}, {
xtype: 'compositefield',
 items: [ {
  xtype : 'displayfield',
  value : 'Estatus a Notificar:',
  width : 150
  }, {
  xtype				: 'combo',	
  name				: 'comboEstatus',	
  hiddenName	: 'estatus',	
  id					: 'comboEstatus',		
  mode				: 'local',
  displayField: 'descripcion',
  valueField 	: 'clave',
  forceSelection : true,	
  triggerAction  : 'all',	
  editable    : true,
  minChars 		: 1,
  typeAhead		: true,
  width				: 395,
  tpl				  : NE.util.templateMensajeCargaCombo,
  store 			: catalogoEstatus,
  listeners   :{
    select    :{
      fn      :function(combo){
     
       var store = Ext.getCmp('comboEstatus').getStore();
     /*   store.insert([x+1], new Est({ clave: info.estatus[x].clave, 		descripcion:info.estatus[x].descripcion, 		loadMsg: ""}));
        store.commitChanges();*/
        //store.reload(procesarEstatus);
        
      Ext.Ajax.request({
        url: 'paramcorreo01EXT.data.jsp',
        params: Ext.apply(fp.getForm().getValues(), {
          informacion: 'carga'
          }),
        callback:procesarInformacion 								
        });
      }//fin fn
     }//fin select
    }//fin listener
  }]
},{
  xtype : 'displayfield',
  width : 200,
  height: 15
  },{
xtype: 'compositefield',
combineErrors: false,
items: [
    {
    xtype : 'displayfield',
    value : '<center>Cuentas de Correo NAFIN</center>',
    width : 200
    },{
    xtype: 'displayfield',
    width : 15 
    },{
    xtype: 'button',
    text:'Agregar',
    iconCls:'icoAgregar',
    id: 'agregaNafin',
    width : 100,
    handler: function() {
    
    var correoNafin = Ext.getCmp('correoNafin').getValue();
    
    var ereg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    var testResult = ereg.test(correoNafin);        
  
  if (Ext.getCmp('correoNafin').isValid() & Ext.getCmp('correoNafin').getValue() !="" ){
      var grid = Ext.getCmp('gridNafin');
      var store = grid.getStore();
      var cmp=0; 
      var uno=1;
      if (store.getCount()== 0){
        TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"}
        ]);
        var record = new TaskLocation({
            clave : Ext.id(),
            descripcion: correoNafin
        });    
        store.add(record);
        store.commitChanges();
        Ext.getCmp('correoNafin').setValue('');
        uno++;
      } else {
      store.each(function(record) {
        var existe = record.data['descripcion'];
        var nuevo  =  correoNafin;
        if (nuevo != existe){           
            cmp=0;
        }else {
          cmp++;
          uno++;
        }
      }); }
      if (cmp > 0){ 
      Ext.Msg.show({
          title: 'Cuentas de Correo Nafin',
          msg: 'La direcci�n de correo proporcionada ya existe en la lista,<br>por lo que no ser� agregada nuevamente.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoNafin');		
                  correo.focus();
              }
          });
      } 
      if (uno == 1 & cmp == 0 ){
       TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"}
        ]);
        var record = new TaskLocation({
            clave : Ext.id(),
            descripcion: correoNafin
        });    
        store.add(record);
        store.commitChanges();
        Ext.getCmp('correoNafin').setValue('');
      } else { } 
      
    } else{          
      if (correoNafin == "" ){
          Ext.Msg.show({
          title: 'Cuentas de Correo Nafin',
          msg: 'Debe especificar una cuenta de correo.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoNafin');		
                  correo.focus();
              }
          });
          }
        if ( ! Ext.getCmp('correoNafin').isValid() ){
          Ext.Msg.show({
          title: 'Cuentas de Correo Nafin',
          msg: 'La cuenta de correo proporcionada <br> no es v�lida, por lo que no sera agregada.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoNafin');		
                  correo.focus();
              }
          });
        }  
       }// FIN ELSE
      }//FIN HANDLER
    },{
    xtype: 'button',
    text:'Quitar',
    iconCls:'icoCancelar',
    id: 'quitaNafin',
    width : 100,
    handler: function() {
   
      var grid = Ext.getCmp('gridNafin');
      var store = grid.getStore();
      var indice = Ext.getCmp('validaNafin').getValue();
      var filas=0;
      store.each(function(record) {
        filas++;
      });
     if(filas>0){ 
       if (indice !=""){
        var rec = grid.getStore().getAt(indice);
        store.remove(rec);
        store.commitChanges();
        Ext.getCmp('validaNafin').setValue("");        
      } else {
          Ext.Msg.show({
          title: 'Cuentas de Correo Nafin',
          msg: 'Debe especificar una cuenta de correo.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
      }
    } else {
      Ext.Msg.show({
          title: 'Cuentas de Correo Nafin',
          msg: 'No hay ninguna direcci�n de correo v�lida que se pueda borrar.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
      }//fin else 
      }//fin handler
    },{
    xtype : 'textfield',
    id : 'validaNafin',
    msgTarget: 'side',
    width : 50,
    hidden: true
    }
  ]
},{
xtype: 'compositefield',
combineErrors: false,
msgTarget: 'side',
items: [{
  xtype : 'textfield',
  id    : 'correoNafin',
  vtype : 'email',
  msgTarget: 'side',
  width : 200
  },{
  xtype: 'displayfield',
  width : 15 
  },gridNafin
  ]
},{
xtype: 'compositefield',
combineErrors: false,
items: [
    {
    xtype: 'displayfield',
    value:'<center>Cuentas de Correo IF</center>',
    width : 200
    },{
    xtype: 'displayfield',
    width : 15 
    },{
    xtype: 'button',
    text:'Agregar',
    iconCls:'icoAgregar',
    id: 'agregaEpp',
    width : 100,
    handler: function() {
    var correoIf = Ext.getCmp('correoEpo').getValue(); 
     if (Ext.getCmp('correoEpo').isValid() & Ext.getCmp('correoEpo').getValue() !="" ){
     var grid = Ext.getCmp('gridEpo');
      var store = grid.getStore();
      var cmp=0; 
      var uno=1;
      if (store.getCount()== 0){
        TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"}
        ]);
        var record = new TaskLocation({
            clave : Ext.id(),
            descripcion: correoIf
        });    
        store.add(record);
        store.commitChanges();
        Ext.getCmp('correoEpo').setValue('');
        uno++;
      } else {
      store.each(function(record) {
        var existe = record.data['descripcion'];
        var nuevo  =  correoIf;
        if (nuevo != existe){           
            cmp=0;
        }else {
          cmp++;
          uno++;
        }
      }); }
      if (cmp > 0){ 
      Ext.Msg.show({
          title: 'Cuentas de Correo If',
          msg: 'La direcci�n de correo proporcionada ya existe en la lista,<br>por lo que no ser� agregada nuevamente.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoEpo');		
                  correo.focus();
              }
          });
      } 
      if (uno == 1 & cmp == 0 ){
       TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"}
        ]);
        var record = new TaskLocation({
            clave : Ext.id(),
            descripcion: correoIf
        });    
        store.add(record);
        store.commitChanges();
        Ext.getCmp('correoEpo').setValue('');
      } else {   }
     } else {
        if (correoIf == "" ){
          Ext.Msg.show({
          title: 'Cuentas de Correo If',
          msg: 'Debe especificar una cuenta de correo.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoEpo');		
                  correo.focus();
              }
          });
          }
        if ( ! Ext.getCmp('correoEpo').isValid() ){
          Ext.Msg.show({
          title: 'Cuentas de Correo If',
          msg: 'La cuenta de correo proporcionada <br> no es v�lida, por lo que no sera agregada.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoEpo');		
                  correo.focus();
              }
          });
        }
      }// FIN ELSE
      }//FIN HANDLER
    },{
    xtype: 'button',
    text:'Quitar',
    iconCls:'icoCancelar',
    id: 'quitaEpo',
    width : 100,
    handler: function() {
      var grid = Ext.getCmp('gridEpo');
      var store = grid.getStore();
      var indice = Ext.getCmp('validaEpo').getValue(); 
      var filas=0;
      store.each(function(record) {
        filas++;
      });
     if(filas>0){ 
      if (indice !=""){
        var rec = grid.getStore().getAt(indice);
        store.remove(rec);
        store.commitChanges();
        Ext.getCmp('validaEpo').setValue("");        
      } else {
          Ext.Msg.show({
          title: 'Cuentas de Correo If',
          msg: 'Debe especificar una cuenta de correo.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
      } 
    } else {
      Ext.Msg.show({
          title: 'Cuentas de Correo If',
          msg: 'No hay ninguna direcci�n de correo v�lida que se pueda borrar.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
        }//fin else
      }//fin handler
    },{
    xtype : 'textfield',
    id : 'validaEpo',
    msgTarget: 'side',
    width : 50,
    hidden: true
    }]
},{
xtype: 'compositefield',
combineErrors: false,
msgTarget: 'side',
items: [{
  xtype     : 'textfield',
  id : 'correoEpo',
  vtype : 'email',
  msgTarget: 'side',
  width : 200
  },{
  xtype: 'displayfield',
  width : 15 
  },gridEpo
  ]
}
];
//-----------------------------Fin Elementos Forma------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: '<center>Parametrizacion de Correo para Notificaciones.</center>',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 1,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls: 'icoGuardar',
				handler: function (boton, evento){
          if (Ext.getCmp('comboIntermediarioFinanciero').getValue() == ""){
            Ext.Msg.show({
              title: 'Intermediario Financiero',
              msg: 'Es obligatorio seleccionar el Intermediario Financiero.',
              modal: true,
              icon: Ext.Msg.WARNING,
              buttons: Ext.Msg.OK
              });
            } else if (Ext.getCmp('comboEstatus').getValue() == ""){
                Ext.Msg.show({
                  title: 'Estatus',
                  msg: 'Debe seleccionar un Estatus.',
                  modal: true,
                  icon: Ext.Msg.WARNING,
                  buttons: Ext.Msg.OK
                  });
            } else if (false){
              } else {
                procesarGuardar();                           
            }
        	}//fin handler
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function (boton, evento){
					Ext.getCmp('forma').getForm().reset();
           var grid = Ext.getCmp('gridNafin');
           var store = grid.getStore();
            store.each(function(record) {
            store.remove(record);
            store.commitChanges();
           });
           var gridE = Ext.getCmp('gridEpo');
           var storeE = gridE.getStore();
            storeE.each(function(record) {
            storeE.remove(record);
            storeE.commitChanges();
           });
          window.location  = "/nafin/29garantias/29if/paramcorreo01EXT.jsp"; 
				}
			}
		]
	});
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fp
		]
	})
//-----------------------------Fin Contenedor Principal-------------------------
catalogoIntermediario.load();
catalogoTransaccion.load();
catalogoEstatus.load();
});//-----------------------------------------------Fin Ext.onReady(function(){}