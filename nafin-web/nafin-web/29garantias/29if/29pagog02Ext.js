Ext.onReady(function() {
var iIcIfSiag;
//---------------------Handlers--------------------------------------
var procesarSucnocessFailureGenerarArchivo = function(opts, success, response){
		verDetalles.show();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnDetalleF= Ext.getCmp('detalleF');
			
			
			if(Ext.util.JSON.decode(response.responseText).urlArchivo==''){
				btnDetalleF.hide();
			}else{
				btnDetalleF.setHandler(function(boton,evento){
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					alert(Ext.util.JSON.decode(response.responseText).urlArchivo);
					forma.submit();
				});
			}
			if(Ext.util.JSON.decode(response.responseText).urlArchivo==''){
							NE.util.mostrarConnError(response,opts);

				}
			
		}
	}
	

function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	
var procesarImagen = function(grid, rowIndex, colIndex, item, event) {  
		var registro = storeImg.getAt(rowIndex);
							Ext.Ajax.request({
							url: '29pagog02Ext.data.jsp',
							params: Ext.apply({informacion:'ConsultarImagen',claveImagen: registro.get('IC_IMAGEN_SOLPAGO') }),
							callback: procesarArchivoPdfSuccess
						});
	}
var procesarConsultaData = function(store,arrRegistros,opts){

			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
			 iIcIfSiag = store.reader.jsonData.iIcIfSiag;
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}


function procesarArchivoPdfSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
var procesarArchivoPdf = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consulta.getAt(rowIndex);
		folio=registro.get('IC_FOLIO');
					Ext.Ajax.request({
							url: '29pagog02apdfExt.jsp',
							params: Ext.apply({folio:folio  }),
							callback: procesarArchivoPdfSuccess
						});
		
	}
	
function procesarImagenesSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			storeImg.loadData(Ext.util.JSON.decode(response.responseText).registros);
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
var procesarImagenes = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consulta.getAt(rowIndex);
		folio=registro.get('IC_FOLIO');
		verImagen.show();
					Ext.Ajax.request({
							url: '29pagog02Ext.data.jsp',
							params: Ext.apply({folio:folio,informacion:'Imagen' }),
							callback: procesarImagenesSuccess
						});
		
	}

	
var procesarArchivo2Pdf = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consulta.getAt(rowIndex);
		folio=registro.get('IC_FOLIO');
					Ext.Ajax.request({
							url: '29pagog02bpdfExt.jsp',
							params: Ext.apply({folio:folio,iIcIfSiag:iIcIfSiag,claveGarantia: registro.get('CC_GARANTIA'),fiso:registro.get('IC_CONTRAGARANTE') }),
							callback: procesarArchivoPdfSuccess
						});
		
	}

//---------------------------------Stores----------------------------------------
var catalogoSituacion = new Ext.data.JsonStore
  ({
	   id: 'catologoSituacion',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29pagog02Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoSituacion'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '29pagog02Ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'FECHA'},
				{name: 'IC_FOLIO'},
				{name: 'SITUACION'},
				{name: 'CC_GARANTIA'},
				{name: 'IC_SITUACION'},
				{name: 'IC_CONTRAGARANTE'}
				
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
//------------------------------Componentes-----------------------------------------

var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						
						header:'Fecha y hora de <br/>Operaci�n',
						tooltip: 'Fecha y hora de <br/>Operaci�n',
						sortable: true,
						dataIndex: 'FECHA',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						
						header: 'Folio de la Operaci�n',
						tooltip: 'Folio de la Operaci�n',
						sortable: true,
						dataIndex: 'IC_FOLIO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Situaci�n',
						tooltip: 'Situaci�n',
						sortable: true,
						width: 170,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						xtype: 'actioncolumn',
						header: 'Solicitud de Pago de <br/>Garant�a',
						tooltip: 'Solicitud de Pago de <br/>Garant�a',
						sortable: true,
						width: 120,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							       return 'iconoLupa';	
								 }
								 ,handler: procesarArchivoPdf
					      }]
						},
						{
						xtype: 'actioncolumn',
						header: 'Archivo de <br/>Resultados',
						tooltip: 'Archivo de <br/>Resultados',
						sortable: true,
						width: 120,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       var situacion = registro.get('IC_SITUACION');
									 if(situacion==5||situacion==8){
										this.items[0].tooltip = 'Ver';								
										return 'iconoLupa';	
									 }else{
										return '';
									 }
								 }
								 ,handler: procesarArchivo2Pdf
					      }]
						
						},
						{
						xtype: 'actioncolumn',
						align:'center',
						header: 'Documentos anexos<br/> a la solicitud',
						tooltip: 'Documentos anexos<br/> a la solicitud',
						sortable: true,
						dataIndex: 'TIPO_COBRANZA',
						width: 120,
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							       return 'iconoLupa';	
								 }
								 ,handler: procesarImagenes
					      }]
						}
						
						
				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 860,
				style: 'margin:0 auto;',
				frame: true
				
		});
var elementosForma= [
						{
						xtype: 'compositefield',
						fieldLabel: 'Fecha de Operaci�n',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fechaInc1',
								id: 'fechaInc1',
								startDay: 0,
								minValue: '01/01/1901',
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'fechaInc2',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'a',
								width: 50
							},
							{
								xtype: 'datefield',
								name: 'fechaInc2',
								id: 'fechaInc2',
								startDay: 1,
								minValue: '01/01/1901',
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoInicioFecha: 'fechaInc1',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					},
					{
						xtype: 'combo',
						fieldLabel: 'Situaci�n',
						emptyText: 'Seleccionar...',
						displayField: 'descripcion',
						valueField: 'clave',
						triggerAction: 'all',
						//anchor:'60%',
						typeAhead: true,
						minChars: 1,
						store: catalogoSituacion,
						tpl: NE.util.templateMensajeCargaCombo,
						name:'Hsituacion',
						id: 'situacion',
						mode: 'local',
						hiddenName: 'Hsituacion',
						forceSelection: true
					},{
						xtype: 'textfield',
						name: 'folio',
						id: 'folio',
						fieldLabel: 'Folio de Operaci�n',
						maxLength: 30,
						//anchor:'40%',
						maskRe: /[0-9]/,
						regex:/^\d+$/,
						msgTarget: 'side'
					}
	]

var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 500,
		title:	'<Center>Solicitud de Pago de Garant�a - Consulta Resultados</Center>',
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: [elementosForma,NE.util.getEspaciador(5)],
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Consultar',
			  name: 'btnBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						if(verificaFechas('fechaInc1','fechaInc2')){
							grid.show();
							consulta.load({ params: Ext.apply(fp.getForm().getValues())});
						}
					}
			 },{
				text:'Limpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});
	//------------------VENTANA IMAGEN----------------------
	var storeImg = new Ext.data.JsonStore({
		fields: [
			{name:'CG_DESCRIPCION'},{name:'IC_IMAGEN_SOLPAGO'}
			],
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
	var gridImagenes = new Ext.grid.GridPanel({
				id: 'gridImg',
				hidden: false,
				header: true,
				title: '<center>Im�genes Cargadas</center>',
				store: storeImg,
				stripeRows: false,
				//loadMask: true,
				height: 135,
				width: 270,
				enableColumnResize: false,
				frame: true,
				columns:[
				//CAMPOS DEL GRID
						{
						xtype: 'actioncolumn',
						width: 70,
						menuDisabled: true,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							       return 'iconoLupa';	
								 }
								 ,handler: procesarImagen
					      }]	
			       },
						{
						
						dataIndex: 'CG_DESCRIPCION',
						width: 180,
						align: 'center',
						menuDisabled: true,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						}
				]
				
		});

var verImagen = new Ext.Window({
			width: 290,
			height: 200,
			maximizable: false,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			maximized: false,
			constrain: true,
			items: [gridImagenes],//fpImg],
			buttons:[
				{
							xtype: 'button',
							text: 			'Regresar',
							handler:    function(boton, evento) {
								verImagen.setVisible(false);
							}
						}
			]
});		

	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		940,
		height: 		'auto',
		disabled: 	false,
		items: 	[fp,NE.util.getEspaciador(30),grid]
	});
	
	catalogoSituacion.load();
});