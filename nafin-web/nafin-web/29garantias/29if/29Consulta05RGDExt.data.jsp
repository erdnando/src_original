<%@ page 
contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
import = "java.util.*, com.netro.model.catalogos.*,net.sf.json.*,
	com.netro.garantias.*"%>
<%@ include file="/appComun.jspf" %>

<%@ include file="/29garantias/29secsessionWMenu.jspf" %>
<jsp:useBean id="consulta" scope="page" class="com.netro.garantias.ConsSolicIFGAbean" />
<%

String informacion= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String tipoUsuarioLog= (request.getParameter("tipoUsuarioLog")==null)?"":request.getParameter("tipoUsuarioLog");
String ifSelct= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");

String resultado="";
	if(informacion.equals("Init")){
		System.out.println("tipo de user-------->>>"+strTipoUsuario);
		resultado = "{\"success\": true,\"tipoUsuario\":'" + strTipoUsuario + "'}";
	}
	else if(informacion.equals("CatalogoIF")){
	
		CatalogoIF cat = new CatalogoIF();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setOrden("cg_razon_social");		
		List elementos = cat.getListaElementos();
		Iterator it = elementos.iterator();
		JSONArray jsonArr = new JSONArray();
		
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		resultado= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
		
	}
	else if(informacion.equals("Consulta")) {
		
		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		String fechaHoy = formatoFecha.format(calendario.getTime());		
		
		String fecha_oper_ini = request.getParameter("df_consultaMin") == null?fechaHoy:(String)request.getParameter("df_consultaMin");
		String fecha_oper_fin = request.getParameter("df_consultaMax") == null?fechaHoy:(String)request.getParameter("df_consultaMax");
		String cbo_situacion  = request.getParameter("situacion") == null?"":(String)request.getParameter("situacion");
		ConsSolicIFGAbeanExt paginador = new ConsSolicIFGAbeanExt();
		paginador.setiTipoOperacion("10");//****************************************
		if(tipoUsuarioLog.equals("NAFIN"))
			paginador.setiIF(ifSelct);
		else
			paginador.setiIF(iNoCliente);
		paginador.setFechaOperIni(fecha_oper_ini);
		paginador.setFechaOperFin(fecha_oper_fin);
		paginador.setSituacion(cbo_situacion);
	
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		Registros reg = queryHelper.doSearch();
		if(reg != null ) {
			resultado = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
		}
		else {
			resultado = "{\"success\": false }";
		}
	   
	}
	else if(informacion.equals("Situacion"))
	{
		CatalogoSituacion catsit = new CatalogoSituacion();
		catsit.setCampoClave("ic_situacion");
		catsit.setCampoDescripcion("cg_descripcion");
		catsit.setClaveTipoOperacion("10"); //Recuperaciones autom�ticas*******************
		List clavesSit = new ArrayList();
		clavesSit.add("1");	//Envio para validacion y carga
		clavesSit.add("5");	//Confirmaci�n
		catsit.setValoresCondicionIn(clavesSit);
		catsit.setOrden("ic_situacion");
		List elementos = catsit.getListaElementos();
		Iterator it = elementos.iterator();
		JSONArray jsonArr = new JSONArray();
		
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		resultado= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
		
	}
	
	//Creacion de PDF
	else if(informacion.equals("pdf"))
	{
	/*consulta.getPathFilePDF(String clave_if,String folio,String origen,String cvePerf,
	String strDirectorioTemp,HttpSession session,String strDirectorioPublicacion,String iNoCliente);*/
	System.out.println("...::Generar archivo PDF::..");
	
	String clave_if = request.getParameter("clave_if");
	String folio = request.getParameter("folio");
	String origen = request.getParameter("origen");
	String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);

	String nomFile=consulta.getPathFilePDF(clave_if,folio,origen,cvePerf,
	strDirectorioTemp,session,strDirectorioPublicacion,iNoCliente);
			if(nomFile.equals("error"))
			{
				resultado=
				"{\"success\": false, \"urlArchivo\": \"error"	+	 "\""+ "}";
			}
			else
			{
				resultado=
				"{\"success\": true, \"urlArchivo\": \""	+	strDirecVirtualTemp+nomFile+ "\""+ "}";	
			}
	}else if(informacion.equals("archivo"))
	{
		
		String folio = request.getParameter("folio");
		String opcion = request.getParameter("opcion");
		String clave_if = "";
		
		if (strTipoUsuario.equals("NAFIN")) {
			//Solo si es NAFIN puede ver lo de cualquier IF, por eso lee el parametro.
			//Si es IF solo puede ver lo de el mismo... por lo que se tomar� iNoCliente
			clave_if = request.getParameter("clave_if")!=null?request.getParameter("clave_if"):"";
		} else {
			clave_if = iNoCliente;
		}
		
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet rs = null;
		String qrySentencia	= "";
		String tabla = "";
		int vigencia = 0;
		int i = 0;
		
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo = null;
		
		try {
			con.conexionDB();
			if(opcion.equals("origen")){
			 tabla = "gti_contenido_arch";
		  }else{
			 tabla = "gti_arch_rechazo";
		  }
		  
			qrySentencia =
				"select cg_contenido"+
				" from "+tabla+" A"+
				" ,comcat_if I"+
				" where A.ic_if_siag = I.ic_if_siag"+
				" and A.ic_folio = ?"+
				" and I.ic_if = ?"+
				" order by A.ic_linea";
			
			ps = con.queryPrecompilado(qrySentencia);
			ps.setBigDecimal(1, new java.math.BigDecimal(folio));
			ps.setLong(2, Long.parseLong(clave_if));
			
			rs = ps.executeQuery();
			while(rs.next()){
				contenidoArchivo.append(rs.getString("cg_contenido")+"\n");
				i++;
			}
			ps.close();
			
			if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt"))
			{
				
				resultado="{\"success\": false, \"urlArchivo\": \"error"	+ "\""+ "}";	
				///out.print("<--!Error al generar el archivo-->");
				}
			else
			{
				if(i>0)
				{
					nombreArchivo = archivo.nombre;
					resultado="{\"success\": true, \"urlArchivo\": \""	+	strDirecVirtualTemp+nombreArchivo+ "\""+ "}";			
				}
				else
				{
					resultado="{\"success\": true, \"urlArchivo\": \"NOREG"	+ "\""+ "}";			
				}
			}
				
		
			}catch(Exception e){
			e.printStackTrace();
			
			}finally{
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
			}
	}
	//actions
	else if(informacion.equals("pdf_a"))
	{
	/*consulta.getPathFilePDF(String clave_if,String folio,String origen,String cvePerf,
	String strDirectorioTemp,HttpSession session,String strDirectorioPublicacion,String iNoCliente);*/
	System.out.println("...::Generar archivo PDF::..");
	
	String clave_if = request.getParameter("clave_if");
	String folio = request.getParameter("folio");
	String origen = request.getParameter("origen");
	String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);

	String nomFile=consulta.getPathFilePDF(clave_if,folio,origen,cvePerf,
	strDirectorioTemp,session,strDirectorioPublicacion,iNoCliente);
			if(nomFile.equals("error"))
			{
				resultado=
				"{\"success\": false, \"urlArchivo\": \"error"	+	 "\""+ "}";
			}
			else
			{
				resultado=
				"{\"success\": true, \"urlArchivo\": \""	+	strDirecVirtualTemp+nomFile+ "\""+ "}";	
			}
	}else if(informacion.equals("archivo_a"))
	{
		
		String folio = request.getParameter("folio");
		String opcion = request.getParameter("opcion");
		String clave_if = "";
		if (strTipoUsuario.equals("NAFIN")) {
			//Solo si es NAFIN puede ver lo de cualquier IF, por eso lee el parametro.
			//Si es IF solo puede ver lo de el mismo... por lo que se tomar� iNoCliente
			clave_if = request.getParameter("clave_if")!=null?request.getParameter("clave_if"):"";
		} else {
			clave_if = iNoCliente;
		}
		
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet rs = null;
		String qrySentencia	= "";
		String tabla = "";
		int vigencia = 0;
		int i = 0;
		
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo = null;
		
		try {
			con.conexionDB();
			if(opcion.equals("origen")){
			 tabla = "gti_contenido_arch";
		  }else{
			 tabla = "gti_arch_rechazo";
		  }
		  
			qrySentencia =
				"select cg_contenido"+
				" from "+tabla+" A"+
				" ,comcat_if I"+
				" where A.ic_if_siag = I.ic_if_siag"+
				" and A.ic_folio = ?"+
				" and I.ic_if = ?"+
				" order by A.ic_linea";
			
			ps = con.queryPrecompilado(qrySentencia);
			ps.setBigDecimal(1, new java.math.BigDecimal(folio));
			ps.setLong(2, Long.parseLong(clave_if));
			
			rs = ps.executeQuery();
			while(rs.next()){
				contenidoArchivo.append(rs.getString("cg_contenido")+"\n");
				i++;
			}
			ps.close();
			
			if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt"))
			{
				
				resultado="{\"success\": false, \"urlArchivo\": \"error"	+ "\""+ "}";	
				///out.print("<--!Error al generar el archivo-->");
				}
			else
			{
				if(i>0)
				{
					nombreArchivo = archivo.nombre;
					resultado="{\"success\": true, \"urlArchivo\": \""	+	strDirecVirtualTemp+nombreArchivo+ "\""+ "}";			
				}
				else
				{
					resultado="{\"success\": true, \"urlArchivo\": \"NOREG"	+ "\""+ "}";			
				}
			}
				
		
			}catch(Exception e){
			e.printStackTrace();
			
			}finally{
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
			}
	}
%>
<%=resultado%>
