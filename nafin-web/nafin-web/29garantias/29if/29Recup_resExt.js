Ext.onReady(function() {
var ahora=new Date();
var mes = (parseInt(ahora.getMonth()) + 1)<10?0+(parseInt(ahora.getMonth()) + 1).toString():(parseInt(ahora.getMonth()) + 1).toString();
var fecha =Ext.util.Format.date(ahora,'d/m/Y');
//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
var bajarArchivo =  function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var arcName= Ext.util.JSON.decode(response.responseText).urlArchivo;
				
				if(arcName=='NOREG')
				{
					Ext.MessageBox.alert("Error",	"No se encuentra ningun registro");
				}
				else
				{
					arcName = arcName.replace('/nafin','');
					var params = {nombreArchivo: arcName};	
					fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					fp.getForm().getEl().dom.submit();
					/*
					var forma = Ext.getDom('formAux');
					forma.action = arcName;
					forma.submit();*/
				}
			
		} else {
			Ext.MessageBox.alert("Error",
			"No se pudo generar el documento");
		}
	}
	
var bajarPDF =  function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			
		} else {
			Ext.MessageBox.alert("Error",
			"No se pudo generar el documento");
		}
	}
var procesarConsultaData = function(store, arrRegistros, opts) {
	
		var grid = Ext.getCmp('grid');
		
		if (arrRegistros != null) {
			
			if (!grid.isVisible()) {
				grid.show();
			}
		

			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
			
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	/**
* Boolean renderer 
*/
var booleanRenderer = function (value, metadata, record) {
    if (value == '')
       out = '0';
    else 
      out =value;

    return out;

} 
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN

 //Situacion
 var catalogoSituacion= new Ext.data.JsonStore
  ({
	   id: 'catSituacion',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29Recup_resExt.data.jsp',
		baseParams: 
		{
		 informacion: 'Situacion'
		},
		autoLoad: true,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '29Recup_resExt.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [		
			{name: 'CLAVE_IF'},
			{name: 'NOMBRE_IF'},
			{name: 'TIPO_ENVIO'},			
			{name: 'FECHA'},
			{name: 'IC_FOLIO'},
			{name: 'SITUACION'},
			{name: 'IN_REGISTROS_ACEP'},
			{name: 'IN_REGISTROS_RECH'},
			{name: 'IN_REGISTROS'},
			{name: 'IC_SITUACION'},
			{name: 'MONTO_ENVIADO'},
			{name: 'REPROCESO'}
			
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
		
	});

	var elementosFormaDetalle = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_consultaMin',
					id: 'df_consultaMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_consultaMax',
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					formBind: true,
					value:fecha
					},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_consultaMax',
					id: 'df_consultaMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_consultaMin',
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					value:fecha

				}
			]
		},
		{//Ciudad
							xtype: 'combo',
							name: 'situacion',
							hiddenName : 'situacion',
							id: 'situacionID',
							fieldLabel: 'Situaci�n',
							emptyText:'Seleccione...',
							mode: 'local',
						  triggerAction: 'all',
						  valueField: 'clave',
						  store: catalogoSituacion,
						   allowBlank: true,
						  displayField: 'descripcion',
							width: 200
						}
	];
	var fpDetalle = {
		xtype: 'form',
		id: 'formaDetalle',
		width: 450,
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaDetalle,
		monitorValid: true,
		buttons: [
			{
				text:'Consultar',
				id: 'btnBuscarDetalle',
				iconCls: 'icoBuscar',
				formBind: true,
				
				handler: function(boton, evento) {
					var cmpForma = Ext.getCmp('formaDetalle');
					var params = (cmpForma)?cmpForma.getForm().getValues():{};
				
				
	
				var fechaOper1 = Ext.getCmp('df_consultaMin');
				var fechaOper2 = Ext.getCmp('df_consultaMax');
								
				if (!Ext.isEmpty(fechaOper1.getValue()) || !Ext.isEmpty(fechaOper2.getValue()) ) 
				{
					if(Ext.isEmpty(fechaOper1.getValue()))	{
						fechaOper1.markInvalid('Debe capturar ambas fechas de operacion o dejarlas en blanco');
						fechaOper1.focus();
						return;
					}else if (Ext.isEmpty(fechaOper2.getValue())){
						fechaOper2.markInvalid('Debe capturar ambas fechas de operacion o dejarlas en blanco');
						fechaOper2.focus();
						return;
					}
				}
				
				consultaDataGrid.load({
							params: Ext.apply(params/*,{
							cc_usuario:Userdetalles,
							ic_cat_proyectos:ProySelected,
							operacion: 'Generar',
							start: 0,
							limit: 15
						}*/)
					});
				}
				
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '29Recup_resExt.jsp'; 
				}
			}
		]
	};
	
	//Grid
	var grid = {
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		columns: [
			{
				header: 'Fecha y Hora de <br>Operaci�n',
				tooltip: 'Fecha y Hora de Operaci�n',
				dataIndex: 'FECHA',
				width: 100,
				resizable: true,
				sortable: true,
				align: 'center'				
			},{
				header: 'Folio de la <br>Operaci�n',
				tooltip: 'Folio de la Operaci�n',
				dataIndex: 'IC_FOLIO',
				sortable: true,	align: 'center',
				width: 150, resizable: true
				
			},{
				header: 'Situaci�n', tooltip: 'Situaci�n',
				dataIndex: 'SITUACION',
				sortable: true,	align: 'center',
				width: 200, resizable: true
				
			},{
				header: 'N�mero de operaciones<br> aceptadas',
				tooltip: 'N�mero de operaciones aceptadas',
				dataIndex: 'IN_REGISTROS_ACEP',
				sortable: true,	align: 'center',
				width: 150, resizable: true,renderer:booleanRenderer
				
			},{
				header: 'N�mero de operaciones<br>con errores',
				tooltip: 'N�mero de operaciones con errores',
				dataIndex: 'IN_REGISTROS_RECH',
				sortable: true,	align: 'center',
				width: 150, resizable: true,renderer:booleanRenderer
				
			},{
				header: 'Total de<br> Operaciones', 
				tooltip: 'Total de Operaciones',
				dataIndex: 'IN_REGISTROS',
				sortable: true,	align: 'center',
				width: 120, resizable: true
				
			},
			{
				xtype: 'actioncolumn',
				header: 'Archivo<br> Origen', tooltip: 'Archivo Origen',
				dataIndex: '',
				width: 60, resizable: true,
				align: 'center', 
				items: [
					{
						iconCls: 'icoLupa',
					
						handler: function(grid, rowIdx, colIds){
							var reg = grid.getStore().getAt(rowIdx);
								
						Ext.Ajax.request({
							url : '29consulta02Ext.data.jsp',
							params: {
								informacion: 'archivo',
								folio:reg.get('IC_FOLIO'),
								opcion:'origen'
							},
							callback: bajarArchivo						});		
						//FIN AJAX
 						}
					}
				]
			},
			{
			 xtype: 'actioncolumn',
			 dataIndex: '',
			 width: 80,
			 header:'Resultados',tooltip:'Resultados',
			 align: 'center',
			 items: [{
				  getClass: function(v, medaData, record) {						
						if(record.get('IC_SITUACION')=="5" || record.get('IC_SITUACION')=="4")
						return 'icoLupa';return '';
				  },
				  handler: function(grid, rowIndex, colIndex) {
								var reg = grid.getStore().getAt(rowIndex);
								
						Ext.Ajax.request({
							url : '29consulta02Ext.data.jsp',
							params: {
								informacion: 'pdf',
								folio:reg.get('IC_FOLIO'),
								origen:'CONSREC'									
							},
							callback: bajarPDF
						});		
						//FIN AJAX	*/
				  }
			 }]
			},{
			 xtype: 'actioncolumn',
			 dataIndex: '',
			 width: 80,
			 header:'Archivo de<br>Errores',tooltip:'Archivo de Errores',
			 align: 'center',
			 items: [{
				  getClass: function(v, medaData, record) {						
						if((record.get('IC_SITUACION')=="5" || record.get('IC_SITUACION')=="4") &&
						parseInt(record.get('IN_REGISTROS_RECH'))>0)
						return 'icoLupa';return '';
				  },
				  handler: function(grid, rowIndex, colIndex) {
						var reg = grid.getStore().getAt(rowIndex);
								
						Ext.Ajax.request({
							url : '29consulta02Ext.data.jsp',
							params: {
								informacion: 'archivo',
								folio:reg.get('IC_FOLIO'),
								opcion:'error'
							},
							callback: bajarArchivo						});		
						//FIN AJAX
				  }
			 }]
			}
			
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		colunmWidth: true,
		frame: true,
		emptyMsg: 'No records',
		collapsible: true
	};
	
var fp = new Ext.form.FormPanel({
	id: 'forma',
	hidden: true
});

var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [fpDetalle,grid,fp]
	});
	
});