Ext.onReady(function() {

	var campoFechas = new Ext.form.DateField({
		startDay: 0
	});


	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	//**********************  Reprocesar Errores *******************************
	var fnTransmitirRegCallback = function(vpkcs7, vtextoFirmar, vic_folio, vic_if){

		if (Ext.isEmpty(vpkcs7)) {
			Ext.getCmp("btnTransmitirReg").disable();
			return;	//Error en la firma. Termina...
		}else  {
			Ext.getCmp('venPreAcuse').setTitle('<center>Su solicitud fue recibida con el n�mero de folio:  '+vic_folio+'</center>');
			consPreAcuseData.load({
				params:{
					ic_folio: vic_folio,
					informacion: 'Transmitir_Registros',
					ic_if: vic_if,
					textoFirmar: vtextoFirmar,
					pkcs7: vpkcs7
				}
			});
		}
	}

	var generaPreacuse = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);

		var  ic_folio = registro.get('IC_FOLIO');
		var ic_if = registro.get('CLAVE_IF');

			consPreAcuseData.load({
				params:{
					ic_folio: ic_folio,
					informacion: 'ConsCifrasControl',
					ic_if:ic_if
				}
			});

		var venPreAcuse = Ext.getCmp('venPreAcuse');
		if(venPreAcuse){
			venPreAcuse.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 530,
				height: 250,
				closeAction: 'destroy',
				closable:false,
				autoDestroy:true,
				id: 'venPreAcuse',
				items: [
					gridGridPreAcuse
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',
					buttons: ['->',
						{
								xtype: 'button',
								text: 'Imprimir',
								tooltip:	'Imprimir',
								id: 'btnImprimirPDF',
								hidden: true,
								iconCls: 'icoPdf',
								handler: function(boton, evento) {
									Ext.Ajax.request({
										url : '/nafin/29garantias/29if/29Consulta03Ext.data.jsp',
										params: {
											tot_registros: Ext.getCmp('tot_registros').getValue(),
											monto_total:	Ext.getCmp('monto_total').getValue(),
											mes_anioCarga:	Ext.getCmp('mes_anioCarga').getValue(),
											fecha: Ext.getCmp('fecha').getValue(),
											hora:	Ext.getCmp('hora').getValue()
										},
										callback: procesarDescargaArchivos
									});
								}
						},
						{	xtype: 'button',
							text: 'Cancelar',
							iconCls: 'icoLimpiar',
							id: 'btnCancelar',
							handler: function(){
								Ext.getCmp('venPreAcuse').destroy();
							}
						},
						'-',
						{	xtype: 'button',
							text: 'Transmitir Registros',
							iconCls: 'icoContinuar',
							id: 'btnTransmitirReg',
							handler: function(boton, evento) {

								var textoFirmar=" Tipo de Operaci�n: 02 Carga de Saldos y Pago de Comisiones \n"+
													 " Mes de carga "+Ext.getCmp('mes_anioCarga').getValue() +"\n"+
													 " No. total de registros transmitidos "+Ext.getCmp('tot_registros').getValue()   +"\n"+
													 " Monto total saldos al fin del mes "+Ext.getCmp('monto_total').getValue();


							 	//NE.util.obtenerPKCS7(fnTransmitirRegCallback, textoFirmar, ic_folio, ic_if);
								Ext.getCmp('venPreAcuse').setTitle('<center>Su solicitud fue recibida con el n�mero de folio:  '+ic_folio+'</center>');
								consPreAcuseData.load({
									params:{
										ic_folio: ic_folio,
										informacion: 'Transmitir_Registros',
										ic_if: ic_if,
										textoFirmar: textoFirmar,
										pkcs7: ''
									}
								});

							}
						}
					]
				}
			}).show().setTitle('');
		}
	}

	var procesarConsPreAcuseData = function(store,arrRegistros,opts){

		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros != null){
			var gridGridPreAcuse = Ext.getCmp('gridGridPreAcuse');
			if (!gridGridPreAcuse.isVisible()) {
				gridGridPreAcuse.show();
			}
			var el = gridGridPreAcuse.getGridEl();
			var info = store.reader.jsonData;

			Ext.getCmp('tot_registros').setValue(info.tot_registros);
			Ext.getCmp('monto_total').setValue(info.monto_total);
			Ext.getCmp('mes_anioCarga').setValue(info.mes_anioCarga);
			Ext.getCmp('fecha').setValue(info.fecha);
			Ext.getCmp('hora').setValue(info.hora);

			if(store.getTotalCount()>0){
				if(info.fecha!='') {
					Ext.getCmp('btnImprimirPDF').show();
					Ext.getCmp('btnTransmitirReg').hide();
				}
			}else{
				Ext.getCmp('btnImprimirPDF').hide();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}else {
			Ext.getCmp('venPreAcuse').setTitle('');
			Ext.getCmp('btnTransmitirReg').disable();
		}
	}


	var consPreAcuseData = new Ext.data.JsonStore({
		root : 'registros',
		url : '/nafin/29garantias/29if/29Consulta03Ext.data.jsp',
		baseParams: {
			//informacion: 'ConsCifrasControl'
		},
		fields: [
			{ name: 'ETIQUETA'},
			{ name: 'INFORMACION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsPreAcuseData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsPreAcuseData(null, null, null);
				}
			}
		}
	});


	var gridGridPreAcuse = {
		xtype: 'grid',
		store: consPreAcuseData,
		id: 'gridGridPreAcuse',
		hidden: false,
		title: '<center>Datos cifras de control </center>',
		columns: [
			{
				header : '',
				dataIndex : 'ETIQUETA',
				width : 200,
				sortable : true
			},
			{
				header : '',
				dataIndex : 'INFORMACION',
				width : 300,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 500,
		frame: true
	}



	//**************Inicializa Valores **************************************
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var fp = Ext.getCmp('forma');
				fp.el.unmask();
				if(jsonData.strTipoUsuario =='NAFIN')  {
					Ext.getCmp('forma').show();
					Ext.getCmp('fecha_oper_ini').setValue(jsonData.fechaHoy);
					Ext.getCmp('fecha_oper_fin').setValue(jsonData.fechaHoy);
					catalogoIFData.load();
				}else  {
					Ext.getCmp('forma').hide();
					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaDataGrid.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consulta'
						})
					});
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	function bajarArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var arcName= Ext.util.JSON.decode(response.responseText).urlArchivo;
			if(arcName=='NOREG') {
				Ext.MessageBox.alert("Error",	"No se encuentra ningun registro");
			}else  {
				var archivo = infoR.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function bajarPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			//NE.util.mostrarConnError(response,opts);
			Ext.MessageBox.alert("Error",	"No se pudo generar el documento");
		}
	}

//Cancelar
var CancelarSol =  function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

				Ext.MessageBox.alert("",
				"La solicitud con folio: "+Ext.util.JSON.decode(response.responseText).folio+" fue Cancelada",function(){consultaDataGrid.load()});


		} else {
			Ext.MessageBox.alert("Error",
			"No se pudo Cancelar la Solicitud");
		}
	}


var booleanRenderer = function (value, metadata, record) {
    if (value == '')
       out = '0';
    else
      out =value;

    return out;

}
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		var grid = Ext.getCmp('grid');
		var el = grid.getGridEl();

		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}

			var jsonData = store.reader.jsonData;
			var cm = grid.getColumnModel();

			if(jsonData.strTipoUsuario =='NAFIN')  {
				grid.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('TIPO_ENVIO'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('REPROCESAR'), false);
				Ext.getCmp('btnArchivoCons').show();
			}

			if(store.getTotalCount() > 0) {

			if(jsonData.strTipoUsuario =='NAFIN')  {
				Ext.getCmp('btnArchivoCons').enable();
			}
				el.unmask();
			} else {

				Ext.getCmp('btnArchivoCons').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '/nafin/29garantias/29if/29Consulta03Ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CLAVE_IF'},
			{name: 'NOMBRE_IF'},
			{name: 'TIPO_ENVIO'},
			{name: 'FECHA'},
			{name: 'IC_FOLIO'},
			{name: 'SITUACION'},
			{name: 'IN_REGISTROS_ACEP'},
			{name: 'IN_REGISTROS_RECH'},
			{name: 'IN_REGISTROS'},
			{name: 'IC_SITUACION'},
			{name: 'MONTO_ENVIADO'},
			{name: 'REPROCESO'},
			{name: 'CANCELAR'},
			{name: 'REPROCESAR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {

			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}

	});


//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
var grid = {
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header: 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex: 'NOMBRE_IF',
				width: 150,
				resizable: true,
				sortable: true,
				align: 'left',
				hidden: true
			},
			{
				header: 'Fecha y Hora de <br>Operaci�n',
				tooltip: 'Fecha y Hora de Operaci�n',
				dataIndex: 'FECHA',
				width: 100,
				resizable: true,
				sortable: true,
				align: 'center'
			},
			{
				header: 'Folio de la <br>Operaci�n',
				tooltip: 'Folio de la Operaci�n',
				dataIndex: 'IC_FOLIO',
				sortable: true,	align: 'center',
				width: 150, resizable: true

			},{
				header: 'Situaci�n', tooltip: 'Situaci�n',
				dataIndex: 'SITUACION',
				sortable: true,	align: 'center',
				width: 200, resizable: true

			},
			{
				header: 'Usuario', tooltip: 'Usuario',
				dataIndex: 'TIPO_ENVIO',
				sortable: true,	align: 'center',
				width: 100, resizable: true,
				hidden: true
			},
			{
				header: 'N�mero de operaciones<br> aceptadas',
				tooltip: 'N�mero de operaciones aceptadas',
				dataIndex: 'IN_REGISTROS_ACEP',
				sortable: true,	align: 'center',
				width: 150, resizable: true,renderer:booleanRenderer

			},{
				header: 'N�mero de operaciones<br>con errores',
				tooltip: 'N�mero de operaciones con errores',
				dataIndex: 'IN_REGISTROS_RECH',
				sortable: true,	align: 'center',
				width: 150, resizable: true,renderer:booleanRenderer

			},{
				header: 'Total de<br> Operaciones',
				tooltip: 'Total de Operaciones',
				dataIndex: 'IN_REGISTROS',
				sortable: true,	align: 'center',
				width: 120, resizable: true

			},
			{
				xtype: 'actioncolumn',
				header: 'Archivo<br> Origen', tooltip: 'Archivo Origen',
				dataIndex: '',
				width: 60, resizable: true,
				align: 'center',
				items: [
					{
						iconCls: 'icoLupa',

						handler: function(grid, rowIdx, colIds){
							var reg = grid.getStore().getAt(rowIdx);

						Ext.Ajax.request({
							url : '/nafin/29garantias/29if/29Consulta03Ext.data.jsp',
							params: {
								informacion: 'archivo',
								folio:reg.get('IC_FOLIO'),
								cbo_if:reg.get('CLAVE_IF'),
								opcion:'origen'
							},
							callback: bajarArchivo						});
						//FIN AJAX
 						}
					}
				]
			},
			{
			 xtype: 'actioncolumn',
			 dataIndex: '',
			 width: 80,
			 header:'Resultados',tooltip:'Resultados',
			 align: 'center',
			 items: [{
				  getClass: function(v, medaData, record) {
						if(record.get('IC_SITUACION')=="5" || record.get('IC_SITUACION')=="4"||record.get('IC_SITUACION')=="3")
						return 'icoLupa';return '';
				  },
				  handler: function(grid, rowIndex, colIndex) {
								var reg = grid.getStore().getAt(rowIndex);

						Ext.Ajax.request({
							url : '/nafin/29garantias/29if/29Consulta03Ext.data.jsp',
							params: {
								informacion: 'pdf',
								folio:reg.get('IC_FOLIO'),
								origen:'CONSSALCOM',
								clave_if:reg.get('CLAVE_IF')

							},
							callback: bajarPDF
						});
						//FIN AJAX	*/
				  }
			 }]
			},
			{
			 xtype: 'actioncolumn',
			 dataIndex: '',
			 width: 80,
			 header:'Archivo de<br>Errores',tooltip:'Archivo de Errores',
			 align: 'center',
			 items: [{
				  getClass: function(v, medaData, record) {
						if((record.get('IC_SITUACION')=="5" || record.get('IC_SITUACION')=="4"||record.get('IC_SITUACION')=="3") &&
						parseInt(record.get('IN_REGISTROS_RECH'))>0)
						return 'icoLupa';return '';
				  },
				  handler: function(grid, rowIndex, colIndex) {
						var reg = grid.getStore().getAt(rowIndex);

						Ext.Ajax.request({
							url : '/nafin/29garantias/29if/29Consulta03Ext.data.jsp',
							params: {
								informacion: 'archivo',
								folio:reg.get('IC_FOLIO'),
								opcion:'error',
								clave_if:reg.get('CLAVE_IF')
							},
							callback: bajarArchivo});
						//FIN AJAX
				  }
			 }]
			}///Borrar
			,{
			 xtype: 'actioncolumn',
			 dataIndex: '',
			 width: 80,
			 header:'Cancelar',
			 tooltip:'Cancelar',
			 align: 'center',

			 items: [{
				  getClass: function(v, medaData, record) {
						if(record.get('IC_SITUACION')=="3")
						return 'cancelar';return '';
				  },
				  handler: function(grid, rowIndex, colIndex) {
						var reg = grid.getStore().getAt(rowIndex);

						Ext.MessageBox.confirm('Borrar',
						'�Est� seguro que desea Cancelar la Solicitud?',
						 function showResult(btn){
								if (btn == 'yes') {
									Ext.Ajax.request({
									url : '/nafin/29garantias/29if/29Consulta03Ext.data.jsp',
									params: {
										informacion: 'cancelar',
										folio:reg.get('IC_FOLIO')

									},
									callback: CancelarSol});
								//FIN AJAX
								  }
							});


				  }
			 }]
			},
			{
				xtype: 'actioncolumn',
				header: 'Reprocesar Errores',
				tooltip: 'Reprocesar Errores',
				dataIndex: 'REPROCESAR',
				width: 130,
				align: 'center',
				hidden: true,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('REPROCESO')!="0")  {
						return 'Reprocesado con folio'+registro.get('REPROCESO');
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('REPROCESO')=="0"  && registro.get('IN_REGISTROS_RECH')!="0")  {
								this.items[0].tooltip = 'Ver';
								return 'icoActualizar';
							}
						}
						,handler: generaPreacuse
					}
				]
			}
		],
		displayInfo: true,
		emptyMsg: "No hay registros.",
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
			'->','-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCons',
					hidden: true,
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '/nafin/29garantias/29if/29Consulta03Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'
							}),
							callback: procesarDescargaArchivos
						});

					}
				}
			]
		}
	};

	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '/nafin/29garantias/29if/29Consulta03Ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIFData'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoSituacionData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			["3",'ACTUALIZACI�N'],
			["4",'AUTORIZADA POR IF'],
			["5",'CONFIRMACI�N'],
			["1",'ENV�O PARA VALIDACI�N Y CARGA']
		 ]
	});

	var  elementosForma  = [
		{
			xtype: 'combo',
			name: 'cbo_if',
			id: 'cbo_if1',
			fieldLabel: 'Nombre del IF',
			mode: 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cbo_if',
			emptyText: 'Seleccione...',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			editable:true,
			minChars : 1,
			allowBlank: true,
			store : catalogoIFData,
			tpl:'<tpl for=".">' +
					'<tpl if="!Ext.isEmpty(loadMsg)">'+
					'<div class="loading-indicator">{loadMsg}</div>'+
					'</tpl>'+
					'<tpl if="Ext.isEmpty(loadMsg)">'+
					'<div class="x-combo-list-item">' +
					'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
					'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			//width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_oper_ini',
					id: 'fecha_oper_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					minValue: '01/01/1901',
					campoFinFecha: 'fecha_oper_fin',
					margins: '0 20 0 0'
				},
				{
					xtype: 'datefield',
					name: 'fecha_oper_fin',
					id: 'fecha_oper_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					minValue: '01/01/1901',
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_oper_ini',
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'combo',
			name: 'cbo_situacion',
			id: 'cbo_situacion1',
			fieldLabel: 'Situaci�n',
			mode: 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cbo_situacion',
			emptyText: 'Seleccione...',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoSituacionData
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'tot_registros', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'monto_total', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'mes_anioCarga', 	value: '' }	,
		{ 	xtype: 'textfield',  hidden: true, id: 'fecha', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'hora', 	value: '' }
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		monitorValid: true,
		title: 'Consulta de Resultados de las Solicitudes ',
		frame: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {


					var fecha_oper_ini = Ext.getCmp('fecha_oper_ini');
					var fecha_oper_fin = Ext.getCmp('fecha_oper_fin');

					if(!Ext.isEmpty(fecha_oper_ini.getValue()) || !Ext.isEmpty(fecha_oper_fin.getValue())){
						if(Ext.isEmpty(fecha_oper_ini.getValue())){
							fecha_oper_ini.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_oper_ini.focus();
							return;
						}
						if(Ext.isEmpty(fecha_oper_fin.getValue())){
							fecha_oper_fin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_oper_fin.focus();
							return;
						}
					}

					fp.el.mask('Enviando...', 'x-mask-loading');

					consultaDataGrid.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consulta'
						})
					});
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function(boton, evento) {
					window.location = '29Consulta03Ext.jsp';
				}
			}
		]

	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		style: 'margin:0 auto;',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid,
			NE.util.getEspaciador(20)
		]
	});



	fp.el.mask('Cargando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '/nafin/29garantias/29if/29Consulta03Ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});

});
