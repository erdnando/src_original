<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,  
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,
	java.io.*,
	java.sql.*,
	javax.naming.Context,
	netropology.utilerias.*,
	com.netro.pdf.*,
	netropology.utilerias.usuarios.*,
	com.netro.seguridadbean.*,
	com.netro.garantias.*,
	net.sf.json.JSONObject,
	java.text.*,
	net.sf.json.JSONArray"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/29garantias/29secsession_extjs.jspf" %>

<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");
String infoRegresar	= "";
if (informacion.equals("CatalogoSituacion")){
		String ic_situacion	=	"1,5, 8";
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave(" ic_situacion");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setCampoLlave("ic_tipo_operacion = 3 and ic_situacion  ");
		cat.setTabla("gticat_situacion");
		cat.setValoresCondicionIn(ic_situacion, Integer.class);
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	} else if (informacion.equals("Consulta")){
	
		String fechaInc1 	= (request.getParameter("fechaInc1")	== null)?"":request.getParameter("fechaInc1");
		String fechaInc2		= (request.getParameter("fechaInc2") 	== null)?"":request.getParameter("fechaInc2");
		String Hsituacion 	= (request.getParameter("Hsituacion")	== null)?"":request.getParameter("Hsituacion");
		String folio		= (request.getParameter("folio") 	== null)?"":request.getParameter("folio");
		
		AccesoDB con          = new AccesoDB();
		PreparedStatement	ps  = null;
		ResultSet rs          = null;
		String qrySentencia   = "";
		int iIcIfSiag         = 0;
		try {
				con.conexionDB();
				qrySentencia = "SELECT IC_IF_SIAG FROM comcat_if WHERE ic_if = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1, Integer.parseInt(iNoCliente));
				rs = ps.executeQuery();
				if(rs.next())
					iIcIfSiag = rs.getInt(1);
				rs.close();
				ps.close();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		
		com.netro.garantias.ConsSolicIFPGbean consulta = new com.netro.garantias.ConsSolicIFPGbean();
		
		consulta.setiIF_SIAG(iIcIfSiag);
		consulta.setdf_fecha_oper_de(fechaInc1);
		consulta.setdf_fecha_oper_a(fechaInc2);
		consulta.setfolioperacion(folio);
		consulta.setcombsituacion(Hsituacion);
		
		Registros reg=consulta.getSolicitudes();
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("registros", reg.getJSONData());
		jsonObj.put("iIcIfSiag", iIcIfSiag+"");
		
		infoRegresar=jsonObj.toString();
		
	}else if (informacion.equals("Imagen")){
		try {
			String folio		= (request.getParameter("folio") 	== null)?"":request.getParameter("folio");
			ImagenPagoGarantia imagenes = new ImagenPagoGarantia();
			imagenes.setFolio(folio);
			Registros registrosImagenPagoGarantias = imagenes.consultarImagenesPagoGarantia();
			JSONObject	resultado	= new JSONObject();
			boolean		success		= true;
			HashMap	datos;
			List reg=new ArrayList();
	
			while(registrosImagenPagoGarantias.next()){
						datos=new HashMap();
						for(int i=0;i<registrosImagenPagoGarantias.getNombreColumnas().size();i++){
										datos.put(registrosImagenPagoGarantias.getNombreColumnas().get(i).toString().toUpperCase(),registrosImagenPagoGarantias.getString(registrosImagenPagoGarantias.getNombreColumnas().get(i).toString()));	
							}
							
							reg.add(datos);
			}
			resultado.put("registros", JSONArray.fromObject(reg));
			resultado.put("total",reg.size()+"");
			resultado.put("success", new Boolean(success));
			infoRegresar = resultado.toString();
		} catch(NafinException ne) {
			System.out.println("PagoGarantiasIFAction::listarArchivosCargados(). NafinException: " + ne.getMessage());
			throw ne;
		} catch(Exception e) {
			System.out.println("PagoGarantiasIFAction::listarArchivosCargados(). Exception: " + e.getMessage());
			throw new NafinException("SIST0001");
		}
	}else if(informacion.equals("ConsultarImagen")){
		String claveImagen  = (request.getParameter("claveImagen")	== null)?"":request.getParameter("claveImagen");
		boolean		success		= false;
		try {
			ImagenPagoGarantia imagen = new ImagenPagoGarantia();
			imagen.setClaveImagen(claveImagen);
			String nombreArchivo = 
					imagen.consultarImagenPagoGarantia(strDirectorioTemp);
			JSONObject	resultado	= new JSONObject();
			success		= true;
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			resultado.put("success", 					new Boolean(success)		);
			infoRegresar = resultado.toString();
			
		} catch(NafinException ne) {
			System.out.println("PagoGarantiasIFAction::consultarArchivo(). NafinException: " + ne.getMessage());
			throw ne;
		} catch(Exception e) {
			System.out.println("PagoGarantiasIFAction::consultarArchivo(). Exception: " + e.getMessage());
			throw new NafinException("SIST0001");
		}
		
	}
%>

<%=infoRegresar%>