<%@ page language="java" %>
<%@ page import="
	java.util.*,
	com.netro.afiliacion.*,
	com.netro.descuento.*,
	com.netro.garantias.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<% 

/*** OBJETOS ***/
CQueryHelperRegExtJS queryHelper;
CatalogoSimple cat;
JSONObject jsonObj;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String resultado			= null;
String operacion 			= (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
String claveTransaccion = (request.getParameter("claveTransaccion")==null)?"":request.getParameter("claveTransaccion");
String claveEstatus 		= (request.getParameter("claveEstatus")==null)?"":request.getParameter("claveEstatus");
String ic_if 				= iNoCliente;
int start = 0;
int limit = 0;
/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/

/*** INICIO CONSULTA ***/
if(informacion.equals("Consulta")) { 	
	CorreosNotificaciones paginador = new CorreosNotificaciones();
	
	paginador.setClaveIf(ic_if);
	paginador.setClaveTransaccion(claveTransaccion);
	paginador.setClaveEstatus(claveEstatus);
	queryHelper	= new CQueryHelperRegExtJS( paginador );
	if((informacion.equals("Consulta") && operacion.equals("Generar")) || (informacion.equals("Consulta") && operacion.equals(""))){ 	 
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));				
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if(operacion.equals("Generar")){
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			String cadena = queryHelper.getJSONPageResultSet(request,start,limit);
			resultado = cadena;
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if(operacion.equals("XLS")){
		jsonObj = new JSONObject();
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,operacion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
	}else if(operacion.equals("PDF")){
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));				
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
			jsonObj = new JSONObject();
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,limit, strDirectorioTemp,operacion);
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			resultado = jsonObj.toString();
	}
} /*** FIN DE CONSULTA ***/

/*** INICIO DE CONSULTA CLAVE TRANSACCION ***/
else if(informacion.equals("claveTransaccion")){
	
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_tipo_operacion");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("gticat_tipooperacion");
		catalogo.setOrden("cg_descripcion");
		
		resultado = catalogo.getJSONElementos();
}
/*** FIN DE CONSULTA CLAVE TRANSACCION ***/

/*** INICIO DE CONSULTA CLAVE ESTATUS ***/
else if(informacion.equals("claveEstatus")){
	List catalogoEstatusCarga = new ArrayList();
	
	if (claveTransaccion.equals("2") || claveTransaccion.equals("")) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave("1");
			elementoCatalogo.setDescripcion("RECHAZADA");
			catalogoEstatusCarga.add(elementoCatalogo);
			
			elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave("2");
			elementoCatalogo.setDescripcion("AUTORIZADA IF");
			catalogoEstatusCarga.add(elementoCatalogo);
			
			elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave("3");
			elementoCatalogo.setDescripcion("AMBOS");
			catalogoEstatusCarga.add(elementoCatalogo);
		} else {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave("1");
			elementoCatalogo.setDescripcion("RECHAZADA");
			catalogoEstatusCarga.add(elementoCatalogo);
		}
		
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", catalogoEstatusCarga);
		resultado=jsonObj.toString();
}
%>
<%= resultado%>
