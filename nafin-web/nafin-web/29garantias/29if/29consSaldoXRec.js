Ext.onReady(function() {

//----------------------------- Handler's ----------------------------//

	var procesarConsultaRegistros = function(store, registros, opts){
		var jsonData = store.reader.jsonData;
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		if (registros != null) {
			var grid  = Ext.getCmp('grid');
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}
	}
	
	function procesarGenerarArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//----------------------------- Maquina de Edos. ----------------------------//
		var accionConsulta = function(estadoSiguiente, respuesta){

			if(  estadoSiguiente == "CONSULTAR" ){
				var forma = Ext.getCmp("forma");
				forma.el.mask('Buscando...','x-mask-loading');
	
				// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
				Ext.getCmp('grid').hide(); 
				Ext.StoreMgr.key('registrosConsultadosDataStore').load({
										params:	Ext.apply(fp.getForm().getValues(),
												{
													informacion: 'ConsultarDatos',
													tipo_conc:2
												}
											)
				});
				
			}else if(	estadoSiguiente == "LIMPIAR"){
				Ext.getCmp('forma').getForm().reset();
				Ext.getCmp('grid').hide();	
			}
			
		}
	
//----------------------------- Store's ----------------------------//
		
	//Catalogo para el combo de seleccion de Situaci�n
	var catalogoSituacionData = new Ext.data.JsonStore({
		id:				'catalogoSituacionDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'29consResultados.data.jsp',
		baseParams:		{	informacion: 'CatSituacion'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
		var consultaDataGrid = new Ext.data.JsonStore({
				root : 'registros',
				id:	'registrosConsultadosDataStore',
				url : '29consResultados.data.jsp',
				baseParams: {
					informacion: 'Consulta',
               flagConsulta:'CONSULTAR'
				},
				fields: [
					{name: 'CVE_MES'},
					{name: 'CVE_FISO'},
					{name: 'CVE_IF_SIAG'},
					{name: 'TIPO_CONC'},
					{name: 'FEC_CARGA'},
					{name: 'SITUACION'},
					{name: 'MES_CONC', type:'date', convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'Ym')); } },
					{name: 'CONCILIADOS'},
               {name: 'NO_CONC'},
					{name: 'MONTO_ENVIADO'},
					{name: 'TOT_OPE'},
               {name: 'ORIGEN'},
					{name: 'RESULTADOS'},
					{name: 'DETALLE'}
				],
				totalProperty : 'total',
				messageProperty: 'msg',
				autoLoad: false,
				listeners: {
					load: procesarConsultaRegistros,
					exception: {
						fn: function(proxy, type, action, optionsRequest, response, args) {
							NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						}
					}
				}
		});
		
//----------------------------- Componentes ----------------------------//
		
		//Grid de consulta de datos
		var grid = {
			store: consultaDataGrid,
			xtype:'grid',
			id: 'grid',
			hidden: true,
			columns: [
				{
					header: 'Fecha y Hora de Carga', 
					tooltip: 'Fecha y Hora de Carga',
					dataIndex: 'FEC_CARGA',
					sortable: true,
					width: 135, 
					resizable: true,
					align: 'center'
				},{
					header: 'Situaci�n', 
					tooltip: 'Situaci�n',
					dataIndex: 'SITUACION',
					sortable: true,	
					align: 'center',
					width: 192,
					resizable: true,
					align: 'center'
				},{
					header: 'Mes Conciliado',
					tooltip: 'Mes Conciliado',
					dataIndex: 'MES_CONC',
					sortable: true,	
					align: 'center',
					width: 120, 
					resizable: true,
					align: 'center',
					xtype: 		'datecolumn', 
					format: 		'F Y'
				},{
					header: 'Conciliados', 
					tooltip: 'Conciliados',
					dataIndex: 'CONCILIADOS',
					sortable: true,	
					align: 'center',
					width: 100, 
					resizable: true,
					align: 'center'
				},{
					header: 'No Conciliados', 
					tooltip: 'No Conciliados',
					dataIndex: 'NO_CONC',
					sortable: true,	
					align: 'center',
					width: 110, 
					resizable: true,
					align: 'center'
				},{
					header: 'Monto Enviado', 
					tooltip: 'Monto Enviado',
					dataIndex: 'MONTO_ENVIADO',
					sortable: true,	
					align: 'right',
					width: 110, 
					resizable: true,
					align: 'right',
               renderer: Ext.util.Format.usMoney
				},{
					header: 'Total de Operaciones',
					tooltip: 'Total de Operaciones',
					dataIndex: 'TOT_OPE',
					sortable: true,	
					align: 'center',
					width: 140, 
					resizable: true,
					align: 'center'
				},{
               xtype: 'actioncolumn',
					header: 'Archivo Origen', tooltip: 'Archivo Origen',
					dataIndex: 'ORIGEN',
					sortable: true,	align: 'center',
					width: 90, resizable: true,
					align: 'center',
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
								var icono = registro.get('ORIGEN')=='S'?'icoZip':'';
								return icono;
							},
							tooltip: 'Buscar Expediente',
							handler: function(grid, rowIdx, colIds){
                        var reg = grid.getStore().getAt(rowIdx);                                          
                        Ext.Ajax.request({
                           url:     '29consResultados.data.jsp',
                           params:  {
										informacion: 'ArchivoTexto', 
										tipo_arch: 'origen', 
										cveMes: reg.get('CVE_MES'),
										cveFiso: reg.get('CVE_FISO'),
										cveIfSiag: reg.get('CVE_IF_SIAG'),
										tipoConc: reg.get('TIPO_CONC')
									},
									callback: procesarGenerarArchivo
                        });									
                     }				
                  }
               ]
				},{
               xtype:   'actioncolumn',
					header: 'Resultados', 
					tooltip: 'Resultados',
					dataIndex: 'RESULTADOS',
					sortable: true,	align: 'center',
					width: 80, resizable: true,
					align: 'center',
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {		
								var icono = registro.get('RESULTADOS')=='S'?'icoPdf':'';
								return icono;
							},
							tooltip: 'Buscar Expediente',
							handler: function(grid, rowIdx, colIds){
                        var reg = grid.getStore().getAt(rowIdx);                                          
                        Ext.Ajax.request({
                           url:     '29consResultados.data.jsp',
                           params:  {
										informacion: 'ArchivoPDF', 
										cveMes: reg.get('CVE_MES'),
										cveFiso: reg.get('CVE_FISO'),
										cveIfSiag: reg.get('CVE_IF_SIAG'),
										tipoConc: reg.get('TIPO_CONC')
									},
									callback: procesarGenerarArchivo
                        });  								
                     }				
                  }
               ]
				},{
               xtype:   'actioncolumn',
					header: 'Archivo de Diferencias', 
					tooltip: 'Archivo de Diferencias',
					dataIndex: 'DETALLE',
					sortable: true,	align: 'center',
					width: 125, resizable: true,
					align: 'center',
               items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {		
								var icono = registro.get('DETALLE')=='S'?'icoZip':'';
								return icono;
							},
							tooltip: 'Buscar Expediente',
							handler: function(grid, rowIdx, colIds){
                        var reg = grid.getStore().getAt(rowIdx);                      
                        Ext.Ajax.request({
                           url:     '29consResultados.data.jsp',
                           params:  {
										informacion: 'ArchivoTexto', 
										tipo_arch: 'detalle', 
										cveMes: reg.get('CVE_MES'),
										cveFiso: reg.get('CVE_FISO'),
										cveIfSiag: reg.get('CVE_IF_SIAG'),
										tipoConc: reg.get('TIPO_CONC')
									},
                           callback: procesarGenerarArchivo
                        });  									
                     }	
                  }
               ]
            }],
			stripeRows: true,
			loadMask: true,
			deferRowRender: false,
			height: 333,
			width: 920,
			colunmWidth: true,
			frame: true,
			style: 'margin:0 auto;',// para centrar el grid
			collapsible: false
		};
		
		var elementosForma = [
			  {
				xtype:			'compositefield',
				fieldLabel: 	'Fecha de Carga',
            combineErrors: false,
				items:[{
						xtype:	'datefield',
						id:		'id_fecha_car_ini',
						name:		'fecha_car_ini',
						startDay: 0,
                  msgTarget: 'side',
						margins: '0 20 0 0'
				},{
					xtype:		'displayfield',
					value:		'a',
					width:		20
				},{
					xtype:		'datefield',
					name:			'fecha_car_fin',
					id:			'id_fecha_car_fin',
					startDay: 1,
               msgTarget: 'side',
					margins: '0 20 0 0'
				}]
		},{
			xtype:				'combo',
			name:					'cbo_situacion',
			hiddenName:			'cbo_situacion',
			id:					'id_cbo_situacion',
			fieldLabel:			'Situaci�n',
			emptyText:			'- Seleccione -',
			mode:					'local',
			displayField:		'descripcion',
			valueField:			'clave',
			forceSelection:	false,
			triggerAction:		'all',
			allowBlank:			true,
			minChars:			1,
			store:				catalogoSituacionData,
         anchor:           '93%'
		}
		];
		
		var fp = new Ext.form.FormPanel({
			xtype:			'form',
			id:				'forma',
			width:			400,
			title:			'Saldos por Recuperar (SPPR) - Consulta de Resultados',
			style:			'text-align:left;margin:0 auto;',
			hidden:			false,
			frame:			true,
			bodyStyle: 		'padding: 6px',
			labelWidth: 	110,
			defaults: {
				msgTarget:	'side',
				anchor: 		'-20'
			},
			items: 			elementosForma,
			monitorValid: 	true,
			buttons:[{
					text: 	'Consultar',
					id: 		'btnConsultar',
					iconCls: 'icoBuscar',
					handler: function(boton, evento) {
									var fechaMin = Ext.getCmp('id_fecha_car_ini');
									var fechaMax = Ext.getCmp('id_fecha_car_fin');
									if (!Ext.isEmpty(fechaMin.getValue()) || !Ext.isEmpty(fechaMax.getValue()) ) {
										if(Ext.isEmpty(fechaMin.getValue()))	{
											fechaMin.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
											fechaMin.focus();
											return;
										}else if (Ext.isEmpty(fechaMax.getValue())){
											fechaMax.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
											fechaMax.focus();
											return;
										}
									}
									if(fechaMax.getValue() < fechaMin.getValue()){
										fechaMax.markInvalid('La fecha final debe ser mayor o igual a la inicial');
										return;
									}
									if(fechaMin.getValue()!=""){
										var fechaDe = Ext.util.Format.date(fechaMin.getValue(),'d/m/Y');
										var fechaA = Ext.util.Format.date(fechaMax.getValue(),'d/m/Y');
										if(!isdate(fechaDe)){
											fechaMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
											return;
										}else if(!isdate(fechaA)){
											fechaMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
											return;
										}
									}
									accionConsulta("CONSULTAR", null);
									Ext.getCmp('grid').show();
						} //fin handler
			},{
            text     :  'limpiar',
            iconCls  :  'icoLimpiar',
            handler  :  function(){
						accionConsulta("LIMPIAR", null);
            }  
         }]
		})
	

	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		940,
		height: 		'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
         grid
		]
	});
	
	Ext.StoreMgr.key('catalogoSituacionDataStore').load();
	
});
