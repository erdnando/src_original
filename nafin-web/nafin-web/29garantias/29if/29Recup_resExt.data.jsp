<%@ page contentType="application/json;charset=UTF-8" errorPage = "/00utils/error_extjs.jsp"
import = "java.text.*,java.util.*, com.netro.model.catalogos.*,net.sf.json.*,
netropology.utilerias.*,
	com.netro.garantias.*"%>
<%@ include file="/appComun.jspf" %>

<%@ include file="/29garantias/29secsessionWMenu.jspf" %>
<jsp:useBean id="consulta" scope="page" class="com.netro.garantias.ConsSolicIFGAbean" />
<%

String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String resultado="";
/*
//Petici�n de consulta Principal
	if(informacion.equals("Consulta"))
	{
		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		String fechaHoy = formatoFecha.format(calendario.getTime());		
		
		String fecha_oper_ini = request.getParameter("df_consultaMin") == null?fechaHoy:(String)request.getParameter("df_consultaMin");
		String fecha_oper_fin = request.getParameter("df_consultaMax") == null?fechaHoy:(String)request.getParameter("df_consultaMax");
		String cbo_situacion = request.getParameter("situacion") == null?"":(String)request.getParameter("situacion");
		
		AccesoDB 			con				= new AccesoDB();
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		String				qrySentencia	= "";
		
		int 				vigencia		= 0;
		int 				i 				= 0;
		
		try {
			con.conexionDB();
			 List params = new ArrayList();
			 consulta.setiTipoOperacion(4);
			consulta.setSituacion(cbo_situacion);
			consulta.setiIF(Integer.parseInt(iNoCliente));
			 
		params.add(new Integer(4));
		params.add(iNoCliente);
		if(!fecha_oper_ini.equals("") && !fecha_oper_fin.equals("")){
		consulta.setFechaOperIni(fecha_oper_ini);
			 consulta.setFechaOperFin(fecha_oper_fin);
			 params.add(fecha_oper_ini);
			 params.add(fecha_oper_fin);
		
		 }
		 if(!cbo_situacion.equals("")){
			params.add(cbo_situacion);
		}
 // if(fecha_oper_ini.equals("") && fecha_oper_fin.equals("") && !cbo_situacion.equals("")){ps.setInt(3, Integer.parseInt(cbo_situacion));}
 // if(!fecha_oper_ini.equals("") && !fecha_oper_fin.equals("") && !cbo_situacion.equals("")){ps.setInt(5, Integer.parseInt(cbo_situacion));}
  
  
			qrySentencia = consulta.getQuerySolicitudes();
		ps = con.queryPrecompilado(qrySentencia);
		
		
		
			Registros reg=con.consultarDB(qrySentencia, params);
			resultado=
			"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		
					} catch (Exception e) {
					throw new AppException("...:::Error al consultar base de NE");
				} finally {
					if (con.hayConexionAbierta()) {
						con.cierraConexionDB();
					}
			}
	}
*/	
	if(informacion.equals("Consulta")) {
		
		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		String fechaHoy = formatoFecha.format(calendario.getTime());		
		
		String fecha_oper_ini = request.getParameter("df_consultaMin") == null?fechaHoy:(String)request.getParameter("df_consultaMin");
		String fecha_oper_fin = request.getParameter("df_consultaMax") == null?fechaHoy:(String)request.getParameter("df_consultaMax");
		String cbo_situacion  = request.getParameter("situacion") == null?"":(String)request.getParameter("situacion");
		ConsSolicIFGAbeanExt paginador = new ConsSolicIFGAbeanExt();
		paginador.setiTipoOperacion("4");
		paginador.setiIF(iNoCliente);
		paginador.setFechaOperIni(fecha_oper_ini);
		paginador.setFechaOperFin(fecha_oper_fin);
		paginador.setSituacion(cbo_situacion);
	
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		Registros reg = queryHelper.doSearch();
		if(reg != null ) {
			resultado = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
		}
		else {
			resultado = "{\"success\": false }";
		}
	   
	}
	else if(informacion.equals("Situacion"))
	{
		CatalogoSituacion catsit = new CatalogoSituacion();
		catsit.setCampoClave("ic_situacion");
		catsit.setCampoDescripcion("cg_descripcion");
		catsit.setClaveTipoOperacion("4"); //Recuperaciones autom�ticas
		List clavesSit = new ArrayList();
		clavesSit.add("1");	//Envio para validacion y carga
		clavesSit.add("4");	//Registro incompleto
		clavesSit.add("5");	//Confirmaci�n
		catsit.setValoresCondicionIn(clavesSit);
		catsit.setOrden("ic_situacion");
		List elementos = catsit.getListaElementos();
		Iterator it = elementos.iterator();
		JSONArray jsonArr = new JSONArray();
		
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		resultado= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
		
	}
	
	//Creacion de PDF
	else if(informacion.equals("pdf"))
	{
	/*consulta.getPathFilePDF(String clave_if,String folio,String origen,String cvePerf,
	String strDirectorioTemp,HttpSession session,String strDirectorioPublicacion,String iNoCliente);*/
	System.out.println("...::Generar archivo PDF::..");
	
	String clave_if = request.getParameter("clave_if");
	String folio = request.getParameter("folio");
	String origen = request.getParameter("origen");
	String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);

	String nomFile=consulta.getPathFilePDF(clave_if,folio,origen,cvePerf,
	strDirectorioTemp,session,strDirectorioPublicacion,iNoCliente);
			if(nomFile.equals("error"))
			{
				resultado=
				"{\"success\": false, \"urlArchivo\": \"error"	+	 "\""+ "}";
			}
			else
			{
				resultado=
				"{\"success\": true, \"urlArchivo\": \""	+	strDirecVirtualTemp+nomFile+ "\""+ "}";	
			}
	}else if(informacion.equals("archivo"))
	{
		
		String folio = request.getParameter("folio");
		String opcion = request.getParameter("opcion");
		String clave_if = request.getParameter("clave_if")!=null?request.getParameter("clave_if"):"";
		
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet rs = null;
		String qrySentencia	= "";
		String tabla = "";
		int vigencia = 0;
		int i = 0;
		
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo = null;
		
		try {
			con.conexionDB();
			if(opcion.equals("origen")){
			 tabla = "gti_contenido_arch";
		  }else{
			 tabla = "gti_arch_rechazo";
		  }
		  
		  if(!"".equals(clave_if))
			iNoCliente = clave_if;
		  
			qrySentencia =
				"select cg_contenido"+
				" from "+tabla+" A"+
				" ,comcat_if I"+
				" where A.ic_if_siag = I.ic_if_siag"+
				" and A.ic_folio = ?"+
				" and I.ic_if = ?"+
				" order by A.ic_linea";
			
			ps = con.queryPrecompilado(qrySentencia);
		  ps.setLong(1, Long.parseLong(folio));
			ps.setLong(2, Long.parseLong(iNoCliente));
			
			rs = ps.executeQuery();
			while(rs.next()){
				contenidoArchivo.append(rs.getString("cg_contenido")+"\n");
				i++;
			}
			ps.close();
			
			if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt"))
			{
				
				resultado="{\"success\": false, \"urlArchivo\": \"error"	+ "\""+ "}";	
				///out.print("<--!Error al generar el archivo-->");
				}
			else
			{
				if(i>0)
				{
					nombreArchivo = archivo.nombre;
					resultado="{\"success\": true, \"urlArchivo\": \""	+	strDirecVirtualTemp+nombreArchivo+ "\""+ "}";			
				}
				else
				{
					resultado="{\"success\": true, \"urlArchivo\": \"NOREG"	+ "\""+ "}";			
				}
			}
				
		
			}catch(Exception e){
			e.printStackTrace();
			
			}finally{
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
			}
	}
%>
<%=resultado%>
