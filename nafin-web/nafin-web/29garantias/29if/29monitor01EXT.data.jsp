<%@ page 
	contentType=
		"application/json;charset=UTF-8" 
	import="
		net.sf.json.JSONArray,
		netropology.utilerias.*,
		net.sf.json.JSONObject,
		com.netro.seguridadbean.*,
		java.util.*,
		java.math.BigDecimal,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.garantias.ConsSolicIFGAbeanExt,
		com.netro.garantias.MonitorResultadosExt01,
		javax.rmi.PortableRemoteObject " 
	errorPage=
		"/00utils/error_extjs.jsp"
%> 
<%@ include file="../29secsession.jspf" %>
<%    
String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String infoRegresar	= "";
String consulta   	= "";

if (informacion.equals("cargaFecha") )	{
  
  JSONObject		 jsonObj  = new JSONObject();
  Calendar calendario = Calendar.getInstance();  
  SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
  String fechaHoy = formatoFecha.format(calendario.getTime());
  
  jsonObj.put("success"	,  new Boolean(true)); 
  jsonObj.put("fechaHoy", fechaHoy);
  infoRegresar = jsonObj.toString();

} else if (informacion.equals("catalogoif") )	{

  JSONObject		 jsonObj  = new JSONObject();
  CatalogoSimple catInt      = new CatalogoSimple();
  
  catInt.setCampoClave("ic_if");
  catInt.setCampoDescripcion("cg_razon_social");
  catInt.setTabla("comcat_if");
  List catInter = catInt.getListaElementos();    
	jsonObj.put("registros", catInter);
  infoRegresar = jsonObj.toString();

}else if (informacion.equals("consultaGeneral")){
  
  JSONObject  jsonObj  = new JSONObject();
  String      noIf		 = (request.getParameter("_cmb_if") 		!=null)	?	request.getParameter("_cmb_if")		:	"";
  String fechaOperIni	 = (request.getParameter("_fechaReg")		!=null)	?	request.getParameter("_fechaReg")	:	"";
  String fechaOperFin  = (request.getParameter("_fechaFinReg") 	!=null)	?	request.getParameter("_fechaFinReg"):	"";
  String situacion     = (request.getParameter("_cmbtd") 			!=null)	?	request.getParameter("_cmbtd")		:	"";
  
  MonitorResultadosExt01 paginador = new MonitorResultadosExt01();
  paginador.setClaveIF(noIf);
  paginador.setFechaIni(fechaOperIni);
  paginador.setFechaFin(fechaOperFin);
  paginador.setOpcion(situacion);
  paginador.setFolio("");

  CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
  Registros reg = queryHelper.doSearch();
  BigDecimal newc = new BigDecimal("0");
  BigDecimal suma = new BigDecimal("0");
  BigDecimal res  = new BigDecimal("0");
  String folio;
  /*while(reg.next()){
    folio = (reg.getString("IC_FOLIO")==null)?"":(reg.getString("IC_FOLIO"));
    newc= new BigDecimal(folio);
		res=suma.add(newc);
    reg.setObject("IC_FOLIO","\"\"\""+res+"\"\"\"");
  }*/
  consulta = 	"{\"success\": true, \"total\": \""+ reg.getNumeroRegistros() +"\", \"registros\": " + reg.getJSONData()+"}";
  jsonObj.put("success"	,  new Boolean(true)); 
  jsonObj = JSONObject.fromObject(consulta);
  infoRegresar = jsonObj.toString();

} else if(informacion.equals("TXT")){

  JSONObject  jsonObj  = new JSONObject();
  MonitorResultadosExt01 monitor = new MonitorResultadosExt01();
  String      claveIf		 = (request.getParameter("claveIf") !=null)	?	request.getParameter("claveIf")	:	"";
  String      folio		   = (request.getParameter("folio") 	!=null)	?	request.getParameter("folio")		:	"";
  String      opcion		 = (request.getParameter("opcion") 	!=null)	?	request.getParameter("opcion")	:	"";
  
  monitor.setClaveIF(claveIf);  
  monitor.setFolio(folio);
  monitor.setOpcion(opcion);
  monitor.setStrDirectorioTemp(strDirectorioTemp);
  
  String nombreArchivo = monitor.crearArchivo();
  if (!nombreArchivo.equals("0")){
    jsonObj.put("urlArchivo", "/nafin/00tmp/29garantias/"+nombreArchivo);
  } else {
    jsonObj.put("urlArchivo", "0");
  }
  
	jsonObj.put("success", new Boolean(true));
  System.err.println ("urlArchivo: "+"/nafin/00tmp/29garantias/"+nombreArchivo);  
  infoRegresar = jsonObj.toString();

} else if(informacion.equals("PDF")){

  JSONObject  jsonObj  = new JSONObject();
  com.netro.seguridadbean.Seguridad BeanSegFacultad = null;
  MonitorResultadosExt01 monitor = new MonitorResultadosExt01();
  String      claveIf		 = (request.getParameter("claveIf") !=null)	?	request.getParameter("claveIf")	:	"";
  String      folio		   = (request.getParameter("folio") 	!=null)	?	request.getParameter("folio")		:	"";
  String      origen		 = "CONSSALCOM";
  String nombreArchivo="";
  int  start= 0, limit =0;
  
  try {
    BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
  } catch (Exception e) {
    System.out.println("EXISTEN DIFICULTADES TECNICAS, INTENTE MÁS TARDE"+e);	
  }
  String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);
  
  monitor.setClaveIF(claveIf);  
  monitor.setFolio(folio);
  monitor.setOrigen(origen);
  monitor.setCvePerf(cvePerf);  

  nombreArchivo= monitor.miPDF(request,strDirectorioTemp,"PDF");
	jsonObj.put("success", new Boolean(true));
  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
  infoRegresar = jsonObj.toString();

} else if(informacion.equals("ArchivoCSV") )  {
  JSONObject  jsonObj  = new JSONObject();
  String      noIf		 = (request.getParameter("_cmb_if") 		!=null)	?	request.getParameter("_cmb_if")		:	"";
  String fechaOperIni	 = (request.getParameter("_fechaReg")		!=null)	?	request.getParameter("_fechaReg")	:	"";
  String fechaOperFin  = (request.getParameter("_fechaFinReg") 	!=null)	?	request.getParameter("_fechaFinReg"):	"";
  String situacion     = (request.getParameter("_cmbtd") 			!=null)	?	request.getParameter("_cmbtd")		:	"";
  MonitorResultadosExt01 paginador = new MonitorResultadosExt01();
  paginador.setClaveIF(noIf);
  paginador.setFechaIni(fechaOperIni);
  paginador.setFechaFin(fechaOperFin);
  paginador.setOpcion(situacion);
  paginador.setFolio("");
  CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

	try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
  infoRegresar = jsonObj.toString();
}

%>
<%=  infoRegresar %>