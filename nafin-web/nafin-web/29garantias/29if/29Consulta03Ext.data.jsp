<%@ page 
contentType="application/json;charset=UTF-8"
import="java.util.ArrayList,
java.util.*,
netropology.utilerias.*,
java.sql.*,
java.math.*,
com.netro.exception.*,
java.text.SimpleDateFormat,
netropology.utilerias.*,
com.netro.model.catalogos.*,
com.netro.garantias.*, 
net.sf.json.JSONArray,	
net.sf.json.JSONObject"
errorPage=	"/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsessionWMenu.jspf" %>
<jsp:useBean id="consulta" scope="page" class="com.netro.garantias.ConsSolicIFGAbean" />
<%

String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String resultado="";
JSONObject jsonObj = new JSONObject();
String fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());	
String hidAction		= (request.getParameter("hidAction")==null)?"":request.getParameter("hidAction");
String txt_folio		= (request.getParameter("folio")==null)?"":request.getParameter("folio");
String cbo_if		= (request.getParameter("cbo_if")==null)?"":request.getParameter("cbo_if");
String fecha_oper_ini		= (request.getParameter("fecha_oper_ini")==null)?"":request.getParameter("fecha_oper_ini");
String fecha_oper_fin		= (request.getParameter("fecha_oper_fin")==null)?"":request.getParameter("fecha_oper_fin");
String cbo_situacion		= (request.getParameter("cbo_situacion")==null)?"":request.getParameter("cbo_situacion");

if(strTipoUsuario.equals("IF"))  {
	cbo_if= iNoCliente;
}
AccesoDB 			con				= new AccesoDB();
PreparedStatement	ps				= null;
ResultSet			rs				= null;
String				qrySentencia	= "";		
int 				vigencia		= 0;
int 				i 				= 0;

ConsSolicIFGAbeanExt paginador = new ConsSolicIFGAbeanExt ();

CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
 
paginador.setiTipoOperacion("2"); 
paginador.setiIF(cbo_if);
paginador.setFechaOperIni(fecha_oper_ini);
paginador.setFechaOperFin(fecha_oper_fin);
paginador.setSituacion(cbo_situacion);


String meses[]={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String diaActual    = fechaActual.substring(0,2);
String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
String anioCarga   = fechaActual.substring(6,10);
String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

String ic_folio = (request.getParameter("ic_folio") != null)?request.getParameter("ic_folio"):"";
String ic_if = (request.getParameter("ic_if") != null)?request.getParameter("ic_if"):"";

JSONArray registros = new JSONArray();
HashMap mapa=new HashMap();

if(informacion.equals("valoresIniciales")){
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("strTipoUsuario", strTipoUsuario);	
	jsonObj.put("fechaHoy", fechaHoy);
	resultado = jsonObj.toString();
		
}else if(informacion.equals("catalogoIFData")){
	if(strTipoUsuario.equals("IF"))  {
		CatalogoIF cat = new CatalogoIF();
		cat.setClave("ic_if");
		cat.setDescripcion("cg_razon_social");
		cat.setOrden("cg_razon_social");
		resultado = cat.getJSONElementos();	
		
	}else if(strTipoUsuario.equals("NAFIN"))  {
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setTabla("comcat_if");		
		cat.setOrden("cg_razon_social");
		resultado = cat.getJSONElementos();	
	}	
	
	
}else  if(informacion.equals("Consulta")){

	Registros reg	=	queryHelper.doSearch();
	String  consreg	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consreg);
	jsonObj.put("strTipoUsuario", strTipoUsuario); 
	resultado = jsonObj.toString();  


}else if(informacion.equals("cancelar")) {
	String mensaje="";
	try {
		Garantias bean = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
		bean.setSolicitudCancelada(txt_folio);
		resultado=	"{\"success\": true,"+"\"folio\":"+txt_folio+"}";
	} catch (Exception e){
		e.printStackTrace();
		resultado=	"{\"success\": false"+"}";
	}
	//Creacion de PDF

} else if(informacion.equals("pdf")){
	
	System.out.println("...::Generar archivo PDF::..");
	
	String clave_if = request.getParameter("clave_if");
	String folio = request.getParameter("folio");
	String origen = request.getParameter("origen");
	String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);

	String nomFile=consulta.getPathFilePDF(clave_if,folio,origen,cvePerf,
	strDirectorioTemp,session,strDirectorioPublicacion,iNoCliente);
	if(nomFile.equals("error"))	{
		resultado="{\"success\": false, \"urlArchivo\": \"error"	+	 "\""+ "}";
	}else	{
		resultado=	"{\"success\": true, \"urlArchivo\": \""	+	strDirecVirtualTemp+nomFile+ "\""+ "}";	
	}			
			
} else if(informacion.equals("ArchivoCSV"))	{
	
	try {
		String  nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}
	resultado = jsonObj.toString();  

}else if(informacion.equals("archivo")){

	String folio = request.getParameter("folio");
	String opcion = request.getParameter("opcion");
	
	String tabla = "";		
		
	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer("");
	String nombreArchivo = null;
		
	try {
		con.conexionDB();
		if(opcion.equals("origen")){
		 tabla = "gti_contenido_arch";
		}else{
			tabla = "gti_arch_rechazo";
		}
		  
		qrySentencia =
			"select cg_contenido"+
			" from "+tabla+" A"+
			" ,comcat_if I"+
			" where A.ic_if_siag = I.ic_if_siag"+
			" and A.ic_folio = ?"+
			" and I.ic_if = ?"+
			" order by A.ic_linea";
			
		ps = con.queryPrecompilado(qrySentencia);
		ps.setLong(1, Long.parseLong(folio));
		ps.setString(2, cbo_if);
			
		rs = ps.executeQuery();
		while(rs.next()){
			contenidoArchivo.append(rs.getString("cg_contenido")+"\n");
			i++;
		}
		ps.close();
		
		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt"))		{				
			resultado="{\"success\": false, \"urlArchivo\": \"error"	+ "\""+ "}";					
		}	else	{
			if(i>0){
				nombreArchivo = archivo.nombre;
				resultado="{\"success\": true, \"urlArchivo\": \""	+	strDirecVirtualTemp+nombreArchivo+ "\""+ "}";			
			}	else	{
				resultado="{\"success\": true, \"urlArchivo\": \"NOREG"	+ "\""+ "}";			
			}
		}
						
	}catch(Exception e){
		e.printStackTrace();			
	}finally{
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}
	
}else if(informacion.equals("ConsCifrasControl")){	

	HashMap reg = paginador.getdatosPreAcuse(ic_folio); // consulta
	
	mapa=new HashMap();
	mapa.put("ETIQUETA","Tipo de Operación");
	mapa.put("INFORMACION","02 Carga de Saldos y Pago de Comisiones");	
	registros.add(mapa);
	
	mapa=new HashMap();
	mapa.put("ETIQUETA","Mes de carga");
	mapa.put("INFORMACION",mesActual +" de " +anioCarga);	
	registros.add(mapa);
	
	
	mapa=new HashMap();
	mapa.put("ETIQUETA","No. total de registros transmitidos");
	mapa.put("INFORMACION",reg.get("NUM_REG"));
	registros.add(mapa);
	
	mapa=new HashMap();
	mapa.put("ETIQUETA","Monto total saldos al fin del mes");
	mapa.put("INFORMACION","$"+Comunes.formatoDecimal(reg.get("MONTO_TOTAL"),2) );	
	registros.add(mapa);
	
	
	String consreg =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consreg); 
	jsonObj.put("monto_total", reg.get("MONTO_TOTAL")); 
	jsonObj.put("mes_anioCarga", mesActual +" de " +anioCarga); 
	jsonObj.put("tot_registros", reg.get("NUM_REG")); 
	jsonObj.put("fecha", ""); 
	jsonObj.put("hora", ""); 	 
	resultado = jsonObj.toString();

}else if(informacion.equals("Transmitir_Registros")){%>
	<%@ include file="/29garantias/29pki/certificado.jspf" %>
	
<%
	String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
	String externContent = (request.getParameter("textoFirmar")==null)?"":request.getParameter("textoFirmar");
	String tot_registros = (request.getParameter("tot_registros")==null)?"":request.getParameter("tot_registros");
	
	char getReceipt = 'Y';
	String folioCert  ="", _acuse  ="";
	Acuse  acuse = new Acuse(Acuse.ACUSE_EPO, "1");
	ResultadosGar rg = 	null;
		
	Garantias bean = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

	String  monto_total="", mes_anioCarga="", fecha="", hora= "", nombreUsuario="";				
	
	System.out.println("_serial" +_serial);
	System.out.println("externContent  "+externContent);
	System.out.println("pkcs7   "+pkcs7);
	System.out.println("ic_folio "+ic_folio);
	System.out.println("ic_if "+ic_if);
	
	//if (!_serial.equals("")	&& !externContent.equals("") && !pkcs7.equals("")) {
	//	folioCert = acuse.toString();			
		netropology.utilerias.Seguridad s = new netropology.utilerias.Seguridad();
	//	if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
		
			rg = bean.reprocesarSaldosRegConError(ic_folio,ic_if);
			
			mapa=new HashMap();
			mapa.put("ETIQUETA","No. total de registros transmitidos");
			mapa.put("INFORMACION",tot_registros);	
			registros.add(mapa);
			
			mapa=new HashMap();
			mapa.put("ETIQUETA","Monto total saldos al fin del mes");
			mapa.put("INFORMACION","$"+Comunes.formatoDecimal(String.valueOf(rg.getSumRegOk()),2) );	
			registros.add(mapa);
			
			mapa=new HashMap();
			mapa.put("ETIQUETA","Mes de carga");
			mapa.put("INFORMACION",mesActual+" de "+anioCarga);	
			registros.add(mapa);
						
			mapa=new HashMap();
			mapa.put("ETIQUETA","Fecha de carga");
			mapa.put("INFORMACION",rg.getFecha());	
			registros.add(mapa);
			
			mapa=new HashMap();
			mapa.put("ETIQUETA","Hora de carga");
			mapa.put("INFORMACION",rg.getHora());	
			registros.add(mapa);
			
			mapa=new HashMap();
			mapa.put("ETIQUETA","Usuario");
			mapa.put("INFORMACION",iNoUsuario+" - "+strNombreUsuario);	
			registros.add(mapa);
			
			monto_total= Double.toString (rg.getSumRegOk());			
			mes_anioCarga=mesActual+" de "+anioCarga;
			fecha=rg.getFecha(); 
			hora= rg.getHora(); 	
			
			String consreg =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consreg);
			jsonObj.put("monto_total", monto_total); 
			jsonObj.put("mes_anioCarga", mes_anioCarga); 
			jsonObj.put("fecha", fecha); 
			jsonObj.put("hora", hora); 	
			resultado = jsonObj.toString();
			
			
	/*	}else{		
			jsonObj.put("success", new Boolean(false));	
			throw new NafinException("GRAL0021");
			
		}
	}else{
		jsonObj.put("success", new Boolean(false));
		throw new NafinException("GRAL0021");
	}*/
		

	resultado = jsonObj.toString();
	

}else if(informacion.equals("generarPDFAcuse")){
	
	String tot_registros = (request.getParameter("tot_registros")==null)?"":request.getParameter("tot_registros");
	String monto_total = (request.getParameter("monto_total")==null)?"":request.getParameter("monto_total");
	String mes_anioCarga = (request.getParameter("mes_anioCarga")==null)?"":request.getParameter("mes_anioCarga");
	String fecha = (request.getParameter("fecha")==null)?"":request.getParameter("fecha");
	String hora = (request.getParameter("hora")==null)?"":request.getParameter("hora");
	
	List parametros = new ArrayList();
	
	parametros.add(tot_registros);
	parametros.add(monto_total);
	parametros.add(mes_anioCarga);
	parametros.add(fecha);
	parametros.add(hora);
	parametros.add(iNoUsuario+" - "+strNombreUsuario);	
	
	try {
		String nombreArchivo= paginador.crearPdfAcuse( request, parametros, strDirectorioTemp  );			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}
	
	resultado = jsonObj.toString(); 
	
	
	
}

%>
<%=resultado%>
