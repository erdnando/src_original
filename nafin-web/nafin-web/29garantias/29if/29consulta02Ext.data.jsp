<%@ page import=" java.sql.*, java.text.*,netropology.utilerias.*"%>
<%@ page import="java.util.ArrayList,java.util.List,java.text.SimpleDateFormat,
netropology.utilerias.usuarios.*,com.netro.seguridadbean.SeguException,
				 javax.naming.* "
	errorPage="/00utils/error_extjs.jsp"
	contentType="application/json;charset=UTF-8"
%>
<%@ include file="/appComun.jspf" %>

<%@ include file="/29garantias/29secsessionWMenu.jspf" %>

<jsp:useBean id="consulta" scope="page" class="com.netro.garantias.ConsSolicIFGAbean" />

<jsp:setProperty name="consulta" property="iTipoOperacion" value="5"/> <%--Tipo de Operaci�n--%>
<jsp:setProperty name="consulta" property="iIF" value="<%=Integer.parseInt(iNoCliente)%>"/>

<%

String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String resultado="";
//Petici�n de consulta Principal
	if(informacion.equals("Consulta"))
	{
		String				hidAction		= (request.getParameter("hidAction")==null)?"":request.getParameter("hidAction");
		String				txt_folio		= (request.getParameter("folio")==null)?"":request.getParameter("folio");
		AccesoDB 			con				= new AccesoDB();
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		String				qrySentencia	= "";
		
		int 				vigencia		= 0;
		int 				i 				= 0;
		
		try {
			con.conexionDB();
			 List params = new ArrayList();
		qrySentencia = consulta.getQuerySolicitudes();
		params.add(new Integer(5));
		params.add(iNoCliente);
			
			Registros reg=con.consultarDB(qrySentencia, params);
			resultado=
			"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		
					} catch (Exception e) {
					throw new AppException("...:::Error al consultar base de NE");
				} finally {
					if (con.hayConexionAbierta()) {
						con.cierraConexionDB();
					}
			}
	}
	//Creacion de PDF
	else if(informacion.equals("pdf"))
	{
	/*consulta.getPathFilePDF(String clave_if,String folio,String origen,String cvePerf,
	String strDirectorioTemp,HttpSession session,String strDirectorioPublicacion,String iNoCliente);*/
	System.out.println("...::Generar archivo PDF::..");
	
	String clave_if = request.getParameter("clave_if");
	String folio = request.getParameter("folio");
	String origen = request.getParameter("origen");
	String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);

	String nomFile=consulta.getPathFilePDF(clave_if,folio,origen,cvePerf,
	strDirectorioTemp,session,strDirectorioPublicacion,iNoCliente);
			if(nomFile.equals("error"))
			{
				resultado=
				"{\"success\": false, \"urlArchivo\": \"error"	+	 "\""+ "}";
			}
			else
			{
				resultado=
				"{\"success\": true, \"urlArchivo\": \""	+	strDirecVirtualTemp+nomFile+ "\""+ "}";	
			}
	}else if(informacion.equals("archivo"))
	{
		
		String folio = request.getParameter("folio");
		String opcion = request.getParameter("opcion");
		String clave_if = request.getParameter("clave_if")!=null?request.getParameter("clave_if"):"";
		
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet rs = null;
		String qrySentencia	= "";
		String tabla = "";
		int vigencia = 0;
		int i = 0;
		
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo = null;
		
		try {
			con.conexionDB();
			if(opcion.equals("origen")){
			 tabla = "gti_contenido_arch";
		  }else{
			 tabla = "gti_arch_rechazo";
		  }
		  
		  if(!"".equals(clave_if))
			iNoCliente = clave_if;
		  
			qrySentencia =
				"select cg_contenido"+
				" from "+tabla+" A"+
				" ,comcat_if I"+
				" where A.ic_if_siag = I.ic_if_siag"+
				" and A.ic_folio = ?"+
				" and I.ic_if = ?"+
				" order by A.ic_linea";
			
			ps = con.queryPrecompilado(qrySentencia);
		  ps.setLong(1, Long.parseLong(folio));
			ps.setLong(2, Long.parseLong(iNoCliente));
			
			rs = ps.executeQuery();
			while(rs.next()){
				contenidoArchivo.append(rs.getString("cg_contenido")+"\n");
				i++;
			}
			ps.close();
			
			if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt"))
			{
				
				resultado="{\"success\": false, \"urlArchivo\": \"error"	+ "\""+ "}";	
				///out.print("<--!Error al generar el archivo-->");
				}
			else
			{
				if(i>0)
				{
					nombreArchivo = archivo.nombre;
					resultado="{\"success\": true, \"urlArchivo\": \""	+	strDirecVirtualTemp+nombreArchivo+ "\""+ "}";			
				}
				else
				{
					resultado="{\"success\": true, \"urlArchivo\": \"NOREG"	+ "\""+ "}";			
				}
			}
				
		
			}catch(Exception e){
			e.printStackTrace();
			
			}finally{
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
			}
	}
%>
<%=resultado%>
