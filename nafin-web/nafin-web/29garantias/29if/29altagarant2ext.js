Ext.onReady(function() {
		
      var bajarArchivoTxt =  function(resp) {
         var jsonData = Ext.util.JSON.decode(resp.responseText);
         if (jsonData.success == true) {
            if(jsonData.result!=''){
               Ext.Msg.show({
                  title:   'Error',
                  msg:     jsonData.result,
                  icon:    Ext.Msg.ERROR,
                  buttons: Ext.Msg.OK
               });
            }else{
               //var fp = Ext.getDom('formAux');
               var infoR = Ext.util.JSON.decode(resp.responseText);
               var archivo = infoR.urlArchivo;				
               archivo = archivo.replace('/nafin','');
               var params = {nombreArchivo: archivo};				
               fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
               fp.getForm().getEl().dom.submit();
/*
               var forma = Ext.getDom('formAux');
               forma.action = jsonData.urlArchivo;
               forma.submit();*/
            }
         } else {
               Ext.Msg.show({
                  title:   'Error',
                  msg:     'No se pudo generar el archivo',
                  icon:    Ext.Msg.ERROR,
                  buttons: Ext.Msg.OK
               });
         }
      }
		/*** PROCESAR **/
		var procesarConsultaData = function(store, arrRegistros, opts) {
			var grid = Ext.getCmp('grid');
			if (arrRegistros != null) {
				if (!grid.isVisible()) {
					grid.show();
				}
				
				var el = grid.getGridEl();
				
				if(store.getTotalCount() > 0) {
					el.unmask();
				} else {
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
		
		/*
		*	Store
		*/
		
		catalogoPrograma = new Ext.data.ArrayStore({
			  fields: ['myId',	'displayText'],
			  data: [['G', 'Garant�as'],['E','Cr�dito Educativo']]
		 });
		 
		var storeSituacion = new Ext.data.SimpleStore({
				fields: 	['clave', 'descripcion'],
				data:		[['5','CONFIRMACI�N'],['1','ENV�O PARA VALIDACI�N Y CARGA'],['6','PARA CORRECCI�N'],['2','VALIDACI�N']]
		});
		
		var consultaDataGrid = new Ext.data.JsonStore({
				root : 'registros',
				url : '29altagarant2ext.data.jsp',
				baseParams: {
					informacion: 'Consulta',
               flagConsulta:'CONSULTAR'
				},
				fields: [
					{name: 'TIPO_ENVIO'},
					{name: 'FECHA'},
					{name: 'FOLIO'},
					{name: 'SITUACION'},
               {name: 'IC_SITUACION'},
					{name: 'MONTO_ENVIADO'},
					{name: 'REG_ACEP'},
					{name: 'REG_RECH'},
					{name: 'NUM_REG'},
               {name: 'DECIDE_RESULTADOS'},
               {name: 'DECIDE_ARCHIVOS_ERRORES'},
					{name: 'PROGRAMA'}
				],
				totalProperty : 'total',
				messageProperty: 'msg',
				autoLoad: false,
				listeners: {
					load: procesarConsultaData,
					exception: {
						fn: function(proxy, type, action, optionsRequest, response, args) {
							NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						}
					}
				}
		});
		/*
		*	GRID
		*/
		var grid = {	
			store: consultaDataGrid,
			xtype:'grid',
			id: 'grid',
			hidden: true,
			columns: [{
					header: 'Tipo de Env�o', tooltip: 'Tipo de Env�o',
					dataIndex: 'TIPO_ENVIO',
					sortable: true,
					width: 80, resizable: true,
					align: 'center'
				},{
					header: 'Fecha y Hora de Operaci�n', tooltip: 'Fecha y Hora de Operaci�n',
					dataIndex: 'FECHA',
					sortable: true,	align: 'center',
					width: 140, resizable: true,
					align: 'center'
				},{
					header: 'Folio de la Operaci�n', tooltip: 'Folio de la Operaci�n',
					dataIndex: 'FOLIO',
					sortable: true,	align: 'center',
					width: 110, resizable: true,
					align: 'center'
				},{
					header: 'Programa', tooltip: 'Programa',
					dataIndex: 'PROGRAMA',
					sortable: true,	align: 'center',
					width: 110, resizable: true,
					align: 'center'
				},{
					header: 'Situaci�n', tooltip: 'Situaci�n',
					dataIndex: 'SITUACION',
					sortable: true,	align: 'center',
					width: 100, resizable: true,
					align: 'center'
				},{
					header: 'Monto Enviado', tooltip: 'Monto Enviado',
					dataIndex: 'MONTO_ENVIADO',
					sortable: true,	align: 'center',
					width: 100, resizable: true,
					align: 'right',
               renderer: Ext.util.Format.usMoney
				},{
					header: 'No. Operaciones aceptadas', tooltip: 'N�mero de operaciones aceptadas',
					dataIndex: 'REG_ACEP',
					sortable: true,	align: 'center',
					width: 150, resizable: true,
					align: 'center'
				},{
					header: 'No. Operaciones con errores', tooltip: 'N�mero de operaciones con errores',
					dataIndex: 'REG_RECH',
					sortable: true,	align: 'center',
					width: 160, resizable: true,
					align: 'center'
				},{
					header: 'Total de Operaciones', tooltip: 'Total de Operaciones',
					dataIndex: 'NUM_REG',
					sortable: true,	align: 'center',
					width: 120, resizable: true,
					align: 'center'
				},{
               xtype: 'actioncolumn',
					header: 'Archivo Origen', tooltip: 'Archivo Origen',
					dataIndex: '',
					sortable: true,	align: 'center',
					width: 90, resizable: true,
					align: 'center',
					items: [
						{ //para cambiar icono segun extension PDF/DOC 
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
								var icono = 'icoBuscar';
								return icono;
							},
							tooltip: 'Buscar Expediente',
							handler: function(grid, rowIdx, colIds){
                        var reg = grid.getStore().getAt(rowIdx);                                          
                        Ext.Ajax.request({
                           url:     '29altagarant2ext.data.jsp',
                           params:  {informacion: 'ArchivoTexto', opcion: 'origen', folio: reg.get('FOLIO')},
                           success:  bajarArchivoTxt,
                           failure: function ( response, request ) {
                               Ext.Msg.show({
                                 title:   'Error: ' + response.status,
                                 msg:     response.responseText,
                                 icon:    Ext.Msg.ERROR,
                                 buttons: Ext.Msg.OK
                               });
                           }
                        });  										
                     }				
                  }
               ]
				},{
               xtype:   'actioncolumn',
					header: 'Resultados', tooltip: 'Resultados',
					dataIndex: 'DECIDE_RESULTADOS',
					sortable: true,	align: 'center',
					width: 80, resizable: true,
					align: 'center',
					items: [
						{ //para cambiar icono segun extension PDF/DOC 
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {		
								var icono = registro.get('DECIDE_RESULTADOS')=='true'?'icoBuscar':'';
								return icono;
							},
							tooltip: 'Buscar Expediente',
							handler: function(grid, rowIdx, colIds){
                        var reg = grid.getStore().getAt(rowIdx);                                          
                        Ext.Ajax.request({
                           url:     '29altagarant2ext.data.jsp',
                           params:  {informacion: 'ArchivoPdf', opcion: 'CONSALTA', folio: reg.get('FOLIO')},
                           success:  bajarArchivoTxt,
                           failure: function ( response, request ) {
                               Ext.Msg.show({
                                 title:   'Error: ' + response.status,
                                 msg:     response.responseText,
                                 icon:    Ext.Msg.ERROR,
                                 buttons: Ext.Msg.OK
                               });
                           }
                        });  										
                     }				
                  }
               ]
				},{
               xtype:   'actioncolumn',
					header: 'Archivo de Errores', tooltip: 'Archivo de Errores',
					dataIndex: 'DECIDE_ARCHIVOS_ERRORES',
					sortable: true,	align: 'center',
					width: 110, resizable: true,
					align: 'center',
               items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {		
								var icono = registro.get('DECIDE_ARCHIVOS_ERRORES')=='true'?'icoBuscar':'';
								return icono;
							},
							tooltip: 'Buscar Expediente',
							handler: function(grid, rowIdx, colIds){
                        var reg = grid.getStore().getAt(rowIdx);                      
                        Ext.Ajax.request({
                           url:     '29altagarant2ext.data.jsp',
                           params:  {informacion: 'ArchivoTexto', opcion: 'errores', folio: reg.get('FOLIO')},
                           success:  bajarArchivoTxt,
                           failure: function ( response, request ) {
                               Ext.Msg.show({
                                 title:   'Error: ' + response.status,
                                 msg:     response.responseText,
                                 icon:    Ext.Msg.ERROR,
                                 buttons: Ext.Msg.OK
                               });
                           }
                        });   										
                     }				
                  }
               ]
            }],
			stripeRows: true,
			loadMask: true,
			deferRowRender: false,
			height: 433,
			width: 920,
			colunmWidth: true,
			frame: true,
			style: 'margin:0 auto;',// para centrar el grid
			collapsible: false
		};
		var elementosForma = [{
				xtype:			'compositefield',
				fieldLabel: 	'Fecha de Operaci�n',
            combineErrors: false,
				items:[{
					xtype:	'datefield',
					id:		'id_fecha_oper_ini',
					name:		'fecha_oper_ini',
					//allowBlank: false,
					msgTarget: 'side',
					value:	new Date(),
					listeners:{
					
					}
				},{
					xtype:		'displayfield',
					value:		'a',
					width:		20
				},{
					xtype:		'datefield',
					name:			'fecha_oper_fin',
					id:			'id_fecha_oper_fin',
					//allowBlank: false,
					msgTarget: 'side',
					value:		new Date()
				}]
		},{
			xtype:				'combo',
			name:					'cbo_situacion',
			hiddenName:			'cbo_situacion',
			id:					'id_cbo_situacion',
			fieldLabel:			'Situaci�n',
			emptyText:			'- Seleccione -',
			mode:					'local',
			displayField:		'descripcion',
			valueField:			'clave',
			forceSelection:	true,
			triggerAction:		'all',
			allowBlank:			true,
			minChars:			1,
			store:				storeSituacion,
         anchor:           '93%'
		},{
			xtype: 'numberfield',
			name: 'folio',
			id: 'folio',
			fieldLabel: 'Folio Operaci�n',
			anchor:'93%',
			maxLength: 30
			},
		{
			xtype: 'combo',
			editable:true,
			forceSelection: true,
			fieldLabel: 'Programa',
			displayField: 'displayText',
			valueField: 'myId',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			name:'Hprogrma',
			store: catalogoPrograma,
			id: 'programa',
			mode: 'local',
			hiddenName: 'Hprograma',
			hidden: false,
			emptyText: 'Seleccionar un programa'
		}
		];
		
		var fp = new Ext.form.FormPanel({
			xtype:			'form',
			id:				'forma',
			width:			390,
			title:			'Alta de Garant�as - Consulta de Resultados',
			style:			'text-align:left;margin:0 auto;',
			hidden:			false,
			frame:			true,
			bodyStyle: 		'padding: 6px',
			labelWidth: 	110,
			defaults: {
				msgTarget:	'side',
				anchor: 		'-20'
			},
			items: 			elementosForma,
			monitorValid: 	true,
			buttons:[{
					text: 	'Consultar',
					id: 		'btnConsultar',
					iconCls: 'icoBuscar',
					handler: function(boton, evento) {
					var fecha_oper_ini = Ext.getCmp('id_fecha_oper_ini');
					var fecha_oper_fin = Ext.getCmp('id_fecha_oper_fin');
                  
					if((fecha_oper_ini.getValue()!='' && fecha_oper_fin.getValue()=='') || (fecha_oper_ini.getValue()=='' && fecha_oper_fin.getValue()!='')){
						if((fecha_oper_ini.getValue()!='' && fecha_oper_fin.getValue()=='')){
							fecha_oper_fin.markInvalid('Debe seleccionar la Fecha de Operaci�n Fin');
						}
						if(   fecha_oper_ini.getValue()=='' && fecha_oper_fin.getValue()!=''  ){
							fecha_oper_ini.markInvalid('Debe seleccionar la Fecha de Operaci�n Inicio');
						}  
						return;
					}
                  
                  Ext.getCmp('grid').show();
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						consultaDataGrid.load({
							params: Ext.apply(paramSubmit,{
                        informacion:'Consulta',
                        flagConsulta:'CONSULTAR'				
							})
						});
					} //fin handler
			},{
            text     :  'limpiar',
            iconCls  :  'icoLimpiar',
            handler  :  function(){
               window.location = '29altagarant2ext.jsp';
            }  
         }]
		})
	

	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		940,
		height: 		'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
         grid
		]
	});
	
});
