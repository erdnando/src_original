<%@ page 
	contentType=
		"application/json;charset=UTF-8" 
	import="
		net.sf.json.JSONArray,
		netropology.utilerias.*,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		java.util.*,
		java.sql.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.garantias.*,
		com.netro.cadenas.ParamCorreoExt01,
		javax.rmi.PortableRemoteObject " 
	errorPage=
		"/00utils/error_extjs.jsp"
%>  
<%@ include file="/01principal/01secsession.jspf" %>
<%    
String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String infoRegresar	= "";
String consulta   	= "";

if (informacion.equals("catalogoint") )	{
  
	JSONObject		 jsonObj  = new JSONObject();
  CatalogoSimple catInt      = new CatalogoSimple();
  
  catInt.setCampoClave("ic_if");
  catInt.setCampoDescripcion("cg_razon_social");
  catInt.setTabla("comcat_if");
  catInt.setOrden("cg_razon_social");
  
  List catInter = catInt.getListaElementos();  
	jsonObj.put("registros", catInter);	
  infoRegresar = jsonObj.toString();
    
}else if (informacion.equals("catalogotran") )	{
  
	JSONObject		 jsonObj  = new JSONObject();
  CatalogoSimple catTra   = new CatalogoSimple();
  
  catTra.setCampoClave("ic_tipo_operacion");
  catTra.setCampoDescripcion("cg_descripcion");
  catTra.setTabla("gticat_tipooperacion");
  catTra.setOrden("cg_descripcion");
  
  List catTransa = catTra.getListaElementos();  
	jsonObj.put("registros", catTransa);	
  infoRegresar = jsonObj.toString();
  System.err.println("catalogotran: "+infoRegresar);

} else if (informacion.equals("catalogoest") )	{
  
	JSONObject		 jsonObj  = new JSONObject();
  List catEstat = new ArrayList(); 
	jsonObj.put("registros", catEstat);	
  infoRegresar = jsonObj.toString();
  
} else if (informacion.equals("carga") ){

  String intermed	= (request.getParameter("intermed")	!=null)	?	request.getParameter("intermed")	:	"";
  String transacc	= (request.getParameter("transacc")	!=null)	?	request.getParameter("transacc")	:	"";
  String estatus	= (request.getParameter("estatus")	!=null)	?	request.getParameter("estatus")	:	"";
  JSONObject		 jsonObj  = new JSONObject();
  
  Garantias garantiasBean = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
  ParamCorreoExt01 param = new ParamCorreoExt01();
  List      principal    = new ArrayList();
 
  HashMap correos = garantiasBean.getParametrosDeNotificacion(intermed, transacc, estatus);
  
  String [] cadenaNafin = (String[])correos.get("cuentasNafin");
  HashMap cuentasN= new HashMap();
  List    cN = new ArrayList();
  int sizecuentasN = 0;
  try {
    for(int i=0;i<cadenaNafin.length;i++){
      cuentasN = new HashMap();
      cuentasN.put("descripcion",cadenaNafin[i]);
      cN.add(cuentasN);
      sizecuentasN++;
      }
  } catch(Exception e){
    sizecuentasN = 0;
  }
  String sizeN = String.valueOf(sizecuentasN);
  
  String [] cadenaIf = (String[])correos.get("cuentasIf");
  HashMap cuentasIf= new HashMap();
  List    cIf = new ArrayList();
  int sizecuentasIf = 0;
  try {
    for(int i=0;i<cadenaIf.length;i++){
      cuentasIf = new HashMap();
      cuentasIf.put("descripcion",cadenaIf[i]);
      cIf.add(cuentasIf);
      sizecuentasIf++;
      }
  } catch(Exception e){
    sizecuentasIf = 0;
  }
  String sizeIf = String.valueOf(sizecuentasIf); 
  
  param.setClaveTransaccion(transacc);
  List generaEs=param.getListaEstatus();  
  String sizeEstatus = String.valueOf(generaEs.size());
  
  jsonObj.put("success", new Boolean(true));
 
  jsonObj.put("sizeN", sizeN);
  jsonObj.put("cuentasN", cN);

  jsonObj.put("sizeIf", sizeIf);
  jsonObj.put("cuentasIf", cIf);
  
  jsonObj.put("sizeEst", sizeEstatus);
  jsonObj.put("estatus", generaEs);	
  
  infoRegresar = jsonObj.toString();

} else if (informacion.equals("guardar")){
  
  JSONObject jsonObj  = new JSONObject();
  String intermed	= (request.getParameter("intermed")	!=null)	?	request.getParameter("intermed")	:	"";
  String transacc	= (request.getParameter("transacc")	!=null)	?	request.getParameter("transacc")	:	"";
  String estatus	= (request.getParameter("estatus")	!=null)	?	request.getParameter("estatus")	  :	"";
  String correoNa = (request.getParameter("correoNa")	!=null)	?	request.getParameter("correoNa")	:	"";
  String correoIf = (request.getParameter("correoIf")	!=null)	?	request.getParameter("correoIf")	:	"";
  String cN="";
  String cI="";
  if(correoNa.equals("") ){
  } else {
   cN = correoNa.substring(0,correoNa.length()-1);
  }
  if(correoIf.equals("") ){
  } else {
	cI = correoIf.substring(0,correoIf.length()-1); 
  }

  Garantias garantiasBean = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
  
  try {
    if (transacc.equals("")){
        transacc="0";
    }      
    garantiasBean.setParametrosDeNotificacion(intermed,transacc,estatus,cN,cI);
    jsonObj.put("success", new Boolean(true));
  } catch (Exception e){
    jsonObj.put("success", new Boolean(false));
    System.err.println("Ocurrio un error al Guardar" + e);
  }
  infoRegresar = jsonObj.toString();
}

%>
<%=  infoRegresar %>