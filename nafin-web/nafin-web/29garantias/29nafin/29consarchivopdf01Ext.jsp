<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*, 
				netropology.utilerias.*, 
				com.netro.pdf.*,
				com.netro.afiliacion.*,
				com.netro.model.catalogos.*,
				com.netro.garantias.*,
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/29garantias/29secsession.jspf"%>
<%
String clave_if = request.getParameter("clave_if");
String folio = request.getParameter("folio");
String origen = request.getParameter("origen");


JSONObject jsonObj = new JSONObject();
String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";

AccesoDB con = new AccesoDB();
PreparedStatement	ps	= null;
ResultSet rs	= null;
String qrySentencia	= "";
String tabla = "";
CreaArchivo archivo = new CreaArchivo();
String nombreArchivo = archivo.nombreArchivo()+".pdf";
int i = 0;
ComunesPDF pdfDoc = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
String icIfSiag = "";
String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String diaActual    = fechaActual.substring(0,2);
String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
String anioActual   = fechaActual.substring(6,10);
String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                                 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		    					 (String)session.getAttribute("sesExterno"),
								 
                                 (String) session.getAttribute("strNombre"),
                                    (String) session.getAttribute("strNombreUsuario"),
					    		 (String)session.getAttribute("strLogo"),
								 strDirectorioPublicacion);

pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

try{
	con.conexionDB();
	
	
	qrySentencia =
		"select "+
		" ES.ic_folio"+
		" ,I.cg_razon_social as NOMBRE_IF"+
		" ,to_char(df_validacion_siag,'dd/mm/yyyy') as FECHA_PROCESO "+
		" ,to_char(df_autorizacion_siag,'dd/mm/yyyy') as FECHA_AUTORIZA "+
		" ,NVL(ES.in_registros_acep,0) as ACEP"+
		" ,NVL(ES.in_registros_rech,0) as RECH"+
		" ,NVL(ES.in_registros,0) as TOTAL"+
		" ,es.IC_IF_SIAG"+
		" ,es.ic_situacion"+
		" ,es.ig_anio_trim_calif"+
		" ,es.ig_trim_calif"+
		" from gti_estatus_solic ES"+
		" ,comcat_if I"+
		" where ES.ic_if_siag = I.ic_if_siag"+
		" and ES.ic_folio = ?"+
		" and I.ic_if = ?";

	ps = con.queryPrecompilado(qrySentencia);
	ps.setString(1, folio);
	ps.setString(2, clave_if);
	
	rs = ps.executeQuery();
	if(rs.next()){
		icIfSiag = rs.getString("IC_IF_SIAG");
		pdfDoc.setTable(1,100);
		if("CONSALTA".equals(origen)) {
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("RESULTADOS DE SOLICITUD DE ALTA DE GARANTIAS","celda04",ComunesPDF.CENTER);
		}else if("CONSSALCOM".equals(origen)){
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE SALDOS Y PAGO DE COMISIONES","celda04",ComunesPDF.CENTER);		
		}else if("CONSCALIF".equals(origen)){
			pdfDoc.addText("\nAño: "+rs.getString("IG_ANIO_TRIM_CALIF")+"   Trimestre: "+rs.getString("IG_TRIM_CALIF")+"\n","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE CALIFICACION DE CARTERA","celda04",ComunesPDF.CENTER);		
		}else{
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("RESULTADOS","celda04",ComunesPDF.CENTER);
		}
		pdfDoc.addTable();

		if(!"CONSSALCOM".equals(origen)){
					if(rs.getString("FECHA_PROCESO")== null ){
					pdfDoc.addText("Fecha de operación en firme: \n","formas",ComunesPDF.RIGHT);
					}else{
					pdfDoc.addText("Fecha de operación en firme: "+rs.getString("FECHA_PROCESO")+"\n","formas",ComunesPDF.RIGHT);
					}
		}else{
			int situacion = rs.getInt("ic_situacion");
			if(situacion ==5){
					if(rs.getString("FECHA_AUTORIZA") == null ){
					pdfDoc.addText("Fecha de operación en firme: \n","formas",ComunesPDF.RIGHT);
					}else{
					pdfDoc.addText("Fecha de operación en firme: "+rs.getString("FECHA_AUTORIZA")+"\n","formas",ComunesPDF.RIGHT);
					}
			}else{
					if(rs.getString("FECHA_PROCESO") == null ){
					pdfDoc.addText("Fecha de cálculo de comisiones: \n","formas",ComunesPDF.RIGHT);
					}else{
					pdfDoc.addText("Fecha de cálculo de comisiones: "+rs.getString("FECHA_PROCESO")+"\n","formas",ComunesPDF.RIGHT);
					}
				}
			}
		pdfDoc.setTable(5,95);
			pdfDoc.setCell("FOLIO DE LA OPERACION","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("INTERMEDIARIO FINANCIERO","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS","celda01",ComunesPDF.CENTER);

			pdfDoc.setCell(rs.getString("IC_FOLIO"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getString("NOMBRE_IF"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getString("ACEP"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getString("RECH"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getString("TOTAL"),"formas",ComunesPDF.CENTER);			

		pdfDoc.addTable();
	}
	ps.close();	

	pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);	
	if("CONSALTA".equals(origen)) {
		qrySentencia =
			" select ic_programa"   +
			"  ,fn_porc_comision"   +
			"  ,in_registros_acep"   +
			"  ,in_registros_rech"   +
			"  ,in_registros"   +
			" from gti_det_alta_gtia GD"   +
			" , comcat_if I"   +
			" where GD.ic_if_siag = I.ic_if_siag" +
			" and GD.ic_folio = ?"+
			" and I.ic_if = ?";
		
		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1, folio);
		ps.setString(2, clave_if);
		
		rs = ps.executeQuery();
		while(rs.next()){
			if(i==0){
				pdfDoc.setTable(1,100);
				pdfDoc.setCell("DETALLE DE GARANTIAS POR PROGRAMA","celda04",ComunesPDF.CENTER);
				pdfDoc.addTable();
				pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(5,95);
				pdfDoc.setCell("CLAVE DEL PROGRAMA","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PORCENTAJE DE COMISION","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS","celda01",ComunesPDF.CENTER);
			}
	
			pdfDoc.setCell(rs.getString("IC_PROGRAMA")+" ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("FN_PORC_COMISION"),4)+" ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getInt("IN_REGISTROS_ACEP")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getInt("IN_REGISTROS_RECH")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getInt("IN_REGISTROS")+" ","formas",ComunesPDF.CENTER);
			i++;
		}	// while
		ps.close();
	}// ALTA
	else if("CONSSALCOM".equals(origen)) {
		qrySentencia =
			"  SELECT nvl(m.cd_nombre,'NA') as cd_nombre,"   +
			"  		in_aceptados,"   +
			"         in_rechazados,"   +
			" 		in_tot_registros, "   +
			" 		fn_impte_autoriza"   +
			"  FROM GTI_DETALLE_SALDOS ds"   +
			"   ,comcat_moneda m"   +
			"   WHERE ds.ic_moneda = m.ic_moneda(+)"   +
			"   and ic_if_siag = ? "+
			" AND ic_folio = ?"  +
			" order by decode(m.ic_moneda,1,1,54,2,3)";
		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1,icIfSiag);
		ps.setString(2,folio);
		rs = ps.executeQuery();
		while(rs.next()){
			if(i==0){
				pdfDoc.setTable(1,100);
				pdfDoc.setCell("DETALLE DE COMISIONES POR MONEDA","celda04",ComunesPDF.CENTER);
				pdfDoc.addTable();
				pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(5,95);
				pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IMPORTE DE COMISIÓN E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
			}
	
			pdfDoc.setCell(rs.getString("cd_nombre")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getInt("in_aceptados")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getInt("in_rechazados")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getInt("in_tot_registros")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
			i++;
		}	// while
		rs.close();ps.close();
	}//CONSSALCOM
	
	if(i>0)
		pdfDoc.addTable();

	pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);			
		
		
		
	qrySentencia =
		"select cg_contenido"+
		" from gti_arch_resultado A"+
		" ,comcat_if I"+
		" where A.ic_if_siag = I.ic_if_siag"+
		" and A.ic_folio = ?"+
		" and I.ic_if = ?"+
		" order by A.ic_linea,A.ic_num_error";
	
	ps = con.queryPrecompilado(qrySentencia);
	ps.setString(1, folio);
	ps.setString(2, clave_if);
	
	rs = ps.executeQuery();
	i=0;
	while(rs.next()){
		if(i==0){
			pdfDoc.setTable(1,100);
			pdfDoc.setCell("ERRORES GENERADOS","celda04",ComunesPDF.CENTER);
			pdfDoc.addTable();		
		}
		pdfDoc.addText(rs.getString("cg_contenido")+"\n","formas",ComunesPDF.LEFT);
		i++;
	}
	ps.close();

	
	pdfDoc.endDocument();
	
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			
			infoRegresar = jsonObj.toString();	
	//response.sendRedirect(strDirecVirtualTemp+nombreArchivo);
	
	//out.println(origen);
	}catch(Exception e){
	e.printStackTrace();
	throw e;
}finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
}
	%>

<%=  infoRegresar%>