<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*, 
				netropology.utilerias.*, 
				com.netro.afiliacion.*,
				com.netro.model.catalogos.*,
				com.netro.garantias.*,
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/29garantias/29secsession.jspf"%>
<%  
	String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
	String operacion = (request.getParameter("operacion") !=null)?request.getParameter("operacion"):"";
	String nombreIF = (request.getParameter("nombreIF")!=null)?request.getParameter("nombreIF"):"0";
	String fechaOperacionMin = (request.getParameter("fechaOperacionMin") !=null)?request.getParameter("fechaOperacionMin"):"";
	String fechaOperacionMax = (request.getParameter("fechaOperacionMax") !=null)?request.getParameter("fechaOperacionMax"):"";
	String situacion = (request.getParameter("situacion") !=null)?request.getParameter("situacion"):"";
	String folio_operacion = (request.getParameter("folioOperacion") !=null)?request.getParameter("folioOperacion"):"";

   
	int  start= 0, limit =0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	
	if(informacion.equals("ConsultaIF")){
		
		CatalogoIF cat = new CatalogoIF();
		cat.setCampoClave("I.ic_if");
		cat.setCampoDescripcion("I.cg_razon_social");
		cat.setOrden("2");
		List lista = cat.getListaElementosGral();
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(lista);
		infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";

	}else if(informacion.equals("ConsultaSituacion")){
		
		CatalogoSituacion cat = new CatalogoSituacion();
		cat.setClaveTipoOperacion("3");
		cat.setCampoClave("ic_situacion");
		cat.setCampoDescripcion("cg_descripcion"); 
		List lista = cat.getListaElementos();
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(lista);
		infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
	}else if (	informacion.equals("Consultar") ||informacion.equals("ArchivoPdfSolPagoGaran")|| informacion.equals("PdfResultados") ){
		ConsSolicPagoGaranExt clasePag = new ConsSolicPagoGaranExt();
		clasePag.setIntermediario(nombreIF);
		clasePag.setFoperacionIni(fechaOperacionMin);
		clasePag.setFoperacionFin(fechaOperacionMax);
		clasePag.setSituacion(situacion);
		clasePag.setFolio(folio_operacion);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(clasePag);
		
		if(informacion.equals("Consultar") )  {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
		
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		}	
		if(informacion.equals("Consultar") ){
	
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
					consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
				jsonObj = JSONObject.fromObject(consulta);
			
		}else if (informacion.equals("ArchivoPdfSolPagoGaran")	){
			
			String folio_archi = (request.getParameter("folio") !=null)?request.getParameter("folio"):"";
			clasePag.setFolio(folio_archi);
			clasePag.setTipoArchivo("PdfSolPagoGaran");
			
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PdfSolPagoGaran");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}

		}else if (informacion.equals("PdfResultados")	){
			
			String folio_archi = (request.getParameter("folio") !=null)?request.getParameter("folio"):"";
			String ic_if_siag = (request.getParameter("ic_if_siag") !=null)?request.getParameter("ic_if_siag"):"";
			String cc_garantias = (request.getParameter("cc_garantias") !=null)?request.getParameter("cc_garantias"):"";
			String ic_contragarante = (request.getParameter("ic_contragarante") !=null)?request.getParameter("ic_contragarante"):"";

			clasePag.setFolio(folio_archi);
			clasePag.setIc_if_siag(ic_if_siag);
			clasePag.setCc_garantias(cc_garantias);
			clasePag.setIc_contragarante(ic_contragarante);
			clasePag.setTipoArchivo("PdfResultados");
			
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PdfResultados");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}

		}
		infoRegresar = jsonObj.toString();		
	}else if(informacion.equals("ventanaImagen")){
		
		String folio_archi = (request.getParameter("folio") !=null)?request.getParameter("folio"):"";
		ImagenPagoGarantia imagenes = new ImagenPagoGarantia();
		imagenes.setFolio(folio_archi);
		try{
			Registros reg = imagenes.consultarImagenesPagoGarantia();
			while(reg.next()) {
				String clave = reg.getString("ic_imagen_solpago");
				String descripcion = reg.getString("cg_descripcion");
			}
			jsonObj.put("registros",reg.getJSONData());
		}catch(Exception e){
			throw new AppException("Error en los parametros recibidos", e);

		}
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("archivoCargado")){
		String claveImagen = (request.getParameter("claveImagen") !=null)?request.getParameter("claveImagen"):"";
		ImagenPagoGarantia imagen = new ImagenPagoGarantia();
		try{
			imagen.setClaveImagen(claveImagen);
			String nombreArchivo =	imagen.consultarImagenPagoGarantia(strDirectorioTemp);
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}catch(Exception e){
			throw new AppException("Error en los parametros recibidos", e);
		}

	infoRegresar = jsonObj.toString();
}


%>
<%=  infoRegresar%>