<%@ page 
	contentType=
		"application/json;charset=UTF-8" 
	import="
    net.sf.json.JSONArray,
    net.sf.json.JSONObject,
    java.util.*,
    java.math.BigDecimal,
    netropology.utilerias.*,
     com.netro.seguridadbean.*,
    com.netro.model.catalogos.*,
    com.netro.garantias.ConsultaResultados,
    com.netro.garantias.MonitorResultadosExt01,
    javax.rmi.PortableRemoteObject " 
	errorPage=
		"/00utils/error_extjs.jsp"
%> 
<%@ include file="../29secsession.jspf" %>
<%    
String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String infoRegresar	= "";
String consulta   	= "";

if (informacion.equals("cargaFecha") )	{
  
  JSONObject		 jsonObj  = new JSONObject();
  Calendar calendario = Calendar.getInstance();  
  SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
  String fechaHoy = formatoFecha.format(calendario.getTime());
  
  CatalogoSimple cat = new CatalogoSimple();
  cat.setTabla("gticat_tipooperacion");
  cat.setCampoClave("ic_tipo_operacion");
  cat.setCampoDescripcion("cg_descripcion");
  List claves = new ArrayList();
  claves.add("4");	//4= Recuperaciones automáticas
  cat.setValoresCondicionIn(claves);
  List l = cat.getListaElementos();
  String tipoOperacion = "";
  if (l!=null && l.size()>0) {
    tipoOperacion = ((ElementoCatalogo)l.get(0)).getDescripcion();
  } 
  
  jsonObj.put("success"	,  new Boolean(true));
  jsonObj.put("fechaHoy", fechaHoy);
  jsonObj.put("tipoOperacion", tipoOperacion);
  infoRegresar = jsonObj.toString();

} else if (informacion.equals("catalogoif") )	{

  JSONObject		 jsonObj  = new JSONObject();
  CatalogoSimple catInt      = new CatalogoSimple();
  
  catInt.setCampoClave("ic_if");
  catInt.setCampoDescripcion("cg_razon_social");
  catInt.setTabla("comcat_if");
  catInt.setOrden("2");
  List catInter = catInt.getListaElementos();    
	jsonObj.put("registros", catInter);
  infoRegresar = jsonObj.toString();

} else if (informacion.equals("situacion") )	{

  JSONObject		 jsonObj  = new JSONObject();
  CatalogoSituacion catsit = new CatalogoSituacion();
  catsit.setCampoClave("ic_situacion");
  catsit.setCampoDescripcion("cg_descripcion");
  catsit.setClaveTipoOperacion("4"); //Recuperaciones automáticas
  List clavesSit = new ArrayList();
  clavesSit.add("1");	//Envio para validacion y carga
  clavesSit.add("4");	//Registro incompleto
  clavesSit.add("5");	//Confirmación
  catsit.setValoresCondicionIn(clavesSit);
  catsit.setOrden("ic_situacion");
  List lSit = catsit.getListaElementos();   
	jsonObj.put("registros", lSit);
  infoRegresar = jsonObj.toString();

} else if (informacion.equals("consultaGeneral")){
  JSONObject  jsonObj  = new JSONObject();
  ConsultaResultados paginador = new ConsultaResultados();
  String      noIf		 = (request.getParameter("_cmb_if") 		!=null)	?	request.getParameter("_cmb_if")		:	"";
  String fechaOperIni	 = (request.getParameter("_fechaReg")		!=null)	?	request.getParameter("_fechaReg")	:	"";
  String fechaOperFin  = (request.getParameter("_fechaFinReg") 	!=null)	?	request.getParameter("_fechaFinReg"):	"";
  String situacion     = (request.getParameter("_cmbtd") 			!=null)	?	request.getParameter("_cmbtd")		:	"";
  if(noIf.equals("0")){
	noIf="";
  }
  paginador.setClaveIF(noIf);
  paginador.setFechaIni(fechaOperIni);
  paginador.setFechaFin(fechaOperFin);
  paginador.setOpcion(situacion);
  paginador.setEjecuta("1");
  //paginador.setFolio("");
  
	Registros reg= paginador.getDocumentQueryFile();
	jsonObj.put("total",new Integer( reg.getNumeroRegistros()) );	
	jsonObj.put("registros", reg.getJSONData() );	
  infoRegresar = jsonObj.toString();	

} else if(informacion.equals("TXT")){

  JSONObject  jsonObj  = new JSONObject();
  ConsultaResultados monitor = new ConsultaResultados();
  String      claveIf		 = (request.getParameter("claveIf") !=null)	?	request.getParameter("claveIf")	:	"";
  String      folio		   = (request.getParameter("folio") 	!=null)	?	request.getParameter("folio")		:	"";
  String      opcion		 = (request.getParameter("opcion") 	!=null)	?	request.getParameter("opcion")	:	"";
  
  monitor.setClaveIF(claveIf);  
  monitor.setFolio(folio);
  monitor.setOpcion(opcion);
  monitor.setStrDirectorioTemp(strDirectorioTemp);
  
  String nombreArchivo = monitor.crearArchivo();
  if (!nombreArchivo.equals("0")){
    jsonObj.put("urlArchivo", "/nafin/00tmp/29garantias/"+nombreArchivo);
  } else {
    jsonObj.put("urlArchivo", "0");
  }
  
	jsonObj.put("success", new Boolean(true));
  System.err.println ("urlArchivo: "+"/nafin/00tmp/29garantias/"+nombreArchivo);  
  infoRegresar = jsonObj.toString();

} else if(informacion.equals("PDF")){

  JSONObject  jsonObj  = new JSONObject();
  com.netro.seguridadbean.Seguridad BeanSegFacultad = null;
  ConsultaResultados monitor = new ConsultaResultados();
  String      claveIf		 = (request.getParameter("claveIf") !=null)	?	request.getParameter("claveIf")	:	"";
  String      folio		   = (request.getParameter("folio") 	!=null)	?	request.getParameter("folio")		:	"";
  String      origen		 = "CONSREC";
  String nombreArchivo="";
  int  start= 0, limit =0;
  
  try {
    BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
  } catch (Exception e) {
    System.out.println("EXISTEN DIFICULTADES TECNICAS, INTENTE MÁS TARDE"+e);	
  }
  String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);
  
  monitor.setClaveIF(claveIf);  
  monitor.setFolio(folio);
  monitor.setOrigen(origen);
  monitor.setCvePerf(cvePerf);  

  nombreArchivo= monitor.miPDF(request,strDirectorioTemp,"PDF");
	jsonObj.put("success", new Boolean(true));
  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
  infoRegresar = jsonObj.toString();

} 

%>
<%=  infoRegresar %>