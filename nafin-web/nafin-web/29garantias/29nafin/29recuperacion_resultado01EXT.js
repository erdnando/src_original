Ext.onReady(function(){

//-----------------------------procesarFecha---------------------------------
function procesarFecha(opts, success, response) {
  if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
    var info = Ext.util.JSON.decode(response.responseText);
    Ext.getCmp('fechaRegistro').setValue(info.fechaHoy);
    Ext.getCmp('fechaFinRegistro').setValue(info.fechaHoy);
    
    Ext.getCmp('formcon').setTitle(info.tipoOperacion+" - Consulta de resultados.");
  }
}  
//---------------------------Fin procesarFecha----------------------------

//---------------------------descargaArchivo------------------------------------
 function descargaArchivo(opts, success, response) {
if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
    var infoR = Ext.util.JSON.decode(response.responseText);
    var archivo = infoR.urlArchivo;	
    if (archivo == "0") {
      Ext.Msg.show({
      title: 'Archivo',
      msg: 'No se encontraro Registros',
      modal: true,
      icon: Ext.Msg.WARNING,
      buttons: Ext.Msg.OK
      });
    } else {
      archivo = archivo.replace('/nafin','');
      var params = {nombreArchivo: archivo};				
      fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
      fp.getForm().getEl().dom.submit();
      fp.el.unmask();
      var el = gridConsulta.getGridEl();
      el.unmask();
      //Ext.getCmp('btnGenerarCSV').enable();
      //var boton=Ext.getCmp('btnGenerarCSV');
      //boton.setIconClass('icoXls');
   }
  }else {
    NE.util.mostrarConnError(response,opts);
    fp.el.unmask();
  }
};
//---------------------------fin descargaArchivo--------------------------------

//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
	var 	fp = Ext.getCmp('formcon');
			fp.el.unmask();				
	var 	gridConsulta = Ext.getCmp('gridConsulta');	
	var 	el = gridConsulta.getGridEl();	
	if (arrRegistros != null) {
		if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
		if(store.getTotalCount() > 0) {					
				el.unmask();
				//Ext.getCmp('btnGenerarCSV').enable();				
			} else {		
				//Ext.getCmp('btnGenerarCSV').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
var consultaData = new Ext.data.JsonStore({
		root 			: 'registros',
		url 			: '29recuperacion_resultado01EXT.data.jsp',
		baseParams	: {
							informacion: 'consultaGeneral'
							},
		hidden		: true,
		fields		: [	
              {	name: 'CLAVE_IF'},	
							{	name: 'NOMBRE_IF'},	
							{	name: 'FECHA'},	
							{	name: 'IC_FOLIO'},	
							{	name: 'SITUACION'},	
              {	name: 'MONTO_ENVIADO'},
              {	name: 'IC_SITUACION'},
							{	name: 'TIPO_ENVIO'},	
							{	name: 'IN_REGISTROS_ACEP'},	
							{	name: 'IN_REGISTROS_RECH'},	
							{	name: 'IN_REGISTROS'}   
							],		
		totalProperty 	: 'total',
		messageProperty: 'msg',
		autoLoad			: false,
		listeners		: {
			load			: procesarConsultaData,
				exception: {
						fn	: function(proxy, type, action, optionsRequest, response, args) {
								NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
								//LLama procesar consulta, para que desbloquee los componentes.
								procesarConsultaData(null, null, null);					
								}
								}
								}					
	})
//----------------------------Fin ConsultaData----------------------------------

//------------------------------------If-------------------------------------
var If = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin If------------------------------------

//------------------------------------If-------------------------------------
var Situacion = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin If------------------------------------

//---------------------------- procesarIf--------------------------------
	var procesarIf = function(store, arrRegistros, opts) {
		store.insert(0,new If({ 
		clave: "0", 
		descripcion: "Seleccionar...", 
		loadMsg: ""})); 		
    Ext.getCmp('cmbif').setValue("0");
		store.commitChanges(); 
	}	
//----------------------------Fin procesarIf--------------------------------

//---------------------------- procesarSituacion--------------------------------
	var procesarSituacion = function(store, arrRegistros, opts) {
		store.insert(0,new Situacion({ 
		clave: "", 
		descripcion: "Seleccionar...", 
		loadMsg: ""})); 		
    Ext.getCmp('cmbTipoDispersion').setValue("");
		store.commitChanges(); 
	}	
//----------------------------Fin procesarSituacion--------------------------------

//------------------------------Catalogo IF-------------------------------------
 var catalogoIf = new Ext.data.JsonStore({
		id					: 'catalogoIf',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '29recuperacion_resultado01EXT.data.jsp',
		baseParams		: {
								informacion		: 'catalogoif'  
								},
		totalProperty 	: 'total',
		autoLoad			: false,
		listeners		: {
                load: procesarIf,
								beforeload: NE.util.initMensajeCargaCombo,
								exception: NE.util.mostrarDataProxyError		
								}
	})
//------------------------------Fin Catalogo IF---------------------------------

//------------------------------Catalogo Situacion-------------------------------------
 var catalogoSituacion = new Ext.data.JsonStore({
		id					: 'catalogoSituacion',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '29recuperacion_resultado01EXT.data.jsp',
		baseParams		: {
								informacion		: 'situacion'  
								},
		totalProperty 	: 'total',
		autoLoad			: false,
		listeners		: {
                load: procesarSituacion,
								beforeload: NE.util.initMensajeCargaCombo,
								exception: NE.util.mostrarDataProxyError		
								}
	})
//------------------------------Fin Catalogo Situacion---------------------------------
	
//-------------------------------Elementos Forma--------------------------------
	var  elementosForma =  [
		{
		xtype				: 'combo',	
		name				: 'cmb_epo',	
		hiddenName		: '_cmb_if',	
		id					: 'cmbif',	
		fieldLabel		: 'Nombre del If', 
		mode				: 'local',
		editable			: true,
		displayField 	: 'descripcion',
		valueField 		: 'clave',
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,	
		store 			: catalogoIf		
		},{
		xtype				: 'compositefield',
		id					:'fr',
		combineErrors	: false,
		msgTarget		: 'side',
		items				:[{
							xtype			: 'datefield',
							fieldLabel	: 'Fecha de Operaci�n',
							name			: '_fechaReg',
							id				: 'fechaRegistro',
							hiddenName	: 'fecha_registro',
							allowBlank	: true,
							startDay		: 0,
							width			: 100,
							minValue		: '01/01/1901',
							msgTarget	: 'side',
							margins		: '0 20 0 0'
							},	{
							xtype			: 'displayfield',
							value			: 'al:',
							width			: 27
							},	{
							xtype			: 'datefield',
							name			: '_fechaFinReg',
							id				: 'fechaFinRegistro',
							hiddenName	: 'fecha_fin_reg',
							allowBlank	: true,
							startDay		: 0,
							width			: 100,
							minValue		: '01/01/1901',
							msgTarget	: 'side',
							margins		: '0 20 0 0'
							}]
		},{
		xtype				: 'combo',
		name				: 'cmbtd',
		hiddenName		: '_cmbtd',
		id					: 'cmbTipoDispersion',	
		fieldLabel		: 'Situaci�n', 
		mode				: 'local',
    editable			: true,
		displayField 	: 'descripcion',
    valueField 		: 'clave',
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,	
		store 			: catalogoSituacion,		
    anchor      : '65%'
		}]
//-----------------------------Fin Elementos Forma------------------------------

//--------------------------------Grid Consulta---------------------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store				:consultaData,
		id					:'gridConsulta',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Monitor de Resultado de Alta de Saldos',	
		clicksToEdit	:1,
		hidden			:true,
		columns			:[{
							header		:'Nombre IF',
							tooltip		:'Nombre IF',
							dataIndex	:'NOMBRE_IF',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'left'				
							}, {
							header		:'Usuario que envi�',
							tooltip		:'Usuario que envi�',
							dataIndex	:'TIPO_ENVIO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							}, {
							header		: 'Fecha y Hora <br> de Operaci�n',
							tooltip		: 'Fecha y Hora <br> de Operaci�n',
							dataIndex	: 'FECHA',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center'				
							},{
							header		:'Folio de la <br> Operaci�n',
							tooltip		:'Folio de la <br> Operaci�n',
							dataIndex	:'IC_FOLIO',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Situaci�n',
							tooltip		:'Situaci�n',
							dataIndex	:'SITUACION',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center'							
							},{
							header		:'Monto Enviado',
							tooltip		:'Monto Enviado',
							dataIndex	:'MONTO_ENVIADO',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}							
							},{
							header		:'N�mero de <br> operaciones aceptadas',
							tooltip		:'N�mero de <br> operaciones aceptadas',
							dataIndex	:'IN_REGISTROS_ACEP',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'				
							},{
							header		:'N�mero de <br> operaciones con errores',
							tooltip		:'N�mero de <br> operaciones con errores',
							dataIndex	:'IN_REGISTROS_RECH',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Total de <br> Operaciones',
							tooltip		:'Total de <br> Operaciones',
							dataIndex	:'IN_REGISTROS',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
              xtype		: 'actioncolumn',
              header	: 'Archivo Origen',
              tooltip	: 'Archivo Origen',
              dataIndex	:'IN_REGISTROS',
              width		: 85,
              align		: 'center',
              items		: [ {
                getClass: function(value,metadata,record,rowIndex,colIndex,store){
                      this.items[0].tooltip = 'Archivo Origen';
                      return 'iconoLupa';
                  },						
                  handler: function(grid, rowIndex, colIndex) {
                        var store = Ext.getCmp('gridConsulta').getStore();
                        var rec = store.getAt(rowIndex);
                        Ext.Ajax.request({
                        url: '29recuperacion_resultado01EXT.data.jsp',
                        params: {
                          informacion: 'TXT',
                          opcion: 'origen',
                          claveIf: rec.get('CLAVE_IF'),
                          folio  : rec.get('IC_FOLIO')
                        },
                        callback:descargaArchivo 
                      });
                    }
                }]
              },{
              xtype		: 'actioncolumn',
              header	: 'Resultados',
              tooltip	: 'Resultados',	
              dataIndex	:'IC_SITUACION',
              width		: 80,							
              align		: 'center',
              items		: [ {
                getClass: function(value,metadata,record,rowIndex,colIndex,store){
                    if (record.get('IC_SITUACION') == 4 || record.get('IC_SITUACION') == 5){
                      this.items[0].tooltip = 'Resultados';
                      return 'iconoLupa';
                    } else {
                        this.items[0].tooltip = '';
                        return '';
                        }
                  },						
                  handler: function(grid, rowIndex, colIndex) {
                        var store = Ext.getCmp('gridConsulta').getStore();
                        var rec = store.getAt(rowIndex);
                        Ext.Ajax.request({
                        url: '29recuperacion_resultado01EXT.data.jsp',
                        params: {
                          informacion: 'PDF',
                          start: 0,
                          limit: 15,
                          claveIf: rec.get('CLAVE_IF'),
                          folio  : rec.get('IC_FOLIO')
                        },
                        callback:descargaArchivo 
                      });
                    }
                }]
              },{
              xtype		: 'actioncolumn',
              header	: 'Archivo de Errores',
              tooltip	: 'Archivo de Errores',	
              dataIndex	:'IN_REGISTROS_RECH',
              width		: 100,							
              align		: 'center',
              items		: [ {
                getClass: function(value,metadata,record,rowIndex,colIndex,store){
                    if (record.get('IN_REGISTROS_RECH') > 0 && ( record.get('IC_SITUACION') == 4 || record.get('IC_SITUACION') == 5) ){
                      this.items[0].tooltip = 'Archivo de Errores';
                      return 'iconoLupa';
                    } else {
                        this.items[0].tooltip = '';
                        return '';
                        }
                  },						
                  handler: function(grid, rowIndex, colIndex) {
                        var store = Ext.getCmp('gridConsulta').getStore();
                        var rec = store.getAt(rowIndex);
                        Ext.Ajax.request({
                        url: '29recuperacion_resultado01EXT.data.jsp',
                        params: {
                          informacion: 'TXT',
                          opcion: 'errores',
                          claveIf: rec.get('CLAVE_IF'),
                          folio  : rec.get('IC_FOLIO')
                        },
                        callback:descargaArchivo 
                      });
                    }
                }]
              }],	
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 350,
    width				: 800,
		align				: 'center',
		frame				: true/*,
		bbar				: {
							store			: consultaData,
							emptyMsg		: "No hay registros.",
							items			: [
											'->','-',
											{
											xtype			: 'button',
											text			: 'Generar Archivo',					
											tooltip		:	'Generar Archivo ',
											iconCls		: 'icoXls',
											id				: 'btnGenerarCSV',
											handler		: function(boton, evento) {
                           			fp.el.mask('Generando archivo...', 'x-mask-loading');
																boton.setIconClass('loading-indicator');
																Ext.getCmp('btnGenerarCSV').disable();
																Ext.Ajax.request({
																	url: '29monitor01EXT.data.jsp',
																	params: Ext.apply(fp.getForm().getValues(),{
																				informacion: 'ArchivoCSV'
																				}),																						
																callback: descargaArchivo
																})
															}
											}]
								}*/
	})
//-----------------------------Fin Grid Consulta--------------------------------
		
//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id					:'formcon',
		width				:650,
		heigth			:'auto',
		//title				:'Monitor de Resultado de Alta de Saldos',
		layout			:'form',
		frame				:true,
		collapsible		:true,
		titleCollapse	:false,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 5px',
		labelWidth		:150,	
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosForma
							],
		buttons			:[{
							text				:'Consultar',
							id					:'btnConsultar',
							iconCls			:'icoBuscar',
							handler			:function(boton, evento){	                            
							var fechaMinReg = Ext.getCmp('fechaRegistro');
							var fechaMaxReg = Ext.getCmp('fechaFinRegistro');
							if (!Ext.isEmpty(fechaMinReg.getValue()) || !Ext.isEmpty(fechaMaxReg.getValue()) ) {
									if(Ext.isEmpty(fechaMinReg.getValue()))	{
										fechaMinReg.markInvalid('Debe capturar ambas fechas de Operaci�n o dejarlas en blanco.');
										fechaMinReg.focus();                    
										return;
									}else if (Ext.isEmpty(fechaMaxReg.getValue())){
										fechaMaxReg.markInvalid('Debe capturar ambas fechas de Registro o dejarlas en blanco.');
										fechaMaxReg.focus();
										return;
									}
								}
							if(Ext.getCmp('fechaRegistro').isValid() & Ext.getCmp('fechaFinRegistro').isValid()){
							} else { 
								return ;
							}	
							fp.el.mask('Cargando...', 'x-mask-loading');	
							consultaData.load({
							params		:Ext.apply(fp.getForm().getValues(),{
							informacion	:'consultaGeneral'
											})
									}); 	
								}//fin handler 
							},{
							text				:'Limpiar',
							id					:'btnLimpiar',
							iconCls			:'icoLimpiar',
							handler			:function(boton, evento){
              window.location  = "/nafin/29garantias/29nafin/29recuperacion_resultado01EXT.jsp";
												}
							}]
	})
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		: 900,
		height	: 600,
		style		:'margin:0 auto;',
		items		:[
					NE.util.getEspaciador(20),
					fp,
					NE.util.getEspaciador(20),
					gridConsulta,
					NE.util.getEspaciador(20)
					]
	})
//-----------------------------Fin Contenedor Principal-------------------------
//Ext.getCmp('cmbTipoDispersion').setValue("");
catalogoIf.load();
catalogoSituacion.load();
Ext.Ajax.request({
			url: '29recuperacion_resultado01EXT.data.jsp',
			params: {
				informacion: 'cargaFecha'
			},
			callback:procesarFecha 
		});
});//-----------------------------------------------Fin Ext.onReady(function(){}