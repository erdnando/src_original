//29ConsDesMasivo01Ext.js 

Ext.onReady(function() {

//Ext.onReady(function(){
var programa = "Test ";//(document.getElementById("nombrePrograma").value);
//});
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = store.reader.jsonData;
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			if(store.getTotalCount() > 0) {//////////Verifica que existan registros
				//Ext.getCmp('btnArchivoPDF').enable();
				//Ext.getCmp('btnArchivoCSV').enable();
				el.unmask();					
			} else {	
				//Ext.getCmp('btnArchivoPDF').disable();
				//Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var procesaVerDetalle = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var folio = Ext.util.JSON.decode(response.responseText).IC_FOLIO;
				ventanaDetalle.setVisible(true);
				ventanaDetalle.setTitle('Folio de la Operaci�n : '+folio);
				gridDetalle.setVisible(true);
				storeDetalle.loadData(Ext.util.JSON.decode(response.responseText).registros);
				if(storeDetalle.getTotalCount()>0){
				}else{
				
				}
				
				
		}else{
				Ext.Msg.alert("Mensaje de p�gina web.",'Error al eliminar la Afianzadora' );
		}
	}
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------

	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '29altagarant01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'CLAVE_IF'},
			{	name: 'NOMBRE_IF'},	
			{ name :'NOMBRE_PORTAFOLIO'},
			{ name :'PROGRAMA'},
			{ name :'TIPO_ENVIO'},
			{	name: 'FECHA'},
			{ name :'IC_FOLIO'},
			{	name: 'SITUACION'},
			{	name: 'IN_REGISTROS_ACEP'},
			{	name: 'IN_REGISTROS_RECH'},
			{	name: 'IN_REGISTROS'},
			{	name: 'IC_SITUACION'},
			{	name: 'MONTO_ENVIADO'},
			{ name :'REPROCESO'},
			{ name :'DETALLE'},
			{ name :'IC_SITUACION'},
			{ name :'DF_FECHA_HORA'}
			

		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	var catalogoIfData = new Ext.data.JsonStore({
		id: 'catalogoIfData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29altagarant01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	/*var catalogoSituacionData = new Ext.data.JsonStore({
		id: 'catalogoSituacionData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29altagarant01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaSituacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});*/
	var catalogoSituacionData = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],
		data : [
			['5','CONFIRMACI�N'],
			['1','ENV�O PARA VALIDACI�N Y CARGA'],
			['6','PARA CORRECCI�N'],
			['2','VALIDACI�N']
		]
	});
	
	var storeDetalle   = new Ext.data.JsonStore({ 
		fields: [	
			{name: 'CLAVE_IF'},
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_PORTAFOLIO'},
			{name: 'PROGRAMA'},
			{name: 'IN_REGISTROS_ACEP'},
			{name: 'IN_REGISTROS_RECH'},
			{name: 'IN_REGISTROS'}
		]		
	});
//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
	var programa = new Ext.Container ({
		layout: 'table',
		id: 'labelPrograma',
		width: '481',
		heigth: 'auto',
		style: 'margin: 0 auto',
		align: 'center',
		defaults:{
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
			{
				xtype: 'displayfield',
				///<b>'+programa+'</b></div><br><br>
				value: '<div align=center style="width:481px;"><div align=center> <font size="3"><b>Alta de Garant�as - Consulta de Resultados</b></font></div>'
			}
		]
	});
	
	
	var elementosForma=[
		{
			xtype:'combo',
			id:	'cboNombreIF',
			name:	'nombreIF',
			fieldLabel: 'Nombre del IF',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'nombreIF',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			width: 240,
			minChars : 1,
			allowBlank: true,
			store : catalogoIfData,
			tpl:'<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}</div>'+
				'</tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' + '</div></tpl></tpl>',
				setNewEmptyText: function(emptyTextMsg){
					this.emptyText = emptyTextMsg;
					this.setValue('');
					this.applyEmptyText();
					this.clearInvalid();
				}
    
		},   
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Operaci�n',
			combineErrors: false,
			
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaOperacionMin',
					id: 'txtFechaOperacionMin',
					allowBlank: true,
					startDay: 0,
					vtype: 'rangofecha',
					width: 90,
					value : new Date(),
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaOperacionMax',
					margins: '0 20 0 0',
					regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
					regex     : /.*[1-9][0-9]{3}$/
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 5,margins:{top:0, right:10, bottom:0, left:-15}
					
				},
				{
					xtype: 'datefield',
					name: 'fechaOperacionMax',
					id: 'txtFechaOperacionMax',
					allowBlank: true,
					startDay: 1,
					vtype: 'rangofecha',
					width: 90,
					value : new Date(),
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaOperacionMin',
					margins: '0 20 0 0',
					regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
					regex     : /.*[1-9][0-9]{3}$/
				}
			]
		},
		{
			xtype:'combo',
			id:	'cboSutiacion',
			name:	'situacion',
			fieldLabel: 'Situaci�n',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'situacion',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			width: 240,
			minChars : 1,
			allowBlank: true,
			store : catalogoSituacionData//,
			//tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype:'hidden',
			id:	'cboNombreIF_Res',
			name:	'cboNombreIF_Res'
		},
		{
			xtype:'datefield',
			id:	'fechaMin_Res',
			hidden:true,
			name:	'fechaMin_Res',
			allowBlank: true,
			startDay: 0,
			vtype: 'rangofecha',
			width: 90,
			value : new Date(),
			msgTarget: 'side',
			vtype: 'rangofecha'
		},
		{
			xtype:'datefield',
			id:	'fechaFin_Res',
			hidden:true,
			name:	'fechaFin_Res',
			allowBlank: true,
			startDay: 0,
			vtype: 'rangofecha',
			width: 90,
			value : new Date(),
			msgTarget: 'side',
			vtype: 'rangofecha'
			
		},
		{
			xtype:'hidden',
			id:	'cboSut_Res',
			name:	'cboSut_Res'
		}
	];
	
	var procesarVerDetalle = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		var IC_FOLIO = registro.get('IC_FOLIO');
		var CLAVE_IF =  Ext.getCmp('cboNombreIF_Res').getValue();
		var fechaMin =  Ext.getCmp('fechaMin_Res').getValue();
		var fechaFin = Ext.getCmp('fechaFin_Res').getValue();
		var cboSut = Ext.getCmp('cboSut_Res').getValue();
		Ext.Ajax.request({
				url: '29altagarant01Ext.data.jsp',
				params: Ext.apply(
					{
						informacion:'obtenerDetalle',
						IC_SITUACION:cboSut,
						CLAVE_IF:CLAVE_IF,
						IC_FOLIO:IC_FOLIO,
						fechaMin:Ext.util.Format.date(fechaMin,'d/m/Y'),
						fechaFin:Ext.util.Format.date(fechaFin,'d/m/Y')
					}
				),
				callback: procesaVerDetalle
		});
	}
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 450,
		title: 'Alta de Garant�as - Consulta de Resultados',
		frame: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 160,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		titleCollapse: false,
		items: elementosForma,			
		monitorValid: true,
		buttons: [
		{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,	
				handler: function(boton, evento) {
					var cboIF = Ext.getCmp('cboNombreIF');
					var cboSut = Ext.getCmp('cboSutiacion');
					var fechaCargoDE = Ext.getCmp('txtFechaOperacionMin');
					var fechaCargoA = Ext.getCmp('txtFechaOperacionMax');
					if(!Ext.isEmpty(fechaCargoDE.getValue()) || !Ext.isEmpty(fechaCargoA.getValue())){
						if(Ext.isEmpty(fechaCargoDE.getValue())){
							fechaCargoDE.markInvalid('Por favor escribe la fecha incial.(dd/mm/aaaa)');
							fechaCargoDE.focus();
							return;
						}      
						if(Ext.isEmpty(fechaCargoA.getValue())){
							fechaCargoA.markInvalid('Por favor escribe la fecha final.(dd/mm/aaaa)');
							fechaCargoA.focus();
							return;
						}
					}
					Ext.getCmp('cboNombreIF_Res').setValue(cboIF.getValue());
					Ext.getCmp('fechaMin_Res').setValue(fechaCargoDE.getValue());
					Ext.getCmp('fechaFin_Res').setValue(fechaCargoA.getValue());
					Ext.getCmp('cboSut_Res').setValue(cboSut.getValue());
					fp.el.mask('Procesando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion : 'Consultar',
							operacion : 'Generar',
							start:0,
							limit :15
						})
					}); 
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					//window.location = '29Consulta05RGDAdmGExt.jsp'; 
					Ext.getCmp('forma').getForm().reset();
					//fp.getForm().reset();
					Ext.getCmp('gridConsulta').hide();
				}

			}
		]
	});
	
	//grid de resultados de la consulta
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		//title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 115,			
				resizable: true,		
				align: 'left'/*,//Return una tabla con los valores
				renderer:function(value){
					return value.replace('[','');
				}		*/	
			},
			{
				header: 'Tipo de Env�o',
				tooltip: 'Tipo de Env�o',
				dataIndex: 'TIPO_ENVIO',
				sortable: true,
				width: 80,			
				resizable: true,				
				align: 'center',//Return una tabla con los valores
				renderer:function(value){
							value =(value=='proc_aut')?"AUTOMATICO":"MANUAL";
					return value;
				}		
			},{
				header: 'Usuario<br>que Envio',
				tooltip: 'Usuario que Envio',
				dataIndex: 'TIPO_ENVIO',
				sortable: true,
				width: 80,			
				resizable: true,				
				align: 'center',//Return una tabla con los valores
				renderer:function(value){
					return value;
				}			
			},
			{
				header: 'Fecha y Hora<br>de Operaci�n',
				tooltip: 'Fecha y Hora de Operaci�n',
				dataIndex: 'FECHA',
				sortable: true,
				width: 115,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Folio de la <br>Operaci�n',
				tooltip: 'Folio de la <br>Operaci�n',
				dataIndex: 'IC_FOLIO',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return value;
				}
			},
			{
				header: 'Situaci�n',
				tooltip: 'Situaci�n',
				dataIndex: 'SITUACION',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return value;
				}
			},
			{
				header: 'Monto Enviado',
				tooltip: 'Monto Enviado',
				dataIndex: 'MONTO_ENVIADO',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'right',
				renderer:function(value){
					return '<div align=rigth >'+Ext.util.Format.number(value,'$0,0.00')+'</div>';
				}
			}	,
			
			{
				header: 'N�mero total<br>de operaciones<BR>aceptadas',
				tooltip: 'N�mero de operaciones aceptadas',
				dataIndex: 'IN_REGISTROS_ACEP',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
						value = (value!='')?value:0;
						return Ext.util.Format.number(value, "0");
					
					
				}
			}	,
			{
				header: 'N�mero total<br>de operaciones<BR>con errores',
				tooltip: 'N�mero de operaciones con errores',
				dataIndex: 'IN_REGISTROS_RECH',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
						value = (value!='')?value:0;
						return Ext.util.Format.number(value, "0");
					
					
				}
			}	,
			{
				header: 'Total de<br>Operaciones',
				tooltip: 'Total de Operaciones',
				dataIndex: 'IN_REGISTROS',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
						value = (value!='')?value:0;
						return Ext.util.Format.number(value, "0");
					
					
				}
			},
			{
				header: 'Detalle',
				tooltip: 'Detalle',
				xtype: 'actioncolumn',
				sortable: true,
				width: 114,
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							var reg_acep = parseInt((registro.get('IN_REGISTROS_ACEP')=='')?'0':registro.get('IN_REGISTROS_ACEP'));
							var reg_rech = parseInt((registro.get('IN_REGISTROS_RECH')=='')?'0':registro.get('IN_REGISTROS_RECH'));
							if(reg_acep >0 || reg_rech >0 ){
								 return 'iconoLupa';	
							}
						},handler: procesarVerDetalle
					}
				],
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Archivo<br>Origen',
				tooltip: 'Archivo Origen',
				xtype: 'actioncolumn',
				sortable: true,
				width: 114,
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								return 'iconoLupa';	
						},	handler: function(grid, rowIndex, colIndex, item, event){
							var reg= grid.getStore().getAt(rowIndex);
							var informacion ='Archivos';
							var opcion ='origen';
							var folio = reg.get('IC_FOLIO');
							var clave_if = reg.get('CLAVE_IF');
								Ext.Ajax.request({
								//var strURL = "29consarchivo.jsp?opcion="+opcion+"&folio="+folio+"&clave_if="+clave_if;
									url: '29consarchivo01Ext.jsp',
									params: Ext.apply(fp.getForm().getValues(),{							
										informacion: 'Archivos',
										opcion:opcion,
										folio:folio,
										clave_if:clave_if
									}),
									success : function(response) {
										//boton.setIconClass('icoXls');     
										//boton.setDisabled(false);
									},	
									callback: procesarDescargaArchivos
								});	
							//Ext.Msg.alert('','Descargar Archivo Origen.txt '+opcion+' _ '+folio+' _ '+claveIF);
						}
					}
				],
				resizable: true,				
				align: 'center'
			}	,
			{
				header: 'Resultados',
				tooltip: 'Resultados',
				xtype: 'actioncolumn',
				dataIndex:'IC_SITUACION',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('IC_SITUACION') !=5 ){
						 return '';
					}
				},
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( registro.get('IC_SITUACION') ==5   ){
								//this.items[0].tooltip = 'Autorizar';
								return 'iconoLupa';	
							}
						},	handler: function(grid, rowIndex, colIndex, item, event){
							var reg= grid.getStore().getAt(rowIndex);
							var informacion ='Archivospdf';
							var origen ='CONSALTA';
							var folio = reg.get('IC_FOLIO');
							var claveIF = reg.get('CLAVE_IF');
							grid.el.mask('Procesando...', 'x-mask-loading');
							var archivo = '';
							Ext.Ajax.request({
								url : '29consarchivopdf01Ext.jsp',
								params: Ext.apply(fp.getForm().getValues(),{							
									informacion: 'Archivospdf',
									origen:origen,
									folio:folio,
									clave_if:claveIF
								}),
								success : function(response) {
									grid.el.unmask();
								},	
								callback: procesarDescargaArchivos
							});	
						}
					}
				]
			}	,
			{
				header: 'Archivo<br>de<br> Errores',
				tooltip: 'Archivo de Errores',
				dataIndex: 'IN_REGISTROS_RECH',
				xtype: 'actioncolumn',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					
					if(registro.get('IN_REGISTROS_RECH') ==0){
						 return '';
					}
				},
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( registro.get('IN_REGISTROS_RECH') !=0 &&  registro.get('IC_SITUACION')==5 ){
								//this.items[0].tooltip = 'Autorizar';
								return 'iconoLupa';	
							}
						},	handler: function(grid, rowIndex, colIndex, item, event){
							var reg= grid.getStore().getAt(rowIndex);
							var informacion ='Archivos';
							var opcion ='errores';
							var folio = reg.get('IC_FOLIO');
							var claveIF = reg.get('CLAVE_IF');
								Ext.Ajax.request({
									//url: '29ConsDesMasivo01Ext.data.jsp',
									url: '29consarchivo01Ext.jsp',
									params: Ext.apply(fp.getForm().getValues(),{							
										informacion: 'Archivos',
										opcion:opcion,
										folio:folio,
										clave_if:claveIF
									}),
									success : function(response) {
										//boton.setIconClass('icoXls');     
										//boton.setDisabled(false);
									},	
									callback: procesarDescargaArchivos
								});	
						}
					}
				]
			},	
			{
				header: 'Reprocesar<br>Errores',
				tooltip: 'Reprocesar Errores',
				dataIndex: 'IN_REGISTROS_RECH',
				xtype: 'actioncolumn',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer:function(value, metadata, registro, rowindex, colindex, store){
					if((registro.get('IN_REGISTROS_RECH')==null) && registro.get('REPROCESO')!="" ){
						return '';
					}
					
				},
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if((registro.get('IN_REGISTROS_RECH')!="" && registro.get('IN_REGISTROS_RECH')!="0") && registro.get('REPROCESO')=="" ){
								this.items[0].tooltip = 'Reprocesar';
								return 'iconoLupa';	
							}
						},	handler: function(grid, rowIndex, colIndex, item, event){
							var reg= grid.getStore().getAt(rowIndex);
							var informacion ='Archivos';
							var opcion ='errores';
							var v_folio = reg.get('IC_FOLIO');
							var v_if = reg.get('CLAVE_IF');
							var pantallaOrigen = '../../29nafin/29altagarant01Ext.jsp';
								window.location='../29pki/29nafin/29altagarantb01Ext.jsp?v_folio='+v_folio+'&v_if='+v_if+'&pantallaOrigen='+pantallaOrigen;
						}
					}
				]
			}							
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: true/*,
		bbar: ['->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',
					hidden: false,
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '29ConsDesMasivo01Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoCSV'
							}),
							success : function(response) {
								boton.setIconClass('icoXls');     
								boton.setDisabled(false);
							},	
							callback: procesarDescargaArchivos
						});						
					}
				},
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '29ConsDesMasivo01Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoPDF'
							}),
							success : function(response) {
								boton.setIconClass('icoPdf');     
								boton.setDisabled(false);
							},
							callback: procesarDescargaArchivos
						});						
					}
				}
		]*/
	});
	var gridDetalle = new Ext.grid.GridPanel({
		store: storeDetalle,
		id: 'gridDetalle',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: false,
		title: ' ',
		columns: [	
			
			{
				header: 'Portafolio',
				tooltip: 'Portafolio',
				sortable: true,
				dataIndex: 'NOMBRE_PORTAFOLIO',
				width: 100,
				align: 'center'
			},
			{
				header: 'Programa',
				tooltip: 'Programa',
				sortable: true,
				dataIndex: 'PROGRAMA',
				width: 100,
				align: 'center'
			},
			{
				header: 'N�mero de<br>operaciones<br>aceptadas',
				tooltip: 'N�mero de operaciones aceptadas',
				sortable: true,
				dataIndex: 'IN_REGISTROS_ACEP',
				width: 100,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
						value = (value!='')?value:0;
						return Ext.util.Format.number(value, "0");
					
				}
			},
			{
				header: 'N�mero de<br>operaciones<br>con errores',
				tooltip: 'N�mero de operaciones con errores',
				sortable: true,
				dataIndex: 'IN_REGISTROS_RECH',
				width: 100,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					value = (value!='')?value:0;
					return Ext.util.Format.number(value, "0");
					
				}
			},
			{
				header: 'Total<br>de<br>Operaciones',
				tooltip: 'Total de Operaciones',
				sortable: true,
				dataIndex: 'IN_REGISTROS',
				width: 100,
				align: 'center'
			}
		],	
     	stripeRows: true,
		loadMask: true,
		height: 250,
		width: 535,		
		frame: true
	});
	var ventanaDetalle = new Ext.Window({
			width: 545,
			height: 'auto',
			//title:'Detalle',
			modal: true,
			closable: true,
			closeAction: 'hide',
			resizable: false,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			items: [gridDetalle]
			
});
	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 947,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			programa,
			NE.util.getEspaciador(10),
			fp,		
			NE.util.getEspaciador(10),
			gridConsulta,
			NE.util.getEspaciador(20)
			
		]
	});
	//catalogoSituacionData.load();
	catalogoIfData.load();

});	
