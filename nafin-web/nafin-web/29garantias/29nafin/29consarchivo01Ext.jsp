<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*, 
				netropology.utilerias.*, 
				com.netro.afiliacion.*,
				com.netro.model.catalogos.*,
				com.netro.garantias.*,
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/29garantias/29secsession.jspf"%>
<%
String clave_if = request.getParameter("clave_if");
String folio = request.getParameter("folio");
String opcion = request.getParameter("opcion");

JSONObject jsonObj = new JSONObject();
String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";

AccesoDB con = new AccesoDB();
PreparedStatement	ps = null;
ResultSet rs = null;
String qrySentencia	= "";
String tabla = "";
int vigencia = 0;
int i = 0;

CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer("");
String nombreArchivo = null;

try {
	con.conexionDB();
	if(opcion.equals("origen")){
    tabla = "gti_contenido_arch";
  }else{
    tabla = "gti_arch_rechazo";
  }
  
	qrySentencia =
		"select cg_contenido"+
		" from "+tabla+" A"+
		" ,comcat_if I"+
		" where A.ic_if_siag = I.ic_if_siag"+
		" and A.ic_folio = ?"+
		" and I.ic_if = ?"+
		" order by A.ic_linea";

	ps = con.queryPrecompilado(qrySentencia);
  ps.setString(1, (folio));
	ps.setString(2, (clave_if));
	
	rs = ps.executeQuery();
	while(rs.next()){
		contenidoArchivo.append(rs.getString("cg_contenido")+"\n");
		i++;
	}
	ps.close();
	if(i>0){
		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt")){
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("msg", "Error al generar el archivo");
			infoRegresar = jsonObj.toString();	
		}else{
			nombreArchivo = archivo.nombre;
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			
			infoRegresar = jsonObj.toString();	
			
		}
	}else{
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("msg","No hay registros");
		infoRegresar = jsonObj.toString();	
	}
	
	
}catch(Exception e){
	e.printStackTrace();
	throw e;
}finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
}

%>

<%=  infoRegresar%>