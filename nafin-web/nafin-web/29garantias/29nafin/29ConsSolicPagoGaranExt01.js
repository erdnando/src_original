Ext.onReady(function() {


//-----------------------------HANDLER�S-----------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = store.reader.jsonData;
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			if(store.getTotalCount() > 0) {
				el.unmask();					
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			Ext.getCmp('VerImagenes').hide();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarVerImagen= function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var folio = registro.get('FOLIO');
		consDataImagen.load({
					params: Ext.apply(fp.getForm().getValues(),{
					 informacion: 'ventanaImagen',
					 folio :folio
					})
				});
		new Ext.Window({					
					modal: true,
					width: 300,
					height: 200,
					modal: true,					
					closeAction: 'hide',
					resizable: true,
					constrain: true,
					closable:false,
					id: 'VerImagenes',
					items: [					
						gridImagen					
					],
					bbar: {
						xtype: 'toolbar',	buttonAlign:'center',	
						buttons: ['-',
							{	xtype: 'button',	text: 'Regresar',	iconCls: 'icoRegresar', 	id: 'btnRegresar',
								handler: function(){																		
									Ext.getCmp('VerImagenes').hide();
								} 
							}	
						]
					}
				}).show();
	}
		
	var procesaImagen = function(grid, rowIndex, colIndex, item, event) { 
		var registro = grid.getStore().getAt(rowIndex);
		var claveImagen = registro.get('IC_IMAGEN_SOLPAGO');
		Ext.Ajax.request({
			url: '29ConsSolicPagoGaranExt01.data.jsp',
			params: {
				informacion: 'archivoCargado',		
				claveImagen:claveImagen
			},
			callback:procesarDescargaArchivos 
		});
	}

	var procesarConImagenData = function(store,arrRegistros,opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros != null){			
			var gridImagen = Ext.getCmp('gridImagen');
			if (!gridImagen.isVisible()) {
				gridImagen.show();
			}	
			var el = gridImagen.getGridEl();			
			
			if(store.getTotalCount()>0){				
			}else{
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}else{
			Ext.getCmp('VerImagenes').hide();
		}
	}
//-------------------------------STORE-----------------------------------
	var consDataImagen = new Ext.data.JsonStore({
		root : 'registros',
		url : '29ConsSolicPagoGaranExt01.data.jsp',
		baseParams: {
			informacion: 'ventanaImagen'			
		},		
		fields: [	
			{	name: 'IC_IMAGEN_SOLPAGO'},
			{	name: 'CG_DESCRIPCION'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConImagenData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConImagenData(null, null, null);					
				}
			}
		}					
	});
	var catalogoIfData = new Ext.data.JsonStore({
		id: 'catalogoIfData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29ConsSolicPagoGaranExt01.data.jsp',
		baseParams: {
			informacion: 'ConsultaIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoSituacionData = new Ext.data.JsonStore({
		id: 'catalogoSituacionData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29ConsSolicPagoGaranExt01.data.jsp',
		baseParams: {
			informacion: 'ConsultaSituacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '29ConsSolicPagoGaranExt01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'IC_IF_SIAG'},
			{	name: 'NOMBRE_IF'},	
			{ name :'FECHA'},
			{ name :'FOLIO'},
			{ name :'SITUACION'},
			{	name: 'IC_SITUACION'},
			{ name :'IC_CONTRAGARANTE'},
			{	name: 'CC_GARANTIA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});

//-------------------------------COMPONENTES---------------------------------	
	
	var elementosForma =  [
		{
			xtype:'combo',
			id:	'cboNombreIF',
			name:	'nombreIF',
			fieldLabel: 'Nombre del IF',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'nombreIF',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			width: 240,
			minChars : 1,
			allowBlank: true,
			store : catalogoIfData,
			tpl:'<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}</div>'+
				'</tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' + '</div></tpl></tpl>',
				setNewEmptyText: function(emptyTextMsg){
					this.emptyText = emptyTextMsg;
					this.setValue('');
					this.applyEmptyText();
					this.clearInvalid();
				}
    
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaOperacionMin',
					id: 'txtFechaOperacionMin',
					allowBlank: true,
					startDay: 0,
					vtype: 'rangofecha',
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaOperacionMax',
					margins: '0 20 0 0',
					regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
					regex     : /.*[1-9][0-9]{3}$/
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 5,margins:{top:0, right:10, bottom:0, left:-15}
					
				},
				{
					xtype: 'datefield',
					name: 'fechaOperacionMax',
					id: 'txtFechaOperacionMax',
					allowBlank: true,
					startDay: 1,
					vtype: 'rangofecha',
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaOperacionMin',
					margins: '0 20 0 0',
					regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
					regex     : /.*[1-9][0-9]{3}$/
				},
				{
					xtype: 'displayfield',
					value: '(dd/mm/yyyy)'
					
				}
			]
		},
		{
			xtype:'combo',
			id:	'cboSutiacion',
			name:	'situacion',
			fieldLabel: 'Situaci�n',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'situacion',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			width: 240,
			minChars : 1,
			allowBlank: true,
			store : catalogoSituacionData,
			tpl:'<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}</div>'+
				'</tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' + '</div></tpl></tpl>',
				setNewEmptyText: function(emptyTextMsg){
					this.emptyText = emptyTextMsg;
					this.setValue('');
					this.applyEmptyText();
					this.clearInvalid();
				}
    
		},
		{
			xtype	    : 'textfield',
			id        : 'txfFolioOperacion',
			name		 : 'folioOperacion',
			hiddenName: 'folioOperacion',
			fieldLabel: 'Folio de Operaci�n',
			anchor    : '76%',
			msg       : 'side',
			hidden    : false,
			margins	 : '0 20 0 0',
			maxLength: 30,
			maskRe:/[0-9]/
		}
	];
	//grid de resultados de la consulta
	var gridImagen = {
		xtype: 'grid',
		store: consDataImagen,
		id: 'gridImagen',		
		hidden: false,
		title: '<center>Im�genes Cargadas</center>',	
		columns: [	
			{
				xtype:	'actioncolumn',
				dataIndex: 'IC_IMAGEN_SOLPAGO',
				align: 'center',
				width: 50,
				id:'descImagen',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return '    iconoLupa';					
						},
						handler:	procesaImagen  
					}
					
				]
			},
			{
				xtype:	'actioncolumn',
				dataIndex: 'CG_DESCRIPCION',
				align: 'center',
				width: 230,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
							return (record.get('CG_DESCRIPCION'));
					}
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 190,
		width: 290,		
		frame: true
	}
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta', 
		margins: '20 0 0 0',	
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		hidden: true,
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: true,
		columns: [	
			{
				header: '<center>Nombre IF</center>',
				tooltip: 'Nombre IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'
			},
			{
				header: 'Fecha y Hora<br>de Operaci�n',
				tooltip: 'Fecha y Hora de Operaci�n',
				dataIndex: 'FECHA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Folio de la <br>Operaci�n',
				tooltip: 'Folio de la <br>Operaci�n',
				dataIndex: 'FOLIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return value;
				}
			},
			{
				header: 'Situaci�n<br>',
				tooltip: 'Situaci�n',
				dataIndex: 'SITUACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return value;
				}
			},
			
			{
				header: 'Solicitud de Pago<br>de Garantias',
				tooltip: 'Solicitud de Pago<br>de Garantias',
				xtype: 'actioncolumn',
				sortable: true,
				width: 100,
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								
								return 'iconoLupa';	
						},	handler: function(grid, rowIndex, colIndex, item, event){
							var reg= grid.getStore().getAt(rowIndex);
							var folio = reg.get('FOLIO');
								Ext.Ajax.request({
									url: '29ConsSolicPagoGaranExt01.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{							
										informacion: 'ArchivoPdfSolPagoGaran',
										folio:folio
									}),
									callback: procesarDescargaArchivos
								});	
						}
					}
				],
				resizable: true,				
				align: 'center'
			}	,
			{
				header: 'Archivo de<br>Resultados',
				tooltip: 'Archivo de<br>Resultados',
				xtype: 'actioncolumn',
				dataIndex:'IC_SITUACION',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( registro.get('IC_SITUACION') ==5 || registro.get('IC_SITUACION') ==8 ){
									return 'iconoLupa';	
							   }
						},	
						handler: function(grid, rowIndex, colIndex, item, event){
							var reg= grid.getStore().getAt(rowIndex);
							var folio = reg.get('FOLIO');
							var ic_if_siag = reg.get('IC_IF_SIAG');
							var cc_garantias = reg.get('CC_GARANTIA');
							var ic_contragarante = reg.get('IC_CONTRAGARANTE');
							Ext.Ajax.request({
								url: '29ConsSolicPagoGaranExt01.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{							
									informacion: 'PdfResultados',
									folio:folio,
									ic_if_siag : ic_if_siag,
									cc_garantias : cc_garantias,
									ic_contragarante : ic_contragarante
								}),
								callback: procesarDescargaArchivos
							});	
						}
					}
				]
			}	,
			{
				header: 'Documentos Anexo<br>a la Solicitud',
				tooltip: 'Documentos Anexo<br>a la Solicitud',
				xtype: 'actioncolumn',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								return 'iconoLupa';	
						},	handler:procesarVerImagen
					}
				]
			}
		]
		
		
	});

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: '<center>Solicitud de Pago de Garant�a - Consulta de Resultados</center>',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,	
		buttons: [
			{
				text : 'Consultar',
				id   : 'btnConsultar',
				iconCls :'icoBuscar',
				formBind: true,	
				handler: function (boton, evento){
					var fechaCargoDE = Ext.getCmp('txtFechaOperacionMin');
					var fechaCargoA = Ext.getCmp('txtFechaOperacionMax');
					if(!Ext.isEmpty(fechaCargoDE.getValue()) || !Ext.isEmpty(fechaCargoA.getValue())){
						if(Ext.isEmpty(fechaCargoDE.getValue())){
							fechaCargoDE.markInvalid('Por favor escribe la fecha incial.(dd/mm/aaaa)');
							fechaCargoDE.focus();
							return;
						}      
						if(Ext.isEmpty(fechaCargoA.getValue())){
							fechaCargoA.markInvalid('Por favor escribe la fecha final.(dd/mm/aaaa)');
							fechaCargoA.focus();
							return;
						}
					}
					fp.el.mask('Procesando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion : 'Consultar',
							operacion : 'Generar',
							start:0,
							limit :15
						})
					}); 
				}
			},
			{
				text : 'Limpiar',
				id   : 'btnLimpiar',
				iconCls :'icoLimpiar',
				handler: function (boton, evento){
					Ext.getCmp('forma').getForm().reset();
					Ext.getCmp('gridConsulta').hide();
				
				}
			}
		
		]
		
	});

//----------------------------CONTENEDOR PRINCIPAL------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,	
	//	height: 'auto',
		//style: 'margin:0 auto;',
		align: 'center',
		items: [
			//NE.util.getEspaciador(20),
			//programa,
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta
		]
	});	
	catalogoSituacionData.load();
	catalogoIfData.load();   
});