Ext.onReady(function() {


	var procesarSuccessConsCamposParam = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			if(resp.numeroRegistros=='0'){
				Ext.Msg.confirm('Confirmaci�n', '�El IF y Base de Operaci�n seleccionados no tiene parametrizados los campos para el Alta Autom�tica de Garant�as. �Desea llevar a cabo la parametrizaci�n?', function(btn){
						if(btn=='yes'){
							Ext.getCmp('btnGuardarParams').enable();
							Ext.getCmp('btnLimpiar').show();
							fpParams.getForm().reset();
							fpParams.show();							
							
						}
					});
			}else{
				Ext.getCmp('btnGuardarParams').enable();
				Ext.getCmp('btnLimpiar').show();
				fpParams.getForm().setValues(resp.objParams);
				fpParams.show();
				
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessGuardarCamposParam = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert('Aviso','La parametrizaci�n de los campos se guardo exitosamente!', function(){
					//pnl.el.unmask();
					window.location.href='29camposparam_ext.jsp';
				}
			);

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var guardarParams = function(){
		var formObj = fpParams.getForm();
		if(formObj.isValid()){
			pnl.el.mask('Guardando parametrizaci�n...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '29camposparam_ext.data.jsp',
				params: Ext.apply(fpCriteriosBusq.getForm().getValues(), fpParams.getForm().getValues(),{
					informacion: 'guardarCamposParam'
				}),
				callback: procesarSuccessGuardarCamposParam
			});
		}
	}
	
	var procesarSuccessGenerarArhivoCsv = function(opts, success, response) {
		var btnGenArchivo = Ext.getCmp('btnArchivoCsv');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			btnGenArchivo.setIconClass('icoXls');
			btnGenArchivo.setText('Generar Archivo');
			
			var archivo = resp.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			btnGenArchivo.enable();
			
			var fp = Ext.getCmp('fpParams');
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			

		} else {
			btnGenArchivo.setIconClass('icoXls');
			btnGenArchivo.setText('Generar Archivo');
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessGenerarArhivoPdf = function(opts, success, response) {
		var btnGenArchivo = Ext.getCmp('btnImprimir');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			btnGenArchivo.setIconClass('icoImprimir');
			btnGenArchivo.setText('Imprimir');
			
			var archivo = resp.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			btnGenArchivo.enable();
			
			var fp = Ext.getCmp('fpParams');
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			

		} else {
			btnGenArchivo.setIconClass('icoImprimir');
			btnGenArchivo.setText('Imprimir');
			NE.util.mostrarConnError(response,opts);
		}
	}


//------------------------------------------------------------------------------

	var storeCatIfData = new Ext.data.JsonStore({
		id: 'storeCatIfData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29camposparam_ext.data.jsp',
		baseParams: {
			informacion: 'consCatIf'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeCatBoData = new Ext.data.JsonStore({
		id: 'storeCatBoData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29camposparam_ext.data.jsp',
		baseParams: {
			informacion: 'consCatBo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, oprion){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
						Ext.getCmp('claveBO1').setValue('');
						store.each(function(record) {	
							record.data['descripcion'] = record.data['clave']+' - '+record.data['descripcion'];
						});
					}
			}
		}
	});

//------------------------------------------------------------------------------

	var elementosForm =
	[
		{
			xtype: 'combo',
			name: 'claveIf',
			id: 'claveIf1',
			fieldLabel: 'Nombre IF',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveIf',
			emptyText: 'Seleccione...',
			allowBlank: false,
			anchor: '100%',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatIfData,
			listeners:{
				select : function(cboIf, record, index ) {
					var valIF = cboIf.getValue();
					storeCatBoData.load({
						params:{
							claveIf: valIF
						}
					});
				},
				change: function(obj, newVal, oldVal){
					if(newVal!=oldVal){
						Ext.getCmp('btnGuardarParams').disable();
						 
						fpParams.hide();
					}
				}
			}
		},
		{
			xtype: 'combo',
			name: 'claveBO',
			id: 'claveBO1',
			fieldLabel: 'Base de Operaci�n',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveBO',
			emptyText: 'Seleccione...',
			autoSelect :true,
			allowBlank: false,
			anchor: '100%',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : 	storeCatBoData,
			listeners:{
				change: function(obj, newVal, oldVal){
					if(newVal!=oldVal){
						Ext.getCmp('btnGuardarParams').disable();
						 
						fpParams.hide();
					}
				}
			}

		}
	];
	
	var elementosParam = [
		{
			xtype:			'panel',
			title:			'',
			layout:			'table',
			anchor:			'100%',
			frame:			false,
			border:true,
			style:			'margin:0 auto;',
			layoutConfig:	{ columns: 2 },
			defaults:		{frame:true,	border:true,	height:40,	bodyStyle:'padding:5px', msgTarget:	'side',	anchor:	'-20'},
			items:[
				{html:'<p align="center"><b>Nombre Del Campo</b></p>',width:250, height:25},{html:'<p align="center"><b>Valor</b></p>',width:350, height:25},
				{html:'Solicita Fondeo NAFIN',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'solicFondeo1',
							name:			"solicFondeo",
							allowBlank: false,
							maxLength:	1,
							anchor:		'85%',
							msgTarget:	'side',
							regex:		/^[N|S]{1}$/,
							regexText:	'S�lo puede ingresar S � N'
						}
					]
				},
				{html:'Solicita participaci�n en Riesgo',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'solicPartiRiesgo1',
							name:			"solicPartiRiesgo",
							allowBlank: false,
							maxLength:	1,
							anchor:		'85%',
							msgTarget:	'side',
							regex:		/^[N|S]{1}$/,
							regexText:	'S�lo puede ingresar S � N'
						}
					]
				},
				{html:'% Participaci�n',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'porcentajeParti1',
							name:			"porcentajeParti",
							allowBlank: false,
							maxLength:	6,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Prop�sito Proyecto',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'propositoProyec1',
							name:			"propositoProyec",
							allowBlank: false,
							maxLength:	2,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Clave Tipo Financiamiento',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'numberfield',
							id:			'claveTipoCred1',
							name:			"claveTipoCred",
							allowBlank: false,
							allowDecimal: false,
							maxLength:	2,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'El campo es num�rico'
						}
					]
				},
				{html:'Clave Garant�a',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'claveGarantia1',
							name:			"claveGarantia",
							allowBlank: false,
							maxLength:	14,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'% Participaci�n Financiamiento',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'porcenPartiFinan1',
							name:			"porcenPartiFinan",
							allowBlank: false,
							maxLength:	11,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'% Producci�n Mercado Interno',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'porcenProdMerInter1',
							name:			"porcenProdMerInter",
							allowBlank: false,
							maxLength:	3,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'% Producci�n Mercado Externo',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'porcenProdMerExter1',
							name:			"porcenProdMerExter",
							allowBlank: false,
							maxLength:	3,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Forma de Amortizaci�n',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'numberfield',
							id:			'formaAmortizacion1',
							name:			"formaAmortizacion",
							allowBlank: false,
							allowDecimal: false,
							maxLength:	1,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Moneda',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'moneda1',
							name:			"moneda",
							allowBlank: false,
							maxLength:	7,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'No. Meses de gracia del Financiamiento',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'numberfield',
							id:			'numMesesGraciaFinan1',
							name:			"numMesesGraciaFinan",
							allowBlank: false,
							allowDecimal: false,
							maxLength:	3,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Clave Funcionario facultado del IF',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'numberfield',
							id:			'claveFuncionarioFaculIf1',
							name:			"claveFuncionarioFaculIf",
							allowBlank: false,
							allowDecimal: false,
							maxLength:	2,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Tipo Autorizaci�n',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'numberfield',
							id:			'tipoAutorizacion1',
							name:			"tipoAutorizacion",
							allowBlank: false,
							allowDecimal: false,
							maxLength:	1,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Tipo Programa',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'numberfield',
							id:			'tipoPrograma1',
							name:			"tipoPrograma",
							allowBlank: false,
							allowDecimal: false,
							maxLength:	5,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Tipo Tasa',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'tipoTasa1',
							name:			"tipoTasa",
							allowBlank: false,
							maxLength:	1,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Plazo en d�as',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'numberfield',
							id:			'plazoDias1',
							name:			"plazoDias",
							allowBlank: false,
							allowDecimal: false,
							maxLength:	3,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Calificaci�n Final',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'textfield',
							id:			'cgCalificacionFinal1',
							name:			"cgCalificacionFinal",
							allowBlank: false,
							maxLength:	2,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'Antig�edad Cliente en meses',width:250},
				{
					layout:		'form',
					labelWidth:	1,
					width:		350,
					items:[
						{
							xtype:		'numberfield',
							id:			'antiguedadClientMeses1',
							name:			"antiguedadClientMeses",
							allowBlank: false,
							allowDecimal: false,
							maxLength:	3,
							anchor:		'85%',
							msgTarget:	'side'
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				}
			]
		}
	];


//------------------------------------------------------------------------------
	var fpCriteriosBusq = new Ext.form.FormPanel({
		id: 'fpCriteriosBusq1',
		width: 600,
		title: 'Parametrizaci�n de Campos para el Alta Autom�tica de Garant�as',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 20px',
		style: 'margin:0 auto;',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForm,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					fpParams.hide();
					Ext.getCmp('btnGuardarParams').disable();
					 
					Ext.Ajax.request({
						url: '29camposparam_ext.data.jsp',
						params: Ext.apply(fpCriteriosBusq.getForm().getValues(),{
							informacion: 'consultaCamposParam'
						}),
						callback: procesarSuccessConsCamposParam
					});
				}
			},
			{
				text: 'Guardar',
				id: 'btnGuardarParams',
				iconCls: 'icoGuardar',
				//formBind: true,
				hidden: false,
				disabled: true,
				handler: function(boton, evento) {
					guardarParams();
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				hidden: false,
				handler: function() {
					window.location.href='29camposparam_ext.jsp';
					//fp.getForm().reset();
				}
				
			}
		]
	});
	
	var fpParams = new Ext.form.FormPanel({
		id:				'fpParams',
		title:			undefined,
		width:			600,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			false,
		hidden:			true,
		bodyStyle:		'padding: 10px',
		labelWidth:		100,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosParam,
		monitorValid:	true,
		buttonAlign:	'center',
		buttons: [
			{
				text:		'Generar Archivo',
				id:		'btnArchivoCsv',
				iconCls:	'icoXls',
				width:	100,
				//formBind:true,
				handler: function(boton, evento) {
					
					boton.disable();
					boton.setIconClass('loading-indicator');
					boton.setText('Abriendo Archivo');
					
					var cboIf = Ext.getCmp('claveIf1');
					var recordIf = cboIf.findRecord(cboIf.valueField, cboIf.getValue());
					var nombreIf = recordIf.data['descripcion'];
					
					var cboBO = Ext.getCmp('claveBO1');
					var recordBO = cboBO.findRecord(cboBO.valueField, cboBO.getValue());
					var nombreBO = recordBO.data['descripcion'];
					
					Ext.Ajax.request({
						url: '29camposparam_ext.data.jsp',
						params: Ext.apply(fpCriteriosBusq.getForm().getValues(),{
							informacion: 'generarArhivoCsv',
							operacion: 'CSV',
							nombreIf: nombreIf,
							nombreBO: nombreBO
						}),
						callback: procesarSuccessGenerarArhivoCsv
					});
					

				} //fin handler
			},
			{
				text:		'Imprimir',
				id:		'btnImprimir',
				iconCls:	'icoImprimir',
				width:	100,
				//formBind:true,
				handler: function(boton, evento) {
					
					boton.disable();
					boton.setIconClass('loading-indicator');
					boton.setText('Abriendo Archivo');
					
					var cboIf = Ext.getCmp('claveIf1');
					var recordIf = cboIf.findRecord(cboIf.valueField, cboIf.getValue());
					var nombreIf = recordIf.data['descripcion'];
					
					var cboBO = Ext.getCmp('claveBO1');
					var recordBO = cboBO.findRecord(cboBO.valueField, cboBO.getValue());
					var nombreBO = recordBO.data['descripcion'];
					
					Ext.Ajax.request({
						url: '29camposparam_ext.data.jsp',
						params: Ext.apply(fpCriteriosBusq.getForm().getValues(),{
							informacion: 'generarArhivoCsv',
							operacion: 'PDF',
							nombreIf: nombreIf,
							nombreBO: nombreBO
						}),
						callback: procesarSuccessGenerarArhivoPdf
					});
					

				} //fin handler
			}
		]
	});

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		renderHidden: true,
		layoutConfig: {
			align:'center'
		},
		items: [
			NE.util.getEspaciador(20),
			fpCriteriosBusq,
			NE.util.getEspaciador(20),
			fpParams
		]
	});
	
	
	storeCatIfData.load();

});