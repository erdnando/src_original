<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*, 
				netropology.utilerias.*, 
				com.netro.afiliacion.*,
				com.netro.model.catalogos.*,
				com.netro.garantias.*,
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/29garantias/29secsession.jspf"%>
<%  
String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion") !=null)?request.getParameter("operacion"):"";
String nombreIF = (request.getParameter("nombreIF") !=null)?request.getParameter("nombreIF"):"";
String fechaOperacionMin = (request.getParameter("fechaOperacionMin") !=null)?request.getParameter("fechaOperacionMin"):"";
String fechaOperacionMax = (request.getParameter("fechaOperacionMax") !=null)?request.getParameter("fechaOperacionMax"):"";
String situacion = (request.getParameter("situacion") !=null)?request.getParameter("situacion"):"";
String folio_operacion = (request.getParameter("folio_operacion") !=null)?request.getParameter("folio_operacion"):"";


int  start= 0, limit =0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();


 if(informacion.equals("ConsultaIF")){
	CatalogoIF cat = new CatalogoIF();
	cat.setCampoClave("I.ic_if");
	cat.setCampoDescripcion("I.cg_razon_social");
	
	//cat.setG_vobonafin("S");
	List lista = cat.getListaElementosGral();
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lista);
	infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";

}else if(informacion.equals("ConsultaSituacion")){
	CatalogoSituacion cat = new CatalogoSituacion();
	cat.setClaveTipoOperacion("6");
	cat.setCampoClave("ic_situacion");
   cat.setCampoDescripcion("cg_descripcion"); 
	List lista = cat.getListaElementos();
	String cadena="[{\"clave\":\"6\",\"descripcion\":\"ARCHIVO A REPROCESAR\"},{\"clave\":\"5\",\"descripcion\":\"CONFIRMACIÓN\"},{\"clave\":\"2\",\"descripcion\":\"EN VALIDACION POR MDC\"},{\"clave\":\"1\",\"descripcion\":\"ENVIÓ PARA VALIDACIÓN Y CARGA\"}]";
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lista);
	infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " +cadena+ "}";

}else if (	informacion.equals("Consultar")	){
	
	ConsDesembolsoMasivo clase = new ConsDesembolsoMasivo();
	clase.setIntermediario(nombreIF);
	clase.setFoperacionIni(fechaOperacionMin);
	clase.setFoperacionFin(fechaOperacionMax);
	clase.setSituacion(situacion);
	clase.setFolio(folio_operacion);
	
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( clase);

		if(informacion.equals("Consultar") )  {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
										
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			Registros reg = new Registros();
			if(informacion.equals("Consultar") ){
				try {
					if (operacion.equals("Generar")) {	//Nueva consulta
						//reg =queryHelper.doSearch();//.
						queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
					}
						//consulta = "{total:"+reg.getNumeroRegistros()+",registos:"+reg.getJSONData()+"}";//queryHelper.getJSONPageResultSet(request,start,limit);	
						consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
				} catch(Exception e) {
					throw new AppException("Error en la paginacion", e);
				}
					//Thread.sleep(5000);
					jsonObj = JSONObject.fromObject(consulta);
				
			}else if(informacion.equals("ConsultarTotales")) {
				consulta = queryHelper.getJSONResultCount(request);
				jsonObj = JSONObject.fromObject(consulta);
				//Thread.sleep(2000);
			}
			
		}
		
	infoRegresar = jsonObj.toString();		

}else if(informacion.equals("Archivos")){
	String opcion = (request.getParameter("opcion") !=null)?request.getParameter("opcion"):"";	
	String claveIF = (request.getParameter("claveIF") !=null)?request.getParameter("claveIF"):"";
	String folio = (request.getParameter("folio") !=null)?request.getParameter("folio"):"";
	
	ConsDesembolsoMasivo clase = new ConsDesembolsoMasivo();
	clase.setIntermediario(claveIF);
	clase.setFoperacionIni(fechaOperacionMin);
	clase.setFoperacionFin(fechaOperacionMax);
	clase.setSituacion(situacion);
	clase.setFolio(folio);
	
	try {
		String nombreArchivo = clase.descargarArchivos( strDirectorioTemp, opcion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo", e);
	}
	
	infoRegresar = jsonObj.toString();	

}else if (	informacion.equals("Archivospdf")	){
//generaPDF(HttpServletRequest request,String rutaFisica, String rutaVirtual,String opcion, String claveSiag )
	String opcion = (request.getParameter("opcion") !=null)?request.getParameter("opcion"):"";	
	String claveIF = (request.getParameter("claveIF") !=null)?request.getParameter("claveIF"):"";
	String folio = (request.getParameter("folio") !=null)?request.getParameter("folio"):"";
	String claveSiag = (request.getParameter("claveSiag") !=null)?request.getParameter("claveSiag"):"";
	
	ConsDesembolsoMasivo clase = new ConsDesembolsoMasivo();
	clase.setIntermediario(claveIF);
	clase.setFoperacionIni(fechaOperacionMin);
	clase.setFoperacionFin(fechaOperacionMax);
	clase.setSituacion(situacion);
	clase.setFolio(folio);
	
	try {
		String nombreArchivo = clase.generaPDF(request, strDirectorioTemp, strDirectorioPublicacion, opcion,claveSiag);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo", e);
	}
	
	infoRegresar = jsonObj.toString();
}


%>
<%=  infoRegresar%>