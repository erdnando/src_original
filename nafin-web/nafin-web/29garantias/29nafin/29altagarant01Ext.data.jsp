<%@ page contentType="application/json;charset=UTF-8" 
	import="java.util.*, 
		netropology.utilerias.*,
		com.netro.seguridadbean.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,
		com.netro.garantias.*,
		net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/29garantias/29secsession.jspf"%> 
<%  
String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion") !=null)?request.getParameter("operacion"):"";
String nombreIF = (request.getParameter("nombreIF")!=null)?request.getParameter("nombreIF"):"0";
String fechaOperacionMin = (request.getParameter("fechaOperacionMin") !=null)?request.getParameter("fechaOperacionMin"):"";
String fechaOperacionMax = (request.getParameter("fechaOperacionMax") !=null)?request.getParameter("fechaOperacionMax"):"";
String situacion = (request.getParameter("situacion") !=null)?request.getParameter("situacion"):"";
String folio_operacion = (request.getParameter("folio_operacion") !=null)?request.getParameter("folio_operacion"):"";

 
int  start= 0, limit =0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();


 if(informacion.equals("ConsultaIF")){
	CatalogoIF cat = new CatalogoIF();
	cat.setCampoClave("I.ic_if");
	cat.setCampoDescripcion("I.cg_razon_social");
	
	//cat.setG_vobonafin("S");
	List lista = cat.getListaElementosGral();
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lista);
	infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";

}else if(informacion.equals("ConsultaSituacion")){
	CatalogoSituacion cat = new CatalogoSituacion();
	cat.setClaveTipoOperacion("6");
	cat.setCampoClave("ic_situacion");
   cat.setCampoDescripcion("cg_descripcion"); 
	
	//cat.setG_vobonafin("S");
	List lista = cat.getListaElementos();
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lista);
	infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";


}else if (	informacion.equals("Consultar")	){
	com.netro.seguridadbean.Seguridad BeanSegFacultad = null;
	try {
	BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
} catch (Exception e) {
	System.out.println(e);
	///retronar un mensaje de arror
	//alert("EXISTEN DIFICULTADES TECNICAS, INTENTE MÁS TARDE"); 
}	
	String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);
	
	ConsSolicNafinGAbean01 clase = new ConsSolicNafinGAbean01();
	nombreIF = (nombreIF.equals(""))?"":nombreIF;
	clase.setIc_if(nombreIF);
	clase.setFechaOperIni(fechaOperacionMin);
	clase.setFechaOperFin(fechaOperacionMax);
	clase.setSituacion(situacion);
	clase.setiTipoOperacion(1);
	clase.setCve_portafolio("");
	Registros reg = new Registros();
	HashMap datos = new HashMap();
	List listReg = new ArrayList();
	List listRegA = new ArrayList();
			try {
				reg= clase.consultar();
				int cont_reg = 0;
				while(reg.next()){
					datos = new HashMap();
					datos.put("CLAVE_IF",(reg.getString("CLAVE_IF")==null)?"":reg.getString("CLAVE_IF"));
					datos.put("NOMBRE_IF",(reg.getString("NOMBRE_IF")==null)?"":reg.getString("NOMBRE_IF"));
					datos.put("TIPO_ENVIO",(reg.getString("TIPO_ENVIO")==null)?"":reg.getString("TIPO_ENVIO"));
					datos.put("FECHA",(reg.getString("FECHA")==null)?"":reg.getString("FECHA"));
					datos.put("SITUACION",(reg.getString("SITUACION")==null)?"":reg.getString("SITUACION"));
					datos.put("IN_REGISTROS_ACEP",(reg.getString("IN_REGISTROS_ACEP")==null)?"":reg.getString("IN_REGISTROS_ACEP"));
					datos.put("IN_REGISTROS_RECH",(reg.getString("IN_REGISTROS_RECH")==null)?"":reg.getString("IN_REGISTROS_RECH"));
					datos.put("IN_REGISTROS",(reg.getString("IN_REGISTROS")==null)?"":reg.getString("IN_REGISTROS"));
					datos.put("IC_SITUACION",(reg.getString("IC_SITUACION")==null)?"":reg.getString("IC_SITUACION"));
					datos.put("MONTO_ENVIADO",(reg.getString("MONTO_ENVIADO")==null)?"":reg.getString("MONTO_ENVIADO"));
					datos.put("REPROCESO",(reg.getString("REPROCESO")==null)?"":reg.getString("REPROCESO"));
					datos.put("PANTALLA",(reg.getString("PANTALLA")==null)?"":reg.getString("PANTALLA"));
					datos.put("IC_FOLIO",(reg.getString("IC_FOLIO")==null)?"":reg.getString("IC_FOLIO"));
					datos.put("DF_FECHA_HORA",(reg.getString("DF_FECHA_HORA")==null)?"":reg.getString("DF_FECHA_HORA"));
					listReg.add(datos);
					
				}
				
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
	JSONArray json = JSONArray.fromObject(listReg);
	consulta= "{\"success\": true, \"total\": \"" + json.size() + "\", \"registros\": " + json.toString()+"}";	
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar =jsonObj.toString();

}else if(informacion.equals("Archivos")){
	String opcion = (request.getParameter("opcion") !=null)?request.getParameter("opcion"):"";	
	String clave_if = (request.getParameter("clave_if") !=null)?request.getParameter("clave_if"):"";
	String folio = (request.getParameter("folio") !=null)?request.getParameter("folio"):"";
	
	ConsDesembolsoMasivo clase = new ConsDesembolsoMasivo();
	clase.setIntermediario(clave_if);
	clase.setFoperacionIni(fechaOperacionMin);
	clase.setFoperacionFin(fechaOperacionMax);
	clase.setSituacion(situacion);
	clase.setFolio(folio);
	
	try {
		String nombreArchivo = clase.descargarArchivos( strDirectorioTemp, opcion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo", e);
	}
	
	infoRegresar = jsonObj.toString();	

}else if (	informacion.equals("Archivospdf")	){
//generaPDF(HttpServletRequest request,String rutaFisica, String rutaVirtual,String opcion, String claveSiag )
	String origen = (request.getParameter("origen") !=null)?request.getParameter("origen"):"";	
	String clave_if = (request.getParameter("claveIF") !=null)?request.getParameter("claveIF"):"";
	String folio = (request.getParameter("folio") !=null)?request.getParameter("folio"):"";
	
	ConsDesembolsoMasivo clase = new ConsDesembolsoMasivo();
	clase.setIntermediario(clave_if);
	clase.setFoperacionIni(fechaOperacionMin);
	clase.setFoperacionFin(fechaOperacionMax);
	clase.setSituacion(situacion);
	clase.setFolio(folio);
	
	try {
		String nombreArchivo = "";//clase.generaPDF(request, strDirectorioTemp, strDirectorioPublicacion, origen);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo", e);
	}
	
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("obtenerDetalle")){
	String IC_SITUACION = (request.getParameter("IC_SITUACION") !=null)?request.getParameter("IC_SITUACION"):"";	
	String CLAVE_IF = (request.getParameter("CLAVE_IF") !=null)?request.getParameter("CLAVE_IF"):"";
	String IC_FOLIO = (request.getParameter("IC_FOLIO") !=null)?request.getParameter("IC_FOLIO"):"";
	String fechaMin = (request.getParameter("fechaMin") !=null)?request.getParameter("fechaMin"):"";
	String fechaFin = (request.getParameter("fechaFin") !=null)?request.getParameter("fechaFin"):"";
	ConsSolicNafinGAbean01 clase = new ConsSolicNafinGAbean01();
	
	List listReg = new ArrayList();
	try {
		listReg =clase.getDetalleReg(CLAVE_IF,IC_SITUACION,"1",IC_FOLIO,fechaMin,fechaFin);
		
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo", e);
	}
	
	jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("registros",listReg);
	jsonObj.put("IC_FOLIO",IC_FOLIO);
	infoRegresar = jsonObj.toString();
	
	System.out.println(" IC_SITUACION *** "+infoRegresar);
}


%>
<%=  infoRegresar%>