var showPanelLayoutDeCarga;

Ext.onReady(function() {

	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			// w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth, hidden ? 0 : dom.clientWidth || me.getComputedWidth() ) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

	//----------------------------------- HANDLERS ------------------------------------

	var procesaConsultaCedulaConciliacion = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						consultaCedulaConciliacion(resp.estadoSiguiente,resp);
					}
				);
			} else {
				consultaCedulaConciliacion(resp.estadoSiguiente,resp);
			}

		} else {

			// Mostrar mensaje de error
			NE.util.mostrarConnError(
				response,
				opts,
				function(){

					var formaConsulta = Ext.getCmp("formaConsulta");
					if( formaConsulta.getEl().isMasked() ){
						formaConsulta.getEl().unmask();
					}

					var gridCedulaConciliacion = Ext.getCmp("gridCedulaConciliacion");
					var cedulaConciliacionData = Ext.StoreMgr.key('cedulaConciliacionDataStore');

					if( gridCedulaConciliacion.generandoArchivo && !Ext.isEmpty(gridCedulaConciliacion.recordId) ){

						// Suprimir animacion
						for(var i=0;i<gridCedulaConciliacion.recordId.length;i++){

							// Obtener registro
							var record			= 	cedulaConciliacionData.getById(gridCedulaConciliacion.recordId[i]);
							if( Ext.isEmpty(record) ){
								continue;
							}

							// Suprimir animacion del registro
							if( record.data['DESCARGA_CEDULA_CONCILIACION_LOADING'] ){
								record.data['DESCARGA_CEDULA_CONCILIACION_LOADING'] 	= false;
							}

						}
						gridCedulaConciliacion.getView().refresh();

						// Deshabilitar flag que indica que hay una consulta en proceso
						gridCedulaConciliacion.generandoArchivo = false;
						gridCedulaConciliacion.recordId			 = null;

					}

				}
			);

		}

	}

	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var fnFirmarDescCedulaCallback = function(vpkcs7, vtextoFirmar, vrecordId, vclaveIF, vclaveMesConciliar, vfechaActual){

        // Determinar el estado siguiente
		Ext.Ajax.request({
			url: 								'29ConsultaCedulaConciliacion01ext.data.jsp',
			params: {
				informacion:				'ConsultaCedulaConciliacion.firmarDescargaCedula',
				recordId:					vrecordId,
				claveIF:					vclaveIF,
				claveMesConciliar:			vclaveMesConciliar,
				isEmptyPkcs7: 				Ext.isEmpty(vpkcs7),
				pkcs7: 						vpkcs7,
				textoFirmado: 				vtextoFirmar,
				fechaActual:				vfechaActual
			},
			callback: 						procesaConsultaCedulaConciliacion
		});
	}

	var consultaCedulaConciliacion = function(estado, respuesta ){

		if(			estado == "INICIALIZACION"					){

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ConsultaCedulaConciliacion01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaCedulaConciliacion.inicializacion'
				},
				callback: 				procesaConsultaCedulaConciliacion
			});

		} else if(  estado == "VALIDA_NUMERO_FISO"         ){

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 							'29ConsultaCedulaConciliacion01ext.data.jsp',
				params: 	{
					informacion:			'ConsultaCedulaConciliacion.validaNumeroFiso',
					estadoFinal:			respuesta.estadoFinal,
					formaConsultaValues:	Ext.isDefined(respuesta.formaConsultaValues) ?Ext.encode(respuesta.formaConsultaValues):undefined,
					claveIF:					Ext.isDefined(respuesta.claveIF)             ?respuesta.claveIF            				:undefined
				},
				callback: 					procesaConsultaCedulaConciliacion
			});

		} else if(  estado == "MOSTRAR_AVISO"             ){

			if(!Ext.isEmpty(respuesta.aviso)){

				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);

				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();

			}

		} else if(  estado == "MOSTRAR_FORMA_CONSULTA"    ){

			// Cargar Catalogo de Intermediarios Financieros
			var catalogoIFData = Ext.StoreMgr.key("catalogoIFDataStore");
			catalogoIFData.load();

			// Cargar Catalogo de Meses Conciliados
			var catalogoMesConciliadoData = Ext.StoreMgr.key("catalogoMesConciliadoDataStore");
			catalogoMesConciliadoData.load({
				params: {
					selectFirstValue: true
				}
			});

			// Cargar Catalogo de Monedas
			var catalogoMonedaDataStore	= Ext.StoreMgr.key("catalogoMonedaDataStore");
			catalogoMonedaDataStore.load();

			// Mostrar forma de consulta
			var formaConsulta = Ext.getCmp("formaConsulta");
			formaConsulta.show();

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ConsultaCedulaConciliacion01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaCedulaConciliacion.mostrarFormaConsulta'
				},
				callback: 				procesaConsultaCedulaConciliacion
			});

		} else if(  estado == "CONSULTA_RESULTADOS" ){

			// Agregar mascara para indicar que se est� realizando una consulta
			var formaConsulta = Ext.getCmp("formaConsulta");
			formaConsulta.getEl().mask('Consultando...','x-mask-loading');

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ConsultaCedulaConciliacion01ext.data.jsp',
				params: 	{
					informacion:			'ConsultaCedulaConciliacion.consultaResultados',
					formaConsultaValues:	Ext.encode(respuesta.formaConsultaValues),
					claveIF:					respuesta.claveIF
				},
				callback: 					procesaConsultaCedulaConciliacion
			});

		} else if(  estado == "REALIZAR_CONSULTA_CEDULA_CONCILIACION" 	){

			// Realizar consulta
			var cedulaConciliacionData = Ext.StoreMgr.key('cedulaConciliacionDataStore');
			cedulaConciliacionData.load({
				params: Ext.apply(
					{},
					respuesta.formaConsultaValues
				)
			});

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ConsultaCedulaConciliacion01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaCedulaConciliacion.realizarConsultaCedulaConciliacion'
				},
				callback: 				procesaConsultaCedulaConciliacion
			});

      } else if(  estado == "ESPERAR_DECISION"  							){

      	var gridCedulaConciliacion = Ext.getCmp("gridCedulaConciliacion");
      	var cedulaConciliacionData = Ext.StoreMgr.key('cedulaConciliacionDataStore');

      	if( Ext.isDefined( respuesta.formaConsultaUnmask ) && respuesta.formaConsultaUnmask === true ){
      		// Agregar mascara para indicar que se est� realizando una consulta
      		var formaConsulta = Ext.getCmp("formaConsulta");
      		formaConsulta.getEl().unmask();
      	}

      	if( !Ext.isEmpty( respuesta.recordId ) && gridCedulaConciliacion.generandoArchivo ){

      		// Deshabilitar flag que indica que hay una consulta en proceso
      		gridCedulaConciliacion.generandoArchivo = false;
      		gridCedulaConciliacion.recordId			 = null;

      		// Suprimir animacion
      		for(var i=0;i<respuesta.recordId.length;i++){

      			// Obtener registro
      			var record = cedulaConciliacionData.getById(respuesta.recordId[i]);
      			if(Ext.isEmpty(record)){
      				continue;
      			}

      			// Suprimir animacion del registro
      			if( record.data['DESCARGA_CEDULA_CONCILIACION_LOADING'] ){
      				record.data['DESCARGA_CEDULA_CONCILIACION_LOADING'] = false;
      			}

      		}
      		gridCedulaConciliacion.getView().refresh();

      	}

      	// Este es el unico estado que no pasa por el jsp, para determinar
      	// el estado siguiente... se hace as� para reducir la complejidad del
      	// codigo
      	return;

		} else if(	estado == "DESCARGA_CEDULA_CONCILIACION"		){

      	var gridCedulaConciliacion	= Ext.getCmp("gridCedulaConciliacion");
      	var cedulaConciliacionData = Ext.StoreMgr.key('cedulaConciliacionDataStore');

      	// Indicar que hay una consulta en proceso
      	gridCedulaConciliacion.generandoArchivo = true;
      	gridCedulaConciliacion.recordId			 = Ext.apply(new Array(),respuesta.recordId);

      	// Mostrar animacion, de momento, solo para el registro seleccionado por el usuario
      	var record = cedulaConciliacionData.getById(respuesta.recordId[0]);
      	record.data['DESCARGA_CEDULA_CONCILIACION_LOADING'] = true;
      	gridCedulaConciliacion.getView().refresh();

         // Determinar el estado siguiente
			Ext.Ajax.request({
				url: 								'29ConsultaCedulaConciliacion01ext.data.jsp',
				params: {
					informacion:				'ConsultaCedulaConciliacion.descargaCedulaConciliacion',
					recordId:					respuesta.recordId,
					claveMesConciliar:		respuesta.claveMesConciliar,
					claveIF:						respuesta.claveIF
				},
				callback: 						procesaConsultaCedulaConciliacion
			});

		} else if(  estado == "FIRMAR_DESCARGA_CEDULA"						){

			var gridCedulaConciliacion	= Ext.getCmp("gridCedulaConciliacion");
			var cedulaConciliacionData	= Ext.StoreMgr.key('cedulaConciliacionDataStore');

			// Extraer registro seleccionado
			var record						= 	cedulaConciliacionData.getById(respuesta.recordId[0]);

			// Firmar contenido del portafolio a extraer
			var textoFirmar				= 	"";
			var pkcs7 						=	null;

			// Construir texto a firmar
			textoFirmar += "Mes Conciliado: "   + record.data["MES_CONCILIADO"].format("F Y") + "\n";
			textoFirmar += "Fecha Descarga: "   + respuesta.fechaActual                       + "\n";
			textoFirmar += "Moneda: ";
			for(var i=0;i<respuesta.recordId.length;i++){
				var record = cedulaConciliacionData.getById(respuesta.recordId[i]);
				if( i > 0 && i == respuesta.recordId.length - 1 ){
					 textoFirmar += " y ";
				} else if( i > 0 ){
					 textoFirmar += ", ";
				}
				textoFirmar += record.data["MONEDA"];
			}
			textoFirmar += "\n";

			// Solicitar al usuario que firme el texto
			NE.util.obtenerPKCS7(fnFirmarDescCedulaCallback, textoFirmar, respuesta.recordId,
				respuesta.claveIF, respuesta.claveMesConciliar, respuesta.fechaActual);


		} else if(	estado == "MOSTRAR_DETALLE_FIRMA_DESCARGA_CEDULA"		){

			var gridCedulaConciliacion	= Ext.getCmp("gridCedulaConciliacion");
			var cedulaConciliacionData	= Ext.StoreMgr.key('cedulaConciliacionDataStore');

			// Obtener usuario y Fecha de Descarga
			var usuarioDescarga	= respuesta.datosFirmaExtraccion.usuarioDescarga;
      	var fechaDescarga		= Ext.isEmpty(respuesta.datosFirmaExtraccion.fechaDescarga)?null:Date.parseDate(respuesta.datosFirmaExtraccion.fechaDescarga, 'd/m/Y');

      	// Actualizar fecha y usuario de descarga
      	for(var i=0;i<respuesta.recordId.length;i++){
      		var record = cedulaConciliacionData.getById(respuesta.recordId[i]);
      		record.set('FECHA_DESCARGA',   fechaDescarga   );
      		record.set('USUARIO_DESCARGA', usuarioDescarga );
      		record.commit();
      	}

      	// Actualizar registros
      	gridCedulaConciliacion.getView().refresh();

      	// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 								'29ConsultaCedulaConciliacion01ext.data.jsp',
				params: {
					informacion:				'ConsultaCedulaConciliacion.mostrarDetalleFirmaDescargaCedula',
					recordId:					respuesta.recordId,
					claveIF: 					respuesta.claveIF,
					claveMesConciliar:		respuesta.claveMesConciliar,
					claveFiso: 					respuesta.claveFiso,
					claveIfSiag: 				respuesta.claveIfSiag
				},
				callback: 						procesaConsultaCedulaConciliacion
			});

		} else if(	estado == "EXTRAER_CEDULA_CONCILIADA"		){

			var gridCedulaConciliacion	= Ext.getCmp("gridCedulaConciliacion");
			var cedulaConciliacionData = Ext.StoreMgr.key("cedulaConciliacionDataStore");

			// Extraer lista de monedas, de acuerdo a los registros seleccionados
			var clavesMoneda = new Array();
      	for(var i=0;i<respuesta.recordId.length;i++){
      		var record = cedulaConciliacionData.getById(respuesta.recordId[i]);
      		clavesMoneda.push(record.get('CLAVE_MONEDA'));
      	}

      	// En caso de que haya mas de un registro involucrado, asegurarse que
      	// en ambos se muestre la animacion de generando archivo
      	for(var i=0;i<respuesta.recordId.length;i++){
      		var record = cedulaConciliacionData.getById(respuesta.recordId[i]);
      		record.data['DESCARGA_CEDULA_CONCILIACION_LOADING'] = true;
      	}
      	gridCedulaConciliacion.getView().refresh();

      	// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 								'29ConsultaCedulaConciliacion01ext.data.jsp',
				params: {
					informacion:				'ConsultaCedulaConciliacion.extraerCedulaConciliada',
					recordId:					respuesta.recordId,
					claveIF: 					respuesta.claveIF,
					claveMesConciliar:		respuesta.claveMesConciliar,
					claveFiso: 					respuesta.claveFiso,
					claveIfSiag: 				respuesta.claveIfSiag,
					clavesMoneda:				clavesMoneda
				},
				callback: 						procesaConsultaCedulaConciliacion
			});

		} else if(	estado == "MOSTRAR_LINK_DESCARGA_CEDULA_CONCILIADA" 	){

			var gridCedulaConciliacion 				 = Ext.getCmp("gridCedulaConciliacion");
      	var cedulaConciliacionData					 = Ext.StoreMgr.key('cedulaConciliacionDataStore');

      	// Deshabilitar flag que indica que hay una consulta en proceso

      	gridCedulaConciliacion.generandoArchivo = false;
      	gridCedulaConciliacion.recordId			 = null;

      	// Agregar archivos generados
      	for(var i=0;i<respuesta.recordId.length;i++){

      		var record 			= cedulaConciliacionData.getById(respuesta.recordId[i]);
      		var claveMoneda 	= record.get('CLAVE_MONEDA');

      		// Suprimir animacion
      		record.data['DESCARGA_CEDULA_CONCILIACION_LOADING'] 	= false;
      		record.data['DESCARGA_CEDULA_CONCILIACION_URL'] 		= respuesta.urlArchivo[claveMoneda];

      	}

      	// Mostrar  ZIP
      	gridCedulaConciliacion.getView().refresh();

		} else if(	estado == "FIN"									){

			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "29ConsultaCedulaConciliacion01ext.jsp";
			forma.target	= "_self";
			forma.submit();

		}

		return;

	}

	//-------------------------- PANEL CEDULA CONCILIACION ----------------------------

	var procesarConsultaCedulaConciliacion = function(store, registros, opts){

		var formaConsulta   				= Ext.getCmp('formaConsulta');
		formaConsulta.el.unmask();

		var gridCedulaConciliacion 	= Ext.getCmp('gridCedulaConciliacion');
		var el 								= gridCedulaConciliacion.getGridEl();
		el.unmask();

		if (registros != null) {

			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

		// Restaurar el estado original del Grid
		} else {

			var store = Ext.StoreMgr.key('cedulaConciliacionDataStore');
			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

		}

		/*
		// Si ocurri� un error deshabilitar el grid
		if( store == null && registros == null && opts == null){
			gridCedulaConciliacion.setDisabled(true);
		}
		*/

	}

	var monedaRenderer = function( value, metadata, record, rowIndex, colIndex, store){

		metadata.attr = 'style="white-space: normal; word-wrap:break-word;" ';
		return value;

	}

	var usuarioDescargaRenderer = function( value, metadata, record, rowIndex, colIndex, store){

		metadata.attr = 'style="white-space: normal; word-wrap:break-word;" ';
		return value;

	}

	var cedulaConciliacionData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'cedulaConciliacionDataStore',
		url: 		'29ConsultaCedulaConciliacion01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaCedulaConciliacion'
		},
		fields: [
			{ name: 'CLAVE_IF',										type: 'string'    },
			{ name: 'CLAVE_MES_CONCILIAR', 						type: 'string' 	},
			{ name: 'CLAVE_FISO', 									type: 'string' 	},
			{ name: 'CLAVE_IF_SIAG', 								type: 'string' 	},
			{ name: 'CLAVE_MONEDA', 								type: 'string' 	},
			{ name: 'MES_CONCILIADO', 								type: 'date',   convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'Ym')); } },
			{ name: 'MONEDA', 										type: 'string' 	},
			{ name: 'FECHA_DESCARGA', 								type: 'date',   convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } },
			{ name: 'USUARIO_DESCARGA', 							type: 'string'		},
			{ name: 'DESCARGA_CEDULA_CONCILIACION_LOADING',	type: 'boolean'	},
			{ name: 'DESCARGA_CEDULA_CONCILIACION_URL', 		type: 'string'		},
			{ name: 'EXISTE_CEDULA_CONCILIACION',				type: 'boolean' 	}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {

					// Suprimir mascara que indica: No se encontraron registros
					var gridEl = Ext.getCmp("gridCedulaConciliacion").getGridEl();
					if( gridEl.isMasked() ){
						gridEl.unmask();
					}

					// En caso el grid no este visible, mostrarlo
					var panelCedulaConciliacion = Ext.getCmp("panelCedulaConciliacion");
					if( !panelCedulaConciliacion.isVisible() ){
						panelCedulaConciliacion.show();
					}

				}
			},
			load: 	procesarConsultaCedulaConciliacion,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCedulaConciliacion(null, null, null);
				}
			}
		}

	});

	var elementosCedulaConciliacion = [
		{
			store: 		cedulaConciliacionData,
			xtype: 		'grid',
			id:			'gridCedulaConciliacion',
			stripeRows: true,
			loadMask: 	true,
			height: 		294,
			style: {
				borderWidth: 1
			},
			generandoArchivo: false,
			recordId: 			null,
			autoExpandColumn: 'USUARIO_DESCARGA',
			columns: [
				{
					header: 		'Mes Conciliado',
					tooltip: 	'Mes Conciliado',
					dataIndex: 	'MES_CONCILIADO',
					sortable: 	true,
					resizable: 	true,
					width: 		100,
					hidden: 		false,
					align:		'center',
					xtype: 		'datecolumn',
					format: 		'F Y'
				},
				{
					header: 		'Moneda',
					tooltip: 	'Moneda',
					dataIndex: 	'MONEDA',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hidden: 		false,
					align:		'center',
					renderer:	monedaRenderer
				},
				{
					header: 		'Fecha Descarga',
					tooltip: 	'Fecha Descarga',
					dataIndex: 	'FECHA_DESCARGA',
					sortable: 	true,
					resizable: 	true,
					width: 		100,
					hidden: 		false,
					align:		'center',
					xtype: 		'datecolumn',
					format: 		'd/m/Y' // 'd-F-Y'
				},
				{
					id:			'USUARIO_DESCARGA',
					header: 		'Usuario Descarga',
					tooltip: 	'Usuario Descarga',
					dataIndex: 	'USUARIO_DESCARGA',
					sortable: 	true,
					resizable: 	true,
					//width: 		120,
					hidden: 		false,
					align:		'center',
					renderer:	usuarioDescargaRenderer
				},
				{
					xtype: 		'actioncolumn',
					header: 		'C�dula',
					tooltip: 	'C�dula',
					width: 		50,
					hidden: 		false,
					align:		'center',
					items: 		[
							{
								getClass: function(value, metadata, record) {

									if (record.data['DESCARGA_CEDULA_CONCILIACION_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['DESCARGA_CEDULA_CONCILIACION_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo PDF';
										 return 'icoPdf';
									} else if( record.data['EXISTE_CEDULA_CONCILIACION'] ){
										 this.items[0].tooltip = 'Realizar la descarga de la C�dula de Conciliaci�n';
										 return 'extraerDetalle';
									} else {
										 this.items[0].tooltip = null;
										 return null;
									}

								},
								handler: function(grid, rowIndex, colIndex) {

									var record 			= grid.getStore().getAt(rowIndex);

									// Validar que no haya otra consulta en proceso
									if ( record.data['DESCARGA_CEDULA_CONCILIACION_LOADING'] ) { // grid.generandoArchivo

										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;

									} else if ( !Ext.isEmpty(record.data['DESCARGA_CEDULA_CONCILIACION_URL']) ){

										var forma    = Ext.getDom('formAux');
										forma.action = NE.appWebContextRoot+'/DescargaArchivo';
										forma.method = 'post';
										forma.target = '_self';
										// Preparar Archivo a Descargar
										var archivo  = record.data['DESCARGA_CEDULA_CONCILIACION_URL'];
										archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
										// Insertar archivo
										var inputNombreArchivo = Ext.DomHelper.insertFirst(
											forma,
											{
												tag: 	'input',
												type: 'hidden',
												id: 	'nombreArchivo',
												name: 'nombreArchivo',
												value: archivo
											},
											true
										);
										// Solicitar Archivo Al Servidor
										forma.submit();
										// Remover nodo agregado
										inputNombreArchivo.remove();

									} else if( record.data['EXISTE_CEDULA_CONCILIACION'] ){

										// Obtener ID del Registro en cuestion
										var respuesta 						= new Object();
										respuesta.recordId				= new Array();
										// Obtener las claves del Mes a Conciliar, Fiso y IF SIAG del registro seleccionado.
										var claveIF							= record.data['CLAVE_IF'];
										var claveMesConciliar			= record.data['CLAVE_MES_CONCILIAR'];
										var claveFiso						= record.data['CLAVE_FISO'];
										var claveIfSiag					= record.data['CLAVE_IF_SIAG'];
										respuesta.claveIF   				= claveIF;
										respuesta.claveMesConciliar   = claveMesConciliar;
										// Obtener Lista de IDs de los registros agrupados por Clave Mes Conciliar
										respuesta.recordId.push(record.id); // El primer id siempre correspondera con el registro seleccionado por el usuario
										// A�adir IDs de los registros faltantes
										grid.getStore().each(
											function(record){
												if(
													claveIF 				=== record.data['CLAVE_IF']
														&&
													claveMesConciliar === record.data['CLAVE_MES_CONCILIAR']
														&&
													claveFiso         === record.data['CLAVE_FISO']
														&&
													claveIfSiag       === record.data['CLAVE_IF_SIAG']
														&&
													record.id		   !== respuesta.recordId[0] // Para evitar duplicidad de ids
												){
													respuesta.recordId.push(record.id);
												}
											}
										);

										// Realizar la descarga del portafolio
										consultaCedulaConciliacion("DESCARGA_CEDULA_CONCILIACION", respuesta );

									} else {

										return;

									}

								}

							}
						]
            }
			]
		}
	];

	var panelCedulaConciliacion = {
		hidden:			true,
		xtype:			'panel',
		id: 				'panelCedulaConciliacion',
		width: 			510,
		frame: 			true,
		style: 			'margin: 0 auto',
		items: 			elementosCedulaConciliacion
	}

	//------------------------------- FORMA CONSULTA CEDULA -----------------------------------

	var procesaConsultar = function( boton, evento ){

		var formaConsulta = Ext.getCmp("formaConsulta");

		var invalidForm = false;
		if( !formaConsulta.getForm().isValid() ){
			invalidForm = true;
		}

		// VALIDAR QUE SE HAYA SELECCIONADO UN MES CONCILIADO
		// Nota: La validacion ya se realiza en la forma

		if( invalidForm ){
			return; // La forma es invalida, se cancela la operacion
		}

		// Realizar consulta

		// Comenzar consulta
		var respuesta = new Object();
		respuesta.formaConsultaValues = formaConsulta.getForm().getValues();
		respuesta.claveIF					= respuesta.formaConsultaValues.claveIF;

		consultaCedulaConciliacion("CONSULTA_RESULTADOS", respuesta );

	}

	var procesaLimpiar   = function( boton, evento ){

		Ext.getCmp("formaConsulta").getForm().reset();

	}

	var catalogoIFData = new Ext.data.JsonStore({
		id: 					'catalogoIFDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'29ConsultaCedulaConciliacion01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIF'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){

				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}

				// Si se especific� un valor por default
				if( existeParametro ){

					var index = store.findExact( 'clave', defaultValue );
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboIF = Ext.getCmp("comboIF");
						comboIF.setValue(defaultValue);
						comboIF.originalValue = comboIF.getValue();
					}

				}

			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMesConciliadoData = new Ext.data.JsonStore({
		id: 					'catalogoMesConciliadoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'29ConsultaCedulaConciliacion01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoMesConciliado'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){

				// Debido al beforeload que agrega la animacion: Cargando...; cuando sea as�, no hacer nada...
				if(
					!Ext.isDefined(options)
						||
					!Ext.isDefined(options.params)
						||
					(
						!Ext.isDefined(options.params.selectFirstValue) && !Ext.isDefined(options.params.defaultValue)
					)
				){
					return;
				}

				// Se deber� seleccionar por default el primer valor que arroje la consulta
				var firstValue = null;
				if( options.params.selectFirstValue && records.length > 0 ){
					firstValue = records[0].data['clave'];
				}

				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = !Ext.isEmpty(firstValue)?String(firstValue):String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}

				// Si se especific� un valor por default
				if( existeParametro ){

					var index = store.findExact( 'clave', defaultValue );
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboMesConciliado = Ext.getCmp("comboMesConciliado");
						comboMesConciliado.setValue(defaultValue);
						comboMesConciliado.originalValue = comboMesConciliado.getValue();
					}

				}

			},
			exception: 	 function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboMesConciliado = Ext.getCmp("comboMesConciliado");
						// Resetear valor en el combo Moneda
						Ext.StoreMgr.key("catalogoMesConciliadoDataStore").removeAll(); // Para quitar la animacion
					}
				);
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 					'catalogoMonedaDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'29ConsultaCedulaConciliacion01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoMoneda'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){

				// Debido al beforeload que agrega la animacion: Cargando...; cuando sea as�, no hacer nada...
				if(
					!Ext.isDefined(options)
						||
					!Ext.isDefined(options.params)
						||
					(
						!Ext.isDefined(options.params.selectFirstValue) && !Ext.isDefined(options.params.defaultValue)
					)
				){
					return;
				}

				// Se deber� seleccionar por default el primer valor que arroje la consulta
				var firstValue = null;
				if( options.params.selectFirstValue && records.length > 0 ){
					firstValue = records[0].data['clave'];
				}

				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = !Ext.isEmpty(firstValue)?String(firstValue):String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}

				// Si se especific� un valor por default
				if( existeParametro ){

					var index = store.findExact( 'clave', defaultValue );
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboMoneda = Ext.getCmp("comboMoneda");
						comboMoneda.setValue(defaultValue);
						comboMoneda.originalValue = comboMoneda.getValue();
					}

				}

			},
			exception: 	 function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboMoneda 		= Ext.getCmp("comboMoneda");
						// Resetear valor en el combo Moneda
						Ext.StoreMgr.key("catalogoMonedaDataStore").removeAll(); // Para quitar la animacion
					}
				);
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var elementosFormaConsulta = [
		// COMBO IF
		{
			xtype: 				'combo',
			name: 				'if',
			id: 					'comboIF',
			fieldLabel: 		'Intermediario Financiero',
			allowBlank:			false,
			mode: 				'local',
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveIF',
			emptyText:			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoIFData,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			autoSelect: 		true
		},
		// COMBO MES CONCILIADO
		{
			xtype: 				'combo',
			name: 				'comboMesConciliado',
			id: 					'comboMesConciliado',
			fieldLabel: 		'Mes Conciliado',
			mode: 				'local',
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveMesConciliado',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMesConciliadoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>'
		},
		// COMBO MONEDA
		{
			xtype: 				'combo',
			hidden:				true, // Nota (07/11/2013 12:52:13 p.m.): Este campo queda oculto siempre.
			name: 				'comboMoneda',
			id: 					'comboMoneda',
			fieldLabel: 		'Moneda',
			mode: 				'local',
			allowBlank:			true,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveMoneda',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMonedaData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>'
		}
	];

	var formaConsulta = new Ext.form.FormPanel({
		id: 					'formaConsulta',
		width: 				510,
		title: 				'C�dula',
		hidden:				true,
		frame: 				true,
		collapsible: 		true,
		titleCollapse: 	true,
		trackResetOnLoad: true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px;',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		labelWidth: 		134,
		labelAlign:			'right',
		defaultType: 		'textfield',
		items: 				elementosFormaConsulta,
		monitorValid: 		false,
		buttons: [
			{
				text: 		'Consultar',
				iconCls: 	'icoBuscar',
				handler: 	procesaConsultar
			},
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	procesaLimpiar
			}
		]
	});

	//------------------------------- FORMA CONSULTA CEDULA -----------------------------------

	var procesaConsultar = function( boton, evento ){

		var formaConsulta = Ext.getCmp("formaConsulta");

		var invalidForm = false;
		if( !formaConsulta.getForm().isValid() ){
			invalidForm = true;
		}

		// VALIDAR QUE SE HAYA SELECCIONADO UN MES CONCILIADO
		// Nota: La validacion ya se realiza en la forma

		if( invalidForm ){
			return; // La forma es invalida, se cancela la operacion
		}

		// Realizar consulta

		// Comenzar consulta
		var respuesta = new Object();
		respuesta.formaConsultaValues = formaConsulta.getForm().getValues();
		respuesta.claveIF					= respuesta.formaConsultaValues.claveIF;

		consultaCedulaConciliacion("CONSULTA_RESULTADOS", respuesta );

	}

	var procesaLimpiar   = function( boton, evento ){

		Ext.getCmp("formaConsulta").getForm().reset();

	}

	var catalogoIFData = new Ext.data.JsonStore({
		id: 					'catalogoIFDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'29ConsultaCedulaConciliacion01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIF'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){

				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}

				// Si se especific� un valor por default
				if( existeParametro ){

					var index = store.findExact( 'clave', defaultValue );
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboIF = Ext.getCmp("comboIF");
						comboIF.setValue(defaultValue);
						comboIF.originalValue = comboIF.getValue();
					}

				}

			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMesConciliadoData = new Ext.data.JsonStore({
		id: 					'catalogoMesConciliadoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'29ConsultaCedulaConciliacion01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoMesConciliado'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){

				// Debido al beforeload que agrega la animacion: Cargando...; cuando sea as�, no hacer nada...
				if(
					!Ext.isDefined(options)
						||
					!Ext.isDefined(options.params)
						||
					(
						!Ext.isDefined(options.params.selectFirstValue) && !Ext.isDefined(options.params.defaultValue)
					)
				){
					return;
				}

				// Se deber� seleccionar por default el primer valor que arroje la consulta
				var firstValue = null;
				if( options.params.selectFirstValue && records.length > 0 ){
					firstValue = records[0].data['clave'];
				}

				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = !Ext.isEmpty(firstValue)?String(firstValue):String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}

				// Si se especific� un valor por default
				if( existeParametro ){

					var index = store.findExact( 'clave', defaultValue );
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboMesConciliado = Ext.getCmp("comboMesConciliado");
						comboMesConciliado.setValue(defaultValue);
						comboMesConciliado.originalValue = comboMesConciliado.getValue();
					}

				}

			},
			exception: 	 function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboMesConciliado = Ext.getCmp("comboMesConciliado");
						// Resetear valor en el combo Moneda
						Ext.StoreMgr.key("catalogoMesConciliadoDataStore").removeAll(); // Para quitar la animacion
					}
				);
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 					'catalogoMonedaDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'29ConsultaCedulaConciliacion01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoMoneda'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){

				// Debido al beforeload que agrega la animacion: Cargando...; cuando sea as�, no hacer nada...
				if(
					!Ext.isDefined(options)
						||
					!Ext.isDefined(options.params)
						||
					(
						!Ext.isDefined(options.params.selectFirstValue) && !Ext.isDefined(options.params.defaultValue)
					)
				){
					return;
				}

				// Se deber� seleccionar por default el primer valor que arroje la consulta
				var firstValue = null;
				if( options.params.selectFirstValue && records.length > 0 ){
					firstValue = records[0].data['clave'];
				}

				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = !Ext.isEmpty(firstValue)?String(firstValue):String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}

				// Si se especific� un valor por default
				if( existeParametro ){

					var index = store.findExact( 'clave', defaultValue );
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboMoneda = Ext.getCmp("comboMoneda");
						comboMoneda.setValue(defaultValue);
						comboMoneda.originalValue = comboMoneda.getValue();
					}

				}

			},
			exception: 	 function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboMoneda 		= Ext.getCmp("comboMoneda");
						// Resetear valor en el combo Moneda
						Ext.StoreMgr.key("catalogoMonedaDataStore").removeAll(); // Para quitar la animacion
					}
				);
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var elementosFormaConsulta = [
		// COMBO IF
		{
			xtype: 				'combo',
			name: 				'if',
			id: 					'comboIF',
			fieldLabel: 		'Intermediario Financiero',
			allowBlank:			false,
			mode: 				'local',
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveIF',
			emptyText:			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoIFData,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			autoSelect: 		true
		},
		// COMBO MES CONCILIADO
		{
			xtype: 				'combo',
			name: 				'comboMesConciliado',
			id: 					'comboMesConciliado',
			fieldLabel: 		'Mes Conciliado',
			mode: 				'local',
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveMesConciliado',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMesConciliadoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>'
		},
		// COMBO MONEDA
		{
			xtype: 				'combo',
			hidden:				true, // Nota (07/11/2013 12:52:13 p.m.): Este campo queda oculto siempre.
			name: 				'comboMoneda',
			id: 					'comboMoneda',
			fieldLabel: 		'Moneda',
			mode: 				'local',
			allowBlank:			true,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveMoneda',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMonedaData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>'
		}
	];

	var formaConsulta = new Ext.form.FormPanel({
		id: 					'formaConsulta',
		width: 				510,
		title: 				'C�dula',
		hidden:				true,
		frame: 				true,
		collapsible: 		true,
		titleCollapse: 	true,
		trackResetOnLoad: true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px;',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		labelWidth: 		134,
		labelAlign:			'right',
		defaultType: 		'textfield',
		items: 				elementosFormaConsulta,
		monitorValid: 		false,
		buttons: [
			{
				text: 		'Consultar',
				iconCls: 	'icoBuscar',
				handler: 	procesaConsultar
			},
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	procesaLimpiar
			}
		]
	});

	//------------------------------------ 1. PANEL AVISOS ---------------------------------------

	var elementosPanelAvisos = [
		{
			xtype: 	'label',
			id:	 	'labelAviso',
			cls:		'x-form-item',
			style: 	'font-weight:normal;text-align:center;margin:15px;color:black;',
			html:  	''
		}
	];

	var panelAvisos = {
		xtype:			'panel',
		id: 				'panelAvisos',
		hidden:			true,
		width: 			700,
		title: 			'Avisos',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosPanelAvisos
	}

	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		disabled: false,
		items: 	[
			panelAvisos,
			formaConsulta,
			NE.util.getEspaciador(10),
			panelCedulaConciliacion
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------
	consultaCedulaConciliacion("INICIALIZACION", null );

});;