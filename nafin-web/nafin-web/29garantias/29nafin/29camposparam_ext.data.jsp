<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoIFBOGarant,
		com.netro.model.catalogos.CatalogoBOGarant,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.garantias.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String tipoMonitor  = request.getParameter("tipoMonitor")==null?"":request.getParameter("tipoMonitor");

/*	SE CREA NUEVO OBJETO PARA ACCESAR EL EJB */
Garantias garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

if (informacion.equals("consCatIf")) {
	CatalogoIFBOGarant cat = new CatalogoIFBOGarant();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setCsAltaAutGarant("S");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

} else if (informacion.equals("consCatBo")) {
	String claveIf = (request.getParameter("claveIf")==null)?"":request.getParameter("claveIf");
	CatalogoBOGarant cat = new CatalogoBOGarant();
	cat.setCampoClave("ig_codigo_base");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setCsAltaAutGarant("S");
	cat.setClaveIf(claveIf);
	infoRegresar = cat.getJSONElementos();
} else if (informacion.equals("consultaCamposParam")){
	JSONObject jsonObj = new JSONObject();  
	String claveIf = (request.getParameter("claveIf")==null)?"":request.getParameter("claveIf");
	String claveBO = (request.getParameter("claveBO")==null)?"":request.getParameter("claveBO");
	
	HashMap resultado = garantias.getCamposParametricos(claveIf, claveBO);
	String numeroRegistros = resultado.get("numeroRegistros").toString();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("numeroRegistros", numeroRegistros);
   jsonObj.put("objParams", resultado);
	infoRegresar = jsonObj.toString();
} else if(informacion.equals("guardarCamposParam")){
	JSONObject jsonObj = new JSONObject();  
	String claveIf = (request.getParameter("claveIf")==null)?"":request.getParameter("claveIf");
	String claveBO = (request.getParameter("claveBO")==null)?"":request.getParameter("claveBO");
	
	String csSolicFondeo = (request.getParameter("solicFondeo")==null)?"":request.getParameter("solicFondeo");
	String csSolicPartiRiesgo = (request.getParameter("solicPartiRiesgo")==null)?"":request.getParameter("solicPartiRiesgo");							
	String cgPorcentajeParti = (request.getParameter("porcentajeParti")==null)?"":request.getParameter("porcentajeParti");
	String cgPropositoProyec = (request.getParameter("propositoProyec")==null)?"":request.getParameter("propositoProyec");
	String inClaveTipoCred = (request.getParameter("claveTipoCred")==null)?"":request.getParameter("claveTipoCred");
	String cgClaveGarantia = (request.getParameter("claveGarantia")==null)?"":request.getParameter("claveGarantia");
	String cgPorcenPartiFinan = (request.getParameter("porcenPartiFinan")==null)?"":request.getParameter("porcenPartiFinan");
	String inPorcenProdMerInter = (request.getParameter("porcenProdMerInter")==null)?"":request.getParameter("porcenProdMerInter");
	String inPorcenProdMerExter = (request.getParameter("porcenProdMerExter")==null)?"":request.getParameter("porcenProdMerExter");
	String inFormaAmortizacion = (request.getParameter("formaAmortizacion")==null)?"":request.getParameter("formaAmortizacion");
	String cgMoneda = (request.getParameter("moneda")==null)?"":request.getParameter("moneda");
	String inNumMesesGraciaFinan = (request.getParameter("numMesesGraciaFinan")==null)?"":request.getParameter("numMesesGraciaFinan");
	String inClaveFuncionarioFaculIf = (request.getParameter("claveFuncionarioFaculIf")==null)?"":request.getParameter("claveFuncionarioFaculIf");
	String inTipoAutorizacion = (request.getParameter("tipoAutorizacion")==null)?"":request.getParameter("tipoAutorizacion");
	String inTipoPrograma = (request.getParameter("tipoPrograma")==null)?"":request.getParameter("tipoPrograma");
	String csTipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");
	String inPlazoDias = (request.getParameter("plazoDias")==null)?"":request.getParameter("plazoDias");
	String cgCalificacionFinal = (request.getParameter("cgCalificacionFinal")==null)?"":request.getParameter("cgCalificacionFinal");
	String inAntiguedadClientMeses = (request.getParameter("antiguedadClientMeses")==null)?"":request.getParameter("antiguedadClientMeses");
				
	garantias.setcamposParam(claveIf, claveBO, csSolicFondeo, csSolicPartiRiesgo, cgPorcentajeParti, 
										cgPropositoProyec, inClaveTipoCred, cgClaveGarantia, cgPorcenPartiFinan, inPorcenProdMerInter, 
										inPorcenProdMerExter, inFormaAmortizacion, cgMoneda, inNumMesesGraciaFinan, inClaveFuncionarioFaculIf, 
										inTipoAutorizacion, inTipoPrograma, csTipoTasa, inPlazoDias, cgCalificacionFinal, inAntiguedadClientMeses);	
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
} else if (informacion.equals("generarArhivoCsv")){
	JSONObject jsonObj = new JSONObject();
	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer("");	
	String 	nombreArchivo = "";	
	
	String claveIf = (request.getParameter("claveIf")==null)?"":request.getParameter("claveIf");
	String claveBO = (request.getParameter("claveBO")==null)?"":request.getParameter("claveBO");
	
	String nombreIf = (request.getParameter("nombreIf")==null)?"":request.getParameter("nombreIf");
	String nombreBO = (request.getParameter("nombreBO")==null)?"":request.getParameter("nombreBO");
	
	HashMap resultado = garantias.getCamposParametricos(claveIf, claveBO);
	
	String solicFondeo = resultado.get("solicFondeo")==null?"":(String)resultado.get("solicFondeo");
	String solicPartiRiesgo = resultado.get("solicPartiRiesgo")==null?"":(String)resultado.get("solicPartiRiesgo");
	String porcentajeParti = resultado.get("porcentajeParti")==null?"":(String)resultado.get("porcentajeParti");
	String propositoProyec = resultado.get("propositoProyec")==null?"":(String)resultado.get("propositoProyec");
	String claveTipoCred = resultado.get("claveTipoCred")==null?"":(String)resultado.get("claveTipoCred");
	String claveGarantia = resultado.get("claveGarantia")==null?"":(String)resultado.get("claveGarantia");
	String porcenPartiFinan = resultado.get("porcenPartiFinan")==null?"":(String)resultado.get("porcenPartiFinan");
	String porcenProdMerInter = resultado.get("porcenProdMerInter")==null?"":(String)resultado.get("porcenProdMerInter");
	String porcenProdMerExter = resultado.get("porcenProdMerExter")==null?"":(String)resultado.get("porcenProdMerExter");
	String formaAmortizacion = resultado.get("formaAmortizacion")==null?"":(String)resultado.get("formaAmortizacion");
	String moneda = resultado.get("moneda")==null?"":(String)resultado.get("moneda");
	String numMesesGraciaFinan = resultado.get("numMesesGraciaFinan")==null?"":(String)resultado.get("numMesesGraciaFinan");
	String claveFuncionarioFaculIf = resultado.get("claveFuncionarioFaculIf")==null?"":(String)resultado.get("claveFuncionarioFaculIf");
	String tipoAutorizacion = resultado.get("tipoAutorizacion")==null?"":(String)resultado.get("tipoAutorizacion");
	String tipoPrograma = resultado.get("tipoPrograma")==null?"":(String)resultado.get("tipoPrograma");
	String tipoTasa = resultado.get("tipoTasa")==null?"":(String)resultado.get("tipoTasa");
	String plazoDias = resultado.get("plazoDias")==null?"":(String)resultado.get("plazoDias");
	String cgCalificacionFinal = resultado.get("cgCalificacionFinal")==null?"":(String)resultado.get("cgCalificacionFinal");
	String antiguedadClientMeses = resultado.get("antiguedadClientMeses")==null?"":(String)resultado.get("antiguedadClientMeses");
	
	if(operacion.equals("CSV")){
	
		nombreIf = nombreIf.replace(',', ' ');
		contenidoArchivo.append("Intermediario Financiero: "+nombreIf+"\r\n");
		
		nombreBO = nombreBO.replace(',', ' ');
		contenidoArchivo.append("Base de Operación: "+nombreBO+"\r\n\r\n");
		
		contenidoArchivo.append("\"Solicita Fondeo NAFIN\",");
		contenidoArchivo.append("\"Solicita participación en Riesgo\",");
		contenidoArchivo.append("\"% Participación\",");
		contenidoArchivo.append("\"Propósito Proyecto\",");
		contenidoArchivo.append("\"Clave Tipo Financiamiento\",");
		contenidoArchivo.append("\"Clave Garantía\",");
		contenidoArchivo.append("\"% Participación Financiamiento\",");
		contenidoArchivo.append("\"% Producción Mercado Interno\",");
		contenidoArchivo.append("\"% Producción Mercado Externo\",");
		contenidoArchivo.append("\"Forma de Amortización\",");
		contenidoArchivo.append("\"Moneda\",");
		contenidoArchivo.append("\"No. Meses de gracia del Financiamiento\",");
		contenidoArchivo.append("\"Clave Funcionario facultado del IF\",");
		contenidoArchivo.append("\"Tipo Autorización\",");
		contenidoArchivo.append("\"Tipo Programa\",");
		contenidoArchivo.append("\"Tipo Tasa\",");
		contenidoArchivo.append("\"Plazo en días\",");
		contenidoArchivo.append("\"Calificación Final\",");
		contenidoArchivo.append("\"Antigüedad Cliente en meses\",\"\"\r\n");
		
		contenidoArchivo.append("\""+solicFondeo+"\",");
		contenidoArchivo.append("\""+solicPartiRiesgo+"\",");
		contenidoArchivo.append("\""+porcentajeParti+"\",");
		contenidoArchivo.append("\""+propositoProyec+"\",");
		contenidoArchivo.append("\""+claveTipoCred+"\",");
		contenidoArchivo.append("\""+claveGarantia+"\",");
		contenidoArchivo.append("\""+porcenPartiFinan+"\",");
		contenidoArchivo.append("\""+porcenProdMerInter+"\",");
		contenidoArchivo.append("\""+porcenProdMerExter+"\",");
		contenidoArchivo.append("\""+formaAmortizacion+"\",");
		contenidoArchivo.append("\""+moneda+"\",");
		contenidoArchivo.append("\""+numMesesGraciaFinan+"\",");
		contenidoArchivo.append("\""+claveFuncionarioFaculIf+"\",");
		contenidoArchivo.append("\""+tipoAutorizacion+"\",");
		contenidoArchivo.append("\""+tipoPrograma+"\",");
		contenidoArchivo.append("\""+tipoTasa+"\",");
		contenidoArchivo.append("\""+plazoDias+"\",");
		contenidoArchivo.append("\""+cgCalificacionFinal+"\",");
		contenidoArchivo.append("\""+antiguedadClientMeses+"\",");
		
		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			//log.error("Error al generar el archivo");
		} else {
			nombreArchivo = strDirecVirtualTemp+archivo.nombre;
		}
	}
	
	if(operacion.equals("PDF")){//IMPRIMIR
		nombreArchivo = archivo.nombreArchivo()+".pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),(session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString(),
								 (String)session.getAttribute("sesExterno"), (String) session.getAttribute("strNombre"), (String) session.getAttribute("strNombreUsuario"),
								 (String)session.getAttribute("strLogo"),strDirectorioPublicacion);

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
		pdfDoc.setTable(2,80);
		pdfDoc.setCell("Nombre del Campo","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Valor","celda01",ComunesPDF.CENTER,1);
		
		pdfDoc.setCell("Solicita Fondeo NAFIN","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(solicFondeo,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Solicita participación en Riesgo","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(solicPartiRiesgo,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("% Participación","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(porcentajeParti,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Propósito Proyecto","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(propositoProyec,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Clave Tipo Financiamiento","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(claveTipoCred,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Clave Garantía","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(claveGarantia,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("% Participación Financiamiento","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(porcenPartiFinan,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("% Producción Mercado Interno","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(porcenProdMerInter,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("% Producción Mercado Externo","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(porcenProdMerExter,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Forma de Amortización","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(formaAmortizacion,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Moneda","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(moneda,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("No. Meses de gracia del Financiamiento","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(numMesesGraciaFinan,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Clave Funcionario facultado del IF","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(claveFuncionarioFaculIf,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Tipo Autorización","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(tipoAutorizacion,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Tipo Programa","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(tipoPrograma,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Tipo Tasa","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Plazo en días","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(plazoDias,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Calificación Final","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(cgCalificacionFinal,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Antigüedad Cliente en meses","formas",ComunesPDF.LEFT);
		pdfDoc.setCell(antiguedadClientMeses,"formas",ComunesPDF.LEFT);
		
		pdfDoc.addTable();
		pdfDoc.endDocument();
		
		nombreArchivo = strDirecVirtualTemp+nombreArchivo;
	}
	
	jsonObj.put("success", new Boolean(true));
   jsonObj.put("urlArchivo", nombreArchivo);
	infoRegresar = jsonObj.toString();
}
%>


<%=infoRegresar%>