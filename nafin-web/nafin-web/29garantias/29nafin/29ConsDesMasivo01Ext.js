//29ConsDesMasivo01Ext.js

Ext.onReady(function() {

//Ext.onReady(function(){
var programa = "Test ";//(document.getElementById("nombrePrograma").value);
//});
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = store.reader.jsonData;
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			if(store.getTotalCount() > 0) {//////////Verifica que existan registros
				//Ext.getCmp('btnArchivoPDF').enable();
				//Ext.getCmp('btnArchivoCSV').enable();
				el.unmask();					
			} else {	
				//Ext.getCmp('btnArchivoPDF').disable();
				//Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '29ConsDesMasivo01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'CLAVE_IF'},
			{	name: 'NOMBRE_IF'},	
			{	name: 'FECHA'},
			{	name: 'SITUACION'},
			{	name: 'MONTO_ENVIADO'},
			{	name: 'IN_REGISTROS_ACEP'},
			{	name: 'IN_REGISTROS_RECH'},
			{	name: 'IN_REGISTROS'},
			{	name: 'CLAVESIAG'},
			{	name: 'FOLIO'},
			{	name: 'IC_SITUACION'}

		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	var catalogoIfData = new Ext.data.JsonStore({
		id: 'catalogoIfData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29ConsDesMasivo01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoSituacionData = new Ext.data.JsonStore({
		id: 'catalogoSituacionData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29ConsDesMasivo01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaSituacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
	var programa = new Ext.Container ({
		layout: 'table',
		id: 'labelPrograma',
		width: '481',
		heigth: 'auto',
		style: 'margin: 0 auto',
		align: 'center',
		defaults:{
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
			{
				xtype: 'displayfield',
				///<b>'+programa+'</b></div><br><br>
				value: '<div align=center style="width:481px;"><div align=center> <font size="3"><b>Solicitud de Desembolsos Masivos de Garantias - Consulta de Resultados</b></font></div>'
			}
		]
	});
	var elementosForma=[
		{
			xtype:'combo',
			id:	'cboNombreIF',
			name:	'nombreIF',
			fieldLabel: 'Nombre del IF',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'nombreIF',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			width: 240,
			minChars : 1,
			allowBlank: true,
			store : catalogoIfData,
			tpl :  NE.util.templateMensajeCargaComboConDescripcionCompleta
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Operaci�n',
			combineErrors: false,
			
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaOperacionMin',
					id: 'txtFechaOperacionMin',
					allowBlank: true,
				//	startDay: 0,
				//	vtype: 'rangofecha',
					width: 90,
					minValue		: '01/01/1901',
					msgTarget: 'side',
					//vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaOperacionMax',
				//	tabIndex			: 11,
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},{
					xtype: 'displayfield',
					value: ' ',
					width: 10
					
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 10,margins:{top:0, right:10, bottom:0, left:-15}
					
				},
				{
					xtype: 'datefield',
					name: 'fechaOperacionMax', 
					id: 'txtFechaOperacionMax',
					allowBlank: true,
				//	startDay: 1,
				//	vtype: 'rangofecha',
					width: 90,
					minValue		: '01/01/1901',
					msgTarget: 'side',
					//vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaOperacionMin',
					//tabIndex			: 12,
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype:'combo',
			id:	'cboSutiacion',
			name:	'situacion',
			fieldLabel: 'Situaci�n',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'situacion',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			width: 240,
			minChars : 1,
			allowBlank: true,
			store : catalogoSituacionData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			//xtype : 'numberfield',
			xtype : 'textfield', 
			name :'folio_operacion',
			id:'folio',
			maskRe : /^\d+$/,
			regex: /^\d+$/,
			fieldLabel: 'Folio de Operaci�n',
			maxLength : 20
			
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		//title: 'Solicitud de Desembolsos Masivos de<br> Garantias - Consulta de Resultados',
		frame: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 160,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		titleCollapse: true,
		items: elementosForma,			
		monitorValid: true,
		buttons: [
		{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,	
				handler: function(boton, evento) {
				
				 
				if(Ext.getCmp('txtFechaOperacionMin').getValue()=="" & Ext.getCmp('txtFechaOperacionMax').getValue()!=""){
					Ext.getCmp('txtFechaOperacionMin').markInvalid('Ingrese la fecha.');
					return;
				} else if( Ext.getCmp('txtFechaOperacionMin').getValue() !="" & Ext.getCmp('txtFechaOperacionMax').getValue() =="" ){
					Ext.getCmp('txtFechaOperacionMax').markInvalid('Ingrese la fecha.');
					return;
				} 
				if(Ext.getCmp('txtFechaOperacionMin').getValue() > Ext.getCmp('txtFechaOperacionMax').getValue()){
					Ext.getCmp('txtFechaOperacionMin').markInvalid('La fecha inicial no puede ser mayor que la final.');
					return;
				}
				if(Ext.getCmp('txtFechaOperacionMin').isValid() & Ext.getCmp('txtFechaOperacionMax').isValid()){
					Ext.getCmp('txtFechaOperacionMin').clearInvalid();
					Ext.getCmp('txtFechaOperacionMax').clearInvalid();
				}
					fp.el.mask('Procesando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion : 'Consultar',
							operacion : 'Generar',
							start:0,
							limit :15
						})
					}); 
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					//window.location = '29Consulta05RGDAdmGExt.jsp'; 
					Ext.getCmp('forma').getForm().reset();
					Ext.getCmp('txtFechaOperacionMin').setValue('01/01/1901');
					Ext.getCmp('txtFechaOperacionMax').setValue('01/01/1901');
					Ext.getCmp('forma').getForm().reset();
					Ext.getCmp('gridConsulta').hide();
				}
			}
		]
	});
	//grid de resultados de la consulta
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		//title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 115,			
				resizable: true,				
				align: 'center'/*,
				renderer:function(value){
					return value.replace('[','');
				}		*/	
			},
			{
				header: 'Fecha y Hora<br>de Operaci�n',
				tooltip: 'Fecha y Hora de Operaci�n',
				dataIndex: 'FECHA',
				sortable: true,
				width: 115,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Folio de la <br>Operaci�n',
				tooltip: 'Folio de la <br>Operaci�n',
				dataIndex: 'FOLIO',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return value;
				}
			},
			{
				header: 'Situaci�n',
				tooltip: 'Situaci�n',
				dataIndex: 'SITUACION',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('IC_SITUACION') ==1 ){
						 return 'ENVI� PARA VALIDACI�N Y CARGA';
					} else if(registro.get('IC_SITUACION') ==5 ){
						 return 'CONFIRMACI�N';
					} else {
							 return value;
					}
				}
			},
			{
				header: 'Monto Enviado',
				tooltip: 'Monto Enviado',
				dataIndex: 'MONTO_ENVIADO',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=rigth >'+value+'</div>';
				}
			}	,
			{
				header: 'N�mero de<br>operaciones<BR>aceptadas',
				tooltip: 'N�mero de operaciones aceptadas',
				dataIndex: 'IN_REGISTROS_ACEP',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return value;
				}
			}	,
			{
				header: 'N�mero de<br>operaciones<BR>con errores',
				tooltip: 'N�mero de operaciones con errores',
				dataIndex: 'IN_REGISTROS_RECH',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return value;
				}
			}	,
			{
				header: 'Total de<br>Operaciones',
				tooltip: 'Total de Operaciones',
				dataIndex: 'IN_REGISTROS',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return value;
				}
			}	,
			{
				header: 'Archivo<br>Origen',
				tooltip: 'Archivo Origen',
				xtype: 'actioncolumn',
				sortable: true,
				width: 114,
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								//this.items[0].tooltip = 'Autorizar';
								return 'iconoLupa';	
						},	handler: function(grid, rowIndex, colIndex, item, event){
							var reg= grid.getStore().getAt(rowIndex);
							var informacion ='Archivos';
							var opcion ='origen';
							var folio = reg.get('FOLIO');
							var claveIF = reg.get('CLAVE_IF');
								Ext.Ajax.request({
									url: '29ConsDesMasivo01Ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{							
										informacion: 'Archivos',
										opcion:opcion,
										folio:folio,
										claveIF:claveIF
									}),
									success : function(response) {
										//boton.setIconClass('icoXls');     
										//boton.setDisabled(false);
									},	
									callback: procesarDescargaArchivos
								});	
							//Ext.Msg.alert('','Descargar Archivo Origen.txt '+opcion+' _ '+folio+' _ '+claveIF);
						}
					}
				],
				resizable: true,				
				align: 'center'
			}	,
			{
				header: 'Resultados',
				tooltip: 'Resultados',
				xtype: 'actioncolumn',
				dataIndex:'IC_SITUACION',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('IC_SITUACION') !=5  ||  registro.get('IC_SITUACION') !=8){
						 return '';
					}
				},
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( (registro.get('IC_SITUACION') ==5  ||  registro.get('IC_SITUACION') ==8) ){
								//this.items[0].tooltip = 'Autorizar';
								return 'iconoLupa';	
							}
						},	handler: function(grid, rowIndex, colIndex, item, event){
							var reg= grid.getStore().getAt(rowIndex);
							var informacion ='Archivospdf';
							var opcion ='CONSREC';
							var folio = reg.get('FOLIO');
							var claveIF = reg.get('CLAVE_IF');
							var claveSIAG = reg.get('CLAVESIAG');
							grid.el.mask('Procesando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '29ConsDesMasivo01Ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{							
										informacion: 'Archivospdf',
										opcion:opcion,
										folio:folio,
										claveIF:claveIF,
										claveSiag:claveSIAG 
									}),
									success : function(response) {
									grid.el.unmask();
										//boton.setIconClass('icoXls');     
										//boton.setDisabled(false);
									},	
									callback: procesarDescargaArchivos
								});	
						}
					}
				]
			}	,
			{
				header: 'Archivo<br>de<br> Errores',
				tooltip: 'Archivo de Errores',
				dataIndex: 'IN_REGISTROS_RECH',
				xtype: 'actioncolumn',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('IN_REGISTROS_RECH') ==0){
						 return '';
					}
				},
				items:[
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( registro.get('IN_REGISTROS_RECH') !=0  ){
								//this.items[0].tooltip = 'Autorizar';
								return 'iconoLupa';	
							}
						},	handler: function(grid, rowIndex, colIndex, item, event){
							var reg= grid.getStore().getAt(rowIndex);
							var informacion ='Archivos';
							var opcion ='errores';
							var folio = reg.get('FOLIO');
							var claveIF = reg.get('CLAVE_IF');
								Ext.Ajax.request({
									url: '29ConsDesMasivo01Ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{							
										informacion: 'Archivos',
										opcion:opcion,
										folio:folio,
										claveIF:claveIF
									}),
									success : function(response) {
										//boton.setIconClass('icoXls');     
										//boton.setDisabled(false);
									},	
									callback: procesarDescargaArchivos
								});	
						}
					}
				]
			}								
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: true/*,
		bbar: ['->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',
					hidden: false,
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '29ConsDesMasivo01Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoCSV'
							}),
							success : function(response) {
								boton.setIconClass('icoXls');     
								boton.setDisabled(false);
							},	
							callback: procesarDescargaArchivos
						});						
					}
				},
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '29ConsDesMasivo01Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoPDF'
							}),
							success : function(response) {
								boton.setIconClass('icoPdf');     
								boton.setDisabled(false);
							},
							callback: procesarDescargaArchivos
						});						
					}
				}
		]*/
	});

	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 947,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			programa,
			NE.util.getEspaciador(10),
			fp,		
			NE.util.getEspaciador(10),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	catalogoSituacionData.load();
	catalogoIfData.load();

});	
