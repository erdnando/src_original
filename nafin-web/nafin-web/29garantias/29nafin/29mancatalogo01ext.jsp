<%@ page import="java.util.*" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<% 
	if( esEsquemaExtJS ){
		String idMenu = (request.getParameter("idMenu") == null)?"":request.getParameter("idMenu");
		response.sendRedirect(appWebContextRoot + "/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu="+idMenu);
	} else {
		response.sendRedirect(appWebContextRoot + "/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?parametros=GPROTPAG|GPAG");
	}
%>