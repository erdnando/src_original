<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>

<%-- El jsp que contiene el dise�o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/29garantias/29nafin/29camposparam_ext.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/00utils/extjs/ux/GroupSummary.js"></script>
<link rel="stylesheet" href="/nafin/00utils/extjs/ux/GroupSummary.css">
</head>

<%@ include file="/01principal/menu.jspf"%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%if(esEsquemaExtJS) {%>
	
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:230px"></div></div>
		</div>
	</div>
	
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
	<input type="hidden" id="hidStrUsuario" value="<%=strTipoUsuario%>">
	
<%}else  {%>
	<div id="areaContenido"><div style="height:190px"></div></div>
	
	<form id='formAux' name="formAux" target='_new'></form>
	<!-- Valores iniciales recibidos como parametros-->
	<input type="hidden" id="hidStrUsuario" value="<%=strTipoUsuario%>">
<%}%>
</body>
</html>