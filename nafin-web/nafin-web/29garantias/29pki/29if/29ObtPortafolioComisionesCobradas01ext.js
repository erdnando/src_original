var showPanelLayoutDeCarga;

Ext.onReady(function() {

	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			// w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth, hidden ? 0 : dom.clientWidth || me.getComputedWidth() ) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

	//----------------------------------- HANDLERS ------------------------------------

	var procesaObtenerPortafolio = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						obtenerPortafolio(resp.estadoSiguiente,resp);
					}
				);
			} else {
				obtenerPortafolio(resp.estadoSiguiente,resp);
			}

		} else {

			// Mostrar mensaje de error
			NE.util.mostrarConnError(
				response,
				opts,
				function(){

					var gridPortafolios = Ext.getCmp("gridPortafolios");

					if( gridPortafolios.generandoArchivo && !Ext.isEmpty(gridPortafolios.recordId) ){

						// Obtener registro
						var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(gridPortafolios.recordId);
						if( Ext.isEmpty(record) ){
							return;
						}

						// Deshabilitar flag que indica que hay una consulta en proceso
						gridPortafolios.generandoArchivo = false;
						gridPortafolios.recordId			= null;

						// Suprimir animacion
						if( record.data['DESCARGA_DETALLADA_LOADING'] ){
							record.data['DESCARGA_DETALLADA_LOADING'] 	= false;
						}
						gridPortafolios.getView().refresh();

					}

				}
			);

		}

	}

	//----------------------------- "MAQUINA DE ESTADO" -------------------------------

	var fnExtracFirmadaCallback = function(vpkcs7, vtextoFirmar, vrecordId, vclaveMesConciliar,
					vclaveFiso, vclaveIfSiag, vclaveTipoConciliacion, vanio, vmes, vfechaActual){

	 // Determinar el estado siguiente
		Ext.Ajax.request({
			url: 								'29ObtPortafolioComisionesCobradas01ext.data.jsp',
			params: {
				informacion:				'ObtenerPortafolio.firmarExtraccion',
				recordId:					vrecordId,
				claveMesConciliar:			vclaveMesConciliar,
				claveFiso: 					vclaveFiso,
				claveIfSiag: 				vclaveIfSiag,
				claveTipoConciliacion:		vclaveTipoConciliacion,
				anio: 						vanio,
				mes: 						vmes,
				isEmptyPkcs7: 				Ext.isEmpty(vpkcs7),
				pkcs7: 						vpkcs7,
				textoFirmado: 				vtextoFirmar,
				fechaActual:				vfechaActual
			},
			callback: 						procesaObtenerPortafolio
		});
	}

	var obtenerPortafolio = function(estado, respuesta ){

		if(			estado == "INICIALIZACION"					){

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ObtPortafolioComisionesCobradas01ext.data.jsp',
				params: 	{
					informacion:		'ObtenerPortafolio.inicializacion'
				},
				callback: 				procesaObtenerPortafolio
			});

		} else if(  estado == "VALIDA_NUMERO_FISO"         ){

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 							'29ObtPortafolioComisionesCobradas01ext.data.jsp',
				params: 	{
					informacion:			'ObtenerPortafolio.validaNumeroFiso',
					estadoFinal:			respuesta.estadoFinal,
					formaConsultaValues:	Ext.isDefined(respuesta.formaConsultaValues) ?Ext.encode(respuesta.formaConsultaValues):undefined
				},
				callback: 					procesaObtenerPortafolio
			});

		} else if(  estado == "MOSTRAR_AVISO"             ){

			if(!Ext.isEmpty(respuesta.aviso)){

				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);

				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();

			}

		} else if(	estado == "MOSTRAR_PORTAFOLIO"		  ){

			Ext.getCmp("panelPortafolios").show();

			// Habilitar grid y mostrar mensaje de espera mientras se cargan los registros
			var gridPortafolios = Ext.getCmp("gridPortafolios");
			gridPortafolios.enable();
			gridPortafolios.getGridEl().mask("Cargando...",'x-mask-loading');

			// Extraer portafolio
			var portafoliosDataStore = Ext.StoreMgr.key("portafoliosDataStore");
			portafoliosDataStore.load();

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ObtPortafolioComisionesCobradas01ext.data.jsp',
				params: 	{
					informacion:		'ObtenerPortafolio.mostrarPortafolio'
				},
				callback: 				procesaObtenerPortafolio
			});

		} else if(  estado == "CONSULTA_PORTAFOLIO" ){

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ObtPortafolioComisionesCobradas01ext.data.jsp',
				params: 	{
					informacion:			'ObtenerPortafolio.consultaPortafolio',
					formaConsultaValues:	Ext.encode(respuesta.formaConsultaValues)
				},
				callback: 					procesaObtenerPortafolio
			});

      } else if(  estado == "ESPERAR_DECISION"  			){

      	var gridPortafolios = Ext.getCmp("gridPortafolios");
      	if( !Ext.isEmpty( respuesta.recordId ) && gridPortafolios.generandoArchivo ){

      		// Obtener registro
      		var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);
      		if(Ext.isEmpty(record)){
      			return;
      		}

      		// Deshabilitar flag que indica que hay una consulta en proceso
      		gridPortafolios.generandoArchivo = false;
      		gridPortafolios.recordId			= null;

      		// Suprimer animacion
      		if( record.data['DESCARGA_DETALLADA_LOADING'] ){
      			record.data['DESCARGA_DETALLADA_LOADING'] = false;
      		}
      		gridPortafolios.getView().refresh();

      	}

      	// Este es el unico estado que no pasa por el jsp, para determinar
      	// el estado siguiente... se hace as� para reducir la complejidad del
      	// codigo
      	return;

		} else if(	estado == "DESCARGA_DETALLADA"		){

      	var gridPortafolios  = Ext.getCmp("gridPortafolios");

      	// Generar Texto a Firmar
      	var record				= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);

      	// Indicar que hay una consulta en proceso
      	gridPortafolios.generandoArchivo = true;
      	gridPortafolios.recordId			= respuesta.recordId;

      	// Mostrar animacion
      	record.data['DESCARGA_DETALLADA_LOADING'] = true;
      	gridPortafolios.getView().refresh();

         // Determinar el estado siguiente
			Ext.Ajax.request({
				url: 								'29ObtPortafolioComisionesCobradas01ext.data.jsp',
				params: {
					informacion:				'ObtenerPortafolio.descargaDetallada',
					recordId:					respuesta.recordId,
					claveMesConciliar:		record.data['CLAVE_MES_CONCILIAR'],
					claveFiso: 					record.data['CLAVE_FISO'],
					claveIfSiag: 				record.data['CLAVE_IF_SIAG'],
					claveTipoConciliacion:	record.data['CLAVE_TIPO_CONCILIACION']
				},
				callback: 						procesaObtenerPortafolio
			});

		} else if(  estado == "FIRMAR_EXTRACCION"						){

			var gridPortafolios  = Ext.getCmp("gridPortafolios");

      	// Generar Texto a Firmar
      	var record				= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);

      	// Firmar contenido del portafolio a extraer
      	var textoFirmar	= 	"";
      	var pkcs7 			=	null;


      	// Construir texto a firmar
      	textoFirmar += "Tipo de Operaci�n: "   + respuesta.tipoOperacion					         + "\n";
      	textoFirmar += "A�o de Conciliaci�n: " + record.data["ANIO"].format("Y")					+ "\n";
      	textoFirmar += "Mes Conciliaci�n: "    + mesRenderer( record.data["MES"], {}, record )	+ "\n";
      	textoFirmar += "Fecha de Descarga: "   + respuesta.fechaActual;

			// Solicitar al usuario que firme el texto
			NE.util.obtenerPKCS7(fnExtracFirmadaCallback, textoFirmar, respuesta.recordId, respuesta.claveMesConciliar,
					respuesta.claveFiso, respuesta.claveIfSiag, respuesta.claveTipoConciliacion,
					record.data["ANIO"].format("Y"), record.data["MES"], respuesta.fechaActual);

		} else if(	estado == "MOSTRAR_DETALLE_FIRMA_EXTRACCION"		){

			var gridPortafolios	= Ext.getCmp("gridPortafolios");
			var usuarioDescarga	= respuesta.datosFirmaExtraccion.usuarioDescarga;
      	var fechaDescarga		= Ext.isEmpty(respuesta.datosFirmaExtraccion.fechaDescarga)?null:Date.parseDate(respuesta.datosFirmaExtraccion.fechaDescarga, 'd/m/Y');

      	// Actualizar fecha y usuario de descarga
      	var record				= Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);
      	record.set('FECHA_DESCARGA',   fechaDescarga   );
      	record.set('USUARIO_DESCARGA', usuarioDescarga );
      	record.commit();

      	// Actualizar registros
      	gridPortafolios.getView().refresh();

      	// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 								'29ObtPortafolioComisionesCobradas01ext.data.jsp',
				params: {
					informacion:				'ObtenerPortafolio.mostrarDetalleFirmaExtraccion',
					recordId:					respuesta.recordId,
					claveMesConciliar:		respuesta.claveMesConciliar,
					claveFiso: 					respuesta.claveFiso,
					claveIfSiag: 				respuesta.claveIfSiag,
					claveTipoConciliacion:	respuesta.claveTipoConciliacion
				},
				callback: 						procesaObtenerPortafolio
			});

		} else if(	estado == "EXTRAER_PORTAFOLIO_NAFIN"		){

      	// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 								'29ObtPortafolioComisionesCobradas01ext.data.jsp',
				params: {
					informacion:				'ObtenerPortafolio.extraerPortafolioNafin',
					recordId:					respuesta.recordId,
					claveMesConciliar:		respuesta.claveMesConciliar,
					claveFiso: 					respuesta.claveFiso,
					claveIfSiag: 				respuesta.claveIfSiag,
					claveTipoConciliacion:	respuesta.claveTipoConciliacion
				},
				callback: 						procesaObtenerPortafolio
			});

		} else if(	estado == "MOSTRAR_LINK_DESCARGA_DETALLADA" 	){

			// Obtener detalle del portafolio
      	var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);

      	// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridPortafolios 					= Ext.getCmp("gridPortafolios");
      	gridPortafolios.generandoArchivo = false;
      	gridPortafolios.recordId			= null;

      	// Suprimir animacion
      	record.data['DESCARGA_DETALLADA_LOADING'] 	= false;
      	record.data['DESCARGA_DETALLADA_URL'] 			= respuesta.urlArchivo;

      	// Mostrar  ZIP
      	gridPortafolios.getView().refresh();

		} else if(	estado == "FIN"									){

			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "29ObtPortafolioComisionesCobradas01ext.jsp";
			forma.target	= "_self";
			forma.submit();

		}

		return;

	}

	//-------------------------- PANEL PORTAFOLIOS ----------------------------

	var procesarConsultaPortafolios = function(store, registros, opts){

		var gridPortafolios 				= Ext.getCmp('gridPortafolios');
		var el 								= gridPortafolios.getGridEl();
		el.unmask();

		if (registros != null) {

			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

		}

		// Si ocurri� un error deshabilitar el grid
		if( store == null && registros == null && opts == null){
			gridPortafolios.setDisabled(true);
		}

	}

	var usuarioDescargaRenderer = function( value, metadata, record, rowIndex, colIndex, store){

		metadata.attr = 'style="white-space: normal; word-wrap:break-word;" ';
		return value;

	}

	var mesRenderer = function( value, metadata, record, rowIndex, colIndex, store){

		value 			= Date.monthNames[ record.data['MES']-1 ];
		return value;

	}

	var portafoliosData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'portafoliosDataStore',
		url: 		'29ObtPortafolioComisionesCobradas01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaPortafolios'
		},
		fields: [
			{ name: 'CLAVE_MES_CONCILIAR', 			type: 'string' 	},
			{ name: 'CLAVE_FISO', 						type: 'string' 	},
			{ name: 'CLAVE_IF_SIAG', 					type: 'string' 	},
			{ name: 'CLAVE_TIPO_CONCILIACION', 		type: 'string' 	},
			{ name: 'ANIO', 								type: 'date',   convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'Ym')); } },
			{ name: 'MES', 								type: 'int'			},
			{ name: 'FECHA_DESCARGA', 					type: 'date',   convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } },
			{ name: 'USUARIO_DESCARGA', 				type: 'string'		},
			{ name: 'DESCARGA_DETALLADA_LOADING',	type: 'boolean'	},
			{ name: 'DESCARGA_DETALLADA_URL', 		type: 'string'		},
			{ name: 'EXISTE_PORTAFOLIO_NAFIN',		type: 'boolean' 	}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarConsultaPortafolios,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaPortafolios(null, null, null);
				}
			}
		}

	});

	var elementosPortafolios = [
		{
			store: 		portafoliosData,
			xtype: 		'grid',
			id:			'gridPortafolios',
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			style: {
				borderWidth: 1
			},
			generandoArchivo: false,
			recordId: 			null,
			autoExpandColumn: 'USUARIO_DESCARGA',
			columns: [
				{
					header: 		'A�o de<br>Conciliaci�n',
					tooltip: 	'A�o de Conciliaci�n',
					dataIndex: 	'ANIO',
					sortable: 	true,
					resizable: 	true,
					width: 		80,
					hidden: 		false,
					align:		'center',
					xtype: 		'datecolumn',
					format: 		'Y'
				},
				{
					header: 		'Mes<br>Conciliaci�n',
					tooltip: 	'Mes Conciliaci�n',
					dataIndex: 	'MES',
					sortable: 	true,
					resizable: 	true,
					width: 		80,
					hidden: 		false,
					align:		'center',
					renderer:	mesRenderer
				},
				{
					header: 		'Fecha de<br>Descarga',
					tooltip: 	'Fecha de Descarga',
					dataIndex: 	'FECHA_DESCARGA',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hidden: 		false,
					align:		'center',
					xtype: 		'datecolumn',
					format: 		'd/m/Y' // 'd-F-Y'
				},
				{
					id:			'USUARIO_DESCARGA',
					header: 		'Usuario <br>Descarga',
					tooltip: 	'Usuario Descarga',
					dataIndex: 	'USUARIO_DESCARGA',
					sortable: 	true,
					resizable: 	true,
					//width: 		120,
					hidden: 		false,
					align:		'center',
					renderer:	usuarioDescargaRenderer
				},
				{
					xtype: 		'actioncolumn',
					header: 		'Descarga<br>Detallada',
					tooltip: 	'Descarga Detallada',
					width: 		90,
					hidden: 		false,
					align:		'center',
					items: 		[
							{
								getClass: function(value, metadata, record) {

									if (record.data['DESCARGA_DETALLADA_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['DESCARGA_DETALLADA_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo ZIP';
										 return 'icoZip';
									} else if( record.data['EXISTE_PORTAFOLIO_NAFIN'] ){
										 this.items[0].tooltip = 'Realizar la extracci�n del portafolio';
										 return 'extraerDetalle';
									} else {
										 this.items[0].tooltip = null;
										 return null;
									}

								},
								handler: function(grid, rowIndex, colIndex) {

									var record 			= grid.getStore().getAt(rowIndex);

									// Validar que no haya otra consulta en proceso
									if ( record.data['DESCARGA_DETALLADA_LOADING'] ) { // grid.generandoArchivo

										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;

									} else if ( !Ext.isEmpty(record.data['DESCARGA_DETALLADA_URL']) ){

										var forma    = Ext.getDom('formAux');
										forma.action = NE.appWebContextRoot+'/DescargaArchivo';
										forma.method = 'post';
										forma.target = '_self';
										// Preparar Archivo a Descargar
										var archivo  = record.data['DESCARGA_DETALLADA_URL'];
										archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
										// Insertar archivo
										var inputNombreArchivo = Ext.DomHelper.insertFirst(
											forma,
											{
												tag: 	'input',
												type: 'hidden',
												id: 	'nombreArchivo',
												name: 'nombreArchivo',
												value: archivo
											},
											true
										);
										// Solicitar Archivo Al Servidor
										forma.submit();
										// Remover nodo agregado
										inputNombreArchivo.remove();

									} else if( record.data['EXISTE_PORTAFOLIO_NAFIN'] ){

										// Obtener ID del Registro en cuestion
										var respuesta 				= new Object();
										respuesta['recordId'] 	= record.id;

										// Realizar la descarga del portafolio
										obtenerPortafolio("DESCARGA_DETALLADA", respuesta );

									} else {

										return;

									}

								}

							}
						]
            }
			]
		}
	];

	var panelPortafolios = {
		title:			'Comisiones Cobradas',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelPortafolios',
		width: 			510,
		frame: 			true,
		style: 			'margin: 0 auto',
		items: 			elementosPortafolios
	}

	//------------------------------------ 1. PANEL AVISOS ---------------------------------------

	var elementosPanelAvisos = [
		{
			xtype: 	'label',
			id:	 	'labelAviso',
			cls:		'x-form-item',
			style: 	'font-weight:normal;text-align:center;margin:15px;color:black;',
			html:  	''
		}
	];

	var panelAvisos = {
		xtype:			'panel',
		id: 				'panelAvisos',
		hidden:			true,
		width: 			700,
		title: 			'Avisos',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosPanelAvisos
	}

	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		disabled: false,
		items: 	[
			panelAvisos,
			NE.util.getEspaciador(10),
			panelPortafolios
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------
	obtenerPortafolio("INICIALIZACION", null );

});;