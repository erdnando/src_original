Ext.onReady(function() {

var proceso = Ext.get('proceso').getValue();
var periodicidadAux;
var textoFirmado;
var folio;
//----------------------------------Handlers----------------------------------------
Ext.apply(Ext.form.VTypes,{
	phone: function(value,field){
    return value.replace(/[ \-\(\)]/g,'').length <= 15;
		},
		phoneText: 'El numero telefonico no es valido',
		phoneMask: /[ \d\-\(\)]/
});

var procesoInicial = function(opts, success, response){

		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			if(Ext.util.JSON.decode(response.responseText).mensaje == true){


				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();
				fp.hide();
				//
			}
		}
	}
function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

var fnFirmaCallback = function(vpkcs7, vtextoFirmado, vproceso){
	if (Ext.isEmpty(vpkcs7) || vpkcs7 == 'error:noMatchingCert') {
			Ext.Msg.alert('Firmar','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;	//Error en la firma. Termina...
		}


		Ext.Ajax.request({
			url : '29pagog01Ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{informacion: 'GuardaDatos',proceso: vproceso,pkcs7: vpkcs7, textoFirmado: vtextoFirmado}),
			callback: procesaFirma
	});
}

var firma= function(){

	NE.util.obtenerPKCS7(fnFirmaCallback, textoFirmado, proceso);

}

var procesaFirma = function(opts,success,response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fp.hide();
			folio=Ext.util.JSON.decode(response.responseText).folioSolicitud;
			Ext.getCmp("labelAutentificacion").setText(folio,false);
			Ext.getCmp("panelAcuseCargaArchivo").show();
			registrosAgregadosData.loadData(Ext.util.JSON.decode(response.responseText).registrosAgregadosDataArray)

	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

var procesarImagen = function(grid, rowIndex, colIndex, item, event) {
		var registro = storeImg.getAt(rowIndex);
					Ext.Ajax.request({
							url: '29pagog01Ext.data.jsp',
							params: Ext.apply({informacion:'ConsultarImagen',proceso: proceso,claveImagen: registro.get('IC_IMAGEN_SOLPAGO') }),
							callback: procesarArchivoSuccess
						});
	}

var procesarEliminar = function(grid, rowIndex, colIndex, item, event) {
		var registro = storeImg.getAt(rowIndex);



					Ext.Ajax.request({
							url: '29pagog01Ext.data.jsp',
							params: Ext.apply({informacion:'EliminarImagen',proceso: proceso,claveImagen: registro.get('IC_IMAGEN_SOLPAGO') }),
							callback: procesarEliminarSuccess
						});
	}
var procesarEliminarSuccess = function(opts, success, response){

		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			storeImg.loadData(Ext.util.JSON.decode(response.responseText).registros);
			if(Ext.util.JSON.decode(response.responseText).total==0){
					Ext.getCmp('panelGrid').setVisible(false);
				}else{
					Ext.getCmp('panelGrid').setVisible(true);
					 Ext.EventManager.onWindowResize(Ext.getCmp('panelGrid').doLayout(), Ext.getCmp('panelGrid'));
			}
		}
	}

var validarFechas = function(opts, success, response){

		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
		if(Ext.util.JSON.decode(response.responseText).CLAVE == false){
			if(Ext.util.JSON.decode(response.responseText).mensaje == true){


				Ext.Msg.confirm('',
											'Las operaciones quedar�n registradas con fecha valor del siguiente d�a h�bil : '+Ext.util.JSON.decode(response.responseText).valorFecha,
											function(botonConf) {
												if (botonConf == 'ok' || botonConf == 'yes') {
													firma();
												}
											}
										);
			}else{firma();}
		}else{
				Ext.getCmp('claveFin').markInvalid('La clave: '+Ext.getCmp('claveFin').getValue()+' ya se encuentra registrada y en proceso, con el folio: '+
						Ext.util.JSON.decode(response.responseText).FOLIO );
			}
		}
	}
var procesarSucnocessFailureGenerarArchivo = function(opts, success, response){
		verDetalles.show();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnDetalleF= Ext.getCmp('detalleF');


			if(Ext.util.JSON.decode(response.responseText).urlArchivo==''){
				btnDetalleF.hide();
			}else{
				btnDetalleF.setHandler(function(boton,evento){
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					//alert(Ext.util.JSON.decode(response.responseText).urlArchivo);
					forma.submit();
				});
			}
			if(Ext.util.JSON.decode(response.responseText).urlArchivo==''){
							NE.util.mostrarConnError(response,opts);

				}

		}
	}

//-----------------------------------STORES-------------------------------------------
var catalogoPeriodicidad = new Ext.data.JsonStore
  ({
	   id: 'catologoPeriodicidad',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29pagog01Ext.data.jsp',
		baseParams:
		{
		 informacion: 'CatalogoPeriodicidad'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  var catalogoImagen = new Ext.data.JsonStore
  ({
	   id: 'catologoImagen',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29pagog01Ext.data.jsp',
		baseParams:
		{
		 informacion: 'CatalogoImagen'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

  var storeImg = new Ext.data.JsonStore({
		fields: [
			{name:'IC_TIPO_IMAGEN'},{name:'CG_DESCRIPCION'},{name:'IC_IMAGEN_SOLPAGO'}
			],
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
//----------------------------------COMPONENTES--------------------------------------------


var elementosPanelAvisos = [
		{
			xtype: 	'label',
			id:	 	'labelAviso',
			cls:		'x-form-item',
			style: 	'font-weight:normal;text-align:center;margin:15px;color:black;',
			html:  	'No se tiene definido un Fideicomiso para realizar sus operaciones.<br/><br/>Por favor comun�quese al Centro de Atenci�n a clientes al tel�fono 50-89-61-07 <br/>o del Interior al 01-800-NAFINSA (01-800-6234672).'
		}
	];

	var panelAvisos = {
		xtype:			'panel',
		id: 				'panelAvisos',
		hidden:			true,
		width: 			700,
		title: 			'Avisos',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosPanelAvisos
	}


//ACUSE
var procesarSuccessFailureGeneraArchivoPDF =  function(opts, success, response) {

		var botonImprimirPDF = Ext.getCmp('botonImprimirPDF');
		botonImprimirPDF.enable();

		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			botonImprimirPDF.setIconClass('icoPdf');
			botonImprimirPDF.setText('Descargar PDF');

			botonImprimirPDF.urlArchivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			botonImprimirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});

		} else {

			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						botonImprimirPDF.setIconClass('icoImprimir');
						botonImprimirPDF.setText('Imprimir PDF');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				botonImprimirPDF.setIconClass('icoImprimir');
				botonImprimirPDF.setText('Imprimir PDF');
				NE.util.mostrarConnError(response,opts);
			}

		}

	}

	var procesarConsultaRegistrosAgregados = function(store, registros, opts){

		var gridRegistrosAgregados 			= Ext.getCmp('gridRegistrosAgregados');

		if (registros != null) {

			var el 							= gridRegistrosAgregados.getGridEl();

			if(store.getTotalCount() > 0) {

				el.unmask();

			} else {

				el.mask('Se present� un error al leer los par�metros', 'x-mask');

			}

		}

	}

	var registrosAgregadosData = new Ext.data.ArrayStore({

		storeId: 'registrosAgregadosDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosAgregados,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosAgregados(null, null, null);
				}
			}
		}

	});

	var renderContenidoGridAcuse = function(value, metaData, record, rowIndex, colIndex, store) {

		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: left !important';
		} else {
			metaData.style += 'text-align: right !important';
		}
		return value;

	}

	var elementosAcuseCargaArchivo = [
		{
			xtype: 	'label',
			id:	 	'labelAutentificacion',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:15px;',
			html:  	'Cargando...'
		},
		{
			xtype: 	'label',
			id:	 	'labelAcuseCifrasControl',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Datos del acuse'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		173,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'west'
					},
					{
						store: 			registrosAgregadosData,
						xtype: 			'grid',
						id:				'gridRegistrosAgregados',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '80%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		185,
								hidden: 		false,
								hideable:	false,
								fixed:		true
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								align:		'right',
								renderer: 	renderContenidoGridAcuse
							}
						]
					},
					{
						xtype: 	'box',
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'east'
					}
			]
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			items: [
				{
					xtype: 		'button',
					text: 		'Imprimir PDF',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoImprimir',
					id: 			'botonImprimirPDF',
					urlArchivo: '',
					handler:    function(boton, evento) {

						if(Ext.isEmpty(boton.urlArchivo)){

							// Cambiar icono
							boton.disable();
							boton.setIconClass('loading-indicator');

							// Genera Archivo PDF
							//CONTENIDO
							Ext.Ajax.request({
								url: 			'29pagog01Ext.data.jsp',
								params: 		{
									informacion: 		'GeneraArchivoPDF',
									folio:	folio,
									garantia:			registrosAgregadosData.getAt(1).get('CONTENIDO'),
									hora:			registrosAgregadosData.getAt(3).get('CONTENIDO'),
									fechaValor:			registrosAgregadosData.getAt(4).get('CONTENIDO'),
									fecha:			registrosAgregadosData.getAt(2).get('CONTENIDO')
								},
								callback: 	procesarSuccessFailureGeneraArchivoPDF
							});

						// Descargar el archivo generado
						} else {

							var forma 		= Ext.getDom('formAux');
							forma.action 	= boton.urlArchivo;
							forma.submit();

						}

					},
					style: {
						width: 137
					}
				},
				{
					xtype: 		'button',
					text: 		'Salir',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoAceptar',
					id: 			'botonSalir',
					handler:    function(boton, evento) {
						location.reload();
					},
					style: {
						width: 137
					}
				}
			]
		}
	];

	var panelAcuseCargaArchivo = {
		title:			'<center>Solicitud de Pago de Garant�a</center>',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelAcuseCargaArchivo',
		width: 			650, //949,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosAcuseCargaArchivo
	}

//FIN ACUSE


var gridImagenes = new Ext.grid.GridPanel({
				id: 'gridImg',
				hidden: false,
				header: true,
				title: '<center>Im�genes Cargadas</center>',
				store: storeImg,
				stripeRows: false,
				//loadMask: true,
				height: 135,
				width: 270,
				enableColumnResize: false,
				frame: true,
				columns:[
				//CAMPOS DEL GRID
						{
						xtype: 'actioncolumn',
						width: 50,
						menuDisabled: true,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';
							       return 'iconoLupa';
								 }
								 ,handler: procesarImagen
					      }]
			       },
						{

						dataIndex: 'CG_DESCRIPCION',
						width: 150,
						align: 'center',
						menuDisabled: true,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
					{
						xtype: 'actioncolumn',
						menuDisabled: true,
						width: 50,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Eliminar';
							       return 'borrar';
								 }
								 ,handler: procesarEliminar
					      }]
			       }
				]

		});
var elementosImagen= [
		{
						xtype: 'combo',
						fieldLabel: 'Tipo de Imagen',
						emptyText: 'Seleccionar...',
						displayField: 'descripcion',
						allowBlank: false,
						valueField: 'clave',
						triggerAction: 'all',
						anchor:'60%',
						typeAhead: true,
						minChars: 1,
						store: catalogoImagen,
						tpl: NE.util.templateMensajeCargaCombo,
						name:'Himg',
						id: 'img',
						mode: 'local',
						hiddenName: 'Himg',
						forceSelection: true

					},{
				  xtype: 		'fileuploadfield',
				  id: 			'archivo',
				  name: 			'archivo',
				  emptyText: 	'Seleccione...',
				  fieldLabel: 	"Ruta del Archivo de Registros",
				  buttonText: 	null,
				  buttonCfg: {
					  iconCls: 	'upload-icon'
				  },
				  anchor: 		'-20'
				  //vtype: 		'archivotxt'
				},
				{
					xtype: 		'panel',
					anchor: 		'100%',
					style: {
						marginTop: 		'10px'
					},
					layout: {
						type: 'hbox',
						pack: 'end',
						align: 'middle'
					},
					items: [
						{
							xtype: 			'button',
							text: 			'Aceptar',
							iconCls: 		'icoContinuar',
							id: 				'botonContinuarCargaArchivo',
							handler:    function(boton, evento) {

								// Revisar si la forma es invalida
								var forma = fpImg.getForm();
								if(!forma.isValid()){
									return;
								}

								// Validar que se haya especificado un archivo
								var archivo = Ext.getCmp("archivo");
								if( Ext.isEmpty( archivo.getValue() ) ){
									archivo.markInvalid("Debe especificar un archivo");
									return;
								}

								// Revisar el tipo de extension del archivo
								var myRegexGIF = /^.+\.([gG][iI][fF])$/;
								var myRegexPDF = /^.+\.([pP][dD][fF])$/;
								var myRegexTIF = /^.+\.([tT][iI][fF])$/;

								var nombreArchivo 	= archivo.getValue();


								if( !myRegexGIF.test(nombreArchivo) && !myRegexPDF.test(nombreArchivo) && !myRegexTIF.test(nombreArchivo)) {
									archivo.markInvalid("El formato del archivo de origen no es el correcto. Formatos soportados: GIF, PDF, TIF");
									return;
								}

								// Cargar archivo
								fpImg.getForm().submit({
								clientValidation: 	true,
								url: 						'29pagog01Ext.data.jsp?informacion=SubirArchivo&proceso='+proceso+'&Himg='+Ext.getCmp('img').getValue(),
								waitMsg:   				'Subiendo archivo...',
								waitTitle: 				'Por favor espere',
								success: function(form, action) {
									storeImg.loadData(action.result.registros);
									if(action.result.total==0){

										Ext.getCmp('panelGrid').setVisible(false);
									}else{
										Ext.getCmp('panelGrid').setVisible(true);
										Ext.EventManager.onWindowResize(fpImg.doLayout(), fpImg);

									}
									Ext.getCmp("archivo").reset();
								},
								failure: function(form, action) {
									 action.response.status = 200;
									 Ext.MessageBox.alert('Error',action.result.mensaje!=undefined?action.result.mensaje:action.result.msg);
								}
							});

							}
						}/*,{
							xtype: 'button',
							text: 			'Regresar',
							handler:    function(boton, evento) {
								verImagen.setVisible(false);
							}
						}*/
					]
				},{
					xtype: 		'panel',
					anchor: 		'100%',
					hidden: false,
					id: 'panelGrid',
					//title:	'<center></center>',
					/*style: {
						marginTop: 		'6px'
					},*/
					layout: {
						type: 'hbox',
						pack: 'center'
						//,
						//align: 'middle'
					},
					items: [gridImagenes]
					}


]
var elementosForma= [
					{
						xtype: 'textfield',
						name: 'claveFin',
						id: 'claveFin',
						fieldLabel: 'Clave del Financiamiento',
						maxLength: 20,
						allowBlank: false,
						anchor:'60%',
						msgTarget: 			'side'
					},{
						xtype: 'combo',
						fieldLabel: 'Periodicidad de Pago',
						emptyText: 'Seleccionar...',
						displayField: 'descripcion',
						allowBlank: false,
						valueField: 'clave',
						triggerAction: 'all',
						anchor:'60%',
						typeAhead: true,
						minChars: 1,
						store: catalogoPeriodicidad,
						tpl: NE.util.templateMensajeCargaCombo,
						name:'HperPago',
						id: 'perPago',
						mode: 'local',
						hiddenName: 'HperPago',
						forceSelection: true,
						listeners: {
									select: {
										fn: function(combo) {
											var seleccion=combo.getValue();
											var lista=seleccion.split("-");
											var flagPeriodicidadPago = lista[1];
											if(flagPeriodicidadPago == "N"){
												periodicidadAux = "N";
												Ext.getCmp('fechaInc2').allowBlank=true;
												Ext.getCmp('fechaInc2').disable();
												Ext.getCmp('fechaInc2').reset();
											 }else{
												periodicidadAux= "S";
												Ext.getCmp('fechaInc2').allowBlank=false;
												Ext.getCmp('fechaInc2').enable();

											 }
										}
									}
							}
					},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha del Primer<br/>Incumplimiento(dd/mm/aaaa)',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fechaInc1',
								id: 'fechaInc1',
								allowBlank: false,
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoFinFecha: 'fechaInc2',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'Fecha del Segundo<br/>Incumplimiento(dd/mm/aaaa):',
								width: 180
							},
							{
								xtype: 'datefield',
								name: 'fechaInc2',
								id: 'fechaInc2',
								allowBlank: false,
								startDay: 1,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha',
								campoInicioFecha: 'fechaInc1',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					},{
						xtype: 'textarea',
						name: 'causas',
						id: 'causas',
						allowBlank: false,
						fieldLabel: 'Causas de Incumplimiento',
						maxLength: 150,
						anchor:'82%',
						msgTarget: 			'side'
					},{
						xtype: 'panel',
						title: 'Funcionario asignado para supervisi�n:'
					},{
						xtype: 'displayfield',
						html: '<br/>'
					},{
						xtype: 'textfield',
						name: 'nombre',
						id: 'nombre',
						allowBlank: false,
						fieldLabel: 'Nombre',
						maxLength: 100,
						anchor:'60%',
						msgTarget: 			'side'
					},{
						xtype: 'textfield',
						name: 'telefono',
						id: 'telefono',
						vtype: 'phone',
						allowBlank: false,
						fieldLabel: 'Tel�fono',
						maxLength: 30,
						anchor:'50%',
						msgTarget: 			'side'
					},{
						xtype: 'numberfield',
						name: 'extension',
						id: 'extension',
						allowBlank: false,
						fieldLabel: 'Extensi�n',
						maxLength: 5,
						anchor:'40%',
						msgTarget: 			'side'
					},{
						xtype: 'textfield',
						vtype: 'email',
						name: 'correo',
						allowBlank: false,
						id: 'correo',
						fieldLabel: 'Correo Electr�nico',
						maxLength: 100,
						anchor:'60%',
						msgTarget: 			'side'
					},{
						xtype: 'panel',
						title: 'Datos de la ubicaci�n donde se realizar� la supervisi�n:'
					},{
						xtype: 'displayfield',
						html: '<br/>'
					},{
						xtype: 'textfield',
						name: 'domicilio',
						allowBlank: false,
						id: 'domicilio',
						fieldLabel: 'Domicilio',
						maxLength: 150,
						anchor:'60%',
						msgTarget: 			'side'
					},{
						xtype: 'textfield',
						name: 'ciudad',
						allowBlank: false,
						id: 'ciudad',
						fieldLabel: 'Ciudad',
						maxLength: 100,
						anchor:'60%',
						msgTarget: 			'side'
					},{
						xtype: 'textfield',
						name: 'estado',
						id: 'estado',
						allowBlank: false,
						fieldLabel: 'Estado',
						maxLength: 100,
						anchor:'60%',
						msgTarget: 			'side'
					},{
						xtype: 'panel',
						title: 'Carga de Imagen:'
					},{
						xtype: 'displayfield',
						html: '<br/>'
					},{
						xtype: 'displayfield',
						hidden: true,
						id: 'errorImg',
						html: '<font color="#990000">Es obligatorio cargar al menos el Estado de cuenta certificado</font>'
					},{
						xtype: 'button',
						name: 'cargaImg',
						id: 'cargaImg',
						text: 'Subir Imagen',
						width: 60,
						style: 'margin:0 auto;',
						anchor:'25%',
						handler: function(){
							verImagen.show();

							if(storeImg.data.length<1){
								Ext.getCmp('panelGrid').setVisible(false);
							}
							Ext.getCmp('errorImg').setVisible(false);
						}
					}
]

var fpImg = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 500,
		width: 750,
		title:	'<Center>Carga de Im�genes de la Solicitud de Pago de Garant�a</Center>',
		collapsible: false,
		titleCollapse: false,
		fileUpload: 	true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'formaImg',
		defaults:
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: [elementosImagen,NE.util.getEspaciador(5)]
	});

var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 750,
		title:	'<Center>Solicitud de Pago de Garant�a</Center>',
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults:
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: [elementosForma,NE.util.getEspaciador(5)],
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Aceptar',
			  name: 'btnBuscar',

			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento)
					{
						Ext.getCmp('errorImg').setVisible(false);
						var banderaImg=false;
						textoFirmado="Tipo de Operaci�n: 03 Solicitud de Pago de Garant�as\n"
						+"No. Garant�a: "+Ext.getCmp('claveFin').getValue();
						storeImg.each(function(record){
							if(record.get('IC_TIPO_IMAGEN')=='1'){
								banderaImg=true;
							}
						});
						if(banderaImg){

							Ext.Ajax.request({
												url: '29pagog01Ext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'ValidaFechas'
												}),
												callback: validarFechas
											});
						}
						else{
							Ext.getCmp('errorImg').setVisible(true);
						}
					}
			 },{
				text:'Cancelar',
				handler: function() {
						 location.reload();
				}
			 }

		]
	});
var panelDetalles = new Ext.form.FormPanel
  ({

		hidden: false,
		height: 'auto',
		width: 'auto',
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 160,
		defaultType: 'textfield',
		frame: true,
		id: 'formaDetalles',
		defaults:
		{
			msgTarget: 'side',
			anchor: '-20'
		},

		monitorValid: true,
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  align: 'center',
			  text: 'Bajar Archivo',
			  id:'detalleF',
			  hidden: false,
			  formBind: true
			 }

		  ]
  });

var verDetalles = new Ext.Window({
			title: 'Bajar Archivo',
			width: 300,
			height: 100,
			maximizable: false,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			maximized: false,
			constrain: true,
			items: [panelDetalles]
});

var verImagen = new Ext.Window({
			width: 750,
			height: 500,
			maximizable: false,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			maximized: false,
			constrain: true,
			items: [fpImg],
			buttons:[
				{
							xtype: 'button',
							text: 			'Regresar',
							handler:    function(boton, evento) {
								verImagen.setVisible(false);
							}
						}
			]
});

	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[fp,verDetalles,verImagen,panelAcuseCargaArchivo,panelAvisos]
	});

	catalogoPeriodicidad.load();
	catalogoImagen.load();
	Ext.Ajax.request({
			url : '29pagog01Ext.data.jsp',
			params: Ext.apply({informacion: 'ValoresIniciales'}),
			callback: procesoInicial
		});
});