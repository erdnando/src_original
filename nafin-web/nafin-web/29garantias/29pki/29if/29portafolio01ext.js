var showPanelLayoutDeCarga;

Ext.onReady(function() {

	//----------------------------------- HANDLERS ------------------------------------

	var procesaMonitoreoPortafolio = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						monitoreoPortafolio(resp.estadoSiguiente,resp);
					}
				);
			} else {
				monitoreoPortafolio(resp.estadoSiguiente,resp);
			}

		} else {

			// Mostrar mensaje de error
			NE.util.mostrarConnError(
				response,
				opts,
				function(){

					var gridPortafolios = Ext.getCmp("gridPortafolios");

					if( gridPortafolios.generandoArchivo && !Ext.isEmpty(gridPortafolios.recordId) ){

						// Obtener registro
						var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(gridPortafolios.recordId);
						if( Ext.isEmpty(record) ){
							return;
						}

						// Deshabilitar flag que indica que hay una consulta en proceso
						gridPortafolios.generandoArchivo = false;
						gridPortafolios.recordId			= null;

						// Suprimir animacion
						if( record.data['EXTRACCION_CONSOLIDADA_LOADING'] ){
							record.data['EXTRACCION_CONSOLIDADA_LOADING'] 	= false;
						} else if (record.data['EXTRACCION_DETALLADA_PDF_LOADING']){
							record.data['EXTRACCION_DETALLADA_PDF_LOADING'] = false;
						} else if (record.data['EXTRACCION_DETALLADA_CSV_LOADING']){
							record.data['EXTRACCION_DETALLADA_CSV_LOADING'] = false;
						}
						gridPortafolios.getView().refresh();

					}

				}
			);

		}

	}

	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var fnExtracConsolidadaCallback = function(vpkcs7, vtextoFirmar, vrecordId, vrecord){
		// Indicar que hay una consulta en proceso
		var gridPortafolios 					= Ext.getCmp("gridPortafolios");
		gridPortafolios.generandoArchivo = true;
		gridPortafolios.recordId			= vrecordId;

		// Mostrar animacion
		vrecord.data['EXTRACCION_CONSOLIDADA_LOADING'] = true;
		gridPortafolios.getView().refresh();

	 	// Determinar el estado siguiente
		Ext.Ajax.request({
			url: 						'29portafolio01ext.data.jsp',
			params: {
				informacion:			'MonitoreoPortafolio.extraccionConsolidada',
				recordId:				vrecordId,
				portafolioExtraido: 	vrecord.data['PORTAFOLIO_EXTRAIDO'],
				anio:					vrecord.data['ANIO'],
				trimestre:				vrecord.data['TRIMESTRE'],
				fechaCorte: 			vrecord.data['FECHA_CORTE'],
				isEmptyPkcs7: 			Ext.isEmpty(vpkcs7),
				pkcs7: 					vpkcs7,
				textoFirmado: 			vtextoFirmar
			},
			callback: 					procesaMonitoreoPortafolio
		});
	}

	var monitoreoPortafolio = function(estado, respuesta ){

		if(			estado == "INICIALIZACION"					){

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29portafolio01ext.data.jsp',
				params: 	{
					informacion:		'MonitoreoPortafolio.inicializacion'
				},
				callback: 				procesaMonitoreoPortafolio
			});

		} else if(	estado == "MOSTRAR_PORTAFOLIO"		  ){

			Ext.getCmp("contenedorPrincipal").enable();

			// Habilitar grid y mostra mensaje de espera mientras se cargan los registros
			var gridPortafolios = Ext.getCmp("gridPortafolios");
			gridPortafolios.enable();
			gridPortafolios.getGridEl().mask("Cargando...",'x-mask-loading');

			// Extraer portafolio
			var portafoliosDataStore = Ext.StoreMgr.key("portafoliosDataStore");
			portafoliosDataStore.load();

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29portafolio01ext.data.jsp',
				params: 	{
					informacion:		'MonitoreoPortafolio.mostrarPortafolio'
				},
				callback: 				procesaMonitoreoPortafolio
			});

      } else if(  estado == "ESPERAR_DECISION"  			){

      	var gridPortafolios = Ext.getCmp("gridPortafolios");
      	if( !Ext.isEmpty( respuesta.recordId ) && gridPortafolios.generandoArchivo ){

      		// Obtener registro
      		var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);
      		if(Ext.isEmpty(record)){
      			return;
      		}

      		// Deshabilitar flag que indica que hay una consulta en proceso
      		gridPortafolios.generandoArchivo = false;
      		gridPortafolios.recordId			= null;

      		// Suprimer animacion
      		if( record.data['EXTRACCION_CONSOLIDADA_LOADING'] ){
      			record.data['EXTRACCION_CONSOLIDADA_LOADING'] 	= false;
      		} else if (record.data['EXTRACCION_DETALLADA_PDF_LOADING']){
      			record.data['EXTRACCION_DETALLADA_PDF_LOADING'] = false;
      		} else if (record.data['EXTRACCION_DETALLADA_CSV_LOADING']){
      			record.data['EXTRACCION_DETALLADA_CSV_LOADING'] = false;
      		}
      		gridPortafolios.getView().refresh();

      	}

      	// Este es el unico estado que no pasa por el jsp, para determinar
      	// el estado siguiente... se hace as� para reducir la complejidad del
      	// codigo
      	return;

		} else if(	estado == "EXTRACCION_CONSOLIDADA"		){

			// Generar Texto a Firmar
			var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);

			// Firmar contenido del portafolio a extraer
			var textoFirmar	= 	"";
			var pkcs7 			=	null;
			if(!record.data['PORTAFOLIO_EXTRAIDO']){

				// Construir texto a firmar
				textoFirmar += "A�o de Calificaci�n|Trimestre Calificaci�n|Total de garant�as sujetas a env�o de calificaci�n|N�mero de garant�as recibidas|N�mero de garant�as No recibidas|Situaci�n de la recepci�n\n";
				textoFirmar += "\n";
				textoFirmar	+= record.data["ANIO"]									+ "|" +
								record.data["TRIMESTRE"]							+ "|" +
								record.data["TOTAL_GARANTIAS"]					+ "|" +
								record.data["NUMERO_GARANTIAS_RECIBIDAS"]		+ "|" +
								record.data["NUMERO_GARANTIAS_NO_RECIBIDAS"]	+ "|" +
								record.data["SITUACION_RECEPCION"]				+ "\n";

					// Solicitar al usuario que firme el texto

					NE.util.obtenerPKCS7(fnExtracConsolidadaCallback, textoFirmar, respuesta.recordId, record);

      		}else{

				fnExtracConsolidadaCallback(pkcs7, textoFirmar, respuesta.recordId, record);

			}

		} else if(	estado == "EXTRACCION_DETALLADA_PDF"	){

			// Obtener detalle del portafolio
      	var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);

      	// Indicar que hay una consulta en proceso
      	var gridPortafolios 					= Ext.getCmp("gridPortafolios");
      	gridPortafolios.generandoArchivo = true;
      	gridPortafolios.recordId			= respuesta.recordId;

      	// Mostrar animacion
      	record.data['EXTRACCION_DETALLADA_PDF_LOADING'] = true;
      	gridPortafolios.getView().refresh();

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29portafolio01ext.data.jsp',
				params: {
					informacion:			'MonitoreoPortafolio.extraccionDetalladaPDF',
					recordId:				respuesta.recordId,
					totalGarantias: 		record.data['TOTAL_GARANTIAS'],
					anio:						record.data['ANIO'],
					trimestre:				record.data['TRIMESTRE'],
					fechaCorte: 			record.data['FECHA_CORTE']
				},
				callback: 					procesaMonitoreoPortafolio
			});

		} else if(	estado == "EXTRACCION_DETALLADA_CSV"	){

			// Obtener detalle del portafolio
      	var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);

      	// Indicar que hay una consulta en proceso
      	var gridPortafolios 					= Ext.getCmp("gridPortafolios");
      	gridPortafolios.generandoArchivo = true;
      	gridPortafolios.recordId			= respuesta.recordId;

      	// Mostrar animacion
      	record.data['EXTRACCION_DETALLADA_CSV_LOADING'] = true;
      	gridPortafolios.getView().refresh();

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 							'29portafolio01ext.data.jsp',
				params: {
					informacion:			'MonitoreoPortafolio.extraccionDetalladaCSV',
					recordId:				respuesta.recordId,
					anio:						record.data['ANIO'],
					trimestre:				record.data['TRIMESTRE'],
					fechaCorte: 			record.data['FECHA_CORTE']
				},
				callback: 					procesaMonitoreoPortafolio
			});

		} else if(	estado == "MOSTRAR_LINK_PDF_EXTRACCION" 	){

			// Obtener detalle del portafolio
      	var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);

      	// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridPortafolios 					= Ext.getCmp("gridPortafolios");
      	gridPortafolios.generandoArchivo = false;
      	gridPortafolios.recordId			= null;

      	// Suprimir animacion
      	record.data['EXTRACCION_CONSOLIDADA_LOADING'] 	= false;
      	record.data['MOSTRAR_LINK_PDF_EXTRACCION'] 		= respuesta.portafolioExtraido;
      	record.data['EXTRACCION_CONSOLIDADA_URL'] 		= respuesta.urlNombreArchivo;

      	// Mostrar  PDF
      	gridPortafolios.getView().refresh();

		} else if(	estado == "MOSTRAR_LINK_PDF_DETALLE" 		){

			// Obtener detalle del portafolio
      	var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);

      	// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridPortafolios 					= Ext.getCmp("gridPortafolios");
      	gridPortafolios.generandoArchivo = false;
      	gridPortafolios.recordId			= null;

      	// Suprimir animacion
      	record.data['EXTRACCION_DETALLADA_PDF_LOADING'] = false;
      	record.data['EXTRACCION_DETALLADA_PDF_URL'] 		= respuesta.urlNombreArchivo;

      	// Mostrar  PDF
      	gridPortafolios.getView().refresh();

		} else if(	estado == "MOSTRAR_LINK_CSV_DETALLE" 		){

			// Obtener detalle del portafolio
      	var record			= 	Ext.StoreMgr.key('portafoliosDataStore').getById(respuesta.recordId);

      	// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridPortafolios 					= Ext.getCmp("gridPortafolios");
      	gridPortafolios.generandoArchivo = false;
      	gridPortafolios.recordId			= null;

      	// Suprimir animacion
      	record.data['EXTRACCION_DETALLADA_CSV_LOADING'] = false;
      	record.data['EXTRACCION_DETALLADA_CSV_URL'] 		= respuesta.urlNombreArchivo;

      	// Mostrar  PDF
      	gridPortafolios.getView().refresh();

		} else if(	estado == "FIN"									){

			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "29portafolio01ext.jsp";
			forma.target	= "_self";
			forma.submit();

		}

		return;

	}

	//-------------------------- PANEL PORTAFOLIOS ----------------------------

	var procesarConsultaPortafolios = function(store, registros, opts){

		var gridPortafolios 				= Ext.getCmp('gridPortafolios');
		var el 								= gridPortafolios.getGridEl();
		el.unmask();

		if (registros != null) {

			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

		}

		// Si ocurri� un error deshabilitar el grid
		if( store == null && registros == null && opts == null){
			gridPortafolios.setDisabled(true);
		}

	}

	var portafoliosData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'portafoliosDataStore',
		url: 		'29portafolio01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaPortafolios'
		},
		fields: [
			{ name: 'ANIO',  										type: 'int'			},
			{ name: 'TRIMESTRE', 								type: 'int'			},
			{ name: 'TOTAL_GARANTIAS', 						type: 'int',	convert: function(value,record){ return (Ext.isEmpty(value)?0:Ext.util.Format.number(value,'0,000')); } },
			{ name: 'NUMERO_GARANTIAS_RECIBIDAS', 			type: 'int',	convert: function(value,record){ return (Ext.isEmpty(value)?0:Ext.util.Format.number(value,'0,000')); } },
			{ name: 'NUMERO_GARANTIAS_NO_RECIBIDAS', 		type: 'int',	convert: function(value,record){ return (Ext.isEmpty(value)?0:Ext.util.Format.number(value,'0,000')); } },
			{ name: 'SITUACION_RECEPCION' 											},
			{ name: 'EXTRACCION_CONSOLIDADA', 				type: 'boolean'	},
			{ name: 'EXTRACCION_DETALLADA_PDF', 			type: 'boolean' 	},
			{ name: 'EXTRACCION_DETALLADA_CSV',				type: 'boolean'   },
			{ name: 'PORTAFOLIO_EXTRAIDO',					type: 'boolean'	},
			{ name: 'FECHA_CORTE'														},
			{ name: 'EXTRACCION_CONSOLIDADA_LOADING', 	type: 'boolean'	},
			{ name: 'EXTRACCION_DETALLADA_PDF_LOADING', 	type: 'boolean'	},
			{ name: 'EXTRACCION_DETALLADA_CSV_LOADING', 	type: 'boolean'	},
			{ name: 'EXTRACCION_CONSOLIDADA_URL' 									},
			{ name: 'EXTRACCION_DETALLADA_PDF_URL' 								},
			{ name: 'EXTRACCION_DETALLADA_CSV_URL' 								}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarConsultaPortafolios,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaPortafolios(null, null, null);
				}
			}
		}

	});

	var elementosPortafolios = [
		{
			store: 		portafoliosData,
			xtype: 		'grid',
			id:			'gridPortafolios',
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			style: {
				borderWidth: 1
			},
			generandoArchivo: false,
			recordId: 			null,
			columns: [
				{
					header: 		'A�o de<br>Calificaci�n<br>&nbsp;',
					tooltip: 	'A�o de Calificaci�n',
					dataIndex: 	'ANIO',
					sortable: 	true,
					resizable: 	true,
					width: 		80,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'Trimestre<br>Calificaci�n<br>&nbsp;',
					tooltip: 	'Trimestre Calificaci�n',
					dataIndex: 	'TRIMESTRE',
					sortable: 	true,
					resizable: 	true,
					width: 		80,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'Total de garant�as<br>sujetas a env�o de<br>calificaci�n',
					tooltip: 	'Total de garant�as sujetas a env�o de calificaci�n',
					dataIndex: 	'TOTAL_GARANTIAS',
					sortable: 	true,
					resizable: 	true,
					width: 		115,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'N�mero<br>de garant�as<br>recibidas',
					tooltip: 	'N�mero de garant�as recibidas',
					dataIndex: 	'NUMERO_GARANTIAS_RECIBIDAS',
					sortable: 	true,
					resizable: 	true,
					width: 		90,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'N�mero<br>de garant�as<br>No recibidas',
					tooltip: 	'N�mero de garant�as No recibidas',
					dataIndex: 	'NUMERO_GARANTIAS_NO_RECIBIDAS',
					sortable: 	true,
					resizable: 	true,
					width: 		90,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'Situaci�n de la<br>recepci�n<br>&nbsp;',
					tooltip: 	'Situaci�n de la recepci�n',
					dataIndex: 	'SITUACION_RECEPCION',
					sortable: 	true,
					resizable: 	true,
					width: 		95,
					hidden: 		false,
					align:		'center'
				},
				{
					xtype: 		'actioncolumn',
					header: 		'Extracci�n<br>consolidada<br>&nbsp;',
					tooltip: 	'Extracci�n consolidada',
					width: 		80,
					hidden: 		false,
					align:		'center',
					items: 		[
							{
								getClass: function(value, metadata, record) {

									if (record.data['EXTRACCION_CONSOLIDADA_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_CONSOLIDADA_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo PDF';
										 return 'icoPdf';
									} else if (record.data['EXTRACCION_CONSOLIDADA'] ) {
										 this.items[0].tooltip = 'Realizar extracci�n consolidada';
										 return 'extraerDetalle';
									} else {
										 return null;
									}

								},
								handler: function(grid, rowIndex, colIndex) {

									// Validar que no haya otra consulta en proceso
									if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}

									var record 			= Ext.StoreMgr.key('portafoliosDataStore').getAt(rowIndex);
									if(!record.data['PORTAFOLIO_EXTRAIDO']){

										var faltanDatos 	= false;
										if(        Ext.isEmpty(record.data["ANIO"])                          ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["TRIMESTRE"])                     ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["TOTAL_GARANTIAS"])               ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["NUMERO_GARANTIAS_RECIBIDAS"])    ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["NUMERO_GARANTIAS_NO_RECIBIDAS"]) ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["SITUACION_RECEPCION"])           ){
											faltanDatos 	= true;
										}

										if( faltanDatos ){
											Ext.Msg.alert("Aviso","No pueden haber campos vac�os en los datos a firmar");
											return;
										}

									}
									// Obtener ID del Registro en cuestion
									var respuesta 				= new Object();
									respuesta['recordId'] 	= record.id;

									if ( !Ext.isEmpty(record.data['EXTRACCION_CONSOLIDADA_URL']) ){

										var forma    = Ext.getDom('formAux');
										forma.action = NE.appWebContextRoot+'/DescargaArchivo';
										forma.method = 'post';
										forma.target = '_self';
										// Preparar Archivo a Descargar
										var archivo  = record.data['EXTRACCION_CONSOLIDADA_URL'];
										archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
										// Insertar archivo
										var inputNombreArchivo = Ext.DomHelper.insertFirst(
											forma,
											{
												tag: 	'input',
												type: 'hidden',
												id: 	'nombreArchivo',
												name: 'nombreArchivo',
												value: archivo
											},
											true
										);
										// Solicitar Archivo Al Servidor
										forma.submit();
										// Remover nodo agregado
										inputNombreArchivo.remove();

									} else {
										// Realizar la extraccion consolidada
										monitoreoPortafolio("EXTRACCION_CONSOLIDADA", respuesta );
									}

								}

							}
						]
            },
				{
					xtype: 		'actioncolumn',
					header: 		'Extracci�n<br>detallada<br>(PDF)',
					tooltip: 	'Extracci�n detallada (PDF)',
					width: 		75,
					hidden: 		false,
					align:		'center',
					items: 		[
							{
								getClass: function(value, metadata, record) {

									if (record.data['EXTRACCION_DETALLADA_PDF_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_DETALLADA_PDF_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo PDF';
										 return 'icoPdf';
									} else if (record.data['EXTRACCION_DETALLADA_PDF'] ) {
										 this.items[0].tooltip = 'Extraer detalle en formato PDF';
										 return 'extraerDetalle';
									} else {
										 return null;
									}

								},
								handler: function(grid, rowIndex, colIndex) {

									// Validar que no haya otra consulta en proceso
									if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}

									// Obtener ID del Registro en cuestion
									var respuesta 				= new Object();
									var record					= Ext.StoreMgr.key('portafoliosDataStore').getAt(rowIndex);
									respuesta['recordId'] 	= record.id;

									if ( !Ext.isEmpty(record.data['EXTRACCION_DETALLADA_PDF_URL']) ){

										var forma    = Ext.getDom('formAux');
										forma.action = NE.appWebContextRoot+'/DescargaArchivo';
										forma.method = 'post';
										forma.target = '_self';
										// Preparar Archivo a Descargar
										var archivo  = record.data['EXTRACCION_DETALLADA_PDF_URL'];
										archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
										// Insertar archivo
										var inputNombreArchivo = Ext.DomHelper.insertFirst(
											forma,
											{
												tag: 	'input',
												type: 'hidden',
												id: 	'nombreArchivo',
												name: 'nombreArchivo',
												value: archivo
											},
											true
										);
										// Solicitar Archivo Al Servidor
										forma.submit();
										// Remover nodo agregado
										inputNombreArchivo.remove();

									} else {
										// Realizar la extraccion consolidada
										monitoreoPortafolio("EXTRACCION_DETALLADA_PDF", respuesta );
									}

								}
							}
						]
            },
            {
					xtype: 		'actioncolumn',
					header: 		'Extracci�n<br>detallada<br>(CSV)',
					tooltip: 	'Extracci�n detallada (CSV)',
					width: 		75,
					hidden: 		false,
					align:		'center',
					items: 		[
							{
								getClass: function(value, metadata, record) {

									if (record.data['EXTRACCION_DETALLADA_CSV_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_DETALLADA_CSV_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo CSV';
										 return 'icoXls';
									} else if (record.data['EXTRACCION_DETALLADA_CSV'] ) {
										 this.items[0].tooltip = 'Extraer detalle en formato CSV';
										 return 'extraerDetalle';
									} else {
										 return null;
									}

								},
								handler: function(grid, rowIndex, colIndex) {

									// Validar que no haya otra consulta en proceso
									if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}

									// Obtener ID del Registro en cuestion
									var respuesta 				= new Object();
									var record					= Ext.StoreMgr.key('portafoliosDataStore').getAt(rowIndex);
									respuesta['recordId'] 	= record.id;

									if ( !Ext.isEmpty(record.data['EXTRACCION_DETALLADA_CSV_URL']) ){

										var forma    = Ext.getDom('formAux');
										forma.action = NE.appWebContextRoot+'/DescargaArchivo';
										forma.method = 'post';
										forma.target = '_self';
										// Preparar Archivo a Descargar
										var archivo  = record.data['EXTRACCION_DETALLADA_CSV_URL'];
										archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
										// Insertar archivo
										var inputNombreArchivo = Ext.DomHelper.insertFirst(
											forma,
											{
												tag: 	'input',
												type: 'hidden',
												id: 	'nombreArchivo',
												name: 'nombreArchivo',
												value: archivo
											},
											true
										);
										// Solicitar Archivo Al Servidor
										forma.submit();
										// Remover nodo agregado
										inputNombreArchivo.remove();

									} else {
										// Realizar la extraccion consolidada
										monitoreoPortafolio("EXTRACCION_DETALLADA_CSV", respuesta );
									}

								}
							}
						]
            }
			]
		}
	];

	var panelPortafolios = {
		title:			'Portafolio a Calificar y Monitoreo de Avance',
		hidden:			false,
		xtype:			'panel',
		id: 				'panelPortafolios',
		width: 			810,
		frame: 			true,
		style: 			'margin: 0 auto',
		items: 			elementosPortafolios
	}

	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		disabled: true,
		items: 	[
			NE.util.getEspaciador(10),
			panelPortafolios
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------
	monitoreoPortafolio("INICIALIZACION", null );

});;