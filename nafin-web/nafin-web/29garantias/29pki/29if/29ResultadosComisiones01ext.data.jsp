<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		java.io.OutputStream,
		java.io.PrintStream,
		java.io.File,
		java.io.BufferedInputStream, 
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("ResultadosComisiones.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;
	
	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
				
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){
 
		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}
	
	// 2. Validar Numero Fiso
	if (!hayAviso && !seguridad.validaNumeroFISO(claveIF) ) {
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se tiene definido un Fideicomiso para realizar sus operaciones.<br/><br/>Por favor comuníquese al Centro de Atención a clientes al teléfono 50-89-61-07 <br/>o del Interior al 01-800-NAFINSA (01-800-6234672).";
		hayAviso				= true;
	}
	
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_CONSULTA";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();

} else if (    informacion.equals("ResultadosComisiones.extraccionArchivoTXTOrigen" )   ){

	JSONObject	resultado				= new JSONObject();
	boolean		success					= true;
	String 		msg 						= "";
	boolean		hayError					= false;
	String 		estadoSiguiente		= null;
	
	String 		recordId 				= (request.getParameter("recordId")			== null)?"":request.getParameter("recordId");
	String 		folioOperacion			= (request.getParameter("folioOperacion")	== null)?"":request.getParameter("folioOperacion");
	String 		claveIF 					= iNoCliente;
 
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
					
	}catch(Exception e){
	 
		success			= false;
		log.error("ResultadosComisiones.extraccionArchivoTXTOrigen(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
 
	// Revisar que se hayan incluido todos los parametros
	if ( recordId.equals("") || folioOperacion.equals("") ) {
			
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "Faltan parámetros, se aborta la operación.";
		hayError				= true;
		
	}
 
	// Generar Archivo TXT
	String 	urlNombreArchivo 	= null;
	if(!hayError){
		
		try {

			// Obtener query que se utilizara para consulta del detalle de las comisiones
			HashMap query			= garantias.getQueryComisionesArchivoOriginal( claveIF, folioOperacion );
 
			String nombreArchivo = ExtraccionResultadosComisiones.generaArchivoTXTOrigen( strDirectorioTemp, query );
			urlNombreArchivo 	   = appWebContextRoot + "/DescargaArchivo?nombreArchivo=" + strDirectorioTempRelativePath + nombreArchivo;
			estadoSiguiente		= "MOSTRAR_LINK_ARCHIVO_TXT_ORIGEN";
			
		}catch(Exception e){
			
			log.error("ResultadosComisiones.extraccionArchivoTXTOrigen(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "ESPERAR_DECISION";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			urlNombreArchivo	= "";
			hayError				= true;
			
		}
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)					);
	resultado.put("msg", 					msg										);
	resultado.put("estadoSiguiente", 	estadoSiguiente  						);
	resultado.put("urlNombreArchivo",	urlNombreArchivo 						);
	resultado.put("recordId",				recordId									);
	infoRegresar = resultado.toString();

} else if (    informacion.equals("ResultadosComisiones.extraccionArchivoPDFResultados" )   ){
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		msg 					= "";
	boolean		hayError				= false;
	String 		estadoSiguiente	= null;
	String 		urlNombreArchivo 	= null;
	
	String 		recordId 			= (request.getParameter("recordId")			== null)?"":request.getParameter("recordId");
	String 		folioOperacion		= (request.getParameter("folioOperacion")	== null)?"":request.getParameter("folioOperacion");
	String 		claveIF				= iNoCliente;

	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
					
	}catch(Exception e){
	 
		success			= false;
		log.error("ResultadosComisiones.extraccionArchivoPDFResultados(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// Validar que se hayan proporcionado los parametros necesarios
	if ( recordId.equals("") || folioOperacion.equals("") ) {
			
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "Faltan parámetros, se aborta la operación.";	
		hayError				= true;
		
	} 
	
	// Generar Archivo PDF
	if( !hayError ){
		
		try {
			
			// Obtener query que se utilizara para consulta del detalle de las comisiones
			HashMap	queries			= garantias.getQueriesComisionesArchivoResultados( claveIF, folioOperacion );
			
			// Agregar cabecera
			HashMap 	cabecera 		= new HashMap();
			cabecera.put("strPais", 				(String) session.getAttribute("strPais"));
			cabecera.put("iNoNafinElectronico", (String) ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()));
			cabecera.put("sesExterno", 			(String) session.getAttribute("sesExterno"));
			cabecera.put("strNombre", 				(String) session.getAttribute("strNombre"));
			cabecera.put("strNombreUsuario", 	(String) session.getAttribute("strNombreUsuario"));
			cabecera.put("strLogo", 				(String) session.getAttribute("strLogo"));
 
			String nombreArchivo 	= ExtraccionResultadosComisiones.generaArchivoPDFResultados( strDirectorioTemp, strDirectorioPublicacion, queries, cabecera );
			urlNombreArchivo 	   	= appWebContextRoot + "/DescargaArchivo?nombreArchivo=" + strDirectorioTempRelativePath + nombreArchivo;
			estadoSiguiente			= "MOSTRAR_LINK_ARCHIVO_PDF_RESULTADOS";
			
		}catch(Exception e){
			
			log.error("ResultadosComisiones.extraccionArchivoPDFResultados(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "ESPERAR_DECISION";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			urlNombreArchivo	= "";
			hayError				= true;
			
		}
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("msg", 					msg						);
	resultado.put("estadoSiguiente", 	estadoSiguiente  		);
	resultado.put("urlNombreArchivo",	urlNombreArchivo 		);
	resultado.put("recordId",				recordId					);
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("ResultadosComisiones.extraccionArchivoTXTErrores" )   ){	
 
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		msg 					= "";
	boolean		hayError				= false;
	String 		estadoSiguiente	= null;
	String 		urlNombreArchivo 	= null;
	
	String 		recordId 			= (request.getParameter("recordId")			== null)?"":request.getParameter("recordId");
	String 		folioOperacion		= (request.getParameter("folioOperacion")	== null)?"":request.getParameter("folioOperacion");
	String 		claveIF				= iNoCliente;
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
					
	}catch(Exception e){
	 
		success			= false;
		log.error("ResultadosComisiones.extraccionArchivoTXTErrores(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// Validar que se hayan proporcionado los parametros necesarios
	if ( recordId.equals("") || folioOperacion.equals("") ) {
			
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "Faltan parámetros, se aborta la operación.";	
		hayError				= true;
		
	} 
	
	// Generar Archivo TXT
	if( !hayError ){
		
		try {
			
			// Obtener query que se utilizara para consulta del detalle de las comisiones
			HashMap	query				= garantias.getQueryComisionesArchivoErrores( claveIF, folioOperacion );
 
			String nombreArchivo 	= ExtraccionResultadosComisiones.generaArchivoTXTErrores( strDirectorioTemp, query );
			urlNombreArchivo 	   	= appWebContextRoot + "/DescargaArchivo?nombreArchivo=" + strDirectorioTempRelativePath + nombreArchivo;
			estadoSiguiente			= "MOSTRAR_LINK_ARCHIVO_TXT_ERRORES";
			
		}catch(Exception e){
			
			log.error("ResultadosComisiones.extraccionArchivoTXTErrores(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "ESPERAR_DECISION";
			urlNombreArchivo	= "";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			hayError				= true;
			
		}
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("msg", 					msg						);
	resultado.put("estadoSiguiente", 	estadoSiguiente  		);
	resultado.put("urlNombreArchivo",	urlNombreArchivo 		);
	resultado.put("recordId",				recordId					);
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("ResultadosComisiones.fin" )                      ){

	// Nota: no se tiene contemplado, llegar hasta aqui desde el cliente, pero se deja
	// por compatibilidad.
	
	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("29ResultadosComisiones01ext.jsp");
	
} else if (    informacion.equals("ConsultaResultadosCarga")									)  {
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String 		claveIF				= iNoCliente;

	String 		fechaOperacionDe	= (request.getParameter("fechaOperacionDe") == null)?"":request.getParameter("fechaOperacionDe");
	String 		fechaOperacionA	= (request.getParameter("fechaOperacionA")  == null)?"":request.getParameter("fechaOperacionA");
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
					
	}catch(Exception e){
	 
		success			= false;
		log.error("ConsultaResultadosCarga(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// Consultar Resultados
	List 		resultados	 = garantias.getResultadosCargaComisiones(claveIF,fechaOperacionDe,fechaOperacionA);
 
	// Construir respuesta
	JSONArray registros	 = new JSONArray();
	for(int indice=0;indice<resultados.size();indice++){
		
		HashMap 	resultadoCarga 					= (HashMap) resultados.get(indice);
 
		String 	fechaHoraOperacion 				= (String) resultadoCarga.get("fechaHoraOperacion");
		String 	folioOperacion 					= (String) resultadoCarga.get("folioOperacion");
		String 	situacion 							= (String) resultadoCarga.get("situacion");
		String 	numeroOperacionesAceptadas 	= (String) resultadoCarga.get("numeroOperacionesAceptadas");
		String 	numeroOperacionesConErrores 	= (String) resultadoCarga.get("numeroOperacionesConErrores");
		String 	totalOperaciones 					= (String) resultadoCarga.get("totalOperaciones");

		String 	extraccionArchivoOrigen			= (String) resultadoCarga.get("extraccionArchivoOrigen");
		String 	extraccionArchivoResultados	= (String) resultadoCarga.get("extraccionArchivoResultados");
		String 	extraccionArchivoErrores		= (String) resultadoCarga.get("extraccionArchivoErrores");
 
		JSONObject	registro = new JSONObject();
		registro.put("FECHA_HORA_OPERACION",						fechaHoraOperacion								);
		registro.put("FOLIO_OPERACION",								folioOperacion										);
		registro.put("SITUACION",										situacion											);
		registro.put("NUMERO_OPERACIONES_ACEPTADAS",				numeroOperacionesAceptadas						);
		registro.put("NUMERO_OPERACIONES_CON_ERRORES",			numeroOperacionesConErrores					);
		registro.put("TOTAL_OPERACIONES",							totalOperaciones									);
		registro.put("EXTRACCION_ARCHIVO_ORIGEN",					new Boolean(extraccionArchivoOrigen)		);
		registro.put("EXTRACCION_ARCHIVO_RESULTADOS",			new Boolean(extraccionArchivoResultados)	);
		registro.put("EXTRACCION_ARCHIVO_ERRORES",				new Boolean(extraccionArchivoErrores)		);
		registro.put("EXTRACCION_ARCHIVO_ORIGEN_LOADING",		new Boolean("false")								);
		registro.put("EXTRACCION_ARCHIVO_RESULTADOS_LOADING",	new Boolean("false")								);
		registro.put("EXTRACCION_ARCHIVO_ERRORES_LOADING",		new Boolean("false")								);
		registro.put("EXTRACCION_ARCHIVO_ORIGEN_URL",			""														);
		registro.put("EXTRACCION_ARCHIVO_RESULTADOS_URL",		""														);
		registro.put("EXTRACCION_ARCHIVO_ERRORES_URL",			""														);
		registros.add(registro);
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)					);
	resultado.put("total",    	String.valueOf(registros.size()) );
	resultado.put("registros",	registros			 					);
	
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%><%=infoRegresar%>