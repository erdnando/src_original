Ext.onReady(function(){
var opcion='';
//------------------Handlers--------------------
var procesarConsultaData = function(store,arrRegistros,opts){
			if(arrRegistros!=null){
				var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
function procesaConsulta(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('descarga');
			//btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			verDescarga.setVisible(true);
			btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
var procesarRechazo = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		
					Ext.Ajax.request({
							url: '29ConsResemMext.data.jsp',
							params: Ext.apply({informacion:'Archivos',opcion:'rechazo',folio:registro.get('FOLIO') }),
							callback: procesarArchivoSuccess
						});
	}
var procesarOrigen = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		
					Ext.Ajax.request({
							url: '29ConsResemMext.data.jsp',
							params: Ext.apply({informacion:'Archivos',opcion:'origen',folio:registro.get('FOLIO') }),
							callback: procesarArchivoSuccess
						});
	}
var procesarResultados = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		
					Ext.Ajax.request({
							url: '29ConsResemMext.data.jsp',
							params: Ext.apply({informacion:'ArchivoResultado',folio:registro.get('FOLIO') }),
							callback: procesarArchivoSuccess
						});
	}
//------------------Stores----------------------
	var catologoSituacion = new Ext.data.JsonStore
  ({
	   id: 'catologoSituacion',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29ConsResemMext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoSituacion'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError
		}
  });
  
var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '29ConsResemMext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'FECHA'},
				{name: 'FOLIO'},
				{name: 'SITUACION'},
				{name: 'MONTO'},
				{name: 'ACEPTADAS'},
				{name: 'RECHAZADAS'},
				{name: 'TOTAL'},
				{name: 'ORIGEN'},
				{name: 'RESULTADO'},
				{name: 'ERROR'},
				{name: 'IC_SITUACION'}

	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

//---------------Componentes--------------------
var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				bodyStyle: 'padding: 6px',
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						
						header:'Fecha y Hora <br/>de Operaci�n',
						tooltip: 'Fecha y Hora <br/>de Operaci�n',
						sortable: true,
						dataIndex: 'FECHA',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'Folio de la Operaci�n',
						tooltip: 'Folio de la Operaci�n',
						sortable: true,
						dataIndex: 'FOLIO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Situaci�n',
						tooltip: 'Situaci�n',
						sortable: true,
						dataIndex: 'SITUACION',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Monto Enviado',
						tooltip: 'Monto Enviado',
						sortable: true,
						dataIndex: 'MONTO',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						header: 'N�mero de <br/>Operaciones Aceptadas',
						tooltip: 'N�mero de <br/>Operaciones Aceptadas',
						sortable: true,
						dataIndex: 'ACEPTADAS',
						width: 100,
						align: 'center'
						
						},
						{
						align:'center',
						header: 'N�mero de <br/>Operaciones con errores',
						tooltip: 'N�mero de <br/>Operaciones con errores',
						sortable: true,
						dataIndex: 'RECHAZADAS',
						width: 100
						},{
						
						header:'Total de <br/>Operaciones',
						tooltip: 'Total de <br/>Operaciones',
						sortable: true,
						dataIndex: 'TOTAL',
						width: 100,
						align: 'center'
						},
						{
						xtype: 'actioncolumn',
						header: 'Archivo Origen',
						tooltip: 'Archivo Origen',
						sortable: true,
						//dataIndex: 'ARCHIVO',
						width: 120,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							       return 'iconoLupa';	
								 }
								 ,handler: procesarOrigen
					      }]
						},
						{
						xtype: 'actioncolumn',
						header: 'Archivo de Resultados',
						tooltip: 'Archivo de Resultados',
						sortable: true,
						//dataIndex: 'IMPORTEDOCTO',
						width: 120,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';	
									 if(registro.get('IC_SITUACION')==5||registro.get('IC_SITUACION')==8){
							       return 'iconoLupa';	
									 }
									 else{
									 return '';
									 }
								 }
								 ,handler: procesarResultados
					      }]
						},
						{
						xtype: 'actioncolumn',
						header: 'Archivo de Errores ',
						tooltip: 'Archivo de Errores ',
						sortable: true,
						//dataIndex: 'IMPORTEDSCTO',
						width: 120,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							        if(registro.get('RECHAZADAS')!=''){
							       return 'iconoLupa';	
									 }
									 else{
									 return '';
									 }
								 }
								 ,handler: procesarRechazo
					      }]
						}

				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true
		});


var elementosForma = [
	{
			 xtype: 'compositefield',
			 fieldLabel: 'Fecha de Operaci�n',
			 combineErrors: false,
			 forceSelection: false,
			 msgTarget: 'side',
			 items: [
            {
			    // Fecha Inicio
			   xtype: 'datefield',
				name: 'dFechaOper1',
				id: 'dFechaOper1',
				vtype: 'rangofecha',
				campoFinFecha: 'dFechaOper2',
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 margins: '0 20 0 0'
         },
			   {
				 xtype: 'displayfield',
				 value: 'a',
				 width: 50
			   },
		     {
			    // Fecha Final
			   xtype: 'datefield',
				name: 'dFechaOper2',
				id: 'dFechaOper2',
				vtype: 'rangofecha',
				campoInicioFecha: 'dFechaOper1',
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 margins: '0 20 0 0'

         },
				 {
				 xtype: 'displayfield',
				 value: '(dd/mm/aaaa)',
				 width: 40
			  }
      ]
	 },
	 {
				//EPO
				xtype: 'combo',
				fieldLabel: 'Situaci�n',
				emptyText: 'Seleccionar',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catologoSituacion,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'situacion',
				id: 'situacion',
				mode: 'local',
				anchor:'80%',
				hiddenName: 'Hsituacion',
				forceSelection: true
    },{
				xtype: 'numberfield',
				name: 'folio',		
				fieldLabel: 'Folio de Operaci�n',
				id: 'folio',
				anchor:'80%',
				maxLength: 25
				}
]


var panelDetalles = new Ext.form.FormPanel
  ({
	
		hidden: false,
		height: 'auto',
		width: 'auto',
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 160,
		defaultType: 'textfield',
		frame: true,
		id: 'forma5',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		
		monitorValid: true,
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  align: 'center',
			  text: 'Descargar Archivo',
			  id:'descarga',
			  hidden: false,
			  formBind: true
			 }
		  ]
  });

var verDescarga = new Ext.Window({
			title: 'Descarga Archivo',
			width: 200,
			id:'ventana1',
			height: 100,
			maximizable: false,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			maximized: false,
			constrain: true,
			items: [panelDetalles]
});
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		title: '<center>Solicitud de Desembolsos Masivos de Garantias - Consulta de Resultados</center>',
		width: 600,
		labelWidth: 130,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						
						//Mi acci�n	
						
						if((Ext.getCmp('dFechaOper1').getValue()==''&&Ext.getCmp('dFechaOper2').getValue()!='')||(Ext.getCmp('dFechaOper1').getValue()!=''&&Ext.getCmp('dFechaOper2').getValue()=='')){
									Ext.MessageBox.alert('Error','Debe de capturar ambas fechas para usar este criterio de b�squeda');
						}else{
									grid.setVisible(true);
									consulta.load({ params: Ext.apply(fp.getForm().getValues())});
						}
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					location.reload();
				}
			 }
			 
		]
	});

//----------------Principal---------------------
var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		940,
		height: 		'auto',
		disabled: 	false,
		items: 	[fp,NE.util.getEspaciador(30),grid]
	});
catologoSituacion.load();

});