<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.exception.*, 
		com.netro.garantias.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("MonitoreoPortafolio.inicializacion") )	{
	
	JSONObject	resultado					= new JSONObject();
	boolean		success						= true;
	
	// Realizar actividades de inicializacion
	// Actividad 1. Remover variables de sesion
	session.removeAttribute("rgprint");
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success));
	resultado.put("estadoSiguiente", 	"MOSTRAR_PORTAFOLIO" );
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("MonitoreoPortafolio.mostrarPortafolio") 		){
	
	JSONObject	resultado					= new JSONObject();
	boolean		success						= true;

	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success));
	resultado.put("estadoSiguiente", 	"ESPERAR_DECISION"  );
	infoRegresar = resultado.toString();
	
} else if ( 	 informacion.equals("MonitoreoPortafolio.extraccionConsolidada")  ){
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	
	String recordId 				= (request.getParameter("recordId")					== null)?""			:request.getParameter("recordId");
	String portafolioExtraido 	= (request.getParameter("portafolioExtraido")	== null)?"false"	:request.getParameter("portafolioExtraido");
	String anio 					= (request.getParameter("anio")						== null)?""			:request.getParameter("anio");
	String trimestre 				= (request.getParameter("trimestre")				== null)?""			:request.getParameter("trimestre");
	String fechaCorte 			= (request.getParameter("fechaCorte")				== null)?""			:request.getParameter("fechaCorte");
	String isEmptyPkcs7 			= (request.getParameter("isEmptyPkcs7")			== null)?"false"	:request.getParameter("isEmptyPkcs7");
	String pkcs7 					= (request.getParameter("pkcs7")						== null)?""			:request.getParameter("pkcs7").trim();
	String textoFirmado 			= (request.getParameter("textoFirmado")			== null)?""			:request.getParameter("textoFirmado").trim();
 
	String 	externContent 		= textoFirmado;
	char 		getReceipt 			= 'Y';
	boolean	hayError				= false;
	String 	claveIF				= iNoCliente;
	String 	estadoSiguiente	= null;

	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
					
	}catch(Exception e){
	 
		success			= false;
		log.error("MonitoreoPortafolio.extraccionConsolidada(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// El portafolio no ha sido extraido
	if ( anio.equals("") || trimestre.equals("") || fechaCorte.equals("") ) {
			
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "Faltan parámetros, se aborta la operación.";
		hayError				= true;
		
	// El usuario canceló la operacion de firmado... abortar la operación y no mostrar ningun error
	} else if ( 
		"false".equals(portafolioExtraido) 	&&  
		"true".equals(isEmptyPkcs7)			&&
		!"".equals(externContent) 	
	){
		
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "";
		hayError				= true;
		
	} else if ( 

		"false".equals(portafolioExtraido) 	&& 
		!_serial.equals("") 						&& 
		"false".equals(isEmptyPkcs7) 			&& 
		!"".equals(pkcs7) 						&&
		!"".equals(externContent) 				 
		
	) {
 
		String 		folioCert   = "01CC"+claveIF+new SimpleDateFormat("ddMMyyHHmm").format(new java.util.Date());
		Seguridad 	s 				= new Seguridad();
		
		try {
			
			// Autenticar texto firmado
			if (!s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {	
				msg 						= "La autentificacion no se llevo a cabo: \n"+s.mostrarError();
				estadoSiguiente 		= "ESPERAR_DECISION";
				hayError					= true;
			}		
			
			// Actualizar estatus del portafolio a extraido
			if( !hayError ){
				garantias.actualizaStatusRecepcionPortafolioCalificacion(claveIF,anio,trimestre,fechaCorte,"02");
				portafolioExtraido	= "true";
			}
		
		}catch(Exception e){
			
			log.error("MonitoreoPortafolio.extraccionConsolidada(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "FIN";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			hayError				= true;
 
		}
		
	} else if ( "false".equals(portafolioExtraido) ){
	
		try {
			throw new NafinException("GRAL0021");	
		}catch(Exception e){
			estadoSiguiente 	= "ESPERAR_DECISION";
			msg					= e.getMessage();
			hayError				= true;
		}
		
	}
 
	// Generar Archivo PDF
	String 	urlNombreArchivo 	= null;
	if(!hayError){
		
		try {
			
			HashMap 	cabecera 		= null; 
			String 	nombreArchivo 	= "";
			
			// GENERAR ARCHIVO PDF
			cabecera = new HashMap();
			cabecera.put("PAIS",								(String)	session.getAttribute("strPais"));
			cabecera.put("NUM_NAFIN_ELECTRONICO",	   session.getAttribute("iNoNafinElectronico").toString());//(session.getAttribute("iNoNafinElectronico")==null?"":(String) session.getAttribute("iNoNafinElectronico")));
			cabecera.put("EXTERNO",							(String)	session.getAttribute("sesExterno"));
			cabecera.put("NOMBRE",							(String) session.getAttribute("strNombre"));
			cabecera.put("NOMBRE_USUARIO",				(String) session.getAttribute("strNombreUsuario"));
			cabecera.put("LOGO",								(String)	session.getAttribute("strLogo"));
			cabecera.put("DIRECTORIO_PUBLICACION",		strDirectorioPublicacion );
 
			nombreArchivo 		= ExtraccionPortafolio.generaArchivoPDFExtraccionConsolidada(strDirectorioTemp, cabecera, anio, trimestre, fechaCorte, claveIF);
			urlNombreArchivo 	= strDirecVirtualTemp + nombreArchivo;
			estadoSiguiente	= "MOSTRAR_LINK_PDF_EXTRACCION";
			
		}catch(Exception e){
			
			log.error("MonitoreoPortafolio.extraccionConsolidada(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "FIN";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			hayError				= true;
			
		}
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("msg", 					msg						);
	resultado.put("estadoSiguiente", 	estadoSiguiente  		);
	resultado.put("urlNombreArchivo",	urlNombreArchivo 		);
	resultado.put("recordId",				recordId					);
	resultado.put("portafolioExtraido",	new Boolean(portafolioExtraido)	);
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("MonitoreoPortafolio.extraccionDetalladaPDF") ) {
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		msg 					= "";
	boolean		hayError				= false;
	String 		estadoSiguiente	= null;
	String 		urlNombreArchivo 	= null;
	
	String recordId 				= (request.getParameter("recordId")					== null)?""			:request.getParameter("recordId");
	String totalGarantias 		= (request.getParameter("totalGarantias")			== null)?""			:request.getParameter("totalGarantias");
	String anio 					= (request.getParameter("anio")						== null)?""			:request.getParameter("anio");
	String trimestre 				= (request.getParameter("trimestre")				== null)?""			:request.getParameter("trimestre");
	String fechaCorte 			= (request.getParameter("fechaCorte")				== null)?""			:request.getParameter("fechaCorte");

	totalGarantias 				= totalGarantias.replaceAll(",","");

	// Validar que se hayan proporcionado los parametros necesarios
	if ( totalGarantias.equals("") || anio.equals("") || trimestre.equals("") || fechaCorte.equals("") ) {
			
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "Faltan parámetros, se aborta la operación.";	
		hayError				= true;
		
	// Validar que numero total de garantias sea valido
	} else if( !Comunes.esNumeroEnteroPositivo(totalGarantias) ) {
		
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "El número total de garantías no es válido.";
		hayError				= true;
		
	// Validar que no sean mas de 25000 registros lo que se consultarán
	} else if( Integer.parseInt(totalGarantias) > 25000 ){
		
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "El número máximo de registros permitidos ha sido excedido.";
		hayError				= true;
		
	} else {
		
		// GENERAR ARCHIVO PDF
		try {
			
			HashMap cabecera = new HashMap();
			cabecera.put("PAIS",								(String)	session.getAttribute("strPais"));
			cabecera.put("NUM_NAFIN_ELECTRONICO",	   session.getAttribute("iNoNafinElectronico").toString());//(session.getAttribute("iNoNafinElectronico")==null?"":(String) session.getAttribute("iNoNafinElectronico")));
			cabecera.put("EXTERNO",							(String)	session.getAttribute("sesExterno"));
			cabecera.put("NOMBRE",							(String) session.getAttribute("strNombre"));
			cabecera.put("NOMBRE_USUARIO",				(String) session.getAttribute("strNombreUsuario"));
			cabecera.put("LOGO",								(String)	session.getAttribute("strLogo"));
			cabecera.put("DIRECTORIO_PUBLICACION",		strDirectorioPublicacion );
 
			String claveIF 		= iNoCliente;
			String nombreArchivo = ExtraccionPortafolio.generaArchivoPDFExtraccionDetalle(strDirectorioTemp,cabecera,anio,trimestre,fechaCorte,claveIF);
			urlNombreArchivo 		= strDirecVirtualTemp + nombreArchivo;
			estadoSiguiente		= "MOSTRAR_LINK_PDF_DETALLE";
			
		}catch(Exception e){
			
			log.error("MonitoreoPortafolio.extraccionDetalladaPDF(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "ESPERAR_DECISION";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			hayError				= true;
			
		}
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("msg", 					msg						);
	resultado.put("estadoSiguiente", 	estadoSiguiente  		);
	resultado.put("urlNombreArchivo",	urlNombreArchivo 		);
	resultado.put("recordId",				recordId					);
	infoRegresar = resultado.toString();

} else if( informacion.equals("MonitoreoPortafolio.extraccionDetalladaCSV") ) {
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		msg 					= "";
	boolean		hayError				= false;
	String 		estadoSiguiente	= null;
	String 		urlNombreArchivo 	= null;
	
	String recordId 				= (request.getParameter("recordId")					== null)?""			:request.getParameter("recordId");
	String anio 					= (request.getParameter("anio")						== null)?""			:request.getParameter("anio");
	String trimestre 				= (request.getParameter("trimestre")				== null)?""			:request.getParameter("trimestre");
	String fechaCorte 			= (request.getParameter("fechaCorte")				== null)?""			:request.getParameter("fechaCorte");

	// Validar que se hayan proporcionado los parametros necesarios
	if ( anio.equals("") || trimestre.equals("") || fechaCorte.equals("") ) {
			
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "Faltan parámetros, se aborta la operación.";	
		hayError				= true;
		
	} else {
		
		// GENERAR ARCHIVO CSV
		try {
			
			HashMap cabecera = new HashMap();
			cabecera.put("PAIS",								(String)	session.getAttribute("strPais"));
			cabecera.put("NUM_NAFIN_ELECTRONICO",	   session.getAttribute("iNoNafinElectronico").toString());//(session.getAttribute("iNoNafinElectronico")==null?"":(String) session.getAttribute("iNoNafinElectronico")));
			cabecera.put("EXTERNO",							(String)	session.getAttribute("sesExterno"));
			cabecera.put("NOMBRE",							(String) session.getAttribute("strNombre"));
			cabecera.put("NOMBRE_USUARIO",				(String) session.getAttribute("strNombreUsuario"));
			cabecera.put("LOGO",								(String)	session.getAttribute("strLogo"));
			cabecera.put("DIRECTORIO_PUBLICACION",		strDirectorioPublicacion );
 
			String claveIF 		= iNoCliente;
			String nombreArchivo = ExtraccionPortafolio.generaArchivoCSV(strDirectorioTemp,cabecera,anio,trimestre,fechaCorte,claveIF);
			urlNombreArchivo 		= strDirecVirtualTemp + nombreArchivo;
			estadoSiguiente		= "MOSTRAR_LINK_CSV_DETALLE";

		}catch(Exception e){
			
			log.error("MonitoreoPortafolio.extraccionDetalladaCSV(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "ESPERAR_DECISION";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			hayError				= true;
			
		}

	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("msg", 					msg						);
	resultado.put("estadoSiguiente", 	estadoSiguiente  		);
	resultado.put("urlNombreArchivo",	urlNombreArchivo 		);
	resultado.put("recordId",				recordId					);
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("ConsultaPortafolios") 	)	{

	JSONObject	resultado					= new JSONObject();
	boolean		success						= true;
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
					
	}catch(Exception e){
	 
		success			= false;
		log.error("MonitoreoPortafolio.mostrarPortafolio(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// Consultar Portafolios
	String 	 claveIF		 = iNoCliente;
	ArrayList portafolios = garantias.getPortafolioACalificar(claveIF);

	// Construir respuesta
	JSONArray registros	 = new JSONArray();
	for(int indice=0;indice<portafolios.size();indice++){
		
		HashMap portafolio = (HashMap) portafolios.get(indice);
		
		String anio 								= (String) portafolio.get("AÑO");
		String trimestre 							= (String) portafolio.get("TRIMESTRE");
		String totalGarantias 					= (String) portafolio.get("TOTAL_GARANTIAS");
		String numeroGarantiasRecibidas 		= (String) portafolio.get("NUMERO_GARANTIAS_RECIBIDAS");
		String numeroGarantiasNoRecibidas 	= (String) portafolio.get("NUMERO_GARANTIAS_NO_RECIBIDAS");
		String situacionRecepcion 				= (String) portafolio.get("SITUACION_RECEPCION");
		String extraccionConsolidada			= "true";
		String extraccionDetalladaPdf 		= (String) portafolio.get("MOSTRAR_EXTRACCION_DETALLADA");
		String extraccionDetalladaCsv 		= (String) portafolio.get("MOSTRAR_EXTRACCION_DETALLADA");
		String portafolioExtraido 				= (String) portafolio.get("PORTAFOLIO_EXTRAIDO");
		String fechaCorte 						= (String) portafolio.get("FECHA_CORTE");
		
		totalGarantias								= totalGarantias.replaceAll(",","");
		numeroGarantiasRecibidas				= numeroGarantiasRecibidas.replaceAll(",","");
		numeroGarantiasNoRecibidas				= numeroGarantiasNoRecibidas.replaceAll(",","");
		
		JSONObject	registro = new JSONObject();
		registro.put("ANIO", 									anio);
		registro.put("TRIMESTRE",  							trimestre);
		registro.put("TOTAL_GARANTIAS",  					totalGarantias);
		registro.put("NUMERO_GARANTIAS_RECIBIDAS", 		numeroGarantiasRecibidas);
		registro.put("NUMERO_GARANTIAS_NO_RECIBIDAS",	numeroGarantiasNoRecibidas);
		registro.put("SITUACION_RECEPCION",  				situacionRecepcion);
		registro.put("EXTRACCION_CONSOLIDADA",  			new Boolean(extraccionConsolidada) );
		registro.put("EXTRACCION_DETALLADA_PDF",  		new Boolean(extraccionDetalladaPdf));
		registro.put("EXTRACCION_DETALLADA_CSV",  		new Boolean(extraccionDetalladaCsv));
		registro.put("PORTAFOLIO_EXTRAIDO",  				new Boolean(portafolioExtraido));		
		registro.put("FECHA_CORTE",  							fechaCorte);
		registro.put("EXTRACCION_CONSOLIDADA_LOADING",	new Boolean("false"));
		registro.put("EXTRACCION_DETALLADA_PDF_LOADING",new Boolean("false"));
		registro.put("EXTRACCION_DETALLADA_CSV_LOADING",new Boolean("false"));
		registro.put("EXTRACCION_CONSOLIDADA_URL",		"");
		registro.put("EXTRACCION_DETALLADA_PDF_URL",		"");
		registro.put("EXTRACCION_DETALLADA_CSV_URL",		"");
		registros.add(registro);
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)					);
	resultado.put("total",    	String.valueOf(registros.size()) );
	resultado.put("registros",	registros			 					);
	
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>