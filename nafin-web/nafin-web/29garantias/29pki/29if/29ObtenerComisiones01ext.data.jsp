<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		java.io.OutputStream,
		java.io.PrintStream,
		java.io.File,
		java.io.BufferedInputStream, 
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("ObtencionComisiones.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;
	
	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
				
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){
 
		log.error("ObtencionComisiones.inicializacion(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}
	
	// 2. Validar Numero Fiso
	if (!hayAviso && !seguridad.validaNumeroFISO(claveIF) ) {
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se tiene definido un Fideicomiso para realizar sus operaciones.<br/><br/>Por favor comuníquese al Centro de Atención a clientes al teléfono 50-89-61-07 <br/>o del Interior al 01-800-NAFINSA (01-800-6234672).";
		hayAviso				= true;
	}
	
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_CONSULTA";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();

} else if (    informacion.equals("ObtencionComisiones.extraccionDetalladaTXT" )   ){

	JSONObject	resultado				= new JSONObject();
	boolean		success					= true;
	String 		msg 						= "";
	
	String 		recordId 				= (request.getParameter("recordId")		== null)?""			:request.getParameter("recordId");
	String 		claveMes 				= (request.getParameter("claveMes")		== null)?""			:request.getParameter("claveMes");
	String 		fiso 						= (request.getParameter("fiso")			== null)?""			:request.getParameter("fiso");
	String 		fechaCorte 				= (request.getParameter("fechaCorte")	== null)?""		 	:request.getParameter("fechaCorte");
	String 		isEmptyPkcs7 			= (request.getParameter("isEmptyPkcs7")== null)?"false"	:request.getParameter("isEmptyPkcs7");
	String 		pkcs7 					= (request.getParameter("pkcs7")			== null)?""			:request.getParameter("pkcs7").trim();
	String 		textoFirmado 			= (request.getParameter("textoFirmado")== null)?""			:request.getParameter("textoFirmado").trim();
	String 		claveIF 					= iNoCliente;
	boolean 		extraccionFirmada		= false;
	boolean		actualizarEstatus		= false;
	String 		acuseFirmaDigital 	= "00000";
	
	String 		externContent 			= textoFirmado;
	char 			getReceipt 				= 'Y';
	boolean		hayError					= false;
	String 		estadoSiguiente		= null;

	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
					
	}catch(Exception e){
	 
		success			= false;
		log.error("ObtencionComisiones.extraccionDetalladaTXT(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// Revisar si ya se firmó la extracción
	if ( !fiso.equals("") && !fechaCorte.equals("") ) {
		extraccionFirmada  = "2".equals( garantias.getEstatusRecepcionComisiones(fiso, claveIF, fechaCorte) ) ?true:false;
	}
	
	// El portafolio no ha sido extraido
	if ( recordId.equals("") ||  claveMes.equals("") || fiso.equals("") || fechaCorte.equals("") ) {
			
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "Faltan parámetros, se aborta la operación.";
		hayError				= true;
 
	} else if (
		
		!extraccionFirmada				&&
		"true".equals(isEmptyPkcs7)	&&
		!"".equals(externContent) 
		
	){
		
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "";
		hayError				= true;
		
	} else if ( 

		!extraccionFirmada 					&&
		!_serial.equals("") 					&& 
		"false".equals(isEmptyPkcs7) 		&& 
		!"".equals(pkcs7) 					&&
		!"".equals(externContent) 				 
		
	) {
 
		String 									folioCert   = "01OBTCM"+claveIF+new SimpleDateFormat("ddMMyyHHmm").format(new java.util.Date());
		netropology.utilerias.Seguridad 	s 				= new netropology.utilerias.Seguridad();
		
		try {
			
			// Autenticar texto firmado
			if (!s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {	
				msg 						= "La autentificacion no se llevo a cabo: \n"+s.mostrarError();
				estadoSiguiente 		= "ESPERAR_DECISION";
				hayError					= true;
			} 	else {
				// Obtener acuse de la firma digital
				acuseFirmaDigital 	= s.getAcuse();
				// Actualizar estatus de recepcion de comisiones
				actualizarEstatus 	= true;
			}
 
		}catch(Exception e){
			
			log.error("ObtencionComisiones.extraccionDetalladaTXT(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "FIN";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			hayError				= true;
 
		}
		
	} else if (
		!extraccionFirmada 
	){
	
		try {
			throw new NafinException("GRAL0021");	
		}catch(Exception e){
			estadoSiguiente 	= "ESPERAR_DECISION";
			msg					= e.getMessage();
			hayError				= true;
		}
		
	}
 
	// Generar Archivo TXT
	String 	urlNombreArchivo 	= null;
	if(!hayError){
		
		try {
			
			HashMap 	query 			= null; 
			String 	nombreArchivo 	= "";
			
			// Obtener query que se utilizara para consulta del detalle de las comisiones
			query					= garantias.getQueryDetalleComisiones( claveIF, fiso, fechaCorte );
 
			nombreArchivo 		= ExtraccionComisionesSHF.generaArchivoTXTDetalle( strDirectorioTemp, query );
			urlNombreArchivo 	= appWebContextRoot + "/DescargaArchivo?nombreArchivo=" + strDirectorioTempRelativePath + nombreArchivo;
			estadoSiguiente	= "MOSTRAR_LINK_TXT_DETALLE";
			
		}catch(Exception e){
			
			log.error("ObtencionComisiones.extraccionDetalladaTXT(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "FIN";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			hayError				= true;
			
		}
		
	}
 
	// Actualizar estatus de recepcion de comisiones
	if( !hayError && actualizarEstatus ){
		
		try {
 
			// Enviar folio de la operacion
			resultado.put("acuseAutentificacion",			"La autentificación se llevó a cabo con éxito<br>Recibo: "+ acuseFirmaDigital );
 
			// Construir Array con los datos del acuse
			JSONArray 	registrosDeControlDataArray 	= new JSONArray();
			JSONArray	registro								= null;
			
			registro = new JSONArray();
			registro.add("Tipo de Operación");
			registro.add("Obtención de Comisiones");
			registrosDeControlDataArray.add(registro);
			
			registro = new JSONArray();
			registro.add("Fecha de descarga");
			registro.add(Fecha.getFechaActual());
			registrosDeControlDataArray.add(registro);
			
			registro = new JSONArray();
			registro.add("Hora de decarga");
			registro.add( Fecha.getHoraActual("HH:MI:SS AM") );
			registrosDeControlDataArray.add(registro);
		
			registro = new JSONArray();
			registro.add("Usuario");
			registro.add(iNoUsuario+" - "+strNombreUsuario);
			registrosDeControlDataArray.add(registro);
	 
			resultado.put("registrosDeControlDataArray",	registrosDeControlDataArray	);
			
			garantias.actualizaStatusRecepcionComisiones( fiso, claveIF, fechaCorte, "2" );
			extraccionFirmada	= true;
			estadoSiguiente 	= "MOSTRAR_ACUSE";
			
		} catch(Exception e) {
				
			log.error("ObtencionComisiones.extraccionDetalladaTXT(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "FIN";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			hayError				= true;
	 
		}
		
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)					);
	resultado.put("msg", 					msg										);
	resultado.put("estadoSiguiente", 	estadoSiguiente  						);
	resultado.put("urlNombreArchivo",	urlNombreArchivo 						);
	resultado.put("recordId",				recordId									);
	resultado.put("extraccionFirmada",	new Boolean(extraccionFirmada)	);
	infoRegresar = resultado.toString();

} else if (    informacion.equals("ObtencionComisiones.extraccionDetalladaPDF" )   ){
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		msg 					= "";
	boolean		hayError				= false;
	String 		estadoSiguiente	= null;
	String 		urlNombreArchivo 	= null;
	
	String recordId 			= (request.getParameter("recordId")				== null)?"":request.getParameter("recordId");
	String numeroComisiones = (request.getParameter("numeroComisiones")	== null)?"":request.getParameter("numeroComisiones");
	String claveMes 			= (request.getParameter("claveMes")				== null)?"":request.getParameter("claveMes");
	String fiso 				= (request.getParameter("fiso")					== null)?"":request.getParameter("fiso");
	String fechaCorte 		= (request.getParameter("fechaCorte")			== null)?"":request.getParameter("fechaCorte");
	String claveIF				= iNoCliente;
	
	numeroComisiones 			= numeroComisiones.replaceAll(",","");

	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
					
	}catch(Exception e){
	 
		success			= false;
		log.error("ObtencionComisiones.extraccionDetalladaPDF(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// Validar que se hayan proporcionado los parametros necesarios
	if ( recordId.equals("") || numeroComisiones.equals("") || claveMes.equals("") || fiso.equals("") || fechaCorte.equals("") ) {
			
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "Faltan parámetros, se aborta la operación.";	
		hayError				= true;
		
	// Validar que numero total de garantias sea valido
	} else if( !Comunes.esNumeroEnteroPositivo(numeroComisiones) ) {
		
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "El número de comisiones no es válido.";
		hayError				= true;
		
	// Validar que no sean mas de 25000 registros lo que se consultarán
	} else if( Integer.parseInt(numeroComisiones) > 25000 ){
		
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "El número máximo de registros permitidos ha sido excedido.";
		hayError				= true;
		
	} else {
		
		// GENERAR ARCHIVO PDF
		try {
			
			// Obtener query que se utilizara para consulta del detalle de las comisiones
			HashMap	query				= garantias.getQueryDetalleComisiones( claveIF, fiso, fechaCorte );
			HashMap 	queryTotales	= garantias.getQueryTotalesDetalleComisiones( claveIF, fiso, fechaCorte );
			
			// Obtener nombre del mes con el año
			String 	fecha[]			= fechaCorte.split("/");
			String 	nombreMesAnio	= Fecha.getNombreDelMes(Integer.parseInt(fecha[1]),'M') + " DE " + fecha[2];
			
			// Agregar cabecera
			HashMap 	cabecera = new HashMap();
			cabecera.put("NOMBRE",							(String) session.getAttribute("strNombre"));
			cabecera.put("NOMBRE_USUARIO",				(String) session.getAttribute("strNombreUsuario"));
			cabecera.put("NUM_NAFIN_ELECTRONICO",	   session.getAttribute("iNoNafinElectronico").toString());
			cabecera.put("LOGO_NAFIN",						strDirectorioLogo + "nafinsa.gif" 	);
			cabecera.put("DATOS_PROGRAMA",				"PROGRAMA DE FINANCIAMIENTO DE VIVIENDA PARA NO AFILIADOS\n" +
																	"(PROGRAMA)\n" +
																	"ESQUEMA DE PRESTACION DE SERVICIOS\n" +
																	"CREDITOS REGISTRADOS EN EL MES DE " + nombreMesAnio
							 );
			cabecera.put("LOGO_SHF",		 				strDirectorioLogo + "shf.gif"			);
 
			String nombreArchivo = ExtraccionComisionesSHF.generaArchivoPDFDetalle( strDirectorioTemp, query, queryTotales, cabecera );
			urlNombreArchivo 	   = appWebContextRoot + "/DescargaArchivo?nombreArchivo=" + strDirectorioTempRelativePath + nombreArchivo;
			estadoSiguiente		= "MOSTRAR_LINK_PDF_DETALLE";
			
		}catch(Exception e){
			
			log.error("ObtencionComisiones.extraccionDetalladaPDF(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "ESPERAR_DECISION";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			hayError				= true;
			
		}
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("msg", 					msg						);
	resultado.put("estadoSiguiente", 	estadoSiguiente  		);
	resultado.put("urlNombreArchivo",	urlNombreArchivo 		);
	resultado.put("recordId",				recordId					);
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("ObtencionComisiones.extraccionDetalladaXLSX" )   ){	
 
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		msg 					= "";
	boolean		hayError				= false;
	String 		estadoSiguiente	= null;
	String 		urlNombreArchivo 	= null;
	
	String recordId 			= (request.getParameter("recordId")				== null)?"":request.getParameter("recordId");
	String numeroComisiones = (request.getParameter("numeroComisiones")	== null)?"":request.getParameter("numeroComisiones");
	String claveMes 			= (request.getParameter("claveMes")				== null)?"":request.getParameter("claveMes");
	String fiso 				= (request.getParameter("fiso")					== null)?"":request.getParameter("fiso");
	String fechaCorte 		= (request.getParameter("fechaCorte")			== null)?"":request.getParameter("fechaCorte");
	String claveIF				= iNoCliente;
	
	numeroComisiones 			= numeroComisiones.replaceAll(",","");

	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
					
	}catch(Exception e){
	 
		success			= false;
		log.error("ObtencionComisiones.extraccionDetalladaXLSX(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// Validar que se hayan proporcionado los parametros necesarios
	if ( recordId.equals("") || numeroComisiones.equals("") || claveMes.equals("") || fiso.equals("") || fechaCorte.equals("") ) {
			
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "Faltan parámetros, se aborta la operación.";	
		hayError				= true;
		
	// Validar que numero total de garantias sea valido
	} else if( !Comunes.esNumeroEnteroPositivo(numeroComisiones) ) {
		
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "El número de comisiones no es válido.";
		hayError				= true;
		
	// Validar que no sean mas de 25000 registros lo que se consultarán
	} else if( Integer.parseInt(numeroComisiones) > 25000 ){
		
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg					= "El número máximo de registros permitidos ha sido excedido.";
		hayError				= true;
		
	} else {
		
		// GENERAR ARCHIVO PDF
		try {
			
			// Obtener query que se utilizara para consulta del detalle de las comisiones
			HashMap	query				= garantias.getQueryDetalleComisiones( claveIF, fiso, fechaCorte );
			HashMap 	queryTotales	= garantias.getQueryTotalesDetalleComisiones( claveIF, fiso, fechaCorte );
			
			// Obtener nombre del mes con el año
			String 	fecha[]			= fechaCorte.split("/");
			String 	nombreMesAnio	= Fecha.getNombreDelMes(Integer.parseInt(fecha[1]),'M') + " DE " + fecha[2];
			
			String 	directorioPlantilla  = strDirectorioPublicacion + "00archivos/plantillas/29garantias/";
			// Agregar cabecera
			HashMap 	cabecera = new HashMap();
			cabecera.put("NOMBRE",							(String) session.getAttribute("strNombre"));
			cabecera.put("NOMBRE_USUARIO",				(String) session.getAttribute("strNombreUsuario"));
			cabecera.put("NUM_NAFIN_ELECTRONICO",	   session.getAttribute("iNoNafinElectronico").toString());
			cabecera.put("PROGRAMA",						"PROGRAMA DE FINANCIAMIENTO DE VIVIENDA PARA NO AFILIADOS");
			cabecera.put("DESCRIPCION_PROGRAMA",		"(PROGRAMA)");
			cabecera.put("ESQUEMA",							"ESQUEMA DE PRESTACION DE SERVICIOS");
			cabecera.put("CREDITOS",						"CREDITOS REGISTRADOS EN EL MES DE " + nombreMesAnio);
 
			String nombreArchivo = ExtraccionComisionesSHF.generaArchivoXLSXDetalle( strDirectorioTemp, query, queryTotales, cabecera, directorioPlantilla );
			urlNombreArchivo 	   = appWebContextRoot + "/DescargaArchivo?nombreArchivo=" + strDirectorioTempRelativePath + nombreArchivo;
			estadoSiguiente		= "MOSTRAR_LINK_XLSX_DETALLE";
			
		}catch(Exception e){
			
			log.error("ObtencionComisiones.extraccionDetalladaXLSX(Exception)");
			e.printStackTrace();
			estadoSiguiente 	= "ESPERAR_DECISION";
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			hayError				= true;
			
		}
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("msg", 					msg						);
	resultado.put("estadoSiguiente", 	estadoSiguiente  		);
	resultado.put("urlNombreArchivo",	urlNombreArchivo 		);
	resultado.put("recordId",				recordId					);
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("ObtencionComisiones.fin" )                      ){

	// Nota: no se tiene completado, llegar hasta aqui desde el cliente, pero se deja
	// por compatibilidad.
	
	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("29ObtenerComisiones01ext.jsp");
	
} else if (    informacion.equals("CatalogoMes")							)	{
	
	boolean		success		= true;
	JSONObject	resultado	= new JSONObject();
		
	// Se declaran las variables...
	String 	meses[] 	= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	Calendar cal 		= Calendar.getInstance();
	cal.add(Calendar.MONTH,-1);
	
	// Obtener lista de los ultimos 12 meses
	String 		mesanio 		= null;
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	for(int i=11;i>=0;i--){ 
		
		mesanio = new SimpleDateFormat("yyyyMM").format(cal.getTime());
 
		registro.put("clave",			mesanio );
		registro.put("descripcion",	meses[cal.get(Calendar.MONTH)]+" - " + cal.get(Calendar.YEAR));
		registros.add(registro);
		
		cal.add(Calendar.MONTH,-1);
		
	}
 
	// Enviar resultado
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("ConsultaComisiones")									)  {
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	
	String 		claveIF		= iNoCliente;
	String 		claveMes 	= (request.getParameter("claveMes")		== null)?"":request.getParameter("claveMes");
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
					
	}catch(Exception e){
	 
		success			= false;
		log.error("ConsultaComisiones(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// Consultar Portafolios
	String 	claveFiso 	= garantias.getClaveFISO(claveIF);
	List 		comisiones	= garantias.getResumenComisiones(claveIF,claveFiso,claveMes);

	// Construir respuesta
	JSONArray registros	 = new JSONArray();
	for(int indice=0;indice<comisiones.size();indice++){
		
		HashMap 	comision 						= (HashMap) comisiones.get(indice);
		
		String 	claveMesRegistro				= (String) comision.get("claveMes");
		String 	fiso 								= (String) comision.get("fiso");
		String 	fechaCorte 						= (String) comision.get("fechaCorte");
		String 	fechaComision 					= (String) comision.get("fechaComision");
		String 	numeroComisionesExtraer 	= (String) comision.get("numeroComisionesExtraer");
		String 	extraccionFirmada 			= (String) comision.get("extraccionFirmada");

		JSONObject	registro = new JSONObject();
		registro.put("CLAVE_MES",									claveMesRegistro						);
		registro.put("FISO",											fiso										);
		registro.put("FECHA_CORTE",								fechaCorte								);
		registro.put("FECHA_COMISION",							fechaComision							);
		registro.put("NUMERO_COMISIONES_EXTRAER",				numeroComisionesExtraer				);
		registro.put("EXTRACCION_DETALLADA_TXT",				new Boolean("true")					);
		registro.put("EXTRACCION_DETALLADA_PDF",				new Boolean("true")					);
		registro.put("EXTRACCION_DETALLADA_XLSX",				new Boolean("true")					);
		registro.put("EXTRACCION_FIRMADA",						new Boolean(extraccionFirmada) 	);
		registro.put("EXTRACCION_DETALLADA_TXT_LOADING",	new Boolean("false")					);
		registro.put("EXTRACCION_DETALLADA_PDF_LOADING",	new Boolean("false")					);
		registro.put("EXTRACCION_DETALLADA_XLSX_LOADING",	new Boolean("false")					);
		registro.put("EXTRACCION_DETALLADA_TXT_URL",			""											);
		registro.put("EXTRACCION_DETALLADA_PDF_URL",			""											);
		registro.put("EXTRACCION_DETALLADA_XLSX_URL",		""											);
		registros.add(registro);
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)					);
	resultado.put("total",    	String.valueOf(registros.size()) );
	resultado.put("registros",	registros			 					);
	
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%><%=infoRegresar%>