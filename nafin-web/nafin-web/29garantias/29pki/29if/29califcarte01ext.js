var showPanelCalificacionLayout;

Ext.onReady(function() {
	
	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});	
	
	//-------------------------------- VALIDACIONES -----------------------------------
 
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaCargaArchivo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaArchivo(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaArchivo(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelFormaSolicitudDeCarga").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceValidacion();
         
         // Ocultar mascara del panel de resultados de la validacion
         element = Ext.getCmp("panelResultadosValidacion").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Ocultar mascara del panel de preacuse
			element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var cargaArchivo = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"					){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29califcarte01ext.data.jsp',
				params: 	{
					informacion:		'CargaArchivo.inicializacion'
				},
				callback: 				procesaCargaArchivo
			});
			
		} else if(			estado == "MOSTRAR_AVISO"								){

			if(!Ext.isEmpty(respuesta.aviso)){
				
				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);
				
				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();
				
			}
			
		} else if(			estado == "ESPERAR_CAPTURA_SOLICITUD_DE_CARGA"	){
 
			// Cargar Contenido del Combo A�o
			var catalogoAnioData      = Ext.StoreMgr.key('catalogoAnioDataStore');
			catalogoAnioData.load({
				params: { anio: "" }
			});
			// Limpiar contenido del combo trimestre
			var catalogoTrimestreData = Ext.StoreMgr.key('catalogoTrimestreDataStore');
			catalogoTrimestreData.removeAll();
			// Limpiar contenido del numberfield
			Ext.getCmp('numeroRegistros').setValue("");
			// Limpiar contenido del numberfield
			Ext.getCmp('numeroSumatoriaExposicionIncumplimiento').setValue("");
			
			var panelFormaSolicitudDeCarga = Ext.getCmp("panelFormaSolicitudDeCarga");
			panelFormaSolicitudDeCarga.getForm().clearInvalid();
			panelFormaSolicitudDeCarga.show();
		
			// Definir el texto del layout de carga
			if(!Ext.isEmpty(respuesta.calificacionLayout)){
				Ext.getCmp("labelLayoutCarga").setText(respuesta.calificacionLayout,false);
			}
			
		} else if(			estado == "ENVIAR_SOLICITUD_DE_CARGA" 				){
			
			Ext.getCmp("panelFormaSolicitudDeCarga").el.mask("Enviando...",'x-mask-loading');
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29califcarte01ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CargaArchivo.enviarSolicitudDeCarga'
					},
					respuesta
				),
				callback: 				procesaCargaArchivo
			});
 
		} else if(			estado == "ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES"	){
			
			// Ocultar panel con la solicitud de carga
			var panelFormaSolicitudDeCarga = Ext.getCmp("panelFormaSolicitudDeCarga");
			panelFormaSolicitudDeCarga.el.unmask();
			panelFormaSolicitudDeCarga.hide();
 
			// Esperar decision
			Ext.getCmp("claveAnio1").setValue(respuesta.claveAnio);
			Ext.getCmp("claveTrimestre1").setValue(respuesta.claveTrimestre);
			Ext.getCmp("numeroRegistros1").setValue(respuesta.numeroRegistros);
			Ext.getCmp("numeroSumatoriaExposicionIncumplimiento1").setValue(respuesta.numeroSumatoriaExposicionIncumplimiento);
 
			// Mostrar panel de carga de archivo
			var panelFormaCargaArchivo = Ext.getCmp("panelFormaCargaArchivo");
			panelFormaCargaArchivo.show();
			
		} else if(			estado == "SUBIR_ARCHIVO"								){
			
			Ext.getCmp("panelFormaCargaArchivo").getForm().submit({
				clientValidation: 	true,
				url: 						'29califcarte01ext.data.jsp?informacion=CargaArchivo.subirArchivo',
				waitMsg:   				'Subiendo archivo...',
				waitTitle: 				'Por favor espere',
				success: function(form, action) {
					 procesaCargaArchivo(null,  true,  action.response );
				},
				failure: function(form, action) {
				 	 action.response.status = 200;
					 procesaCargaArchivo(null,  false, action.response );
				}
			});
			
		} else if(			estado == "REALIZAR_VALIDACION"         			){
 
			showWindowAvanceValidacion();
			
			Ext.Ajax.request({
				url: 		'29califcarte01ext.data.jsp',
				params: 	{
					informacion:								'CargaArchivo.realizarValidacion',
					claveAnio: 									respuesta.claveAnio,
					claveTrimestre: 							respuesta.claveTrimestre,
					numeroRegistros: 							respuesta.numeroRegistros,
					numeroSumatoriaExposicionIncumplimiento: 	respuesta.numeroSumatoriaExposicionIncumplimiento,
					fileName:									respuesta.fileName
				},
				callback: 										procesaCargaArchivo
			});
		
		} else if(  estado == "ESPERAR_DECISION_VALIDACION" ){
 
			var claveProceso 											= respuesta.claveProceso;
			var totalRegistros 										= respuesta.totalRegistros;
			
			var numeroRegistrosSinErrores 						= respuesta.numeroRegistrosSinErrores;
			var numeroImportesExposicionIncumplimientoSinErrores 	= respuesta.numeroImportesExposicionIncumplimientoSinErrores;
			var numeroRegistrosConErrores 						= respuesta.numeroRegistrosConErrores;
			var numeroImportesExposicionIncumplimientoConErrores 	= respuesta.numeroImportesExposicionIncumplimientoConErrores;
			
			var continuarCarga 									= respuesta.continuarCarga;
			
			var claveAnio 												= respuesta.claveAnio;
			var claveTrimestre 										= respuesta.claveTrimestre;
			var numeroRegistros 										= respuesta.numeroRegistros;
			var numeroSumatoriaExposicionIncumplimiento 				= respuesta.numeroSumatoriaExposicionIncumplimiento;
	
			// SETUP PARAMETROS VALIDACION
			// Recordar numero de proceso y el total de registros
         Ext.getCmp("panelResultadosValidacion.claveProceso").setValue(claveProceso);
			Ext.getCmp("panelResultadosValidacion.totalRegistros").setValue(totalRegistros);
			
         Ext.getCmp("panelResultadosValidacion.claveAnio").setValue(claveAnio);
         Ext.getCmp("panelResultadosValidacion.claveTrimestre").setValue(claveTrimestre);
         Ext.getCmp("panelResultadosValidacion.numeroRegistros").setValue(numeroRegistros);
         Ext.getCmp("panelResultadosValidacion.numeroSumatoriaExposicionIncumplimiento").setValue(numeroSumatoriaExposicionIncumplimiento);
         
         // Inicializar panel de resultados
			Ext.getCmp("numeroRegistrosSinErrores").setValue(numeroRegistrosSinErrores);
			Ext.getCmp("numeroImportesExposicionIncumplimientoSinErrores").setValue(numeroImportesExposicionIncumplimientoSinErrores);
			Ext.getCmp("numeroRegistrosConErrores").setValue(numeroRegistrosConErrores);
			Ext.getCmp("numeroImportesExposicionIncumplimientoConErrores").setValue(numeroImportesExposicionIncumplimientoConErrores);
		
			if(continuarCarga){
         	Ext.getCmp('botonContinuarCarga').enable();
         }else{
         	Ext.getCmp('botonContinuarCarga').disable();
         }
         
			// MOSTRAR COMPONENTES
			// Resetear campo de carga de archivo
			Ext.getCmp("archivo").reset();
			
			// Ocultar ventana de avance validacion
			hideWindowAvanceValidacion();
			
			 // Remover contenido anterior de los grids
         var solicitudesCorrectasData 		= Ext.StoreMgr.key('solicitudesCorrectasDataStore');
         var solicitudesConErroresData 	= Ext.StoreMgr.key('solicitudesConErroresDataStore');
         var erroresVsCifrasControlData 	= Ext.StoreMgr.key('erroresVsCifrasControlDataStore');
         //solicitudesCorrectasData.removeAll();
         //solicitudesConErroresData.removeAll();
         //erroresVsCifrasControlData.removeAll();
         
			// Mostrar Panel de resultados
         Ext.getCmp('panelResultadosValidacion').show();

         // Mostrar Panel de Resultados de la validacion
         var tabPanelResultadosValidacion = Ext.getCmp('tabPanelResultadosValidacion');
         tabPanelResultadosValidacion.setActiveTab(2);
         erroresVsCifrasControlData.loadData(respuesta.erroresVsCifrasControlDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
         tabPanelResultadosValidacion.setActiveTab(1);
         solicitudesConErroresData.loadData(respuesta.solicitudesConErroresDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
         tabPanelResultadosValidacion.setActiveTab(0);
         solicitudesCorrectasData.loadData(respuesta.solicitudesCorrectasDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
		   
      } else if(  estado == "PRESENTAR_PREACUSE"         ){
      	
      	Ext.getCmp("panelResultadosValidacion").el.mask("Generando Pre-Acuse...","x-mask-loading");
      	
      	Ext.Ajax.request({
				url: 		'29califcarte01ext.data.jsp',
				params: 	{
					informacion:								'CargaArchivo.presentarPreacuse',
					claveProceso: 								Ext.getCmp("panelResultadosValidacion.claveProceso").getValue(),
					totalRegistros: 							Ext.getCmp("panelResultadosValidacion.totalRegistros").getValue(),
					claveAnio: 									Ext.getCmp("panelResultadosValidacion.claveAnio").getValue(),
					claveTrimestre: 							Ext.getCmp("panelResultadosValidacion.claveTrimestre").getValue(),
					numeroRegistros: 							Ext.getCmp("panelResultadosValidacion.numeroRegistros").getValue(),
					numeroSumatoriaExposicionIncumplimiento: 	Ext.getCmp("panelResultadosValidacion.numeroSumatoriaExposicionIncumplimiento").getValue()
				},
				callback: 										procesaCargaArchivo
			});
      	
		} else if(  estado == "ESPERAR_DECISION_PREACUSE"         ){
			
			var claveProceso 								= respuesta.claveProceso;
			var totalRegistros 							= respuesta.totalRegistros;
			var claveAnio 									= respuesta.claveAnio;
			var claveTrimestre 							= respuesta.claveTrimestre;
			var numeroRegistros 							= respuesta.numeroRegistros;
			var numeroSumatoriaExposicionIncumplimiento 	= respuesta.numeroSumatoriaExposicionIncumplimiento;
			
			// Establecer los valores de los parametros de la carga
			Ext.getCmp("panelPreacuseCargaArchivo.claveProceso").setValue(claveProceso);
			Ext.getCmp("panelPreacuseCargaArchivo.totalRegistros").setValue(totalRegistros);
			Ext.getCmp("panelPreacuseCargaArchivo.claveAnio").setValue(claveAnio);
			Ext.getCmp("panelPreacuseCargaArchivo.claveTrimestre").setValue(claveTrimestre);
			Ext.getCmp("panelPreacuseCargaArchivo.numeroRegistros").setValue(numeroRegistros);
			Ext.getCmp("panelPreacuseCargaArchivo.numeroSumatoriaExposicionIncumplimiento").setValue(numeroSumatoriaExposicionIncumplimiento);
			
			// Suprimir Mascara
			Ext.getCmp("panelResultadosValidacion").el.unmask();
			
			// Ocultar panel de Carga
			Ext.getCmp("panelFormaCargaArchivo").hide();
			// Ocultar espaciador
			Ext.getCmp("espaciadorpanelFormaCargaArchivo").hide();
			// Ocultar panel de resultados
			Ext.getCmp("panelResultadosValidacion").hide();
			// Ocultar espaciador del layout
			Ext.getCmp("espaciadorPanelCalificacionLayout").hide();
			// Ocultar panel con el layout de carga
			Ext.getCmp("panelCalificacionLayout").hide();
			
			// Mostrar panel de preacuse
			Ext.getCmp("panelPreacuseCargaArchivo").show();
			
			// Cargar resultados
			Ext.StoreMgr.key('solicitudesPorAgregarDataStore').loadData(respuesta.solicitudesPorAgregarDataArray);
			
      } else if(  estado == "ESPERAR_DECISION"  			){
      	
      	// Ocultar mascara del panel de preacuse
			var element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
      	return;
      	
		} else if(  estado == "TRANSMITIR_REGISTROS" 		){
			
			var claveProceso								= Ext.getCmp("panelPreacuseCargaArchivo.claveProceso").getValue();
			var totalRegistros							= Ext.getCmp("panelPreacuseCargaArchivo.totalRegistros").getValue();
			var claveAnio 									= Ext.getCmp("panelPreacuseCargaArchivo.claveAnio").getValue();
			var claveTrimestre 							= Ext.getCmp("panelPreacuseCargaArchivo.claveTrimestre").getValue();
			var numeroRegistros 							= Ext.getCmp("panelPreacuseCargaArchivo.numeroRegistros").getValue();
			var numeroSumatoriaExposicionIncumplimiento	= Ext.getCmp("panelPreacuseCargaArchivo.numeroSumatoriaExposicionIncumplimiento").getValue();
			
			// Agregar mensaje transmitiendo solicitudes
			Ext.getCmp("panelPreacuseCargaArchivo").el.mask("Transmitiendo solicitudes...","x-mask-loading");
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 												'29califcarte01ext.data.jsp',
				params: {	
					informacion:								'CargaArchivo.transmitirRegistros',
					claveProceso:								claveProceso,
					totalRegistros:							totalRegistros,
					claveAnio:									claveAnio,
					claveTrimestre:							claveTrimestre,
					numeroRegistros:							numeroRegistros,
					numeroSumatoriaExposicionIncumplimiento: 	numeroSumatoriaExposicionIncumplimiento,
					isEmptyPkcs7: 								Ext.isEmpty(respuesta.pkcs7),
					pkcs7: 										respuesta.pkcs7,
					textoFirmado: 								respuesta.textoFirmado
				},
				callback: 										procesaCargaArchivo
			});
         
		} else if(	estado == "MOSTRAR_ACUSE_TRANSMISION_SOLICITUDES"	){
 
			var claveProceso 								= respuesta.claveProceso;
			var totalRegistros 							= respuesta.totalRegistros;
			var claveAnio 									= respuesta.claveAnio;
			var claveTrimestre 							= respuesta.claveTrimestre;
			var numeroRegistros 							= respuesta.numeroRegistros;
			var numeroSumatoriaExposicionIncumplimiento 	= respuesta.numeroSumatoriaExposicionIncumplimiento;
			
			// Establecer los valores de los parametros de la carga
			Ext.getCmp("panelAcuseCargaArchivo.claveProceso").setValue(claveProceso);
			Ext.getCmp("panelAcuseCargaArchivo.totalRegistros").setValue(totalRegistros);
			Ext.getCmp("panelAcuseCargaArchivo.claveAnio").setValue(claveAnio);
			Ext.getCmp("panelAcuseCargaArchivo.claveTrimestre").setValue(claveTrimestre);
			Ext.getCmp("panelAcuseCargaArchivo.numeroRegistros").setValue(numeroRegistros);
			Ext.getCmp("panelAcuseCargaArchivo.numeroSumatoriaExposicionIncumplimiento").setValue(numeroSumatoriaExposicionIncumplimiento);
		
			// Agregar mensaje transmitiendo solicitudes
			Ext.getCmp("panelPreacuseCargaArchivo").el.unmask();
			
			// Ocultar panel de preacuse
			Ext.getCmp("panelPreacuseCargaArchivo").hide();
			
			// Mostrar panel de preacuse
			Ext.getCmp("panelAcuseCargaArchivo").show();
			
			// Mostrar el folio de la validacion
			Ext.getCmp("labelAutentificacion").setText(respuesta.folioSolicitud,false);
			
			// Resetear cualquier url de archivo que pudiera existir anteriormente
			Ext.getCmp("botonImprimirPDF").urlArchivo = "";
			
			// Cargar resultados
			Ext.StoreMgr.key('solicitudesAgregadasDataStore').loadData(respuesta.solicitudesAgregadasDataArray);
 
		} else if(	estado == "FIN"								){
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "29califcarte01ext.jsp";
			forma.target	= "_self";
			forma.submit();
 
		}
		
		return;
		
	}
 
	//-------------------------- 7. PANEL ACUSE CARGA CUENTAS -------------------------
	
	var procesarSuccessFailureGeneraArchivoPDF =  function(opts, success, response) {
 
		var botonImprimirPDF = Ext.getCmp('botonImprimirPDF');
		botonImprimirPDF.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			botonImprimirPDF.setIconClass('icoPdf');
			botonImprimirPDF.setText('Descargar PDF');
			
			botonImprimirPDF.urlArchivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			botonImprimirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		
			if(Ext.isIE7){ // Nota: debido a un bug en el width en IE7, se vuelve a realizar el layout de los componentes
				Ext.getCmp('botonesAcuseCargaArchivo').doLayout();
			}
			
		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						botonImprimirPDF.setIconClass('icoImprimir');
						botonImprimirPDF.setText('Imprimir PDF');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				botonImprimirPDF.setIconClass('icoImprimir');
				botonImprimirPDF.setText('Imprimir PDF');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
 
	}
	
	var procesarConsultaSolicitudesAgregadas = function(store, registros, opts){
		
		var gridSolicitudesAgregadas 			= Ext.getCmp('gridSolicitudesAgregadas');
		
		if (registros != null) {
			
			var el 							= gridSolicitudesAgregadas.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
				
			}
			
		}
		
	}
	
	var solicitudesAgregadasData = new Ext.data.ArrayStore({
			
		storeId: 'solicitudesAgregadasDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaSolicitudesAgregadas,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaSolicitudesAgregadas(null, null, null);						
				}
			}
		}
		
	});
	
	var renderContenidoGridAcuse = function(value, metaData, record, rowIndex, colIndex, store) {
		
		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: left !important';
		} else {
			metaData.style += 'text-align: right !important';
		}
		return value;
		
	}
	
	var elementosAcuseCargaArchivo = [
		{
			xtype: 	'label',
			id:	 	'labelAutentificacion',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:15px;',
			html:  	'Cargando...'
		},
		{
			xtype: 	'label',
			id:	 	'labelAcuseCifrasControl',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		173,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'west'
					},
					{
						store: 			solicitudesAgregadasData,
						xtype: 			'grid',
						id:				'gridSolicitudesAgregadas',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '80%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		215,
								hidden: 		false,
								hideable:	false,
								fixed:		true
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								align:		'right',
								renderer: 	renderContenidoGridAcuse
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'east'
					}
			]
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			id:			'botonesAcuseCargaArchivo',
			anchor: 		'100%',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			items: [
				{
					xtype: 		'button',
					text: 		'Imprimir PDF',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoImprimir',
					id: 			'botonImprimirPDF',
					urlArchivo: '',
					handler:    function(boton, evento) {
 
						if(Ext.isEmpty(boton.urlArchivo)){
							
							// Cambiar icono
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							// Genera Archivo PDF
							Ext.Ajax.request({
								url: 			'29califcarte01ext.data.jsp',
								params: 		{ 
									informacion: 								'GeneraArchivoPDF',
									totalRegistros:							Ext.getCmp("panelAcuseCargaArchivo.totalRegistros").getValue(),
									numeroSumatoriaExposicionIncumplimiento:	Ext.getCmp("panelAcuseCargaArchivo.numeroSumatoriaExposicionIncumplimiento").getValue()									
								},
								callback: 	procesarSuccessFailureGeneraArchivoPDF
							});
							
						// Descargar el archivo generado	
						} else {
							
							var forma 		= Ext.getDom('formAux');
							forma.action 	= boton.urlArchivo;
							forma.submit();
							
						}
						
					},
					style: {
						width: 137
					}
				},
				{
					xtype: 		'button',
					text: 		'Salir',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoAceptar',
					id: 			'botonSalir',
					handler:    function(boton, evento) {
						cargaArchivo("FIN",null);
					},
					style: {
						width: 137
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.claveProceso',
        id: 	'panelAcuseCargaArchivo.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.totalRegistros',
        id: 	'panelAcuseCargaArchivo.totalRegistros'
      },
      // INPUT HIDDEN: A�O
      {  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.claveAnio',
        id: 	'panelAcuseCargaArchivo.claveAnio'
      },
      // INPUT HIDDEN: NUMERO DE TRIMESTRE
      {  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.claveTrimestre',
        id: 	'panelAcuseCargaArchivo.claveTrimestre'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD
      {  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.numeroRegistros',
        id: 	'panelAcuseCargaArchivo.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE IMPORTES DE EXPOSICI�N AL INCUMPLIMIENTO
      {  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.numeroSumatoriaExposicionIncumplimiento',
        id: 	'panelAcuseCargaArchivo.numeroSumatoriaExposicionIncumplimiento'
      }
	];
	
	var panelAcuseCargaArchivo = {
		title:			'Acuse de la Carga',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelAcuseCargaArchivo',
		width: 			650, //949,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosAcuseCargaArchivo
	}
	
	//----------------------------- 6. PANEL PREACUSE CARGA ---------------------------
	
	var procesarConsultaSolicitudesPorAgregar = function(store, registros, opts){
		
		var gridSolicitudesPorAgregar 			= Ext.getCmp('gridSolicitudesPorAgregar');
		
		if (registros != null) {
			
			var el 							= gridSolicitudesPorAgregar.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
				
			}
			
		}
		
	}
	
	var solicitudesPorAgregarData = new Ext.data.ArrayStore({
			
		storeId: 'solicitudesPorAgregarDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaSolicitudesPorAgregar,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaSolicitudesPorAgregar(null, null, null);						
				}
			}
		}
		
	});
	
	var renderContenidoGridPreacuse = function(value, metaData, record, rowIndex, colIndex, store) {
		
		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: left !important';
		} else {
			metaData.style += 'text-align: right !important';
		}
		return value;
		
	}
	
	var elementosPreacuseCargaArchivo = [
		{
			xtype: 	'label',
			id:	 	'labelPreacuseCargaArchivo',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		95,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'west'
					},
					{
						store: 			solicitudesPorAgregarData,
						xtype: 			'grid',
						id:				'gridSolicitudesPorAgregar',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '80%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		215,
								hidden: 		false,
								hideable:	false,
								fixed:		true
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								//align:		'right',
								renderer: 	renderContenidoGridPreacuse
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'east'
					}
			]
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonPreacuseCancelar',
					margins: 	' 3',
					handler:    function(boton, evento) {
						
						Ext.MessageBox.confirm(
							'Confirm', 
							"�Esta usted seguro de cancelar la operaci�n?", 
							function(confirmBoton){   
								if( confirmBoton == "yes" ){
									cargaArchivo("FIN",null);
								}
							}
						);
						
					},
					style: {
						width: 100
					}
				},
				{
					xtype: 		'button',
					//width: 		80,
					text: 		'Transmitir Registros',
					iconCls: 	'icoContinuar',
					id: 			'botonTransmitirRegistros',
					margins: 	' 3',
					handler:    function(boton, evento) {
						
						// Obtener texto a firmar
						var solicitudesPorAgregarData = Ext.StoreMgr.key('solicitudesPorAgregarDataStore');
						var textoFirmar = ""; 
						solicitudesPorAgregarData.each(
							function(record){
								textoFirmar += record.get("DESCRIPCION") + ": " + record.get("CONTENIDO")+"\n";
							}
						);
						// Suprimir acentos
						textoFirmar = textoFirmar.replace(/�/g, "a").replace(/�/g, "e").replace(/�/g, "i").replace(/�/g, "o").replace(/�/g, "u");
						textoFirmar = textoFirmar.replace(/�/g, "A").replace(/�/g, "E").replace(/�/g, "I").replace(/�/g, "O").replace(/�/g, "U");
						
						// Firmar texto
						NE.util.obtenerPKCS7(transmitir, textoFirmar);
						
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.claveProceso',
        id: 	'panelPreacuseCargaArchivo.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.totalRegistros',
        id: 	'panelPreacuseCargaArchivo.totalRegistros'
      },
      // INPUT HIDDEN: A�O
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.claveAnio',
        id: 	'panelPreacuseCargaArchivo.claveAnio'
      },
      // INPUT HIDDEN: NUMERO DE TRIMESTRE
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.claveTrimestre',
        id: 	'panelPreacuseCargaArchivo.claveTrimestre'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.numeroRegistros',
        id: 	'panelPreacuseCargaArchivo.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE IMPORTES DE EXPOSICI�N AL INCUMPLIMIENTO
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.numeroSumatoriaExposicionIncumplimiento',
        id: 	'panelPreacuseCargaArchivo.numeroSumatoriaExposicionIncumplimiento'
      }
	];
	
	var transmitir = function(pkcs7, textoFirmar) {
		// Si no hay firma, cancelar la operacion
		if( Ext.isEmpty(pkcs7) ){
			return;
		// Transmitir registros
		} else {
			var respuesta 					= new Object();
			respuesta['pkcs7'] 			= pkcs7;
			respuesta['textoFirmado'] 	= textoFirmar;
			cargaArchivo("TRANSMITIR_REGISTROS",respuesta);
		}
	}
	
	var panelPreacuseCargaArchivo = {
		title:			'Pre-Acuse',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelPreacuseCargaArchivo',
		width: 			550, //949,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosPreacuseCargaArchivo
	}
	
	//---------------------------- 5. PANEL LAYOUT DE CARGA ------------------------------
	
	var hidePanelCalificacionLayout = function(){
		Ext.getCmp('panelCalificacionLayout').hide();
		Ext.getCmp('espaciadorPanelCalificacionLayout').hide();
	}
	
	showPanelCalificacionLayout = function(){
		Ext.getCmp('panelCalificacionLayout').show();
		Ext.getCmp('espaciadorPanelCalificacionLayout').show();
	}
	                             
	var elementosCalificacionLayout = [
		{
			xtype: 	'label',
			id:		'labelLayoutCarga',
			name:		'labelLayoutCarga',
			html: 	"" ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginBottom: 	'10px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		}
	];
	
	var panelCalificacionLayout = {
		xtype:			'panel',
		id: 				'panelCalificacionLayout',
		hidden:			true,
		width: 			700,
		title: 			'Descripci�n del Layout de Carga',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosCalificacionLayout,
		tools: [
			{
				id:			'close',
				handler: function(event, toolEl, panel){
					hidePanelCalificacionLayout();
				}
    		}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      )
	}
	
	var espaciadorPanelCalificacionLayout = {
		xtype: 	'box',
		id:		'espaciadorPanelCalificacionLayout',
		hidden:	true,
		height: 	10,
		width: 	800
	}
	
	//------------------------- 4. PANEL RESULTADOS VALIDACION ---------------------------
 
	var procesarConsultaSolicitudesCorrectas 	= function(store, registros, opts){
		
		var gridSolicitudesCorrectas 						= Ext.getCmp('gridSolicitudesCorrectas');
		
		if (registros != null) {
			
			var el 							= gridSolicitudesCorrectas.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var procesarConsultaSolicitudesConErrores = function(store, registros, opts){
		
		var gridSolicitudesConErrores						= Ext.getCmp('gridSolicitudesConErrores');
		
		if (registros != null) {
			
			var el 							= gridSolicitudesConErrores.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var procesarConsultaErroresVsCifrasControl = function(store, registros, opts){
		
		var gridErroresVsCifrasControl = Ext.getCmp('gridErroresVsCifrasControl');
		
		if (registros != null) {
			
			var el 							= gridErroresVsCifrasControl.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var solicitudesCorrectasData = new Ext.data.ArrayStore({
			
		storeId: 'solicitudesCorrectasDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'NUMERO_LINEA',   mapping: 0, type: 'int' },
			{ name: 'NUMERO_CREDITO', mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaSolicitudesCorrectas,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaSolicitudesCorrectas(null, null, null);						
				}
			}
		}
		
	});
	
	var solicitudesConErroresData = new Ext.data.ArrayStore({
			
		storeId: 'solicitudesConErroresDataStore',
		fields:  [
			{ name: 'NUMERO_LINEA',   	mapping: 0, type: 'int' },
			{ name: 'DESCRIPCION', 		mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaSolicitudesConErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaSolicitudesConErrores(null, null, null);						
				}
			}
		}
		
	});
	
	var erroresVsCifrasControlData  = new Ext.data.ArrayStore({
			
		storeId: 'erroresVsCifrasControlDataStore',
		fields:  [
			{ name: 'NUMERO_LINEA',   	mapping: 0, type: 'int' },
			{ name: 'DESCRIPCION', 		mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaErroresVsCifrasControl,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaErroresVsCifrasControl(null, null, null);						
				}
			}
		}
		
	});

	var elementosResultadosValidacion = [
		{ 
			xtype: 			'tabpanel',
			id:				'tabPanelResultadosValidacion',
			activeTab:		0,
			plain:			true,
			height: 			400,
			bodyStyle:		'padding:10px',
			items: [
				{
					xtype:		'container',
					title: 		'Solicitudes Correctas',
					layout:		'vbox',
               /*layout: {			// Nota: usando esta configuracion flex no funciona bien
						type:'vbox',
                  align:'stretch'
					},
					*/
					items: [
						{ 
							store: 		solicitudesCorrectasData,
							xtype: 		'grid',
							id:			'gridSolicitudesCorrectas',
							stripeRows: true,
							loadMask: 	true,	
							frame: 		false,
							flex:			1,
							columns: [
								{
									header: 		'Linea',
									tooltip: 	'Linea',
									dataIndex: 	'NUMERO_LINEA',
									sortable: 	true,
									resizable: 	true,
									width: 		50,
									hidden: 		true,
									hideable:	false
								},
								{
									header: 		'N�meros de Cr�dito',
									tooltip: 	'N�meros de Cr�dito',
									dataIndex: 	'NUMERO_CREDITO',
									sortable: 	true,
									resizable: 	true,
									width: 		675,
									hidden: 		false
								}
							]
						},
						{
							xtype: 		'container',
							layout:		'form',
							labelWidth: 260,
							style:		"padding:8px;",
							height:		102,
							width:		460,
							items: [
								// TEXTFIELD Total de Registros
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Total de Registros',
									name: 			'numeroRegistrosSinErrores',
									id: 				'numeroRegistrosSinErrores',
									readOnly:		true,
									hidden: 			false,
									maxLength: 		29,	 // Maximo 10 digitos
									msgTarget: 		'side',
									style: {
									  textAlign: 'right',
									  width:		 '100%'
								  }
								},
								// TEXTFIELD Sumatoria de Exposici�n al Incumplimiento
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Sumatoria de Exposici�n al Incumplimiento',
									name: 			'numeroImportesExposicionIncumplimientoSinErrores',
									id: 				'numeroImportesExposicionIncumplimientoSinErrores',
									readOnly:		true,
									hidden: 			false,
									maxLength: 		29,	// Maximo 16 digitos y punto decimal
									msgTarget: 		'side',
									style: {
									  textAlign: 'right',
									  width:		 '100%'
								  }
								}
							]
						}
					]
				},
				{
					xtype:		'container',
					title: 		'Solicitudes con Errores',
					layout:		'vbox',
					 /*layout: {			// Nota: usando esta configuracion flex no funciona bien
						type:'vbox',
                  align:'stretch'
					},
					*/
					items: [
						{
							store: 		solicitudesConErroresData,
							xtype: 		'grid',
							id:			'gridSolicitudesConErrores',
							stripeRows: true,
							loadMask: 	true,		
							frame: 		false,
							flex:			1,
							columns: [
								{
									header: 		'Linea',
									tooltip: 	'Linea',
									dataIndex: 	'NUMERO_LINEA',
									sortable: 	true,
									resizable: 	true,
									width: 		50,
									hidden: 		true,
									hideable:	false
								},
								{
									header: 		'Descripci�n',
									tooltip: 	'Descripci�n',
									dataIndex: 	'DESCRIPCION',
									sortable: 	true,
									resizable: 	true,
									width: 		1150,
									hidden: 		false
								}
							]					
						},
						{
							xtype: 		'container',
							layout:		'form',
							labelWidth: 260,
							style:		"padding:8px;",
							height:		102,
							width:		460,
							items: [
								// TEXTFIELD Total de Registros
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Total de Registros',
									name: 			'numeroRegistrosConErrores',
									id: 				'numeroRegistrosConErrores',
									readOnly:		true,
									hidden: 			false,
									maxLength: 		29,	// Maximo 10 digitos	
									msgTarget: 		'side',
									style: {
									  textAlign: 'right',
									  width:		 '100%'
								  }
								},
								// TEXTFIELD Sumatoria de Exposici�n al Incumplimiento
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Sumatoria de Exposici�n al Incumplimiento',
									name: 			'numeroImportesExposicionIncumplimientoConErrores',
									id: 				'numeroImportesExposicionIncumplimientoConErrores',
									readOnly:		true,
									hidden: 			false,
									maxLength: 		29,	// Maximo 16 digitos y un punto decimal	
									msgTarget: 		'side',
									style: {
									  textAlign: 'right',
									  width:		 '100%'
								  }
								}
							]
						}
					]
				},
				{
					title: 		'Errores vs. Cifras de Control',
					store: 		erroresVsCifrasControlData,
					xtype: 		'grid',
					id:			'gridErroresVsCifrasControl',
					stripeRows: true,
					loadMask: 	true,
					columns: [
						{
							header: 		'Linea',
							tooltip: 	'Linea',
							dataIndex: 	'NUMERO_LINEA',
							sortable: 	true,
							resizable: 	true,
							width: 		50,
							hidden: 		true,
							hideable:	false
						},
						{
							header: 		'Descripci�n',
							tooltip: 	'Descripci�n',
							dataIndex: 	'DESCRIPCION',
							sortable: 	true,
							resizable: 	true,
							width: 		1150,
							hidden: 		false
						}
					]
				}
			]
		},
		{
			xtype: 			'container',
			width: 			'100%',
			style: 			'margin: 8px;padding-top:15px;',
			renderHidden: 	true,
			layout: {
				type: 		'hbox',
				pack: 		'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonCancelarInsercion',
					margins: 	' 3',
					handler:    function(boton, evento) {
						cargaArchivo("FIN",null);
					},
					style: {
						width: 	100
					}
				},
				{
					xtype: 		'button',
					//width: 		100,
					text: 		'Continuar Carga',
					iconCls: 	'icoContinuar',
					id: 			'botonContinuarCarga',
					margins: 	' 3',
					handler:    function(boton, evento) {
						cargaArchivo("PRESENTAR_PREACUSE",null);
					}/*,
					style: {
						width: 100
					}*/
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.claveProceso',
        id: 	'panelResultadosValidacion.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.totalRegistros',
        id: 	'panelResultadosValidacion.totalRegistros'
      },
      // INPUT HIDDEN: A�O
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.claveAnio',
        id: 	'panelResultadosValidacion.claveAnio'
      },
      // INPUT HIDDEN: NUMERO DE TRIMESTRE
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.claveTrimestre',
        id: 	'panelResultadosValidacion.claveTrimestre'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.numeroRegistros',
        id: 	'panelResultadosValidacion.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE IMPORTES DE EXPOSICI�N AL INCUMPLIMIENTO
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.numeroSumatoriaExposicionIncumplimiento',
        id: 	'panelResultadosValidacion.numeroSumatoriaExposicionIncumplimiento'
      }
 
	];
	
	var panelResultadosValidacion = {
		title:			'Resultado de la Validaci�n',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosValidacion',
		width: 			700, //949,
		frame: 			true,
		style: 			'margin:  0 auto',
		bodyStyle:		'padding: 0px',
		items: 			elementosResultadosValidacion
	}

	//---------------------------- PANEL AVANCE VALIDACION ------------------------------
	
	var elementosAvanceValidacion = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
			text: 	'Revisando solicitudes...'
		},
		{
			xtype: 'progress',
			id:	 'barrarAvanceValidacion',
			name:	 'barrarAvanceValidacion',
			cls:	 'center-align',
			value: 0.3756,
			text:  '37.56 %'
		}
	];
	
	var panelAvanceValidacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelAvanceValidacion',
		frame: 			true,
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		items: 			elementosAvanceValidacion
	});
	
	var showWindowAvanceValidacion = function(){
		
		var barrarAvanceValidacion		= Ext.getCmp("barrarAvanceValidacion");
 
		barrarAvanceValidacion.updateProgress(0.00,"0.00 %",true);
		barrarAvanceValidacion.wait({
            interval:	200,
            increment:	15
      });
 
		var ventana = Ext.getCmp('windowAvanceValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				resizable:		false,
				id: 				'windowAvanceValidacion',
				modal:			true,
				closable: 		false,
				width:			Ext.isIE7?300:300, //undefined,
				height: 			Ext.isIE7?144:undefined,
				items: [
					Ext.getCmp("panelAvanceValidacion")
				]
			}).show();
			
		}
		
	}	

	var hideWindowAvanceValidacion = function(){
		
		var ventanaAvanceValidacion = Ext.getCmp('windowAvanceValidacion');
		
		if(ventanaAvanceValidacion && ventanaAvanceValidacion.isVisible() ){
			var barrarAvanceValidacion	 = Ext.getCmp("barrarAvanceValidacion");
			barrarAvanceValidacion.reset(); 
			
			
			if (ventanaAvanceValidacion) {
				ventanaAvanceValidacion.hide();
			}
      }
      
	}
	
	//-------------------------- 3. FORMA DE CARGA DE ARCHIVO ---------------------------
 
	var elementosFormaCargaArchivo = [
		{
		  xtype: 		'fileuploadfield',
		  id: 			'archivo',
		  name: 			'archivo',
		  emptyText: 	'Seleccione...',
		  fieldLabel: 	"Ruta del Archivo de Registros", 
		  buttonText: 	null,
		  buttonCfg: {
			  iconCls: 	'upload-icon'
		  },
		  anchor: 		'-20'
		  //vtype: 		'archivotxt'
		},
		{
			xtype: 		'panel',
			anchor: 		'100%',
			style: {
				marginTop: 		'10px'
			},
			layout: {
				type: 'hbox',
				pack: 'end',
				align: 'middle'
			},
			items: [
				{
					xtype: 			'button',
					text: 			'Continuar',
					iconCls: 		'icoContinuar',
					id: 				'botonContinuarCargaArchivo',				
					handler:    function(boton, evento) {
						
						// Revisar si la forma es invalida
						var forma = Ext.getCmp("panelFormaCargaArchivo").getForm();
						if(!forma.isValid()){
							return;
						}
	 
						// Validar que se haya especificado un archivo
						var archivo = Ext.getCmp("archivo");
						if( Ext.isEmpty( archivo.getValue() ) ){
							archivo.markInvalid("Debe especificar un archivo");
							return;
						}
						
						// Revisar el tipo de extension del archivo
						var myRegexTXT = /^.+\.([tT][xX][tT])$/;
						var myRegexZIP = /^.+\.([zZ][iI][pP])$/;
						
						var nombreArchivo 	= archivo.getValue();
						var numeroRegistros 	= Ext.getCmp("numeroRegistros1").getValue();
						if( numeroRegistros >= 5000 && !myRegexZIP.test(nombreArchivo) ){
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n zip.");
							return;
						} else if( !myRegexTXT.test(nombreArchivo) && !myRegexZIP.test(nombreArchivo) ) {
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n zip o txt.");
							return;
						}
						
						// Cargar archivo
						cargaArchivo("SUBIR_ARCHIVO",null);

					}
				}
			]
		},
		{
			xtype: 	'label',
			anchor:  '100%',
			html: 	"<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" width=\"100%\" > "  +
						"   <tr>"  +
						"	    <td class=\"celda01\" align=\"center\" colspan=\"10\"  style=\"height:30px;\" >"  +
						"         Si el archivo que va a enviar contiene 5,000 registros o m�s, deber� convertirlo a formato .ZIP antes de iniciar la carga "  +
						"	    </td>"  +
						"   </tr>"  +
						"</table>" ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginTop: 		'20px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		},
		{
			xtype: 	'hidden',
			name:		'claveAnio1',
			id:		'claveAnio1'
		},
		{
			xtype: 	'hidden',
			name:		'claveTrimestre1',
			id:		'claveTrimestre1'
		},
		{
			xtype: 	'hidden',
			name:		'numeroRegistros1',
			id:		'numeroRegistros1'
		},
		{
			xtype: 	'hidden',
			name:		'numeroSumatoriaExposicionIncumplimiento1',
			id:		'numeroSumatoriaExposicionIncumplimiento1'
		}
	];
	
	var panelFormaCargaArchivo = new Ext.form.FormPanel({
		id: 				'panelFormaCargaArchivo',
		width: 			700,
		title: 			'Carga de Archivo',
		frame: 			true,
		collapsible: 	false,
		titleCollapse: false,
		fileUpload: 	true,
		hidden:			true,
		renderHidden:	true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	190,
		defaultType: 	'textfield',
		layoutConfig: {
			fieldTpl: new Ext.XTemplate(
				'<tpl if="id==\'archivo\'">',
           		 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">',
						  		'<a class="x-tool x-tool-ayudaLayout" onfocus="blur()" style="float:left;" onclick="javascript:showPanelCalificacionLayout();"></a>&nbsp;',
						  		'{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
						  '</div>',
					 '</div>',
			  '</tpl>',
			  '<tpl if="id!=\'archivo\'">',
					 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
					 '</div>',
			  '</tpl>'
		   )
		},
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementosFormaCargaArchivo,
		monitorValid: 	false
	});
	
	var espaciadorpanelFormaCargaArchivo = {
		xtype: 	'box',
		id:		'espaciadorpanelFormaCargaArchivo',
		hidden:	true,
		height: 	10,
		width: 	800
	}
	
	//------------------------------- 2. PANEL SOLICITUD DE CARGA --------------------------------
	var catalogoAnioData = new Ext.data.JsonStore({
		id: 			'catalogoAnioDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'29califcarte01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoAnio'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load: function(store,records,options){
				
						// Leer valor default
						var defaultValue 		= null;
						var existeParametro	= true;
						try {
							defaultValue = String(options.params.defaultValue);
						}catch(err){
							existeParametro	= false;
							defaultValue 		= null;
						}
								
						// Si se especific� un valor por default
						if( existeParametro ){ 
									
							var index = store.findExact( 'clave', defaultValue ); 
							// El registro fue encontrado por lo que hay que seleccionarlo
							if (index >= 0){
								Ext.getCmp("comboAnio").setValue(defaultValue);
							}
							//Ext.getCmp("comboAnio").setReadOnly(true);
								
						} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoTrimestreData = new Ext.data.JsonStore({
		id: 			'catalogoTrimestreDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'29califcarte01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoTrimestre'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load: function(store,records,options){
				
						// Leer valor default
						var defaultValue 		= null;
						var existeParametro	= true;
						try {
							defaultValue = String(options.params.defaultValue);
						}catch(err){
							existeParametro	= false;
							defaultValue 		= null;
						}
								
						// Si se especific� un valor por default
						if( existeParametro ){ 
									
							var index = store.findExact( 'clave', defaultValue ); 
							// El registro fue encontrado por lo que hay que seleccionarlo
							if (index >= 0){
								Ext.getCmp("comboTrimestre").setValue(defaultValue);
							}
							//Ext.getCmp("comboTrimestre").setReadOnly(true);
								
						} 
								
					},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementosFormaSolicitudDeCarga = [
		// COMBO A�O 
		{
			xtype: 				'combo',
			name: 				'anio',
			id: 					'comboAnio',
			fieldLabel: 		'A�o del trimestre al que corresponde la informaci�n',
			allowBlank: 		false,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveAnio',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoAnioData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			msgTarget: 			'side',
			anchor:				'-20',
			listeners: {
				select: function(combo, record, index ) { 
					var selectedValue 			= combo.getValue(); 
					var catalogoTrimestreData  = Ext.StoreMgr.key('catalogoTrimestreDataStore');
					catalogoTrimestreData.load({
						params: { anio: selectedValue }
					});
				}
			}
		},
		// COMBO TRIMESTRE
		{
			xtype: 				'combo',
			name: 				'trimestre',
			id: 					'comboTrimestre',
			fieldLabel: 		'Trimestre al que corresponde la informaci�n',
			allowBlank: 		false,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveTrimestre',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTrimestreData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20'
		},
		// NUMBERFIELD NUMERO DE REGISTROS EN LA SOLICITUD
		{ 
			xtype: 			'numberfield',
			name: 			'numeroRegistros',
			id: 				'numeroRegistros',
			allowDecimals: false,
			allowNegative: false,
			fieldLabel: 	'N�mero de registros que contiene la solicitud',
			allowBlank: 	false,
			hidden: 			false,
			maxLength: 		30,	
			msgTarget: 		'side',
			anchor:			'-20',
			style: {
           textAlign: 'right'
        }
		},
		// NUMBERFIELD SUMATORIA EXPOSICI�N AL INCUMPLIMIENTO
		{ 
			xtype: 			'numberfield',
			name: 			'numeroSumatoriaExposicionIncumplimiento',
			id: 				'numeroSumatoriaExposicionIncumplimiento',
			fieldLabel:		'Exposici�n al Incumplimiento',
			blankText:		'Favor de capturar la sumatoria de exposici�n al incumplimiento',
			allowBlank:		false,
			hidden: 			false,
			maxLength: 		30,	
			msgTarget: 		'side',
			anchor:			'-20', //necesario para mostrar el icono de error
			style: {
           textAlign: 	'right'
        }
		}
	];
	
	var panelFormaSolicitudDeCarga 		= new Ext.form.FormPanel({
		id: 				'panelFormaSolicitudDeCarga',
		width: 			700,
		title: 			'Solicitud de Carga de Calificaci�n de Cartera',
		frame: 			true,
		hidden:			true,
		collapsible: 	false,
		titleCollapse: false,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		renderHidden:	true, // Nota: no usar hidden: true -> hara que los componentes no se muestren del tamanio correcto
		labelWidth: 	400,
		defaultType: 	'textfield',
		items: 			elementosFormaSolicitudDeCarga,
		monitorValid: 	false,
		buttons: [
			{
				id:		'botonEnviar',
				text: 	'Enviar',
				hidden: 	false,
				iconCls: 'icoContinuar',
				handler: function() {
					
					// Cancelar validaciones adicionales si alguno de los campos de la forma
					// viene vacio
					var formaSolicitudDeCarga = Ext.getCmp("panelFormaSolicitudDeCarga").getForm();
					if( !formaSolicitudDeCarga.isValid() ){
						return;
					}

					// Enviar solicitud
					respuesta = formaSolicitudDeCarga.getValues();
					cargaArchivo("ENVIAR_SOLICITUD_DE_CARGA", respuesta);
					
				}
				
			},
			// BOTON LIMPIAR
			{
				text: 	'Limpiar',
				hidden: 	false,
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('contenedorPrincipal').doLayout(true,true);
					Ext.getCmp('panelFormaSolicitudDeCarga').getForm().reset();
				}
			}
		]
	});
 
	//------------------------------------ 1. PANEL AVISOS ---------------------------------------
	
	var elementosPanelAvisos = [
		{
			xtype: 	'label',
			id:	 	'labelAviso',
			cls:		'x-form-item',
			style: 	'font-weight:normal;text-align:center;margin:15px;color:black;',
			html:  	''
		}
	];
	
	var panelAvisos = {
		xtype:			'panel',
		id: 				'panelAvisos',
		hidden:			true,
		width: 			700,
		title: 			'Avisos',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosPanelAvisos
	}
	
	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelAvisos,
			panelFormaSolicitudDeCarga,
			panelFormaCargaArchivo,
			espaciadorpanelFormaCargaArchivo,
			panelResultadosValidacion,
			espaciadorPanelCalificacionLayout,
			panelCalificacionLayout,
			panelPreacuseCargaArchivo,
			panelAcuseCargaArchivo
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	cargaArchivo("INICIALIZACION",null);
 
});;