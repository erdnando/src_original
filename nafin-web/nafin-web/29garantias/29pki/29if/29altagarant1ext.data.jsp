<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		com.netro.seguridadbean.*,
		java.util.Date,
		java.text.SimpleDateFormat,
		java.util.Calendar,
		com.netro.exception.*, 
		netropology.utilerias.*,
		com.netro.garantias.*,
		com.netro.afiliacion.ConsCargaProvEcon, 
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.afiliacion.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (informacion.equals("CargaArchivo.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;
	
	// 1. Obtener instancia de EJB de Seguridad
   
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
				
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){
 
		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}
	
	// 2. Validar Numero Fiso
	if ( !hayAviso && !seguridad.validaNumeroFISO(claveIF) ) {
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se tiene definido un Fideicomiso para realizar sus operaciones.<br/><br/>Por favor comuníquese al Centro de Atención a clientes al teléfono 50-89-61-07 <br/>o del Interior al 01-800-NAFINSA (01-800-6234672).";
		hayAviso				= true;
	}

	// 3. Remover variables de sesion
	if ( !hayAviso ){
		session.removeAttribute("CLAVES_FINAN");
		session.removeAttribute("rgprint");
	}
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
			
	} catch(Exception e) {
	 
		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
		
	}
	
	if(!hayAviso){
		
		String 			longitudClaveFinanciamiento	= garantias.operaProgramaSHFCreditoHipotecario(claveIF)?"30":"20";
		ReglasPrograma reglasPrograma 					= garantias.getReglasValidacionParametrosPorPrograma(claveIF);

		String saldosLayout = 
		"<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" aling =\"center\" style=\"margin-left:30px;width:600px;background:#FFFFFF;\"> "  +
      "			<tr> "  +
      "				<td class=\"celda01\" align=\"center\" colspan=\"5\" style=\"height:45px;font-weight:bold;\">Layout de Alta de Garant&iacute;as</td> "  +
      "			</tr> "  +
		"<tr>"  +
		"	<td class=\"celda01\" align=\"center\" height=\"45px\">No.</td>"  +
		"	<td class=\"celda01\" align=\"center\">Nombre del Campo</td>"  +
		"	<td class=\"celda01\" align=\"center\">Tipo de Dato</td>"  +
		"	<td class=\"celda01\" align=\"center\">Longitud M&aacute;xima</td>"  +
		"	<td class=\"celda01\" align=\"center\">Condici&oacute;n</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >1</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Nombre o Raz&oacute;n Social del acreditado"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">80</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >2</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Registro Federal de Causante del acreditado"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">13</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >3</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Calle y N&uacute;mero del acreditado"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">80</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >4</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Colonia del acreditado"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">80</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >5</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		C&oacute;digo Postal"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">5</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >6</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Ciudad"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">80</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >7</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Clave del Municipio o Delegaci&oacute;n del acreditado"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">4</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >8</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Clave del Estado o Entidad Federativa del acreditado"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">4</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >9</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Giro de la Actividad a la que se dedica el acreditado"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">300</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >10</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Clave del financiamiento"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">"+longitudClaveFinanciamiento+"</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >11</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Sector de Actividad Econ&oacute;mica"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">8</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >12</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Solicita Fondeo de NAFIN"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">1</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >13</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Solicita la Participaci&oacute;n en el Riesgo de NAFIN"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">1</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >14</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Porcentaje de Participaci&oacute;n en el Riesgo Solicitado."  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">5,2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >15</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Personal Ocupado (n&uacute;mero)"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">10</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >16</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Promedio de Ventas Anual (en pesos)"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">13,2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >17</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Prop&oacute;sito del Proyecto"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >18</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Clave del Tipo de financiamiento"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">6</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" rowspan=\"2\" valign=\"middle\">19</td>"  +
		"	<td class=\"formas\" align=\"left\"   height=\"45px\"  >"  +
		"		19a. Clave Garant&iacute;a Otorgada por el Acreditado"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"left\"   height=\"45px\"  >"  +
		"		19b. Monto de la garant&iacute;a Otorgada"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">12,2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" rowspan=\"4\" valign=\"middle\">20</td>"  +
		"	<td class=\"formas\" align=\"left\"   height=\"45px\" >"  +
		"		20a. Clave del Tipo de Destino de los Recursos"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"left\"   height=\"45px\" >"  +
		"		20b. Porcentaje Parcial"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">3</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"left\"  height=\"45px\" >"  +
		"		20c. Porcentaje de origen nacional"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">3</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"left\"  height=\"45px\" >"  +
		"		20d. Porcentaje de importaci&oacute;n"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">3</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >21</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Porcentaje de la Producci&oacute;n destinada al Mercado Interno"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">5,2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >22</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Porcentaje de la Producci&oacute;n destinada al Mercado Externo"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">5,2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >23</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Forma de Amortizaci&oacute;n"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >24</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Monto del Financiamiento"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">17,2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >25</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Fecha de Disposici&oacute;n y de Participaci&oacute;n en el Riesgo"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">DATE</td>"  +
		"	<td class=\"formas\" align=\"center\">8</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >26</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Importe de Disposici&oacute;n"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">17,2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >27</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Moneda"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">7</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >28</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		N&uacute;mero de Meses de Plazo"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">6</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >29</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		N&uacute;mero de Meses de la Gracia del Financiamiento"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">6</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >30</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Sobretasa"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">7,4</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >31</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Clave del Funcionario Facultado del Intermediario Financiero "  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">8</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >32</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Clave del Intermediario Financiero."  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">5</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >33</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Tipo de Autorizaci&oacute;n"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">6</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >34</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Tipo Programa"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">5</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >35</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Tipo Tasa"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">6</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >36</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Plazo en n&uacute;mero de d&iacute;as"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">2</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >37</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Calificaci&oacute;n Inicial"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">4</td>"  +
		"	<td class=\"formas\" align=\"center\">" + ( reglasPrograma.getProgramaSHFCreditoHipotecario()?"&nbsp;":"Obligatorio" ) + "</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >38</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Antig&uuml;edad como cliente en meses"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">4</td>"  +
		"	<td class=\"formas\" align=\"center\">Obligatorio</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >39</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Nombre del Contacto y/o Representante"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">80</td>"  +
		"	<td class=\"formas\" align=\"center\">&nbsp;</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" rowspan=\"3\" valign=\"middle\">40</td>"  +
		"	<td class=\"formas\" align=\"left\"   height=\"45px\"  >"  +
		"		40a. Clave del Tipo de Tel&eacute;fono"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">2</td>"  +
		"	<td class=\"formas\" align=\"center\">&nbsp;</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"left\"   height=\"45px\">"  +
		"		40b. Tel&eacute;fono con clave lada"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">10</td>"  +
		"	<td class=\"formas\" align=\"center\">&nbsp;</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"left\"   height=\"45px\">"  +
		"		40c. Extensi&oacute;n"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">6</td>"  +
		"	<td class=\"formas\" align=\"center\">&nbsp;</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >41</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Correo Electr&oacute;nico"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
		"	<td class=\"formas\" align=\"center\">40</td>"  +
		"	<td class=\"formas\" align=\"center\">&nbsp;</td>"  +
		"</tr>"  +
		"<tr>"  +
		"	<td class=\"formas\" align=\"center\" height=\"45px\" >42</td>"  +
		"	<td class=\"formas\" align=\"left\">"  +
		"		Dependencia a la que vende"  +
		"	</td>"  +
		"	<td class=\"formas\" align=\"center\">NUMBER</td>"  +
		"	<td class=\"formas\" align=\"center\">4</td>"  +
		"	<td class=\"formas\" align=\"center\">&nbsp;</td>"  +
		"</tr>";
		if(        reglasPrograma.getProgramaSHFCreditoHipotecario()     ){
			saldosLayout +=
			"<tr>"  +
			"   <td class=\"formas\" align=\"center\" height=\"45px\" >43</td>"  +
			"   <td class=\"formas\" align=\"left\">"  +
			"      Tipo de Persona"  +
			"   </td>"  +
			"   <td class=\"formas\" align=\"center\">CHAR</td>"  +
			"   <td class=\"formas\" align=\"center\">1</td>"  +
			"   <td class=\"formas\" align=\"center\">Obligatorio</td>"  +
			"</tr>"  +
			"<tr>"  +
			"   <td class=\"formas\" align=\"center\" height=\"45px\" >44</td>"  +
			"   <td class=\"formas\" align=\"left\">"  +
			"      Clave Única de Registro de Población"  +
			"   </td>"  +
			"   <td class=\"formas\" align=\"center\">CHAR</td>"  +
			"   <td class=\"formas\" align=\"center\">18</td>"  +
			"   <td class=\"formas\" align=\"center\">Obligatorio</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td class=\"formas\" align=\"center\" height=\"45px\" >45</td>"  +
			"	<td class=\"formas\" align=\"left\">"  +
			"		Género del Acreditado"  +
			"	</td>"  +
			"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"	<td class=\"formas\" align=\"center\">1</td>"  +
			"	<td class=\"formas\" align=\"center\">&nbsp;</td>"  +
			"</tr>";
		} else {
			saldosLayout +=
			"<tr>"  +
			"   <td class=\"formas\" align=\"center\" height=\"45px\" >43</td>"  +
			"   <td class=\"formas\" align=\"left\">"  +
			"      Tipo de Persona"  +
			"   </td>"  +
			"   <td class=\"formas\" align=\"center\">CHAR</td>"  +
			"   <td class=\"formas\" align=\"center\">1</td>"  +
			"   <td class=\"formas\" align=\"center\">Obligatorio</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td class=\"formas\" align=\"center\" height=\"45px\" >44</td>"  +
			"	<td class=\"formas\" align=\"left\">"  +
			"		Género del Acreditado"  +
			"	</td>"  +
			"	<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"	<td class=\"formas\" align=\"center\">1</td>"  +
			"	<td class=\"formas\" align=\"center\">&nbsp;</td>"  +
			"</tr>";
		}
		saldosLayout += 
      "		</table> "  +
      "		<span>&nbsp;</span> "  +
      "		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" aling =\"center\" style=\"margin-left:30px;width:600px;\"> "  +
      "			<tr> "  +
      "				<td class=\"celda01\" align=\"center\" style=\"vertical-align:middle;\"> "  +
      "					<table> "  +
      "						<tr> "  +
      "							<td class=\"celda01\"> "  +
      "								Reglamento de los \"Campos obligatorios\" con base en el programa. "  +
      "							</td> "  +
      "							<td align=\"center\" style=\"vertical-align:middle;\"> "  +
      "								<a href=\"JavaScript:descargarReglamento();\" taget=\"_blank\" > "  +
      "									<img src=\"/nafin/00utils/extjs/pdf.png\" width=\"32\" height=\"32\" border=\"0\" > "  +
      "								</a> "  +
      "							</td> "  +
      "						</tr> "  +
      "					</table> ";	
		resultado.put("saldosLayout", 	saldosLayout			);
		
	}
	
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_SOLICITUD_DE_CARGA";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();

} else if (          informacion.equals("CargaArchivo.enviarSolicitudDeCarga") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	
	String claveMes 					= (request.getParameter("claveMes")						== null)?"":request.getParameter("claveMes");
	String numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String sumatoriaSaldosFinMes 	= (request.getParameter("sumatoriaSaldosFinMes")	== null)?"":request.getParameter("sumatoriaSaldosFinMes");
	
	resultado.put("claveMes",					claveMes						);
	resultado.put("numeroRegistros",			numeroRegistros			);
	resultado.put("sumatoriaSaldosFinMes",	sumatoriaSaldosFinMes	);
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	"ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES" 		);
	infoRegresar = resultado.toString();
	
} else if (        	informacion.equals("CargaArchivo.subirArchivo") 				)	{	
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
 
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(8097152);
		myUpload.upload();
		
	} catch(Exception e) {
			
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 8097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 8 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
 
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	String claveMes 					= (myRequest.getParameter("claveMes1")						== null)?"":myRequest.getParameter("claveMes1");
	String numeroRegistros 			= (myRequest.getParameter("numeroRegistros1")			== null)?"":myRequest.getParameter("numeroRegistros1");
	String sumatoriaSaldosFinMes	= (myRequest.getParameter("sumatoriaSaldosFinMes1")	== null)?"":myRequest.getParameter("sumatoriaSaldosFinMes1");

	// Guardar Archivo en Disco
	int numFiles = 0;
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," ")	 );
		resultado.put("fileName", 	myFile.getFileName().replaceAll("\\s+"," ")			);

	}catch(Exception e){
		
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
	
	// Enviar parametros adicionales
	resultado.put("claveMes",					claveMes						);
	resultado.put("numeroRegistros",			numeroRegistros			);
	resultado.put("sumatoriaSaldosFinMes",	sumatoriaSaldosFinMes	);
 
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 		"REALIZAR_VALIDACION" 	);
	// Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success)		);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.realizarValidacion") 				)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveMes 					= (request.getParameter("claveMes")						== null)?"":request.getParameter("claveMes");
	String 		numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldosFinMes 	= (request.getParameter("sumatoriaSaldosFinMes")	== null)?"":request.getParameter("sumatoriaSaldosFinMes");
	String 		fileName 					= (request.getParameter("fileName")						== null)?"":request.getParameter("fileName");
	
	String 		rutaArchivo 				= strDirectorioTemp + iNoUsuario + "." + fileName	;
	String 		claveIF 						= iNoCliente;
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
			
	} catch(Exception e) {
	 
		log.error("CargaArchivo.realizarValidacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
		
	}
			
	// Realizar validacion
	ResultadosGar rg = garantias.procesarAltaGarantia(
									strDirectorioTemp,
									rutaArchivo,
									numeroRegistros,
									sumatoriaSaldosFinMes,
									claveIF
								);
	
	// Guardar en sesión las claves de financiamiento
	List		  clavesFinanciamiento = rg.getClavesFinan();
	session.setAttribute("CLAVES_FINAN",clavesFinanciamiento);
 
	// Leer Resultado de la Validacion
	String claveProceso 		= String.valueOf(rg.getIcProceso());
	String totalRegistros	= String.valueOf(rg.getNumRegOk());
	String montoTotal 		= String.valueOf(rg.getSumRegOk());
	String montoTotalPagar 	= String.valueOf(rg.getSumTotPag());

	//  Leer Resultado de la Validacion ( Registros sin Errores )
	String 		 registrosSinErrores 				  = rg.getCorrectos();
	
		StringBuffer 	buffer 	= new StringBuffer();
		JSONArray		registrosSinErroresDataArray = new JSONArray();
		int				ctaRegistros = 0;
		
		for(int i=0;i<registrosSinErrores.length();i++){
			char lastChar = registrosSinErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosSinErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosSinErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		// Si el primer registro dice: Claves de Financiamiento, borrarlo
		if( 	registrosSinErroresDataArray.size() > 0 ){
				
			JSONArray registro = (JSONArray) registrosSinErroresDataArray.get(0);
			if("Claves de Financiamiento:".equals( registro.getString(1) )){
				registrosSinErroresDataArray.remove(0);
			}
			
		}
 
	String numeroRegistrosSinErrores 		= String.valueOf(rg.getNumRegOk());
	String sumatoriaSaldosFinMesSinErrores	= "$ "+Comunes.formatoDecimal(rg.getSumRegOk(),2);
 
	//  Leer Resultado de la Validacion ( Registros con Errores )
	String registrosConErrores 				= rg.getErrores();

		buffer 			= new StringBuffer();
		JSONArray		registrosConErroresDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<registrosConErrores.length();i++){
			char lastChar = registrosConErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosConErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosConErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		
	String numeroRegistrosConErrores 		= String.valueOf(rg.getNumRegErr());
	String sumatoriaSaldosFinMesConErrores	= "$ " + Comunes.formatoDecimal(rg.getSumRegErr(),2); 
 
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	String erroresVsCifrasControl = rg.getCifras();
	
		buffer 			= new StringBuffer();
		JSONArray		erroresVsCifrasControlDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<erroresVsCifrasControl.length();i++){
			char lastChar = erroresVsCifrasControl.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				erroresVsCifrasControlDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			erroresVsCifrasControlDataArray.add(registro);
			buffer.setLength(0);
		}
		
	// Determinar si se mostrará el boton continuar carga	
	Boolean continuarCarga 								= ("".equals(rg.getErrores())&&"".equals(rg.getCifras()))
																					?new Boolean(true):new Boolean(false);

	// Enviar parametros adicionales
	resultado.put("claveMes",												claveMes												);
	resultado.put("numeroRegistros",										numeroRegistros									);
	resultado.put("sumatoriaSaldosFinMes",								sumatoriaSaldosFinMes							);

	// Enviar resultado general de la validacion
	resultado.put("claveProceso",											claveProceso										);
	resultado.put("totalRegistros", 										totalRegistros										);
	resultado.put("montoTotal",											montoTotal											);
	resultado.put("montoTotalPagar",										montoTotalPagar									);
	
	resultado.put("registrosSinErroresDataArray",					registrosSinErroresDataArray					);
	resultado.put("numeroRegistrosSinErrores",						numeroRegistrosSinErrores						);
	resultado.put("sumatoriaSaldosFinMesSinErrores",				sumatoriaSaldosFinMesSinErrores				);
	
	resultado.put("registrosConErroresDataArray",					registrosConErroresDataArray					);
	resultado.put("numeroRegistrosConErrores",						numeroRegistrosConErrores						);
	resultado.put("sumatoriaSaldosFinMesConErrores",				sumatoriaSaldosFinMesConErrores				);
	
	resultado.put("erroresVsCifrasControlDataArray",				erroresVsCifrasControlDataArray				);
	resultado.put("continuarCarga",										continuarCarga										);
		
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.presentarPreacuse") 			)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveProceso 							= (request.getParameter("claveProceso")					== null)?"":request.getParameter("claveProceso");
	String 		totalRegistros 						= (request.getParameter("totalRegistros")					== null)?"":request.getParameter("totalRegistros");
	String 		montoTotal 								= (request.getParameter("montoTotal")						== null)?"":request.getParameter("montoTotal");
	String 		montoTotalPagar 						= (request.getParameter("montoTotalPagar")				== null)?"":request.getParameter("montoTotalPagar");
	
	String 		claveMes 								= (request.getParameter("claveMes")							== null)?"":request.getParameter("claveMes");
	String 		numeroRegistros 						= (request.getParameter("numeroRegistros")				== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldosFinMes 				= (request.getParameter("sumatoriaSaldosFinMes")		== null)?"":request.getParameter("sumatoriaSaldosFinMes");
	
	// Construir Array con los datos del preacuse
	JSONArray 	registrosPorAgregarDataArray 		= new JSONArray();
	JSONArray	registro									= null;

	// Determinar el mes de carga
	String 	meses[]		={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	//int 		indice 		= Integer.parseInt( claveMes.substring(4,6) ) - 1;
	//String   nombreMes	= meses[indice] ;
	// Determinar el año de la carga
	//String 	numeroAnio 	= claveMes.substring(0,4);

	registro = new JSONArray();
	registro.add("Tipo de Operación");
	registro.add("01 Alta de Garantía");
	registrosPorAgregarDataArray.add(registro);
	/*
	registro = new JSONArray();
	registro.add("Mes de carga");
	registro.add( nombreMes + " de " + numeroAnio );
	registrosPorAgregarDataArray.add(registro);
	*/
	registro = new JSONArray();
	registro.add("No. total de registros transmitidos");
	registro.add(totalRegistros);
	registrosPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("Monto total de los registros transmitidos");
	registro.add(Comunes.formatoDecimal(montoTotal,2));
	registrosPorAgregarDataArray.add(registro);
	
	// Hacer eco de los parametros de la carga
	resultado.put("claveProceso",								claveProceso);
	resultado.put("totalRegistros",							totalRegistros);
	resultado.put("montoTotal",								montoTotal);
	resultado.put("montoTotalPagar",							montoTotalPagar);
	
	resultado.put("claveMes",									claveMes);
	resultado.put("numeroRegistros",							numeroRegistros);
	resultado.put("sumatoriaSaldosFinMes",					sumatoriaSaldosFinMes);

	// Enviar descripcion de los registros por agregar
	resultado.put("registrosPorAgregarDataArray", 		registrosPorAgregarDataArray);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_PREACUSE" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.transmitirRegistros")		)  {
 
	JSONObject		resultado							= new JSONObject();
	String 			estadoSiguiente					= null;
	boolean			success								= true;
	boolean			hayError								= false;
	ResultadosGar 	rg 									= null;
	String 			msg 									= "";
	
	// Leer campos del preacuse
	String claveProceso 				= (request.getParameter("claveProceso")				== null)?"0"	:request.getParameter("claveProceso");
	String totalRegistros 			= (request.getParameter("totalRegistros")				== null)?"0"	:request.getParameter("totalRegistros");
	String montoTotal 				= (request.getParameter("montoTotal")					== null)?"0.00":request.getParameter("montoTotal");
	String montoTotalPagar 			= (request.getParameter("montoTotalPagar")			== null)?"0.00":request.getParameter("montoTotalPagar");
	
	String claveMes 					= (request.getParameter("claveMes")						== null)?""		:request.getParameter("claveMes");
	String numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?""		:request.getParameter("numeroRegistros");
	String sumatoriaSaldosFinMes	= (request.getParameter("sumatoriaSaldosFinMes")	== null)?""		:request.getParameter("sumatoriaSaldosFinMes");

	// Leer campos de la firma digital
	String isEmptyPkcs7 				= (request.getParameter("isEmptyPkcs7")	 			== null)?"false"	:request.getParameter("isEmptyPkcs7");
	String textoFirmado				= (request.getParameter("textoFirmado")	 			== null)?""			:request.getParameter("textoFirmado");
	String pkcs7						= (request.getParameter("pkcs7")			 	 			== null)?""			:request.getParameter("pkcs7");

	if( session.getAttribute("rgprint")!=null ){
		msg 					= "Para realizar otra operacion por favor seleccione la opcion Cargar Archivo en el menu Carga de Saldos y Pago de Comisiones";
		estadoSiguiente 	= "ESPERAR_DECISION";
		hayError				= true;
	} else {
		session.setAttribute("rgprint", new ResultadosGar() ); //Se establece la variable de sesión para controlar envíos duplicados
	} 
	
	if( !hayError ){
		
		// Obtener instancia del EJB de Garantias
		Garantias garantias = null;
		try {
					
			garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

		} catch(Exception e) {
			
			log.error("CargaArchivo.transmitirRegistros(Exception): Obtener instancia del EJB de Garantías");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
			
		}
		
		// Se declaran variables adicionales
		String 									folioCert 		= "";
		String 									externContent 	= textoFirmado;
		char 										getReceipt 		= 'Y';
		netropology.utilerias.Seguridad 	s 					= null;
		String 									claveIF 			= iNoCliente;
 
		/* Nota: Para otros navegadores diferentes de IE y Mozilla
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		*/
			
		// Autenticar certificado
		if ( !_serial.equals("") && !externContent.equals("") && !pkcs7.equals("") ) {
			
			folioCert 	= "02SPC"+claveIF+new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			s 				= new netropology.utilerias.Seguridad();
			
			List clavesFinanciamiento = (List)session.getAttribute("CLAVES_FINAN");
			if ( s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {	
				rg = garantias.transmitirAltaGarantia(
							claveProceso,
							claveIF,
							totalRegistros,
							montoTotal,
							//montoTotalPagar,
							//claveMes,
							//clavesFinanciamiento,
							iNoUsuario
						);
      
				estadoSiguiente 	= "MOSTRAR_ACUSE_TRANSMISION_REGISTROS";
			}else{
				msg 					= "La autentificacion no se llevo a cabo: "+s.mostrarError();
				estadoSiguiente 	= "ESPERAR_DECISION";
				hayError				= true;
			}
			
		}else{
			
			try {
				throw new NafinException("GRAL0021");
			}catch(Exception e){
				estadoSiguiente 	= "ESPERAR_DECISION";
				msg					= e.getMessage();
				hayError				= true;
			}
			
		}
 
	}
	
	// Poner en sesion el resultado de la transmicion de solicitudes
	if( "MOSTRAR_ACUSE_TRANSMISION_REGISTROS".equals(estadoSiguiente) ){
		
		session.setAttribute("rgprint",	rg);
		resultado.put("folioSolicitud", 	"Su solicitud fue recibida con el número de folio: " + rg.getFolio() );
	
		// Determinar el mes de carga
		String 	meses[]		={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
      //int 		indice 		= Integer.parseInt( claveMes.substring(4,6) ) - 1;
		//String   nombreMes	= meses[indice] ;
		// Determinar el año de la carga
		//String 	numeroAnio 	= claveMes.substring(0,4);
	
		// Construir Array con los datos del acuse
		JSONArray 	registrosAgregadosDataArray 	= new JSONArray();
		JSONArray	registro								= null;
		
		registro = new JSONArray();
		registro.add("Tipo de Operación");
		registro.add("01 Alta de Garantía");
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("No. total de registros transmitidos");
		registro.add(totalRegistros);
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Monto total de los registros transmitidos");
		registro.add(Comunes.formatoDecimal(montoTotal,2));
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Fecha de carga");
		registro.add(rg.getFecha());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Hora de carga");
		registro.add(rg.getHora());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Usuario");
		registro.add(iNoUsuario+" - "+strNombreUsuario);
		registrosAgregadosDataArray.add(registro);
 
		resultado.put("registrosAgregadosDataArray",	registrosAgregadosDataArray);
		
		// Hacer eco de los parametros de la carga
		resultado.put("claveProceso",						claveProceso				);
		resultado.put("totalRegistros",					totalRegistros				);
		resultado.put("montoTotal",						montoTotal					);
		resultado.put("montoTotalPagar",					montoTotalPagar			);
		
		//resultado.put("claveMes",							claveMes						);
		resultado.put("numeroRegistros",					numeroRegistros			);
		resultado.put("sumatoriaSaldosFinMes",			sumatoriaSaldosFinMes	);
		
	}
	
	resultado.put("msg",								msg									);	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 			estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
 
} else if (    informacion.equals("CargaArchivo.fin") 							)	{

	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("29cargasaldos01ext.jsp"); 
	
} else if (    informacion.equals("CatalogoMes")									)	{
	
	boolean		success		= true;
	JSONObject	resultado	= new JSONObject();
		
	// Se declaran las variables...
	String 	meses[] 	= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	Calendar cal 		= Calendar.getInstance();
	cal.add(Calendar.MONTH,-11);
	
	// Obtener lista de los ultimos 12 meses
	String 		mesanio 		= null;
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	for(int i=0;i<12;i++){
		
		mesanio = new SimpleDateFormat("yyyyMM").format(cal.getTime());
 
		registro.put("clave",			mesanio );
		registro.put("descripcion",	meses[cal.get(Calendar.MONTH)]+" - " + cal.get(Calendar.YEAR));
		registros.add(registro);
		
		cal.add(Calendar.MONTH,1);
		
	}
 
	// Enviar resultado
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("GeneraArchivoPDF")	)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String 		msg 			= "";
	
	// Leer parametros
	String totalRegistros 	= (request.getParameter("totalRegistros")	== null)?"":request.getParameter("totalRegistros");
	String montoTotal 		= (request.getParameter("montoTotal")		== null)?"":request.getParameter("montoTotal");
	String claveMes 			= (request.getParameter("claveMes")			== null)?"":request.getParameter("claveMes");
	
	String 	urlArchivo 		= "";
	HashMap 	cabecera 		= null;
	try {
		
		// Crear map con la informacion del cabcera del PDF
		cabecera = new HashMap();
		cabecera.put("strPais", 				(String) session.getAttribute("strPais"));
		cabecera.put("iNoNafinElectronico", (String) ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()));
		cabecera.put("sesExterno", 			(String) session.getAttribute("sesExterno"));
		cabecera.put("strNombre", 				(String) session.getAttribute("strNombre"));
		cabecera.put("strNombreUsuario", 	(String) session.getAttribute("strNombreUsuario"));
		cabecera.put("strLogo", 				(String) session.getAttribute("strLogo"));
		
		String nombreArchivo = CargaArchivoGarantias.generaAcuseArchivoPDF(
			(ResultadosGar)session.getAttribute("rgprint"),
			strDirectorioTemp,
			cabecera,
			strDirectorioPublicacion,
			totalRegistros,
			montoTotal,
			iNoUsuario, //loginUsuario
			strNombreUsuario
		);

		urlArchivo = strDirecVirtualTemp+nombreArchivo;
		 
	}catch(Exception e){
			
		log.error("GeneraArchivoPDF(Exception)");
		e.printStackTrace();
		log.error("rg                       = <" + (ResultadosGar)session.getAttribute("rgprint") + ">");
		log.error("strDirectorioTemp        = <" + strDirectorioTemp                              + ">");
		log.error("cabecera                 = <" + cabecera                                       + ">");
		log.error("strDirectorioPublicacion = <" + strDirectorioPublicacion                       + ">");
		log.error("totalRegistros           = <" + totalRegistros                                 + ">");
		log.error("montoTotal               = <" + montoTotal                                     + ">");
		log.error("claveMes                 = <" + claveMes                                       + ">");
		log.error("iNoUsuario               = <" + iNoUsuario                                     + ">");
		log.error("strNombreUsuario         = <" + strNombreUsuario                               + ">");
			
		msg		= e.getMessage();
		success	= false;
			
	}
		
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo ); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)						);
	
	infoRegresar = resultado.toString();
					
}else  if(  informacion.equals("DescargaArchAyuda")	)	{

	String nombreArchivo					= strDirecVirtualTrabajo + "/SIAG_AltaGarantia_ReglamentoCamposObligatorios.pdf";
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", nombreArchivo);
	infoRegresar = jsonObj.toString();

}else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
//log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>