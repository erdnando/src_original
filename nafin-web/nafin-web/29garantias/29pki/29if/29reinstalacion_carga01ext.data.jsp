<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.afiliacion.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("CargaArchivo.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;
	
	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
				
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){
 
		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}
	
	// 2. Validar Numero Fiso
	if ( !hayAviso && !seguridad.validaNumeroFISO(claveIF) ) {
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se tiene definido un Fideicomiso para realizar sus operaciones.<br/><br/>Por favor comuníquese al Centro de Atención a clientes al teléfono 50-89-61-07 <br/>o del Interior al 01-800-NAFINSA (01-800-6234672).";
		hayAviso				= true;
	}

	// 3. Remover variables de sesion
	if ( !hayAviso ){
		session.removeAttribute("rgprint"); 
	}
 
	if(!hayAviso){
		
		String reinstalacionesLayout = 
			"<table>"  +
			"<tr>"  +
			"	<td class=\"titulos\" align=\"center\">"  +
			"		<span class=\"titulos\">"  +
			"			Estructura de Archivo<br>Carga masiva"  +
			"		</span>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td colspan=\"2\" align=\"center\">"  +
			"		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" >"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"center\" colspan=\"10\"  style=\"height:30px;\" >"  +
			"					Layout de Reinstalaciones de Garant&iacute;as<br>"  +
			"					Archivo de extensi&oacute;n TXT &oacute; ZIP; los campos deben de estar separados por arrobas (\"@\")"  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"center\" width=\"25px\"  style=\"height:30px;\" >No.</td>"  +
			"				<td class=\"celda01\" align=\"center\">Nombre del Campo</td>"  +
			"				<td class=\"celda01\" align=\"center\">Tipo</td>"  +
			"				<td class=\"celda01\" align=\"center\">Longitud<br>M&aacute;xima</td>"  +
			"				<td class=\"celda01\" align=\"center\">Obligatorio</td>"  +
			"				<td class=\"celda01\" align=\"center\">Observaciones</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >1</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Clave del intermediario "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">5,0</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					N&uacute;mero de identificaci&oacute;n del intermediario "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >2</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Clave de la garant&iacute;a "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">20</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					No. de pr&eacute;stamo o garant&iacute;a "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >3</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Nombre del acreditado "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">80</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Nombre completo del cliente "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >4</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Fecha honrada "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">DATE</td>"  +
			"				<td class=\"formas\" align=\"center\">8</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Formato: AAAAMMDD "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >5</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Monto honrado "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Total Pagado "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >6</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Monto total de la reinstalaci&oacute;n del acreditado  "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Monto pagado por el cliente "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >7</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Moneda "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">7</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Tipo de Moneda: PESOS, DOLARES. "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >8</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Tipo de recuperaci&oacute;n "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">1</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					3) Recuperaci&oacute;n por reinstalaci&oacute;n  "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >9</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Devoluci&oacute;n de Comisi&oacute;n de Aniversario "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					 &nbsp;"  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >10</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Devoluci&oacute;n de IVA de Comisi&oacute;n de Aniversario "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					 &nbsp;"  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >11</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Total de Devoluci&oacute;n de Comisi&oacute;n e IVA de Aniversario "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					 &nbsp;"  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >12</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Capital reembolsado por el acreditado "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Recuperaci&oacute;n que corresponde a capital "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >13</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Inter&eacute;s reembolsado por el acreditado "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Recuperaci&oacute;n que corresponde a inter&eacute;s ordinario "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >14</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Moratorios reembolsado por el acreditado "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Recuperaci&oacute;n que corresponde a moratorios "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >15</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Fecha del Dep&oacute;sito "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">DATE</td>"  +
			"				<td class=\"formas\" align=\"center\">8</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Formato: AAAAMMDD "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >16</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Importe del Reembolso de Reinstalaci&oacute;n de NAFIN "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Importe del reembolso a NAFIN "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >17</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Importe de Costo Financiero "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Importe de Costo Financiero para pago a NAFIN;<br> si el valor de este campo es mayor a cero, se requiere que el valor del campo 18 sea cero. "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >18</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Penalizaci&oacute;n "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Importe de la penalizaci&oacute;n para pago a NAFIN;<br> si el valor de este campo es mayor a cero, se requiere que el valor del campo 17 sea cero. "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >19</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					IVA generado por Costos Financieros / Penalizaci&oacute;n "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">20,2</td>"  +
			"				<td class=\"formas\" align=\"center\">&nbsp;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					 &nbsp;"  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >20</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Fecha de reembolso a NAFIN "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">DATE</td>"  +
			"				<td class=\"formas\" align=\"center\">8</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Formato: AAAAMMDD "  +
			"				</td>"  +
			"			</tr>"  +
			"		</table>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"</table>";	
		resultado.put("reinstalacionesLayout", 	reinstalacionesLayout			);
		
	}
	
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_SOLICITUD_REINSTALACION";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();

} else if (          informacion.equals("CargaArchivo.enviarSolicitudReinstalacion") )	{
	
	JSONObject	resultado 							= new JSONObject();
	boolean		success	 							= true;
 
	String 		numeroRegistros 					= (request.getParameter("numeroRegistros")					== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaMontosReinstalacion 	= (request.getParameter("sumatoriaMontosReinstalacion")	== null)?"":request.getParameter("sumatoriaMontosReinstalacion");
	
	// Suprimir caracteres de formato: ","
	numeroRegistros									= numeroRegistros.replaceAll(",","");
	sumatoriaMontosReinstalacion					= sumatoriaMontosReinstalacion.replaceAll(",","");
	
	resultado.put("numeroRegistros",						numeroRegistros					);
	resultado.put("sumatoriaMontosReinstalacion",	sumatoriaMontosReinstalacion	);
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	"ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES" 		);
	infoRegresar = resultado.toString();
	
} else if (        	informacion.equals("CargaArchivo.subirArchivo") 				)	{	
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
 
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(8097152);
		myUpload.upload();
		
	} catch(Exception e) {
			
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 8097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 8 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
 
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	String numeroRegistros 							= (myRequest.getParameter("numeroRegistros1")					== null)?"":myRequest.getParameter("numeroRegistros1");
	String sumatoriaMontosReinstalacion			= (myRequest.getParameter("sumatoriaMontosReinstalacion1")	== null)?"":myRequest.getParameter("sumatoriaMontosReinstalacion1");

	// Guardar Archivo en Disco
	int numFiles = 0;
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," ")	 );
		resultado.put("fileName", 	myFile.getFileName().replaceAll("\\s+"," ")			);

	}catch(Exception e){
		
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
	
	// Enviar parametros adicionales
	resultado.put("numeroRegistros",						numeroRegistros					);
	resultado.put("sumatoriaMontosReinstalacion",	sumatoriaMontosReinstalacion	);
 
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 		"REALIZAR_VALIDACION" 	);
	// Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success)		);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.realizarValidacion") 				)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		numeroRegistros 					= (request.getParameter("numeroRegistros")					== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaMontosReinstalacion 	= (request.getParameter("sumatoriaMontosReinstalacion")	== null)?"":request.getParameter("sumatoriaMontosReinstalacion");
	String 		fileName 							= (request.getParameter("fileName")								== null)?"":request.getParameter("fileName");
	
	String 		rutaArchivo 						= strDirectorioTemp + iNoUsuario + "." + fileName	;
	String 		claveIF 								= iNoCliente;
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
			
	} catch(Exception e) {
	 
		log.error("CargaArchivo.realizarValidacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
		
	}
			
	// Realizar validacion
	ResultadosGar rg = garantias.procesarAltaReinstalaciones(
									strDirectorioTemp,
									rutaArchivo,
									numeroRegistros,
									sumatoriaMontosReinstalacion,
									claveIF
								);
 
	// Leer Resultado de la Validacion
	String claveProceso 		= String.valueOf(rg.getIcProceso());
	String totalRegistros	= String.valueOf(rg.getNumRegOk());
	String montoTotal 		= String.valueOf(rg.getSumRegOk());

	//  Leer Resultado de la Validacion ( Registros sin Errores )
	String 		 registrosSinErrores 				  = rg.getCorrectos();
	
		StringBuffer 	buffer 	= new StringBuffer();
		JSONArray		registrosSinErroresDataArray = new JSONArray();
		int				ctaRegistros = 0;
		
		for(int i=0;i<registrosSinErrores.length();i++){
			char lastChar = registrosSinErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosSinErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosSinErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		// Si el primer registro dice: Claves de Financiamiento, borrarlo
		if( 	registrosSinErroresDataArray.size() > 0 ){
				
			JSONArray registro = (JSONArray) registrosSinErroresDataArray.get(0);
			if("Claves de la garantía:".equals( registro.getString(1) )){
				registrosSinErroresDataArray.remove(0);
			}
			
		}
 
	String numeroRegistrosSinErrores 					= Comunes.formatoDecimal(rg.getNumRegOk(),0);
	String sumatoriaMontosReinstalacionSinErrores	= "$ "+Comunes.formatoDecimal(rg.getSumRegOk(),2);
 
	//  Leer Resultado de la Validacion ( Registros con Errores )
	String registrosConErrores 				= rg.getErrores();

		buffer 			= new StringBuffer();
		JSONArray		registrosConErroresDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<registrosConErrores.length();i++){
			char lastChar = registrosConErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosConErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosConErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		
	String numeroRegistrosConErrores 					= Comunes.formatoDecimal(rg.getNumRegErr(),0);
	String sumatoriaMontosReinstalacionConErrores	= "$ " + Comunes.formatoDecimal(rg.getSumRegErr(),2); 
 
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	String erroresVsCifrasControl = rg.getCifras();
	
		buffer 			= new StringBuffer();
		JSONArray		erroresVsCifrasControlDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<erroresVsCifrasControl.length();i++){
			char lastChar = erroresVsCifrasControl.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				erroresVsCifrasControlDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			erroresVsCifrasControlDataArray.add(registro);
			buffer.setLength(0);
		}
		
	// Determinar si se mostrará el boton continuar carga	
	Boolean continuarCarga 								= ("".equals(rg.getErrores())&&"".equals(rg.getCifras()))
																					?new Boolean(true):new Boolean(false);

	// Enviar parametros adicionales
	resultado.put("numeroRegistros",										numeroRegistros									);
	resultado.put("sumatoriaMontosReinstalacion",					sumatoriaMontosReinstalacion					);

	// Enviar resultado general de la validacion
	resultado.put("claveProceso",											claveProceso										);
	resultado.put("totalRegistros", 										totalRegistros										);
	resultado.put("montoTotal",											montoTotal											);
	
	resultado.put("registrosSinErroresDataArray",					registrosSinErroresDataArray					);
	resultado.put("numeroRegistrosSinErrores",						numeroRegistrosSinErrores						);
	resultado.put("sumatoriaMontosReinstalacionSinErrores",		sumatoriaMontosReinstalacionSinErrores		);
	
	resultado.put("registrosConErroresDataArray",					registrosConErroresDataArray					);
	resultado.put("numeroRegistrosConErrores",						numeroRegistrosConErrores						);
	resultado.put("sumatoriaMontosReinstalacionConErrores",		sumatoriaMontosReinstalacionConErrores		);
	
	resultado.put("erroresVsCifrasControlDataArray",				erroresVsCifrasControlDataArray				);
	resultado.put("continuarCarga",										continuarCarga										);
		
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.presentarPreacuse") 			)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveProceso 							= (request.getParameter("claveProceso")						== null)?"":request.getParameter("claveProceso");
	String 		totalRegistros 						= (request.getParameter("totalRegistros")						== null)?"":request.getParameter("totalRegistros");
	String 		montoTotal 								= (request.getParameter("montoTotal")							== null)?"":request.getParameter("montoTotal");

	String 		numeroRegistros 						= (request.getParameter("numeroRegistros")					== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaMontosReinstalacion 		= (request.getParameter("sumatoriaMontosReinstalacion")	== null)?"":request.getParameter("sumatoriaMontosReinstalacion");
  
	// Obtener Descripción del Tipo de Operación
	String 				tipoOperacion 	= "";
	// Preparar consulta
	CatalogoSimple 	cat 				= new CatalogoSimple();
	cat.setTabla("gticat_tipooperacion");
	cat.setCampoClave("ic_tipo_operacion");
	cat.setCampoDescripcion("cg_descripcion");
	List claves = new ArrayList();
	claves.add("10");	// 10 = REINSTALACION DE GARANTÍAS
	cat.setValoresCondicionIn(claves);
	// Consultar descripcion del Tipo de Operacion 10.
	List listaResultados = cat.getListaElementos();
	if (listaResultados!=null && listaResultados.size()>0) {
		tipoOperacion = Comunes.rellenaCeros(((ElementoCatalogo)listaResultados.get(0)).getClave(),2) + " " + ((ElementoCatalogo)listaResultados.get(0)).getDescripcion();
	}

	// Construir Array con los datos del preacuse
	JSONArray 	registrosPorAgregarDataArray 		= new JSONArray();
	JSONArray	registro									= null;

	registro = new JSONArray();
	registro.add("Tipo de Operación");
	registro.add(tipoOperacion);
	registrosPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("No. total de registros transmitidos");
	registro.add( Comunes.formatoDecimal(totalRegistros,0) );
	registrosPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("Monto total de los registros transmitidos");
	registro.add( Comunes.formatoDecimal(montoTotal,2) 	 );
	registrosPorAgregarDataArray.add(registro);
	
	// Hacer eco de los parametros de la carga
	resultado.put("claveProceso",								claveProceso);
	resultado.put("totalRegistros",							totalRegistros);
	resultado.put("montoTotal",								montoTotal);
	
	resultado.put("numeroRegistros",							numeroRegistros);
	resultado.put("sumatoriaMontosReinstalacion",		sumatoriaMontosReinstalacion);

	// Enviar descripcion de los registros por agregar
	resultado.put("registrosPorAgregarDataArray", 		registrosPorAgregarDataArray);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 						"ESPERAR_DECISION_PREACUSE" );
	// Enviar resultado de la operacion
	resultado.put("success", 									new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.transmitirRegistros")		)  {
 
	JSONObject		resultado				= new JSONObject();
	String 			estadoSiguiente		= null;
	boolean			success					= true;
	boolean			hayError					= false;
	ResultadosGar 	rg 						= null;
	String 			msg 						= "";
	
	// Leer campos del preacuse
	String claveProceso 						= (request.getParameter("claveProceso")						== null)?"0"		:request.getParameter("claveProceso");
	String totalRegistros 					= (request.getParameter("totalRegistros")						== null)?"0"		:request.getParameter("totalRegistros");
	String montoTotal 						= (request.getParameter("montoTotal")							== null)?"0.00"	:request.getParameter("montoTotal");
	
	// Leer campos de entrada
	String numeroRegistros 					= (request.getParameter("numeroRegistros")					== null)?""			:request.getParameter("numeroRegistros");
	String sumatoriaMontosReinstalacion	= (request.getParameter("sumatoriaMontosReinstalacion")	== null)?""			:request.getParameter("sumatoriaMontosReinstalacion");

	// Leer campos de la firma digital
	String isEmptyPkcs7 						= (request.getParameter("isEmptyPkcs7")	 					== null)?"false"	:request.getParameter("isEmptyPkcs7");
	String textoFirmado						= (request.getParameter("textoFirmado")	 					== null)?""			:request.getParameter("textoFirmado");
	String pkcs7								= (request.getParameter("pkcs7")			 	 					== null)?""			:request.getParameter("pkcs7");

	if( session.getAttribute("rgprint")!=null ){
		msg 					= "Para realizar otra operacion por favor seleccione la opcion Cargar Archivo en el menu Reinstalacion de Garantias ";
		estadoSiguiente 	= "ESPERAR_DECISION";
		hayError				= true;
	} else {
		session.setAttribute("rgprint", new ResultadosGar() ); //Se establece la variable de sesión para controlar envíos duplicados
	} 
	
	if( !hayError ){
		
		// Obtener instancia del EJB de Garantias
		Garantias garantias = null;
		try {
					
			garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

		} catch(Exception e) {
			
			log.error("CargaArchivo.transmitirRegistros(Exception): Obtener instancia del EJB de Garantías");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
			
		}
		
		// Se declaran variables adicionales
		String 									folioCert 		= "";
		String 									externContent 	= textoFirmado;
		char 										getReceipt 		= 'Y';
		netropology.utilerias.Seguridad 	s 					= null;
		String 									claveIF 			= iNoCliente;
		String 									loginUsuario	= iNoUsuario;
		
		/* Nota: Para otros navegadores diferentes de IE y Mozilla
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		*/
			
		// Autenticar certificado
		if ( !_serial.equals("") && !externContent.equals("") && !pkcs7.equals("") ) {
			
			folioCert 	= "10ARG"+claveIF+new SimpleDateFormat("ddMMyyyy").format(new java.util.Date());
			s 				= new netropology.utilerias.Seguridad();
 
			if ( s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {	
				
				rg = garantias.transmitirAltaReinstalaciones(
							claveProceso,
							claveIF,
							totalRegistros,
							montoTotal,
							loginUsuario, 
							s.getAcuse()
						);
				estadoSiguiente 	= "MOSTRAR_ACUSE_TRANSMISION_REGISTROS";
				
			}else{
				msg 					= "La autentificacion no se llevo a cabo: "+s.mostrarError();
				estadoSiguiente 	= "ESPERAR_DECISION";
				hayError				= true;
			}
			
		}else{
			
			try {
				throw new NafinException("GRAL0021"); // La autentificacion no se llevo a cabo
			}catch(Exception e){
				estadoSiguiente 	= "ESPERAR_DECISION";
				msg					= e.getMessage();
				hayError				= true;
			}
			
		}
 
	}
	
	// Poner en sesion el resultado de la transmicion de solicitudes
	if( "MOSTRAR_ACUSE_TRANSMISION_REGISTROS".equals(estadoSiguiente) ){
		
		// Obtener Descripción del Tipo de Operación
		String 				tipoOperacion 	= "";
		// Preparar consulta
		CatalogoSimple 	cat 				= new CatalogoSimple();
		cat.setTabla("gticat_tipooperacion");
		cat.setCampoClave("ic_tipo_operacion");
		cat.setCampoDescripcion("cg_descripcion");
		List claves = new ArrayList();
		claves.add("10");	// 10 = REINSTALACION DE GARANTÍAS
		cat.setValoresCondicionIn(claves);
		// Consultar descripcion del Tipo de Operacion 10.
		List listaResultados = cat.getListaElementos();
		if (listaResultados!=null && listaResultados.size()>0) {
			tipoOperacion = Comunes.rellenaCeros(((ElementoCatalogo)listaResultados.get(0)).getClave(),2) + " " + ((ElementoCatalogo)listaResultados.get(0)).getDescripcion();
		}
	
		session.setAttribute("rgprint",	rg);
		resultado.put("folioSolicitud", 	"Su solicitud fue recibida con el número de folio: " + rg.getFolio() );
	
		// Construir Array con los datos del acuse
		JSONArray 	registrosAgregadosDataArray 	= new JSONArray();
		JSONArray	registro								= null;
 
		registro = new JSONArray();
		registro.add("Tipo de Operación");
		registro.add(tipoOperacion);
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("No. total de registros transmitidos");
		registro.add(Comunes.formatoDecimal(totalRegistros,0));
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Monto total de los registros transmitidos");
		registro.add(Comunes.formatoDecimal(montoTotal,2));
		registrosAgregadosDataArray.add(registro);
 
		registro = new JSONArray();
		registro.add("Fecha de carga");
		registro.add(rg.getFecha());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Hora de carga");
		registro.add(rg.getHora());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Fecha valor");
		registro.add(rg.getFechaValor());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Usuario");
		registro.add(iNoUsuario+" - "+strNombreUsuario);
		registrosAgregadosDataArray.add(registro);
 
		resultado.put("registrosAgregadosDataArray",		registrosAgregadosDataArray	);
		
		// Hacer eco de los parametros de la carga
		resultado.put("claveProceso",							claveProceso						);
		resultado.put("totalRegistros",						totalRegistros						);
		resultado.put("montoTotal",							montoTotal							);
		
		resultado.put("numeroRegistros",						numeroRegistros					);
		resultado.put("sumatoriaMontosReinstalacion",	sumatoriaMontosReinstalacion	);
		resultado.put("folio",									rg.getFolio()						);
		
	}
	
	resultado.put("msg",											msg									);	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 						estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 									new Boolean(success)				);
	
	infoRegresar = resultado.toString();
 
} else if (    informacion.equals("CargaArchivo.fin") 							)	{

	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("29reinstalacion_carga01ext.jsp"); 
	
} else if ( 	informacion.equals("ExtraerErroresValidacion")					)	{
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	
	// Leer parametros
	String detalleErroresVsCifrasControl	= (request.getParameter("detalleErroresVsCifrasControl")	== null)?"[]":request.getParameter("detalleErroresVsCifrasControl");
	String detalleRegistrosConErrores 		= (request.getParameter("detalleRegistrosConErrores")		== null)?"[]":request.getParameter("detalleRegistrosConErrores");
 
	String 		urlArchivo 		= "";
	try {
		
		String nombreArchivo = CargaArchivoAltaReinstalaciones.generaArchivoCSVErrores(
			detalleErroresVsCifrasControl,
			detalleRegistrosConErrores,
			strDirectorioTemp,
			iNoUsuario // loginUsuario
		); 
		
		urlArchivo = strDirecVirtualTemp+nombreArchivo;
		
	}catch(Exception e){
			
		log.error("ExtraerErroresValidacion(Exception)");
		log.error("ExtraerErroresValidacion.detalleErroresVsCifrasControl = <" + detalleErroresVsCifrasControl + ">");
		log.error("ExtraerErroresValidacion.detalleRegistrosConErrores    = <" + detalleRegistrosConErrores    + ">");
		log.error("ExtraerErroresValidacion.strDirectorioTemp             = <" + strDirectorioTemp             + ">");
		log.error("ExtraerErroresValidacion.iNoUsuario                    = <" + iNoUsuario                    + ">"); // loginUsuario
		e.printStackTrace();
	 
		msg		= e.getMessage();
		success	= false;
			
	}
		
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
	
} else if ( 	informacion.equals("ExtraerRegistrosCargados")					)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String 		msg 			= "";
	
	String 		folio 		= (request.getParameter("folio")		== null)?"":request.getParameter("folio");
	String 		claveIF		= iNoCliente;
	
	System.err.println("folio   = <" + folio   + ">");
	System.err.println("claveIF = <" + claveIF + ">");
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
					
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

	} catch(Exception e) {
			
		log.error("ExtraerRegistrosCargados(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
			
	}
		
	// Obtener el Detalle de Solicitud de Reinstalacion de Garantías
	String 		urlArchivo 		= "";
	try {
		
		HashMap	queryDetalleSolicitud = garantias.getQueryDetalleSolicitudReinstalaciones( claveIF, folio );
		
		String 	nombreArchivo 			 = CargaArchivoAltaReinstalaciones.generaArchivoCSVSolicitudReinstalaciones(
			folio,
			queryDetalleSolicitud,
			strDirectorioTemp
		); 
		
		urlArchivo = strDirecVirtualTemp+nombreArchivo;
 
	}catch(Exception e){
			
		log.error("ExtraerRegistrosCargados(Exception)");
		log.error("ExtraerRegistrosCargados.folio   = <" + folio   + ">");
		log.error("ExtraerRegistrosCargados.claveIF = <" + claveIF + ">");
		e.printStackTrace();
		
		msg		= e.getMessage();
		success	= false;
			
	}
		
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("GeneraArchivoPDF")								)	{
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	
	// Leer parametros
	String 		totalRegistros = (request.getParameter("totalRegistros")	== null)?"":request.getParameter("totalRegistros");
	String 		montoTotal 		= (request.getParameter("montoTotal")		== null)?"":request.getParameter("montoTotal");
	
	String 		urlArchivo 		= "";
	HashMap 		cabecera 		= null;
	try {
		
		// Crear map con la informacion del cabcera del PDF
		cabecera = new HashMap();
		cabecera.put("strPais", 				(String) session.getAttribute("strPais"));
		cabecera.put("iNoNafinElectronico", (String) ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()));
		cabecera.put("sesExterno", 			(String) session.getAttribute("sesExterno"));
		cabecera.put("strNombre", 				(String) session.getAttribute("strNombre"));
		cabecera.put("strNombreUsuario", 	(String) session.getAttribute("strNombreUsuario"));
		cabecera.put("strLogo", 				(String) session.getAttribute("strLogo"));
		
		String nombreArchivo = CargaArchivoAltaReinstalaciones.generaAcuseArchivoPDF(
			(ResultadosGar)session.getAttribute("rgprint"),
			strDirectorioTemp,
			cabecera,
			strDirectorioPublicacion,
			totalRegistros,
			montoTotal,
			iNoUsuario, // loginUsuario
			strNombreUsuario
		); 
		
		urlArchivo = strDirecVirtualTemp+nombreArchivo;
		 
	}catch(Exception e){
			
		log.error("GeneraArchivoPDF(Exception)");
		log.error("GeneraArchivoPDF.rg                       = <" + (ResultadosGar)session.getAttribute("rgprint") + ">");
		log.error("GeneraArchivoPDF.strDirectorioTemp        = <" + strDirectorioTemp                              + ">");
		log.error("GeneraArchivoPDF.cabecera                 = <" + cabecera                                       + ">");
		log.error("GeneraArchivoPDF.strDirectorioPublicacion = <" + strDirectorioPublicacion                       + ">");
		log.error("GeneraArchivoPDF.totalRegistros           = <" + totalRegistros                                 + ">");
		log.error("GeneraArchivoPDF.montoTotal               = <" + montoTotal                                     + ">");
		log.error("GeneraArchivoPDF.iNoUsuario               = <" + iNoUsuario                                     + ">");
		log.error("GeneraArchivoPDF.strNombreUsuario         = <" + strNombreUsuario                               + ">");
		e.printStackTrace();
		
		msg		= e.getMessage();
		success	= false;
			
	}
		
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
 
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>