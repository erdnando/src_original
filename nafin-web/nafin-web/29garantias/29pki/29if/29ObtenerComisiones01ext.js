Ext.onReady(function() {

	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

	Ext.grid.ActionTextColumn = Ext.extend(Ext.grid.ActionColumn, {
			constructor: function(cfg) {
			  var me = this,
					items = cfg.items || (me.items = [me]),
					l = items.length,
					i,
					item;

			  Ext.grid.ActionTextColumn.superclass.constructor.call(me, cfg);

	//      Renderer closure iterates through items creating an <img> element for each and tagging with an identifying
	//      class name x-action-col-{n}
			  me.renderer = function(v, meta) {
	//          Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
					v = Ext.isFunction(cfg.renderer) ? cfg.renderer.apply(this, arguments)||'' : '';

					meta.css += ' x-action-col-cell';
					for (i = 0; i < l; i++) {
						 item = items[i];
						 v += '<img alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
							  '" class="x-action-col-icon x-action-col-' + String(i) + ' ' + (item.iconCls || '') +
							  ' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope||this.scope||this, arguments) : '') + '"' +
							  ((item.tooltip) ? ' ext:qtip="' + item.tooltip + '"' : '') + ' align="middle" />';
							v += Ext.isEmpty(item.text)?"":"&nbsp;"+item.text;
					}
					return v;
			  };
		 }
	});
	Ext.apply(Ext.grid.Column.types, { actiontextcolumn: Ext.grid.ActionTextColumn }  );


	//-------------------------------- VALIDACIONES -----------------------------------

	//----------------------------------- HANDLERS ------------------------------------
   var resetWindowAcuseExtraccionArchivoTXT = function(){

   	// Borrar contenido
      Ext.getCmp("panelFormaAcuseExtraccionArchivoTXT.recordId").setValue(null);
      Ext.getCmp("panelFormaAcuseExtraccionArchivoTXT.extraccionFirmada").setValue(false);
      Ext.getCmp("panelFormaAcuseExtraccionArchivoTXT.urlNombreArchivo").setValue('');

      // Limpiar folio de operacion
		Ext.getCmp("labelAcuseAutentificacion").setText('Cargando...');

		// Suprimir cifras de control
		Ext.StoreMgr.key('registrosDeControlDataStore').removeAll();

	}

	var procesaObtencionComisiones = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						obtencionComisiones(resp.estadoSiguiente,resp);
					}
				);
			} else {
				obtencionComisiones(resp.estadoSiguiente,resp);
			}

		} else {

			// Suprimir mascaras segun se requiera
			obtencionComisiones("ESPERAR_DECISION", null );

			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);

		}

	}

	//----------------------------- "MAQUINA DE ESTADO" -------------------------------

	var obtencionComisiones = function(estado, respuesta ){

		if(			estado == "INICIALIZACION"					){

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ObtenerComisiones01ext.data.jsp',
				params: 	{
					informacion:		'ObtencionComisiones.inicializacion'
				},
				callback: 				procesaObtencionComisiones
			});

		} else if(  estado == "MOSTRAR_AVISO"					){

			if(!Ext.isEmpty(respuesta.aviso)){

				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);

				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();

			}

		} else if(  estado == "ESPERAR_CAPTURA_CONSULTA"   ){

			var panelFormaConsulta = Ext.getCmp("panelFormaConsulta");
			panelFormaConsulta.show();
			panelFormaConsulta.doLayout();

			// Cargar Contenido del Combo Mes
			var catalogoMesData      = Ext.StoreMgr.key('catalogoMesDataStore');
			catalogoMesData.load();

		} else if(  estado == "ESPERAR_DECISION"  			){

			// Se deja al usuario la determinacion del estado siguiente.

			// Limpiar M�scara en la Forma de B�squeda
			var element = Ext.getCmp("panelFormaConsulta").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Resetear Grid de Comisiones
			var gridComisiones = Ext.getCmp("gridComisiones");
			if( gridComisiones.generandoArchivo && !Ext.isEmpty(gridComisiones.recordId) ){

				// Obtener registro
				var record			= 	Ext.StoreMgr.key('comisionesDataStore').getById(gridComisiones.recordId);
				if( Ext.isEmpty(record) ){
					return;
				}

				// Deshabilitar flag que indica que hay una consulta en proceso
				gridComisiones.generandoArchivo	= false;
				gridComisiones.recordId				= null;

				// Suprimir animacion
				if( record.data['EXTRACCION_DETALLADA_TXT_LOADING'] ){
					record.data['EXTRACCION_DETALLADA_TXT_LOADING'] = false;
				} else if (record.data['EXTRACCION_DETALLADA_PDF_LOADING']){
					record.data['EXTRACCION_DETALLADA_PDF_LOADING'] = false;
				} else if (record.data['EXTRACCION_DETALLADA_XLSX_LOADING']){
					record.data['EXTRACCION_DETALLADA_XLSX_LOADING'] = false;
				}
				gridComisiones.getView().refresh();

			}

		} else if(  estado == "BUSCA_COMISIONES"           ){

			// Agregar mascara de busqueda
			var panelFormaConsulta  = Ext.getCmp("panelFormaConsulta");
			panelFormaConsulta.getEl().mask('Buscando...','x-mask-loading');

			// Agregar mascara que diga.
			var comisionesData  = Ext.StoreMgr.key('comisionesDataStore');
			comisionesData.load({
				params: {
					claveMes: respuesta.claveMes
				}
			});

			// Se deja a la funcion: procesarConsultaComisiones del store: comisionesData
			// el paso al siguiente estado: "ESPERA_DECISION".

		} else if( estado  == "EXTRACCION_DETALLADA_TXT"        ){

			// Indicar que hay una operaci�n en proceso
      	var gridComisiones 					= Ext.getCmp("gridComisiones");
      	gridComisiones.generandoArchivo	= true;
      	gridComisiones.recordId				= respuesta.recordId;

      	// Obtener registro
      	var record								= 	Ext.StoreMgr.key('comisionesDataStore').getById(respuesta.recordId);

      	// Mostrar animacion
      	record.data['EXTRACCION_DETALLADA_TXT_LOADING'] = true;
      	gridComisiones.getView().refresh();

			// Finalizar extracci�n
			Ext.Ajax.request({
				url: 						'29ObtenerComisiones01ext.data.jsp',
				params: 	{
					informacion:		'ObtencionComisiones.extraccionDetalladaTXT',
					recordId: 			respuesta.recordId,
					claveMes:			record.data['CLAVE_MES'],
					fiso: 				record.data['FISO'],
					fechaCorte: 		record.data['FECHA_CORTE'],
					isEmptyPkcs7: 		Ext.isEmpty(respuesta.pkcs7),
					pkcs7:				respuesta.pkcs7,
					textoFirmado:		respuesta.textoFirmado
				},
				callback: 				procesaObtencionComisiones
			});

		} else if(  estado == "MOSTRAR_ACUSE"            	 ){

      	// Guardar memoria de la operaci�n realizada.
      	Ext.getCmp("panelFormaAcuseExtraccionArchivoTXT.recordId").setValue(respuesta.recordId);
      	Ext.getCmp("panelFormaAcuseExtraccionArchivoTXT.extraccionFirmada").setValue(respuesta.extraccionFirmada);
      	Ext.getCmp("panelFormaAcuseExtraccionArchivoTXT.urlNombreArchivo").setValue(respuesta.urlNombreArchivo);

			// Mostrar ventana de acuse
			var ventana = Ext.getCmp('windowAcuseExtraccionArchivoTXT');
			if (ventana) {
				ventana.show();
			} else {
				new Ext.Window({
					layout: 			'fit',
					width: 			500,
					height: 			285+15,
					minWidth: 		500,
					minHeight: 		285+15,
					id: 				'windowAcuseExtraccionArchivoTXT',
					modal:			true,
					closeAction: 	'hide',
					closable:		false,
					items: [
						Ext.getCmp("panelFormaAcuseExtraccionArchivoTXT")
					],
					listeners: 		{
						hide: resetWindowAcuseExtraccionArchivoTXT
					}
				}).show();
			}

			// Mostrar el folio de la validacion
			Ext.getCmp("labelAcuseAutentificacion").setText(respuesta.acuseAutentificacion,false);

			// Cargar resultados
			Ext.StoreMgr.key('registrosDeControlDataStore').loadData(respuesta.registrosDeControlDataArray);

		} else if(   estado == "EXTRACCION_DETALLADA_PDF"  ){

			// Obtener registro
      	var record								= 	Ext.StoreMgr.key('comisionesDataStore').getById(respuesta.recordId);

      	// Indicar que hay una consulta en proceso
      	var gridComisiones 					= Ext.getCmp("gridComisiones");
      	gridComisiones.generandoArchivo 	= true;
      	gridComisiones.recordId				= respuesta.recordId;

      	// Mostrar animacion
      	record.data['EXTRACCION_DETALLADA_PDF_LOADING'] = true;
      	gridComisiones.getView().refresh();

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ObtenerComisiones01ext.data.jsp',
				params: {
					informacion:		'ObtencionComisiones.extraccionDetalladaPDF',
					recordId:			respuesta.recordId,
					numeroComisiones:	record.data['NUMERO_COMISIONES_EXTRAER'],
					claveMes: 			record.data['CLAVE_MES'],
					fiso: 				record.data['FISO'],
					fechaCorte: 		record.data['FECHA_CORTE']
				},
				callback: 				procesaObtencionComisiones
			});

		} else if(   estado == "EXTRACCION_DETALLADA_XLSX"  ){

			// Obtener registro
      	var record								= 	Ext.StoreMgr.key('comisionesDataStore').getById(respuesta.recordId);

      	// Indicar que hay una consulta en proceso
      	var gridComisiones 					= Ext.getCmp("gridComisiones");
      	gridComisiones.generandoArchivo 	= true;
      	gridComisiones.recordId				= respuesta.recordId;

      	// Mostrar animacion
      	record.data['EXTRACCION_DETALLADA_XLSX_LOADING'] = true;
      	gridComisiones.getView().refresh();

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ObtenerComisiones01ext.data.jsp',
				params: {
					informacion:		'ObtencionComisiones.extraccionDetalladaXLSX',
					recordId:			respuesta.recordId,
					numeroComisiones:	record.data['NUMERO_COMISIONES_EXTRAER'],
					claveMes: 			record.data['CLAVE_MES'],
					fiso: 				record.data['FISO'],
					fechaCorte: 		record.data['FECHA_CORTE']
				},
				callback: 				procesaObtencionComisiones
			});

		} else if(	 estado == "MOSTRAR_LINK_TXT_DETALLE" 	){

			// Obtener registro del que se realiz� la extraccion detallada.
      	var record								= Ext.StoreMgr.key('comisionesDataStore').getById(respuesta.recordId);

			// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridComisiones 					= Ext.getCmp("gridComisiones");
      	gridComisiones.generandoArchivo 	= false;
      	gridComisiones.recordId				= null;

      	// Suprimir animacion
      	record.data['EXTRACCION_DETALLADA_TXT_LOADING'] = false;
      	record.data['EXTRACCION_FIRMADA'] 					= respuesta.extraccionFirmada;
      	record.data['EXTRACCION_DETALLADA_TXT_URL'] 		= respuesta.urlNombreArchivo;

      	// Mostrar TXT
      	gridComisiones.getView().refresh();

			if(respuesta['highlightRecord']){

				var rowIndex 		= Ext.StoreMgr.key('comisionesDataStore').indexOfId(respuesta.recordId);
				var columnIndex	= gridComisiones.getColumnModel().getIndexById('extraccionDetallada');

				Ext.fly(gridComisiones.getView().getCell( rowIndex, columnIndex )).highlight('FFF700', {duration: 5, easing:'bounceOut'});
				/*
				Ext.fly(gridComisiones.getView().getRow(rowIndex)).animate(
					 // animation control object
					 {
					 	 backgroundColor: { from: '#FFF700', to: 'ffffff' }
					 },
					 5,      		// animation duration
					 null,      	// callback
					 'bounceOut', 	// easing method
					 'color'      	// animation type ('run','color','motion','scroll')
				);
				*/
				// Nota: tambien se podr�a utilizar un baloon help para indicar que ya se puede
				// descargar el archivo y as� quitar el alert del handler del boton aceptar.

			}

		} else if(	 estado == "MOSTRAR_LINK_PDF_DETALLE" 	){

			// Obtener registro
      	var record			= 	Ext.StoreMgr.key('comisionesDataStore').getById(respuesta.recordId);

      	// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridComisiones 					= Ext.getCmp("gridComisiones");
      	gridComisiones.generandoArchivo 	= false;
      	gridComisiones.recordId				= null;

      	// Suprimir animacion
      	record.data['EXTRACCION_DETALLADA_PDF_LOADING'] = false;
      	record.data['EXTRACCION_DETALLADA_PDF_URL'] 		= respuesta.urlNombreArchivo;

      	// Mostrar  PDF
      	gridComisiones.getView().refresh();

		} else if(	 estado == "MOSTRAR_LINK_XLSX_DETALLE" 	){

			// Obtener registro
      	var record			= 	Ext.StoreMgr.key('comisionesDataStore').getById(respuesta.recordId);

      	// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridComisiones 					= Ext.getCmp("gridComisiones");
      	gridComisiones.generandoArchivo 	= false;
      	gridComisiones.recordId				= null;

      	// Suprimir animacion
      	record.data['EXTRACCION_DETALLADA_XLSX_LOADING'] = false;
      	record.data['EXTRACCION_DETALLADA_XLSX_URL'] 		= respuesta.urlNombreArchivo;

      	// Mostrar  XLSX
      	gridComisiones.getView().refresh();

		} else if(	estado == "FIN"								){

			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "29ObtenerComisiones01ext.jsp";
			forma.target	= "_self";
			forma.submit();

		}

		return;

	}

	//---------------------------------- PANEL DE ACUSE ------------------------------------

	var procesarConsultaRegistrosDeControl = function(store, registros, opts){

		var gridRegistrosDeControl 			= Ext.getCmp('gridRegistrosDeControl');

		if (registros != null) {

			var el 							= gridRegistrosDeControl.getGridEl();

			if(store.getTotalCount() > 0) {

				el.unmask();

			} else {

				el.mask('Se present� un error al leer los par�metros', 'x-mask');

			}

		}

	}

	var registrosDeControlData = new Ext.data.ArrayStore({

		storeId: 'registrosDeControlDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosDeControl,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosDeControl(null, null, null);
				}
			}
		}

	});

	var renderContenidoGridAcuse = function(value, metaData, record, rowIndex, colIndex, store) {

		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: left !important';
		} else {
			metaData.style += 'text-align: left !important';
		}
		return value;

	}

	var elementosFormaAcuseExtraccionArchivoTXT = [
		{
			xtype: 	'label',
			id:	 	'labelAcuseAutentificacion',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:15px;',
			html:  	'Cargando...'
		},
		{
			xtype: 	'label',
			id:	 	'labelAcuseCifrasControl',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		100,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '5%'
						},
						region:	'west'
					},
					{
						store: 			registrosDeControlData,
						xtype: 			'grid',
						id:				'gridRegistrosDeControl',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '90%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		125,
								hidden: 		false,
								hideable:	false,
								fixed:		true
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								align:		'right',
								renderer: 	renderContenidoGridAcuse
							}
						]
					},
					{
						xtype: 	'box',
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '5%'
						},
						region:	'east'
					}
			]
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			height:		75,
			items: [
				{
					xtype: 		'button',
					text: 		'Aceptar',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoAceptar',
					id: 			'botonAceptar',
					handler:    function(boton, evento) {

						// Construir objeto respuesta
						var respuesta = new Object();
						respuesta['recordId'] 				= Ext.getCmp("panelFormaAcuseExtraccionArchivoTXT.recordId").getValue();
						respuesta['extraccionFirmada']	= Ext.getCmp("panelFormaAcuseExtraccionArchivoTXT.extraccionFirmada").getValue();
						respuesta['urlNombreArchivo']		= Ext.getCmp("panelFormaAcuseExtraccionArchivoTXT.urlNombreArchivo").getValue();
						respuesta['highlightRecord']		= true;

						// Ocultar ventana de acuse
						var ventana = Ext.getCmp('windowAcuseExtraccionArchivoTXT');
						ventana.hide();

						// Construir objeto con la respuesta
						obtencionComisiones("MOSTRAR_LINK_TXT_DETALLE",respuesta);

					},
					style: {
						width: 137
					}
				}
			]
		},
		// INPUT HIDDEN: ID DEL REGISTRO DE LA EXTRACCION DETALLADA
		{
        xtype:	'hidden',
        name:	'panelFormaAcuseExtraccionArchivoTXT.recordId',
        id: 	'panelFormaAcuseExtraccionArchivoTXT.recordId'
      },
		// INPUT HIDDEN: FLAG EXTRACCION FIRMADA
		{
        xtype:	'hidden',
        name:	'panelFormaAcuseExtraccionArchivoTXT.extraccionFirmada',
        id: 	'panelFormaAcuseExtraccionArchivoTXT.extraccionFirmada'
      },
		// INPUT HIDDEN: URL DEL ARCHIVO GENERADO
		{
        xtype:	'hidden',
        name:	'panelFormaAcuseExtraccionArchivoTXT.urlNombreArchivo',
        id: 	'panelFormaAcuseExtraccionArchivoTXT.urlNombreArchivo'
      }
	];

	var panelFormaAcuseExtraccionArchivoTXT = new Ext.Panel({
		id: 					'panelFormaAcuseExtraccionArchivoTXT',
		//width: 				500,
		height:				285,
		title: 				'Acuse',
		frame: 				true,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 6px',
		labelWidth: 		100,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side'
		},
		items: 				elementosFormaAcuseExtraccionArchivoTXT,
		monitorValid: 		false
	});

	//-------------------------------- PANEL DE RESULTADOS ------------------------------------

	var procesarConsultaComisiones = function(store, registros, opts){

		var gridComisiones 				= Ext.getCmp('gridComisiones');
		var el 								= gridComisiones.getGridEl();
		el.unmask();

		if (registros != null) {

			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

		}

		// Limpiar M�scara en la Forma de B�squeda
		obtencionComisiones("ESPERAR_DECISION",null);

	}

	var comisionesData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'comisionesDataStore',
		url: 		'29ObtenerComisiones01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaComisiones'
		},
		idIndex: 		0,
		fields: [
			{ name: 'CLAVE_MES'  														},
			{ name: 'FISO'  																},
			{ name: 'FECHA_CORTE'  														},
			{ name: 'FECHA_COMISION'  													},
			{ name: 'NUMERO_COMISIONES_EXTRAER', 			type: 'int',	convert: function(value,record){ return (Ext.isEmpty(value)?0:Ext.util.Format.number(value,'0,000')); } },
			{ name: 'EXTRACCION_DETALLADA_TXT', 			type: 'boolean'	},
			{ name: 'EXTRACCION_DETALLADA_PDF', 			type: 'boolean' 	},
			{ name: 'EXTRACCION_DETALLADA_XLSX',			type: 'boolean'   },
			{ name: 'EXTRACCION_FIRMADA',						type: 'boolean'	},
			{ name: 'EXTRACCION_DETALLADA_TXT_LOADING', 	type: 'boolean'	},
			{ name: 'EXTRACCION_DETALLADA_PDF_LOADING', 	type: 'boolean'	},
			{ name: 'EXTRACCION_DETALLADA_XLSX_LOADING', type: 'boolean'	},
			{ name: 'EXTRACCION_DETALLADA_TXT_URL' 								},
			{ name: 'EXTRACCION_DETALLADA_PDF_URL' 								},
			{ name: 'EXTRACCION_DETALLADA_XLSX_URL' 								}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
					var panelComisiones = Ext.getCmp("panelComisiones");
					if( !panelComisiones.isVisible() ){
						panelComisiones.show();
					}
				}
			},
			load: 	procesarConsultaComisiones,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaComisiones(null, null, null);
				}
			}
		}

	});

	var fnExtracFirmadaCallback = function(vpkcs7, vtextoFirmar, vid){

		// Si no hay firma, cancelar la operacion
		if( Ext.isEmpty(vpkcs7) ){
			return;
		// Firmar extraccion
		} else {
			// Obtener ID del Registro en cuestion
			var respuesta 					= new Object();
			respuesta['recordId'] 		= vid;
			respuesta['pkcs7'] 			= vpkcs7;
			respuesta['textoFirmado'] 	= vtextoFirmar;
			obtencionComisiones("EXTRACCION_DETALLADA_TXT",respuesta);
		}
	}

	var elementosComisiones = [
		{
			store: 		comisionesData,
			xtype: 		'grid',
			id:			'gridComisiones',
			stripeRows: true,
			loadMask: 	true,
			height: 		75,
			style: {
				borderWidth: 1
			},
			generandoArchivo: false,
			recordId: 			null,
			columns: [
				{
					header: 		'Clave del Mes',
					tooltip: 	'Clave del Mes',
					dataIndex: 	'CLAVE_MES',
					sortable: 	true,
					resizable: 	true,
					width: 		80,
					hidden: 		true,
					hideable:	false,
					align:		'center'
				},
				{
					header: 		'Fecha de<br>Comisi&oacute;n',
					tooltip: 	'Fecha de Comisi�n',
					dataIndex: 	'FECHA_COMISION',
					sortable: 	true,
					resizable: 	true,
					width: 		100,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'N&uacute;mero de Comisiones<br>a Extraer',
					tooltip: 	'N�mero de Comisiones a Extraer',
					dataIndex: 	'NUMERO_COMISIONES_EXTRAER',
					sortable: 	true,
					resizable: 	true,
					width: 		150,
					hidden: 		false,
					align:		'center'
				},
				{
					xtype: 		'actiontextcolumn',
					header: 		'Extracci&oacute;n<br>detallada (TXT)',
					tooltip: 	'Extracci�n detallada (TXT)',
					width: 		110,
					hidden: 		false,
					align:		'center',
					id: 			'extraccionDetallada',
					items: 		[
							{
								getClass: function(value, metadata, record) {

									if (record.data['EXTRACCION_DETALLADA_TXT_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 this.items[0].text    = 'Generando...';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_DETALLADA_TXT_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo TXT';
										 this.items[0].text    = null;
										 return 'icoTxt16';
									} else if (record.data['EXTRACCION_DETALLADA_TXT'] ) {
										 this.items[0].tooltip = 'Realizar extracci�n';
										 this.items[0].text    = null;
										 return 'extraerDetalle';
									} else {
										 this.items[0].tooltip = null;
										 this.items[0].text    = null;
										 return null;
									}

								},
								handler: function(grid, rowIndex, colIndex) {

									// Validar que no haya otra consulta en proceso
									if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}

									// Obtener registro
									var record 			= Ext.StoreMgr.key('comisionesDataStore').getAt(rowIndex);

									// Si el archivo ya fue generado , descargarlo
									if ( !Ext.isEmpty(record.data['EXTRACCION_DETALLADA_TXT_URL']) ){

										var forma 		= Ext.getDom('formAux');
										forma.action 	= record.data['EXTRACCION_DETALLADA_TXT_URL'];
										forma.method	= "post";
										forma.target	= "_blank";
										forma.submit();

									// Si no se ha firmado la extraccion, firmar la extraccion
									} else if(!record.data['EXTRACCION_FIRMADA']){

										// Revisar que los campos a firmar no vengan vacios
										var faltanDatos 	= false;
										if(        Ext.isEmpty(record.data["FECHA_COMISION"])                ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["NUMERO_COMISIONES_EXTRAER"])     ){
											faltanDatos 	= true;
										}

										if( faltanDatos ){
											Ext.Msg.alert("Aviso","No pueden haber campos vac�os en los datos a firmar");
											return;
										}

										// Obtener texto a firmar
										var comisionesData = Ext.StoreMgr.key('comisionesDataStore');
										var textoFirmar = "Extracci�n de Archivo de Comisiones\n";
										comisionesData.each(
											function(record){
												textoFirmar += "Fecha de Comisi�n: " + record.get("FECHA_COMISION") + "\n";
												textoFirmar += "N�mero de Comisiones a Extraer:" + record.get("NUMERO_COMISIONES_EXTRAER")+"\n";
											}
										);

										// Suprimir acentos
										textoFirmar = textoFirmar.replace(/�/g, "a").replace(/�/g, "e").replace(/�/g, "i").replace(/�/g, "o").replace(/�/g, "u");
										textoFirmar = textoFirmar.replace(/�/g, "A").replace(/�/g, "E").replace(/�/g, "I").replace(/�/g, "O").replace(/�/g, "U");

										// Firmar texto

										NE.util.obtenerPKCS7(fnExtracFirmadaCallback, textoFirmar, record.id);


								// Realizar la extraccion del archivo TXT
								} else {

									// Obtener ID del Registro en cuestion
									var respuesta 					= new Object();
									respuesta['recordId'] 		= record.id;
									obtencionComisiones("EXTRACCION_DETALLADA_TXT", respuesta );

								}

							}
						}
					]
            },
				{
					xtype: 		'actiontextcolumn',
					header: 		'Extracci�n<br>detallada (PDF)',
					tooltip: 	'Extracci�n detallada (PDF)',
					width: 		110,
					hidden: 		false,
					align:		'center',
					items: 		[
							{
								getClass: function(value, metadata, record) {

									if( !record.data['EXTRACCION_FIRMADA'] ){
										 this.items[0].tooltip = 'Se requiere que primero realice la extracci�n detallada en formato TXT';
										 this.items[0].text    = null;
										 return 'icoBloqueado';
									} else if (record.data['EXTRACCION_DETALLADA_PDF_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 this.items[0].text    = 'Generando...';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_DETALLADA_PDF_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo PDF';
										 this.items[0].text    = null;
										 return 'icoPdf';
									} else if (record.data['EXTRACCION_DETALLADA_PDF'] ) {
										 this.items[0].tooltip = 'Extraer detalle en formato PDF';
										 this.items[0].text    = null;
										 return 'extraerDetalle';
									} else {
										 this.items[0].tooltip = null;
										 this.items[0].text    = null;
										 return null;
									}

								},
								handler: function(grid, rowIndex, colIndex) {

									// Obtener registro
									var record 			= Ext.StoreMgr.key('comisionesDataStore').getAt(rowIndex);

									// Validar que se tenga permiso para extraer el archivo
									if( !record.data['EXTRACCION_FIRMADA'] ){
										return;
									// Validar que no haya otra consulta en proceso
									} else if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}

									// Obtener ID del Registro en cuestion
									var respuesta 				= new Object();
									var record					= Ext.StoreMgr.key('comisionesDataStore').getAt(rowIndex);
									respuesta['recordId'] 	= record.id;

									if ( !Ext.isEmpty(record.data['EXTRACCION_DETALLADA_PDF_URL']) ){
										var forma 		= Ext.getDom('formAux');
										forma.action 	= record.data['EXTRACCION_DETALLADA_PDF_URL'];
										forma.method	= "post";
										forma.target	= "_blank";
										forma.submit();
									} else {
										// Realizar la extraccion consolidada
										obtencionComisiones("EXTRACCION_DETALLADA_PDF", respuesta );
									}

								}
							}
						]
            },
            {
					xtype: 		'actiontextcolumn',
					header: 		'Extracci�n<br>detallada (XLSX)',
					tooltip: 	'Extracci�n detallada (XLSX)',
					width: 		110,
					hidden: 		false,
					align:		'center',
					items: 		[
							{
								getClass: function(value, metadata, record) {

									if( !record.data['EXTRACCION_FIRMADA'] ){
										 this.items[0].tooltip = 'Se requiere que primero realice la extracci�n detallada en formato TXT';
										 this.items[0].text    = null;
										 return 'icoBloqueado';
									} else if (record.data['EXTRACCION_DETALLADA_XLSX_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 this.items[0].text    = 'Generando...';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_DETALLADA_XLSX_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo XLSX';
										 this.items[0].text    = null;
										 return 'icoXls';
									} else if (record.data['EXTRACCION_DETALLADA_XLSX'] ) {
										 this.items[0].tooltip = 'Extraer detalle en formato XLSX';
										 this.items[0].text    = null;
										 return 'extraerDetalle';
									} else {
										 this.items[0].tooltip = null;
										 this.items[0].text    = null;
										 return null;
									}

								},
								handler: function(grid, rowIndex, colIndex) {

									// Obtener registro
									var record 			= Ext.StoreMgr.key('comisionesDataStore').getAt(rowIndex);

									// Validar que se tenga permiso para extraer el archivo
									if( !record.data['EXTRACCION_FIRMADA'] ){
										return;
									// Validar que no haya otra consulta en proceso
									} else if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}

									// Obtener ID del Registro en cuestion
									var respuesta 				= new Object();
									var record					= Ext.StoreMgr.key('comisionesDataStore').getAt(rowIndex);
									respuesta['recordId'] 	= record.id;

									if ( !Ext.isEmpty(record.data['EXTRACCION_DETALLADA_XLSX_URL']) ){
										var forma 		= Ext.getDom('formAux');
										forma.action 	= record.data['EXTRACCION_DETALLADA_XLSX_URL'];
										forma.method	= "post";
										forma.target	= "_blank";
										forma.submit();
									} else {
										// Realizar la extraccion consolidada
										obtencionComisiones("EXTRACCION_DETALLADA_XLSX", respuesta );
									}

								}
							}
						]
            }
			]
		}
	];

	var panelComisiones = {
		title:			'Comisiones',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelComisiones',
		width: 			600,
		frame: 			true,
		style: 			'margin: 0 auto',
		items: 			elementosComisiones
	}

	//-------------------------- PANEL DE CONSULTA EXTRACCIONES -------------------------------

	var catalogoMesData = new Ext.data.JsonStore({
		id: 			'catalogoMesDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'29ObtenerComisiones01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMes'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load: function(store,records,options){

				var index        =  0;
				var defaultValue = store.getAt(index).get('clave');

				// El registro fue encontrado por lo que hay que seleccionarlo
				if ( !Ext.isEmpty(defaultValue) ){

					var comboMes = Ext.getCmp("comboMes");
					comboMes.setValue(defaultValue);
					comboMes.originalValue = defaultValue;

					// Lanzar evento de seleccion
					var record = store.getAt(index);
					comboMes.fireEvent('select', comboMes, record, index );

				}

			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var elementosFormaConsulta = [
		// COMBO MES
		{
			xtype: 				'combo',
			name: 				'mes',
			id: 					'comboMes',
			fieldLabel: 		'Mes',
			allowBlank: 		true,
			mode: 				'local',
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveMes',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMesData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			msgTarget: 			'side',
			anchor:				'-20',
			listeners: {
				select: function(combo, record, index ) {

					var selectedValue  = combo.getValue();

					var respuesta		 		= new Object();
					respuesta['claveMes']	= selectedValue;

					obtencionComisiones("BUSCA_COMISIONES", respuesta );

				}

			}
		}
	];

	var panelFormaConsulta = new Ext.form.FormPanel({
		id: 					'panelFormaConsulta',
		width: 				300,
		title: 				'Consulta',
		frame: 				true,
		hidden:				true,
		collapsible: 		false,
		titleCollapse: 	false,
		trackResetOnLoad: true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		labelWidth: 		75,
		defaultType: 		'textfield',
		items: 				elementosFormaConsulta,
		monitorValid: 		false
	});

	//------------------------------------ 1. PANEL AVISOS ---------------------------------------

	var elementosPanelAvisos = [
		{
			xtype: 	'label',
			id:	 	'labelAviso',
			cls:		'x-form-item',
			style: 	'font-weight:normal;text-align:center;margin:15px;color:black;',
			html:  	''
		}
	];

	var panelAvisos = {
		xtype:			'panel',
		id: 				'panelAvisos',
		hidden:			true,
		width: 			700,
		title: 			'Avisos',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosPanelAvisos
	}

	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelAvisos,
			panelFormaConsulta,
			NE.util.getEspaciador(10),
			panelComisiones
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------
	obtencionComisiones("INICIALIZACION",null);

});