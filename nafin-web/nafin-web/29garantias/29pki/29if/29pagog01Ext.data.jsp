<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,  
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,
	java.io.*,
	netropology.utilerias.*,
	com.netro.pdf.*,
	netropology.utilerias.usuarios.*,
	com.netro.seguridadbean.*,
	com.netro.garantias.*,
	net.sf.json.JSONObject,
	java.text.*,
	net.sf.json.JSONArray"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>


<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");
String infoRegresar	= "";
	if(informacion.equals("ValoresIniciales")){
			JSONObject	resultado	= new JSONObject();
			com.netro.seguridadbean.Seguridad BeanSegFacultad = null;
			try {
				BeanSegFacultad =  ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			} catch (Exception e) {
				System.out.println(e);
			}
			resultado.put("mensaje",new Boolean(!BeanSegFacultad.validaNumeroFISO(iNoCliente)));
			resultado.put("success",new Boolean(true));
			infoRegresar = resultado.toString();
	}	else if (informacion.equals("CatalogoPeriodicidad")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_periodicidad_pago ||'-'|| cs_aplica_segundo_incum");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("gticat_periodicidad_pago");
		catalogo.setOrden("ic_periodicidad_pago");
		infoRegresar = catalogo.getJSONElementos();
	}else if(informacion.equals("CatalogoImagen")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_tipo_imagen");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setTabla("gticat_tipo_imagen");
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();
	
	}else if (informacion.equals("SubirArchivo"))	{
	String proceso = (request.getParameter("proceso")	== null)?"":request.getParameter("proceso");
	String tipoArchivo  = (request.getParameter("Himg")	== null)?"":request.getParameter("Himg");
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(12097152);
		myUpload.upload();
		
	} catch(Exception e) {
			
		success		= false;
		e.printStackTrace();
		resultado.put("mensaje", "El archivo supera el tamaño permitido de 12 MB");
		
	}
 
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	// Guardar Archivo en Disco
	int numFiles = 0;
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp +myFile.getFileName()	 );
		//resultado.put("fileName", 	myFile.getFileName()			);
		
		File archivo= new File(strDirectorioTemp +myFile.getFileName());
		InputStream is = new FileInputStream (archivo);

		try {

			ImagenPagoGarantia imagen = new ImagenPagoGarantia();
			imagen.setProceso(proceso);
				String extension = "." + Comunes.getExtensionNombreArchivo(myFile.getFileName());
				imagen.setClaveTipoImagen(tipoArchivo);
				System.out.println(myFile.getContentType());
				imagen.setContentType(myFile.getContentType());
				imagen.setExtension(extension);
				imagen.setSize(myFile.getSize());
				
				if (!imagen.existeTipoImagenPagoGarantiaTmp()) {
					//Solo se inserta en BD si no existe una imagen asociada a ese tipo de imagen
					imagen.guardarImagenPagoGarantiaTmp(is);
				}else{
					success=false;
					resultado.put("mensaje", "Ya existe una imagen asociada al tipo de imagen seleccionada");
				}
			
			
			Registros registrosImagenPagoGarantiasTmp = imagen.consultarImagenesPagoGarantiaTmp();
			
			HashMap	datos;
			List reg=new ArrayList();
	
			while(registrosImagenPagoGarantiasTmp.next()){
						datos=new HashMap();
						for(int i=0;i<registrosImagenPagoGarantiasTmp.getNombreColumnas().size();i++){
										datos.put(registrosImagenPagoGarantiasTmp.getNombreColumnas().get(i).toString().toUpperCase(),registrosImagenPagoGarantiasTmp.getString(registrosImagenPagoGarantiasTmp.getNombreColumnas().get(i).toString()));	
							}
							
							reg.add(datos);
			}
			resultado.put("registros", JSONArray.fromObject(reg));
			resultado.put("total",reg.size()+"");
			} catch(NafinException ne) {
			System.out.println("PagoGarantiasIFAction::cargarArchivos(). NafinException: " + ne.getMessage());
			throw ne;
		}
		
	}catch(Exception e){
		
		success		= false;
		resultado.put("mensaje", "Ocurrió un error al guardar el archivo");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo "+e);
	}
	
	
	// Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success)		);
 
	infoRegresar = resultado.toString();

}else if(informacion.equals("ConsultarImagen")){
String proceso = (request.getParameter("proceso")	== null)?"":request.getParameter("proceso");
	String claveImagen  = (request.getParameter("claveImagen")	== null)?"":request.getParameter("claveImagen");
	boolean		success		= false;
try {
	ImagenPagoGarantia imagen = new ImagenPagoGarantia();
			imagen.setProceso(proceso);
			imagen.setClaveImagen(claveImagen);
			String nombreArchivoTmp = 
					imagen.consultarImagenPagoGarantiaTmp(strDirectorioTemp);
			JSONObject	resultado	= new JSONObject();
			success		= true;
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivoTmp);
			resultado.put("success", 					new Boolean(success)		);
			infoRegresar = resultado.toString();
			
		} catch(NafinException ne) {
			System.out.println("PagoGarantiasIFAction::consultarArchivo(). NafinException: " + ne.getMessage());
			throw ne;
		} catch(Exception e) {
			System.out.println("PagoGarantiasIFAction::consultarArchivo(). Exception: " + e.getMessage());
			throw new NafinException("SIST0001");
		}
}else if (informacion.equals("EliminarImagen"))	{
	String proceso = (request.getParameter("proceso")	== null)?"":request.getParameter("proceso");
	JSONObject	resultado	= new JSONObject();
	String claveImagen  = (request.getParameter("claveImagen")	== null)?"":request.getParameter("claveImagen");

	ImagenPagoGarantia imagen = new ImagenPagoGarantia();
	imagen.setProceso(proceso);
	imagen.setClaveImagen(claveImagen);
	imagen.eliminarImagenPagoGarantiaTmp();
	Registros registrosImagenPagoGarantiasTmp = imagen.consultarImagenesPagoGarantiaTmp();
			
	HashMap	datos;
	List reg=new ArrayList();
	
	while(registrosImagenPagoGarantiasTmp.next()){
				datos=new HashMap();
				for(int i=0;i<registrosImagenPagoGarantiasTmp.getNombreColumnas().size();i++){
								datos.put(registrosImagenPagoGarantiasTmp.getNombreColumnas().get(i).toString().toUpperCase(),registrosImagenPagoGarantiasTmp.getString(registrosImagenPagoGarantiasTmp.getNombreColumnas().get(i).toString()));	
					}
					
					reg.add(datos);
	}
	resultado.put("registros", JSONArray.fromObject(reg));
	resultado.put("success", 					new Boolean(true)	);
	resultado.put("total",reg.size()+"");
	infoRegresar = resultado.toString();
	}else if(informacion.equals("ValidaFechas")){
		JSONObject	resultado	= new JSONObject();	
	
		try {
			Garantias bean = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
			
			
			String claveFin = (request.getParameter("claveFin")	== null)?"":request.getParameter("claveFin");
			PagoGarantiasIF pagoGarantia = new PagoGarantiasIF();
			pagoGarantia.setClaveIF(iNoCliente);
			pagoGarantia.setClaveGarantia(claveFin);//claveFin);
			String folioGarantiaExistente = pagoGarantia.existeGarantia();
			
			boolean hayErrores = false;
			
			if (!folioGarantiaExistente.equals("")) { //Existe un folio no procesado para la garantia capturada
				hayErrores = true;
				resultado.put("FOLIO",folioGarantiaExistente);
				
				}
			
      Hashtable htFechas = bean.getFechaConAdecuaciones("3");
      resultado.put("CLAVE",new Boolean(hayErrores));
      if(!htFechas.get("FECHA_CARGA").toString().equals(htFechas.get("FECHA_VALOR").toString())){
  			resultado.put("valorFecha",htFechas.get("FECHA_VALOR").toString());
			resultado.put("mensaje",new Boolean(true));
				  
		  
      }else{
        resultado.put("mensaje",new Boolean(false));
      }
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			throw new AppException("Error inesperado ", e);
		}
		
		resultado.put("success",new Boolean(true));
		infoRegresar = resultado.toString();

	
	
	
	}else	if(informacion.equals("GuardaDatos")){
	JSONObject	resultado	= new JSONObject();
	String proceso1 = (request.getParameter("proceso")	== null)?"":request.getParameter("proceso");
	String claveFin = (request.getParameter("claveFin")	== null)?"":request.getParameter("claveFin");
	String HperPago = (request.getParameter("HperPago")	== null)?"":request.getParameter("HperPago");
	String fechaInc1 = (request.getParameter("fechaInc1")	== null)?"":request.getParameter("fechaInc1");
	String fechaInc2 = (request.getParameter("fechaInc2")	== null)?"":request.getParameter("fechaInc2");
	String causas = (request.getParameter("causas")	== null)?"":request.getParameter("causas");
	String nombre = (request.getParameter("nombre")	== null)?"":request.getParameter("nombre");
	String telefono = (request.getParameter("telefono")	== null)?"":request.getParameter("telefono");
	String extension = (request.getParameter("extension")	== null)?"":request.getParameter("extension");
	String correo = (request.getParameter("correo")	== null)?"":request.getParameter("correo");
	String domicilio = (request.getParameter("domicilio")	== null)?"":request.getParameter("domicilio");
	String ciudad = (request.getParameter("ciudad")	== null)?"":request.getParameter("ciudad");
	String estado = (request.getParameter("estado")	== null)?"":request.getParameter("estado");
	String pkcs7 = (request.getParameter("pkcs7")	== null)?"":request.getParameter("pkcs7");
	String textoFirmado = (request.getParameter("textoFirmado")	== null)?"":request.getParameter("textoFirmado");

	
		
		PagoGarantiasIF pagoGarantia = new PagoGarantiasIF();
		pagoGarantia.setClaveGarantia(claveFin);
		pagoGarantia.setFechaPrimerInclumplimiento(fechaInc1);
		pagoGarantia.setCausasIncumplimiento(causas);
		pagoGarantia.setNombreFuncionarioIF(nombre);
		pagoGarantia.setTelefonoFuncionario(telefono);
		pagoGarantia.setExtensionFuncionario(extension);
		pagoGarantia.setMailFuncionario(correo);
		pagoGarantia.setLocalizacionSupervision(domicilio);
		pagoGarantia.setCiudadSupervision(ciudad);
		pagoGarantia.setEstadoSupervision(estado);
		pagoGarantia.setProceso(proceso1);
		pagoGarantia.setClaveIF(iNoCliente);
		pagoGarantia.setClaveUsuario(iNoUsuario);
		pagoGarantia.setPkcs7(pkcs7);
		pagoGarantia.setTextoFirmado(textoFirmado);
		pagoGarantia.setSerialCertificado(_serial);
		pagoGarantia.setFechaSegundoInclumplimiento(fechaInc2);
		pagoGarantia.setPeriodicidadPago(HperPago);
	
		try {
			
			boolean hayErrores = false;
			
			
			String proceso = pagoGarantia.getProceso();
			ImagenPagoGarantia imagen = new ImagenPagoGarantia();
			imagen.setProceso(pagoGarantia.getProceso());
			imagen.setClaveTipoImagen("1"); //Requerido al menos el "Estado de cuenta certificado"
			boolean imagenRequeridaCargada = imagen.existeTipoImagenPagoGarantiaTmp();
			if (imagenRequeridaCargada == false) {
				hayErrores = true;
		}
			
			
			Garantias bean = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
			AcusePagoGarantia acuse = null;
      
			acuse = bean.guardarPagoGarantiasIF(pagoGarantia);
			resultado.put("success",new Boolean(true));
			resultado.put("folioSolicitud", 	"Su solicitud fue recibida con el número de folio: " + acuse.getFolioSolicitud() );
			// Determinar el mes de carga

			JSONArray 	registrosAgregadosDataArray 	= new JSONArray();
			JSONArray	registro								= null;
			
			registro = new JSONArray();
			registro.add("Tipo de Operación");
			registro.add("03 Solicitud de Pago de Garantías");
			registrosAgregadosDataArray.add(registro);
			
			registro = new JSONArray();
			registro.add("No. de Garantía");
			registro.add(acuse.getClaveGarantia());
			registrosAgregadosDataArray.add(registro);
			
			registro = new JSONArray();
			registro.add("Fecha de carga");
			registro.add(acuse.getFechaCarga());
			registrosAgregadosDataArray.add(registro);
			
			registro = new JSONArray();
			registro.add("Hora de carga");
			registro.add(acuse.getHoraCarga());
			registrosAgregadosDataArray.add(registro);
			
			registro = new JSONArray();
			registro.add("Fecha Valor");
			registro.add(acuse.getFechaValor());
			registrosAgregadosDataArray.add(registro);
			
			registro = new JSONArray();
			registro.add("Usuario");
			registro.add(iNoUsuario+" - "+strNombreUsuario);
			registrosAgregadosDataArray.add(registro);
			
			resultado.put("registrosAgregadosDataArray",	registrosAgregadosDataArray);
			
			
			
			
			
			
			
			
			infoRegresar = resultado.toString();

		} catch(NafinException ne) {
			System.out.println("PagoGarantiasIFAction::guardar(). NafinException: " + ne.getMessage());
      ne.printStackTrace();
			throw ne;
		} catch(Exception e) {
			System.out.println("PagoGarantiasIFAction::guardar(). Exception: " + e.getMessage());
      e.printStackTrace();
			throw new NafinException("SIST0001");
		}
	
		
	}else if (    informacion.equals("GeneraArchivoPDF")	)	{
				
	String folio = (request.getParameter("folio")	== null)?"":request.getParameter("folio");
	String garantia = (request.getParameter("garantia")	== null)?"":request.getParameter("garantia");
	String fecha = (request.getParameter("fecha")	== null)?"":request.getParameter("fecha");
	String hora = (request.getParameter("hora")	== null)?"":request.getParameter("hora");
	String fechaValor = (request.getParameter("fechaValor")	== null)?"":request.getParameter("fechaValor");

	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	
	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = archivo.nombreArchivo()+".pdf";
			
	
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),
			strDirectorioPublicacion);

	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
	pdfDoc.addText("Solicitud de Pago de Garantías","formas",ComunesPDF.CENTER);
	pdfDoc.addText("\n\n\n","formas",ComunesPDF.CENTER);
	pdfDoc.addText(folio,"formas",ComunesPDF.CENTER);
	pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
	
	float proporciones[] = {1,2};
	pdfDoc.setTable(2,50, proporciones);
	
	pdfDoc.setCell("Datos del Acuse","celda03",ComunesPDF.CENTER,2,1,1);
	
	pdfDoc.setCell("Tipo de Operación","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("03 Solicitud de Pago de Garantías","formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("No. Garantía","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell(garantia,"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Fecha de carga","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell(fecha,"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Hora de carga","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell(hora,"formas",ComunesPDF.RIGHT);

	pdfDoc.setCell("Fecha Valor","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell(fechaValor,"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Usuario","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell(iNoUsuario+" - "+strNombreUsuario,"formas",ComunesPDF.RIGHT);
	
	pdfDoc.addTable();
	pdfDoc.endDocument();
	JSONObject jsonObj 	      = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar=jsonObj.toString();
} 
	

%>

<%=infoRegresar%>