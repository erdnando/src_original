	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	java.sql.*,
	com.netro.exception.*,  
	com.netro.model.catalogos.*,
	net.sf.json.JSONObject,
	net.sf.json.JSONArray,
	com.netro.pdf.*,
	java.math.*,
	java.text.*,
	netropology.utilerias.*"
	
	errorPage="/00utils/error_extjs.jsp"%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>

<%	
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String informacion         =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";

	if (informacion.equals("catologoSituacion")) {
	  CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("gticat_situacion");
		cat.setCampoClave("ic_situacion");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setCampoLlave("ic_tipo_operacion = 6 and ic_situacion  ");
		cat.setValoresCondicionIn("1,5",Integer.class);
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("Consulta")){
		AccesoDB con          = new AccesoDB();
		PreparedStatement	ps  = null;
		ResultSet rs          = null;
		String qrySentencia   = "";
		int iIcIfSiag         = 0;
		String df_fecha_oper_de         =(request.getParameter("dFechaOper1")    != null) ?   request.getParameter("dFechaOper1") :"";
		String df_fecha_oper_a         =(request.getParameter("dFechaOper2")    != null) ?   request.getParameter("dFechaOper2") :"";
		String combsituacion         =(request.getParameter("Hsituacion")    != null) ?   request.getParameter("Hsituacion") :"";
		String folioperacion         =(request.getParameter("folio")    != null) ?   request.getParameter("folio") :"";

		try {
			con.conexionDB();
			qrySentencia = "SELECT IC_IF_SIAG FROM comcat_if WHERE ic_if = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(iNoCliente));
			rs = ps.executeQuery();
			if(rs.next())
				iIcIfSiag = rs.getInt(1);
			rs.close();
			ps.close(); 
			
		
		com.netro.garantias.ConsResemMasivo consulta= new com.netro.garantias.ConsResemMasivo();
		consulta.setiIF_SIAG(iIcIfSiag);
		consulta.setdf_fecha_oper_de(df_fecha_oper_de);
		consulta.setdf_fecha_oper_a(df_fecha_oper_a);
		consulta.setfolioperacion(folioperacion);
		consulta.setcombsituacion(combsituacion);
		
		qrySentencia = consulta.getQuerySolicitudes();
		ps = con.queryPrecompilado(qrySentencia);
		if(iIcIfSiag!=0) {
		ps.setInt(1, iIcIfSiag);  
  }
  if(!df_fecha_oper_de.equals("") && !df_fecha_oper_a.equals("")){
    ps.setString(2, df_fecha_oper_de);
    ps.setString(3, df_fecha_oper_a);
  }
   
  if(!folioperacion.equals("")  && !df_fecha_oper_de.equals("") && !df_fecha_oper_a.equals("")){
    ps.setString(4, folioperacion);    
  }   
  
  if(!combsituacion.equals("") && !folioperacion.equals("") &&  !df_fecha_oper_de.equals("") && !df_fecha_oper_a.equals("") ){
    ps.setString(5, combsituacion);    
  }
 
  if(!combsituacion.equals("") && folioperacion.equals("") &&  !df_fecha_oper_de.equals("") && !df_fecha_oper_a.equals("") ){
    ps.setString(4, combsituacion);    
  }
  
  if(!folioperacion.equals("")  && df_fecha_oper_de.equals("") && df_fecha_oper_a.equals("")){
    ps.setString(2, folioperacion);    
  }   
  
  if(!combsituacion.equals("") &&folioperacion.equals("") &&  df_fecha_oper_de.equals("") && df_fecha_oper_a.equals("") ){
    ps.setString(2, combsituacion);    
  }
  
  if(!combsituacion.equals("") && !folioperacion.equals("") &&  df_fecha_oper_de.equals("") && df_fecha_oper_a.equals("") ){
    ps.setString(3, combsituacion);    
  }
  
	rs = ps.executeQuery();
	
	HashMap	datos;
	List reg=new ArrayList();
	
	String fecha		    = null;
	String folio		    = null;
	String situacion	  = null;
	int 	ic_situacion  = 0;
	String montoEnviado     = "", OpAceptadas="", OpRechazadad="", TotalOperaciones= "";
	datos=new HashMap();
	while(rs.next()){
		datos=new HashMap();
		fecha        = (rs.getString("fecha")==null)?"":rs.getString("fecha");
		folio        = (rs.getString("ic_folio")==null)?"":rs.getString("ic_folio");
		situacion    = (rs.getString("situacion")==null)?"":rs.getString("situacion");
		ic_situacion = rs.getInt("ic_situacion");	
		montoEnviado = (rs.getString("fn_impte_total")==null)?"":rs.getString("fn_impte_total");
		OpAceptadas  = (rs.getString("IN_REGISTROS_ACEP")==null)?"":rs.getString("IN_REGISTROS_ACEP");
		OpRechazadad =(rs.getString("IN_REGISTROS_RECH")==null)?"":rs.getString("IN_REGISTROS_RECH");
		TotalOperaciones= (rs.getString("in_registros")==null)?"":rs.getString("in_registros");
		datos.put("FECHA",fecha);									
		datos.put("FOLIO",folio);									
		datos.put("SITUACION",situacion);									
		datos.put("IC_SITUACION",ic_situacion+"");									
		datos.put("MONTO",montoEnviado);
		datos.put("ACEPTADAS",OpAceptadas);									
		datos.put("RECHAZADAS",OpRechazadad);									
		datos.put("TOTAL",TotalOperaciones);	
		reg.add(datos);
	}
	
	jsonObj.put("registros", reg);
	jsonObj.put("total", reg.size()+"");
	jsonObj.put("success", new Boolean(true));
	infoRegresar=jsonObj.toString();
	
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		
		
	
	}else if(informacion.equals("Archivos")){
		String folio = request.getParameter("folio");
		String opcion = request.getParameter("opcion");
		String clave_if = request.getParameter("clave_if")!=null?request.getParameter("clave_if"):"";
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet rs = null;
		String qrySentencia	= "";
		String tabla = "";
		int vigencia = 0;
		int i = 0;
		
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo = null;
		
		try {
			con.conexionDB();
			if(opcion.equals("origen")){
			 tabla = "gti_contenido_arch";
		  }else if (opcion.equals("rechazo")){
			 tabla = "gti_arch_rechazo";
		  }
		  if(!"".equals(clave_if)) {
			iNoCliente = clave_if;
		  }
			qrySentencia =
				" select cg_contenido"+
				" from "+tabla+" A"+
				" ,comcat_if I"+
				" where  "+
			 //A.ic_if_siag = I.ic_if_siag"+
				"  A.ic_folio = ?"+
				" and I.ic_if = ?"+
				" order by A.ic_linea";
		
		System.out.println("qrySentencia "+qrySentencia);
		//System.out.println("folio  "+folio+"  iNoCliente  "+iNoCliente);
		  
			ps = con.queryPrecompilado(qrySentencia);
		  ps.setLong(1, Long.parseLong(folio));
			ps.setLong(2, Long.parseLong(iNoCliente));
			
			rs = ps.executeQuery();
			while(rs.next()){
				contenidoArchivo.append(rs.getString("cg_contenido")+"\n");     
				i++;
			}
			ps.close();
		  if (opcion.equals("rechazo")){
			  contenidoArchivo.append("Cantidad de Registros Rechazados : "+i+"\n");
		  }
		
			if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt"))
			{
			//out.print("<--!Error al generar el archivo-->");
			jsonObj.put("success", new Boolean(false));
			}else{
				nombreArchivo = archivo.nombre;
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				jsonObj.put("success", new Boolean(true));
			}
			infoRegresar=jsonObj.toString();
			
			}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		
	}else if(informacion.equals("ArchivoResultado")){
		int iIcIfSiag         = 0;
		jsonObj.put("success", new Boolean(false));
		String folio          = request.getParameter("folio");
		AccesoDB con          = new AccesoDB();
		PreparedStatement	ps  = null;
		ResultSet rs          = null;
		ResultSet rsArchi     = null;
		ResultSet rsContrInv  = null;
		String qrySentencia   = "";
		StringBuffer sArchi   = new StringBuffer();
		CreaArchivo archivo   = new CreaArchivo();
		String nombreArchivo  = archivo.nombreArchivo()+".pdf";
		ComunesPDF pdfDoc     = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
		String icIfSiag       = "";
		String meses[]        = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual    = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual      = fechaActual.substring(0,2);
		String mesActual      = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual     = fechaActual.substring(6,10);
		String horaActual     = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		ResultSet resultado = null;
		PreparedStatement	psresultado  = null;
		int i = 0;
		PreparedStatement	psf  = null;
		ResultSet rsFechaHonrada          = null;
		String qrySentenciaF = "";
		String fechaHonrada = "";
		String moneda = "";
		String fechap = "";
		String fechad = "";
		try {
			con.conexionDB();
			qrySentencia = "SELECT IC_IF_SIAG FROM comcat_if WHERE ic_if = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(iNoCliente));
			rs = ps.executeQuery();
			if(rs.next())
				iIcIfSiag = rs.getInt(1);
			rs.close();
			ps.close(); 
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		
		try{
	con.conexionDB();

	qrySentencia = 
	" SELECT E.IC_FOLIO, "+
  "     i.CG_RAZON_SOCIAL, "+
  "     E.IN_REGISTROS_ACEP, "+
  "     E.IN_REGISTROS_RECH, "+
  "     E.IN_REGISTROS, "+
  "     s.cg_descripcion  "+
  " FROM GTI_ESTATUS_SOLIC E, "+
  "      gticat_situacion s,  "+
  "      comcat_if i "+
  " WHERE e.ic_if_siag = i.ic_if_siag  "+
  " AND e.ic_situacion = s.ic_situacion "+
  " AND e.ic_tipo_operacion = s.ic_tipo_operacion  "+
  " AND E.IC_TIPO_OPERACION  = ? "+
  " AND e.ic_folio = ?   "+
  " AND i.ic_if = ? ";
  
  psresultado = con.queryPrecompilado(qrySentencia);
	psresultado.setInt(1, 6); 
	psresultado.setBigDecimal(2,new BigDecimal(folio));
  psresultado.setLong(3, Long.parseLong(iNoCliente));
	resultado = psresultado.executeQuery();

// System.out.println("qrySentencia::"+qrySentencia);
System.out.println("folio::"+folio+"  if::  "+iNoCliente);
   
	qrySentencia = 
 " SELECT  i.cg_razon_social, "+ 
 "    SUM(CASE WHEN (D.cg_moneda IN('PESOS'))  THEN  d.IN_NUM_REGISTROS_ACEP END) AS aceptadosP,"+
 "    SUM(CASE WHEN (D.cg_moneda = 'DOLARES') THEN  d.IN_NUM_REGISTROS_ACEP END) AS aceptadosD,"+
 "    SUM(CASE WHEN (D.cg_moneda IN('PESOS'))  THEN  d.IN_NUM_REGISTROS_RECH END) AS rechazadosP,"+
 "    SUM(CASE WHEN (D.cg_moneda = 'DOLARES') THEN  d.IN_NUM_REGISTROS_RECH END) AS rechazadosD, "+
 "    SUM(CASE WHEN (D.cg_moneda IN('PESOS'))  THEN  d.IN_NUM_REGISTROS  END) AS totalregistrosP, "+
 "    SUM(CASE WHEN (D.cg_moneda = 'DOLARES') THEN  d.IN_NUM_REGISTROS  END) AS totalregistrosD, "+
 "    SUM(CASE WHEN (D.cg_moneda IN('PESOS'))  THEN  d.fn_capital END) AS capitalP, "+
 "    SUM(CASE WHEN (D.cg_moneda = 'DOLARES') THEN  d.fn_capital END) AS capitalD,"+
 "    SUM(CASE WHEN (D.cg_moneda IN('PESOS'))  THEN  d.FN_INTERESES_ORDINARIOS END) AS intordinarioP, "+
 "    SUM(CASE WHEN (D.cg_moneda = 'DOLARES') THEN  d.FN_INTERESES_ORDINARIOS END) AS intordinarioD,"+
 "    SUM(CASE WHEN (D.cg_moneda IN('PESOS'))  THEN  d.FN_INTERESES_MORATORIOS END) AS intmoratorioP, "+
 "    SUM(CASE WHEN (D.cg_moneda = 'DOLARES') THEN  d.FN_INTERESES_MORATORIOS END) AS intmoratorioD,"+
 "    SUM(CASE WHEN (D.cg_moneda IN('PESOS'))  THEN  d.FN_COMISION_ANIVERSARIO END) AS comianiversarioP, "+
 "    SUM(CASE WHEN (D.cg_moneda = 'DOLARES') THEN  d.FN_COMISION_ANIVERSARIO END) AS comianiversarioD,"+
 "    SUM(CASE WHEN (D.cg_moneda IN('PESOS'))  THEN  d.FN_IVA_COMISION_ANIVERSARIO END) AS ivaniversarioP, "+
 "    SUM(CASE WHEN (D.cg_moneda = 'DOLARES') THEN  d.FN_IVA_COMISION_ANIVERSARIO END) AS ivaniversarioD, "+ 
 "    SUM(CASE WHEN (D.cg_moneda IN('PESOS'))  THEN  nvl(d.fn_capital,0) +nvl(d.FN_INTERESES_ORDINARIOS,0) +nvl(d.FN_INTERESES_MORATORIOS,0)  END) AS totalResembolsoP, "+
 "    SUM(CASE WHEN (D.cg_moneda = 'DOLARES') THEN  nvl(d.fn_capital,0) +nvl(d.FN_INTERESES_ORDINARIOS,0) +nvl(d.FN_INTERESES_MORATORIOS,0)  END) AS totalResembolsoD,  "+
 "    SUM(CASE WHEN (D.cg_moneda IN('PESOS'))  THEN  nvl(d.FN_COMISION_ANIVERSARIO,0) +nvl(d.FN_IVA_COMISION_ANIVERSARIO,0)  END) AS ComisionAniversarioP,   "+
 "    SUM(CASE WHEN (D.cg_moneda = 'DOLARES') THEN  nvl(d.FN_COMISION_ANIVERSARIO,0) +nvl(d.FN_IVA_COMISION_ANIVERSARIO,0)  END) AS ComisionAniversarioD   "+
 " FROM gti_detalle_desmasivos d, "+ 
 "    gti_estatus_solic e, "+
 "    comcat_if i "+
 " WHERE e.ic_folio = d.ic_folio_operacion  "+
 " AND e.ic_if_siag = i.ic_if_siag "+
 " AND e.ic_tipo_operacion = ? "+
 " AND e.ic_folio = ? "+
 " AND i.ic_if = ? "+
 " GROUP BY i.cg_razon_social"+
 " ORDER BY i.cg_razon_social ";
   
System.out.println("qrySentencia::"+qrySentencia);
System.out.println("folio::"+folio+"  if::  "+iNoCliente);
	
	ps = con.queryPrecompilado(qrySentencia);
	ps.setInt(1, 6); 
	ps.setBigDecimal(2,new BigDecimal(folio));
  ps.setLong(3, Long.parseLong(iNoCliente));
	rs = ps.executeQuery();

	
	qrySentencia = 
	" SELECT CG_CONTENIDO "+
  " FROM gti_arch_resultado "+  
	" WHERE IC_IF_SIAG         = ? "+
	" AND   IC_FOLIO           = ? "+
	" ORDER BY IC_LINEA ";

 //System.out.println(qrySentencia);
 //System.out.println("iIcIfSiag  "+iIcIfSiag+" folio "+folio);

	ps = con.queryPrecompilado(qrySentencia);
	ps.setInt(1,Integer.parseInt(iIcIfSiag+""));
	ps.setBigDecimal(2,new BigDecimal(folio));
	rsArchi = ps.executeQuery();

	while(rsArchi.next()){
		sArchi.append( ((rsArchi.getString("CG_CONTENIDO") == null)?"":rsArchi.getString("CG_CONTENIDO")+"\n") );
	}
	sArchi.append("\n ");
	rsArchi.close();
	ps.close();
//**********************************************************

qrySentenciaF = "select s.ic_moneda,TO_CHAR(s.df_fecha_honrada,'dd/mm/yyyy') AS fecha  "+
                " from  gti_solicitud_pago s "+
                " where  s.ic_folio = ?  "+
                " GROUP BY s.ic_moneda,s.df_fecha_honrada ";

System.out.println("qrySentenciaF    " +qrySentenciaF +folio);

psf = con.queryPrecompilado(qrySentenciaF);
psf.setBigDecimal(1,new BigDecimal(folio));

rsFechaHonrada = psf.executeQuery();

	while(rsFechaHonrada.next()){
		fechaHonrada = ((rsFechaHonrada.getString("fecha") == null)?"":rsFechaHonrada.getString("fecha")+"\n");
    moneda = ((rsFechaHonrada.getString("ic_moneda") == null)?"":rsFechaHonrada.getString("ic_moneda")+"\n");
	}
  	
	rsFechaHonrada.close();
	psf.close();
  
 
	String sSituacionDescripcion = "REPORTE DE RESULTADOS";
 
 //begin page 1
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
																 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
																 (String)session.getAttribute("sesExterno"),
																 (String)session.getAttribute("strNombre"),
																 (String)session.getAttribute("strNombreUsuario"),
																 (String)session.getAttribute("strLogo"),
																 strDirectorioPublicacion);

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);	

   if (resultado.next()){
 
      String sIc_folio             = (resultado.getString("IC_FOLIO") == null)?"":resultado.getString("IC_FOLIO") ;
      String sIntermediario       = (resultado.getString("CG_RAZON_SOCIAL") == null)?"":resultado.getString("CG_RAZON_SOCIAL") ;
      String sRegSinError          = (resultado.getString("IN_REGISTROS_ACEP") == null)?"":resultado.getString("IN_REGISTROS_ACEP") ;
      String sRegConError          = (resultado.getString("IN_REGISTROS_RECH") == null)?"":resultado.getString("IN_REGISTROS_RECH") ;
      String sTotalRegistros       = (resultado.getString("IN_REGISTROS") == null)?"":resultado.getString("IN_REGISTROS") ;
      String sDescripSituacion       = (resultado.getString("cg_descripcion") == null)?"":resultado.getString("cg_descripcion") ;
 
		float widthtb1[] = {100f};
		pdfDoc.setTable(1, 100, widthtb1);
		pdfDoc.addTable();
		pdfDoc.addText(sSituacionDescripcion,"formasG",ComunesPDF.CENTER);
	 	pdfDoc.addText("Situacion de la Solicitud: "+sDescripSituacion+"\n","formas",ComunesPDF.LEFT);
	
	  float widthtb2[] = {100f};    
		pdfDoc.setTable(1, 100, widthtb2);
		pdfDoc.setCell("RESULTADO DE SOLICITUD DE DESEMBOLSOS MASIVOS","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.addTable();
  
    float widthtb3[] = {16.6f,16.6f,16.6f};
		pdfDoc.setTable(3, 100, widthtb3);
		//row 1
    pdfDoc.setCell("Folio de Operación"+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell(sIc_folio+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell(""+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("Intermediario Financiero"+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell(sIntermediario+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell(""+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
	  pdfDoc.setCell("Registros Sin Error "+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);
   	pdfDoc.setCell(sRegSinError+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell(""+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
	  pdfDoc.setCell("Registros Con Error"+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);
   	pdfDoc.setCell(sRegConError+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);	
    pdfDoc.setCell(""+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("Total de Registros"+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);    
   	pdfDoc.setCell(sTotalRegistros+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell(""+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
		pdfDoc.addTable();

  }
  resultado.close();
  psresultado.close();
  
   if (rs.next()){
		i++;
    
      String aceptadosP          = (rs.getString("aceptadosP") == null)?"":rs.getString("aceptadosP") ;
      String aceptadosD          = (rs.getString("aceptadosD") == null)?"":rs.getString("aceptadosD") ;
      String rechazadosP         = (rs.getString("rechazadosP") == null)?"":rs.getString("rechazadosP") ;
      String rechazadosD         = (rs.getString("rechazadosD") == null)?"":rs.getString("rechazadosD") ;
      String totalregistrosP     =(rs.getString("totalregistrosP") == null)?"":rs.getString("totalregistrosP") ;
      String totalregistrosD     =(rs.getString("totalregistrosD") == null)?"":rs.getString("totalregistrosD") ;  
     // String fecha               =  (rs.getString("fecha") == null)?"":rs.getString("fecha") ;
      String capitalP            = (rs.getString("capitalP") == null)?"":rs.getString("capitalP") ;  
      String capitalD            = (rs.getString("capitalD") == null)?"":rs.getString("capitalD") ;  
      String intordinarioP       =  (rs.getString("intordinarioP") == null)?"0":rs.getString("intordinarioP") ;
      String intordinarioD       =  (rs.getString("intordinarioD") == null)?"0":rs.getString("intordinarioD") ;
      String intmoratorioP       = (rs.getString("intmoratorioP") == null)?"0":rs.getString("intmoratorioP") ;
      String intmoratorioD       = (rs.getString("intmoratorioD") == null)?"0":rs.getString("intmoratorioD") ;
      String comianiversarioP    =(rs.getString("comianiversarioP") == null)?"0":rs.getString("comianiversarioP") ;
      String comianiversarioD    =(rs.getString("comianiversarioD") == null)?"0":rs.getString("comianiversarioD") ;
      String ivaniversarioP      = (rs.getString("ivaniversarioP") == null)?"0":rs.getString("ivaniversarioP") ;
      String ivaniversarioD      = (rs.getString("ivaniversarioD") == null)?"0":rs.getString("ivaniversarioD") ;
      String totalResembolsoP    = (rs.getString("totalResembolsoP") == null)?"0":rs.getString("totalResembolsoP") ;
      String totalResembolsoD    = (rs.getString("totalResembolsoD") == null)?"0":rs.getString("totalResembolsoD") ;
      String ComisionAniversarioP= (rs.getString("ComisionAniversarioP") == null)?"0":rs.getString("ComisionAniversarioP") ;
      String ComisionAniversarioD= (rs.getString("ComisionAniversarioD") == null)?"0":rs.getString("ComisionAniversarioD") ;
      double ImporteDesembolsoP  = Double.parseDouble((String)totalResembolsoP)-Double.parseDouble((String)ComisionAniversarioP);
      double ImporteDesembolsoD  = Double.parseDouble((String)totalResembolsoD)-Double.parseDouble((String)ComisionAniversarioD);
       
    float widthtb4[] = {100f};    
		pdfDoc.setTable(1, 100, widthtb4);
		pdfDoc.setCell("DETALLE DE DESEMBOLSOS MASIVOS POR MONEDA","celda01",ComunesPDF.CENTER,1,1,1);
		pdfDoc.addTable();   
    
		float widthtb5[] = {16.6f,16.6f,16.6f};
		pdfDoc.setTable(3, 100, widthtb5);
		pdfDoc.setCell("Moneda"+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Pesos "+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("Dolares"+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);   
		
		pdfDoc.setCell("Registros Aceptados ","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell(aceptadosP+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell(aceptadosD+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    
		pdfDoc.setCell("Registros Rechazados","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell(rechazadosP+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell(rechazadosD+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    
    pdfDoc.setCell("Total de Registros","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell(totalregistrosP+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell(totalregistrosD+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    
    pdfDoc.setCell("Fecha honrada","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell(fechaHonrada+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell(fechaHonrada+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    
    pdfDoc.setCell("Capital","formasrep",ComunesPDF.LEFT,1,1,0);   
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(capitalP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(capitalD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
   
    pdfDoc.setCell("Interés ordinario ","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(intordinarioP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(intordinarioD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    
    pdfDoc.setCell("Interés moratorio ","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(intmoratorioP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(intmoratorioD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
     
    pdfDoc.setCell("Total Desembolso ","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalResembolsoP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalResembolsoD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
      
    pdfDoc.setCell("Comision Aniversario ","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(comianiversarioP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(comianiversarioD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
      
    pdfDoc.setCell("Iva de la Comision Aniversario ","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(ivaniversarioP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(ivaniversarioD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
  
    pdfDoc.setCell("Total Comision Aniversario ","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(ComisionAniversarioP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(ComisionAniversarioD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
  
    pdfDoc.setCell("Importe Neto a Desembolsar","formasrep",ComunesPDF.LEFT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(ImporteDesembolsoP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
    pdfDoc.setCell("$ "+Comunes.formatoDecimal(ImporteDesembolsoD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
  	pdfDoc.addTable();
	}
 	rs.close();
	ps.close();
  
  float widthtb9[] = {100f};
	pdfDoc.setTable(1, 100, widthtb9);
 	pdfDoc.setCell("Errores Generados","celda01",ComunesPDF.CENTER,1,1,1);	   
  pdfDoc.addTable();    
  float widthtb6[] = {100f};
	pdfDoc.setTable(1, 100, widthtb6);
  pdfDoc.setCell(sArchi.toString(),"formasrep",ComunesPDF.LEFT,1,1,0);
	pdfDoc.addTable();
	pdfDoc.endDocument();
		
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	
	}catch(Exception e){
	e.printStackTrace();
	throw e;
}finally{
	infoRegresar=jsonObj.toString();
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
	}
}
%>

<%=infoRegresar%>