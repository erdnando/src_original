Ext.onReady(function() {
	var numFolios = [];


		//**************Inicializa Valores **************************************
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var fp = Ext.getCmp('forma');
				fp.el.unmask();

				if(jsonData.cvePerf =='4' )  {
					Ext.getCmp('claveIF1').show();
					catalogoIF.load();
				}else  {
					catalogoFolio.load();
					catalogoFechas.load();
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//generaci�n de Archivo de Acuse
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarPDF');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarPDF');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	// *************************Grid de Consulta Acuse  *******************
	var procesarConsAcuseData = function(store, arrRegistros, opts) 	{
		var gridAcuse = Ext.getCmp('gridAcuse');
		if (arrRegistros != null) {
			if (!gridAcuse.isVisible()) {
				gridAcuse.show();
			}
			//edito el titulo de la columna
			var el = gridAcuse.getGridEl();
			var cm = gridAcuse.getColumnModel();
			var jsonData = store.reader.jsonData;

			Ext.getCmp('btnBajarPDF').hide();

			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarPDF').enable();
				el.unmask();
			} else {
				Ext.getCmp('btnGenerarPDF').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var autentificacion = new Ext.Container({
		layout: 'table',
		id: 'autentificacion',
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		layoutConfig: {
			columns: 1
		},
		items: [
			{
				xtype: 'displayfield',
				id:	'mensaje',
				value: '',
				cls:		'x-form-item',
				style: { 	width: 	'100%', textAlign:'center'	}
			}
		]
	});

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		items: [
			{
				xtype: 'button',
				text: 'Salir',
				id: 'btnSalir',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = "29forma4ext.jsp";
				}
			}
		]
	});

	var consAcuseData   = new Ext.data.JsonStore({
		root : 'registros',
		url : '29forma4.data.jsp',
		baseParams: {
			informacion: 'ConsultaAcuse'
		},
		fields: [
			{name: 'FOLIO_SOLIC'},
			{name: 'TIPO_SOLIC'},
			{name: 'FECHA_HORA'},
			{name: 'NUM_OPERACIONES_ACEP_MN'},
			{name: 'IMPORTE_PAGO_MN'},
			{name: 'NUM_OPERACIONES_ACEP_DL'},
			{name: 'IMPORTE_PAGO_DL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsAcuseData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsAcuseData(null, null, null);
				}
			}
		}
	});


	var grupConsAcuse = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: ' ', colspan: 3, align: 'center'},
				{	header: '<b>Moneda Nacional ', colspan: 2, align: 'center'},
				{	header: '<b>D�lares ', colspan: 2, align: 'center'}
			]
		]
	});

	var gridAcuse = new Ext.grid.GridPanel({
		store: consAcuseData,
		id: 'gridAcuse',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '',
		plugins: grupConsAcuse,
		columns: [
			{
				header: 'Folio de Solicitud',
				tooltip: 'Folio de Solicitud',
				dataIndex: 'FOLIO_SOLIC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Tipo de Solicitud',
				tooltip: 'Tipo  de Solicitud',
				dataIndex: 'TIPO_SOLIC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'left'
			},
			{
				header: 'Fecha y Hora de Operaci�n',
				tooltip: 'Fecha y Hora de Operaci�n',
				dataIndex: 'FECHA_HORA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			//Moneda Nacional
			{
				header: '<center>N�mero de Operaciones <br>Aceptadas</center>',
				tooltip: 'N�mero de Operaciones Aceptadas',
				dataIndex: 'NUM_OPERACIONES_ACEP_MN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'left'
			},
			{
				header: '<center>Importe de pago<br> de comisiones e IVA</center>',
				tooltip: 'Importe de pago de comisiones e IVA',
				dataIndex: 'IMPORTE_PAGO_MN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			//Moneda  Dolares
			{
				header: '<center>N�mero de Operaciones <br>Aceptadas<center>',
				tooltip: 'N�mero de Operaciones Aceptadas',
				dataIndex: 'NUM_OPERACIONES_ACEP_DL',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header: '<center>Importe de pago <br>de comisiones e IVA</center>',
				tooltip: 'Importe de pago de comisiones e IVA',
				dataIndex: 'IMPORTE_PAGO_DL',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
     	stripeRows: true,
		loadMask: true,
		height: 300,
		width: 900,
		frame: true,
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '29forma4.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GenerarPDF',
								numFolios:numFolios
							}),
							callback: procesarGenerarPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Salir',
					id: 'btnSalirA',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = "29forma4ext.jsp";
					}
				}
			]
		}
	});

	// funcion que valida si se genero el acuse
	function procesarSuccessFailureAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);

			var autentificacion = Ext.getCmp('autentificacion');
			Ext.getCmp("mensaje").setValue(jsonData.mensaje);
			Ext.getCmp("recibo").setValue(jsonData.acuse);
			autentificacion.show();
			Ext.getCmp('gridPreAcuse').hide();

			if(jsonData.acuse !='N'){
				consAcuseData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultaAcuse',
						numFolios:numFolios,
						recibo:jsonData.acuse
					})
				});
			}else {
				Ext.getCmp('fpBotones').show();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	//Funciones apra validar que se haya seleccioando un regsitro
	var fnProcesaConfirmCallback = function(vpkcs7, vtextoFirmar, vnumFolios){

		if (Ext.isEmpty(vpkcs7)) {
			return;	//Error en la firma. Termina...
		}

		Ext.Ajax.request({
			url : '29forma4.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'GeneraAcuse',
				numFolios: vnumFolios,
				pkcs7: vpkcs7,
				textoFirmar: vtextoFirmar
			}),
			callback: procesarSuccessFailureAcuse
		});
	}

	var procesarConfirmacion = function() {
		numFolios = [];
		var  totOperMN=0;
		var  comOperMN=0;
		var  totOperUSD=0;
		var  comOperUSD=0;
		var gridPreAcuse = Ext.getCmp('gridPreAcuse');
		var store = gridPreAcuse.getStore();
		store.each(function(record) {
			numFolios.push(record.data['FOLIO_SOLIC']);
			totOperMN +=  parseFloat(record.data['NUM_OPERACIONES_ACEP_MN']);
			comOperMN += parseFloat(record.data['IMPORTE_PAGO_MN']);
			totOperUSD +=parseFloat(record.data['NUM_OPERACIONES_ACEP_DL']);
			comOperUSD +=parseFloat(record.data['IMPORTE_PAGO_DL']);
		});

		var textoFirmar = 'Folio:'+numFolios+'\n'+
								' Numero de Operaciones Aceptadas Moneda Nacional: '+totOperMN+"\n"+
								' Monto Total de Operaciones Aceptadas Moneda Nacional: $ '+comOperMN+'\n'+
								' Numero de Operaciones Aceptadas D�lares: '+totOperUSD+'\n'+
								' Monto Total de Operaciones Aceptadas D�lares: $ '+comOperUSD+'\n';


		NE.util.obtenerPKCS7(fnProcesaConfirmCallback, textoFirmar, numFolios);

	}

	// *************************Grid de Consulta PreAcuse  *******************

	var procesarConsPreAcuseData = function(store, arrRegistros, opts) 	{
		var gridPreAcuse = Ext.getCmp('gridPreAcuse');
		if (arrRegistros != null) {
			if (!gridPreAcuse.isVisible()) {
				gridPreAcuse.show();
			}
			//edito el titulo de la columna
			var el = gridPreAcuse.getGridEl();
			var cm = gridPreAcuse.getColumnModel();
			var jsonData = store.reader.jsonData;
			Ext.getCmp('forma').hide();
			Ext.getCmp('gridConsulta').hide();

			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnConfirmar').enable();
				Ext.getCmp('btnCancelarP').enable();
			} else {
				Ext.getCmp('btnConfirmar').disable();
				Ext.getCmp('btnCancelarP').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consPreAcuseData   = new Ext.data.JsonStore({
		root : 'registros',
		url : '29forma4.data.jsp',
		baseParams: {
			informacion: 'ConsPreAcuse'
		},
		fields: [
			{name: 'FOLIO_SOLIC'},
			{name: 'TIPO_SOLIC'},
			{name: 'FECHA_HORA'},
			{name: 'NUM_OPERACIONES_ACEP_MN'},
			{name: 'IMPORTE_PAGO_MN'},
			{name: 'NUM_OPERACIONES_ACEP_DL'},
			{name: 'IMPORTE_PAGO_DL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsPreAcuseData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsPreAcuseData(null, null, null);
				}
			}
		}
	});

	var grupConsPreA = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '', colspan: 3, align: 'center'},
				{	header: '<b>Moneda Nacional ', colspan: 2, align: 'center'},
				{	header: '<b>D�lares ', colspan: 2, align: 'center'}
			]
		]
	});

	var gridPreAcuse = new Ext.grid.GridPanel({
		store: consPreAcuseData,
		id: 'gridPreAcuse',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '',
		plugins: grupConsPreA,
		columns: [
			{
				header: 'Folio de Solicitud',
				tooltip: 'Folio de Solicitud',
				dataIndex: 'FOLIO_SOLIC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Tipo de Solicitud',
				tooltip: 'Tipo  de Solicitud',
				dataIndex: 'TIPO_SOLIC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'left'
			},
			{
				header: 'Fecha y Hora <br>de Operaci�n',
				tooltip: 'Fecha y Hora de Operaci�n',
				dataIndex: 'FECHA_HORA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			//Moneda Nacional
			{
				header: '<center>N�mero de Operaciones <br>Aceptadas</center>',
				tooltip: 'N�mero de Operaciones Aceptadas',
				dataIndex: 'NUM_OPERACIONES_ACEP_MN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header: '<center>Importe de pago <br>de comisiones e IVA</center>',
				tooltip: 'Importe de pago de comisiones e IVA',
				dataIndex: 'IMPORTE_PAGO_MN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			//Moneda  Dolares
			{
				header: '<center>N�mero de Operaciones <br>Aceptadas</center>',
				tooltip: 'N�mero de Operaciones Aceptadas',
				dataIndex: 'NUM_OPERACIONES_ACEP_DL',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header: '<center>Importe de pago <br>de comisiones e IVA</center>',
				tooltip: 'Importe de pago de comisiones e IVA',
				dataIndex: 'IMPORTE_PAGO_DL',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
     	stripeRows: true,
		loadMask: true,
		height: 300,
		width: 900,
		frame: true,
		bbar: {
		items: [
			'->',
			'-',
				{
					xtype: 'button',
					text: 'Confirmar Autorizaci�n',
					id: 'btnConfirmar',
					iconCls: 'icoAceptar',
					handler: procesarConfirmacion
				},
				'-',
				{
					xtype: 'button',
					text: 'Cancelar',
					iconCls: 'icoLimpiar',
					id: 'btnCancelarP',
					handler: function(boton, evento) {
						window.location = '29forma4ext.jsp';
					}
				}
			]
		}
	});


	//Funcion apra validar que se haya seleccioando un regsitro
	var procesarAutorizar = function() {

		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		numFolios = [];

		store.each(function(record) {
			if( record.data['SELECCIONAR'] ==true){
				numFolios.push(record.data['FOLIO_SOLIC']);
			}
		});

		if(numFolios =='') {
			Ext.MessageBox.alert("Mensaje","Se debe seleccionar al menos un registro para poder continuar con la operaci�n.");
			return false;
		}

		consPreAcuseData.load({
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ConsPreAcuse',
				numFolios:numFolios
			})
		});
	}

	// *************************Grid de COnsulta *******************

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		var gridConsulta = Ext.getCmp('gridConsulta');

		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;

			if(store.getTotalCount() > 0) {
				el.unmask();
				Ext.getCmp('btnAutorizar').enable();
				Ext.getCmp('btnCancelar').enable();
			} else {
				Ext.getCmp('btnAutorizar').disable();
				Ext.getCmp('btnCancelar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consultaData   = new Ext.data.JsonStore({
		root : 'registros',
		url : '29forma4.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'FOLIO_SOLIC'},
			{name: 'TIPO_SOLIC'},
			{name: 'FECHA_HORA'},
			{name: 'NUM_OPERACIONES_ACEP_MN'},
			{name: 'IMPORTE_PAGO_MN'},
			{name: 'NUM_OPERACIONES_ACEP_DL'},
			{name: 'IMPORTE_PAGO_DL'},
			{name: 'SELECCIONAR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var grupCons = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '', colspan: 3, align: 'center'},
				{	header: '<b>Moneda Nacional ', colspan: 2, align: 'center'},
				{	header: '<b>D�lares ', colspan: 2, align: 'center'},
				{	header: '', colspan: 1, align: 'center'}
			]
		]
	});

	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		hidden: true,
		title: '',
		plugins: grupCons,
		columns: [
			{
				header: 'Folio de Solicitud',
				tooltip: 'Folio de Solicitud',
				dataIndex: 'FOLIO_SOLIC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Tipo de Solicitud',
				tooltip: 'Tipo  de Solicitud',
				dataIndex: 'TIPO_SOLIC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'left'
			},
			{
				header: 'Fecha y Hora <br>de Operaci�n',
				tooltip: 'Fecha y Hora de Operaci�n',
				dataIndex: 'FECHA_HORA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			//Moneda Nacional
			{
				header: 'N�mero de Operaciones <br> Aceptadas',
				tooltip: 'N�mero de Operaciones Aceptadas',
				dataIndex: 'NUM_OPERACIONES_ACEP_MN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header: '<center>Importe de pago <br> de comisiones e IVA </center>',
				tooltip: 'Importe de pago de comisiones e IVA',
				dataIndex: 'IMPORTE_PAGO_MN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			//Moneda  Dolares
			{
				header: 'N�mero de Operaciones <br>Aceptadas',
				tooltip: 'N�mero de Operaciones Aceptadas',
				dataIndex: 'NUM_OPERACIONES_ACEP_DL',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header: '<center>Importe de pago <br> de comisiones e IVA</center>',
				tooltip: 'Importe de pago de comisiones e IVA',
				dataIndex: 'IMPORTE_PAGO_DL',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 'checkcolumn',
				header : 'Seleccione',
				tooltip: 'Seleccione',
				dataIndex : 'SELECCIONAR',
				width : 150,
				align: 'center',
				sortable : false
			}
		],
      displayInfo: true,
		emptyMsg: "No hay registros.",
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
		items: [
			'->',
			'-',
				{
					xtype: 'button',
					text: 'Autorizar',
					iconCls: 'icoAceptar',
					id: 'btnAutorizar',
					handler: procesarAutorizar
				},
				'-',
				{
					xtype: 'button',
					text: 'Cancelar',
					iconCls: 'icoLimpiar',
					id: 'btnCancelar',
					handler: function(boton, evento) {
						window.location = '29forma4ext.jsp';
					}
				}
			]
		}
	});


	//*********************Forma ********************


	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29forma4.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoFolio = new Ext.data.JsonStore({
		id: 'catalogoFolio',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29forma4.data.jsp',
		baseParams: {
			informacion: 'catalogoFolio'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoFechas = new Ext.data.JsonStore({
		id: 'catalogoFechas',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '29forma4.data.jsp',
		baseParams: {
			informacion: 'catalogoFechas'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var  elementosForma  = [
		{ 	xtype: 'textfield',  hidden: true, id: 'recibo', 	value: '' },
		{
			xtype: 'combo',
			fieldLabel: 'Nombre del IF',
			name: 'claveIF',
			id: 'claveIF1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',
			valueField: 'clave',
			hiddenName : 'claveIF',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIF,
			hidden: true,
			listeners: {
				select: {
						fn: function(combo) {
							var ic_folio1 = Ext.getCmp('ic_folio1').getValue();

							catalogoFolio.load({
								params: {
									claveIF:combo.getValue()
								}
							});

							var df_fecha_hora1 = Ext.getCmp('df_fecha_hora1').getValue();
							catalogoFechas.load({
								params: {
									claveIF:combo.getValue()
								}
							});

						}
					}
				}

		},
		{
			xtype: 'combo',
			fieldLabel: 'Folio de Solicitud',
			name: 'ic_folio',
			id: 'ic_folio1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione el N�mero de Folio ...',
			valueField: 'clave',
			hiddenName : 'ic_folio',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoFolio
		},
		{
			xtype: 'combo',
			fieldLabel: 'Fecha y Hora de Operaci�n',
			name: 'df_fecha_hora',
			id: 'df_fecha_hora1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione una Fecha ...',
			valueField: 'clave',
			hiddenName : 'df_fecha_hora',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoFechas
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: 'margin:0 auto;',
		frame: true,
		border: false,
		title: 'Autorizaci�n de pago de comisiones',
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '29forma4ext.jsp';
				}
			}
		]
	});

	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			autentificacion,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridPreAcuse,
			gridAcuse,
			fpBotones,
			NE.util.getEspaciador(20)
		]
	});

	 //Peticion para obtener valores iniciales y la parametrizaci�n
	 fp.el.mask('Cargando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '29forma4.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});

});