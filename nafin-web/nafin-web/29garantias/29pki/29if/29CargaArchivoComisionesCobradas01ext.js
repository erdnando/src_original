var showPanelDescripcionLayout;

Ext.onReady(function() {
	
	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});
	
	//-------------------------------- VALIDACIONES -----------------------------------
 
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaCargaArchivo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaArchivo(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaArchivo(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelFormaSolicitudConciliacion").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana preparar validacion si esta esta siendo mostrada
         hideWindowPrepararValidacion();
         
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceValidacion();
         
         // Ocultar mascara del panel de resultados de la validacion
         element = Ext.getCmp("panelResultadosValidacion").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Ocultar mascara del panel de preacuse
			element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var cargaArchivo = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"					){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29CargaArchivoComisionesCobradas01ext.data.jsp',
				params: 	{
					informacion:		'CargaArchivo.inicializacion'
				},
				callback: 				procesaCargaArchivo
			});
			
		} else if(			estado == "MOSTRAR_AVISO"								){

			// Ocultar Box con animacion de inicializacion
			Ext.getCmp('contenedorInicializacion').hide();
			
			if(!Ext.isEmpty(respuesta.aviso)){
				
				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);
				
				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();
				
			}
			
		} else if(			estado == "ESPERAR_CAPTURA_SOLICITUD_CONCILIACION"	){
  
			var nombreMesConciliar 	= Ext.getCmp('nombreMesConciliar');
			var claveMesConciliar 	= Ext.getCmp('claveMesConciliar');
			
			// Cargar nombre del Mes a Conciliar
			nombreMesConciliar.setValue(respuesta.nombreMesConciliar);
			nombreMesConciliar.originalValue = nombreMesConciliar.getValue();
			// Cargar clave del Mes a Conciliar
			claveMesConciliar.setValue(respuesta.claveMesConciliar);
			claveMesConciliar.originalValue = claveMesConciliar.getValue();
			// Limpiar contenido del numberfield
			Ext.getCmp('numeroRegistros').setValue("");
			// Limpiar contenido del numberfield
			Ext.getCmp('sumatoriaComisionMensual').setValue("");
			
			// Ocultar Box con animacion de inicializacion
			Ext.getCmp('contenedorInicializacion').hide();
			
			var panelFormaSolicitudConciliacion = Ext.getCmp("panelFormaSolicitudConciliacion");
			panelFormaSolicitudConciliacion.getForm().clearInvalid();
			panelFormaSolicitudConciliacion.show();
		
			// Definir el texto del layout de carga
			if(!Ext.isEmpty(respuesta.descripcionLayout)){
				Ext.getCmp("labelLayoutCarga").setText(respuesta.descripcionLayout,false);
			}
 
		} else if(			estado == "ENVIAR_SOLICITUD_CONCILIACION" 				){
			
			Ext.getCmp("panelFormaSolicitudConciliacion").el.mask("Enviando...",'x-mask-loading');
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29CargaArchivoComisionesCobradas01ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CargaArchivo.enviarSolicitudConciliacion'
					},
					respuesta
				),
				callback: 				procesaCargaArchivo
			});
 
		} else if(			estado == "ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES"	){
			
			// Ocultar panel con la solicitud de carga
			var panelFormaSolicitudConciliacion = Ext.getCmp("panelFormaSolicitudConciliacion");
			panelFormaSolicitudConciliacion.el.unmask();
			panelFormaSolicitudConciliacion.hide();
 
			// Esperar decision
			Ext.getCmp("numeroRegistros1").setValue(respuesta.numeroRegistros);
			Ext.getCmp("sumatoriaComisionMensual1").setValue(respuesta.sumatoriaComisionMensual);
			Ext.getCmp("claveMesConciliar1").setValue(respuesta.claveMesConciliar);

			// Mostrar panel de carga de archivo
			var panelFormaCargaArchivo = Ext.getCmp("panelFormaCargaArchivo");
			panelFormaCargaArchivo.show();
			
		} else if(			estado == "SUBIR_ARCHIVO"								){
			
			Ext.getCmp("panelFormaCargaArchivo").getForm().submit({
				clientValidation: 	true,
				url: 						'29CargaArchivoComisionesCobradas01ext.data.jsp?informacion=CargaArchivo.subirArchivo',
				waitMsg:   				'Subiendo archivo...',
				waitTitle: 				'Por favor espere',
				success: function(form, action) {
					 procesaCargaArchivo(null,  true,  action.response );
				},
				failure: function(form, action) {
				 	 action.response.status = 200;
					 procesaCargaArchivo(null,  false, action.response );
				}
			});
			
		} else if(			estado == "REALIZAR_VALIDACION"         			){

			// Mostrar ventana de inicializacion de la validacion
			showWindowPrepararValidacion();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 		'29CargaArchivoComisionesCobradas01ext.data.jsp',
				params: 	{
					informacion:								'CargaArchivo.realizarValidacion',
					numeroRegistros: 							respuesta.numeroRegistros,
					sumatoriaComisionMensual: 				respuesta.sumatoriaComisionMensual,
					claveMesConciliar:						respuesta.claveMesConciliar,
					fileName:									respuesta.fileName
				},
				callback: 										procesaCargaArchivo
			});
			
		} else if(			estado == "MUESTRA_PANTALLA_AVANCE"		         ){

			// Ocultar ventana de inicializacion de la validacion
			hideWindowPrepararValidacion();
			
			// Mostrar popup de avance de la validacion
			showWindowAvanceValidacion(respuesta.numeroTotalRegistrosProcesar);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 		'29CargaArchivoComisionesCobradas01ext.data.jsp',
				params: 	{
					informacion: 								'CargaArchivo.muestraPantallaAvance',
					numeroTotalRegistrosProcesar:			respuesta.numeroTotalRegistrosProcesar,
					numeroRegistros: 							respuesta.numeroRegistros,
					sumatoriaComisionMensual: 				respuesta.sumatoriaComisionMensual,
					claveMesConciliar:						respuesta.claveMesConciliar,
					fileName:									respuesta.fileName
				},
				callback: 		 procesaCargaArchivo
			});
	
		} else if( estado == "ACTUALIZAR_AVANCE"						) {
 			
			// Actualizar porcensaje de avance

			var avance								= respuesta.avance;
			var textoPorcentajeAvance 			= formateaPorcentajeAvance(respuesta.porcentajeAvance);
			var numeroRegistrosProcesados		= respuesta.numeroRegistrosProcesados;
	
 			var barrarAvanceValidacion			= Ext.getCmp("barrarAvanceValidacion");
			var valorRegistrosProcesados		= Ext.getCmp("valorRegistrosProcesados");

			barrarAvanceValidacion.updateProgress( Number(avance), textoPorcentajeAvance, true);
			valorRegistrosProcesados.setText(numeroRegistrosProcesados);
		
			// En 1.618 segundos "aureos" volver a consultar el porcentaje de avance
			var actualizarAvanceTask = new Ext.util.DelayedTask(
				function(){
					Ext.Ajax.request({
						url: 		'29CargaArchivoComisionesCobradas01ext.data.jsp',
						params: 	{
							informacion: 						'CargaArchivo.actualizarAvance',
							envioFinalizado: 					respuesta.envioFinalizado,
							numeroRegistros: 					respuesta.numeroRegistros,
							sumatoriaComisionMensual: 		respuesta.sumatoriaComisionMensual,
							claveMesConciliar:				respuesta.claveMesConciliar,
							fileName:							respuesta.fileName
						},
						callback: procesaCargaArchivo 
					});
				},
				this
			);
			actualizarAvanceTask.delay(1618);
		
		} else if(  estado == "PREPARAR_RESULTADOS_VALIDACION" ){
			
			Ext.Ajax.request({
				url: 		'29CargaArchivoComisionesCobradas01ext.data.jsp',
				params: 	{
					informacion:								'CargaArchivo.prepararResultadosValidacion',
					numeroRegistros: 							respuesta.numeroRegistros,
					sumatoriaComisionMensual: 				respuesta.sumatoriaComisionMensual,
					claveMesConciliar:						respuesta.claveMesConciliar,
					fileName:									respuesta.fileName
				},
				callback: 										procesaCargaArchivo
			});
			
		} else if(  estado == "ESPERAR_DECISION_VALIDACION" ){
 
			var numeroRegistros 										= respuesta.numeroRegistros;
			var sumatoriaComisionMensual 							= respuesta.sumatoriaComisionMensual;
			var claveMesConciliar									= respuesta.claveMesConciliar;
			var fileName												= respuesta.fileName;
			
			var claveProceso 											= respuesta.claveProceso;
			var totalRegistros 										= respuesta.totalRegistros;
			var montoTotal 											= respuesta.montoTotal;      
			
			var numeroRegistrosSinErrores 						= respuesta.numeroRegistrosSinErrores;
			var sumatoriaComisionMensualSinErrores 			= respuesta.sumatoriaComisionMensualSinErrores;
			
			var numeroRegistrosConErrores 						= respuesta.numeroRegistrosConErrores;
			var sumatoriaComisionMensualConErrores 			= respuesta.sumatoriaComisionMensualConErrores;
 
			var continuarCarga 										= respuesta.continuarCarga;

			// SETUP PARAMETROS VALIDACION
			
			Ext.getCmp("panelResultadosValidacion.numeroRegistros").setValue(numeroRegistros);
         Ext.getCmp("panelResultadosValidacion.sumatoriaComisionMensual").setValue(sumatoriaComisionMensual);
         Ext.getCmp("panelResultadosValidacion.claveMesConciliar").setValue(claveMesConciliar);
         Ext.getCmp("panelResultadosValidacion.fileName").setValue(fileName);

			// Recordar numero de proceso y el total de registros
         Ext.getCmp("panelResultadosValidacion.claveProceso").setValue(claveProceso);
			Ext.getCmp("panelResultadosValidacion.totalRegistros").setValue(totalRegistros);
			Ext.getCmp("panelResultadosValidacion.montoTotal").setValue(montoTotal);
 
         // Inicializar panel de resultados
			Ext.getCmp("numeroRegistrosSinErrores").setValue(numeroRegistrosSinErrores);
			Ext.getCmp("sumatoriaComisionMensualSinErrores").setValue(sumatoriaComisionMensualSinErrores);
			
			Ext.getCmp("numeroRegistrosConErrores").setValue(numeroRegistrosConErrores);
			Ext.getCmp("sumatoriaComisionMensualConErrores").setValue(sumatoriaComisionMensualConErrores);
 
			// Determinar si se mostrar� el bot�n de continuar carga
			var botonExtraerErroresValidacion = Ext.getCmp('botonExtraerErroresValidacion');
			var botonContinuarCarga           = Ext.getCmp('botonContinuarCarga');
			
			if(continuarCarga){
				botonExtraerErroresValidacion.disable();
				botonExtraerErroresValidacion.urlArchivo = '';
				botonExtraerErroresValidacion.setIconClass('icoGenerarXls');
				botonExtraerErroresValidacion.setText('Extraer Errores');
         	botonContinuarCarga.enable();
         } else {
         	botonExtraerErroresValidacion.enable();
         	botonExtraerErroresValidacion.urlArchivo = '';
         	botonExtraerErroresValidacion.setIconClass('icoGenerarXls');
				botonExtraerErroresValidacion.setText('Extraer Errores');
         	botonContinuarCarga.disable();
         }
         
			// MOSTRAR COMPONENTES
			// Resetear campo de carga de archivo
			Ext.getCmp("archivo").reset();
			
			// Ocultar ventana de avance validacion
			hideWindowAvanceValidacion();
			
			 // Remover contenido anterior de los grids
         var registrosSinErroresData 		= Ext.StoreMgr.key('registrosSinErroresDataStore');
         var registrosConErroresData 		= Ext.StoreMgr.key('registrosConErroresDataStore');
         var erroresVsCifrasControlData 	= Ext.StoreMgr.key('erroresVsCifrasControlDataStore');
         //registrosSinErroresData.removeAll();
         //registrosConErroresData.removeAll();
         //erroresVsCifrasControlData.removeAll();
 
         // Mostrar espaciador del Panel de resultados
			Ext.getCmp("espaciadorPanelResultadosValidacion").show();
			// Mostrar Panel de resultados
         Ext.getCmp('panelResultadosValidacion').show();

         // Mostrar Panel de Resultados de la validacion
         var tabPanelResultadosValidacion = Ext.getCmp('tabPanelResultadosValidacion');
         tabPanelResultadosValidacion.setActiveTab(2);
         erroresVsCifrasControlData.loadData(respuesta.erroresVsCifrasControlDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
         tabPanelResultadosValidacion.setActiveTab(1);
         registrosConErroresData.loadData(respuesta.registrosConErroresDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
         tabPanelResultadosValidacion.setActiveTab(0);
         registrosSinErroresData.loadData(respuesta.registrosSinErroresDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
 
      } else if(  estado == "PRESENTAR_PREACUSE"         ){
      	
      	Ext.getCmp("panelResultadosValidacion").el.mask("Generando Pre-Acuse...","x-mask-loading");
      	
      	Ext.Ajax.request({
				url: 		'29CargaArchivoComisionesCobradas01ext.data.jsp',
				params: 	{
					informacion:								'CargaArchivo.presentarPreacuse',
					claveProceso: 								Ext.getCmp("panelResultadosValidacion.claveProceso").getValue(),
					totalRegistros: 							Ext.getCmp("panelResultadosValidacion.totalRegistros").getValue(),
					montoTotal: 								Ext.getCmp("panelResultadosValidacion.montoTotal").getValue(),
					numeroRegistros: 							Ext.getCmp("panelResultadosValidacion.numeroRegistros").getValue(),
					sumatoriaComisionMensual: 				Ext.getCmp("panelResultadosValidacion.sumatoriaComisionMensual").getValue(),
					claveMesConciliar:						Ext.getCmp("panelResultadosValidacion.claveMesConciliar").getValue(),
					fileName:									Ext.getCmp("panelResultadosValidacion.fileName").getValue()
				},
				callback: 										procesaCargaArchivo
			});
      	
		} else if(  estado == "ESPERAR_DECISION_PREACUSE"         ){
			
			var claveProceso 								= respuesta.claveProceso;
			var totalRegistros 							= respuesta.totalRegistros;
			var montoTotal 								= respuesta.montoTotal;
 
			var numeroRegistros 							= respuesta.numeroRegistros;
			var sumatoriaComisionMensual 				= respuesta.sumatoriaComisionMensual;
			var claveMesConciliar						= respuesta.claveMesConciliar;
			var fileName 									= respuesta.fileName;

			// Establecer los valores de los parametros de la carga
			Ext.getCmp("panelPreacuseCargaArchivo.claveProceso").setValue(claveProceso);
			Ext.getCmp("panelPreacuseCargaArchivo.totalRegistros").setValue(totalRegistros);
			Ext.getCmp("panelPreacuseCargaArchivo.montoTotal").setValue(montoTotal);
 
			Ext.getCmp("panelPreacuseCargaArchivo.numeroRegistros").setValue(numeroRegistros);
			Ext.getCmp("panelPreacuseCargaArchivo.sumatoriaComisionMensual").setValue(sumatoriaComisionMensual);
			Ext.getCmp("panelPreacuseCargaArchivo.claveMesConciliar").setValue(claveMesConciliar);
			Ext.getCmp("panelPreacuseCargaArchivo.fileName").setValue(fileName);
			
			// Suprimir Mascara
			Ext.getCmp("panelResultadosValidacion").el.unmask();
			
			// Ocultar panel de Carga
			Ext.getCmp("panelFormaCargaArchivo").hide();
			// Ocultar espaciador del panel de resultados
			Ext.getCmp("espaciadorPanelResultadosValidacion").hide();
			// Ocultar panel de resultados
			Ext.getCmp("panelResultadosValidacion").hide();
			// Ocultar espaciador del layout
			Ext.getCmp("espaciadorPanelDescripcionLayout").hide();
			// Ocultar panel con el layout de carga
			Ext.getCmp("panelDescripcionLayout").hide();
			
			// Mostrar panel de preacuse
			Ext.getCmp("panelPreacuseCargaArchivo").show();
			
			// Cargar resultados
			Ext.StoreMgr.key('registrosPorAgregarDataStore').loadData(respuesta.registrosPorAgregarDataArray);
			
      } else if(  estado == "ESPERAR_DECISION"  			){
      	
      	// Ocultar ventana preparar validacion si esta esta siendo mostrada
         hideWindowPrepararValidacion();
         
      	// Ocultar mascara del panel de preacuse
			var element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
      	return;
      	
		} else if(  estado == "TRANSMITIR_REGISTROS" 		){
			
			var claveProceso							= Ext.getCmp("panelPreacuseCargaArchivo.claveProceso").getValue();
			var totalRegistros						= Ext.getCmp("panelPreacuseCargaArchivo.totalRegistros").getValue();
			var montoTotal								= Ext.getCmp("panelPreacuseCargaArchivo.montoTotal").getValue();
			
			var numeroRegistros						= Ext.getCmp("panelPreacuseCargaArchivo.numeroRegistros").getValue();
			var sumatoriaComisionMensual			= Ext.getCmp("panelPreacuseCargaArchivo.sumatoriaComisionMensual").getValue();
			var claveMesConciliar					= Ext.getCmp("panelPreacuseCargaArchivo.claveMesConciliar").getValue();
			var fileName 								= Ext.getCmp("panelPreacuseCargaArchivo.fileName").getValue();
			
			// Agregar mensaje transmitiendo registros
			Ext.getCmp("panelPreacuseCargaArchivo").el.mask("Transmitiendo registros...","x-mask-loading");
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 												'29CargaArchivoComisionesCobradas01ext.data.jsp',
				params: {	
					informacion:								'CargaArchivo.transmitirRegistros',
					claveProceso:								claveProceso,
					totalRegistros:							totalRegistros,
					montoTotal:									montoTotal,
					numeroRegistros:							numeroRegistros,
					sumatoriaComisionMensual:				sumatoriaComisionMensual,
					claveMesConciliar:						claveMesConciliar,
					fileName:									fileName,
					isEmptyPkcs7: 								Ext.isEmpty(respuesta.pkcs7),
					pkcs7: 										respuesta.pkcs7,
					textoFirmado: 								respuesta.textoFirmado
				},
				callback: 										procesaCargaArchivo
			});
         
		} else if(	estado == "MOSTRAR_ACUSE_TRANSMISION_REGISTROS"	){
 
			var claveProceso 						= respuesta.claveProceso;
			var totalRegistros 					= respuesta.totalRegistros;
			var montoTotal 						= respuesta.montoTotal;
			 
			var numeroRegistros 					= respuesta.numeroRegistros;
			var sumatoriaComisionMensual 		= respuesta.sumatoriaComisionMensual;
			var claveMesConciliar 				= respuesta.claveMesConciliar;
			var fileName							= respuesta.fileName;
			
			var folio								= respuesta.folio;
			
			// Establecer los valores de los parametros de la carga
			Ext.getCmp("panelAcuseCargaArchivo.claveProceso").setValue(claveProceso);
			Ext.getCmp("panelAcuseCargaArchivo.totalRegistros").setValue(totalRegistros);
			Ext.getCmp("panelAcuseCargaArchivo.montoTotal").setValue(montoTotal);
 
			Ext.getCmp("panelAcuseCargaArchivo.numeroRegistros").setValue(numeroRegistros);
			Ext.getCmp("panelAcuseCargaArchivo.sumatoriaComisionMensual").setValue(sumatoriaComisionMensual);
			Ext.getCmp("panelAcuseCargaArchivo.claveMesConciliar").setValue(claveMesConciliar);
			
			Ext.getCmp("panelAcuseCargaArchivo.folio").setValue(folio);
				
			// Agregar mensaje transmitiendo solicitudes
			Ext.getCmp("panelPreacuseCargaArchivo").el.unmask();
			
			// Ocultar panel de preacuse
			Ext.getCmp("panelPreacuseCargaArchivo").hide();
			
			// Mostrar panel de preacuse
			Ext.getCmp("panelAcuseCargaArchivo").show();
			
			// Mostrar el folio de la validacion
			Ext.getCmp("labelAutentificacion").setText(respuesta.folioSolicitud,false);
			
			// Resetear cualquier url de archivo que pudiera existir anteriormente
			var botonImprimirPDF = Ext.getCmp("botonImprimirPDF");
			botonImprimirPDF.setIconClass('icoImprimir');
			botonImprimirPDF.setText('Imprimir PDF');
			botonImprimirPDF.urlArchivo = "";
						
			// Cargar resultados
			Ext.StoreMgr.key('registrosTransmitidosDataStore').loadData(respuesta.registrosTransmitidosDataArray);
 
		} else if(	estado == "FIN"								){
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "29CargaArchivoComisionesCobradas01ext.jsp";
			forma.target	= "_self";
			forma.submit();
 
		}
		
		return;
		
	}
 
	//-------------------------- 9. PANEL ACUSE CARGA CUENTAS -------------------------
		
	var procesarSuccessFailureGeneraArchivoPDF =  function(opts, success, response) {
 
		var botonImprimirPDF = Ext.getCmp('botonImprimirPDF');
		botonImprimirPDF.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			botonImprimirPDF.setIconClass('icoPdf');
			botonImprimirPDF.setText('Descargar PDF');
			
			botonImprimirPDF.urlArchivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			botonImprimirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		
			if(Ext.isIE7){ // Nota: debido a un bug en el width en IE7, se vuelve a realizar el layout de los componentes
				Ext.getCmp('botonesAcuseCargaArchivo').doLayout();
			}
			
		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						botonImprimirPDF.setIconClass('icoImprimir');
						botonImprimirPDF.setText('Imprimir PDF');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				botonImprimirPDF.setIconClass('icoImprimir');
				botonImprimirPDF.setText('Imprimir PDF');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
 
	}
	
	var procesarConsultaRegistrosTransmitidos = function(store, registros, opts){
		
		var gridRegistrosTransmitidos 			= Ext.getCmp('gridRegistrosTransmitidos');
		
		if (registros != null) {
			
			var el 							= gridRegistrosTransmitidos.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
				
			}
			
		}
		
	}
	
	var registrosTransmitidosData = new Ext.data.ArrayStore({
			
		storeId: 'registrosTransmitidosDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosTransmitidos,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosTransmitidos(null, null, null);						
				}
			}
		}
		
	});
	
	var renderContenidoGridAcuse = function(value, metaData, record, rowIndex, colIndex, store) {
		
		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: right !important';
		} else {
			metaData.style += 'text-align: right !important';
		}
		return value;
		
	}
	
	var elementosAcuseCargaArchivo = [
		{
			xtype: 	'label',
			id:	 	'labelAutentificacion',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:15px;',
			html:  	'Cargando...'
		},
		{
			xtype: 	'label',
			id:	 	'labelAcuseCifrasControl',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		173,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'west'
					},
					{
						store: 			registrosTransmitidosData,
						xtype: 			'grid',
						id:				'gridRegistrosTransmitidos',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '80%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		215,
								hidden: 		false,
								hideable:	false,
								fixed:		true
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								align:		'right',
								renderer: 	renderContenidoGridAcuse
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'east'
					}
			]
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			id:			'botonesAcuseCargaArchivo',
			anchor: 		'100%',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			items: [
				{
					xtype: 		'button',
					text: 		'Imprimir PDF',
					minWidth: 	137,
					margins: 	' 3',
					iconCls: 	'icoImprimir',
					id: 			'botonImprimirPDF',
					urlArchivo: '',
					handler:    function(boton, evento) {
 
						if(Ext.isEmpty(boton.urlArchivo)){
							
							// Cambiar icono
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							// Genera Archivo PDF
							Ext.Ajax.request({
								url: 			'29CargaArchivoComisionesCobradas01ext.data.jsp',
								params: 		{ 
									informacion: 		'GeneraArchivoPDF',
									totalRegistros:	Ext.getCmp("panelAcuseCargaArchivo.totalRegistros").getValue(),
									montoTotal:			Ext.getCmp("panelAcuseCargaArchivo.montoTotal").getValue()
								},
								callback: 	procesarSuccessFailureGeneraArchivoPDF
							});
							
						// Descargar el archivo generado	
						} else {
							
							var forma    = Ext.getDom('formAux');
							forma.action = NE.appWebContextRoot+'/DescargaArchivo';
							forma.method = 'post';
							forma.target = '_self';
							// Preparar Archivo a Descargar
							var archivo  = boton.urlArchivo;				
							archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
							// Insertar archivo
							var inputNombreArchivo = Ext.DomHelper.insertFirst(
								forma, 
								{ 
									tag: 	'input', 
									type: 'hidden', 
									id: 	'nombreArchivo', 
									name: 'nombreArchivo', 
									value: archivo
								},
								true
							); 
							// Solicitar Archivo Al Servidor
							forma.submit();
							// Remover nodo agregado
							inputNombreArchivo.remove();
							
						}
						
					},
					style: {
						width: 137
					}
				},
				{
					xtype: 		'button',
					text: 		'Salir',
					minWidth: 	137,
					margins: 	' 3',
					iconCls: 	'icoAceptar',
					id: 			'botonSalir',
					handler:    function(boton, evento) {
						cargaArchivo("FIN",null);
					},
					style: {
						width: 137
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.claveProceso',
        id: 	'panelAcuseCargaArchivo.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.totalRegistros',
        id: 	'panelAcuseCargaArchivo.totalRegistros'
      },
      // INPUT HIDDEN: MONTO TOTAL
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.montoTotal',
        id: 	'panelAcuseCargaArchivo.montoTotal'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.numeroRegistros',
        id: 	'panelAcuseCargaArchivo.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE MONTOS DE REINSTALACION
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.sumatoriaComisionMensual',
        id: 	'panelAcuseCargaArchivo.sumatoriaComisionMensual'
      },
      // INPUT HIDDEN: CLAVE DEL MES A CONCILIAR
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.claveMesConciliar',
        id: 	'panelAcuseCargaArchivo.claveMesConciliar'
      },
      // INPUT HIDDEN: FOLIO
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.folio',
        id: 	'panelAcuseCargaArchivo.folio'
      }
	];
	
	var panelAcuseCargaArchivo = {
		title:			'Acuse de la Carga',
		hidden:			true, 
		xtype:			'panel',
		id: 				'panelAcuseCargaArchivo',
		width: 			700, //949,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosAcuseCargaArchivo
	}
	
	//----------------------------- 8. PANEL PREACUSE CARGA ---------------------------
	
	var procesarConsultaSolicitudesPorAgregar = function(store, registros, opts){
		
		var gridSolicitudesPorAgregar 			= Ext.getCmp('gridSolicitudesPorAgregar');
		
		if (registros != null) {
			
			var el 							= gridSolicitudesPorAgregar.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
				
			}
			
		}
		
	}
	
	var registrosPorAgregarData = new Ext.data.ArrayStore({
			
		storeId: 'registrosPorAgregarDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaSolicitudesPorAgregar,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaSolicitudesPorAgregar(null, null, null);						
				}
			}
		}
		
	});
	
	var renderContenidoGridPreacuse = function(value, metaData, record, rowIndex, colIndex, store) {
		
		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: right !important';
		} else {
			metaData.style += 'text-align: right !important';
		}
		return value;
		
	}
	
	var fnTransmitirRegistrosCallback = function(vpkcs7, vtextoFirmar){
		if( Ext.isEmpty(vpkcs7) ){
			return;
		// Transmitir registros
		} else {
			var respuesta 		= new Object();
			respuesta['pkcs7'] 			= vpkcs7;
			respuesta['textoFirmado'] 	= vtextoFirmar;
			cargaArchivo("TRANSMITIR_REGISTROS", respuesta);
		}
	}
	
	var elementosPreacuseCargaArchivo = [
		{
			xtype: 	'label',
			id:	 	'labelPreacuseCargaArchivo',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		95,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'west'
					},
					{
						store: 			registrosPorAgregarData,
						xtype: 			'grid',
						id:				'gridSolicitudesPorAgregar',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '80%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		215,
								hidden: 		false,
								hideable:	false,
								fixed:		true
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								//align:		'right',
								renderer: 	renderContenidoGridPreacuse
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'east'
					}
			]
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonPreacuseCancelar',
					margins: 	' 3',
					handler:    function(boton, evento) {
						
						Ext.MessageBox.confirm(
							'Confirm', 
							"�Esta usted seguro de cancelar la operaci�n?", 
							function(confirmBoton){   
								if( confirmBoton == "yes" ){
									cargaArchivo("FIN",null);
								}
							}
						);
						
					},
					style: {
						width: 100
					}
				},
				{
					xtype: 		'button',
					//width: 		80,
					text: 		'Transmitir Registros',
					iconCls: 	'icoContinuar',
					id: 			'botonTransmitirRegistros',
					margins: 	' 3',
					handler:    function(boton, evento) {
						
						// Obtener texto a firmar
						var registrosPorAgregarData = Ext.StoreMgr.key('registrosPorAgregarDataStore');
						var textoFirmar = ""; 
						registrosPorAgregarData.each(
							function(record){
								textoFirmar += record.get("DESCRIPCION") + ": " + record.get("CONTENIDO")+"\n";
							}
						);
						
						// Suprimir acentos
						textoFirmar = textoFirmar.replace(/�/g, "a").replace(/�/g, "e").replace(/�/g, "i").replace(/�/g, "o").replace(/�/g, "u");
						textoFirmar = textoFirmar.replace(/�/g, "A").replace(/�/g, "E").replace(/�/g, "I").replace(/�/g, "O").replace(/�/g, "U");
						
						// Firmar texto
						
						NE.util.obtenerPKCS7(fnTransmitirRegistrosCallback, textoFirmar);
						
						
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.claveProceso',
        id: 	'panelPreacuseCargaArchivo.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.totalRegistros',
        id: 	'panelPreacuseCargaArchivo.totalRegistros'
      },
      // INPUT HIDDEN: CLAVE DEL MES A CONCILIAR
		{  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.claveMesConciliar',
        id: 	'panelPreacuseCargaArchivo.claveMesConciliar'
      },
      // INPUT HIDDEN: MONTO TOTAL
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.montoTotal',
        id: 	'panelPreacuseCargaArchivo.montoTotal'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD 
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.numeroRegistros',
        id: 	'panelPreacuseCargaArchivo.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE MONTOS DE REINSTALACION
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.sumatoriaComisionMensual',
        id: 	'panelPreacuseCargaArchivo.sumatoriaComisionMensual'
      },
      // INPUT HIDDEN: NOMBRE DEL ARCHIVO ZIP
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.fileName',
        id: 	'panelPreacuseCargaArchivo.fileName'
      }
	];

	var panelPreacuseCargaArchivo = {
		title:			'Pre-Acuse',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelPreacuseCargaArchivo',
		width: 			600, //949,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosPreacuseCargaArchivo
	}
	
	//---------------------------- 7. PANEL LAYOUT DE CARGA ------------------------------
	
	var hidePanelDescripcionLayout = function(){
		Ext.getCmp('panelDescripcionLayout').hide();
		Ext.getCmp('espaciadorPanelDescripcionLayout').hide();
	}
	
	showPanelDescripcionLayout = function(){
		Ext.getCmp('panelDescripcionLayout').show();
		Ext.getCmp('espaciadorPanelDescripcionLayout').show();
	}
	                             
	var elementosLayoutNotificacion = [
		{
			xtype: 	'label',
			id:		'labelLayoutCarga',
			name:		'labelLayoutCarga',
			html: 	"" ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginBottom: 	'10px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		}
	];
	
	var panelDescripcionLayout = {
		xtype:			'panel',
		id: 				'panelDescripcionLayout',
		hidden:			true,
		width: 			700,
		title: 			'Descripci�n del Layout de Carga',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosLayoutNotificacion,
		tools: [
			{
				id:			'close',
				handler: function(event, toolEl, panel){
					hidePanelDescripcionLayout();
				}
    		}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      )
	}
	
	var espaciadorPanelDescripcionLayout = {
		xtype: 	'box',
		id:		'espaciadorPanelDescripcionLayout',
		hidden:	true,
		height: 	10,
		width: 	800
	}
	
	//------------------------- 6. PANEL RESULTADOS VALIDACION ---------------------------
 
	var procesarSuccessFailureExtraerErroresValidacion = function(opts, success, response){
		
		var botonExtraerErroresValidacion = Ext.getCmp('botonExtraerErroresValidacion');
		botonExtraerErroresValidacion.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del JSON puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			botonExtraerErroresValidacion.setIconClass('icoXls');
			botonExtraerErroresValidacion.setText('Descargar Errores');
			
			botonExtraerErroresValidacion.urlArchivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			botonExtraerErroresValidacion.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
 
			if(Ext.isIE7){ // Nota: debido a un bug en el width en IE7, se vuelve a realizar el layout de los componentes
				Ext.getCmp('botonesResultadosValidacion').doLayout();
			}
			
		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						botonExtraerErroresValidacion.setIconClass('icoGenerarXls');
						botonExtraerErroresValidacion.setText('Extraer Errores');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				botonExtraerErroresValidacion.setIconClass('icoGenerarXls');
				botonExtraerErroresValidacion.setText('Extraer Errores');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
 
	}
	
	var procesarConsultaRegistrosSinErrores 	= function(store, registros, opts){
		
		var gridRegistrosSinErrores 						= Ext.getCmp('gridRegistrosSinErrores');
		
		if (registros != null) {
			
			var el 							= gridRegistrosSinErrores.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var procesarConsultaRegistrosConErrores = function(store, registros, opts){
		
		var gridRegistrosConErrores						= Ext.getCmp('gridRegistrosConErrores');
		
		if (registros != null) {
			
			var el 							= gridRegistrosConErrores.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var procesarConsultaErroresVsCifrasControl = function(store, registros, opts){
		
		var gridErroresVsCifrasControl = Ext.getCmp('gridErroresVsCifrasControl');
		
		if (registros != null) {
			
			var el 							= gridErroresVsCifrasControl.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var registrosSinErroresData = new Ext.data.ArrayStore({
			
		storeId: 'registrosSinErroresDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'NUMERO_LINEA',   			mapping: 0, type: 'int' },
			{ name: 'CLAVE_IF_SIAG', 			mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosSinErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosSinErrores(null, null, null);						
				}
			}
		}
		
	});
	
	var registrosConErroresData = new Ext.data.ArrayStore({
			
		storeId: 'registrosConErroresDataStore',
		fields:  [
			{ name: 'NUMERO_LINEA',   	mapping: 0, type: 'int' },
			{ name: 'DESCRIPCION', 		mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosConErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosConErrores(null, null, null);						
				}
			}
		}
		
	});
	
	var erroresVsCifrasControlData  = new Ext.data.ArrayStore({
			
		storeId: 'erroresVsCifrasControlDataStore',
		fields:  [
			{ name: 'NUMERO_LINEA',   	mapping: 0, type: 'int' },
			{ name: 'DESCRIPCION', 		mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaErroresVsCifrasControl,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaErroresVsCifrasControl(null, null, null);						
				}
			}
		}
		
	});

	var elementosResultadosValidacion = [
		{ 
			xtype: 			'tabpanel',
			id:				'tabPanelResultadosValidacion',
			activeTab:		0,
			plain:			true,
			height: 			400,
			//bodyStyle:		'padding: 10px;',
			items: [
				{
					xtype:		'container',
					title: 		'Registros sin Errores',
					/*layout:		'vbox',
               layoutConfig: {
						align:   'stretch',
						pack:    'start'
					},*/
					items: [
						{
							store: 		registrosSinErroresData,
							xtype: 		'grid',
							id:			'gridRegistrosSinErrores',
							stripeRows: true,
							loadMask: 	true,	
							frame: 		false,
							height:		330,
							flex:			1,
							columns: [
								{
									header: 		'Linea',
									tooltip: 	'Linea',
									dataIndex: 	'NUMERO_LINEA',
									sortable: 	true,
									resizable: 	true,
									width: 		50,
									hidden: 		true,
									hideable:	false
								},
								{
									header: 		'Clave de Intermediario Financiero',
									tooltip: 	'Clave de Intermediario Financiero',
									dataIndex: 	'CLAVE_IF_SIAG',
									sortable: 	true,
									resizable: 	true,
									width: 		675,
									hidden: 		false
								}
							]					
						},
						{
							xtype: 		'container',
							layout:		'form',
							labelWidth: 200,
							style:		'padding:8px;',
							height:		70,
							//width:		400,
							items: [
								// TEXTFIELD Total de Registros
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Total de Registros',
									name: 			'numeroRegistrosSinErrores',
									id: 				'numeroRegistrosSinErrores',
									readOnly:		true,
									hidden: 			false,
									//maxLength: 		6, // Maximo 6 	
									msgTarget: 		'side',
									anchor:			'-220',
									style: {
										textAlign: 'right'
									}
								},
								// TEXTFIELD Sumatoria montos
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Comisi�n Mensual', // #TAG
									name: 			'sumatoriaComisionMensualSinErrores',
									id: 				'sumatoriaComisionMensualSinErrores',
									readOnly:		true,
									hidden: 			false,
									// maxLength: 	 28, // Maximo 18 digitos, un punto y dos decimales	+ "$ "
									msgTarget: 		'side',
									anchor:			'-220',
									style: {
									  textAlign: 'right'
									  //, width:		 '100%'
								  }
								}
							]
						}
					]
				},
				{
					xtype:		'container',
					title: 		'Registros con Errores',
					/*layout:		'vbox',
               layoutConfig: {
						align:   'stretch',
						pack:    'start'
					},*/
					items: [
						{
							store: 		registrosConErroresData,
							xtype: 		'grid',
							id:			'gridRegistrosConErrores',
							stripeRows: true,
							loadMask: 	true,	
							frame: 		false,
							height:		330,
							flex:			1,
							columns: [
								{
									header: 		'Linea',
									tooltip: 	'Linea',
									dataIndex: 	'NUMERO_LINEA',
									sortable: 	true,
									resizable: 	true,
									width: 		50,
									hidden: 		true,
									hideable:	false
								},
								{
									header: 		'Descripci�n',
									tooltip: 	'Descripci�n',
									dataIndex: 	'DESCRIPCION',
									sortable: 	true,
									resizable: 	true,
									width: 		1150,
									hidden: 		false
								}
							]
						},
						{
							xtype: 		'container',
							layout:		'form',
							labelWidth: 200,
							style:		"padding:8px;",
							height:		70,
							//width:		400,
							items: [
								// TEXTFIELD Total de Registros
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Total de Registros',
									name: 			'numeroRegistrosConErrores',
									id: 				'numeroRegistrosConErrores',
									readOnly:		true,
									hidden: 			false,
									//maxLength: 		6,	// Maximo 6
									msgTarget: 		'side',
									anchor:			'-220',
									style: {
									  textAlign: 'right'
								   }
								},
								// TEXTFIELD Sumatoria montos
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Comisi�n Mensual', // #TAG
									name: 			'sumatoriaComisionMensualConErrores',
									id: 				'sumatoriaComisionMensualConErrores',
									readOnly:		true,
									hidden: 			false,
									//maxLength: 		28,	 // Maximo 18 digitos, un punto y dos decimales	+ "$ "
									msgTarget: 		'side',
									anchor:			'-220',
									style: {
									  textAlign: 'right'
								   }
								}
							]
						}
					]
				},
				{
					title: 		'Errores vs. Cifras de Control',
					store: 		erroresVsCifrasControlData,
					xtype: 		'grid',
					id:			'gridErroresVsCifrasControl',
					stripeRows: true,
					loadMask: 	true,
					columns: [
						{
							header: 		'Linea',
							tooltip: 	'Linea',
							dataIndex: 	'NUMERO_LINEA',
							sortable: 	true,
							resizable: 	true,
							width: 		50,
							hidden: 		true,
							hideable:	false
						},
						{
							header: 		'Descripci�n',
							tooltip: 	'Descripci�n',
							dataIndex: 	'DESCRIPCION',
							sortable: 	true,
							resizable: 	true,
							width: 		1150,
							hidden: 		false
						}
					]
				}
			]
		},
		{
			xtype: 			'container',
			id:				'botonesResultadosValidacion',
			anchor: 			'100%',
			style: 			'margin: 0px;padding-top:15px;',
			renderHidden: 	false,
			layout: {
				type: 		'hbox',
				pack: 		'center'
			},
			items: [
				{
					xtype: 		'button',
					minWidth: 	128,				// Nota: Se usa minWidth debido a un raro bug en IE7 que ocasiona no se actualicen las posiciones de los botones
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonCancelarInsercion',
					margins: 	' 3',
					handler:    function(boton, evento) {
						cargaArchivo("FIN",null);
					}
				},
				{
					xtype: 		'button',
					minWidth: 		128,			// Nota: Se usa minWidth debido a un raro bug en IE7 que ocasiona no se actualicen las posiciones de los botones
					text: 		'Extraer Errores',
					iconCls: 	'icoGenerarXls',
					id: 			'botonExtraerErroresValidacion',
					margins: 	' 3',
					urlArchivo: '',
					handler:    function(boton, evento) {
 
						if(Ext.isEmpty(boton.urlArchivo)){
							
							// Cambiar icono
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							// Extraer detalle de los Errores vs. Cifras de Control
							var erroresVsCifrasControlData		= Ext.StoreMgr.key('erroresVsCifrasControlDataStore');
							var detalleErroresVsCifrasControl	= new Array(); 
							
							erroresVsCifrasControlData.each( function(record){
								detalleErroresVsCifrasControl.push(record.data['DESCRIPCION']);
							}); 
							
							// Extraer detalle de los Registros con Errores
							var registrosConErroresData 			= Ext.StoreMgr.key('registrosConErroresDataStore');
							var detalleRegistrosConErrores		= new Array();
							
							registrosConErroresData.each( function(record){
								detalleRegistrosConErrores.push(record.data['DESCRIPCION']);
							});
 
							// Genera Archivo CSV
							Ext.Ajax.request({
								url: 			'29CargaArchivoComisionesCobradas01ext.data.jsp',
								params: 		{ 
									informacion: 		'ExtraerErroresValidacion',
									detalleErroresVsCifrasControl:	Ext.encode(detalleErroresVsCifrasControl),
									detalleRegistrosConErrores:		Ext.encode(detalleRegistrosConErrores)
								},
								callback: 	procesarSuccessFailureExtraerErroresValidacion
							});
							
						// Descargar el archivo generado	
						} else {
							
							var forma    = Ext.getDom('formAux');
							forma.action = NE.appWebContextRoot+'/DescargaArchivo';
							forma.method = 'post';
							forma.target = '_self';
							// Preparar Archivo a Descargar
							var archivo  = boton.urlArchivo;				
							archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
							// Insertar archivo
							var inputNombreArchivo = Ext.DomHelper.insertFirst(
								forma, 
								{ 
									tag: 	'input', 
									type: 'hidden', 
									id: 	'nombreArchivo', 
									name: 'nombreArchivo', 
									value: archivo
								},
								true
							); 
							// Solicitar Archivo Al Servidor
							forma.submit();
							// Remover nodo agregado
							inputNombreArchivo.remove();
							
						}
						
					}
				},
				{
					xtype: 		'button',
					minWidth: 	128,				// Nota: Se usa minWidth debido a un raro bug en IE7 que ocasiona no se actualicen las posiciones de los botones
					text: 		'Continuar Carga',
					iconCls: 	'icoContinuar',
					id: 			'botonContinuarCarga',
					margins: 	' 3',
					handler:    function(boton, evento) {
						cargaArchivo("PRESENTAR_PREACUSE",null);
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.claveProceso',
        id: 	'panelResultadosValidacion.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.totalRegistros',
        id: 	'panelResultadosValidacion.totalRegistros'
      },
      // INPUT HIDDEN: MONTO TOTAL
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.montoTotal',
        id: 	'panelResultadosValidacion.montoTotal'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.numeroRegistros',
        id: 	'panelResultadosValidacion.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE MONTOS DE REINSTALACION
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.sumatoriaComisionMensual',
        id: 	'panelResultadosValidacion.sumatoriaComisionMensual'
      },
      // INPUT HIDDEN: CLAVE DEL MES A CONCILIAR
      {
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.claveMesConciliar',
        id: 	'panelResultadosValidacion.claveMesConciliar'
      },
      // INPUT HIDDEN: NOMBRE DEL ARCHIVO ZIP
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.fileName',
        id: 	'panelResultadosValidacion.fileName'
      }
	];
	
	var panelResultadosValidacion = {
		title:			'Resultado de la Validaci�n',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosValidacion',
		width: 			700, //949,
		frame: 			true,
		style: 			'margin:  0 auto',
		bodyStyle:		'padding: 10px',
		items: 			elementosResultadosValidacion,
		layout:			'anchor'
	};

	var espaciadorPanelResultadosValidacion = {
		xtype: 	'box',
		id:		'espaciadorPanelResultadosValidacion',
		hidden:	true,
		height: 	7,
		width: 	800
	}
	
	// ---------------------------- 5. PANEL AVANCE VALIDACION ------------------------------
	
	var formateaPorcentajeAvance = function(porcentajeAvance){
		var porcentajeFormateado  = Ext.util.Format.number( Number(porcentajeAvance), "0.00" ) + " %";
		return porcentajeFormateado;
	}
	
	var elementosAvanceValidacion = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
			text: 	'Validando Registros...'
		},
		{
			xtype: 'progress',
			id:	 'barrarAvanceValidacion',
			name:	 'barrarAvanceValidacion',
			cls:	 'center-align',
			value: 0.3756,
			text:  '37.56 %'
		},
		{
			xtype: 'panel',
			layout: 'hbox',
			border:			false,
			baseCls:			'x-plain',
			layoutConfig: {
				padding:    '5 0 0 0',
				pack:			'start',
				align:		'middle'
			},
			defaults:{
				margins:'0 5 0 0'
			},
			items: [
				{
					xtype: 	'label',
					width: 	'130',
					cls: 		'x-form-item',
					text: 	'Registros Procesados:'
				},
				{
					xtype: 	'label',
					id:		'valorRegistrosProcesados',
					name:		'valorRegistrosProcesados',
					cls: 		'x-form-item',
					style:	{
						textAlign: 'right'
					}
				}
			]
		},
		{
			xtype: 'panel',
			layout: 'hbox',
			border:			false,
			baseCls:			'x-plain',
			layoutConfig: {
				padding:    '0 0 0 0',
				pack:			'start',
				align:		'middle'
			},
			defaults:{
				margins:'0 5 0 0'
			},
			items: [
				{
					xtype: 	'label',
					width: 	'130',
					cls: 		'x-form-item',
					text: 	'Total de Registros:'
				},
				{
					xtype: 	'label',
					id:		'valorTotalRegistros',
					name:		'valorTotalRegistros',
					cls: 		'x-form-item'
				}
			]
		}
	];
	
	var panelAvanceValidacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelAvanceValidacion',
		frame: 			false,
		border:			false,
		collapsible: 	false,
		titleCollapse: false,
		bodyStyle:		'padding: 20px',
		baseCls:			'x-plain',
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		items: 			elementosAvanceValidacion
	});
	
	var hideWindowAvanceValidacion = function(){
		
		var ventanaAvanceValidacion = Ext.getCmp('windowAvanceValidacion');
		
		if(ventanaAvanceValidacion && ventanaAvanceValidacion.isVisible() ){
			var barrarAvanceValidacion	 = Ext.getCmp("barrarAvanceValidacion");
			barrarAvanceValidacion.reset(); 
 
			if (ventanaAvanceValidacion) {
				ventanaAvanceValidacion.hide();
			}
      }
      
	}
	
	var showWindowAvanceValidacion = function(numeroRegistros){
		
		var barrarAvanceValidacion		= Ext.getCmp("barrarAvanceValidacion");
		var valorRegistrosProcesados	= Ext.getCmp("valorRegistrosProcesados");
		var valorTotalRegistros			= Ext.getCmp("valorTotalRegistros");
 
		barrarAvanceValidacion.updateProgress(0.00,"0.00 %",true);
		valorTotalRegistros.setText(numeroRegistros); 
		valorRegistrosProcesados.setText("0");
			
		var ventana = Ext.getCmp('windowAvanceValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				resizable:		false,
				id: 				'windowAvanceValidacion',
				modal:			true,
				closable: 		false,
				width:			Ext.isIE7?500:undefined,
				height: 			Ext.isIE7?144:undefined,
				listeners: 		{
					show: function(component){
						var valorRegistrosProcesados	= Ext.getCmp("valorRegistrosProcesados");
						var valorTotalRegistros			= Ext.getCmp("valorTotalRegistros");
						valorRegistrosProcesados.setWidth(valorTotalRegistros.getWidth());
					}
				},
				items: [
					Ext.getCmp("panelAvanceValidacion")
				]
			}).show();
			
		}
		
	}

	// ----------------------------- 4. PANEL PREPARAR VALIDACION ------------------------------

	var elementosPrepararValidacion = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
			text: 	'Preparando Validaci�n...'
		},
		{
			xtype: 'progress',
			id:	 'barrarPrepararValidacion',
			name:	 'barrarPrepararValidacion',
			cls:	 'center-align',
			value: 0.00,
			text:  '0.00%'
		}
	];
	
	var panelPrepararValidacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelPrepararValidacion',
		frame: 			true,
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		items: 			elementosPrepararValidacion
	});
	
	var showWindowPrepararValidacion = function(){
		
		var barrarPrepararValidacion		= Ext.getCmp("barrarPrepararValidacion");
 
		barrarPrepararValidacion.updateProgress(0.00,"0.00 %",true);
		barrarPrepararValidacion.wait({
            interval:	200,
            increment:	15
      });
 
		var ventana = Ext.getCmp('windowPrepararValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				width:			300,
				resizable:		false,
				id: 				'windowPrepararValidacion',
				modal:			true,
				closable: 		false,
				items: [
					Ext.getCmp("panelPrepararValidacion")
				]
			}).show();
			
		}
		
	}	

	var hideWindowPrepararValidacion = function(){
		
		var ventanaPrepararValidacion = Ext.getCmp('windowPrepararValidacion');
		
		if(ventanaPrepararValidacion && ventanaPrepararValidacion.isVisible() ){
			
			var barrarPrepararValidacion	 = Ext.getCmp("barrarPrepararValidacion");
			barrarPrepararValidacion.reset(); 

			if (ventanaPrepararValidacion) {
				ventanaPrepararValidacion.hide();
			}
			
      }
      
	}
 
	//-------------------------- 3. FORMA DE CARGA DE ARCHIVO ---------------------------
 
	var elementosFormaCargaArchivo = [
		{
		  xtype: 		'fileuploadfield',
		  id: 			'archivo',
		  name: 			'archivo',
		  emptyText: 	'Seleccione...',
		  fieldLabel: 	"Ruta del Archivo de Registros", 
		  buttonText: 	null,
		  buttonCfg: {
			  iconCls: 	'upload-icon'
		  },
		  anchor: 		'-20'
		  //vtype: 		'archivotxt'
		},
		{
			xtype: 		'panel',
			anchor: 		'100%',
			style: {
				marginTop: 		'10px'
			},
			layout: {
				type: 'hbox',
				pack: 'end',
				align: 'middle'
			},
			items: [
				{
					xtype: 			'button',
					text: 			'Continuar',
					iconCls: 		'icoContinuar',
					id: 				'botonContinuarCargaArchivo',				
					handler:    function(boton, evento) {
						
						// Revisar si la forma es invalida
						var forma = Ext.getCmp("panelFormaCargaArchivo").getForm();
						if(!forma.isValid()){
							return;
						}
	 
						// Validar que se haya especificado un archivo
						var archivo = Ext.getCmp("archivo");
						if( Ext.isEmpty( archivo.getValue() ) ){
							archivo.markInvalid("Debe especificar un archivo");
							return;
						}
						
						// Revisar el tipo de extension del archivo
						var myRegexZIP = /^.+\.([zZ][iI][pP])$/;
						
						var nombreArchivo 	= archivo.getValue();
						var numeroRegistros 	= Ext.getCmp("numeroRegistros1").getValue();
						if( !myRegexZIP.test(nombreArchivo) ){
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n zip.");
							return;
						}
						
						// Cargar archivo
						cargaArchivo("SUBIR_ARCHIVO",null);

					}
				}
			]
		},
		{
			xtype: 	'label',
			anchor:  '100%',
			html: 	"<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" width=\"100%\" > "  +
						"   <tr>"  +
						"	    <td class=\"celda01\" align=\"left\" colspan=\"10\"  style=\"height:30px;\" >"  +
						"         El archivo deber� convertirlo a formato .ZIP. El nombre del TXT no deber� tener acentos, di�resis o � antes de iniciar la carga. Este proceso s�lo admitir� una carga por concepto y por mes. "  +
						"	    </td>"  +
						"   </tr>"  +
						"</table>" ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginTop: 		'20px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		},
		{
			xtype: 	'hidden',
			name:		'numeroRegistros1',
			id:		'numeroRegistros1'
		},
		{
			xtype: 	'hidden',
			name:		'sumatoriaComisionMensual1',
			id:		'sumatoriaComisionMensual1'
		},
		{
			xtype: 	'hidden',
			name:		'claveMesConciliar1',
			id:		'claveMesConciliar1'
		}
	];
	
	var panelFormaCargaArchivo = new Ext.form.FormPanel({
		id: 				'panelFormaCargaArchivo',
		width: 			700,
		title: 			'Carga de Archivo',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		fileUpload: 	true,
		hidden:			true,
		renderHidden:	true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	190,
		defaultType: 	'textfield',
		layoutConfig: {
			fieldTpl: new Ext.XTemplate(
				'<tpl if="id==\'archivo\'">',
           		 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">',
						  		'<a class="x-tool x-tool-ayudaLayout" onfocus="blur()" style="float:left;" onclick="javascript:showPanelDescripcionLayout();"></a>&nbsp;',
						  		'{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
						  '</div>',
					 '</div>',
			  '</tpl>',
			  '<tpl if="id!=\'archivo\'">',
					 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
					 '</div>',
			  '</tpl>'
		   )
		},
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementosFormaCargaArchivo,
		monitorValid: 	false
	});
 
	//------------------------------- 2. PANEL SOLICITUD DE CARGA --------------------------------
 
	var elementosFormaSolicitudReinstalacion = [
		// DISPLAYFIELD MES
		{
			xtype: 		'displayfield', 
			id: 			'nombreMesConciliar', 
			name: 		'nombreMesConciliar', 
			fieldLabel: 'Mes', 
			value: 		'',
			style:		'text-align:right;'
		},
		// NUMBERFIELD NUMERO DE REGISTROS EN LA SOLICITUD
		{ 
			xtype: 			'bigdecimal',
			name: 			'numeroRegistros',
			id: 				'numeroRegistros',
			allowDecimals: false,
			allowNegative: false,
			fieldLabel: 	'N�mero de registros',
			blankText:		'Favor de capturar el n�mero de registros',
			allowBlank: 	false,
			hidden: 			false,
			maxLength: 		11,	
			msgTarget: 		'side',
			anchor:			'-20',
			maxValue: 		'999,999,999',
			format:			'0,000'
		},
		// NUMBERFIELD SUMATORIA DE MONTOS DE REINSTALACION
		{ 
			xtype: 			'bigdecimal',
			name: 			'sumatoriaComisionMensual',// #TAG
			id: 				'sumatoriaComisionMensual',
			fieldLabel:		'Comisi�n Mensual',// #TAG
			blankText:		'Favor de capturar la sumatoria de comisiones', // #TAG
			allowBlank:		false,
			allowNegative: false,
			hidden: 			false,
			maxLength: 		22,	
			msgTarget: 		'side',
			anchor:			'-20', //necesario para mostrar el icono de error
			maxValue: 		'999,999,999,999,999.99',
			format:			'0,000.00'
		},
		// INPUT HIDDEN CLAVE MES CONCILIAR
		{
			xtype: 		'hidden', 
			id: 			'claveMesConciliar', 
			name: 		'claveMesConciliar'
		}
	];
	
	var panelFormaSolicitudConciliacion 		= new Ext.form.FormPanel({
		id: 					'panelFormaSolicitudConciliacion',
		width: 				700,
		title: 				'Mes a Conciliar',
		frame: 				true,
		hidden:				true,
		collapsible: 		false,
		titleCollapse: 	false,
		trackResetOnLoad: true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		renderHidden:		true, // Nota: no usar hidden: true -> hara que los componentes no se muestren del tamanio correcto
		labelWidth: 		400,
		defaultType: 		'textfield',
		items: 				elementosFormaSolicitudReinstalacion,
		monitorValid: 		false,
		buttons: [
			{
				id:		'botonEnviar',
				text: 	'Enviar',
				hidden: 	false,
				iconCls: 'icoContinuar',
				handler: function() {
					
					// Cancelar validaciones adicionales si alguno de los campos de la forma
					// viene vacio
					var formaSolicitudConciliacion = Ext.getCmp("panelFormaSolicitudConciliacion").getForm();
					if( !formaSolicitudConciliacion.isValid() ){
						return;
					}

					// Enviar solicitud
					respuesta = formaSolicitudConciliacion.getValues();
					cargaArchivo("ENVIAR_SOLICITUD_CONCILIACION", respuesta);
					 
				}
				
			},
			// BOTON LIMPIAR
			{
				text: 	'Limpiar',
				hidden: 	false,
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('contenedorPrincipal').doLayout(true,true);
					Ext.getCmp('panelFormaSolicitudConciliacion').getForm().reset();
				}
			}
		]
	});
 
	//------------------------------------ 1. PANEL AVISOS ---------------------------------------
	
	var elementosPanelAvisos = [
		{
			xtype: 	'label',
			id:	 	'labelAviso',
			cls:		'x-form-item',
			style: 	'font-weight:normal;text-align:center;margin:15px;color:black;',
			html:  	''
		}
	];
	
	var panelAvisos = {
		xtype:			'panel',
		id: 				'panelAvisos',
		hidden:			true,
		width: 			700,
		title: 			'Avisos',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosPanelAvisos
	}
	
	//--------------------------------- 0. BOX DE INICIALIZACION ---------------------------------------
	
	var boxInicializacion = [
		{
			xtype: 		'box',
			id: 			'contenedorInicializacion',
			cls:			'realizandoActividad',
			style: 		'margin: 0 auto',
			width: 		16,
			height: 		16
		}
	];
	
	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			boxInicializacion,
			panelAvisos,
			panelFormaSolicitudConciliacion,
			panelFormaCargaArchivo,
			espaciadorPanelResultadosValidacion,
			panelResultadosValidacion,
			espaciadorPanelDescripcionLayout,
			panelDescripcionLayout,
			panelPreacuseCargaArchivo,
			panelAcuseCargaArchivo
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	cargaArchivo("INICIALIZACION",null);

});;