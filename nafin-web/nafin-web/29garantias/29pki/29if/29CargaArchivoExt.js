var showPanelLayoutDeCarga;

Ext.onReady(function() {
	
	//-------------------------------- VALIDACIONES -----------------------------------
 
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaCargaArchivo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaArchivo(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaArchivo(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelFormaSolicitudDeCarga").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceValidacion();
         
         // Ocultar mascara del panel de resultados de la validacion
         element = Ext.getCmp("panelResultadosValidacion").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Ocultar mascara del panel de preacuse
			element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var cargaArchivo = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"					){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29CargaArchivoExt.data.jsp',
				params: 	{
					informacion:		'CargaArchivo.inicializacion'
				},
				callback: 				procesaCargaArchivo
			});
			
		} else if(			estado == "MOSTRAR_AVISO"								){

			if(!Ext.isEmpty(respuesta.aviso)){
				
				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);
				
				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();
				
			}
			
		} else if(			estado == "ESPERAR_CAPTURA_SOLICITUD_DE_CARGA"	){
 
			// Cargar Contenido del Combo A�o
			var catalogoMesData      = Ext.StoreMgr.key('catalogoMesDataStore');
			catalogoMesData.load();
			
			// Limpiar contenido del numberfield
			Ext.getCmp('numeroRegistros').setValue("");
			// Limpiar contenido del numberfield
			Ext.getCmp('sumatoriaSaldosFinMes').setValue("");
			
			var panelFormaSolicitudDeCarga = Ext.getCmp("panelFormaSolicitudDeCarga");
			panelFormaSolicitudDeCarga.getForm().clearInvalid();
			panelFormaSolicitudDeCarga.show();
		
			
			

			
		} else if(			estado == "ENVIAR_SOLICITUD_DE_CARGA" 				){
			
			Ext.getCmp("panelFormaSolicitudDeCarga").el.mask("Enviando...",'x-mask-loading');
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29CargaArchivoExt.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CargaArchivo.enviarSolicitudDeCarga'
					},
					respuesta
				),
				callback: 				procesaCargaArchivo
			});
 
		} else if(			estado == "ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES"	){
			
			// Ocultar panel con la solicitud de carga
			var panelFormaSolicitudDeCarga = Ext.getCmp("panelFormaSolicitudDeCarga");
			panelFormaSolicitudDeCarga.el.unmask();
			panelFormaSolicitudDeCarga.hide();
 
			// Esperar decision
			Ext.getCmp("claveMes1").setValue(respuesta.claveMes);
			Ext.getCmp("numeroRegistros1").setValue(respuesta.numeroRegistros);
			Ext.getCmp("sumatoriaSaldosFinMes1").setValue(respuesta.sumatoriaSaldosFinMes);
 
			// Mostrar panel de carga de archivo
			var panelFormaCargaArchivo = Ext.getCmp("panelFormaCargaArchivo");
			panelFormaCargaArchivo.show();
			
		} else if(			estado == "SUBIR_ARCHIVO"								){
			
			Ext.getCmp("panelFormaCargaArchivo").getForm().submit({
				clientValidation: 	true,
				url: 						'29CargaArchivoExt.data.jsp?informacion=CargaArchivo.subirArchivo',
				waitMsg:   				'Subiendo archivo...',
				waitTitle: 				'Por favor espere',
				success: function(form, action) {
					 procesaCargaArchivo(null,  true,  action.response );
				},
				failure: function(form, action) {
				 	 action.response.status = 200;
					 procesaCargaArchivo(null,  false, action.response );
				}
			});
			
		} else if(			estado == "REALIZAR_VALIDACION"         			){
 
			showWindowAvanceValidacion();
			
			Ext.Ajax.request({
				url: 		'29CargaArchivoExt.data.jsp',
				params: 	{
					informacion:								'CargaArchivo.realizarValidacion',
					claveMes: 									respuesta.claveMes,
					numeroRegistros: 							respuesta.numeroRegistros,
					sumatoriaSaldosFinMes: 					respuesta.sumatoriaSaldosFinMes,
					fileName:									respuesta.fileName
				},
				callback: 										procesaCargaArchivo
			});
		
		} else if(  estado == "ESPERAR_DECISION_VALIDACION" ){
 
			var claveMes 												= respuesta.claveMes;
			var numeroRegistros 										= respuesta.numeroRegistros;
			var sumatoriaSaldosFinMes 								= respuesta.sumatoriaSaldosFinMes;
			
			var claveProceso 											= respuesta.claveProceso;
			var totalRegistros 										= respuesta.totalRegistros;
			var montoTotal 											= respuesta.montoTotal;      // Debug info: por el momento no se usa
			var montoTotalPagar 										= respuesta.montoTotalPagar; // Debug info: por el momento no se usa
			
			var numeroRegistrosSinErrores 						= respuesta.numeroRegistrosSinErrores;
			var sumatoriaSaldosFinMesSinErrores 				= respuesta.sumatoriaSaldosFinMesSinErrores;
			
			var numeroRegistrosConErrores 						= respuesta.numeroRegistrosConErrores;
			var sumatoriaSaldosFinMesConErrores 				= respuesta.sumatoriaSaldosFinMesConErrores;
 
			var continuarCarga 									= respuesta.continuarCarga;
 
			// SETUP PARAMETROS VALIDACION
			// Recordar numero de proceso y el total de registros
         Ext.getCmp("panelResultadosValidacion.claveProceso").setValue(claveProceso);
			Ext.getCmp("panelResultadosValidacion.totalRegistros").setValue(totalRegistros);
			Ext.getCmp("panelResultadosValidacion.montoTotal").setValue(montoTotal);
			Ext.getCmp("panelResultadosValidacion.montoTotalPagar").setValue(montoTotalPagar);
			
         Ext.getCmp("panelResultadosValidacion.claveMes").setValue(claveMes);
         Ext.getCmp("panelResultadosValidacion.numeroRegistros").setValue(numeroRegistros);
         Ext.getCmp("panelResultadosValidacion.sumatoriaSaldosFinMes").setValue(sumatoriaSaldosFinMes);
 
         // Inicializar panel de resultados
			Ext.getCmp("numeroRegistrosSinErrores").setValue(numeroRegistrosSinErrores);
			Ext.getCmp("sumatoriaSaldosFinMesSinErrores").setValue(sumatoriaSaldosFinMesSinErrores);
			Ext.getCmp("numeroRegistrosConErrores").setValue(numeroRegistrosConErrores);
			Ext.getCmp("sumatoriaSaldosFinMesConErrores").setValue(sumatoriaSaldosFinMesConErrores);
 
			// Determinar si se mostrar� el bot�n de continuar carga
			if(continuarCarga){
         	Ext.getCmp('botonContinuarCarga').enable();
         }else{
         	Ext.getCmp('botonContinuarCarga').disable();
         }
         
			// MOSTRAR COMPONENTES
			// Resetear campo de carga de archivo
			Ext.getCmp("archivo").reset();
			
			// Ocultar ventana de avance validacion
			hideWindowAvanceValidacion();
			
			 // Remover contenido anterior de los grids
         var registrosSinErroresData 		= Ext.StoreMgr.key('registrosSinErroresDataStore');
         var registrosConErroresData 		= Ext.StoreMgr.key('registrosConErroresDataStore');
         var erroresVsCifrasControlData 	= Ext.StoreMgr.key('erroresVsCifrasControlDataStore');
         //registrosSinErroresData.removeAll();
         //registrosConErroresData.removeAll();
         //erroresVsCifrasControlData.removeAll();
 
         // Mostrar espaciador del Panel de resultados
			Ext.getCmp("espaciadorPanelResultadosValidacion").show();
			// Mostrar Panel de resultados
         Ext.getCmp('panelResultadosValidacion').show();

         // Mostrar Panel de Resultados de la validacion
         var tabPanelResultadosValidacion = Ext.getCmp('tabPanelResultadosValidacion');
         tabPanelResultadosValidacion.setActiveTab(2);
         erroresVsCifrasControlData.loadData(respuesta.erroresVsCifrasControlDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
         tabPanelResultadosValidacion.setActiveTab(1);
         registrosConErroresData.loadData(respuesta.registrosConErroresDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
         tabPanelResultadosValidacion.setActiveTab(0);
         registrosSinErroresData.loadData(respuesta.registrosSinErroresDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
		   
      } else if(  estado == "PRESENTAR_PREACUSE"         ){
      	
      	Ext.getCmp("panelResultadosValidacion").el.mask("Generando Pre-Acuse...","x-mask-loading");
      	
      	Ext.Ajax.request({
				url: 		'29CargaArchivoExt.data.jsp',
				params: 	{
					informacion:								'CargaArchivo.presentarPreacuse',
					claveProceso: 								Ext.getCmp("panelResultadosValidacion.claveProceso").getValue(),
					totalRegistros: 							Ext.getCmp("panelResultadosValidacion.totalRegistros").getValue(),
					montoTotal: 								Ext.getCmp("panelResultadosValidacion.montoTotal").getValue(),
					montoTotalPagar: 							Ext.getCmp("panelResultadosValidacion.montoTotalPagar").getValue(),
					claveMes: 									Ext.getCmp("panelResultadosValidacion.claveMes").getValue(),
					numeroRegistros: 							Ext.getCmp("panelResultadosValidacion.numeroRegistros").getValue(),
					sumatoriaSaldosFinMes: 					Ext.getCmp("panelResultadosValidacion.sumatoriaSaldosFinMes").getValue()
				},
				callback: 										procesaCargaArchivo
			});
      	
		} else if(  estado == "ESPERAR_DECISION_PREACUSE"         ){
			
			var claveProceso 								= respuesta.claveProceso;
			var totalRegistros 							= respuesta.totalRegistros;
			var montoTotal 								= respuesta.montoTotal;
			var montoTotalPagar 							= respuesta.montoTotalPagar;
			
			var claveMes 									= respuesta.claveMes;
			var numeroRegistros 							= respuesta.numeroRegistros;
			var sumatoriaSaldosFinMes 					= respuesta.sumatoriaSaldosFinMes;
			
			// Establecer los valores de los parametros de la carga
			Ext.getCmp("panelPreacuseCargaArchivo.claveProceso").setValue(claveProceso);
			Ext.getCmp("panelPreacuseCargaArchivo.totalRegistros").setValue(totalRegistros);
			Ext.getCmp("panelPreacuseCargaArchivo.montoTotal").setValue(montoTotal);
			Ext.getCmp("panelPreacuseCargaArchivo.montoTotalPagar").setValue(montoTotalPagar);
			
			Ext.getCmp("panelPreacuseCargaArchivo.claveMes").setValue(claveMes);
			Ext.getCmp("panelPreacuseCargaArchivo.numeroRegistros").setValue(numeroRegistros);
			Ext.getCmp("panelPreacuseCargaArchivo.sumatoriaSaldosFinMes").setValue(sumatoriaSaldosFinMes);
 
			// Suprimir Mascara
			Ext.getCmp("panelResultadosValidacion").el.unmask();
			
			// Ocultar panel de Carga
			Ext.getCmp("panelFormaCargaArchivo").hide();
			// Ocultar espaciador del panel de resultados
			Ext.getCmp("espaciadorPanelResultadosValidacion").hide();
			// Ocultar panel de resultados
			Ext.getCmp("panelResultadosValidacion").hide();
			// Ocultar espaciador del layout
			Ext.getCmp("espaciadorPanelLayoutDeCarga").hide();
			// Ocultar panel con el layout de carga
			Ext.getCmp("panelLayoutDeCarga").hide();
			
			// Mostrar panel de preacuse
			Ext.getCmp("panelPreacuseCargaArchivo").show();
			
			// Cargar resultados
			Ext.StoreMgr.key('registrosPorAgregarDataStore').loadData(respuesta.registrosPorAgregarDataArray);
			
      } else if(  estado == "ESPERAR_DECISION"  			){
      	
      	// Ocultar mascara del panel de preacuse
			var element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
      	return;
      	
		} else if(  estado == "TRANSMITIR_REGISTROS" 		){
			
			var claveProceso							= Ext.getCmp("panelPreacuseCargaArchivo.claveProceso").getValue();
			var totalRegistros						= Ext.getCmp("panelPreacuseCargaArchivo.totalRegistros").getValue();
			var montoTotal								= Ext.getCmp("panelPreacuseCargaArchivo.montoTotal").getValue();
			var montoTotalPagar						= Ext.getCmp("panelPreacuseCargaArchivo.montoTotalPagar").getValue();
			
			var claveMes								= Ext.getCmp("panelPreacuseCargaArchivo.claveMes").getValue();
			var numeroRegistros						= Ext.getCmp("panelPreacuseCargaArchivo.numeroRegistros").getValue();
			var sumatoriaSaldosFinMes				= Ext.getCmp("panelPreacuseCargaArchivo.sumatoriaSaldosFinMes").getValue();
 
			// Agregar mensaje transmitiendo solicitudes
			Ext.getCmp("panelPreacuseCargaArchivo").el.mask("Transmitiendo solicitudes...","x-mask-loading");
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 												'29CargaArchivoExt.data.jsp',
				params: {	
					informacion:								'CargaArchivo.transmitirRegistros',
					claveProceso:								claveProceso,
					totalRegistros:							totalRegistros,
					montoTotal:									montoTotal,
					montoTotalPagar:							montoTotalPagar,
					claveMes:									claveMes,
					numeroRegistros:							numeroRegistros,
					sumatoriaSaldosFinMes:					sumatoriaSaldosFinMes,
					isEmptyPkcs7: 								Ext.isEmpty(respuesta.pkcs7),
					pkcs7: 										respuesta.pkcs7,
					textoFirmado: 								respuesta.textoFirmado
				},
				callback: 										procesaCargaArchivo
			});
         
		} else if(	estado == "MOSTRAR_ACUSE_TRANSMISION_REGISTROS"	){
 
			var claveProceso 					= respuesta.claveProceso;
			var totalRegistros 				= respuesta.totalRegistros;
			var montoTotal 					= respuesta.montoTotal;
			var montoTotalPagar 				= respuesta.montoTotalPagar;
			
			var claveMes 						= respuesta.claveMes;
			var numeroRegistros 				= respuesta.numeroRegistros;
			var sumatoriaSaldosFinMes 		= respuesta.sumatoriaSaldosFinMes;
			
			// Establecer los valores de los parametros de la carga
			Ext.getCmp("panelAcuseCargaArchivo.claveProceso").setValue(claveProceso);
			Ext.getCmp("panelAcuseCargaArchivo.totalRegistros").setValue(totalRegistros);
			Ext.getCmp("panelAcuseCargaArchivo.montoTotal").setValue(montoTotal);
			Ext.getCmp("panelAcuseCargaArchivo.montoTotalPagar").setValue(montoTotalPagar);
			
			Ext.getCmp("panelAcuseCargaArchivo.claveMes").setValue(claveMes);
			Ext.getCmp("panelAcuseCargaArchivo.numeroRegistros").setValue(numeroRegistros);
			Ext.getCmp("panelAcuseCargaArchivo.sumatoriaSaldosFinMes").setValue(sumatoriaSaldosFinMes);
 
			// Agregar mensaje transmitiendo solicitudes
			if(respuesta.validaFechas=true){
				Ext.Msg.alert(
					'Mensaje','Las operaciones quedar�n registradas con fecha valor del siguiente dia h�bil : '+
					respuesta.fechaValor
				);
			}
			
			
			Ext.getCmp("panelPreacuseCargaArchivo").el.unmask();
			
			// Ocultar panel de preacuse
			Ext.getCmp("panelPreacuseCargaArchivo").hide();
			
			// Mostrar panel de preacuse
			Ext.getCmp("panelAcuseCargaArchivo").show();
			
			// Mostrar el folio de la validacion
			Ext.getCmp("labelAutentificacion").setText(respuesta.folioSolicitud,false);
			
			// Resetear cualquier url de archivo que pudiera existir anteriormente
			Ext.getCmp("botonImprimirPDF").urlArchivo = "";
			
			// Cargar resultados
			Ext.StoreMgr.key('registrosAgregadosDataStore').loadData(respuesta.registrosAgregadosDataArray);
 
		} else if(	estado == "FIN"								){
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "29CargaArchivoExt.data.jsp";
			forma.target	= "_self";
			forma.submit();
 
		}
		
		return;
		
	}
 
	//-------------------------- 7. PANEL ACUSE CARGA CUENTAS -------------------------
	
	var procesarSuccessFailureGeneraArchivoPDF =  function(opts, success, response) {
 
		var botonImprimirPDF = Ext.getCmp('botonImprimirPDF');
		botonImprimirPDF.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			botonImprimirPDF.setIconClass('icoPdf');
			botonImprimirPDF.setText('Descargar PDF');
			
			botonImprimirPDF.urlArchivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			botonImprimirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		
		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						botonImprimirPDF.setIconClass('icoImprimir');
						botonImprimirPDF.setText('Imprimir PDF');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				botonImprimirPDF.setIconClass('icoImprimir');
				botonImprimirPDF.setText('Imprimir PDF');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
 
	}
	
	var procesarConsultaRegistrosAgregados = function(store, registros, opts){
		
		var gridRegistrosAgregados 			= Ext.getCmp('gridRegistrosAgregados');
		
		if (registros != null) {
			
			var el 							= gridRegistrosAgregados.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
				
			}
			
		}
		
	}
	
	var registrosAgregadosData = new Ext.data.ArrayStore({
			
		storeId: 'registrosAgregadosDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosAgregados,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosAgregados(null, null, null);						
				}
			}
		}
		
	});
	
	var renderContenidoGridAcuse = function(value, metaData, record, rowIndex, colIndex, store) {
		
		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: left !important';
		} else {
			metaData.style += 'text-align: right !important';
		}
		return value;
		
	}
	
	var elementosAcuseCargaArchivo = [
		{
			xtype: 	'label',
			id:	 	'labelAutentificacion',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:15px;',
			html:  	'Cargando...'
		},
		{
			xtype: 	'label',
			id:	 	'labelAcuseCifrasControl',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		173,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'west'
					},
					{
						store: 			registrosAgregadosData,
						xtype: 			'grid',
						id:				'gridRegistrosAgregados',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '80%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		185,
								hidden: 		false,
								hideable:	false,
								fixed:		true
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								align:		'right',
								renderer: 	renderContenidoGridAcuse
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'east'
					}
			]
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			items: [
				{
					xtype: 		'button',
					text: 		'Imprimir PDF',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoImprimir',
					id: 			'botonImprimirPDF',
					urlArchivo: '',
					handler:    function(boton, evento) {
 
						if(Ext.isEmpty(boton.urlArchivo)){
							
							// Cambiar icono
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							// Genera Archivo PDF
							Ext.Ajax.request({
								url: 			'29CargaArchivoExt.data.jsp',
								params: 		{ 
									informacion: 		'GeneraArchivoPDF',
									totalRegistros:	Ext.getCmp("panelAcuseCargaArchivo.totalRegistros").getValue(),
									montoTotal:			Ext.getCmp("panelAcuseCargaArchivo.montoTotal").getValue(),
									claveMes:			Ext.getCmp("panelAcuseCargaArchivo.claveMes").getValue()
								},
								callback: 	procesarSuccessFailureGeneraArchivoPDF
							});
							
						// Descargar el archivo generado	
						} else {
							
							var forma 		= Ext.getDom('formAux');
							forma.action 	= boton.urlArchivo;
							forma.submit();
							
						}
						
					},
					style: {
						width: 137
					}
				},
				{
					xtype: 		'button',
					text: 		'Salir',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoAceptar',
					id: 			'botonSalir',
					handler:    function(boton, evento) {
						location.reload();
					},
					style: {
						width: 137
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.claveProceso',
        id: 	'panelAcuseCargaArchivo.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.totalRegistros',
        id: 	'panelAcuseCargaArchivo.totalRegistros'
      },
      // INPUT HIDDEN: MONTO TOTAL
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.montoTotal',
        id: 	'panelAcuseCargaArchivo.montoTotal'
      },
      // INPUT HIDDEN: MONTO TOTAL A PAGAR
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.montoTotalPagar',
        id: 	'panelAcuseCargaArchivo.montoTotalPagar'
      },
      // INPUT HIDDEN: NUMERO DEL MES
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.claveMes',
        id: 	'panelAcuseCargaArchivo.claveMes'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.numeroRegistros',
        id: 	'panelAcuseCargaArchivo.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE SALDOS DE FIN DE MES
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.sumatoriaSaldosFinMes',
        id: 	'panelAcuseCargaArchivo.sumatoriaSaldosFinMes'
      }
	];
	
	var panelAcuseCargaArchivo = {
		title:			'Solicitud de Desembolsos Masivos de Garant�as',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelAcuseCargaArchivo',
		width: 			650, //949,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosAcuseCargaArchivo
	}
	
	//----------------------------- 6. PANEL PREACUSE CARGA ---------------------------
	
	var procesarConsultaSolicitudesPorAgregar = function(store, registros, opts){
		
		var gridSolicitudesPorAgregar 			= Ext.getCmp('gridSolicitudesPorAgregar');
		
		if (registros != null) {
			
			var el 							= gridSolicitudesPorAgregar.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
				
			}
			
		}
		
	}
	
	var registrosPorAgregarData = new Ext.data.ArrayStore({
			
		storeId: 'registrosPorAgregarDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaSolicitudesPorAgregar,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaSolicitudesPorAgregar(null, null, null);						
				}
			}
		}
		
	});
	
	var renderContenidoGridPreacuse = function(value, metaData, record, rowIndex, colIndex, store) {
		
		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: left !important';
		} else {
			metaData.style += 'text-align: right !important';
		}
		return value;
		
	}
	
	var fnTransmitirRegistrosCallback = function(vpkcs7, vtextoFirmar){
		// Si no hay firma, cancelar la operacion
		if( Ext.isEmpty(vpkcs7) ){
			return;
		// Transmitir registros
		} else {
			var respuesta 					= new Object();
			respuesta['pkcs7'] 			= vpkcs7;
			respuesta['textoFirmado'] 	= vtextoFirmar;
			cargaArchivo("TRANSMITIR_REGISTROS", respuesta);
		}
	}
	
	var elementosPreacuseCargaArchivo = [
		{
			xtype: 	'label',
			id:	 	'labelPreacuseCargaArchivo',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		95,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'west'
					},
					{
						store: 			registrosPorAgregarData,
						xtype: 			'grid',
						id:				'gridSolicitudesPorAgregar',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '80%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		185,
								hidden: 		false,
								hideable:	false,
								fixed:		true
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								//align:		'right',
								renderer: 	renderContenidoGridPreacuse
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'east'
					}
			]
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonPreacuseCancelar',
					margins: 	' 3',
					handler:    function(boton, evento) {
						
						Ext.MessageBox.confirm(
							'Confirm', 
							"�Esta usted seguro de cancelar la operaci�n?", 
							function(confirmBoton){   
								if( confirmBoton == "yes" ){
									 location.reload();
								}
							}
						);
						
					},
					style: {
						width: 100
					}
				},
				{
					xtype: 		'button',
					//width: 		80,
					text: 		'Transmitir Registros',
					iconCls: 	'icoContinuar',
					id: 			'botonTransmitirRegistros',
					margins: 	' 3',
					handler:    function(boton, evento) {
						
						// Obtener texto a firmar
						var registrosPorAgregarData = Ext.StoreMgr.key('registrosPorAgregarDataStore');
						var textoFirmar = ""; 
						registrosPorAgregarData.each(
							function(record){
								textoFirmar += record.get("DESCRIPCION") + ": " + record.get("CONTENIDO")+"\n";
							}
						);
						
						// Suprimir acentos
						textoFirmar = textoFirmar.replace(/�/g, "a").replace(/�/g, "e").replace(/�/g, "i").replace(/�/g, "o").replace(/�/g, "u");
						textoFirmar = textoFirmar.replace(/�/g, "A").replace(/�/g, "E").replace(/�/g, "I").replace(/�/g, "O").replace(/�/g, "U");
						
						// Firmar texto
						
						NE.util.obtenerPKCS7(fnTransmitirRegistrosCallback, textoFirmar);
						
						
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.claveProceso',
        id: 	'panelPreacuseCargaArchivo.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.totalRegistros',
        id: 	'panelPreacuseCargaArchivo.totalRegistros'
      },
      // INPUT HIDDEN: MONTO TOTAL
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.montoTotal',
        id: 	'panelPreacuseCargaArchivo.montoTotal'
      },
      // INPUT HIDDEN: MONTO TOTAL A PAGAR 
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.montoTotalPagar',
        id: 	'panelPreacuseCargaArchivo.montoTotalPagar'
      },
      // INPUT HIDDEN: NUMERO DEL MES 
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.claveMes',
        id: 	'panelPreacuseCargaArchivo.claveMes'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD 
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.numeroRegistros',
        id: 	'panelPreacuseCargaArchivo.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE SALDOS DE FIN DE MES 
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.sumatoriaSaldosFinMes',
        id: 	'panelPreacuseCargaArchivo.sumatoriaSaldosFinMes'
      }
	];

	var panelPreacuseCargaArchivo = {
		title:			'Pre-Acuse',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelPreacuseCargaArchivo',
		width: 			550, //949,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosPreacuseCargaArchivo
	}
	
	//---------------------------- 3. PANEL LAYOUT DE CARGA ------------------------------
	
	var hidePanelLayoutDeCarga = function(){
		Ext.getCmp('panelLayoutDeCarga').hide();
		Ext.getCmp('espaciadorPanelLayoutDeCarga').hide();
	}
	
	showPanelLayoutDeCarga = function(){
		Ext.getCmp('panelLayoutDeCarga').show();
		Ext.getCmp('espaciadorPanelLayoutDeCarga').show();
	}
	                             
	var elementosLayoutDeCarga = [
		{
			xtype: 	'label',
			id:		'labelLayoutCarga',
			name:		'labelLayoutCarga',
			html: 	'<table align ="center">'+
			'	<tr>'+
			'		<td class="titulos" align="center" >'+
			'			<span class="titulos"> 		</span>'+
			'		</td>'+
			'	</tr>'+
			'	<tr>'+
			'		<td>&nbsp;</td>'+
			'	</tr>'+
			'	<tr>'+
				'	<td colspan="2" align="center" width="600px" >'+
				'		<table cellpadding="3" cellspacing="0" border="1" bordercolor="#A5B8BF" style="background:#FFFFFF; aling ="center">'+
				'			<tr>'+
				'				<td class="celda01" align="center" colspan="6">Layout de Desembolsos Masivos</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="celda01" align="center">No.</td>'+
				'				<td class="celda01" align="center">CAMPO</td>'+
				'				<td class="celda01" align="center">TIPO DATO</td>'+
				'				<td class="celda01" align="center">LONG.MAX</td>'+
				'				<td class="celda01" align="center">OBLIGATORIO</td>'+
				'				<td class="celda01" align="center">OBSERVACIONES</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">1</td>'+
				'				<td class="formas" align="left">'+
				'				  Clave del intermediario'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">5</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">2</td>'+
				'				<td class="formas" align="left">'+
				'				  Clave de la garant&iacute;a'+
				'				</td>'+
				'				<td class="formas" align="left">Alfanum&eacute;rico</td>'+
				'				<td class="formas" align="center">20</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					N&uacute;mero &uacute;nico por registro'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">3</td>'+
				'				<td class="formas" align="left">'+
				'				  RFC del Acreditado'+
				'				</td>'+
				'				<td class="formas" align="left">Alfanum&eacute;rico</td>'+
				'				<td class="formas" align="center">13</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">4</td>'+
				'				<td class="formas" align="left">'+
				'				  Nombre del Acreditado'+
				'				</td>'+
				'				<td class="formas" align="left">Alfanum&eacute;rico</td>'+
				'				<td class="formas" align="center">80</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">5</td>'+
				'				<td class="formas" align="left">'+
				'				  Monto original del cr&eacute;dito'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">17,2</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">6</td>'+
				'				<td class="formas" align="left">'+
				'				  Fecha de disposici&oacute;n (AAAAMMDD)'+
				'				</td>'+
				'				<td class="formas" align="left">Fecha</td>'+
				'				<td class="formas" align="center">8</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">7</td>'+
				'				<td class="formas" align="left">'+
				'				  Saldo insoluto del estado de cuenta'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">17,2</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">8</td>'+
				'				<td class="formas" align="left">'+
				'				  Capital vigente'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">17,2</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">9</td>'+
				'				<td class="formas" align="left">'+
				'				  Capital vencido'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">17,2</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">10</td>'+
				'				<td class="formas" align="left">'+
				'				  Inter&eacute;s ordinario'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">17,2</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">11</td>'+
				'				<td class="formas" align="left">'+
				'				  Inter&eacute;s moratorio'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">17,2</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">12</td>'+
				'				<td class="formas" align="left">'+
				'				  Fecha &uacute;ltimo pago de capital (AAAAMMDD)'+
				'				</td>'+
				'				<td class="formas" align="left">Fecha</td>'+
				'				<td class="formas" align="center">8</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">13</td>'+
				'				<td class="formas" align="left">'+
				'				  Fecha &uacute;ltimo pago de inter&eacute;s (AAAAMMDD)'+
				'				</td>'+
				'				<td class="formas" align="left">Fecha</td>'+
				'				<td class="formas" align="center">8</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">14</td>'+
				'				<td class="formas" align="left">'+
				'				  Intereses recuperados posteriores al primer incumplimiento'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">17,2</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">15</td>'+
				'				<td class="formas" align="left">'+
				'				  Periodicidad de pago'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">2</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">16</td>'+
				'				<td class="formas" align="left">'+
				'				  Fecha de primer incumplimiento (AAAAMMDD)'+
				'				</td>'+
				'				<td class="formas" align="left">Fecha</td>'+
				'				<td class="formas" align="center">8</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">17</td>'+
				'				<td class="formas" align="left">'+
				'				  Fecha de segundo incumplimiento (AAAAMMDD)'+
				'				</td>'+
				'				<td class="formas" align="left">Fecha</td>'+
				'				<td class="formas" align="center">8</td>'+
				'				<td class="formas" align="center">Cuando periodicidad =<br>Mensual</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">18</td>'+
				'				<td class="formas" align="left">'+
				'				  Fondeo Nafin'+
				'				</td>'+
				'				<td class="formas" align="left">Alfanum&eacute;rico</td>'+
				'				<td class="formas" align="center">1</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					Valores permitidos:<br>S o N'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">19</td>'+
				'				<td class="formas" align="left">'+
				'				  Tasa de fondeo Nafin'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">5,2</td>'+
				'				<td class="formas" align="center">Cuando Fondeo = S</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">20</td>'+
				'				<td class="formas" align="left">'+
				'				  Fecha de liquidaci&oacute;n de fondeo (AAAAMMDD)'+
				'				</td>'+
				'				<td class="formas" align="left">Fecha</td>'+
				'				<td class="formas" align="center">8</td>'+
				'				<td class="formas" align="center">Cuando Fondeo = S</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">21</td>'+
				'				<td class="formas" align="left">'+
				'				  N&uacute;mero pr&eacute;stamo SIRAC'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">12</td>'+
				'				<td class="formas" align="center">Cuando Fondeo = S</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">22</td>'+
				'				<td class="formas" align="left">'+
				'				  Ejecutivo responsable del expediente de cr&eacute;dito'+
				'				</td>'+
				'				<td class="formas" align="left">Alfanum&eacute;rico</td>'+
				'				<td class="formas" align="center">100</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">23</td>'+
				'				<td class="formas" align="left">'+
				'				  Correo electr&oacute;nico'+
				'				</td>'+
				'				<td class="formas" align="left">Alfanum&eacute;rico</td>'+
				'				<td class="formas" align="center">40</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">24</td>'+
				'				<td class="formas" align="left">'+
				'				  Ubicaci&oacute;n del expediente calle y n&uacute;mero'+
				'				</td>'+
				'				<td class="formas" align="left">Alfanum&eacute;rico</td>'+
				'				<td class="formas" align="center">150</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">25</td>'+
				'				<td class="formas" align="left">'+
				'				  Ubicaci&oacute;n del expediente colonia'+
				'				</td>'+
				'				<td class="formas" align="left">Alfanum&eacute;rico</td>'+
				'				<td class="formas" align="center">80</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">26</td>'+
				'				<td class="formas" align="left">'+
				'				  Clave del Estado o Entidad Federativa'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">4</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">27</td>'+
				'				<td class="formas" align="left">'+
				'				  Clave del Municipio o Delegaci&oacute;n'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">4</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">28</td>'+
				'				<td class="formas" align="left">'+
				'				  Tel&eacute;fono'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">10</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'			<tr>'+
				'				<td class="formas" align="center">29</td>'+
				'				<td class="formas" align="left">'+
				'				  Extensi&oacute;n'+
				'				</td>'+
				'				<td class="formas" align="left">Num&eacute;rico</td>'+
				'				<td class="formas" align="center">6</td>'+
				'				<td class="formas" align="center">S&iacute;</td>'+
				'				<td class="formas" align="center">'+
				'					&nbsp;'+
				'				</td>'+
				'			</tr>'+
				'		</table>'+
				'	</td>'+
				'</tr>'+
				'<tr>'+
				'	<td>&nbsp;</td>'+
				'</tr>'+
				'</table>',
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginBottom: 	'10px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		}
	];
	
	var panelLayoutDeCarga = {
		xtype:			'panel',
		id: 				'panelLayoutDeCarga',
		hidden:			true,
		width: 			700,
		title: 			'Descripci�n del Layout de Carga',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosLayoutDeCarga,
		tools: [
			{
				id:			'close',
				handler: function(event, toolEl, panel){
					hidePanelLayoutDeCarga();
				}
    		}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      )
	}
	
	var espaciadorPanelLayoutDeCarga = {
		xtype: 	'box',
		id:		'espaciadorPanelLayoutDeCarga',
		hidden:	true,
		height: 	10,
		width: 	800
	}
	
	//------------------------- 5. PANEL RESULTADOS VALIDACION ---------------------------
 
	var procesarConsultaRegistrosSinErrores 	= function(store, registros, opts){
		
		var gridRegistrosSinErrores 						= Ext.getCmp('gridRegistrosSinErrores');
		
		if (registros != null) {
			
			var el 							= gridRegistrosSinErrores.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var procesarConsultaRegistrosConErrores = function(store, registros, opts){
		
		var gridRegistrosConErrores						= Ext.getCmp('gridRegistrosConErrores');
		
		if (registros != null) {
			
			var el 							= gridRegistrosConErrores.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var procesarConsultaErroresVsCifrasControl = function(store, registros, opts){
		
		var gridErroresVsCifrasControl = Ext.getCmp('gridErroresVsCifrasControl');
		
		if (registros != null) {
			
			var el 							= gridErroresVsCifrasControl.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var registrosSinErroresData = new Ext.data.ArrayStore({
			
		storeId: 'registrosSinErroresDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'NUMERO_LINEA',   			mapping: 0, type: 'int' },
			{ name: 'CLAVE_FINANCIAMIENTO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosSinErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosSinErrores(null, null, null);						
				}
			}
		}
		
	});
	
	var registrosConErroresData = new Ext.data.ArrayStore({
			
		storeId: 'registrosConErroresDataStore',
		fields:  [
			{ name: 'NUMERO_LINEA',   	mapping: 0, type: 'int' },
			{ name: 'DESCRIPCION', 		mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosConErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosConErrores(null, null, null);						
				}
			}
		}
		
	});
	
	var erroresVsCifrasControlData  = new Ext.data.ArrayStore({
			
		storeId: 'erroresVsCifrasControlDataStore',
		fields:  [
			{ name: 'NUMERO_LINEA',   	mapping: 0, type: 'int' },
			{ name: 'DESCRIPCION', 		mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaErroresVsCifrasControl,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaErroresVsCifrasControl(null, null, null);						
				}
			}
		}
		
	});

	var elementosResultadosValidacion = [
		{ 
			xtype: 			'tabpanel',
			id:				'tabPanelResultadosValidacion',
			activeTab:		0,
			plain:			true,
			height: 			400,
			//bodyStyle:		'padding: 10px',
			items: [
				{
					xtype:		'container',
					title: 		'Registros sin Errores',
					/*layout:		'vbox',
               layout: {			// Nota: usando esta configuracion flex no funciona bien
						type:'vbox',
                  align:'stretch'
					},
					*/
					items: [
						{
							store: 		registrosSinErroresData,
							xtype: 		'grid',
							id:			'gridRegistrosSinErrores',
							stripeRows: true,
							loadMask: 	true,	
							frame: 		false,
							height:		315,
							flex:			1,
							columns: [
								{
									header: 		'Linea',
									tooltip: 	'Linea',
									dataIndex: 	'NUMERO_LINEA',
									sortable: 	true,
									resizable: 	true,
									width: 		50,
									hidden: 		true,
									hideable:	false
								},
								{
									header: 		'Claves de Financiamiento',
									tooltip: 	'Claves de Financiamiento',
									dataIndex: 	'CLAVE_FINANCIAMIENTO',
									sortable: 	true,
									resizable: 	true,
									width: 		675,
									hidden: 		false
								}
							]					
						},
						{
							xtype: 		'panel',
							layout:		'form',
							labelWidth: 260,
							style:		"padding:8px;",
							height:		70,
							items: [
								// TEXTFIELD Total de Registros
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Total de Registros',
									name: 			'numeroRegistrosSinErrores',
									id: 				'numeroRegistrosSinErrores',
									readOnly:		true,
									hidden: 			false,
									maxLength: 		15,	
									msgTarget: 		'side',
									style: {
										textAlign: 'right'
									}
								},
								// TEXTFIELD Sumatoria de saldos de fin de mes
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Sumatoria de montos de financiamiento',
									name: 			'sumatoriaSaldosFinMesSinErrores',
									id: 				'sumatoriaSaldosFinMesSinErrores',
									readOnly:		true,
									hidden: 			false,
									maxLength: 		15,	
									msgTarget: 		'side',
									style: {
									  textAlign: 'right'
								  }
								}
							]
						}
					]
				},
				{
					xtype:		'container',
					title: 		'Registros con Errores',
					/*layout:		'vbox',
					 layout: {			// Nota: usando esta configuracion flex no funciona bien
						type:'vbox',
                  align:'stretch'
					},
					*/
					items: [
						{
							store: 		registrosConErroresData,
							xtype: 		'grid',
							id:			'gridRegistrosConErrores',
							stripeRows: true,
							loadMask: 	true,	
							frame: 		false,
							height:		315,
							flex:			1,
							columns: [
								{
									header: 		'Linea',
									tooltip: 	'Linea',
									dataIndex: 	'NUMERO_LINEA',
									sortable: 	true,
									resizable: 	true,
									width: 		50,
									hidden: 		true,
									hideable:	false
								},
								{
									header: 		'Descripci�n',
									tooltip: 	'Descripci�n',
									dataIndex: 	'DESCRIPCION',
									sortable: 	true,
									resizable: 	true,
									width: 		1150,
									hidden: 		false
								}
							]
						},
						{
							xtype: 		'container',
							layout:		'form',
							labelWidth: 260,
							style:		"padding:8px;",
							items: [
								// TEXTFIELD Total de Registros
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Total de Registros',
									name: 			'numeroRegistrosConErrores',
									id: 				'numeroRegistrosConErrores',
									readOnly:		true,
									hidden: 			false,
									maxLength: 		15,	
									msgTarget: 		'side',
									style: {
									  textAlign: 'right'
								  }
								},
								// TEXTFIELD Sumatoria de saldos de fin de mes
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Sumatoria de montos de financiamiento',
									name: 			'sumatoriaSaldosFinMesConErrores',
									id: 				'sumatoriaSaldosFinMesConErrores',
									readOnly:		true,
									hidden: 			false,
									maxLength: 		15,	
									msgTarget: 		'side',
									style: {
									  textAlign: 'right'
								  }
								}
							]
						}
					]
				},
				{
					title: 		'Errores vs. Cifras de Control',
					store: 		erroresVsCifrasControlData,
					xtype: 		'grid',
					id:			'gridErroresVsCifrasControl',
					stripeRows: true,
					loadMask: 	true,
					columns: [
						{
							header: 		'Linea',
							tooltip: 	'Linea',
							dataIndex: 	'NUMERO_LINEA',
							sortable: 	true,
							resizable: 	true,
							width: 		50,
							hidden: 		true,
							hideable:	false
						},
						{
							header: 		'Descripci�n',
							tooltip: 	'Descripci�n',
							dataIndex: 	'DESCRIPCION',
							sortable: 	true,
							resizable: 	true,
							width: 		1150,
							hidden: 		false
						}
					]
				}
			]
		},
		{
			xtype: 			'container',
			width: 			'100%',
			style: 			'margin: 8px;padding-top:15px;',
			renderHidden: 	true,
			layout: {
				type: 		'hbox',
				pack: 		'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonCancelarInsercion',
					margins: 	' 3',
					handler:    function(boton, evento) {
									 location.reload();
					},
					style: {
						width: 	100
					}
				},
				{
					xtype: 		'button',
					//width: 		100,
					text: 		'Continuar Carga',
					iconCls: 	'icoContinuar',
					id: 			'botonContinuarCarga',
					margins: 	' 3',
					handler:    function(boton, evento) {
						cargaArchivo("PRESENTAR_PREACUSE",null);
					}/*,
					style: {
						width: 100
					}*/
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.claveProceso',
        id: 	'panelResultadosValidacion.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.totalRegistros',
        id: 	'panelResultadosValidacion.totalRegistros'
      },
      // INPUT HIDDEN: MONTO TOTAL
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.montoTotal',
        id: 	'panelResultadosValidacion.montoTotal'
      },
      // INPUT HIDDEN: MONTO TOTAL A PAGAR
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.montoTotalPagar',
        id: 	'panelResultadosValidacion.montoTotalPagar'
      },
      // INPUT HIDDEN: NUMERO DEL MES
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.claveMes',
        id: 	'panelResultadosValidacion.claveMes'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.numeroRegistros',
        id: 	'panelResultadosValidacion.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE SALDOS DE FIN DE MES
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.sumatoriaSaldosFinMes',
        id: 	'panelResultadosValidacion.sumatoriaSaldosFinMes'
      }
 
	];
	
	var panelResultadosValidacion = {
		title:			'Resultado de la Validaci�n',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosValidacion',
		width: 			700, //949,
		frame: 			true,
		style: 			'margin:  0 auto',
		bodyStyle:		'padding: 10px',
		items: 			elementosResultadosValidacion
	};

	var espaciadorPanelResultadosValidacion = {
		xtype: 	'box',
		id:		'espaciadorPanelResultadosValidacion',
		hidden:	true,
		height: 	7,
		width: 	800
	}
	
	//---------------------------- PANEL AVANCE VALIDACION ------------------------------
	
	var elementosAvanceValidacion = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
			text: 	'Revisando solicitudes...'
		},
		{
			xtype: 'progress',
			id:	 'barrarAvanceValidacion',
			name:	 'barrarAvanceValidacion',
			cls:	 'center-align',
			value: 0.3756,
			text:  '37.56 %'
		}
	];
	
	var panelAvanceValidacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelAvanceValidacion',
		frame: 			true,
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		items: 			elementosAvanceValidacion
	});
	
	var showWindowAvanceValidacion = function(){
		
		var barrarAvanceValidacion		= Ext.getCmp("barrarAvanceValidacion");
 
		barrarAvanceValidacion.updateProgress(0.00,"0.00 %",true);
		barrarAvanceValidacion.wait({
            interval:	200,
            increment:	15
      });
 
		var ventana = Ext.getCmp('windowAvanceValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				width:			300,
				resizable:		false,
				id: 				'windowAvanceValidacion',
				modal:			true,
				closable: 		false,
				items: [
					Ext.getCmp("panelAvanceValidacion")
				]
			}).show();
			
		}
		
	}	

	var hideWindowAvanceValidacion = function(){
		
		var ventanaAvanceValidacion = Ext.getCmp('windowAvanceValidacion');
		
		if(ventanaAvanceValidacion && ventanaAvanceValidacion.isVisible() ){
			var barrarAvanceValidacion	 = Ext.getCmp("barrarAvanceValidacion");
			barrarAvanceValidacion.reset(); 
			
			
			if (ventanaAvanceValidacion) {
				ventanaAvanceValidacion.hide();
			}
      }
      
	}
 
	//-------------------------- 4. FORMA DE CARGA DE ARCHIVO ---------------------------
 
	var elementosFormaCargaArchivo = [
		{
		  xtype: 		'fileuploadfield',
		  id: 			'archivo',
		  name: 			'archivo',
		  emptyText: 	'Seleccione...',
		  fieldLabel: 	"Ruta del Archivo de Registros", 
		  buttonText: 	null,
		  buttonCfg: {
			  iconCls: 	'upload-icon'
		  },
		  anchor: 		'-20'
		  //vtype: 		'archivotxt'
		},
		{
			xtype: 		'panel',
			anchor: 		'100%',
			style: {
				marginTop: 		'10px'
			},
			layout: {
				type: 'hbox',
				pack: 'end',
				align: 'middle'
			},
			items: [
				{
					xtype: 			'button',
					text: 			'Continuar',
					iconCls: 		'icoContinuar',
					id: 				'botonContinuarCargaArchivo',				
					handler:    function(boton, evento) {
						
						// Revisar si la forma es invalida
						var forma = Ext.getCmp("panelFormaCargaArchivo").getForm();
						if(!forma.isValid()){
							return;
						}
	 
						// Validar que se haya especificado un archivo
						var archivo = Ext.getCmp("archivo");
						if( Ext.isEmpty( archivo.getValue() ) ){
							archivo.markInvalid("Debe especificar un archivo");
							return;
						}
						
						// Revisar el tipo de extension del archivo
						var myRegexTXT = /^.+\.([tT][xX][tT])$/;
						var myRegexZIP = /^.+\.([zZ][iI][pP])$/;
						
						var nombreArchivo 	= archivo.getValue();
						var numeroRegistros 	= Ext.getCmp("numeroRegistros1").getValue();
						if( numeroRegistros >= 5000 && !myRegexZIP.test(nombreArchivo) ){
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n zip.");
							return;
						} else if( !myRegexTXT.test(nombreArchivo) && !myRegexZIP.test(nombreArchivo) ) {
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n zip o txt.");
							return;
						}
						
						// Cargar archivo
						cargaArchivo("SUBIR_ARCHIVO",null);

					}
				}
			]
		},
		{
			xtype: 	'label',
			anchor:  '100%',
			html: 	"<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" width=\"100%\" > "  +
						"   <tr>"  +
						"	    <td class=\"celda01\" align=\"center\" colspan=\"10\"  style=\"height:30px;\" >"  +
						"         Si el archivo que va a enviar contiene 5,000 registros o m�s, deber� convertirlo a formato .ZIP antes de iniciar la carga "  +
						"	    </td>"  +
						"   </tr>"  +
						"</table>" ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginTop: 		'20px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		},
		{
			xtype: 	'hidden',
			name:		'claveMes1',
			id:		'claveMes1'
		},
		{
			xtype: 	'hidden',
			name:		'numeroRegistros1',
			id:		'numeroRegistros1'
		},
		{
			xtype: 	'hidden',
			name:		'sumatoriaSaldosFinMes1',
			id:		'sumatoriaSaldosFinMes1'
		}
	];
	
	var panelFormaCargaArchivo = new Ext.form.FormPanel({
		id: 				'panelFormaCargaArchivo',
		width: 			700,
		title: 			'Carga de Archivo',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		fileUpload: 	true,
		hidden:			true,
		renderHidden:	true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	190,
		defaultType: 	'textfield',
		layoutConfig: {
			fieldTpl: new Ext.XTemplate(
				'<tpl if="id==\'archivo\'">',
           		 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">',
						  		'<a class="x-tool x-tool-ayudaLayout" onfocus="blur()" style="float:left;" onclick="javascript:showPanelLayoutDeCarga();"></a>&nbsp;',
						  		'{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
						  '</div>',
					 '</div>',
			  '</tpl>',
			  '<tpl if="id!=\'archivo\'">',
					 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
					 '</div>',
			  '</tpl>'
		   )
		},
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementosFormaCargaArchivo,
		monitorValid: 	false
	});
 
	//------------------------------- 2. PANEL SOLICITUD DE CARGA --------------------------------
	
	var catalogoMesData = new Ext.data.JsonStore({
		id: 			'catalogoMesDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'29CargaArchivoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoMes'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load: function(store,records,options){
 	
				var index = store.getCount()-1; 
				// El registro fue encontrado por lo que hay que seleccionarlo
				if (index >= 0){
					var defaultValue = store.getAt(index).get('clave');
					Ext.getCmp("comboMes").setValue(defaultValue);
					Ext.getCmp("comboMes").originalValue = defaultValue;
				}
			
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementosFormaSolicitudDeCarga = [
		// COMBO MES 
		{
			xtype: 				'combo',
			name: 				'mes',
			id: 					'comboMes',
			hidden:				true,
			fieldLabel: 		'Mes',
			allowBlank: 		false,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveMes',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMesData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			msgTarget: 			'side',
			anchor:				'-20'
		},
		// NUMBERFIELD NUMERO DE REGISTROS EN LA SOLICITUD
		{ 
			xtype: 			'numberfield',
			name: 			'numeroRegistros',
			id: 				'numeroRegistros',
			allowDecimals: false,
			allowNegative: false,
			fieldLabel: 	'N�mero de registros que contiene la solicitud',
			allowBlank: 	false,
			hidden: 			false,
			maxLength: 		30,	
			msgTarget: 		'side',
			anchor:			'-20',
			style: {
           textAlign: 'right'
        }
		},
		// NUMBERFIELD SUMATORIA DE SALDOS AL FIN DEL MES
		{ 
			xtype: 			'numberfield',
			name: 			'sumatoriaSaldosFinMes',
			id: 				'sumatoriaSaldosFinMes',
			fieldLabel:		'Sumatoria de montos de financiamiento',
			blankText:		'Favor de capturar la sumatoria de saldos al fin de mes',
			allowBlank:		false,
			hidden: 			false,
			maxLength: 		30,	
			msgTarget: 		'side',
			anchor:			'-20', //necesario para mostrar el icono de error
			style: {
           textAlign: 	'right'
        }
		}
	];
	
	var panelFormaSolicitudDeCarga 		= new Ext.form.FormPanel({
		id: 					'panelFormaSolicitudDeCarga',
		width: 				700,
		title: 				'Solicitud de Pago de Garant�as',
		frame: 				true,
		hidden:				true,
		collapsible: 		false,
		titleCollapse: 	false,
		trackResetOnLoad: true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		renderHidden:		true, // Nota: no usar hidden: true -> hara que los componentes no se muestren del tamanio correcto
		labelWidth: 		400,
		defaultType: 		'textfield',
		items: 				elementosFormaSolicitudDeCarga,
		monitorValid: 		false,
		buttons: [
			{
				id:		'botonEnviar',
				text: 	'Enviar',
				hidden: 	false,
				iconCls: 'icoContinuar',
				handler: function() {
					
					// Cancelar validaciones adicionales si alguno de los campos de la forma
					// viene vacio
					var formaSolicitudDeCarga = Ext.getCmp("panelFormaSolicitudDeCarga").getForm();
					if( !formaSolicitudDeCarga.isValid() ){
						return;
					}

					// Enviar solicitud
					respuesta = formaSolicitudDeCarga.getValues();
					cargaArchivo("ENVIAR_SOLICITUD_DE_CARGA", respuesta);
					
				}
				
			},
			// BOTON LIMPIAR
			{
				text: 	'Limpiar',
				hidden: 	false,
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('contenedorPrincipal').doLayout(true,true);
					Ext.getCmp('panelFormaSolicitudDeCarga').getForm().reset();
				}
			}
		]
	});
 
	//------------------------------------ 1. PANEL AVISOS ---------------------------------------
	
	var elementosPanelAvisos = [
		{
			xtype: 	'label',
			id:	 	'labelAviso',
			cls:		'x-form-item',
			style: 	'font-weight:normal;text-align:center;margin:15px;color:black;',
			html:  	''
		}
	];
	
	var panelAvisos = {
		xtype:			'panel',
		id: 				'panelAvisos',
		hidden:			true,
		width: 			700,
		title: 			'Avisos',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosPanelAvisos
	}
	
	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelAvisos,
			panelFormaSolicitudDeCarga,
			panelFormaCargaArchivo,
			espaciadorPanelResultadosValidacion,
			panelResultadosValidacion,
			espaciadorPanelLayoutDeCarga,
			panelLayoutDeCarga,
			panelPreacuseCargaArchivo,
			panelAcuseCargaArchivo
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	cargaArchivo("INICIALIZACION",null);
 
});;