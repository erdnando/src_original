Ext.onReady(function() {
 
	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});	
 
	Ext.grid.ActionTextColumn = Ext.extend(Ext.grid.ActionColumn, {
			constructor: function(cfg) {
			  var me = this,
					items = cfg.items || (me.items = [me]),
					l = items.length,
					i,
					item;
	
			  Ext.grid.ActionTextColumn.superclass.constructor.call(me, cfg);
	
	//      Renderer closure iterates through items creating an <img> element for each and tagging with an identifying 
	//      class name x-action-col-{n}
			  me.renderer = function(v, meta) {
	//          Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
					v = Ext.isFunction(cfg.renderer) ? cfg.renderer.apply(this, arguments)||'' : '';
	
					meta.css += ' x-action-col-cell';
					for (i = 0; i < l; i++) {
						 item = items[i];
						 v += '<img alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
							  '" class="x-action-col-icon x-action-col-' + String(i) + ' ' + (item.iconCls || '') +
							  ' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope||this.scope||this, arguments) : '') + '"' +
							  ((item.tooltip) ? ' ext:qtip="' + item.tooltip + '"' : '') + ' align="middle" />'; 
							v += Ext.isEmpty(item.text)?"":"&nbsp;"+item.text;
					}
					return v;
			  };
		 }
	});
	Ext.apply(Ext.grid.Column.types, { actiontextcolumn: Ext.grid.ActionTextColumn }  ); 
	
	//-------------------------------- VALIDACIONES -----------------------------------
 
	//----------------------------------- HANDLERS ------------------------------------
	
	var procesaResultadosComisiones = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						resultadosComisiones(resp.estadoSiguiente,resp);
					}
				);
			} else {
				resultadosComisiones(resp.estadoSiguiente,resp);
			}
			
		} else {
			
			// Suprimir mascaras segun se requiera
			resultadosComisiones("ESPERAR_DECISION", null );
 
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	
	var resultadosComisiones = function(estado, respuesta ){
 
		if(			estado == "INICIALIZACION"					){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ResultadosComisiones01ext.data.jsp',
				params: 	{
					informacion:		'ResultadosComisiones.inicializacion'
				},
				callback: 				procesaResultadosComisiones
			});
			
		} else if(			estado == "MOSTRAR_AVISO"								){

			if(!Ext.isEmpty(respuesta.aviso)){
				
				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);
				
				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();
				
			}
			
		} else if(  estado == "ESPERAR_CAPTURA_CONSULTA"   ){
 
			var panelFormaConsultaResultados = Ext.getCmp("panelFormaConsultaResultados");
			panelFormaConsultaResultados.show();
			panelFormaConsultaResultados.doLayout();
			
		} else if(  estado == "ESPERAR_DECISION"  			){ 
			
			// Se deja al usuario la determinacion del estado siguiente.
			
			// Limpiar M�scara en la Forma de B�squeda
			var element = Ext.getCmp("panelFormaConsultaResultados").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Resetear Grid de Comisiones
			var gridResultadosCarga = Ext.getCmp("gridResultadosCarga");		
			if( gridResultadosCarga.generandoArchivo && !Ext.isEmpty(gridResultadosCarga.recordId) ){
 
				// Obtener registro
				var record			= 	Ext.StoreMgr.key('resultadosDataStore').getById(gridResultadosCarga.recordId);
				if( Ext.isEmpty(record) ){
					return;
				}
						
				// Deshabilitar flag que indica que hay una consulta en proceso
				gridResultadosCarga.generandoArchivo	= false;
				gridResultadosCarga.recordId				= null;
						
				// Suprimir animacion
				if( record.data['EXTRACCION_ARCHIVO_ORIGEN_LOADING'] ){
					record.data['EXTRACCION_ARCHIVO_ORIGEN_LOADING'] = false;
				} else if (record.data['EXTRACCION_ARCHIVO_RESULTADOS_LOADING']){
					record.data['EXTRACCION_ARCHIVO_RESULTADOS_LOADING'] = false;
				} else if (record.data['EXTRACCION_ARCHIVO_ERRORES_LOADING']){
					record.data['EXTRACCION_ARCHIVO_ERRORES_LOADING'] = false;
				}
				gridResultadosCarga.getView().refresh();
						
			}
			
		} else if(  estado == "CONSULTA_RESULTADOS"           ){
			
			// Agregar mascara de busqueda
			var panelFormaConsultaResultados  = Ext.getCmp("panelFormaConsultaResultados");
			panelFormaConsultaResultados.getEl().mask('Buscando...','x-mask-loading');

			// Agregar mascara que diga.
			var resultadosData  = Ext.StoreMgr.key('resultadosDataStore');
			resultadosData.load({
				params: respuesta
			});
			
			// Se deja a la funcion: procesarConsultaResultadosCarga del store: resultadosData 
			// el paso al siguiente estado: "ESPERA_DECISION".
			
		} else if( estado  == "EXTRACCION_ARCHIVO_ORIGEN"        ){
			
			// Indicar que hay una operaci�n en proceso
      	var gridResultadosCarga 					= Ext.getCmp("gridResultadosCarga");
      	gridResultadosCarga.generandoArchivo	= true;
      	gridResultadosCarga.recordId				= respuesta.recordId;
      	
      	// Obtener registro
      	var record = Ext.StoreMgr.key('resultadosDataStore').getById(respuesta.recordId);
 
      	// Mostrar animacion
      	record.data['EXTRACCION_ARCHIVO_ORIGEN_LOADING'] = true;
      	gridResultadosCarga.getView().refresh();
			
			// Finalizar extracci�n
			Ext.Ajax.request({
				url: 						'29ResultadosComisiones01ext.data.jsp',
				params: 	{
					informacion:		'ResultadosComisiones.extraccionArchivoTXTOrigen',
					recordId: 			respuesta.recordId,
					folioOperacion:	record.data['FOLIO_OPERACION']
				},
				callback: 				procesaResultadosComisiones
			});
				 
		} else if(   estado == "EXTRACCION_ARCHIVO_RESULTADOS"  ){
 
      	// Indicar que hay una consulta en proceso
      	var gridResultadosCarga 					= Ext.getCmp("gridResultadosCarga");
      	gridResultadosCarga.generandoArchivo 	= true;
      	gridResultadosCarga.recordId				= respuesta.recordId;
      	
      	// Obtener registro
      	var record = Ext.StoreMgr.key('resultadosDataStore').getById(respuesta.recordId);
      	
      	// Mostrar animacion
      	record.data['EXTRACCION_ARCHIVO_RESULTADOS_LOADING'] = true;
      	gridResultadosCarga.getView().refresh();
      	
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ResultadosComisiones01ext.data.jsp',
				params: {	
					informacion:		'ResultadosComisiones.extraccionArchivoPDFResultados',
					recordId:			respuesta.recordId,
					folioOperacion:	record.data['FOLIO_OPERACION']
				},
				callback: 				procesaResultadosComisiones
			});
			
		} else if(   estado == "EXTRACCION_ARCHIVO_ERRORES"  ){
	 
      	// Indicar que hay una consulta en proceso
      	var gridResultadosCarga 					= Ext.getCmp("gridResultadosCarga");
      	gridResultadosCarga.generandoArchivo 	= true;
      	gridResultadosCarga.recordId				= respuesta.recordId;
      	
      	// Obtener registro
      	var record = Ext.StoreMgr.key('resultadosDataStore').getById(respuesta.recordId);
      	
      	// Mostrar animacion
      	record.data['EXTRACCION_ARCHIVO_ERRORES_LOADING'] = true;
      	gridResultadosCarga.getView().refresh();
      	
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29ResultadosComisiones01ext.data.jsp',
				params: {	
					informacion:		'ResultadosComisiones.extraccionArchivoTXTErrores',
					recordId:			respuesta.recordId,
					folioOperacion:	record.data['FOLIO_OPERACION']
				},
				callback: 				procesaResultadosComisiones
			});
			
		} else if(	 estado == "MOSTRAR_LINK_ARCHIVO_TXT_ORIGEN" 	){
			
			// Obtener registro del que se realiz� la extraccion detallada.
      	var record = Ext.StoreMgr.key('resultadosDataStore').getById(respuesta.recordId);
 
			// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridResultadosCarga 					= Ext.getCmp("gridResultadosCarga");
      	gridResultadosCarga.generandoArchivo 	= false;
      	gridResultadosCarga.recordId				= null;
      	
      	// Suprimir animacion
      	record.data['EXTRACCION_ARCHIVO_ORIGEN_LOADING']	= false;
      	record.data['EXTRACCION_ARCHIVO_ORIGEN_URL'] 		= respuesta.urlNombreArchivo;
      	
      	// Mostrar TXT Origen
      	gridResultadosCarga.getView().refresh();
 	
		} else if(	 estado == "MOSTRAR_LINK_ARCHIVO_PDF_RESULTADOS" ){
			
			// Obtener registro
      	var record = Ext.StoreMgr.key('resultadosDataStore').getById(respuesta.recordId);
      	
      	// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridResultadosCarga 					= Ext.getCmp("gridResultadosCarga");
      	gridResultadosCarga.generandoArchivo 	= false;
      	gridResultadosCarga.recordId				= null;
      	
      	// Suprimir animacion
      	record.data['EXTRACCION_ARCHIVO_RESULTADOS_LOADING']  = false;
      	record.data['EXTRACCION_ARCHIVO_RESULTADOS_URL'] 		= respuesta.urlNombreArchivo;
      	
      	// Mostrar PDF Resultados
      	gridResultadosCarga.getView().refresh();
			
		} else if(	 estado == "MOSTRAR_LINK_ARCHIVO_TXT_ERRORES" 	){
			
			// Obtener registro
      	var record			= 	Ext.StoreMgr.key('resultadosDataStore').getById(respuesta.recordId);
      	
      	// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridResultadosCarga 					= Ext.getCmp("gridResultadosCarga");
      	gridResultadosCarga.generandoArchivo 	= false;
      	gridResultadosCarga.recordId				= null;
      	
      	// Suprimir animacion
      	record.data['EXTRACCION_ARCHIVO_ERRORES_LOADING']  = false;
      	record.data['EXTRACCION_ARCHIVO_ERRORES_URL'] 		= respuesta.urlNombreArchivo;
      	
      	// Mostrar  XLSX
      	gridResultadosCarga.getView().refresh();
			
		} else if(	estado == "FIN"								){
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "29ResultadosComisiones01ext.jsp";
			forma.target	= "_self";
			forma.submit();
 
		}
		
		return;
		
	}
 
	//-------------------------------- PANEL DE RESULTADOS ------------------------------------
	
	var procesarConsultaResultadosCarga = function(store, registros, opts){

		var gridResultadosCarga 				= Ext.getCmp('gridResultadosCarga');
		var el 								= gridResultadosCarga.getGridEl();
		el.unmask();
		
		if (registros != null) {
			
			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
 
		// Limpiar M�scara en la Forma de B�squeda
		resultadosComisiones("ESPERAR_DECISION",null);
		
	}
	
	var resultadosData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'resultadosDataStore',
		url: 		'29ResultadosComisiones01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaResultadosCarga'
		},
		idIndex: 		1,
		fields: [
			{ name: 'FECHA_HORA_OPERACION',							type: 'date',  	convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y H:i:s')); } },
			{ name: 'FOLIO_OPERACION'															},
			{ name: 'SITUACION'																	},
			{ name: 'NUMERO_OPERACIONES_ACEPTADAS',				type: 'int',		convert: function(value,record){ return (Ext.isEmpty(value)?0:Ext.util.Format.number(value,'0,000'));   } },
			{ name: 'NUMERO_OPERACIONES_CON_ERRORES',				type: 'int',		convert: function(value,record){ return (Ext.isEmpty(value)?0:Ext.util.Format.number(value,'0,000'));   } },
			{ name: 'TOTAL_OPERACIONES',								type: 'int',		convert: function(value,record){ return (Ext.isEmpty(value)?0:Ext.util.Format.number(value,'0,000'));   } },
			{ name: 'EXTRACCION_ARCHIVO_ORIGEN',					type: 'boolean'	},
			{ name: 'EXTRACCION_ARCHIVO_RESULTADOS',				type: 'boolean'	},
			{ name: 'EXTRACCION_ARCHIVO_ERRORES',					type: 'boolean'	},
			{ name: 'EXTRACCION_ARCHIVO_ORIGEN_LOADING', 		type: 'boolean'	},
			{ name: 'EXTRACCION_ARCHIVO_RESULTADOS_LOADING',	type: 'boolean'	},
			{ name: 'EXTRACCION_ARCHIVO_ERRORES_LOADING', 		type: 'boolean'	},
			{ name: 'EXTRACCION_ARCHIVO_ORIGEN_URL' 										},
			{ name: 'EXTRACCION_ARCHIVO_RESULTADOS_URL' 									},
			{ name: 'EXTRACCION_ARCHIVO_ERRORES_URL' 										}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
					var panelResultadosCargaComisiones = Ext.getCmp("panelResultadosCargaComisiones");
					if( !panelResultadosCargaComisiones.isVisible() ){
						panelResultadosCargaComisiones.show();
					}
				}
			},
			load: 	procesarConsultaResultadosCarga,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaResultadosCarga(null, null, null);						
				}
			}
		}
		
	});
	

	var elementosResultadosCargaComisiones = [
		{
			store: 		resultadosData,
			xtype: 		'grid',
			id:			'gridResultadosCarga',
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			style: {
				borderWidth: 1
			},
			generandoArchivo: false,
			recordId: 			null,
			columns: [
				{
					header: 		'Fecha y Hora<br>de Operaci�n<br>&nbsp;',
					tooltip: 	'Fecha y Hora de Operaci�n',
					dataIndex: 	'FECHA_HORA_OPERACION',
					sortable: 	true,
					resizable: 	true,
					width: 		110,
					align:		'center',
					xtype: 		'datecolumn', 
					format: 		'd/m/Y H:i:s'
				},
				{
					header: 		'Folio de la<br>Operaci�n<br>&nbsp;',
					tooltip: 	'Folio de la Operaci�n',
					dataIndex: 	'FOLIO_OPERACION',
					sortable: 	true,
					resizable: 	true,
					width: 		100,
					align:		'center'
				},
				{
					header: 		'Situaci�n<br>&nbsp;<br>&nbsp;',
					tooltip: 	'Situaci�n',
					dataIndex: 	'SITUACION',
					sortable: 	true,
					resizable: 	true,
					width: 		170,
					align:		'center'
				},
				{
					header: 		'N�mero de<br>Operaciones<br>Aceptadas',
					tooltip: 	'N�mero de Operaciones Aceptadas',
					dataIndex: 	'NUMERO_OPERACIONES_ACEPTADAS',
					sortable: 	true,
					resizable: 	true,
					width: 		100,
					align:		'center'
				},
				{
					header: 		'N�mero de<br>Operaciones<br>con Errores',
					tooltip: 	'N�mero de Operaciones con Errores',
					dataIndex: 	'NUMERO_OPERACIONES_CON_ERRORES',
					sortable: 	true,
					resizable: 	true,
					width: 		100,
					align:		'center'
				},
				{
					header: 		'Total de<br>Operaciones<br>&nbsp;',
					tooltip: 	'Total de Operaciones',
					dataIndex: 	'TOTAL_OPERACIONES',
					sortable: 	true,
					resizable: 	true,
					width: 		100,
					align:		'center'
				},
				{
					xtype: 		'actiontextcolumn',
					header: 		'Archivo<br>Origen<br>&nbsp;',
					tooltip: 	'Archivo Origen',
					width: 		95,
					hidden: 		false,
					align:		'center',
					id: 			'archivoOrigen',
					items: 		[
							{
								getClass: function(value, metadata, record) { 
									 
									if (record.data['EXTRACCION_ARCHIVO_ORIGEN_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 this.items[0].text    = 'Generando...';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_ARCHIVO_ORIGEN_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo TXT';
										 this.items[0].text    = null;
										 return 'icoTxt16';
									} else if (record.data['EXTRACCION_ARCHIVO_ORIGEN'] ) {
										 this.items[0].tooltip = 'Extraer archivo original';
										 this.items[0].text    = null;
										 return 'extraerDetalle';
									} else {
										 this.items[0].tooltip = null;
										 this.items[0].text    = null;
										 return null;
									}
									
								},
								handler: function(grid, rowIndex, colIndex) {
									
									// Validar que no haya otra consulta en proceso
									if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}
									
									// Obtener ID del Registro en cuestion
									var respuesta 				= new Object();
									var record 					= Ext.StoreMgr.key('resultadosDataStore').getAt(rowIndex);
									respuesta['recordId'] 	= record.id;
									
									// Si el archivo ya fue generado, descargarlo
									if ( !Ext.isEmpty(record.data['EXTRACCION_ARCHIVO_ORIGEN_URL']) ){
										var forma 		= Ext.getDom('formAux');
										forma.action 	= record.data['EXTRACCION_ARCHIVO_ORIGEN_URL'];
										forma.method	= "post";
										forma.target	= "_blank";
										forma.submit();
									// Realizar la extraccion del archivo TXT
									} else {
										resultadosComisiones("EXTRACCION_ARCHIVO_ORIGEN", respuesta );
									}
								
								}
							}
					]
            },
				{
					xtype: 		'actiontextcolumn',
					header: 		'Resultados<br>&nbsp;<br>&nbsp;',
					tooltip: 	'Resultados',
					width: 		95,
					hidden: 		false,
					align:		'center',
					id: 			'archivoResultados',
					items: 		[
							{
								getClass: function(value, metadata, record) { 
									 
									if (record.data['EXTRACCION_ARCHIVO_RESULTADOS_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 this.items[0].text    = 'Generando...';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_ARCHIVO_RESULTADOS_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo PDF';
										 this.items[0].text    = null;
										 return 'icoPdf';
									} else if (record.data['EXTRACCION_ARCHIVO_RESULTADOS'] ) {
										 this.items[0].tooltip = 'Extraer archivo de resultados';
										 this.items[0].text    = null;
										 return 'extraerDetalle';
									} else {
										 this.items[0].tooltip = null;
										 this.items[0].text    = null;
										 return null;
									}
									
								},
								handler: function(grid, rowIndex, colIndex) {
 
									// Validar que no haya otra consulta en proceso
									if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}
 
									// Obtener ID del Registro en cuestion
									var respuesta 				= new Object();
									var record					= Ext.StoreMgr.key('resultadosDataStore').getAt(rowIndex);
									respuesta['recordId'] 	= record.id;
									
									// Si el archivo ya fue generado, descargarlo
									if ( !Ext.isEmpty(record.data['EXTRACCION_ARCHIVO_RESULTADOS_URL']) ){
										var forma 		= Ext.getDom('formAux');
										forma.action 	= record.data['EXTRACCION_ARCHIVO_RESULTADOS_URL'];
										forma.method	= "post";
										forma.target	= "_blank";
										forma.submit();
									// Realizar la extraccion del archivo PDF
									} else {
										resultadosComisiones("EXTRACCION_ARCHIVO_RESULTADOS", respuesta );
									}
									
								}
							}
						]
            },
            {
					xtype: 		'actiontextcolumn',
					header: 		'Archivo<br>de Errores<br>&nbsp;',
					tooltip: 	'Archivo de Errores',
					width: 		95,
					hidden: 		false,
					align:		'center',
					id:			'archivoErrores',
					items: 		[
							{
								getClass: function(value, metadata, record) { 
									 
									if (record.data['EXTRACCION_ARCHIVO_ERRORES_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 this.items[0].text    = 'Generando...';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_ARCHIVO_ERRORES_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo TXT';
										 this.items[0].text    = null;
										 return 'icoTxt16';
									} else if (record.data['EXTRACCION_ARCHIVO_ERRORES'] ) {
										 this.items[0].tooltip = 'Extraer archivo de errores';
										 this.items[0].text    = null;
										 return 'extraerDetalle';
									} else {
										 this.items[0].tooltip = null;
										 this.items[0].text    = null;
										 return null;
									}
									
								},
								handler: function(grid, rowIndex, colIndex) {
 
									// Validar que no haya otra consulta en proceso
									if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}
									
									// Obtener ID del Registro en cuestion
									var respuesta 				= new Object();
									var record					= Ext.StoreMgr.key('resultadosDataStore').getAt(rowIndex);
									respuesta['recordId'] 	= record.id;
									
									// Si el archivo ya fue generado, descargarlo
									if ( !Ext.isEmpty(record.data['EXTRACCION_ARCHIVO_ERRORES_URL']) ){
										var forma 		= Ext.getDom('formAux');
										forma.action 	= record.data['EXTRACCION_ARCHIVO_ERRORES_URL'];
										forma.method	= "post";
										forma.target	= "_blank";
										forma.submit();
									// Realizar la extraccion del archivo TXT
									} else {
										resultadosComisiones("EXTRACCION_ARCHIVO_ERRORES", respuesta );
									}
									
								}
						  }
					]
            }
			]
		}
	];
	
	var panelResultadosCargaComisiones = {
		title:			'Resultados de la Carga de Comisiones',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosCargaComisiones',
		width: 			935,
		frame: 			true,
		style: 			'margin: 0 auto',
		items: 			elementosResultadosCargaComisiones
	}
 
	//-------------------------- PANEL DE CONSULTA EXTRACCIONES -------------------------------
	
	var elementosFormaConsultaResultados = [
		// DATE FIELDS: FECHA DE OPERACI�N
		{
			xtype: 						'compositefield',
			fieldLabel: 				'Fecha de Operaci�n',
			combineErrors: 			false,
			msgTarget: 					'side',
			items: [
				{
					xtype: 				'datefield',
					name: 				'fechaOperacionDe',
					id: 					'fechaOperacionDe',
					allowBlank: 		false,
					startDay: 			0,
					width: 				100,
					msgTarget: 			'side',                              
					vtype: 				'rangofecha',                         
					campoFinFecha: 	'fechaOperacionA',
					margins: 			'0 20 0 0'
				},
				{
					xtype: 				'displayfield',
					value: 				'a',
					width: 				20
				},
				{
					xtype: 				'datefield',
					name: 				'fechaOperacionA',
					id: 					'fechaOperacionA',
					allowBlank: 		false,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoInicioFecha: 'fechaOperacionDe',
					margins: 			'0 20 0 0'  
				}
			]
		}
	];
	
	var panelFormaConsultaResultados = new Ext.form.FormPanel({
		id: 					'panelFormaConsultaResultados',
		width: 				455,
		title: 				'Consulta',
		frame: 				true,
		hidden:				true,
		collapsible: 		true,
		titleCollapse: 	true,
		trackResetOnLoad: true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		labelWidth: 		150,
		defaultType: 		'textfield',
		items: 				elementosFormaConsultaResultados,
		monitorValid: 		false,
		buttons: [
			// BOTON CONSULTAR
			{
				id:				'botonConsultar',
				text: 			'Consultar',
				hidden: 			false,
				errorActivo:	false,
				iconCls: 		'icoBuscar',
				handler: 		function( button, event ) {
					
					var panelFormaConsultaResultados 	= Ext.getCmp("panelFormaConsultaResultados");
					
					if(!panelFormaConsultaResultados.getForm().isValid()){
						return;
					}
					
					var respuesta								= new Object();
					respuesta               				= Ext.apply( respuesta, panelFormaConsultaResultados.getForm().getValues() );
					resultadosComisiones("CONSULTA_RESULTADOS",respuesta);
					
				}
			},
			// BOTON LIMPIAR
			{
				text: 		'Limpiar',
				hidden: 		false,
				iconCls: 	'icoLimpiar',
				handler: 	function( button, event ) {
					/*
					Ext.getCmp("fechaOperacionDe").setMaxValue(null);
					Ext.getCmp("fechaOperacionA").setMinValue(null);
					Ext.getCmp("panelFormaConsultaResultados").getForm().reset();
					*/
					window.location = "29ResultadosComisiones01ext.jsp";
				}
			}
		]
	});
 	
	//------------------------------------ 1. PANEL AVISOS ---------------------------------------
	
	var elementosPanelAvisos = [
		{
			xtype: 	'label',
			id:	 	'labelAviso',
			cls:		'x-form-item',
			style: 	'font-weight:normal;text-align:center;margin:15px;color:black;',
			html:  	''
		}
	];
	
	var panelAvisos = {
		xtype:			'panel',
		id: 				'panelAvisos',
		hidden:			true,
		width: 			700,
		title: 			'Avisos',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosPanelAvisos
	}
	
	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelAvisos,
			panelFormaConsultaResultados,
			NE.util.getEspaciador(10),
			panelResultadosCargaComisiones
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	resultadosComisiones("INICIALIZACION",null);
 
});