<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.pdf.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,	
		netropology.utilerias.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		com.netro.seguridadbean.*,
		com.netro.garantias.*"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%
String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String ic_folio 	= (request.getParameter("ic_folio")	== null)?"":request.getParameter("ic_folio");
String df_fecha_hora 	= (request.getParameter("df_fecha_hora")	== null)?"":request.getParameter("df_fecha_hora");
String claveIF 	= (request.getParameter("claveIF")	== null)?"0":request.getParameter("claveIF");

String numFolios[] = request.getParameterValues("numFolios");
String infoRegresar	= "",  cvePerf ="", consulta ="";
List lRenglones  =new ArrayList (); 

JSONArray registros = new JSONArray();
HashMap info = new HashMap();
JSONObject jsonObj = new JSONObject();

Garantias bean = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

com.netro.seguridadbean.Seguridad ejb = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			



// metodo para sacar la clave del Perfil
cvePerf = ejb.getTipoAfiliadoXPerfil(strPerfil);
if(!"4".equals(cvePerf)){
 claveIF = iNoCliente;
}

if(informacion.equals("valoresIniciales")){ 
 
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("cvePerf", cvePerf);	
	infoRegresar = jsonObj.toString();		
	
 
 }else  if(informacion.equals("catalogoIF")){ 
 
	 if(strTipoUsuario.equals("NAFIN")) {
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setTabla("comcat_if");		
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();	
	 }else  {
		CatalogoIF cat = new CatalogoIF();
		cat.setClave("ic_if");
		cat.setDescripcion("cg_razon_social");
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();	
	
	}

}else   if(informacion.equals("catalogoFolio") &&  !"".equals(claveIF) ){ 
 
	lRenglones = bean.getFoliosFechas(claveIF);  
	List lFolios = (List)lRenglones.get(0);
	if(lFolios.size()>0){
		for(int i=0;i<lFolios.size();i++) {
			String rs_clave = (String)lFolios.get(i);		 	
			info = new HashMap();
			info.put("clave", rs_clave);
			info.put("descripcion", rs_clave);	
			registros.add(info);
		}
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		
}else if(informacion.equals("catalogoFechas") &&  !"".equals(claveIF)) {

	lRenglones = bean.getFoliosFechas(claveIF);  
	List lFechas = (List)lRenglones.get(1);
	if(lFechas.size()>0){
		for(int i=0; i<lFechas.size();i++) {
			String rs_clave = (String)lFechas.get(i);			
			info = new HashMap();
			info.put("clave", rs_clave);
			info.put("descripcion", rs_clave);	
			registros.add(info);
		}
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

}else if(informacion.equals("Consultar")) {
	
	if("".equals(claveIF)){  claveIF = "0";  }  
	
	lRenglones = bean.getSolicitudesPorAutorizar(claveIF, ic_folio, df_fecha_hora);	
	if(lRenglones.size()>0){ 
		for(int i=0;i<lRenglones.size();i++) {
			List datos = (List)lRenglones.get(i);		
			String folio =datos.get(0).toString();
			String tipo_solic = datos.get(1).toString();
			String fecha_hora =datos.get(2).toString();
			String num_opera_MN =datos.get(3).toString();
			String impor_pago_MN =datos.get(5).toString();
			String num_opera_DL =datos.get(4).toString();
			String impor_pago_DL =datos.get(6).toString();
						
			info = new HashMap();
			info.put("FOLIO_SOLIC", folio);
			info.put("TIPO_SOLIC", tipo_solic);
			info.put("FECHA_HORA", fecha_hora);		
			info.put("NUM_OPERACIONES_ACEP_MN", num_opera_MN);
			info.put("IMPORTE_PAGO_MN", impor_pago_MN);
			info.put("NUM_OPERACIONES_ACEP_DL", num_opera_DL);
			info.put("IMPORTE_PAGO_DL", impor_pago_DL);
			info.put("SELECCIONAR", "");	
			registros.add(info);
		}
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

}else if(informacion.equals("ConsPreAcuse")) {

	lRenglones = bean.getSolicitudesSeleccionadas(claveIF, numFolios);
	if(lRenglones.size()>0){ 
		for(int i=0;i<lRenglones.size();i++) {
			List datos = (List)lRenglones.get(i);	
			String folio =datos.get(0).toString();
			String tipo_solic = datos.get(1).toString();
			String fecha_hora =datos.get(2).toString();
			String num_opera_MN =datos.get(3).toString();
			String impor_pago_MN =datos.get(5).toString();
			String num_opera_DL =datos.get(4).toString();
			String impor_pago_DL =datos.get(6).toString();

			info = new HashMap();
			info.put("FOLIO_SOLIC", folio);
			info.put("TIPO_SOLIC", tipo_solic);
			info.put("FECHA_HORA", fecha_hora);		
			info.put("NUM_OPERACIONES_ACEP_MN", num_opera_MN);
			info.put("IMPORTE_PAGO_MN", impor_pago_MN);
			info.put("NUM_OPERACIONES_ACEP_DL", num_opera_DL);
			info.put("IMPORTE_PAGO_DL", impor_pago_DL);
			info.put("SELECCIONAR", "");	
			registros.add(info);			
		}
	}	
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

}else if(informacion.equals("GeneraAcuse")) {

	String folioCert = "",  _acuse = "N", horaHoy ="", mensaje ="";
	String pkcs7 = request.getParameter("pkcs7");	
	String externContent = request.getParameter("textoFirmar");
	char getReceipt = 'Y';	
	Acuse acuse = null;		
	horaHoy= (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
		
	try{	
		netropology.utilerias.Seguridad s = new netropology.utilerias.Seguridad();
		if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			folioCert = "02CSAUT"+iNoCliente+new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
				_acuse = s.getAcuse();			
					bean.setConfirmarSolicitudes(_acuse, iNoUsuario, claveIF, numFolios);
				mensaje = "<b>La autentificación se llevo a cabo con éxito  <b>Recibo:"+_acuse+"</b>";
			
			}else { //autenticación fallida
				mensaje = "<b>La autentificación no se llevo a cabo "+s.mostrarError()+"</b>";
			}
		} else { //autenticación fallida
			mensaje = "<b>La autentificación no se llevo a cabo "+s.mostrarError()+"</b>";
		}	
	}catch(Exception e){
		out.println("Error en jsp");
		System.out.println("error:::"+e);
		e.printStackTrace();
	}finally{
		
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", mensaje);	
	jsonObj.put("acuse", _acuse);	
	infoRegresar = jsonObj.toString();		
	

}else if(informacion.equals("ConsultaAcuse")) {

	lRenglones = bean.getSolicitudesConfirmadas(claveIF, numFolios);
	
	if(lRenglones.size()>0){ 
		for(int i=0;i<lRenglones.size();i++) {
			List datos = (List)lRenglones.get(i);	
			String folio =datos.get(0).toString();
			String tipo_solic = datos.get(1).toString();
			String fecha_hora =datos.get(2).toString();
			String num_opera_MN =datos.get(3).toString();
			String impor_pago_MN =datos.get(5).toString();
			String num_opera_DL =datos.get(4).toString();
			String impor_pago_DL =datos.get(6).toString();

			info = new HashMap();
			info.put("FOLIO_SOLIC", folio);
			info.put("TIPO_SOLIC", tipo_solic);
			info.put("FECHA_HORA", fecha_hora);		
			info.put("NUM_OPERACIONES_ACEP_MN", num_opera_MN);
			info.put("IMPORTE_PAGO_MN", impor_pago_MN);
			info.put("NUM_OPERACIONES_ACEP_DL", num_opera_DL);
			info.put("IMPORTE_PAGO_DL", impor_pago_DL);
			info.put("SELECCIONAR", "");	
			registros.add(info);			
		}
	}

	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

}else if(informacion.equals("GenerarPDF")) {

	String recibo 	= (request.getParameter("recibo")	== null)?"":request.getParameter("recibo");
	List  parametros = new ArrayList();

	parametros= new ArrayList();
	parametros.add(claveIF);
	parametros.add(recibo);
	parametros.add(strDirectorioTemp);
	parametros.add((String)session.getAttribute("strPais"));
	parametros.add(strNombre);
	parametros.add(strNombreUsuario);
	parametros.add(strLogo);
	parametros.add(strDirectorioPublicacion);
	parametros.add(iNoCliente);
	
	String  nombreArchivo = this.archivAcusePDF(parametros, numFolios );

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();		


}

	
%>

<%=infoRegresar%>

<%!
	//** Metodo para generar el PDF de acuse */
public String  archivAcusePDF(List parametros, String numFolios[]  )  {
		
		AccesoDB con =new AccesoDB();
		CreaArchivo archivo 				= new CreaArchivo();
		StringBuffer contenidoArchivo 	= new StringBuffer();
		String 	nombreArchivo 		= null;		
		String query = "";	
		ComunesPDF pdfDoc = new ComunesPDF();
		
		String  claveIF = parametros.get(0).toString();	
		String  recibo = parametros.get(1).toString();	
		String  strDirectorioTemp  = parametros.get(2).toString();
		String pais = parametros.get(3).toString();
		String nombre = parametros.get(4).toString();
		String nombreUsr = parametros.get(5).toString();
		String logo = parametros.get(6).toString();
		String strDirectorioPublicacion = parametros.get(7).toString();
		String noCliente  = parametros.get(8).toString();	
        nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		try{	
			
			con.conexionDB();
			
			Garantias bean = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

		
			pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String nombreMesIngles[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
			pdfDoc.encabezadoConImagenes(pdfDoc,pais, noCliente, noCliente , nombre, nombreUsr, logo, strDirectorioPublicacion);
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	
			pdfDoc.addText("\n\n","formas",ComunesPDF.RIGHT);
			
			pdfDoc.addText("La autentificación se llevó a cabo con éxito\nRecibo:"+recibo,"formas",ComunesPDF.CENTER);	
		
			pdfDoc.addText("\n\n","formas",ComunesPDF.RIGHT);
			pdfDoc.setTable(6,100);			
			pdfDoc.setCell("Folio de Solicitud","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Solicitud","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha y Hora de Operación","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Número de operaciones aceptadas","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Importe de pago de comisiones e IVA","celda01",ComunesPDF.CENTER);
	
			
			List renglones = bean.getSolicitudesConfirmadas(claveIF, numFolios);
			
			for(int i=0;i<renglones.size();i++) {
				List datos = (List)renglones.get(i);
				
				pdfDoc.setCell(datos.get(0).toString(),"formas",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell(datos.get(1).toString(),"formas",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell(datos.get(2).toString(),"formas",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Moneda Nacional","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(datos.get(3).toString(),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(datos.get(5).toString(),2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("Dólares","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(datos.get(4).toString(),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(datos.get(6).toString(),2),"formas",ComunesPDF.CENTER);
		  
			}  
			pdfDoc.addTable();
			pdfDoc.endDocument();
				  			
		} catch(Exception e) { 
			e.printStackTrace();
		//	throw new NafinException("SIST0001");
		} finally {				
			
			if(con.hayConexionAbierta())  con.cierraConexionDB();	
		}
			return nombreArchivo;
	}
	
%>