<!DOCTYPE html>
<%@ page import="
		java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%@ include file="/00utils/componente_firma.jspf" %>

<script type="text/javascript" src="29forma4ext.js?<%=session.getId()%>"></script>

</head>

<%

if(esEsquemaExtJS   && strTipoUsuario.equals("IF") ) {%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01if/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01if/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
<% } else if(esEsquemaExtJS   && strTipoUsuario.equals("NAFIN") ) {%>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
		<div id="Contcentral">	
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
			<div id="areaContenido"><div style="height:230px"></div>
		</div>                                                                                     
		</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>		
	</body>
<% } else {%> 

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<div id='areaContenido' style="margin-left: 3px; margin-top: 3px;"></div>
		<form id='formAux' name="formAux" target='_new'></form>		
		
	</body>	

<%}%>

</html>