var showPanelLayoutDeCarga;

Ext.onReady(function() {

	//----------------------------------- HANDLERS ------------------------------------
	
	var procesaMonitoreoCierreRecepcion = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						monitoreoCierreRecepcion(resp.estadoSiguiente,resp);
					}
				);
			} else {
				monitoreoCierreRecepcion(resp.estadoSiguiente,resp);
			}
			
		} else {
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError( 
				response,
				opts, 
				function(){
					
					var gridCierreRecepcion = Ext.getCmp("gridCierreRecepcion");
					
					if( gridCierreRecepcion.generandoArchivo && !Ext.isEmpty(gridCierreRecepcion.recordId) ){
 
						// Obtener registro
						var record			= 	Ext.StoreMgr.key('cierreRecepcionDataStore').getById(gridCierreRecepcion.recordId);
						if( Ext.isEmpty(record) ){
							return;
						}
						
						// Deshabilitar flag que indica que hay una consulta en proceso
						gridCierreRecepcion.generandoArchivo = false;
						gridCierreRecepcion.recordId			= null;
						
						// Suprimir animacion
						if( record.data['EXTRACCION_RESULTADOS_CIERRE_LOADING'] ){
							record.data['EXTRACCION_RESULTADOS_CIERRE_LOADING'] 	= false;
						} else if (record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_LOADING']){
							record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_LOADING'] = false;
						}
						gridCierreRecepcion.getView().refresh();
						
					}
					
				} 
			);  
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	
	var monitoreoCierreRecepcion = function(estado, respuesta ){
 
		if(			estado == "INICIALIZACION"					){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29cierre_recepcion01ext.data.jsp',
				params: 	{
					informacion:		'MonitoreoCierreRecepcion.inicializacion'
				},
				callback: 				procesaMonitoreoCierreRecepcion
			});
			
		} else if(	estado == "MOSTRAR_DETALLE_CIERRE_RECEPCION"		  ){
			
			Ext.getCmp("contenedorPrincipal").enable();
			
			// Habilitar grid y mostra mensaje de espera mientras se cargan los registros
			var gridCierreRecepcion = Ext.getCmp("gridCierreRecepcion");
			gridCierreRecepcion.enable();
			gridCierreRecepcion.getGridEl().mask("Cargando...",'x-mask-loading');
			
			// Extraer portafolio
			var cierreRecepcionDataStore = Ext.StoreMgr.key("cierreRecepcionDataStore");
			cierreRecepcionDataStore.load();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29cierre_recepcion01ext.data.jsp',
				params: 	{
					informacion:		'MonitoreoCierreRecepcion.mostrarDetalleCierreRecepcion'
				},
				callback: 				procesaMonitoreoCierreRecepcion
			});
 
      } else if(  estado == "ESPERAR_DECISION"  			){
      	
      	var gridCierreRecepcion = Ext.getCmp("gridCierreRecepcion");
      	if( !Ext.isEmpty( respuesta.recordId ) && gridCierreRecepcion.generandoArchivo ){
 
      		// Obtener registro
      		var record			= 	Ext.StoreMgr.key('cierreRecepcionDataStore').getById(respuesta.recordId);
      		if(Ext.isEmpty(record)){
      			return;
      		}
      		
      		// Deshabilitar flag que indica que hay una consulta en proceso
      		gridCierreRecepcion.generandoArchivo 	= false;
      		gridCierreRecepcion.recordId				= null;
      		
      		// Suprimer animacion
      		if( record.data['EXTRACCION_RESULTADOS_CIERRE_LOADING'] ){
      			record.data['EXTRACCION_RESULTADOS_CIERRE_LOADING'] 		= false;
      		} else if (record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_LOADING']){
      			record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_LOADING'] = false;
      		}
      		gridCierreRecepcion.getView().refresh();
      		
      	}
      	
      	return;
      	
      } else if(	estado == "EXTRACCION_RESULTADOS_CIERRE"		){
      	
      	// Generar Texto a Firmar
      	var record			= 	Ext.StoreMgr.key('cierreRecepcionDataStore').getById(respuesta.recordId);
 
      	// Firmar contenido del portafolio a extraer
      	var textoFirmar	= 	"";
      	var pkcs7 			=	null;
      	if(!record.data['HAY_ACUSE_DE_RECIBO']){
      		
      		// Construir texto a firmar
      		textoFirmar += "A�o de Calificaci�n|Trimestre Calificaci�n|Total de garant�as sujetas a env�o de calificaci�n|N�mero de garant�as recibidas|N�mero de garant�as No recibidas|Situaci�n de la recepci�n\n";
				textoFirmar += "\n";
				textoFirmar	+= record.data["ANIO"]									+ "|" + 
									record.data["TRIMESTRE"]						+ "|" +
									record.data["TOTAL_GARANTIAS"]					+ "|" +
									record.data["NUMERO_GARANTIAS_RECIBIDAS"]		+ "|" +
									record.data["NUMERO_GARANTIAS_NO_RECIBIDAS"]	+ "|" +
									record.data["SITUACION_RECEPCION"]				+ "\n";
	
				// Solicitar al usuario que firme el texto
				NE.util.obtenerPKCS7(confirmar, textoFirmar, record, respuesta.recordId);
				
      	}

			
		} else if(	estado == "EXTRACCION_ARCHIVO_INCUMPLIMIENTO"	){
 
			// Obtener detalle del portafolio
      	var record			= 	Ext.StoreMgr.key('cierreRecepcionDataStore').getById(respuesta.recordId);
      	
      	// Indicar que hay una consulta en proceso
      	var gridCierreRecepcion 					= Ext.getCmp("gridCierreRecepcion");
      	gridCierreRecepcion.generandoArchivo 	= true;
      	gridCierreRecepcion.recordId				= respuesta.recordId;
      	
      	// Mostrar animacion
      	record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_LOADING'] = true;
      	gridCierreRecepcion.getView().refresh();
      	
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 							'29cierre_recepcion01ext.data.jsp',
				params: {	
					informacion:			'MonitoreoCierreRecepcion.extraccionArchivoIncumplimiento',
					recordId:				respuesta.recordId,
					anio:						record.data['ANIO'],
					trimestre:				record.data['TRIMESTRE']
				},
				callback: 					procesaMonitoreoCierreRecepcion
			});
			
		} else if(	estado == "MOSTRAR_LINK_PDF_RESULTADOS" 	){
			
			// Obtener detalle del portafolio
      	var record			= 	Ext.StoreMgr.key('cierreRecepcionDataStore').getById(respuesta.recordId);
      	
      	// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridCierreRecepcion 					= Ext.getCmp("gridCierreRecepcion");
      	gridCierreRecepcion.generandoArchivo 	= false;
      	gridCierreRecepcion.recordId				= null;
      	
      	// Suprimir animacion
      	record.data['EXTRACCION_RESULTADOS_CIERRE_LOADING'] 	= false;
      	record.data['HAY_ACUSE_DE_RECIBO'] 							= respuesta.hayAcuseDeRecibo;
      	record.data['EXTRACCION_RESULTADOS_CIERRE_URL'] 		= respuesta.urlNombreArchivo;
 
      	// Mostrar  PDF
      	gridCierreRecepcion.getView().refresh();
      	
		} else if(	estado == "MOSTRAR_LINK_PDF_INCUMPLIMIENTO" 		){
			
			// Obtener detalle del portafolio
      	var record			= 	Ext.StoreMgr.key('cierreRecepcionDataStore').getById(respuesta.recordId);
      	
      	// Deshabilitar flag que indica que hay una consulta en proceso
      	var gridCierreRecepcion 					= Ext.getCmp("gridCierreRecepcion");
      	gridCierreRecepcion.generandoArchivo 	= false;
      	gridCierreRecepcion.recordId				= null;
      	
      	// Suprimir animacion
      	record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_LOADING'] = false;
      	record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_URL'] 		= respuesta.urlNombreArchivo;
      	
      	// Mostrar  PDF
      	gridCierreRecepcion.getView().refresh();
      	
		} else if(	estado == "FIN"									){
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "29cierre_recepcion01ext.jsp";
			forma.target	= "_self";
			forma.submit();
 
		}
		
		return;
		
	}

	var confirmar = function(pkcs7, textoFirmar, record, recordId) {
		
      	// Indicar que hay una consulta en proceso
      	var gridCierreRecepcion 					= Ext.getCmp("gridCierreRecepcion");
      	gridCierreRecepcion.generandoArchivo 	= true;
      	gridCierreRecepcion.recordId				= recordId;
      	
      	// Mostrar animacion
      	record.data['EXTRACCION_RESULTADOS_CIERRE_LOADING'] = true;
      	gridCierreRecepcion.getView().refresh();
      	
         // Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29cierre_recepcion01ext.data.jsp',
				params: {	
					informacion:			'MonitoreoCierreRecepcion.extraccionResultadosCierre',
					recordId:				recordId,
					hayAcuseDeRecibo: 	record.data['HAY_ACUSE_DE_RECIBO'],
					anio:						record.data['ANIO'],
					trimestre:				record.data['TRIMESTRE'],
					fechaCorte: 			record.data['FECHA_CORTE'],
					isEmptyPkcs7: 			Ext.isEmpty(pkcs7),
					pkcs7: 					pkcs7,
					textoFirmado: 			textoFirmar
				},
				callback: 					procesaMonitoreoCierreRecepcion
			}); 
	}
	//-------------------------- PANEL PORTAFOLIOS ----------------------------
	
	var procesarConsultaCierreRecepcion = function(store, registros, opts){

		var gridCierreRecepcion 		= Ext.getCmp('gridCierreRecepcion');
		var el 								= gridCierreRecepcion.getGridEl();
		el.unmask();
		
		if (registros != null) {
			
			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
		
		// Si ocurri� un error deshabilitar el gridpanel
		if( store == null && registros == null && opts == null){
			gridCierreRecepcion.setDisabled(true);
		}
		
	}
	
	var cierreRecepcionData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'cierreRecepcionDataStore',
		url: 		'29cierre_recepcion01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaCierreRecepcion'
		},
		fields: [
			{ name: 'ANIO',  													type: 'int'			}, 
			{ name: 'TRIMESTRE', 											type: 'int'			}, 
			{ name: 'TOTAL_GARANTIAS', 									type: 'int',	convert: function(value,record){ return (Ext.isEmpty(value)?0:Ext.util.Format.number(value,'0,000')); } }, 
			{ name: 'NUMERO_GARANTIAS_RECIBIDAS', 						type: 'int',	convert: function(value,record){ return (Ext.isEmpty(value)?0:Ext.util.Format.number(value,'0,000')); } }, 
			{ name: 'NUMERO_GARANTIAS_NO_RECIBIDAS', 					type: 'int',	convert: function(value,record){ return (Ext.isEmpty(value)?0:Ext.util.Format.number(value,'0,000')); } },
			{ name: 'SITUACION_RECEPCION' 														},
			{ name: 'FECHA_CORTE'																	},
			{ name: 'EXTRACCION_RESULTADOS_CIERRE', 					type: 'boolean'	},
			{ name: 'HAY_ACUSE_DE_RECIBO', 								type: 'boolean' 	},
			{ name: 'EXTRACCION_ARCHIVO_INCUMPLIMIENTO',				type: 'boolean'   },
			{ name: 'EXTRACCION_RESULTADOS_CIERRE_LOADING', 		type: 'boolean'	},
			{ name: 'EXTRACCION_ARCHIVO_INCUMPLIMIENTO_LOADING', 	type: 'boolean'	},
			{ name: 'EXTRACCION_RESULTADOS_CIERRE_URL' 										},
			{ name: 'EXTRACCION_ARCHIVO_INCUMPLIMIENTO_URL' 								}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarConsultaCierreRecepcion,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCierreRecepcion(null, null, null);						
				}
			}
		}
		
	});
	
	var elementosCierreRecepcion = [
		{
			store: 		cierreRecepcionData,
			xtype: 		'grid',
			id:			'gridCierreRecepcion',
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			style: {
				borderWidth: 1
			},
			generandoArchivo: false,
			recordId: 			null,
			columns: [
				{
					header: 		'A�o de<br>Calificaci�n<br>&nbsp;',
					tooltip: 	'A�o de Calificaci�n',
					dataIndex: 	'ANIO',
					sortable: 	true,
					resizable: 	true,
					width: 		85,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'Trimestre<br>Calificaci�n<br>&nbsp;',
					tooltip: 	'Trimestre Calificaci�n',
					dataIndex: 	'TRIMESTRE',
					sortable: 	true,
					resizable: 	true,
					width: 		85,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'Total de garant�as<br>sujetas a env�o<br>de calificaci�n',
					tooltip: 	'Total de garant�as sujetas a env�o de calificaci�n',
					dataIndex: 	'TOTAL_GARANTIAS',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'N�mero de<br>garant�as<br>recibidas',
					tooltip: 	'N�mero de garant�as recibidas',
					dataIndex: 	'NUMERO_GARANTIAS_RECIBIDAS',
					sortable: 	true,
					resizable: 	true,
					width: 		85,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'N�mero de garant�as<br>No<br>recibidas',
					tooltip: 	'N�mero de garant�as No recibidas',
					dataIndex: 	'NUMERO_GARANTIAS_NO_RECIBIDAS',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					hidden: 		false,
					align:		'center'
				},
				{
					header: 		'Situaci�n<br>de la<br>recepci�n',
					tooltip: 	'Situaci�n de la recepci�n',
					dataIndex: 	'SITUACION_RECEPCION',
					sortable: 	true,
					resizable: 	true,
					width: 		95,
					hidden: 		false,
					align:		'center'
				},
				{
					xtype: 		'actioncolumn',
					header: 		'Archivo de<br>resultados<br>de cierre',
					tooltip: 	'Archivo de resultados de cierre',
					width: 		88,
					hidden: 		false,
					align:		'center',
					items: 		[
							{
								getClass: function(value, metadata, record) { 
									 
									if (record.data['EXTRACCION_RESULTADOS_CIERRE_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_RESULTADOS_CIERRE_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo PDF';
										 return 'icoPdf';
									} else if (record.data['EXTRACCION_RESULTADOS_CIERRE'] ) {
										 this.items[0].tooltip = 'Generar archivo de Resultados';
										 return 'extraerDetalle';
									} else {
										 return null;
									}
									
								},
								handler: function(grid, rowIndex, colIndex) {
									
									// Validar que no haya otra consulta en proceso
									if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}
									
									var record 			= Ext.StoreMgr.key('cierreRecepcionDataStore').getAt(rowIndex);
									if(!record.data['HAY_ACUSE_DE_RECIBO']){
 
										var faltanDatos 	= false;
										if(        Ext.isEmpty(record.data["ANIO"])                          ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["TRIMESTRE"])                     ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["TOTAL_GARANTIAS"])               ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["NUMERO_GARANTIAS_RECIBIDAS"])    ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["NUMERO_GARANTIAS_NO_RECIBIDAS"]) ){
											faltanDatos 	= true;
										} else if( Ext.isEmpty(record.data["SITUACION_RECEPCION"])           ){
											faltanDatos 	= true;
										}
										
										if( faltanDatos ){
											Ext.Msg.alert("Aviso","No pueden haber campos vac�os en los datos a firmar");
											return;
										}
										
									}
									// Obtener ID del Registro en cuestion
									var respuesta 				= new Object();
									respuesta['recordId'] 	= record.id;
									
									if ( !Ext.isEmpty(record.data['EXTRACCION_RESULTADOS_CIERRE_URL']) ){
										
										var forma    = Ext.getDom('formAux');
										forma.action = NE.appWebContextRoot+'/DescargaArchivo';
										forma.method = 'post';
										forma.target = '_self';
										// Preparar Archivo a Descargar
										var archivo  = record.data['EXTRACCION_RESULTADOS_CIERRE_URL'];				
										archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
										// Insertar archivo
										var inputNombreArchivo = Ext.DomHelper.insertFirst(
											forma, 
											{ 
												tag: 	'input', 
												type: 'hidden', 
												id: 	'nombreArchivo', 
												name: 'nombreArchivo', 
												value: archivo
											},
											true
										); 
										// Solicitar Archivo Al Servidor
										forma.submit();
										// Remover nodo agregado
										inputNombreArchivo.remove();
										
									} else {
										// Realizar la extraccion consolidada
										monitoreoCierreRecepcion("EXTRACCION_RESULTADOS_CIERRE", respuesta );
									}
									
								}
								
							}
						]
            },
				{
					xtype: 		'actioncolumn',
					header: 		'Archivo de<br>incumplimiento<br>&nbsp;',
					tooltip: 	'Archivo de incumplimiento',
					width: 		100,
					hidden: 		false,
					align:		'center',
					items: 		[
							{
								getClass: function(value, metadata, record) { 
									 
									if (record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_LOADING'] ) {
										 this.items[0].tooltip = 'Operaci�n en proceso';
										 return 'realizandoActividad';
									} else if ( !Ext.isEmpty(record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_URL']) ){
										 this.items[0].tooltip = 'Descargar Archivo PDF';
										 return 'icoPdf';
									} else if (record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO'] ) {
										 this.items[0].tooltip = 'Extraer detalle en formato PDF';
										 return 'extraerDetalle';
									} else {
										 return null;
									}
									
								},
								handler: function(grid, rowIndex, colIndex) {
									
									// Validar que no haya otra consulta en proceso
									if( grid.generandoArchivo ){
										Ext.Msg.alert("Aviso","Actualmente hay una consulta en proceso, espere a que termine la consulta para realizar otra.");
										return;
									}
 
									// Obtener ID del Registro en cuestion
									var respuesta 				= new Object();
									var record					= Ext.StoreMgr.key('cierreRecepcionDataStore').getAt(rowIndex);
									respuesta['recordId'] 	= record.id;
									
									if ( !Ext.isEmpty(record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_URL']) ){

										var forma    = Ext.getDom('formAux');
										forma.action = NE.appWebContextRoot+'/DescargaArchivo';
										forma.method = 'post';
										forma.target = '_self';
										// Preparar Archivo a Descargar
										var archivo  = record.data['EXTRACCION_ARCHIVO_INCUMPLIMIENTO_URL'];				
										archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
										// Insertar archivo
										var inputNombreArchivo = Ext.DomHelper.insertFirst(
											forma, 
											{ 
												tag: 	'input', 
												type: 'hidden', 
												id: 	'nombreArchivo', 
												name: 'nombreArchivo', 
												value: archivo
											},
											true
										); 
										// Solicitar Archivo Al Servidor
										forma.submit();
										// Remover nodo agregado
										inputNombreArchivo.remove();
										
									} else {
										// Realizar la extraccion consolidada
										monitoreoCierreRecepcion("EXTRACCION_ARCHIVO_INCUMPLIMIENTO", respuesta );
									}
									
								}
							}
						]
            }
			]
		}
	];
	
	var panelCierreRecepcion = {
		title:			'Cierre de Recepci�n de Calificaciones',
		hidden:			false,
		xtype:			'panel',
		id: 				'panelCierreRecepcion',
		width: 			810,
		frame: 			true,
		style: 			'margin: 0 auto',
		items: 			elementosCierreRecepcion
	}
 
	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		disabled: true,
		items: 	[
			NE.util.getEspaciador(10),
			panelCierreRecepcion
		]
	});
	
	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------
	monitoreoCierreRecepcion("INICIALIZACION", null );
			
});