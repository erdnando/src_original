<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*,
		com.netro.afiliacion.ConsCargaProvEcon, 
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.afiliacion.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("CargaArchivo.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;
	
	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
				
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){
 
		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}
	
	// 2. Validar Numero Fiso
	if (!hayAviso && !seguridad.validaNumeroFISO(claveIF) ) {
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se tiene definido un Fideicomiso para realizar sus operaciones.<br/><br/>Por favor comuníquese al Centro de Atención a clientes al teléfono 50-89-61-07 <br/>o del Interior al 01-800-NAFINSA (01-800-6234672).";
		hayAviso				= true;
	}

	// 3. Remover variables de sesion
	if ( !hayAviso ){
		session.removeAttribute("rgprint");
	}
	
	// 4. Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
				
	} catch(Exception e) {
	 
		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	/* 5. VALIDAR QUE:
	 	Para realizar la Carga de Archivos el portafolio deberá tener el acuse de cierre 
	 	del trimestre anterior y el estatus "Extraído" para el trimestre actual 
	*/ 
	if(!hayAviso && !garantias.esStatusDeRecepcionDePortafolioDelTrimestreAnteriorValido(claveIF)){
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso 				= "Se debe extraer el portafolio a calificar antes de iniciar la carga de calificación.";
		hayAviso				= true;
	}else	if(!hayAviso && !garantias.esStatusDeAcuseDeReciboDelTrimestreAnteAnteriorValido(claveIF)){
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso 				= "Se requiere dar acuse de recibo del trimestre anterior antes de iniciar la carga de la calificaci&oacute;n<br/>del trimestre en curso.";
		hayAviso				= true;
	} 
	
	// Definir el layout a mostrar ( en caso de que las validaciones sean exitosas )
	if(!hayAviso){
		
		
		String longitudClaveFinanciamiento = "20"; // garantias.operaProgramaSHFCreditoHipotecario(claveIF)?"30":"20";
				 												 // A indicación de lmrojas se sobreescribe la longitud de la clave de financiamiento a 20 caracteres.
		String calificacionLayout =
			"<table>"  +
			"<tr>"  +
			"	<td class=\"titulos\" align=\"center\">"  +
			"		<span class=\"titulos\">"  +
			"			Estructura de Archivo<br>Carga masiva"  +
			"		</span>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td colspan=\"2\" align=\"center\">"  +
			"		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" >"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"center\" colspan=\"10\"  style=\"height:30px;\" >"  +
			"					Layout Nueva Metodolog&iacute;a de Calificaci&oacute;n<br>"  +
			"					Archivo de extensi&oacute;n TXT &oacute; ZIP; los campos deben de estar separados por arrobas (\"@\")"  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"center\" width=\"25px\"  style=\"height:30px;\" >No.</td>"  +
			"				<td class=\"celda01\" align=\"center\">Nombre del Campo</td>"  +
			"				<td class=\"celda01\" align=\"center\">Tipo</td>"  +
			"				<td class=\"celda01\" align=\"center\">Longitud<br>M&aacute;xima</td>"  +
			"				<td class=\"celda01\" align=\"center\">Obligatorio</td>"  +
			"				<td class=\"celda01\" align=\"center\">Observaciones</td>"  +
			"			</tr>"  +		
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >1</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Clave del Intermediario Financiero SIAG "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">5</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					N&uacute;mero de identificaci&oacute;n del Intermediario "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >2</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					A&ntilde;o Trimestre de Calificaci&oacute;n "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">4</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					&nbsp; "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >3</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Trimestre de Calificaci&oacute;n "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">1</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					&nbsp; "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >4</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Clave del Cr&eacute;dito "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">20</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					N&uacute;mero de pr&eacute;stamo "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >5</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					RFC del Acreditado "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">13</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					RFC completo del cliente, sin espacios ni guiones "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >6</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Tipo de Cartera "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">3</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					&nbsp; "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >7</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Probabilidad de Incumplimiento (PI) "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">9,6</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Porcentaje redondeado a seis decimales; debe estar entre 0 y 100 "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >8</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Severidad de la P&eacute;rdida (%SP) "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">9,6</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Porcentaje redondeado a seis decimales; debe estar entre 0 y 100 "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >9</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Exposici&oacute;n al Incumplimiento (EI) "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">17,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Se redondea a dos decimales "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >10</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Porcentaje de Reservas (%PE) "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">9,6</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Porcentaje redondeado a seis decimales; debe estar entre 0 y 100 "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >11</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Nivel de Riesgo "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">4</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					&nbsp; "  +
			"				</td>"  +
			"			</tr>"  +
			"		</table>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"</table>";
			
		resultado.put("calificacionLayout", calificacionLayout );
			
	}
	
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_SOLICITUD_DE_CARGA";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();

} else if (          informacion.equals("CargaArchivo.enviarSolicitudDeCarga") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	
	String claveAnio 									= (request.getParameter("claveAnio")								== null)?"":request.getParameter("claveAnio");
	String claveTrimestre 							= (request.getParameter("claveTrimestre")							== null)?"":request.getParameter("claveTrimestre");
	String numeroRegistros 							= (request.getParameter("numeroRegistros")						== null)?"":request.getParameter("numeroRegistros");
	String numeroSumatoriaExposicionIncumplimiento 	= (request.getParameter("numeroSumatoriaExposicionIncumplimiento")	== null)?"":request.getParameter("numeroSumatoriaExposicionIncumplimiento");
	
	resultado.put("claveAnio", 								claveAnio);
	resultado.put("claveTrimestre", 							claveTrimestre);
	resultado.put("numeroRegistros", 						numeroRegistros);
	resultado.put("numeroSumatoriaExposicionIncumplimiento",	numeroSumatoriaExposicionIncumplimiento);
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	"ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES" 		);
	infoRegresar = resultado.toString();
	
} else if (        	informacion.equals("CargaArchivo.subirArchivo") 				)	{	
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
 
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(8097152);
		myUpload.upload();
			
	} catch(Exception e) {
			
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 8097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 8 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
 
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	String claveAnio 									= (myRequest.getParameter("claveAnio1")									== null)?"":myRequest.getParameter("claveAnio1");
	String claveTrimestre 							= (myRequest.getParameter("claveTrimestre1")								== null)?"":myRequest.getParameter("claveTrimestre1");
	String numeroRegistros 							= (myRequest.getParameter("numeroRegistros1")							== null)?"":myRequest.getParameter("numeroRegistros1");
	String numeroSumatoriaExposicionIncumplimiento 	= (myRequest.getParameter("numeroSumatoriaExposicionIncumplimiento1")		== null)?"":myRequest.getParameter("numeroSumatoriaExposicionIncumplimiento1");
 
	// Guardar Archivo en Disco
	int numFiles = 0;
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		String nuevoNombre = Comunes.cadenaAleatoria(16)+"."+myFile.getFileExt();;
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + nuevoNombre	 );
		resultado.put("fileName", 	nuevoNombre			);
		
	}catch(Exception e){
		
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
	
	// Enviar parametros adicionales
	resultado.put("claveAnio", 								claveAnio);
	resultado.put("claveTrimestre", 							claveTrimestre);
	resultado.put("numeroRegistros", 						numeroRegistros);
	resultado.put("numeroSumatoriaExposicionIncumplimiento",	numeroSumatoriaExposicionIncumplimiento);
 
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "REALIZAR_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.realizarValidacion") 				)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveAnio 								= (request.getParameter("claveAnio")									== null)?"":request.getParameter("claveAnio");
	String 		claveTrimestre 						= (request.getParameter("claveTrimestre")								== null)?"":request.getParameter("claveTrimestre");
	String 		numeroRegistros 						= (request.getParameter("numeroRegistros")							== null)?"":request.getParameter("numeroRegistros");
	String 		numeroSumatoriaExposicionIncumplimiento = (request.getParameter("numeroSumatoriaExposicionIncumplimiento")		== null)?"":request.getParameter("numeroSumatoriaExposicionIncumplimiento");
	String 		fileName 								= (request.getParameter("fileName")										== null)?"":request.getParameter("fileName");
	
	String 		rutaArchivo 							= strDirectorioTemp + iNoUsuario + "." + fileName	;
	String 		claveIF 									= iNoCliente;
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
			
	} catch(Exception e) {
	 
		log.error("CargaArchivo.realizarValidacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
		
	}
			
	// Realizar validacion
	ResultadosGar 	rg 	= garantias.procesarAltaCalif(
									strDirectorioTemp,
									rutaArchivo,
									claveAnio,
									claveTrimestre,
									numeroRegistros,
									numeroSumatoriaExposicionIncumplimiento,
									claveIF
								);

	// Leer Resultado de la Validacion
	String claveProceso 											= String.valueOf(rg.getIcProceso());
	String totalRegistros										= String.valueOf(rg.getNumRegOk());
	
	//  Leer Resultado de la Validacion ( Registros sin Errores )
	String 		 solicitudesCorrectas 							= rg.getCorrectos();
	
		StringBuffer 	buffer 	= new StringBuffer();
		JSONArray		solicitudesCorrectasDataArray = new JSONArray();
		int				ctaRegistros = 0;
		
		for(int i=0;i<solicitudesCorrectas.length();i++){
			char lastChar = solicitudesCorrectas.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				solicitudesCorrectasDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			solicitudesCorrectasDataArray.add(registro);
			buffer.setLength(0);
		}
		// Si el primer registro dice: Numeros de Credito, borrarlo
		if( 	solicitudesCorrectasDataArray.size() > 0 ){
				
			JSONArray registro = (JSONArray) solicitudesCorrectasDataArray.get(0);
			if("Numeros de Credito:".equals( registro.getString(1) )){
				solicitudesCorrectasDataArray.remove(0);
			}
			
		}
		
	String numeroRegistrosSinErrores 						= String.valueOf(rg.getNumRegOk());
	String numeroImportesExposicionIncumplimientoSinErrores	= Comunes.formatoDecimal(rg.getSumRegOk(),2);
	
	//  Leer Resultado de la Validacion ( Registros con Errores )
	String solicitudesConErrores = 				rg.getErrores();

		buffer 			= new StringBuffer();
		JSONArray		solicitudesConErroresDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<solicitudesConErrores.length();i++){
			char lastChar = solicitudesConErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				solicitudesConErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			solicitudesConErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		
	String numeroRegistrosConErrores 						= String.valueOf(rg.getNumRegErr());
	String numeroImportesExposicionIncumplimientoConErrores	= Comunes.formatoDecimal(rg.getSumRegErr(),2); 
	
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	String erroresVsCifrasControl = rg.getCifras();
	
		buffer 			= new StringBuffer();
		JSONArray		erroresVsCifrasControlDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<erroresVsCifrasControl.length();i++){
			char lastChar = erroresVsCifrasControl.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				erroresVsCifrasControlDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			erroresVsCifrasControlDataArray.add(registro);
			buffer.setLength(0);
		}
		
	// Determinar si se mostrará el boton continuar carga	
	Boolean continuarCarga 								= ("".equals(rg.getErrores())&&"".equals(rg.getCifras()))
																				?new Boolean(true):new Boolean(false);

	// Enviar parametros adicionales
	resultado.put("claveAnio", 								claveAnio);
	resultado.put("claveTrimestre", 							claveTrimestre);
	resultado.put("numeroRegistros", 						numeroRegistros);
	resultado.put("numeroSumatoriaExposicionIncumplimiento",	numeroSumatoriaExposicionIncumplimiento);
	// Enviar resultado de la validacion
	resultado.put("claveProceso",											claveProceso										);
	resultado.put("totalRegistros", 										totalRegistros										);
	resultado.put("solicitudesCorrectasDataArray",					solicitudesCorrectasDataArray					);
	resultado.put("numeroRegistrosSinErrores",						numeroRegistrosSinErrores						);
	resultado.put("numeroImportesExposicionIncumplimientoSinErrores",	numeroImportesExposicionIncumplimientoSinErrores	);
	resultado.put("solicitudesConErroresDataArray",					solicitudesConErroresDataArray				);
	resultado.put("numeroRegistrosConErrores",						numeroRegistrosConErrores						);
	resultado.put("numeroImportesExposicionIncumplimientoConErrores",	numeroImportesExposicionIncumplimientoConErrores	);
	resultado.put("erroresVsCifrasControlDataArray",				erroresVsCifrasControlDataArray				);
	resultado.put("continuarCarga",									continuarCarga								);
		
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.presentarPreacuse") 			)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveProceso 							= (request.getParameter("claveProceso")								== null)?"":request.getParameter("claveProceso");
	String 		totalRegistros 						= (request.getParameter("totalRegistros")								== null)?"":request.getParameter("totalRegistros");
	String 		claveAnio 								= (request.getParameter("claveAnio")									== null)?"":request.getParameter("claveAnio");
	String 		claveTrimestre 						= (request.getParameter("claveTrimestre")								== null)?"":request.getParameter("claveTrimestre");
	String 		numeroRegistros 						= (request.getParameter("numeroRegistros")							== null)?"":request.getParameter("numeroRegistros");
	String 		numeroSumatoriaExposicionIncumplimiento = (request.getParameter("numeroSumatoriaExposicionIncumplimiento")		== null)?"":request.getParameter("numeroSumatoriaExposicionIncumplimiento");
	
	// Construir Array con los datos del preacuse
	JSONArray 	solicitudesPorAgregarDataArray 	= new JSONArray();
	JSONArray	registro									= null;

	registro = new JSONArray();
	registro.add("Tipo de Operación");
	registro.add("05 Calificación de Cartera");
	solicitudesPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("No. total de registros transmitidos");
	registro.add(totalRegistros);
	solicitudesPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("Total de Exposición al Incumplimiento");
	registro.add(Comunes.formatoDecimal(numeroSumatoriaExposicionIncumplimiento,2));
	solicitudesPorAgregarDataArray.add(registro);
		
	// Hacer eco de los parametros de la carga
	resultado.put("claveProceso",										claveProceso									);
	resultado.put("totalRegistros",									totalRegistros									);
	resultado.put("claveAnio",											claveAnio										);
	resultado.put("claveTrimestre",									claveTrimestre									);
	resultado.put("numeroRegistros",									numeroRegistros								);
	resultado.put("numeroSumatoriaExposicionIncumplimiento",	numeroSumatoriaExposicionIncumplimiento);
	
	// Enviar descripcion de las solicitudes por agregar
	resultado.put("solicitudesPorAgregarDataArray", solicitudesPorAgregarDataArray);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_PREACUSE" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.transmitirRegistros")		)  {
 
	JSONObject		resultado							= new JSONObject();
	String 			estadoSiguiente					= null;
	boolean			success								= true;
	boolean			hayError								= false;
	ResultadosGar 	rg 									= null;
	String 			msg 									= "";
	
	String 		claveProceso 							= (request.getParameter("claveProceso")	 							== null)?"0"		:request.getParameter("claveProceso");
	String 		totalRegistros 						= (request.getParameter("totalRegistros")	 							== null)?"0"		:request.getParameter("totalRegistros");
	
	String 		claveAnio 								= (request.getParameter("claveAnio")									== null)?""			:request.getParameter("claveAnio");
	String 		claveTrimestre 						= (request.getParameter("claveTrimestre")								== null)?""			:request.getParameter("claveTrimestre");
	String 		numeroRegistros 						= (request.getParameter("numeroRegistros")							== null)?"0"		:request.getParameter("numeroRegistros");
	String 		numeroSumatoriaExposicionIncumplimiento = (request.getParameter("numeroSumatoriaExposicionIncumplimiento")		== null)?"0.00"	:request.getParameter("numeroSumatoriaExposicionIncumplimiento");
	
	String 		isEmptyPkcs7 							= (request.getParameter("isEmptyPkcs7")	 							== null)?"false"	:request.getParameter("isEmptyPkcs7");
	String		textoFirmado							= (request.getParameter("textoFirmado")	 							== null)?""			:request.getParameter("textoFirmado");
	String		pkcs7										= (request.getParameter("pkcs7")			 	 							== null)?""			:request.getParameter("pkcs7");
 
	if( session.getAttribute("rgprint")!=null ){
		msg 					= "Para realizar otra operacion por favor seleccione la opcion Cargar Archivo en el menu Carga de Calificacion de Cartera";
		estadoSiguiente 	= "ESPERAR_DECISION";
		hayError				= true;
	} else {
		session.setAttribute("rgprint", new ResultadosGar() ); //Se establece la variable de sesión para controlar envíos duplicados
	} 
	
	if( !hayError ){
		
		// Obtener instancia del EJB de Garantias
		Garantias garantias = null;
		try {
					
			garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

		} catch(Exception e) {
			
			log.error("CargaArchivo.transmitirRegistros(Exception): Obtener instancia del EJB de Garantías");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
			
		}
		
		// Se declaran variables adicionales
		String 									folioCert 		= "";
		String 									externContent 	= textoFirmado;
		char 										getReceipt 		= 'Y';
		netropology.utilerias.Seguridad 	s 					= null;
		String 									claveIF 			= iNoCliente;
		
		/* Nota: Para otros navegadres diferentes de IE y de Mozilla
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		*/
			
		// Autenticar certificado
		if ( !_serial.equals("") && !externContent.equals("") && !pkcs7.equals("") ) {
			
			folioCert 	= "01CC"+claveIF+new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			s 				= new netropology.utilerias.Seguridad();
			
			if ( s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {	
				rg = garantias.transmitirCalifCartera(
					claveProceso,
					claveIF,
					totalRegistros,
					numeroSumatoriaExposicionIncumplimiento,
					claveAnio,
					claveTrimestre,
					iNoUsuario
				);
				estadoSiguiente 	= "MOSTRAR_ACUSE_TRANSMISION_SOLICITUDES";
			}else{
				msg 					= "La autentificacion no se llevo a cabo: "+s.mostrarError();
				estadoSiguiente 	= "ESPERAR_DECISION";
				hayError				= true;
			}
			
		}else{
			
			try {
				throw new NafinException("GRAL0021");
			}catch(Exception e){
				estadoSiguiente 	= "ESPERAR_DECISION";
				msg					= e.getMessage();
				hayError				= true;
			}
			
		}
 
	}
	
	// Poner en sesion el resultado de la transmicion de solicitudes
	if( "MOSTRAR_ACUSE_TRANSMISION_SOLICITUDES".equals(estadoSiguiente) ){
		
		session.setAttribute("rgprint",	rg);
		resultado.put("folioSolicitud", 	"Su solicitud fue recibida con el n&uacute;mero de folio: " + rg.getFolio() );
	
		// Construir Array con los datos del acuse
		JSONArray 	solicitudesAgregadasDataArray 	= new JSONArray();
		JSONArray	registro							= null;
		
		registro = new JSONArray();
		registro.add("Tipo de Operación");
		registro.add("05 Calificación de Cartera");
		solicitudesAgregadasDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("No. total de registros transmitidos");
		registro.add(totalRegistros);
		solicitudesAgregadasDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Total de Exposición al Incumplimiento");
		registro.add(Comunes.formatoDecimal(numeroSumatoriaExposicionIncumplimiento,2));
		solicitudesAgregadasDataArray.add(registro);
				
		registro = new JSONArray();
		registro.add("Fecha de carga");
		registro.add(rg.getFecha());
		solicitudesAgregadasDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Hora de carga");
		registro.add(rg.getHora());
		solicitudesAgregadasDataArray.add(registro);
	
		registro = new JSONArray();
		registro.add("Usuario");
		registro.add(iNoUsuario+" - "+strNombreUsuario);
		solicitudesAgregadasDataArray.add(registro);
		
		resultado.put("solicitudesAgregadasDataArray",solicitudesAgregadasDataArray);
		
		// Hacer eco de los parametros de la carga
		resultado.put("claveProceso",								claveProceso);
		resultado.put("totalRegistros",							totalRegistros);
		resultado.put("claveAnio",									claveAnio);
		resultado.put("claveTrimestre",							claveTrimestre);
		resultado.put("numeroRegistros",							numeroRegistros);
		resultado.put("numeroSumatoriaExposicionIncumplimiento",	numeroSumatoriaExposicionIncumplimiento);
	
	}
	
	resultado.put("msg",								msg									);	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 			estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
 
} else if (    informacion.equals("CargaArchivo.fin") 							)	{

	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("29califcarte01ext.jsp"); 
	
} else if (    informacion.equals("CatalogoAnio")									)	{
	
	boolean		success		= true;
	JSONObject	resultado	= new JSONObject();
	
	// Leer parametros de la consulta
	String 		anio 			= (request.getParameter("anio")		== null)?"":request.getParameter("anio");
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
				
	} catch(Exception e) {
	 
		log.error("CatalogoAnio(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// Consultar Catalogo de Años
	List 			trimAnios	= garantias.getTrimestresEnRango(anio);
	List 			anios			= (List)trimAnios.get(0);
	
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	for(int i=0;i<anios.size();i++){	
		registro.put("clave",			anios.get(i));
		registro.put("descripcion",	anios.get(i));
		registros.add(registro);
	}
 
	// Enviar resultado
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CatalogoTrimestre")							)	{
	
	boolean		success		= true;
	JSONObject	resultado	= new JSONObject();
	
	// Leer parametros de la consulta
	String 		anio 			= (request.getParameter("anio")		== null)?"":request.getParameter("anio");
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
				
	} catch(Exception e) {
	 
		log.error("CatalogoTrimestre(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
	 
	}
	
	// Consultar Catalogo de Trimestres
	List 			trimAnios	= garantias.getTrimestresEnRango(anio);
	List 			trims			= (List) trimAnios.get(1);
	
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	for(int i=0;i<trims.size();i++){	
		registro.put("clave",			trims.get(i));
		registro.put("descripcion",	trims.get(i));
		registros.add(registro);
	}
 
	// Enviar resultado
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("GeneraArchivoPDF")	)	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		msg 					= "";
	
	// Leer parametros
	String totalRegistros 							= (request.getParameter("totalRegistros")								== null)?"":request.getParameter("totalRegistros");
	String numeroSumatoriaExposicionIncumplimiento 	= (request.getParameter("numeroSumatoriaExposicionIncumplimiento")		== null)?"":request.getParameter("numeroSumatoriaExposicionIncumplimiento");
	
	String 	urlArchivo 								= "";
	HashMap 	cabecera 								= null;
	try {
		
		// Crear map con la informacion del cabcera del PDF
		cabecera = new HashMap();
		cabecera.put("strPais",					(String)	session.getAttribute("strPais"));
		cabecera.put("iNoNafinElectronico",	(String) ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()));
		cabecera.put("sesExterno",				(String)	session.getAttribute("sesExterno"));
		cabecera.put("strNombre",				(String) session.getAttribute("strNombre"));
		cabecera.put("strNombreUsuario",		(String) session.getAttribute("strNombreUsuario"));
		cabecera.put("strLogo",					(String)	session.getAttribute("strLogo"));
		
		String nombreArchivo = CargaArchivoCalificacionCartera.generaAcuseArchivoPDF(
				(ResultadosGar)session.getAttribute("rgprint"), 
				iNoUsuario, 
				cabecera, 
				strDirectorioTemp, 
				strDirectorioPublicacion, 
				totalRegistros, 
				numeroSumatoriaExposicionIncumplimiento, 
				strNombreUsuario 
		);
	
		urlArchivo = strDirecVirtualTemp+nombreArchivo;
		 
	}catch(Exception e){
			
		log.error("GeneraArchivoPDF(Exception)");
		e.printStackTrace();
		log.error("iNoUsuario                       = <" + iNoUsuario                       + ">"); 
		log.error("cabecera                         = <" + cabecera                         + ">"); 
		log.error("strDirectorioTemp                = <" + strDirectorioTemp                + ">"); 
		log.error("strDirectorioPublicacion         = <" + strDirectorioPublicacion         + ">"); 
		log.error("totalRegistros                   = <" + totalRegistros                   + ">"); 
		log.error("numeroSumatoriaExposicionIncumplimiento = <" + numeroSumatoriaExposicionIncumplimiento + ">"); 
		msg		= "Ocurrió un error inesperado: " + e.getMessage();
		success	= false;
			
	}
		
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo ); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)						);
	
	infoRegresar = resultado.toString();
					
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>