<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*,
		com.netro.afiliacion.ConsCargaProvEcon, 
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.afiliacion.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("CargaArchivo.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;
	
	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
				
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){
 
		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}
	
	// 2. Validar Numero Fiso
	if ( !hayAviso && seguridad.validaNumeroFISO(claveIF) ) {
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se tiene definido un Fideicomiso para realizar sus operaciones.<br/><br/>Por favor comuníquese al Centro de Atención a clientes al teléfono 50-89-61-07 <br/>o del Interior al 01-800-NAFINSA (01-800-6234672).";
		hayAviso				= false;
	}

	// 3. Remover variables de sesion
	if ( !hayAviso ){
		
		session.removeAttribute("rgprint");
	}
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
			
	} catch(Exception e) {
	 
		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
		
	}
	
	if(!hayAviso){
		
		String saldosLayout = "";	
		resultado.put("saldosLayout", 	saldosLayout			);
		
	}
	
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_SOLICITUD_DE_CARGA";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();

} else if (          informacion.equals("CargaArchivo.enviarSolicitudDeCarga") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	
	String claveMes 					= (request.getParameter("claveMes")						== null)?"":request.getParameter("claveMes");
	String numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String sumatoriaSaldosFinMes 	= (request.getParameter("sumatoriaSaldosFinMes")	== null)?"":request.getParameter("sumatoriaSaldosFinMes");
	
	resultado.put("claveMes",					claveMes						);
	resultado.put("numeroRegistros",			numeroRegistros			);
	resultado.put("sumatoriaSaldosFinMes",	sumatoriaSaldosFinMes	);
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	"ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES" 		);
	infoRegresar = resultado.toString();
	
} else if (        	informacion.equals("CargaArchivo.subirArchivo") 				)	{	
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
 
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(8097152);
		myUpload.upload();
		
	} catch(Exception e) {
			
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 8097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 8 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
 
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	String claveMes 					= (myRequest.getParameter("claveMes1")						== null)?"":myRequest.getParameter("claveMes1");
	String numeroRegistros 			= (myRequest.getParameter("numeroRegistros1")			== null)?"":myRequest.getParameter("numeroRegistros1");
	String sumatoriaSaldosFinMes	= (myRequest.getParameter("sumatoriaSaldosFinMes1")	== null)?"":myRequest.getParameter("sumatoriaSaldosFinMes1");

	// Guardar Archivo en Disco
	int numFiles = 0;
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		String nuevoNombre = Comunes.cadenaAleatoria(16)+"."+myFile.getFileExt();;
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + nuevoNombre	 );
		resultado.put("fileName", 	nuevoNombre			);

	}catch(Exception e){
		
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
	
	// Enviar parametros adicionales
	resultado.put("claveMes",					claveMes						);
	resultado.put("numeroRegistros",			numeroRegistros			);
	resultado.put("sumatoriaSaldosFinMes",	sumatoriaSaldosFinMes	);
 
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 		"REALIZAR_VALIDACION" 	);
	// Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success)		);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.realizarValidacion") 				)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveMes 					= (request.getParameter("claveMes")						== null)?"":request.getParameter("claveMes");
	String 		numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldosFinMes 	= (request.getParameter("sumatoriaSaldosFinMes")	== null)?"":request.getParameter("sumatoriaSaldosFinMes");
	String 		fileName 					= (request.getParameter("fileName")						== null)?"":request.getParameter("fileName");
	
	String 		rutaArchivo 				= strDirectorioTemp + iNoUsuario + "." + fileName	;
	String 		claveIF 						= iNoCliente;
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
			
	} catch(Exception e) {
	 
		log.error("CargaArchivo.realizarValidacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
		
	}
			
	// Realizar validacion
	ResultadosGar rg = garantias.procesarAltaDesembolsosMasivos(
									strDirectorioTemp,
									rutaArchivo,
									numeroRegistros,
									sumatoriaSaldosFinMes,
									iNoCliente
									);
	
	// Guardar en sesión las claves de financiamiento
	//List		  clavesFinanciamiento = rg.getClavesFinan();
	//session.setAttribute("CLAVES_FINAN",clavesFinanciamiento);
 
	// Leer Resultado de la Validacion
	String claveProceso 		= String.valueOf(rg.getIcProceso());
	String totalRegistros	= String.valueOf(rg.getNumRegOk());
	String montoTotal 		= String.valueOf(rg.getSumRegOk());
	String montoTotalPagar 	= String.valueOf(rg.getSumTotPag());

	//  Leer Resultado de la Validacion ( Registros sin Errores )
	String 		 registrosSinErrores 				  = rg.getCorrectos();
	
		StringBuffer 	buffer 	= new StringBuffer();
		JSONArray		registrosSinErroresDataArray = new JSONArray();
		int				ctaRegistros = 0;
		
		for(int i=0;i<registrosSinErrores.length();i++){
			char lastChar = registrosSinErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosSinErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosSinErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		// Si el primer registro dice: Claves de Financiamiento, borrarlo
		if( 	registrosSinErroresDataArray.size() > 0 ){
				
			JSONArray registro = (JSONArray) registrosSinErroresDataArray.get(0);
			if("Claves de Financiamiento:".equals( registro.getString(1) )){
				registrosSinErroresDataArray.remove(0);
			}
			
		}
 
	String numeroRegistrosSinErrores 		= String.valueOf(rg.getNumRegOk());
	String sumatoriaSaldosFinMesSinErrores	= "$ "+Comunes.formatoDecimal(rg.getSumRegOk(),2);
 
	//  Leer Resultado de la Validacion ( Registros con Errores )
	String registrosConErrores 				= rg.getErrores();

		buffer 			= new StringBuffer();
		JSONArray		registrosConErroresDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<registrosConErrores.length();i++){
			char lastChar = registrosConErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosConErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosConErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		
	String numeroRegistrosConErrores 		= String.valueOf(rg.getNumRegErr());
	String sumatoriaSaldosFinMesConErrores	= "$ " + Comunes.formatoDecimal(rg.getSumRegErr(),2); 
 
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	String erroresVsCifrasControl = rg.getCifras();
	
		buffer 			= new StringBuffer();
		JSONArray		erroresVsCifrasControlDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<erroresVsCifrasControl.length();i++){
			char lastChar = erroresVsCifrasControl.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				erroresVsCifrasControlDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			erroresVsCifrasControlDataArray.add(registro);
			buffer.setLength(0);
		}
		
	// Determinar si se mostrará el boton continuar carga	
	Boolean continuarCarga 								= ("".equals(rg.getErrores())&&"".equals(rg.getCifras()))
																					?new Boolean(true):new Boolean(false);

	// Enviar parametros adicionales
	resultado.put("claveMes",												claveMes												);
	resultado.put("numeroRegistros",										numeroRegistros									);
	resultado.put("sumatoriaSaldosFinMes",								sumatoriaSaldosFinMes							);

	// Enviar resultado general de la validacion
	resultado.put("claveProceso",											claveProceso										);
	resultado.put("totalRegistros", 										totalRegistros										);
	resultado.put("montoTotal",											montoTotal											);
	resultado.put("montoTotalPagar",										montoTotalPagar									);
	
	resultado.put("registrosSinErroresDataArray",					registrosSinErroresDataArray					);
	resultado.put("numeroRegistrosSinErrores",						numeroRegistrosSinErrores						);
	resultado.put("sumatoriaSaldosFinMesSinErrores",				sumatoriaSaldosFinMesSinErrores				);
	
	resultado.put("registrosConErroresDataArray",					registrosConErroresDataArray					);
	resultado.put("numeroRegistrosConErrores",						numeroRegistrosConErrores						);
	resultado.put("sumatoriaSaldosFinMesConErrores",				sumatoriaSaldosFinMesConErrores				);
	
	resultado.put("erroresVsCifrasControlDataArray",				erroresVsCifrasControlDataArray				);
	resultado.put("continuarCarga",										continuarCarga										);
		
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.presentarPreacuse") 			)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveProceso 							= (request.getParameter("claveProceso")					== null)?"":request.getParameter("claveProceso");
	String 		totalRegistros 						= (request.getParameter("totalRegistros")					== null)?"":request.getParameter("totalRegistros");
	String 		montoTotal 								= (request.getParameter("montoTotal")						== null)?"":request.getParameter("montoTotal");
	String 		montoTotalPagar 						= (request.getParameter("montoTotalPagar")				== null)?"":request.getParameter("montoTotalPagar");
	
	String 		claveMes 								= (request.getParameter("claveMes")							== null)?"":request.getParameter("claveMes");
	String 		numeroRegistros 						= (request.getParameter("numeroRegistros")				== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldosFinMes 				= (request.getParameter("sumatoriaSaldosFinMes")		== null)?"":request.getParameter("sumatoriaSaldosFinMes");
	
	// Construir Array con los datos del preacuse
	JSONArray 	registrosPorAgregarDataArray 		= new JSONArray();
	JSONArray	registro									= null;

	// Determinar el mes de carga
	String 	meses[]		={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	int 		indice 		= Integer.parseInt( claveMes.substring(4,6) ) - 1;
	String   nombreMes	= meses[indice] ;
	// Determinar el año de la carga
	String 	numeroAnio 	= claveMes.substring(0,4);
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("gticat_tipooperacion");
	cat.setCampoClave("ic_tipo_operacion");
	cat.setCampoDescripcion("cg_descripcion");
	List claves = new ArrayList();
	claves.add("6");	//6= Desembolsos Masivos
	cat.setValoresCondicionIn(claves);
	List l = cat.getListaElementos();
	String tipoOperacion = "";
	if (l!=null && l.size()>0) {
		tipoOperacion = Comunes.rellenaCeros(((ElementoCatalogo)l.get(0)).getClave(),2) + " " + ((ElementoCatalogo)l.get(0)).getDescripcion();
	}
	
	registro = new JSONArray();
	registro.add("Tipo de Operación");
	registro.add(tipoOperacion);
	registrosPorAgregarDataArray.add(registro);
	
	
	
	registro = new JSONArray();
	registro.add("No. total de registros transmitidos");
	registro.add(totalRegistros);
	registrosPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("Sumatoria de montos de financiamiento");
	registro.add(Comunes.formatoDecimal(montoTotal,2));
	registrosPorAgregarDataArray.add(registro);
	
	// Hacer eco de los parametros de la carga
	resultado.put("claveProceso",								claveProceso);
	resultado.put("totalRegistros",							totalRegistros);
	resultado.put("montoTotal",								montoTotal);
	resultado.put("montoTotalPagar",							montoTotalPagar);
	
	resultado.put("claveMes",									claveMes);
	resultado.put("numeroRegistros",							numeroRegistros);
	resultado.put("sumatoriaSaldosFinMes",					sumatoriaSaldosFinMes);

	// Enviar descripcion de los registros por agregar
	resultado.put("registrosPorAgregarDataArray", 		registrosPorAgregarDataArray);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_PREACUSE" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.transmitirRegistros")		)  {
 
	JSONObject		resultado							= new JSONObject();
	String 			estadoSiguiente					= null;
	boolean			success								= true;
	boolean			hayError								= false;
	ResultadosGar 	rg 									= null;
	String 			msg 									= "";
	
	// Leer campos del preacuse
	String claveProceso 				= (request.getParameter("claveProceso")				== null)?"0"	:request.getParameter("claveProceso");
	String totalRegistros 			= (request.getParameter("totalRegistros")				== null)?"0"	:request.getParameter("totalRegistros");
	String montoTotal 				= (request.getParameter("montoTotal")					== null)?"0.00":request.getParameter("montoTotal");
	String montoTotalPagar 			= (request.getParameter("montoTotalPagar")			== null)?"0.00":request.getParameter("montoTotalPagar");
	
	String claveMes 					= (request.getParameter("claveMes")						== null)?""		:request.getParameter("claveMes");
	String numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?""		:request.getParameter("numeroRegistros");
	String sumatoriaSaldosFinMes	= (request.getParameter("sumatoriaSaldosFinMes")	== null)?""		:request.getParameter("sumatoriaSaldosFinMes");

	// Leer campos de la firma digital
	String isEmptyPkcs7 				= (request.getParameter("isEmptyPkcs7")	 			== null)?"false"	:request.getParameter("isEmptyPkcs7");
	String textoFirmado				= (request.getParameter("textoFirmado")	 			== null)?""			:request.getParameter("textoFirmado");
	String pkcs7						= (request.getParameter("pkcs7")			 	 			== null)?""			:request.getParameter("pkcs7");

	if( session.getAttribute("rgprint")!=null ){
		msg 					= "Para realizar otra operacion por favor seleccione la opcion Cargar Archivo en el menu Recuperaciones";
		estadoSiguiente 	= "ESPERAR_DECISION";
		hayError				= true;
	} else {
		session.setAttribute("rgprint", new ResultadosGar() ); //Se establece la variable de sesión para controlar envíos duplicados
	} 
	
	if( !hayError ){
		
		// Obtener instancia del EJB de Garantias
		Garantias garantias = null;
		try {
					
			garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

		} catch(Exception e) {
			
			log.error("CargaArchivo.transmitirRegistros(Exception): Obtener instancia del EJB de Garantías");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
			
		}
		
		// Se declaran variables adicionales
		String 									folioCert 		= "";
		String 									externContent 	= textoFirmado;
		char 										getReceipt 		= 'Y';
		netropology.utilerias.Seguridad 	s 					= null;
		String 									claveIF 			= iNoCliente;
 
		/* Nota: Para otros navegadores diferentes de IE y Mozilla
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		*/
			
		// Autenticar certificado
		if ( !_serial.equals("") && !externContent.equals("") && !pkcs7.equals("") ) {
			
			folioCert 	= "02SPC"+claveIF+new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			s 				= new netropology.utilerias.Seguridad();
			
			//List clavesFinanciamiento = (List)session.getAttribute("CLAVES_FINAN");
			if ( s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {	
				rg = garantias.transmitirDesembolsosMasivos(
							claveProceso,
							iNoCliente,
							totalRegistros,
							montoTotal,
							iNoUsuario,
							s.getAcuse()	
						);
				estadoSiguiente 	= "MOSTRAR_ACUSE_TRANSMISION_REGISTROS";
			}else{
				msg 					= "La autentificacion no se llevo a cabo: "+s.mostrarError();
				estadoSiguiente 	= "ESPERAR_DECISION";
				hayError				= true;
			}
			
		}else{
			
			try {
				throw new NafinException("GRAL0021");
			}catch(Exception e){
				estadoSiguiente 	= "ESPERAR_DECISION";
				msg					= e.getMessage();
				hayError				= true;
			}
			
		}
 
	}
	
	// Poner en sesion el resultado de la transmicion de solicitudes
	if( "MOSTRAR_ACUSE_TRANSMISION_REGISTROS".equals(estadoSiguiente) ){
		
		session.setAttribute("rgprint",	rg);
		resultado.put("folioSolicitud", 	"Su solicitud fue recibida con el número de folio: " + rg.getFolio() );
	
		// Determinar el mes de carga
		String 	meses[]		={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		int 		indice 		= Integer.parseInt( claveMes.substring(4,6) ) - 1;
		String   nombreMes	= meses[indice] ;
		// Determinar el año de la carga
		String 	numeroAnio 	= claveMes.substring(0,4);
	
		// Construir Array con los datos del acuse
		JSONArray 	registrosAgregadosDataArray 	= new JSONArray();
		JSONArray	registro								= null;
		
		
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("gticat_tipooperacion");
		cat.setCampoClave("ic_tipo_operacion");
		cat.setCampoDescripcion("cg_descripcion");
		List claves = new ArrayList();
		claves.add("6");	//6= Desembolsos Masivos
		cat.setValoresCondicionIn(claves);
		List l = cat.getListaElementos();
		String tipoOperacion = "";
		if (l!=null && l.size()>0) {
			tipoOperacion = Comunes.rellenaCeros(((ElementoCatalogo)l.get(0)).getClave(),2) + " " + ((ElementoCatalogo)l.get(0)).getDescripcion();
		}
	
		registro = new JSONArray();
		registro.add("Tipo de Operación");
		registro.add(tipoOperacion);
		registrosAgregadosDataArray.add(registro);
		
		
		registro = new JSONArray();
		registro.add("Fecha de carga");
		registro.add(rg.getFecha());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Hora de carga");
		registro.add(rg.getHora());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Fecha valor");
		registro.add(rg.getFechaValor());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Usuario");
		registro.add(iNoUsuario+" - "+strNombreUsuario);
		registrosAgregadosDataArray.add(registro);
		
		
		boolean fechaDistinta = false;
		if(rg.getFecha() == null || rg.getFecha().trim().equals("")){ 
				
		}
		if(rg.getFechaValor() == null || rg.getFechaValor().trim().equals("")){		
			
		}
		if(!rg.getFecha().equals(rg.getFechaValor())){
			fechaDistinta = true;
		}
		
		resultado.put("validaFechas",new Boolean(fechaDistinta));
		resultado.put("fechaValor",rg.getFechaValor() );
		
		resultado.put("registrosAgregadosDataArray",	registrosAgregadosDataArray);
		
		// Hacer eco de los parametros de la carga
		resultado.put("claveProceso",						claveProceso				);
		resultado.put("totalRegistros",					totalRegistros				);
		resultado.put("montoTotal",						montoTotal					);
		resultado.put("montoTotalPagar",					montoTotalPagar			);
		
		resultado.put("claveMes",							claveMes						);
		resultado.put("numeroRegistros",					numeroRegistros			);
		resultado.put("sumatoriaSaldosFinMes",			sumatoriaSaldosFinMes	);
		
	}
	
	resultado.put("msg",								msg									);	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 			estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
 
} else if (    informacion.equals("CargaArchivo.fin") 							)	{

	// La siguiente línea se pone por compatibilidad
	//response.sendRedirect("29cargasaldos01ext.jsp"); 
	
} else if (    informacion.equals("CatalogoMes")									)	{
	
	boolean		success		= true;
	JSONObject	resultado	= new JSONObject();
		
	// Se declaran las variables...
	String 	meses[] 	= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	Calendar cal 		= Calendar.getInstance();
	cal.add(Calendar.MONTH,-11);
	
	// Obtener lista de los ultimos 12 meses
	String 		mesanio 		= null;
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	for(int i=0;i<12;i++){
		
		mesanio = new SimpleDateFormat("yyyyMM").format(cal.getTime());
 
		registro.put("clave",			mesanio );
		registro.put("descripcion",	meses[cal.get(Calendar.MONTH)]+" - " + cal.get(Calendar.YEAR));
		registros.add(registro);
		
		cal.add(Calendar.MONTH,1);
		
	}
 
	// Enviar resultado
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("GeneraArchivoPDF")	)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String 		msg 			= "";
	
	// Leer parametros
	String totalRegistros 	= (request.getParameter("totalRegistros")	== null)?"":request.getParameter("totalRegistros");
	String montoTotal 		= (request.getParameter("montoTotal")		== null)?"":request.getParameter("montoTotal");
	String claveMes 			= (request.getParameter("claveMes")			== null)?"":request.getParameter("claveMes");
	
	String 	urlArchivo 		= "";
	HashMap 	cabecera 		= null;
	try {
		
		// Crear map con la informacion del cabcera del PDF
		cabecera = new HashMap();
		cabecera.put("strPais", 				(String) session.getAttribute("strPais"));
		cabecera.put("iNoNafinElectronico", (String) ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()));
		cabecera.put("sesExterno", 			(String) session.getAttribute("sesExterno"));
		cabecera.put("strNombre", 				(String) session.getAttribute("strNombre"));
		cabecera.put("strNombreUsuario", 	(String) session.getAttribute("strNombreUsuario"));
		cabecera.put("strLogo", 				(String) session.getAttribute("strLogo"));
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("gticat_tipooperacion");
		cat.setCampoClave("ic_tipo_operacion");
		cat.setCampoDescripcion("cg_descripcion");
		List claves = new ArrayList();
		claves.add("6");	//6= Desembolsos Masivos
		cat.setValoresCondicionIn(claves);
		List l = cat.getListaElementos();
		String tipoOperacion = "";
		if (l!=null && l.size()>0) {
			tipoOperacion = Comunes.rellenaCeros(((ElementoCatalogo)l.get(0)).getClave(),2) + " " + ((ElementoCatalogo)l.get(0)).getDescripcion();
		}
	

		
		String nombreArchivo = CargaArchivoDesembolsoMasivo.generaAcuseArchivoPDF(
			(ResultadosGar)session.getAttribute("rgprint"),
			strDirectorioTemp,
			cabecera,
			strDirectorioPublicacion,
			totalRegistros,
			montoTotal,
			claveMes,
			iNoUsuario, //loginUsuario
			strNombreUsuario,tipoOperacion
		);

		urlArchivo = strDirecVirtualTemp+nombreArchivo;
		 
	}catch(Exception e){
			
		log.error("GeneraArchivoPDF(Exception)");
		e.printStackTrace();
		log.error("rg                       = <" + (ResultadosGar)session.getAttribute("rgprint") + ">");
		log.error("strDirectorioTemp        = <" + strDirectorioTemp                              + ">");
		log.error("cabecera                 = <" + cabecera                                       + ">");
		log.error("strDirectorioPublicacion = <" + strDirectorioPublicacion                       + ">");
		log.error("totalRegistros           = <" + totalRegistros                                 + ">");
		log.error("montoTotal               = <" + montoTotal                                     + ">");
		log.error("claveMes                 = <" + claveMes                                       + ">");
		log.error("iNoUsuario               = <" + iNoUsuario                                     + ">");
		log.error("strNombreUsuario         = <" + strNombreUsuario                               + ">");
			
		msg		= e.getMessage();
		success	= false;
			
	}
		
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo ); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)						);
	
	infoRegresar = resultado.toString();
					
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>