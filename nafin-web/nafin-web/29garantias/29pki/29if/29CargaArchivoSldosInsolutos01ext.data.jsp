<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.File,
		java.io.FileInputStream,
		java.io.BufferedOutputStream,
		java.io.FileInputStream,
		java.io.FileOutputStream,
		java.io.InputStream,
		java.io.OutputStream,
		java.math.BigDecimal,
		org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileUploadException,
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*,
		com.netro.garantias.ValidaArchivoGarantiasThread,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.afiliacion.*,
		org.apache.commons.logging.Log"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%!

	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	private final int MAX_PLANTILLA_FILE_SIZE = 52428800; // 50 MB   

%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("CargaArchivo.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;
	
	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
				
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){
 
		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}
	
	// 2. Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
			
	} catch(Exception e) {
	 
		log.error("CargaArchivo.realizarValidacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
		
	}

	// 3. Obtener el Mes a conciliar
	HashMap 			mesConciliar 						= garantias.getMesConciliacionAutomatica();
	
	// 4. Determinar la Situación de la Conciliación Automática
	String 			claveMesConciliar					= (String) mesConciliar.get("CLAVE_MES");
	String 			claveFiso   						= garantias.getClaveFISO(claveIF);
	String 			claveIfSiag 						= String.valueOf(garantias.getClaveIfSiag(claveIF));
	String 			claveTipoConciliacion			= "1";   // CLAVE 1, SALDO INSOLUTO DE GTICAT_TIPO_CONCILIACION // #TAG
	int 				claveSituacionConciliacion 	= garantias.getClaveSituacionConciliacionAutomatica( claveMesConciliar, claveFiso, claveIfSiag, claveTipoConciliacion ); // #TAG

	// Constantes
	final int 		GENERACION_PORTAFOLIO_NAFIN  	= 1; // CLAVE 1, GENERACION DEL PORTAFOLIO NAFIN
	final int 		CARGA_PORTAFOLIO_IF				= 3; // CLAVE 3, CARGA DEL PORTAFOLIO DEL IF
	final String 	SALDO_INSOLUTO						= "1"; // CLAVE 1, GTICAT_TIPO_CONCILIACION
	
	// 5. Validar Numero Fiso
	if ( !seguridad.validaNumeroFISO(claveIF) ){
		
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se tiene definido un Fideicomiso para realizar sus operaciones.<br/><br/>Por favor comuníquese al Centro de Atención a clientes al teléfono 50-89-61-07 <br/>o del Interior al 01-800-NAFINSA (01-800-6234672).";
		hayAviso				= true;
		
	// 6. Validar que la informacion no haya sido enviada al SIAG
	} else if( claveSituacionConciliacion >= CARGA_PORTAFOLIO_IF ){
		
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "La información del mes: " + (String) mesConciliar.get("NOMBRE_MES") + " ya fue enviada<br/>al sistema SIAG, no se puede realizar la Carga.";
		hayAviso				= true;
		
	// 7. Validar que no se hayan vencido los primeros días hábiles del mes en curso.
	} else if( !garantias.primerosDiasHabilesMesConciliacionAutomatica(SALDO_INSOLUTO) ){

		int numeroDiasHabiles = garantias.getNumeroDiasHabilesPermitidosConciliacionAutomatica(SALDO_INSOLUTO);
		
		estadoSiguiente 	= "MOSTRAR_AVISO";
		if( numeroDiasHabiles == 1 ){
			aviso				= "No se puede realizar la carga del Saldo Insoluto debido a que ha vencido<br/>el primer día hábil del mes en curso.<br/><br/>Por favor comuníquese al teléfono 01-800-04GTIAS."; // #TAG
		} else {
			aviso				= "No se puede realizar la carga del Saldo Insoluto debido a que han vencido<br/>los primeros "+numeroDiasHabiles+" días hábiles del mes en curso.<br/><br/>Por favor comuníquese al teléfono 01-800-04GTIAS."; // #TAG
		}
		hayAviso				= true;
		
	// 8. Validar que ya se haya realizado la descarga del portafolio NAFIN
	} else if( claveSituacionConciliacion <= GENERACION_PORTAFOLIO_NAFIN ){
		
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se puede realizar la carga del mes correspondiente: " + (String) mesConciliar.get("NOMBRE_MES") + ", favor de realizar primero la descarga del Portafolio que envió el SIAG.";
		hayAviso				= true;

	}
	
	// 9. Remover objetos de sesion
	if ( !hayAviso ){
		
		// Revisar si hay un thread de una opracion anterior, y si es así detenerlo.
		ValidaArchivoGarantiasThread thread = (ValidaArchivoGarantiasThread) session.getAttribute("29CARGAARCHIVOSLDOSINSOLUTOS_THREAD");
		if(thread != null){
			thread.setRequestStop(true);
			// Esperar a que el thread finalice
			while(thread.isRunning() && !thread.isCompleted()){
				Thread.sleep(500);
			}
			session.removeAttribute("29CARGAARCHIVOSLDOSINSOLUTOS_THREAD"); // Remover thread de sesion // #TAG
		}
		session.removeAttribute("rgprint");
		
	}
 
	// 10. Definir el layout a mostrar ( en caso de que las validaciones sean exitosas )
	if(!hayAviso){
		
		String longitudClaveFinanciamiento	= garantias.operaProgramaSHFCreditoHipotecario(claveIF)?"30":"20";
		
		String descripcionLayout = 
			"<table>"  +
			"<tr>"  +
			"	<td class=\"titulos\" align=\"center\">"  +
			"		<span class=\"titulos\">"  +
			"			Estructura de Archivo<br>Carga masiva"  +
			"		</span>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td colspan=\"2\" align=\"center\">"  +
			"		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" >"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"center\" colspan=\"10\"  style=\"height:30px;\" >"  +
			"					Layout de Conciliaci&oacute;n de Saldos<br>"  +
			"					Archivo ZIP: los campos deben de estar separados por (\"@\")"  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"center\" width=\"25px\"  style=\"height:30px;\" >No.</td>"  +
			"				<td class=\"celda01\" align=\"center\">Nombre del Campo</td>"  +
			"				<td class=\"celda01\" align=\"center\">Tipo</td>"  +
			"				<td class=\"celda01\" align=\"center\">Longitud<br>M&aacute;xima</td>"  +
			"				<td class=\"celda01\" align=\"center\">Obligatorio</td>"  +
			"				<td class=\"celda01\" align=\"center\">Observaciones</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >1</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Clave del Intermediario Financiero SIAG "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">5</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					N&uacute;mero de identificaci&oacute;n del intermediario en SIAG "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >2</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Clave del Financiamiento "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">"+longitudClaveFinanciamiento+"</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					No. de pr&eacute;stamo o garant&iacute;a (Sociedad  Hipotecaria Federal) "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >3</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Nombre del Acreditado "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">80</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Nombre completo del cliente "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >4</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					RFC "  + 
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">13</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					RFC completo del cliente sin espacios ni guiones. "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >5</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Saldo Insoluto Fin de Mes "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">17,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Saldo registrado al cierre del mes "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >6</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Fecha de Disposici&oacute;n "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">DATE</td>"  +
			"				<td class=\"formas\" align=\"center\">8</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Se reporta en formato texto y se conforma \"aaaammdd\" "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >7</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Fecha de Vencimiento "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">DATE</td>"  +
			"				<td class=\"formas\" align=\"center\">8</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Se reporta en formato texto y se conforma \"aaaammdd\" "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >8</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Estatus de la Garant&iacute;a "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">3</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Se indica &uacute;nicamente en 3 letras de acuerdo al estatus correspondiente (VIG, VEN, CAD, LIQ, CAN, REC ó CXS) "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >9</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Moneda "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">7</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Se indica con la palabra PESOS/DOLARES "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >10</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Porcentaje Garantizado "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">5,2</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					&Uacute;nicamente en n&uacute;mero y  dos decimales. "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >11</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Tipo de Cr&eacute;dito "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">6</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					&Uacute;nicamente en n&uacute;mero y sin decimales. "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >12</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Mes a Conciliar "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">6</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Fecha del mes que se est&aacute;n conciliando, formato texto \"aaaamm\" "  +
			"				</td>"  +
			"			</tr>"  +
			"		</table>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"</table>";	
					
		resultado.put("descripcionLayout", 	descripcionLayout			);
		
	}
	
	// 11. Enviar clave y nombre del mes a conciliar ( en caso de que las validaciones sean exitosas )
	if(!hayAviso){
		resultado.put( "nombreMesConciliar", (String) mesConciliar.get("NOMBRE_MES") );
		resultado.put( "claveMesConciliar",  (String) mesConciliar.get("CLAVE_MES")  );		
	}
	
	// 12. Definir el estado siguiente ( en caso de que las validaciones sean exitosas )
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_SOLICITUD_CONCILIACION";
	}
	 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();

} else if (          informacion.equals("CargaArchivo.enviarSolicitudConciliacion") )	{
	
	JSONObject	resultado 							= new JSONObject();
	boolean		success	 							= true;
 
	String 		numeroRegistros 					= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldoInsoluto 			= (request.getParameter("sumatoriaSaldoInsoluto")	== null)?"":request.getParameter("sumatoriaSaldoInsoluto");
	String 		claveMesConciliar 				= (request.getParameter("claveMesConciliar")			== null)?"":request.getParameter("claveMesConciliar");
	
	// Suprimir caracteres de formato: ","
	numeroRegistros									= numeroRegistros.replaceAll(",","");
	sumatoriaSaldoInsoluto							= sumatoriaSaldoInsoluto.replaceAll(",","");
	
	resultado.put("numeroRegistros",				numeroRegistros			);
	resultado.put("sumatoriaSaldoInsoluto",	sumatoriaSaldoInsoluto	);
	resultado.put("claveMesConciliar",			claveMesConciliar			);
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	"ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES" 		);
	infoRegresar = resultado.toString();
	
} else if ( informacion.equals("CargaArchivo.subirArchivo")      			) {
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);

	// Variables adicionales
	String   	directorioTemporal  		= strDirectorioTemp ;
	String 		login 						= iNoUsuario;
	
	// Variables de la Respuesta
	JSONObject	resultado					= new JSONObject();
	boolean		success						= true;
 
	// Create a factory for disk-based file items
	DiskFileItemFactory 	factory 			= new DiskFileItemFactory(DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD, new File(directorioTemporal) );
	// Create a new file upload handler
	ServletFileUpload 	upload 			= new ServletFileUpload(factory);
	List						fileItemList	= null; // List of FileItem
	try {
		
		// Definir el tamaño máximo que pueden tener los archivos
		upload.setSizeMax(MAX_PLANTILLA_FILE_SIZE);
		// Parsear request
		fileItemList = upload.parseRequest(request);
		
	} catch(Throwable t) {
			
		success		= false;
		log.error("CargaPlantilla.subirArchivo(Exception): Cargar en memoria o disco el contenido del archivo");
		t.printStackTrace();
		if( t instanceof SizeLimitExceededException  ) {
			throw new AppException("El Archivo es muy Grande, excede el límite que es de " + ( MAX_PLANTILLA_FILE_SIZE / 1048576 ) + " MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
 
	// Leer parametros
	String numeroRegistros 					= "";
	String sumatoriaSaldoInsoluto			= "";
	String claveMesConciliar				= "";
	String archivo		 						= "";
	
	// Leer parametros
	for(int i=0;i<fileItemList.size();i++){
		
		FileItem fileItem = (FileItem) fileItemList.get(i);

		 if(        fileItem.isFormField()  &&  "numeroRegistros1".equals(    fileItem.getFieldName() ) ){
		 	 
		 	 numeroRegistros					= fileItem.getString();
		 	 
		 } else if( fileItem.isFormField()  &&  "sumatoriaSaldoInsoluto1".equals( fileItem.getFieldName() ) ){
		 	 
		 	 sumatoriaSaldoInsoluto			= fileItem.getString();
		 	 
		 } else if( fileItem.isFormField()  &&  "claveMesConciliar1".equals( fileItem.getFieldName() ) ){
		 	
		 	 claveMesConciliar 				= fileItem.getString();
		 	 
		 } else if( !fileItem.isFormField() &&  "archivo".equals(           fileItem.getFieldName() ) ){
		
		 	 archivo								= fileItem.getName();
		 	 archivo								= archivo == null?"":archivo;
		 	 
		 	 // Validar que la extension del archivo cargado sea zip
		 	 if( !archivo.matches("(?i)^.*\\.zip$") ){
		 	 	 throw new AppException("El formato del archivo de origen no es el correcto. Debe tener extensión zip.");
		 	 }
		 	 
		 	 // Guardar Archivo en Disco
		 	 try {
		 
				// Generar Nombre del Archivo
				String fileName		= Comunes.cadenaAleatoria(16)+".zip";
				
				// Guardar Archivo en Disco
				File 	 uploadedFile	= new File( directorioTemporal + login + "." + fileName );
				fileItem.write(uploadedFile);
				
				resultado.put("fileName", fileName );
					
			 } catch(Throwable e) {
				
				success		= false;
				log.error("CargaPlantilla.subirArchivo(Exception): Guardar Archivo en Disco");
				e.printStackTrace();
				throw new AppException("Ocurrió un error al guardar el archivo");
				
			 }
			 
		 }
		 
	}
 
	// Enviar parametros adicionales
	resultado.put("numeroRegistros",				numeroRegistros			);
	resultado.put("sumatoriaSaldoInsoluto",	sumatoriaSaldoInsoluto	);
	resultado.put("claveMesConciliar",			claveMesConciliar			);
	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "REALIZAR_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)		);

	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.realizarValidacion") 				)	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		estadoSiguiente 	= null;
	String 		msg					= null;
	
	// Leer Parametros
	String 		numeroRegistros 					= (request.getParameter("numeroRegistros")			== null)?"0":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldoInsoluto 			= (request.getParameter("sumatoriaSaldoInsoluto")	== null)?"0":request.getParameter("sumatoriaSaldoInsoluto");
	String 		claveMesConciliar 				= (request.getParameter("claveMesConciliar")			== null)?"" :request.getParameter("claveMesConciliar");
	String 		fileName 							= (request.getParameter("fileName")						== null)?"" :request.getParameter("fileName");
		
	// Variables adicionales
	String   	directorioTemporal  				= strDirectorioTemp ;
	String 		login 								= iNoUsuario;
	
	String 		rutaArchivoZIP						= directorioTemporal + login + "." + fileName;
	String 		claveIF 								= iNoCliente;

	// "Lanzar" thread para realizar la validación del archivo
	ValidaArchivoGarantiasThread validaArchivo = new ValidaArchivoGarantiasThread();
	validaArchivo.setClaveIF(claveIF);
	validaArchivo.setFileValidator(new SaldoInsolutoConciliacionAutomaticaValidator());
	validaArchivo.setDirectorioTemporal(directorioTemporal);
	validaArchivo.setLogin(login);
	validaArchivo.setRutaArchivoZIP(rutaArchivoZIP);
	validaArchivo.setRunning(true);
	validaArchivo.setGarantiasBean(new GarantiasBean());
	validaArchivo.setTotalRegistros(Integer.parseInt(numeroRegistros));
   validaArchivo.setSumatoriaMontos(new BigDecimal(sumatoriaSaldoInsoluto));
   // Agregar parametros especificos
   SaldoInsolutoConciliacionAutomaticaValidator fileValidator = (SaldoInsolutoConciliacionAutomaticaValidator) validaArchivo.getFileValidator();
	fileValidator.setClaveMesConciliar(claveMesConciliar);
	// Finalizar inicializacion
	validaArchivo.initialize();
	request.getSession().setAttribute("29CARGAARCHIVOSLDOSINSOLUTOS_THREAD",validaArchivo); // #TAG
	
	// Obtener numero total de registros que el Thread va a procesar
	int numeroTotalRegistrosProcesar = validaArchivo.getNumberOfRegisters();
	
	// Lanzar Thread
	if( !validaArchivo.hasError() ){
		new Thread(validaArchivo).start();
	}
	
	// Determinar el estado siguiente
	if( !validaArchivo.hasError() ){
		estadoSiguiente = "MUESTRA_PANTALLA_AVANCE";
	} else {
		estadoSiguiente = "ESPERAR_DECISION";
	}
	
	// En caso de que haya algun mensaje mostrarlo
	if( validaArchivo.getMensajes().length() > 0 ){
		msg = validaArchivo.getMensajes();
	}
	
	// Enviar detalle del envío de las notificaciones
	resultado.put("numeroTotalRegistrosProcesar",			String.valueOf( numeroTotalRegistrosProcesar )	);
	resultado.put("numeroRegistrosProcesados",	"0"	 												);
	resultado.put("avance",								"0"	 												);
	resultado.put("porcentajeAvance",				"0"	 												);
	resultado.put("envioFinalizado",					new Boolean(false)	 							);

	// Enviar el total de envios a Flujo de Fondos
	resultado.put("numeroRegistros",						numeroRegistros			);
	resultado.put("sumatoriaSaldoInsoluto",			sumatoriaSaldoInsoluto	);
	resultado.put("claveMesConciliar",					claveMesConciliar			);
	resultado.put("fileName",								fileName						);

	resultado.put("totalRegistros",		"0"			); // rg.getNumRegOk() 
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);

	infoRegresar = resultado.toString();
	
} else if( informacion.equals("CargaArchivo.muestraPantallaAvance") ){
	
	JSONObject	resultado 							= new JSONObject();
	boolean		success	 							= true;
	String 		estadoSiguiente 					= null;
	String		msg									= null;
	
	String 		numeroTotalRegistrosProcesar	= (request.getParameter("numeroTotalRegistrosProcesar")	== null)?"0":request.getParameter("numeroTotalRegistrosProcesar");
	String 		numeroRegistros 					= (request.getParameter("numeroRegistros")					== null)? "":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldoInsoluto 			= (request.getParameter("sumatoriaSaldoInsoluto")			== null)? "":request.getParameter("sumatoriaSaldoInsoluto");
	String 		claveMesConciliar 				= (request.getParameter("claveMesConciliar")					== null)? "":request.getParameter("claveMesConciliar");
	String 		fileName 							= (request.getParameter("fileName")								== null)? "":request.getParameter("fileName");
					
	resultado.put("numeroRegistros",					numeroRegistros			);
	resultado.put("sumatoriaSaldoInsoluto",		sumatoriaSaldoInsoluto	);
	resultado.put("claveMesConciliar",				claveMesConciliar			);
	resultado.put("fileName",							fileName						);
	
	resultado.put("numeroTotalRegistrosProcesar",numeroTotalRegistrosProcesar	);
	resultado.put("numeroRegistrosProcesados",	"0"	 								);
	resultado.put("avance",								"0"	 								);
	resultado.put("porcentajeAvance",				"0"	 								);
	resultado.put("envioFinalizado",					new Boolean(false)				);
	
	// Determinar el estado siguiente
	estadoSiguiente = "ACTUALIZAR_AVANCE";
	
	// Enviar resultado de la operacion
	resultado.put("success", 							new Boolean(success)				);
	resultado.put("estadoSiguiente", 				estadoSiguiente 					);
	resultado.put("msg",									msg									);
	
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("CargaArchivo.actualizarAvance") 		   ){
	
	JSONObject	resultado 						= new JSONObject();
	boolean		success	 						= true;
	String 		estadoSiguiente 				= null;
	String		msg								= null;
	
	// LEER PARAMETROS
	boolean		envioFinalizado 				= "true".equals(request.getParameter("envioFinalizado"))?true:false;
	String 		numeroRegistros 				= (request.getParameter("numeroRegistros")			== null)? "":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldoInsoluto 		= (request.getParameter("sumatoriaSaldoInsoluto")	== null)? "":request.getParameter("sumatoriaSaldoInsoluto");
	String 		claveMesConciliar 			= (request.getParameter("claveMesConciliar")			== null)? "":request.getParameter("claveMesConciliar");
	String 		fileName 						= (request.getParameter("fileName")						== null)? "":request.getParameter("fileName");

	// LEER THREAD DE SESION
	ValidaArchivoGarantiasThread thread 	= (ValidaArchivoGarantiasThread) session.getAttribute("29CARGAARCHIVOSLDOSINSOLUTOS_THREAD");
	
	// UNA VEZ QUE AE HA MOSTRADO QUE SE HA LLEGADO AL 100%, PRESENTAR RESULTADOS
	if(        envioFinalizado && !thread.hasError() ){ // El envio a flujo de fondos fue exitoso

		resultado.put("numeroRegistros",					numeroRegistros					);
		resultado.put("sumatoriaSaldoInsoluto",		sumatoriaSaldoInsoluto			);
		resultado.put("claveMesConciliar",				claveMesConciliar					);
		resultado.put("fileName",							fileName								);
	
		// Determinar el estado siguiente
		estadoSiguiente = "PREPARAR_RESULTADOS_VALIDACION";
		
	// EN CASO DE QUE HAYA FALLADO EL THREAD, MOSTRAR DETALLE DEL ERROR Y REINICIALIZAR LA PANTALLA
	} else if( envioFinalizado &&  thread.hasError() ){ 
	
		// Mostrar mensaje de error mediante un alert.
		msg = thread.getMensajes();
			
		// Remover thread de sesion
		session.removeAttribute("29CARGAARCHIVOSLDOSINSOLUTOS_THREAD");
		
		// DETERMINAR EL ESTADO SIGUIENTE
		estadoSiguiente = "FIN";
			
	// ENVIAR PORCENTAJE DE AVANCE ACTUALIZADO
	} else {
		
		resultado.put("numeroRegistros",					numeroRegistros					);
		resultado.put("sumatoriaSaldoInsoluto",		sumatoriaSaldoInsoluto			);
		resultado.put("claveMesConciliar",				claveMesConciliar					);
		resultado.put("fileName",							fileName								);
		
		// ENVIAR PORCENTAJE DE AVANCE
		resultado.put("numeroTotalRegistrosProcesar",new Integer(thread.getNumberOfRegisters())	);
		resultado.put("numeroRegistrosProcesados",	new Integer(thread.getProcessedRegisters())	);
		resultado.put("avance",								new Double(thread.getPercent()/100.0f)			);
		resultado.put("porcentajeAvance",				new Double(thread.getPercent())					);
		resultado.put("envioFinalizado",             new Boolean(thread.isCompleted())				);
			
		// DETERMINAR EL ESTADO SIGUIENTE
		estadoSiguiente = "ACTUALIZAR_AVANCE";
		
	}
 
	// ENVIAR RESULTADO DE LA OPERACION
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if( informacion.equals("CargaArchivo.prepararResultadosValidacion")  ){

	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	
	// Leer Parametros
	String 		numeroRegistros 						= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldoInsoluto 				= (request.getParameter("sumatoriaSaldoInsoluto")	== null)?"":request.getParameter("sumatoriaSaldoInsoluto");
	String 		claveMesConciliar 					= (request.getParameter("claveMesConciliar")			== null)?"":request.getParameter("claveMesConciliar");
	String 		fileName 								= (request.getParameter("fileName")						== null)?"":request.getParameter("fileName");
		
	ValidaArchivoGarantiasThread validaArchivo 	= (ValidaArchivoGarantiasThread) request.getSession().getAttribute("29CARGAARCHIVOSLDOSINSOLUTOS_THREAD");
	
	// Obtener Resultado de la Validacion
	ResultadosGar rg        = validaArchivo.getResultadosGar();
	
	// Leer Resultado de la Validacion
	String claveProceso 		= String.valueOf(rg.getIcProceso());
	String totalRegistros	= String.valueOf(rg.getNumRegOk());
	String montoTotal 		= String.valueOf(rg.getSumRegOk());

	//  Leer Resultado de la Validacion ( Registros sin Errores )
	String 		 registrosSinErrores 				  = rg.getCorrectos();
	
	StringBuffer 	buffer 	= new StringBuffer();
	JSONArray		registrosSinErroresDataArray = new JSONArray();
	int				ctaRegistros = 0;
		
	for(int i=0;i<registrosSinErrores.length();i++){
		char lastChar = registrosSinErrores.charAt(i);
		if(lastChar == '\n'){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosSinErroresDataArray.add(registro);
			buffer.setLength(0);
		} else {
			buffer.append(lastChar);
		}
	}
	if(buffer.length() > 0){
		JSONArray registro = new JSONArray();
		registro.add(String.valueOf(ctaRegistros++));
		registro.add(buffer.toString());
		registrosSinErroresDataArray.add(registro);
		buffer.setLength(0);
	}
	// Si el primer registro dice: Claves de Financiamiento, borrarlo
	if( 	registrosSinErroresDataArray.size() > 0 ){
				
		JSONArray registro = (JSONArray) registrosSinErroresDataArray.get(0);
		if("Claves de la garantía:".equals( registro.getString(1) )){
			registrosSinErroresDataArray.remove(0);
		}
			
	}

	String numeroRegistrosSinErrores 			= Comunes.formatoDecimal(rg.getNumRegOk(),0);
	String sumatoriaSaldoInsolutoSinErrores	= "$ "+Comunes.formatoDecimal(rg.getSumRegOk(),2);
 
	//  Leer Resultado de la Validacion ( Registros con Errores )
	String registrosConErrores 				= rg.getErrores();

	buffer 			= new StringBuffer();
	JSONArray		registrosConErroresDataArray = new JSONArray();
	ctaRegistros 	= 0;
		
	for(int i=0;i<registrosConErrores.length();i++){
		char lastChar = registrosConErrores.charAt(i);
		if(lastChar == '\n'){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosConErroresDataArray.add(registro);
			buffer.setLength(0);
		} else {
			buffer.append(lastChar);
		}
	}
	if(buffer.length() > 0){
		JSONArray registro = new JSONArray();
		registro.add(String.valueOf(ctaRegistros++));
		registro.add(buffer.toString());
		registrosConErroresDataArray.add(registro);
		buffer.setLength(0);
	}
		
	String numeroRegistrosConErrores 			= Comunes.formatoDecimal(rg.getNumRegErr(),0);
	String sumatoriaSaldoInsolutoConErrores	= "$ " + Comunes.formatoDecimal(rg.getSumRegErr(),2); 
 
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	String erroresVsCifrasControl = rg.getCifras();
	
	buffer 			= new StringBuffer();
	JSONArray		erroresVsCifrasControlDataArray = new JSONArray();
	ctaRegistros 	= 0;
		
	for(int i=0;i<erroresVsCifrasControl.length();i++){
		char lastChar = erroresVsCifrasControl.charAt(i);
		if(lastChar == '\n'){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			erroresVsCifrasControlDataArray.add(registro);
			buffer.setLength(0);
		} else {
			buffer.append(lastChar);
		}
	}
	if(buffer.length() > 0){
		JSONArray registro = new JSONArray();
		registro.add(String.valueOf(ctaRegistros++));
		registro.add(buffer.toString());
		erroresVsCifrasControlDataArray.add(registro);
		buffer.setLength(0);
	}
		
	// Determinar si se mostrará el boton continuar carga	
	Boolean continuarCarga 								= ("".equals(rg.getErrores())&&"".equals(rg.getCifras()))
																					?new Boolean(true):new Boolean(false);

	// Remover thread de sesion
	session.removeAttribute("29CARGAARCHIVOSLDOSINSOLUTOS_THREAD");
		
	// Enviar parametros adicionales
	resultado.put("numeroRegistros",										numeroRegistros							);
	resultado.put("sumatoriaSaldoInsoluto",							sumatoriaSaldoInsoluto					);
	resultado.put("claveMesConciliar",									claveMesConciliar							);
	resultado.put("fileName",												fileName										);

	// Enviar resultado general de la validacion
	resultado.put("claveProceso",											claveProceso								);
	resultado.put("totalRegistros", 										totalRegistros								);
	resultado.put("montoTotal",											montoTotal									);
	
	resultado.put("registrosSinErroresDataArray",					registrosSinErroresDataArray			);
	resultado.put("numeroRegistrosSinErrores",						numeroRegistrosSinErrores				);
	resultado.put("sumatoriaSaldoInsolutoSinErrores",				sumatoriaSaldoInsolutoSinErrores		);
	
	resultado.put("registrosConErroresDataArray",					registrosConErroresDataArray			);
	resultado.put("numeroRegistrosConErrores",						numeroRegistrosConErrores				);
	resultado.put("sumatoriaSaldoInsolutoConErrores",				sumatoriaSaldoInsolutoConErrores		);
	
	resultado.put("erroresVsCifrasControlDataArray",				erroresVsCifrasControlDataArray		);
	resultado.put("continuarCarga",										continuarCarga								);
		
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.presentarPreacuse") 			)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveProceso 							= (request.getParameter("claveProceso")						== null)?"":request.getParameter("claveProceso");
	String 		totalRegistros 						= (request.getParameter("totalRegistros")						== null)?"":request.getParameter("totalRegistros");
	String 		montoTotal 								= (request.getParameter("montoTotal")							== null)?"":request.getParameter("montoTotal");

	String 		numeroRegistros 						= (request.getParameter("numeroRegistros")					== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldoInsoluto 				= (request.getParameter("sumatoriaSaldoInsoluto")			== null)?"":request.getParameter("sumatoriaSaldoInsoluto");
	String 		claveMesConciliar 					= (request.getParameter("claveMesConciliar")					== null)?"":request.getParameter("claveMesConciliar");
	String 		fileName 								= (request.getParameter("fileName")								== null)?"":request.getParameter("fileName");
	
	// Obtener Descripción del Tipo de 
	String 				tipoOperacion 	= "";
	// Preparar consulta
	CatalogoSimple 	cat 				= new CatalogoSimple();
	cat.setTabla("gticat_tipo_conciliacion");
	cat.setCampoClave("ic_tipo_conciliacion");
	cat.setCampoDescripcion("cg_descripcion");
	List claves = new ArrayList();
	claves.add("1");	// 1 = SALDO INSOLUTO // #TAG
	cat.setValoresCondicionIn(claves);
	// Consultar descripcion del Tipo de Operacion 1.
	List listaResultados = cat.getListaElementos();
	if (listaResultados!=null && listaResultados.size()>0) {
		tipoOperacion = Comunes.rellenaCeros(((ElementoCatalogo)listaResultados.get(0)).getClave(),2) + " " + ((ElementoCatalogo)listaResultados.get(0)).getDescripcion();
	}

	// Construir Array con los datos del preacuse
	JSONArray 	registrosPorAgregarDataArray 		= new JSONArray();
	JSONArray	registro									= null;

	registro = new JSONArray();
	registro.add("Tipo de Operación");
	registro.add(tipoOperacion);
	registrosPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("No. total de registros");
	registro.add( Comunes.formatoDecimal(totalRegistros,0) );
	registrosPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("Saldo Insoluto"); // #TAG
	registro.add( Comunes.formatoDecimal(montoTotal,2) 	 );
	registrosPorAgregarDataArray.add(registro);
	
	// Hacer eco de los parametros de la carga
	resultado.put("claveProceso",								claveProceso);
	resultado.put("totalRegistros",							totalRegistros);
	resultado.put("montoTotal",								montoTotal);
	
	resultado.put("numeroRegistros",							numeroRegistros);
	resultado.put("sumatoriaSaldoInsoluto",				sumatoriaSaldoInsoluto);
	resultado.put("claveMesConciliar",						claveMesConciliar);
	resultado.put("fileName",									fileName);

	// Enviar descripcion de los registros por agregar
	resultado.put("registrosPorAgregarDataArray", 		registrosPorAgregarDataArray);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 						"ESPERAR_DECISION_PREACUSE" );
	// Enviar resultado de la operacion
	resultado.put("success", 									new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.transmitirRegistros")		)  {
 
	JSONObject		resultado				= new JSONObject();
	String 			estadoSiguiente		= null;
	boolean			success					= true;
	boolean			hayError					= false;
	ResultadosGar 	rg 						= null;
	String 			msg 						= "";
	
	// Leer campos del preacuse
	String claveProceso 						= (request.getParameter("claveProceso")						== null)?"0"		:request.getParameter("claveProceso");
	String totalRegistros 					= (request.getParameter("totalRegistros")						== null)?"0"		:request.getParameter("totalRegistros");
	String montoTotal 						= (request.getParameter("montoTotal")							== null)?"0.00"	:request.getParameter("montoTotal");
	
	// Leer campos de entrada
	String numeroRegistros 					= (request.getParameter("numeroRegistros")					== null)?""			:request.getParameter("numeroRegistros");
	String sumatoriaSaldoInsoluto			= (request.getParameter("sumatoriaSaldoInsoluto")			== null)?""			:request.getParameter("sumatoriaSaldoInsoluto");
	String claveMesConciliar 				= (request.getParameter("claveMesConciliar")					== null)?""			:request.getParameter("claveMesConciliar");
	String fileName 							= (request.getParameter("fileName")								== null)?""			:request.getParameter("fileName");

	// Leer campos de la firma digital
	String isEmptyPkcs7 						= (request.getParameter("isEmptyPkcs7")	 					== null)?"false"	:request.getParameter("isEmptyPkcs7");
	String textoFirmado						= (request.getParameter("textoFirmado")	 					== null)?""			:request.getParameter("textoFirmado");
	String pkcs7								= (request.getParameter("pkcs7")			 	 					== null)?""			:request.getParameter("pkcs7");

	if( session.getAttribute("rgprint")!=null ){
		msg 					= "Para realizar otra operacion por favor seleccione la opcion Cargar Archivo en el menu Saldos Insolutos "; // #TAG
		estadoSiguiente 	= "ESPERAR_DECISION";
		hayError				= true;
	} else {
		session.setAttribute("rgprint", new ResultadosGar() ); //Se establece la variable de sesión para controlar envíos duplicados
	} 
	
	if( !hayError ){
		
		// Obtener instancia del EJB de Garantias
		Garantias garantias = null;
		try {
					
			garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

		} catch(Exception e) {
			
			log.error("CargaArchivo.transmitirRegistros(Exception): Obtener instancia del EJB de Garantías");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
			
		}
		
		// Se declaran variables adicionales
		String 									folioCert 		= "";
		String 									externContent 	= textoFirmado;
		char 										getReceipt 		= 'Y';
		netropology.utilerias.Seguridad 	s 					= null;
		String 									claveIF 			= iNoCliente;
		String 									loginUsuario	= iNoUsuario;
		
		/* Nota: Para otros navegadores diferentes de IE y Mozilla
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		*/
			
		// Autenticar certificado
		if ( !_serial.equals("") && !externContent.equals("") && !pkcs7.equals("") ) {
			
			folioCert 	= "11CRSLIN"+claveIF+new SimpleDateFormat("ddMMyyyy").format(new java.util.Date()); // #TAG
			s 				= new netropology.utilerias.Seguridad();
 
			if ( s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {	
				
				String			directorioTemporal  		= strDirectorioTemp;
				String			login 						= iNoUsuario;
				
				String 			claveFiso   				= garantias.getClaveFISO(claveIF);
				String 			claveIfSiag 				= String.valueOf(garantias.getClaveIfSiag(claveIF));
				String			claveTipoConciliacion	= "1";   // CLAVE 1, SALDO INSOLUTO DE GTICAT_TIPO_CONCILIACION // #TAG
				
				rg = AuxiliarConciliacionAutomatica.transmitirCargaPortafolioIfConciliacionAutomatica( directorioTemporal, login, fileName, s.getAcuse(), numeroRegistros, sumatoriaSaldoInsoluto, claveMesConciliar, claveFiso, claveIfSiag, claveTipoConciliacion );
				
				String 			 rutaArchivoZip 			= directorioTemporal + login + "." + fileName;
				File 				 archivoZip 				= new File(rutaArchivoZip);
				FileInputStream archivoZipInputstream 	= new FileInputStream(archivoZip);
				
				estadoSiguiente 	= "MOSTRAR_ACUSE_TRANSMISION_REGISTROS";
				
			} else {
				
				msg 					= "La autentificacion no se llevo a cabo: "+s.mostrarError();
				estadoSiguiente 	= "ESPERAR_DECISION";
				hayError				= true;
				
			}
			
		}else{
			
			try {
				throw new NafinException("GRAL0021"); // La autentificacion no se llevo a cabo
			}catch(Exception e){
				estadoSiguiente 	= "ESPERAR_DECISION";
				msg					= e.getMessage();
				hayError				= true;
			}
			
		}
 
	}
	
	// Poner en sesion el resultado de la transmicion de solicitudes
	if( "MOSTRAR_ACUSE_TRANSMISION_REGISTROS".equals(estadoSiguiente) ){
		
		// Obtener Descripción del Tipo de Operación
		String 				tipoOperacion 	= "";
		// Preparar consulta
		CatalogoSimple 	cat 				= new CatalogoSimple();
		cat.setTabla("gticat_tipo_conciliacion");
		cat.setCampoClave("ic_tipo_conciliacion");
		cat.setCampoDescripcion("cg_descripcion");
		List claves = new ArrayList();
		claves.add("1");	// 1 = SALDO INSOLUTO // #TAG
		cat.setValoresCondicionIn(claves);
		// Consultar descripcion del Tipo de Operacion 1.
		List listaResultados = cat.getListaElementos();
		if (listaResultados!=null && listaResultados.size()>0) {
			tipoOperacion = Comunes.rellenaCeros(((ElementoCatalogo)listaResultados.get(0)).getClave(),2) + " " + ((ElementoCatalogo)listaResultados.get(0)).getDescripcion();
		}
	
		session.setAttribute("rgprint",	rg);
		resultado.put("folioSolicitud", 	"Su solicitud fue recibida con el número de folio: " + rg.getFolio() );
	
		// Construir Array con los datos del acuse
		JSONArray 	registrosTransmitidosDataArray 	= new JSONArray();
		JSONArray	registro								= null;
 
		registro = new JSONArray();
		registro.add("Tipo de Operación");
		registro.add(tipoOperacion);
		registrosTransmitidosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("No. total de registros");
		registro.add(Comunes.formatoDecimal(totalRegistros,0));
		registrosTransmitidosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Saldo Insoluto");
		registro.add(Comunes.formatoDecimal(montoTotal,2));
		registrosTransmitidosDataArray.add(registro);
 
		registro = new JSONArray();
		registro.add("Fecha de carga");
		registro.add(rg.getFecha());
		registrosTransmitidosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Hora de carga");
		registro.add(rg.getHora());
		registrosTransmitidosDataArray.add(registro);
				
		registro = new JSONArray();
		registro.add("Usuario");
		registro.add(iNoUsuario+" - "+strNombreUsuario);
		registrosTransmitidosDataArray.add(registro);
 
		resultado.put("registrosTransmitidosDataArray",		registrosTransmitidosDataArray	);
		
		// Hacer eco de los parametros de la carga
		resultado.put("claveProceso",							claveProceso						);
		resultado.put("totalRegistros",						totalRegistros						);
		resultado.put("montoTotal",							montoTotal							);
		
		resultado.put("numeroRegistros",						numeroRegistros					);
		resultado.put("sumatoriaSaldoInsoluto",			sumatoriaSaldoInsoluto			);
		resultado.put("claveMesConciliar",					claveMesConciliar					);
		resultado.put("fileName",								fileName								);
		
		resultado.put("folio",									rg.getFolio()						);
		
	}
	
	resultado.put("msg",											msg									);	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 						estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 									new Boolean(success)				);
	
	infoRegresar = resultado.toString();
 
} else if (    informacion.equals("CargaArchivo.fin") 							)	{

	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("29CargaArchivoSldosInsolutos01ext.jsp"); 
	
} else if ( 	informacion.equals("ExtraerErroresValidacion")					)	{
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	
	// Leer parametros
	String detalleErroresVsCifrasControl	= (request.getParameter("detalleErroresVsCifrasControl")	== null)?"[]":request.getParameter("detalleErroresVsCifrasControl");
	String detalleRegistrosConErrores 		= (request.getParameter("detalleRegistrosConErrores")		== null)?"[]":request.getParameter("detalleRegistrosConErrores");
 
	String 		urlArchivo 		= "";
	try {
		
		String nombreArchivo = CargaArchivoSaldoInsoluto.generaArchivoCSVErrores(
			detalleErroresVsCifrasControl,
			detalleRegistrosConErrores,
			strDirectorioTemp,
			iNoUsuario // loginUsuario
		); 
		
		urlArchivo = strDirecVirtualTemp+nombreArchivo;
		
	}catch(Exception e){
			
		log.error("ExtraerErroresValidacion(Exception)");
		log.error("ExtraerErroresValidacion.detalleErroresVsCifrasControl = <" + detalleErroresVsCifrasControl + ">");
		log.error("ExtraerErroresValidacion.detalleRegistrosConErrores    = <" + detalleRegistrosConErrores    + ">");
		log.error("ExtraerErroresValidacion.strDirectorioTemp             = <" + strDirectorioTemp             + ">");
		log.error("ExtraerErroresValidacion.iNoUsuario                    = <" + iNoUsuario                    + ">"); // loginUsuario
		e.printStackTrace();
	 
		msg		= e.getMessage();
		success	= false;
			
	}
		
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("GeneraArchivoPDF")								)	{
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	
	// Leer parametros
	String 		totalRegistros = (request.getParameter("totalRegistros")	== null)?"":request.getParameter("totalRegistros");
	String 		montoTotal 		= (request.getParameter("montoTotal")		== null)?"":request.getParameter("montoTotal");
	
	String 		urlArchivo 		= "";
	HashMap 		cabecera 		= null;
	try {
		
		// Crear map con la informacion del cabcera del PDF
		cabecera = new HashMap();
		cabecera.put("strPais", 				(String) session.getAttribute("strPais"));
		cabecera.put("iNoNafinElectronico", (String) ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()));
		cabecera.put("sesExterno", 			(String) session.getAttribute("sesExterno"));
		cabecera.put("strNombre", 				(String) session.getAttribute("strNombre"));
		cabecera.put("strNombreUsuario", 	(String) session.getAttribute("strNombreUsuario"));
		cabecera.put("strLogo", 				(String) session.getAttribute("strLogo"));
		
		String nombreArchivo = CargaArchivoSaldoInsoluto.generaAcuseArchivoPDF(
			(ResultadosGar)session.getAttribute("rgprint"),
			strDirectorioTemp,
			cabecera,
			strDirectorioPublicacion,
			totalRegistros,
			montoTotal,
			iNoUsuario, // loginUsuario
			strNombreUsuario
		); 
		
		urlArchivo = strDirecVirtualTemp+nombreArchivo;
		 
	}catch(Exception e){
			
		log.error("GeneraArchivoPDF(Exception)");
		log.error("GeneraArchivoPDF.rg                       = <" + (ResultadosGar)session.getAttribute("rgprint") + ">");
		log.error("GeneraArchivoPDF.strDirectorioTemp        = <" + strDirectorioTemp                              + ">");
		log.error("GeneraArchivoPDF.cabecera                 = <" + cabecera                                       + ">");
		log.error("GeneraArchivoPDF.strDirectorioPublicacion = <" + strDirectorioPublicacion                       + ">");
		log.error("GeneraArchivoPDF.totalRegistros           = <" + totalRegistros                                 + ">");
		log.error("GeneraArchivoPDF.montoTotal               = <" + montoTotal                                     + ">");
		log.error("GeneraArchivoPDF.iNoUsuario               = <" + iNoUsuario                                     + ">");
		log.error("GeneraArchivoPDF.strNombreUsuario         = <" + strNombreUsuario                               + ">");
		e.printStackTrace();
		
		msg		= e.getMessage();
		success	= false;
			
	}
		
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
 
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>