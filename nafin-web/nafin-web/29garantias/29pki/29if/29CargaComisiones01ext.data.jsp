<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		java.io.OutputStream,
		java.io.PrintStream,
		java.io.File,
		java.io.BufferedInputStream, 
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*,
		com.netro.afiliacion.ConsCargaProvEcon, 
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.afiliacion.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession_extjs.jspf" %>
<%@ include file="/29garantias/29pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("CargaArchivo.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;
	
	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
				
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){
 
		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}
	
	// 2. Validar Numero Fiso
	if ( !hayAviso && !seguridad.validaNumeroFISO(claveIF) ) {
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se tiene definido un Fideicomiso para realizar sus operaciones.<br/><br/>Por favor comuníquese al Centro de Atención a clientes al teléfono 50-89-61-07 <br/>o del Interior al 01-800-NAFINSA (01-800-6234672).";
		hayAviso				= true;
	}

	// 3. Remover variables de sesion
	if ( !hayAviso ){
		session.removeAttribute("CLAVES_FINAN");
		session.removeAttribute("rgprint");
	}
	
	if(!hayAviso){
		
		String saldosLayout = 
			"<table width=\"100%\">"  +
			"<tr>"  +
			"	<td class=\"titulos\" align=\"center\">"  +
			"		<span class=\"titulos\">"  +
			"			Estructura de Archivo<br>Carga masiva"  +
			"		</span>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td colspan=\"2\" align=\"center\">"  +
			"		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" >"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"center\" colspan=\"10\"  style=\"height:30px;\" >"  +
			"					Layout de Carga de Comisiones <br>"  +
			"					Archivo de extensión TXT ó ZIP; los campos deben de estar separados por arrobas (\"@\")"  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr> "  +
			"				<td class=\"celda01\" align=\"center\" width=\"25px\"  style=\"height:30px;\" >No.</td> "  +
			"				<td class=\"celda01\" align=\"center\">Descripción</td> "  +
			"				<td class=\"celda01\" align=\"center\">Tipo</td> "  +
			"				<td class=\"celda01\" align=\"center\">Longitud<br>Máxima</td> "  +
			"				<td class=\"celda01\" align=\"center\">Obligatorio&nbsp;/&nbsp;Observaciones</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\"  style=\"height:30px;\" >1.</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Nombre o Razón Social del Acreditado "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"left\">Texto</td> "  +
			"				<td class=\"formas\" align=\"center\">240</td> "  +
			"				<td class=\"formas\" align=\"center\">Sí</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\"  style=\"height:30px;\" >2.</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					CURP "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"left\">Texto</td> "  +
			"				<td class=\"formas\" align=\"center\">18</td> "  +
			"				<td class=\"formas\" align=\"center\">Sí</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\"  style=\"height:30px;\" >3.</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Clave del Financiamiento "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"left\">Texto</td> "  +
			"				<td class=\"formas\" align=\"center\">30</td> "  +
			"				<td class=\"formas\" align=\"center\">Sí</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\"  style=\"height:30px;\" >4.</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto del Financiamiento "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"left\">Numérico</td> "  +
			"				<td class=\"formas\" align=\"center\">17,2</td> "  +
			"				<td class=\"formas\" align=\"center\">Sí</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\"  style=\"height:30px;\" >5.</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Clave del Intermediario Financiero "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"left\">Numérico</td> "  +
			"				<td class=\"formas\" align=\"center\">5</td> "  +
			"				<td class=\"formas\" align=\"center\">Sí</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\"  style=\"height:30px;\" >6.</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto Comisión a Cobrar "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"left\">Numérico</td> "  +
			"				<td class=\"formas\" align=\"center\">26,6</td> "  +
			"				<td class=\"formas\" align=\"center\">Sí</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\"  style=\"height:30px;\" >7.</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto IVA "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"left\">Numérico</td> "  +
			"				<td class=\"formas\" align=\"center\">26,6</td> "  +
			"				<td class=\"formas\" align=\"center\">Sí</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\"  style=\"height:30px;\" >8.</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Total Monto a Cobrar "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"left\">Numérico</td> "  +
			"				<td class=\"formas\" align=\"center\">26,6</td> "  +
			"				<td class=\"formas\" align=\"center\">Sí</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\"  style=\"height:30px;\" >9.</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Fecha de Pago de la Comisión "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"left\">Fecha(AAAAMMDD)</td> "  +
			"				<td class=\"formas\" align=\"center\">8</td> "  +
			"				<td class=\"formas\" align=\"center\">Sí</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\"  style=\"height:30px;\" >10.</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto Pagado a la SHF "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"left\">Numérico</td> "  +
			"				<td class=\"formas\" align=\"center\">26,6</td> "  +
			"				<td class=\"formas\" align=\"center\">Sí</td> "  +
			"			</tr> "  +
			"		</table>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"</table>";	
		resultado.put("saldosLayout", 	saldosLayout			);
		
	}
	
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_SOLICITUD_DE_CARGA";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();

} else if (          informacion.equals("CargaArchivo.enviarSolicitudDeCarga") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String		estadoSiguiente	= null;
	String  		msg					= "";
	
	String 	claveMes 				= (request.getParameter("claveMes")						== null)?"":request.getParameter("claveMes");
	String 	numeroRegistros 		= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String 	montoTotalComisiones = (request.getParameter("montoTotalComisiones")		== null)?"":request.getParameter("montoTotalComisiones");
	String   claveIF					= iNoCliente;
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
			
	} catch(Exception e) {
	 
		log.error("CargaArchivo.realizarValidacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
		
	}
	
	// Revisar que no se haya enviado registros previamente para el mes y año indicados 
	String 	anioCarga 	= claveMes.substring(0,4);
	String 	mesCarga  	= claveMes.substring(4,6);
	boolean 	existeEnvio = garantias.existeEnvioComisionesParaValidacionYCarga(claveIF,anioCarga,mesCarga);
	
	if(existeEnvio){
		estadoSiguiente 	= "ESPERAR_DECISION";
		msg				 	= "No se puede realizar la carga. Ya existe información para el año-mes seleccionado.";
	} else {
		estadoSiguiente 	= "ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES";
	}

   resultado.put("claveMes",					claveMes						);
	resultado.put("numeroRegistros",			numeroRegistros			);
	resultado.put("montoTotalComisiones",	montoTotalComisiones		);
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)			);
	resultado.put("estadoSiguiente", 	estadoSiguiente				);
	resultado.put("msg", 					msg								);
	infoRegresar = resultado.toString();
	
} else if (        	informacion.equals("CargaArchivo.subirArchivo") 				)	{	
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
 
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(8097152);
		myUpload.upload();
		
	} catch(Exception e) {
			
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 8097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 8 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
 
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	String claveMes 					= (myRequest.getParameter("claveMes1")						== null)?"":myRequest.getParameter("claveMes1");
	String numeroRegistros 			= (myRequest.getParameter("numeroRegistros1")			== null)?"":myRequest.getParameter("numeroRegistros1");
	String montoTotalComisiones	= (myRequest.getParameter("montoTotalComisiones1")		== null)?"":myRequest.getParameter("montoTotalComisiones1");

	// Guardar Archivo en Disco
	int numFiles = 0;
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," ")	 );
		resultado.put("fileName", 	myFile.getFileName().replaceAll("\\s+"," ")			);

	}catch(Exception e){
		
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
	
	// Enviar parametros adicionales
	resultado.put("claveMes",					claveMes						);
	resultado.put("numeroRegistros",			numeroRegistros			);
	resultado.put("montoTotalComisiones",	montoTotalComisiones		);
 
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 		"REALIZAR_VALIDACION" 	);
	// Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success)		);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.realizarValidacion") 				)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveMes 					= (request.getParameter("claveMes")						== null)?"":request.getParameter("claveMes");
	String 		numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String 		montoTotalComisiones 	= (request.getParameter("montoTotalComisiones")		== null)?"":request.getParameter("montoTotalComisiones");
	String 		fileName 					= (request.getParameter("fileName")						== null)?"":request.getParameter("fileName");
	
	String 		rutaArchivo 				= strDirectorioTemp + iNoUsuario + "." + fileName	;
	String 		claveIF 						= iNoCliente;
	
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
				
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);;
			
	} catch(Exception e) {
	 
		log.error("CargaArchivo.realizarValidacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
		
	}
			
	// Realizar validacion
	ResultadosGar rg = garantias.procesarCargaComisiones(
									strDirectorioTemp,
									rutaArchivo,
									numeroRegistros,
									montoTotalComisiones,
									claveIF,
									claveMes
								);
	
	// Guardar en sesión las claves de financiamiento
	List		  clavesFinanciamiento = rg.getClavesFinan();
	session.setAttribute("CLAVES_FINAN",clavesFinanciamiento);
 
	// Leer Resultado de la Validacion
	String claveProceso 		= String.valueOf(rg.getIcProceso());
	String totalRegistros	= String.valueOf(rg.getNumRegOk());
	String montoTotal 		= String.valueOf(rg.getSumRegOk());

	//  Leer Resultado de la Validacion ( Registros sin Errores )
	String 		 registrosSinErrores 				  = rg.getCorrectos();
	
		StringBuffer 	buffer 	= new StringBuffer();
		JSONArray		registrosSinErroresDataArray = new JSONArray();
		int				ctaRegistros = 0;
		
		for(int i=0;i<registrosSinErrores.length();i++){
			char lastChar = registrosSinErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosSinErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosSinErroresDataArray.add(registro);
			buffer.setLength(0);
		}
 
	String numeroRegistrosSinErrores 		= String.valueOf(rg.getNumRegOk());
	String montoTotalComisionesSinErrores	= "$ "+Comunes.formatoDecimal(rg.getSumRegOk(),2);
 
	//  Leer Resultado de la Validacion ( Registros con Errores )
	String registrosConErrores 				= rg.getErrores();

		buffer 			= new StringBuffer();
		JSONArray		registrosConErroresDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<registrosConErrores.length();i++){
			char lastChar = registrosConErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosConErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosConErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		
	String numeroRegistrosConErrores 		= String.valueOf(rg.getNumRegErr());
	String montoTotalComisionesConErrores	= "$ " + Comunes.formatoDecimal(rg.getSumRegErr(),2); 
 
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	String erroresVsCifrasControl = rg.getCifras();
	
		buffer 			= new StringBuffer();
		JSONArray		erroresVsCifrasControlDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<erroresVsCifrasControl.length();i++){
			char lastChar = erroresVsCifrasControl.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				erroresVsCifrasControlDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			erroresVsCifrasControlDataArray.add(registro);
			buffer.setLength(0);
		}
		
	// Determinar si se mostrará el boton continuar carga	
	Boolean continuarCarga = ("".equals(rg.getErrores())&&"".equals(rg.getCifras())) ? new Boolean(true): new Boolean(false);

	// Enviar parametros adicionales
	resultado.put("claveMes",												claveMes												);
	resultado.put("numeroRegistros",										numeroRegistros									);
	resultado.put("montoTotalComisiones",								montoTotalComisiones								);

	// Enviar resultado general de la validacion
	resultado.put("claveProceso",											claveProceso										);
	resultado.put("totalRegistros", 										totalRegistros										);
	resultado.put("montoTotal",											montoTotal											);
	
	resultado.put("registrosSinErroresDataArray",					registrosSinErroresDataArray					);
	resultado.put("numeroRegistrosSinErrores",						numeroRegistrosSinErrores						);
	resultado.put("montoTotalComisionesSinErrores",					montoTotalComisionesSinErrores				);
	
	resultado.put("registrosConErroresDataArray",					registrosConErroresDataArray					);
	resultado.put("numeroRegistrosConErrores",						numeroRegistrosConErrores						);
	resultado.put("montoTotalComisionesConErrores",					montoTotalComisionesConErrores				);
	
	resultado.put("erroresVsCifrasControlDataArray",				erroresVsCifrasControlDataArray				);
	resultado.put("continuarCarga",										continuarCarga										);
		
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.presentarPreacuse") 			)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveProceso 							= (request.getParameter("claveProceso")					== null)?"":request.getParameter("claveProceso");
	String 		totalRegistros 						= (request.getParameter("totalRegistros")					== null)?"":request.getParameter("totalRegistros");
	String 		montoTotal 								= (request.getParameter("montoTotal")						== null)?"":request.getParameter("montoTotal");
	
	String 		claveMes 								= (request.getParameter("claveMes")							== null)?"":request.getParameter("claveMes");
	String 		numeroRegistros 						= (request.getParameter("numeroRegistros")				== null)?"":request.getParameter("numeroRegistros");
	String 		montoTotalComisiones 				= (request.getParameter("montoTotalComisiones")			== null)?"":request.getParameter("montoTotalComisiones");
	
	// Construir Array con los datos del preacuse
	JSONArray 	registrosPorAgregarDataArray 		= new JSONArray();
	JSONArray	registro									= null;

	// Determinar el mes de carga
	String 	meses[]		={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	int 		indice 		= Integer.parseInt( claveMes.substring(4,6) ) - 1;
	String   nombreMes	= meses[indice] ;
	// Determinar el año de la carga
	String 	numeroAnio 	= claveMes.substring(0,4);

	registro = new JSONArray();
	registro.add("Tipo de Operación");
	registro.add("09 Comisiones");
	registrosPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("Mes de carga");
	registro.add( nombreMes + " de " + numeroAnio );
	registrosPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("No. total de registros transmitidos");
	registro.add(totalRegistros);
	registrosPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("Monto total de los registros transmitidos");
	registro.add(Comunes.formatoDecimal(montoTotal,2));
	registrosPorAgregarDataArray.add(registro);
	
	// Hacer eco de los parametros de la carga
	resultado.put("claveProceso",								claveProceso);
	resultado.put("totalRegistros",							totalRegistros);
	resultado.put("montoTotal",								montoTotal);
	
	resultado.put("claveMes",									claveMes);
	resultado.put("numeroRegistros",							numeroRegistros);
	resultado.put("montoTotalComisiones",					montoTotalComisiones);

	// Enviar descripcion de los registros por agregar
	resultado.put("registrosPorAgregarDataArray", 		registrosPorAgregarDataArray);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_PREACUSE" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.transmitirRegistros")		)  {
 
	JSONObject		resultado							= new JSONObject();
	String 			estadoSiguiente					= null;
	boolean			success								= true;
	boolean			hayError								= false;
	ResultadosGar 	rg 									= null;
	String 			msg 									= "";
	String 			acuseFirmaDigital 				= "";
	String 			loginUsuario						= iNoUsuario;
	
	// Leer campos del preacuse
	String claveProceso 				= (request.getParameter("claveProceso")				== null)?"0"		:request.getParameter("claveProceso");
	String totalRegistros 			= (request.getParameter("totalRegistros")				== null)?"0"		:request.getParameter("totalRegistros");
	String montoTotal 				= (request.getParameter("montoTotal")					== null)?"0.00"	:request.getParameter("montoTotal");
	
	String claveMes 					= (request.getParameter("claveMes")						== null)?""			:request.getParameter("claveMes");
	String numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?""			:request.getParameter("numeroRegistros");
	String montoTotalComisiones	= (request.getParameter("montoTotalComisiones")		== null)?""			:request.getParameter("montoTotalComisiones");

	// Leer campos de la firma digital
	String isEmptyPkcs7 				= (request.getParameter("isEmptyPkcs7")	 			== null)?"false"	:request.getParameter("isEmptyPkcs7");
	String textoFirmado				= (request.getParameter("textoFirmado")	 			== null)?""			:request.getParameter("textoFirmado");
	String pkcs7						= (request.getParameter("pkcs7")			 	 			== null)?""			:request.getParameter("pkcs7");

	if( session.getAttribute("rgprint")!=null ){
		msg 					= "Para realizar otra operacion por favor seleccione la opcion Cargar Archivo en el menu Carga de Saldos y Pago de Comisiones";
		estadoSiguiente 	= "ESPERAR_DECISION";
		hayError				= true;
	} else {
		session.setAttribute("rgprint", new ResultadosGar() ); //Se establece la variable de sesión para controlar envíos duplicados
	} 
	
	if( !hayError ){
		
		// Obtener instancia del EJB de Garantias
		Garantias garantias = null;
		try {
					
			garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);

		} catch(Exception e) {
			
			log.error("CargaArchivo.transmitirRegistros(Exception): Obtener instancia del EJB de Garantías");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
			
		}
		
		// Se declaran variables adicionales
		String 									folioCert 			= "";		
		String 									externContent 		= textoFirmado;
		char 										getReceipt 			= 'Y';
		netropology.utilerias.Seguridad 	s 						= null;
		String 									claveIF 				= iNoCliente;
 
		/* Nota: Para otros navegadores diferentes de IE y Mozilla
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		*/
			
		// Autenticar certificado
		if ( !_serial.equals("") && !externContent.equals("") && !pkcs7.equals("") ) {
			
			folioCert 	= "09CRCM"+claveIF+new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			s 				= new netropology.utilerias.Seguridad();
			
			List clavesFinanciamiento = (List)session.getAttribute("CLAVES_FINAN");
			if ( s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {	
				
				acuseFirmaDigital = s.getAcuse();
				rg 					= garantias.transmitirComisiones( 
												claveProceso, 
												claveIF, 
												numeroRegistros, 
												montoTotalComisiones, 
												claveMes, 
												loginUsuario,
												acuseFirmaDigital
										  ); 
				
				estadoSiguiente 	= "MOSTRAR_ACUSE_TRANSMISION_REGISTROS";
			}else{
				msg 					= "La autentificacion no se llevo a cabo: "+s.mostrarError();
				estadoSiguiente 	= "ESPERAR_DECISION";
				hayError				= true;
			}
			
		} else {
			
			try {
				throw new NafinException("GRAL0021");
			}catch(Exception e){
				estadoSiguiente 	= "ESPERAR_DECISION";
				msg					= e.getMessage();
				hayError				= true;
			}
			
		}
 
	}
	
	// Poner en sesion el resultado de la transmicion de solicitudes
	if( "MOSTRAR_ACUSE_TRANSMISION_REGISTROS".equals(estadoSiguiente) ){
		
		session.setAttribute("rgprint",	rg);
		resultado.put("folioSolicitud", 	"Su solicitud fue recibida con el número de folio: " + rg.getFolio() );
	
		// Determinar el mes de carga
		String 	meses[]				= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		int 		indice 				= Integer.parseInt( claveMes.substring(4,6) ) - 1;
		String   nombreMes			= meses[indice] ;
		// Determinar el año de la carga
		String 	numeroAnio 			= claveMes.substring(0,4);
		String   nombreMesCarga 	= nombreMes + " de " + numeroAnio;
		
		// Construir Array con los datos del acuse
		JSONArray 	registrosAgregadosDataArray 	= new JSONArray();
		JSONArray	registro								= null;
 
		registro = new JSONArray();
		registro.add("Acuse Firma Digital");
		registro.add(acuseFirmaDigital);
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Tipo de Operación");
		registro.add("09 Comisiones");
		registrosAgregadosDataArray.add(registro);
 
		registro = new JSONArray();
		registro.add("No. total de registros transmitidos");
		registro.add(totalRegistros);
		registrosAgregadosDataArray.add(registro);
 
		registro = new JSONArray();
		registro.add("Mes de carga");
		registro.add(nombreMesCarga);
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Fecha de carga");
		registro.add(rg.getFecha());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Hora de carga");
		registro.add(rg.getHora());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Fecha Valor");
		registro.add(rg.getFechaValor());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Usuario");
		registro.add(iNoUsuario+" - "+strNombreUsuario);
		registrosAgregadosDataArray.add(registro);
 
		resultado.put("registrosAgregadosDataArray",	registrosAgregadosDataArray);
		
		// Hacer eco de los parametros de la carga
		resultado.put("claveProceso",						claveProceso				);
		resultado.put("totalRegistros",					totalRegistros				);
		resultado.put("montoTotal",						montoTotal					);
		// resultado.put("montoTotalPagar",				montoTotalPagar			);
		
		resultado.put("claveMes",							claveMes						);
		resultado.put("numeroRegistros",					numeroRegistros			);
		resultado.put("montoTotalComisiones",			montoTotalComisiones		);
		
		// Agregar acuse de la firma digital
		resultado.put("acuseFirmaDigital",				acuseFirmaDigital			);
		// Agregar nombre del mes de carga
		resultado.put("nombreMesCarga",					nombreMesCarga				);
	}
	
	resultado.put("msg",								msg									);	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 			estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
 
} else if (    informacion.equals("CargaArchivo.fin") 							)	{

	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("29CargaComisiones01ext.jsp"); 
	
} else if (    informacion.equals("CatalogoMes")									)	{

	boolean		success		= true;
	JSONObject	resultado	= new JSONObject();
	
	String      claveIF		= iNoCliente;
	
	// Se declaran las variables...
	String 	meses[] 	= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	Calendar cal 		= Calendar.getInstance();
	cal.add(Calendar.MONTH,-1);
	
	// Obtener lista de los ultimos 12 meses
	String 		mesanio 		= null;
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	for(int i=0;i<12;i++){
		
		mesanio = new SimpleDateFormat("yyyyMM").format(cal.getTime());
 
		registro.put("clave",					mesanio 																			);
		registro.put("descripcion",			meses[cal.get(Calendar.MONTH)]+" - " + cal.get(Calendar.YEAR)	);
		registro.put("comisionesExtraidas",	new Boolean("false") 														);
		registros.add(registro);
		
		cal.add(Calendar.MONTH,-1);
		
	}
 
	// Obtener instancia del EJB de Garantias
	Garantias garantias = null;
	try {
					
		garantias = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
	} catch(Exception e) {
			
		log.error("CargaArchivo.transmitirRegistros(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
			
	}
		
	// Obtener el Estatus de Obtencion de las Comisiones por Parte del IF
	String  claveMesInicial = (String) ( (JSONObject) registros.get(11)).get("clave");
	String  claveMesFinal	= (String) ( (JSONObject) registros.get(0) ).get("clave");
	String  claveFiso 		= garantias.getClaveFISO(claveIF);	
	HashMap estatus		   = garantias.getEstatusObtencionComisiones( claveIF, claveFiso, claveMesInicial, claveMesFinal );
	
	for(int i=0;i<12;i++){
		
		registro     					= (JSONObject) registros.get(i);
		String clave 					= (String) registro.get("clave");
		String comisionesExtraidas	= (String) estatus.get(clave);
		if( comisionesExtraidas != null && !"".equals(comisionesExtraidas) ){
			registro.put("comisionesExtraidas",	new Boolean(comisionesExtraidas) );
		}
		
	}
	
	// Enviar resultado
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("GeneraArchivoPDF")	)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String 		msg 			= "";
	
	// Leer parametros
	String	totalRegistros 	= (request.getParameter("totalRegistros")		== null)?"":request.getParameter("totalRegistros");
	String	montoTotal 			= (request.getParameter("montoTotal")			== null)?"":request.getParameter("montoTotal");
	String	claveMes 			= (request.getParameter("claveMes")				== null)?"":request.getParameter("claveMes");
	
	String	acuseFirmaDigital = (request.getParameter("acuseFirmaDigital")	== null)?"":request.getParameter("acuseFirmaDigital");
	String 	nombreMesCarga 	= (request.getParameter("nombreMesCarga")		== null)?"":request.getParameter("nombreMesCarga");
	
	String	urlArchivo 			= "";
	HashMap	cabecera 			= null;
	try {
		
		// Crear map con la informacion del cabcera del PDF
		cabecera = new HashMap();
		cabecera.put("strPais", 				(String) session.getAttribute("strPais"));
		cabecera.put("iNoNafinElectronico", (String) ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()));
		cabecera.put("sesExterno", 			(String) session.getAttribute("sesExterno"));
		cabecera.put("strNombre", 				(String) session.getAttribute("strNombre"));
		cabecera.put("strNombreUsuario", 	(String) session.getAttribute("strNombreUsuario"));
		cabecera.put("strLogo", 				(String) session.getAttribute("strLogo"));
		
		String nombreArchivo = CargaArchivoComisiones.generaAcuseArchivoPDF(
			(ResultadosGar)session.getAttribute("rgprint"), 
			iNoUsuario, //loginUsuario
			cabecera, 
			strDirectorioTemp, 
			strDirectorioPublicacion, 
			totalRegistros, 
			montoTotal, 
			acuseFirmaDigital,
			nombreMesCarga,
			strNombreUsuario
		);
 
		urlArchivo =  appWebContextRoot + "/DescargaArchivo?nombreArchivo=" + strDirectorioTempRelativePath + nombreArchivo;
		 
	}catch(Exception e){
			
		log.error("GeneraArchivoPDF(Exception)");
		e.printStackTrace();
		log.error("rg                       = <" + (ResultadosGar)session.getAttribute("rgprint") + ">");
		log.error("strDirectorioTemp        = <" + strDirectorioTemp                              + ">");
		log.error("cabecera                 = <" + cabecera                                       + ">");
		log.error("strDirectorioPublicacion = <" + strDirectorioPublicacion                       + ">");
		log.error("totalRegistros           = <" + totalRegistros                                 + ">");
		log.error("montoTotal               = <" + montoTotal                                     + ">");
		log.error("claveMes                 = <" + claveMes                                       + ">");
		log.error("iNoUsuario               = <" + iNoUsuario                                     + ">");
		log.error("strNombreUsuario         = <" + strNombreUsuario                               + ">");
			
		msg		= e.getMessage();
		success	= false;
			
	}
		
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo ); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)						);
	
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%><%=infoRegresar%>