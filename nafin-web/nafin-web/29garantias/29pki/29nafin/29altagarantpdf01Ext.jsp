<%@ page import="java.util.*, java.sql.*, netropology.utilerias.*,com.netro.exception.*,net.sf.json.JSONArray,net.sf.json.JSONObject,
javax.naming.*, com.jspsmart.upload.*,com.netro.garantias.*,com.netro.pdf.*"
contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="../../29secsession.jspf" %>

<%

	String infoRegresar	= "";
		JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
try{
	

	String tot_registros = (request.getParameter("tot_registros")==null)?"0":request.getParameter("tot_registros");
	String folio	 = (request.getParameter("v_folio")==null)?"0":request.getParameter("v_folio");
	String monto_total	 = (request.getParameter("monto_total")==null)?"0":request.getParameter("monto_total");
	String fecha	 = (request.getParameter("fecha")==null)?"0":request.getParameter("fecha");
	String hora	 = (request.getParameter("hora")==null)?"0":request.getParameter("hora");

	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = archivo.nombreArchivo()+".pdf";


	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                                 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		    					 (String)session.getAttribute("sesExterno"),
                                 (String) session.getAttribute("strNombre"),
                                    (String) session.getAttribute("strNombreUsuario"),
					    		 (String)session.getAttribute("strLogo"),
								 strDirectorioPublicacion);


	pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
	pdfDoc.addText("Su solicitud fue recibida con el n�mero de folio: "+folio,"formas",ComunesPDF.CENTER);
	pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);

	pdfDoc.setTable(2,70);

	pdfDoc.setCell("CIFRAS CONTROL","celda03",ComunesPDF.CENTER,2,1,1);

	pdfDoc.setCell("Tipo de Operacion","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("01 Alta de Garantias","formas",ComunesPDF.RIGHT);

	pdfDoc.setCell("No. total de regisrtos","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell(tot_registros,"formas",ComunesPDF.RIGHT);

	pdfDoc.setCell("Monto total de los registros transmitidos","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell(monto_total,"formas",ComunesPDF.RIGHT);

		pdfDoc.setCell("Fecha de carga","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell(fecha,"formas",ComunesPDF.RIGHT);

	pdfDoc.setCell("Hora de carga","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell(hora,"formas",ComunesPDF.RIGHT);

	pdfDoc.setCell("Usuario","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell(iNoUsuario+" - "+strNombreUsuario,"formas",ComunesPDF.RIGHT);

	pdfDoc.addTable();
	pdfDoc.endDocument();
	
		JSONArray 	cifrasControlDataArray 	= new JSONArray();
		JSONArray	registro						= null;
		
		resultado.put("success", 				new Boolean(success)	);
		resultado.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo 		);
		infoRegresar = resultado.toString();

//		response.sendRedirect(strDirecVirtualTemp+nombreArchivo);

}catch(Exception e){
	System.err.println("Error al gernerar el archivo" +e);
	e.printStackTrace();
}%>

<%=infoRegresar%>