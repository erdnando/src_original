<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="java.util.*, 
		java.sql.*, 
		netropology.utilerias.*,
		com.netro.exception.*,
		java.math.*,
		javax.naming.*, 
		com.netro.garantias.*,
		java.text.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("AltaGarant.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// 1. Leer parametros
	String folio = request.getParameter("v_folio")!=null?request.getParameter("v_folio"):"";
	String ic_if = request.getParameter("v_if")!=null?request.getParameter("v_if"):"";
	
	// 2. Obtener los datos
	AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		VectorTokenizer vt = new VectorTokenizer();
		Vector vct = null;
		BigDecimal monto_total = new BigDecimal(0);
		int numReg = 0;
		try{
			con.conexionDB();
		
			String querySentence =	"select cg_contenido  " +
														"from gti_arch_rechazo " +
														"where ic_folio = "+folio;
		
			rs = con.queryDB(querySentence);
			while(rs!=null && rs.next()){
		
				System.out.println("rs.getString(cg_contenido)"+rs.getString("cg_contenido"));
				vt = new VectorTokenizer(rs.getString("cg_contenido"),"@");
				vct = vt.getValuesVector();
				if(vct != null || vct.size()>7){
					numReg++;
					System.out.println("vct.get(23).toString()"+vct.get(23).toString());
					monto_total = monto_total.add(new BigDecimal(vct.get(23).toString()));
					System.out.println("monto_total"+monto_total.toPlainString());
				}
		
			}
		
		
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	
	// 3.Construir Array con las cifras de control
		JSONArray 	cifrasControlDataArray 	= new JSONArray();
		JSONArray	registro						= null;
		
		registro = new JSONArray();
		registro.add("Tipo de Operación:");
		registro.add("01 Alta de Garantias");
		registro.add("OPERACION");
		cifrasControlDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("No. total de registros transmitidos:");
		registro.add(new Integer(numReg));
		registro.add("NUMERO_ENTERO");
		cifrasControlDataArray.add(registro);
			
		registro = new JSONArray();
		registro.add("Monto total de registros transmitidos:");
		registro.add(Comunes.formatoMN(monto_total.toPlainString()));
		registro.add("MONTO");
		cifrasControlDataArray.add(registro);

	

		resultado.put("cifrasControlDataArray",cifrasControlDataArray);
		
	
	// 4. Determinar el estado siguiente
		estadoSiguiente = "MOSTRAR_CIFRAS_CONTROL";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("tipo", 	"PRE-ACUSE" 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if (        informacion.equals("AltaGarant.cancelarEjecucion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	String 		destino 				= (request.getParameter("v_pantallaOrigen") == null)?"":request.getParameter("v_pantallaOrigen");
 
	// Determinar el estado siguiente
	estadoSiguiente = "FIN";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	resultado.put("destino",				destino					);
	infoRegresar = resultado.toString();

} else if (        informacion.equals("AltaGarant.transmitir_registros") )	{

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;

		// 1. Leer parametros
	String folio = request.getParameter("v_folio")!=null?request.getParameter("v_folio"):"";
	String ic_if = request.getParameter("v_if")!=null?request.getParameter("v_if"):"";
	
	// 2. Obtener los datos
	AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		VectorTokenizer vt = new VectorTokenizer();
		Vector vct = null;
		BigDecimal monto_total = new BigDecimal(0);
		int numReg = 0;
		try{
			con.conexionDB();
		
			String querySentence =	"select cg_contenido  " +
														"from gti_arch_rechazo " +
														"where ic_folio = "+folio;
		
			rs = con.queryDB(querySentence);
			while(rs!=null && rs.next()){
		
				System.out.println("rs.getString(cg_contenido)"+rs.getString("cg_contenido"));
				vt = new VectorTokenizer(rs.getString("cg_contenido"),"@");
				vct = vt.getValuesVector();
				if(vct != null || vct.size()>7){
					numReg++;
					System.out.println("vct.get(23).toString()"+vct.get(23).toString());
					monto_total = monto_total.add(new BigDecimal(vct.get(23).toString()));
					System.out.println("monto_total"+monto_total.toPlainString());
				}
		
			}
		
		
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		
		/*if(session.getAttribute("rgprint")!=null){
			throw new Exception("Para realizar otra operacion por favor seleccione la opcion Cargar Archivo en el menu Carga de Saldos y Pago de Comisiones");
		} else {
			session.setAttribute("rgprint", new ResultadosGar() ); //Se establece la variable de sesión para controlar envíos duplicados
		}*/
		Garantias bean = ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
		String tot_registros = (request.getParameter("tot_registros")==null)?"0":request.getParameter("tot_registros");

	//String folio	 = (request.getParameter("folio")==null)?"0":request.getParameter("folio");
	//String ic_if	 = (request.getParameter("ic_if")==null)?"0":request.getParameter("ic_if");

	ResultadosGar rg = 	null;

	String pkcs7 = (request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
	String serial = "";
	String folioCert = "";
	String externContent = (request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
	//FODEA023-2010 FVR
	externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
	char getReceipt = 'Y';
	List clavesFinan = (List)session.getAttribute("CLAVES_FINAN");

	String meses[]={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioCarga   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
		rg = bean.reprocesarAltaGarantiasRegConError(folio,ic_if);
/*
	if (!serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		folioCert = "02SPC"+iNoCliente+new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		Seguridad s = new Seguridad();

		if (s.autenticar(folioCert, serial, pkcs7, externContent, getReceipt)) {
			rg = bean.reprocesarAltaGarantiasRegConError(folio,ic_if);
		}else{
			throw new NafinException("GRAL0021");
		}

	}else{
		throw new NafinException("GRAL0021");
	}
*/

	session.setAttribute("rgprint",rg);
	
	// 3.Construir Array con las cifras de control
		JSONArray 	cifrasControlDataArray 	= new JSONArray();
		JSONArray	registro						= null;
		
		registro = new JSONArray();
		registro.add("Tipo de Operación:");
		registro.add("01 Alta de Garantias");
		registro.add("OPERACION");
		cifrasControlDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("No. total de registros transmitidos:");
		registro.add(new Integer(numReg));
		registro.add("NUMERO_ENTERO");
		cifrasControlDataArray.add(registro);
			
		registro = new JSONArray();
		registro.add("Monto total de registros transmitidos:");
		registro.add(Comunes.formatoMN(monto_total.toPlainString()));
		registro.add("MONTO");
		cifrasControlDataArray.add(registro);

		registro = new JSONArray();
		registro.add("Fecha de carga:");
		registro.add(rg.getFecha());
		registro.add("FECHA");
		cifrasControlDataArray.add(registro);

		registro = new JSONArray();
		registro.add("Hora de carga:");
		registro.add(rg.getHora());
		registro.add("FECHA");
		cifrasControlDataArray.add(registro);

		registro = new JSONArray();
		registro.add("Usuario:");
		registro.add(strNombreUsuario);
		registro.add("TEXTO");
		cifrasControlDataArray.add(registro);

	

		resultado.put("cifrasControlDataArray",cifrasControlDataArray);
		
	
	// 4. Determinar el estado siguiente
		estadoSiguiente = "MOSTRAR_CIFRAS_CONTROL";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("tipo", 	"ACUSE" 		);
	resultado.put("folio", 	folio 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if (        informacion.equals("AltaGarant.llenaEncabezadoFlujoFondos") )	{

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String 		destino 				= null;
	String		msg					= null;
 
	// 1. Leer parametros
	
	String claveUsuario 				= (request.getParameter("claveUsuario")				== null)?"":request.getParameter("claveUsuario");
	String contrasena 				= (request.getParameter("contrasena")					== null)?"":request.getParameter("contrasena");
	
	String pantallaOrigen 			= (request.getParameter("pantallaOrigen")				== null)?"":request.getParameter("pantallaOrigen");
	String origen 						= (request.getParameter("origen")						== null)?"":request.getParameter("origen");
	
	String claveEPO 					= (request.getParameter("claveEPO")						== null)?"":request.getParameter("claveEPO");
	String claveMoneda 				= (request.getParameter("claveMoneda")					== null)?"":request.getParameter("claveMoneda");
	String fechaVencimiento 		= (request.getParameter("fechaVencimiento")			== null)?"":request.getParameter("fechaVencimiento");
	String claveIF 					= (request.getParameter("claveIF")						== null)?"":request.getParameter("claveIF");
	String claveIfFondeo 			= (request.getParameter("claveIfFondeo")				== null)?"":request.getParameter("claveIfFondeo");
	String claveEstatus 				= (request.getParameter("claveEstatus")				== null)?"":request.getParameter("claveEstatus");
	String fechaOperacion			= (request.getParameter("fechaOperacion")				== null)?"":request.getParameter("fechaOperacion");
	
	String numeroDocumentos			= (request.getParameter("numeroDocumentos")			== null)?"":request.getParameter("numeroDocumentos");
	String importeTotal 				= (request.getParameter("importeTotal")				== null)?"":request.getParameter("importeTotal");
	String numeroDocumentosError	= (request.getParameter("numeroDocumentosError")	== null)?"":request.getParameter("numeroDocumentosError");
	String importeTotalError		= (request.getParameter("importeTotalError")			== null)?"":request.getParameter("importeTotalError");
	
	// 2. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
				
	} catch(Exception e) {
 
		msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
 
	}
	
	// VALIDAR USUARIO
		
	boolean esUsuarioCorrecto = false;
	
	try {
		
		esUsuarioCorrecto = dispersion.esUsuarioCorrecto(strLogin, claveUsuario, contrasena);
		
	} catch(NafinException ne){
		
		esUsuarioCorrecto = false;
		msg 					= ne.getMessage();
		
	} catch(Exception e){
	
		throw e;
		
	}
	
	// LLENAR ENCABEZADO DE FLUJO DE FONDOS
	
	if( esUsuarioCorrecto ) {
		
		// Llena encabezado flujo fondos
		dispersion.llenaEncabezadoFlujoFondos( claveEPO, claveMoneda, fechaVencimiento, claveIF, origen, claveEstatus, fechaOperacion, false, claveIfFondeo );
		// Determinar mensaje a mostrar
		if( claveMoneda != null && claveMoneda.equals("54") ){
			// Dependiente del tipo de moneda
			msg = "Las operaciones fueron registradas con éxito.";
		} else {
			msg = "Las operaciones fueron registradas con éxito para el flujo de fondos.";
		}	
		
	} else {
		
		// Falló la validacion del password: se finaliza la operacion
		msg = "El login y/o password del usuario es incorrecto, operación cancelada.";

	}

	// 4. Determinar el estado siguiente
	estadoSiguiente = "FIN";
	destino 			 = pantallaOrigen;
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("destino",				destino					);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>