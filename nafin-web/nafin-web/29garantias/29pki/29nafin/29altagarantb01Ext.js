Ext.onReady(function() {
	var numReg = 0;
	var montoTotal = 0.0;
	var fecha = '';
	var hora = '';
	
	var folio = '';
	//----------------------------------- HANDLERS ------------------------------------
 	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;		
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			var forma 		= Ext.getDom('formAux');
			//forma.action 	=  NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			//forma.target	= "_self";
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			//forma.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
		var fp = new Ext.form.FormPanel({
			hidden:true
	});
	var procesaPreacuseAltaGarant = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						altaGarant(resp.estadoSiguiente,resp);
					}
				);
			} else {
				altaGarant(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			var element;
			
			element = Ext.getCmp("botonAceptarCifrasControl");
			if( element.iconCls === 'loading-indicator'){
				element.enable();
				element.setIconClass('icoAceptar');
			}
			
			
			
	
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var altaGarant = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"							   ){
 
			// Cargar Catalogo
			var formParams = Ext.apply( {}, NE.exportaraltagarantb.inputParams);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29altagarantb01Ext.data.jsp',
				params: 	Ext.apply(
					formParams,
					{
						informacion:		'AltaGarant.inicializacion'
					}
				),
				callback: 				procesaPreacuseAltaGarant
			});
 
		} else if(		   estado == "MOSTRAR_CIFRAS_CONTROL"				  ){
			
			var labelAcuse = Ext.getCmp('estatus');
			if(respuesta.tipo == "PRE-ACUSE"){
				// Actualizar la altura del panel de cifras de control
				var panelCifrasControlACUSE = Ext.getCmp('panelCifrasControlACUSE');
				var panelCifrasControl = Ext.getCmp("panelCifrasControl");
				panelCifrasControl.show();
				panelCifrasControlACUSE.hide();
				var cifrasControlData = Ext.StoreMgr.key('cifrasControlDataStore');
				cifrasControlData.loadData(respuesta.cifrasControlDataArray);
				 numReg = respuesta.cifrasControlDataArray[1][1];
				 monto = respuesta.cifrasControlDataArray[2][1]
				 montoTotal = monto;
				if( cifrasControlData.getTotalCount() > 2 ){
					panelCifrasControl.setHeight(220);
				} else  {
					panelCifrasControl.setHeight(176);
				}
			}
			if(respuesta.tipo == "ACUSE"){
				// Actualizar la altura del panel de cifras de control
				var panelCifrasControlACUSE = Ext.getCmp('panelCifrasControlACUSE');
				var panelCifrasControl = Ext.getCmp("panelCifrasControl");
				panelCifrasControlACUSE.show();
				panelCifrasControl.hide();
				folio = respuesta.folio;
				labelAcuse.update('Su solicitud fue recibida con el n�mero de folio:'+respuesta.folio);
				var cifrasControlData = Ext.StoreMgr.key('cifrasControlDataStore');
				cifrasControlData.loadData(respuesta.cifrasControlDataArray);
				 numReg = respuesta.cifrasControlDataArray[1][1];
				 monto = respuesta.cifrasControlDataArray[2][1]
				 montoTotal = monto;
				 fecha = respuesta.cifrasControlDataArray[3][1]
				 hora = respuesta.cifrasControlDataArray[4][1]
				/*if( cifrasControlData.getTotalCount() == 6 ){
					panelCifrasControl.setHeight(620);
				} else  {
					panelCifrasControl.setHeight(176);
				}*/
			}
			
			// Cargar Cifras de Control (Pre-acuse, ACUSE)
			
 
		} else if(			estado == "CANCELAR_EJECUCION"					  ){
			
			// Cargar Catalogo
			var formParams = Ext.apply( {}, NE.exportaraltagarantb.inputParams);

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'29altagarantb01Ext.data.jsp',
				params: 	Ext.apply(
					formParams,
					{
						informacion:		'AltaGarant.cancelarEjecucion'
					}
				),
				callback: 				procesaPreacuseAltaGarant
			});
 	
		} else if(		   estado == "TRANSMITIR_REGISTROS"				  ){
		
			// Determinar el estado siguiente
			var formParams = Ext.apply( {}, NE.exportaraltagarantb.inputParams);
			Ext.Ajax.request({
				url:	  		'29altagarantb01Ext.data.jsp',
				params: 	Ext.apply(
					formParams,
					{
						informacion:		'AltaGarant.transmitir_registros'
					}
				),
				callback:	procesaPreacuseAltaGarant
			});
			
			
			
			
			
		} else if(		   estado == "IMPRIMIR_PDF"				  ){
			var formParams = Ext.apply( {}, NE.exportaraltagarantb.inputParams);
			Ext.Ajax.request({
				url:	  		'29altagarantpdf01Ext.jsp',
				params: 	Ext.apply(
					formParams,
					{
						informacion:		'AltaGarant.transmitir_registros',
						monto_total:montoTotal,
						fecha: fecha,
						hora:hora,
						tot_registros:numReg
						
					}
				),
				callback:	procesarDescargaArchivos
			});
			
		} else if(			estado == "LLENA_ENCABEZADO_FLUJO_FONDOS" 	  ){
 
			// Cargar Catalogo
			var formParams = Ext.apply( respuesta, NE.exportarinterfaseff.inputParams);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15exportarinterfaseff01ext.data.jsp',
				params: 	Ext.apply(
					formParams,
					{
						informacion:		'AltaGarant.llenaEncabezadoFlujoFondos'
					}
				),
				callback: 				procesaPreacuseAltaGarant
			})
		
		} else if(			estado == "ESPERAR_DECISION"  					  ){ 
 
		} else if(			estado == "FIN"  					                 ){
 	
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"15exportarinterfaseff01ext.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			
			// Agregar parametros de forma dinamica
			if( Ext.isDefined(respuesta.formParams) ){

				Ext.iterate(
					respuesta.formParams, 
					function( key, value ){
						Ext.DomHelper.insertFirst(
							forma, 
							{ 
								tag: 		'input', 
								type: 	'hidden', 
								id: 		key, 
								name: 	key, 
								value: 	value
							}
						); 
					}
				);
		
			}
			
			forma.submit();
						
		}
		
	}
	
	// 2. -------------------------------- PANEL CIFRAS DE CONTROL (acuse) ----------------------------------
	


 
	// 1. -------------------------------- PANEL CIFRAS DE CONTROL (pre-acuse) ----------------------------------
	
	var procesaCancelar = function(boton, evento) {
		
		// Agregar animacion de operacion en proceso...
		boton.disable();
		boton.setIconClass('loading-indicator');
		
		// Cancelar ejecucion	///

		altaGarant("CANCELAR_EJECUCION",null);
		
	}
	
	var procesaAceptar = function(boton, evento) {
		
		// Agregar animacion de operacion en proceso...
		boton.disable();
		boton.setIconClass('loading-indicator');
		
		// Aceptar Cifras de Control	
		altaGarant("ACEPTAR_CIFRAS_CONTROL",null);
		
	}
					
	var procesarConsultaCifrasControl = function(store, registros, opts){
		
		var gridCifrasControl 			= Ext.getCmp('gridCifrasControl');
		
		if (registros != null) {
			
			var el 							= gridCifrasControl.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
				
			}
			
		}
		
	}
	
	var cifrasControlData = new Ext.data.ArrayStore({
			
		storeId: 'cifrasControlDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  	mapping: 0 },
			{ name: 'CONTENIDO', 		mapping: 1 },
			{ name: 'TIPO_CONTENIDO', 	mapping: 2 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaCifrasControl,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCifrasControl(null, null, null);						
				}
			}
		}
		
	});
	
	var renderContenido = function(value, metaData, record, rowIndex, colIndex, store) {
		
		if(        record.get('TIPO_CONTENIDO') == 'NUMERO_ENTERO' ){
			metaData.style += 'text-align: center !important';
		} else if( record.get('TIPO_CONTENIDO') == 'MONTO' 		  ){
			metaData.style += 'text-align: right  !important';
		} else if( record.get('TIPO_CONTENIDO') == 'OPERACION' 		  ){
			metaData.style += 'text-align: right  !important';
		}
		return value;
		
	}
	
	var elementosCifrasControl = [
		
		{
			xtype: 	'label',
			id:	 	'labelCifrasControl',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		100,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
				width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '5%'
						},
						region:	'west'
					},
					{
						store: 			cifrasControlData,
						xtype: 			'grid',
						id:				'gridCifrasControl',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '90%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		200,
								hidden: 		false,
								hideable:	false,
								fixed:		true,
								align: 		'right'
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								align:		'right',
								renderer: 	renderContenido
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '5%'
						},
						region:	'east'
					}
			]
		}
	];
		var elementosCifrasControlACUSE = [
		{
			xtype: 	'label',
			id:	 	'estatus',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;'
			
		},
		{
			xtype: 	'label',
			id:	 	'labelCifrasControlAcuse',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		100,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
				width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '%5' 
						},
						region:	'west'
					},
					{
						store: 			cifrasControlData,
						xtype: 			'grid',
						id:				'gridCifrasControlAcuse',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
						height: 300,
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '90%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		200,
								hidden: 		false,
								hideable:	false,
								fixed:		true,
								align: 		'right'
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								align:		'right',
								renderer: 	renderContenido
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '5%'
						},
						region:	'east'
					}
			]
		}
	];
	
	var confirmar = function(pkcs7, textoFirmar){
	
		if (Ext.isEmpty(pkcs7)) {
			Ext.Msg.alert('','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;	//Error en la firma. Termina...
		}else{
			altaGarant("TRANSMITIR_REGISTROS",null);							
		}						
	}
	
	
	
	var panelCifrasControl = new Ext.Panel({
		id: 					'panelCifrasControl',
		width: 				430,
		height:				220,
		title: 				'Pre-Acuse',
		hidden:				'true',
		frame: 				true,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 6px;',
		style: 				'margin: 0 auto;',
		labelWidth: 		100,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side'
		},
		items: 				elementosCifrasControl,
		monitorValid: 		false,
		buttonAlign:		'center',
		buttons: [
			
			{
					xtype: 		'button',
					text: 		'Cancelar',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoCancelar',
					id: 			'botonCancelar',
					handler:    function (boton,v){
								Ext.Msg.show({
									title:'',
									msg: "�Esta usted seguro de cancelar la operaci�n?",
									buttons: Ext.Msg.YESNOCANCEL,
									fn: function(btn ){
										
										if(btn == 'yes'){
											// Agregar animacion de operacion en proceso...
											boton.disable();
											boton.setIconClass('loading-indicator');
											// Cancelar ejecucion	///
									
											altaGarant("CANCELAR_EJECUCION",null);
										}
									},
									animEl: 'elId',
									icon: Ext.MessageBox.QUESTION
								});
					}
			},
			{
					xtype: 		'button',
					text: 		'Transmitir Registros',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoAceptar',
					id: 			'botonAceptarCifrasControl',
					handler:    function(b, v){
						var dt = new Date();
						var numMes =(dt.format('m'));
						var mes =['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
						var anio = (dt.format('Y'));
									 montoTotal = montoTotal.replace('$','');

						var textoFirmar = "Tipo de Operacion: 01 Alta de Garantias\n"+
							"Mes de carga: "+mes[(numMes-1)]+" de "+anio+"\n"+
							"No total de registros transmitidos:"+numReg+"\n"+
							"Monto total de saldos al fin de mes:"+Ext.util.Format.number(montoTotal,'$0,0.00');
							
						NE.util.obtenerPKCS7(confirmar, textoFirmar);
						
					}
			}
		]
	});
	
	var panelCifrasControlACUSE = new Ext.Panel({
		id: 					'panelCifrasControlACUSE',
		width: 				430,
		height:				320,
		title: 				'Acuse',
		hidden:				'true',
		frame: 				true,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 6px;',
		style: 				'margin: 0 auto;',
		labelWidth: 		100,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side'
		},
		items: 				elementosCifrasControlACUSE,
		monitorValid: 		false,
		buttonAlign:		'center',
		buttons: [
			{
					xtype: 		'button',
					text: 		'Imprimir PDF',
					width: 		137,
					margins: 	' 3',
					iconCls: 'icoPdf',
					id: 			'imprimir_pdf',
					handler:    function(b, v){
						/*
							PARAMETROS NECESARIOS
							String tot_registros = (request.getParameter("tot_registros")==null)?"0":request.getParameter("tot_registros");
							String folio	 = (request.getParameter("folio")==null)?"0":request.getParameter("folio");
							String monto_total	 = (request.getParameter("monto_total")==null)?"0":request.getParameter("monto_total");
							String fecha	 = (request.getParameter("fecha")==null)?"0":request.getParameter("fecha");
							String hora	 = (request.getParameter("hora")==null)?"0":request.getParameter("hora");
						*/
						altaGarant("IMPRIMIR_PDF",null);	
					}
			},
			{
					xtype: 		'button',
					text: 		'Salir',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoCancelar',
					id: 			'botonSalir',
					handler:    function (boton,v){
							boton.disable();
							boton.setIconClass('loading-indicator');
							altaGarant("CANCELAR_EJECUCION",null);
					}
			}
		]
	});
	
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelCifrasControl,panelCifrasControlACUSE,fp
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	altaGarant("INICIALIZACION",null);
	
});