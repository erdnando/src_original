<!DOCTYPE html>
<%@ page import="java.util.*" 
	contentType="text/html;charset=windows-1252"
	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/29garantias/29secsession.jspf" %>
<html>
<head>
<title>.:: Nafin :Programa de Garantia Automatica::.</title>
<%

	String v_folio 		= (request.getParameter("v_folio")			== null)? "":request.getParameter("v_folio");
	String v_if 					= (request.getParameter("v_if")						== null)? "":request.getParameter("v_if");
//	String origen 						= (request.getParameter("origen")						== null)? "":request.getParameter("origen");
//	String fechaOperacion 			= (request.getParameter("fechaOperacion")				== null)? "":request.getParameter("fechaOperacion");
	String pantallaOrigen 			= (request.getParameter("pantallaOrigen")				== null)? "":request.getParameter("pantallaOrigen");
	
%>
<%@ include file="/extjs.jspf" %>
<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="../certificado.jspf" %>
<% if( esEsquemaExtJS ){ %>
	<%@ include file="/01principal/menu.jspf"%>
<% } %>
<script language="JavaScript">
	Ext.onReady( function() {
			
		Ext.ns('NE.exportaraltagarantb');
	
		NE.exportaraltagarantb.inputParams = {
			v_folio	:           "<%= v_folio %>",
			v_if		:				"<%= v_if %>",
			v_pantallaOrigen : "<%= pantallaOrigen %>"
		};
		
	});
</script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
<script type="text/javascript" src="29altagarantb01Ext.js?<%=session.getId()%>"></script> 


</head>

<!--%@ include file="/01principal/menu.jspf"%-->
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS) {%>
	
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:230px"></div></div>
		</div>
	</div>
	
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
<%}else  {%>
	
	<div id="Contcentral">
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<form id='formAux' name="formAux" target='_new'></form>	
		
<%}%>

</body>
</html>