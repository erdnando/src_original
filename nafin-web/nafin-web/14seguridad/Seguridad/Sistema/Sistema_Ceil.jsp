<html>
<link rel="stylesheet" href="../css/formato.css">
<%@ page buffer="none" %>
<%@ page import="java.sql.*,netropology.utilerias.*"%>
<%
String sSql = "",sClave = "",sDescripcion = "";
AccesoDB con = new AccesoDB();
ResultSet resultSet = null;
%>
<body bgcolor="#E0E0E0" OnLoad="Ceil.txtClave.focus();">
<FORM Name=Ceil>

<Script language="JavaScript">
	// Validando 1 frames
	if(self.parent.frames.length != 1)
		parent.parent.window.location.replace('../home.jsp');
</Script>

<Script Language="Javascript" src="../scripts/Validaciones.js">  </Script>

<center>
<iframe name="Head" width="85" height="0" bgcolor="#3366CC" hspace=0 vspace=0
	src="head.jsp?Head" scrolling="no" marginwidth=0 marginheight=0 frameborder=0>
 </iframe>
</center>

<table width="610" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="formas">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" class="titulo">Mantenimiento de Sistemas</td>
	</tr>
	<tr>
		<td class="formas">&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<table align="center" width="95%"  cellspacing="0" cellpadding="0" height="101" border="0">
				<tr>
					<td width="30%" height="0" valign="middle">
						<A TARGET='_top' TITLE='Clave de Sistema'><font class="Etiketas"> Clave </font></A>
					</td>
					<!-- /////////////////////////////////////   TD  /////////////////////////////////////-->
					<td Colspan="2" height="0">
						<font size="1">
						<input type="text" name="txtClave" size="11" maxlength="5" TABINDEX="1"
							OnBlur="mtdValida(txtClave.name)" STYLE="FONT-FAMILY: Verdana; FONT-SIZE: 10px;">
						</font> 
					</td>
					<td width="23%" height="0" rowspan=1>
						<table cellpadding="3" cellspacing="1" border="0">
							<tr>
								<td class="celda02" width="50" height="21" align="center"><a href="#" OnClick="fnModificar('Agregar');"  TABINDEX="9">Agregar</a></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="30%" height="0" valign="middle">
						<A TARGET='_top' TITLE='Descripci&oacute;n del Sistema'><font class="Etiketas"> Descripci&oacute;n Espa&ntilde;ol</font> </a>
					</td>
					<td Colspan="2" height="0">
						<font size="1">
						<input type="text" name="txtDescripcion" size="50" maxlength="80"  TABINDEX="2"
							OnBlur="mtdValida(txtDescripcion.name)" style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; ">
						</font>
					</td>
					<td width="23%">
						<table cellpadding="3" cellspacing="1" border="0">
							<tr>
								<td class="celda02" width="50" height="21" align="center"><a href="#"  onClick="fnModificar('Modificar');"  tabindex="10">Modificar</a></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="30%" height="0" valign="middle">
						<A TARGET='_top' TITLE='Descripci&oacute;n del Sistema'><font class="Etiketas"> Descripci&oacute;n Ingl&eacute;s</font> </a>
					</td>
					<td Colspan="2" height="0">
						<font size="1">
						<input type="text" name="txtDescripcionIngles" size="50" maxlength="80"  TABINDEX="3"
							OnBlur="mtdValida(txtDescripcionIngles.name)" style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; ">
						</font>
					</td>
					<td width="23%">
						<table cellpadding="3" cellspacing="1" border="0">
							<tr>
								<td class="celda02" width="50" height="21" align="center">
									<a href="#"  onClick="fnModificar('Eliminar');"  tabindex="11" OnBlur="txtClave.focus();"  >Eliminar</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="30%"> <font class="Etiketas"> Sist Padre </font> </td>
					<td Colspan="2">
						<font size="1">
						<select name="lstSistemaPadre"
							OnChange="if(lstSistemaPadre.value == txtClave.value)lstSistemaPadre.value='';"
							style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; FONT-VARIANT: small-caps; TEXT-TRANSFORM: uppercase" 
							TABINDEX="4">
							<option selected>----------------------------</option>
<%
						try{
							con.conexionDB();
							sSql =  "SELECT c_sistema, d_descripcion FROM seg_sistemas " +
									" ORDER BY c_sistema ";
							resultSet = con.queryDB(sSql);
							
							while (resultSet.next()){
								sClave = resultSet.getString(1).trim();
								sDescripcion  = resultSet.getString(2).trim();
%>
							<option value="<%=sClave%>"><%=sClave%>-<%=sDescripcion%> </option>
<%
							}
							resultSet.close();
							con.cierraStatement();
						}
						catch (Exception e){
							System.out.println("Error:" + e);
							e.printStackTrace();
							out.print("Error = " + e);
%>
							<Script Language="JavaScript">
								alert("Error: <%=e.toString()%>");
							</Script>
<%
						}
						finally{
							con.cierraConexionDB();
						}
%>
						</select>
						</font>
					</td>
					<td width="23%">
					&nbsp;
					</td>
				</tr>
				<tr>
					<td width="30%"> <font class="Etiketas"> Liga </font> </td>
					<td align="left"  ColsPan="2">
						<font size="1">
						<input type="text" name="txtImgLiga" size="40" maxlength="40"  tabindex="5"
							onBlur="mtdValida(txtImgLiga.name)"style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; ">
						</font>
					</td>
					<td width="23%">&nbsp;</td>
				</tr>
				<tr>
					<td width="30%"> <font class="Etiketas"> Acc Liga</font> </td>
					<td align="left"  ColsPan="2">
						<font size="1">
						<select name="lstAccion"
							style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; FONT-VARIANT: small-caps;
							TEXT-TRANSFORM: uppercase" TABINDEX="6" >
							<option value="1"> Ventana arriba </option>
							<option value="0"> Ventana adentro </option>
						</select>
						</font>
					</td>
					<td width="23%">&nbsp;</td>
				</tr>
				<tr>
					<td colspan=1 width="30%" height="33" valign="middle" align="left"> 
						<font class="Etiketas">Imagen</font>
					</td>
					<td colspan=1 height="33"  width="60%" align="left">
						<font size="1">
						<input type="text" name="txtImagen" size="35" maxlength="30"  tabindex="7"
							onBlur="mtdValida(txtImagen.name);if(txtImagen.value==''){txtImagen.disabled=true; chkImagen.checked=false;}"
							style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; "Disabled>
						<input type="checkbox" name="chkImagen" TABINDEX="8" OnClick="mtdActivaImg(chkImagen.checked);">
						</font>
					</td>
				</tr>
				<tr>
					<td ColsPan="2">
						<font class="Etiketas"> Bloqueado &nbsp;&nbsp;&nbsp;&nbsp;Si
						<input type="radio" name="rad" value="S" onClick="mtdBloq('S')" tabindex="9" >
						&nbsp;&nbsp;No
						<input type="radio" name="rad" value="N" onClick="mtdBloq('N')" checked tabindex="9">
						</font>
					</td>
				</tr>
			</table>
			<p>
			<!--///////////////////////////   Cabecera   ///////////////////////////-->
			<table align="center" width="600px" cellspacing="0" cellpadding="0">
				<tr>
					<td width="18%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Clave</font></td>
					<td width="*%" class="ColorCabecera" align="center"><font class="EncabezadoTabla" >Descripcion</font></td>
					<td width="25%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Sist Padre</font></td>
					<td width="15%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Bloqueo</font></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type="hidden" name="hidBloque"  value="" >
			<center>
				<iframe name="Floor" width="600px" height="250px" bgcolor="#3366CC"  hspace=0 vspace=0
					src="Sistema_Floor.jsp?Floor" scrolling="yes" marginwidth=0 marginheight=0 frameborder=0>
				</iframe>
			</center>
		</td>
	</tr>
</table>

<!-- ///////////////////////////   SCRIPTs  /////////////////////////////////  -->

<Script Language="JavaScript">
function fnModificar(Tipo){
	var Clave =  window.document.Ceil.txtClave.value;
	var Descripcion = window.document.Ceil.txtDescripcion.value;
	var DescripcionIngles = window.document.Ceil.txtDescripcionIngles.value;
	var Imagen      = window.document.Ceil.txtImagen.value;
    var sSwImg;

	if(window.document.Ceil.chkImagen.checked){
		sSwImg = "I";
		if (!fnValidaArchivoImg(Imagen)){
			alert("Nombre de archivo no valido");
			return;
		}
	} 	else {
		sSwImg = "D";
	}
	
	var sLiga       = window.document.Ceil.txtImgLiga.value;
	var iAccLiga    = window.document.Ceil.lstAccion.value;
	var sSisPadre   = window.document.Ceil.lstSistemaPadre.value;

	if( Clave != ""   &&   Descripcion){
		var Bloqueo     = window.document.Ceil.hidBloque.value;
		var Parametros = "Sistema_Floor.jsp?Mod=" + Clave;
		var sMensaje = "Esta Seguro de " + Tipo + " el registro?";

		switch (Tipo){
			case "Agregar":
				Parametros += "&Desc=" + Descripcion;
				Parametros += "&DescIngles=" + DescripcionIngles;
				Parametros += "&Img="  + Imagen;
				Parametros += "&Bloq=" + Bloqueo;
				Parametros += "&sLiga=" + sLiga;
				Parametros += "&iAccLiga=" + iAccLiga;
				Parametros += "&sSwImg=" + sSwImg;
				Parametros += "&sSisPadre=" + sSisPadre;
				Parametros += "&Tipo=Agregar";
				break;
			case "Eliminar":
				Parametros += "&Tipo=Eliminar";
				break;
			case "Modificar":
				Parametros += "&Desc=" + Descripcion;
				Parametros += "&DescIngles=" + DescripcionIngles;
				Parametros += "&Img="  + Imagen;
				Parametros += "&Bloq=" + Bloqueo;
				Parametros += "&sLiga=" + sLiga;
				Parametros += "&iAccLiga=" + iAccLiga;
				Parametros += "&sSwImg=" + sSwImg;
				Parametros += "&sSisPadre=" + sSisPadre;
				Parametros += "&Tipo=Modificar";
				break;
		}

		if(confirm(sMensaje)){
			mtdResetTxt(0);
			//alert("Parametros = " + Parametros);
			parent.Ceil.Floor.location.href=Parametros;
		}
		else{
			window.document.Ceil.txtClave.focus();
			window.document.Ceil.txtClave.select();
		}
	}
	else{
		alert("Debe de llenar todos los campos");
	}
}

/********************************************************  *
*  function mtRecibeDatos(Cve, Desc, Imag, Bloq)  
*             Recibe Datos de Floor
************************************************************/
function mtRecibeDatos(Cve, Desc, DescIngles, Imag, 
		Bloq, iAccLiga, sSwImg, sLiga, SisPadre){
	with(window.document.Ceil){
		txtClave.value        = Cve;
		txtDescripcion.value  = Desc;
		txtDescripcionIngles.value  = DescIngles;
		txtImagen.value    = Imag;

		if(Imag != ""){
			txtImagen.disabled = false;
			chkImagen.checked  = true;
		}
		else{
			txtImagen.disabled = true;
			chkImagen.checked  = false;
		}

		hidBloque.value       = Bloq;

		if (Bloq == 'S' ){
			rad(0).checked = true;
		}
		else{
			rad(1).checked = true;
		}

		lstSistemaPadre.value = SisPadre;

		txtImgLiga.value = sLiga;
		lstAccion.value = iAccLiga;

		txtClave.focus();
		txtClave.select();
	}
}

function mtdBloq(sDato){
	window.document.Ceil.hidBloque.value = sDato;
}

/*******************************************************  **
*  function mtdActivaImg ()
************************************************************/
function mtdActivaImg(lbEstado)  {
	if (!lbEstado)
		window.document.Ceil.txtImagen.value="";
	window.document.Ceil.txtImagen.disabled= !lbEstado;
}

/*******************************************************  **
 *     function  mtdAgregaSistema(sClave,  sDescripcion)
************************************************************/
function  mtdAgregaSistema(sClave,  sDescripcion){
	var objCombo;
	objCombo = window.document.Ceil.lstSistemaPadre;
	objCombo.length += 1;
	var iIndi = objCombo.length;
	objCombo.options[iIndi-1].value = sClave;
	objCombo.options[iIndi-1].text  = sDescripcion;
}
</Script>
</form>
</body>
</html>
