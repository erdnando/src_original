<%@ page import="java.sql.*, netropology.utilerias.*" %>
<html>
<head>
  <!--   
  <SCRIPT language="JavaScript">
     function sobre(src,Color)      {
        if (!src.contains(event.fromElement))    {
          src.style.cursor = 'n-resize';
          src.bgColor = Color;
        }
     }
  //-->
  </SCRIPT>
</head>
<link rel="stylesheet" href="../css/formato.css">  </style>
<%
String sSql = null,sClave = null,sDescripcion = null;
int iBanco = 0,iCleinte = 0,iCentral = 0;
ResultSet resultSet= null,resultSet2= null,resulRestric= null;

iCentral = 1;
iCleinte = 2; 
iBanco   = 4;
AccesoDB con = new AccesoDB();

try{
	con.conexionDB();
	sSql = "SELECT c_sistema, c_sistema||'-'||d_descripcion as d_descripcion " +
			" FROM seg_sistemas ORDER BY d_descripcion";
	resultSet  = con.queryDB(sSql);
	resultSet2 = con.queryDB(sSql);
	sSql = "SELECT * FROM SEG_RESTRICFAC";
	resulRestric = con.queryDB(sSql);
%>

<Script Language="Javascript" src="../scripts/Validaciones.js">  </Script>
<Script Language="JavaScript">
var gsIsIE  = (document.all ? true : false);
var giTamanoTexto = (gsIsIE ? 50 : 35);
var iCveAsig = 0;
////////////////////
////////////////////   FORMATEA LOS DATOS DE UN COMBO PASANDO COMO DATOS DOS CADENAS Y LA LONGITUD MAXIMA DE LA PRIMERA CADENA, Y 
function mtdFormateaCombo(iMaxlen, sDato1, sDato2){
	var LONGITUDAD = 4;
	var paso = "&nbsp;&nbsp;";
	var longDato1 = sDato1.length;
	document.write(sDato1);
	
	for (var i=1; i<=(iMaxlen-longDato1)+LONGITUDAD; i++){
		document.write("<font face=verdana>" + paso + "</font>");
	}
	document.write(sDato2);
}
</Script>


<body  OnLoad="Ceil.lstSistemas.focus();">
<FORM Name=Ceil >
<center>
	<iframe name="Head" width="30" height="0:px" bgcolor="#3366CC" hspace=0 vspace=0
		src="head.jsp?Head"   scrolling="no" marginwidth=0 marginheight=0 frameborder=0>
	</iframe>
</center>

<table width="650" cellpadding="3" cellspacing="3" border="0">
	<tr><td class="titulo" align="center">Mantenimiento de Facultades</td></tr>
	<tr>
		<td align="center">
			<table align="center" width="90%"  cellpadding="0" cellspacing="3" border="0">
				<tr> 
					<td width="10%" valign="middle"> 
						<font class="Etiketas"><A TARGET='_top' TITLE='Clave de Sistema'>Sistemas </A> </font> 
					</td>
					<td width="63%">
						<!-- /////////////////////////   Llenando Combo de SISTEMAS //////////////////////// -->
						<select name="lstSistemas" Style="Font-Size:10px; FONT-FAMILY:Verdana;"
							OnChange="mtdResetTxt(0);" TABINDEX="1">
							<option value=""> ---------------------- </option>
<%
							while (resultSet.next()){
								sClave        = resultSet.getString(1).trim();
								sDescripcion  = resultSet.getString(2).trim();
								//sDescripcion  = sClave + " " + resultSet.getString(2).trim();
%>
							<option value="<%=sClave%>"> <%=sDescripcion%> </option>
							<%}%>
						</select>
					</td>
					<td width="27%" rowspan=2 valign="bottom" align="right"> 
						<div align="right">
						<table width="50%" border="0" cellspacing="0" cellpadding="0">
							<tr align="right"> 
								<td>
									<table cellpadding="3" cellspacing="1" border="0">
										<tr>
											<td class="celda02" width="50" height="21" align="center"><a href="#" OnClick="fnModificar('Agregar');"  TABINDEX="11">Agregar</a></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr align="right"> 
								<td>
									<table cellpadding="3" cellspacing="1" border="0">
										<tr>
											<td class="celda02" width="50" height="21" align="center"><a href="#" OnClick="fnModificar('Eliminar');"  TABINDEX="11">Eliminar</a></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<font size="1" face="Verdana, Arial, Helvetica, sans-serif">
							<A TARGET='_top' TITLE='Agrega Clave'></A>
							<A TARGET='_top' TITLE='Eliminar Registro'> </A>
						</font>
						</div>
					</td>
				</tr>
				<tr> 
					<td width="10%" valign="middle" height="39">
						<font class="Etiketas"><a TARGET='_top' TITLE='Clave de la Facultad'>Facultad </a></font>
					</td>
					<td width="63%" height="0"> 
						<input type="text" name="txtClaveFacultad" size="25" maxlength="20"  TABINDEX="2" 
							OnBlur="mtdValida(txtClaveFacultad.name);"STYLE="FONT-FAMILY: Verdana; FONT-SIZE: 10px;" >
					</td>
				</tr>
				<tr> 
					<td colspan=1 width="10%" valign="middle"> 
						<div align="left">
						<font class="Etiketas"><A TARGET='_top' TITLE='Descripci&oacute;n'> Descripci&oacute;n </a></font>
						</div>
					</td>
					<td colspan=1 width="63%"> 
						<div align="left">
						<font size=1> 
						<Script language="javaScript">
							//if (gsIsIE)    giTamanoTexto = 50;
							//else           giTamanoTexto = 40;
							document.write('<input type="text" name="txtDescripcion" size="'+giTamanoTexto+'" maxlength="80"  tabindex="3" ');
							document.write('onBlur="mtdValida(txtDescripcion.name)"  ');
							document.write('STYLE="FONT-FAMILY: Verdana; FONT-SIZE: 10px;">');
						</script>
						<!-- <input type="text" name="txtDescripcion" size="50" maxlength="80"  tabindex="3" onBlur="mtdValida(txtDescripcion.name)"  STYLE="FONT-FAMILY: Verdana; FONT-SIZE: 10px;"> -->
						</font> 
						</div>
					</td>
					<td colspan=1 width="27%" valign="bottom" align="right">
						<table cellpadding="3" cellspacing="1" border="0">
							<tr>
								<td class="celda02" width="50" height="21" align="center"><a href="#" OnClick="fnModificar('Modificar');"  TABINDEX="12" OnBlur="lstSistemas.focus();">Modificar</a></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr> 
					<td colspan="1" valign="middle" align="left"> 
						<font class="Etiketas">Descripci&oacute;n Ingl&eacute;s
						</font>
					</td>
					<td colspan="2" align="left"> 
						<font size=1> 
							<input type="text" name="txtDescripcionIngles" size="50" 
									maxlength="80"  tabindex="4" onBlur="mtdValida(txtDescripcion.name)"
									style="FONT-FAMILY: Verdana; FONT-SIZE: 10px;">
						</font> 
					</td>
				</tr>
				<tr> 
					<td width="10%" valign="middle">
						<font class="Etiketas"><A TARGET='_top' TITLE='Tipos de Restricciones'> Restricciones </A></font>
					</td>
					<td width="63%"> <!-- /////////////////////////   Llenando Combo de RESTRICCIONES //////////////////////// -->
						<select name="lstRestricciones" Style="Font-Size:10px"  TABINDEX="4">
							<option value="0">0.- &nbsp;&nbsp;&nbsp; Sin Restriccion
<%
							while (resulRestric.next()){
								String sNumRes   =  resulRestric.getString(1).trim();
								String sFchIni   =  resulRestric.getDate(2).toString();
								String sFchFin   =  resulRestric.getDate(3).toString();
								String sFchVen   =  resulRestric.getDate(4).toString();
%>
							<option value="<%=sNumRes%>">
								<Script Languege="JavaScript">
									mtdFormateaCombo(9, "<%=sNumRes%>", "<%=sFchIni%>");
								</Script>&nbsp;&nbsp;&nbsp;&nbsp;
								<%=sFchFin%> &nbsp;&nbsp;&nbsp;&nbsp; <%=sFchVen%>
							<%}%>
						</select>
					</td>
				</tr>
				<tr> 
					<td width="10%" valign="middle">
						<font class="Etiketas"><A TARGET='_top' TITLE='Tipo de Facultad'> Tipo </A></font>
					</td>
					<td width="63%"> <!-- /////////////////////////   Llenando Combo de TIPOS //////////////////////// -->
						<select name="lstTipoFac" Style="Font-Size:10px" TABINDEX="5">
<%
						sSql = "SELECT * FROM SEG_TIPOFAC";
						resulRestric = con.queryDB(sSql);
						while (resulRestric.next()) {
							String sTipo   =  resulRestric.getString(1).trim();
							String sDesc   =  resulRestric.getString(2).trim();
%>
							<option value="<%=sTipo%>"> <%=sTipo%>.- &nbsp;&nbsp;&nbsp; <%=sDesc%> 
<%
						}
						resulRestric.close();
						con.cierraStatement();
%>
						</select>
					</td>
				</tr>
				<tr> 
					<td width="10%" valign="middle">
						<font class="Etiketas"><A TARGET='_top' TITLE='Clave EPO'> Clave EPO </A></font>
					</td>
					<td width="63%"> <!-- /////////////////////////   Llenando Combo de TIPOS //////////////////////// -->
						<input type="text" name="txtClaveRevisar" Style="Font-Size:10px" TABINDEX="6">
					</td>
				</tr>
				
				<tr>
					<td width="10%" valign="middle">
						<font class="Etiketas"><a target='_top' title='Archivo Imagen'>Imagen</a></font> 
					</td>
					<td align="left">
						<font size=1> 
						<Script language="javaScript">
							document.write('<input type="text" name="txtImagen" size="'+giTamanoTexto+'" maxlength="50"  TABINDEX="6" ');
							document.write('OnBlur="if(txtImagen.value==\'\'){txtImagen.disabled=true; chkImagen.checked=false;}" ');
							document.write('STYLE="FONT-FAMILY: Verdana; FONT-SIZE: 10px;"  Disabled> ');
						</script>
						<input type="checkbox" name="chkImagen" TABINDEX="7" OnClick="mtdActivaImg(chkImagen.checked);">
						</font>
					</td>
				</tr>
				<tr>
					<td width="10%" valign="middle">
						<font class="Etiketas"><a target='_top' title='Archivo Pagina'>Pagina</a></font>
					</td>
					<td width="63%" align="left"> 
						<font size=1>
						<Script language="javaScript">
							document.write('<input type="text" name="txtPagina" size="'+giTamanoTexto+'" maxlength="150"  TABINDEX="8" ');
							document.write('OnBlur="mtdValida(txtPagina.name);" ');
							document.write('STYLE="FONT-FAMILY: Verdana; FONT-SIZE: 10px;" >');
						</script>
						</font>
					</td>
					<td width="27%" valign="bottom"> 
						<div align="right">
						<font class="Etiketas">Bloqueo</font>
						<font class="Etiketas">&nbsp;&nbsp;&nbsp; Si</font>
						<input type="radio" name="radBloqueo" Value="S" TABINDEX="10" Onclick=mtdAsingBloq(radBloqueo(0).value)>
						<br><font class="Etiketas">No</font>
						<input type="radio" name="radBloqueo" Value="N" TABINDEX="10" Onclick=mtdAsingBloq(radBloqueo(1).value) checked>
						</div>
					</td>
				</tr>
				<tr>
					<td valign="middle">
						<font class="Etiketas">Pagina Ingl&eacute;s</font>
					</td>
					<td align="left" colspan="2">
						<font size=1>
							<input type="text" name="txtPaginaIngles" size="50" maxlength="150"  TABINDEX="8"
									OnBlur="mtdValida(txtPaginaIngles.name);"
									STYLE="FONT-FAMILY: Verdana; FONT-SIZE: 10px;">
						</font>
					</td>
				</tr>
				<tr>
					<td width="10%" valign="middle">
						<font class="Etiketas">&nbsp;<!--Asignaci&oacute;n--></font>
					</td>
					<td width="63%" >
						&nbsp;&nbsp;&nbsp;&nbsp;<font class="Etiketas"> Quitar Menu </font>
						&nbsp;&nbsp;&nbsp; <font class="Etiketas"> No </Font>
						<input type="radio" name="radQMenu"  Value="0" TABINDEX="9" Checked>
						&nbsp;&nbsp;&nbsp; <font class="Etiketas"> Si </Font>
						<input type="radio" name="radQMenu"  Value="1" TABINDEX="9" >
					</td>
					<td width="27%" valign="bottom">&nbsp;</td>
				</tr>
				<tr>
					<td><font class="Etiketas"> Consultar </font></td>
					<td>
						<select name="lstSistemasCons" Style="Font-Size:10px; FONT-FAMILY:Verdana;"  TABINDEX="14" 
							OnChange=mtdConsulta(lstSistemasCons.value);>
							<option value=""> ---------------------- </option>
<%
							while (resultSet2.next()) 	{
								sClave        = resultSet2.getString(1).trim();
								sDescripcion  = resultSet2.getString(2).trim();
								//sDescripcion  = sClave + " " + resultSet.getString(2).trim();
%>
							<option value="<%=sClave%>"> <%=sDescripcion%> </option>
<%
							}
							resultSet2.close();
							con.cierraStatement();
%>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center"></td>
	</tr>
</table>

<!-- <a href="javascript:mtdBuscar();" > <small> buskar </small></a> -->

<table cellpadding="5" cellspacing="2" border="0">
	<tr>
		<td align="center">
			<!-- /////////////////////////////////////   Cabecera   /////////////////////////////////////-->
			<table width="640px" align="center" height="19"  cellpadding="0" cellspacing="0"   >
				<tr>
					<td width="12%" class="ColorCabecera">&nbsp;</td>
					<td width="10%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Clave</font></td>
					<td width="31%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Descripci&oacute;n</font></td>
					<td width="6%" class="ColorCabecera" align="center"> <font class="EncabezadoTabla">Tipo</font></td>
					<td width="6%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Res</font></td>
					<td width="26%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Pagina</font></td>
					<td width="*%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Bloqueo</font></td>
				</tr>
			</table>
			<input type="hidden" name="hidBloque"  value="N" >
			<iframe name="Floor" width="640px" height="200px" bgcolor="#3366CC" hspace=0 vspace=0
				src="Facultad_Floor.jsp?Floor" marginwidth=0 marginheight=0 frameborder=0>
			</iframe>
		</td>
	</tr>
</table>

<!-- ///////////////////////////   SCRIPTs  /////////////////////////////////  -->                                     
<Script Language="JavaScript">
function mtdConsulta(lsSistema){
	var lsParametrosURL = "Facultad_Floor.jsp?parConsulta=" + lsSistema;
	self.Floor.location.href = lsParametrosURL;
	document.Ceil.reset()
	document.Ceil.lstSistemasCons.value = lsSistema;
}

function fnModificar(Tipo){
	var Clave       = window.document.Ceil.txtClaveFacultad.value;
	var Descripcion = window.document.Ceil.txtDescripcion.value;
	var DescripcionIngles = window.document.Ceil.txtDescripcionIngles.value;
	var Pagina      = window.document.Ceil.txtPagina.value;
	var PaginaIngles      = window.document.Ceil.txtPaginaIngles.value;

	if(Clave != ""  &&  Descripcion != ""  &&    Pagina != "" ){
		var Bloqueo     = window.document.Ceil.hidBloque.value;
		var Restric     = window.document.Ceil.lstRestricciones.value;
		var Tip         = window.document.Ceil.lstTipoFac.value;
		var ClaveRevisar  = window.document.Ceil.txtClaveRevisar.value;
		var Sist        = window.document.Ceil.lstSistemas.value;        
		var Imagen2      = window.document.Ceil.txtImagen.value;
		var Parametros = "Facultad_Floor.jsp?Fac=" + Clave + "&Sist=" + Sist;
		var sMensaje = "Esta Seguro de " + Tipo + " el registro?";
		var Menu;

		if (Tip == 5 && ClaveRevisar == "") { //Si es pantalla especial requiere de la Clave a Revisar
			alert ("Especifique la clave de comparaci�n");
			window.document.Ceil.txtClaveRevisar.focus();
			return;
		}
		
		if(window.document.Ceil.radQMenu[0].checked)
			Menu = window.document.Ceil.radQMenu[0].value;
		else
			Menu = window.document.Ceil.radQMenu[1].value;

		var sSwImg;
		if(window.document.Ceil.chkImagen.checked)
			sSwImg = "I";
		else
			sSwImg = "D";

		switch (Tipo){
			case "Agregar":
				Parametros += "&Desc=" + Descripcion;
				Parametros += "&DescIngles=" + DescripcionIngles;
				Parametros += "&Pagina="  + Pagina;
				Parametros += "&PaginaIngles="  + PaginaIngles;
				Parametros += "&Bloq=" + Bloqueo;
				Parametros += "&Res="  + Restric;
				Parametros += "&Tipo=" + Tip;
				Parametros += "&CveRevisar=" + ClaveRevisar;
				Parametros += "&CveAsig=" + iCveAsig;
				Parametros += "&Imagen2=" + Imagen2;
				Parametros += "&Menu=" + Menu;
				Parametros += "&sSwImg=" + sSwImg;
				Parametros += "&TipoOp=Agregar";
				break;
			case "Eliminar":
				Parametros += "&TipoOp=Eliminar";
				break;
			case "Modificar":
				Parametros += "&Desc=" + Descripcion;
				Parametros += "&DescIngles=" + DescripcionIngles;
				Parametros += "&Pagina="  + Pagina;
				Parametros += "&PaginaIngles="  + PaginaIngles;
				Parametros += "&Bloq=" + Bloqueo;
				Parametros += "&Res="  + Restric;
				Parametros += "&Tipo=" + Tip;
				Parametros += "&CveRevisar=" + ClaveRevisar;
				Parametros += "&CveAsig=" + iCveAsig;
				Parametros += "&Imagen2=" + Imagen2;
				Parametros += "&Menu=" + Menu;
				Parametros += "&sSwImg=" + sSwImg;
				Parametros += "&TipoOp=Modificar"; 
				break;
		}

		if(confirm(sMensaje)){
			mtdBorra(0);
			parent.Ceil.Floor.location.href=Parametros;
		}
		else{
			window.document.Ceil.txtClaveFacultad.focus();
			window.document.Ceil.txtClaveFacultad.select();
		}
	}
	else{
		alert("Debe de llenar todos los campos.");
	}
}

///////////////////////////// Recibe Datos de Floor  //////////////////////////////
function mtRecibeDatos(Sist, Fac, Desc, DescIngles, Bloq, Tip, CveRevisar, Res, Page, PageIngles, iAsing, Imag, iSwMenu){
	//alert("Recibe = " + Sist + "   " + Fac + "   " + Desc + "   " + Bloq + "   " + Tip + "   " + Res + "   " + Page);
	//alert(iAsing + "  " + Sist + "   " + sPage2);
	window.document.Ceil.lstSistemas.value      = Sist;
	window.document.Ceil.txtClaveFacultad.value      = Fac;
	window.document.Ceil.txtDescripcion.value   = Desc;
	window.document.Ceil.txtDescripcionIngles.value   = DescIngles;

	window.document.Ceil.lstTipoFac.value       = Tip;
	window.document.Ceil.txtClaveRevisar.value  = CveRevisar;
	window.document.Ceil.lstRestricciones.value = Res;
	window.document.Ceil.txtPagina.value        = Page;
	window.document.Ceil.txtPaginaIngles.value  = PageIngles;
	window.document.Ceil.hidBloque.value        = Bloq;

	if (Bloq == "S"){
		window.document.Ceil.radBloqueo[0].checked = true;
	}
	else{
		window.document.Ceil.radBloqueo[1].checked = true;
	}

	window.document.Ceil.txtImagen.value = Imag;
	if(Imag != ""){
		window.document.Ceil.txtImagen.disabled = false;
		window.document.Ceil.chkImagen.checked  = true;
	}
	else{
		window.document.Ceil.txtImagen.disabled = true;
		window.document.Ceil.chkImagen.checked  = false;
	}
	if(iSwMenu == "1")
		window.document.Ceil.radQMenu[1].checked  = true;
	else
		window.document.Ceil.radQMenu[0].checked  = true;

	window.document.Ceil.lstSistemas.focus();
}

///////////////////////////// hidden de Bloqueo  //////////////////////////////
function mtdAsingBloq(Dato){
	//alert(Dato);
	window.document.Ceil.hidBloque.value = Dato;
	//alert(window.document.Ceil.hidBloque.value);
}

/*******************************************************  **
*  function mtdActivaImg ()
************************************************************/
function mtdActivaImg(lbEstado){
	if(!lbEstado)
		window.document.Ceil.txtImagen.value="";
	window.document.Ceil.txtImagen.disabled= !lbEstado;
}

function mtdBuscar(){
	var lsCveBuscar = prompt("Clave a Buscar", "");
	if ( lsCveBuscar != "" )
		self.Floor.findInPage(lsCveBuscar);
	//top.menu.Ceil.Floor.findInPage(lsCveBuscar);

	//self.Floor.location.href = "Facultad_Floor.jsp#lsCveBuscar";
}
</Script>
</FORM>
<%
}
catch (Exception e){
	System.out.println("Error:" + e);
	e.printStackTrace();
%>
	<Script Language="JavaScript">
		alert("Error: <%=e.toString()%>")
	</Script>
<%
}
finally{
	con.cierraConexionDB();
}
%> 

</body>
</html>
