<!--
/*****************************************************************************

                 /////  Pragrama:    NAFIN                         /////
                 /////  Funci�n:     Lista de datos                /////
                 /////                                             /////
                 /////  Autor:       Manuel Ramos Mendoza          /////
                 /////  Mod:         Ricardo Jimenez Vargas        /////
                 /////  Realizado:   08/  Enero/2001               /////
                 /////  Ult. Modif:  26/  Febrero/2003             /////

******************************************************************************/
-->
<%@ page import="java.util.*,java.lang.*, netropology.utilerias.*,com.netro.seguridadbean.Seguridad, com.netro.seguridadbean.* ,javax.naming.*"%>
<%@ page session="true" %>
<!--jsp:usebean id="objSeguridad" class="com.netro.seguridadbean.SeguridadBean" scope="page"/-->
<%
Seguridad objSeguridad = null;

try {
	objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",Seguridad.class);
} catch (Exception e) {
	System.out.println(e);
%>
	<script>
		alert("EXISTEN DIFICULTADES TECNICAS, INTENTE M�S TARDE");
	</script>
<%
	return;
}
%>

<html>
<LINK REL="stylesheet" HREF="../css/formato.css" TYPE="text/css">
<Script Language="JavaSCript" src="../css/colores.js">  </Script>
<head>
<Script Language="JavaScript">
function mtdCambiaPuntero(src){
	src.style.cursor = 'hand';
}

function mtdRegresaPuntero(src){
	src.style.cursor = 'default';
}
</Script>
</head>
<%
Vector  vTabla       = new Vector(),
	vRegistro    = null,
	vRegistroTmp = new Vector(),
	vSubTabla    = new Vector(),
	vSubRegistroTmp = new Vector(), 
	vSubRegistro = null; 

String
	sCveSistemaAnt  = "",
	sCveSistema     = "",
	sCvePerfilProt = "",
	sDescripcion = "",
	sBloqueo     = "",
	sDato        = null,
	sTipoAc      = "",
	sDatoTemp    = null,
	sDatosHidden = null,
	sCveAsig     = null,
	sTipUsr      = null,

sValorBoton  = null,
sCveFacultad = null,
sValCveSistema  = null;

final String AGREGAR   = "Agregar", 
	ELIMINAR  = "Eliminar",
	MODIFICAR = "Modificar";

int iAsingUsr      = 0,
	iFilaBuscar    = 0,
	iContRenglones = 0;
%>

<Script Language="JavaScript">
var VClaves = new Array(1);  
var i=0, iContadorColor = 0, iContadorColor2=0, j=0;
var sColor = "White";

/**************************************************************************************
*       function mtdCambiaColor()
**************************************************************************************/
function mtdCambiaColor(){
	iContadorColor++;
	if(iContadorColor == 5 )
		iContadorColor = 1;
	var Color = "";
	switch(iContadorColor){
		case 1:
			Color = sColor1;
			break;
		case 2:
			Color = sColor2;
			break;
		case 3:
			Color = sColor1;
			break;
		case 4:
		Color = sColor2;
		break;
	}
	return Color;
}

/****************************************************************  *
*   function mtdCambiaColor2()     Determina el color de la Celda
********************************************************************/
function mtdCambiaColor2(ColorAnt){
	//alert("Cachando ColorAnt = " + ColorAnt);
	iContadorColor2++;
	if(iContadorColor2 == 3)
		iContadorColor2 = 1;
	var Color = "";

	if(ColorAnt == sColor1){
		switch(iContadorColor2){
			case 1:  { Color = sColor11; } break;
			case 2:  { Color = sColor1; } break; 
		}
	}
	else if(ColorAnt == sColor2){
		switch(iContadorColor2){
			case 1:  { Color = sColor22; } break;
			case 2:  { Color = sColor2; } break; 
		}
	}
	else
		alert("ColorAnt (" + ColorAnt + ")  sColor2("+sColor2+")");

	return Color;
}
var sColor = "";

/**********************************************************************
*       function fnPasaDatos()
***********************************************************************/
function fnPasaDatos(esCveSistema, esCvePerfilProt, esDescripcion, esBloqueo, esAsing){
	top.body.body2.frmCaptura.mtRecibeDatos(esCveSistema, esCvePerfilProt, esDescripcion, esBloqueo, esAsing);   
	var Parametros = "Perfil_Etiqueta.jsp?sCvePerfilProt=" + esDescripcion;

	fnPasaDatosFac(esCveSistema, esCvePerfilProt);

	var esDescripcionTrunc = top.body.body2.frmCaptura.fnTruncaCadena(esDescripcion, 36);
	var sCodigo = "<center><b><font class='Dato1'>";
	sCodigo += esDescripcionTrunc +"</font> </b> </center>";

	top.body.body2.frmCaptura.CnvEtiketa.mtdPRepinta(sCodigo);
}

/**************************************************************************************
*     function fnPasaDatosFac()
**************************************************************************************/
function fnPasaDatosFac(esCveSistema, esCvePerfilProt){
	var Parametros  = "ConsFacPP.jsp?parCveSistema=" + esCveSistema;
	Parametros += "&parCvePerfilProt="  + esCvePerfilProt+ "&iFilaBuscar=" + iFilaBuscar;
	//alert(Parametros);
	parent.frmConsultaFac.location.replace(Parametros);
}
</Script>
<%
	sCveAsig       =     request.getParameter("parCveAsig");  
	sCveSistema    =     request.getParameter("parCveSistema");  
	sCvePerfilProt =     request.getParameter("parCvePerfilProt");  
	sDescripcion   =     request.getParameter("parDescripcion");  
	sBloqueo       =     request.getParameter("parBloqueado");
	sTipoAc        =     request.getParameter("parTipo");
	sDatosHidden = request.getParameter("hidInter");

try{
	sTipUsr = "";
	if("".equals(sTipUsr.trim())){
		iAsingUsr = 1;
		iFilaBuscar = 7;
	}
	else if("C".equals(sTipUsr.trim())){
		iAsingUsr = 2;
		iFilaBuscar = 6;
	}
	else if ("B".equals(sTipUsr.trim())){
		iAsingUsr = 4;
		iFilaBuscar = 5;
	}
	
	if(sCveSistema != null  && sCvePerfilProt != null){
		sValCveSistema  = "SEGU";
		System.out.print("\t VALIDAR:");
		System.out.print("\t    Clave del sistema --> " + sValCveSistema);
		System.out.print("\t    Clave Perfil Prot --> " + (String) session.getAttribute("sesCvePerfilProt"));
		
		if(sTipoAc.equals(ELIMINAR)){
			sCveFacultad = "SEGPERFPROTB";
			System.out.print("\t    Clave de la facultad --> " + sCveFacultad);
			//objSeguridad.validaFacultad( (String) session.getAttribute("sesCveUsuario"),     (String) session.getAttribute("sesCvePerfilProt"),    sCveFacultad,     sValCveSistema,    (String) request.getRemoteAddr(),    (String) request.getRemoteHost()  );
			objSeguridad.borrarPerfilProt(sCveSistema, sCvePerfilProt);      
		}
		
		if(sTipoAc.equals(AGREGAR)){
			sCveFacultad   = "SEGPERFPROTA";
			//objSeguridad.validaFacultad( (String) session.getAttribute("sesCveUsuario"),     (String) session.getAttribute("sesCvePerfilProt"),    sCveFacultad,     sValCveSistema,    (String) request.getRemoteAddr(),    (String) request.getRemoteHost()  );
			objSeguridad.agregarPerfilProt(sCveSistema, sCvePerfilProt , sDescripcion, sBloqueo, Integer.parseInt(sCveAsig));   
		}
		
		if(sTipoAc.equals(MODIFICAR)){
			sCveFacultad   = "SEGPERFPROTC";
			//objSeguridad.validaFacultad( (String) session.getAttribute("sesCveUsuario"),     (String) session.getAttribute("sesCvePerfilProt"),    sCveFacultad,     sValCveSistema,    (String) request.getRemoteAddr(),    (String) request.getRemoteHost()  );
			objSeguridad.modificarPerfilProt(sCveSistema, sCvePerfilProt , sDescripcion, sBloqueo, Integer.parseInt(sCveAsig)); 
		}
	}
%>
<body class="MarcaBody">
<Form Name="Floor"  METHOD="POST">
<Script Language = "JavaScript"> 
	var sOnclick = "";
	var sColorFondoTdAnterior = "";
	var sNombreTdAnterior = "";
</Script>
<table width="100%"  borderColor="#339933" cellspacing="0" cellpadding="3" align="center" name="Cabecera">
<input type=hidden name=hidInter Value="">
<%
	vSubTabla = objSeguridad.consultarPerfilesProt();
	
	for(int yy=0; yy<vSubTabla.size(); yy++){
		iContRenglones++;
		vSubRegistro = (Vector) vSubTabla.elementAt(yy);
		sCveSistema  =         (String) vSubRegistro.elementAt(0);
		sCvePerfilProt =       (String) vSubRegistro.elementAt(1);
		sDescripcion =         (String) vSubRegistro.elementAt(2);
		sBloqueo     =         (String) vSubRegistro.elementAt(3);
		
		String  sAsing       =         (String) vSubRegistro.elementAt(iFilaBuscar);
		
		if(iFilaBuscar != 7  &&  sAsing.equals("N"))
			continue;
		
		if(iFilaBuscar == 7 ){
			iAsingUsr = ((Integer) vSubRegistro.elementAt(4)).intValue();
		}
		
		if(sCveSistema.equals(sCveSistemaAnt)){
%>
	<!-- ///////////////////////////////////  ARAM TABLA   ///////////////////////// -->
	<TR>
		<Script Language = "JavaScript"> 
			var sColorSub = mtdCambiaColor2(sColor); 
			sOnclick = "OnClick='mtdCambiaColorTd(this.id, ";
			sOnclick += '"' + sColorSub + '","<%=sCveSistema%>", "<%=sCvePerfilProt%>", "<%=sDescripcion%>", "<%=sBloqueo%>", "<%=iAsingUsr%>"';
			sOnclick += "); '";
			window.document.write("<TD width='*%' BgColor='"+sColorSub+"' ");
			window.document.write("id='td<%=iContRenglones%>' " + sOnclick ); 
			window.document.write(" OnMouseOver='mtdCambiaPuntero(this);' "); 
			window.document.write(" OnMouseOut='mtdRegresaPuntero(this);' >"); 
			window.document.write("<A HREF='#0'");
			window.document.write(' onclick="fnPasaDatos(');          
			window.document.write("'<%=sCveSistema%>', '<%=sCvePerfilProt%>', '<%=sDescripcion%>', '<%=sBloqueo%>', '<%=iAsingUsr%>');")
			window.document.write('"> <font class="Dato2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=sDescripcion%> </font> </A>');
			window.document.write('</td>'); 


			window.document.write("<TD width='5%' BgColor='"+sColorSub+"' id='td<%=iContRenglones%>s0' >");
			window.document.write("<font size=1> <CENTER>");

			if("<%=sBloqueo%>" == "N" || "<%=sBloqueo%>" == "N")
				window.document.write("<Input Type='CheckBox' Name='chkBloq'>");
			else
				window.document.write("<Input Type='CheckBox' Name='chkBloq' checked>");

			window.document.write("</CENTER>");
			window.document.write("</font> </td>"); 
		</Script>
	</TR>
<%
		}
		else{
%>
	<TR>
		<Script Language = "JavaScript"> 
			sColor = mtdCambiaColor();
			window.document.write("<TD width='*%' BgColor='" + sColor + "'> <font class='Dato1'> <b> <%=sCveSistema%> </b> </font> </td>"); 
			window.document.write("<TD width='5%'  BgColor='" + sColor + "'> <font class='Dato1'> &nbsp; </font> </td>"); 
		</Script>
	</TR>
	<TR>
		<Script Language = "JavaScript"> 
			var sColorSub = mtdCambiaColor2(sColor); 
			sOnclick = "OnClick='mtdCambiaColorTd(this.id, ";
			sOnclick += '"' + sColorSub + '","<%=sCveSistema%>", "<%=sCvePerfilProt%>", "<%=sDescripcion%>", "<%=sBloqueo%>", "<%=iAsingUsr%>"';
			sOnclick += "); '";

			window.document.write("<TD width='*%' BgColor='" + sColorSub + "' ");
			window.document.write("id='td<%=iContRenglones%>' " + sOnclick ); 
			window.document.write(" OnMouseOver='mtdCambiaPuntero(this);' "); 
			window.document.write(" OnMouseOut='mtdRegresaPuntero(this);' >"); 
			
			window.document.write("<A HREF='#0'");
			window.document.write(' onclick="fnPasaDatos(');
			window.document.write("'<%=sCveSistema%>', '<%=sCvePerfilProt%>', '<%=sDescripcion%>', '<%=sBloqueo%>' , '<%=iAsingUsr%>');")
			window.document.write('"> <font class="Dato2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=sDescripcion%> </a> </font>');
			
			window.document.write("<TD width='5%' BgColor='"+sColorSub+"' id='td<%=iContRenglones%>s0' > ");
			window.document.write("<font size=1> <CENTER>");
			
			if("<%=sBloqueo%>" == "N" || "<%=sBloqueo%>" == "N")
				window.document.write("<Input Type='CheckBox' Name='chkBloq'>");
			else
				window.document.write("<Input Type='CheckBox' Name='chkBloq' checked>");

			window.document.write("</CENTER>"); 
			window.document.write("</font> </td>"); 
		</Script>
	</TR>
<%
		}
		sCveSistemaAnt = sCveSistema;
	}
}
catch(com.netro.seguridadbean.SeguException e){
	System.out.println("Error:" + e.toString());
	e.printStackTrace();
	String sError = e.getMessage();
%>
	<body class="MarcaBody">
	<Form>
		<Script Language="JavaScript">
			var Error = "<%=sError%>";
			if(Error == "")
				alert("Error: <%=sError%>");
			else
				alert(Error);
				window.location.replace('PerfilProt_Down.jsp');
			</Script>
<%
}
catch(Exception e){
	System.out.println("Error:" + e.toString());
	e.printStackTrace();
	String Error = e.getMessage();
%>
	<body class="MarcaBody">
	<Form>
	<Script Language="JavaScript">
		var Error = "<%=Error%>"
		alert("Error: <%=Error%>");
		parent.parent.parent.close();
	</Script>
	</Form>
	</body>
<%}%>
</table>
<Script Language="JavaScript">
var iFilaBuscar = "<%=iFilaBuscar%>";
/*********************************************************************
*   function mtdCambiaColorTd(sNombreTd)
*********************************************************************/
function mtdCambiaColorTd(sNombreTd, sColorTd, esCveSistema, esCvePerfilProt, esDescripcion, esBloqueo, esAsing){
	//alert("sColorTd = " + sColorTd);
	document.all[sNombreTd].style.backgroundColor = sColorIndi;
	var sDemasTds = "";

	sDemasTds = sNombreTd + "s0";
	document.all[sDemasTds].style.backgroundColor = sColorIndi;

	//// Regresa el color a los td antes clickeados.
	if(sColorFondoTdAnterior != "" &&  sNombreTdAnterior != "" && sNombreTdAnterior != sNombreTd ){
		document.all[sNombreTdAnterior].style.backgroundColor = sColorFondoTdAnterior;

		sDemasTds = sNombreTdAnterior + "s0";
		document.all[sDemasTds].style.backgroundColor = sColorFondoTdAnterior;
	}

	sColorFondoTdAnterior = sColorTd;
	sNombreTdAnterior =  sNombreTd;

	fnPasaDatos(esCveSistema, esCvePerfilProt, esDescripcion, esBloqueo, esAsing)
}
</Script>
</Form>
</body>
</html>
