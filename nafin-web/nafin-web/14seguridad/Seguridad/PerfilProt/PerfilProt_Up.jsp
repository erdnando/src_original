<!--
/*****************************************************************************
                 /////  Pragrama:    NAFIN                         /////
                 /////  Funci�n:     Introduce los datos de los    /////
                 /////                   Perfiles                  /////
                 /////                                             /////
                 /////  Autor:       Manuel Ramos Mendoza          /////
                 /////  Realizado:   08/  Enero/2001               /////
                 /////  Ult. Modif:  19/  Junio/2001               /////
*****************************************************************************/
-->
<%@ page import="java.sql.*,com.netro.seguridadbean.SeguException,java.lang.*,netropology.utilerias.*"%>
<html>
<link rel="stylesheet" href="../css/formato.css">  </style>
<head>
<Script language="JavaScript">
function sobre(src,Color){
	if(!src.contains(event.fromElement)){
		src.bgColor = Color;
	}
}
</Script>
</head>

<%
String  sSql = null,sClave = null,sTipUsr = null,sDescripcion = null,sBrinca = "";

int iAsingUsr = 0;

AccesoDB con = new AccesoDB();
ResultSet   resultSet = null,resulRestric =  null;

try{
	con.conexionDB();
	sTipUsr = "";
	
	if("".equals(sTipUsr.trim()))
		iAsingUsr = 1;
	else if("C".equals(sTipUsr.trim()))
		iAsingUsr = 2;
	else if("B".equals(sTipUsr.trim()))
		iAsingUsr = 4;
	
	/*javax.naming.Context naming = new javax.naming.InitialContext();
	javax.sql.DataSource ds = (javax.sql.DataSource) naming.lookup("seguridadData");
	Connection conn = ds.getConnection();*/
	
	sSql = "SELECT c_sistema, d_descripcion FROM seg_sistemas ORDER BY  c_sistema";
	resultSet = con.queryDB(sSql);
%>
<Script Language="Javascript"     src="../scripts/Validaciones.js">  </Script>
<Script language="JavaScript1.2"  src="../scripts/JsCanvas.js">  </Script>
<body bgcolor="#E0E0E0" OnLoad="frmCaptura.lstSistemas.focus();">
<FORM Name=frmCaptura>
<table width="620" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td class="formas">&nbsp;</td>
	</tr>
	<tr>
		<td class="titulo" align="center">Mantenimiento de Perfiles Prototipo</td>
	</tr>
	<tr>
		<td align="center">
			<table width="70%"  cellspacing="0" cellpadding="5" height="120">
				<tr> 
					<td width="11%" > 
						<font class="Etiketas"><A TARGET='_top' TITLE='Clave de Sistema'> Sistema</A></font>
					</td>
					<td width="41%" >
						<font size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
						<select name="lstSistemas" SIZE = "1" tabindex="1" OnChange="mtdResetTxt(0);"
							Style="Font-Size:10px;" >
<%
						while(resultSet.next()){
							sClave        = resultSet.getString(1).trim();
							sDescripcion  = resultSet.getString(2).trim();
%>
							<option value="<%=sClave%>"> <%=sClave + " - "+ sDescripcion%>
<%
						}
						resultSet.close();
						con.cierraStatement();
%>
						</select>
						</font>
					</td>
					<td width="20%" > 
						<font class="Etiketas"><A TARGET='_top' TITLE='Clave de Perfil'>Clave Perfil</A></font>
					</td>
					<td width="18%" >
						<font size=1>
						<input type="text" name="txtClavePerfilProt" size="15" maxlength="10"  TABINDEX="2"
							OnBlur="mtdValida(txtClavePerfilProt.name)"STYLE="FONT-FAMILY: Arial; FONT-SIZE: 10px;">
						</font> 
					</td>
					<td width="10%" rowspan=2 valign="bottom" align="right">
						<table cellpadding="3" cellspacing="1" border="0">
							<tr>
								<td class="celda02" width="50" height="21" align="center"><a href="#" OnClick="fnModificar('Agregar');">Agregar</a></td>
							</tr>
						</table>
						<table cellpadding="3" cellspacing="1" border="0">
							<tr>
								<td class="celda02" width="50" height="21" align="center"><a href="#" OnClick="fnModificar('Modificar');">Modificar</a></td>
							</tr>
						</table>
<%
						if (iAsingUsr != 1)
							sBrinca = "OnBlur='frmCaptura.lstSistemas.focus();'";
						else
							sBrinca = "";
%>
						<table cellpadding="3" cellspacing="1" border="0">
							<tr>
								<td class="celda02" width="50" height="21" align="center">
									<a href="#" onClick="fnModificar('Eliminar');" <%=sBrinca%> >Eliminar</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="11%">
						<font class="Etiketas"><A TARGET='_top' TITLE='Descripci�n del Sistema'>Descripci&oacute;n</a></font>
					</td>
					<td colspan="2"> 
						<font size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
						<Script Language="JavaScript">
							var isIE = (document.all ? true : false);
							var sSizeText = "";
							if(isIE)
								sSizeText = "45";
							else
								sSizeText = "30";
							var sOnBlur = "onBlur='mtdValida(txtDescripcion.name";
							sOnBlur += ");'";
							window.document.write('<input type="text" size="'+sSizeText+'" ');
							window.document.write('name="txtDescripcion" maxlength="40"  tabindex="3" ');
							window.document.write(sOnBlur + ' required="Yes" ');
							window.document.write('style="FONT-FAMILY: Verdana; FONT-SIZE: 10px;"> ');
						</Script>
						</font>
					</td>
				</tr>
				<tr>
					<td height="40" width="11%"><font class="Etiketas">Bloqueado</font></td>
					<td colspan=1 height="40" width="41%">
						<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="46%">
									<font class="Etiketas">
									<input type = "Radio" name = "optBloqueado" value="N" onClick="mtdBloq('N')" tabindex="4" Checked>
									No
									</font>
								</td>
								<td width="54%">
									<font class="Etiketas">
									<input type = "Radio" name = "optBloqueado" value="S" onClick="mtdBloq('N')" tabindex="5">
									Si
									</font>
								</td>
							</tr>
						</table>
					</td>
					<td width="20%" height="40" >&nbsp;</td>
					<td width="18%" height="40" >&nbsp;</td>
					<td valign="bottom" align="right" height="40" width="10%">
						<font size="1"><a href="#" onClick="fnModificar('Eliminar');" <%=sBrinca%> > </a> </font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type="hidden" name="hidBloque"  value="">
			<Script Language="JavaScript">
				var CnvEtiketa = new JSCanvas("Canvas_IE0");
			</Script>
			<!-- //////////////   INCRUSTA FRAME DE ETIKETA   ///////////// -->
			<!-- /////////////////////////////////////   Cabecera   /////////////////////////////////////-->
			<table align="center"  cellspacing="0" cellpadding="0"  border="0">
				<tr>
					<td>&nbsp;  </td>
					<td>&nbsp;  </td>
					<td>
						<Script Language="JavaScript">
							CnvEtiketa.mtdPDibuja(250, 18);
						</Script>
					</td>
				</tr>
				<tr>
					<td width="350">
						<table align="center" width="100%" cellspacing="0" cellpadding="3">
							<tr>
								<td width="18%" class="ColorCabecera" align="center">
									<font class="EncabezadoTabla">M&oacute;dulo</font>
								</td>
								<td width="%57" class="ColorCabecera" align="center">
									<font class="EncabezadoTabla">Perfil Prototipo</b></font>
								</td>
								<td width="25%" class="ColorCabecera" align="center">
									<font class="EncabezadoTabla">Bloqueo</font>
								</td>
							</tr>
						</table>
						<center>
						<!-- /////////////////////   INCRUSTA FRAMES   ////////////////////////  -->
						<iframe name="frmConsultaPP" width="350" height="240:px" bgcolor="#3366CC" hspace=0 vspace=0
							src="PerfilProt_Down.jsp?frmConsultaPP" marginwidth="0"
							marginheight="0" frameborder="0"  scrolling="yes">
						</iframe>
						</center>
					</td>
					<td width="10">&nbsp;</td>
					<td width="250">
						<!-- //////////////////////   Cabecera   //////////////////////-->
						<table align="center" width="100%" cellspacing="0" cellpadding="3">
							<TR>
								<td width="15%" class="ColorCabecera"> <font class="EncabezadoTabla">Facultad</font></td>
								<td width="15%" class="ColorCabecera" align="center"> 
									<a href="#0"  OnClick="top.body.body2.frmCaptura.frmConsultaFac.mtdActivaChecks(1)">
										<font class="EncabezadoTabla">Fac-Perf</font>
									</a>
								</td>
							</TR>
						</table>
						<center>
						<iframe name="frmConsultaFac" width="250" height="240:px" bgcolor="#3366CC" hspace=0 vspace=0
							src="ConsFacPP.jsp?frmConsultaFac" marginwidth=0 marginheight=0 frameborder=0>
						</iframe>
						</center>
					</td>
				</tr>
				<tr>
					<td class="formas">&nbsp;</td>
				</tr>
				<tr>
					<td>
						<!-- //////////////////  BOTON ASOCIAR  /////////////////  -->
						<table cellpadding="3" cellspacing="1" border="0" align="right">
							<tr>
								<td class="celda02" width="50" height="21" align="center"><a href="#" onclick='top.body.body2.frmCaptura.frmConsultaFac.generaDatos();' TABINDEX="9">Asociar</a></td>
							</tr>
						</table>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!-- //////////////////////   SCRIPTs  ///////////////////////  -->
<Script Language="JavaScript">
var iCveAsig = 0;
var USR = "<%=iAsingUsr%>";

/****************************************************************************
*   function fnModificar(Tipo)
****************************************************************************/
function fnModificar(Tipo){
	var CvePerfilProt =  window.document.frmCaptura.txtClavePerfilProt.value;
	CvePerfilProt = fnFiltraPorCaracter(CvePerfilProt," ");

	if(CvePerfilProt == ""   ||   iCveAsig == 0){
		alert("Debe llenar todos los campos");
	}
	else{
		var CveSistema = window.document.frmCaptura.lstSistemas.value;
		var Descripcion = window.document.frmCaptura.txtDescripcion.value;

		if(window.document.frmCaptura.optBloqueado(0).checked == true)
			var Bloqueo = "N";
		else
			var Bloqueo = "S";

		var Parametros = "PerfilProt_Down.jsp?parCveSistema=" + CveSistema;
		var sMensaje = "Esta Seguro de " + Tipo + " el registro?";

		switch (Tipo){
			case "Agregar":
				Parametros += "&parCvePerfilProt="  + CvePerfilProt;
				Parametros += "&parDescripcion=" + Descripcion;
				Parametros += "&parBloqueado=" + Bloqueo;
				Parametros += "&parCveAsig=" + iCveAsig;
				Parametros += "&parTipo=Agregar";
				break;
			case "Eliminar":
				Parametros += "&parCvePerfilProt="  + CvePerfilProt;
				Parametros += "&parTipo=Eliminar";
				parent.frmCaptura.frmConsultaFac.location.replace('ConsFacPP.jsp');
				//parent.frmCaptura.frmEtiketa.location.replace('Perfil_Etiqueta.jsp');
				CnvEtiketa.mtdPRepinta("");
			break;
			case "Modificar":
				Parametros += "&parCvePerfilProt="  + CvePerfilProt;
				Parametros += "&parDescripcion=" + Descripcion;
				Parametros += "&parBloqueado=" + Bloqueo ;
				Parametros += "&parCveAsig=" + iCveAsig;
				Parametros += "&parTipo=Modificar";
				break;
		}

		if(confirm(sMensaje)){
			//alert(Parametros);
			mtdBorra(0);
			parent.frmCaptura.frmConsultaPP.location.href=Parametros;
		}
		else{
			window.document.frmCaptura.txtClavePerfilProt.focus();
			window.document.frmCaptura.txtClavePerfilProt.select();
		}
	}
}

///////////////////////////// Recibe Datos de Floor  //////////////////////////////
function mtRecibeDatos(esCveSistema, esCvePerfilProt, esDescripcion, esBloqueo){
	//alert("Recibiendo en Ceil " + esAsing);
	window.document.frmCaptura.lstSistemas.value   = esCveSistema;
	window.document.frmCaptura.txtClavePerfilProt.value  = esCvePerfilProt;
	window.document.frmCaptura.txtDescripcion.value    = esDescripcion;
	window.document.frmCaptura.hidBloque.value       = esBloqueo;

	if(esBloqueo == 'S' ){
		document.frmCaptura.optBloqueado[1].checked = true;
	}
	else{
		window.document.frmCaptura.optBloqueado[0].checked = true;
	}
}

function mtdInhiveBtns(Activa){
	window.document.frmCaptura.btnAgregar.disabled = Activa;
	window.document.frmCaptura.btnModificar.disabled = Activa;
	window.document.frmCaptura.btnEliminar.disabled = Activa;
}

///////////////////////////// Valida Numeros  //////////////////////////////
function  mtdChecaNumero(Dato){
	if(fnNumerico(Dato) == false){
		//alert("Campo de valor num�rico")
		window.document.frmCaptura.txtCveSistema.value = "0";
	}
}

   //////////////////  Valida ke un dato sea de solo tipo numerico     //////////////////
function fnNumerico(iNum){
	if(isNaN(iNum))
		return false;
	else
		return true;
}

function mtdBloq(sDato){
	window.document.frmCaptura.hidBloque.value = sDato;
}

function mtdAsing(Valor){
	iCveAsig = Valor;
}

mtdAsing("<%=iAsingUsr%>");
//if (USR == 1)     mtdConvinacionAsing();
</Script>

</FORM>
</body>
</html>

<%
///////////////////////   Cierra Conexion   //////////////////
}
catch (Exception e){
	System.out.println("Error:" + e.toString());
	e.printStackTrace();
	//out.print("Error = " + e);
%>
	<script Language="JavaScript">
		alert("ERROR: <%=e.toString()%>");
	</script>
<%
}
finally{
	con.cierraConexionDB();
}
%>
