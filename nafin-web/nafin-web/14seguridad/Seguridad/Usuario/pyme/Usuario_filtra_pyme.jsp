<%@ page import="netropology.utilerias.*,java.sql.*,com.netro.seguridadbean.SeguException,java.lang.*"%>
<html>
<%
String  sSql          = null,
	sClave        = null,
	sNombrePYME   = null,
	sDescripcion  = null,
	sNumEPO       = null;
	
	sNumEPO = request.getParameter("parNumEpo");
	AccesoDB con = new AccesoDB();
	
	try{
		con.conexionDB();
		sSql  = "SELECT py.ic_pyme, py.cg_nombre || ' ' || py.cg_appat || py.cg_apmat,  py.cg_razon_social ";
		sSql += "FROM comcat_pyme py, comrel_pyme_epo rel  ";
		sSql += "WHERE py.ic_pyme = rel.ic_pyme   AND  rel.ic_epo = " + sNumEPO + "ORDER BY py.cg_razon_social";
		
	     ResultSet  resultSet = con.queryDB(sSql);
		
	     sClave       = "";
	     sNombrePYME  = "";
	     sDescripcion = "";
		
	     while (resultSet.next()){
			 sClave       += resultSet.getString(1)  + "|";
			 sDescripcion = resultSet.getString(3);
			 
			 if (sDescripcion == null  ||  sDescripcion.equals(""))
			    sNombrePYME += resultSet.getString(2) + "|";
			 else
			    sNombrePYME += sDescripcion + "|";
		}
		
	     resultSet.close();
		 con.cierraStatement();
%>
<Script Language="JavaScript">
	top.body.body2.frmTipoPantalla.frmCaptura.mtdCachaPymes("<%=sNombrePYME%>", "<%=sClave%>");
</Script>
<%
	}
	catch (Exception e)	{
		System.out.println("Error:" + e);
		e.printStackTrace();
		out.print("Error = " + e);
%>
		<Script Language="JavaScript">
			alert("Erro de conexion:  <%=e%>");
		</Script>
<%
	}
	finally{
		con.cierraConexionDB();
	}
%>
</html>