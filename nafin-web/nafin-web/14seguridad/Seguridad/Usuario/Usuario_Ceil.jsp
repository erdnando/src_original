<!--
/***************************************************************************************
*
*                /////  Pragrama:    NAFIN                         /////
*                /////  Funci�n:     Entrada de datos              /////
*                /////                                             /////
*                /////                                             /////
*                /////  Autor:       Manuel Ramos Mendoza          /////
*                /////  Realizado:   20/Enero  /2001               /////
*                /////  Ult. Modif:  30/Abril  /2001               /////
*
****************************************************************************************/
-->

<%@ page import="java.sql.*,com.netro.seguridadbean.SeguException,java.lang.*"%>
<html>
<LINK REL="stylesheet" HREF="../css/formato.css" TYPE="text/css">
<Script Language="JavaSCript" src="../css/colores.js">  </Script>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<style type="text/css">
		.fmargin{background:#ffffff; font-family: courier, "courier new", monospace; ; color:#000066; border-style:solid;}
	</style>
</head>

<%
String  sSql          = null,
	sClave        = null,
	sDescripcion  = null;
%>
<Script Language="JavaScript">
	alert('sesiNoUsuario:  <%=session.getAttribute("sesiNoUsuario")%>');
</Script>
<%try{%>

<Script Language="Javascript" src="../scripts/Validaciones.js">  </Script>
<Script Language="Javascript" src="../scripts/conversiones.js">  </Script>

<body  OnLoad="Ceil.txtNombre.focus();"> 
<FORM Name=Ceil >
<Script Language="JavaScript">
var Documento = window.document.Ceil;
var isIE = (document.all ? true : false);
var sOnBlur = "";
var sSizeText = "";

/**************************************************************************
*    function mtdFormateaCombo(iMaxlen, sDato1, sDato2)
*    FORMATEA LOS DATOS DE UN COMBO PASANDO COMO DATOS DOS CADENAS Y 
*           LA LONGITUD MAXIMA DE LA PRIMERA CADENA. 
**************************************************************************/
function mtdFormateaCombo(iMaxlen, sDato1, sDato2){
	var LONGITUDAD = 4;
	var paso = "&nbsp;&nbsp;";
	var longDato1 = sDato1.length;
	document.write(sDato1);
	for(var i=1; i<=(iMaxlen-longDato1)+LONGITUDAD; i++){
		document.write("<font face=verdana>" + paso + "</font>");
	}
	document.write(sDato2);
}
</Script>
<!-- /////////////////////////////////   INCRUSTA A HEAD  /////////////////////////////////////-->
<center>
<iframe name="Head" width="300" height="0:px" bgcolor="#3366CC" hspace=0 vspace=0
	src="head.jsp?Head"  scrolling="yes" marginwidth=0 marginheight=0 frameborder=0>
</iframe>
</center>

<center>
<table border="0" cellspacing="0" cellpadding="0" width="647" align="center">
	<tr>
		<td width="65"><font class="EtiketaUsr"><a target='_top' title='Tipo de Usuario'><b>*Tipo Usr</b></a></font></td>
		<td width="250">
			<select name="select" Style="Font-Size:10px">
				<option value="4" selected>Nafin</option>
				<option value="1">EPO</option>
				<option value="3">Pyme</option>
				<option value="2">If</option>
			</select>
		</td>
		<td width="72">&nbsp;</td>
		<td width="182">&nbsp;</td>
	</tr>
	<tr>
		<td width="65"> <font class="EtiketaUsr"> <a target='_top' title='Nombre del Usuario'><b>*Nombre</b> </a> </font> </td>
		<td width="250"> 
			<Script Language="JavaScript">
				if(isIE)
					sSizeText = "25";
				else
					sSizeText = "20";
				sOnBlur = "onBlur='mtdValida(txtNombre.name,";
				sOnBlur += '"Mayusculas"';
				sOnBlur += ");'";
				window.document.write('<input type="text" size="'+sSizeText+'" ');
				window.document.write('name="txtNombre" maxlength="40"  tabindex="1" '); 
				window.document.write(sOnBlur + ' required="Yes" ');
				window.document.write('style="FONT-FAMILY: Verdana; FONT-SIZE: 10px;" ');
				window.document.write('OnKeyPress="if(event.keyCode==13) {mtdValida(txtNombre.name,\'Mayusculas\'); mtdConsultar();}">');
			</Script>
		</td>
		<td width="72"> <font class="EtiketaUsr"> <a target='_top' title='Apellido Paterno'>*<b>Paterno </b> </a> </font> </td>
		<td width="182"> 
			<Script Language="JavaScript">
				if(isIE)
					sSizeText = "25";
				else
					sSizeText = "20";
				sOnBlur = "onBlur='mtdValida(this.name,";
				sOnBlur += '"Mayusculas"';
				sOnBlur += ");'";
				window.document.write('<input type="text" size="'+sSizeText+'" ');
				window.document.write('name="txtAPaterno" maxlength="40"  tabindex="2" '); 
				window.document.write(sOnBlur + ' required="Yes" ');
				window.document.write('style="FONT-FAMILY: Verdana; FONT-SIZE: 10px;" ');
				window.document.write('OnKeyPress="if(event.keyCode==13) {mtdValida(txtAPaterno.name,\'Mayusculas\'); mtdConsultar();}">');
			</Script>
		</td>
	</tr>
	<tr>
		<td width="65" height="18" valign="middle">
			<font class="EtiketaUsr"><a target='_top' title='Apellido Materno'> *<b>Materno</b> </a> </font>
		</td>
		<td width="250" height="18">
			<Script Language="JavaScript">
				if(isIE)
					sSizeText = "25";
				else
					sSizeText = "20";
				sOnBlur = "onBlur='mtdValida(this.name,";
				sOnBlur += '"Mayusculas"';
				sOnBlur += ");'";
				window.document.write('<input type="text" size="'+sSizeText+'" ');
				window.document.write('name="txtAMaterno" maxlength="40"  tabindex="3" '); 
				window.document.write(sOnBlur + ' required="Yes" ');
				window.document.write('style="FONT-FAMILY: Verdana; FONT-SIZE: 10px;" ');
				window.document.write('OnKeyPress="if(event.keyCode==13) {mtdValida(txtAMaterno.name,\'Mayusculas\'); mtdConsultar();} ">');
			</Script>
			<!--
			<input type="text" size="25" maxlength="40" name="txtAMaterno"  tabindex="3"
			onBlur="mtdValida(txtAMaterno.name , 'Mayusculas');"   required="Yes"
			style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; FONT-VARIANT: small-caps; TEXT-TRANSFORM: uppercase">
			-->
		</td>
		<td width="72" height="18" valign="middle">
			<font class="EtiketaUsr"><a target='_top' title='Usuario'>*<b>Usuario</b></a></font>
		</td>
		<td width="182" height="18">
			<font face="Verdana, Arial, Helvetica, sans-serif"> 
			<!-- onBlur="mtdPasaClave(txtClaveUsuario.name);"  --> 
			<input type="text" name="txtClaveUsuario" size="10" maxlength="8"  tabindex="4" required="Yes"
				style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; " OnKeyPress="if(event.keyCode==13) mtdConsultar();">
			<!-- FONT-VARIANT: small-caps; TEXT-TRANSFORM: uppercase --> </font> 
		</td>
		<td width="78" height="18" align="left">
			<font face="Verdana, Arial, Helvetica, sans-serif"> 
			<a href="#" OnClick="fnModificar('Agregar');"     title='Agrega Clave' TABINDEX="12">
				<img src="../gif/tab_agregar.gif" alt="Agrega Clave" Border="0" width="54" height="14">
			</a>
			</Font>
		</td>
	</tr>
	<tr>
		<td width="65" height="16" valign="middle">
			&nbsp;
			<!--
			<font class="EtiketaUsr">
			<a target='_top' title='Contrase&ntilde;a de Usuario'> 
			*Contrase&ntilde;a 
			</a>
			</font>
			-->
		</td>
		<td width="250" height="16" valign="bottom">
			&nbsp;
			<!--
			<Script Language="JavaScript">
				if(isIE)
					sSizeText = "20";
				else
					sSizeText = "15";
				window.document.write('<input type="password" size="'+sSizeText+'" ');
				window.document.write('name="txtContrasena" maxlength="15"  tabindex="5" '); 
				window.document.write(' required="Yes" ');
				window.document.write('style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; ');
				window.document.write('FONT-VARIANT: small-caps; TEXT-TRANSFORM: uppercase"> ');
			</Script>
			-->
		</td>
		<td width="72" height="16" valign="middle">
			&nbsp;
			<!--
			<font class="EtiketaUsr">
			<a target='_top' title='Confirmacion de la Contrase&ntilde;a'>
			*Confirmacion 
			</a>
			</font>
			-->
		</td>
		<td width="182" height="16">
			&nbsp;
			<!--
			<font face="Verdana, Arial, Helvetica, sans-serif">
			<Script Language="JavaScript">
				if(isIE)
					sSizeText = "20";
				else
					sSizeText = "15";
				window.document.write('<input type="password" size="'+sSizeText+'" ');
				window.document.write('name="txtConfirmacion" maxlength="15"  tabindex="6" '); 
				window.document.write(' required="Yes" ');
				window.document.write('style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; ');
				window.document.write('FONT-VARIANT: small-caps; TEXT-TRANSFORM: uppercase"> ');
			</Script>
			-->
		</td>
		<td width="78" height="16" align="left" valign="bottom">
			<font face="Verdana, Arial, Helvetica, sans-serif">
			<a href="#" OnClick="fnModificar('Eliminar');"   title='Modifica Registro' TABINDEX="13">
				<img src="../gif/tab_eliminar.gif" alt="Eliminar Registro" Border="0">
			</a>
			</font>
		</td>
	</tr>
	<tr>
		<td width="65" height="25" valign="middle">
			<font class="EtiketaUsr"><a target='_top' title='Fecha de Vencimiento'> *Vencimiento </a> </font>
		</td>
		<td width="250" height="25" nowrap valign="bottom">
			<input type="text" value="aaaa-mm-dd" name="txtFchVencimiento" size="15" maxlength="10"  tabindex="7"
				OnFocus="if(txtFchVencimiento.value=='' || txtFchVencimiento.value == 'aaaa-mm-dd' )
				txtFchVencimiento.value='<%=session.getAttribute("sesFchOper")%>';"required="Yes"
				OnBlur="ValidaFecha(txtFchVencimiento.name, txtFchVencimiento.value);"
				style="FONT-FAMILY: Arial; FONT-SIZE: 10px; FONT-VARIANT: small-caps; TEXT-TRANSFORM: uppercase">
		</td>
		<td width="72" height="25" valign="middle">
			<!--<font class="Etiketas"><a target='_top' title='Empresa'> Empresa </a></font>-->
			&nbsp;
		</td>
		<td width="182" height="25">
			<!--
			<Script Language="JavaScript">
				if(isIE)
					sSizeText = "30";
				else
					sSizeText = "20";

			sOnBlur = "onBlur='mtdValida(this.name,";
			sOnBlur += '"Mayusculas"';
			sOnBlur += ");'";
			
			window.document.write('<input type="text" size="'+sSizeText+'" ');
			window.document.write('name="txtEmpresa" maxlength="30"  tabindex="8" '); 
			window.document.write(sOnBlur + ' required="Yes" ');
			window.document.write('style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; FONT-VARIANT: small-caps; TEXT-TRANSFORM: uppercase"> ');
			</Script>
			-->
			&nbsp;
		</td>
		<td width="78" height="25" align="left" valign="bottom">
			<font face="Verdana, Arial, Helvetica, sans-serif">
			<a href="#" OnClick="fnModificar('Modificar');"   title='Modifica Registro' TABINDEX="14"> 
				<img src="../gif/tab_modificar.gif" alt="Modifica Registro" Border="0"> 
			</a> </font>
		</td>
	</tr>
	<tr>
		<td width="65" valign="middle">
			<font class="EtiketaUsr"> <a target='_top' title='Correo Electronico'>Mail</a></font>
		</td>
		<td width="250" height="0">
			<Script Language="JavaScript">
				if(isIE)
					sSizeText = "40";
				else
					sSizeText = "20";

				window.document.write('<input type="text" size="'+sSizeText+'" ');
				window.document.write('name="txtMail" maxlength="80"  tabindex="9" '); 
				window.document.write('onBlur="mtdValida(txtMail.name)" required="Yes" ');
				window.document.write('style="FONT-FAMILY: Arial; FONT-SIZE: 10px;"> ');
			</Script>
		</td>
		<td width="72" valign="middle">
			<font class="EtiketaUsr"> <a title='Estatus' >*Estatus </a> </font>
		</td>
		<td width="182" valign="bottom">
			<font class="EtiketaUsr"> &nbsp;&nbsp;A</font>
			<font class="Etiketas"> 
			<input type="radio" name="radEstatus" value="A" tabindex="10" 
				onClick=mtdAsingBloq(radEstatus(0).value) checked>
			I</font>
			<input type="radio" name="radEstatus" value="I" tabindex="11" 
				onClick=mtdAsingBloq(radEstatus(1).value) ></font>
		</td>
		<td width="78">
			<a href="#"   onClick="mtdConsultar();" OnBlur="Ceil.txtNombre.focus();" tabindex="15">
				<img src="../gif/buscar.gif"  alt="Buscar Usuario" Border="0">
			</a>
		</td>
	</tr>
</table>
</center>

<P>
<!-- /////////////////////////////////////   Cabecera   /////////////////////////////////////--> 
<table border="0" cellspacing="0" cellpadding="0" width="640px" height="18" class="ColorCabecera" align="center">
	<tr> 
		<td width="8%" align="center"><font class="EncabezadoTabla">Usuario</font></td>
		<td width="*%" align="center"><font class="EncabezadoTabla">Nombre</font></td>
		<td width="16%" lign="center"><font class="EncabezadoTabla">Estatus</font></td>
		<td width="26%" align="center"><font class="EncabezadoTabla">Fch Ven</font></td>
		<!--
		<td width="28%" ColsPan="2" align="center"><font class="EncabezadoTabla">Empresa</font></td>
		-->
	</tr>
</table>
<input type="hidden" name="hidEstatus"  value="A" >
<!-- /////////////////////////////////   INCRUSTA A FLOOR  /////////////////////////////////////-->   
<center>
<iframe name="Floor" width="640px" height="230px" bgcolor="#3366CC" hspace=0 vspace=0
	src=""  scrolling="yes" marginwidth=0 marginheight=0 frameborder=0>
</iframe>
<!-- Usuario_Floor.jsp?Floor -->
</center>
<!-- ///////////////////////////   SCRIPTs  /////////////////////////////////  -->
<Script Language="JavaScript">
var sFchOp = "<%=session.getAttribute("sesFchOper")%>";
function fnModificar(Tipo){
	with(window.document.Ceil){
		var Usr = txtClaveUsuario.value;
		var Nombre = txtNombre.value;
		var APat   = txtAPaterno.value;
		var AMat   = txtAMaterno.value;
		var FchVen = mtdValFchDaniel(txtFchVencimiento.value, "-");
		var Correo = txtMail.value;
		var Estat  = hidEstatus.value; 
		var bError = false;
		var sMensaje = "Esta Seguro de " + Tipo + " el registro?";
	
		if(FchVen == "aaaa-mm-dd"  ||  FchVen == "AAAA-MM-DD"){
			txtFchVencimiento.value = ""; 
			FchVen="";  
		}
	
		if((Nombre == ""   ||  FchVen == ""  ||  APat  == "" ||  AMat  == "" )   &&
			( Tipo == 'Agregar'  ||  Tipo == 'Modificar' )){
	
			alert("Hacen falta campos por llenar.")
		}
		else{
			//if (Usr != "" )  
			var Parametros = "Usuario_Floor.jsp?Usuario=" + Usr;
			switch (Tipo){
				case "Agregar":
					Parametros += "&Nombre=" + Nombre;
					Parametros += "&APaterno="  + APat;
					Parametros += "&AMaterno=" + AMat;
					Parametros += "&Correo="  + Correo;
					Parametros += "&Estatus=" + Estat;
					Parametros += "&FchVent=" + FchVen;
					Parametros += "&TipoOp=Agregar#" + Usr;
					break;
				case "Eliminar":
					Parametros += "&TipoOp=Eliminar";  
					break;
				case "Modificar":
					Parametros += "&Nombre=" + Nombre;
					Parametros += "&APaterno="  + APat;
					Parametros += "&AMaterno=" + AMat;
					Parametros += "&Correo="  + Correo;
					Parametros += "&Estatus=" + Estat;
					Parametros += "&FchVent=" + FchVen;
					Parametros += "&TipoOp=Modificar"; 
					break;
			}
			if(confirm(sMensaje)){
				if(bError == false){
					mtdBorra(0);
					parent.Ceil.Floor.location.href="";
					parent.Ceil.Floor.location.href=Parametros;
				}
				else
					alert("Esta mal la contrase�a");
			}
			else{
				window.document.Ceil.txtClaveUsuario.focus(); 
				window.document.Ceil.txtClaveUsuario.select(); 
			}
		}
	} // with
}

///////////////////////////// Recibe Datos de Floor  //////////////////////////////
function mtRecibeDatos(Usr, Nom, APat, AMat, Edo, Mail, FchVen, Empre){
	Documento.txtClaveUsuario.value   = Usr;
	Documento.txtNombre.value         = Nom;
	Documento.txtAPaterno.value       = APat;
	Documento.txtAMaterno.value       = AMat;
	Documento.txtMail.value           = Mail;
	Documento.txtFchVencimiento.value = FchVen;
	//Documento.txtEmpresa.value        = Empre;
	Documento.hidEstatus.value        = Edo; 
	//Documento.txtContrasena.value     = "";
	//Documento.txtConfirmacion.value   = "";
	if(Edo == "A")
		Documento.radEstatus[0].checked = true;
	else
		Documento.radEstatus[1].checked = true;
}

  ///////////////////////////// hidden de Bloqueo  //////////////////////////////
function mtdAsingBloq(Dato){
	window.document.Ceil.hidEstatus.value = Dato;
}

/********************************************************************
* function mtdConsultar()
********************************************************************/
function mtdConsultar(){
	var lsNombre  = fnTrim(window.document.Ceil.txtNombre.value);
	var lsPaterno = fnTrim(window.document.Ceil.txtAPaterno.value);
	var lsMaterno = fnTrim(window.document.Ceil.txtAMaterno.value);
	var lsClave   = fnTrim(window.document.Ceil.txtClaveUsuario.value);
	
	var Parametros = "Usuario_Floor.jsp?Usuario=" + lsClave;
	Parametros += "&Nombre=" + lsNombre;
	Parametros += "&APaterno="  + lsPaterno;
	Parametros += "&AMaterno=" + lsMaterno;
	Parametros += "&TipoOp=Consultar"; 

	parent.Ceil.Floor.location.href=Parametros;
	
}

///////////////////////////// BUSCA UNA CLAVE PINTADA   //////////////////////////////
function mtdBuscar(){
	var sDato = prompt("Clave a Buscar", "");
	if(sDato != null){
		sDato = fnTrim(sDato);
		var Parametros = "Usuario_Floor.jsp#" + sDato;
		parent.Ceil.Floor.location.href=Parametros;
	}
}

/**************************************************************************************************
*                              Pasa la clave a los campos Passwors                        
***************************************************************************************************/
function mtdPasaClave(txtName){
	mtdValida(txtName);
	mtdSetValue("txtContrasena", 0, fnGetValue (txtName, 0), fnGetNumElement("txtContrasena", 0));
	mtdSetValue("txtConfirmacion", 0, fnGetValue (txtName, 0), fnGetNumElement("txtConfirmacion", 0));
}

/**************************************************************************************************
*                        ValidaFecha(txtFchVencimiento.value)
***************************************************************************************************/
function ValidaFecha(sNombre, sDato){
	sDato = mtdValFchDaniel(sDato ,"-");
	if(sDato != "aaaa-mm-dd"){
		if(fnCadenaDate(sFchOp, "-") > fnCadenaDate(sDato, "-")){
			alert("La fecha no debe ser menor a (" + sFchOp + ")");
			sDato = sFchOp;
		}
		//var dFchOp   = fnCadenaDate(sFchOp, "-");
		//var dCaptura = fnCadenaDate(sDato, "-");
	}
	window.document.forms[0].elements[sNombre].value = sDato;
	//alert(dFchOp);
}

/**************************************************************************************************
*                              Valida ke sea fecha valida
***************************************************************************************************/
function mtdValFchDaniel(Dato ,Separador){
	var VFch = Dato.split("-");
	if((VFch.length == 3) && (VFch[0] > ANOMIN)  && (VFch[0] < ANOMAX) && (VFch[1] <= MESES) && (VFch[2] <= MAXDIAS)){
		var FechaValida = new Date();
		FechaValida.setFullYear(VFch[0]);
		FechaValida.setMonth(VFch[1]-1);
		FechaValida.setDate(VFch[2]);
		Dato  = FechaValida.getFullYear();
		if(FechaValida.getMonth() < 9)
			Dato += Separador + "0" + ( parseInt(FechaValida.getMonth()) + 1 );
		else
			Dato += Separador + ( parseInt(FechaValida.getMonth()) + 1 );

		if(FechaValida.getDate() < 10)
			Dato += Separador + "0" + FechaValida.getDate();
		else
			Dato += Separador + FechaValida.getDate();
		return Dato;
	}
	else{
		return "aaaa"+Separador+"mm"+Separador+"dd";
	}
}
</Script>
<%
}
catch (Exception e){
	System.out.println("Error:" + e.toString());
	e.printStackTrace();
	out.print("Error = " + e.toString());
}
%>
</FORM>
</body>
</html>
