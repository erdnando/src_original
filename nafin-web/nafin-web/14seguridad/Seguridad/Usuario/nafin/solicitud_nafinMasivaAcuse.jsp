<%@ page import="com.netro.seguridadbean.*,
									java.sql.*, 
									com.jspsmart.upload.*,
									netropology.utilerias.*, 
									netropology.utilerias.usuarios.*,
									java.util.*,
									javax.naming.*,
									java.io.File,
									java.io.FileInputStream,
									java.io.BufferedReader,
									java.io.InputStreamReader" 
		 errorPage="/00utils/error.jsp"%>	
<%@ include file="/14seguridad/14secsession.jspf" %>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload" />
<% String continuar  = request.getParameter("continuar")==null ? ""  : (String)request.getParameter("continuar"); %>
<% String numero  = request.getParameter("numero")==null ? ""  : (String)request.getParameter("numero"); %>
<% String nombreAfiliado  = request.getParameter("nombreAfiliado")==null ? ""  : (String)request.getParameter("nombreAfiliado"); %>
<% String nuErrores  = request.getParameter("nuErrores")==null ? ""  : (String)request.getParameter("nuErrores"); %>
<% String tipoAfiliado9  = request.getParameter("tipoAfiliado")==null ? ""  : (String)request.getParameter("tipoAfiliado"); %>
<%List resultados =  new ArrayList(); %>
<%List Tcorrectos =  new ArrayList(); %>
<%StringBuffer contenidoArchivo = new StringBuffer(); %>
<%CreaArchivo archivo = new CreaArchivo(); %>
<%String nombreArchivo=""; %>
<%boolean fallo = false; %>
<%int totalRegistros = 0; %>
<%int totalB = 0; %>


	<% try { %>

		<%com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);%>

	  <%Tcorrectos = BeanSegFacultad.getUsarios( numero);%>
	 	<%for( int v = 0; v<Tcorrectos.size(); v++){%>
		<%List infor = (ArrayList)Tcorrectos.get(v);%>
		<%String tipoAfiliado1 =  (String)infor.get(0);%>
		<%String claveAfiliado = (String)infor.get(1);		%>
		<%String nombre =  (String)infor.get(2);%>
		<%String apellidoPaterno =  (String)infor.get(3);%>
		<%String apellidoMaterno =  (String)infor.get(4);%>
		<%String email =  (String)infor.get(5);%>
		<%String perfil =  (String)infor.get(6);%>
		<%String siac =  (String)infor.get(7);%>
		<%String tipoAfiliado ="";%>
		<%if(tipoAfiliado1.equals("NAFIN")) {tipoAfiliado ="N"; }%>
		<%if(tipoAfiliado1.equals("EPO")) {tipoAfiliado ="E"; }%>
		<%if(tipoAfiliado1.equals("IF")) {tipoAfiliado ="I"; }			%>
		<%if(!siac.equals("")) {claveAfiliado =siac; } // el numero de Siac  funge como numero de clave Afiliado cuando es NAFIN%>
		
	 <% SolicitudArgus solicitud = BeanSegFacultad.generarSolicitudArgus(tipoAfiliado,claveAfiliado, nombre, apellidoPaterno,  apellidoMaterno, email, perfil); %>
		
		<%if(tipoAfiliado1.equals("NAFIN")) {%>
			<%solicitud.setPreautorizacion(true);%>
		<%}else{%>
			<%solicitud.setPreautorizacion(true); //duda con esto porque tenia como False%>
		<%}		%>
		<%UtilUsr utilUsr = new UtilUsr();%>
		<%String ic_usuario_tentativo = utilUsr.registrarSolicitudAltaUsuario(solicitud);	%>
	<%}%>
	
	<%totalB = Tcorrectos.size();	%>
	<%totalRegistros = totalB + Integer.parseInt(nuErrores); %>															 
	<%}catch (Exception e){%>
	<%e.printStackTrace();%>
	<%fallo = true;%>
	<%out.println(e);%>
	<%}%>
<html>
<head>
<title>Genera Acuse</title>
<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">

<script language="JavaScript" src="../../../../00utils/valida.js"></script>

<script language="JavaScript1.2">
	function Regresar(){
		var f = document.forms[0];		
		f.action="about:blank";	
		f.submit();
	}

</script>
</head>
<body>
<form name="forma" method="post" enctype="multipart/form-data">
<br>
<br>
<% if(fallo==false) { // no hubo error  y se genero Exitosamente los Usuarios %>
<table size="100%" align="center" cellpadding="3" cellspacing="0" border="1" bordercolor="#A5B8BF" aling ="center">
	<tr>
		<td class="celda01" align="right">&nbsp;</td>
		<td class="celda01" align="center"><b>Total</td>		
	</tr>	
	<tr>
		<td class="formas" align="left">No Total de Registros</td>
		<td class="formas" align="center">&nbsp;<%=totalRegistros%></td>				
	</tr>	
	<tr>
		<td class="formas" align="left">No Total de Registros Cargados Correctamente</td>
		<td class="formas" align="center">&nbsp;<%=Tcorrectos.size()%></td>				
	</tr>	
	<tr>
		<td class="formas" align="left">No Total de Registros con Error</td>
		<td class="formas" align="center">&nbsp;<%=nuErrores%></td>				
	</tr>	
	<tr>
		<td class="formas" align="left">Tipo de usuario</td>	
		<% if(tipoAfiliado9.equals("NAFIN")){ %>		
		<td class="formas" align="center">NAFIN</td>
		<%}else{ %>
		<td class="formas" align="center">&nbsp;<%=nombreAfiliado%></td>	
		<%} %>
	</tr>
	<tr>
		<td class="formas" align="left">Usuario</td>
		<td class="formas" align="center">&nbsp;<%=strNombreUsuario.replace(',', ' ')%></td>				
	</tr>	
</table>
<br>
<br>
<% contenidoArchivo.append("No. total de Registros ,"+totalRegistros+"\n");%>	
<% contenidoArchivo.append("No. total de Registros Cargados Correctamente,"+Tcorrectos.size()+"\n");%>	
<% contenidoArchivo.append("No. total de Registros Cargados con Error,"+nuErrores+"\n");%>	
<% if(tipoAfiliado9.equals("NAFIN")){ %>
<% contenidoArchivo.append("Tipo Usuario,"+"NAFIN"+"\n");%>	
<%}else{%>
<% contenidoArchivo.append("Tipo Usuario,"+nombreAfiliado.replace(',', ' ')+"\n");%>	
<%}%>
<% contenidoArchivo.append("Usuario,"+strNombreUsuario.replace(',', ' ')+"\n");%>	
<% contenidoArchivo.append(" "+"\n");%>	
<% contenidoArchivo.append("Nombre, Email, Pefil, Numero SIRAC,"+"\n");%>
	<table size="100%" align="center" cellpadding="3" cellspacing="0" border="1" bordercolor="#A5B8BF" aling ="center">
	<tr>
		<td class="celda01" align="center"><b>Nombre</td>
		<td class="celda01" align="center"><b>Email</td>		
		<td class="celda01" align="center"><b>Perfil</td>		
		<td class="celda01" align="center"><b>Numero SIRAC</td>				
	</tr>	
	<% for( int v = 0; v<Tcorrectos.size(); v++){ %>
	<%	List infor = (ArrayList)Tcorrectos.get(v); %>
	<%	String nombre = (String)infor.get(2)+" "+(String)infor.get(3)+" "+(String)infor.get(4);%>
	<%	String tipoAfiliado2 = (String)infor.get(0); %>	
		<tr>
		<td class="formas" align="center">&nbsp;<%=nombre%></td>
		<td class="formas" align="center">&nbsp;<%=(String)infor.get(5)%></td>
		<td class="formas" align="center">&nbsp;<%=(String)infor.get(6)%></td>		
		<% if(tipoAfiliado2.equals("NAFIN")){ %>
		<td class="formas" align="center">&nbsp;<%=(String)infor.get(7)%></td>
		<%}else{%>
		<td class="formas" align="center">&nbsp;N/A</td>
		<% } %>
	</tr>	
	<% contenidoArchivo.append(nombre.toString().replace(',', ' ')+",");%>		
	<% contenidoArchivo.append((String)infor.get(5).toString().replace(',', ' ')+",");%>		
	<% contenidoArchivo.append((String)infor.get(6).toString().replace(',', ' ')+",");%>
	<% if(tipoAfiliado2.equals("NAFIN")){ %>
	<% contenidoArchivo.append((String)infor.get(7).toString().replace(',', ' ')+"\n");%>		
	<%}else{%>
	<% contenidoArchivo.append("N/A"+"\n");%>	
	<% } %>
<%
} %>
</table>
<br>
<br>
	<%	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){  %>
	<%		out.print("<--!Error al generar el archivo-->"); %>
	<%	}else{ %>
	<%		nombreArchivo = archivo.nombre; %>
	<%	}		%>
<table size="100%" align="center" cellpadding="3" cellspacing="0" border="0" bordercolor="#A5B8BF" aling ="right">
	<tr>	
	  <td align="center" class="celda02" width = "100" height="24"><a href="<%=strDirecVirtualTemp+nombreArchivo%>"><b>Generar Archivo</a></td>
		<td align="right">&nbsp;</td>
		<td align="center" class="celda02" width = "100" height="24"><a href="javascript:Regresar();"><b>Salir</a></td>	
	</tr>		
</table>
<% } if(fallo==true) { //  hubo error  y  NO generaron los Usuarios %>

<table size="100%" align="center" cellpadding="3" cellspacing="0" border="0" bordercolor="#A5B8BF" aling ="center">
	<tr>	
		<td align="center"><b>La solicitud de alta de usuario No ha sido registrada Ocurrio un Error</td>
		</tr>	
</table>
<br>
<br>
<table size="100%" align="center" cellpadding="3" cellspacing="0" border="0" bordercolor="#A5B8BF" aling ="center">
	<tr>	
		<td align="center" class="celda02" width = "100" height="24"><a href="javascript:Regresar();"><b>Salir</a></td>	
	</tr>	
</table>
<%}%>
<br>
<br>
</form>
</body>
</html>