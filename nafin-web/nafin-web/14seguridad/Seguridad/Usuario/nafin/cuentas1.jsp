<%@ page import="java.util.*, netropology.utilerias.*, netropology.utilerias.usuarios.*, java.sql.*" 
errorPage="/00utils/error.jsp" %>
<%@ include file="/appComun.jspf" %>
<%@ include file="/14seguridad/14secsession.jspf" %>

<%

String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");

String email = (request.getParameter("email") == null)?"":request.getParameter("email");



String nombre = (request.getParameter("nombre") == null || request.getParameter("nombre").equals(""))?"Nombre Contacto":request.getParameter("nombre");

String paterno = (request.getParameter("paterno") == null || request.getParameter("paterno").equals(""))?"Ap. Pat.":request.getParameter("paterno");

String materno = (request.getParameter("materno") == null || request.getParameter("materno").equals(""))?"Ap. Mat.":request.getParameter("materno");



boolean bloqueada = false;

boolean preautorizada = true;

UtilUsr utilUsr = new UtilUsr();



String usuarioTentativo = null;

if (!utilUsr.getExisteSolicitudAltaUsuario(ic_pyme,"P")) {

	usuarioTentativo = setSolicitudUsuario(

			ic_pyme, "P", 

			email+"@nafin.gob.mx",

			paterno, materno, nombre, "ADMIN PYME",

			"Raz. Soc. Pyme", "DUMY-000000-PPP", "Insurgentes Sur 1971", "Guadalupe Inn",

			"Alvaro Obregon", "DF", "Mexico", "01020",

			"53256000", "53256000","","",bloqueada,preautorizada);

}

%>



<html>

<head>

<title></title>

	<link rel="stylesheet" href="/nafin/14seguridad/Seguridad/css/nafin.css">

</head>



<body>

<TABLE width="600">

<TR>

	<TD>

		<table border="0" align="center">

			<tr>

				<td class="formas" align="center"" bgcolor="#CCCCCC"><br>

<%

if (usuarioTentativo!=null) {

%>

				<b>La SOLICITUD de alta de usuario ha sido registrada (<%=usuarioTentativo%>). </b>

<%

} else {

%>

				<b>La solicitud para la pyme especificada ya habia sido enviada anteriormente</b>

<%

}

%>

				<br>

				</td>

			</tr>

			<tr>

				<td align="center"><a href="cuentas.jsp">Regresar</a></td>

			</tr>

		</table>

	</TD>

</TR>

</TABLE>

</body>

</html>





<%!

/**

 * Genera una solicitud de alta de usuario

 *

 * @param sCveAfiliadoU Clave del Afiliado (ic_pyme, ic_epo, etc.)

 * @param sTipoAfiliadoU Tipo de Afiliado (P pyme, E Epo, etc..)

 * @param sEmailU Mail del usuario

 * @param sAppPaternoU Apellido paterno del usuario

 * @param sAppMaternoU Apellido Materno del usuario

 * @param sNombreU Nombre del usuario

 * @param sPerfilU Perfil a asignar al usuario

 * @param sCompaniaS Razon social de la empresa

 * @param sRFCS RFC

 * @param sDireccionS Datos de la direcci�n

 * @param sColoniaS Datos de la direcci�n

 * @param sMunicipioS Datos de la direcci�n

 * @param sEstadoS Datos de la direcci�n

 * @param sPaisS Datos de la direcci�n

 * @param sCodigoPostalS Datos de la direcci�n

 * @param sTelefonoS Datos de la direcci�n

 * @param sFaxS Datos de la direcci�n

 * @param spreguntaSecreta Pregunta secreta

 * @param srespuestaSecreta Respuesta secreta

 * @param bBloqueada Indicador de bloqueo de la solicitud

 * @param bPreAutorizacion Indicador de preautorizaci�n

 *

 * @return Clave del usuario asignada en esta solicitud

 *

 */



private String setSolicitudUsuario(String sCveAfiliadoU, String sTipoAfiliadoU, String sEmailU,

		String sAppPaternoU, String sAppMaternoU, String sNombreU, String sPerfilU,

		String sCompaniaS, String sRFCS, String sDireccionS, String sColoniaS,

		String sMunicipioS, String sEstadoS, String sPaisS,

		String sCodigoPostalS, String sTelefonoS, String sFaxS,

		String spreguntaSecreta, String srespuestaSecreta,

		boolean bBloqueada, boolean bPreAutorizacion) throws Exception {

	String login = null;

	try {

		Usuario usuario = new Usuario();

		SolicitudArgus solicitud = new SolicitudArgus();

		UtilUsr utilUsr = new UtilUsr();



		usuario.setClaveAfiliado(sCveAfiliadoU);

		usuario.setTipoAfiliado(sTipoAfiliadoU);

		usuario.setEmail(sEmailU);

		usuario.setApellidoPaterno((sAppPaternoU==null || sAppPaternoU.equals("")?"No especificado":sAppPaternoU.toUpperCase()));

		usuario.setApellidoMaterno((sAppMaternoU==null || sAppMaternoU.equals("")?"No especificado":sAppMaternoU.toUpperCase()));

		usuario.setNombre(sNombreU.toUpperCase());

		usuario.setPerfil(sPerfilU);



		solicitud.setCompania(sCompaniaS.toUpperCase());

		solicitud.setRFC(sRFCS);

		solicitud.setDireccion(sDireccionS.toUpperCase());

		solicitud.setColonia(sColoniaS.toUpperCase());

		solicitud.setMunicipio(sMunicipioS);

		solicitud.setEstado(sEstadoS);

		solicitud.setPais(sPaisS);

		solicitud.setCodigoPostal(sCodigoPostalS);

		solicitud.setTelefono(sTelefonoS);



		if (sFaxS != null && !sFaxS.equals("")) {

			solicitud.setFax(sFaxS);

		}



		if( spreguntaSecreta != null && srespuestaSecreta != null ){

			solicitud.setPreguntaSecreta(spreguntaSecreta);

			solicitud.setRespuestaSecreta(srespuestaSecreta);

		}



		//F078-2005. Crea la solicitud pero con estatus bloqueado si bBloqueada == true





		solicitud.setBloqueo(bBloqueada);

		solicitud.setPreautorizacion(bPreAutorizacion);

		solicitud.setUsuario(usuario);





		login = utilUsr.registrarSolicitudAltaUsuario(solicitud);



	} catch(Exception e) {

		e.printStackTrace();

		System.out.println("Problemas al generarse la Solicitud del Usuario, para el OID. Exception en AfiliacionBean::setSolicitudUsuario()");

		throw e;

	}

	return login;

}

%>