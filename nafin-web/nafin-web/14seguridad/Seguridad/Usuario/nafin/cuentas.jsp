<%@ page import="java.util.*, netropology.utilerias.*, netropology.utilerias.usuarios.*, java.sql.*" 
errorPage="/00utils/error.jsp" %>
<%@ include file="/appComun.jspf" %>
<%@ include file="/14seguridad/14secsession.jspf" %>

<%
String ic_nafin_electronico = (request.getParameter("ic_nafin_electronico") == null)?"":request.getParameter("ic_nafin_electronico");
%>

<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/nafin/14seguridad/Seguridad/css/nafin.css">
<script language="JavaScript">
function generarSolicitud() {
	var f = document.envio;
	if(f.email.value == "") {
		alert("Debe especificar su cuenta de correo");
		return;
	}
        /*
	if(f.email.value.indexOf("@") == -1) { 
		alert("La cuenta de correo: " + f.email.value + " NO es v�lida");
		return;
	}
        */
	f.submit();
}
</script>
</head>

<body>
<TABLE width="600">
<TR>
	<TD class="formas">
		<p align="center"><b>Generaci�n de Solicitudes de Alta de Cuenta para Usuarios Pyme</b></p>
		<form name="forma" method="POST" action="">
		<table align="center">
			<tr>
				<td class="formas">N@E Pyme</td>
				<td><input type="text" name="ic_nafin_electronico" class="formas"></td>
			</tr>
			<tr>
				<td colspan="2">
				<table>
					<tr>
						<td>
							<input class="formas" type="submit" value="Buscar">
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</form>
	</TD>
</TR>
<%
if (!ic_nafin_electronico.equals("")) {
	
	AccesoDB con = new AccesoDB();
	UtilUsr utilusr = new UtilUsr();
	con.conexionDB();
	
	String strSQL = 
			" SELECT p.ic_pyme, p.cg_razon_social " +
			" FROM comcat_pyme p, comrel_nafin n " +
			" WHERE p.ic_pyme = n.ic_epo_pyme_if " +
			" 	AND n.cg_tipo = 'P' " + 
			" 	AND n.ic_nafin_electronico = " + ic_nafin_electronico;
	ResultSet rs = con.queryDB(strSQL);
	List lUsuarios = null;
	String clavePyme = null;
	String razonSocial = null;
	if (rs.next()) {
		clavePyme = rs.getString("ic_pyme");
		razonSocial = rs.getString("cg_razon_social");
		lUsuarios = utilusr.getUsuariosxAfiliado(clavePyme, "P");
	}
	rs.close();
	con.cierraStatement();
	if (con.hayConexionAbierta()) {
		con.cierraConexionDB();
	}
	if (clavePyme == null) {
%>
<tr>
<td>
	<table align="center">
		<tr>
			<td class="formas">No se encontr� ninguna PYME con N@E <%=ic_nafin_electronico%></td>
		</tr>
	</table>
</td>
</tr>
<%
	} else {
%>
<tr>
<td>
	<table align="center">
		<tr>
			<td class="formas">N@E Pyme:</td>
			<td class="formas"><%=ic_nafin_electronico%></td>
		</tr>
		<tr>
			<td class="formas">Nombre Pyme:</td>
			<td class="formas"><%=razonSocial%></td>
		</tr>
<%
		if (lUsuarios.size() > 0) {
%>
		<tr>
			<td class="formas">Usuario:</td>
			<td class="formas">
<%
			for (int i = 0; i < lUsuarios.size(); i++) {
%>				
				<%=lUsuarios.get(i)%><br>
<%
			}
%>
			</td>
		</tr>
<%
		} else { //No hay Usuario en OID para la Pyme
%>
		<form name="envio" method="post" action="cuentas1.jsp">
		<input type="hidden" name="ic_pyme" value="<%=clavePyme%>">
		<tr>
			<td class="formas">Email:</td>
			<td class="formas"><input type="text" name="email" value="" class="formas"><br>
			</td>
		</tr>
		<tr>
			<td class="formas">Nombre:</td>
			<td class="formas"><input type="text" name="nombre" value="" class="formas"><br>
			</td>
		</tr>
		<tr>
			<td class="formas">Apellido Paterno:</td>
			<td class="formas"><input type="text" name="paterno" value="" class="formas"><br>
			</td>
		</tr>
		<tr>
			<td class="formas">Apellido Materno:</td>
			<td class="formas"><input type="text" name="materno" value="" class="formas"><br>
			</td>
		</tr>

		<tr>
			<td colspan="2" class="formas">
				<table cellpadding="0" cellspacing="5" border="0" class="formas">
				<tr>
					<td align="center" class="celda02" height="25" width="120"><a href="javascript:generarSolicitud();">Crear Solicitud Usuario</a></td>
				</tr>
				</table>
			</td>
		</tr>
		</form>
<%
		} 
%>
	</table>

</td>
</tr>
<%
	} //clavePyme != null
}
%>

</TABLE>
</body>
</html>