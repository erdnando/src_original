<%@ page import="com.netro.seguridadbean.*,
									java.sql.*, 
									com.jspsmart.upload.*,
									netropology.utilerias.*, 
									java.util.*,
									javax.naming.*,java.io.File,
									java.io.FileInputStream,
									java.io.BufferedReader,
									java.io.InputStreamReader" 
		 errorPage="/00utils/error.jsp"%>	
<%@ include file="/14seguridad/14secsession.jspf" %>

<%@ page contentType="text/html;charset=windows-1252"%>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload"/>
<% String tipoAfiliado  = request.getParameter("tipoAfiliado")==null?"":(String)request.getParameter("tipoAfiliado"); %>
<% String continuar  = request.getParameter("continuar")==null ? ""  : (String)request.getParameter("continuar"); %>
<% String clave_afiliado  = request.getParameter("clave_afiliado")==null ? ""  : (String)request.getParameter("clave_afiliado"); %>
<% List errores =  new ArrayList();%>
<%List correctos =  new ArrayList();%>
<%List resultados =  new ArrayList();%>
<%int nuErrores =0;%>
<%int nuCorrectos =0;%>
<%List datosC  =  new ArrayList();%>
<%List datosS  =  new ArrayList();%>
<%List Tcorrectos =  new ArrayList();%>
<%List lineaErrores =  new ArrayList();%>
<%int nuLineasC =0; %>
<%int nuLineasE =0;%>
<%String numero = "";%>
<%String nombreAfiliado  ="";%>
<%String ruta		= null;%>
<%StringBuffer contenidoArchivo = new StringBuffer();%>
<%CreaArchivo archivo = new CreaArchivo();%>
<%String nombreArchivo=""; %>
<%List datosOK  =  new ArrayList();%>

<%if(continuar.equals("S")){%>
	<%try {%>
	
		<%try {%>
		<%myUpload.initialize(pageContext);%>
		<%myUpload.setTotalMaxFileSize(20097152);		%>
		<%myUpload.upload();%>
	<%}catch(Exception e){}%>
	<%	// Cargar Archivo en Directorio Temporal			%>
	<%myUpload.save(strDirectorioTemp);%>
	<%com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);%>
	<%ruta	= strDirectorioTemp+myFile.getFileName();%>
	
	<%com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);%>
	<%//valida  los datos del Archivo de Carga%>
	<%resultados = 	BeanSegFacultad.validaCargaMasiva(ruta, tipoAfiliado);%>
	<%	//regresa el nombre del Afiliado cuando este es EPO O if%>
	<%	if(tipoAfiliado.equals("EPO") || tipoAfiliado.equals("IF") ){%>
	<%		nombreAfiliado = BeanSegFacultad.nombreAfiliado(clave_afiliado,  tipoAfiliado);%>
	<%	} %>
	<%if(resultados.size()>0){  %>
	<%  correctos = (ArrayList)resultados.get(0);	 %>
	<%	errores = (ArrayList)resultados.get(1);	  %>
	<%	datosOK =  (ArrayList)resultados.get(2);	  %>	
	<%	lineaErrores =  (ArrayList)resultados.get(3);	  %>		
	<%  nuErrores =errores.size(); %>
  <%  nuCorrectos =datosOK.size();	   %>
	<%}//if(resultados.size()>0){ %>
  
	<%	//guarda los Usuarios de forma temporal para posteiormente usarlos 		 %>
	<%	if(correctos.size()>0){  %>
	<%		numero = BeanSegFacultad.tmpUsarios(correctos, tipoAfiliado, clave_afiliado); %>
	<%	}  %>
	
	
			
																					 
	<% }catch (Exception e){ %>
	<% e.printStackTrace();	 %>
  <% System.out.println(e); %>
	<% } %>

<% } %>
<html>
<head>
<title>Solicitud de Alta de Usuario</title>
<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
<script language="JavaScript" src="../../../../00utils/valida.js"></script>
<script language="JavaScript1.2">
function mostrarTabla(nomdiv){
  showdiv(nomdiv);
}

function showdiv(id) {
	if (document.getElementById) { 
		document.getElementById(id).style.display = 'block';
	} else {
		if (document.layers) { 
			document.id.display = 'block';
		} else { 
			document.all.id.style.display = 'block';
		}
	}
}
// Ocualta la Tabla del Layaou
function ocultarTabla(nomdiv){
  hidediv(nomdiv);
}

function hidediv(id) {
	if (document.getElementById) { 
		document.getElementById(id).style.display = 'none';
	} else {
		if (document.layers) { 
			document.id.display = 'none';
		} else { 
			document.all.id.style.display = 'none';
		}
	}
}

		function Regresar(){
		var f = document.forms[0];		
		f.action="about:blank";	
		f.submit();
	}
	
	//Valida el Formato del Archivo el cual debe de ser .txt
function Continuar(valor) {
	var f = document.forma;
	var Archivo = f.txtarchivo.value;
	tipo = 1;
	if(!formatoValido(Archivo, tipo)) {
		alert('El Formato del Archivo es Incorrecto.');
		f.txtarchivo.focus();
		f.txtarchivo.select();
		return;
	}
	f.target = "_self";
	f.submit();
}



	function ContinuarCarga(valor1, valor2, valor3, valor4, valor5, valor6){  
		var f = document.forms[0];
		f.action="solicitud_nafinMasivaAcuse.jsp?continuar="+valor1+"&tipoAfiliado="+valor2+"&numero="+valor3+"&nombreAfiliado="+valor4+"&nuErrores="+valor5+"&tipoAfiliado="+valor6;
		f.submit();
	}
	
	
function MM_openBrWindow(theURL,winName,features)
{ //v2.0
  window.open(theURL,winName,features);
}

function VerArchivo(valor){
  var f = document.forms[0];	
  var strURL='verArchivoErrores.jsp?ruta='+valor;
  var strWinName="wndCDoctos";
  var strFeatures="toolbar=no,status=no,location=no,menubar=no,scrollbars=no,resizable=no,directories=no,fullscreen=no,channelmode=no,top=300,left=300,width=300,height=100";
  MM_openBrWindow(strURL,strWinName,strFeatures);
}

 

</script>
</head>
<body>
<form name="forma" method="post" enctype="multipart/form-data">
<input type="Hidden" name="continuar" value="S">
<br>
<br>
<table border="0" cellspacing="0" cellpadding="0" width="500" align="center">
	<tr>
		<td  colspan="3" class="formas" align="center"><b>Ruta del Archivo de Origen:</td>
	</tr>
	<tr>
		<td><a href="JavaScript:mostrarTabla('Layout');">
			<img src="/nafin/00utils/gif/ihelp.gif" border="0" width="15">
			</a>
		</td>
		<td class="formas" ><input type="File" name="txtarchivo" value="Examinar" size="45"></td>	
		<td align="right">&nbsp;</td>	
	</tr>
	<tr>
		<td colspan="3" align="right">&nbsp;</td>	
	 </tr>
	<tr>
		<td align="right">&nbsp;</td>	
		<td align="right">&nbsp;</td>
	  <td align="center" class="celda02" width = "100" height="24"><a href="javascript:Continuar('S');"><b>Recarga Archivo</a></td>
	</tr>	
	<tr>
		<td colspan="3" align="right">&nbsp;</td>	
	 </tr>
	 <% if(tipoAfiliado.equals("EPO") || tipoAfiliado.equals("IF") ){ %>
	<tr>
		<td colspan="3">
			<table size="100%" align="center" cellpadding="3" cellspacing="0" border="0" bordercolor="#A5B8BF">
				<tr>
				<td class="formas" align="right"><b>Tipo de Usuario:</td>	
				<td colspan="2" align="left" class="formas"><%=nombreAfiliado%></td>
				</tr>
			 </table>
	</tr>		
	<% } %>
	<tr>
		<td colspan="4">
			<div align="center" id="Layout" style="display:none">&nbsp;									
				<%@ include file="/14seguridad/Seguridad/Usuario/nafin/LayoutMasivaUsuarios.jsp"%>
			</div>
		</td>		
	</tr>
</table>
<br>
<br>

<%if(resultados.size()>0){ %>
	<table size="100%" align="center" cellpadding="3" cellspacing="0" border="0" bordercolor="#A5B8BF">
	<tr>
			<td>
				<table size="100%" align="center" cellpadding="3" cellspacing="0" border="0" bordercolor="#A5B8BF">
					<tr>
						<td class="formas" align="center"><b>Registros Sin Errores</td>					
					</tr>						 
				</table>
			</td>
			<td align="right">&nbsp;</td>
			<td align="right">&nbsp;</td>
			<td>
				<table size="100%" align="center" cellpadding="3" cellspacing="0" border="0" bordercolor="#A5B8BF">
					<tr>
						<td class="formas" align="center"><b>Registros Con Errores</td>
						</tr>
				</table>
			</td>		
		</tr>	
		
		<tr>
			<td>
				<table size="100%" align="center" cellpadding="3" cellspacing="0" border="1" bordercolor="#A5B8BF">
					<tr>
						<td class="celda01" align="center"><b>Linea</td>						
						<td class="celda01" align="center"><b>Observación</td>
					</tr>	
					<%//Sin Errores	 %>				
					<%if(datosOK.size()>0){	 %>	
			   	<%for(int x=0; x<datosOK.size(); x++) {	 %>
				  <% List infor = (ArrayList)datosOK.get(x);%>
					<% nuLineasE+=infor.size();%>							
			   	<%for(int y=0; y<infor.size(); y++) {	 %>
					<% List infor1 = (ArrayList)infor.get(y);%>						   	
					<tr>
						<td class="formas" align="center"><%=(String)infor1.get(0)%></td>						
						<td class="formas" align="center"><%=(String)infor1.get(1)%></td>
					</tr>								
					<%}%>	
					<%}%>					
					<%}else{ %>
						<tr>
						<td colspan="2" class="celda01" align="center"><b>No se encontraron registros sin error</td>
						</tr>						
					 <%}%>	
				</table>
			</td>
			
			<td align="right">&nbsp;</td>
			<td align="right">&nbsp;</td>			
			<td>
				<table size="100%" cellpadding="3" cellspacing="0" border="1" bordercolor="#A5B8BF">
					<tr>
						<td class="celda01" align="center"><b>Linea</td>
						<td class="celda01" align="center"><b>Campo</td>
						<td class="celda01" align="center"><b>Observación</td>
					</tr>						
					<%if(errores.size()>0){	 %>
					<%for(int x=0; x<errores.size(); x++) {	 %>
				  <% List infor = (ArrayList)errores.get(x);%>
					<% nuLineasE+=infor.size();%>							
			   	<%for(int y=0; y<infor.size(); y++) {	 %>
					<% List infor1 = (ArrayList)infor.get(y);%>
					<tr>
						<td class="formas" align="center"><%=(String)infor1.get(0)%></td>
						<td class="formas" align="center"><%=(String)infor1.get(1)%></td>
						<td class="formas" align="left"><%=(String)infor1.get(2)%></td>
					</tr>
						<%} %>
					 <%}%>			   
					 <% for(int b= 0; b < nuLineasC-nuLineasE-2; b++) { %>
						<br>					
						<%} %>										 
					<%}else{ %>
					<%int noLineas = nuLineasC-nuLineasE;
						 noLineas =noLineas -correctos.size()+3;						
						%>
						<tr>
						<td colspan="3" class="celda01" align="center"><b>No se encontraron registros con error</td>
						 <% for(int b= 0; b < noLineas; b++) { %>
						<!--<tr><td colspan="3"  align="right">&nbsp;</td></tr>	-->
						<%} %>	
						</tr>
					 <%} %>					
				</table>
			</td>		
		</tr>
			<tr>
			<td>
				<table size="100%" cellpadding="3" cellspacing="0" border="0" bordercolor="#A5B8BF" aling ="center">
					<tr>
						<td class="formas" align="center"><b>Total</td>
						<td align="right">&nbsp;</td>  
						<td class="formas" align="center"><%=nuCorrectos%></td>
					</tr>
				</table>
			</td>
			<td align="right">&nbsp;</td>
			<td align="right">&nbsp;</td>
			<td>
				<table size="100%" align="center" cellpadding="3" cellspacing="0" border="0" bordercolor="#A5B8BF" aling ="center">
					<tr>
						<td class="formas" align="center"><b>Total</td>
						<td align="right">&nbsp;</td>
						<td class="formas" align="center"><%=nuErrores%></td>
					</tr>
				</table>
			</td>		
		</tr>
	</table>
	<%}%>
<br>
<br>
<%//para formar el archito .txt con los errores %>
<%if(lineaErrores.size()>0){	 %>
	<%for(int x=0; x<lineaErrores.size(); x++) {	%>
	<% List infor = (ArrayList)lineaErrores.get(x);%>
	<% nuLineasE+=infor.size();%>
	<% for(int y=0; y<infor.size(); y++) {	%>
	<% List infor1 = (ArrayList)infor.get(y);%>
	<%String campo1 = (String)infor1.get(0);%>
	<%String campo2 = (String)infor1.get(1);%>
	<%String campo3 = (String)infor1.get(2);%>
	<%String campo4 = (String)infor1.get(3);%>
	<%String campo5 = (String)infor1.get(4);%>
	<%String campo6 = (String)infor1.get(5);%>	
	<%if(!campo1.equals("")){   contenidoArchivo.append(campo1);  }%>
	<%if(!campo2.equals("")){   contenidoArchivo.append("|"+campo2);  }%>
	<%if(!campo3.equals("")){   contenidoArchivo.append("|"+campo3);  }%>
	<%if(!campo4.equals("")){   contenidoArchivo.append("|"+campo4);  }%>
	<%if(!campo5.equals("")){   contenidoArchivo.append("|"+campo5);  }%>
	<%if(!campo6.equals("")){   contenidoArchivo.append("|"+campo6);  }	%>
	<%contenidoArchivo.append(" "+"\n");%>	
	<%}%>
	<%}%>
<%}%>
<%	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt")){  %>
<%		out.print("<--!Error al generar el archivo-->"); %>
<%	}else{ %>
<%		nombreArchivo = archivo.nombre; %>
<%	}		%>
			
<table size="100%" align="center" cellpadding="3" cellspacing="0" border="0" bordercolor="#A5B8BF">
	<tr>	
	 <td align="center" class="celda02" width = "100" height="24"><a href="javascript:Regresar();"><b>Regresar</a></td>
	 <td align="right">&nbsp;</td>
	 	<% if(errores.size()>0){ %>
		<td class="celda02" colspan="1"  width = "100" height="24" align="center"><a href="javascript:VerArchivo('<%=strDirecVirtualTemp+nombreArchivo%>')"><b>Generar Archivo Con Errores</a>&nbsp;</td>
		<%}%>
    <td align="right">&nbsp;</td>
		<% if(correctos.size()>0 ){ %>
		<td align="center" class="celda02" width = "100" height="24"><a href="javascript:ContinuarCarga('G','<%=tipoAfiliado%>','<%=numero%>','<%=nombreAfiliado%>','<%=nuErrores%>', '<%=tipoAfiliado%>');"><b>Continuar Carga</a></td>	
		<%}%>
	</tr>		
</table>
<br>
<br>
<br>
<br>
</form>
</body>
</html>