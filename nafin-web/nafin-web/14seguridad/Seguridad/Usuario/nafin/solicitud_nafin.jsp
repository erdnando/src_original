<%@ page import="com.netro.seguridadbean.*,java.sql.*,
		 netropology.utilerias.*, java.util.*,javax.naming.*" 
		 contentType="text/html;charset=windows-1252"
		 errorPage="/00utils/error.jsp"%>

<%@ include file="/14seguridad/14secsession.jspf" %>

<%
com.netro.seguridadbean.Seguridad objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);

try {
	String _strLogin = (String)session.getAttribute("Clave_usuario");
	String _strFacultad = "SEGUSRNAF";
	String _strPerfilProt = "PPSEGNAF";
	String _strSistema = "SEGU";	
	String _strPerfil = "ADMIN NAFIN";
	
	//El validaFacultad en este caso se usa m?s bien con la finalidad de 
	//registrar el movimiento en la bitacora: bit_diaria
	objSeguridad.validaFacultad( _strLogin, _strPerfil, 
			_strPerfilProt, _strFacultad, _strSistema, 
			request.getRemoteAddr(), request.getRemoteHost() );
} catch(com.netro.seguridadbean.SeguException errFacAsing) 	{
	System.out.println(errFacAsing.getMessage());
}


AccesoDB con2 = new AccesoDB();
String				qrySentencia	= "";
PreparedStatement 	ps				= null;
ResultSet			rs				= null;

String nombre = (request.getParameter("nombre")==null)?"":request.getParameter("nombre");
String apellido_paterno = (request.getParameter("apellido_paterno")==null)?"":request.getParameter("apellido_paterno");
String apellido_materno = (request.getParameter("apellido_materno")==null)?"":request.getParameter("apellido_materno");
String email = (request.getParameter("email")==null)?"":request.getParameter("email");
String confirmacion_email = (request.getParameter("confirmacion_email")==null)?"":request.getParameter("confirmacion_email");
String perfil = (request.getParameter("perfil")==null)?"":request.getParameter("perfil");
String clave_afiliado = (request.getParameter("clave_afiliado")==null)?"":request.getParameter("clave_afiliado");
String tipoAfiliado = (request.getParameter("tipoAfiliado")==null)?"":request.getParameter("tipoAfiliado");

%>

<html>
<head>
<title>Solicitud de Alta de Usuario</title>
<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">

<script language="JavaScript" src="../../../../00utils/valida.js"></script>

<script language="JavaScript">
function enviar() {
	var f = document.forma;
	if (!revisaCampo(f.nombre) || 
			!revisaCampo(f.apellido_paterno) || 
			!revisaCampo(f.apellido_materno) ||
			!revisaCampo(f.email) ||
			!revisaCampo(f.confirmacion_email)) {
		return;
	}

	if (f.perfil.options[f.perfil.selectedIndex].value == "") {
		alert("Debe especificar un perfil");
		f.perfil.focus();
		return;
	}
	
	if (f.email.value != f.confirmacion_email.value) {
		alert("La confirmaci?n del Email no concuerda con el Email. \n Por favor verifique");
		f.confirmacion_email.focus();
		return;
	}
<%if(perfil.equals("CREDITO")){%>	
	if(!revisaCampo(f.telefono) ||
			!revisaCampo(f.fax)){
		return;
	}
	
	if(f.area.options[f.area.selectedIndex].value == "") {
		alert("Debe especificar un area");
		f.area.focus();
		return;
	}	
<%}%>
	if (confirm("?Est? seguro de solicitar el alta de usuario?")) {
		f.submit();
	} else {
		return;
	}
}

function verificar(){
	var f = document.forma;
	f.action = "solicitud_nafin.jsp";
	f.submit();
}
</script>
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="550" align="left">
<tr>
	<td class="formas" align="center">&nbsp;</td>
</tr>
<tr>
	<td class="formas" align="center"><b>Introduzca los datos del usuario</b></td>
</tr>
<tr>
	<td class="formas">
	

		<form name="forma" method="post" action="../genera_solicitud.jsp">
		<input type="hidden" name="tipoAfiliado" value="<%=tipoAfiliado%>">
		<input type="hidden" name="tipo_afiliado" value="N">
		<input type="hidden" name="ligaRetorno" value="<%=request.getRequestURL()%>?tipoAfiliado=<%=tipoAfiliado%>">
		<table border="0" cellspacing="0" cellpadding="0" align="center">
		
<%
	List listaPerfiles = objSeguridad.consultarPerfiles(Integer.parseInt(tipoAfiliado));	//4. NAFIN
	//la lista "listaPerfiles" se procesa en campos_comunes.jspf
%>

<%-- NCM<%@ include file="../campos_comunes.jspf" %>--%>
		<!-- Inicio Include campos_comunes.jspf -->
		<tr>
			<td class="formas">*Nombre</td>
			<td class="formas"><input type="text" name="nombre" size="30" maxlenght="40" value="<%=nombre%>" onBlur="this.value = this.value.toUpperCase()"></td>
		</tr>
		<tr>
			<td class="formas">*Apellido Paterno</td>
			<td class="formas"><input type="text" name="apellido_paterno" size="25" maxlenght="40" value="<%=apellido_paterno%>" onBlur="this.value = this.value.toUpperCase()"></td>
		</tr>
		<tr>
			<td class="formas">*Apellido Materno</td>
			<td class="formas"><input type="text" name="apellido_materno" size="25" maxlenght="40" value="<%=apellido_materno%>" onBlur="this.value = this.value.toUpperCase()"></td>
		</tr>
		<tr>
			<td class="formas">*Email</td>
			<td class="formas"><input type="text" name="email" size="40" maxlenght="100" value="<%=email%>" ></td>
		</tr>
		<tr>
			<td class="formas">*Confirmaci&oacute;n Email</td>
			<td class="formas"><input type="text" name="confirmacion_email" size="40" maxlenght="100" value="<%=confirmacion_email%>"></td>
		</tr>
		<tr>
			<td class="formas">*Perfil del Usuario</td>
			<td class="formas">
				<select name="perfil" class="formas" onchange="javascript:verificar()">
					<option value="">Seleccione el perfil</option>
		<%
		//listaPerfiles se define en la pagina que realiza el include de esta p?gina
		Iterator it = listaPerfiles.iterator();
		while(it.hasNext()) {
			List campos = (List) it.next();
			String clave = (String) campos.get(0);	//Clave del perfil
		%>
					<option <%=((clave.equals(perfil))?"selected":"")%> value="<%=clave%>"><%=clave%></option>
		<%
		}
		%>			
				</select>
			</td>
		</tr>		
		<!-- Fin Include -->
		
		<tr>
			<td class="formas">N&uacute;mero Sirac</td>
			<td class="formas"><input type="text" name="clave_afiliado" size="40" maxlenght="100"></td>
		</tr>
		
		<%if(perfil.equals("CREDITO")){%>
		<tr>
			<td class="formas">*Telefono:</td>
			<td class="formas"><input type="text" name="telefono" size="15" maxlenght="8"></td>
		</tr>
		<tr>
			<td class="formas">*Fax:</td>
			<td class="formas"><input type="text" name="fax" size="15" maxlenght="8"></td>
		</tr>
		<%
		try {
			
			con2.conexionDB();
			qrySentencia = " select ic_area,cg_descripcion from cotcat_area ";
			ps = con2.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			
		%>
		<tr>
			<td class="formas">*Area:</td>
			<td class="formas">
				<select name="area" class="formas">
					<option value="">Seleccione Area</option>
					<%
					while(rs.next()){
						out.println("<option value="+rs.getString("ic_area")+">"+rs.getString("cg_descripcion")+"</option>");
					}
					%>				
				</select>
			</td>
		</tr>
		<%
		}catch(Exception e){
			System.out.println("error :"+e);
			e.printStackTrace();
		}finally {
			con2.terminaTransaccion(false);
			if(con2.hayConexionAbierta())
				con2.cierraConexionDB();
		}
		%>		
		<tr>
			<td class="formas">Director de Area:</td>
			<td class="formas"><input name="director" type="checkbox"></td>
		</tr>
		<%}//if perfil es CREDITO%>		

		<tr>
			<td width="100%" colspan="2" class="formas">
				<table cellpadding="3" cellspacing="1" border="0" align="right">
				<tr>
					<td class="celda02" width="50" height="21" align="center">
						<a href="javascript:enviar()">Agregar</a>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
</table>
</body>
</html>
