<!--
/***************************************************************************************
*
*                /////  Pragrama:    Nafin                     /////
*                /////  Funci�n:     Entrada de datos              /////
*                /////                                             /////
*                /////                                             /////
*                /////  Autor:       Manuel Ramos Mendoza          /////
*                /////  Realizado:   20/Enero  /2001               /////
*                /////  Ult. Modif:  30/Abril  /2001               /////
*
****************************************************************************************/
-->

<%@ page import="netropology.utilerias.*, java.sql.*,com.netro.seguridadbean.SeguException,java.lang.*"%>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  <style type="text/css">
  .fmargin{background:#ffffff; font-family: courier, "courier new", monospace; ; color:#000066; border-style:solid;}
  </style>

</head>


<%
    String  sSql          = null,
            sClave        = null,
            sDescripcion  = null;
%>





<%

  try
  {
%>



<LINK REL="stylesheet" HREF="../../css/formato.css" TYPE="text/css">
<Script Language="JavaSCript" src="../../css/colores.js">  </Script>
<Script Language="Javascript" src="../../scripts/Validaciones.js">  </Script>
<Script Language="Javascript" src="../../scripts/conversiones.js">  </Script>
<Script Language="Javascript" src="../../scripts/JsCanvas.js">      </Script>


<body  OnLoad="Ceil.txtNombre.focus();">
<FORM Name=Ceil >
<Script Language="JavaScript">
   var Documento = window.document.Ceil;
   var isIE = (document.all ? true : false);
   var sOnBlur = "";
   var sSizeText = "";

  /**************************************************************************
  *    function mtdFormateaCombo(iMaxlen, sDato1, sDato2)
  *    FORMATEA LOS DATOS DE UN COMBO PASANDO COMO DATOS DOS CADENAS Y
  *           LA LONGITUD MAXIMA DE LA PRIMERA CADENA.
  **************************************************************************/
  function mtdFormateaCombo(iMaxlen, sDato1, sDato2)
  {
     var LONGITUDAD = 4;
     var paso = "&nbsp;&nbsp;";
     var longDato1 = sDato1.length;

     document.write(sDato1);
     for (var i=1; i<=(iMaxlen-longDato1)+LONGITUDAD; i++)
     { document.write("<font face=verdana>" + paso + "</font>");  }
     document.write(sDato2);
  }
</Script>


  <br><br>

    <!-- ------------------------ INCLUYENDO  PARTE DE CAPTURA  GENERICA  ------------------- -->
    <%@ include file="../Usuario_Campura.jsp" %>


  <br><br>




  <table width="630" cellpadding="0" cellspacing="0" border="0">
  <tr>
  	<td align="center">
  <Script Language="JavaScript">

     var cvnRegistro = new JSCanvas("cvnRegistro");
     cvnRegistro.mtdPDibuja(600, 30, 1, "#A5B8BF");

  </Script>
  </td>
  </tr>
  </table>

<br>

    <iframe name="frmGraba" width="440px" height="0px" bgcolor="#3366CC" hspace=0 vspace=0
         src=""  scrolling="yes" marginwidth=0 marginheight=0 frameborder=0>
    </iframe>
  </center>



                 <!-- ///////////////////////////   SCRIPTs  /////////////////////////////////  -->
 <Script Language="JavaScript">

  var sFchOp = "<%=session.getAttribute("sesFchOper")%>";




  /***************************************************************************
  *     function fnModificar()
  ***************************************************************************/

  function fnModificar()	{

	var liNoUsuario = parseInt(parent.frmSeleccion.document.frmSeleccion.lstNumUsuario.value);
	var lsUsuario = "";

	with (window.document.Ceil)	{
		switch (liNoUsuario)	{
			case 1:	{
				lsUsuario = "EPO";
	      		break;
			}
			case 2:	{
				lsUsuario = "IF";
				break;
			}
			case 3:	{
				lsUsuario = "PYME";
				break;
			}
			case 4:	{
				lsUsuario = "NAFIN";
				break;
			}
		}
		
		var Nombre = txtNombre.value;
		var APat   = txtAPaterno.value;
		var AMat   = txtAMaterno.value;
		var FchVen = fnInvierteFecha(txtFchVencimiento.value);
		FchVen = mtdValFchDaniel(FchVen, "/");
	    var Correo = txtMail.value;
	    var Estat  = hidEstatus.value;
		var SIRAC  = txtSIRAC.value;
	
	    var bError = false;
	    var sMensaje = "Esta Seguro de Agregar el registro?";
	
	    if (FchVen == FORMATO_FECHA )	{
			txtFchVencimiento.value = "";
	        FchVen="";
		}



      if ( ( Nombre == ""   ||  FchVen == ""  ||  APat  == "" ||  AMat  == "" )  )
          alert("Hacen falta campos por llenar.")
      else
      {
            var Parametros = "../Usuario_Graba.jsp?";
	        Parametros += "Nombre=" + Nombre;
            Parametros += "&APaterno="  + APat;
            Parametros += "&AMaterno=" + AMat;
            Parametros += "&Correo="  + Correo;
            Parametros += "&Estatus=" + Estat;
            Parametros += "&FchVent=" + FchVen;
	        Parametros += "&Usuario=" + lsUsuario;
	        Parametros += "&NoUsuario=" + liNoUsuario;

	        Parametros += "&CveEPO=0";
	        Parametros += "&CvePYME=0";
	        Parametros += "&CveIF=0";
	        Parametros += "&Afiliacion=";

            Parametros += "&TipoOp=Agregar";
			Parametros += "&NumSIRAC=" + SIRAC

            if (confirm(sMensaje))	{
               mtdBorra(0);
	           self.frmGraba.location.replace(Parametros);
            }
      } /// else de contrase�a
    } // with

  }




  ///////////////////////////// hidden de Bloqueo  //////////////////////////////
  function mtdAsingBloq(Dato)
  {   window.document.Ceil.hidEstatus.value = Dato;  }









/**************************************************************************************************
*                        ValidaFecha(txtFchVencimiento.value)
***************************************************************************************************/
  function ValidaFecha(sNombre, sDato)
  {
     sDato = mtdValFchDaniel(sDato ,"-");
     if (sDato != "dd-mm-aaaa")
     {
       if ( fnCadenaDate(sFchOp, "-") > fnCadenaDate(sDato, "-") )
       {
         alert("La fecha no debe ser menor a (" + sFchOp + ")");
         sDato = sFchOp;
       }
       //var dFchOp   = fnCadenaDate(sFchOp, "-");
       //var dCaptura = fnCadenaDate(sDato, "-");
     }
     window.document.forms[0].elements[sNombre].value = sDato;
     //alert(dFchOp);
  }









  /************************************************************************
  *  Pasa DATOS a Reporte
  ************************************************************************/
  var winRepUsr;

  function fnRep(Cve, Nom, APat, AMat, Edo, Mail, FchVen, Afiliacion, CveIF, CveEPO, CvePYME, CveUsNafin)
  {

	 if(typeof(winRepUsr) != 'undefined')
	   winRepUsr.close();

	 var lsPagina = "../consultar/Usuario_Perfil00.jsp?parClave=" + Cve + "&parNombre=" + Nom;
	 lsPagina += "&parAPaterno=" + APat + "&parAMaterno=" + AMat + "&parEdo=" + Edo + "&parMail=" + Mail;
	 lsPagina += "&parFchVen=" + FchVen + "&parCvePYME=" + CvePYME + "&parAfiliacion=" + Afiliacion;
	 lsPagina += "&parCveIF=" + CveIF + "&parCveEPO=" + CveEPO + "&parCveUsNafin=" + CveUsNafin;

	 //alert(lsPagina);

	 window.open(lsPagina,  "winUsrPerf", "ToolBar=No,Status=No,Location=No,MenuBar=No,ScrollBars=No,Resizable=No,Directories=No,Fullscreen=No,left=200,ChannelMode=No,Width=460,Height=500,resizable=False");

  }



</Script>
  <%
  ///////////////////////   Cierra Conexion   //////////////////

    }

    catch (Exception e)
    {
       System.out.println("Error:" + e);
       e.printStackTrace();
       out.print("Error = " + e);
    }
%>
</FORM>
</body>
</html>



