<!--
/**********************************************************************
*
*           /////  Pragrama:    NAFIN                         /////
*           /////  Funci�n:     Muestra datos grales de       /////
*           /////                 clientes                    /////
*           /////                                             /////
*           /////  Autor:       Manuel Ramos Mendoza          /////
*           /////  Mod:         Ricardo Jimenez Vargas        /////
*           /////  Realizado:   20/Enero  /2001               /////
*           /////  Ult. Modif:  26/Febrero/2003               /////
*
************************************************************************/
-->
<%@ page import=" java.text.*, java.lang.*,java.util.*, netropology.utilerias.*,com.netro.seguridadbean.Seguridad, com.netro.seguridadbean.* ,javax.naming.*"%>
<!--jsp:usebean id="objSeguridad" class="com.netro.seguridadbean.SeguridadBean" scope="page"/-->
<%
Seguridad objSeguridad = null;

try {
	objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",Seguridad.class);
} catch (Exception e) {
	System.out.println(e);
%>
	<script>
		alert("EXISTEN DIFICULTADES TECNICAS, INTENTE M�S TARDE");
	</script>
<%
	return;
}
%>

<html>
<Script Language="JavaSCript" src="../css/colores.js">  </Script>
<LINK REL="stylesheet" HREF="../css/formato.css" TYPE="text/css">
<%
   Vector  vRegistro     = null,
           vTabla        = null;
           
    String  sCorreo      = null,
            sClave       = null,
            sAPaterno    = "",
            sAMaterno    = "",
            sDato        = null,
            sNombre       = null,  
            sDatoTemp    = null,
            sDatosHidden = null,
            sValorBoton  = null,
            sEstatus      = null,
            sVerifCarga  = null,
            sContrasena  = null,
            sTipoAc      = null,
            sSql         = null,
            sFchVen      = null,
            sFchLog      = null,
            sFchCam      = null,
            sEmpresa     = null,
            sStop        = null,
            sCveSistema  = null,
            sCveFacultad = null,
			sNumSIRAC    = null,
            
            sAfiliacion  = null,
            sCveUsNafin  = null,
            sUsuario     = null,
	    sCvePYME     = null,
	    sCveIF       = null,
            sCveEPO      = null;
            
            
   Date     dFchVen;
   Date     dFchLog;
   Date     dFchCam;
            
             
   boolean  bPrimero;        
    
   final String AGREGAR   = "Agregar", 
                ELIMINAR  = "Eliminar",
                MODIFICAR = "Modificar",
				CONSULTAR = "Consultar";
   int   iBanco    = 0,
         iCliente  = 0;
   int iContRenglones = 0;
   
   int iNoUsuario = 0;
   
   AccesoDB con = new AccesoDB();
   
   
   sNombre      = request.getParameter("Nombre");
   sAPaterno    = request.getParameter("APaterno");
   sAMaterno    = request.getParameter("AMaterno");
   sEstatus     = request.getParameter("Estatus");
   sContrasena  = request.getParameter("Contrasena");
   sCorreo      = request.getParameter("Correo");
   sFchVen      = request.getParameter("FchVent");
   sUsuario     = request.getParameter("Usuario");
   sCveEPO      = request.getParameter("CveEPO");
   sCvePYME     = request.getParameter("CvePYME");
   sCveIF       = request.getParameter("CveIF");
   sCveUsNafin  = request.getParameter("CveUsNafin");
   sAfiliacion  = request.getParameter("Afiliacion");
   sNumSIRAC    = request.getParameter("NumSIRAC");


   if ( sNumSIRAC == null )  sNumSIRAC = "";



   sTipoAc      = request.getParameter("TipoOp");
   iNoUsuario   = Integer.parseInt((String) request.getParameter("NoUsuario"));

   out.print("sCveIF: " + sCveIF + "<br>");
   out.print("sCvePYME: " + sCvePYME + "<br>");
   out.print("sCveEPO: " + sCveEPO + "<br>");
   out.print("iNoUsuario: " + iNoUsuario + "<br>");
   out.print("sUsuario: " + sUsuario + "<br>");
   out.print("sNombre: " + sNombre + "<br>");
   out.print("sAPaterno: " + sAPaterno + "<br>");
   out.print("sAMaterno: " + sAMaterno + "<br>");
   out.print("sEstatus: " + sEstatus + "<br>");
   out.print("sCorreo: " + sCorreo + "<br>");
   out.print("sFchVen: " + sFchVen + "<br>");
   out.print("sTipoAc: " + sTipoAc + "<br>");
   out.print("sCveUsNafin: " + sCveUsNafin + "<br>");
   out.print("sAfiliacion: " + sAfiliacion + "<br>");
   
   
  
  try 	{
  	String sTpUsr = "";
    char [] chTipo = new char[1];
	
    if (sTipoAc.equals(AGREGAR)) 	{
		sCveFacultad   = "SEGUSUARIOA";
        //AccesoDB con = new AccesoDB();
        con.conexionDB();
        String  consecutivo = null,
		SQLicUsuario = null,
		strLogin     = null;
		
		SQLicUsuario="SELECT TO_NUMBER(max(substr(IC_USUARIO,1,7)))+1 as cveu from seg_usuario";
		java.sql.ResultSet rsicUsuario = con.queryDB(SQLicUsuario);
		rsicUsuario.next();
		consecutivo = String.valueOf(rsicUsuario.getInt(1));
		sClave = Comunes.calculaFolio(consecutivo,7);    //_____________  CALCULO del FOLIO con DIGITO VERIFICADOR
        sContrasena = Comunes.generapassword();
        sCveUsNafin = sClave.substring(0,7)+"-"+sClave.substring(7,8);
		
		out.print("sCveUsNafin: (" + sCveUsNafin + ")<br>");
		//objSeguridad.agregarUsuario(sClave, sCveUsNafin, sNombre, sAPaterno, sAMaterno, sEstatus, sCorreo,  sContrasena, sFchVen,  iNoUsuario,  Integer.parseInt(sCveEPO), Integer.parseInt(sCvePYME), Integer.parseInt(sCveIF), "O");
 		
		objSeguridad.agregarUsuario(sClave, sCveUsNafin, sNombre, sAPaterno, sAMaterno, sEstatus, sCorreo,  sContrasena, sFchVen,  iNoUsuario,  Integer.parseInt(sCveEPO), Integer.parseInt(sCvePYME), Integer.parseInt(sCveIF), "O", sNumSIRAC);
%>      <Script Language="JavaScript">
		   alert("Se ha dado de alta el usuario [<%=sClave%>] \n con contrase�a [<%=sContrasena%>]");
        </Script>  

		       
       <Script Laguage="JavaScript">
	       var sContrasena = "<%=sContrasena%>";
	       var sClave = "<%=sClave%>";
	       var sNombre = "<%=sNombre%> <%=sAPaterno%> <%=sAMaterno%>";
	       var sFchVen = "<%=sFchVen%>";
	       var sCorreo = "<%=sCorreo%>"
	       var sEstatus = "<%=sEstatus%>";

	       var lstabla = '<table align="center" width="600px" cellspacing="0" cellpadding="0">';
	       lstabla += '<tr>';
	       lstabla += '<td width="18%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Clave</font></td>';
	       lstabla += '<td width="*%" class="ColorCabecera" align="center"><font class="EncabezadoTabla" >Nombre</font></td>';
	       lstabla += '<td width="20%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Fch Ven</font></td>';
	       lstabla += '<td width="15%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Correo</font></td>';
	       lstabla += '<td width="10%" class="ColorCabecera" align="center"><font class="EncabezadoTabla">Estatus</font></td>';
	       lstabla += '</tr>';
	       lstabla += '<tr>';
	       lstabla += '<td width="18%" align="center"><font class="Dato1">';
	       lstabla += '<a href="#0" OnClick="fnRep(\'<%=sClave%>\', \'<%=sNombre%>\', \'<%=sAPaterno%>\', \'<%=sAMaterno%>\', \'<%=sEstatus%>\',  \'<%=sCorreo%>\', \'<%=sFchVen%>\', \'<%=sAfiliacion%>\', \'<%=sCveIF%>\', \'<%=sCveEPO%>\', \'<%=sCvePYME%>\', \'<%=sCveUsNafin%>\');"> ';
	       lstabla +=       sClave;
	       lstabla += '</a>';
	       lstabla += '</font></td>';
	       lstabla += '<td width="*%"  align="center"><font class="Dato1" >'+sNombre+'</font></td>';
	       lstabla += '<td width="20%" align="center"><font class="Dato1">'+sFchVen+'</font></td>';
	       lstabla += '<td width="15%" align="center"><font class="Dato1">'+sCorreo+'</font></td>';
	       lstabla += '<td width="10%" align="center"><font class="Dato1">'+sEstatus+'</font></td>';
	       lstabla += '</tr>';
	       lstabla += '</table>';

	       top.body.body2.frmTipoPantalla.frmCaptura.cvnRegistro.mtdPRepinta(lstabla);
		</Script>
<%  }

	if (sTipoAc.equals(MODIFICAR)) 	{
    	sCveFacultad   = "SEGUSUARIOC";
		
		//objSeguridad.modificarUsuario(sUsuario, sCveUsNafin, sNombre, sAPaterno, sAMaterno, sEstatus, sCorreo, "", sFchVen, iNoUsuario, Integer.parseInt(sCveEPO), Integer.parseInt(sCvePYME), Integer.parseInt(sCveIF), sAfiliacion);
		 objSeguridad.modificarUsuario(sUsuario, sCveUsNafin, sNombre, sAPaterno, sAMaterno, sEstatus, sCorreo, "", sFchVen, iNoUsuario, Integer.parseInt(sCveEPO), Integer.parseInt(sCvePYME), Integer.parseInt(sCveIF), sAfiliacion, sNumSIRAC);
%>		<Script Language="JavaScript">
        	alert("EL usuario <%=sUsuario%> ha sido actualizado");
			parent.parent.window.opener.window.document.location.reload();
		</Script>  
<%      sTipoAc = CONSULTAR;
       }
	}
    catch (com.netro.seguridadbean.SeguException errSeg) 	{
		System.out.println("Error:" + errSeg);
       	errSeg.printStackTrace();
       	String sError = errSeg.getMessage();
        %>
        <Script Language="JavaSCript">
	   		alert("No se agrego el registro.\n\n Error: <%=sError%>")
		</Script>
       <%
	   out.print(sError);
    }
    catch (Exception e) 	{
		System.out.println("Error:" + e);
       	e.printStackTrace();
       	String sError = e.getMessage();
		out.print("Error: " + sError);
    }
	finally	{
        con.cierraConexionDB();
    }
%> 
</html>