<%@ page import= "netropology.utilerias.usuarios.*, java.sql.*, java.util.List.*,
			      netropology.utilerias.*,javax.naming.*,
				  com.netro.cotizador.*"
errorPage="/00utils/error.jsp"%>

<%@ include file="../../14secsession.jspf" %>
<%
	AccesoDB			con				=  new AccesoDB();
	//			con = new AccesoDB();
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	String perfil = (request.getParameter("perfil")==null)?"":request.getParameter("perfil");
	String director = request.getParameter("director");
	String area = request.getParameter("area");
	boolean existe_director=false;
	boolean genera_solicitud=true;
	String Mensajillo="";
	if(perfil.equals("CREDITO")){
		if(director != null){ //se intenta dar de alta alta a un director de area
			try {
				con.conexionDB();
				qrySentencia = " select * from cotcat_ejecutivo where ic_area = ? and cs_director = 'S' ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1, area);
				rs = ps.executeQuery();
				if(rs.next()){//ya existe un director para esta area
					existe_director=true;
					genera_solicitud=false;
				}
			}catch(Exception e){
				System.out.println("errro :"+e);
				e.printStackTrace();
			}finally {
				con.terminaTransaccion(false);
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
			}
		}//if director
		if(existe_director){
%>
			<script language="JavaScript">
				alert("No se puede dar de alta, debido a que ya existe un director para el area espesificada");
				history.back();
			</script>
<%		}
	}//si perfil es CREDITO
	
	if(genera_solicitud){
		SolicitudArgus solicitud = generarSolicitudArgus(request);
		//Las solicitudes son preautorizadas para el usuario Nafin, para los dem�s usuarios
		//se requiere una autorizacion del administrador ARGUS.
		if(strTipoUsuario.equals("NAFIN")) {
			solicitud.setPreautorizacion(true);
		}else{
			solicitud.setPreautorizacion(false);
		}
		UtilUsr utilUsr = new UtilUsr();

		String ic_usuario_tentativo = utilUsr.registrarSolicitudAltaUsuario(solicitud);

		if(perfil.equals("CREDITO")){
			String nombre = request.getParameter("nombre");
			String apellido_paterno = request.getParameter("apellido_paterno");
			String apellido_materno = request.getParameter("apellido_materno");
			String email = request.getParameter("email");
			String telefono = request.getParameter("telefono");
			String fax = request.getParameter("fax");
			director = (director==null)?"N":"S";
			//System.out.println("director:"+director+"*");
			MantenimientoCotBean cotBean = new MantenimientoCotBean();
			cotBean.guardaEjecutivo(ic_usuario_tentativo,  nombre,  apellido_paterno,
									 apellido_materno,  email,  telefono,  fax,area, director);
			//System.out.println("El usuario tentativo es:"+ic_usuario_tentativo+"*");
		}
	}//si genera_solicitud
%>
<html>
<head>
	<title>Generacion de Solicitud - Alta de Usuario</title>
	<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="550" align="left">
<tr>
	<td class="formas" align="center">&nbsp;</td>
</tr>
<tr>
	<td class="formas" align="center"><b>La solicitud de alta de usuario ha sido registrada</b></td>
</tr>
<tr>
	<td class="formas" align="center">
		<table cellpadding="3" cellspacing="1" border="0" align="center">
		<tr>
			<td class="celda02" width="50" height="21" align="center">
			<%
			String ligaRetorno = request.getParameter("ligaRetorno");
			String tipoAfiliado = request.getParameter("tipo_afiliado");
			if(tipoAfiliado.equals("A")){ //Afianzadora
				String tipoAfiliado2 = request.getParameter("tipoAfiliado");
				ligaRetorno = ligaRetorno+"?tipoAfiliado="+tipoAfiliado2;	
			}
			%>
			<a href="<%=ligaRetorno%>">Regresar</a>
			</td>
		</tr>
	</td>
</tr>
</table>
</body>
</html>

<%!
/**
 * Genera la solicitud de alta de usuario al sistema ARGUS
 * Si los datos estan incompletos, genera un error.
 * Los datos para generar la solicitud los toma de los par�metros del
 * request
 * @param request request de la p�gina
 */
public SolicitudArgus generarSolicitudArgus(HttpServletRequest request)
		throws Exception {
	Usuario usr = new Usuario();
	SolicitudArgus solicitud = new SolicitudArgus();
	
	String tipoAfiliado = request.getParameter("tipo_afiliado");
	String claveAfiliado = request.getParameter("clave_afiliado");
	String nombre = request.getParameter("nombre");
	String apellidoPaterno = request.getParameter("apellido_paterno");
	String apellidoMaterno = request.getParameter("apellido_materno");
	String email = request.getParameter("email");
	String perfil = request.getParameter("perfil");
	String pregunta_secreta = request.getParameter("pregunta_secreta");
	String respuesta_secreta = request.getParameter("respuesta_secreta");

	System.out.println("perfil---> "+perfil);
	if(tipoAfiliado.equals("N")  &&   
	(  perfil.equals("OP CON")  ||  perfil.equals("OP OP")  ||  perfil.equals("FONDO LIQ")  ||   perfil.equals("SUB SEG") 
	|| perfil.equals("SUP SEG")  || perfil.equals("SUB LI CP")  || perfil.equals("SUB MICRO")   || perfil.equals("EJE PROMO") )  ) {
		claveAfiliado = perfil;
	}
	
	usr.setNombre(nombre);
	usr.setApellidoPaterno(apellidoPaterno);
	usr.setApellidoMaterno(apellidoMaterno);
	usr.setEmail(email);
	usr.setClaveAfiliado(claveAfiliado);
	usr.setTipoAfiliado(tipoAfiliado);
	usr.setPerfil(perfil);
	
	solicitud.setUsuario(usr);
	if (pregunta_secreta != null && !pregunta_secreta.equals("")) {
		solicitud.setPreguntaSecreta(pregunta_secreta);
	}
	if (respuesta_secreta != null  && !respuesta_secreta.equals("")) {
		solicitud.setRespuestaSecreta(respuesta_secreta);
	}
	
	UtilUsr utilUsr = new UtilUsr();
	String mensajeError = utilUsr.complementarSolicitudArgus(solicitud, tipoAfiliado, claveAfiliado);
	if(!mensajeError.equals("")){//Si se genera algun mensaje, debido a que algun campo de la consulta fue nulo, telefono, cp, etc
		throw new Exception(mensajeError);
	}
	return solicitud;
}
%>