<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.sql.*,
	com.netro.seguridadbean.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	netropology.utilerias.*,
	com.netro.seguridad.*"
	errorPage="/00utils/error_extjs.jsp"  
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/14seguridad/14secsession_extjs.jspf" %>  
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String txtLogin = (request.getParameter("txtLogin") != null) ? request.getParameter("txtLogin") : "";
String afiliados = (request.getParameter("afiliados")==null)?"":request.getParameter("afiliados");

String infoRegresar ="", mensaje ="";
JSONObject jsonObj = new JSONObject();

CambioPerfil clase = new  CambioPerfil();

if( informacion.equals("Consulta") ){  

	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true));
	
	HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  afiliados  );	
	
	if(catPerfil.size()>0){
	
		List perfiles = (List)catPerfil.get("PERFILES");
	
		String lblNombreEmpresa = (String)catPerfil.get("EMPRESA");
		String lblNafinElec = (String)catPerfil.get("NAELECTRONICO");
		String lblNombre = (String)catPerfil.get("NOMBRE_USUARIO");
		String lblApellidoPaterno = (String)catPerfil.get("APELLIDO_PATERNO");
		String lblApellidoMaterno = (String)catPerfil.get("APELLIDO_MATERNO");
		String lblEmail = (String)catPerfil.get("EMAIL");
		String lblPerfilActual = (String)catPerfil.get("PERFIL_ACTUAL");
		String internacional = (String)catPerfil.get("INTERNACIONAL");
		String sTipoAfiliado = (String)catPerfil.get("TIPOAFILIADO");		
			
		jsonObj.put("lblNombreEmpresa", lblNombreEmpresa);
		jsonObj.put("lblNafinElec", lblNafinElec);
		jsonObj.put("lblNombre", lblNombre);
		jsonObj.put("lblApellidoPaterno", lblApellidoPaterno);
		jsonObj.put("lblApellidoMaterno", lblApellidoMaterno);
		jsonObj.put("lblEmail", lblEmail);
		jsonObj.put("lblPerfilActual", lblPerfilActual);
		
		jsonObj.put("internacional", internacional);
		jsonObj.put("sTipoAfiliado", sTipoAfiliado);	
		jsonObj.put("txtNafinElectronico", lblNafinElec);	
		jsonObj.put("txtPerfilAnt", lblPerfilActual);	
		jsonObj.put("txtLoginC", txtLogin);
		jsonObj.put("mensaje", mensaje);			
	}else  {
		mensaje = "No se encontró el usuario con la clave especificada, por favor vuelva a intentarlo";
		jsonObj.put("mensaje", mensaje);
	}
	
	infoRegresar = jsonObj.toString();
	
} if(informacion.equals("catalogoPerfil") ){  

	HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  afiliados  );	
	List perfiles = (List)catPerfil.get("PERFILES");	
	Iterator it = perfiles.iterator();	
	HashMap datos = new HashMap();
	List registros  = new ArrayList();	
	while(it.hasNext()) {
		List campos = (List) it.next();
		String clave = (String) campos.get(0);
		datos = new HashMap();
		datos.put("clave", clave );
		datos.put("descripcion", clave );
		registros.add(datos);
	}
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();

} if(informacion.equals("modificar") ){  

	String txtPerfil = (request.getParameter("txtPerfil")==null)?"":request.getParameter("txtPerfil");
	String txtNafinElectronico = (request.getParameter("txtNafinElectronico")==null)?"":request.getParameter("txtNafinElectronico");
	String txtPerfilAnt = (request.getParameter("txtPerfilAnt")==null)?"":request.getParameter("txtPerfilAnt");
	String internacional = (request.getParameter("internacional")==null)?"":request.getParameter("internacional");
	String txtTipoAfiliado = (request.getParameter("sTipoAfiliado")==null)?"":request.getParameter("sTipoAfiliado");
	String clave_usuario = (String)session.getAttribute("Clave_usuario");
	String txtLoginC = (request.getParameter("txtLoginC") != null) ? request.getParameter("txtLoginC") : "";
	
	mensaje =  clase.getConsultaUsuario( txtLoginC,  txtPerfil, txtPerfilAnt,  txtNafinElectronico ,  internacional,  txtTipoAfiliado, 	 clave_usuario ) ;

	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("mensaje", mensaje);
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>

