<%@ page import="java.sql.*,
	com.netro.seguridadbean.SeguException,
	netropology.utilerias.*"  
	errorPage="/00utils/error.jsp" 
%>
<%@ include file="../../14secsession.jspf" %>
<html>
<head>
<title>Solicitud de Alta de Usuario</title>
<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
<Script language="JavaSCript">
function abrirPagina() {
	var f = document.forma_seleccion;
	var tipoAfiliado = f.tipo_usuario_afiliar.options[f.tipo_usuario_afiliar.selectedIndex].value;
	if (tipoAfiliado=="NAFIN") {
		f.action="nafin/solicitud_nafin.jsp?tipoAfiliado=4";
	} else if (tipoAfiliado=="BANCOMEXT") {
		f.action="nafin/solicitud_nafin.jsp?tipoAfiliado=8";
	} else if (tipoAfiliado=="EPO") {
		f.action="epo/solicitud_epo.jsp";
	} else if (tipoAfiliado=="IF") {
		f.action="if/solicitud_if.jsp";
	} else if (tipoAfiliado=="DIST") {
		f.action="dist_fondos/solicitud_dist_fondos.jsp";
	} else if (tipoAfiliado=="CONTRAGARAN") {
		f.action="contragarante/solicitud_contragarante.jsp";
	} else if (tipoAfiliado=="MANDANTE") {
		f.action="mandante/solicitud_mandante.jsp";
	} else if (tipoAfiliado=="PYME") {//FODEA 041 - 2010 ACF
		f.action="pyme/solicitud_pyme.jsp";
	} else if (tipoAfiliado=="AFIANZADORA") {//FODEA XXX - 2011 Fianza Electr�nica
		f.action="afianzadora/solicitud_afianzadora.jsp?tipoAfiliado=AFIANZADORA";
	} else if (tipoAfiliado=="FIADO") {//FODEA XXX - 2011 Fianza Electr�nica
		f.action="afianzadora/solicitud_afianzadora.jsp?tipoAfiliado=FIADO";
	} else if (tipoAfiliado=="UNIVERSIDAD") {//FODEA XXX - 2011 Fianza Electr�nica
		f.action="universidad/solicitud_universidad.jsp?tipoAfiliado=UNIVERSIDAD";
		
		
	} else {
		f.action="about:blank";
	}
	f.submit();
}

	function recarga(){
		var f = document.forms[0];
		f.action="about:blank";			
		f.submit();
	}
	
	
	function abrirPaginaMasiva() {
	var f = document.forma_seleccion;
	var tipoAfiliado = f.tipo_usuario_afiliar.options[f.tipo_usuario_afiliar.selectedIndex].value;
	if (tipoAfiliado=="NAFIN") {
		f.action="nafin/solicitud_nafinMasiva.jsp?tipoAfiliado="+tipoAfiliado;
	} else if (tipoAfiliado=="EPO") {
		f.action="nafin/solicitud_nafinMasiva.jsp?tipoAfiliado="+tipoAfiliado;
	} else if (tipoAfiliado=="IF") {
		f.action="nafin/solicitud_nafinMasiva.jsp?tipoAfiliado="+tipoAfiliado;
	} else if (tipoAfiliado=="AFIANZADORA") {
		alert("ESTA OPCION NO ESTA DISPONIBLE PARA EL ALTA DE USUARIOS DE AFIANZADORAS.");	
	} else if (tipoAfiliado=="UNIVERSIDAD") {
		alert("Esta opci�n no est� disponible para el alta de usuarios de Universidades.");	
	} else {		
		f.action="about:blank";
	}
	f.submit();
}
</Script>
</head>

<body>

<br>

<br>
<form name="forma_seleccion" method="post" target="frameTipoPantalla">

<table border="0" cellspacing="0" cellpadding="0" width="550" align="center">
<tr>
	<td class="formas" align="center">							
			<select name="tipo_usuario_afiliar" class="formas" onChange="recarga()">
				<option value="">Seleccione el tipo de usuario</option>
				<option value="NAFIN">Nafin</option>
				<option value="BANCOMEXT">Bancomext</option>
				<option value="EPO">EPO</option>
				<option value="IF">IF</option>
				<option value="DIST">Distribuidor de Fondos de Inversi&oacute;n</option>
				<option value="CONTRAGARAN">Contragarante</option>				
				<option value="MANDANTE">Mandante</option>				
				<option value="PYME">PYME</option><!--FODEA 041 - 2010 ACF-->
				<option value="AFIANZADORA">Afianzadora</option><!--FODEA XXX - 2011 Fianza Electr�nica-->
				<option value="UNIVERSIDAD">Universidad</option><!--FODEA XXX - 2011 Fianza Electr�nica-->
				</select>		
	</td>
	<td valign="top" align="right"><br>
		<table  cellpadding="0" cellspacing="0" border="0">
		<tr>
			
			<td class="celda02" height="25"  valign="top" align="RIGHT"><a target="_parent" href="seleccionExt.jsp">Nueva version</a></td>
		</tr>
		</table>	
	</td>
</tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="200"  align="center">
<tr>
<td align="middle" class="celda02" width = "120" height="24"><a href="javascript:abrirPagina();"><b>Carga Indiviual</a></td>
<td align="center">&nbsp;</td>
<td align="middle" class="celda02" width = "120" height="24"><a href="javascript:abrirPaginaMasiva();"><b>Carga Masiva</a></td>
</tr>					
</table>
	</form>		
	<br><br><br><br><br>
</body>
</html>
