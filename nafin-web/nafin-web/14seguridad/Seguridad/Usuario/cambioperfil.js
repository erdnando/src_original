
Ext.onReady(function() {


	function procesarModificar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('fpMensaje').show();		
			Ext.getCmp('fpCambio').hide();
			Ext.getCmp('forma').hide();
			Ext.getCmp('lblMensaje').setValue(info.mensaje);			
			Ext.getCmp('btnRegresar').show();	
			
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
    

	function procesarInformacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			if(info.mensaje =='') {		
				Ext.getCmp('fpCambio').show();
				Ext.getCmp('lblNombreEmpresa').update(info.lblNombreEmpresa);
				Ext.getCmp('lblNafinElec').update(info.lblNafinElec);
				Ext.getCmp('lblNombre').update(info.lblNombre);
				Ext.getCmp('lblApellidoPaterno').update(info.lblApellidoPaterno);
				Ext.getCmp('lblApellidoMaterno').update(info.lblApellidoMaterno);
				Ext.getCmp('lblEmail').update(info.lblEmail);
				Ext.getCmp('lblPerfilActual').update(info.lblPerfilActual);
				Ext.getCmp('txtPerfil_1').setValue(info.txtPerfilAnt);
				
				Ext.getCmp('internacional').setValue(info.internacional);	
				Ext.getCmp('sTipoAfiliado').setValue(info.sTipoAfiliado);	
				Ext.getCmp('txtNafinElectronico').setValue(info.txtNafinElectronico);	
				Ext.getCmp('txtPerfilAnt').setValue(info.txtPerfilAnt);	
				Ext.getCmp('txtLoginC').setValue(info.txtLoginC);					
				
				Ext.getCmp('fpMensaje').hide();	
				
				catalogoPerfil.load({
					params: Ext.apply(fp.getForm().getValues(),{  })  
				});
			}else  {
				Ext.getCmp('fpMensaje').show();		
				Ext.getCmp('fpCambio').hide();
				Ext.getCmp('lblMensaje').setValue(info.mensaje);	
			}
			
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
  
  
  //****** Forma para ver el mensaje  cuando  le usuario no se encontro**********************
  	var fpMensaje = new Ext.form.FormPanel({
		id: 'fpMensaje',
		width: 500,
		title: '',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 10,
		defaultType: 'textfield',
		hidden:true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'displayfield',
				id: 'lblMensaje',
				style: 'text-align:center;',
				fieldLabel: ''				
			}
		],			
		monitorValid: true,
		buttons: [				
			{
				text: 'Regresar',
				id: 'btnRegresar',
				iconCls: 'icoLimpiar',
				hidden:true,
				handler: function() {
					window.location = 'cambioperfilExt.jsp';					
				}
			}
		]
	});
	
	
//***********************FORMA PARA VISUALIZAR LOS DATOS DEL USUARIOS *******************************

	var catalogoPerfil = new Ext.data.JsonStore({
		id: 'catalogoPerfil',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: 'cambioperfil.data.jsp',
		baseParams: {
			informacion: 'catalogoPerfil'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {				
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var  elementosFormaCambios =  [
		{ 	xtype: 'textfield',  hidden: true, id: 'internacional', 	value: '' },		
		{ 	xtype: 'textfield',  hidden: true, id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'txtNafinElectronico', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'txtPerfilAnt', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'txtLoginC', 	value: '' },		
		{
			xtype: 'displayfield',
			id: 'lblNombreEmpresa',
			style: 'text-align:left;',
			fieldLabel: 'Empresa',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblNafinElec',
			style: 'text-align:left;',
			fieldLabel: 'Nafin Electr�nico',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblNombre',
			style: 'text-align:left;',
			fieldLabel: 'Nombre',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblApellidoPaterno',
			style: 'text-align:left;',
			fieldLabel: 'Apellido Paterno',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblApellidoMaterno',
			style: 'text-align:left;',
			fieldLabel: 'Apellido Materno',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblEmail',
			style: 'text-align:left;',
			fieldLabel: 'Email',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblPerfilActual',
			style: 'text-align:left;',
			fieldLabel: 'Perfil Actual',
			text: '-'
		},
		{
			xtype: 'combo',
			fieldLabel: 'Perfil',
			name: 'txtPerfil',
			id: 'txtPerfil_1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'txtPerfil',
			emptyText: 'Seleccionar...',			
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,			
			minChars: 1,
			store: catalogoPerfil,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120			
		}
	];
	
	var fpCambio = new Ext.form.FormPanel({
		id: 'fpCambio',
		width: 500,
		title: '',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		hidden:true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaCambios,			
		monitorValid: true,
		buttons: [		
			{
				text: 'Modificar',
				id: 'btnModificar',
				iconCls: 'modificar',				
				formBind: true,				
				handler: function(boton, evento) {	
								
					Ext.Ajax.request({
						url: 'cambioperfil.data.jsp',
						params: Ext.apply(fpCambio.getForm().getValues(),{
							informacion: 'modificar'							
						}),
						callback:procesarModificar
					});
					
				}
			},
			{
				text: 'Salir',
				id: 'btnSalir',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = 'cambioperfilExt.jsp';					
				}
			}
		]
	});
	

	
	//***********************FORMA BUSCAR*******************************+
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Cambio de Perfil',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
			{
				xtype: 'compositefield',
				fieldLabel: 'Clave de Usuario',
				combineErrors: false,
				msgTarget: 'side',
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Clave de Usuario ',		
						name: 'txtLogin',
						id: 'txtLogin1',
						allowBlank: true,
						maxLength: 8,
						width: 150,
						msgTarget: 'side',
						margins: '0 20 0 0'
					}
				]
			}
		],				
		monitorValid: true,
			buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {				
				
					var txtLogin = Ext.getCmp('txtLogin1');
					if (Ext.isEmpty(txtLogin.getValue()) ){
						txtLogin.markInvalid('"Debe especificar un valor a la clave de usuario para realizar la b�squeda');
						return;
					}
					
					Ext.Ajax.request({
						url: 'cambioperfil.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consulta'				
						}),
						callback:procesarInformacion 
					});
					
				}	
			}
		]
	});
		
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,			
			fpCambio, 
			fpMensaje,
			NE.util.getEspaciador(20)			
		]
	});
	
});	
