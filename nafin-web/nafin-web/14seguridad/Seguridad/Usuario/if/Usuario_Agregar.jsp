<!--
/***************************************************************************************
*
*                /////  Pragrama:    DECIMALES                     /////
*                /////  Funci�n:     Entrada de datos              /////
*                /////                                             /////
*                /////                                             /////
*                /////  Autor:       Manuel Ramos Mendoza          /////
*                /////  Realizado:   20/Enero  /2001               /////
*                /////  Ult. Modif:  30/Abril  /2001               /////
*
****************************************************************************************/
-->
<%@ page import="netropology.utilerias.*,java.sql.*,com.netro.seguridadbean.SeguException,java.lang.*"%>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  <style type="text/css">
  .fmargin{background:#ffffff; font-family: courier, "courier new", monospace; ; color:#000066; border-style:solid;}
  </style>
</head>


<%
String  sSql          = null,
	sClave        = null,
	sDescripcion  = null;
			
	/*javax.naming.Context naming = new javax.naming.InitialContext();
	javax.sql.DataSource ds = (javax.sql.DataSource) naming.lookup("seguridadData");
	Connection conn = ds.getConnection();*/
AccesoDB con = new AccesoDB ();
%>


<Script Language="JavaScript">
   var gsNoUsuario = parseInt('<%=session.getAttribute("sesiNoUsuario")%>');
   var gsNoCliente = parseInt('<%=session.getAttribute("iNoCliente")%>');
   var gbIsNS4 = (document.layers ? true : false);
</Script>



<%

  try
  {
%>

<LINK REL="stylesheet" HREF="../../css/formato.css" TYPE="text/css">
<Script Language="JavaSCript" src="../../css/colores.js">  </Script>
<Script Language="Javascript" src="../../scripts/Validaciones.js">  </Script>
<Script Language="Javascript" src="../../scripts/conversiones.js">  </Script>
<Script Language="Javascript" src="../../scripts/JsCanvas.js">      </Script>


<body  >
<FORM Name="Ceil" >
<Script Language="JavaScript">
   var Documento = window.document.Ceil;
   var isIE = (document.all ? true : false);
   var sOnBlur = "";
   var sSizeText = "";

  /**************************************************************************
  *    function mtdFormateaCombo(iMaxlen, sDato1, sDato2)
  *    FORMATEA LOS DATOS DE UN COMBO PASANDO COMO DATOS DOS CADENAS Y
  *           LA LONGITUD MAXIMA DE LA PRIMERA CADENA.
  **************************************************************************/
  function mtdFormateaCombo(iMaxlen, sDato1, sDato2)
  {
     var LONGITUDAD = 4;
     var paso = "&nbsp;&nbsp;";
     var longDato1 = sDato1.length;

     document.write(sDato1);
     for (var i=1; i<=(iMaxlen-longDato1)+LONGITUDAD; i++)
     { document.write("<font face=verdana>" + paso + "</font>");  }
     document.write(sDato2);
  }
</Script>



<%
////  SI ES NAFIN
if ( session.getAttribute("sesiNoUsuario").equals("4"))
{
%>
  <br>

  <!-- -------------------  COMBO IF --------------------------->
<table width="610" cellpadding="2" cellspacing="1" border="0">
<tr>
	<td align="center">
    <table border="0" cellspacing="0" cellpadding="0" align="center" width="250">
      <tr>
        <td width="50%">
           <font class="EtiketaUsr">
              <b>IF: </b>
           </font>
        </td>

        <td width="50%">
          <select Style="Font-Size:10px"  name="lstIF" >
			<option value="" selected> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </option>
<%
		try{
			con.conexionDB();
			sSql =  "SELECT ic_if, cg_razon_social FROM comcat_if  ORDER BY cg_razon_social";
			
			ResultSet resultSet = con.queryDB(sSql);
			
			while(resultSet.next()){
				//sClave        = String.valueOf((Integer) resultSet.getString(1));
				sClave        = resultSet.getString(1);
				sDescripcion  = resultSet.getString(2).trim();
%>
			<option value="<%=sClave%>" > <%=sDescripcion%> </option>
<%
			}
			resultSet.close();
			con.cierraStatement();
		}
		catch(Exception e){
			System.out.println("Error:" + e);
			e.printStackTrace();
			out.print("Error = " + e);
%>
			<Script Language="JavaScript">
				alert("Error de conexion:  <%=e%>");
			</Script>
		<%}%>
		</select>
        </td>
      </tr>
    </table>
	</td>
</tr>
</table>

  <br>


<%
 }
%>



    <!-- ------------------------ INCLUYENDO  PARTE DE CAPTURA  GENERICA  ------------------- -->
    <%@ include file="../Usuario_Campura.jsp" %>






  <br><br>

<table width="610" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td align="center">
    <Script Language="JavaScript">

       var cvnRegistro = new JSCanvas("cvnRegistro");
       cvnRegistro.mtdPDibuja(600, 30, 1, "#A5B8BF");

    </Script></td>
</tr>
</table>
    <br>

    <iframe name="frmGraba" width="440px" height="00px" bgcolor="#3366CC" hspace=0 vspace=0
         src=""  scrolling="yes" marginwidth=0 marginheight=0 frameborder=0>
		 <ilayer name="frmGraba" src="../vacio.html" width="1" height="1" bgcolor="blue"  LEFT=250 TOP=350  > </ilayer>
    </iframe>
  </center>


                 <!-- ///////////////////////////   SCRIPTs  /////////////////////////////////  -->
 <Script Language="JavaScript">

  var sFchOp = "<%=session.getAttribute("sesFchOper")%>";


  /*****************************************************************
  *   INVIERTE LA POSICION DE LAS FECHAS
  *****************************************************************/

  function fnInvierteFecha ( ls_Fecha )
  {
	  var ls_fecha_tmp = ls_Fecha.split("/");
	  ls_Fecha = ls_fecha_tmp[2] + "/" + ls_fecha_tmp[1] + "/" + ls_fecha_tmp[0];
	  return ls_Fecha;
  }


  /***************************************************************************
  *     function fnModificar()
  ***************************************************************************/

  function fnModificar()
  {

      if (gsNoUsuario == 2)
  	    var liCveIF = gsNoCliente;
	 else
        var liCveIF = window.document.Ceil.lstIF.value;


      var liNoUsuario = 2;
      var lsUsuario = "IF";

      with (window.document.Ceil)
      {

	      var Nombre = txtNombre.value;
	      var APat   = txtAPaterno.value;
	      var AMat   = txtAMaterno.value;

	      var FchVen = fnInvierteFecha(txtFchVencimiento.value);
	      FchVen = mtdValFchDaniel(FchVen, "/");

	      var Correo = txtMail.value;
	      var Estat  = hidEstatus.value;

		  if ( Nombre == ""  ||  liCveIF == "")
		  {
		      alert("Te hacen falta datos.");
			  if (liCveIF == "" ) lstIF.focus();
			  else txtNombre.focus();
			  return;
		  }

	      var bError = false;
	      var sMensaje = "Esta Seguro de Agregar el registro?";


	      if (FchVen == FORMATO_FECHA )
	      {
		    txtFchVencimiento.value = "";
		    FchVen="";
	      }


	      if ( ( Nombre == ""   ||  FchVen == ""  ||  APat  == "" ||  AMat  == "" )  )
		  alert("Hacen falta campos por llenar.")
	      else
	      {
		    var Parametros = "../Usuario_Graba.jsp?";
		    Parametros += "Nombre=" + Nombre;
		    Parametros += "&APaterno="  + APat;
		    Parametros += "&AMaterno=" + AMat;
		    Parametros += "&Correo="  + Correo;
		    Parametros += "&Estatus=" + Estat;
		    Parametros += "&FchVent=" + FchVen;
		    Parametros += "&Usuario=" + lsUsuario;
		    Parametros += "&NoUsuario=" + liNoUsuario;

		    Parametros += "&CveEPO=0";
	        Parametros += "&CvePYME=0";
	        Parametros += "&CveIF=" + liCveIF;
	        Parametros += "&Afiliacion=";

		    Parametros += "&TipoOp=Agregar";

		    if (confirm(sMensaje))
		    {
		       mtdBorra(0);
		       self.frmGraba.location.replace(Parametros);
		    }
		    else
		    {
		     /*  window.document.Ceil.txtClaveUsuario.focus();
		       window.document.Ceil.txtClaveUsuario.select();*/
		    }
	      } /// else de contrase�a
    } // with

  }




  ///////////////////////////// hidden de Bloqueo  //////////////////////////////
  function mtdAsingBloq(Dato)
  {   window.document.Ceil.hidEstatus.value = Dato;  }









  /************************************************************************
  *  Pasa DATOS a Reporte
  ************************************************************************/
  var winRepUsr;

  function fnRep(Cve, Nom, APat, AMat, Edo, Mail, FchVen, Afiliacion, CveIF, CveEPO, CvePYME, CveUsNafin)
  {

	 if(typeof(winRepUsr) != 'undefined')
	   winRepUsr.close();

	 var lsPagina = "../consultar/Usuario_Perfil00.jsp?parClave=" + Cve + "&parNombre=" + Nom;
	 lsPagina += "&parAPaterno=" + APat + "&parAMaterno=" + AMat + "&parEdo=" + Edo + "&parMail=" + Mail;
	 lsPagina += "&parFchVen=" + FchVen + "&parCvePYME=" + CvePYME + "&parAfiliacion=" + Afiliacion;
	 lsPagina += "&parCveIF=" + CveIF + "&parCveEPO=" + CveEPO + "&parCveUsNafin=" + CveUsNafin;

	 //alert(lsPagina);

	 window.open(lsPagina,  "winUsrPerf", "ToolBar=No,Status=No,Location=No,MenuBar=No,ScrollBars=No,Resizable=No,Directories=No,Fullscreen=No,left=200,ChannelMode=No,Width=460,Height=460,resizable=False");

  }





</Script>





  <%
  ///////////////////////   Cierra Conexion   //////////////////

    }

    catch (Exception e)
    {
       System.out.println("Error:" + e);
       e.printStackTrace();
       out.print("Error = " + e);
    }
	finally {
		con.cierraConexionDB();
	}
%>
</FORM>
</body>
</html>



