<%@ page import="netropology.utilerias.*
	, netropology.utilerias.usuarios.*
	, java.util.*
	, java.sql.*
	, javax.naming.*" 
		errorPage="/00utils/error.jsp"%>
<%@ include file="/14seguridad/14secsession.jspf" %>
<%
netropology.utilerias.usuarios.Usuario oUsuario = null;
UtilUsr oUtilUsr = null;
boolean exito = true;

String enviar = (request.getParameter("enviar")==null)?"":request.getParameter("enviar");
String afiliados = (request.getParameter("afiliados")==null)?"":request.getParameter("afiliados");
String txtLogin = (request.getParameter("txtLogin2")==null)?"":request.getParameter("txtLogin2");
String txtPerfil = (request.getParameter("txtPerfil")==null)?"":request.getParameter("txtPerfil");
String txtPerfilAnt = (request.getParameter("txtPerfilAnt")==null)?"":request.getParameter("txtPerfilAnt");
String txtNafinElectronico = (request.getParameter("txtNafinElectronico")==null)?"":request.getParameter("txtNafinElectronico");
String txtTipoAfiliado = (request.getParameter("txtTipoAfiliado")==null)?"":request.getParameter("txtTipoAfiliado");
String claveAfiliado = (request.getParameter("claveAfiliado")==null)?"":request.getParameter("claveAfiliado");
String internacional = (request.getParameter("internacional")==null)?"":request.getParameter("internacional");

if ("".equals(txtPerfil) || "".equals(txtLogin) || "".equals(enviar)) 
				throw new Exception("No se recibieron todos los parametros");
String[] nombres = {
	"Perfil"
};
AccesoDB con = new AccesoDB();
List datosAnteriores = new ArrayList();
List datosActuales = new ArrayList();
StringBuffer valoresAnteriores = new StringBuffer();
StringBuffer valoresActuales = new StringBuffer();

%>

<html>
<head>
<title>Cambio de Perfil</title>
<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">

</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="550" align="left">
<%if ("S".equals(afiliados)) {%>
<tr>
	<td class="formas" align="center">
		<table border="0" cellspacing="0" cellpadding="0" width="150" align="center">
		<tr>
			<td class="titulos" align="center"> 	<b>CAMBIO DE PERFIL</b> <br><br></td>
		</tr>
		</table>
	
	</td>
</tr>
<%}%>
<tr>
	<td class="formas" align="center">&nbsp;</td>
</tr>
<tr>
	<td class="formas" align="center"><br><br><br><br>
	
<%
	if ("S".equals(enviar)) {
		try { 
			con.conexionDB();
			oUtilUsr = new UtilUsr();
			oUtilUsr.setPerfilUsuario(txtLogin, txtPerfil);

			datosAnteriores.add(0, txtLogin + " - " + txtPerfilAnt);
			datosActuales.add(0, txtLogin + " - " + txtPerfil);
			List cambios = Bitacora.getCambiosBitacora(datosAnteriores, datosActuales, nombres);
			valoresAnteriores.append(cambios.get(0));
			valoresActuales.append(cambios.get(1));
			
			Bitacora.grabarEnBitacora(con, "SEGCAMBIOPERFIL", txtTipoAfiliado+internacional,
					txtNafinElectronico, (String)session.getAttribute("Clave_usuario"),
					valoresAnteriores.toString(), valoresActuales.toString());
	
%>			
		<table border="1" cellspacing="0" cellpadding="0" align="center" width="500">
			<tr>
		   		<td class="celda01"  align="center"><b>El cambio de perfil fue realizado con &eacute;xito.</b></td>
	        </tr>
		</table><br>
		<table cellpadding="0" cellspacing="2" border="0" align="center">
		   <tr>
			  <%if ("S".equals(afiliados)) {%>
			  <td valign="middle" align="center" class="celda02" width="55" height="20"><a href="/nafin/15cadenas/15mantenimiento/15clientes/15forma5_cuentas.jsp?tipoAfiliado=<%=txtTipoAfiliado%>&claveAfiliado=<%=claveAfiliado%>">Regresar</a></td>
			  <td valign="middle" align="center" class="celda02" width="55" height="20"><a href="javascript:window.close()">Cerrar</a></td>
			  <%} else if ("A".equals(afiliados)) {%>
				 <td valign="middle" align="center" class="celda02" width="55" height="20"><a href="/nafin/15cadenas/15mantenimiento/15clientes/15Afianzadora_cuentas.jsp?tipoAfiliado=<%=txtTipoAfiliado%>&claveAfiliado=<%=claveAfiliado%>">Regresar</a></td>
			  <td valign="middle" align="center" class="celda02" width="55" height="20"><a href="javascript:window.close()">Cerrar</a></td>
				<%}else{%>
			  <td valign="middle" align="center" class="celda02" width="55" height="20"><a href="cambioperfil.jsp">Regresar</a></td>
			  <%}%>
		   </tr>
		</table>
		
<%
		} catch(Exception e) {
			exito = false;
			throw e;
		} finally {
			if (con.hayConexionAbierta() == true)
			con.terminaTransaccion(exito);
			con.cierraConexionDB();
		}

	}
%>			

	</td>
</tr>
</table>
</body>
</html>
