<!--
/***************************************************************************************
*
*                /////  Pragrama:    NAFIN                         /////
*                /////  Funci�n:     Entrada de datos              /////
*                /////                                             /////
*                /////                                             /////
*                /////  Autor:       Manuel Ramos Mendoza          /////
*                /////  Mod:         Ricardo Jimenez Vargas        /////
*                /////  Realizado:   20/Enero  /2001               /////
*                /////  Ult. Modif:  26/Febrero/2003               /////
*
****************************************************************************************/
-->
<%@ page import="java.sql.*,com.netro.seguridadbean.SeguException,java.lang.*"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<style type="text/css">
		.fmargin{background:#ffffff; font-family: courier, "courier new", monospace; ; color:#000066; border-style:solid;}
	</style>
</head>
<%
String  sSql      = null,
	sClave        = null,
	sDescripcion  = null;
%>
<Script Language="JavaScript">
	var gbIsNS4 = (document.layers ? true : false);
</Script>
<%try{%>
<Script Language="Javascript" src="../../scripts/Validaciones.js">  </Script>
<Script Language="Javascript" src="../../scripts/conversiones.js">  </Script>
<LINK REL="stylesheet" HREF="../../css/formato.css" TYPE="text/css">
<Script Language="JavaSCript" src="../../css/colores.js">  </Script>

<body  OnLoad="Ceil.txtNombre.focus();"> 
<FORM Name=Ceil >
<Script Language="JavaScript">
var Documento = window.document.Ceil;
var isIE = (document.all ? true : false);
var sOnBlur = "";
var sSizeText = "";

/**************************************************************************
*    function mtdFormateaCombo(iMaxlen, sDato1, sDato2)
*    FORMATEA LOS DATOS DE UN COMBO PASANDO COMO DATOS DOS CADENAS Y 
*           LA LONGITUD MAXIMA DE LA PRIMERA CADENA. 
**************************************************************************/
function mtdFormateaCombo(iMaxlen, sDato1, sDato2){
	var LONGITUDAD = 4;
	var paso = "&nbsp;&nbsp;";
	var longDato1 = sDato1.length;
	document.write(sDato1);
	for(var i=1; i<=(iMaxlen-longDato1)+LONGITUDAD; i++){
		document.write("<font face=verdana>" + paso + "</font>");
	}
	document.write(sDato2);
}
</Script>
<br>
<table width="610" cellpadding="2" cellspacing="1" border="0">
	<tr>
		<td align="center">
			<table border="0" cellspacing="0" cellpadding="4" width="620" align="center">
				<tr>
					<td > <font class="EtiketaUsr"> <a target='_top' title='Nombre del Usuario'>Nombre</a> </font> </td>
					<td width="250"> 
						<Script Language="JavaScript">
							if(isIE)
								sSizeText = "25";
							else
								sSizeText = "20";
							sOnBlur = "onBlur='mtdValida(txtNombre.name,";
							sOnBlur += '"Mayusculas"';
							sOnBlur += ");'";
							window.document.write('<input type="text" size="'+sSizeText+'" ');
							window.document.write('name="txtNombre" maxlength="40"  tabindex="1" '); 
							window.document.write(sOnBlur + ' required="Yes" ');
							window.document.write('style="FONT-FAMILY: Verdana; FONT-SIZE: 10px;" ');
							window.document.write('OnKeyPress="if(event.keyCode==13) {mtdValida(txtNombre.name,\'Mayusculas\'); mtdConsultar();}">');
						</Script>
					</td>
					<td ><font class="EtiketaUsr"> <a target='_top' title='Apellido Paterno'>Paterno</a> </font> </td>
					<td width="182"> 
						<Script Language="JavaScript">
							if(isIE)
								sSizeText = "25";
							else
								sSizeText = "20";
							sOnBlur = "onBlur='mtdValida(this.name,";
							sOnBlur += '"Mayusculas"';
							sOnBlur += ");'";
							window.document.write('<input type="text" size="'+sSizeText+'" ');
							window.document.write('name="txtAPaterno" maxlength="40"  tabindex="2" '); 
							window.document.write(sOnBlur + ' required="Yes" ');
							window.document.write('style="FONT-FAMILY: Verdana; FONT-SIZE: 10px;" ');
							window.document.write('OnKeyPress="if(event.keyCode==13) {mtdValida(txtAPaterno.name,\'Mayusculas\'); mtdConsultar();}">');
						</Script>
					</td>
					<td width="78" height="10" align="left">
						<table cellpadding="3" cellspacing="1" border="0">
							<tr>
								<td class="celda02" width="50" height="21" align="center"><a href="#"   onClick="mtdConsultar();"   onBlur="Ceil.txtNombre.focus();" tabindex="15" >Buscar</a></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="10" valign="middle">
						<font class="EtiketaUsr"><a target='_top' title='Apellido Materno'>Materno</a> </font>
					</td>
					<td width="250" height="10"> 
						<Script Language="JavaScript">
							if (isIE)
								sSizeText = "25";
							else
								sSizeText = "20";
							sOnBlur = "onBlur='mtdValida(this.name,";
							sOnBlur += '"Mayusculas"';
							sOnBlur += ");'";
							window.document.write('<input type="text" size="'+sSizeText+'" ');
							window.document.write('name="txtAMaterno" maxlength="40"  tabindex="3" '); 
							window.document.write(sOnBlur + ' required="Yes" ');
							window.document.write('style="FONT-FAMILY: Verdana; FONT-SIZE: 10px;" ');
							window.document.write('OnKeyPress="if(event.keyCode==13) {mtdValida(txtAMaterno.name,\'Mayusculas\'); mtdConsultar();} ">');
						</Script>
					</td>
					<td height="10" valign="middle">
						<font class="EtiketaUsr"><a target='_top' title='Clave de Usuario'>Usuario</a></font>
					</td>
					<td width="182" height="10">
						<font face="Verdana, Arial, Helvetica, sans-serif">
						<input type="text" name="txtClaveUsuario" size="10" maxlength="8"  tabindex="4" 
							required="Yes" style="FONT-FAMILY: Verdana; FONT-SIZE: 10px; "
							OnKeyPress="if(event.keyCode==13) mtdConsultar();">
						</font>
					</td>
					<td width="78" height="10" align="left">
						&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!-- ///////////////////////////   SCRIPTs  /////////////////////////////////  -->
<Script Language="JavaScript">
/********************************************************************
* function mtdConsultar()
********************************************************************/
function mtdConsultar(){
	var lsNombre  = fnTrim(window.document.Ceil.txtNombre.value);
	var lsPaterno = fnTrim(window.document.Ceil.txtAPaterno.value);
	var lsMaterno = fnTrim(window.document.Ceil.txtAMaterno.value);
	var lsClave   = fnTrim(window.document.Ceil.txtClaveUsuario.value);

	var Parametros = "Usuario_Floor.jsp?Usuario=" + lsClave;
	Parametros += "&Nombre=" + lsNombre;
	Parametros += "&APaterno="  + lsPaterno;
	Parametros += "&AMaterno=" + lsMaterno;
	Parametros += "&TipoOp=Consultar"; 

	parent.frmCosultar_2.location.href = Parametros;
}
</Script>
<%
}
catch(Exception e){
	System.out.println("Error:" + e.toString());
	e.printStackTrace();
	out.print("Error = " + e.toString());
}
%>
</FORM>
</body>
</html>
