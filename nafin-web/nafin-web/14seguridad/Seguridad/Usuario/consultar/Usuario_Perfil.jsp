<!--
/***************************************************************************************
*
*                /////  Pragrama:    NAFIN                         /////
*                /////  Funci�n:     Entrada de datos              /////
*                /////                                             /////
*                /////                                             /////
*                /////  Autor:       Manuel Ramos Mendoza          /////
*                /////  Realizado:   20/Enero     /2001            /////
*                /////  Ult. Modif:  01/Septiembre/2002            /////
*
****************************************************************************************/
-->

<%
     String m_sClave,
            m_sNombre,
            m_sAPaterno,
            m_sAMaterno,
            m_sEdo,
            m_sMail,
            m_sFchVen,
	        m_sCvePYME,
    	    m_sAfiliacion,
	        m_sCveIF,
	        m_sCveEPO,
    	    m_sCveUsNafin,
            m_sNumSIRAC,
			
            
            m_sInactivo  = "",
            m_sActivo    = "";

     int    m_sNumTipoUsuario = 0;    //  NAFIN = 4    EPO = 1 

%>



<!--<LINK REL="stylesheet" HREF="../../css/<%=session.getAttribute("iNoCliente")%>.css" TYPE="text/css">-->
<LINK REL="stylesheet" HREF="../../css/formato.css" TYPE="text/css">  
  
<Script Language="JavaSCript" src="../../css/colores.js">  </Script>
<Script Language="Javascript" src="../../scripts/Validaciones.js">  </Script>
<Script Language="Javascript" src="../../scripts/conversiones.js">  </Script>


<head>
  <title>  Perfiles de Usuario  </title>
</head>

<body>

<form name="frmDatos">




<%

     m_sClave      = request.getParameter("parClave");
     m_sNombre     = request.getParameter("parNombre");
     m_sAPaterno   = request.getParameter("parAPaterno");
     m_sAMaterno   = request.getParameter("parAMaterno");
     m_sEdo        = request.getParameter("parEdo");
     m_sMail       = request.getParameter("parMail");
     m_sFchVen     = request.getParameter("parFchVen");
     m_sCvePYME    = request.getParameter("parCvePYME");
     m_sAfiliacion = request.getParameter("parAfiliacion");
     m_sCveEPO     = request.getParameter("parCveEPO");
     m_sCveIF      = request.getParameter("parCveIF");
     m_sCveUsNafin = request.getParameter("parCveUsNafin");
     m_sNumSIRAC   = request.getParameter("parNumSIRAC");
	 
	 if (m_sNumSIRAC == null)	   m_sNumSIRAC = "";

//    strTipoUsuario = (String) session.getAttribute("strTipoUsuario");

     if (!m_sCveIF.equals("0"))                                // es if
        m_sNumTipoUsuario = 2;

     else if(!m_sCvePYME.equals("0") )   // es pyme	 
        m_sNumTipoUsuario = 3;

     else if(m_sCvePYME.equals("0")   &&   !m_sCveEPO.equals("0"))   // es epo
        m_sNumTipoUsuario = 1;

     else
        m_sNumTipoUsuario = 4;                            // pues es nafin


     if (m_sEdo.equals("A"))
	 {
        m_sActivo   = "checked";
		m_sInactivo = "";
	 }
     else 
	 {
       m_sInactivo = "checked";
	   m_sActivo   = "";
     }
%>


<html>



<input type="hidden"  name="hddCveUsNafin"  value="<%=m_sCveUsNafin%>">


<table border="1" cellspacing="0" cellpadding="3" align="center" width="400" bordercolor="#A5B8BF">
  <tr>
     <td class="celda01" width="100"><b>Nombre:</b> </td>
    
     <td class="formas" width="288">
       <input type="text" name="txtNombre" size="48"  
       	  value="<%=m_sNombre%>"
	  onBlur='mtdValida(txtNombre.name, "");'
          STYLE="font-size:10px; " >
     </td>
   </tr>

  <tr>
     <td class="celda01" width="100"><b>Paterno:</b> </td>
    
     <td class="formas" width="288">	 
       <input type="text" name="txtAPaterno" size="48"  
       	  value="<%=m_sAPaterno%>"
          STYLE="font-size:10px; " >
     </td>
   </tr>

  <tr>
     <td class="celda01" width="100"><b>Materno:</b> </td>
    
     <td class="formas" width="288">	 
       <input type="text" name="txtMaterno" size="48"  
       	  value="<%=m_sAMaterno%>"
          STYLE="font-size:10px;" >
     </td>
   </tr>


   <tr>
    <td class="celda01"><b>Clave Usuario:</b></td>
    <td class="formas"> <%=m_sClave%> </td>  
  </tr>

  <%if (m_sNumTipoUsuario == 4) {%>
  <tr>
    <td class="celda01"><b>Num SIRAC:</b></td>
    <td class="formas"> 
       <input type="text" name="txtNumSIRAC" size="22"  
       	  value="<%=m_sNumSIRAC%>"   MaxLength="20"
          STYLE="font-size:10px;" >
    </td>
  </tr>
  <%}%>
  
    
  <tr>
    <td class="celda01"><b>Mail:</b></td>
    <td class="formas"> 
       <input type="text" name="txtCorreo" size="48"  
       	  value="<%=m_sMail%>"   
          STYLE="font-size:10px;" >
    </td>
  </tr>

  <tr>
    <td class="celda01"><b>Estatus:</b></td>
    <td class="formas">
      A<input type="radio" name="radEstado"    <%=m_sActivo%>  >
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      I<input type="radio"   name="radEstado"  <%=m_sInactivo%>>
    </td>
  </tr>

  <tr>
    <td class="celda01"><b>Vigencia:</b></td>
    <td class="formas">		 
       <input type="text" name="txtFchVencimiento" size="14"  
          OnFocus="txtFchVencimiento.select();"
          OnBlur="ValidaFecha(txtFchVencimiento.name, txtFchVencimiento.value);"
       	  value="<%=m_sFchVen%>"  maxlength="10"
          STYLE="font-size:10px; " >
		  
<!-- font:normal xx-small Verdana; background-bolor:#FFFFFF ;color:Verdana;
               BACKGROUND-ATTACHMENT: fixed; BACKGROUND-COLOR:#FFFFFF; BORDER-BOTTOM: thick;
               BORDER-LEFT-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-TOP-STYLE: none;
			   -->		  
    </td>
  </tr>

</table>







  <!-- ///////////////////////////////////   Cabecera /////////////////////////////////////--> 
  <table border="0" cellspacing="3" cellpadding="0" align="center" width="400" bordercolor="#A5B8BF">
    <tr> 
      <td class="formas"> 
        <div align="right">
           <script language="JavaScript">   
	          document.write('<input type="button" name="btnModificar" value="Modificar" ');
			  
			  if ( <%=m_sNumTipoUsuario%> == 4)
			    document.write('OnClick="mtdModificar(\'<%=m_sClave%>\', txtNombre.value, txtAPaterno.value, txtMaterno.value, txtCorreo.value, txtFchVencimiento.value, txtNumSIRAC.value);" ');
			  else
	            document.write('OnClick="mtdModificar(\'<%=m_sClave%>\', txtNombre.value, txtAPaterno.value, txtMaterno.value, txtCorreo.value, txtFchVencimiento.value);" ');
	          document.write('STYLE="font-size:10px;font:bold xx-small Verdana; background-bolor:'+ColorBtn+';color:'+LetraBtn+';');
	          document.write('BACKGROUND-ATTACHMENT: fixed; BACKGROUND-COLOR:'+ColorBtn+'; BORDER-BOTTOM: thick;');
	          document.write('BORDER-LEFT-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-TOP-STYLE: none;" >');
           </script>
        </div>
      </td>
    </tr>
  </table>
  




    <iframe name="frmGrabar" width="0px" height="0px" bgcolor="#3366CC" hspace=0 vspace=0
         src=""  scrolling="yes" marginwidth=0 marginheight=0 frameborder=0>
		 <ilayer name="frmGraba" src="../vacio.html" width="1" height="1" bgcolor="blue"  LEFT=250 TOP=350  > </ilayer>
    </iframe>



















<Script Language="JavaScript">


 var gbIsNS4 = (document.layers ? true : false);

/**************************************************************************************************
*                        ValidaFecha(txtFchVencimiento.value)
***************************************************************************************************/
  function ValidaFecha(sNombre, sDato)
  {
     sDato = mtdValFchDaniel(sDato ,"/");

     window.document.forms[0].elements[sNombre].value = sDato;
  }



/**************************************************************************************************
*                              Valida ke sea fecha valida
***************************************************************************************************/

  function mtdValFchDaniel(Dato ,Separador)  
  {
    var VFch = Dato.split(Separador);
    if ((VFch.length == 3) && (VFch[0] > ANOMIN)  && (VFch[0] < ANOMAX) && (VFch[1] <= MESES) && (VFch[2] <= MAXDIAS))
    {
       var FechaValida = new Date();
       FechaValida.setFullYear(VFch[0]);
       FechaValida.setMonth(VFch[1]-1);
       FechaValida.setDate(VFch[2]);
       Dato  = FechaValida.getFullYear();
       if ( FechaValida.getMonth() < 9)
	      Dato += Separador + "0" + ( parseInt(FechaValida.getMonth()) + 1 );
	   else	  
		  Dato += Separador + ( parseInt(FechaValida.getMonth()) + 1 );
		  
       if(FechaValida.getDate() < 10)
	     Dato += Separador + "0" + FechaValida.getDate();
	   else
	   	   Dato += Separador + FechaValida.getDate();
       return Dato;
    }
    else   {  return "aaaa"+Separador+"mm"+Separador+"dd"; }
  }
  



  /****************************************************************************************
  *   mtdModificar(lsClave, lsNombre, lsAPaterno, lsMaterno, lsCorreo, lsFchVencimiento, lsNumSIRAC)
  ****************************************************************************************/

  function mtdModificar(lsClave, lsNombre, lsAPaterno, lsMaterno, lsCorreo, lsFchVencimiento, lsNumSIRAC)
  {
	 if (  lsFchVencimiento == "aaaa/mm/dd" )
	 {  alert("La fecha no es valida");
	    return;
	 }
	 else
	 lsFchVencimiento = fnSustituyeCaracter(lsFchVencimiento ,"/", "-");
	 
	 var lsEstatus  = (window.document.frmDatos.radEstado[0].checked ? "A" : "I");
     var lsCveUsNafin = window.document.frmDatos.hddCveUsNafin.value;

     var lsDatosURL = "../Usuario_Graba.jsp?Usuario="+lsClave+"&Nombre="+lsNombre + "&APaterno="+lsAPaterno +"&Estatus="+lsEstatus
     lsDatosURL += "&AMaterno="+lsMaterno + "&Correo="+lsCorreo + "&FchVent="+lsFchVencimiento;
     lsDatosURL += "&TipoOp=Modificar&NoUsuario=<%=m_sNumTipoUsuario%>&CveEPO=<%=m_sCveEPO%>&CvePYME=<%=m_sCvePYME%>&CveIF=<%=m_sCveIF%>";
     lsDatosURL += "&CveUsNafin=" + lsCveUsNafin + "&Afiliacion=<%=m_sAfiliacion%>";
	 if ( typeof(lsNumSIRAC) != 'undefined' )
	   lsDatosURL += "&NumSIRAC=" + lsNumSIRAC;
              
     //alert("lsDatosURL: " + lsDatosURL);

	 if (gbIsNS4)
		document.frmGraba.src = lsDatosURL;
	 else	  
        self.frmGrabar.location.replace(lsDatosURL);
  }






</Script>








</form>

</body>
</html>