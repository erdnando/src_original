<!--
/***************************************************************************************
*
*                /////  Pragrama:    NAFIN                         /////
*                /////  Funci�n:     Entrada de datos              /////
*                /////                                             /////
*                /////                                             /////
*                /////  Autor:       Manuel Ramos Mendoza          /////
*                /////  Mod:         Ricardo Jimenez Vargas        /////
*                /////  Realizado:   20/Enero  /2001               /////
*                /////  Ult. Modif:  26/Febrero/2003               /////
*
****************************************************************************************/
-->
<%@ page import="java.sql.*,com.netro.seguridadbean.SeguException,java.lang.*"%>
<html>

<LINK REL="stylesheet" HREF="../css/formato.css" TYPE="text/css">
<Script Language="JavaSCript" src="../css/colores.js">  </Script>
<Script Language="Javascript" src="../scripts/Validaciones.js">  </Script>
<Script Language="Javascript" src="../scripts/conversiones.js">  </Script>

<body> 
<FORM Name="frmCabecera">
<Script Language="JavaScript">
	var Documento = window.document.Ceil;
	var isIE = (document.all ? true : false);
	var sOnBlur = "";
	var sSizeText = "";
</Script>

<table width="620" cellpadding="2" cellspacing="1" border="0">
	<tr>
		<td class="formas">&nbsp;</td>
	</tr>
	<tr>
		<td class="titulo" align="center">Mantenimiento a Usuarios</td>
	</tr>
	<tr>
		<td align="center">
			<table width="29%" border="0" cellspacing="2" cellpadding="0" align="center" height="14">
				<tr>
					<td width="18%" height="16"> <Font class="EtiketaUsr"> Consultar </font></td>
					<td width="46%" height="16"> 
						<input type="radio" name="rdOpcion" OnClick="mtdCambiaPantalla(0);"> <!-- CONSULTAR -->
					</td>
					<td width="2%" height="16"> <font class="EtiketaUsr"> Agregar </font> </td>
					<td width="34%" height="16">
						<input type="radio" name="rdOpcion" OnClick="mtdCambiaPantalla(1);"> <!-- AGREGAR  -->
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<Script Language="JavaSCript">
var giNumCliente = parseInt("<%=session.getAttribute("iNoCliente")%>");
var giNumUsuario = parseInt("<%=session.getAttribute("sesiNoUsuario")%>");   // IDENTIFICA EL TIPO DE USUARIO:
// NAFIN = 4
// EPO  = 1
/******************************************************************************
*      CAMBIO DE PANTALLA     AGREGAR  Y CONSULTAR
******************************************************************************/
function mtdCambiaPantalla(lsNumPatalla){
	if(lsNumPatalla == 0)	//-------------------------- CONSULTAR
		parent.frmTipoPantalla.location.replace("consultar/Usuario_consulta.jsp");
	else{					//-------------------------- AGREGAR
		var sDatosURL = "nafin/Usuario_Nafin.jsp?parNumUsr="+giNumUsuario+"&parNumCliente="+giNumCliente;
		parent.frmTipoPantalla.location.replace(sDatosURL);
	}
}
</Script>
</FORM>
</body>
</html>
