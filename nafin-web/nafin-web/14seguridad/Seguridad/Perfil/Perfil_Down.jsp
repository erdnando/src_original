<%@ page import="java.util.*, netropology.utilerias.*, com.netro.seguridadbean.* ,javax.naming.*" errorPage="/00utils/error.jsp"%>
<%@ page session="true" %>

<%
com.netro.seguridadbean.Seguridad objSeguridad = null;

objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
%>


<html>
<LINK REL="stylesheet" HREF="../css/formato.css" TYPE="text/css">
<Script Language="JavaSCript" src="../css/colores.js">  </Script>
<Script Language="JavaSCript" src="../scripts/Validaciones.js">  </Script>
<head>
<Script Language="JavaScript">
function mtdCambiaPuntero(src){
	src.style.cursor = 'hand';
}

function mtdRegresaPuntero(src){
	src.style.cursor = 'default';
}
</Script>
</head>
<%
Vector  vTabla       = new Vector(),
	vRegistro    = null,
	vRegistroTmp = new Vector(),
	vSubTabla    = new Vector(),
	vSubRegistroTmp = new Vector(), 
	vSubRegistro = null; 
String  
	sCveSistemaAnt  = "",
	sCveSistema     = "",
	sCvePerfilProt = "",
	sDescripcion = "",
	sBloqueo     = "",
	sDato        = null,
	sTipoAc      = "",
	sDatoTemp    = null,
	sDatosHidden = null,
	sCveAsig     = null,
	sTipUsr      = null,
	sValorBoton  = null,
	sCveFacultad = null,
	sValCveSistema  = null;
final String AGREGAR   = "Agregar", 
	ELIMINAR  = "Eliminar",
	MODIFICAR = "Modificar";
int          iAsingUsr      = 0,
	iFilaBuscar    = 0,
	iContRenglones = 0;
%>
<Script Language="JavaScript">
	var VClaves = new Array(1);  
	var i=0, iContadorColor = 0, iContadorColor2=0, j=0;
	var sColor = "White";

/**************************************************************************************
*       function mtdCambiaColor()
**************************************************************************************/
function mtdCambiaColor(){
	iContadorColor++;
	if(iContadorColor == 5)
		iContadorColor = 1;
	var Color = "";
	switch(iContadorColor){
		case 1:  { Color = sColor1; } break;
		case 2:  { Color = sColor2; } break; 
		case 3:  { Color = sColor1; } break;
		case 4:  { Color = sColor2; } break;
	}
	return Color;
}

/****************************************************************  *
*   function mtdCambiaColor2()     Determina el color de la Celda
********************************************************************/
function mtdCambiaColor2(ColorAnt){
	//alert("Cachando ColorAnt = " + ColorAnt);
	iContadorColor2++;
	if(iContadorColor2 == 3)
		iContadorColor2 = 1;
	var Color = "";

	if(ColorAnt == sColor1)
		switch(iContadorColor2){
			case 1:  { Color = sColor11; } break;
			case 2:  { Color = sColor1; } break; 
		}
	else if(ColorAnt == sColor2)
		switch(iContadorColor2){
			case 1:  { Color = sColor22; } break;
			case 2:  { Color = sColor2; } break; 
		}
	else
		alert("ColorAnt (" + ColorAnt + ")  sColor2("+sColor2+")");

	return Color;
}
	var sColor = "";
</Script>
<%
sCveAsig       =     request.getParameter("parCveAsig");
sCveSistema    =     request.getParameter("parCveSistema");
sCvePerfilProt =     request.getParameter("parCvePerfilProt");
sDescripcion   =     request.getParameter("parDescripcion");
sBloqueo       =     request.getParameter("parBloqueado");
sTipoAc        =     request.getParameter("parTipo");

sDatosHidden = request.getParameter("hidInter");

try{
	sTipUsr = "";
	if("".equals(sTipUsr.trim())){
		iAsingUsr = 1;
		iFilaBuscar = 7;
	}
	else if("C".equals(sTipUsr.trim())){
		iAsingUsr = 2;
		iFilaBuscar = 6;
	}
	else if("B".equals(sTipUsr.trim())){
		iAsingUsr = 4;
		iFilaBuscar = 5;
	}
	
	if(sCveSistema != null  && sCvePerfilProt != null){
		sValCveSistema  = "SEGU";
		
		if(sTipoAc.equals(ELIMINAR)){
			sCveFacultad = "SEGPERFPROTB";
			objSeguridad.borrarPerfilProt(sCveSistema, sCvePerfilProt);      
		}
		
		if(sTipoAc.equals(AGREGAR)){
			sCveFacultad   = "SEGPERFPROTA";
			objSeguridad.agregarPerfilProt(sCveSistema, sCvePerfilProt , sDescripcion, sBloqueo, Integer.parseInt(sCveAsig));   
		}
		
		if(sTipoAc.equals(MODIFICAR)){
			sCveFacultad   = "SEGPERFPROTC";
			objSeguridad.modificarPerfilProt(sCveSistema, sCvePerfilProt , sDescripcion, sBloqueo, Integer.parseInt(sCveAsig)); 
		}
	}
%>
<body class="MarcaBody">
<Form Name="Floor"  METHOD="POST">
<Script Language = "JavaScript"> 
	var sOnclick = "";
	var sColorFondoTdAnterior = "";
	var sNombreTdAnterior = "";
</Script>
<table width="100%"  borderColor="#339933" cellspacing="0" cellpadding="0" align="center" name="Cabecera">
<input type=hidden name=hidInter Value="">
<%
	vSubTabla = objSeguridad.consultarPerfilesProt();
	//System.out.print("Vector de tama�o = " + vSubTabla.size());
	for(int yy=0; yy<vSubTabla.size(); yy++){
		iContRenglones++; 
		
		vSubRegistro   = (Vector) vSubTabla.elementAt(yy);
		sCveSistema    = (String) vSubRegistro.elementAt(0);
		sCvePerfilProt = (String) vSubRegistro.elementAt(1);
		sDescripcion   = (String) vSubRegistro.elementAt(2);
		sBloqueo       = (String) vSubRegistro.elementAt(3);
		
		String  sAsing       =         (String) vSubRegistro.elementAt(iFilaBuscar);
		
		if(iFilaBuscar != 7  &&  sAsing.equals("N"))
			continue;
		
		if(iFilaBuscar == 7 ){
			iAsingUsr = ((Integer) vSubRegistro.elementAt(4)).intValue();
		}
		
		if(sCveSistema.equals(sCveSistemaAnt)){
%>

<!-- ///////////////////////////////////  ARAM TABLA   ///////////////////////// -->
	<TR>
		<Script Language = "JavaScript"> 
			var sColorSub = mtdCambiaColor2(sColor); 

			window.document.write("<TD width='*%' BgColor='"+sColorSub+"' >");
			window.document.write('<font class="Dato2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=sDescripcion%> </font> ');
			window.document.write('</td>'); 

			window.document.write("<TD width='5%' BgColor='"+sColorSub+"'>");
			window.document.write("<font size=1> <CENTER>");

			window.document.write("<Input Type='CheckBox' Name='chkBloq'  value='<%=sCveSistema%>|<%=sCvePerfilProt%>' >");

			window.document.write("</CENTER>");
			window.document.write("</font> </td>"); 
		</Script>
	</TR>
<%
		}
		else{
%>
	<TR>
		<Script Language = "JavaScript">
			sColor = mtdCambiaColor();
			window.document.write("<TD width='*%' BgColor='" + sColor + "'> <font class='Dato1'> <b> <%=sCveSistema%> </b> </font> </td>"); 
			window.document.write("<TD width='5%'  BgColor='" + sColor + "'> <font class='Dato1'> &nbsp; </font> </td>"); 
		</Script>
	</TR>
	<TR>
		<Script Language = "JavaScript"> 
			var sColorSub = mtdCambiaColor2(sColor); 
			sOnclick = "OnClick='mtdCambiaColorTd(this.id, ";
			//sOnclick += 'this.style.backgroundColor,"<%=sCveSistema%>", "<%=sCvePerfilProt%>", "<%=sDescripcion%>", "<%=sBloqueo%>", "<%=iAsingUsr%>"';
			sOnclick += '"' + sColorSub + '","<%=sCveSistema%>", "<%=sCvePerfilProt%>", "<%=sDescripcion%>", "<%=sBloqueo%>", "<%=iAsingUsr%>"';
			sOnclick += "); '";

			window.document.write("<TD width='*%' BgColor='" + sColorSub + "' >");
			window.document.write('<font class="Dato2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=sDescripcion%> </font>');
			
			window.document.write("<TD width='5%' BgColor='"+sColorSub+"' > ");
			window.document.write("<font size=1> <CENTER>");
			
			window.document.write("<Input Type='CheckBox' Name='chkBloq'   value='<%=sCveSistema%>|<%=sCvePerfilProt%>'>"); 
			
			window.document.write("</CENTER>"); 
			window.document.write("</font> </td>"); 
		</Script>
	</TR>
<%
		}
		sCveSistemaAnt = sCveSistema;
	}
}
catch(com.netro.seguridadbean.SeguException e){
	System.out.println("Error:" + e.toString());
	e.printStackTrace();
	String sError = e.getMessage();
%>
	<body class="MarcaBody">
	<Form>
	<Script Language="JavaScript">
		var Error = "<%=sError%>";
		if (Error == "")
			alert("Error: "+Error);
		else
			alert(Error);
			window.location.replace('PerfilProt_Down.jsp');
		</Script>
	</Form>
	</body>
<%
}
catch(Exception e){
	System.out.println("Error:" + e.toString());
	e.printStackTrace();
	String Error = e.getMessage();
%>
	<body class="MarcaBody">
	<Form>
	<Script Language="JavaScript">
		var Error = "<%=Error%>"
		alert("Error:" + Error);
		parent.parent.parent.close();
	</Script>
	</Form>
	</body>
<%}%>
</table>
<iframe name="frmAsociar" width="200" height="0px"  hspace=0 vspace=0
	src="" marginwidth=0 marginheight=0 scrolling="no" frameborder=0>
</iframe>
<Script Language="JavaScript">
/**********************************************************************
*       function generaDatos()
**********************************************************************/
function generaDatos(){
	var lsCvePerfil = parent.parent.frmCaptura.frmConsulta.document.frmConsulta.hdnCvePerfil.value;
	var lsTramaperfiles = "";
	var lsNumChkActivos = 0;

	for(var i=0;  i<document.getElementsByTagName("input").length; i++ ){
		if(document.getElementsByTagName("input")[i].name == "chkBloq" &&  document.getElementsByTagName("input")[i].checked){
			lsTramaperfiles += document.getElementsByTagName("input")[i].value + "~";
			lsNumChkActivos++;
		}
	}

//	if(lsTramaperfiles != ""){
		var sDatosURL = "Perfil_Asociar.jsp?parTrama="+lsTramaperfiles+"&parNum="+lsNumChkActivos + "&parCvePerfil=" + lsCvePerfil;
		sDatosURL = fnSustituyeCaracter(sDatosURL, " ", "%20");
		self.frmAsociar.location.replace(sDatosURL);
//	}
}

/*************************************************************************************
*  function mtdMuestraPP(lsCvePerfil, lsBloqueado, lsTpUsr)
*************************************************************************************/
function mtdSeleccionaChk(matPerfileP){
	//alert("toda: " + matPerfileP[0]);
	// BARRE SISTEMAS
	for(var sis=0; sis<matPerfileP.length; sis++){
		//alert(matPerfileP[sis].length +  "  perfiles:   " + matPerfileP[sis]);
		// BARRE PERFILES PROTOTIPO
		for(var per=1; per<matPerfileP[sis].length; per++){
			// BARRE TODOS LOS OBJETOS "INPUT"
			for (var i=0; i<document.getElementsByTagName("input").length; i++ ){
				// SOLO LOS CHECKBOX
				if(document.getElementsByTagName("input")[i].name == "chkBloq"){
					var lsSisPer = matPerfileP[sis][0] + "|" + matPerfileP[sis][per];
					//alert(lsSisPer + " = " + document.getElementsByTagName("input")[i].value);
					if(document.getElementsByTagName("input")[i].value == lsSisPer ){
						document.getElementsByTagName("input")[i].checked = true;
					}
				}
			}
		}
	}
}

/*************************************************************************************
*  function mtdMuestraPP(lsCvePerfil, lsBloqueado, lsTpUsr)
*************************************************************************************/
function mtdDesSeleccionaChk(){
	// BARRE TODOS LOS OBJETOS "INPUT"
	for(var i=0; i<document.getElementsByTagName("input").length; i++ ){
		// SOLO LOS CHECKBOX
		if(document.getElementsByTagName("input")[i].name == "chkBloq"){
			document.getElementsByTagName("input")[i].checked = false;
		}
	}
}
</Script>
</Form>
</body>
</html>
