function prueba(){
	alert('si');
}

Ext.onReady(function() {




//||||||||||||||||||||||||||||||||||||||HADLER||||||||||||||||||||||||||||||||||

	var procesarConsultaPerfilesData = function(store, arrRegistros, opts) {
		fpMantPerfiles.el.unmask();
		var vPnlGrids = Ext.getCmp('pnlGrids_');
		gdPerfiles.show();
		
		if (arrRegistros != null) {
			if (!gdPerfiles.isVisible()) {
				vPnlGrids.add(gdPerfiles);
				vPnlGrids.doLayout();
			}
			
			var el = gdPerfiles.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};
	
	var procesarConsultaPerfilProtData = function(store, arrRegistros, opts) {
		//fpMantPerfiles.el.unmask();
		var vPnlGrids = Ext.getCmp('pnlGrids_');
		gdPerfilesProt.hide();
		
		if (arrRegistros != null) {
			if (!gdPerfilesProt.isVisible()) {
				vPnlGrids.add(gdPerfilesProt);
				vPnlGrids.doLayout();
			}
			
			var el = gdPerfilesProt.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};
	
	var procesarEjecutaAccion = function(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			Ext.Msg.alert('Aviso', resp.msg+' con �xito', function(){
				storePerfilesData.load();
				storePerfilProtData.load();
			});
		
		} else {
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				
				NE.util.mostrarConnError(response,opts);
				storePerfilesData.load();
				storePerfilProtData.load();
			}
 
		}
	};
	
	var ejecutaAccion = function(tipoAccion){
		
		Ext.Msg.show({
			title:	"Mensaje",
			msg:		'Est� seguro de '+tipoAccion+' el registro?',
			buttons:	Ext.Msg.OKCANCEL,
			fn: function resultMsj(btn){
				if (btn == 'ok') {
					pnl.el.mask(tipoAccion+' en proceso...', 'x-mask-loading');
					Ext.Ajax.request({  
						url: 'perfil_ext.data.jsp',  
						method: 'POST',  
						params:Ext.apply(fpMantPerfiles.getForm().getValues(),{
							informacion: 'EjecutaAccion',
							operacion: tipoAccion
						}),
						callback: procesarEjecutaAccion
					});
				}
			},
			closable:false,
			icon: Ext.MessageBox.QUESTION
		});
	}

	var fnAsociarPerfilPerfProt = function(){
		var registrosEnviar = [];
		
		storePerfilProtData.each(function(record){
			if(record.data['gdSeleccion']){
				registrosEnviar.push(record.data);
			}
		});
		
		
		Ext.Ajax.request({
			url: 'perfil_ext.data.jsp',
			params:Ext.apply(fpMantPerfiles.getForm().getValues(),{
				informacion: 'EjecutaAccion',
				operacion: 'Asociar',
				registros: Ext.encode(registrosEnviar)
			}),
			callback: procesarEjecutaAccion
		});
		
	}
	
//|||||||||||||||||||||||||||||||||||||||||STORE||||||||||||||||||||||||||||||

	var storeCatAsocData = new Ext.data.JsonStore({
		id: 'storeCatAsocData_',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : 'perfil_ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoAsociado'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			//load: procesarCatalogoEpoData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var storePerfilesData = new Ext.data.JsonStore({
		url : 'perfil_ext.data.jsp',
		id: 'storePerfilesData_',
		baseParams:{
			informacion: 'ConsultaPerfiles'
		},
		root : 'registros',
		fields: [
			{name: 'fpCvePerfil'},
			{name: 'fpBloqueo'},
			{name: 'fpCveOID'},
			{name: 'fpCveAsoc'},
			{name: 'fpSeleccion'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			load: procesarConsultaPerfilesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaPerfilesData(null, null, null);
				}
			}
		}
	});
	
	
	var storePerfilProtData = new Ext.data.GroupingStore({
		url : 'perfil_ext.data.jsp',
		id: 'storePerfilProtData_',
		baseParams:{
			informacion: 'ConsultaModPerfProt'
		},
		root : 'registros',
		reader: new Ext.data.JsonReader({
			root : 'registros',	totalProperty: 'total',
			fields: [
				{name: 'gdCveSistema'},
				{name: 'gdCvePerfilProt'},
				{name: 'gdDescripcion'},
				{name: 'gdBloqueo'},
				{name: 'gdSeleccion'}
			]
		}),
		totalProperty : 'total',
		messageProperty: 'msg',
		groupField:'gdCveSistema',
		sortInfo:{field: 'gdCveSistema', direction: "ASC", align: 'left' },
		autoLoad: true,
		listeners: {
			load: procesarConsultaPerfilProtData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaPerfilProtData(null, null, null);
				}
			}
		}
	});
	

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
	var elementosForm =
	[
		{
			xtype: 'textfield',
			name: 'fpCvePerfil',
			id: 'fpCvePerfil_',
			fieldLabel: 'Clave de Perfil',
			allowBlank: false,
			maxLength: 100,
			anchor: '90%',
			msgTarget: 'side',
			margins: '0 20 0 0'//Necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'fpCveOID',
			id: 'fpCveOID_',
			fieldLabel: 'Clave OID',
			allowBlank: false,
			maxLength: 100,
			anchor: '90%',
			msgTarget: 'side',
			margins: '0 20 0 0'//Necesario para mostrar el icono de error
		},
		{
			xtype: 'combo',
			name: 'fpCveAsoc',
			id: 'fpCveAsoc_',
			fieldLabel: 'Asociado a',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'fpCveAsoc',
			emptyText: 'Seleccione...',
			anchor: '90%',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			hidden: false,
			allowBlank: false,
			store : storeCatAsocData
		},
		{
			xtype: 'radiogroup',
			fieldLabel: 'Bloqueado',
			name:'fpRadioBloq',
			allowBlank: false,
			id:'fpRadioBloq_',
			items: [
				 {boxLabel: 'Si', name: 'fpBloqueo', inputValue:'S'},
				 {boxLabel: 'No', name: 'fpBloqueo', inputValue:'N'}
			]
      }
	];
	

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
	var fpMantPerfiles = new Ext.form.FormPanel({
		id: 'fpMantPerfiles_',
		width: 600,
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForm,
		monitorValid: true,
		buttons: [
			{
				text: 'Agregar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(){
					ejecutaAccion('Agregar');
					
				}
			},
			{
				text: 'Eliminar',
				iconCls: 'icoBorrar',
				formBind: true,
				handler: function(){
					ejecutaAccion('Eliminar');
					
				}
			},
			{
				text: 'Modificar',
				iconCls: 'icoModificar',
				formBind: true,
				handler: function(){
					ejecutaAccion('Modificar');
					
				}
			}
		]
	});
	
	
	var selectModel = new Ext.grid.CheckboxSelectionModel({
	  checkOnly: true,
	  header:'Test',
	  id:'test',
	  width: 100
	  /*renderer: function(v, p, record){
			if (record.data['SELECBOOL']){
				return '<div class="x-grid3-row-checker">&#160;</div>';
			}else{
				return '<div>&#160;</div>';
			}
		}*/
	});
	
	var gdPerfiles = new Ext.grid.EditorGridPanel({
	id: 'gdPerfiles_',
	store: storePerfilesData,
	style:	'margin:0 auto;',
	margins: '20 0 0 0',
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	//sm: selectModel,
	columns: [
		{
			header: 'Perfil',
			tooltip: 'Perfil',
			dataIndex: 'fpCvePerfil',
			sortable: true,
			width: 470,
			align: 'left',
			hidden: false
			/*renderer: function(valor, metadata, registro, rowindex, colindex, store) {
				
			}*/
		},
		{
			xtype: 'checkcolumn',
			//header : '<p><a href="javascript:prueba();">Bloqueado</a></p>',
			header : 'Bloqueado',
			tooltip: 'Bloqueado',
			dataIndex : 'fpBloqueo',
			width : 80,
			sortable : false,
			renderer : function(v, p, record){
			
				p.css += ' x-grid3-check-col-td'; 
				return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', (v=='S'||v==true)? '-on' : '');
			}
		}
		
		
	],
	listeners:{
		cellclick: function(grid, rowIndex, columnIndex, e) {
			var record = grid.getStore().getAt(rowIndex);  // Get the Record
			var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
			var data = record.get(fieldName);
			fpMantPerfiles.getForm().loadRecord(record);
			
		}
	},
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 300,
	width: 590,
	style: 'margin:0 auto;',
	title: '',
	frame: true
	});
	
	
	var gdPerfilesProt = new Ext.grid.EditorGridPanel({
	id: 'gdPerfilesProt_',
	store: storePerfilProtData,
	style:	'margin:0 auto;',
	margins: '20 0 0 0',
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	view: new Ext.grid.GroupingView({
		forceFit:true,
		groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
	}),
	columns: [
		
		{
			header: 'Modulo',
			tooltip: 'Modulo',
			dataIndex: 'gdCveSistema',
			sortable: true,
			width: 0,
			align: 'left',
			hidden: true
			/*renderer: function(valor, metadata, registro, rowindex, colindex, store) {
				
			}*/
		},
		{
			header: 'Perfil Prototipo',
			tooltip: 'Perfil Prototipo',
			dataIndex: 'gdDescripcion',
			sortable: true,
			width: 300,
			align: 'left',
			hidden: false
			/*renderer: function(valor, metadata, registro, rowindex, colindex, store) {
				
			}*/
		},
		{
			xtype: 'checkcolumn',
			header : 'Seleccionar',
			tooltip: 'Seleccionar',
			dataIndex : 'gdSeleccion',
			width : 50,
			sortable : false
			/*renderer : function(v, p, record){
				p.css += ' x-grid3-check-col-td'; 
				return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v=='S' ? '-on' : '');
			}*/
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 300,
	width: 550,
	style: 'margin:0 auto;',
	title: '',
	frame: true,
	bbar: {
		xtype: 'toolbar',
		items: [
			{
				text: 'Asociar',
				id: 'btnAsocuar',
				hidden: true,
				handler: fnAsociarPerfilPerfProt
			}
		]
	}
	});

//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		renderHidden: true,
		layoutConfig: {
			align:'center'
		},
		items: [
			NE.util.getEspaciador(20),
			fpMantPerfiles,
			NE.util.getEspaciador(20),
			new Ext.Panel({
				name: 'pnlGrids',
				id: 'pnlGrids_',
				width: 600,
				style: 'margin:0 auto;',
				layout:'hbox',
				layoutConfig: {
					 padding:'5',
					 pack:'center',
					 align:'middle'
				},
				defaults:{margins:'0 10 0 0'},
				frame: false
			})
			

		]
	});

});