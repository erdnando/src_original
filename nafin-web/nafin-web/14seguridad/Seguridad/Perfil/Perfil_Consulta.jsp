<%@ page import="java.util.*,netropology.utilerias.*,com.netro.seguridadbean.* ,javax.naming.*" errorPage="/00utils/error.jsp" %>
<%@ page session="true" %>
<%
com.netro.seguridadbean.Seguridad objSeguridad = null;

objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
%>

<html>
<LINK REL="stylesheet" HREF="../css/formato.css" TYPE="text/css">
<Script Language="JavaSCript" src="../css/colores.js">  </Script>
<head>
<Script Language="JavaScript">
function mtdCambiaPuntero(src){
	src.style.cursor = 'hand';
}

function mtdRegresaPuntero(src){
	src.style.cursor = 'default';
}
</Script>
</head>
<%
Vector  vPerfiles  = new Vector(),
	vRegistro  = new Vector();
String       sCvePerfil     = "",
	claveOID     = null,
	sBloqueo       = "",
	tipoAfiliado       = "",
	sTipoAc        = "";
final String AGREGAR   = "Agregar", 
	ELIMINAR  = "Eliminar",
	MODIFICAR = "Modificar";
%>

<Script Language="JavaScript">
var VClaves = new Array(1);  
var i=0, iContadorColor = 0, iContadorColor2=0, j=0;
var sColor = "White";

/**************************************************************************************
*       function mtdCambiaColor()
**************************************************************************************/
function mtdCambiaColor(){
	iContadorColor++;
	if(iContadorColor == 5)
	iContadorColor = 1;
	var Color = "";
	switch(iContadorColor){
		case 1:  { Color = sColor1; } break;
		case 2:  { Color = sColor2; } break; 
		case 3:  { Color = sColor1; } break;
		case 4:  { Color = sColor2; } break;
	}
	return Color;
}

/****************************************************************  *
*   function mtdCambiaColor2()     Determina el color de la Celda
********************************************************************/
function mtdCambiaColor2(ColorAnt){
	iContadorColor2++;
	if(iContadorColor2 == 3)
		iContadorColor2 = 1;
	var Color = "";

	if(ColorAnt == sColor1){
		switch(iContadorColor2){
			case 1:  { Color = sColor11; } break;
			case 2:  { Color = sColor1; } break; 
		}
	}
	else if(ColorAnt == sColor2){
		switch(iContadorColor2){
			case 1:  { Color = sColor22; } break;
			case 2:  { Color = sColor2; } break; 
		}
	}
	return Color;
}
var sColor = "";
</Script>
<%
sCvePerfil  = request.getParameter("parCvePerfil");	//Clave del Perfil
sBloqueo    = request.getParameter("parBloqueado");	//Bloqueado o no?
claveOID  = request.getParameter("parClaveOID");	//Clave asociada al perfil dentro del OID
tipoAfiliado  = request.getParameter("parTipoAfiliado");	//Clave del tipo de Afiliado al que esta orientado el perfil
sTipoAc     = request.getParameter("parTipo");		//Tipo de acci�n


//System.out.println(netropology.utilerias.HttpUtilerias.getCadenaParametros(request));
String mensajeErrorRegistroPerfinEnOID = "";
try{
	if (sTipoAc != null){
		try{
			if(sTipoAc.equals(ELIMINAR)){
				objSeguridad.borrarPerfil(sCvePerfil);
			}
			
			if(sTipoAc.equals(AGREGAR)){
				mensajeErrorRegistroPerfinEnOID = 
						objSeguridad.agregarPerfil(sCvePerfil, sBloqueo, 
						Integer.parseInt(claveOID), Integer.parseInt(tipoAfiliado));
			}
			
			if(sTipoAc.equals(MODIFICAR)){
				objSeguridad.modificarPerfil(sCvePerfil, sBloqueo, Integer.parseInt(claveOID), Integer.parseInt(tipoAfiliado));
			}
		} catch(com.netro.seguridadbean.SeguException errFacAsing){
%>
		<script language="JavaScript">
			alert("<%=errFacAsing.getMsgError()%>");
		</script>
<%	
		}	//fin try
	} //fin if
	
	if (mensajeErrorRegistroPerfinEnOID!=null && 
			!mensajeErrorRegistroPerfinEnOID.equals("")) {
%>
		<script language="JavaScript">
			alert("Error al registrar el perfil en el OID: " + "\n" +
				"<%=mensajeErrorRegistroPerfinEnOID%>");
		</script>
<%	
	}
%>

<body class="MarcaBody">

<Form Name="frmConsulta">
<Script Language = "JavaScript"> 
	var sOnclick = "";
	var sColorFondoTdAnterior = "";
	var sNombreTdAnterior = "";
</Script>

<table width="100%"  cellspacing="0" cellpadding="0" align="center" name="Cabecera">
<%
vPerfiles = objSeguridad.consultarPerfiles();

for(int y=0; y<vPerfiles.size(); y++){
	vRegistro   = (Vector) vPerfiles.elementAt(y);
	sCvePerfil = (String) vRegistro.elementAt(0);
	sBloqueo   = (String) vRegistro.elementAt(1);
	claveOID = String.valueOf((Integer) vRegistro.elementAt(2));
	tipoAfiliado = String.valueOf((Integer) vRegistro.elementAt(3));
	String lsChkBloq = "";
	
	if(!sBloqueo.equals("N"))   lsChkBloq = "checked";
%>
<!-- ///////////////////////////////////  ARMA TABLA   ///////////////////////// -->
	<TR>
		<Script Language = "JavaScript"> 
			var sColorSub = mtdCambiaColor(sColor); 
			window.document.write("<TD width='*%' BgColor='"+sColorSub+"' >");

			window.document.write('<a href="javascript:mtdMuestraPP(\'<%=sCvePerfil%>\', \'<%=sBloqueo%>\', \'<%=claveOID%>\', \'<%=tipoAfiliado%>\');" >');
			window.document.write('<font class="Dato2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=sCvePerfil%> </font> ');
			window.document.write('</td>'); 
			window.document.write('</a>');
			window.document.write("<TD width='5%' BgColor='"+sColorSub+"'>");
			window.document.write("<font size=1> <CENTER>");

			window.document.write("<Input Type='CheckBox' Name='chkBloq'  <%=lsChkBloq%> >");

			window.document.write(" </CENTER>");
			window.document.write("</font> </td>"); 
		</Script>
	</TR>
<%
	}
}
catch(com.netro.seguridadbean.SeguException e){
	System.out.println("Error:" + e.toString());
	e.printStackTrace();
	String sError = e.getMessage();
%>
	<body class="MarcaBody">
	<Form>
	<Script Language="JavaScript">
		var Error = "<%=sError%>";
		if(Error == "")
			alert("Error inesperado S");
		else
			alert(Error);
			window.location.replace('PerfilProt_Down.jsp');
	</Script>
	</Form>
	</body>
<%
}
catch(Exception e){
	System.out.println("Error:" + e.toString());
	e.printStackTrace();
	String Error = e.getMessage();
%>
	<body class="MarcaBody">
	<Form>
	<Script Language="JavaScript">
		var Error = "<%=Error%>"
		alert("Error: <%=Error%>");
		parent.parent.parent.close();
	</Script>
	</Form>
	</body>
<%}%>
</table>
<iframe name="frmExtrarPP" width="200" height="0:px" bgcolor="#3366CC" hspace=0 vspace=0
	src="" marginwidth="0" marginheight="0" frameborder="0"  scrolling="yes">
</iframe>
<input type="hidden"  name="hdnCvePerfil">
<Script Language="JavaScript">
/*************************************************************************************
*  function mtdMuestraPP()
*************************************************************************************/
function mtdMuestraPP(lsCvePerfil, lsBloqueado, lsClaveOID, lsTipoAfiliado){
	window.document.frmConsulta.hdnCvePerfil.value = lsCvePerfil;

	top.body.body2.frmCaptura.mtRecibeDatos(lsCvePerfil, lsBloqueado, lsClaveOID, lsTipoAfiliado);
	self.frmExtrarPP.location.replace("Perfil_ExtrarPP.jsp?parCvePerfil="+lsCvePerfil);
}
</Script>
</Form>
</body>
</html>
