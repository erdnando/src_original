<!--
/*****************************************************************************

                 /////  Pragrama:    NAFIN                         /////
                 /////  Funci�n:     Lista de datos                /////
                 /////                                             /////
                 /////  Autor:       Manuel Ramos Mendoza          /////
                 /////  Realizado:   08/  Enero/2001               /////
                 /////  Ult. Modif:  19/  Junio/2001               /////

******************************************************************************/
-->
<%@ page import="java.util.*,java.lang.*, netropology.utilerias.*,com.netro.seguridadbean.Seguridad, com.netro.seguridadbean.* ,javax.naming.*"%>
<%@ page session="true" %>
<!--jsp:usebean id="objSeguridad" class="com.netro.seguridadbean.SeguridadBean" scope="page"/-->
<%
Seguridad objSeguridad = null;

try {
	objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",Seguridad.class);
} catch (Exception e) {
	System.out.println(e);
%>
	<script>
		alert("EXISTEN DIFICULTADES TECNICAS, INTENTE M�S TARDE");
	</script>
<%
	return;
}
%>

<%!
String  m_sTrama,m_sCvePerfil;

Vector vecPerfilesP;

int m_iNumPP = 0;


/************************************************************************************************
*   mtdDesentrama       SUBE TRAMA DE PERFILESPORT. Y  SISTEMA  A UN VECTOR
************************************************************************************************/

Vector mtdDesentrama(String lsTrama, int lsNumElementos){
	String lsDatoTmp = "";
	int    liPos = 0;
	
	Vector vector    = new Vector();
	Vector vectorTem = new Vector();
	
	for(int j=0 ; j< lsNumElementos; j++){
		vectorTem = new Vector();
		
		lsDatoTmp = lsTrama.substring(liPos, lsTrama.indexOf("~", liPos));
		liPos = lsTrama.indexOf("~", liPos) + 1;
		
		String lsDatoTmp_2 = "";
		
		lsDatoTmp_2 = lsDatoTmp.substring(0, lsDatoTmp.indexOf("|"));
		vectorTem.add(lsDatoTmp_2);
		
		lsDatoTmp_2 = lsDatoTmp.substring(lsDatoTmp.indexOf("|")+1);
		vectorTem.add(lsDatoTmp_2);
		
		vector.add(vectorTem);
	}
	return vector;
}
%>


<%
int m_iNumPP = 0;
m_sTrama = "";
m_sCvePerfil = "";
Vector vecPerfilesP = new Vector();
m_iNumPP = 0;

m_sTrama = request.getParameter("parTrama");

if(m_sTrama != null){
	m_sCvePerfil = (String) request.getParameter("parCvePerfil");
	m_iNumPP     = Integer.parseInt(request.getParameter("parNum"));
	
	vecPerfilesP = mtdDesentrama(m_sTrama, m_iNumPP);
	
	try{
		objSeguridad.asociarPerfilPerfilesProt(m_sCvePerfil, vecPerfilesP);
%>
		<Script Language="JavaScript">
			alert("Datos asociados.");
		</Script>
<%
	}
	catch (com.netro.seguridadbean.SeguException e){
		out.print("<br> Error: " + e.toString());
	}
}
%>
<html>

</html>