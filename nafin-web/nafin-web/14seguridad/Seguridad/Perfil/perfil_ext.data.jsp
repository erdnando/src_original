<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.lang.*,
		netropology.utilerias.*,
		com.netro.seguridadbean.*,
		com.netro.model.catalogos.*,
		com.netro.fondojr.*,
		com.netro.cadenas.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";

/*	SE CREA NUEVO OBJETO PARA ACCESAR EL EJB */
//SeguridadHome seguridadHome = (SeguridadHome)ServiceLocator.getInstance().getEJBHome("SeguridadEJB", SeguridadHome.class);
//com.netro.seguridadbean.Seguridad seguridad = seguridadHome.create();

com.netro.seguridadbean.Seguridad seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);

if(informacion.equals("CatalogoAsociado")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_tipo_afiliado");
	cat.setCampoClave("ic_tipo_afiliado");
	cat.setCampoDescripcion("cd_afiliado");
	cat.setOrden("ic_tipo_afiliado");
	infoRegresar = cat.getJSONElementos();	

}else if (informacion.equals("ConsultaPerfiles") ) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsonArray = new JSONArray();
	
	Vector vPerfiles  = new Vector();
	Vector vRegistro  = new Vector();
	String sCvePerfil = "";
	String claveOID = null;
	String sBloqueo = "";
	String tipoAfiliado = "";
	String sTipoAc = "";
	
	//String cveEpo  = request.getParameter("ic_epo")==null?"":request.getParameter("ic_epo");
	vPerfiles = seguridad.consultarPerfiles();
	List lstPerfiles = new ArrayList();
	HashMap hmPerfil = null;
	
	for(int y=0; y<vPerfiles.size(); y++){
		hmPerfil = new HashMap();
		vRegistro   = (Vector) vPerfiles.elementAt(y);
		
		sCvePerfil = (String) vRegistro.elementAt(0);
		sBloqueo   = (String) vRegistro.elementAt(1);
		claveOID = String.valueOf((Integer) vRegistro.elementAt(2));
		tipoAfiliado = String.valueOf((Integer) vRegistro.elementAt(3));
		String lsChkBloq = "";
		if(!sBloqueo.equals("N"))   lsChkBloq = "checked";
		
		hmPerfil.put("fpCvePerfil", sCvePerfil);
		hmPerfil.put("fpBloqueo", sBloqueo);
		hmPerfil.put("fpCveOID", claveOID);
		hmPerfil.put("fpCveAsoc", tipoAfiliado);
		
		lstPerfiles.add(hmPerfil);
		
	}
	jsonArray = JSONArray.fromObject(lstPerfiles);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", jsonArray.toString());
	
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("ConsultaModPerfProt") ) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsonArray = new JSONArray();
	List lstPerfilProt = new ArrayList();
	
	Vector  vTabla       = new Vector(),
	vRegistro    = null,
	vRegistroTmp = new Vector(),
	vSubTabla    = new Vector(),
	vSubRegistroTmp = new Vector(), 
	vSubRegistro = null; 
	String  
	sCveSistemaAnt  = "",
	sCveSistema     = "",
	sCvePerfilProt = "",
	sDescripcion = "",
	sBloqueo     = "",
	sDato        = null,
	sTipoAc      = "",
	sDatoTemp    = null,
	sDatosHidden = null,
	sCveAsig     = null,
	sTipUsr      = null,
	sValorBoton  = null,
	sCveFacultad = null,
	sValCveSistema  = null;
	
	int iAsingUsr      = 0,
	iFilaBuscar    = 0,
	iContRenglones = 0;
	
	vSubTabla = seguridad.consultarPerfilesProt();
	List lstPerfiles = new ArrayList();
	HashMap hmPerfilProt = null;
	
	for(int yy=0; yy<vSubTabla.size(); yy++){
		iContRenglones++; 
		
		vSubRegistro   = (Vector) vSubTabla.elementAt(yy);
		sCveSistema    = (String) vSubRegistro.elementAt(0);
		sCvePerfilProt = (String) vSubRegistro.elementAt(1);
		sDescripcion   = (String) vSubRegistro.elementAt(2);
		sBloqueo       = (String) vSubRegistro.elementAt(3);
		
		String  sAsing       =         (String) vSubRegistro.elementAt(iFilaBuscar);
		
		/*if(iFilaBuscar != 7  &&  sAsing.equals("N"))
			continue;
		
		if(iFilaBuscar == 7 ){
			iAsingUsr = ((Integer) vSubRegistro.elementAt(4)).intValue();
		}*/
		
		hmPerfilProt = new HashMap();
		hmPerfilProt.put("gdCveSistema", sCveSistema);
		hmPerfilProt.put("gdCvePerfilProt", sCvePerfilProt);
		hmPerfilProt.put("gdDescripcion", sDescripcion);
		hmPerfilProt.put("gdBloqueo", sBloqueo);
		
		lstPerfilProt.add(hmPerfilProt);
		
	}
	jsonArray = JSONArray.fromObject(lstPerfilProt);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", jsonArray.toString());
	
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ConsultaPerfProtXperfil") ) {
	List lstPerfProt = new ArrayList();
	String fpCvePerfil  = request.getParameter("fpCvePerfil")==null?"":request.getParameter("fpCvePerfil");
	Hashtable hsPerfilesP = seguridad.consultarPerfilesProtXPerfil(fpCvePerfil);
	
	String lsCveSistema = "";
	for(Enumeration enu = hsPerfilesP.keys(); enu.hasMoreElements();){
		lsCveSistema = (String) enu.nextElement();
		Vector vecTempPerf  = (Vector) hsPerfilesP.get(lsCveSistema);

		for(int i=1; i<=vecTempPerf.size(); i++){
			HashMap hmData = new HashMap();
			hmData.put("SISTEMA", lsCveSistema);
			hmData.put("PERFPROT", vecTempPerf.elementAt(i-1));
			lstPerfProt.add(hmData);
		}
	}

}else if (informacion.equals("EjecutaAccion") ) {
	JSONObject jsonObj = new JSONObject();
	boolean exitoPeticion = true;
	String mensajeErrorRegistroPerfinEnOID = "";
	String mensaje = "";
	
	String fpCvePerfil  = request.getParameter("fpCvePerfil")==null?"":request.getParameter("fpCvePerfil");
	String fpBloqueo  = request.getParameter("fpBloqueo")==null?"":request.getParameter("fpBloqueo");
	String fpCveOID  = request.getParameter("fpCveOID")==null?"":request.getParameter("fpCveOID");
	String fpCveAsoc  = request.getParameter("fpCveAsoc")==null?"":request.getParameter("fpCveAsoc");

	try{
		mensaje = operacion;
		if(operacion.equals("Agregar")){
			mensajeErrorRegistroPerfinEnOID = seguridad.agregarPerfil(fpCvePerfil, fpBloqueo, Integer.parseInt(fpCveOID), Integer.parseInt(fpCveAsoc));
			
		}else if(operacion.equals("Modificar")){
			seguridad.modificarPerfil(fpCvePerfil, fpBloqueo, Integer.parseInt(fpCveOID), Integer.parseInt(fpCveAsoc));
			
		}else if(operacion.equals("Eliminar")){
			seguridad.borrarPerfil(fpCvePerfil);
			
		}else if(operacion.equals("Asociar")){
			Vector vPerfProt = new Vector();
			String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
			List arrRegistros = JSONArray.fromObject(jsonRegistros);
			Iterator itReg = arrRegistros.iterator();
			while (itReg.hasNext()) {
				Vector vReg = new Vector();
				JSONObject registro = (JSONObject)itReg.next();
				System.out.println("----------------------------------------------------------------------");
				System.out.println("registro.getString(gdCveSistema)=="+registro.getString("gdCveSistema"));
				System.out.println("registro.getString(gdCvePerfilProt)=="+registro.getString("gdCvePerfilProt"));
				vReg.add(registro.getString("gdCveSistema"));
				vReg.add(registro.getString("gdCvePerfilProt"));
				
				vPerfProt.add(vReg);
			}
			
			seguridad.asociarPerfilPerfilesProt(fpCvePerfil, vPerfProt);
			
		}
		
	}catch(com.netro.seguridadbean.SeguException t){
		exitoPeticion = false;
		mensaje = t.getMsgError();
	}

	jsonObj.put("msg", mensaje);
	jsonObj.put("msgErrorPerfilEnOID", mensajeErrorRegistroPerfinEnOID);
	jsonObj.put("success", new Boolean(exitoPeticion));
	infoRegresar = jsonObj.toString();
}

System.out.println("infoRegresar = "+infoRegresar);

%>
<%=infoRegresar%>