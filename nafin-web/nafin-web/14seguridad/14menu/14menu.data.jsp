<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	netropology.utilerias.*,
        org.apache.commons.logging.Log,
	com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/14seguridad/14secsession_extjs.jspf" %>  

<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";

//JSONArray registros = new JSONArray();
//JSONObject 	jsonObj	= new JSONObject();	
String infoRegresar="";

log.debug("informacion: "+informacion);


if (informacion.equals("menus")){    //<------------------------------------------------------
	String tipoAfiliado = (request.getParameter("tipoAfiliado") != null) ? request.getParameter("tipoAfiliado") : "";
	if (tipoAfiliado.equals("")) {
		//Cuando rootVisible: true hace cosas raras el treegrid, por eso 
		//en se ocupa rootVisible: false y se agrega un nodo "/" 
		//pero esto provoca que se haga una carga automatica aun con autoLoad: false
		//Por eso se genera un arbol vacio
		infoRegresar = "[ ]";	
	} else {
		infoRegresar = Menu.generarArbolMenuJSON(Integer.parseInt(tipoAfiliado));
		infoRegresar = (infoRegresar ==  null || infoRegresar.equals(""))?"[ ]":infoRegresar;
	}
} else if (informacion.equals("menusAsociar")){   //<------------------------------------------------------
	infoRegresar = Menu.generarArbolMenuPerfilJSON(4, "ADMIN NAFIN");
} else if (informacion.equals("catalogoTipoAfiliado")){   //<------------------------------------------------------
	//Thread.sleep(1000 * 10);
	CatalogoSimple cat = new CatalogoSimple();       
	cat.setTabla("comcat_tipo_afiliado");
	cat.setCampoClave("ic_tipo_afiliado");
	cat.setCampoDescripcion("cd_afiliado");
	cat.setOrden("ic_tipo_afiliado");

	infoRegresar = cat.getJSONElementos();
}  else if (informacion.equals("agregarMenu")){   //<------------------------------------------------------
	JSONObject jObj = new JSONObject();

	String cc_menu_padre = (request.getParameter("cc_menu_padre") != null) ? request.getParameter("cc_menu_padre") : "";
	String cc_menu = (request.getParameter("cc_menu") != null) ? request.getParameter("cc_menu") : "";
	String cg_nombre = (request.getParameter("cg_nombre") != null) ? request.getParameter("cg_nombre") : "";
	String cg_descripcion = (request.getParameter("cg_descripcion") != null) ? request.getParameter("cg_descripcion") : "";
	String cg_url = (request.getParameter("cg_url") != null) ? request.getParameter("cg_url") : "";
	String cs_activo = (request.getParameter("cs_activo") != null) ? request.getParameter("cs_activo") : "";
	String cg_tipo = (request.getParameter("cg_tipo") != null) ? request.getParameter("cg_tipo") : "";
	String ic_tipo_afiliado = (request.getParameter("ic_tipo_afiliado") != null) ? request.getParameter("ic_tipo_afiliado") : "";
	
	OpcionMenu opcion = new OpcionMenu();
	opcion.setClaveTipoAfiliado(ic_tipo_afiliado);
	opcion.setClaveMenuPadre(cc_menu_padre);
	opcion.setClaveMenu(cc_menu);
	opcion.setNombre(cg_nombre);
	opcion.setDescripcion(cg_descripcion);
	opcion.setUrl(cg_url);
	opcion.setActivo(cs_activo);
	opcion.setTipo(cg_tipo);
	Map errores = OpcionMenuDAO.validar("insertar", opcion);
	if (errores.size() == 0) {
		OpcionMenuDAO.insertar(opcion);
		jObj.put("success", Boolean.TRUE);
		jObj.put("msg", "El alta se realizó exitosamente");
	} else {
		jObj.put("success", Boolean.FALSE);
		jObj.put("erroresValidacion", errores);
	}
	infoRegresar = jObj.toString();
} else if (informacion.equals("eliminarMenu")){   //<------------------------------------------------------
	JSONObject jObj = new JSONObject();
	String claveMenus = (request.getParameter("claveMenus") != null) ? request.getParameter("claveMenus") : "";
	String claveTipoAfiliado = (request.getParameter("claveTipoAfiliado") != null) ? request.getParameter("claveTipoAfiliado") : "";
	List lClavesMenus  = Comunes.explode(",", claveMenus);
	Iterator it = lClavesMenus.iterator();
	List lOpcionesMenu = new ArrayList();
	while(it.hasNext()) {
		String claveMenu = (String)it.next();
		OpcionMenu opcion = new OpcionMenu();
		opcion.setClaveMenu(claveMenu);
		opcion.setClaveTipoAfiliado(claveTipoAfiliado);
		lOpcionesMenu.add(opcion);
	}
	Map errores = OpcionMenuDAO.validar("eliminar", lOpcionesMenu);
	if (errores.size() == 0) {
		OpcionMenuDAO.eliminar(lOpcionesMenu);
		jObj.put("success", Boolean.TRUE);
		jObj.put("msg", "La eliminación se realizó exitosamente");
	} else {
		jObj.put("success", Boolean.FALSE);
		jObj.put("msg", "Proceso cancelado. " + errores.get("cc_menu"));
	}

	
	infoRegresar = jObj.toString();
	
}  else if (informacion.equals("modificarMenu")){   //<------------------------------------------------------

	JSONObject jObj = new JSONObject();

	String cc_menu = (request.getParameter("cc_menu") != null) ? request.getParameter("cc_menu") : "";
	String cg_tipo = (request.getParameter("cg_tipo") != null) ? request.getParameter("cg_tipo") : "";
	String ic_tipo_afiliado = (request.getParameter("ic_tipo_afiliado") != null) ? request.getParameter("ic_tipo_afiliado") : "";
	String cg_nombre = (request.getParameter("cg_nombre") != null) ? request.getParameter("cg_nombre") : "";
	String cg_descripcion = (request.getParameter("cg_descripcion") != null) ? request.getParameter("cg_descripcion") : "";
	String cg_url = (request.getParameter("cg_url") != null) ? request.getParameter("cg_url") : "";
	String cs_activo = (request.getParameter("cs_activo") != null) ? request.getParameter("cs_activo") : "";
	
	OpcionMenu opcion = new OpcionMenu();
	opcion.setClaveTipoAfiliado(ic_tipo_afiliado);
	opcion.setTipo(cg_tipo);
	opcion.setClaveMenu(cc_menu);
	opcion.setNombre(cg_nombre);
	opcion.setDescripcion(cg_descripcion);
	opcion.setUrl(cg_url);
	opcion.setActivo(cs_activo);
	
	Map errores = OpcionMenuDAO.validar("actualizar", opcion);
	if (errores.size()==0) {
		OpcionMenuDAO.actualizar(opcion);
		jObj.put("success", Boolean.TRUE);
		jObj.put("msg", "La actualización se realizó exitosamente");
	} else {
		jObj.put("success", Boolean.FALSE);
		jObj.put("erroresValidacion", errores);
	}
	infoRegresar = jObj.toString();
}
log.debug("infoRegresar: "+infoRegresar);
%>
<%=infoRegresar%>