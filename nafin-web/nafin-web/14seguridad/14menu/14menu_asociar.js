Ext.onReady(function() {

//_--------------------------------- HANDLERS -------------------------------

var procesarSuccessFailureGuardarCambios = function(options, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	Ext.getCmp('btnGuardarCambios').enable();
	if (success == true && Ext.JSON.decode(response.responseText).success == true) {
		Ext.MessageBox.alert('Cambios Realizados','Los cambios han sido realizados con �xito');
	} else {
		NE.util.mostrarErrorPeticion(response);
	}
	ts.reload();
}


var procesarSuccessFailureRegenerarMenus = function(options, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	maskRegenerar.hide();
	Ext.getCmp('btnRegenerarMenu').enable();
	if (success == true && Ext.JSON.decode(response.responseText).success == true) {
		Ext.MessageBox.alert('Cambios Realizados','La regeneraci�n de los menus por perfil, para esta instancia ha sido realizada con �xito');
	} else {
		NE.util.mostrarErrorPeticion(response);
	}
}

var procesarSuccessFailureAfilAplica = function(options, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	maskRegenerar.hide();
	Ext.getCmp('btnRegenerarMenu').enable();
	if (success == true && Ext.JSON.decode(response.responseText).success == true) {
		Ext.MessageBox.alert('Cambios Realizados','Guardado con �xito', function(){
			Ext.getCmp('winAfiliados').hide();
		});
	} else {
		NE.util.mostrarErrorPeticion(response);
	}
}


function findColumnByDataIndex(grid, dataIndex) {
    var selector = "gridcolumn[dataIndex=" + dataIndex + "]";
    return grid.down(selector);
};


var procesarConsultaAfiliados = function(store,  arrRegistros, success, opts) {

		if (arrRegistros != null) {
			if(store.getTotalCount() >= 0) {
				var cmHide = findColumnByDataIndex(	gridAfiliados,'cs_excluir');
				
				if(afiliadosData.tipoRest=='D'){
					if(cmHide){
                  cmHide.show();
					}
				}
				
				if(afiliadosData.tipoRest=='R'){
					if(cmHide){
                  cmHide.hide();
					}
				}
				
				if(Ext.getCmp('winAfiliados')){
					Ext.getCmp('winAfiliados').show();
				}else{
					Ext.create('Ext.window.Window', {
						title: 'Afiliados',
						id: 'winAfiliados',
						width: 600,
						height: 230,
						x: 120,
						y: 120,
						layout: 'fit',
						items: gridAfiliados,
						modal: true,
						closeAction: 'hide'
					}).show();
				}
				
				//el.unmask();
			}else {
				//el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};
	
var procesarCatalogoAfilidos = function(store,  arrRegistros, success, opts) {

	if (arrRegistros != null) {
		//var el = gridAfiliados.getEl();
		if(store.getTotalCount() > 0) {
			
			afiliadosData.load({
				params: {
					tipoAfiliado: Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue(),
					perfil: Ext.getCmp('hdnPerfilSeleccionado').getValue(),
					cc_menu: afiliadosData.ccmenu,
					tipoRestriccion: afiliadosData.tipoRest
				}
			});
			
		}else {
			//el.mask('No se encontr� ning�n registro', 'x-mask');
		}
	}
};


//-------------------------------- STORES -----------------------------------

Ext.define('Catalogo', {
    extend: 'Ext.data.Model',
    fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }
    ]
});

Ext.define('ListaAfiliados', {
    extend: 'Ext.data.Model',
    fields: [
			{ name: 'cg_tipo', type: 'string' },
			{ name: 'nombre_afiliado', type: 'string' },
			{ name: 'ic_nafin_electronico', type: 'string' },
			{ name: 'cs_excluir', type: 'boolean' },
			{ name: 'cs_borrar', type: 'boolean' }
			
    ]
});

var catalogoTipoAfiliadoData = Ext.create('Ext.data.Store', {
	model: 'Catalogo',
	proxy: {
		type: 'ajax',
		url: '14menu_asociar.data.jsp',
		reader: {
			type: 'json',
			root: 'registros'
		},
		extraParams: {
			informacion: 'catalogoTipoAfiliado'
		},
		listeners: {
			exception: NE.util.mostrarProxyAjaxError
		}
	}
});

var catalogoPerfilData = Ext.create('Ext.data.Store', {
	model: 'Catalogo',
	proxy: {
		type: 'ajax',
		url: '14menu_asociar.data.jsp',
		reader: {
			type: 'json',
			root: 'registros'
		},
		extraParams: {
			informacion: 'catalogoPerfil'
		},
		listeners: {
			exception: NE.util.mostrarProxyAjaxError
		}
	}
});


var catalogoEpoData = Ext.create('Ext.data.Store', {
	model: 'Catalogo',
	proxy: {
		type: 'ajax',
		url: '14menu_asociar.data.jsp',
		reader: {
			type: 'json',
			root: 'registros'
		},
		extraParams: {
			informacion: 'catalogoEpo'
		},
		listeners: {
			exception: NE.util.mostrarProxyAjaxError
		}
	}
});

var catalogoAfiliadoData = Ext.create('Ext.data.Store', {
	model: 'Catalogo',
	//autoLoad:true,
	proxy: {
		type: 'ajax',
		url: '14menu_asociar.data.jsp',
		reader: {
			type: 'json',
			root: 'registros'
		},
		extraParams: {
			informacion: 'catalogoAfiliado'
			//tipoAfilAsoc: 'I'
		},
		listeners: {
			exception: NE.util.mostrarProxyAjaxError
		}
	},
	listeners: {
			load: procesarCatalogoAfilidos
		}
});

var afiliadosData = Ext.create('Ext.data.Store', {
	model: 'ListaAfiliados',
	tipoRest: 'D',
	proxy: {
		type: 'ajax',
		url: '14menu_asociar.data.jsp',
		reader: {
			type: 'json',
			root: 'registros'
		},
		extraParams: {
			informacion: 'listaAfiliados'
		},
		listeners: {
			exception: NE.util.mostrarProxyAjaxError
		}
	},
	autoLoad: false,
	listeners: {
			load: procesarConsultaAfiliados
		}
});

Ext.define("NE.model.tree.MenuRel",{
	extend: 'Ext.data.Model',
	fields: [
		{name:"cc_menu",type:"string"},
		{name:"cg_nombre",type:"string"},
		{name:"cg_descripcion",type:"string"},
		{name:"ig_nivel",type:"string"},
		{name:"cc_menu_padre",type:"string"},
		{name:"cg_tipo",type:"string"},
		//{name:"relacionada",type:"string", convert: function(v, record){return v=='S'}},
		{name:"relacionada",type:"boolean"},
		{name:"ig_posicion",type:"string"},
		{name:"cs_activo",type:"string"},
		{name:"cs_acceso_directo",type:"string"},
		{name:"df_alta",type:"string"},
		{name:"nombre_afiliado",type:"string"},	//Nombre del afiliado al que esta restringido la opci�n de menu
		{name:"nombre_afiliado_rel",type:"string"},	//Nombre del afiliado al con el que se debe estar relacionado para poder ver la opci�n de menu
		{name:"ic_clave_afiliado",type:"string"},
		{name:"cg_tipo_afiliado",type:"string"},
		{name:"ic_clave_afiliado_rel",type:"string"},
		{name:"cg_tipo_afiliado_rel",type:"string"},
		{name:"cg_conteo_d",type:"string"},
		{name:"cg_conteo_r",type:"string"}
	]
});

var ts = new Ext.data.TreeStore({
	model: 'NE.model.tree.MenuRel',
	proxy: {
		type: 'ajax',
		url: '14menu_asociar.data.jsp',
		extraParams: {
			informacion: 'menusAsociar'
		},
		listeners: {
			exception: NE.util.mostrarProxyAjaxError
		}
	},
	autoLoad: false,
	folderSort: false,
	listeners: {
		load: {
			fn: function(store, node, records, successful, eOpts) {
				//Aun cuando aun no se haga la carga del treeStore, se lanza una petici�n
				//Que regresa un objeto vaci�. por lo que ese evento no es una carga real
				//Asi que solo se considera este evento si ya hay un tipo de afiliado seleccionado
				if (!Ext.isEmpty(Ext.getCmp('tipoAfiliado').getValue()) ) {
					Ext.getCmp('btnConsultar').enable();
					var tree = Ext.getCmp('tg');
					tree.setLoading(false);
				}
			}
		}
	}
})
//-------------------------------- COMPONENTES -----------------------------------
var info = '<img src="' + NE.appWebContextRoot + '/00utils/gif/help.png">';


var fp = Ext.widget({
	xtype: 'form',
	frame: true,
	title: 'Seleccione',
	bodyPadding: '10 10 10',
	width: 500,
	fieldDefaults: {
		msgTarget: 'side',
		anchor: '100%',
		labelWidth: 100
	},
	items: [{
		xtype: 'combobox',
		id: 'tipoAfiliado',
		forceSelection: true,
		fieldLabel: 'Tipo de Afiliado',
//		afterLabelTextTpl: info,
		name: 'ic_tipo_afiliado',
		allowBlank: false,
		store: catalogoTipoAfiliadoData,
		valueField: 'clave',
		displayField: 'descripcion',
		typeAhead: true,
		queryMode: 'local',
		listeners: {
			boxready: function(combo, width, height, eOpts) {
				//Workaround extjs 4.2.0 debido a que la mascara de carga la pone fuera del componente
				//al parecer porque aun no existe el "picker" que es la caja donde salen
				//las opciones del combo.
				combo.expand();
				combo.collapse();
			},
			/*change: function(combo, valorNuevo, valorAnterior, eOpts) {
					//El change no debe ocuparse debido a que se lanza cuando se teclea alguna letra en el combo
					//usar select en su lugar.
			}*/
			select: function(combo, arrRecords, eOpts) {
				catalogoPerfilData.load({
					params: {
						tipoAfiliado: combo.getValue()
					}
				});
			}
		}
	},
	{
		xtype: 'combobox',
		id: 'perfil',
		forceSelection: true,
		fieldLabel: 'Perfil',
		//afterLabelTextTpl: required,
		name: 'cc_perfil',
		allowBlank: false,
		store: catalogoPerfilData,
		valueField: 'clave',
		displayField: 'descripcion',
		typeAhead: true,
		queryMode: 'local',
		listeners: {
			boxready: function(combo, width, height, eOpts) {
				//Workaround extjs 4.2.0 debido a que la mascara de carga la pone fuera del componente
				//al parecer porque aun no existe el "picker" que es la caja donde salen
				//las opciones del combo.
				combo.expand();
				combo.collapse();
			}
		}
	},
	{
		xtype: 'hiddenfield',
		id: 'hdnTipoAfiliadoSeleccionado'
	},
	{
		xtype: 'hiddenfield',
		id: 'hdnPerfilSeleccionado'
	}
	],
	buttons: [
	{
		text: 'Consultar',
		id: 'btnConsultar',
		iconCls: 'icoBuscar',
		handler: function(btn, ev) {
			var form = this.up('form').getForm();
			if (form.isValid()) {
				btn.disable();
				var tree = Ext.getCmp('tg');
				tree.show();
				tree.setLoading(true);
				var tipoAfiliadoSeleccionado = Ext.getCmp('tipoAfiliado').getValue();
				var perfil = Ext.getCmp('perfil').getValue();
				ts.load(
					{
						params: {
							tipoAfiliado: tipoAfiliadoSeleccionado,
							perfil: perfil
						}
					}
				);
				Ext.getCmp('hdnTipoAfiliadoSeleccionado').setValue(tipoAfiliadoSeleccionado);
				Ext.getCmp('hdnPerfilSeleccionado').setValue(perfil);
			}
		}
	},
	{
		text: 'Limpiar',
		iconCls: 'icoLimpiar',
		handler: function(boton, evento) {
			window.location = '14menu_asociar.jsp';
		}
	}
	]
});

var fpModificar = {
	xtype: 'form',
	id: 'formaModificar',
	url: '14menu_asociar.data.jsp',
	frame: true,
	bodyPadding: '10 10 10',
	width: 600,
	fieldDefaults: {
		msgTarget: 'side',
		anchor: '100%',
		labelWidth: 200
	},
	plugins: {
		ptype: 'datatip'
	},
	items: [
		{
			xtype: 'textfield',
			fieldLabel: 'Clave',
			name: 'cc_menu',
			readOnly: true,
			cls: 'x-item-disabled',
			enableKeyEvents: true,  //Para poder interceptar el backspace con el listener "keydown"
			listeners: {
				keydown: NE.util.ignorarBackspace
			}
		},
		{
			xtype: 'radiogroup',
			id: 'accesoDirectoModificar',
			fieldLabel: 'Acceso Directo',
			afterLabelTextTpl: info,
			tooltip: 'Permite elegir si un men� aparece directamente en el menu lateral en la secci�n de "Mis servicios"',
			defaultType: 'radiofield',
			anchor: '60%',
			columns: 2,
			items: [
				{
					xtype: 'radiofield',
					boxLabel: 'Si',
					name: 'cs_acceso_directo',
					inputValue: 'S'
				},
				{
					xtype: 'radiofield',
					boxLabel: 'No',
					name: 'cs_acceso_directo',
					inputValue: 'N'
				}
			]
		},
		{
			xtype: 'radiogroup',
			fieldLabel: 'Activo',
			afterLabelTextTpl: info,
			tooltip: 'Permite especificar si la relaci�n est� activa a no. De no estarla, la opci�n no ser� contemplada para el armado de men� que ve el usuario',
			defaultType: 'radiofield',
			anchor: '60%',
			columns: 2,
			items: [
				{
					xtype: 'radiofield',
					boxLabel: 'Si',
					name: 'cs_activo',
					inputValue: 'S'
				},
				{
					xtype: 'radiofield',
					boxLabel: 'No',
					name: 'cs_activo',
					inputValue: 'N'
				}
			]
		}
		/*{
			xtype: 'combobox',
			id: 'tipoAfiliadoModificar',
			forceSelection: true,
			fieldLabel: 'Tipo de Afiliado',
			afterLabelTextTpl: info,
			tooltip: 'Permite especificar el tipo de afiliado al que se refiere el siguiente campo de "Afiliado"',
			allowBlank: true,
			store: {
				xtype: 'store',
				autoDestroy: true,
				model: 'Catalogo',
				data: [{"clave":"", "descripcion":"No aplica restricci�n"},{"clave":"E", "descripcion":"EPO"}, {"clave":"I", "descripcion":"IF"}]
			},
			valueField: 'clave',
			displayField: 'descripcion',
			name: 'cg_tipo_afiliado',
			typeAhead: true,
			queryMode: 'local',
			listeners: {
				change: function(combo, valorNuevo, valorAnterior, eOpts) {
					if ((valorNuevo == 'E' || valorNuevo == 'I') && catalogoEpoData.getCount() == 0) {
						catalogoAfiliadoData.load({
							params:{
								tipoAfilAsoc:valorNuevo
							}
						});
					}else{
						catalogoAfiliadoData.removeAll();
					}
				}
			}
		},
		{
			xtype: 'combobox',
			id: 'claveAfiliadoModificar',
			forceSelection: true,
			fieldLabel: 'Afiliado',
			afterLabelTextTpl: info,
			tooltip: 'Permite especificar que una opci�n de men� solo aparezca para el usuario con esta clave de afiliado',
			name: 'ic_clave_afiliado',
			allowBlank: true,
			store: catalogoAfiliadoData,
			valueField: 'clave',
			displayField: 'descripcion',
			typeAhead: true,
			multiSelect: true,
			queryMode: 'local',
			listeners: {
				boxready: function(combo, width, height, eOpts) {
					//Workaround extjs 4.2.0 debido a que la mascara de carga la pone fuera del componente
					//al parecer porque aun no existe el "picker" que es la caja donde salen
					//las opciones del combo.
					combo.expand();
					combo.collapse();
				}
			}
		},
		{
			xtype: 'combobox',
			id: 'tipoAfiliadoRelModificar',
			forceSelection: true,
			fieldLabel: 'Tipo de Afiliado Relacionado',
			afterLabelTextTpl: info,
			tooltip: 'Permite especificar el tipo de afiliado al que se refiere el siguiente campo de "Afiliado Relacionado"',
			name: 'cg_tipo_afiliado_rel',
			allowBlank: false,
			store: {
				xtype: 'store',
				autoDestroy: true,
				model: 'Catalogo',
				data: [{"clave":"", "descripcion":"No aplica restricci�n"}, {"clave":"E", "descripcion":"EPO"}]
			},
			valueField: 'clave',
			displayField: 'descripcion',
			typeAhead: true,
			queryMode: 'local',
			listeners: {
				change: function(combo, valorNuevo, valorAnterior, eOpts) {
					if (valorNuevo == 'E' && catalogoEpoData.getCount() == 0) {
						catalogoEpoData.load();
					}else{
						catalogoEpoData.removeAll();
					}
				}
			}
		},
		{
			xtype: 'combobox',
			id: 'claveAfiliadoRelModificar',
			forceSelection: true,
			fieldLabel: 'Afiliado Relacionado',
			afterLabelTextTpl: info,
			tooltip: 'Permite especificar que una opci�n de men� solo aparezca para afiliados que tengan relaci�n con especificado en este campo',
			name: 'ic_clave_afiliado_rel',
			allowBlank: true,
			multiSelect: true,
			store: catalogoEpoData,
			valueField: 'clave',
			displayField: 'descripcion',
			typeAhead: true,
			queryMode: 'local',
			listeners: {
				boxready: function(combo, width, height, eOpts) {
					//Workaround extjs 4.2.0 debido a que la mascara de carga la pone fuera del componente
					//al parecer porque aun no existe el "picker" que es la caja donde salen
					//las opciones del combo.
					combo.expand();
					combo.collapse();
				}
			}
		}*/
	],
	buttons: [
	{
		text: 'Actualizar',
		iconCls: 'icoAceptar',
		handler: function(btn, ev) {
			btn.disable();
			var form = this.up('form').getForm();
			if (form.isValid()) {
				Ext.getCmp('accesoDirectoModificar').enable();
				form.submit({
					params: {
						informacion: 'modificarMenuPerfil',
						tipoAfiliado: Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue(),
						perfil: Ext.getCmp('hdnPerfilSeleccionado').getValue()
					},
					success: function(form, action) {
						//btn.enable();  //NO es necesario habilitarlo porque se destruye la ventana
						Ext.Msg.alert('Exito', action.result.msg);
						ts.reload();
						Ext.getCmp('winModificar').close();
					},
					failure: function(form, action) {
						btn.enable();	//Se habilita el bot�n si hubo error
						var jsnObj = Ext.JSON.decode(action.response.responseText);
						if (jsnObj.erroresValidacion) {
							//Existen errores de validaci�n
							form.markInvalid(jsnObj.erroresValidacion);
						} else {
							NE.util.mostrarErrorPeticion(action.response);	//Cambian los metodos de error, debido a que el decode cambio de paquete en Extjs4
						}
					}
				});
			} else {
				btn.enable(); //Se habilita el bot�n si hubo error de validaci�n
			}
		}
	},
	{
		text: 'Cancelar',
		iconCls: 'icoCancelar',
		handler: function(btn, evt) {
			Ext.getCmp('winModificar').close();
		}
	}
	]
};


/*
var sm = Ext.create('Ext.selection.CheckboxModel', {
	checkOnly: false, 
	showHeaderCheckbox: false, 
	mode: 'MULTI', 
	checkSelector: '.x-grid-cell-row-checker'}
);
*/

Ext.util.Format.comboRenderer = function(combo) {
        return function(value) {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField)
                    : combo.valueNotFoundText;
        }
    }

var comboAfil = new Ext.form.field.ComboBox({
					//xtype: 'combobox',
					id:'cmbAfiliadosGrid',
					forceSelection: true,
					allowBlank: false,
					store: catalogoAfiliadoData,
					valueField: 'clave',
					displayField: 'descripcion',
					typeAhead: true,
				   triggerAction: 'all',
				   selectOnTab: true,
					lazyRender: true,
               listClass: 'x-combo-list-small',
					queryMode: 'local',
					listeners: {
						boxready: function(combo, width, height, eOpts) {
								//Workaround extjs 4.2.0 debido a que la mascara de carga la pone fuera del componente
								//al parecer porque aun no existe el "picker" que es la caja donde salen
								//las opciones del combo.
								combo.expand();
								combo.collapse();
							}
						}
		 });

var gridAfiliados = Ext.create('Ext.grid.Panel', {
	  id: 'gdAfiliados',
	  store: afiliadosData,
	  plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
	  columns: [
			{
				text: "Tipo", 
				width: 60, 
				dataIndex: 'cg_tipo', 
				renderer:function(value,metadata,registro){
					var tipo = '';
					if(value=='E'){tipo='EPO';}
					else if(value=='I'){tipo='IF'}
					else {tipo=value}
					return tipo;
				}
			},
			{
				header: 'Afiliado',
				sortable: true,
				dataIndex: 'ic_nafin_electronico',
				width: 400,
				editor: comboAfil,
				renderer:function(value,metadata,registro){
					var record = comboAfil.findRecord(comboAfil.valueField, value);
					return record ? record.get(comboAfil.displayField) : comboAfil.valueNotFoundText;
				}
         },
			{
				xtype: 'checkcolumn',
				text: 'Excluir',
				width: 60,
				dataIndex: 'cs_excluir',
				//hidden:true,
				stopSelection: false
			},
			{
				xtype: 'checkcolumn',
				text: 'Borrar',
				width: 60,
				dataIndex: 'cs_borrar',
				stopSelection: false
			}
	  ],
	  dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    iconCls: 'icon-register-add',
                    text: 'Agregar',
                    scope: this,
                    handler: function(){
								
								var cveAfiliSelec = Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue();
								
								
								var afil = Ext.create('ListaAfiliados', {
									cg_tipo: ((cveAfiliSelec==2)?'I':'E'),
									nombre_afiliado: '',
									ic_nafin_electronico: '',
									cs_excluir:'',
									cs_borrar:''
								});
								afiliadosData.insert(0,afil);
						  }
                }, {
                    iconCls: 'icon-register-delete',
                    text: 'Borrar',
                    itemId: 'delete',
                    scope: this,
                    handler: function(){
								var recorsArray = [];
								afiliadosData.each(function(record){
									if(record.get('cs_borrar')){
										recorsArray.push(record);
									}
								});
								afiliadosData.remove(recorsArray);
						  }
                }]
            },
				{
					weight: 2,
					xtype: 'toolbar',
					dock: 'bottom',
					items: ['->', 
						{
							  text: 'Guardar',
							  //enableToggle: true,
							  //pressed: true,
							  iconCls: 'icoGuardar',
							  scope: this,
							  handler: function(btn){
									var registrosEnviar = [];
									var valido = true;
									afiliadosData.each(function(record){
										if(record.data['ic_nafin_electronico']==''){
											valido = false;
										}
										registrosEnviar.push(record.data);
									});
									
									if(valido){
										Ext.Ajax.request({
											url : '14menu_asociar.data.jsp',
											params : {
												informacion: 'asignaAfilAplica',
												tipoAfiliado: Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue(),
												perfil: Ext.getCmp('hdnPerfilSeleccionado').getValue(),
												tipoRest: afiliadosData.tipoRest,
												cc_menu: afiliadosData.ccmenu,
												registros: Ext.encode(registrosEnviar)
											},
											callback: procesarSuccessFailureAfilAplica
										});
									}else{
										Ext.Msg.alert('Aviso', 'Es necesario seleccionar todos los Afiliados');
									}
									
									
							  }
						  }
                ]
				}

				
		]
	  //width: 380,
	  //height: 200
 });


Ext.define('NE.view.tree.TreeGrid', {
	extend: 'Ext.tree.Panel',
	xtype: 'tree-grid',
	id: 'tg',
	title: 'Administraci�n de Men� por Perfil',
	emptyText: 'No se encontraron Registros',
	viewConfig: {
		deferEmptyText: false
	},
	hidden: true,
	
	height: 300,
	useArrows: false,
//	selModel: sm,
/*	listeners: {
		select: function(panel, record, index,eOpts) {
			claveMenuSeleccionado = record.get('cc_menu');
			
			if (operacionCheck == "CHECK" || operacionCheck == "UNCHECK") {
				operacionCheck = ""; //Se resetea el valor
				if (operacionCheck == "CHECK") {
					alert("autoCheck(" + claveMenuSeleccionado + ")");
				} else if (operacionCheck == "UNCHECK") {
					alert("autoUncheck(" + claveMenuSeleccionado + ")");
				}

			claveMenuSeleccionado = record.get('cc_menu');
				alert(record.get('cc_menu'));
				var nodoMenu = ts.getNodeById(claveMenu);
				var nodoPadre = nodoMenu.parentNode;
				var nodoHijo = nodoMenu;
				if (nodoMenu.isLeaf()) {
					while( nodoPadre.get('relacionada') == false ) {
						nodoPadre.set('relacionada', true);
						
						nodoHijo = nodoPadre;
						nodoPadre = nodoHijo.parentNode;
					}
				}
			}
		}
	},
*/
	rootVisible: true,
	multiSelect: false,
	singleExpand: false,
	style: {margin: '0 auto'},
	bbar: [
		'->',
		'-',
		{
			text: 'Regenerar Men�s x Perfil',
			id: 'btnRegenerarMenu',
			iconCls: 'icoRefrescar',
			tooltip: 'Fuerza la generaci�n de men�s por cada perfil existente',
			handler: function(btn, ev) {
				btn.disable();
				//main.getEl();
				
				maskRegenerar.show();
				Ext.Ajax.request({
					url : '14menu_asociar.data.jsp',
					params : {
						informacion: 'regenerar'
					},
					callback: procesarSuccessFailureRegenerarMenus
				});
			}
		},
		'-',
		{
			text: 'Guardar Cambios',
			iconCls: 'icoGuardar',
			id: 'btnGuardarCambios',
			handler: function(btn, ev) {
				btn.disable();
				var arrRecords = ts.getModifiedRecords();
				if(arrRecords.length == 0) {
					btn.enable();
					return;
				}
				var arrRegAsociar = [];
				var arrRegDesasociar = [];
				
				Ext.Msg.confirm('Confirmaci�n', 
						'�Est� seguro de realizar los cambios?',
						function(btn) {
							if (btn == 'yes') {
								Ext.Array.each(arrRecords, 
									function(v) {
										if (v.get('relacionada') == true) {
											arrRegAsociar.push(v.get('cc_menu'));
										} else {
											arrRegDesasociar.push(v.get('cc_menu'));
										}
									}
								);

								Ext.Ajax.request({
									url : '14menu_asociar.data.jsp',
									params : {
										informacion: 'guardarCambios',
										menusAsociar : Ext.JSON.encode(arrRegAsociar),
										menusDesasociar: Ext.JSON.encode(arrRegDesasociar),
										tipoAfiliado: Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue(),
										perfil: Ext.getCmp('hdnPerfilSeleccionado').getValue()
									},
									callback: procesarSuccessFailureGuardarCambios
								});
							} else {
								Ext.getCmp('btnGuardarCambios').enable();
							}
						}
				);
			}
		},
		{
			text: 'Vista Previa',
			iconCls: 'icoVistaPrevia',
			id: 'btnVistaPrevia',
			handler: function(){
				
				var perfil = Ext.getCmp('hdnPerfilSeleccionado').getValue();
				
				if(perfil!=''){
					maskRegenerar.show();
					Ext.Ajax.request({
						url : '14menu_asociar.data.jsp',
						params : {
							informacion: 'regenerar'
						},
						callback: function(options, success, response) {
								//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
								maskRegenerar.hide();
								Ext.getCmp('btnRegenerarMenu').enable();
								if (success == true && Ext.JSON.decode(response.responseText).success == true) {
									
									var perfil = Ext.getCmp('hdnPerfilSeleccionado').getValue();
									window.open("/nafin/14seguridad/14menu/14menu_vista_previa.jsp?perfil="+perfil,"","height=300,width=1000,scrollbars=no,resizable=yes,title=Resumen");
								} else {
									NE.util.mostrarErrorPeticion(response);
								}
							}
					});
				}
			}
		}
	],
	initComponent: function() {
		this.width = 945;
		Ext.apply(this, {
			store: ts,
			columns: [{
				xtype: 'treecolumn', //this is so we know which column will show the tree
				text: 'Menu',
				width: 300,
				sortable: false,
				dataIndex: 'cc_menu'
			},
			{
				text: 'Nombre',
				width: 300,
				dataIndex: 'cg_nombre',
				renderer: NE.util.addGridTooltip4,
				sortable: false
			}, 
			{
				xtype: 'checkcolumn',
				text: 'Asociada',
				width: 60,
				dataIndex: 'relacionada',
				stopSelection: false,
				listeners: {
					beforecheckchange: function (chkCol, rowIndex, checked, eOpts) {
						if (rowIndex == 0) {
							return false;
						}

						var store = Ext.getCmp('tg').getView().getSelectionModel().getStore();
						var record = store.getAt(rowIndex);
						var claveMenuSeleccionado = record.get('cc_menu');
						
						var nodoMenu = ts.getNodeById(claveMenuSeleccionado);
						var nodoPadre = nodoMenu.parentNode;

						if (!nodoMenu.isLeaf() && checked == false) {
							//Muestra la confirmaci�n y cancela la actualizaci�n
							//Y en cuando se de la confirmaci�n se deselecciona junto con todos los hijos
							Ext.Msg.confirm('Confirmaci�n', 
									'Al deseleccionar el men� todos los submenus y pantallas asociadas tambi�n lo ser�n <br>�Desea Continuar?',
									function(btn) {
										if (btn == 'yes') {
											record.set('relacionada', false);
											record.set('cs_activo', '');
											nodoMenu.cascadeBy(function(nodo) {
												nodo.set('relacionada', false);
												nodo.set('cs_activo', '');
											});
										}
									}
							);
							return false;
						}
					},
					checkchange: function(chkCol, rowIndex, checked, eOpts) {
						var store = Ext.getCmp('tg').getView().getSelectionModel().getStore();
						var record = store.getAt(rowIndex);
						var claveMenuSeleccionado = record.get('cc_menu');
						
						var nodoMenu = ts.getNodeById(claveMenuSeleccionado);
						var nodoPadre = nodoMenu.parentNode;

						if (checked == true) {
							record.set('cs_activo', 'S');
						} else {
							record.set('cs_activo', '');
						}
						
						if (checked == true) {
							//Si una opcion de menu se selecciona, automaticamente se seleccionan los menus padres no seleccionados
							while( nodoPadre.get('relacionada') == false ) {
								nodoPadre.set('relacionada', true);
								nodoPadre.set('cs_activo', 'S');
								nodoPadre = nodoPadre.parentNode;
							}
						}
					}
				},
				/*renderer: function(value, metadata, record, rowIndex, colIndex, store){
					var cadena = '';
					if (rowIndex == 0) {
						cadena = '';
					} else {
						cadena = (value == 'S')?'Si':'No';
					}
					return cadena;
				},*/
				sortable: false
			},
			{
				text: 'Acceso Directo',
				width: 90,
				dataIndex: 'cs_acceso_directo',
				sortable: false,
				renderer: function(value, metadata, registro, rowIndex, colIndex, store){
					var cadena='';
					if (rowIndex == 0 || registro.get('cg_tipo') != 'P') {
						//Si es el nodo raiz o no es una pantalla, no aplica como "acceso directo"
						cadena = '';
					} else {
						cadena = ( value == 'S'?'Si':'No' );
					}
					return cadena;
				}
			},
			{
				//xtype: 'checkcolumn',
				text: 'Activo',
				//header: 'Activo',
				dataIndex: 'cs_activo',
				width: 40,
				renderer: function(value, metadata, registro, rowIndex, colIndex, store){
					var cadena = '';
					if (registro.get('relacionada') == true && rowIndex > 0) {
						cadena = (value=='S')?'Si':'No';
					} else {
						cadena = '';
					}
					return cadena;
				}
				//stopSelection: false
			}, {
				text: 'Modif.',
				width: 40,
				menuDisabled: true,
				xtype: 'actioncolumn',
				tooltip: 'Modificar el registro',
				align: 'center',
				//iconCls: 'modificar',
				items: [
					{
						getClass: function(value, metadata, record) {
							if (record.get('cc_menu') != '/' && record.get('relacionada') == true && !record.dirty) {
								return 'modificar';
							} else {
								return '';
							}
						}
					}
				],
				handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
					if (record.get('cc_menu') != '/' && record.get('relacionada') == true && !record.dirty) {
						if (record.get('ic_clave_afiliado')!='' || record.get('ic_clave_afiliado_rel')!='') {
							catalogoEpoData.load();
						}
						Ext.create('Ext.window.Window', {
							title: 'Modificaci�n del Men�',
							id: 'winModificar',
							width: 600,
							height: 200,
							x: 120,
							y: 120,
							layout: 'fit',
							items: fpModificar,
							modal: true,
							closeAction: 'destroy'
						}).show();
						
						if (record.get('cg_tipo') != 'P') {
							//inhabilita el radio de acceso directo si no es una pantalla
							Ext.getCmp('accesoDirectoModificar').disable();
							/*
							Ext.getCmp('tipoAfiliadoModificar').disable();
							Ext.getCmp('tipoAfiliadoRelModificar').disable();
							Ext.getCmp('claveAfiliadoModificar').disable();
							Ext.getCmp('claveAfiliadoRelModificar').disable();
							*/
						}
						
						var forma = Ext.getCmp('formaModificar').getForm();
						//forma.loadRecord( ( (sm.getSelection())[0] ) );
						forma.loadRecord( record );
					}	
					//}
				}
				//isDisabled tiene un bug, porque si se modifica el valor de los campos despues del primer render, 
				//s� deshabilita la acci�n pero no se ve reflejado en el estilo de la imagen.
				/*isDisabled: function(view, rowIdx, colIdx, item, record) {
					var disabled = false;
					if (rowIdx == 0 || record.get('relacionada') == false || record.get('relacionada') == true && record.dirty) {
						disabled = true;
					}
					
					return disabled;
				}*/
			},
			{
				text: 'Fecha Alta',
				width: 100,
				dataIndex: 'df_alta',
				sortable: false
			},
			
			{
				text: 'Afiliado al que aplica',
				width: 120,
				menuDisabled: true,
				xtype: 'actioncolumn',
				tooltip: 'Ver Afiliados',
				align: 'center',
				//iconCls: 'modificar',
				items: [
					{
						getClass: function(value, metadata, record) {
							var cveAfiliSelec = Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue();
							//if (record.get('cc_menu') != '/' && record.get('relacionada') == true && record.get('cg_conteo_d') != '0' && !record.dirty) {
							if (record.get('cc_menu') != '/' && record.get('relacionada') == true  && !record.dirty && record.get('leaf')==true) {
								
								if(cveAfiliSelec==1 || cveAfiliSelec==2){
									return 'icon-register-edit';
								}else{
									return '';
								}
							
							} else {
								return '';
							}
						}
					}
				],
				handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
					var cveAfiliSelec = Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue();
					//if (record.get('cc_menu') != '/' && record.get('relacionada') == true && record.get('cg_conteo_d') != '0' && !record.dirty) {
					if (record.get('cc_menu') != '/' && record.get('relacionada') == true  && !record.dirty && record.get('leaf')==true) {
						if(cveAfiliSelec==1 || cveAfiliSelec==2){
							var cveAfiliSelec = Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue();
							catalogoAfiliadoData.load({
								params:{
									tipoAfilAsoc: ((cveAfiliSelec==2)?'I':'E')
								}
							});
							
							afiliadosData.tipoRest = 'D';
							afiliadosData.ccmenu = record.get('cc_menu');
						}
					}	
				}
			},
			{
				text: 'Afiliado Relacionado con el que aplica',
				width: 120,
				menuDisabled: true,
				xtype: 'actioncolumn',
				tooltip: 'Ver Afiliados',
				align: 'center',
				//iconCls: 'modificar',
				items: [
					{
						getClass: function(value, metadata, record) {
							var cveAfiliSelec = Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue();
							//if (record.get('cc_menu') != '/' && record.get('relacionada') == true && record.get('cg_conteo_r') != '0' && !record.dirty) {
							if (record.get('cc_menu') != '/' && record.get('relacionada') == true  && !record.dirty && record.get('leaf')==true) {
								if(cveAfiliSelec==1){
									return 'icon-register-edit';
								} else {
									return '';
								}
							} else {
								return '';
							}
						}
					}
				],
				handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
					var cveAfiliSelec = Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue();
					//if (record.get('cc_menu') != '/' && record.get('relacionada') == true && record.get('cg_conteo_r') != '0' && !record.dirty) {
					if (record.get('cc_menu') != '/' && record.get('relacionada') == true  && !record.dirty && record.get('leaf')==true) {
						
						if(cveAfiliSelec==1){
							var cveAfiliSelec = Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue();
							
							catalogoAfiliadoData.load({
								params:{
									tipoAfilAsoc: 'E'
								}
							});
							
							afiliadosData.tipoRest = 'R';
							afiliadosData.ccmenu = record.get('cc_menu');
						}
					}
				}

				
			}
			/*{
				text: 'Afiliado al que aplica',
				columns: [
					{
						text: 'Tipo',
						width: 50,
						dataIndex: 'cg_tipo_afiliado',
						sortable: false
					},
					{
						text: 'Nombre',
						width: 300,
						dataIndex: 'nombre_afiliado',
						sortable: false
					}
				]
			},
			{
				text: 'Afiliado Relacionado con el que aplica',
				columns: [
					{
						text: 'Tipo',
						width: 50,
						dataIndex: 'cg_tipo_afiliado_rel',
						sortable: false
					},
					{
						text: 'Nombre',
						width: 300,
						dataIndex: 'nombre_afiliado_rel',
						sortable: false
					}
				]
			}*/
			]

			/*,
			selType: 'cellmodel',    //Tanto el modelo de edicion por columna o renglon vuelven muy lento el grid. Suponemos que por la cantidad de registros
			plugins: [
				Ext.create('Ext.grid.plugin.CellEditing', { clicksToEdit: 1})
				// Ext.create('Ext.grid.plugin.RowEditing', {
				//	clicksToMoveEditor: 1,
				//	autoCancel: false
				//})
			]*/
		});
		this.callParent();
	}
});	 







//-------------------------------- PRINCIPAL -----------------------------------
/*	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,	
		items: [
			tree,
			NE.util.getEspaciador(20),
			treeChk
		]
	});
*/
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			fp,
			NE.util.getEspaciador(20),
			NE.view.tree.TreeGrid
		]
	});
//-------------------------------- INICIALIZACION -----------------------------------
	catalogoTipoAfiliadoData.load();
	
	Ext.getCmp('tipoAfiliado').focus();
	var maskRegenerar = new Ext.LoadMask(Ext.getCmp("tg"), {msg:"Regenerando XMLs de menus por perfil..."});
	
});