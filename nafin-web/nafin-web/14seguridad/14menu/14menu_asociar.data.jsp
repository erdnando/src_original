<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	netropology.utilerias.*,
        org.apache.commons.logging.Log,
	com.netro.afiliacion.AfiliadoInfo,
	com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/14seguridad/14secsession_extjs.jspf" %>  

<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>


<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";

String infoRegresar="";

log.debug("informacion: "+informacion);

String tipoAfiliado = (request.getParameter("tipoAfiliado") != null) ? request.getParameter("tipoAfiliado") : "";
String perfil = (request.getParameter("perfil") != null) ? request.getParameter("perfil") : "";

if (informacion.equals("menusAsociar")){    //<------------------------------------------------------
	//Thread.sleep(5000);
	if (tipoAfiliado.equals("")) {
		//Cuando rootVisible: true hace cosas raras el treegrid, por eso 
		//en se ocupa rootVisible: false y se agrega un nodo "/" 
		//pero esto provoca que se haga una carga automatica aun con autoLoad: false
		//Por eso se genera un arbol vacio
		infoRegresar = "[ ]";	
	} else {
		infoRegresar = Menu.generarArbolMenuPerfilJSON(Integer.parseInt(tipoAfiliado), perfil);
		infoRegresar = (infoRegresar ==  null || infoRegresar.equals(""))?"[ ]":infoRegresar;
	}
} else if (informacion.equals("catalogoTipoAfiliado")){   //<------------------------------------------------------
	//Thread.sleep(1000 * 10);
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_tipo_afiliado");
	cat.setCampoClave("ic_tipo_afiliado");
	cat.setCampoDescripcion("cd_afiliado");
	cat.setOrden("ic_tipo_afiliado");

	infoRegresar = cat.getJSONElementos();
} else if (informacion.equals("catalogoPerfil")){   //<------------------------------------------------------
	//Thread.sleep(1000 * 10);


	CatalogoPerfil cat = new CatalogoPerfil();
	cat.setCampoClave("cc_perfil");
	cat.setCampoDescripcion("cc_perfil"); 
	cat.setClaveTipoAfiliado(tipoAfiliado);
	cat.setOrden("cc_perfil");
	infoRegresar = cat.getJSONElementos();
} else if (informacion.equals("catalogoEpo")){   //<------------------------------------------------------
	//Thread.sleep(1000 * 10);

	/*
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setTabla("comcat_epo");
	cat.setOrden("cg_razon_social");
	*/
	CatalogoEpoNafElec cat = new CatalogoEpoNafElec();
	cat.setCampoClave("cn.ic_nafin_electronico");
	cat.setCampoDescripcion("ce.cg_razon_social");
	cat.setOrden("ce.cg_razon_social");
	
	JSONObject jsonObj = JSONObject.fromObject(cat.getJSONElementos());
	JSONArray jsonArr = (JSONArray)jsonObj.get("registros");
	ElementoCatalogo el = new ElementoCatalogo();
	el.setClave("");
	el.setDescripcion("No aplica ninguna restriccion");
	jsonArr.add(0,(Object)el);
	infoRegresar = jsonObj.toString();
	
} else if (informacion.equals("catalogoAfiliado")){
	String tipoAfilAsoc = (request.getParameter("tipoAfilAsoc") != null) ? request.getParameter("tipoAfilAsoc") : "";
	JSONObject jsonObj = null;
	JSONArray jsonArr = null;
	
	if(tipoAfilAsoc.equals("E")){
		CatalogoEpoNafElec cat = new CatalogoEpoNafElec();
		cat.setCampoClave("cn.ic_nafin_electronico");
		cat.setCampoDescripcion("ce.cg_razon_social");
		cat.setOrden("ce.cg_razon_social");
		
		jsonObj = JSONObject.fromObject(cat.getJSONElementos());
		jsonArr = (JSONArray)jsonObj.get("registros");
	}
	if(tipoAfilAsoc.equals("I")){
		CatalogoIfNafElec cat = new CatalogoIfNafElec();
		cat.setCampoClave("cn.ic_nafin_electronico");
		cat.setCampoDescripcion("ci.cg_razon_social");
		cat.setOrden("ci.cg_razon_social");
		
		jsonObj = JSONObject.fromObject(cat.getJSONElementos());
		jsonArr = (JSONArray)jsonObj.get("registros");
	}
	
	
	
	
	//ElementoCatalogo el = new ElementoCatalogo();
	//el.setClave("");
	//el.setDescripcion("No aplica ninguna restriccion");
	//jsonArr.add(0,(Object)el);
	infoRegresar = jsonObj.toString();
} else if (informacion.equals("guardarCambios")){   //<------------------------------------------------------
	String menusAsociar = (request.getParameter("menusAsociar") != null) ? request.getParameter("menusAsociar") : "";
	String menusDesasociar = (request.getParameter("menusDesasociar") != null) ? request.getParameter("menusDesasociar") : "";
	List lMenusAsociar = JSONArray.fromObject(menusAsociar);
	List lMenusDesasociar = JSONArray.fromObject(menusDesasociar);	
	//System.out.println("----------------------------------------------------" +lMenusAsociar);
	//System.out.println("----------------------------------------------------" +lMenusDesasociar);
	
	List lMenusPerfilAsociar = new ArrayList();
	Iterator itAsoc = lMenusAsociar.iterator();
	while(itAsoc.hasNext()) {
		String claveMenu = (String)itAsoc.next();
		MenuPerfil menuPerfil = new MenuPerfil();
		menuPerfil.setClaveTipoAfiliado(tipoAfiliado);
		menuPerfil.setClavePerfil(perfil);
		menuPerfil.setClaveMenu(claveMenu);
		lMenusPerfilAsociar.add(menuPerfil);
	}
	if (lMenusPerfilAsociar.size()>0) {
		MenuPerfilDAO.asociarMenuXPerfil(lMenusPerfilAsociar);
	}

	List lMenusPerfilDesasociar = new ArrayList();
	Iterator itDesasoc = lMenusDesasociar.iterator();
	while(itDesasoc.hasNext()) {
		String claveMenu = (String)itDesasoc.next();
		MenuPerfil menuPerfil = new MenuPerfil();
		menuPerfil.setClaveTipoAfiliado(tipoAfiliado);
		menuPerfil.setClavePerfil(perfil);
		menuPerfil.setClaveMenu(claveMenu);
		lMenusPerfilDesasociar.add(menuPerfil);
	}
	
	if (lMenusPerfilDesasociar.size()>0) {
		MenuPerfilDAO.eliminar(lMenusPerfilDesasociar);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();
	
	
} else if (informacion.equals("listaAfiliados")){   //<------------------------------------------------------
	List lstAfiliados = new ArrayList();
	JSONObject jObj = new JSONObject();
	JSONArray jsonArr = new JSONArray();
	
	//String tipoAfiliado = (request.getParameter("tipoAfiliado") != null) ? request.getParameter("tipoAfiliado") : "";
	//String perfil = (request.getParameter("perfil") != null) ? request.getParameter("perfil") : "";
	String cc_menu = (request.getParameter("cc_menu") != null) ? request.getParameter("cc_menu") : "";
	String tipoRestriccion = (request.getParameter("tipoRestriccion") != null) ? request.getParameter("tipoRestriccion") : "";
	
	
	lstAfiliados = AfiliadoInfo.getNombresAfiliados(tipoAfiliado, perfil, cc_menu, tipoRestriccion);
	
	jsonArr = jsonArr.fromObject(lstAfiliados);
	
	jObj.put("registros", jsonArr.toString());
	jObj.put("success", Boolean.TRUE);
	infoRegresar = jObj.toString();
	System.out.println("infoRegresar===="+infoRegresar);
} else if (informacion.equals("modificarMenuPerfil")){   //<------------------------------------------------------

	JSONObject jObj = new JSONObject();

	String cc_menu = (request.getParameter("cc_menu") != null) ? request.getParameter("cc_menu") : "";
		
	String cs_acceso_directo = (request.getParameter("cs_acceso_directo") != null) ? request.getParameter("cs_acceso_directo") : "";
	String cs_activo = (request.getParameter("cs_activo") != null) ? request.getParameter("cs_activo") : "";
	String cg_tipo_afiliado = (request.getParameter("cg_tipo_afiliado") != null) ? request.getParameter("cg_tipo_afiliado") : "";
	String ic_clave_afiliado[] = request.getParameterValues("ic_clave_afiliado");
	String cg_tipo_afiliado_rel = (request.getParameter("cg_tipo_afiliado_rel") != null) ? request.getParameter("cg_tipo_afiliado_rel") : "";
	//String ic_clave_afiliado_rel = (request.getParameter("ic_clave_afiliado_rel") != null) ? request.getParameter("ic_clave_afiliado_rel") : "";
	String ic_clave_afiliado_rel[] = request.getParameterValues("ic_clave_afiliado_rel");
	
	
	/*
	String s_ic_clave_afiliado_rel = "";
	if(ic_clave_afiliado_rel!=null){
		for(int x=0; x<ic_clave_afiliado_rel.length;x++){
			s_ic_clave_afiliado_rel += (x==0)?ic_clave_afiliado_rel[x]:(","+ic_clave_afiliado_rel[x]);
		}
	}
	

	String s_ic_clave_afiliado = "";
	if(ic_clave_afiliado!=null){
		for(int x=0; x<ic_clave_afiliado.length;x++){
			s_ic_clave_afiliado += (x==0)?ic_clave_afiliado[x]:(","+ic_clave_afiliado[x]);
		}
	}
	*/
	
	
	
	MenuPerfil mp = new MenuPerfil();
	mp.setClaveTipoAfiliado(tipoAfiliado);
	mp.setClavePerfil(perfil);
	mp.setClaveMenu(cc_menu);
	
	mp.setAccesoDirecto(cs_acceso_directo);
	mp.setActivo(cs_activo);
	
	/*
	mp.setTipoAfiliado(cg_tipo_afiliado);
	mp.setClaveAfiliado(s_ic_clave_afiliado);
	
	mp.setTipoAfiliadoRelacionado(cg_tipo_afiliado_rel);
	mp.setClaveAfiliadoRelacionado(s_ic_clave_afiliado_rel);
	*/
	
	Map errores = MenuPerfilDAO.validar("actualizar", mp);
	if (errores.size()==0) {
		MenuPerfilDAO.actualizar(mp);
	
		jObj.put("success", Boolean.TRUE);
		jObj.put("msg", "La actualización se realizó exitosamente");
	} else {
		jObj.put("success", Boolean.FALSE);
		jObj.put("erroresValidacion", errores);
	}
	
	infoRegresar = jObj.toString();

} else if (informacion.equals("asignaAfilAplica")){   //<------------------------------------------------------

	JSONObject jObj = new JSONObject();

	String cc_menu = (request.getParameter("cc_menu") != null) ? request.getParameter("cc_menu") : "";
	String cs_acceso_directo = (request.getParameter("cs_acceso_directo") != null) ? request.getParameter("cs_acceso_directo") : "";
	String cs_activo = (request.getParameter("cs_activo") != null) ? request.getParameter("cs_activo") : "";
	String cg_tipo_afiliado = (request.getParameter("cg_tipo_afiliado") != null) ? request.getParameter("cg_tipo_afiliado") : "";
	String tipoRest = (request.getParameter("tipoRest") != null) ? request.getParameter("tipoRest") : "D";
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	
	
	List arrRegistrosModificados = JSONArray.fromObject(jsonRegistros);
	System.out.println("arrRegistrosModificados========"+arrRegistrosModificados.size());
	Iterator itReg = arrRegistrosModificados.iterator();
	List lstAfilRel = new ArrayList();
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		HashMap mp = new HashMap();

		mp.put("NAFELECAFIL",registro.getString("ic_nafin_electronico"));
		mp.put("CSEXCLUIR","true".equals(registro.getString("cs_excluir"))?"S":"N");
		mp.put("TPRESTRICCION",tipoRest);
		
		lstAfilRel.add(mp);
	}

	
	System.out.println("cs_acceso_directo==="+cs_acceso_directo);
	System.out.println("cc_menu==="+cc_menu);
	
	MenuPerfil mp = new MenuPerfil();
	mp.setClaveTipoAfiliado(tipoAfiliado);
	mp.setClavePerfil(perfil);
	mp.setClaveMenu(cc_menu);
	mp.setTipoRestriccion(tipoRest);
	
	if("1".equals(tipoAfiliado))cg_tipo_afiliado = "E";
	if("2".equals(tipoAfiliado))cg_tipo_afiliado = "I";
	
	

	mp.setTipoAfiliado(cg_tipo_afiliado);
	mp.setLstAfilRelacionados(lstAfilRel);

	//Map errores = MenuPerfilDAO.validar("actualizar", mp);
	//if (errores.size()==0) {
	if (true) {
		MenuPerfilDAO.asignaAfiliadosRestringidos(mp);
	
		jObj.put("success", Boolean.TRUE);
		jObj.put("msg", "La actualización se realizó exitosamente");
	} else {
		jObj.put("success", Boolean.FALSE);
		//jObj.put("erroresValidacion", errores);
	}
	
	infoRegresar = jObj.toString();

} else if (informacion.equals("regenerar")){    //<------------------------------------------------------
	//Realiza una regeneración forzosa de los XMLs de los menus x perfil
	JSONObject jObj = new JSONObject();
	//Thread.sleep(1000 * 10);
	Menu.regenerarMenus();
	jObj.put("success", Boolean.TRUE);
	infoRegresar = jObj.toString();
}
log.debug("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>