<!DOCTYPE html>
<%@ page import="
		java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/14seguridad/14secsession.jspf" %>

<%

String perfil = request.getParameter("perfil");
session.setAttribute("sesMenuPerfil",MenuUsuario.getMenuPerfil(perfil));

%>

<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs4.jspf" %>


<%
if (session.getAttribute("sesMenuPerfil") != null && !session.getAttribute("sesMenuPerfil").equals("") ) {

%>
<link rel="stylesheet" type="text/css" href="<%=appWebContextRoot%>/00utils/css/<%=session.getAttribute("sesDiseno")%>/dhtmlxmenu_dhx_nafin.css" />
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/dhtmlx/dhtmlxcommon.js"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/dhtmlx/dhtmlxmenu.js"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/dhtmlx/ext/dhtmlxmenu_ext.js"></script>

<script type="text/javascript">
var _menuApp;
var _rutaAccesoMenu = "";
Ext.onReady(function() {
	_menuApp = new dhtmlXMenuObject("_menuApp","dhx_nafin");
	_menuApp.loadXML("<%=appWebContextRoot%>/MenuServlet?informacion=ObtieneMenuPerfil", function(){});
	

});
</script>
<%
}
%>

<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs4/ux/DataTip.js"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	
	<form id='formAux' name="formAux" target='_new'></form>
</body>

</html>