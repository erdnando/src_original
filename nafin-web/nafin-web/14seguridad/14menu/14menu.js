Ext.onReady(function() {

//_--------------------------------- HANDLERS -------------------------------
var procesarSuccessFailureEliminar = function(options, success, response) {
	Ext.getCmp('btnEliminar').enable();
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.JSON.decode(response.responseText).success == true) {
		Ext.MessageBox.alert('Cambios Realizados','La eliminaci�n ha sido realizada con �xito');
	} else {
		NE.util.mostrarErrorPeticion(response);
	}

	ts.reload();
}
//-------------------------------- STORES -----------------------------------

Ext.define('Catalogo', {
    extend: 'Ext.data.Model',
    fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }
    ]
});


var catalogoTipoAfiliadoData = Ext.create('Ext.data.Store', {
	model: 'Catalogo',
	proxy: {
		type: 'ajax',
		url: '14menu.data.jsp',
		reader: {
			type: 'json',
			root: 'registros'
		},
		extraParams: {
			informacion: 'catalogoTipoAfiliado'
		},
		listeners: {
			exception: NE.util.mostrarProxyAjaxError
		}
	}
});

Ext.define("NE.model.tree.Menu",{
	extend: 'Ext.data.Model',
	fields: [
		{name:"cc_menu",type:"string"},
		{name:"cc_menu_padre",type:"string"},
		{name:"cg_nombre",type:"string"},
		{name:"cg_descripcion",type:"string"},
		{name:"cg_url",type:"string"},
		{name:"df_alta",type:"string"},
		{name:"cg_tipo",type:"string"},
		{name:"cs_activo",type:"string"},
		{name:"sel",type:"boolean"}
	]
});

var ts = Ext.create('Ext.data.TreeStore',{
	model: 'NE.model.tree.Menu',
	proxy: {
		type: 'ajax',
		url: '14menu.data.jsp',
		extraParams: {
			informacion: 'menus'
		},
		listeners: {
			exception: NE.util.mostrarProxyAjaxError
		}
	},
	autoLoad: false,
	folderSort: false,
	listeners: {
		load: {
			fn: function(store, node, records, successful, eOpts) {
				//Aun cuando aun no se haga la carga del treeStore, se lanza una petici�n
				//Que regresa un objeto vaci�. por lo que ese evento no es una carga real
				//Asi que solo se considera este evento si ya hay un tipo de afiliado seleccionado
				if (!Ext.isEmpty(Ext.getCmp('tipoAfiliado').getValue()) ) {
					Ext.getCmp('btnConsultar').enable();
					var treegrid = Ext.getCmp('treeGrid');
					treegrid.setLoading(false);
					//alert (records.length);
					//treegrid.getEl().mask("ccccccccccc");
				}
			}
		}
	}
});
//-------------------------------- COMPONENTES -----------------------------------

var fp = Ext.widget({
	xtype: 'form',
//	url: 'save-form.php',
	frame: true,
	title: 'Seleccione el tipo de afiliado',
	bodyPadding: '10 10 10',
	width: 500,
	fieldDefaults: {
		msgTarget: 'side',
		anchor: '100%',
		labelWidth: 100
	},
	//plugins: {
		//    ptype: 'datatip'
		//},
	//defaultType: 'textfield',
	items: [{
		xtype: 'combobox',
		id: 'tipoAfiliado',
		forceSelection: true,
		fieldLabel: 'Tipo de Afiliado',
		//afterLabelTextTpl: required,
		name: 'ic_tipo_afiliado',
		allowBlank: false,
		store: catalogoTipoAfiliadoData,
		valueField: 'clave',
		displayField: 'descripcion',
		typeAhead: true,
		queryMode: 'local',
		listeners: {
			boxready: function(combo, width, height, eOpts) {
				//Workaround extjs 4.2.0 debido a que la mascara de carga la pone fuera del componente
				//al parecer porque aun no existe el "picker" que es la caja donde salen
				//las opciones del combo.
				combo.expand();
				combo.collapse();
			}
		}
	},
	{
		xtype: 'hiddenfield',
		id: 'hdnTipoAfiliadoSeleccionado'
	}],
	buttons: [
	{
		text: 'Consultar',
		id: 'btnConsultar',
		iconCls: 'icoBuscar',
		handler: function(btn, ev) {
			var form = this.up('form').getForm();
			if (form.isValid()) {
				btn.disable();
				var tipoAfiliadoSeleccionado = Ext.getCmp('tipoAfiliado').getValue();
				var tg = Ext.getCmp('treeGrid');
				tg.show();
				tg.setLoading(true);
				ts.load(
					{
						params: {
							tipoAfiliado: tipoAfiliadoSeleccionado
						}
					}
				);
				Ext.getCmp('hdnTipoAfiliadoSeleccionado').setValue(tipoAfiliadoSeleccionado);
			}
		}
	},
	{
		text: 'Limpiar',
		iconCls: 'icoLimpiar',
		handler: function(btn, evt) {
			window.location = '14menu.jsp';
		}
	}
	]
});

var fpAgregar = {
	xtype: 'form',
	url: '14menu.data.jsp',
	frame: true,
//	title: 'Alta de Menu',
	bodyPadding: '10 10 10',
	width: 600,
	fieldDefaults: {
		msgTarget: 'side',
		anchor: '100%',
		labelWidth: 150
	},
	items: [
		{
			xtype: 'textfield',
			id: 'menuPadre',
			name: 'cc_menu_padre',
			fieldLabel: 'Clave del men� padre',
			readOnly: true,
			cls: 'x-item-disabled',
			enableKeyEvents: true,  //Para poder interceptar el backspace con el listener "keydown"
			listeners: {
				keydown: NE.util.ignorarBackspace
			}
		},
		{
			xtype: 'radiogroup',
			fieldLabel: 'Tipo',
			defaultType: 'radiofield',
			anchor: '70%',
			columns: 2,
			items: [
				{
					xtype: 'radiofield',
					boxLabel: 'Pantalla',
					name: 'cg_tipo',
					inputValue: 'P',
					checked: true
				},
				{
					xtype: 'radiofield',
					boxLabel: 'Menu o Submenu',
					name: 'cg_tipo',
					inputValue: 'M'
				}
			]
		},
		{
			xtype: 'textfield',
			id: 'claveMenu',
			fieldLabel: 'Clave',
			allowBlank: false,
			name: 'cc_menu'
		},
		{
			xtype: 'textfield',
			id: 'nombreMenu',
			fieldLabel: 'Nombre',
			allowBlank: false,
			name: 'cg_nombre'
		},
		{
			xtype: 'textfield',
			id: 'descripcionMenu',
			fieldLabel: 'Descripci�n',
			allowBlank: false,
			name: 'cg_descripcion'
		},
		{
			xtype: 'textfield',
			id: 'urlMenu',
			fieldLabel: 'URL',
			allowBlank: true,
			name: 'cg_url'
		},
		{
			xtype: 'radiogroup',
			fieldLabel: 'Activo',
			defaultType: 'radiofield',
			anchor: '50%',
			columns: 2,
			items: [
				{
					xtype: 'radiofield',
					id: 'activoMenuS',
					boxLabel: 'Si',
					name: 'cs_activo',
					inputValue: 'S',
					checked: true
				},
				{
					xtype: 'radiofield',
					id: 'activoMenuN',
					boxLabel: 'No',
					name: 'cs_activo',
					inputValue: 'N'
				}
			]
		}
	],
	buttons: [
	{
		text: 'Agregar',
		iconCls: 'icoAgregar',
		handler: function(btn, ev) {
			btn.disable();
			var form = this.up('form').getForm();
			if (form.isValid()) {
				form.submit({
					params: {
						informacion: 'agregarMenu',
						ic_tipo_afiliado: Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue()
					},
					success: function(form, action) {
						//btn.enable(); //No es necesario habilitar el bot�n, porque la pantalla se destruye
						Ext.Msg.alert('Exito', action.result.msg);
						ts.reload();
						Ext.getCmp('winAgregar').close();
					},
					failure: function(form, action) {
						btn.enable();		//Si hay error se habilita el boton de Agregar por si se desea reintentar
						var jsnObj = Ext.JSON.decode(action.response.responseText);
						if (jsnObj.erroresValidacion) {
							//Existen errores de validaci�n
							form.markInvalid(jsnObj.erroresValidacion);
						} else {
							NE.util.mostrarErrorPeticion(action.response);	//Cambian los metodos de error, debido a que el decode cambio de paquete en Extjs4
						}
					}
				});
			} else {
				btn.enable();	//Si hay error de validaci�n se vuelve a habilitar el boton para poder renviar la informaci�n
			}
		}
	},
	{
		text: 'Cancelar',
		iconCls: 'icoCancelar',
		handler: function(boton, evento) {
			Ext.getCmp('winAgregar').close();
		}
	}
	]
};


var fpModificar = {
	xtype: 'form',
	id: 'formaModificar',
	url: '14menu.data.jsp',
	frame: true,
	bodyPadding: '10 10 10',
	width: 600,
	fieldDefaults: {
		msgTarget: 'side',
		anchor: '100%',
		labelWidth: 150
	},
	items: [
		{
			xtype: 'displayfield',
			fieldLabel: 'Clave del men� padre',
			name: 'cc_menu_padre'
		},
		{
			xtype: 'radiogroup',
			id: 'rgTipoModificar',
			fieldLabel: 'Tipo',
			cls: 'x-item-disabled',
			defaultType: 'radiofield',
			anchor: '70%',
			columns: 2,
			items: [
				{
					xtype: 'radiofield',
					boxLabel: 'Pantalla',
					name: 'cg_tipo',
					inputValue: 'P',
					readOnly: true
				},
				{
					xtype: 'radiofield',
					boxLabel: 'Menu o Submenu',
					name: 'cg_tipo',
					inputValue: 'M',
					readOnly: true
				}
			]
		},

		/*{
			xtype: 'displayfield',
			id: 'dspTipoModificar',
			fieldLabel: 'Tipo',
			name: 'cg_tipo',
			renderer: function(val, field) {
				if (val=='P') {
					return 'Pantalla';
				} else {
					return 'Men�';
				}
			}
		},*/
		{
			xtype: 'textfield',
			fieldLabel: 'Clave',
			name: 'cc_menu',
			readOnly: true,
			cls: 'x-item-disabled',
			enableKeyEvents: true,  //Para poder interceptar el backspace con el listener "keydown"
			listeners: {
				keydown: NE.util.ignorarBackspace
			}
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Nombre',
			allowBlank: false,
			name: 'cg_nombre'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Descripci�n',
			allowBlank: false,
			name: 'cg_descripcion'
		},
		{
			xtype: 'textfield',
			id: 'txtUrlModificar',
			fieldLabel: 'URL',
			allowBlank: true,
			name: 'cg_url',
			readOnly: true,
			cls: 'x-item-disabled',
			enableKeyEvents: true,  //Para poder interceptar el backspace con el listener "keydown"
			listeners: {
				keydown: NE.util.ignorarBackspace
			}
		},
		{
			xtype: 'radiogroup',
			fieldLabel: 'Activo',
			defaultType: 'radiofield',
			anchor: '50%',
			columns: 2,
			items: [
				{
					xtype: 'radiofield',
					boxLabel: 'Si',
					name: 'cs_activo',
					inputValue: 'S'
				},
				{
					xtype: 'radiofield',
					boxLabel: 'No',
					name: 'cs_activo',
					inputValue: 'N'
				}
			]
		}
	],
	buttons: [
	{
		text: 'Actualizar',
		iconCls: 'icoGuardar',
		handler: function(btn, ev) {
			btn.disable();
			var form = this.up('form').getForm();
			if (form.isValid()) {
				form.submit({
					params: {
						informacion: 'modificarMenu',
						ic_tipo_afiliado: Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue()
					},
					success: function(form, action) {
						//btn.enable();  //NO es necesario habilitarlo porque se destruye la ventana
						Ext.Msg.alert('Exito', action.result.msg);
						ts.reload();
						Ext.getCmp('winModificar').close();
					},
					failure: function(form, action) {
						var jsnObj = Ext.JSON.decode(action.response.responseText);
						if (jsnObj.erroresValidacion) {
							//Existen errores de validaci�n
							form.markInvalid(jsnObj.erroresValidacion);
						} else {
							NE.util.mostrarErrorPeticion(action.response);	//Cambian los metodos de error, debido a que el decode cambio de paquete en Extjs4
						}
						btn.enable();	//Se habilita el bot�n si hubo error
					}
				});
			} else {
				btn.enable(); //Se habilita el bot�n si hubo error de validaci�n
			}
		}
	},
	{
		text: 'Cancelar',
		iconCls: 'icoCancelar',
		handler: function(btn, evt) {
			Ext.getCmp('winModificar').close();
		}
	}
	]
};


/*
var sm = Ext.create('Ext.selection.CheckboxModel', {
	checkOnly: false, 
	showHeaderCheckbox: false, 
	mode: 'MULTI', 
	checkSelector: '.x-grid-cell-row-checker'
});
*/

Ext.define('NE.view.tree.TreeGrid', {
	id: 'treeGrid',
	extend: 'Ext.tree.Panel',
	xtype: 'tree-grid',
	title: 'Administraci�n de Men�',
	emptyText: 'No se encontraron Registros',
	viewConfig: {
		deferEmptyText: false
	},
	hidden: true,
	height: 300,
	useArrows: false,
	//selModel: sm,
	rootVisible: true,
	multiSelect: false,
	singleExpand: false,
	style: {margin: '0 auto'},
	bbar: [
		'->',
		'-',
		{
			text: 'Eliminar',
			iconCls: 'icoEliminar',
			id: 'btnEliminar',
			handler: function(btn, ev) {
				btn.disable();
				var arrRecords = ts.getModifiedRecords();
				if(arrRecords.length == 0) {
					btn.enable();
					Ext.Msg.alert('Error Eliminaci�n', 'Favor de seleccionar al menos un registro para realizar la eliminaci�n');
					return;
				} else {
					var claveMenus = "";
					Ext.Msg.confirm('Confirmaci�n', 
						'�Est� seguro de realizar la eliminaci�n de los registros?',
						function(btn) {
							if (btn == 'yes') {
								Ext.Array.each(arrRecords, 
									function(v) {
										claveMenus = claveMenus + v.get('cc_menu') + ",";
									}
								);
								
								//Se podria codificar un arreglo como json y asi enviar la cadena, pero para esta pantalla basta con concatenar las claves
								//clavesMenu = Ext.JSON.encode(arrClaves);
								Ext.Ajax.request({
									url : '14menu.data.jsp',
									params : {
										informacion: 'eliminarMenu',
										claveMenus: claveMenus,
										claveTipoAfiliado: Ext.getCmp('hdnTipoAfiliadoSeleccionado').getValue()
									},
									callback: procesarSuccessFailureEliminar
								});
							} else {
								Ext.getCmp('btnEliminar').enable();
							}
						}
					)
				}
			}
		}
	],
	initComponent: function() {
		this.width = 945;
		Ext.apply(this, {
			store: ts,
			columns: [
			{
				xtype: 'checkcolumn',
				text: 'Sel.',
				width: 30,
				dataIndex: 'sel',
				stopSelection: false,
				listeners: {
					beforecheckchange: function (chkCol, rowIndex, checked, eOpts) {
						if (rowIndex == 0) {
							return false;
						}
					},
					checkchange: function (chkCol, rowIndex, checked, eOpts) {
							//Se trat� de poner una m�scara cuando se realiza la operaci�n
							//de cascadeBy pero no funciona... ya que la m�scara se pinta
							//una vez que termina este m�todo.. :-?
							//Se intent� con varios listeners sobre la m�scara pero ninguno
							//funcion�... La unica forma de hacerlo funcionar es poner un
							//alert de javascript despues de mascaraX.show()... �?
							var store = Ext.getCmp('treeGrid').getView().getSelectionModel().getStore();
							var record = store.getAt(rowIndex);
							var claveMenuSeleccionado = record.get('cc_menu');
											
							var nodoMenu = ts.getNodeById(claveMenuSeleccionado);
							//var nodoPadre = nodoMenu.parentNode;
							
							if (!nodoMenu.isLeaf()) {
								nodoMenu.cascadeBy(function(nodo) {
									nodo.set('sel', checked);
								});
							}
					}
				},
				sortable: false
			},
			{
				xtype: 'treecolumn', //this is so we know which column will show the tree
				text: '<center>Menu</center>',
				flex: 2,
				sortable: false,
				dataIndex: 'cc_menu'
			},
			{
				text: 'Nombre',
				flex: 1,
				dataIndex: 'cg_nombre',
				renderer: NE.util.addGridTooltip4,
				sortable: false/*,		//Se hace muy lenta la edici�n por eso no escogimos el esquema de editar directamente el grid
				editor: {
					xtype: 'textfield',
					allowBlank: false
				}*/
			}, 
			{
				text: 'Descripcion',
				flex: 1,
				dataIndex: 'cg_descripcion',
				renderer: NE.util.addGridTooltip4,
				sortable: false/*,
				editor: {
					xtype: 'textfield',
					allowBlank: false
				}*/
			},
			{
				text: 'Url',
				flex: 1,
				dataIndex: 'cg_url',
				renderer: NE.util.addGridTooltip4,
				sortable: false/*,
				editor: {
					xtype: 'textfield',
					allowBlank: false
				}*/
			},
			{
				text: 'Fecha Alta',
				flex: 1,
				dataIndex: 'df_alta',
				sortable: false
			},
			{
				//xtype: 'checkcolumn',
				text: 'Activo',
				//header: 'Activo',
				dataIndex: 'cs_activo',
				width: 40,
				renderer: function(value, metadata, registro, rowindex, colindex, store){
					return (value=='S' || registro.get('cc_menu')=='/')?'Si':'No';
				}
				//stopSelection: false
			}, {
				text: 'Modif.',
				width: 40,
				menuDisabled: true,
				xtype: 'actioncolumn',
				tooltip: 'Modificar el registro',
				align: 'center',
				iconCls: 'icoModificar',
				handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
					//var arrModel = sm.getSelection();
					//if(arrModel.length !=1) {
					//	alert("aqui iria un mensaje de extjs indicando que es necesario seleccionar solo un registro");
					//	sm.deselectAll();
					//} else {
						//alert('Modificar **' + arrModel[0].get('cc_menu') + '**');
						Ext.create('Ext.window.Window', {
							title: 'Modificaci�n del Men�',
							id: 'winModificar',
							width: 600,
							height: 280,
							x: 350,
							y: 380,
							layout: 'fit',
							items: fpModificar,
							modal: true,
							closeAction: 'destroy'
						}).show();
						
						
						var forma = Ext.getCmp('formaModificar').getForm();
						//forma.loadRecord( ( (sm.getSelection())[0] ) );
						forma.loadRecord( record );
						if (Ext.getCmp('rgTipoModificar').getValue().cg_tipo == 'M') {
							Ext.getCmp('txtUrlModificar').disable();
						}
						
					//}
				},
				isDisabled: function(view, rowIdx, colIdx, item, record) {
					return rowIdx == 0;
				}
			},
			{
				text: 'Agregar',
				width: 50,
				menuDisabled: true,
				xtype: 'actioncolumn',
				tooltip: 'Agregar una opci�n de men�',
				align: 'center',
				iconCls: 'aceptar',
				handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {
					Ext.create('Ext.window.Window', {
							title: 'Alta del Menu',
							id: 'winAgregar',
							width: 600,
							height: 280,
							x: 120,
							y: 120,
							layout: 'fit',
							items: fpAgregar,
							modal: true,
							closeAction: 'destroy'
					}).show();
						
					Ext.getCmp('menuPadre').setValue(record.get('cc_menu'));
				},
				// Only leaf level tasks may be edited
				isDisabled: function(view, rowIdx, colIdx, item, record) {
					return record.data.leaf;
				}
			}]

			/*,
			selType: 'cellmodel',    //Tanto el modelo de edicion por columna o renglon vuelven muy lento el grid. Suponemos que por la cantidad de registros
			plugins: [
				Ext.create('Ext.grid.plugin.CellEditing', { clicksToEdit: 1})
				// Ext.create('Ext.grid.plugin.RowEditing', {
				//	clicksToMoveEditor: 1,
				//	autoCancel: false
				//})
			]*/
		});
		this.callParent();
	}
});	 







//-------------------------------- PRINCIPAL -----------------------------------
/*	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,	
		items: [
			tree,
			NE.util.getEspaciador(20),
			treeChk
		]
	});
*/
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			fp,
			NE.util.getEspaciador(20),
			NE.view.tree.TreeGrid
		]
	});
//-------------------------------- INICIALIZACION -----------------------------------
	catalogoTipoAfiliadoData.load();
	
});