
	function selecccion(check){
		
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		if(check.checked==true)  {
			store.each(function(rec){ 
				rec.set('SELECCION', true); 				
			})
		}if(check.checked==false)  {
			store.each(function(rec){ 
				rec.set('SELECCION', false); 				
			})
		}
		store.commitChanges();
	}


Ext.onReady(function() {

	
	function transmiteEliminarUsu(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);			
			
			Ext.MessageBox.alert('Mensaje',info.strMensaje,
			function(){
				Ext.getCmp("gridConsulta").hide();	
				fp.el.mask('Enviando...', 'x-mask-loading');			
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar'
					})
				});
			});
			
		} else {
				NE.util.mostrarConnError(response,opts);							
		}
	}
	
	
	var procesoEliminar = function() {
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		var regEliminar   = '';
		var noEliminar=0;
		var nafinELec  =  Ext.getCmp('nafinELec').getValue();
		
		store.each(function(record) {
			if(record.data['SELECCION']==true){
				regEliminar+=record.data['ID_REGISTRO']+'|';
				noEliminar++;
			}
		});
		if(noEliminar==0) {
			Ext.MessageBox.alert('Error de validaci�n','Debe selecccionar al menos un registro');
			return;
		}else  {
			Ext.Ajax.request({
			url: '14consulta_usr.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Eliminar',
					noEliminar:noEliminar,
					regEliminar:regEliminar,
					numeroElectronico:nafinELec
				}),
				callback: transmiteEliminarUsu
			});	
		}
	}
	
	
	function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(infoR.tipoArchivo=='PDF') {
				Ext.getCmp('btnArchivoPDF').setIconClass('icoPdf');
			}
			if(infoR.tipoArchivo=='CSV') {
				Ext.getCmp('btnArchivoCSV').setIconClass('icoXls');
			}	
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//***************GRID DE CONSULTA ****************************************
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			//edito el titulo de la columna  
			var jsonData = store.reader.jsonData;		
			var cm = gridConsulta.getColumnModel();			
			var el = gridConsulta.getGridEl();
			if(jsonData.tipoUsuario=='1' || jsonData.tipoUsuario=='2') {
				gridConsulta.setTitle('<div><div style="float:left"> Empresa: '+jsonData.nombreEmpresa+'<br> N�mero N@E : '+jsonData.nafinELec+'</div>');
			}else  {
				gridConsulta.setTitle('<div><div style="float:left">  </div>');
			}
			
			Ext.getCmp('nafinELec').setValue(jsonData.nafinELec);
			
			if(jsonData.tipoUsuario=='8') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SELECCION'), true);	
				Ext.getCmp('btnEliminar').hide();
			}else  {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SELECCION'), false);	
				Ext.getCmp('btnEliminar').show();
			}
			
			
			if(store.getTotalCount() > 0) {	
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
				Ext.getCmp('btnEliminar').enable();
				el.unmask();					
			} else {	
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				Ext.getCmp('btnEliminar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '14consulta_usr.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [
			{	name: 'ID_REGISTRO'},
			{  name: 'SELECCION', convert: NE.util.string2boolean},
			{	name: 'PERFIL'},
			{	name: 'LOGIN'},
			{	name: 'NOMBRE'},
			{	name: 'APELLIDO_PATERMO'},
			{	name: 'APELLIDO_MATERNO'},
			{	name: 'CORREO'}					
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [		
			{
				xtype: 'checkcolumn',				
				header:'<input type="checkbox" name="chek"  id="todoAcepta"  onclick="selecccion(this);"/>',
				tooltip: 'Seleccionar',
				dataIndex : 'SELECCION',
				width : 70,
				align: 'center',
				sortable : false, 
				hidden:true
			},	
			{
				header: 'Perfil',
				tooltip: 'Perfil',
				dataIndex: 'PERFIL',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'
			},
			{
				header: 'Login',
				tooltip: 'Login',
				dataIndex: 'LOGIN',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'
			},
			{
				header: 'Nombre',
				tooltip: 'Nombre',
				dataIndex: 'NOMBRE',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'
			},
			{
				header: 'Apellido�Paterno',
				tooltip: 'Apellido�Paterno',
				dataIndex: 'APELLIDO_PATERMO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'
			},
			{
				header: 'Apellido�Materno',
				tooltip: 'Apellido�Materno',
				dataIndex: 'APELLIDO_MATERNO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'
			},
			{
				header: 'Correo�Electr�nico',
				tooltip: 'Correo�Electr�nico',
				dataIndex: 'CORREO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'
			}
		],				
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			id: 'barraPagina',
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Descargar PDF',					
					tooltip:	'Descargar PDF ',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						
						boton.setIconClass('loading-indicator');
						
						Ext.Ajax.request({
							url: '14consulta_usr.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'GenerarArchivo',
							tipoArchivo:'PDF'
							}),
							callback: procesarArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Descarga Archivo',					
					tooltip:	'Descarga Archivo',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');					
						Ext.Ajax.request({
							url: '14consulta_usr.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'GenerarArchivo',
							tipoArchivo:'CSV'
							}),
							callback: procesarArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Eliminar',					
					tooltip:	'Eliminar',
					iconCls: 'icoRechazar',
					id: 'btnEliminar',
					handler:procesoEliminar
				}
			]
		}
	});
	//****************CRITERIOS DE BUSQUEDA **********************************

	var catalogoEmpresa = new Ext.data.JsonStore({
		id: 'catalogoEmpresa',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '14consulta_usr.data.jsp',
		baseParams: {
			informacion: 'catalogoEmpresa'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});

	
	var catalogoPerfil = new Ext.data.JsonStore({
		id: 'catalogoPerfil',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '14consulta_usr.data.jsp',
		baseParams: {
			informacion: 'catalogoPerfil'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var dataStatus = new Ext.data.ArrayStore({
		fields: ['clave','descripcion','loadMsg'],		 
		 data : [
			["8",'BANCOMEXT'],
			["1",'EPO'],
			["2",'IF'],
			["4",'NAFIN']
		],
		autoLoad: false
	});
	
	var  elementosForma =  [
		{
			xtype: 'combo',	
			name: 'tipoUsuario',	
			id: 'tipoUsuario1',	
			fieldLabel: 'Tipo de Usuario', 
			mode: 'local',	
			hiddenName : 'tipoUsuario',	
			emptyText: 'Seleccione el tipo de usuario. . . ',
			forceSelection: false, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,	
			anchor: '90%',
			width: 120,
			store : dataStatus,	
			tpl: NE.util.templateMensajeCargaCombo,
			displayField : 'descripcion',	valueField : 'clave',
			listeners: {
				select:{ 
					fn:function (combo) {
									
						Ext.getCmp("perfil1").setValue('');	
						Ext.getCmp("empresa1").setValue('');	
	
						catalogoPerfil.load({
							params : Ext.apply(fp.getForm().getValues(),{
								tipoUsuario: combo.getValue()								
							})
						});
						
						Ext.getCmp("empresa1").hide();	
						
						if(combo.getValue()==1 ||  combo.getValue()==2)  {		
							Ext.getCmp("empresa1").show();	
							
							catalogoEmpresa.load({
								params : Ext.apply(fp.getForm().getValues(),{
									tipoUsuario: combo.getValue()								
								})
							});
						}
						
						
						
					}
				}
			}	
		},
		{
			xtype: 'combo',
			fieldLabel: 'Perfil',
			name: 'perfil',
			id: 'perfil1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'perfil',
			emptyText: 'Seleccionar...',
			//autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,			
			minChars: 1,
			store: catalogoPerfil,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120			
		},
		{
			xtype: 'combo',
			fieldLabel: 'Empresa',
			name: 'empresa',
			id: 'empresa1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'empresa',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,			
			minChars: 1,
			store: catalogoEmpresa,
			tpl: NE.util.templateMensajeCargaCombo,					
			anchor: '90%',
			width: 120
		},
		{ 	xtype: 'textfield',  hidden:true,  id: 'nafinELec', 	value: '' }	
		
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,
		title: 'Consulta Usuarios',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
			buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
					
					var tipoUsuario=Ext.getCmp('tipoUsuario1');
					var empresa =Ext.getCmp('empresa1');
					
					if(Ext.isEmpty(tipoUsuario.getValue())){
						tipoUsuario.markInvalid('Debe seleccionar un tipo de usuario');
						tipoUsuario.focus();
						return;
					}
					
					
					if(tipoUsuario.getValue() ==1  || tipoUsuario.getValue() ==2 ){ 
						if(Ext.isEmpty(empresa.getValue())){
							empresa.markInvalid('Debe seleccionar una Empresa');
							empresa.focus();
						return;
						}
					}
					
					Ext.getCmp("gridConsulta").hide();	
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '14consulta_usrExt.jsp';					
				}
			}
		]
		
		
	});
	
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)			
		]
	});
	
	Ext.getCmp('empresa1').hide();
});	
