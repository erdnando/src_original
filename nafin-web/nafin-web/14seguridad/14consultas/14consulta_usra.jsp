<%@ page import="	java.util.*, 
						java.sql.*,
						java.text.*,
						java.math.*,
						netropology.utilerias.*,  
						netropology.utilerias.usuarios.*" 
			errorPage="/00utils/error.jsp" %>
<%@ include file="/14seguridad/14secsession.jspf" %>

<% 
String tipoUsuario	= (request.getParameter("tipoUsuario") == null) ? "" 	: request.getParameter("tipoUsuario");
String perfil			= (request.getParameter("perfil")		== null) ? "" 	: request.getParameter("perfil");
String empresa			= (request.getParameter("empresa")		== null) ? "" 	: request.getParameter("empresa");
String generarArchivo			= (request.getParameter("generarArchivo") == null) ? "" 	: request.getParameter("generarArchivo");


List listClaveAfiliado 	= new ArrayList();

CreaArchivo 	archivo 				= new CreaArchivo();
StringBuffer 	contenidoArchivo 	= new StringBuffer("");
String 			nombreArchivo		= null;
boolean			hayRegistros		= false;
AccesoDB con 				= new AccesoDB();

try {
	PreparedStatement ps = null;
	con.conexionDB();
%>
<html>
<head>
	<title>Nafinet</title>
	<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
<script language="JavaScript1.2" src="../../00utils/valida.js?<%=session.getId()%>"></script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	
			<% if( generarArchivo.equals("s")) {

					List listaUsuarios;
	
					// Realizar consulta
					if (tipoUsuario.equals("4") || tipoUsuario.equals("8"))
						listaUsuarios = getListaUsuarios("N", "", perfil);
					else
						listaUsuarios = getListaUsuarios((tipoUsuario.equals("1")?"E":"I"), empresa, perfil);
	
					// Extraer datos
					 if( !"".equals(empresa)){
						 List datos = getDatosAfiliado((tipoUsuario.equals("1")?"E":"I"), empresa);
						 String nombreAfiliado 		= "";
						 String numeroElectronico 	= ""; 
		
						 if (datos.size() >0) {
							nombreAfiliado 	= (String) datos.get(0); //RazonSocial;
							numeroElectronico = (String) datos.get(1); //Numero de Nafin Electronico
		}
							
						contenidoArchivo.append(",\n");
						contenidoArchivo.append("Empresa:,"		+	nombreAfiliado 	+ "\n");
						contenidoArchivo.append("N�mero N@E:,"	+	numeroElectronico + "\n");
						contenidoArchivo.append(",\n");
	}
	
					if( listaUsuarios.size() > 0 ){
	
						hayRegistros = true;
						contenidoArchivo.append("Perfil,Login,Nombre,Apellido Paterno,Apellido Materno,Correo Electr�nico");
						contenidoArchivo.append("\n");
						
					if(tipoUsuario.equals("8")){
					   for (int indice=0; indice < listaUsuarios.size(); indice++){ 
						netropology.utilerias.usuarios.Usuario oUsuario = (Usuario)listaUsuarios.get(indice); 
						int num=0;
						String SQLConsulta =
							"SELECT count(CC_PERFIL) FROM SEG_PERFIL WHERE SC_TIPO_USUARIO = " + tipoUsuario + "AND CC_PERFIL='"+oUsuario.getPerfil()+"'";

						ps = con.queryPrecompilado(SQLConsulta);
						ResultSet rs = ps.executeQuery();
						ps.clearParameters();
						if(rs.next()) {
							num = rs.getInt(1);
						}
					    ps.close();rs.close();
						if(num!=0){		
					  
						removeCommas(oUsuario);
								
						contenidoArchivo.append(oUsuario.getPerfil() 			+ "," );				
						contenidoArchivo.append("=\"" + oUsuario.getLogin() 	+ "\",");
						contenidoArchivo.append(oUsuario.getNombre() 			+ ",");
						contenidoArchivo.append(oUsuario.getApellidoPaterno() + ",");
						contenidoArchivo.append(oUsuario.getApellidoMaterno() + ",");
						contenidoArchivo.append(oUsuario.getEmail());
						contenidoArchivo.append("\n");

				}}}else	if(tipoUsuario.equals("4")){
					   for (int indice=0; indice < listaUsuarios.size(); indice++){ 
						netropology.utilerias.usuarios.Usuario oUsuario = (Usuario)listaUsuarios.get(indice); 
						int num=0;
						String SQLConsulta =
							"SELECT count(CC_PERFIL) FROM SEG_PERFIL WHERE SC_TIPO_USUARIO = " + tipoUsuario + "AND CC_PERFIL='"+oUsuario.getPerfil()+"'";

						ps = con.queryPrecompilado(SQLConsulta);
						ResultSet rs = ps.executeQuery();
						ps.clearParameters();
						if(rs.next()) {
							num = rs.getInt(1);
						}
					    ps.close();rs.close();
						if(num!=0){		

						removeCommas(oUsuario);

						contenidoArchivo.append(oUsuario.getPerfil() 			+ "," );				
						contenidoArchivo.append("=\"" + oUsuario.getLogin() 	+ "\",");
						contenidoArchivo.append(oUsuario.getNombre() 			+ ",");
						contenidoArchivo.append(oUsuario.getApellidoPaterno() + ",");
						contenidoArchivo.append(oUsuario.getApellidoMaterno() + ",");
						contenidoArchivo.append(oUsuario.getEmail());
						contenidoArchivo.append("\n");
					
				}}}else{
				
				
				
					for (int indice=0; indice < listaUsuarios.size(); indice++) { 
						
						netropology.utilerias.usuarios.Usuario oUsuario = (Usuario)listaUsuarios.get(indice);

						removeCommas(oUsuario);

						contenidoArchivo.append(oUsuario.getPerfil() 			+ "," );				
						contenidoArchivo.append("=\"" + oUsuario.getLogin() 	+ "\",");
						contenidoArchivo.append(oUsuario.getNombre() 			+ ",");
						contenidoArchivo.append(oUsuario.getApellidoPaterno() + ",");
						contenidoArchivo.append(oUsuario.getApellidoMaterno() + ",");
						contenidoArchivo.append(oUsuario.getEmail());
						contenidoArchivo.append("\n");


					
					
					}
				}
						
		
	}
		
					if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
						out.print("<--!Error al generar el archivo-->");
					else
						nombreArchivo = archivo.nombre;
									%>
			
			 <table width="300" cellpadding="0" height="100%" cellspacing="0" border="0">
			 <% if(hayRegistros){ %>
						<tr>
							<td class="formas" valign="center" align="center">
								<table cellpadding="3" cellspacing="1">
								<tr>
									<td class="celda02" width="100" align="center" height="30">
										<a href="<%=strDirecVirtualTemp%>/<%=nombreArchivo%>">Bajar Archivo</a>
									</td>
									<td class="celda02" width="100" align="center">
										<a href="javascript:close();">Cerrar</a>
									</td>
								</tr>
								</table>
						</td>
					</tr>
	 <% } else { %>
	  	<tr>
					<td class="formas" valign="center" align="center"><br>
						<table width="300" border="1" cellspacing="0" cellpadding="2" bordercolor="#A5B8BF">
					<tr>
								<td class="celda01" align="center" bgcolor="silver">No se Encontr&oacute; Ning&uacute;n Registro</td>							
					</tr>
      		</table>
      	</td>
		</tr>
			<%	} %>				
	  </table>
	  
		<% } %>
</body>
</html>
<%
} catch (Exception e) {
	out.println("An exception has ocurred");
}finally {
	if (con.hayConexionAbierta()) {
		con.cierraConexionDB();
	}
}
%>

<%!
/**
 * Obtiene los datos del afiliado.
 * @param tipoAfiliado E epo I If P Pyme
 * @param claveAfiliado Clave del afiliado (ic_epo, ic_if o ic_pyme)
 * @return Lista con los datos del afiliado:
 * 		0.- Razon social del afiliado
 * 		1.- Numero de Nafin Electr�nico
 */
private List getDatosAfiliado(String tipoAfiliado, String claveAfiliado)
		throws Exception {
	AccesoDB con = new AccesoDB();
	String tabla = "";
	String campo = "";
	if (tipoAfiliado.equals("E")) {
		tabla = "comcat_epo";
		campo = "ic_epo";
	} else if (tipoAfiliado.equals("I")) {
		tabla = "comcat_if";
		campo = "ic_if";
	} else {
		tabla = "comcat_pyme";
		campo = "ic_pyme";
	}
	try {
		con.conexionDB();
		con.terminaTransaccion(true);

		String strSQL = 
					"SELECT afiliado.cg_razon_social, n.ic_nafin_electronico " +
					" FROM " + tabla + " afiliado, comrel_nafin n " +
					" WHERE afiliado." + campo + " = n.ic_epo_pyme_if " +
					" AND n.cg_tipo = ? " +
					" AND n.ic_epo_pyme_if = ? ";
		PreparedStatement ps = con.queryPrecompilado(strSQL);
		ps.setString(1, tipoAfiliado);
		ps.setInt(2, Integer.parseInt(claveAfiliado));
		ResultSet rs = ps.executeQuery();
		List datos = new ArrayList();
		if (rs.next()) {
			datos.add(rs.getString("cg_razon_social"));
			datos.add(rs.getString("ic_nafin_electronico"));
		}
		rs.close();
		ps.close();
		return datos;
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
}

private ArrayList getListaUsuarios(String tipoAfiliado, String claveAfiliado, String perfil)
		throws Exception {
		ArrayList 	listaUsuarios 	= new ArrayList();
		UtilUsr 		oUtilUsr 		= new UtilUsr();
		UtilUsr 		utilUsr 			= new UtilUsr();
		
		List cuentas = utilUsr.getUsuariosxAfiliado( claveAfiliado, tipoAfiliado);
		
		Iterator itCuentas = cuentas.iterator();
		while (itCuentas.hasNext()) {
			netropology.utilerias.usuarios.Usuario oUsuario = null;
			String login = (String) itCuentas.next();
			oUsuario = oUtilUsr.getUsuario(login);
			if( "".equals(perfil)) {		
				listaUsuarios.add(oUsuario);
			} else {
				if(perfil.equals(oUsuario.getPerfil())){
					listaUsuarios.add(oUsuario);
				}
			}
		}

		Collections.sort(listaUsuarios);
		return listaUsuarios;
}

private void removeCommas(Usuario usr){
	String tmp = null;

	tmp=removeCommas(usr.getPerfil()); 				usr.setPerfil(tmp);
	tmp=removeCommas(usr.getLogin()); 				usr.setLogin(tmp);
	tmp=removeCommas(usr.getNombre());				usr.setNombre(tmp);
	tmp=removeCommas(usr.getApellidoPaterno());	usr.setApellidoPaterno(tmp);
	tmp=removeCommas(usr.getApellidoMaterno());	usr.setApellidoMaterno(tmp);
	tmp=removeCommas(usr.getEmail());				usr.setEmail(tmp);
}

private String removeCommas(String str) {
	
	String oStr = "";
	char c = ',';
	
	for (int index = 0; index < str.length(); index++) {
		if (str.charAt(index) != c) oStr += str.charAt(index);
   }
	return oStr;
}
%>
