<%@ page import="java.util.Vector, java.sql.*,java.text.*,java.math.*,netropology.utilerias.*" errorPage="/00utils/error.jsp" %>
<%@ include file="../../15cadenas/015secsession.jspf" %>
<% 
////////////////////////////////////////////////////
//	Declaraci�n de variables
////////////////////////////////////////////////////
String SQLperfil = "", SQLtipoEpo = "", SQLPyme = "", SQLsentencia = "", SQLcatPerfil = "";
String SQLModulos = "", clave = "", nombre = "", interno = "", sel = "", tabla = "", campo = "";
String str_padre = "";
int registros = 0, usuasoc = 0, modasoc = 0, facasoc = 0;
ResultSet rs = null;

String perfil = (request.getParameter("perfil") == null) ? "" : request.getParameter("perfil");
String cc_perfil = (request.getParameter("cc_perfil") == null) ? "" : request.getParameter("cc_perfil");
String tipo_usuario = (request.getParameter("tipo_usuario") == null) ? "" : request.getParameter("tipo_usuario");
String epo = (request.getParameter("epo") == null) ? "" : request.getParameter("epo");
String ic_pyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");

AccesoDB con = new AccesoDB();
try {
con.conexionDB();
	
SQLperfil = "select sc_tipo_usuario, cc_perfil from seg_perfil";
%>
<html>
<head>
<title>Nafinet</title>
<link rel="stylesheet" href="<%=strDirecVirtualCSS%>css/<%=strClase%>">
<script language="JavaScript1.2" src="../../00utils/valida.js?<%=session.getId()%>"></script>
<script language="JavaScript">
function envia() {
  var f = document.consultas;

  if (f.perfil.selectedIndex==0){
	 	alert("Debe seleccionar un perfil.");
		f.perfil.focus();		
		return;
  }else{
		var Lstvalue = f.perfil.options[f.perfil.selectedIndex].value;
		var lstArray = Lstvalue.split("|");
		
		if(lstArray[0] == 1) {
			f.tipo_usuario.value = "EPO";
		}else if(lstArray[0] == 2) {
			f.tipo_usuario.value = "IF";
		}else if(lstArray[0] == 3) {
			f.tipo_usuario.value = "PYME";
		}else{
			f.tipo_usuario.value = "NAFIN";
		}
<%		if (tipo_usuario.equals("PYME")){ %>
			if(f.ic_pyme.selectedIndex==0){ 
		 		alert("Debe seleccionar una pyme.");
				f.ic_pyme.focus();		
				return;
			}	
			
<%		} %>
		f.cc_perfil.value = lstArray[1];
		f.submit();
  }
}
function consultar()
{
	var f = document.consultas;

	//tipo=ACTUALIZAR_COMBOS|CONSULTAR
	if (f.epo.options[f.epo.selectedIndex].value=="")
	{
		f.epo.focus();
		return;
	}
	f.submit();
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="consultas" method="post" action="14consulperfil.jsp">
<input type="hidden" name="accion">
<input type="Hidden" name="cc_perfil" value="<%=cc_perfil%>">
<input type="Hidden" name="tipo_usuario" value="<%=tipo_usuario%>">

  <table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr valign="middle">
  	<td valign="top" align="center">
	<table width="600"  cellpadding="0" cellspacing="0" border="0">
        <table>
		<tr>
			<td class="formas" align="right">Clave Perfil:</td>
			<td class="formas">
				<%ResultSet rsPerfil = con.queryDB(SQLperfil);%>
				<select name="perfil" class="formas" onChange="">
				<option value="" <%if(perfil.equals("")) out.print("selected");%>>Seleccionar...</option>
				<%while (rsPerfil.next()) {%>
					<option value="<%out.print(rsPerfil.getString(1) + "|" + rsPerfil.getString(2));%>" <%if(perfil.equals(rsPerfil.getString(1) + "|" + rsPerfil.getString(2))) out.print("selected");%> ><%out.print(rsPerfil.getString(2));%></option>
				<%}
				rsPerfil.close();
				con.cierraStatement();
				%>
				</select>
			</td>
		</tr>
		

		<% if (tipo_usuario.equals("PYME")){
			SQLtipoEpo = "select IC_EPO AS CLAVE,CG_RAZON_SOCIAL AS DESCRIPCION from comcat_epo WHERE CS_HABILITADO='S' order by DESCRIPCION";
			tabla = "comcat_pyme c";
			campo = "b.ic_pyme = c.ic_pyme";
		%>
		<tr>
			<td class="formas" align="right">Epo:</td>
			<td class="formas">
			<%rs = con.queryDB(SQLtipoEpo);%>
			<select name="epo" class="formas" onChange="consultar();"">
			<%while (rs.next()) {%>
				<option value="<%out.print(rs.getString(1));%>" <%if(epo.equals(rs.getString(1))) out.print("selected");%> ><%out.print(rs.getString(2));%></option>
			<%}
			rs.close();
			con.cierraStatement();
			%>
			</select>
			</td>
		</tr>
		<tr>
			<td class="formas" align="right">PYME:</td>
			<td class="formas">
			<select name="ic_pyme" class="formas">
			<option value="" <%if(ic_pyme.equals("")) out.print("selected");%>>Seleccionar...</option>			
<% 			if(!epo.equals("")){%>
				<%
				SQLPyme = "select p.ic_pyme, p.cg_razon_social" +
					" from comcat_pyme p, comrel_pyme_epo pe" +
					" where" +
					" p.ic_pyme=pe.ic_pyme and pe.ic_epo=" + epo +
					" and pe.cs_habilitado = 'S'" +
					" order by p.cg_razon_social";
				rs = con.queryDB(SQLPyme);
				while(rs.next())
				{
					clave = rs.getString(1);
					nombre = rs.getString(2);
					if (clave.equals(ic_pyme)){sel="SELECTED";} else {sel="";}
					out.println("<option "+sel+" value='"+clave+"'>"+nombre+"</option>");
				}
				rs.close();
				con.cierraStatement();
			}else{
				out.print("<option value=''>-----------</option>\n");
			}				
%>
			</select>
			</td>
		</tr>	
		<%if (!ic_pyme.equals("")){%>
				<td valign="top" align="left">
			    <tr>
			    </tr>
			  	 <table align="center" width="800px" cellspacing="0" cellpadding="3" border="0">
 				    <tr>
						<td class="formas" align="left" >M&oacute;dulos Asociados</td>					  
						<td class="formas" align="left" >Facultades Asociados</td>					  
					</tr>				  
					<tr>
						<td>
<%					SQLModulos = "select a.c_sistema ,a.d_descripcion, b.cc_sistema_2  from seg_sistemas a, seg_rel_sist b" +
					" where a.c_sistema in (select c_sistema from seg_rel_perfil_perfpr" +
					" where cc_perfil ='"+ cc_perfil + "') and a.c_sistema = b.cc_sistema (+) order by a.d_descripcion";
					rs = con.queryDB(SQLModulos);%>
					<select name="modulos" size="10" multiple style="WIDTH: 300px" onBlur="" class="formas">
					<option value="" class="celda01" bgcolor="silver">M&oacute;dulo  -  Descripci&oacute;n  - Sist. Padre</option>
					<%	while (rs.next()) {str_padre = (rs.getString(3) == null ? "":rs.getString(3));%>
						<option value=""><%out.print(rs.getString(1) + "  -  " +rs.getString(2) + "  -  " + str_padre);%></option>
					<%	modasoc++;
						}
						rs.close();
						con.cierraStatement();%>
					</select> 
				  </td>	
	   		      <td>
<%					SQLcatPerfil = "select a.c_perfil_prot,b.d_descripcion" +
					" from seg_facperfpr a, seg_facultades b, seg_rel_perfil_perfpr c" +
					" where c.cc_perfil ='"+ cc_perfil + "' and a.c_facultad = b.c_facultad" + 
					" and a.c_sistema = b.c_sistema and c.c_perfil_prot = a.c_perfil_prot" +
					" and c.c_sistema = a.c_sistema and b.t_tipo_facultad = 1 order by a.c_perfil_prot,b.d_descripcion ";
				    rs = con.queryDB(SQLcatPerfil);%>
					<select name="facultades" size="10" multiple style="WIDTH: 300px" onBlur="" class="formas">
					<option value="" class="celda01" bgcolor="silver">P.Prototipo  -  Descripci&oacute;n Facultad</option>					
					<%	while (rs.next()) {%>
						<option value=""><%out.print(rs.getString(1) + "  -  " + rs.getString(2));%></option>
					<%	facasoc++;
						}
						rs.close();
						con.cierraStatement();%>
					</select> 
				  </td>
				  </tr>
				  <tr>
					<td class="formas" align="left">Total M&oacute;dulos Asociados: <%out.print(modasoc);%></td>					  
					<td class="formas" align="left">Total Facultades Asociadas: <%out.print(facasoc);%></td>
				  </tr>
				 </table>
			   </td>	
<%		  }//if ic_pyme
		}else if (!tipo_usuario.equals("")){
%>
				<td valign="top" align="left">
			    <tr>
			    </tr>
			  	 <table align="center" width="800px" cellspacing="0" cellpadding="3" border="0">
 				  <tr>
					<td class="formas" align="left" >M&oacute;dulos Asociados</td>					  
					<td class="formas" align="left" >Facultades Asociados</td>					  
				  </tr>

				  <tr>
				   <td>
<%					/*SQLModulos = "select c_sistema ,d_descripcion  from seg_sistemas" +
					" where c_sistema in (select c_sistema from seg_rel_perfil_perfpr where cc_perfil ='"+ cc_perfil + "')";*/
					SQLModulos = "select a.c_sistema ,a.d_descripcion, b.cc_sistema_2  from seg_sistemas a, seg_rel_sist b" +
					" where a.c_sistema in (select c_sistema from seg_rel_perfil_perfpr" +
					" where cc_perfil ='"+ cc_perfil + "') and a.c_sistema = b.cc_sistema (+) order by a.d_descripcion";		
					rs = con.queryDB(SQLModulos);%>
					<select name="modulos" size="10" multiple style="WIDTH: 300px" onBlur="" class="formas">
					<option value="" class="celda01" bgcolor="silver">M&oacute;dulo  -  Descripci&oacute;n  -  Sist. Padre</option>
					<%	while (rs.next()) { 
							str_padre = (rs.getString(3) == null ? "":rs.getString(3));%>
						<option value=""><%out.print(rs.getString(1) + "  -  " + rs.getString(2) + "  -  " + str_padre);%></option>
					<%	modasoc++;
						}
						rs.close();
						con.cierraStatement();%>
					</select> 
				   </td>	
	   		       <td>
<%					SQLcatPerfil = "select a.c_perfil_prot,b.d_descripcion" +
					" from seg_facperfpr a, seg_facultades b, seg_rel_perfil_perfpr c" +
					" where c.cc_perfil ='"+ cc_perfil + "' and a.c_facultad = b.c_facultad" + 
					" and a.c_sistema = b.c_sistema and c.c_perfil_prot = a.c_perfil_prot" +
					" and c.c_sistema = a.c_sistema and b.t_tipo_facultad = 1 order by a.c_perfil_prot,b.d_descripcion";
				    rs = con.queryDB(SQLcatPerfil);%>
					<select name="facultades" size="10" multiple style="WIDTH: 300px" onBlur="" class="formas">
					<option value="" class="celda01" bgcolor="silver">P.Prototipo  -  Descripci&oacute;n Facultad</option>					
					<%	while (rs.next()) {%>
						<option value=""><%out.print(rs.getString(1) + "  -  " + rs.getString(2));%></option>
					<%	facasoc++;
						}
						rs.close();
						con.cierraStatement();%>
					</select> 
				   </td>
				  </tr>
				  <tr>
					<td class="formas" align="left">Total M&oacute;dulos Asociados: <%out.print(modasoc);%></td>					  
					<td class="formas" align="left">Total Facultades Asociadas: <%out.print(facasoc);%></td>
				  </tr>
				  <tr>
					<td class="formas" align="center">&nbsp;</td>
				  </tr>
			   </td>
<%	    } //if principal%>
		<table width="600" cellpadding="3" cellspacing="1" border="0" class="formas">
		 <tr>
  		  <td valign="top" align="center">
		   <td class="celda02" width="50" align="center"><a href="javascript:envia();">Buscar</a></td>
		  </td> 
		 </tr>
		</table>		
		<!--<br>-->
	</form>
	
<%	 if (registros > 0){%>	
		<table cellpadding="0" cellspacing="0" border="0">
		 <tr>
  		  <td class="celda02" width="50" align="center">&nbsp;</td>		
		  <td>&nbsp;</td>		 
		  <form name="frmimprimir" method="post" action="14consulperfil3i.jsp" target="_blank">
		  <input type="Hidden" name="cc_perfil" value="<%=cc_perfil%>">
  		  <input type="Hidden" name="tipo_usuario" value="<%=tipo_usuario%>">					
		  </form>	
  		  <td class="celda02" width="50" align="center"><a href="javascript:document.frmimprimir.submit();" onmouseout="window.status=''; return true;" onmouseover ="window.status='Imprimir'; return true;">Imprimir Modulos</a></td>		
		  <td>&nbsp;</td>
		  <form name="frmimprimir2" method="post" action="14consulperfil2i.jsp" target="_blank">
		  <input type="Hidden" name="cc_perfil" value="<%=cc_perfil%>">
  		  <input type="Hidden" name="tipo_usuario" value="<%=tipo_usuario%>">					
		  </form>	
  		  <td class="celda02" width="50" align="center"><a href="javascript:document.frmimprimir2.submit();" onmouseout="window.status=''; return true;" onmouseover ="window.status='Imprimir'; return true;">Imprimir Facultades</a></td>		
		  <td>&nbsp;</td>

		  <form name="frmimprimirPF" method="post" action="14perfil_facultad.jsp" target="_blank">
		  <input type="Hidden" name="cc_perfil" value="<%=cc_perfil%>">
  		  <input type="Hidden" name="tipo_usuario" value="<%=tipo_usuario%>">					
		  </form>	
		  <Script language="JavaScript">
		   function mtdImpPer()
		   {
		      var ls_isNS4 = (document.layers ? true:false);
			  if (!ls_isNS4)   document.frmimprimirPF.submit();
			  else
			  { 
			       document.frmimprimirPF.action = "14perfil_facultadNS.jsp";
			       document.frmimprimirPF.submit();
			  }
	  	   }
		  </script>
		  <td class="celda02" width="50" align="center"><a href="javascript:mtdImpPer();" onmouseout="window.status=''; return true;" onmouseover ="window.status='Imprimir'; return true;">Imprimir Perfil</a></td>
		</tr>
		</table>
		
	<%}%>			
	</table>	
  </td>
</tr>
</table>

</body>
</html>
<%
} finally{
	if (con.hayConexionAbierta()) {
		con.cierraConexionDB();
	}
}
%>