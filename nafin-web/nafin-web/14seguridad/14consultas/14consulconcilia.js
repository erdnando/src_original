
Ext.onReady(function() {


	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnImprimir').setIconClass('icoPdf');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {					
				el.unmask();
				Ext.getCmp('btnImprimir').enable();
			} else {		
				Ext.getCmp('btnImprimir').disable();
				el.mask('No se encontró ningún registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '14consulconcilia.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [	
			{	name: 'NOMBRE'},		
			{	name: 'APELLIDO_PATERNO'},		
			{	name: 'APELLIDO_MATERNO'},		
			{	name: 'CORREO_ELECTRONICO'},		
			{	name: 'ESTATUS'},	
			{	name: 'CAUSA'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});
	
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Conciliación de Solicitudes',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Nombre',
				tooltip: 'Nombre',
				dataIndex: 'NOMBRE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Apellido Paterno',
				tooltip: 'Apellido Paterno',
				dataIndex: 'APELLIDO_PATERNO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Apellido Materno',
				tooltip: 'Apellido Materno',
				dataIndex: 'APELLIDO_MATERNO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			}	,
			{
				header: 'Correo Electrónico ',
				tooltip: 'Correo Electrónico ',
				dataIndex: 'CORREO_ELECTRONICO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Causa',
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			}		
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir',
					handler: function(boton, evento) {
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '14consulconcilia.data.jsp',
							params: {
								informacion: 'GeneraArchivo',
								start: barraPaginacion.cursor,
								limit: barraPaginacion.pageSize									
							},
							callback: descargaArchivo
						});
						
						
					}
				}				
			]
		}
	});
	
	//****************CRITERIOS DE BUSQUEDA **********************************

	var catalogoEstatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			["1",'Solicitud en proceso'],
			["2",'Solicitud en aprobada'],
			["3",'Solicitud en rechazada'],
			["4",'Solicitud eliminada o dada de baja']
		 ]
	});
	
	var  elementosForma =  [
		{
			xtype: 'textfield',
			fieldLabel: 'Nombre',		
			name: 'nombre',
			id: 'nombre1',
			allowBlank: true,
			maxLength: 20,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Apellido Paterno',		
			name: 'apellidoPaterno',
			id: 'apellidoPaterno1',
			allowBlank: true,
			maxLength: 20,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},			
		{
			xtype: 'textfield',
			fieldLabel: 'Apellido Materno',		
			name: 'apellidoMaterno',
			id: 'apellidoMaterno1',
			allowBlank: true,
			maxLength: 20,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},	
		{
			xtype			: 'textfield',
			id				: 'email_',
			name			: 'email',
			fieldLabel	: ' E-mail',
			allowBlank	: true,
			maxLength	: 100,
			width			: 230,
			allowBlank	: true,
			margins		: '0 20 0 0',
			msgTarget	: 'side',
			tabIndex		: 11,
			vtype: 'email' //validacion en evento on blur de si verEmail
		},		
		{
			xtype: 'combo',
			fieldLabel: 'Estatus',
			name: 'estatus',
			id: 'estatus1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'estatus',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			forceSelection : true,
			store: catalogoEstatus			
		}
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Conciliación de Solicitudes',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
				
					fp.el.mask('Enviando...', 'x-mask-loading');		
					
					consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar',
						informacion: 'Consultar',
						start:0,
						limit:15						
					})
				});					
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '14consulconciliaExt.jsp';					
				}
			}
		]	
		
	});
		
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	
});	
