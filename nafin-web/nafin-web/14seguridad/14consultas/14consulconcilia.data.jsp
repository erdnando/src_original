<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	netropology.utilerias.*,
	com.netro.seguridad.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/14seguridad/14secsession_extjs.jspf" %>  
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String nombre = (request.getParameter("nombre") != null) ? request.getParameter("nombre") : "";
String apellidoPaterno = (request.getParameter("apellidoPaterno") != null) ? request.getParameter("apellidoPaterno") : "";
String apellidoMaterno = (request.getParameter("apellidoMaterno") != null) ? request.getParameter("apellidoMaterno") : "";
String email = (request.getParameter("email") != null) ? request.getParameter("email") : "";
String estatus = (request.getParameter("estatus") != null) ? request.getParameter("estatus") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";

int  start= 0, limit =0;
String infoRegresar="", consulta="";
JSONObject jsonObj = new JSONObject();


if(informacion.equals("Consultar") ||  informacion.equals("GeneraArchivo")  ) {

	ConcSolicNafinSeg paginador = new ConcSolicNafinSeg();
	paginador.setNombre(nombre);
	paginador.setApellidoPaterno(apellidoPaterno);
	paginador.setApellidoMaterno(apellidoMaterno);
	paginador.setEmail(email);
	paginador.setEstatus(estatus);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
				
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	if(informacion.equals("Consultar")) {
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);										
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
			
		jsonObj = JSONObject.fromObject(consulta);				

	}else  if ( informacion.equals("GeneraArchivo") ) {
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}
	
	infoRegresar = jsonObj.toString();		
}

%>
<%=infoRegresar%>