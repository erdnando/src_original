<%@ page import="java.sql.*,java.text.*,java.math.*,netropology.utilerias.*"%>
<%@ include file="../../15cadenas/015secsession.jspf" %>
<%
////////////////////////////////////////////////////
//	Declaraci�n de variables
////////////////////////////////////////////////////
String SQLperfil = "", SQLtipoEpo = "", SQLPyme = "", SQLsentencia = "", SQLcatPerfil = "";
String SQLModulos = "", clave = "", nombre = "", interno = "", sel = "", tabla = "", campo = "";
String str_padre = "";
int registros = 0, contador = 0;
ResultSet rs = null;

String cc_perfil = (request.getParameter("cc_perfil") == null) ? "" : request.getParameter("cc_perfil");
String tipo_usuario = (request.getParameter("tipo_usuario") == null) ? "" : request.getParameter("tipo_usuario");

AccesoDB con = new AccesoDB();
try{
	con.conexionDB();
%>
<html>
<head>
<title>Nafinet</title>
<link rel="stylesheet" href="<%=strDirecVirtualCSS%>css/<%=strClase%>">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="window.print()">
<%@ include file="../../15cadenas/cabezai.jspf" %>
<form name="consultas" method="post" action="14consulperfil.jsp">
<input type="hidden" name="accion">
<input type="Hidden" name="cc_perfil" value="<%=cc_perfil%>">
<input type="Hidden" name="tipo_usuario" value="<%=tipo_usuario%>">
<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr valign="middle">
    <td valign="top" align="center">
	  <table width="600"  cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="formas" align="center">Clave Perfil: <%= cc_perfil %></td>
		</tr>
		<tr>
			<td valign="top" align="left">
			  <tr>
			  </tr>
			  <td class="formas" align="left" >
 				<tr>
				  <td class="formas" align="center">M&oacute;dulos Asociados</td>					  
				</tr>	
			  </td>
			  <td valign="top">
				<table align="center" cellspacing="0" cellpadding="3" width="270px" border="1">
<%					SQLModulos = "select a.c_sistema ,a.d_descripcion, b.cc_sistema_2  from seg_sistemas a, seg_rel_sist b" +
					" where a.c_sistema in (select c_sistema from seg_rel_perfil_perfpr" +
					" where cc_perfil ='"+ cc_perfil + "') and a.c_sistema = b.cc_sistema (+) order by a.d_descripcion";
					rs = con.queryDB(SQLModulos);%>
						<tr>
							<td class="formas" align="center" bgcolor="silver">M&oacute;dulo</td>
							<td class="formas" align="center" bgcolor="silver">Descripci&oacute;n</td>
							<td class="formas" align="center" bgcolor="silver">Sist. Padre</td>
					    </tr>
					<%	contador=0;
						while (rs.next()) { 
							str_padre = (rs.getString(3) == null ? "":rs.getString(3));%>
						<tr>
							<td class="formas" align="left"><%out.print(rs.getString(1));%></td>
							<td class="formas" align="left"><%out.print(rs.getString(2));%></td>
							<td class="formas" align="center"><%out.print(str_padre);%>&nbsp;</td>
					    </tr>						
					<%	contador++;
						}
						rs.close();
						con.cierraStatement();%>
						<tr>
							<td class="formas" align="center" colspan="2">Total de M&oacute;dulos Asociados:</td>
							<td class="formas" align="center"><%out.print(contador);%></td>
					    </tr>
				</table>
			  </td>	
			</td>	
		</tr>
	  </table>	
    </td>
  </tr>
</table>
</div>
</body>
</html>
<%
}catch(Exception e){
	out.println(e);
}
finally{
	con.cierraConexionDB();
}
%>
