<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.sql.*,
	com.netro.seguridadbean.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	netropology.utilerias.*,
	com.netro.seguridad.*,
	com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/14seguridad/14secsession_extjs.jspf" %>  
<%

String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String tipoUsuario = (request.getParameter("tipoUsuario") != null) ? request.getParameter("tipoUsuario") : "";
String perfil = (request.getParameter("perfil") != null) ? request.getParameter("perfil") : "";
String empresa = (request.getParameter("empresa") != null) ? request.getParameter("empresa") : "";
String tipoArchivo = (request.getParameter("tipoArchivo") != null) ? request.getParameter("tipoArchivo") : "";
String noEliminar = (request.getParameter("noEliminar") != null) ? request.getParameter("noEliminar") : "";
String numeroElectronico = (request.getParameter("numeroElectronico") != null) ? request.getParameter("numeroElectronico") : "";
String regEliminar = (request.getParameter("regEliminar") != null) ? request.getParameter("regEliminar") : "";
String infoRegresar="", nombreAfiliado = "", claveAfiliado ="";
if(tipoUsuario.equals("4")){  claveAfiliado ="N"; }
if(tipoUsuario.equals("1")){  claveAfiliado ="E"; }
if(tipoUsuario.equals("2")){  claveAfiliado ="I"; } 
if(numeroElectronico.equals("")){ 	numeroElectronico="0";   }
JSONObject jsonObj = new JSONObject();

ConsUsuariosNafinSeg clase = new  ConsUsuariosNafinSeg();

if(informacion.equals("catalogoPerfil")){

	List catPerfil = clase.getCatalogoPerfil(tipoUsuario);
	jsonObj.put("registros", catPerfil);
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString();

}else if(informacion.equals("catalogoEmpresa")){   

	if(tipoUsuario.equals("1")) {
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");		
		catalogo.setOrden("cg_razon_social");
		infoRegresar = catalogo.getJSONElementos();
	
	}else  if(tipoUsuario.equals("2")) {
		CatalogoIF catalogo = new CatalogoIF();
		catalogo.setCampoClave("ic_if");
		catalogo.setCampoDescripcion("cg_razon_social");		
		catalogo.setOrden("cg_razon_social");
		List catIF = catalogo.getListaElementosGral();	
		jsonObj.put("registros", catIF);
		jsonObj.put("success",  new Boolean(true)); 
		infoRegresar = jsonObj.toString();
	
	}

}else if(informacion.equals("Consultar")  ||  informacion.equals("GenerarArchivo")   ){ 	 
	
	if( !"".equals(empresa)){
		List datos = clase.getDatosAfiliado((tipoUsuario.equals("1")?"E":"I"), empresa);						
		if (datos.size() >0) {
			nombreAfiliado 	= (String) datos.get(0); //RazonSocial;
			numeroElectronico = (String) datos.get(1); //Numero de Nafin Electronico
		}	
		jsonObj.put("nombreEmpresa", nombreAfiliado);	
		jsonObj.put("nafinELec",numeroElectronico);		
		
	}
	
	clase.setTipoUsuario(tipoUsuario);
	clase.setNombreEmpresa(nombreAfiliado);
	clase.setNumeroElectronico(numeroElectronico);
	
	List registros =  clase.getConsultar(tipoUsuario, empresa,  perfil);
		
	if(informacion.equals("Consultar"))  {
	
		jsonObj.put("registros", registros);	
		jsonObj.put("tipoUsuario", tipoUsuario);	
		jsonObj.put("success",  new Boolean(true)); 		
		
	}else if(informacion.equals("GenerarArchivo"))  {
	
		try {
			String nombreArchivo = clase.generarArchivos(request, registros, tipoArchivo, strDirectorioTemp );				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			jsonObj.put("tipoArchivo", tipoArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		}		
	}
	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("Eliminar"))  {


	//SeguridadHome seguridadHome = (SeguridadHome)ServiceLocator.getInstance().getEJBHome("SeguridadEJB", SeguridadHome.class); 
	//com.netro.seguridadbean.Seguridad SeguridadBean = seguridadHome.create();
	
	com.netro.seguridadbean.Seguridad SeguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);

	String strMensaje ="", fueEliminado ="";
	int numero = Integer.parseInt(noEliminar);
	fueEliminado =SeguridadBean.getBitacora(numeroElectronico, iNoUsuario,  strNombreUsuario, regEliminar, numero, claveAfiliado, empresa);
	if(fueEliminado.equals("G")){ 
		strMensaje =  "Los registros fueron Eliminados con Exito";
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("strMensaje", strMensaje);
	infoRegresar = jsonObj.toString();
		
}

%>

<%=infoRegresar%>

