<%@ page import="java.sql.*,java.text.*,java.math.*,netropology.utilerias.*"%>
<%@ include file="../../15cadenas/015secsession.jspf" %>
<%
////////////////////////////////////////////////////
//	Declaraci�n de variables
////////////////////////////////////////////////////
String SQLperfil = "", SQLtipoEpo = "", SQLPyme = "", SQLsentencia = "", SQLcatPerfil = "";
String SQLModulos = "", clave = "", nombre = "", interno = "", sel = "", tabla = "", campo = "";
int registros = 0, contador = 0;
ResultSet rs = null;

String cc_perfil = (request.getParameter("cc_perfil") == null) ? "" : request.getParameter("cc_perfil");
String tipo_usuario = (request.getParameter("tipo_usuario") == null) ? "" : request.getParameter("tipo_usuario");
String ic_pyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");

AccesoDB con = new AccesoDB();
try{
	con.conexionDB();
%>
<html>
<head>
<title>Nafinet</title>
<link rel="stylesheet" href="<%=strDirecVirtualCSS%>css/<%=strClase%>">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="window.print()">
<%@ include file="../../15cadenas/cabezai.jspf" %>
<form name="consultas" method="post" action="14consulperfil.jsp">
<div align="center">
  <table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr valign="middle">
  	<td valign="top" align="center">
	<table width="600" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="formas" align="center">Clave Perfil: <%= cc_perfil %></td>
		</tr>
		<% if (tipo_usuario.equals("PYME")){
			SQLtipoEpo = "select IC_EPO AS CLAVE,CG_RAZON_SOCIAL AS DESCRIPCION from comcat_epo WHERE CS_HABILITADO='S' order by DESCRIPCION";
			tabla = "comcat_pyme c";
			campo = "b.ic_pyme = c.ic_pyme";
			if (!ic_pyme.equals("")){
					SQLsentencia = "select count(c.cg_razon_social), c.cg_razon_social" +
					" from seg_rel_usuario_perfil a, seg_usuario b," + tabla + 
					" where a.cc_perfil ='" + cc_perfil + "' and a.ic_usuario = b.ic_usuario" +
					" and " + campo + " and b.ic_pyme =" + ic_pyme + 
					" group by c.cg_razon_social";
					rs = con.queryDB(SQLsentencia);%>
			<td valign="top" align="left">
			    <tr>
			    </tr>
			  	<table align="center" width="500" cellspacing="0" cellpadding="3">
 				  <tr>
					  <td class="formas" align="center" >Usuarios Asociados</td>
				  </tr>				  

				  <tr>
				    <td valign="top">
					  <table align="center" cellspacing="0" cellpadding="3" width="270px" border="1">
						<tr>
							<td class="formas" align="center" bgcolor="silver">Cliente</td>
							<td class="formas" align="center" bgcolor="silver">N&uacute;mero</td>
					    </tr>					  
					<%	while (rs.next()) {%>
						<tr>
							<td class="formas" align="left"><%out.print(rs.getString(2));%></td>
							<td class="formas" align="center"><%out.print(rs.getString(1));%></td>
					    </tr>
					<%	contador++;
						}
						rs.close();
						con.cierraStatement();%>
						<tr>
							<td class="formas" align="center">Total de Usuarios Asociados:</td>
							<td class="formas" align="center"><%out.print(contador);%></td>
					    </tr>
					  </table>
				    </td>				   
				  </tr>
				</table>
			</td>	
<%		  }//if ic_pyme
		}else if (!tipo_usuario.equals("")){
				if (tipo_usuario.equals("EPO")){
					tabla = "comcat_epo c";
					campo = "b.ic_epo = c.ic_epo";
				}	
				if (tipo_usuario.equals("IF")){
					tabla = "comcat_if c";
					campo = "b.ic_if = c.ic_if";
				}	
				if (tipo_usuario.equals("NAFIN")){
					SQLsentencia = "select distinct count(a.ic_usuario),b.cg_nombre || ' ' || b.cg_appaterno || ' ' || b.cg_apmaterno as Nombre" +
					" from seg_rel_usuario_perfil a, seg_usuario b where a.cc_perfil ='" + cc_perfil + "' and a.ic_usuario = b.ic_usuario" +
					" group by a.ic_usuario,b.cg_nombre, b.cg_appaterno,b.cg_apmaterno";
				}else{
					SQLsentencia = "select distinct count(a.ic_usuario),c.cg_razon_social" +
					" from seg_rel_usuario_perfil a, seg_usuario b," + tabla + 
					" where a.cc_perfil ='" + cc_perfil + "' and a.ic_usuario = b.ic_usuario" +
					" and " + campo + " group by a.ic_usuario, c.cg_razon_social";
				}//out.print(SQLsentencia);
				rs = con.queryDB(SQLsentencia);%>
				<td valign="top" align="left">
			    <tr>
			    </tr>
			  	 <table align="center" width="500" cellspacing="0" cellpadding="3">
 				  <tr>
					  <td class="formas" align="center">Usuarios Asociados</td>
				  </tr>
				  <tr>
				    <td valign="top">
					  <table align="center" cellspacing="0" cellpadding="3" width="270px" border="1">
						<tr>
							<td class="formas" align="center" bgcolor="silver">Cliente</td>
							<td class="formas" align="center" bgcolor="silver">N&uacute;mero</td>
					    </tr>					  
					<%	while (rs.next()) {%>
						<tr>
							<td class="formas" align="left"><%out.print(rs.getString(2));%></td>
							<td class="formas" align="center"><%out.print(rs.getString(1));%></td>
					    </tr>
					<%	contador++;
						}
						rs.close();
						con.cierraStatement();%>
						<tr>
							<td class="formas" align="center">Total de Usuarios Asociados:</td>
							<td class="formas" align="center"><%out.print(contador);%></td>
					    </tr>
					  </table>
				    </td>
				  </tr>
				 </table> 
				</td>
<%	    } //if principal%>
	</table>	
  </td>
</tr>
</table>
</div>
</body>
</html>
<%
}catch(Exception e){
	out.println(e);
}
finally{
	con.cierraConexionDB();
}
%>
