<%@ page import="java.sql.*,java.io.*,netropology.utilerias.*"%>
<%response.setHeader("Pragma","no-cache");%>
<html>
<head>
<link rel="stylesheet" href="/nafin/14seguridad/Seguridad/css/<%=session.getAttribute("strClase")%>">
</head>

<body>

 <table align="right">
  <tr>
    <td class="celda02" width="50" align="center">
      <a href="javascript:mtdDespliegaTodas();" >
        Mostrar Facultades
      </a>
    </td>		

    <td>&nbsp;</td>
    <td class="celda02" width="50" align="center">
     <a href="javascript:window.print();" >
      Imprimir
     </a>
    </td>			

    <td>&nbsp;</td>
    <td class="celda02" width="50" align="center">
     <a href="javascript:window.close();" >
      Cancelar
     </a>
    </td>		
  </tr>
  </table><br><br>

	  
<%
String ms_Perfil = "";
String ms_Trama_Sist = "";
  
AccesoDB con = new AccesoDB();
ms_Perfil = request.getParameter("cc_perfil");
  
out.print("<center> <a> "+ms_Perfil+" </a> </center>");
  
try {
	 con.conexionDB();
         String ls_Cabecera_Sist = "";
		 ls_Cabecera_Sist += "<td class='bullet' align='left'> <img src='../../00utils/gif/m45_3.gif' border='0'> </td>";
		 ls_Cabecera_Sist += "<td class='bullet' align='center'> &nbsp;&nbsp;&nbsp;&nbsp; Sistema &nbsp;&nbsp;&nbsp;&nbsp; </td>";
		 ls_Cabecera_Sist += "<td class='bullet' align='center'> &nbsp;&nbsp;&nbsp;&nbsp; Descripci&oacute;n &nbsp;&nbsp;&nbsp;&nbsp; </td>";
		 ls_Cabecera_Sist += "<td class='bullet' align='center'> &nbsp;&nbsp;&nbsp;&nbsp; Pagina &nbsp;&nbsp;&nbsp;&nbsp; </td>";
		 ls_Cabecera_Sist += "<td class='bullet' align='center'> &nbsp;&nbsp;&nbsp;&nbsp; Bloqueado &nbsp;&nbsp;&nbsp;  </td>";
		 ls_Cabecera_Sist += "<td class='bullet' align='center'> &nbsp;&nbsp;&nbsp;&nbsp; Sis. Padre &nbsp;&nbsp;&nbsp;&nbsp;</td>";
		 ls_Cabecera_Sist += "<td class='bullet' align='right'> <img src='../../00utils/gif/m45_2.gif' border='0'> </td></tr>";
		 
         
		 String qryPerfil = "SELECT cc_perfil FROM seg_perfil WHERE cs_bloqueado = 'N' ";
		 qryPerfil += "AND cc_perfil = '"+ms_Perfil+"'";
		 ResultSet curPerfiles = con.queryDB(qryPerfil);
		 String ls_Perfil = "";
		 
		 
		 /////********* INICIA TABLA GENERAL
		 out.print("<table  border='0'>");
		 while(curPerfiles.next())  //------- WHILE DE PERFILES
		 {
			ls_Perfil = curPerfiles.getString("cc_perfil");
			//out.print("<br>" + ls_Perfil);
			
			String qrySistemas = "SELECT DISTINCT ppp.c_sistema, sis.d_descripcion, sis.cg_liga, sis.b_bloqueado, sis.sg_sw_accion_click ";
			qrySistemas += "FROM seg_rel_perfil_perfpr ppp, seg_sistemas sis ";
            qrySistemas += "WHERE sis.c_sistema = ppp.c_sistema AND  ppp.cc_perfil = '"+ls_Perfil+"' order by ppp.c_sistema";
			ResultSet curSistemas = con.queryDB(qrySistemas);
			String ls_Sistema = "";
			
			out.print("<tr> <td colspan='4'> ");  //  ******  RENGLON DE TABLA GENERAL
			out.print("<br><table width='600px' border='0' cellspacing='0' cellpadding='0'><tr><td>");
			boolean lb_cabecera_sist = true;
			while(curSistemas.next())  //------- WHILE DE SISTEMAS
			{
			   ls_Sistema = curSistemas.getString("c_sistema").trim();
			   ms_Trama_Sist += ls_Sistema + "|";
			   
			   out.print("<tr> <td colspan='4'> ");  //  ******  RENGLON DE TABLA GENERAL
			   
			   
			   out.print("<br><table width='100%' frame='below' cellspacing='0' cellpadding='0'>");
			   if (lb_cabecera_sist)   out.print("<tr  id='tr_cab_"+ls_Sistema+"'  style='display:\"\";' >");
			   else                    out.print("<tr  id='tr_cab_"+ls_Sistema+"'  style='display:\"none\";' >");
			   lb_cabecera_sist = false;  //----- PARA KE LA CABECERA DE SISTEMAS SOLO APARESCA UNA SOLA VEZ.
			   out.print(ls_Cabecera_Sist);
			   out.print("<tr>");
			   out.print("<td width='10px' align='right'> <a href='#0' OnClick='mtdDespliegaSis(  \"tr_"+ls_Sistema+"\" ,  \"tr_cab_"+ls_Sistema+"\" )'> +</a></td>");
			   out.print("<td width='100px' align='center'><a>" + ls_Sistema + "</a></td>");
			   out.print("<td width='*px' align='center'><a>" + curSistemas.getString("d_descripcion") + "</a></td>");
			   out.print("<td width='100px' align='center'><a>" + curSistemas.getString("cg_liga") + "</a></td>");
			   out.print("<td width='100px' align='center'><a>" + curSistemas.getString("b_bloqueado") + "</a></td>");
			   out.print("<td width='100px' align='center'><a>" + curSistemas.getString("c_sistema") + "</a></td>");
			   out.print("<td width='10px'> &nbsp;</td>");
			   
			   
			   out.print("</tr> </table>");
			   out.print("</td> </tr>");
			   
			   
			   
			   String qryPerfProt = "SELECT rpp.c_perfil_prot, pp.d_descripcion, pp.b_bloqueado ";
			   qryPerfProt += "FROM seg_rel_perfil_perfpr rpp, seg_perfilprot pp ";
			   qryPerfProt += "WHERE rpp.cc_perfil = '"+ls_Perfil+"' AND rpp.c_sistema = '"+ls_Sistema+"' ";
			   qryPerfProt += "AND rpp.c_perfil_prot = pp.c_perfil_prot order by rpp.c_perfil_prot";
			   ResultSet curPerfProt = con.queryDB(qryPerfProt);
			   String ls_PerfProt = "";
			   while (curPerfProt.next())
			   {
		          out.print("</td></tr>   <tr id='tr_"+ls_Sistema+"' style='display:none;'>");  //  ******  RENGLON DE TABLA GENERAL
			      out.print("<td width='09%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>");
			      out.print("<td colspan='3'>");                        //  ******  RENGLON DE TABLA GENERAL

			      ls_PerfProt = curPerfProt.getString("c_perfil_prot");
				  out.print("<br><table border='1' cellspacing='0' cellpadding='0'><tr><td>");
				  out.print("<table border='0'>");
				  out.print("<tr>");
				  out.print("<td class='menu' align='center'> <a href='#0' OnClick='mtdDespliegaFac(  tr_"+ls_PerfProt+"  )'> + </a>  &nbsp;&nbsp;Perf. Prot. </td>");
				  
		          out.print("<td class='menu' align='center'> Descripci&oacute;n </td>");
		          out.print("<td class='menu' align='center'> Bloqueado </td></tr>");
				  
				  out.print("<tr>");
				  out.print("<td><a>&nbsp;&nbsp;" + ls_PerfProt + "&nbsp;&nbsp;</a></td>");
				  out.print("<td><a>&nbsp;&nbsp;&nbsp;" + curPerfProt.getString("d_descripcion") + "&nbsp;&nbsp;&nbsp;</a></td>");
				  out.print("<td align='center'><a>" + curPerfProt.getString("b_bloqueado") + "</a></td>");
				  out.print("</tr> </table>");
				  out.print("</td> </tr></table>");
				  
				  
				  
			      out.print("</td></tr>   <tr id='tr_"+ls_PerfProt+"' style='display:none;' ><td>&nbsp;&nbsp;&nbsp;&nbsp;");  //  ******  RENGLON DE TABLA GENERAL
				  out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>");
				  out.print("<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>");
			      out.print("<td colspan='2'>");                        //  ******  RENGLON DE TABLA GENERAL
				  
				  String qryFacultades = "SELECT fpp.c_facultad, fac.d_descripcion, fac.p_pagina, fac.b_bloqueado, fac.sg_sw_quita_mnu, tp.d_descripcion ";
				  qryFacultades += "FROM seg_facperfpr fpp, seg_facultades fac, seg_tipofac tp ";
				  qryFacultades += "WHERE fpp.c_sistema = '"+ls_Sistema+"' ";
				  qryFacultades += "AND fpp.c_perfil_prot = '"+ls_PerfProt+"'  AND fpp.c_facultad = fac.c_facultad ";
				  qryFacultades += "AND tp.t_tipo_facultad = fac.t_tipo_facultad ";
				  qryFacultades += "order by fpp.c_sistema, fpp.c_perfil_prot";
				  
				  ResultSet curFacultad = con.queryDB(qryFacultades);
				  out.print("<br><table border='1' cellspacing='0' cellpadding='0'><tr><td>");
				  out.print("<table border='0' >");
                  out.print("<tr><td class='celdaMenu2' align='center'> <a>Facultades </a></td>");
		          out.print("<td class='celdaMenu2' align='center'> <a>Descripci&oacute;n </a></td>");
				  out.print("<td class='celdaMenu2' align='center'> <a>Pagina </a></td>");
				  out.print("<td class='celdaMenu2' align='center'> <a>Bloqueado </a></td>");
				  out.print("<td class='celdaMenu2' align='center'> <a>Tipo Fac </a></td>");
		          out.print("<td class='celdaMenu2' align='center'> <a>Quitar Men� </a></td></tr>");
				  while (curFacultad.next())  // ----- 	WHILE DE FACULTADES
				  {          
				     out.print("<tr>");
				     out.print("<td><a>" + curFacultad.getString("c_facultad") + "</a></td>");
				     out.print("<td><a>" + curFacultad.getString(2) + "</a></td>");
					 out.print("<td><a>" + curFacultad.getString("p_pagina") + "</a></td>");
				     out.print("<td align='center'><a>" + curFacultad.getString("b_bloqueado") + "</a></td>");
					 out.print("<td><a>" + curFacultad.getString(6) + "</a></td>");
					 out.print("<td align='center'><a>" + curFacultad.getString("sg_sw_quita_mnu") + "</a></td></tr> ");
				  }
				  out.print("</table>    </td> </tr></table>");
				  out.print("</td> </tr> ");  //  ******  RENGLON DE TABLA GENERAL
				  curFacultad.close();
			   }
			   curPerfProt.close();
			}
			curSistemas.close();
		 }
		 out.print("</table>");
		 
		 curPerfiles.close();
		 con.cierraConexionDB();
		 
		 out.print("</td> </tr> </table>");
%>
	  
	  
	  <br><br>
	  <table align="right">
	   <tr>
	     <td class="celda02" width="50" align="center">
		   <a href="javascript:mtdDespliegaTodas();" >
		     Mostrar Facultades
		   </a>
	     </td>		

         <td>&nbsp;</td>
         <td class="celda02" width="50" align="center">
          <a href="javascript:window.print();" >
           Imprimir
          </a>
         </td>			
	         
		 <td>&nbsp;</td>
	     <td class="celda02" width="50" align="center">
		   <a href="javascript:window.close();" >
		     Cancelar
		   </a>
	     </td>		
		 
	   </tr>
      </table>



	  
	  <Script language="JavaScript">
        var ms_Trama_Sist = "<%=ms_Trama_Sist%>";
		var mv_Trama_Sist; 
		if ( ms_Trama_Sist != "" )    mv_Trama_Sist = ms_Trama_Sist.split("|");
		
		
	    /*******************************************************************************************
		*
		********************************************************************************************/
		function mtdDespliegaSis(tr , cab)
		{
		   var bl_encontrado = false;
		   for (var ind=0; ind<mv_Trama_Sist.length-1; ind++)
		   {
			  if (bl_encontrado)
			  {
				  if ( typeof(document.getElementById("tr_cab_" + mv_Trama_Sist[ind])) == "object" )
				  {
					 mtdDespliega( "tr_cab_" + mv_Trama_Sist[ind] );
				     return;
				  }
				  else alert("Vacio");
			  }
			  
			  if ( "tr_cab_"+mv_Trama_Sist[ind] == document.getElementById(cab).id )
			  {
		          var li_num_trs = document.getElementsByTagName('tr').length;
		          for (var i=0; i<li_num_trs; i++)
		          {
					  if ( document.getElementsByTagName('tr')[i].id == tr )
					  {
					      if ( document.getElementsByTagName('tr')[i].style.display == "none" ) document.getElementsByTagName('tr')[i].style.display = "";
						  else   document.getElementsByTagName('tr')[i].style.display = "none";
					  }
					  
		          }		
				  
				  bl_encontrado = true;
			  }
		   }
		   
		}

		
	    /*******************************************************************************************
		*
		********************************************************************************************/
		function mtdDespliega(tr)
		{
		   if ( document.getElementById(tr).style.display == "" )  document.getElementById(tr).style.display = "none";
		   else                                                    document.getElementById(tr).style.display = "";
		}




	    /*******************************************************************************************
		*
		********************************************************************************************/
		function mtdDespliegaFac(tr)
		{
		   if ( tr.style.display == "" )  tr.style.display = "none";
		   else                           tr.style.display = "";
		}
		


	    /*******************************************************************************************
		*
		********************************************************************************************/
		
		function mtdDespliegaTodas()
		{
		   var li_num_trs = document.getElementsByTagName('tr').length;
		   for (var i=0; i<li_num_trs; i++)
		   {
		      if ( document.getElementsByTagName('tr')[i].style.display = "none" )
			    document.getElementsByTagName('tr')[i].style.display = "";
		   }
		}
		


	  </script>	  
	  
	  
	  
	  
<%
}
catch (Exception err){
	out.print("Error: "  + err);
}
finally{
	con.cierraConexionDB();
}
%>





</body>

</html>
