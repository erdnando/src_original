<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<%@ include file="/01principal/menu.jspf"%>

<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script async type="text/javascript" src="/nafin/00utils/SgDataCrypto/org/sgdatajs/SeguriLibJS_module_PKCS12v1.0.min.js"></script>
<script type="text/javascript" src="15certificadodigital_ext.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%

String instal = (request.getParameter("instal")!=null)?request.getParameter("instal"):"";
String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";

if("".equals(opcion)){
	if("".equals(strSerial)){
		opcion = "3";
	}else{
		opcion = "1";
	}
}

System.out.println("strSerial === "+strSerial);
System.out.println("opcion === "+opcion);

if (strTipoUsuario.equals("PYME")) {
%>
<%@ include file="/01principal/01pyme/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01pyme/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01pyme/pie.jspf"%>
<%
} else if (strTipoUsuario.equals("EPO")) {
%>
<%@ include file="/01principal/01epo/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01epo/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01epo/pie.jspf"%>
<%
} else if (strTipoUsuario.equals("IF")) {
%>
<%@ include file="/01principal/01if/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01if/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01if/pie.jspf"%>
<%
} else if (strTipoUsuario.equals("UNIV")) {
%>
	<%@ include file="/01principal/01uni/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01uni/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01uni/pie.jspf"%>
<%
} else if (strTipoUsuario.equals("NAFIN")) {
%>
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01nafin/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01nafin/pie.jspf"%>
<%
} else if (strTipoUsuario.equals("IFNB CEDI")) {
%>
<%@ include file="/01principal/01cedi/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01cedi/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01cedi/pie.jspf"%>
<%
} else {

	throw new AppException("Tipo de usuario: " + strTipoUsuario + "...No implementado aun");
}
%>

<form id='formAux' name="formAux" target='_new'></form>
<input type="hidden" id="hidiNoUsuario" value="<%=iNoUsuario%>">
<input type="hidden" id="hidStrNombreUsuario" value="<%=strNombreUsuario%>">
<input type="hidden" id="hidStrSerial" value="<%=strSerial%>">
<input type="hidden" id="instal" value="<%=instal%>">
<input type="hidden" id="hidOpcion" value="<%=opcion%>">

</body>
</html>

