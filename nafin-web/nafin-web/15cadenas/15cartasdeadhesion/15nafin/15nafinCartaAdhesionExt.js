
Ext.onReady(function() {    

var ESTATUS_FORMALIZADO = 'Formalizado';
var FACTORAJE_AL_VENCIMIENTO ="Al Vencimiento";
// ............... Firma Electr�nica  ...........................................................  
    
    var eliminarCarta = function(record){
        if(record.get("estatus")== ESTATUS_FORMALIZADO){
            Ext.Msg.show({
                title: "Eliminar Carta de Adhesi�n",
                msg: '�Esta seguro de Eliminar la Carta de Adhesi�n con folio: <strong>' + 
                record.get('folio') + '<\/strong>?<br/>�Esta acci�n no podr� ser revertida!',
                buttons: Ext.Msg.YESNO,
                icon: Ext.Msg.QUESTION,
                fn: function (btn) {
                    if (btn === 'yes') {
                        Ext.Ajax.request({
                            url: '15nafinCartaAdhesionExt.data.jsp',
                            params: Ext.apply(  {
                                informacion: 'cartaaSecure',
                                icCartaAdhesion:    record.get('icCartaAdhesion'),
                                icEPO:              record.get('icEPO'),
                                icIF:              record.get('icIF')
                            }),
                            callback: eliminarCallBack
                        });                     
                    }
                }
            });
        }else{
            Ext.MessageBox.alert('Eliminar carta', "No se puede eliminar la carta");
        }
    }    
    

        var error1FechasForm = "Debe capturar la fecha inicial y final";
        var error2FechasForm = "La fecha final debe ser posterior a la inicial";    
        var errorFolio       = "El folio debe tener el formato aaaa/folio";
        
      function validarCampos() {
        var fechaUno = Ext.getCmp('fechaSolicitudUno');
        var fechaDos = Ext.getCmp('fechaSolicitudDos');
        var fechaUnoVal = fechaUno.getValue();
        var fechaDosVal = fechaDos.getValue();
        if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)) {
            fechaUno.markInvalid(error1FechasForm);
            fechaDos.markInvalid(error1FechasForm);
            return error1FechasForm;
        }
        if (fechaUnoVal != null && fechaUnoVal > fechaDosVal) {
            fechaUno.markInvalid(error2FechasForm);
            fechaDos.markInvalid(error2FechasForm);
            fechaDos.focus();
            return error2FechasForm;
        }
        var folio = Ext.getCmp('txtFolio').getValue().trim();
        if (folio!= null && folio != ""){
            var arrayFolio = folio.split('/');
            if (arrayFolio.length ==2){
                return true;
            }
            else{
                Ext.getCmp('txtFolio').markInvalid(errorFolio);
                return errorFolio;
            }        
        }
        return true;
    }
    
    

// ............... Definicion de Stores ...........................................................

    Ext.define('gridModel', {
        extend: 'Ext.data.Model',
        fields: 
        [
            {name:'icCartaAdhesion'   , type: 'string'},
            {name:'icEPO'             , type: 'string'},
            {name:'icIF'              , type: 'string'},
            {name:'folio'             , type: 'string'},
            {name:'nombreCadena'      , type: 'string'},
            {name:'nombreIF'          , type: 'string'},
            {name:'nombreMoneda'      , type: 'string'},
            {name:'tipoFactoraje'     , type: 'string'},
            {name:'fechaFirma'        , type: 'string'},
            {name:'sobretasa'         , type: 'string'},
            {name:'numeroFirmantesIF' , type: 'string'},
            {name:'numeroFirmantesEPO', type: 'string'},
            {name:'nombreFirmantesIF' , type: 'string'},
            {name:'nombreFirmantesEPO', type: 'string'},
            {name:'permitirFirma'     , type: 'string'},
            {name:'estatus'           , type: 'string'}
        ]
    });
    
    var gridStore = Ext.create('Ext.data.Store', {
        model: 'gridModel',
        proxy: {
            type: 'ajax',
            url: '15nafinCartaAdhesionExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'dataGrid'
            }
        },
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }
    });

    Ext.define('ModelCatalogos',    {
        extend: 'Ext.data.Model',
        fields: [
            { name: 'clave', type: 'string' },
            { name: 'descripcion', type: 'string' }             
        ]
    });
    
    function eliminarCallBack(opts, success, response) {
        var infoResponse = Ext.JSON.decode(response.responseText);
        if (success == true && infoResponse.success == true ) {
            Ext.MessageBox.alert('Error', infoResponse.msg);
            gridStore.reload();
        }else{
            Ext.MessageBox.alert('Error', infoResponse.msg);
        }
    }
    
   

    var jsonStoreCatEPOS = new Ext.data.JsonStore({
        storeId: 'jsonStoreCatEPOS',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '15nafinCartaAdhesionExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros',
                idProperty: 'clave'
            },
            extraParams: {
                informacion: 'catEPOS'
            }        
        },
        listeners: {
            exception: NE.util.mostrarProxyAjaxError
        },    
        fields: [
            { name: 'clave', type: 'string' },
            { name: 'descripcion', type: 'string' }             
        ]
    });
            
    var jsonStoreCatIF = new Ext.data.JsonStore({
        storeId: 'jsonStoreCatIF',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '15nafinCartaAdhesionExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros',
                idProperty: 'clave'
            },
            extraParams: {
                informacion: 'catIF'
            }        
        },
        listeners: {
            exception: NE.util.mostrarProxyAjaxError
        },    
        fields: [
            { name: 'clave', type: 'string' },
            { name: 'descripcion', type: 'string' }             
        ]
    });            
            
    var storeMoneda = new Ext.data.ArrayStore({
         fields: ['clave', 'descripcion'],
         data : [
            ['1','Moneda Nacional'],
            ['54','D�lar Americano ']
         ]
    }) ;
        

    var storeCatEstatus = new Ext.data.ArrayStore({
         fields: ['clave', 'descripcion'],
         data : [
            ['I','En Firma IF'],
            ['E','En Firma EPO '],
            ['F','Formalizado'],
            ['A','Autorizado NAFIN']
         ]
    }) ;
        
    
    var storeTipoFactoraje = new Ext.data.ArrayStore({
             fields: ['clave', 'descripcion'],
             data : [
                    ['N','Factoraje Normal'],
                    ['V','Factoraje Al Vencimiento']
             ]
    }) ;   
        
// ............... Definicion de Funciones ...........................................................     

    function generarPDF(record){
        Ext.Ajax.request({
            url: '15nafinCartaAdhesionExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'GenerarArchivoPDF',
                ic_epo: record.get("icEPO"),
                ic_CartaAdhesion: record.get("icCartaAdhesion"),
                ic_if:  record.get("icIF")
            }),
            callback: descargaArchivos
        });    
    }

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;             
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};              
            var forma   = Ext.getDom('formAux'); 
            forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method        = 'post';
            forma.target        = '_self';  
            forma.submit();     
            }else {
                NE.util.mostrarConnError(response,opts);
        }
    }    
    
    var seleccionComboEpo = function(){
        Ext.getCmp('cbIF').setValue("");
        var cmbEPO = Ext.getCmp('cbEPO').getValue();
        jsonStoreCatIF.load({
            params:{
                informacion: 'catIF',
                icEPO: cmbEPO
            }
        });
    }    
    

    function autorizarCallBack(opts, success, response) {
        var infoResponse = Ext.JSON.decode(response.responseText);
        console.log(infoResponse);
        if (success == true && infoResponse.success == true ) {
            gridStore.reload();
            Ext.MessageBox.alert('Autorizar Carta de Adhesi�n', infoResponse.msg);
        }else{
            Ext.MessageBox.alert('Error', infoResponse.msg);
        }
         Ext.getCmp('gridCartas').el.unmask();
    }
    
    function autorizarCarta(record){
        Ext.getCmp('gridCartas').el.mask('Autorizando Carta...', 'x-mask-loading');               
        Ext.Ajax.request({
            url: '15nafinCartaAdhesionExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'autorizarCaa',
                icCartaAdhesion: record.get('icCartaAdhesion'),
                icEPO:    record.get('icEPO'),
                icIF:     record.get('icIF')
            }),
            callback: autorizarCallBack
        });
    
    }


    function rechazarCallBack(opts, success, response) {
        var infoResponse = Ext.JSON.decode(response.responseText);
        if (success == true && infoResponse.success == true ) {
            gridStore.reload();
            Ext.MessageBox.alert('Rechazar Carta de Adhesi�n', infoResponse.msg);
        }else{
            Ext.MessageBox.alert('Error', infoResponse.msg);
        }
         Ext.getCmp('gridCartas').el.unmask();
    }
    
    function rechazarCarta(record){
        Ext.getCmp('gridCartas').el.mask('Rechazando Carta...', 'x-mask-loading');               
        Ext.Ajax.request({
            url: '15nafinCartaAdhesionExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'rechazarCaa',
                icCartaAdhesion: record.get('icCartaAdhesion'),
                icEPO:    record.get('icEPO'),
                icIF:     record.get('icIF')
            }),
            callback: rechazarCallBack
        });
    
    }

// ............... Grid Principal ...........................................................
   var gridPanel = Ext.create('Ext.grid.Panel', {
        id: 'gridCartas',
        store: gridStore,
        stripeRows: true,
        title: 'Cartas de Adhesi�n',
        width: 944,
        height: 400,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
            header: "Folio",
            dataIndex: 'folio',
            width: 90,
            sortable: true,
            hideable: true            
        }, {
            header: "Cadena Productiva",
            dataIndex: 'nombreCadena',
            width: 250,
            sortable: true,
            hideable: true
        }, 
        {
            header: "Intermediario Financiero",
            dataIndex: 'nombreIF',
            width: 250,
            sortable: true,
            hideable: true
        },
        {
            header: "Moneda",
            dataIndex: 'nombreMoneda',
            width: 110,
            sortable: true,
            hideable: true
        }, {
            header: "Tipo de Fatoraje",
            dataIndex: 'tipoFactoraje',
            align: 'left',
            width: 100,
            sortable: true,
            hideable: true        
        }, {
            header: "Fecha de Firma",
            dataIndex: 'fechaFirma',
            align: 'center',
            width: 90,
            sortable: true,
            hideable: true   
        }, {
            header: "Sobretasa 1er plazo",
            align: 'right',
            dataIndex: 'sobretasa',
            hideable: true,
            sortable: true,
            width: 120,
            renderer: function (value, metaData, record) {
                if (record.get('tipoFactoraje')==FACTORAJE_AL_VENCIMIENTO){
                    return "N/A";
                }
                else {
                    return value;
                }
            }
        }, 
        {
            header: "Estatus",
            dataIndex: 'estatus',
            width: 120,
            align: 'center',
            sortable: true,
            hideable: true
        },
         {
            header: "Formantes IF",
            align: 'center',
            dataIndex: 'numeroFirmantesIF',
            hideable: true,
            sortable: true,
            width: 100
        }, 
         {
            header: "Nombre Firmantes IF",
            align: 'left',
            dataIndex: 'nombreFirmantesIF',
            hideable: true,
            sortable: true,
            width: 200
        }, 
         {
            header: "Formantes EPO",
            align: 'center',
            dataIndex: 'numeroFirmantesEPO',
            hideable: true,
            sortable: true,
            width: 100
        }, 
         {
            header: "Nombre Firmantes EPO",
            align: 'left',
            dataIndex: 'nombreFirmantesEPO',
            hideable: true,
            sortable: true,
            width: 200
        },        
        {
            xtype: 'actioncolumn',
            header: "Carta Adhesi�n",
            menuText: "Carta Adhesi�n",
            align: 'center',
            dataIndex: 'icCartaAdhesion',
            hideable: true,
            sortable: true,
            width: 90,
            items: [{
                iconCls: 'icoPdf',
                tooltip: 'Descargar Carta de Adhesi�n',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    generarPDF(grid.getStore().getAt(rowIndex));
                }
            }
            ]
        },        
        {
            xtype: 'actioncolumn',
            header: "Autorizar/Rechazar",
            menuText: "Autorizar/Rechazar",
            align: 'center',
            dataIndex: 'icCartaAdhesion',
            hideable: true,
            sortable: true,
            width: 120,
            items: [
            {
                iconCls: 'correcto marginR15p',
                tooltip: 'Autorizar Carta de Adhesi�n',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    Ext.Msg.confirm({
                        title: "Autorizar Carta de Adhesi�n",
                        msg: 'Favor de realizar los cambios mencionados en la nueva Carta de Adhesi�n en Nafinet antes de cambiar el estatus.<br/><br/>' +
                        'Folio:<tab/><tab/><strong>'+record.get('folio') + '<\/strong><br/>'+
                        'EPO:<tab/><tab/><strong>'+record.get('nombreCadena')+ '<\/strong><br/>'+
                        'IF:<tab/><tab/><strong>'+record.get('nombreIF')+ '<\/strong><br/>'+
                        'Factoraje:<tab/><strong>'+record.get('tipoFactoraje')+ '<\/strong><br/>'+
                        'Moneda:<tab/><strong>'+record.get('nombreMoneda')+ '<\/strong><br/><br/>'+
                        '�Esta seguro que desea Autorizar la Carta de Adhesi�n?<br/><br/>',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                autorizarCarta(record);
                            }
                        }
                    });                    
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('estatus') != ESTATUS_FORMALIZADO;
                }
            },            
            {
                iconCls: 'icoRechazar marginR15p',
                tooltip: 'Rechazar Carta de Adhesi�n',
                handler: function (grid, rowIndex, colIndex) {
                    var rec = grid.getStore().getAt(rowIndex);
                    Ext.Msg.show({
                        title: "Rechazar Carta de Adhesi�n",
                        msg: '�Esta seguro que desea Rechazar la Carta de Adhesi�n con folio: <strong>' + 
                            rec.get('folio') + '<\/strong>?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                rechazarCarta(rec);
                            }
                        }
                    });
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatus') != ESTATUS_FORMALIZADO);
                }
            }
            ]
        },
        {
            xtype: 'actioncolumn',
            header: "Eliminar",
            menuText: "Acciones",
            align: 'center',
            dataIndex: 'icCartaAdhesion',
            hideable: true,
            sortable: true,
            width: 90,
            items: [           
            {
                iconCls: 'icoEliminar',
                tooltip: 'Eliminar Carta de Adhesi�n',
                handler: function (grid, rowIndex, colIndex) {
                    var rec = grid.getStore().getAt(rowIndex);
                    eliminarCarta(rec);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatus') != ESTATUS_FORMALIZADO);
                }
            }
            ]
        }        
        ],
        bbar: ['->','-',
                {
                    xtype:'button',
                    text: 'Exportar a Excel',
                    id: 'btnXLS',
                    iconCls: 'icoBotonXLS',
                    handler: function(boton, evento) {
                        Ext.Ajax.request({
                            url: '15nafinCartaAdhesionExt.data.jsp',
                            params: Ext.apply(primerPanel.getForm().getValues(),{
                                informacion:  'GenerarArchivoXSL'}),
                            callback: descargaArchivos
                        });
                    }   
                },'-'                        
            ]        
    });

   

// ............... Panel Principal ...........................................................

var primerPanel = new Ext.form.FormPanel({
        name: 'formParams01',
        id: 'formParams01',
        width: 700,
        frame: true,
        height: 'auto',
        bodyStyle: 'padding: 6px',
        style: 'margin:0 auto;',
        title: 'Cartas de Adhesi�n',
        items: [
                {
                    xtype: 'combo',
                    id: 'cbEPO',
                    name: 'cbEPO',
                    fieldLabel: 'Cadena Productiva:',
                    padding: '5 5 5 20',
                    width: 600,
                    labelWidth: 150,
                    emptyText: 'Seleccionar una Cadena...',
                    store: jsonStoreCatEPOS,
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: true,
                    triggerAction : 'all',
                    typeAhead: true,
                    minChars : 1,                    
                    mode: 'local',
                    listeners:{
                            select: seleccionComboEpo
                    }                    
                },
                {
                    xtype: 'combo',
                    id: 'cbIF',
                    name: 'cbIF',
                    fieldLabel: 'Intermediario Financiero:',
                    padding: '5 5 5 20',
                    width: 600,
                    labelWidth: 150,
                    emptyText: 'Seleccionar un Intermediario...',
                    store: jsonStoreCatIF,
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: true
                },                
                {
                    xtype: 'combo',
                    id: 'cbMoneda',
                    name: 'cbMoneda',
                    fieldLabel: 'Moneda',
                    labelWidth: 150,
                    padding: '5 5 5 20',
                    width: 600,
                    emptyText: 'Seleccionar Moneda...',
                    store: storeMoneda, 
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: false                     
                },                
                {
                    xtype: 'combo',
                    id: 'cbTipoFactoraje',
                    name: 'cbTipoFactoraje',
                    fieldLabel: 'Tipo de Factoraje:',
                    padding: '5 5 5 20',
                    labelWidth: 150,
                    width: 600,
                    emptyText: 'Seleccionar...',
                    store: storeTipoFactoraje, 
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: false                  
                },
                {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 'fechaSolicitudUno',
                    name: 'fechaSolicitudUno',
                    fieldLabel: 'Fecha de Solicitud',
                    labelWidth: 150,
                    padding: '0 5 0 20',
                    width: 350,
                    emptyText: 'Desde',
                    displayField: 'descripcion'
                }, {
                    xtype: 'datefield',
                    id: 'fechaSolicitudDos',
                    name: 'fechaSolicitudDos',
                    fieldLabel: 'al',
                    labelWidth: 30,
                    padding: '0 5 0 25',
                    width: 220,
                    emptyText: 'Hasta'
                }]
            },                 
                {
                    xtype: 'combo',
                    id: 'cbEstatus',
                    name: 'cbEstatus',
                    fieldLabel: 'Estatus:',
                    store: storeCatEstatus, 
                    labelWidth: 150,
                    padding: '5 5 5 20',
                    width: 600,
                    emptyText: 'Seleccionar...',
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: false                       
                },
            {
                xtype: 'textfield',
                id: 'txtFolio',
                name: 'txtFolio',                
                fieldLabel: 'No. Folio:',
                emptyText: 'aaaa/folio',
                labelWidth: 150,
                padding: '5 5 5 20',
                width: 600
            }
        ],
        buttons: [{
            text: 'Buscar',
            id: 'bobtnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                var resultado = validarCampos();
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else {
                    gridPanel.el.mask('Cargando...', 'x-mask-loading');
                    gridStore.load({
                        params: Ext.apply(primerPanel.getForm().getValues()),
                        callback: function(records, operation, success) {
                            var jsonResonse = Ext.JSON.decode(operation.response.responseText);
                            if(jsonResonse.total == 0){
                                Ext.MessageBox.alert('Aviso', "No se encontraron registros.");
                            }
                            gridPanel.el.unmask();    
                        }                        
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                Ext.getCmp('cbIF').setValue("");
                primerPanel.getForm().reset();
                jsonStoreCatIF.load({
                    params:{
                        informacion: 'catIF',
                        icEPO: ""
                    }
                });
            }
        }]       
    });


// ............... Contenedor Principal ...........................................................

    var contenedor = new Ext.Container({
            id: 'contenedorPrincipal',
            renderTo: 'areaContenido',
            width: 950,
            style: 'margin:0 auto;',
            items: [
                NE.util.getEspaciador(10),
                primerPanel,
                NE.util.getEspaciador(5),
                gridPanel
                ]
    });

});