
Ext.onReady(function() {    

var templateCargado = false;    
var ic_cartaAdhesion_saved =  null;

var ventana = new Ext.Window({
    resizable: true,
    id: 'ventana',
    title: "Vista Previa",
    autoScroll: true,
    closable: false,
    width: 700,
    height: 500,
    items: [{
        xtype: 'displayfield',
        id: 'displayvista'
    }],
    bbar: {
        xtype: 'toolbar',
        buttonAlign: 'center',
        items: ['->', {
            xtype: 'button',
            text: 'Regresar',
            iconCls: 'icoLimpiar',
            id: 'btnCerrar',
            handler: function () {
                Ext.getCmp('ventana').hide();
                contenedor.el.unmask();
            }
        },' ']
    }
})


// ............... Definicion de Stores ...........................................................
    Ext.define('ModelCatalogos', 	{
        extend: 'Ext.data.Model',
        fields: [
            { name: 'clave', type: 'string' },
            { name: 'descripcion', type: 'string' }				
        ]
    });
        
    var  storeCatEPOS = Ext.create('Ext.data.Store', {
           model: 'ModelCatalogos',
           autoLoad: false,
           proxy: {
                type: 'ajax',
                url: '15altaCartaAdhesionExt.data.jsp',
                reader: {
                    type: 'json',
                    root: 'registros'
                },
                extraParams: {
                    informacion: 'catEPOS'
                },
                listeners: {
                    exception: NE.util.mostrarProxyAjaxError
                }
            }
    });
    
    var storeMoneda = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','Moneda Nacional'],
			['54','D�lar Americano ']
		 ]
	}) ;
        

    var storeTipoFactoraje = new Ext.data.ArrayStore({
             fields: ['clave', 'descripcion'],
             data : [
                    ['N','Factoraje Normal'],
                    ['V','Factoraje Al Vencimiento']
             ]
	}) ;   
        
    var storePlazoMXN = new Ext.data.ArrayStore({
         fields: ['clave', 'descripcion'],
         data : [
                ['p1','Hasta 60 d�as'],
                ['p2','Hasta 120 d�as'],
                ['p3','Hasta 180 d�as'],
                ['p4','Hasta 366 d�as']
         ]
    });        
    
    var storePlazoUSD = new Ext.data.ArrayStore({
         fields: ['clave', 'descripcion'],
         data : [
                ['p1','Hasta 1 mes'],
                ['p2','Hasta 3 meses'],
                ['p3','Hasta 6 meses'],
                ['p4','Hasta 12 meses']
         ]
    });   


// ............... Definicion de Funciones ...........................................................

    function actualizaComboYEtiquetas(icMoneda){
        var combPlazo = Ext.getCmp("cbPlazo");
        if (combPlazo.isDisabled()) {
            combPlazo.setDisabled(false);
        }
        combPlazo.reset();
        var toggle = icMoneda == '54';
        combPlazo.bindStore(toggle ?  storePlazoUSD :  storePlazoMXN );    
        
        var sobretasa1 = Ext.getCmp('sobretasa1');
        var sobretasa2 = Ext.getCmp('sobretasa2');
        var sobretasa3 = Ext.getCmp('sobretasa3');
        var sobretasa4 = Ext.getCmp('sobretasa4');
        
        sobretasa1.setFieldLabel(toggle ? "De 1 a 30 d�as" : "De 1 a 60 d�as");
        sobretasa2.setFieldLabel(toggle ? "De 31 a 90 d�as" : "De 61 a 120 d�as");
        sobretasa3.setFieldLabel(toggle ? "De 91 a 180 d�as" : "De 121 a 180 d�as");
        sobretasa4.setFieldLabel(toggle ? "De 181 a 366 d�as" : "De 181 a 366 d�as");
        
        Ext.getCmp('lblSobreTasa').setMargin(toggle ? "0 0 0 460" : "0 0 0 140");
        sobretasa1.setMargin(toggle ? "15 5 5 360" : "15 5 5 40");
        sobretasa2.setMargin(toggle ? "5 5 5 360" : "5 5 5 40");
        sobretasa3.setMargin(toggle ? "5 5 5 360" : "5 5 5 40");
        sobretasa4.setMargin(toggle ? "5 5 15 360" : "5 5 15 40");
        actualizaComponentesSobreTasa();
    }
        
    function actualizaComponentesSobreTasa(){
        var icPlazo = Ext.getCmp("cbPlazo").getValue();
        var mostrarSobreTasa = Ext.getCmp("cbTipoFactoraje").getValue() == 'N';
        var sobretasa1 = Ext.getCmp('sobretasa1');
        var sobretasa2 = Ext.getCmp('sobretasa2');
        var sobretasa3 = Ext.getCmp('sobretasa3');
        var sobretasa4 = Ext.getCmp('sobretasa4');
        sobretasa1.setValue("");
        sobretasa2.setValue("");
        sobretasa3.setValue("");
        sobretasa4.setValue("");
        Ext.getCmp('lblSobreTasa').setVisible(mostrarSobreTasa);
        
        var sobreTasa1Activo = mostrarSobreTasa; 
        var sobreTasa2Activo = mostrarSobreTasa && (icPlazo=="p2"||icPlazo=="p3"||icPlazo =="p4");
        var sobreTasa3Activo = mostrarSobreTasa && (icPlazo=="p3"||icPlazo =="p4");
        var sobreTasa4Activo = mostrarSobreTasa && icPlazo =="p4";
        
        sobretasa1.setVisible(sobreTasa1Activo);
        sobretasa2.setVisible(sobreTasa2Activo);
        sobretasa3.setVisible(sobreTasa3Activo);
        sobretasa4.setVisible(sobreTasa4Activo);        
        
        Ext.apply(sobretasa1, {allowBlank: !sobreTasa1Activo}, {});
        Ext.apply(sobretasa2, {allowBlank: !sobreTasa2Activo}, {});
        Ext.apply(sobretasa3, {allowBlank: !sobreTasa3Activo}, {});
        Ext.apply(sobretasa4, {allowBlank: !sobreTasa4Activo}, {});
    }  

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();		
            }else {
                NE.util.mostrarConnError(response,opts);
        }
    }    

    function validaFormularios(){
        var validForm1 = Ext.getCmp('formParams01').isValid();
        var validForm2 = Ext.getCmp('formParams02').isValid();
        return   validForm1 && validForm2;
    }

    function getTemplateCarta(){
        var panelCarta = Ext.getCmp('formCartaAdhesion');
        panelCarta.el.mask('Buscando...', 'x-mask-loading');
        Ext.Ajax.request({ 		
            url:'15altaCartaAdhesionExt.data.jsp',
            params :{
                informacion:'rawCartaa'
            },
            success : function(response) { 
                var infoR = Ext.JSON.decode(response.responseText);
                if (infoR.success == true){
                    var label = Ext.getCmp('txtDummy');
                    label.setValue(infoR.template);
                    panelCarta.el.unmask();
                    templateCargado = true;
                }
                else {
                    Ext.Msg.alert("Error", infoR.msg);   
                }
            }
        });
    }

    function callBackVistaPrevia(response) {
        var info = Ext.JSON.decode(response.responseText);
        if(info.success == true){
            Ext.getCmp('displayvista').setValue(info.data.Preview);
            contenedor.el.mask('Generando Vista Previa...', 'x-mask-loading');
            ventana.show();	
        }else{
             Ext.Msg.alert("Error", info.msg);
        }
        
    }    


    function guardarCarta(){
        contenedor.el.mask('Buscando...', 'x-mask-loading');
        Ext.Ajax.request({ 		
            url:'15altaCartaAdhesionExt.data.jsp',
            params :{
                informacion:'generarCartaa',
                data: JSON.stringify(segundoPanel.getForm().getValues()),
                parametros: JSON.stringify(primerPanel.getForm().getValues())
            },
            success : function(response) { 
                var infoR = Ext.JSON.decode(response.responseText);
                if (infoR.success == true){
                    Ext.Msg.alert("Generar Carta", infoR.msg);   
                    ic_cartaAdhesion_saved = infoR.ic_val;
                    Ext.getCmp("btnPrint").setDisabled(false);
                    Ext.getCmp("btnGenerar").setDisabled(true);
                    contenedor.el.unmask();
                }
                else {
                    ic_cartaAdhesion_saved = null;
                    Ext.Msg.alert("Error", infoR.msg);   
                }
            }
        });
    }


var botonesPanel = new Ext.form.FormPanel({
        name: 'botonesPanel',
        id: 'botonesPanel',
        width: 700,
        hidden: true,
        frame: true,
        height: 'auto',
        bodyStyle: 'padding: 5px;',
        style: 'margin:0 auto;',
        buttons: [
            {
                text: 'Imprimir',
                id: 'btnPrint',
                iconCls: 'icoImprimir',
                disabled : true,
                width: 90,
                handler: function (boton, evento) {
                    Ext.Ajax.request({
                        url: '15altaCartaAdhesionExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'GenerarArchivoPDF',
                            ic_epo: Ext.getCmp('cbEPO').getValue(),
                            ic_CartaAdhesion: ic_cartaAdhesion_saved
                        }),
                        callback: descargaArchivos
                    });
                }
            },
            {
                text: 'Vista Previa',
                id: 'btnView',
                iconCls: 'icoLupa',
                width: 90,
                handler: function (boton, evento) {
                    if (validaFormularios()){
                        Ext.Ajax.request({
                            url: '15altaCartaAdhesionExt.data.jsp',
                            params: Ext.apply({
                                informacion: 'vistaPrevia',
                                nombreEpo: Ext.getCmp('cbEPO').getRawValue(),
                                plazo: Ext.getCmp('cbPlazo').getRawValue(),
                                icPlazo: Ext.getCmp('cbPlazo').getValue(),
                                factoraje: Ext.getCmp('cbTipoFactoraje').getRawValue(),
                                moneda: Ext.getCmp('cbMoneda').getRawValue(),                                
                                datos: JSON.stringify(segundoPanel.getForm().getValues())
                            }),
                            success: callBackVistaPrevia,
                            listeners: {
                                exception: NE.util.mostrarConnError
                            }
                        });                  
                    }
                    else{
                        Ext.Msg.alert('Vista Previa', 'Debe llenar los campos marcados');
                    }
                }
            },
            {
                text: 'Generar',
                id: 'btnGenerar',
                iconCls: 'icoGuardarCEDI',
                width: 90,
                handler: function (boton, evento) {
                    if (validaFormularios()){
                        Ext.Msg.show({
                            title: "Generar Carta de Adhesi�n",
                            msg: '�Est� seguro de generar la Carta de Adhesi�n con los datos capturados?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.Msg.QUESTION,
                            fn: function(btn) {
                                if (btn === 'yes') {
                                    guardarCarta();
                                }
                            }
                        });                        
                    }
                    else{
                        Ext.Msg.alert('Generar Carta de Adhesi�n','Debe llenar los campos marcados');
                    }
                }
            }            
        ]        
        });
    

var tercerPanel = new Ext.form.FormPanel({
        name: 'formCartaAdhesion',
        id: 'formCartaAdhesion',
        width: 700,
        hidden: true,
        frame: true,
        title: 'Carta de Adhesi�n',
        autoScroll: true,
        collapsible: true,
        collapsed: true,
        height: 400,
        bodyStyle: 'padding: 6px',
        style: 'margin:0 auto;',
        items: [
            {
                xtype:'displayfield',
                id: 'txtDummy'
            }
        ]
    });

// ............... Segundo Panel  ...........................................................
var segundoPanel = new Ext.form.FormPanel({
        name: 'formParams02',
        id: 'formParams02',
        width: 700,
        hidden: true,
        frame: true,
        height: 'auto',
        bodyStyle: 'padding: 6px',
        style: 'margin:0 auto;',
        items: [
                {
                xtype: 'panel',
                border: false,
                bodyStyle: 'padding: 6px; background-color:#dfe8f5;',
                padding: '0 10 10 10',
                items: [
                    {xtype:'label', text:'Con el fin de generar la Carta de Adhesi�n correctamente, favor de carpturar los siguientes datos:' }
                    ]
                },
                {
                    xtype: 'datefield',
                    id: 'dtFechaEpoNafin',
                    name: 'dtFechaEpoNafin',
                    fieldLabel: 'Fecha de Convenio EPO-NAFIN:',
                    padding: '5 5 5 20',
                    width: 600,
                    forceSelection: true,
                    labelWidth: 400,
                    allowBlank: false,
                    emptyText: 'dd/mm/aaaa'
                },
                {
                    xtype: 'datefield',
                    id: 'dtFechaIFNafin',
                    name: 'dtFechaIFNafin',
                    fieldLabel: 'Fecha de Convenio de Apertura de L�nea de Cr�dito IF-NAFIN:',
                    padding: '5 5 15 20',
                    width: 600,
                    forceSelection: true,
                    labelWidth: 400,
                    allowBlank: false,
                    emptyText: 'dd/mm/aaaa'
                },
                //.................... Sobre tasa...................
                {
                    xtype:'label', 
                    id: 'lblSobreTasa',
                    name: 'lblSobreTasa',
                    text:'Sobre tasa',
                    padding: '5 30 5 30',
                    margin: '0 0 0 140',
                    style: 'background-color:#6198DC; border: solid 1px black;color:white;'
                },
                {
                    xtype: 'bigdecimal',
                    name: 'sobretasa1',
                    id: 'sobretasa1',
                    margin: '15 5 5 40',
                    allowBlank: false,
                    decimalPrecision :4,
                    fieldLabel: 'De 1 a 60 d�as',
                    labelWidth: 120,
                    width: 200,
                    maxValue: 99.9999,
                    minValue: 0
                },
                {
                    xtype: 'bigdecimal',
                    name: 'sobretasa2',
                    id: 'sobretasa2',
                    margin: '5 5 5 40',
                    allowBlank: false,
                    hideTrigger:true,
                    decimalPrecision :4,
                    fieldLabel: 'De 61 a 120 d�as',
                    labelWidth: 120,
                    width: 200,
                    maxValue: 99.9999,
                    minValue: 0
                },
                {
                    xtype: 'bigdecimal',
                    name: 'sobretasa3',
                    id: 'sobretasa3',
                    margin: '5 5 5 40',
                    allowBlank: false,
                    allowDecimals: true,
                    hideTrigger:true,
                    decimalPrecision :4,
                    fieldLabel: 'De 121 a 180 d�as',
                    labelWidth: 120,
                    width: 200,
                    maxValue: 99.9999,
                    minValue: 0
                },
                {
                    xtype: 'bigdecimal',
                    name: 'sobretasa4',
                    id: 'sobretasa4',
                    margin: '5 5 15 40',
                    allowBlank: false,
                    allowDecimals: true,
                    hideTrigger:true,
                    decimalPrecision :4,
                    labelWidth: 120,
                    fieldLabel: 'De 181 a 366 d�as',
                    width: 200,
                    maxValue: 99.9999,
                    minValue: 0
                },    
                {
                    xtype: 'textfield',
                    id: 'txtTitularCuenta',
                    name: 'txtTitularCuenta',
                    fieldLabel: 'Titular de la cuenta:',
                    labelWidth: 150,
                    padding: '15 5 5 20',
                    maxLength: 100,
                    width: 600,
                    allowBlank: false
                },
                {
                    xtype: 'textfield',
                    id: 'txtBanco',
                    name: 'txtBanco',
                    fieldLabel: 'Nombre del Banco',
                    labelWidth: 150,
                    padding: '5 5 5 20',
                    maxLength: 100,
                    width: 600,
                    allowBlank: false
                },
                {
                    xtype: 'textfield',
                    id: 'txtNumeroCuenta',
                    name: 'txtNumeroCuenta',
                    fieldLabel: 'Numero de Cuenta:',
                    labelWidth: 150,
                    padding: '5 5 5 20',
                    maskRe: /[0-9]/,
                    maxLength: 25,
                    width: 600,
                    allowBlank: false
                },                
                {
                    xtype: 'textfield',
                    id: 'txtCuentaClabe',
                    name: 'txtCuentaClabe',
                    fieldLabel: 'Cuenta Clabe:',
                    labelWidth: 150,
                    padding: '5 5 5 20',
                    maskRe: /[0-9]/,
                    maxLength: 19,
                    width: 600,
                    allowBlank: false
                },
                {
                    xtype: 'textfield',
                    id: 'txtSucursal',
                    name: 'txtSucursal',
                    fieldLabel: 'Sucursal:',
                    labelWidth: 150,
                    maxLength: 100,
                    padding: '5 5 5 20',
                    width: 600,
                    allowBlank: true
                },
                {
                    xtype: 'textfield',
                    id: 'txtPlazaBanco',
                    name: 'txtPlazaBanco',
                    fieldLabel: 'Plaza:',
                    labelWidth: 150,
                    maxLength: 100,
                    padding: '5 5 5 20',
                    width: 600,
                    allowBlank: false
                },                
                {
                    xtype: 'textfield',
                    id: 'txtTasaMoratoria',
                    name: 'txtTasaMoratoria',
                    fieldLabel: 'Tasa Moratoria:',
                    labelWidth: 150,
                    padding: '5 5 5 20',
                    width: 600,
                    allowBlank: false
                },
                {
                    xtype: 'numberfield',
                    id: 'txtNumeroFirmasIF',
                    name: 'txtNumeroFirmasIF',
                    fieldLabel: 'N�mero de personas autorizadas a firmar IF:',
                    labelWidth: 280,
                    padding: '5 5 5 20',
                    width: 350,
                    value: 1,
                    maxValue: 5,
                    minValue: 1,
                    allowBlank: false
                },
                {
                    xtype: 'numberfield',
                    id: 'txtNumeroFirmasEPO',
                    name: 'txtNumeroFirmasEPO',
                    fieldLabel: 'N�mero de personas autoriazdas a firmar EPO:',
                    labelWidth: 280,
                    padding: '5 5 5 20',
                    width: 350,
                    value: 1,
                    maxValue: 5,
                    minValue: 1,
                    allowBlank: false
                }                       
        ]        
    });


// ............... Panel Principal ...........................................................

var primerPanel = new Ext.form.FormPanel({
        name: 'formParams01',
        id: 'formParams01',
        width: 700,
        frame: true,
        height: 'auto',
        bodyStyle: 'padding: 6px',
        style: 'margin:0 auto;',
        title: 'Captura carta de adhesion',
        items: [
                {
                    xtype: 'combo',
                    id: 'cbEPO',
                    name: 'cbEPO',
                    fieldLabel: 'Nombre de la EPO:',
                    padding: '5 5 5 20',
                    width: 600,
                    forceSelection: true,
                    labelWidth: 150,
                    allowBlank: false,
                    emptyText: 'Seleccionar una EPO...',
                    store: storeCatEPOS, 
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: true
                },
                {
                    xtype: 'combo',
                    id: 'cbTipoFactoraje',
                    name: 'cbTipoFactoraje',
                    fieldLabel: 'Tipo de Factoraje:',
                    padding: '5 5 5 20',
                    labelWidth: 150,
                    width: 600,
                    forceSelection: true,
                    allowBlank:false,
                    emptyText: 'Seleccionar...',
                    store: storeTipoFactoraje, 
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: false,
                    listeners: {
                        select: function(combo, records, eOpts) {
                            actualizaComponentesSobreTasa();
                        }
                    }                    
                },
                {
                    xtype: 'combo',
                    id: 'cbMoneda',
                    name: 'cbMoneda',
                    fieldLabel: 'Moneda',
                    labelWidth: 150,
                    padding: '5 5 5 20',
                    width: 600,
                    forceSelection: true,
                    allowBlank: false,
                    emptyText: 'Seleccionar Moneda...',
                    store: storeMoneda, 
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: false,
                    listeners: {
                        select: function(combo, records, eOpts) {
                            actualizaComboYEtiquetas(records[0].get('clave'));
                        }
                    }                     
                },
                {
                    xtype: 'combo',
                    id: 'cbPlazo',
                    name: 'cbPlazo',
                    fieldLabel: 'Plazo a Operar:',
                    labelWidth: 150,
                    padding: '5 5 5 20',
                    width: 600,
                    forceSelection: true,
                    allowBlank: false,
                    emptyText: 'Seleccionar...',
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: false,
                    disabled: true,
                    listeners: {
                        select: function(combo, records, eOpts) {
                            actualizaComponentesSobreTasa();
                        }
                    }                        
                }
        ],
        buttons: [
            {
                text: 'Capturar',
                id: 'btnCapturar',
                iconCls: 'modificar',
                width: 90,
                handler: function (boton, evento) {
                    if (Ext.getCmp('formParams01').isValid()){
                        Ext.getCmp('formParams02').setVisible(true);
                        Ext.getCmp('formCartaAdhesion').setVisible(true);
                        Ext.getCmp('botonesPanel').setVisible(true);
                        if (!templateCargado){
                            getTemplateCarta();
                        }
                    }
                    else{
                        Ext.Msg.alert('Capturar carta de adhesi�n', '�Debe seleccionar todos los campos!');
                    }
                }
            }
        ]        
    });


// ............... Contenedor Principal ...........................................................

	var contenedor = new Ext.Container({
            id: 'contenedorPrincipal',
            renderTo: 'areaContenido',
            width: 950,
            style: 'margin:0 auto;',
            items: [
                NE.util.getEspaciador(10),
                primerPanel,
                NE.util.getEspaciador(5),
                segundoPanel,
                NE.util.getEspaciador(5),
                tercerPanel,
                NE.util.getEspaciador(5),
                botonesPanel
                ]
	});

});