<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*, mx.gob.nafin.cartasadhesion.FirmaCartaAdhesion"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/15cartasdeadhesion/15secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar="";
String _serial = "";

log.info("informacion: "+informacion);
FirmaCartaAdhesion carta = new FirmaCartaAdhesion();
Integer icIF = Integer.parseInt(iNoCliente);
if (informacion.equals("rawCartaa")){  
    JSONObject jsonObj   =  new JSONObject();    
    Map datos = new HashMap();
    try{   
        jsonObj.put("template", carta.getTemplateCartaAdhesion().replaceAll("\n", "<br/>")); 
        jsonObj.put("success", true);
    }
    catch(Exception e){
        jsonObj.put("success", false);
        jsonObj.put("msg", e.getMessage());
        log.error(e.getMessage());
        e.printStackTrace();
    }
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("catEPOS")){
    String ic_banco_fondeo = "1";
    JSONObject jsonObj = new JSONObject();
    JSONArray jsObjArray = new JSONArray();
    CatalogoEPO cat = new CatalogoEPO();
    cat.setCampoClave("ic_epo");
    cat.setCampoDescripcion("cg_razon_social");
    cat.setOrden("2");
    cat.setBancofondeo(ic_banco_fondeo);
    cat.setClaveIf(icIF+"");
    List elementos = cat.getListaElementos();
    Iterator it = elementos.iterator();
    while(it.hasNext()) {
        Object obj = it.next();
        if (obj instanceof netropology.utilerias.ElementoCatalogo) {
            ElementoCatalogo ec = (ElementoCatalogo)obj;
            jsObjArray.add(JSONObject.fromObject(ec));
        }
    }
    infoRegresar= "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";    
}
else if (informacion.equals("vistaPrevia")){
        String datos        = request.getParameter("datos");
        String nombreEpo    = request.getParameter("nombreEpo");
        String plazo        = request.getParameter("plazo");
        String factoraje    = request.getParameter("factoraje");
        String moneda       = request.getParameter("moneda");
        String sicPlazo      = request.getParameter("icPlazo");
        
        JSONObject  jsonObject = JSONObject.fromObject(datos);
        Map jsonparametros = jsonObject;
        jsonparametros.put("nombreIF", strNombre);
        jsonparametros.put("nombreEpo", nombreEpo);
        jsonparametros.put("plazo", plazo);
        jsonparametros.put("factoraje", factoraje);
        jsonparametros.put("moneda", moneda);
        jsonparametros.put("icPlazo", sicPlazo);
        JSONObject jsonObj   =  new JSONObject();    
        try {
            String vistaPrevia = carta.getVistaPrevia(jsonparametros);
            Map mVistaPrevia = new HashMap();
            mVistaPrevia.put("Preview", vistaPrevia);
            jsonObj.put("data", JSONObject.fromObject(mVistaPrevia)); 
            jsonObj.put("success", true);
        }
        catch (Exception e){
            jsonObj.put("success", false);
            jsonObj.put("msg", e.getMessage());
            log.error(e.getMessage());
            e.printStackTrace();
        }
        infoRegresar = jsonObj.toString();        
}
else if (informacion.equals("generarCartaa")) {
        String datos  = request.getParameter("data");
        String parametros = request.getParameter("parametros");
        JSONObject jsonReturn   =  new JSONObject();  
        try {
            Map data = JSONObject.fromObject(datos);
            Map<String,String> jsonparametros = JSONObject.fromObject(parametros);
            jsonparametros.putAll(data);
            jsonparametros.put("icIF",icIF+"");
            Integer ic_carta = carta.guardaCartaAdhesion(jsonparametros);
            Map mVistaPrevia = new HashMap();
            jsonReturn.put("msg", "Se ha generado la Carta de Adhesion correctamente"); 
            jsonReturn.put("success", true);
            jsonReturn.put("ic_val", ic_carta+"");
        }
        catch (Exception e){
            jsonReturn.put("success", false);
            jsonReturn.put("msg", e.getMessage());
        }
        infoRegresar = jsonReturn.toString();      
    
}
else if (informacion.equals("GenerarArchivoPDF")){
    Map<String,String> parametros = new HashMap<>(); 
    String ic_cartaAdhesion = request.getParameter("ic_CartaAdhesion");
    String ic_epo = request.getParameter("ic_epo");
    JSONObject jsonObj   =  new JSONObject();    
    try{
        parametros.put("ic_if", iNoCliente);
        parametros.put("ic_cartaAdhesion", ic_cartaAdhesion);
        parametros.put("ic_epo", ic_epo);
        String nombreArchivo = carta.generarCartaPDF( strDirectorioPublicacion, parametros);
        jsonObj.put("success", true); 
        jsonObj.put("urlArchivo", strDirecVirtualTemp +nombreArchivo);		
    }
    catch(Exception e){
        log.error(e.getMessage());
        e.printStackTrace();
        jsonObj.put("success", false); 
        jsonObj.put("msg", e.getMessage()); 
    }
    infoRegresar = jsonObj.toString();
}

//log.debug("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>