<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
        java.text.SimpleDateFormat,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*, mx.gob.nafin.cartasadhesion.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/15cartasdeadhesion/15secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar="";

log.info("informacion: "+informacion);
FirmaCartaAdhesion carta = new FirmaCartaAdhesion();
Integer icEPO = Integer.parseInt(iNoCliente);
if (informacion.equals("dataGrid")){  
    try{   
        String cbIF              = "".equals(request.getParameter("cbIF")) ? null : request.getParameter("cbIF");
        String cbEstatus          = "".equals(request.getParameter("cbEstatus")) ? null : request.getParameter("cbEstatus");
        String txtFolio           = "".equals(request.getParameter("txtFolio")) ? null: request.getParameter("txtFolio");
        String cbMoneda           = "".equals(request.getParameter("cbMoneda")) ? null : request.getParameter("cbMoneda");
        String cbTipoFactoraje    = "".equals(request.getParameter("cbTipoFactoraje")) ? null : request.getParameter("cbTipoFactoraje");
        String fechaSolicitudUno  = "".equals(request.getParameter("fechaSolicitudUno")) ? null: request.getParameter("fechaSolicitudUno");
        String fechaSolicitudDos  = "".equals(request.getParameter("fechaSolicitudDos")) ? null: request.getParameter("fechaSolicitudDos");    
        Map<String,String> filtros = new HashMap<>();
        filtros.put("iceEPO", iNoCliente);
        if (cbIF != null) filtros.put("icIF", cbIF);
        if (cbEstatus != null) filtros.put("icEstatus", cbEstatus);
        if (txtFolio != null) filtros.put("icFolio", txtFolio.trim());
        if (cbMoneda != null) filtros.put("icMoneda", cbMoneda);
        if (cbTipoFactoraje != null) filtros.put("icFactoraje", cbTipoFactoraje);
        if (fechaSolicitudUno != null){
            filtros.put("fechaUno", fechaSolicitudUno);
            filtros.put("fechaDos", fechaSolicitudDos);
        }  
        ArrayList<CartaAdhesion> listado = carta.getListaCartasAdhesion(iNoUsuario, filtros);
        JSONArray jsObjArray = new JSONArray();
        jsObjArray = JSONArray.fromObject(listado);
        infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
    }
    catch(Exception e){
        log.error(e.getMessage());
        e.printStackTrace();
        infoRegresar = "{\"success\": false, \"total\":0 , \"msg\":"+e.getMessage()+" }";
    }
}
else if(informacion.equals("catIF")){
	CatalogoIF cat = new CatalogoIF();
	cat.setCampoClave("I.ic_if");
	cat.setCampoDescripcion("I.cg_razon_social");
        cat.setClaveEpo(icEPO+"");
	List lista = cat.getListaElementosGral();
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lista);
	infoRegresar = "{\"success\": true, \"total\": \""+jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
}
else if (informacion.equals("GenerarArchivoPDF")){
    Map<String,String> parametros = new HashMap<>(); 
    String ic_cartaAdhesion = request.getParameter("ic_CartaAdhesion");
    String ic_if = request.getParameter("ic_if");
    JSONObject jsonObj   =  new JSONObject();    
    try{
        parametros.put("ic_if", ic_if);
        parametros.put("ic_cartaAdhesion", ic_cartaAdhesion);
        parametros.put("ic_epo", icEPO+"");
        String nombreArchivo = carta.generarCartaPDF( strDirectorioPublicacion, parametros);
        jsonObj.put("success", true); 
        jsonObj.put("urlArchivo", strDirecVirtualTemp +nombreArchivo);		
    }
    catch(Exception e){
        log.error(e.getMessage());
        e.printStackTrace();
        jsonObj.put("success", false); 
        jsonObj.put("msg", e.getMessage()); 
    }
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("cartaaSecure")){
    String sicCarta = (request.getParameter("icCartaAdhesion")==null)?"":request.getParameter("icCartaAdhesion");
    String sicIF = (request.getParameter("icIF")==null)?"":request.getParameter("icIF");
    JSONObject jsonObj = new JSONObject();
    JSONArray jsObjArray = new JSONArray();
    
    if(!strPerfil.equals("ADMIN EPO")){
        log.error("!Accion no permitida a usuario¡");
        jsonObj.put("success", new Boolean(false));
        jsonObj.put("msg", "Error no identificado");
    }else{
        try {
            Integer icCarta = Integer.parseInt(sicCarta);
            Integer icIF = Integer.parseInt(sicIF);
            carta.eliminarCartaAdhesion(icCarta, icIF, icEPO);
            jsonObj.put("success", new Boolean(true));
            jsonObj.put("msg", "Se ha eliminado la carta de adhesión");
        }catch(Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            jsonObj.put("success", new Boolean(false));
            jsonObj.put("msg", "Error al eliminar la carta de adhesión: "+e.getMessage());        
        }
    }
    infoRegresar= "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";    
}
else if (informacion.equals("GenerarArchivoXSL")){
        //String cbEPO              = "".equals(request.getParameter("cbEPO")) ? null : request.getParameter("cbEPO");
        String cbIF              = "".equals(request.getParameter("cbIF")) ? null : request.getParameter("cbIF");
        String cbEstatus          = "".equals(request.getParameter("cbEstatus")) ? null : request.getParameter("cbEstatus");
        String txtFolio           = "".equals(request.getParameter("txtFolio")) ? null: request.getParameter("txtFolio");
        String cbMoneda           = "".equals(request.getParameter("cbMoneda")) ? null : request.getParameter("cbMoneda");
        String cbTipoFactoraje    = "".equals(request.getParameter("cbTipoFactoraje")) ? null : request.getParameter("cbTipoFactoraje");
        String fechaSolicitudUno  = "".equals(request.getParameter("fechaSolicitudUno")) ? null: request.getParameter("fechaSolicitudUno");
        String fechaSolicitudDos  = "".equals(request.getParameter("fechaSolicitudDos")) ? null: request.getParameter("fechaSolicitudDos");    
        JSONObject jsonObj   =  new JSONObject();   
        try {        
            Map<String,String> filtros = new HashMap<>();
            if (cbIF != null) filtros.put("icIF", cbIF);
            filtros.put("iceEPO", icEPO+"");
            if (cbEstatus != null) filtros.put("icEstatus", cbEstatus);
            if (txtFolio != null) filtros.put("icFolio", txtFolio.trim());
            if (cbMoneda != null) filtros.put("icMoneda", cbMoneda);
            if (cbTipoFactoraje != null) filtros.put("icFactoraje", cbTipoFactoraje);
            if (fechaSolicitudUno != null){
                filtros.put("fechaUno", fechaSolicitudUno);
                filtros.put("fechaDos", fechaSolicitudDos);
            }               
            String nombreArchivo = carta.getReporteXLS(strDirectorioPublicacion, filtros, iNoUsuario);
            log.info("path:"+strDirecVirtualTemp +nombreArchivo);
            jsonObj.put("success", new Boolean(true)); 
            jsonObj.put("urlArchivo", strDirecVirtualTemp +nombreArchivo);	
        }
        catch(Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            jsonObj.put("success", new Boolean(false));
            jsonObj.put("msg", "Error al autorizar la carta de adhesión: "+e.getMessage());        
        }        
        infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>