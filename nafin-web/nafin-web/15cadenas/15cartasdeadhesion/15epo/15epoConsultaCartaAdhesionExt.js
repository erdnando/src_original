
Ext.onReady(function() {    

var ESTATUS_EPO = 'En Firma EPO';
var FACTORAJE_AL_VENCIMIENTO ="Al Vencimiento";
// ............... Firma Electr�nica  ...........................................................  
    
    var eliminarCarta = function(record){
        if(record.get("estatus")== ESTATUS_EPO){
            Ext.Msg.show({
                title: "Eliminar Carta de Adhesi�n",
                msg: '�Esta seguro de Eliminar la Carta de Adhesi�n con folio: <strong>' + 
                record.get('folio') + '<\/strong>?<br/>�Esta acci�n no podr� ser revertida!',
                buttons: Ext.Msg.YESNO,
                icon: Ext.Msg.QUESTION,
                fn: function (btn) {
                    if (btn === 'yes') {
                        Ext.Ajax.request({
                            url: '15epoConsultaCartaAdhesionExt.data.jsp',
                            params: Ext.apply(  {
                                informacion: 'cartaaSecure',
                                icCartaAdhesion:    record.get('icCartaAdhesion'),
                                icIF:              record.get('icIF')
                            }),
                            callback: eliminarCallBack
                        });                     
                    }
                }
            });
        }else{
            Ext.MessageBox.alert('Eliminar carta', "No se puede eliminar la carta");
        }
    }    
    

        var error1FechasForm = "Debe capturar la fecha inicial y final";
        var error2FechasForm = "La fecha final debe ser posterior a la inicial";    
        var errorFolio       = "El folio debe tener el formato aaaa/folio";
      function validarCampos() {
        
        var fechaUno = Ext.getCmp('fechaSolicitudUno');
        var fechaDos = Ext.getCmp('fechaSolicitudDos');
        var fechaUnoVal = fechaUno.getValue();
        
        
        var fechaDosVal = fechaDos.getValue();
        
        if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)) {
            fechaUno.markInvalid(error1FechasForm);
            fechaDos.markInvalid(error1FechasForm);
            return error1FechasForm;
        }
        if (fechaUnoVal != null && fechaUnoVal > fechaDosVal) {
            fechaUno.markInvalid(error2FechasForm);
            fechaDos.markInvalid(error2FechasForm);
            fechaDos.focus();
            return error2FechasForm;
        }
        var folio = Ext.getCmp('txtFolio').getValue().trim();
        if (folio!= null && folio != ""){
            var arrayFolio = folio.split('/');
            if (arrayFolio.length ==2){
                return true;
            }
            else{
                Ext.getCmp('txtFolio').markInvalid(errorFolio);
                return errorFolio;
            }        
        }
        return true;
    }
    
    

// ............... Definicion de Stores ...........................................................

    Ext.define('gridModel', {
        extend: 'Ext.data.Model',
        fields: 
        [
            {name:'icCartaAdhesion'   , type: 'string'},
            {name:'icIF'              , type: 'string'},
            {name:'folio'             , type: 'string'},
            {name:'nombreIF'          , type: 'string'},
            {name:'nombreMoneda'      , type: 'string'},
            {name:'tipoFactoraje'     , type: 'string'},
            {name:'fechaFirma'        , type: 'string'},
            {name:'sobretasa'         , type: 'string'},
            {name:'numeroFirmantesIF' , type: 'string'},
            {name:'numeroFirmantesEPO', type: 'string'},
            {name:'nombreFirmantesIF' , type: 'string'},
            {name:'nombreFirmantesEPO', type: 'string'},
            {name:'permitirFirma'     , type: 'string'},
            {name:'estatus'           , type: 'string'}
        ]
    });
    
    var gridStore = Ext.create('Ext.data.Store', {
        model: 'gridModel',
        proxy: {
            type: 'ajax',
            url: '15epoConsultaCartaAdhesionExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'dataGrid'
            }
        },
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }
    });

    Ext.define('ModelCatalogos', 	{
        extend: 'Ext.data.Model',
        fields: [
            { name: 'clave', type: 'string' },
            { name: 'descripcion', type: 'string' }				
        ]
    });
    
    function eliminarCallBack(opts, success, response) {
        var infoResponse = Ext.JSON.decode(response.responseText);
        if (success == true && infoResponse.success == true ) {
            Ext.MessageBox.alert('Error', infoResponse.msg);
            gridStore.reload();
        }else{
            Ext.MessageBox.error('Error', infoResponse.msg);
        }
    }
    
   

    var jsonStoreCatIF = new Ext.data.JsonStore({
        storeId: 'jsonStoreCatIF',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '15epoConsultaCartaAdhesionExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros',
                idProperty: 'clave'
            },
            extraParams: {
                informacion: 'catIF'
            }        
        },
        listeners: {
            exception: NE.util.mostrarProxyAjaxError
        },    
        fields: [
            { name: 'clave', type: 'string' },
            { name: 'descripcion', type: 'string' }				
        ]
    });
            
    var storeMoneda = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','Moneda Nacional'],
			['54','D�lar Americano ']
		 ]
	}) ;
        

    var storeCatEstatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['I','En Firma IF'],
			['E','En Firma EPO '],
                        ['F','Formalizado'],
                        ['A','Autorizado NAFIN']
		 ]
	}) ;
        
    
    var storeTipoFactoraje = new Ext.data.ArrayStore({
             fields: ['clave', 'descripcion'],
             data : [
                    ['N','Factoraje Normal'],
                    ['V','Factoraje Al Vencimiento']
             ]
	}) ;   
        
// ............... Definicion de Funciones ...........................................................     

    function generarPDF(record){
        Ext.Ajax.request({
            url: '15epoConsultaCartaAdhesionExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'GenerarArchivoPDF',
                ic_if: record.get("icIF"),
                ic_CartaAdhesion: record.get("icCartaAdhesion")
            }),
            callback: descargaArchivos
        });    
    }

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();		
            }else {
                NE.util.mostrarConnError(response,opts);
        }
    }    


// ............... Grid Principal ...........................................................
   var gridPanel = Ext.create('Ext.grid.Panel', {
        id: 'gridBO',
        store: gridStore,
        stripeRows: true,
        title: 'Cartas de Adhesi�n',
        width: 944,
        height: 400,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
            header: "Folio",
            dataIndex: 'folio',
            width: 90,
            sortable: true,
            hideable: true            
        }, {
            header: 'Intermediario Financiero',
            dataIndex: 'nombreIF',
            width: 250,
            sortable: true,
            hideable: true
        }, {
            header: "Moneda",
            dataIndex: 'nombreMoneda',
            width: 110,
            sortable: true,
            hideable: true
        }, {
            header: "Tipo de Fatoraje",
            dataIndex: 'tipoFactoraje',
            align: 'left',
            width: 100,
            sortable: true,
            hideable: true        
        }, {
            header: "Fecha de Firma",
            dataIndex: 'fechaFirma',
            align: 'center',
            width: 90,
            sortable: true,
            hideable: true   
        }, {
            header: "Sobretasa 1er plazo",
            align: 'right',
            dataIndex: 'sobretasa',
            hideable: true,
            sortable: true,
            width: 120,
            renderer: function (value, metaData, record) {
                return record.get('tipoFactoraje')==FACTORAJE_AL_VENCIMIENTO ? "N/A": value;
            }	            
        }, 
        {
            header: "Estatus",
            dataIndex: 'estatus',
            width: 90,
            align: 'center',
            sortable: true,
            hideable: true
        },
         {
            header: "Firmantes IF",
            align: 'center',
            dataIndex: 'numeroFirmantesIF',
            hideable: true,
            sortable: true,
            width: 100
        }, 
         {
            header: "Nombre Firmantes IF",
            align: 'left',
            dataIndex: 'nombreFirmantesIF',
            hideable: true,
            sortable: true,
            width: 200
        }, 
         {
            header: "Firmantes EPO",
            align: 'center',
            dataIndex: 'numeroFirmantesEPO',
            hideable: true,
            sortable: true,
            width: 100
        }, 
         {
            header: "Nombre Firmantes EPO",
            align: 'left',
            dataIndex: 'nombreFirmantesEPO',
            hideable: true,
            sortable: true,
            width: 200
        },        
        {
            xtype: 'actioncolumn',
            header: "Carta Adhesi�n",
            menuText: "Carta Adhesi�n",
            align: 'center',
            dataIndex: 'icCartaAdhesion',
            hideable: true,
            sortable: true,
            width: 90,
            items: [{
                iconCls: 'icoPdf',
                tooltip: 'Descargar Carta de Adhesi�n',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    generarPDF(grid.getStore().getAt(rowIndex));
                }
            }
            ]
        },        
        {
            xtype: 'actioncolumn',
            header: "Acciones",
            menuText: "Acciones",
            align: 'center',
            dataIndex: 'icCartaAdhesion',
            hideable: true,
            sortable: true,
            width: 90,
            items: [
            {
                iconCls: 'icoEliminar',
                tooltip: 'Eliminar Carta de Adhesi�n',
                handler: function (grid, rowIndex, colIndex) {
                    var rec = grid.getStore().getAt(rowIndex);
                    eliminarCarta(rec);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatus') != ESTATUS_EPO);
                }
            }
            ]
        }],
        bbar: ['->','-',
                {
                    xtype:'button',
                    text: 'Exportar a Excel',
                    id: 'btnXLS',
                    iconCls: 'icoBotonXLS',
                    handler: function(boton, evento) {
                        Ext.Ajax.request({
                            url: '15epoConsultaCartaAdhesionExt.data.jsp',
                            params: Ext.apply(primerPanel.getForm().getValues(),{
                                informacion:  'GenerarArchivoXSL'}),
                            callback: descargaArchivos
                        });
                    }   
                },'-'                        
            ]        
    });

// ............... Panel Mensaje ...........................................................
var panelMensaje = new Ext.form.FormPanel({
        name: 'panelMensaje',
        id: 'panelMensaje',
        width: 700,
        hidden: false,
        frame: true,
        height: 145,
        bodyStyle: 'padding: 12px',
        style: 'margin:0 auto;',
        items: [
            {
                xtype:'label',
                id: 'txtLeyenda',
                html: '<p style="text-align:justify;font-style: normal">Al transmitir este mensaje de datos, bajo nuestra responsabilidad aceptamos la suscripci�n del presente documento por medios electr�nicos para todos los efectos legales a que haya lugar. ' +
                'Las partes que suscriben el presente documento electr�nico son responsables y se obligan a responder frente a la otra respecto de la exactitud, vigencia y autenticidad de la informaci�n asentada en el mismo.'+
                'Nacional Financiera S.N.C., I.B.D., no se responsabilizar� de la certeza de los datos establecidos en el presente documento electr�nico ya que el mismo es requisitado y revisado por las partes contratantes.'+   
                'Asimismo, las personas que suscriben el presente documento mediante el uso de medios electr�nicos manifiestan que cuentan con las autorizaciones, facultades y/o permisos para realizar la celebraci�n del mismo, liberando a NAFIN de cualquier responsabilidad al respecto.<\/p>'
                
            }
        ]
    });

    

// ............... Panel Principal ...........................................................

var primerPanel = new Ext.form.FormPanel({
        name: 'formParams01',
        id: 'formParams01',
        width: 700,
        frame: true,
        height: 'auto',
        bodyStyle: 'padding: 6px',
        style: 'margin:0 auto;',
        title: 'Cartas de Adhesi�n',
        items: [
                {
                    xtype: 'combo',
                    id: 'cbIF',
                    name: 'cbIF',
                    fieldLabel: 'Cadena Productiva:',
                    padding: '5 5 5 20',
                    width: 600,
                    labelWidth: 150,
                    emptyText: 'Seleccionar una Cadena...',
                    store: jsonStoreCatIF,
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: false
                },
                {
                    xtype: 'combo',
                    id: 'cbMoneda',
                    name: 'cbMoneda',
                    fieldLabel: 'Moneda',
                    labelWidth: 150,
                    padding: '5 5 5 20',
                    width: 600,
                    emptyText: 'Seleccionar Moneda...',
                    store: storeMoneda, 
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: false                     
                },                
                {
                    xtype: 'combo',
                    id: 'cbTipoFactoraje',
                    name: 'cbTipoFactoraje',
                    fieldLabel: 'Tipo de Factoraje:',
                    padding: '5 5 5 20',
                    labelWidth: 150,
                    width: 600,
                    emptyText: 'Seleccionar...',
                    store: storeTipoFactoraje, 
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: false                  
                },
                {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 'fechaSolicitudUno',
                    name: 'fechaSolicitudUno',
                    fieldLabel: 'Fecha de Solicitud',
                    labelWidth: 150,
                    padding: '0 5 0 20',
                    width: 350,
                    emptyText: 'Desde',
                    displayField: 'descripcion'
                }, {
                    xtype: 'datefield',
                    id: 'fechaSolicitudDos',
                    name: 'fechaSolicitudDos',
                    fieldLabel: 'al',
                    labelWidth: 30,
                    padding: '0 5 0 25',
                    width: 220,
                    emptyText: 'Hasta'
                }]
            },                 
                {
                    xtype: 'combo',
                    id: 'cbEstatus',
                    name: 'cbEstatus',
                    fieldLabel: 'Estatus:',
                    store: storeCatEstatus, 
                    labelWidth: 150,
                    padding: '5 5 5 20',
                    width: 600,
                    emptyText: 'Seleccionar...',
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    editable: false                       
                },
            {
                xtype: 'textfield',
                id: 'txtFolio',
                name: 'txtFolio',                
                fieldLabel: 'No. Folio:',
                emptyText: 'aaaa/folio',
                labelWidth: 150,
                padding: '5 5 5 20',
                width: 600
            }
        ],
        buttons: [{
            text: 'Buscar',
            id: 'bobtnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                var resultado = validarCampos();
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else {
                    gridPanel.el.mask('Cargando...', 'x-mask-loading');
                    gridStore.load({
                        params: Ext.apply(primerPanel.getForm().getValues()),
                        callback: function(records, operation, success) {
                            var jsonResonse = Ext.JSON.decode(operation.response.responseText);
                            if(jsonResonse.total == 0){
                                Ext.MessageBox.alert('Aviso', "No se encontraron registros.");
                            }
                            gridPanel.el.unmask();    
                            
                        }                        
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                primerPanel.getForm().reset();
            }
        }]       
    });


// ............... Contenedor Principal ...........................................................

	var contenedor = new Ext.Container({
            id: 'contenedorPrincipal',
            renderTo: 'areaContenido',
            width: 950,
            style: 'margin:0 auto;',
            items: [
                NE.util.getEspaciador(10),
                primerPanel,
                NE.util.getEspaciador(5),
                panelMensaje,
                NE.util.getEspaciador(5),
                gridPanel
                ]
	});

});