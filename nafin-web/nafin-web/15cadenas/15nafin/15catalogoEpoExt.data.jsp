<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		javax.naming.*,	
		java.util.Date.*,
		netropology.utilerias.*,
		net.sf.json.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
	<%@ include file="/appComun.jspf"%>
	<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
	
	<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String cboEpo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	String infoRegresar="";
	
	if(informacion.equals("CatalogoEPO"))
	{
		
    CatalogoEPO cat = new CatalogoEPO();
    cat.setCampoClave("ic_epo");
    cat.setCampoDescripcion("cg_razon_social"); 
    //cat.setClaveIf(iNoCliente);	//iNoCliente viene de sesion. (ic_if)
    cat.setOrden("cg_razon_social");
    infoRegresar = cat.getJSONElementos();
	
	}	
	%>
	<%=infoRegresar%>