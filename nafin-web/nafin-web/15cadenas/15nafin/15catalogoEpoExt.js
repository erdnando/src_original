
Ext.onReady(function(){
	
  var pagina = Ext.getDom('pagina').value;
  var reporte = Ext.getDom('reporte').value;
  
Ext.define('Catalogo', {
    extend: 'Ext.data.Model',
    fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }
    ]
});
  
var catalogoEPOData = Ext.create('Ext.data.Store', {
	model: 'Catalogo',
	proxy: {
		type: 'ajax',
		url: '15catalogoEpoExt.data.jsp',
		reader: {
			type: 'json',
			root: 'registros'
		},
		extraParams: {
			informacion: 'CatalogoEPO'
		},
		listeners: {
			exception: NE.util.mostrarProxyAjaxError
		}
	}
});
  
//------------------------------------------------------------------------------

  var elementosForma = [
    {
		xtype: 'combobox',
		id: 'EPO1',
		forceSelection: true,
		fieldLabel: 'Tipo de Afiliado',
		name: 'EPO',
		allowBlank: false,
		store: catalogoEPOData,
		valueField: 'clave',
		displayField: 'descripcion',
		typeAhead: true,
		queryMode: 'local',
		listeners: {
			boxready: function(combo, width, height, eOpts) {
				//Workaround extjs 4.2.0 debido a que la mascara de carga la pone fuera del componente
				//al parecer porque aun no existe el "picker" que es la caja donde salen
				//las opciones del combo.
				combo.expand();
				combo.collapse();
			},
      select: {
					fn: function(combo) {
              
              var WORP_USER  ='NAE';
              var url = "/nafin/infoGralEpoGraficas?pagina="+pagina+"+&WORP_USER="+WORP_USER+"&reporte="+reporte+"&EPO="+combo.getValue();
              //var url = "http://"+pagina+"?WORP_USER="+WORP_USER+"&IBIF_ex="+reporte+"&EPO="+combo.getValue();
              iframeExt.load(url);
					}
				}
		}
	}
	];
  
	
  
  var iframeExt = Ext.create('Ext.ux.IFrame',{
    id:'frameUx',
    style: 'margin:0 auto;',
    height: 1000
    
  });
  
  
  
  var fp = Ext.widget({
	xtype: 'form',
	frame: true,
	title: '',
	bodyPadding: '10 10 10',
	width: 500,
	fieldDefaults: {
		msgTarget: 'side',
		anchor: '100%',
		labelWidth: 100
	},
	items: elementosForma
});

  
  var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(20),
      fp,
      NE.util.getEspaciador(20),
      iframeExt
		]
	});
	
	catalogoEPOData.load();
//--------------------------------------------------------------------------
});