<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252"	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="15forma02ext.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%
if (strTipoUsuario.equals("PYME")) {
%>
<%@ include file="/01principal/01pyme/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01pyme/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01pyme/pie.jspf"%>
<%
} else if (strTipoUsuario.equals("EPO")) {
%>
<%@ include file="/01principal/01epo/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01epo/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01epo/pie.jspf"%>
<%
} else if (strTipoUsuario.equals("IF")) {
%>
<%@ include file="/01principal/01if/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01if/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01if/pie.jspf"%>
<%
} else if (strTipoUsuario.equals("UNIV")) {
%>
	<%@ include file="/01principal/01uni/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01uni/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01uni/pie.jspf"%>
<%
} else if (strTipoUsuario.equals("NAFIN")) {
%>
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01nafin/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01nafin/pie.jspf"%>
<%
} else if (strTipoUsuario.equals("IFNB CEDI")) {
%>
<%@ include file="/01principal/01cedi/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01cedi/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01cedi/pie.jspf"%>
<%
} else {

	throw new AppException("Tipo de usuario: " + strTipoUsuario + "...No implementado aun");
}
%>

<form id='formAux' name="formAux" target='_new'></form>
<div id="politicas" style="display: none">
	<table width="500" cellpadding="3" cellspacing="0" border="0" align="center">
	<tr> 
		<td valign="middle" class="menu" colspan="2">Pol�ticas de Contrase�a / Password Policies</td>
	</tr>
	<tr> 
		<td valign="left" class="formas"><li>Debe Contener tanto n�meros como letras, y ser mayor a 6 posiciones.</li></td>
		<td valign="left" class="formas"><li>It must contain at least a 6-digit combination of letters and numbers.</li></td>
	</tr>
	<tr> 
		<td valign="left" class="formas"><li>Se permite el uso de letras may�sculas y min�sculas.</li></td>
		<td valign="left" class="formas"><li>Upper and lower case letters are allowed.</li></td>
	</tr>
	<tr> 
		<td valign="left" class="formas"><li>No se permite el uso de letras acentuadas, ni caracteres especiales como '�', '$', etc.</li></td>
		<td valign="left" class="formas"><li>No accents nor special characters allowed, such as "�, "$", etc.</li></td>
	</tr>
	<tr> 
		<td valign="left" class="formas"><li>El Usuario o parte del Usuario no debe estar contenido en el Password.</li></td>
		<td valign="left" class="formas"><li>The Username, or a part of it, must not be included in the Password.</li></td>
	</tr>
	<tr> 
		<td valign="left" class="formas"><li>No debe contener m�s de 2 n�meros iguales consecutivos. (ej. 12223445)</li></td>
		<td valign="left" class="formas"><li>It must not have more than 2 consecutive numbers repeated (e.g. 12223445)</li></td>
	</tr>
	<tr> 
		<td valign="left" class="formas"><li>No debe contener m�s de 2 letras iguales en forma consecutiva. (ej. bbbccd)</li></td>
		<td valign="left" class="formas"><li>It must not have more than 2 consecutive letters repeated (e.g. bbbccd)<br>Good passwords:
	Pireman28a 
	Soc5erfan1963 </li></td>
	</tr>
	</table>
</div>
</body>
</html>

