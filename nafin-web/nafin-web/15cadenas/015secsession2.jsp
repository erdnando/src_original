<%@ page import="java.sql.*,java.io.*,netropology.utilerias.*"%>

<%
//El siguiente c�digo debe ser lo primero en ejecutarse, el cual determina si la sesi�n est� activa. 
//De lo contrario en lugar de mostrar "Su sesi�n ha expirado..." lanzar� un NullPointer Exception
String gsCveUsuario = (String) session.getAttribute("sesCveUsuario");
if(gsCveUsuario == null){
%>
		<script language="JavaScript">
			alert("Su sesi�n ha expirado, por favor vuelva a entrar.");
			top.location="/nafin/15cadenas/15salir.jsp"
		</script>
<%
	return;
}

	String strTipoUsuario = "",iNoUsuario = "",iNoCliente = "",strNombre = "",strLogin = "",strLogo = "",strClase = "",iNoEPO = "",strTipoAfiliacion = "";
	String bIfRelacionada = "",strSerial = "",strDirTrabajo = "", strDirectorioTemp = "",strDirectorioPublicacion = "",strDirecVirtualTemp = "",strDirecVirtualTrabajo = "",strNombreUsuario = "";
	String strDirecVirtualBanner = "",strDirecVirtualCSS = "",strFacultad = null,strConexion = "",strUser = "",strPassword = "",strPerfil = "";
	String strUsuarioArch = "", sesExterno = "";
	Long iNoNafinElectronico = null;
	String sesIdiomaUsuario = "ES";
	String sesEpoInternacional = "N";
	
	try{
		strTipoUsuario = (session.getAttribute("strTipoUsuario")==null)?"":(String)session.getAttribute("strTipoUsuario");   // POSIBLES VALORES NAFIN-EPO-PYME-IF
		iNoUsuario = (session.getAttribute("iNoUsuario")==null)?"":(String)session.getAttribute("iNoUsuario");               // 
		iNoCliente = (session.getAttribute("iNoCliente")==null)?"":(String)session.getAttribute("iNoCliente");               // NO DE CLIENTE DE LAS TABLAS COMCAT_PYME, COMCAT_EPO, COMCAT_IF
		strNombre = (session.getAttribute("strNombre")==null)?"":(String)session.getAttribute("strNombre");                  // NOMBRE COMERCIAL O DE LA PERSONA DE LAS TABLAS COMCAT_PYME, COMCAT_EPO, COMCAT_IF
		strNombreUsuario = (session.getAttribute("strNombreUsuario")==null)?"":(String)session.getAttribute("strNombreUsuario"); //
		iNoEPO = (session.getAttribute("iNoEPO")==null)?"":(String)session.getAttribute("iNoEPO");                           // NUMERO DE LA EPO A LA CUAL PERTENECE LA PYME
		iNoNafinElectronico = (session.getAttribute("iNoNafinElectronico")==null)?null:(Long)session.getAttribute("iNoNafinElectronico");// NUMERO DE LA EPO A LA CUAL PERTENECE LA PYME
		strLogin = (session.getAttribute("Clave_usuario")==null)?"":(String)session.getAttribute("Clave_usuario");           // LOGIN DEL USUARIO QUE SE LOGUEA
		strLogo = (session.getAttribute("strLogo")==null)?"":(String)session.getAttribute("strLogo");
		strClase = (session.getAttribute("strClase")==null)?"":(String)session.getAttribute("strClase");
		bIfRelacionada = (session.getAttribute("bIfRelacionada")==null)?"":(String)session.getAttribute("bIfRelacionada");
		strTipoAfiliacion = (session.getAttribute("strTipoAfiliacion")==null)?"":(String)session.getAttribute("strTipoAfiliacion");
		strSerial = (session.getAttribute("strSerial")==null)?"":(String)session.getAttribute("strSerial");
		sesExterno = (session.getAttribute("sesExterno")==null)?"":(String)session.getAttribute("sesExterno");
		strDirectorioPublicacion = (String)application.getAttribute("strDirectorioPublicacion");
		strDirectorioTemp = strDirectorioPublicacion+"00tmp/15cadenas/";
		strDirecVirtualTemp = "/nafin/00tmp/15cadenas/";
		strDirTrabajo = strDirectorioPublicacion+"00archivos/15cadenas/";
		strDirecVirtualTrabajo = "/nafin/00archivos/15cadenas/";

		strDirecVirtualBanner = "/nafin/00archivos/15cadenas/";
		strDirecVirtualCSS = "/nafin/14seguridad/Seguridad/"; //VARIABLE  PARA CSS

		sesIdiomaUsuario = (session.getAttribute("sesIdiomaUsuario") == null)?"ES":(String)session.getAttribute("sesIdiomaUsuario");
		sesEpoInternacional = (session.getAttribute("sesEpoInternacional") == null)?"N":(String)session.getAttribute("sesEpoInternacional");
		//String strDirecVirtualTemp = "/nafin/00archivos/15cadenas/";
		strFacultad = null; // Variable que almacena la facultad
		
		strPerfil = (session.getAttribute("sesPerfil")==null)?"":(String)session.getAttribute("sesPerfil"); // Variable que contiene el perfil general del usuario
	}
	catch(Exception e){
		String strErr = e.toString();
%>
		<html><script language="JavaScript">alert("Su sesión ha expirado, por favor vuelva a entrar.");top.location="/nafin/20secure/ne.jsp"</script></html>
<%
	}

/*String strTipoAfiliacion = (String)session.getAttribute("strTipoAfiliacion");//TIPO DE AFILIACION DEL USUARIO B(BAJO),A(ALTO)
*/
//String strTipoUsuario = ""; // POSIBLES VALORES NAFIN-EPO-PYME-IF
//String iNoUsuario = ""; // 
//String iNoCliente = "";//NO DE CLIENTE DE LAS TABLAS COMCAT_PYME, COMCAT_EPO, COMCAT_IF
//String strNombre = ""; // NOMBRE COMERCIAL O DE LA PERSONA DE LAS TABLAS COMCAT_PYME, COMCAT_EPO, COMCAT_IF
//String strNombreUsuario = ""; //
//String iNoEPO = "";//NUMERO DE LA EPO A LA CUAL PERTENECE LA PYME
//String iNoNafinElectronico = "";//NUMERO DE LA EPO A LA CUAL PERTENECE LA PYME
//String strLogin = "";//LOGIN DEL USUARIO QUE SE LOGUEA
//String strTipoAfiliacion = "";//TIPO DE AFILIACION DEL USUARIO B(BAJO),A(ALTO)

//out.print(strTipoUsuario + "-" + iNoUsuario + "-" + iNoCliente + "-" + strNombre + "-" + strNombreUsuario + "-" + iNoEPO + "-" + iNoNafinElectronico + "-" + strLogin + "-" + strTipoAfiliacion + "-");

if(iNoUsuario == null || iNoUsuario.equals("") || strTipoUsuario == null || strTipoUsuario.equals("")){
%>
	<html><script language="JavaScript">alert("Su sesión ha expirado, por favor vuelva a entrar.");top.location="/nafin/20secure/ne.jsp"</script></html>
<%}%>
