Ext.onReady(function() {

	var formDato = {
		numProv : null,
		nomProv: null,
		tipoClas:null
	}

	function procesaValoresIniciales(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales.strTipoUsuario != undefined && jsonValoresIniciales.strTipoUsuario != 'EPO'){
				fp.hide();
				Ext.Msg.alert('','Este men� est� disponible s�lo para p�rfil EPO');
			}else{
				catalogoTipoClasificaData.load();
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			Ext.getCmp('btnBajarArchivo').hide();
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarArchivo').enable();
				el.unmask();
			}else {
				Ext.getCmp('btnGenerarArchivo').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15clasifica01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CG_PYME_EPO_INTERNO'},
			{name: 'RAZON_SOCIAL'},
			{name: 'IC_CALIFICACION'},
			{name: 'CD_DESCRIPCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){Ext.apply(options.params, {numProv:formDato.numProv, nomProv:formDato.nomProv, tipoClas:formDato.tipoClas});}	},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var catalogoTipoClasificaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15clasifica01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoClasificacion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header : 'N�mero proveedor', tooltip: 'N�mero proveedor', dataIndex : 'CG_PYME_EPO_INTERNO',
				sortable: true,	align: 'center',	width: 200,	resizable: true, hidden: false, hideable:false
			},{
				header : 'Nombre proveedor', tooltip: 'Nombre proveedor', dataIndex : 'RAZON_SOCIAL',
				sortable: true,	align: 'center',	width: 200,	resizable: true, hidden: false, hideable:false
			},{
				header : 'Tipo', tooltip: 'Tipo', dataIndex : 'IC_CALIFICACION',
				sortable: true,	align: 'center',	width: 150,	resizable: true, hidden: false, hideable:false
			},{
				header : ' Descripci�n ', tooltip: ' Descripci�n ', dataIndex : 'CD_DESCRIPCION',
				sortable: true,	width: 350,	resizable: true, hidden: false, hideable:false
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		colunmWidth: true,
		frame: true,
		bbar: {
			items: [
				'->','-',{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15clasifica01ext.data.jsp',
							params: {informacion: 'ArchivoCSV',
										numProv:formDato.numProv, 
										nomProv:formDato.nomProv, 
										tipoClas:formDato.tipoClas},
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar Archivo',
					//iconCls: 'icoXls',
					id: 'btnBajarArchivo',
					hidden: true
				}
			]
		}
	});
	var elementosForma = [
		{
			xtype:		'textfield',
			id:			'numProv',
			name:			'numProv',
			maxLength:	30,
			anchor:		'70%',
			vtype: 		'alphanum',
			fieldLabel:	'N�mero de Proveedor'
		},{
			xtype:		'textfield',
			id:			'nomProv',
			name:			'nomProv',
			maxLength:	30,
			vtype: 		'alphanum',
			fieldLabel:	'Nombre Proveedor'
		},{
			xtype:		'combo',
			name:			'tipoClas',
			id:			'cboTipoClasifica',
			hiddenName:	'tipoClas',
			fieldLabel:	'Tipo de Clasificaci�n',
			emptyText: 'Seleccionar Clasificaci�n',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			editable: false,
			minChars : 1,
			store: catalogoTipoClasificaData,
			tpl : NE.util.templateMensajeCargaCombo
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		//title:	'',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		labelWidth: 160,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					if (grid.isVisible()) {
						grid.hide();
					}
					formDato.numProv	=	Ext.getCmp('numProv').getValue();
					formDato.nomProv	=	Ext.getCmp('nomProv').getValue();
					formDato.tipoClas	=	Ext.getCmp('cboTipoClasifica').getValue();
					if (Ext.isEmpty(formDato.numProv) && Ext.isEmpty(formDato.nomProv) && Ext.isEmpty(formDato.tipoClas) ) {
						Ext.Msg.alert(boton.text, 'Debe capturar alg�n criterio de b�squeda');
						return;
					}
					fp.el.mask('Enviando...','x-mask-loading');
					consultaData.load();
				} //fin-handler
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					fp.getForm().reset();
					if (grid.isVisible()) {
						grid.hide();
					}
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});

	fp.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '15clasifica01ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});
	/*//consultaData.load({params: Ext.apply(fp.getForm().getValues())});*/

});