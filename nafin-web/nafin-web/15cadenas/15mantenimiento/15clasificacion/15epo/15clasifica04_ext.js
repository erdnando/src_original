Ext.onReady(function() {

	function procesaValoresIniciales(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales.strTipoUsuario != undefined && jsonValoresIniciales.strTipoUsuario != 'EPO'){
				fp.hide();
				Ext.Msg.alert('','Este men� est� disponible s�lo para p�rfil EPO');
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	function mostrarArchivoTXT(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesaCargaMasiva = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaMasiva(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaMasiva(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	var storeResumenStore = new Ext.data.JsonStore({
	root : 'registrosResumen',
		fields: [
			{name: 'REGISTROS'  	},
			{name: 'TOTAL' }
		],
		listeners: {exception: NE.util.mostrarDataProxyError}	
		
	});
	var cargaMasiva = function(estado, respuesta ){
	if(			estado == "SUBIR_ARCHIVO"					){
			
			fp.getForm().submit({
									url: '15clasifica04ext.data.jsp',
									waitMsg: 'Enviando datos...',
									success: function(form, action) {
										if (action.result.flag){
											 procesaCargaMasiva(null,  true,  action.response );
										
										}else{
											fp.el.unmask();
											Ext.getCmp('form_file').markInvalid('Excede el tama�o m�ximo del archivo');
											/*
											* Este error no esta contemplado, pero en el jsp original el tama�o maximo del archivo es de 100000. Aprox 97KB.
											*Ext.getCmp('form_file').markInvalid('El tama�o m�ximo del archivo debe ser menor a 98 KB');
											*/
											return;
										}
									},
									failure: NE.util.mostrarSubmitError
								})
			
	} else if(	estado == "VALIDA_CARACTERES_CONTROL"				){
			var nombreArchivo	= respuesta.nombreArchivo;
			Ext.getCmp('guardaNomArchivo').setValue(nombreArchivo);
			Ext.Ajax.request({
				url: 		'15clasifica04extControl.data.jsp',
				params: 	{
					informacion:				'validaCaracteresControl',
					nombreArchivo: 	nombreArchivo
				},
				callback: 				procesaCargaMasiva
			});
		
		} else if(	estado == "MOSTRAR_AVANCE_THREAD"				){
			Ext.Ajax.request({
				url: 		'15clasifica04extControl.data.jsp',
				params: 	{
					informacion:				'avanceThreadCaracteres'
				},
				callback: 				procesaCargaMasiva
			});
		
		}else if(	estado == "HAY_CARACTERES_CONTROL"				){
				// fp.hide();
				var mensaje	= respuesta.mns;
				var nombreArchivoCaractEsp	= respuesta.nombreArchivoCaractEsp;
				Ext.getCmp('nombArchivoDetalle').setValue(nombreArchivoCaractEsp);
				fp.el.unmask();
				fp.hide();
				caracteresEspeciales.show();
				caracteresEspeciales.setMensaje(mensaje);
				
			
		}else if(	estado == "ERROR_THREAD_CARACTERES"				){
				var mensaje	= respuesta.mns;
				 Ext.getCmp("forma").hide();
				Ext.Msg.alert("Error", mensaje );
				return;
		}else if(	estado == "DESCARGAR_ARCHIVO"				){
		
			var archivo = Ext.getCmp('nombArchivoDetalle').getValue();
			Ext.Ajax.request({
				url: 		'15clasifica04extControl.data.jsp',
				params: 	{
					informacion:				'descargaArchivoDetalle',
					archivo : archivo
				},
				callback: 				mostrarArchivoTXT
			});
		}else if(	estado == "REVISAR_ERRORES"				){
			/*Ext.getCmp('disLeidos').setValue('');
			Ext.getCmp('disCarga').setValue('');
			Ext.getCmp('disRechaza').setValue('');
			Ext.getCmp('txtError').setValue('');*/
			Ext.Ajax.request({
				url: 		'15clasifica04extControl.data.jsp',
				params: 	{
					informacion:				'validaArchivoErrores',
					nombreArchivo : Ext.getCmp('guardaNomArchivo').getValue(nombreArchivo)
				},
				callback: 				procesaCargaMasiva
			});
		   
		
		}else if(	estado == "MUESTRA_RESULTADO"				){
			var errores = respuesta.errores;
			fp.hide();
			fpResultado.show(); 
			storeResumenStore.loadData(respuesta);
			if (!respuesta.ok){
				Ext.getCmp('btnResumen').setText('Volver a cargar');
				Ext.getCmp('txtError').setValue(errores);
			}else{
				Ext.getCmp('btnResumen').setText('Aceptar');
				Ext.getCmp('formErrores').hide();
			}
			
		
		   
		
		}else if(	estado == "MENSAJE_CODIFICACION"								){
			new NE.cespcial.AvisoCodificacionArchivo({codificacion:respuesta.codificacionArchivo}).show();
 
		}
		
		return;
		
	}
	var gridResumen = new Ext.grid.GridPanel({
		id: 'gridResumen',
		title:'<center>Resultado</center>',
		store: storeResumenStore,
		columns:[			
			{
				dataIndex: 'REGISTROS',
				width: 300,
				align: 'left'
			},
			{
				dataIndex: 'TOTAL',
				width: 230,
				align: 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 100,
		width: 550,
		style: 'margin:0 auto;',
		frame: true
	});
	
	var elementosResultado = [
		{
			xtype:'panel',
			id:	'formResumen',
			width: 580,
			labelWidth: 200,
			frame: true,   
			border: true,
			style: 'margin:0 auto;',
			bodyStyle: 'padding: 10px',
			items:[
				gridResumen
			],
			buttons: [
				{
					text: '',
					id: 'btnResumen',
					iconCls: 'aceptar',
					formBind: true,
					handler: function(boton, evento) {
									window.location = '15clasifica04_ext.jsp';
								}
				}
			]
		},{
			xtype:'panel',
			id:	'formErrores',
			title:'<div align="center">Errores Generados</div>',
			frame: true,   
			border: true,
			style: 'margin:0 auto;',
			width: 580,
			labelWidth: 350,
			defaults:{ 
							layout:		'fit',
							border:		false,
							autoScroll: true
			},
			items:[
				{
					xtype:	'textarea',
					id:		'txtError',
					width:	550,
					height:	300,
					value:''
				}
			]
		}
	];

	var fpResultado = new Ext.form.FormPanel({
		id: 'formaResultado',
		width: 600,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		hidden: true,
		frame: true,
		labelWidth: 100,
		defaultType: 'textfield',
		items: elementosResultado,
		monitorValid: true
	});

	var elementosForma = [
		{
			xtype: 'fileuploadfield',
			id: 'form_file',
			name: 'txtArchivo',
			anchor: '95%',
			allowBlank: false,
			blankText:	'Debe seleccionar una ruta de archivo.',
			emptyText: 'Seleccione un archivo',
			fieldLabel: 'Ruta del Archivo',
			regex: /^.*\.(txt|TXT)$/,
			regexText:'El archivo debe tener extensi�n .txt',
			buttonText: 'Examinar...'
		},
		{ 	
			xtype:	'hidden',
			name:	'guardaNomArchivo',
			id: 'guardaNomArchivo'
		},
		{  
		  xtype:	'hidden',  
		  name:	'nombArchivoDetalle',
		  id: 	'nombArchivoDetalle'
		}	
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		fileUpload: true,
		hidden: false,
		frame: true,
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Iniciar',
				id: 'btnIniciar',
				iconCls: 'icoContinuar',
				formBind: true,
				handler: function(boton, evento) {
							/*	Ext.getCmp('disLeidos').setValue('');
								Ext.getCmp('disCarga').setValue('');
								Ext.getCmp('disRechaza').setValue('');
								Ext.getCmp('txtError').setValue('');*/
								//fp.el.mask('Procesando...', 'x-mask-loading');
								cargaMasiva("SUBIR_ARCHIVO", null);
			}
			},{
				text: 'Salir',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15clasifica01_ext.jsp';
				}
			}
		]
	});
	var caracteresEspeciales = new NE.cespcial.CaracteresEspeciales(
		{
		hidden: true
		}
	);
	
	caracteresEspeciales.setHandlerAceptar(function(){
		caracteresEspeciales.hide(),
		fp.show();
	});
	
	caracteresEspeciales.setHandlerDescargarArchivo(function(){
		 cargaMasiva("DESCARGAR_ARCHIVO", null);
	});
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,fpResultado,
			caracteresEspeciales
		]
	});

	fp.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '15clasifica01ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});

});