Ext.onReady(function() {

	var formDato = {
		numProv : null,
		nomProv: null,
		tipoClas:null
	}

	function procesaValoresIniciales(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales.strTipoUsuario != undefined && jsonValoresIniciales.strTipoUsuario != 'EPO'){
				fp.hide();
				Ext.Msg.alert('','Este men� est� disponible s�lo para p�rfil EPO');
			}else{
				catalogoTipoClasificaData.load();
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarGuardar(opts, success, response) {
		Ext.getCmp('btnGuardar').setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			catalogoTipoClasificaData.load();
			fpCombo.getForm().reset();
			fp.getForm().reset();
			fpCombo.hide();
			grid.hide();
			Ext.getCmp('btnGuardar').setIconClass('');
			Ext.Msg.alert('Guardar','La(s) calificaciones han sido asignadas');
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		if (arrRegistros != null) {
			fpCombo.show();
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGuardar').enable();
				el.unmask();
			}else {
				Ext.getCmp('btnGuardar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15clasifica01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CG_PYME_EPO_INTERNO'},
			{name: 'RAZON_SOCIAL'},
			{name: 'IC_CALIFICACION'},
			{name: 'CD_DESCRIPCION'},
			{name: 'IC_PYME'},
			{name: 'CS_HABILITADO'},
			{name: 'FLAG_CHECK'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){Ext.apply(options.params, {numProv:formDato.numProv, nomProv:formDato.nomProv, tipoClas:formDato.tipoClas});}	},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var catalogoTipoClasificaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15clasifica01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoClasificacion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header : 'N�mero proveedor', tooltip: 'N�mero proveedor', dataIndex : 'CG_PYME_EPO_INTERNO',
				sortable: true,	align: 'center',	width: 200,	resizable: true, hidden: false, hideable:false
			},{
				header : 'Nombre proveedor', tooltip: 'Nombre proveedor', dataIndex : 'RAZON_SOCIAL',
				sortable: true,	align: 'center',	width: 200,	resizable: true, hidden: false, hideable:false
			},{
				header : 'Tipo', tooltip: 'Tipo', dataIndex : 'IC_CALIFICACION',
				sortable: true,	align: 'center',	width: 130,	resizable: true, hidden: false, hideable:false
			},{
				header : ' Descripci�n ', tooltip: ' Descripci�n ', dataIndex : 'CD_DESCRIPCION',
				sortable: true,	width: 300,	resizable: true, hidden: false, hideable:false
			},{
				//xtype: 'checkcolumn',
				header: 'Selecci�n', tooltip: 'Selecci�n', dataIndex: 'FLAG_CHECK',
				sortable : false,	width : 80,	align: 'center', hideable: false,
				renderer:function(value, metadata, record, rowIndex, colIndex, store){
								if ( record.get('CS_HABILITADO')=='S' ){
									return '<input type="checkbox" id="'+ rowIndex +'" name="seleccionado" value="' + value + '"></input>';
								}else{
									return '';
								}
							}
			}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		colunmWidth: true,
		frame: true,
		listeners: {
			beforeedit : function(e){
				var inx = e.row;
				var check = Ext.getCmp('chk'+inx.toString());
				check.setDisabled(true);
				//var record = e.record;
				//record.commit();		
			}
		},
		bbar: {
			items: [
				'->','-',{
					xtype: 'button',
					text: 'Guardar',
					id: 'btnGuardar',
					handler: function(boton, evento) {
						if (Ext.isEmpty(Ext.getCmp('cboAsignaClasifica').getValue())) {
							Ext.getCmp('cboAsignaClasifica').markInvalid('Debe asignar una clasificaci�n');
							return;
						}

						var flag = false;
						var jsonData = consultaData.data.items;
						var lista = [];
						Ext.each(jsonData, function(item,index,arrItem){
							var check = Ext.getDom(index.toString());
							if (check){
								if (check.checked){
									flag = true;
									lista.push(item.data.IC_PYME );
								}
							}
						});

						if (flag){
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url: '15clasifica01ext.data.jsp',
								params: Ext.apply({
									informacion: 'Guarda',
									sel_asignar: Ext.getCmp('cboAsignaClasifica').getValue(),
									lista:	Ext.encode(lista)
								}),
								callback: procesarGuardar
							});
						}else{
							Ext.getCmp('btnGuardar').setIconClass('');
							Ext.Msg.alert(boton.text,'Debe seleccionar al menos un Proveedor');
							return;
						}
					}
				}
			]
		}
	});
	var elementosForma = [
		{
			xtype:		'textfield',
			id:			'numProv',
			name:			'numProv',
			maxLength:	30,
			anchor:		'70%',
			vtype: 		'alphanum',
			fieldLabel:	'N�mero de Proveedor'
		},{
			xtype:		'textfield',
			id:			'nomProv',
			name:			'nomProv',
			maxLength:	30,
			vtype: 		'alphanum',
			fieldLabel:	'Nombre Proveedor'
		},{
			xtype:		'combo',
			name:			'tipoClas',
			id:			'cboTipoClasifica',
			hiddenName:	'tipoClas',
			fieldLabel:	'Tipo de Clasificaci�n',
			emptyText: 'Seleccionar Clasificaci�n',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			editable: false,
			minChars : 1,
			store: catalogoTipoClasificaData,
			tpl : NE.util.templateMensajeCargaCombo
		}
	];

	var fpCombo = new Ext.form.FormPanel({
		id: 'formaCombo',
		width: 500,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		hidden: true,
		frame: true,
		labelWidth: 160,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype:		'combo',
				name:			'sel_asignar',
				id:			'cboAsignaClasifica',
				hiddenName:	'sel_asignar',
				fieldLabel:	'Clasificaci�n  a Asignar',
				emptyText: 'Seleccionar Clasificaci�n',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction : 'all',
				typeAhead: true,
				editable: false,
				minChars : 1,
				store: catalogoTipoClasificaData,
				tpl : NE.util.templateMensajeCargaCombo
			}
		]
	});

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		//title:	'',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		labelWidth: 160,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					fpCombo.hide();
					if (grid.isVisible()) {
						grid.hide();
					}
					formDato.numProv	=	Ext.getCmp('numProv').getValue();
					formDato.nomProv	=	Ext.getCmp('nomProv').getValue();
					formDato.tipoClas	=	Ext.getCmp('cboTipoClasifica').getValue();
					if (Ext.isEmpty(formDato.numProv) && Ext.isEmpty(formDato.nomProv) && Ext.isEmpty(formDato.tipoClas) ) {
						Ext.Msg.alert(boton.text, 'Debe capturar alg�n criterio de b�squeda');
						return;
					}
					fp.el.mask('Enviando...','x-mask-loading');
					consultaData.load();
				} //fin-handler
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					fp.getForm().reset();
					fpCombo.getForm().reset();
					fpCombo.hide();
					if (grid.isVisible()) {
						grid.hide();
					}
				}
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,		NE.util.getEspaciador(10),
			fpCombo,	NE.util.getEspaciador(10),
			grid
		]
	});

	fp.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '15clasifica01ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});

	/*//consultaData.load({params: Ext.apply(fp.getForm().getValues())});*/

});