<%@ page
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,com.netro.parametrosgrales.*,
		netropology.utilerias.caracterescontrol.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	=	"";
	if (informacion.equals("validaCaracteresControl")) {

		JSONObject	resultado	= new JSONObject();
		String nombreArchivo 	= (request.getParameter("nombreArchivo")	== null)?"":request.getParameter("nombreArchivo");
		System.out.println("ANTES DEL HILO");
		session.removeAttribute("ValidaCaracteresControl");
		BuscaCaracteresControlThread ValidaCaractCon= new BuscaCaracteresControlThread();
		
		ResumenBusqueda resCaractCon = new ResumenBusqueda();
		
		ValidaCaractCon.setRutaArchivo(strDirectorioTemp+nombreArchivo);
		ValidaCaractCon.setRegistrador(resCaractCon);
		ValidaCaractCon.setCreaArchivoDetalle(true);
		ValidaCaractCon.setDirectorioTemporal(strDirectorioTemp);
		ValidaCaractCon.setSeparadorCampo("|");
		new Thread(ValidaCaractCon).start();
		session.setAttribute("ValidaCaracteresControl", ValidaCaractCon);
	//ValidaCaractCon.getMensaje();
		resultado.put("success",new Boolean(true));
		resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_THREAD"		);
		infoRegresar = resultado.toString();
		System.out.println("TERMINO Y EMPEZO EL THREAD ");
	
	}else if (    informacion.equals("avanceThreadCaracteres") 				)	{
		JSONObject	resultado	= new JSONObject();
		BuscaCaracteresControlThread ValidaCaractCon = (BuscaCaracteresControlThread)session.getAttribute("ValidaCaracteresControl");
		String[] mensa = {""};
		ResumenBusqueda resCaractCon = new ResumenBusqueda(); 
		int estatusThread = ValidaCaractCon.getStatus(mensa);
		System.out.println(" estatusThread "+estatusThread);
		if(estatusThread==200||estatusThread==100){
			resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_THREAD");	
		}else if(estatusThread==300 ){
			resCaractCon = (ResumenBusqueda)ValidaCaractCon.getRegistrador();
			boolean hayCaracteres = resCaractCon.hayCaracteresControl();
			if(hayCaracteres==true){
				resultado.put("estadoSiguiente", "HAY_CARACTERES_CONTROL");
				resultado.put("mns",resCaractCon.getMensaje());
				resultado.put("nombreArchivoCaractEsp",resCaractCon.getNombreArchivoDetalle());
				
			}else{
				resultado.put("estadoSiguiente", "REVISAR_ERRORES");
			}
		}else if(estatusThread==400 ){
			resultado.put("estadoSiguiente", "ERROR_THREAD_CARACTERES");	
			resultado.put("mns", "Error en el archivo, favor de verificarlo");	
		}
		resultado.put("success",new Boolean(true));
		infoRegresar = resultado.toString();
		System.out.println("TERMINO Y EMPEZO EL THREAD "+infoRegresar);
	
	
}else if (informacion.equals("descargaArchivoDetalle")	){
		String nombreArchivo 	= (request.getParameter("archivo")	== null)?"":request.getParameter("archivo");
		JSONObject jsonObj3 = new JSONObject();
		jsonObj3.put("success", new Boolean(true));
		jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj3.toString();
}else if (    informacion.equals("validaArchivoErrores") 				)	{
		String nombreArchivo 	= (request.getParameter("nombreArchivo")	== null)?"":request.getParameter("nombreArchivo");
		boolean flag = true;
		boolean ok = true;
		ParametrosRequest req = null;
		String itemArchivo = "";
		String linea = "";
		String campo = "";
		String campo_epopyme = "";
		String campo_calif = "";
		int tamanio = 0;
		int lineadocs = 0;
		int iTotalEpos = 0;
		int iTotalReg = 0;
		StringBuffer error = new StringBuffer();
		VectorTokenizer vt = null; 
		Vector vecdat = null; 
		JSONObject registro = new JSONObject();
		List list_registro = new ArrayList();
		try{
		
				ParametrosGrales paramsG  = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
				JSONObject jsonObj = new JSONObject();
				
			
				java.io.File 					file 					= 	new java.io.File(strDirectorioTemp+nombreArchivo);
				
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
				
				while((linea = br.readLine())!= null) {
					ok = true;
						lineadocs++;		
					iTotalReg ++;		
					if (!linea.equals("")) {
						vt = new VectorTokenizer(linea,"|");
						vecdat = vt.getValuesVector(); 
							try {
							 campo_epopyme = (vecdat.size()>=1)?(String)vecdat.get(0):"";
							campo_calif = (vecdat.size()>=2)?(String)vecdat.get(1):"";		
						}
						catch(ArrayIndexOutOfBoundsException aioobe){
							out.println(aioobe); 
						}
						 vecdat.removeAllElements();	
						if((campo_epopyme.equals("")) || (campo_epopyme.length()==0))
						{
							campo = "CG_PYME_EPO_INTERNO"; 
							error.append("Error en la linea "+lineadocs+", el campo " + campo + " no puede estar vacio, debe de tener un valor. \n"); 
							ok = false;
						}
						if((campo_calif.equals("")) || (campo_calif.length()==0))
						{
							campo = "IC_CALIFICACION";
							error.append("Error en la linea "+lineadocs+", el campo " + campo + " no puede estar vacio, debe de tener un valor. \n");
							ok = false; 
						}else if(Comunes.esNumero(campo_calif)==false){
							String prue = "Error en la linea "+lineadocs+", el valor en el campo IC_CALIFICACION del archivo, debe de ser un valor numérico. \n";
							byte[] utf8Bytes = prue.getBytes("ISO-8859-1");
							String roundTrip = new String(utf8Bytes, "UTF-8");
							error.append(roundTrip); 
							ok = false;
						}	
						String resultado	=	paramsG.getConteoClasificacion(iNoEPO,campo_epopyme);
						if (resultado.equals("0")){
							error.append("Error en la linea "+lineadocs+", el proveedor o la clave no existe. \n");
							ok = false;
						}
					}else{
						ok = false;
						error.append("Error en la linea " + lineadocs + " linea vacia. \n");
					}
					if (ok){
						boolean existe = paramsG.getExisteClasificacion(campo_calif);
						if(existe==true){
							paramsG.actualizaClasificacionMasivaEpo(iNoEPO,campo_calif,campo_epopyme);
							iTotalEpos++;
						}
					}//ok
				}//while
				int rechaz = lineadocs - iTotalEpos;
			/*jsonObj.put("leidos", Integer.toString(iTotalReg));
			jsonObj.put("carga", Integer.toString(iTotalEpos));
			jsonObj.put("rechaza", Integer.toString(rechaz));*/
			registro = new JSONObject();
			registro.put("REGISTROS",	"Registros le&iacute;dos");
			registro.put("TOTAL",	Integer.toString(iTotalReg));												
			list_registro.add(registro);
			
			registro = new JSONObject();
			registro.put("REGISTROS",	"Registros cargados o actualizados");
			registro.put("TOTAL",	Integer.toString(iTotalEpos));												
			list_registro.add(registro);
			
			registro = new JSONObject();
			registro.put("REGISTROS",	"Registros rechazados");
			registro.put("TOTAL",	Integer.toString(rechaz));												
			list_registro.add(registro);
			
			JSONArray jsonArray = new JSONArray();
			jsonArray = JSONArray.fromObject(list_registro);
			
				
			jsonObj.put("ok", new Boolean(ok));
			jsonObj.put("estadoSiguiente", "MUESTRA_RESULTADO");	
			jsonObj.put("errores", error.toString());
			jsonObj.put("registrosResumen",jsonArray.toString());
			jsonObj.put("total",jsonArray.size()+"");
			jsonObj.put("success", new Boolean(true));
			infoRegresar = jsonObj.toString();
		
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
		
		
		
		
		System.out.println("TERMINO Y EMPEZO EL THREAD "+infoRegresar);
	
	
}

	
%>
<%=infoRegresar%>
