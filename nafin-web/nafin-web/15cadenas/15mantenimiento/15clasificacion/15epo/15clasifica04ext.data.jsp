<%@ page
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.logging.Log,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,com.netro.parametrosgrales.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="text/html; charset=UTF-8"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
	boolean flag = true;
	ParametrosRequest req = null;
	String infoRegresar	=	"";
	String nombreArchivo = "";
	String itemArchivo	="",  rutaArchivo ="",   error_tam =""; 
	String PATH_FILE	=	strDirectorioTemp;
	String rutaArchivoTemporal ="";
	System.out.println("LLEGO ");
	boolean		success		= true;
	int tamanio = 0;
	if (ServletFileUpload.isMultipartContent(request)) {
		JSONObject jsonObj = new JSONObject();

		try{
			ParametrosGrales paramsG  = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
			
			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();
			// Set factory constraints		
			factory.setRepository(new File(PATH_FILE));
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
			req = new ParametrosRequest(upload.parseRequest(request));
			
			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName();
			InputStream archivo = fItem.getInputStream();
			rutaArchivo = PATH_FILE+itemArchivo;
			 tamanio			= (int)fItem.getSize();
			nombreArchivo= Comunes.cadenaAleatoria(16)+ ".txt";
			rutaArchivoTemporal = PATH_FILE + nombreArchivo;
			fItem.write(new File(rutaArchivoTemporal));
			if(tamanio > 100000) {
				flag = false;
				nombreArchivo="";
			}
			ParametrosGrales paramGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
			boolean continua = true;
			String codificacionArchivo ="";
			if(paramGrales.validaCodificacionArchivoHabilitado()){
				String[]  	CODIFICACION	= {""};
				CodificacionArchivo  valida_codificacion= new CodificacionArchivo();
				valida_codificacion.setProcessTxt(true);
					//codificaArchivo.setProcessZip(true);
				if(valida_codificacion.esCharsetNoSoportado(rutaArchivoTemporal,CODIFICACION)){
						codificacionArchivo = CODIFICACION[0];
						continua = false;
					
				}
			}
			if(continua==true){  
				jsonObj.put("nombreArchivo",nombreArchivo);
				
				jsonObj.put("estadoSiguiente", "VALIDA_CARACTERES_CONTROL");
			}else{
				jsonObj.put("estadoSiguiente", "MENSAJE_CODIFICACION"		);
				jsonObj.put("codificacionArchivo",codificacionArchivo	);
			}
			jsonObj.put("flag", new Boolean(flag));
			jsonObj.put("success", new Boolean(true));
			infoRegresar = jsonObj.toString();
			System.out.println("infoRegresar "+infoRegresar);
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}
	
%>
<%=infoRegresar%>
