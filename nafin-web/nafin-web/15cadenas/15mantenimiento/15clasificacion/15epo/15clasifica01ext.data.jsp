<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.parametrosgrales.*,
		com.netro.model.catalogos.CatalogoEpoClasificacion"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if (informacion.equals("valoresIniciales")) {

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("strTipoUsuario", strTipoUsuario);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoClasificacion")){

	CatalogoEpoClasificacion cat = new CatalogoEpoClasificacion();
	cat.setCampoClave("ic_calificacion");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setClaveEpo(iNoEPO);
	cat.setOrden("ic_calificacion");
	infoRegresar = cat.getJSONElementos();

} else if (informacion.equals("Consulta")	||	informacion.equals("ArchivoCSV")){

	String numProv = (request.getParameter("numProv")!=null)?request.getParameter("numProv"):"";
	String nomProv = (request.getParameter("nomProv")!=null)?request.getParameter("nomProv"):"";
	String tipoClas = (request.getParameter("tipoClas")!=null)?request.getParameter("tipoClas"):"";

	if (!numProv.equals("") || !nomProv.equals("") || !tipoClas.equals("")) {
		com.netro.cadenas.ConsClasificacionEpo paginador = new com.netro.cadenas.ConsClasificacionEpo();
		paginador.setClaveEpo(iNoEPO);
		paginador.setNumProveedor(numProv);
		paginador.setNomProveedor(nomProv);
		paginador.setTipoClasificacion(tipoClas);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		Registros reg = queryHelper.doSearch();
		
		if (informacion.equals("Consulta")) {
			try {
				reg = queryHelper.doSearch();
				infoRegresar =  "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData() + "}";
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		}else if (informacion.equals("ArchivoCSV")) {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0, \"registros\": [] }";
	}
}else if (informacion.equals("Guarda")){

	//ParametrosGralesHome parametrosGralesHome = (ParametrosGralesHome)ServiceLocator.getInstance().getEJBHome("ParametrosGralesEJB", ParametrosGralesHome.class);
	//ParametrosGrales paramsG = parametrosGralesHome.create();
	ParametrosGrales paramsG  = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);

	JSONObject jsonObj= new JSONObject();
	String cadList		=	(request.getParameter("lista")!=null)?request.getParameter("lista"):"";
	String sel_asignar=	(request.getParameter("sel_asignar")!=null)?request.getParameter("sel_asignar"):"";
	JSONArray listaH	=	JSONArray.fromObject(cadList);
	paramsG.actualizaClasificacionEpo(iNoEPO,sel_asignar, listaH);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}
%>
<%=infoRegresar%>