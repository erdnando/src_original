Ext.onReady(function() {
	
	Ext.namespace('NE.monitordispersion');
	
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaMonitorDispersion = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						monitorDispersion(resp.estadoSiguiente,resp);
					}
				);
			} else {
				monitorDispersion(resp.estadoSiguiente,resp);
			}
			
		} else {
						
			/*
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Resetear forma
         resetWindowConfirmacionClave();
 
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			*/
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
					
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var monitorDispersion = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"							   ){
 
			// Cargar Cat�logo de Tipos de Monitoreo
			var catalogoTipoMonitoreoData = Ext.StoreMgr.key('catalogoTipoMonitoreoDataStore');
			catalogoTipoMonitoreoData.load({
				params: { defaultValue: "P" }
			});

			// El Cat�logo EPO se presentar� vac�o por default
			Ext.getCmp("comboEPO").setNewEmptyText("Seleccionar...");
			var catalogoEPOData = Ext.StoreMgr.key('catalogoEPODataStore');
			catalogoEPOData.load();
 
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispPEMEX01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.inicializacion'
				},
				callback: 				procesaMonitorDispersion
			});
 
		} else if(			estado == "MOSTRAR_PANTALLA_MONITOREO"			  ){
									
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispPEMEX01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.mostrarPantallaMonitoreo',
					tipoMonitoreo:		respuesta.tipoMonitoreo
				},
				callback: 				procesaMonitorDispersion
			});
 			
		} else if(			estado == "ESPERAR_DECISION"  					  ){ 
			         
			/*
			
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
			
         // Resetear forma
         resetWindowConfirmacionClave();
         
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			*/
			
		} else if(			estado == "FIN"  					                 ){
 	
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"15monitordispPEMEX01ext.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			forma.submit();
						
		}
		
	}
 
	//--------------------------------- 2. PANEL DE DETALLE MONITOREO PEMEX ---------------------------------------
 
	var procesarSuccessFailureGeneraPDF =  function(opts, success, response) {
		
		var btnGeneraPDF = Ext.getCmp('btnGeneraPDF');
		btnGeneraPDF.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaPDF = Ext.getCmp('btnBajaPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(/^\/nafin/,'');
				// Insertar archivo
				Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					}
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
			});
			
		} else {
			
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarSuccessFailureGeneraCSV =  function(opts, success, response) {
		
		var btnGeneraCSV = Ext.getCmp('btnGeneraCSV');
		btnGeneraCSV.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaCSV = Ext.getCmp('btnBajaCSV');
			btnBajaCSV.show();
			btnBajaCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaCSV.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(/^\/nafin/,'');
				// Insertar archivo
				Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					}
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
			});
			
		} else {
			
			btnGeneraCSV.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarDetalleMonitoreoPEMEXData = function(store, registros, opts){
				
		var panelMonitoreoPEMEX = Ext.getCmp('panelMonitoreoPEMEX');
		panelMonitoreoPEMEX.el.unmask();
		
		var gridDetalleMonitoreoPEMEX = Ext.getCmp('gridDetalleMonitoreoPEMEX');
		
		if (registros != null) {
			
			/*
			if (!gridDetalleMonitoreoPEMEX.isVisible()) {
				gridDetalleMonitoreoPEMEX.show();
			}
			*/
			
			var btnGeneraPDF 				 = Ext.getCmp('btnGeneraPDF');
			var btnGeneraCSV				 = Ext.getCmp('btnGeneraCSV');
			var btnBajaPDF					 = Ext.getCmp('btnBajaPDF');
			var btnBajaCSV					 = Ext.getCmp('btnBajaCSV');		
 
			var el 					 		 = gridDetalleMonitoreoPEMEX.getGridEl();
			
			// Copiar en el grid los parametros de la ultima consulta
			gridDetalleMonitoreoPEMEX.formParams = Ext.apply({},opts.params);
			
			// Actualizar propiedades del grid panel
			if(store.getTotalCount() > 0) {
				
				btnGeneraPDF.enable();
				btnGeneraCSV.enable();
				btnBajaPDF.hide();
				btnBajaCSV.hide();
				
				el.unmask();
				
			} else {
				
				btnGeneraPDF.disable();
				btnGeneraCSV.disable();
				btnBajaPDF.hide();
				btnBajaCSV.hide();
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
 
		}
					
	};
	
	var numeroNafinElectronicoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var nombreIFRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var nombrePYMERenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var estatusRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var fechaVencimientoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var numeroTotalDocumentosRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var montoTotalRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="text-align:right;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		value = record.json['MONTO_TOTAL'];
		return value;
		
	}
	
	var codigoBancoCuentaClabeRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var cuentaClabeRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var descripcionCECOBANRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
 
	// Crear JsonStore que se encargar� de cargar el detalle
	var detalleMonitoreoPEMEXData = new Ext.data.GroupingStore({
		id:					'detalleMonitoreoPEMEXDataStore',
		name:					'detalleMonitoreoPEMEXDataStore',
		root:					'registros',
		url: 					'15monitordispPEMEX01ext.data.jsp',
		autoDestroy:		true,
		baseParams: {
			informacion: 	'ConsultaDetalleMonitoreoPEMEXREF'
		},			
		reader: new Ext.data.JsonReader({
			root: 			'registros',	
			totalProperty: 'total',
			fields: [
				{ name:'GROUP_ID',							type:"string" }, // GROUP_ID
				{ name:'IC_EPO',								type:"string" },
				{ name:'NOMBRE_EPO',							type:"string" },
				{ name:'NUMERO_NAFIN_ELECTRONICO',		type:"string" },
				{ name:'NOMBRE_IF',							type:"string" },
				{ name:'NOMBRE_PYME',						type:"string" },
				{ name:'ESTATUS',								type:"string" },
				{ name:'FECHA_VENCIMIENTO',				type:"string" },
				{ name:'NUMERO_TOTAL_DOCUMENTOS',		type:"int"    },
				{ name:'MONTO_TOTAL',						type:"float", convert: function(value, record){ return Number(value.replace(/[\$,]/g,"")); } },
				{ name:'CODIGO_BANCO_CUENTA_CLABE',		type:"string" },
				{ name:'CUENTA_CLABE',						type:"string" },
				{ name:'DESCRIPCION_CECOBAN',				type:"string" },
				{ name:'ESTATUS_CECOBAN',					type:"string" }
			]
		}),
		groupField: 			'GROUP_ID', // GROUP_ID
		sortInfo: 				{ field: 'NOMBRE_EPO', direction: "ASC" },
		autoLoad: 				false,
		totalProperty: 		'total',
		messageProperty: 		'msg',
		pruneModifiedRecords: true, // Para que no se conserven los registros modificados en consulta anterior.
		listeners: { 
			beforeload: {
				fn: function( store, opts ) {
					var grid = Ext.getCmp('gridDetalleMonitoreoPEMEX');
					if (grid) {
						grid.show();
					} 			
				}
			},
			load: 	procesarDetalleMonitoreoPEMEXData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarDetalleMonitoreoPEMEXData(null, null, null);						
				}
			}
		}
	});
    
	// Crear plugin que se encargar� de desplegar los totales
	var totalesPorGrupoSummary = new Ext.ux.grid.ServerSummary();
	
	var gridDetalleMonitoreoPEMEX = {
		frame:			true,
		store: 			detalleMonitoreoPEMEXData,
		//title:			'Detalle Monitoreo PEMEX',
		xtype: 			'grid',
		id:				'gridDetalleMonitoreoPEMEX',
		hidden:			true,
		stripeRows: 	true,
		loadMask: 		true,
		width: 			940, 
		height:			480,
		region: 			'center',
		style: 			'margin: 0 auto',
		formParams:		{},
		view:		 		new Ext.grid.SelectableGroupingView(
			{
				forceFit:				false, 
				groupTextTpl: 			'{text}', // Usar group text renderer
				hideGroupedColumn: 	true,
				enableGroupingMenu:	false,
				hideGroupCheckBox: 	true
			}
		),
		plugins: totalesPorGrupoSummary,
		columns: [
			{
				header: 		'CLAVE IC_EPO FECHA_VENCIMIENTO', 
				tooltip: 	'CLAVE IC_EPO FECHA_VENCIMIENTO', // GROUP_ID
				dataIndex: 	'GROUP_ID', // IC_EPO:FECHA_VENCIMIENTO
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				// width: 		315,
				hidden: 		true,
				hideable:	false,
				groupTextRenderer: function(value,unused,record,rowIndex,colIndex,dsStore){
					return  record.data['NOMBRE_EPO'];
				}
			},
			{
				header: 		'Num. Nafin<br>Electr&oacute;nico',
				tooltip: 	'Num. Nafin Electr&oacute;nico',
				dataIndex: 	'NUMERO_NAFIN_ELECTRONICO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroNafinElectronicoRenderer
			},
			{
				header: 		'IF<br>&nbsp;',
				tooltip: 	'IF',
				dataIndex: 	'NOMBRE_IF',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	nombreIFRenderer
			},
			{
				header: 		'PYME<br>&nbsp;',
				tooltip: 	'PYME',
				dataIndex: 	'NOMBRE_PYME',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	nombrePYMERenderer
			},
			{
				header: 		'Estatus<br>&nbsp;',
				tooltip: 	'Estatus',
				dataIndex: 	'ESTATUS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		74,
				hidden: 		false,
				renderer:	estatusRenderer
			},
			{
				header: 		'Fecha de<br>Vencimiento',
				tooltip: 	'Fecha de Vencimiento',
				dataIndex: 	'FECHA_VENCIMIENTO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	fechaVencimientoRenderer
			},
			{
				header: 		'Num. Total<br>de Doctos.',
				tooltip: 	'Num. Total de Doctos.',
				dataIndex: 	'NUMERO_TOTAL_DOCUMENTOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroTotalDocumentosRenderer
			},
			{
				header: 		'Monto Total<br>&nbsp;',
				tooltip: 	'Monto Total',
				dataIndex: 	'MONTO_TOTAL',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	montoTotalRenderer
			},
			{
				header: 		'C&oacute;digo Banco<br>Cuenta Clabe',
				tooltip: 	'C&oacute;digo Banco Cuenta Clabe',
				dataIndex: 	'CODIGO_BANCO_CUENTA_CLABE',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				renderer:	codigoBancoCuentaClabeRenderer
			},
			{
				header: 		'Cuenta CLABE<br>&nbsp;',
				tooltip: 	'Cuenta CLABE',
				dataIndex: 	'CUENTA_CLABE',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	cuentaClabeRenderer
			},
			{
				header: 		'Descripci&oacute;n<br>CECOBAN',
				tooltip: 	'Descripci&oacute;n CECOBAN',
				dataIndex: 	'DESCRIPCION_CECOBAN',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	descripcionCECOBANRenderer
			}
		],
		stateful:	false,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'left',
			items: [
				'->',
				{
					xtype: 	'button',
					text: 	'Generar PDF',
					id: 		'btnGeneraPDF',
					iconCls: 'icoGenerarDocumento',
					disabled: true,
					handler: function(boton, evento) {
						
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						var panelMonitoreoPEMEX 		= Ext.getCmp("panelMonitoreoPEMEX");
						var gridDetalleMonitoreoPEMEX = Ext.getCmp("gridDetalleMonitoreoPEMEX");
						
						// Generar Archivo PDF
						Ext.Ajax.request({
							url: 		'15monitordispPEMEX01ext.data.jsp',
							params: 	Ext.apply(
								gridDetalleMonitoreoPEMEX.formParams, // panelMonitoreoPEMEX.getForm().getValues()
								{
									informacion: 'GenerarArchivo',
									tipo: 		 'PDF'
								}
							),
							callback: procesarSuccessFailureGeneraPDF
						});
						
					}
				},
				{
					xtype: 	'button',
					text: 	'Bajar PDF',
					id: 		'btnBajaPDF',
					iconCls:	'icoBotonPDF',
					hidden: 	true
				},
				'-',
				{
					xtype: 	'button',
					text: 	'Generar Archivo',
					id: 		'btnGeneraCSV',
					iconCls:	'icoGenerarDocumento',
					disabled: true,
					handler: function(boton, evento) {
								
						boton.disable();
						boton.setIconClass('loading-indicator');
									
						var panelMonitoreoPEMEX 		= Ext.getCmp("panelMonitoreoPEMEX");
						var gridDetalleMonitoreoPEMEX = Ext.getCmp("gridDetalleMonitoreoPEMEX");
						
						// Generar Archivo CSV
						Ext.Ajax.request({
							url: 		'15monitordispPEMEX01ext.data.jsp',
							params: 	Ext.apply(
								gridDetalleMonitoreoPEMEX.formParams, // panelMonitoreoPEMEX.getForm().getValues()
								{
									informacion: 'GenerarArchivo',
									tipo: 		 'CSV'
								}
							),
							callback: procesarSuccessFailureGeneraCSV
						});
							
					}
				},
				{
					xtype: 	'button',
					text: 	'Bajar Archivo',
					id: 		'btnBajaCSV',
					iconCls:	'icoBotonXLS',
					hidden: 	true
				}
			]
		}
	};

	//------------------------------------- 1. PANEL DE MONITOREO PEMEX ------------------------------------------

	var procesaConsultar = function(boton,evento){
		
		// Por compatibilidad con la versi�n anterior
		var comboTipoMonitoreo = Ext.getCmp("comboTipoMonitoreo");
		if( comboTipoMonitoreo.getValue() !== "P" ){
			return;	
		}
 
		// Validar toda la forma
		var panelMonitoreoPEMEX = Ext.getCmp("panelMonitoreoPEMEX");
		if( !panelMonitoreoPEMEX.getForm().isValid() ){
			return;
		}
 
		/* 
			Nota: las siguientes validaciones ya fueron incluidas en la validacion
			'rangofecha':
				"Debe capturar un rango para la fecha de vencimiento."
				"La primera fecha en el rango debe ser menor a la segunda"
		*/
		
		// Agregar m�scara
		panelMonitoreoPEMEX.getEl().mask('Consultando...','x-mask-loading');
		
		// Si hay una m�scara previa en el grid, suprimirla
		var gridEl = Ext.getCmp("gridDetalleMonitoreoPEMEX").getGridEl();
		if( gridEl.isMasked()){
			gridEl.unmask();
		}
		
		// Realizar consulta
		var detalleMonitoreoPEMEXData = Ext.StoreMgr.key('detalleMonitoreoPEMEXDataStore');
		detalleMonitoreoPEMEXData.load({
			params: Ext.apply(
				panelMonitoreoPEMEX.getForm().getValues(),
				{
					informacion: 'ConsultaDetalleMonitoreoPEMEXREF'
				}
			)
		});
		 
	}
 
	var procesaLimpiar = function(boton,evento){
		Ext.getCmp("panelMonitoreoPEMEX").getForm().reset();
		Ext.getCmp("fechaVencimientoDe").setMaxValue('');
		Ext.getCmp("fechaVencimientoA").setMinValue('');
	}
	
	var catalogoTipoMonitoreoData = new Ext.data.JsonStore({
		id: 					'catalogoTipoMonitoreoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispPEMEX01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoTipoMonitoreo'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboTipoMonitoreo = Ext.getCmp("comboTipoMonitoreo");
						comboTipoMonitoreo.setValue(defaultValue);
						comboTipoMonitoreo.originalValue = comboTipoMonitoreo.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
			
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 					'catalogoEPODataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispPEMEX01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoEPO'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				if( store.getTotalCount() === 0 ){
					Ext.getCmp("comboEPO").setNewEmptyText("No se encontr� ninguna EPO.");
					return;
				}
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboEPO = Ext.getCmp("comboEPO");
						comboEPO.setValue(defaultValue);
						comboEPO.originalValue = comboEPO.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementospanelMonitoreoPEMEX = [
		// COMBO TIPO DE MONITOREO
		{
			xtype: 				'combo',
			name: 				'tipoMonitoreo',
			id: 					'comboTipoMonitoreo',
			fieldLabel: 		'Tipo de Monitoreo',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveTipoMonitoreo',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTipoMonitoreoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'65%',
			autoSelect:			true,
			listeners:			{
				select:	function( combo, record, index ){
					
					var respuesta = new Object();
					respuesta['tipoMonitoreo'] = combo.getValue();
					monitorDispersion("MOSTRAR_PANTALLA_MONITOREO",respuesta);
					
				}
			}
		},
		// COMBO EPO
		{
			xtype: 				'combo',
			name: 				'epo',
			id: 					'comboEPO',
			fieldLabel: 		'EPO',
			allowBlank:			true,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPO',
			emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			msgTarget: 			'side',
			minChars: 			1,
			store: 				catalogoEPOData,
			anchor:				'95%',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}	
		},
		// DATERANGE FECHA DE VENCIMIENTO
		{
			xtype: 						'compositefield',
			fieldLabel: 				'Fecha de Vencimiento',
			combineErrors: 			false,
			msgTarget: 					'side',
			items: [
				{
					xtype: 				'datefield',
					name: 				'fechaVencimientoDe',
					id: 					'fechaVencimientoDe',
					allowBlank: 		true,
					startDay: 			0,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoFinFecha: 	'fechaVencimientoA',
					margins: 			'0 20 0 0',
					allowBlank:			false
				},{
					xtype: 				'displayfield',
					value: 				'a',
					width: 				20
				},{
					xtype: 				'datefield',
					name: 				'fechaVencimientoA',
					id: 					'fechaVencimientoA',
					allowBlank: 		true,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoInicioFecha: 'fechaVencimientoDe',
					margins: 			'0 20 0 0',
					allowBlank:			false
				}
			]
		}

	];
	
	var panelMonitoreoPEMEX = new Ext.form.FormPanel({
		id: 					'panelMonitoreoPEMEX',
		width: 				500,
		title: 				'Monitor PEMEX-REF',
		hidden:				false,
		frame: 				true,
		collapsible: 		true,
		titleCollapse: 	true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px',
		defaults: {	
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		labelWidth: 		130,
		defaultType: 		'textfield',
		trackResetOnLoad:	true,
		items: 				elementospanelMonitoreoPEMEX,
		monitorValid: 		false,
		buttons: [
			{
				text: 		'Consultar',
				iconCls: 	'icoBuscar',
				handler: 	procesaConsultar
			},
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	procesaLimpiar
			}
		]
	});
	
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelMonitoreoPEMEX,
			NE.util.getEspaciador(10),
			gridDetalleMonitoreoPEMEX
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	monitorDispersion("INICIALIZACION",null);
	
});