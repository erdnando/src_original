function selecCSBLOQUEO(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
	if(check.checked == false){
		check.checked = true;
	}
	if(check.id == 'chkGenerica'){
		if(check.checked == true){
			reg.set('GENERICA','S');
			reg.set('AUX_GENERICA','checked');
			reg.set('ESPECIFICA','N');
			reg.set('AUX_ESPECIFICA','');
			reg.set('CG_TIPO','G');
		}
	}else if(check.id == 'chkEspecifica'){
		if(check.checked==true){
			reg.set('GENERICA','N');
			reg.set('AUX_GENERICA','');
			reg.set('ESPECIFICA','S');
			reg.set('AUX_ESPECIFICA','checked');
			reg.set('CG_TIPO','E');
		}
	}else	if(check.id == 'chkTef'){
		if(check.checked==true){
			reg.set('TEF','S');
			reg.set('AUX_TEF','checked');
			reg.set('AMBAS','N');
			reg.set('AUX_AMBAS','');
			reg.set('SPEI','N');
			reg.set('AUX_SPEI','');
			reg.set('CG_AGENCIA_SIRAC','T');
		}
	}else	if(check.id == 'chkAmbas'){
		if(check.checked==true){
			reg.set('TEF','N');
			reg.set('AUX_TEF','');
			reg.set('AMBAS','S');
			reg.set('AUX_AMBAS','checked');
			reg.set('SPEI','N');
			reg.set('AUX_SPEI','');
			reg.set('CG_AGENCIA_SIRAC','A');
		}
	}else	if(check.id == 'chkSpei'){
		if(check.checked==true){
			reg.set('TEF','N');
			reg.set('AUX_TEF','');
			reg.set('AMBAS','N');
			reg.set('AUX_AMBAS','');
			reg.set('SPEI','S');
			reg.set('AUX_SPEI','checked');
			reg.set('CG_AGENCIA_SIRAC','S');
		}
	}
	store.commitChanges();
}
Ext.onReady(function() {
	var tipoDetalle ;
	var tipo =[];
	var via_liquidacion=[];
	var Generica1 = [];   
	var Especifica1 = [];
	var tef1 = [];
	var ambas1 = [];
	var spei1 = [];
	var varGlo=function(){
		tipo= [];
		via_liquidacion=[];
		Generica1 = [];
		Especifica1 = [];
		tef1 = [];
		ambas1 = [];
		spei1 = [];
	}
	var varGloDetalle=function(){
		tipoDetalle ;
	}

	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);
//- - - - - - -  - - - - - HANDLER�S - - - - - - - - - - - - - - - - - - - - - -
	function procesarDescargaArchivos(opts, success, response) {
		Ext.getCmp('btnPdf').enable();
		var btnImprimirPDF = Ext.getCmp('btnPdf');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fpDespliega.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpDespliega.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaRegistrosDetalle = function(store, arrRegistros, opts) 	{
		Ext.getCmp('btnAceptar').enable();
		var btnConsultar = Ext.getCmp('btnAceptar');
		btnConsultar.setIconClass('icoBuscar');
		var gridConsultaDetalle = Ext.getCmp('gridConsultaDetalle');
		gridConsultaDetalle.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsultaDetalle.isVisible() ) {
						gridConsultaDetalle.show();
			}
			var jsonData = store.reader.jsonData;
			var cm = gridConsultaDetalle.getColumnModel();
			if(tipoDetalle=="EPO"){
				gridConsultaDetalle.getColumnModel().setHidden(cm.findColumnIndex('CG_RAZON_SOCIAL'),jsonData.CG_RAZON_SOCIAL);
			}else if(tipoDetalle=="IF"){
				gridConsultaDetalle.getColumnModel().setHidden(cm.findColumnIndex('RAZON_SOCIAL'),jsonData.RAZON_SOCIAL);
				gridConsultaDetalle.getColumnModel().setHidden(cm.findColumnIndex('CG_RAZON_SOCIAL'),jsonData.CG_RAZON_SOCIAL);
			}
			if(store.getTotalCount() > 0) {
				gridConsultaDetalle.el.unmask();
			} else {
				gridConsultaDetalle.el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var procesarGuardarCambiosDetalle = function(opts, success, response) {
		Ext.getCmp('btnGuardarDetalle').enable();
		var btnGuardar = Ext.getCmp('btnGuardarDetalle');
		btnGuardar.setIconClass('icoGuardar');
		var btnGuardarDetalle = Ext.getCmp('btnGuardarDetalle');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var mensaje = Ext.util.JSON.decode(response.responseText).MENSAJE;
			Ext.MessageBox.alert("Mensaje de confirmaci�n", mensaje);
			btnGuardarDetalle.enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function resultadoEliminaDetalle(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var mensaje = Ext.util.JSON.decode(response.responseText).MENSAJE;
			Ext.MessageBox.alert("Mensaje de confirmaci�n", mensaje);
			storeDetalle.load({
				params: Ext.apply(fpDespliega.getForm().getValues(),{							
					informacion: 'ConsultarDetalle',
					tipoDetalle:tipoDetalle
				})
			});

		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	var procesarEliminar= function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var desc_if;
		var desc_epo = registro.get('CG_RAZON_SOCIAL');  
		var via_liquidacion = registro.get('CG_VIA_LIQUIDACION'); 
		if(tipoDetalle=="IF"){
			 desc_if = registro.get('RAZON_SOCIAL');
		}
		Ext.Msg.show({
			title: 'Confirmar',
			msg: '�Esta seguro que desea eliminar el registro?',
			buttons: Ext.Msg.OKCANCEL,
			fn: function peticionAjax(btn){
				if (btn == 'ok'){
					Ext.Ajax.request({
					url: '15paramffExt01.data.jsp',
					params: Ext.apply({
						informacion: 'eliminarDetalle',
						desc_if:desc_if,
						desc_epo:desc_epo,
						via_liquidacion:via_liquidacion,
						tipoDetalle:tipoDetalle
					}),
					callback: resultadoEliminaDetalle
					});
				}
			}
		});
	}
	var procesarVerDetalle = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var verDetalle = Ext.getCmp('ProcesarDetalle');
		varGloDetalle();
		tipoDetalle = registro.get('CD_DESCRIPCION'); 
		if(tipoDetalle=="EPO"){
			Ext.getCmp('cmbIF').hide();
			Ext.getCmp('cmbEPO').show();
			StoreComboEPO.load({});
		}else{
			StoreComboEPO.removeAll();
			StoreComboEPO.total = 0;
			Ext.getCmp('cmbIF').show();
			Ext.getCmp('cmbEPO').show();
			
			StoreComboIF.load({});
		}
		if(verDetalle){
			Ext.getCmp('cmbIF').reset();
			Ext.getCmp('cmbEPO').reset();
			Ext.getCmp('rdgLiquidacion').reset();
			Ext.getCmp('gridConsultaDetalle').hide();
			verDetalle.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 450,
				height: 350,
				modal: true,					
				closeAction: 'hide',
				resizable: true,
				constrain: true,
				closable:false,
				id: 'ProcesarDetalle',
				items: [					
					fpDespliega
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: [
						'->',
						'-',
						{	xtype: 'displayfield',	width: 10	},
						{	xtype: 'button',
							text: 'Guardar',
							id: 'btnGuardarDetalle',
							iconCls: 'icoGuardar', 
							width:90,
							handler: function(boton, evento) {
								var comboIf = Ext.getCmp('cmbIF');
								var comboEpo = Ext.getCmp('cmbEPO');
								var rdgLiquidacion = Ext.getCmp('rdgLiquidacion');
								if(tipoDetalle=="IF"){
									if(comboIf.getValue()==""){
										comboIf.markInvalid("Debe seleccionar un IF para poder guardar cambios.");
										return;
									}
								}
								if(comboEpo.getValue()==""){
									comboEpo.markInvalid("Debe seleccionar una EPO para poder guardar cambios.");
									return;
								}
								if(rdgLiquidacion.getValue()==null){
									rdgLiquidacion.markInvalid("Debe seleccionar una V�a de Liquidaci�n.");
									return;
								}
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '15paramffExt01.data.jsp',
									params: Ext.apply(fpDespliega.getForm().getValues(),{
										informacion : 'GuardaDetalle',
										tipoDetalle : tipoDetalle
									}),
									callback: procesarGuardarCambiosDetalle
								});
							}
						},
						{	xtype: 'displayfield',	width: 10	},
						{	xtype: 'button',
							text: 'Salir',
							iconCls: 'icoLimpiar', 
							id: 'btnCerraP',
							handler: function(){		
								Ext.getCmp('ProcesarDetalle').hide();		
							}
						}
					]
				}
			}).show().setTitle('');
		}
	}
	
	
	var procesarConsultaRegistros = function(total, registros){
		var btnGuardar = Ext.getCmp('btnGuardar');
		var grid  = Ext.getCmp('gridConsulta');
		if (registros != null) {
			if (!grid.isVisible() ) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(registrosConsultadosData.getTotalCount() > 0) {
				btnGuardar.enable();
			} else {
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
				btnGuardar.disable();
			}
		}
	}
	var guardarCambios = function(){
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		var jsonData = store.data.items;
		var lista = new Array();
		varGlo();
		var numeroRegistros = 0;
		Ext.each(jsonData, function(item, index, arrItem){
			tipo.push( item.data.CG_TIPO);
			via_liquidacion.push(item.data.CG_VIA_LIQUIDACION);
			
			Generica1.push(item.data.GENERICA);
			Especifica1.push(item.data.ESPECIFICA);
			tef1.push(item.data.TEF);
			ambas1.push(item.data.AMBAS);
			spei1.push(item.data.SPEI);
			numeroRegistros++;
		});
		Ext.Ajax.request({
			url: '15paramffExt01.data.jsp',
			params: {
				informacion: 'guardarCambios',
				tipo: tipo,
				via_liquidacion:via_liquidacion,
				numeroRegistros:numeroRegistros,
				Generica1: Generica1,
				Especifica1: Especifica1,
				tef1: tef1,
				ambas1 : ambas1,
				spei1 : spei1
			},
			callback: procesarGuardarCambios
		});
	}
	var procesarGuardarCambios = function(opts, success, response) {
		Ext.getCmp('btnGuardar').enable();
		var btnGuarda = Ext.getCmp('btnGuardar');
		btnGuarda.setIconClass('icoGuardar');
		var btnGuardar = Ext.getCmp('btnGuardar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var mensaje = Ext.util.JSON.decode(response.responseText).MENSAJE;
			Ext.MessageBox.alert("Mensaje de confirmaci�n", mensaje);
			registrosConsultadosData.load();
			btnGuardar.enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - -
	var StoreComboIF = new Ext.data.JsonStore({
		id: 'StoreComboIF',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '15paramffExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var StoreComboEPO = new Ext.data.JsonStore({
		id: 'StoreComboEPO',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '15paramffExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var storeDetalle = new Ext.data.JsonStore({
		root: 		'registros',
		id:			'storeDetalle',
		url: 			'15paramffExt01.data.jsp',
		baseParams: {	informacion:	'ConsultarDetalle'	},
		fields: [
			{ name: 'CG_RAZON_SOCIAL'},
			{ name: 'RAZON_SOCIAL' },
			{ name: 'CG_VIA_LIQUIDACION'},
			{ name: 'IC_IF' },
			{ name: 'IC_EPO'}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarConsultaRegistrosDetalle,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosDetalle(null, null, null);
				}
			}
		}
	});

	var registrosConsultadosData = new Ext.data.JsonStore({
		root: 		'registros',
		id:			'registrosConsultadosDataStore',
		url: 			'15paramffExt01.data.jsp',
		baseParams: {	informacion:	'consultaInicial'	},
		fields: [
			{ name: 'IC_PARAM_FF'},
			{ name: 'CD_DESCRIPCION' },
			{ name: 'CG_TIPO'},
			{ name: 'CG_VIA_LIQUIDACION'},
			{ name: 'IC_PRODUCTO_NAFIN'},
			{ name: 'GENERICA'},
			{ name: 'ESPECIFICA' },
			{ name: 'AUX_GENERICA'},
			{ name: 'AUX_ESPECIFICA'},
			{ name: 'TEF'},
			{ name: 'AMBAS'},
			{ name: 'SPEI' },
			{ name: 'DIST_EPO'},
			{ name: 'DIST_IF' },
			{ name: 'AUX_TEF'},
			{ name: 'AUX_AMBAS'},
			{ name: 'AUX_SPEI'}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarConsultaRegistros,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistros(null, null);
				}
			}
		}
	});
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -
	var seccion1 = [
		{	
			xtype				: 'combo',
			id					: 'cmbIF',
			name				: 'comboIF',
			fieldLabel: '&nbsp;&nbsp;IF',
			hiddenName 		: 'comboIF',
			width				: 250,
			forceSelection	: true,
			triggerAction	: 'all',
			mode				: 'local',
			hidden 			:true,
			valueField		: 'clave',
			store				: StoreComboIF,
			displayField	: 'descripcion',
			listeners:{
				select:{
					fn:function(combo){
						var claveIf = combo.getValue();
						StoreComboEPO.load({
							params: {
								claveIf: claveIf
							}
						});
					}
				}
			}
		},
		{	
			xtype				: 'combo',
			id					: 'cmbEPO',
			name				: 'comboEpo',
			fieldLabel: '&nbsp;&nbsp;EPO',
			hiddenName 		: 'comboEpo',
			width				: 250,
			forceSelection	: true,
			triggerAction	: 'all',
			mode				: 'local',
			hidden 			:true,
			valueField		: 'clave',
			store				: StoreComboEPO,
			displayField	: 'descripcion'
		},
		{	width:200,
			xtype:'radiogroup',
			msgTarget	: 'side',
			id:'rdgLiquidacion',	
			name:'liquidacion',
			hiddenName	: 'liquidacion',
			fieldLabel	: 'V�as de Liquidaci�n',
			cls			: 'x-check-group-alt',
			fils:[50, 50, 100],
			tabIndex		:23,
			items: [  
						{boxLabel	: 'TEF',		name : 'liquidacion', inputValue: 'T' },
						{boxLabel	: 'SPEI',		name : 'liquidacion',  inputValue: 'S' },
						{boxLabel	: 'Ambas(TEF y SPEI)',		name : 'liquidacion',  inputValue: 'A' }		
					]
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{	xtype: 'button',
					text: 'Consultar',
					iconCls: 'icoBuscar', 
					id: 'btnAceptar',
					handler: function(boton, record) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						storeDetalle.load({
							params: Ext.apply(fpDespliega.getForm().getValues(),{							
								informacion: 'ConsultarDetalle',
								tipoDetalle:tipoDetalle
							})
						});
					}
				},	
				{
					xtype:'displayfield',
					text:'',
					width: 10
				},
				{
					xtype: 'button',
					text: 'Limpiar V�a de Liquidaci�n',
					iconCls: 'icoLimpiar',
					handler: function(){		
								Ext.getCmp('rdgLiquidacion').reset();		
								
					}
				}
			]
		}
	
	];
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 1, align: 'center'},
				{header: 'Tipo', colspan: 2, align: 'center'},
				{header: 'V�as  de Liquidaci�n', colspan: 3, align: 'center'},
				{header: '', colspan: 1, align: 'center'}
			]
		]
	});
	var grid = new Ext.grid.EditorGridPanel({
		store: 		registrosConsultadosData,
		id:			'gridConsulta',
		style: 		'margin: 0 auto'/*; padding-top:10px;'*/,
		hidden: 		false,
		margins:		'20 0 0 0',
		title:		'Parametrizaci�n',
		clicksToEdit:1,
		stripeRows: true,
		loadMask: 	true,
		height: 	350,
		width: 		620,
		frame: 		true,
		plugins:		grupos,
		columns: [
			{
				header: 		'Dispersi�n',
				tooltip: 	'Dispersi�n',
				dataIndex: 	'CD_DESCRIPCION',
				align:		'center',
				sortable: 	false,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				hideable:	false
			},{
				header: 		'Gen�rica',
				tooltip: 	'Gen�rica',
				dataIndex:'GENERICA',
				sortable:false,
				width: 100,
				hidden:false,
				hideable:false,
				align:'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					if(record.get('DIST_EPO')=="F" ||	record.get('DIST_IF')=="F" ){
						return '<input  id="chkGenerica"'+ rowIndex + '	 value='+ record.data['GENERICA'] + '  disabled="true" type="checkbox"  '+record.data['AUX_GENERICA']+' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
					}else{
						return '<input  id="chkGenerica"'+ rowIndex + '	 value='+ record.data['GENERICA'] + ' type="checkbox"  '+record.data['AUX_GENERICA']+' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
					}
				}
			},{
				header: 		'Espec�fica',
				tooltip: 	'Espec�fica',
				dataIndex:'ESPECIFICA',
				sortable:false,
				width: 100,
				hidden:false,
				hideable:false,
				align:'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					return '<input  id="chkEspecifica"'+ rowIndex + ' value='+ record.data['ESPECIFICA'] + ' type="checkbox"  '+record.data['AUX_ESPECIFICA']+' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
				}
			},{
				header: 		'TEF',
				tooltip: 	'TEF',
				dataIndex:'TEF',
				sortable:false,
				width: 50,
				hidden:false,
				hideable:false,
				align:'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					return '<input  id="chkTef"'+ rowIndex + ' value='+ record.data['TEF'] + ' type="checkbox"  '+record.data['AUX_TEF']+' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
				}
			},{
				header: 		'<div class="formas" align="center">Ambas<br>(TEF y SPEI)</div>',
				tooltip: 	'Ambas (TEF y SPEI)',
				dataIndex:'AMBAS',
				sortable:false,
				width: 100,
				hidden:false,
				hideable:false,
				align:'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					return '<input  id="chkAmbas"'+ rowIndex + ' value='+ record.data['AMBAS'] + ' type="checkbox"  '+record.data['AUX_AMBAS']+' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
				}
			},
			{
				header: 		'SPEI',
				tooltip: 	'SPEI',
				dataIndex:'SPEI',
				sortable:false,
				width: 50,
				hidden:false,
				hideable:false,
				align:'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					if(record.get('CD_DESCRIPCION') =="EPO" || record.get('CD_DESCRIPCION') =="IF" ) {
						return '<input  id="chkSpei"'+ rowIndex + ' value='+ record.data['SPEI'] + ' type="checkbox"  '+record.data['AUX_SPEI']+' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
					}else{
						return '<input  id="chkSpei"'+ rowIndex + ' value='+ record.data['SPEI'] + ' disabled="true" type="checkbox"  '+record.data['AUX_SPEI']+' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
					}
				}
			},
			{
				xtype		: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle ',
				dataIndex: 'CD_DESCRIPCION',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.get('CD_DESCRIPCION') =="EPO" || record.get('CD_DESCRIPCION') =="IF" ) {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else{
								return '';
							}
						},
						handler: procesarVerDetalle
					}
				]
			}
		],
		bbar: {
		items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Guardar',
					id: 'btnGuardar',
					iconCls: 'icoGuardar',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						guardarCambios();
					}
				}
			]
		}
	});
	
	var gridConsultaDetalle= new Ext.grid.GridPanel({	
		title: '',
		id: 'gridConsultaDetalle',
		store: storeDetalle,
		style: 'margin:0 auto;',
		stripeRows : true,
		loadMask: true,
		hidden: true,
		frame: true,
		height: 	120,
		width: 		250,
		header	  : true,
		columns: [
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'RAZON_SOCIAL',
				sortable: true,
				hidden : true,
				width: 150,			
				resizable: true,
				align: 'left'		
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'CG_RAZON_SOCIAL',
				hidden : true,
				sortable: true,
				width: 150,	
				resizable: true,				
				align: 'left'
			},
			{
				header: 'V�as de Liquidaci�n',
				tooltip: 'V�as de Liquidaci�n',
				dataIndex: 'CG_VIA_LIQUIDACION',
				sortable: true,
				width: 100,	
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
							if(record.get('CG_VIA_LIQUIDACION') =="A"  ) {
								
								return 'Ambas';
							}else if(record.get('CG_VIA_LIQUIDACION') =="T"  ) {
								
								return 'TEF';
							}else if(record.get('CG_VIA_LIQUIDACION') =="S"  ) {
								
								return 'SPEI';
							}else{
								return '';
							}
				}
					
			
				
			},
			{
				xtype		: 'actioncolumn',
				header: 'Eliminar',
				tooltip: 'Eliminar ',
				id			:'btnElimina',
				width		:  50,	
				align		: 'center', 
				hidden	: false,
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Eliminar';
							return 'borrar';
						},
						handler:procesarEliminar
					}
				]
			}
		],
		bbar: {
		items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',
					id: 'btnPdf',
					iconCls: 'icoPdf',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15paramffExt01.data.jsp',
							params: Ext.apply(fpDespliega.getForm().getValues(),{							
								informacion: 'ArchivoPDF',
								tipoDetalle:tipoDetalle
							}),
							callback: procesarDescargaArchivos
						});
					}
				}
			]
		}
	});
	var fpDespliega = new Ext.form.FormPanel({
		id					: 'fpDespliega',		
		layout			: 'form',
		width				: 500,
		style				: ' margin:0 auto;',
		frame				: true,
		height: 350,
		title:'Detalle',
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				:  
		[
			seccion1,
			gridConsultaDetalle
		]	
	});
//----------------------------------- CONTENEDOR -------------------------------------
	var contenedorPrincipal = new Ext.Container({
		applyTo: 'areaContenido',
		width: 	930,
		align: 'center',
		items: 	[
			NE.util.getEspaciador(10),
			grid
		]

	});
	registrosConsultadosData.load();
});