Ext.onReady(function(){

//---------------------------descargaArchivoPDF------------------------------------
 function descargaArchivoPDF(opts, success, response) {
	
if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			Ext.getCmp('btnGenerarPDF').enable();
			fp.el.unmask();
      var el = gridConsulta.getGridEl();
      el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
			fp.el.unmask();
			Ext.getCmp('btnGenerarPDF').enable();
			Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
		}
	}
//---------------------------fin descargaArchivoPDF--------------------------------


//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
	var 	fp = Ext.getCmp('formcon');
			fp.el.unmask();				
	var 	gridConsulta = Ext.getCmp('gridConsulta');	
	var 	el = gridConsulta.getGridEl();	
	if (arrRegistros != null) {
		if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
		if(store.getTotalCount() > 0) {					
				el.unmask();			
				Ext.getCmp('btnGenerarPDF').enable();				
			} else {		
				Ext.getCmp('btnGenerarPDF').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
var consultaData = new Ext.data.JsonStore({
		root 			: 'registros',
		url 			: '15BloqServConsultaEXT.data.jsp',
		baseParams	: {
							informacion: 'Consultar'
							},
		fields		: [
							{	name: 'IC_NAFIN_ELECTRONICO'},
							{	name: 'CG_RAZON_SOCIAL'},
							{	name: 'CG_RFC'},
							{	name: 'CS_BLOQUEADO'},
							{	name: 'DF_FECHA'},
							{	name: 'CG_NOMBRE_USUARIO'},
							{	name: 'CG_CAUSA'}
							],		
		totalProperty 	: 'total',
		messageProperty: 'msg',
		autoLoad			: false,
		listeners		: {
			load			: procesarConsultaData,
				exception: {
						fn	: function(proxy, type, action, optionsRequest, response, args) {
								NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
								//LLama procesar consulta, para que desbloquee los componentes.
								procesarConsultaData(null, null, null);					
								}
								}
								}					
	})
//----------------------------Fin ConsultaData----------------------------------

//------------------------------Catalogo EPO------------------------------------
 var catalogoEpo = new Ext.data.JsonStore({
		id					: 'catalogoEpo',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '15BloqServConsultaEXT.data.jsp',
		baseParams		: {
								informacion		: 'catalogoepo'  
								},
		totalProperty 	: 'total',
		autoLoad			: false,
		listeners		: {			
								beforeload: NE.util.initMensajeCargaCombo,
								exception: NE.util.mostrarDataProxyError								
								}
	})
//----------------------------Fin Catalogo EPO----------------------------------
	
//-------------------------------Elementos Forma--------------------------------
	var  elementosForma =  [

		{
	xtype: 'compositefield',
   items: [
	{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 100
		},
		{
		xtype: 'button',
							text				:'Captura',
							id					:'btnCaptura',
							hidden			:false,
							iconCls			:'icoTxt',
							height 			: 15,
							width 			: 25,
							handler			:function(boton, evento){
							window.location  = "/nafin/15cadenas/15pki/15mantenimiento/15dispersion/15BloqServCaptEXT.jsp"; 
												}
		},{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 50
		},{
		xtype: 'button',
							text				:'Consulta',
							id					:'btnConsulta',
							hidden			:false,
							iconCls			:'icoBuscar',
							handler			:function(boton, evento){
							window.location  = "/nafin/15cadenas/15mantenimiento/15dispersion/15BloqServConsultaEXT.jsp";
												}
		}
		]},{
			xtype				: 'combo',
			fieldLabel		: 'EPO',
			name				: 'cmb_epo',	
			hiddenName		: '_cmb_epo',
			id					: 'cmbepo',	
			displayField: 'descripcion',
			valueField: 'clave',
			emptyText: 'Seleccionar ...',
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoEpo,
			tpl: NE.util.templateMensajeCargaComboConDescripcionCompleta,
			width: 120,
			listeners 	:{
				select	:{
					fn		:function(combo){
							Ext.getCmp('gridConsulta').hide()
					}
				}			
			}
		}
]
//-----------------------------Fin Elementos Forma------------------------------

//--------------------------------Grid Consulta---------------------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store				:consultaData,
		id					:'gridConsulta',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Bloqueo de Servicio Consulta',	
		clicksToEdit	:1,
		hidden			:true,
		columns			:[{
							header		:'N@E',
							tooltip		:'N@E',
							dataIndex	:'IC_NAFIN_ELECTRONICO',
							sortable	:true,
							resizable	:true,
							width: 100,
							align     :'center'
							},{
							header		:'Raz�n Social',
							tooltip		:'Raz�n Social',
							dataIndex	:'CG_RAZON_SOCIAL',
							sortable	:true,		
							resizable	:true,		
							width: 230,
							align			:'center',
              renderer: function(value) {
                    return "<div align='left'>" + value + "</div>";
                }				
							},{
							header		:'RFC',
							tooltip		:'RFC',
							dataIndex	:'CG_RFC',
							sortable	:true,		
							resizable	:true,
							width: 150,
							align			:'center'
							},{
							header		:'Bloqueado',
							tooltip		:'Bloqueado',
							dataIndex	:'CS_BLOQUEADO',
							sortable		:true,		
							resizable	:true,
							width: 100,
							align			:'center',
							renderer:function(value,metadata,registro){                              
							var lock = registro.data['CS_BLOQUEADO'];	
							if(lock =='N') {
								return 'No';
							}else if(lock =='S') { 
								return 'Si';
								}
							}//FIN render		
							},{
							header		:'Fecha/Hora',
							tooltip		:'Fecha/Hora',
							dataIndex	:'DF_FECHA',
							sortable	:true,		
							resizable	:true,
							width: 150,
							align			:'center'
							},{
							header		:'Nombre del Usuario',
							tooltip		:'Nombre del Usuario',
							dataIndex	:'CG_NOMBRE_USUARIO',
							sortable	:true,		
							resizable	:true,
							width: 200,
							  align			:'center',
							  renderer: function(value) {
								  return "<div align='left'>" + value + "</div>";
							 }	
							},{
							header: 'Causa',
							tooltip: 'Causa',
							dataIndex: 'CG_CAUSA',
							sortable: true,
							resizable: true,
							width: 200,
							align			:'center',
							  renderer: function(value) {
									  return "<div align='left'>" + value + "</div>";
								 }	
							}],	
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 430,
    width				: 730,
		align				: 'center',
		frame				: true,
		bbar				: {
							xtype			: 'paging',
							pageSize		: 15,
							//buttonAlign	: 'left',
							id				: 'barraPaginacion',
							displayInfo	: true,		
              store			: consultaData,
							displayMsg	: '{0} - {1} de {2}',							
							emptyMsg		: "No hay registros.",
							items			: [
											'->','-',
											{
											xtype			: 'button',
											text			: 'Generar PDF',					
											tooltip		:	'Generar PDF ',
											iconCls		: 'icoPdf', 
											id				: 'btnGenerarPDF',
											handler		: function(boton, evento) {
																fp.el.mask('Generando archivo...', 'x-mask-loading');
                                var el = gridConsulta.getGridEl();	
                                el.mask('Generando Archivo...', 'x-mask-loading');
																Ext.getCmp('btnGenerarPDF').disable();
                                var barraPaginacionA = Ext.getCmp("barraPaginacion");
																Ext.Ajax.request({
																	url: '15BloqServConsultaEXT.data.jsp',
																	params: Ext.apply(fp.getForm().getValues(),{
																				informacion: 'GeneraArchivoPDF',
                                        start: barraPaginacionA.cursor,
                                        limit: barraPaginacionA.pageSize
																				}),		
																callback: descargaArchivoPDF
																});
															}
											}]
								}
	})
//-----------------------------Fin Grid Consulta--------------------------------
		
//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id					:'formcon',
		width				:500,
		heigth			:'auto',
		title				:'Bloqueo de Servicio Consulta',
		layout			:'form',
		frame				:true,
		collapsible		:true,
		titleCollapse	:false,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 6px',
		labelWidth		:25,
		monitorValid	:true,	
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosForma
							],
		buttons			:[{
							text				:'Buscar',
							id					:'btnConsultar',
							hidden			:false,
							iconCls			:'icoBuscar',
							formBind			:true,
							handler			:function(boton, evento){
												fp.el.mask('Cargando...', 'x-mask-loading');	
												consultaData.load({
														params		:Ext.apply(fp.getForm().getValues(),{
														informacion	:'Consultar',
														operacion	:'Generar',
														start			:0,
														limit			:15
														})
													});	
												}
							},{
							text				:'Limpiar',
							id					:'btnLimpiar',
							hidden			:false,
							iconCls			:'icoLimpiar',
							formBind			:false,
							handler			:function(boton, evento){
												Ext.getCmp('formcon').getForm().reset();
												Ext.getCmp('gridConsulta').hide();
												}
							}]
	})
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		:949,
		style		:'margin:0 auto;',
		items		:[
					NE.util.getEspaciador(20),
					fp,
					NE.util.getEspaciador(20),
					gridConsulta,
					NE.util.getEspaciador(20)
					]
	})
//-----------------------------Fin Contenedor Principal-------------------------

})//-----------------------------------------------Fin Ext.onReady(function(){}