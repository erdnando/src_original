<%@ page 
	contentType="application/json;charset=UTF-8"
	import="
		java.text.*, 
		java.util.*, 
		java.math.*, 
		netropology.utilerias.*, 
		com.netro.dispersion.*, 
		com.netro.exception.*, 
		com.netro.model.catalogos.CatalogoEPO,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		javax.naming.Context"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String 			noEpo				= (request.getParameter("epo")		== null)?"":request.getParameter("epo");
String			fechaRegIni 	= (request.getParameter("fechaPeriodoMin")		== null)?sdf.format(new java.util.Date()):request.getParameter("fechaPeriodoMin");
String			fechaRegFin 	= (request.getParameter("fechaPeriodoMax")		== null)?sdf.format(new java.util.Date()):request.getParameter("fechaPeriodoMax");
String infoRegresar	= "";

String 	txt_dias1 		= "",
			txt_dias2 		= "",
			txt_dias1USD 	= "",
			txt_dias2USD 	= "";
String mensaje = "";			

log.debug("informacion = <"+informacion+">");

if (informacion.equals("CatalogoEPO")){
	List   		listaElementos = null;
	CatalogoEPO catalogo 		= null;
		catalogo = new CatalogoEPO();
		catalogo.setClave("ic_epo");
		catalogo.setDescripcion("cg_razon_social");
		catalogo.setHabilitado("S"); 
		catalogo.setOrden("2");
		
		listaElementos = catalogo.getListaElementosMonitorDispersionEPO();	
		
		infoRegresar = catalogo.getJSONElementos(listaElementos);
}else if(informacion.equals("TarifaMondedasValidaDATA")){
	
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	
	if(!noEpo.equals("") && dispersion.hasTarifaDispersionMonedaNacional(noEpo)){
	
	}
	if(!noEpo.equals("") && dispersion.hasTarifaDispersionDolaresAmericanos(noEpo)){

	}

	double  txt_tarifa[] = new double[3];
	List lTarifa 		= new ArrayList();
	if(!"".equals(noEpo)) {
		lTarifa = dispersion.getTarifaEpo(noEpo);
		for(int i=0;i<lTarifa.size();i++) {
			List lDatos = (ArrayList)lTarifa.get(i);
			String rs_dia 		= lDatos.get(0).toString();
			double rs_tarifa 	= Double.parseDouble(lDatos.get(1).toString());
			switch(i) {
				case 0:
					txt_dias1		= rs_dia;
					txt_tarifa[0]	= rs_tarifa;
					break;
				case 1:
					txt_dias2	= rs_dia;
					txt_tarifa[1]	= rs_tarifa;
					break;
				case 2:
					txt_tarifa[2]	= rs_tarifa;
					break;
			}//switch(i)
		}//for(int i=0;i<lRegistro.size();i++)
	}
	
	// Obtener Tarifas en Dolares
	double  txt_tarifaUSD[] 	= new double[3];
	List 		lTarifaUSD 			= new ArrayList();
	if(!"".equals(noEpo)) {
		lTarifaUSD = dispersion.getTarifaEpoUSD(noEpo);
		for(int i=0;i<lTarifaUSD.size();i++) {
			List lDatos = (ArrayList)lTarifaUSD.get(i);
			String rs_dia 		= lDatos.get(0).toString();
			double rs_tarifa 	= Double.parseDouble(lDatos.get(1).toString());
			switch(i) {
				case 0:
					txt_dias1USD		= rs_dia;
					txt_tarifaUSD[0]	= rs_tarifa;
					break;
				case 1:
					txt_dias2USD		= rs_dia;
					txt_tarifaUSD[1]	= rs_tarifa;
					break;
				case 2:
					txt_tarifaUSD[2]	= rs_tarifa;
					break;
			}//switch(i)
		}//for(int i=0;i<lRegistro.size();i++)
	}	

			boolean esTarifaEnMonedaNacionalValida 		= ( !noEpo.equals("") && dispersion.hasTarifaDispersionMonedaNacional(noEpo) 		&& lTarifa.size()   == 2)?true:false;
			boolean esTarifaEnDolaresAmericanosValida 	= ( !noEpo.equals("") && dispersion.hasTarifaDispersionDolaresAmericanos(noEpo) 	&& lTarifaUSD.size()== 2)?true:false;
			
			JSONObject jsonObj = new JSONObject();
			
			
			StringBuffer html = new StringBuffer("");
				
			if(esTarifaEnMonedaNacionalValida ||esTarifaEnDolaresAmericanosValida ){
				html.append("<table border=\"0\">");
					html.append("<tr>");
						html.append("<td>Clasificación<br>Días de<br>Vigencia</td>");
						html.append("<td class=\"formas\">");
							html.append("<table border=\"0\">");
								html.append("<tr>");	
								if(esTarifaEnMonedaNacionalValida){
									html.append("<td>&nbsp;</td>");
									html.append("<td class=\"formas\" style=\"font-weight:bold;text-align:center;\">Moneda Nacional</td>");
									html.append("<td width=\"50px\">&nbsp;</td>");
								}
								if(esTarifaEnDolaresAmericanosValida){
									html.append("<td>&nbsp;</td>");
									html.append("<td class=\"formas\" style=\"font-weight:bold;text-align:center;\">D&oacute;lares Americanos</td>");
								}
								html.append("</tr>");	
								html.append("<tr>");	
								if(esTarifaEnMonedaNacionalValida){
									html.append("<td class=\"formas\"><strong>a)</strong></td>");
									html.append("<td class=\"formas\">Hasta&nbsp;"+txt_dias1+"&nbsp;D&iacute;as&nbsp;Inclusive</td>");
									jsonObj.put("txt_dias1",txt_dias1);
									html.append("<td width=\"50px\">&nbsp;</td>");
								}
								if(esTarifaEnDolaresAmericanosValida){
									html.append("<td><string>&nbsp;a)</strong></td>");
									html.append("<td class='formas'>Hasta&nbsp;"+txt_dias1USD+"&nbsp;D&iacute;as&nbsp;Inclusive	</td>");
									jsonObj.put("txt_dias1USD",txt_dias1USD);
								}
								html.append("</tr>");
								html.append("<tr>");	
								if(esTarifaEnMonedaNacionalValida){
									html.append("<td class=\"formas\"><strong>b)</strong></td>");
									html.append("<td class=\"formas\">De los&nbsp;"+(Integer.parseInt(txt_dias1)+1)+"&nbsp;a&nbsp;los&nbsp;"+txt_dias2+"&nbsp;D&iacute;as</td>");
									jsonObj.put("txt_dias2",txt_dias2);
									html.append("<td width=\"50px\">&nbsp;</td>");
								}
								if(esTarifaEnDolaresAmericanosValida){
									html.append("<td class=\"formas\"><strong>b)</strong></td>");
									html.append("<td class=\"formas\">De los&nbsp;"+(Integer.parseInt(txt_dias1USD)+1)+"&nbsp;a&nbsp;los&nbsp;"+txt_dias2USD+"&nbsp;D&iacute;as</td>");
									jsonObj.put("txt_dias2USD",txt_dias2USD);
								}
								html.append("</tr>");
							html.append("</table>");
						html.append("</td>");
					html.append("</tr>");
				html.append("</table>");
			}else{
				mensaje = "La EPO se debe parametrizar. Favor de verificar";
			}
		jsonObj.put("data", html.toString());
		jsonObj.put("mensaje", mensaje.toString());
		jsonObj.put("esTarifaEnMonedaNacionalValida", new Boolean(esTarifaEnMonedaNacionalValida));
		jsonObj.put("esTarifaEnDolaresAmericanosValida",new Boolean(esTarifaEnDolaresAmericanosValida));
		jsonObj.put("success", new Boolean(true));
		
		
	  infoRegresar = jsonObj.toString();
}else if(informacion.equals("Consultar_Detalle") || informacion.equals("Generar_Archivo")){
	
		
	boolean esTarifaEnMonedaNacionalValida = Boolean.valueOf(request.getParameter("esTarifaEnMonedaNacionalValida")).booleanValue(); 
	boolean esTarifaEnDolaresAmericanosValida = Boolean.valueOf(request.getParameter("esTarifaEnDolaresAmericanosValida")).booleanValue(); 
	
	StringBuffer detalle = new StringBuffer();
	StringBuffer detalleMN = new StringBuffer();
	StringBuffer detalleUSD = new StringBuffer();
	String totalMN = "";
	String totalUSD = "";
	String tarifaMN = "";
	String tarifaUSD = "";
	String MontoTotalMN = "";
	String MontoTotalUSD = "";
	 boolean SIN_COMAS = false;
			txt_dias1	= (request.getParameter("txt_dias1")		== null)?"":request.getParameter("txt_dias1");
			txt_dias2	= (request.getParameter("txt_dias2")		== null)?"":request.getParameter("txt_dias2");
			txt_dias1USD	= (request.getParameter("txt_dias1USD")		== null)?"":request.getParameter("txt_dias1USD");
			txt_dias2USD	= (request.getParameter("txt_dias2USD")		== null)?"":request.getParameter("txt_dias2USD");
			String nombreEPO	= (request.getParameter("nombreEPO")		== null)?"":request.getParameter("nombreEPO");
	
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
 
		// Obtener la descripcion de las monedas
		String descripcionMonedaNacional 	= dispersion.getDescripcionMoneda("1");
		String descripcionDolaresAmericanos = dispersion.getDescripcionMoneda("54");
		
		Hashtable hDatosFactura 		= esTarifaEnMonedaNacionalValida?dispersion.getOperacionesDispersadasEpos(noEpo, fechaRegIni, fechaRegFin, String.valueOf(Integer.parseInt(txt_dias1)+1), String.valueOf(Integer.parseInt(txt_dias2))):new Hashtable();
		// Moneda Nacional
		Vector vOperyOperPag 			= (Vector)hDatosFactura.get("vOperyOperPag");
		Vector vVencSinOperSumRes 		= (Vector)hDatosFactura.get("vVencSinOperSumRes");
		
		vOperyOperPag						= vOperyOperPag 		!= null?vOperyOperPag		:new Vector();
		vVencSinOperSumRes				= vVencSinOperSumRes != null?vVencSinOperSumRes	:new Vector();
		
		Hashtable hDatosFacturaUSD 	= esTarifaEnDolaresAmericanosValida?dispersion.getOperacionesDispersadasEposUSD(noEpo, fechaRegIni, fechaRegFin, String.valueOf(Integer.parseInt(txt_dias1USD)+1), String.valueOf(Integer.parseInt(txt_dias2USD))):new Hashtable();
		// Dolares Americanos
		Vector vOperyOperPagUSD 		= (Vector)hDatosFacturaUSD.get("vOperyOperPagUSD");
		Vector vVencSinOperSumResUSD 	= (Vector)hDatosFacturaUSD.get("vVencSinOperSumResUSD");
		
		vOperyOperPagUSD 					= vOperyOperPagUSD 		!= null?vOperyOperPagUSD		:new Vector();
		vVencSinOperSumResUSD 			= vVencSinOperSumResUSD != null?vVencSinOperSumResUSD	:new Vector();
	
		double  txt_tarifa[] = new double[3];
		List lTarifa 		= new ArrayList();
	if(!"".equals(noEpo)) {
		lTarifa = dispersion.getTarifaEpo(noEpo);
		for(int i=0;i<lTarifa.size();i++) {
			List lDatos = (ArrayList)lTarifa.get(i);
			String rs_dia 		= lDatos.get(0).toString();
			double rs_tarifa 	= Double.parseDouble(lDatos.get(1).toString());
			switch(i) {
				case 0:
					txt_dias1		= rs_dia;
					txt_tarifa[0]	= rs_tarifa;
					break;
				case 1:
					txt_dias2	= rs_dia;
					txt_tarifa[1]	= rs_tarifa;
					break;
				case 2:
					txt_tarifa[2]	= rs_tarifa;
					break;
			}//switch(i)
		}//for(int i=0;i<lRegistro.size();i++)
	}
	// Obtener Tarifas en Dolares
	double  txt_tarifaUSD[] 	= new double[3];
	List 		lTarifaUSD 			= new ArrayList();
	if(!"".equals(noEpo)) {
		lTarifaUSD = dispersion.getTarifaEpoUSD(noEpo);
		for(int i=0;i<lTarifaUSD.size();i++) {
			List lDatos = (ArrayList)lTarifaUSD.get(i);
			String rs_dia 		= lDatos.get(0).toString();
			double rs_tarifa 	= Double.parseDouble(lDatos.get(1).toString());
			switch(i) {
				case 0:
					txt_dias1USD		= rs_dia;
					txt_tarifaUSD[0]	= rs_tarifa;
					break;
				case 1:
					txt_dias2USD		= rs_dia;
					txt_tarifaUSD[1]	= rs_tarifa;
					break;
				case 2:
					txt_tarifaUSD[2]	= rs_tarifa;
					break;
			}//switch(i)
		}//for(int i=0;i<lRegistro.size();i++)
	}
		
		JSONObject jsonObj = new JSONObject();
		
		StringBuffer html = new StringBuffer("");
		HashMap  reg 				= null;
		String 	rowSpan 					= "1";
		JSONArray 	cifrasGrid1DataArray 	= new JSONArray();
		JSONArray 	cifrasGridDetalleMNDataArray 	= new JSONArray();
		JSONArray 	cifrasGridDetalleMNTotalesDataArray 	= new JSONArray();
		JSONArray 	cifrasGridDetalleUSDDataArray 	= new JSONArray();
		JSONArray 	cifrasGridDetalleUSDTotalesDataArray 	= new JSONArray();
		JSONArray	data						= null;
		if(vOperyOperPag.size()>0 || vVencSinOperSumRes.size()>0 || vOperyOperPagUSD.size()>0 || vVencSinOperSumResUSD.size()>0 ) {
			
			jsonObj.put("Fecha_emision",sdf.format(new java.util.Date()));
			jsonObj.put("nombreEPO",nombreEPO);
			
			boolean hayDocumentosOperadosMN 	= ( (vOperyOperPag		!= null) && (vOperyOperPag.size()		>0) )?true:false;
			boolean hayDocumentosOperadosUSD	= ( (vOperyOperPagUSD	!= null) && (vOperyOperPagUSD.size()	>0) )?true:false;
			
			ArrayList listaDocumentosOperados = ordenaRegistros(hayDocumentosOperadosMN?vOperyOperPag:null,hayDocumentosOperadosUSD?vOperyOperPagUSD:null);
			
			int iTotOper 				= 0;//Numador de numero MN
			BigDecimal bMtoOper 		= new BigDecimal(0);
			
			int iTotOperUSD 			= 0;
			BigDecimal bMtoOperUSD 	= new BigDecimal(0);
			if(listaDocumentosOperados.size()>0){
				for(int i=0;i<listaDocumentosOperados.size();i++){
					reg = (HashMap) listaDocumentosOperados.get(i);
					String hayDocumentosMN 		= (String) reg.get("HAY_DOCUMENTOS_MN");
					String hayDocumentosUSD 	= (String) reg.get("HAY_DOCUMENTOS_USD");
					String nombre_intermediario = (String) reg.get("INTERMEDIARIO");
					String monto_mn 				= (String) reg.get("MONTO_MN");
					String numero_mn 				= (String) reg.get("NUMERO_MN");
					String monto_usd				= (String) reg.get("MONTO_USD");		
					String numero_usd 			= (String) reg.get("NUMERO_USD");
//------------------------------------------------------------------------------											
					rowSpan 					= (hayDocumentosMN.equals("true") && hayDocumentosUSD.equals("true"))?"2":"1";
			
					if(hayDocumentosMN.equals("true")){
						iTotOper += new Integer(numero_mn).intValue();	
						bMtoOper = 	bMtoOper.add(new BigDecimal(monto_mn));
						data = new JSONArray();
						data.add(nombre_intermediario);	
						data.add(descripcionMonedaNacional);
						data.add(numero_mn);
						data.add("$"+Comunes.formatoDecimal(monto_mn,2));
						cifrasGrid1DataArray.add(data);
//------------------------------------------------------------------------------						
//------------------------------------------------------------------------------						
						detalle.append("\""+nombre_intermediario+"\";");
						detalle.append("\""+descripcionMonedaNacional+"\";");
						detalle.append("\""+numero_mn+"\";");
						detalle.append("\""+monto_mn+"\"");
						detalle.append("\r\n");
//------------------------------------------------------------------------------												
						
					}
					if(hayDocumentosUSD.equals("true")){
						iTotOperUSD += new Integer(numero_usd).intValue();
						bMtoOperUSD = 	bMtoOperUSD.add(new BigDecimal(monto_usd));
						data = new JSONArray();
						data.add(nombre_intermediario);	
						data.add(descripcionDolaresAmericanos);
						data.add(numero_usd);
						data.add("$"+Comunes.formatoDecimal(monto_usd,2));
						cifrasGrid1DataArray.add(data);
//------------------------------------------------------------------------------						
//------------------------------------------------------------------------------						
						if(!rowSpan.equals("2")){
							detalle.append("\""+((hayDocumentosMN.equals("false") && hayDocumentosUSD.equals("true"))?nombre_intermediario:"")+"\";");
						}else{
							detalle.append(";");
						}
						detalle.append("\""+descripcionDolaresAmericanos+"\";");
						detalle.append("\""+numero_usd+"\";");
						detalle.append("\""+monto_usd+"\"");
						detalle.append("\r\n");
					}
//------------------------------------------------------------------------------					
				}
				//Filas de los totales
				data = new JSONArray();
				data.add("Total M.N:");
				data.add(" ");
				data.add(""+iTotOper);
				data.add("$"+Comunes.formatoDecimal(bMtoOper.toPlainString(),2));
				cifrasGrid1DataArray.add(data);
				data = new JSONArray();
				data.add("Total USD:");
				data.add(" ");
				data.add(""+iTotOperUSD);
				data.add("$"+Comunes.formatoDecimal(bMtoOperUSD.toPlainString(),2));
				cifrasGrid1DataArray.add(data);
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------						
				//Para generar el archivo
				if(hayDocumentosOperadosMN){
					detalle.append("\"Total M.N.:\";");
					detalle.append(";");
					detalle.append("\""+iTotOper+"\";");
					detalle.append("\""+bMtoOper.toPlainString()+"\"");
					detalle.append("\r\n");
				}
				if(hayDocumentosOperadosUSD){
					detalle.append("\"Total USD:\";");
					detalle.append(";");
					detalle.append("\""+iTotOperUSD+"\";");
					detalle.append("\""+bMtoOperUSD.toPlainString()+"\"");
					detalle.append("\r\n");
				}
				
//------------------------------------------------------------------------------
			}else{
				//No hay datos para el grid 1
				if(!hayDocumentosOperadosMN && !hayDocumentosOperadosUSD){
					detalle.append("\"No se encontraron registros.\";");
					detalle.append("\r\n");
				}
				
			}						
			//Detalle Documentos no Operados M.N. por periodo de Vigencia*
			
			if(esTarifaEnMonedaNacionalValida){
			
				String sFechaVenc = "";
				int iNoPymePV1=0, iNoPymePV2=0, iNoPymePV3=0, iTotPymePV1 = 0, iTotPymePV2 = 0, iTotPymePV3 = 0, iSumTotPymes = 0;
				BigDecimal bMtoPV1 = new BigDecimal(0);	BigDecimal bMtoTotPV1 = new BigDecimal(0);
				BigDecimal bMtoPV2 = new BigDecimal(0);	BigDecimal bMtoTotPV2 = new BigDecimal(0);
				BigDecimal bMtoPV3 = new BigDecimal(0);	BigDecimal bMtoTotPV3 = new BigDecimal(0);
				BigDecimal bSumTotMtoH = new BigDecimal("0");
				
			
				if(vVencSinOperSumRes.size()>0) {
					for (int i=0; i<vVencSinOperSumRes.size(); i++) {
						Vector vVencSinOperSum = (Vector)vVencSinOperSumRes.get(i);
						sFechaVenc = vVencSinOperSum.get(0).toString();	
						data = new JSONArray();
						data.add(sFechaVenc);
						
						iNoPymePV1 = iNoPymePV2 = iNoPymePV3 = 0;
						bMtoPV1 = new BigDecimal(0);
						bMtoPV2 = new BigDecimal(0);
						bMtoPV3 = new BigDecimal(0);
						for(int n=1; n<vVencSinOperSum.size(); n++) {
							int iGrupo = Integer.parseInt(((Vector)vVencSinOperSum.get(n)).get(2).toString());
							if(iGrupo==1) {
								iNoPymePV1 = Integer.parseInt(((Vector)vVencSinOperSum.get(n)).get(0).toString());
								bMtoPV1 = new BigDecimal(((Vector)vVencSinOperSum.get(n)).get(1).toString());
								iTotPymePV1 += iNoPymePV1;
								bMtoTotPV1 = bMtoTotPV1.add(bMtoPV1);
							}
							if(iGrupo==2) {
								iNoPymePV2 = Integer.parseInt(((Vector)vVencSinOperSum.get(n)).get(0).toString());
								bMtoPV2 = new BigDecimal(((Vector)vVencSinOperSum.get(n)).get(1).toString());
								iTotPymePV2 += iNoPymePV2;
								bMtoTotPV2 = bMtoTotPV2.add(bMtoPV2);
							}							
						}
						data.add( new Integer(iNoPymePV1));
						data.add(Comunes.formatoMN(bMtoPV1.toPlainString()));
						data.add(new Integer(iNoPymePV2));
						data.add(Comunes.formatoMN(bMtoPV2.toPlainString()));
						
						int iTotPymes = iNoPymePV1 + iNoPymePV2;// + iNoPymePV3;
						BigDecimal bSumTotMtoV = new BigDecimal("0");
						bSumTotMtoV = bSumTotMtoV.add(bMtoPV1).add(bMtoPV2);//.add(bMtoPV3);
						
						data.add(new Integer(iTotPymes));
						data.add(Comunes.formatoMN(bSumTotMtoV.toPlainString()));
						cifrasGridDetalleMNDataArray.add(data);
//------------------------------------------------------------------------------												
//------------------------------------------------------------------------------						
						detalleMN.append(sFechaVenc+","+iNoPymePV1+","+Comunes.formatoDecimal(bMtoPV1.toPlainString(),2,SIN_COMAS)+","+iNoPymePV2+","+Comunes.formatoDecimal(bMtoPV2.toPlainString(),2,SIN_COMAS)+","+iTotPymes+","+Comunes.formatoDecimal(bSumTotMtoV.toPlainString(),2,SIN_COMAS)+"\n");
						

//------------------------------------------------------------------------------						
					}
					//Totales
					data = new JSONArray();
					data.add("<div >Total:</div>");
					data.add(new Integer(iTotPymePV1));
					data.add("<div align=\"right\">"+Comunes.formatoMN(bMtoTotPV1.toPlainString())+"</div>");
					data.add(new Integer(iTotPymePV2));
					data.add("<div align=\"right\">"+Comunes.formatoMN(bMtoTotPV2.toPlainString())+"</div>");
					
					iSumTotPymes = iTotPymePV1 + iTotPymePV2;// + iTotPymePV3;
					bSumTotMtoH = bSumTotMtoH.add(bMtoTotPV1).add(bMtoTotPV2);//.add(bMtoTotPV3);	
					
					data.add(new Integer(iSumTotPymes));
					//data.add("$"+Comunes.formatoMN());
					data.add("<div align=\"right\">"+Comunes.formatoMN(bSumTotMtoH.toPlainString())+"&nbsp;&nbsp;&nbsp;</div>");
					cifrasGridDetalleMNTotalesDataArray.add(data);
					
					//Total Tarifa
					txt_tarifa[0] *= iTotPymePV1;
					txt_tarifa[1] *= iTotPymePV2;
					txt_tarifa[2] *= iTotPymePV3;
					
					data = new JSONArray();
					data.add("<div >&nbsp;Tarifa:</div>");
					data.add("<div align=\"right\">$"+Comunes.formatoDecimal(txt_tarifa[0], 2)+"</div>");
					data.add("<div >&nbsp;</div>");
					data.add("<div align=\"right\">$"+Comunes.formatoDecimal(txt_tarifa[1], 2)+"</div>");
					data.add("<div >&nbsp;</div>");
					data.add("<div >&nbsp;</div>");
					data.add("<div >&nbsp;</div>");
					
					cifrasGridDetalleMNTotalesDataArray.add(data);
					
					//Monto Total de Comisión:
					double		valorIva1							= 0.0;
					double		valorIvaT1							= 0.0;
					
					String	_mes1 			= (request.getParameter("_mes")			== null)?Integer.toString(Fecha.getMesActual()):request.getParameter("_mes").trim();
					String	_anio1 		= (request.getParameter("_anio")			== null)?Integer.toString(Fecha.getAnioActual()):request.getParameter("_anio").trim();
					 String fecha1   = "01/"+ _mes1 + "/" + _anio1; // formato fecha: DD/MM/YYYY
					valorIva1 = ((BigDecimal)Iva.getValor(fecha1)).doubleValue();   
					valorIvaT1 = 1 + ((BigDecimal)Iva.getValor(fecha1)).doubleValue();   
					
					data = new JSONArray();
					data.add("<div >&nbsp;Monto Total de Comisión:</div>");
					data.add("<div align=\"right\">$"+Comunes.formatoDecimal((txt_tarifa[0]+txt_tarifa[1]+txt_tarifa[2])*valorIvaT1, 2)+"</div>");
					data.add("<div >&nbsp;</div>");
					data.add("<div >&nbsp;</div>");
					data.add("<div >&nbsp;</div>");
					data.add("<div >&nbsp;</div>");
					data.add("<div >&nbsp;</div>");
					cifrasGridDetalleMNTotalesDataArray.add(data);
//------------------------------------------------------------------------------						
//------------------------------------------------------------------------------						
					detalleMN.append(" , , , , , , \r\n");
					detalleMN.append("Total:,"+iTotPymePV1+","+Comunes.formatoDecimal(bMtoTotPV1.toPlainString(),2,SIN_COMAS)+","+iTotPymePV2+","+Comunes.formatoDecimal(bMtoTotPV2.toPlainString(),2,SIN_COMAS)+","+iSumTotPymes+","+Comunes.formatoDecimal(bSumTotMtoH.toPlainString(),2,SIN_COMAS)+"\r\n");
					detalleMN.append("Tarifa:,"+txt_tarifa[0]+",,"+txt_tarifa[1]+",,, \r\n");
					detalleMN.append("Monto Total de Comisión:,"+Comunes.formatoDecimal(String.valueOf((txt_tarifa[0]+txt_tarifa[1]+txt_tarifa[2])* valorIvaT1),2,SIN_COMAS)+", , , , , \r\n");
//------------------------------------------------------------------------------											
				}else{
					//no se encontraron registros MN
					detalleMN.append("\"No se encontraron registros.\"");
					detalleMN.append("\r\n");
				}
			}
			
			
			//***************** Inicia  Moneda Dolar *****************************
			
			if(esTarifaEnDolaresAmericanosValida){ 
			  
				String sFechaVenc = "";
				int iNoPymePV1=0, iNoPymePV2=0, iNoPymePV3=0, iTotPymePV1 = 0, iTotPymePV2 = 0, iTotPymePV3 = 0, iSumTotPymes = 0;
				BigDecimal bMtoPV1 = new BigDecimal(0);	BigDecimal bMtoTotPV1 = new BigDecimal(0);
				BigDecimal bMtoPV2 = new BigDecimal(0);	BigDecimal bMtoTotPV2 = new BigDecimal(0);
				BigDecimal bMtoPV3 = new BigDecimal(0);	BigDecimal bMtoTotPV3 = new BigDecimal(0);
				BigDecimal bSumTotMtoH = new BigDecimal("0");
				
				if(vVencSinOperSumResUSD.size()>0) {
					for (int i=0; i<vVencSinOperSumResUSD.size(); i++) {
						Vector vVencSinOperSum = (Vector)vVencSinOperSumResUSD.get(i);
						sFechaVenc = vVencSinOperSum.get(0).toString();	
						data = new JSONArray();
						data.add(sFechaVenc);
						
						iNoPymePV1 = iNoPymePV2 = iNoPymePV3 = 0;
						bMtoPV1 = new BigDecimal(0);
						bMtoPV2 = new BigDecimal(0);
						bMtoPV3 = new BigDecimal(0);
													
						for(int n=1; n<vVencSinOperSum.size(); n++) {
						
							int iGrupo = Integer.parseInt(((Vector)vVencSinOperSum.get(n)).get(2).toString());
							
							if(iGrupo==1) {
								iNoPymePV1 = Integer.parseInt(((Vector)vVencSinOperSum.get(n)).get(0).toString());
								bMtoPV1 = new BigDecimal(((Vector)vVencSinOperSum.get(n)).get(1).toString());
								iTotPymePV1 += iNoPymePV1;
								bMtoTotPV1 = bMtoTotPV1.add(bMtoPV1);
							}
							if(iGrupo==2) {
								iNoPymePV2 = Integer.parseInt(((Vector)vVencSinOperSum.get(n)).get(0).toString());
								bMtoPV2 = new BigDecimal(((Vector)vVencSinOperSum.get(n)).get(1).toString());
								iTotPymePV2 += iNoPymePV2;
								bMtoTotPV2 = bMtoTotPV2.add(bMtoPV2);
							}						
						}
											
						data.add( new Integer(iNoPymePV1));
						data.add(Comunes.formatoMN(bMtoPV1.toPlainString()));
						data.add(new Integer(iNoPymePV2));
						data.add(Comunes.formatoMN(bMtoPV2.toPlainString()));
						
						int iTotPymes = iNoPymePV1 + iNoPymePV2;
						BigDecimal bSumTotMtoV = new BigDecimal("0");
						bSumTotMtoV = bSumTotMtoV.add(bMtoPV1).add(bMtoPV2);
						
						data.add(new Integer(iTotPymes));
						data.add(Comunes.formatoMN(bSumTotMtoV.toPlainString()));
						cifrasGridDetalleUSDDataArray.add(data);
//------------------------------------------------------------------------------						
//------------------------------------------------------------------------------						
				

						detalleUSD.append(sFechaVenc+","+iNoPymePV1+","+Comunes.formatoDecimal(bMtoPV1.toPlainString().replaceAll("-",""),2,SIN_COMAS)+","+iNoPymePV2+","+Comunes.formatoDecimal(bMtoPV2.toPlainString().replaceAll("-",""),2,SIN_COMAS)+","+iTotPymes+","+Comunes.formatoDecimal(bSumTotMtoV.toPlainString().replaceAll("-",""),2,SIN_COMAS)+"\r\n");
						
//------------------------------------------------------------------------------												
					}
					//Totales Moneda nacional 
					data = new JSONArray();
					data.add("<div>Total:</div>");
					data.add(new Integer(iTotPymePV1));
					data.add("<div align=\"right\">"+Comunes.formatoMN(bMtoTotPV1.toPlainString())+"</div>");
					data.add(new Integer(iTotPymePV2));
					data.add("<div align=\"right\">"+Comunes.formatoMN(bMtoTotPV2.toPlainString())+"</div>");
					
					iSumTotPymes = iTotPymePV1 + iTotPymePV2;// + iTotPymePV3;
					bSumTotMtoH = bSumTotMtoH.add(bMtoTotPV1).add(bMtoTotPV2);//.add(bMtoTotPV3);	
					
					data.add(new Integer(iSumTotPymes));
					data.add(Comunes.formatoMN(bSumTotMtoH.toPlainString()));
					cifrasGridDetalleUSDTotalesDataArray.add(data);
					
					
					//*************++ Totales Dolares ***********+
					
					totalUSD = "Total:,"+iTotPymePV1+","+Comunes.formatoDecimal(bMtoTotPV1.toPlainString(),2,SIN_COMAS)+","+iTotPymePV2+","+Comunes.formatoDecimal(bMtoTotPV2.toPlainString(),2,SIN_COMAS)+","+iTotPymePV3+","+Comunes.formatoDecimal(bMtoTotPV3.toPlainString(),2,SIN_COMAS)+","+iSumTotPymes+","+Comunes.formatoDecimal(bSumTotMtoH.toPlainString(),2,SIN_COMAS)+"\n";
					//Total Tarifa
					txt_tarifaUSD[0] *= iTotPymePV1;
					txt_tarifaUSD[1] *= iTotPymePV2;
					//txt_tarifaUSD[2] *= iTotPymePV3;
					
					data = new JSONArray();
					data.add("<div>&nbsp;Tarifa:</div>");
					data.add("<div align=\"right\">"+Comunes.formatoDecimal(txt_tarifaUSD[0], 2)+"</div>");
					data.add("<div>&nbsp;</div>");
					data.add("<div align=\"right\">"+Comunes.formatoDecimal(txt_tarifaUSD[1], 2)+"</div>");
					data.add("<div>&nbsp;</div>");
					data.add("<div>&nbsp;</div>");
					data.add("<div>&nbsp;</div>");
					
					cifrasGridDetalleUSDTotalesDataArray.add(data);
					tarifaUSD = "Tarifa:,"+txt_tarifaUSD[0]+",,"+txt_tarifaUSD[1]+",,\n";
					//Monto Total de Comisión:
					double		valorIva1							= 0.0;
					double		valorIvaT1							= 0.0;
					
					String	_mes1 			= (request.getParameter("_mes")			== null)?Integer.toString(Fecha.getMesActual()):request.getParameter("_mes").trim();
					String	_anio1 		= (request.getParameter("_anio")			== null)?Integer.toString(Fecha.getAnioActual()):request.getParameter("_anio").trim();
					 String fecha1   = "01/"+ _mes1 + "/" + _anio1; // formato fecha: DD/MM/YYYY
					valorIva1 = ((BigDecimal)Iva.getValor(fecha1)).doubleValue();   
					valorIvaT1 = 1 + ((BigDecimal)Iva.getValor(fecha1)).doubleValue();   
					
					data = new JSONArray();
					data.add("<div>&nbsp;Monto Total de Comisión:</div>"); 
					data.add("<div align=\"right\">$"+Comunes.formatoDecimal((txt_tarifaUSD[0]+txt_tarifaUSD[1]+txt_tarifaUSD[2])*valorIvaT1, 2)+"</div>");
					data.add("<div>&nbsp;</div>");
					data.add("<div>&nbsp;</div>");
					data.add("<div>&nbsp;</div>");
					data.add("<div>&nbsp;</div>");
					data.add("<div>&nbsp;</div>");
					cifrasGridDetalleUSDTotalesDataArray.add(data);
									
					
					//para generar el archivo csv				
					MontoTotalUSD = "Monto Total de Comisión:"+","+Comunes.formatoDecimal((String.valueOf((txt_tarifaUSD[0]+txt_tarifaUSD[1]+txt_tarifaUSD[2])* valorIvaT1)),2,SIN_COMAS)+"\r\n";
//------------------------------------------------------------------------------						
//------------------------------------------------------------------------------						
					detalleUSD.append(" , , , , , , \r\n");
					detalleUSD.append("Total:,"+iTotPymePV1+","+Comunes.formatoDecimal(bMtoTotPV1.toPlainString().replaceAll("-",""),2,SIN_COMAS)+","+iTotPymePV2+","+Comunes.formatoDecimal(bMtoTotPV2.toPlainString().replaceAll("-",""),2,SIN_COMAS)+","+iSumTotPymes+","+Comunes.formatoDecimal(bSumTotMtoH.toPlainString().replaceAll("-",""),2,SIN_COMAS)+", \r\n");
					detalleUSD.append("Tarifa:,"+txt_tarifaUSD[0]+",,"+txt_tarifaUSD[1]+",,, \r\n");
					detalleUSD.append("Monto Total de Comisión:,"+Comunes.formatoDecimal(String.valueOf((txt_tarifaUSD[0]+txt_tarifaUSD[1]+txt_tarifaUSD[2])* valorIvaT1),2,SIN_COMAS)+", , , , , \r\n");
//-----------

					

//------------------------------------------------------------------------------						
				}else{
					//no se encontraron registros UDS
					detalleUSD.append("\"No se encontraron registros.\"");
					detalleUSD.append("\r\n");
				}
			}
			
		}else{
			html.append("<td class=\"celda01\" align=\"center\" colspan=\"5\">No se encontraron documentos operados y no operados dispersados.</td>");// "No se encontrar&oacute;n documentos operados y no operados dispersados.";
		}
		if(informacion.equals("Consultar_Detalle")){
			jsonObj.put("esTarifaEnMonedaNacionalValida", new Boolean(esTarifaEnMonedaNacionalValida));
			jsonObj.put("esTarifaEnDolaresAmericanosValida", new Boolean(esTarifaEnDolaresAmericanosValida));
			jsonObj.put("cifrasGrid1DataArray",cifrasGrid1DataArray);
			jsonObj.put("cifrasGridDetalleMNDataArray",cifrasGridDetalleMNDataArray);
			jsonObj.put("cifrasGridDetalleMNTotalesDataArray",cifrasGridDetalleMNTotalesDataArray);
			jsonObj.put("cifrasGridDetalleUSDDataArray",cifrasGridDetalleUSDDataArray);
			jsonObj.put("cifrasGridDetalleUSDTotalesDataArray",cifrasGridDetalleUSDTotalesDataArray);
			jsonObj.put("data",html.toString());
			jsonObj.put("textoHeaderMN_1","Hasta "+txt_dias1+" Días Inclusive");
			jsonObj.put("textoHeaderMN_2","De los "+(Integer.parseInt(txt_dias1)+1)+" a los "+txt_dias2+" Días");
			jsonObj.put("strNombreUsuario",strNombreUsuario);
			jsonObj.put("success", new Boolean(true));
		}
		if(informacion.equals("Generar_Archivo")){
			String tipo	= (request.getParameter("tipo")		== null)?"CSV":request.getParameter("tipo");
			ConsultaDispersionFactura clase = new ConsultaDispersionFactura();
			clase.setClaveEpo(noEpo);
			clase.setNombreEPO(nombreEPO);
			clase.setFechaRegIni(fechaRegIni);
			clase.setFechaRegFin(fechaRegFin);
			String nombreArchivo = clase.generarArchivo(strDirectorioTemp,strNombreUsuario,txt_dias1,txt_dias2,txt_dias1USD,txt_dias2USD,
											esTarifaEnMonedaNacionalValida, esTarifaEnDolaresAmericanosValida,detalle.toString(),detalleMN.toString(),detalleUSD.toString(),tipo, request);
			
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		}
		
		//jsonObj.put("mensaje",mensaje);
		infoRegresar = jsonObj.toString();
}else if(informacion.equals("Generar_Archivo_Detalle")){
	
	ConsultaDispersionFactura clase = new ConsultaDispersionFactura();
	clase.setClaveEpo(noEpo);
	clase.setFechaRegIni(fechaRegIni);
	clase.setFechaRegFin(fechaRegFin);
	String nombreArchivo =clase.generarArchivoDetalle(strDirectorioTemp);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();
	
} else {
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
}
 
%>
<%=infoRegresar%>

<%!
	private ArrayList ordenaRegistros(Vector registrosMN, Vector registrosUSD){
		
		ArrayList lista = new ArrayList();
		boolean hayRegistrosMN 	= (registrosMN 	== null)?false:true;
		boolean hayRegistrosUSD = (registrosUSD 	== null)?false:true;

		HashMap documentosOperadosMN 	= new HashMap();
		HashMap documentosOperadosUSD = new HashMap();
		
		// Crear arreglo con lista de los proveedores
		TreeSet intermediarios = new TreeSet();
		
		if(hayRegistrosMN){
			for(int i=0;i<registrosMN.size();i++){
				Vector registro = (Vector) registrosMN.get(i);
				intermediarios.add((String) registro.get(0));
			}
		}
		if(hayRegistrosUSD){
			for(int i=0;i<registrosUSD.size();i++){
				Vector registro = (Vector) registrosUSD.get(i);
				intermediarios.add((String) registro.get(0));
			}
		}
		
		// Construir HashMap de Registros de Moneda  Nacional
		if(hayRegistrosMN){
			for(int i=0;i<registrosMN.size();i++){
				Vector registro = (Vector) registrosMN.get(i);
				String nombre_intermediario 	= (String) registro.get(0);
				String numero 						= (String) registro.get(1);
				String monto						= (String) registro.get(2);
				
				HashMap registroDocto = new HashMap();
				registroDocto.put("NUMERO",numero);
				registroDocto.put("MONTO",monto);
				
				documentosOperadosMN.put(nombre_intermediario,registroDocto);
			}
		}
		// Construir HashMap de Registros de Dolares Americanos
		if(hayRegistrosUSD){
			for(int i=0;i<registrosUSD.size();i++){
				Vector registro = (Vector) registrosUSD.get(i);
				String nombre_intermediario 	= (String) registro.get(0);
				String numero 						= (String) registro.get(1);
				String monto						= (String) registro.get(2);
				
				HashMap registroDocto = new HashMap();
				registroDocto.put("NUMERO",numero);
				registroDocto.put("MONTO",	monto);
				
				documentosOperadosUSD.put(nombre_intermediario,registroDocto);
			}
		}
		
		// Construir list ordenada con los HashMaps anteriores
		Iterator iterator 				= intermediarios.iterator();
		String 	nombre_intermediario	= "";
		HashMap 	registro					= null;
		String   monto_mn 				= "";
		String   numero_mn 				= "";
		String 	monto_usd 				= "";
		String   numero_usd 				= "";
		HashMap  montos					= null;
		String   hayDocumentosMN		= "";
		String   hayDocumentosUSD		= "";
		while (iterator.hasNext()){
			nombre_intermediario = (String) iterator.next();

			monto_mn 			= null;
			numero_mn 			= null;
			hayDocumentosMN 	= "false";
			if(hayRegistrosMN){
				montos 		= (HashMap) documentosOperadosMN.get(nombre_intermediario);
				if(montos != null){
					hayDocumentosMN = "true";
					monto_mn 	= (String) montos.get("MONTO");
					numero_mn 	= (String) montos.get("NUMERO");
				}
			}
			
			monto_usd 			= null;
			numero_usd 			= null;
			hayDocumentosUSD	= "false";
			if(hayRegistrosUSD){
				montos 		= (HashMap) documentosOperadosUSD.get(nombre_intermediario);
				if(montos != null){
					hayDocumentosUSD	= "true";
					monto_usd 			= (String) montos.get("MONTO");
					numero_usd 			= (String) montos.get("NUMERO");
				}
			}
			
			registro 	= new HashMap();
			registro.put("INTERMEDIARIO",			nombre_intermediario);
			registro.put("MONTO_MN",				monto_mn);
			registro.put("NUMERO_MN",				numero_mn);
			registro.put("HAY_DOCUMENTOS_MN",	hayDocumentosMN);
			registro.put("MONTO_USD",				monto_usd);
			registro.put("NUMERO_USD",				numero_usd);
			registro.put("HAY_DOCUMENTOS_USD",	hayDocumentosUSD);
			
			lista.add(registro);
		}
			
		return lista;
	}	
%>