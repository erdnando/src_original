<%@ page 
	contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoIFMonitorDispersion,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("MonitorFlujoFondos.inicializacion") )	{
	
	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
 
	String 		fechaHoy 				= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String 		fechaRegistroInicial	= fechaHoy;
	String 		fechaRegistroFinal	= fechaHoy;
	
	resultado.put("fechaHoy",					fechaHoy					);
	resultado.put("fechaRegistroInicial",	fechaRegistroInicial	);
	resultado.put("fechaRegistroFinal",		fechaRegistroFinal	);
	
	// Determinar el estado siguiente
	estadoSiguiente = "INICIALIZAR_FORMA_CONSULTA";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if ( informacion.equals("MonitorFlujoFondos.inicializarFormaConsulta") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;

	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
 
} else if ( informacion.equals("MonitorFlujoFondos.reprocesarOperaciones") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;

	String[] 	reprocesar 			= (request.getParameterValues("reprocesar") == null)?new String[0]:request.getParameterValues("reprocesar");
 
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {

		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		
	}catch(Exception e){
 
		msg = "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
		
	}
	
	// 2. Reprocesar Operaciones de Flujo Fondos
	boolean 	actualizarResumenOperaciones = false;
	try {
		
		dispersion.reprocesaOperacionFF(reprocesar); 
		actualizarResumenOperaciones = true;
		resultado.put("mensaje","Las operaciones seleccionadas han sido reprocesadas."); 
 
	} catch(NafinException ne) {

		// Nota: aunque nunca entra aquí, se deja por compatibilidad con la versión original de reprocesaOperacionFF.
		actualizarResumenOperaciones = false;
 
		log.error(ne.getMsgError());
		ne.printStackTrace();
		
		resultado.put("mensaje", ne.getMsgError());
		
	} catch(Exception e){
		
		actualizarResumenOperaciones = false;
		
		log.error(e.getMessage());
		e.printStackTrace();
		
		throw new AppException("Ocurrió un error al reprocesar las operaciones: " + e.getMessage() );
		
	}
 
	// Determinar el estado siguiente
	estadoSiguiente 	= "MOSTRAR_RESULTADO_OPERACIONES";
		
	// Enviar resultado de la operacion
	resultado.put("success", 								new Boolean(success)								);
	resultado.put("estadoSiguiente", 					estadoSiguiente 									);
	resultado.put("msg",										msg													);
	resultado.put("actualizarResumenOperaciones",	new Boolean(actualizarResumenOperaciones)	);
	infoRegresar = resultado.toString();
 
} else if ( informacion.equals("MonitorFlujoFondos.cancelarOperaciones") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;

	String[] 	cancelar 			= (request.getParameterValues("cancelar") == null)?new String[0]:request.getParameterValues("cancelar");

	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {

		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		
	}catch(Exception e){
 
		msg = "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
		
	}
	
	// Obtener Fecha de Cancelacion
	String fechaCancelacion = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	
	// Obtener Nombre del Usuario
	String nombreUsuario 	= strNombreUsuario;
	
	// 2. Reprocesar Operaciones de Flujo Fondos
	boolean 	actualizarResumenOperaciones = false;
	try {
		
		dispersion.cancelarOperacionFF( cancelar, nombreUsuario, fechaCancelacion );
		actualizarResumenOperaciones = true;
		resultado.put("mensaje","Las operaciones seleccionadas han sido canceladas."); 
 
	} catch(NafinException ne) {

		// Nota: aunque nunca entra aquí, se deja por compatibilidad con la versión original de reprocesaOperacionFF.
		actualizarResumenOperaciones = false;
 
		log.error(ne.getMsgError());
		ne.printStackTrace();
		
		resultado.put("mensaje", ne.getMsgError());
		
	} catch(Exception e){
		
		actualizarResumenOperaciones = false;
		
		log.error(e.getMessage());
		e.printStackTrace();
		
		throw new AppException("Ocurrió un error al cancelar las operaciones: " + e.getMessage() );
		
	}
 
	// Determinar el estado siguiente
	estadoSiguiente 	= "MOSTRAR_RESULTADO_OPERACIONES";
		
	// Enviar resultado de la operacion
	resultado.put("success", 								new Boolean(success)								);
	resultado.put("estadoSiguiente", 					estadoSiguiente 									);
	resultado.put("msg",										msg													);
	resultado.put("actualizarResumenOperaciones",	new Boolean(actualizarResumenOperaciones)	);
	infoRegresar = resultado.toString();

} else if( informacion.equals("MonitorFlujoFondos.mostrarResultadoOperaciones") ){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;

	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("CatalogoConcepto")){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Crear Catalogo	
	JSONArray  registros = new JSONArray();
	JSONObject registro	= null;
	
	registro = new JSONObject();
	registro.put("clave",			"A");
	registro.put("descripcion",	"Abonos");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"C");
	registro.put("descripcion",	"Cargos");
	registros.add( JSONObject.fromObject(registro) );
		
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)				);
	resultado.put("total",    	new Integer(registros.size()) );
	resultado.put("registros",	registros			 				);
	
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("CatalogoTipoDispersion") ){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Crear Catalogo	
	JSONArray  registros = new JSONArray();
	JSONObject registro	= null;
	
	registro = new JSONObject();
	registro.put("clave",			"E");
	registro.put("descripcion",	"EPOS");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"F");
	registro.put("descripcion",	"IF's");
	registros.add( JSONObject.fromObject(registro) );
		
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)				);
	resultado.put("total",    	new Integer(registros.size()) );
	resultado.put("registros",	registros			 				);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CatalogoEPO")            							){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");
	cat.setHabilitado("S"); 
	List listaElementos = cat.getListaElementosMonitorDispersionEPO();	
	
	// Agregar la opción todos
	ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
	elementoCatalogo.setClave("TODAS");
	elementoCatalogo.setDescripcion("TODAS");
	listaElementos.add(0,elementoCatalogo);
				
	infoRegresar      = cat.getJSONElementos(listaElementos);

} else if(  informacion.equals("CatalogoIF")            )	{

	CatalogoIFMonitorDispersion cat = new CatalogoIFMonitorDispersion();
	cat.setCampoClave("ic_if"); 
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setProducto("1"); 
	cat.setDispersion("S"); 
	cat.setShowCampoOperaFideicomiso(true); // F017 - 2013: Para que se envíe el contenido del campo: COMCAT_IF.CS_OPERA_FIDEICOMISO
	cat.setOperaFideicomiso("S"); // F017 - 2013: Para que también se envién los IF que operan Fideicomiso
	cat.setOrden("cg_razon_social");
	
	List listaElementos = cat.getListaElementos();	

	// Agregar la opción todos
	HashMap elementoCatalogo = new HashMap();
	elementoCatalogo.put( "clave",       			"TODOS"	);
	elementoCatalogo.put( "descripcion", 			"TODOS"	);
	elementoCatalogo.put( "opera_fideicomiso", 	"N" 		);
	listaElementos.add(0,elementoCatalogo);
				
	infoRegresar      = cat.getJSONElementos(listaElementos);
	
} else if (	informacion.equals("ConsultaResumenOperacion")		){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	
	// Leer parametros
	String claveConcepto 			= (request.getParameter("claveConcepto")			== null)?"":request.getParameter("claveConcepto");
	String claveTipoDispersion 	= (request.getParameter("claveTipoDispersion")	== null)?"":request.getParameter("claveTipoDispersion");
	String fechaRegistroInicial	= (request.getParameter("fechaRegistroInicial")	== null)?"":request.getParameter("fechaRegistroInicial");
	String fechaRegistroFinal 		= (request.getParameter("fechaRegistroFinal")	== null)?"":request.getParameter("fechaRegistroFinal");
	String claveEPO 					= (request.getParameter("claveEPO")					== null)?"":request.getParameter("claveEPO");
	String claveIF 					= (request.getParameter("claveIF")					== null)?"":request.getParameter("claveIF");
 
	String operacion					= (request.getParameter("operacion") 				== null)?"":request.getParameter("operacion");
	String tipo 						= "Monitor" ;
	String viaLiquidacion 			= "";
	String estatus 					= "";
	String estatusFF 					= "";

	claveEPO	= "TODAS".equals(claveEPO)?"":claveEPO;
	claveIF	= "TODOS".equals(claveIF) ?"":claveIF;
	
	// Crear instancia del paginador
	ConsultaResumenOperacionErrorFFON paginador = new ConsultaResumenOperacionErrorFFON();
	paginador.setTipo(tipo);
	paginador.setTipoDispersion(claveTipoDispersion);
	paginador.setFechaRegistroInicial(fechaRegistroInicial);
	paginador.setFechaRegistroFinal(fechaRegistroFinal);
	paginador.setViaLiquidacion(viaLiquidacion);
	paginador.setClaveEPO(claveEPO);
	paginador.setClaveIF(claveIF);
	paginador.setEstatus(estatus);
	paginador.setEstatusFF(estatusFF);

	// Crear instancia del query helper y el objeto de resultado
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	queryHelper.setMaximoNumeroRegistros(2500);
	
	// Leer posicion de los registros
	int start = 0;
	int limit = 0;
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {

		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		
	}catch(Exception e){
 
		String msg = "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
		
	}
	
	// Realizar consulta
	try {

		if ("NuevaConsulta".equals(operacion)) {	//Nueva consulta
			queryHelper.executePKQuery(request);   //Obtiene las llaves primarias con base a los parametros de consulta
		}
		
		JSONArray 	registros 	= new JSONArray();
		JSONObject 	registro 	= null;
		
		Registros 	consulta 	= queryHelper.getPageResultSet(request,start,limit);

		while(consulta != null && consulta.next()){
			
			String tipoColor			= "formas";
			
			String estatusCecoban 	= "".equals(consulta.getString("ic_estatus_cecoban"))?"*":consulta.getString("ic_estatus_cecoban");
			String estatusDep 		= consulta.getString("cs_reproceso");
			String fechaRegistro 	= consulta.getString("df_registro");
			String estatusFFON 		= consulta.getString("status_cen_i");
			String tablaDetalle		= "";
			String verDetalle			= "";
			if( "D".equals(estatusDep) ){
				tablaDetalle = "his";
			}
			
			String cuenta 					= "".equals(consulta.getString("ic_cuenta"))?"*":consulta.getString("ic_cuenta");

			boolean 	 existeDetalle 	= dispersion.getExisteDetalleHist(consulta.getString("ic_flujo_fondos"), tablaDetalle) > 0?true:false;
			if( existeDetalle ){
				verDetalle = "javascript:procesaVerDetalle"; //"<a href=\"javascript:detalle('"+consulta.getString("ic_flujo_fondos")+"', '"+tablaDetalle+"');\">&nbsp;Ver</a>";
			} else {
				verDetalle = "Detalle depurado por proceso interno";
			}
					
			String motivoRechazo 		= consulta.getString("estatus_descripcion") +": "+consulta.getString("error_cen_s");
			if( !"99".equals(estatusCecoban) ) {
				tipoColor = "cuenta";
				if( "-1".equals(estatusFFON) ) {
					motivoRechazo = "Error en la informaci&oacute;n de la cuenta";
				}
			}

			// ENVIAR DETALLE DEL REGISTRO
			registro = new JSONObject();
		
			// Nota: Se podría mejorar la pantalla teniendo un objeto manager de los registros seleccionados,
			// esto permitiría hacer seleccion multipágina.
			
			// CAMPO: EPO
			registro.put("NOMBRE_EPO",			consulta.getString("razon_social_epo")	);
			// CAMPO: IF
			registro.put("NOMBRE_IF",			consulta.getString("razon_social_if")	);
			// CAMPO: PYME
			registro.put("NOMBRE_PYME",		consulta.getString("razon_social_pyme"));
			// CAMPO: RFC
			registro.put("RFC",					consulta.getString("cg_rfc")				);
			// CAMPO: NOMBRE_IF_FONDEO
			registro.put("NOMBRE_IF_FONDEO",	consulta.getString("razon_social_iffon")	);
			// CAMPO: Banco
			registro.put("BANCO",				consulta.getString("ic_bancos_tef")		);
			// CAMPO: Tipo de Cuenta
			registro.put("TIPO_CUENTA",		consulta.getString("ic_tipo_cuenta")	);
			// CAMPO: No. de Cuenta
			registro.put("NUMERO_CUENTA",		consulta.getString("cg_cuenta")			);
			// CAMPO: Importe
			registro.put("IMPORTE",				Comunes.formatoMN(consulta.getString("fn_importe")));
			// CAMPO: Motivo de Rechazo
			registro.put("MOTIVO_RECHAZO",	motivoRechazo									);
			// CAMPO: Ver Detalle
			registro.put("EXISTE_DETALLE", 	new Boolean(existeDetalle)					);
			registro.put("VER_DETALLE",		verDetalle										);
			// CAMPO: Seleccionar
			registro.put("SELECCIONAR", 		new Boolean(false) 							);
			if(!("60".equals(estatusFFON)||"9999".equals(estatusFFON))) { 
				registro.put("VALOR_SELECCIONAR",	estatusCecoban+"|"+consulta.getString("ic_flujo_fondos")+"|"+cuenta+"|"+consulta.getString("origen")+"|"+fechaRegistro );
			} else {
				registro.put("VALOR_SELECCIONAR",	""				 							);
			}
			// CAMPO: Causa
			registro.put("CAUSA",				""						 							);
			// Campo de apoyo
			registro.put("ESTATUS_CECOBAN",	estatusCecoban		 							 );
			registro.put("ESTATUS_FFON",		estatusFFON 		 							 );	
			registro.put("IC_FLUJO_FONDOS",	consulta.getString("ic_flujo_fondos")	 ); // Usado por verDetalle
			registro.put("TABLA_DETALLE",		tablaDetalle	 	 							 ); // Usado por verDetalle
			
			registros.add(registro);
			
		}
		
		resultado.put( "total",			new Integer( queryHelper.getIdsSize() )	);
		resultado.put( "registros",	registros 											);
		
		// Obtener los totales
		if( "NuevaConsulta".equals(operacion) ){
			
			Registros 	totales 							= queryHelper.getResultCount(request);
			String 		totalRegistrosProcesados	= "0";
			String 		totalRegistrosAceptados		= "0";
			String 		totalRegistrosRechazados	= "0";
			
			if( totales != null && totales.next() ){
				totalRegistrosProcesados = totales.getString("EN_PROCESO");
				totalRegistrosAceptados  = totales.getString("ACEPTADOS");
				totalRegistrosRechazados = totales.getString("CON_ERROR");
			}
	
			resultado.put("totalRegistrosProcesados",	totalRegistrosProcesados	);
			resultado.put("totalRegistrosAceptados",	totalRegistrosAceptados		);
			resultado.put("totalRegistrosRechazados",	totalRegistrosRechazados	);
 
		}
		
	} catch(Exception e) {
		
		log.error("ConsultaResumenOperacion(Exception)");
		log.error("ConsultaResumenOperacion.claveConcepto        = <" + claveConcepto        + ">");
		log.error("ConsultaResumenOperacion.claveTipoDispersion  = <" + claveTipoDispersion  + ">");
		log.error("ConsultaResumenOperacion.fechaRegistroInicial = <" + fechaRegistroInicial + ">");
		log.error("ConsultaResumenOperacion.fechaRegistroFinal   = <" + fechaRegistroFinal   + ">");
		log.error("ConsultaResumenOperacion.claveEPO             = <" + claveEPO             + ">");
		log.error("ConsultaResumenOperacion.claveIF              = <" + claveIF              + ">");
		log.error("ConsultaResumenOperacion.operacion            = <" + operacion            + ">");
		e.printStackTrace();
		
		throw new AppException("Error en la paginacion", e);
		
	}	
			
	resultado.put("success",new Boolean(success));
	
	infoRegresar = resultado.toString();	
	
} else if (	informacion.equals("ConsultaDetalleMatrizDispersion")		){

	JSONObject	resultado 		= new JSONObject();
	boolean		success	 		= true;
	
	// Leer parametros
	String 		icFlujoFondos 	= (request.getParameter("icFlujoFondos")	== null)?"":request.getParameter("icFlujoFondos");
	String 		tablaDetalle 	= (request.getParameter("tablaDetalle")	== null)?"":request.getParameter("tablaDetalle");
		
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {

		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		
	}catch(Exception e){
 
		String msg = "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
		
	}
	
	// Realizar consulta
	HashMap 		detalle 		= dispersion.getDetalleOperacion(icFlujoFondos, tablaDetalle);
		
	JSONArray 	registros 	= new JSONArray();
	JSONObject 	registro  	= null;
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"1");
	registro.put("DESCRIPCION",	"Fecha de captura");
	registro.put("VALOR",			(String) detalle.get("1"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"2");
	registro.put("DESCRIPCION",	"ID sistema");
	registro.put("VALOR",			(String) detalle.get("2"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"3");
	registro.put("DESCRIPCION",	"Area que captura");
	registro.put("VALOR",			(String) detalle.get("3"));
	registros.add(registro); 
	 
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"4");
	registro.put("DESCRIPCION",	"Folio");
	registro.put("VALOR",			(String) detalle.get("4"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"5");
	registro.put("DESCRIPCION",	"Tipo de operación");
	registro.put("VALOR",			(String) detalle.get("5"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"6");
	registro.put("DESCRIPCION",	"Referncia del movimiento");
	registro.put("VALOR",			(String) detalle.get("6"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"");
	registro.put("DESCRIPCION",	"Cliente  ordenante");
	registro.put("VALOR",			"");
	registros.add(registro); 
	 
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"8");
	registro.put("DESCRIPCION",	"Clave ordenante");
	registro.put("VALOR",			(String) detalle.get("8"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"9");
	registro.put("DESCRIPCION",	"RFC");
	registro.put("VALOR",			(String) detalle.get("9"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"10");
	registro.put("DESCRIPCION",	"Nombre");
	registro.put("VALOR",			(String) detalle.get("10"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"11");
	registro.put("DESCRIPCION",	"Domicilio");
	registro.put("VALOR",			(String) detalle.get("11"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"12");
	registro.put("DESCRIPCION",	"Banco");
	registro.put("VALOR",			(String) detalle.get("12"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"13");
	registro.put("DESCRIPCION",	"Plaza");
	registro.put("VALOR",			(String) detalle.get("13"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"14");
	registro.put("DESCRIPCION",	"Sucursal");
	registro.put("VALOR",			(String) detalle.get("14"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"15");
	registro.put("DESCRIPCION",	"No. Cuenta");
	registro.put("VALOR",			(String) detalle.get("15"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"16");
	registro.put("DESCRIPCION",	"Tipo de Cuenta");
	registro.put("VALOR",			(String) detalle.get("16"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"17");
	registro.put("DESCRIPCION",	"Tipo de Divisa");
	registro.put("VALOR",			(String) detalle.get("17"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"");
	registro.put("DESCRIPCION",	"Cliente Beneficiario");
	registro.put("VALOR",			"");
	registros.add(registro); 
	 
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"19");
	registro.put("DESCRIPCION",	"Clave del Beneficiario");
	registro.put("VALOR",			(String) detalle.get("19"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"20");
	registro.put("DESCRIPCION",	"RFC");
	registro.put("VALOR",			(String) detalle.get("20"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"21");
	registro.put("DESCRIPCION",	"Nombre");
	registro.put("VALOR",			(String) detalle.get("21"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"22");
	registro.put("DESCRIPCION",	"Domicilio");
	registro.put("VALOR",			(String) detalle.get("22"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"23");
	registro.put("DESCRIPCION",	"Banco");
	registro.put("VALOR",			(String) detalle.get("23"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"24");
	registro.put("DESCRIPCION",	"Plaza");
	registro.put("VALOR",			(String) detalle.get("24"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"25");
	registro.put("DESCRIPCION",	"");
	registro.put("VALOR",			(String) detalle.get("25"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"26");
	registro.put("DESCRIPCION",	"No. Cuenta Clabe");
	registro.put("VALOR",			(String) detalle.get("26"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"27");
	registro.put("DESCRIPCION",	"Tipo de cuenta");
	registro.put("VALOR",			(String) detalle.get("27"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"28");
	registro.put("DESCRIPCION",	"Tipo de Divisa");
	registro.put("VALOR",			(String) detalle.get("28"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"29");
	registro.put("DESCRIPCION",	"Fecha Valor");
	registro.put("VALOR",			(String) detalle.get("29"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"30");
	registro.put("DESCRIPCION",	"Importe");
	registro.put("VALOR",			(String) detalle.get("30"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"31");
	registro.put("DESCRIPCION",	"Descripción de la instrucción");
	registro.put("VALOR",			(String) detalle.get("31"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"34");
	registro.put("DESCRIPCION",	"Concepto");
	registro.put("VALOR",			(String) detalle.get("34"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"35");
	registro.put("DESCRIPCION",	"Tipo de operación (movto.)");
	registro.put("VALOR",			(String) detalle.get("35"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"36");
	registro.put("DESCRIPCION",	"Clave rastreo");
	registro.put("VALOR",			(String) detalle.get("36"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"38");
	registro.put("DESCRIPCION",	"Digito verificador de la cuenta ordenante");
	registro.put("VALOR",			(String) detalle.get("38"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"39");
	registro.put("DESCRIPCION",	"Digito verificador del beneficiario");
	registro.put("VALOR",			(String) detalle.get("39"));
	registros.add(registro); 
	
	registro = new JSONObject();
	registro.put("NUMERO_CAMPO",	"74");
	registro.put("DESCRIPCION",	"Leyenda de rastreo");
	registro.put("VALOR",			(String) detalle.get("74"));
	registros.add(registro); 
 
	// Regresar resultados	
	resultado.put("total",			new Integer( registros.size() )	);
	resultado.put("registros",		registros 								);
	resultado.put("success",		new Boolean(success)					);
	
	infoRegresar = resultado.toString();
	
} else if (	informacion.equals("GenerarArchivoMatrizDispersion")		){
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	String      urlArchivo		= "";
	
	String 		nombreArchivo	= "";
	
	// Leer parametros
	String 		tipo 				= (request.getParameter("tipo") 				== null)?"":request.getParameter("tipo");

	String 		icFlujoFondos 	= (request.getParameter("icFlujoFondos")	== null)?"":request.getParameter("icFlujoFondos");
	String 		tablaDetalle 	= (request.getParameter("tablaDetalle")	== null)?"":request.getParameter("tablaDetalle");
	
	// Preparar clases adicionales
	ConsultaDetalleMonitoreoIfFFON consulta = new ConsultaDetalleMonitoreoIfFFON();
	
	// HttpSession session
	String		directorioTemporal		= strDirectorioTemp; 
	String 		directorioPublicacion 	= strDirectorioPublicacion;
	
	// Crear archivo
	if( "CSV".equals(tipo) ){
		nombreArchivo = consulta.generaArchivoCSV( directorioTemporal, icFlujoFondos, tablaDetalle );
		urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
	}
 
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>