<%@ page 
	contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("InterfaseFlujoFondos.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// 1. Leer parametros
	String claveEPO 					= (request.getParameter("claveEPO")						== null)?"":request.getParameter("claveEPO");
	String claveMoneda 				= (request.getParameter("claveMoneda")					== null)?"":request.getParameter("claveMoneda");
	String fechaVencimiento 		= (request.getParameter("fechaVencimiento")			== null)?"":request.getParameter("fechaVencimiento");
	String claveIF 					= (request.getParameter("claveIF")						== null)?"":request.getParameter("claveIF");
	String claveIfFondeo 			= (request.getParameter("claveIfFondeo")				== null)?"":request.getParameter("claveIfFondeo");
	String claveEstatus 				= (request.getParameter("claveEstatus")				== null)?"":request.getParameter("claveEstatus");
	String fechaOperacion			= (request.getParameter("fechaOperacion")				== null)?"":request.getParameter("fechaOperacion");
	
	String numeroDocumentos			= (request.getParameter("numeroDocumentos")			== null)?"":request.getParameter("numeroDocumentos");
	String importeTotal 				= (request.getParameter("importeTotal")				== null)?"":request.getParameter("importeTotal");
	String numeroDocumentosError	= (request.getParameter("numeroDocumentosError")	== null)?"":request.getParameter("numeroDocumentosError");
	String importeTotalError		= (request.getParameter("importeTotalError")			== null)?"":request.getParameter("importeTotalError");
	
	// 2. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	} catch(Exception e) {
 
		msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
 
	}
	
	// 3. Valida que no existan operaciones 
	boolean existenOperaciones = dispersion.existenOperaciones(claveEPO, claveMoneda, fechaVencimiento, claveIF, claveEstatus, fechaOperacion, claveIfFondeo);
	
	if( !existenOperaciones ){
		
		// Construir Array con las cifras de control
		JSONArray 	cifrasControlDataArray 	= new JSONArray();
		JSONArray	registro						= null;
		
		registro = new JSONArray();
		registro.add("No. Total de Registros por Dispersar:");
		registro.add(numeroDocumentos);
		registro.add("NUMERO_ENTERO");
		cifrasControlDataArray.add(registro);
			
		registro = new JSONArray();
		registro.add("Monto Total por Dispersar:");
		registro.add(Comunes.formatoMN(importeTotal));
		registro.add("MONTO");
		cifrasControlDataArray.add(registro);

		if( Integer.parseInt(numeroDocumentosError) > 0 ) {
			
			registro = new JSONArray();
			registro.add("No. Total de Registros con Error:");
			registro.add(numeroDocumentosError);
			registro.add("NUMERO_ENTERO");
			cifrasControlDataArray.add(registro);
		
			registro = new JSONArray();
			registro.add("Monto Total con Error:");
			registro.add(Comunes.formatoMN(importeTotalError));
			registro.add("MONTO");
			cifrasControlDataArray.add(registro);
 
		}

		resultado.put("cifrasControlDataArray",cifrasControlDataArray);
		
	} else {
		msg 					 = "Ya fueron registradas estas operaciones.";
	}
	
	// 4. Determinar el estado siguiente
	if( existenOperaciones ){
		estadoSiguiente = "CANCELAR_EJECUCION";
	} else {
		estadoSiguiente = "MOSTRAR_CIFRAS_CONTROL";
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if (        informacion.equals("InterfaseFlujoFondos.cancelarEjecucion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	String 		destino 				= (request.getParameter("pantallaOrigen") == null)?"":request.getParameter("pantallaOrigen");
 
	// Determinar el estado siguiente
	estadoSiguiente = "FIN";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	resultado.put("destino",				destino					);
	infoRegresar = resultado.toString();

} else if (        informacion.equals("InterfaseFlujoFondos.aceptarCifrasControl") )	{

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
  
	// Determinar el estado siguiente
	estadoSiguiente = "SOLICITAR_CONFIRMACION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if (        informacion.equals("InterfaseFlujoFondos.solicitarConfirmacion") )	{

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
  
	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if (        informacion.equals("InterfaseFlujoFondos.llenaEncabezadoFlujoFondos") )	{

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String 		destino 				= null;
	String		msg					= null;
 
	// 1. Leer parametros
	
	String claveUsuario 				= (request.getParameter("claveUsuario")				== null)?"":request.getParameter("claveUsuario");
	String contrasena 				= (request.getParameter("contrasena")					== null)?"":request.getParameter("contrasena");
	
	String pantallaOrigen 			= (request.getParameter("pantallaOrigen")				== null)?"":request.getParameter("pantallaOrigen");
	String origen 						= (request.getParameter("origen")						== null)?"":request.getParameter("origen");
	
	String claveEPO 					= (request.getParameter("claveEPO")						== null)?"":request.getParameter("claveEPO");
	String claveMoneda 				= (request.getParameter("claveMoneda")					== null)?"":request.getParameter("claveMoneda");
	String fechaVencimiento 		= (request.getParameter("fechaVencimiento")			== null)?"":request.getParameter("fechaVencimiento");
	String claveIF 					= (request.getParameter("claveIF")						== null)?"":request.getParameter("claveIF");
	String claveIfFondeo 			= (request.getParameter("claveIfFondeo")				== null)?"":request.getParameter("claveIfFondeo");
	String claveEstatus 				= (request.getParameter("claveEstatus")				== null)?"":request.getParameter("claveEstatus");
	String fechaOperacion			= (request.getParameter("fechaOperacion")				== null)?"":request.getParameter("fechaOperacion");
	
	String numeroDocumentos			= (request.getParameter("numeroDocumentos")			== null)?"":request.getParameter("numeroDocumentos");
	String importeTotal 				= (request.getParameter("importeTotal")				== null)?"":request.getParameter("importeTotal");
	String numeroDocumentosError	= (request.getParameter("numeroDocumentosError")	== null)?"":request.getParameter("numeroDocumentosError");
	String importeTotalError		= (request.getParameter("importeTotalError")			== null)?"":request.getParameter("importeTotalError");
	
	// 2. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	} catch(Exception e) {
 
		msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
 
	}
	
	// VALIDAR USUARIO
		
	boolean esUsuarioCorrecto = false;
	
	try {
		
		esUsuarioCorrecto = dispersion.esUsuarioCorrecto(strLogin, claveUsuario, contrasena);
		
	} catch(NafinException ne){
		
		esUsuarioCorrecto = false;
		msg 					= ne.getMessage();
		
	} catch(Exception e){
	
		throw e;
		
	}
	
	// LLENAR ENCABEZADO DE FLUJO DE FONDOS
	
	if( esUsuarioCorrecto ) {
		
		// Llena encabezado flujo fondos
		dispersion.llenaEncabezadoFlujoFondos( claveEPO, claveMoneda, fechaVencimiento, claveIF, origen, claveEstatus, fechaOperacion, false, claveIfFondeo );
		// Determinar mensaje a mostrar
		if( claveMoneda != null && claveMoneda.equals("54") ){
			// Dependiente del tipo de moneda
			msg = "Las operaciones fueron registradas con éxito.";
		} else {
			msg = "Las operaciones fueron registradas con éxito para el flujo de fondos.";
		}	
		
	} else {
		
		// Falló la validacion del password: se finaliza la operacion
		msg = "El login y/o password del usuario es incorrecto, operación cancelada.";

	}

	// 4. Determinar el estado siguiente
	estadoSiguiente = "FIN";
	destino 			 = pantallaOrigen;
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("destino",				destino					);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>