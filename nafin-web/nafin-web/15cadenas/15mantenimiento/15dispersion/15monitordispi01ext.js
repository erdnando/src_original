Ext.onReady(function() {
	
	Ext.namespace('NE.monitordispersion');
	
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaMonitorDispersion = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						monitorDispersion(resp.estadoSiguiente,resp);
					}
				);
			} else {
				monitorDispersion(resp.estadoSiguiente,resp);
			}
			
		} else {
						
			/*
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Resetear forma
         resetWindowConfirmacionClave();
 
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			*/
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
					
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var monitorDispersion = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"							   ){
			
			// Cargar Cat�logo de Tipos de Monitoreo
			var catalogoTipoMonitoreoData = Ext.StoreMgr.key('catalogoTipoMonitoreoDataStore');
			catalogoTipoMonitoreoData.load({
				params: { defaultValue: "F" }
			});
			
			// Cargar Cat�logo de IF
			var catalogoIFData = Ext.StoreMgr.key('catalogoIFDataStore');
			catalogoIFData.load();
			
			// El Cat�logo EPO se presentar� vac�o por default
			var catalogoEPOData = Ext.StoreMgr.key('catalogoEPODataStore');
			catalogoEPOData.removeAll();
 
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispi01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.inicializacion'
				},
				callback: 				procesaMonitorDispersion
			});
 	
		} else if(			estado == "MOSTRAR_PANTALLA_MONITOREO"			  ){
									
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispi01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.mostrarPantallaMonitoreo',
					tipoMonitoreo:		respuesta.tipoMonitoreo
				},
				callback: 				procesaMonitorDispersion
			});
 			
		} else if(			estado == "ESPERAR_DECISION"  					  ){ 
			         
			/*
			
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
			
         // Resetear forma
         resetWindowConfirmacionClave();
         
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			*/
			
		} else if(			estado == "FIN"  					                 ){
 	
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"15monitordispi01ext.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			forma.submit();
						
		}
		
	}
 
	//--------------------------------- 2. PANEL DE DETALLE MONITOREO IF ---------------------------------------
 
	var procesarSuccessFailureGeneraPDF =  function(opts, success, response) {
		
		var btnGeneraPDF = Ext.getCmp('btnGeneraPDF');
		btnGeneraPDF.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaPDF = Ext.getCmp('btnBajaPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(/^\/nafin/,'');
				// Insertar archivo
				Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					}
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
			});
			
		} else {
			
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarSuccessFailureGeneraCSV =  function(opts, success, response) {
		
		var btnGeneraCSV = Ext.getCmp('btnGeneraCSV');
		btnGeneraCSV.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaCSV = Ext.getCmp('btnBajaCSV');
			btnBajaCSV.show();
			btnBajaCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaCSV.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(/^\/nafin/,'');
				// Insertar archivo
				Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					}
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
			});
			
		} else {
			
			btnGeneraCSV.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarDetalleMonitoreoIFData = function(store, registros, opts){
				
		var panelMonitoreoIF = Ext.getCmp('panelMonitoreoIF');
		panelMonitoreoIF.el.unmask();
		
		var gridDetalleMonitoreoIF = Ext.getCmp('gridDetalleMonitoreoIF');
		
		if (registros != null) {
			
			/*
			if (!gridDetalleMonitoreoIF.isVisible()) {
				gridDetalleMonitoreoIF.show();
			}
			*/
			
			var btnGeneraPDF 				 = Ext.getCmp('btnGeneraPDF');
			var btnGeneraCSV				 = Ext.getCmp('btnGeneraCSV');
			var btnBajaPDF					 = Ext.getCmp('btnBajaPDF');
			var btnBajaCSV					 = Ext.getCmp('btnBajaCSV');		
 
			var el 					 		 = gridDetalleMonitoreoIF.getGridEl();
			
			// Copiar en el grid los parametros de la ultima consulta
			gridDetalleMonitoreoIF.formParams = Ext.apply({},opts.params);
			
			// Actualizar propiedades del grid panel
			if(store.getTotalCount() > 0) {
				
				btnGeneraPDF.enable();
				btnGeneraCSV.enable();
				btnBajaPDF.hide();
				btnBajaCSV.hide();
				
				el.unmask();
				
			} else {
				
				btnGeneraPDF.disable();
				btnGeneraCSV.disable();
				btnBajaPDF.hide();
				btnBajaCSV.hide();
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
 
		}
					
	};
	
	var nombreEPORenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var nombrePYMERenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var numeroNafinElectronicoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var habilitadaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var numeroTotalDocumentosRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var nombreMonedaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var montoTotalRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="text-align:right;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		value = record.json['MONTO_TOTAL'];
		return value;
		
	}
	
	var codigoBancoCuentaClabeRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var cuentaClabeSwiftRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var descripcionCECOBANRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var descripcionDOLARESRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
  
	// Crear JsonStore que se encargar� de cargar el detalle
	var detalleMonitoreoIFData = new Ext.data.GroupingStore({
		id:					'detalleMonitoreoIFDataStore',
		name:					'detalleMonitoreoIFDataStore',
		root:					'registros',
		url: 					'15monitordispi01ext.data.jsp',
		autoDestroy:		true,
		baseParams: {
			informacion: 	'ConsultaDetalleMonitoreoIF'
		},			
		reader: new Ext.data.JsonReader({
			root: 			'registros',	
			totalProperty: 'total',
			fields: [
				{ name:'IC_IF',								type:"string" }, // GROUP_ID
				{ name:'NOMBRE_IF',							type:"string" },
				{ name:'NOMBRE_EPO',							type:"string" },
				{ name:'NOMBRE_PYME',						type:"string" },
				{ name:'NUMERO_NAFIN_ELECTRONICO',		type:"string" },
				{ name:'HABILITADA',							type:"string" },
				{ name:'NUMERO_TOTAL_DOCUMENTOS',		type:"int"    },
				{ name:'NOMBRE_MONEDA',						type:"string" },
				{ name:'MONTO_TOTAL',						type:"float", convert: function(value, record){ return Number(value.replace(/[\$,]/g,"")); } },
				{ name:'CODIGO_BANCO_CUENTA_CLABE',		type:"string" },
				{ name:'CUENTA_CLABE_SWIFT',				type:"string" },
				{ name:'DESCRIPCION_CECOBAN',				type:"string" },
				{ name:'DESCRIPCION_DOLARES',				type:"string" },
				{ name:'ESTATUS_CECOBAN',					type:"string" }	
			]
		}),
		groupField: 			'IC_IF', // GROUP_ID
		sortInfo: 				{ field: 'NOMBRE_IF', direction: "ASC" },
		autoLoad: 				false,
		totalProperty: 		'total',
		messageProperty: 		'msg',
		pruneModifiedRecords: true, // Para que no se conserven los registros modificados en consulta anterior.
		listeners: { 
			beforeload: {
				fn: function( store, opts ) {
					var grid = Ext.getCmp('gridDetalleMonitoreoIF');
					if (grid) {
						grid.show();
					} 			
				}
			},
			load: 	procesarDetalleMonitoreoIFData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarDetalleMonitoreoIFData(null, null, null);						
				}
			}
		}
	});
    
	// Crear plugin que se encargar� de desplegar los totales
	var totalesPorGrupoSummary = new Ext.ux.grid.ServerSummary();
	
	var gridDetalleMonitoreoIF = {
		frame:			true,
		store: 			detalleMonitoreoIFData,
		//title:			'Detalle Monitoreo IF',
		xtype: 			'grid',
		id:				'gridDetalleMonitoreoIF',
		hidden:			true,
		stripeRows: 	true,
		loadMask: 		true,
		width: 			940, 
		height:			480,
		region: 			'center',
		style: 			'margin: 0 auto',
		formParams:		{},
		view:		 		new Ext.grid.SelectableGroupingView(
			{
				forceFit:				false, 
				groupTextTpl: 			'{text}', // Usar group text renderer
				hideGroupedColumn: 	true,
				enableGroupingMenu:	false,
				hideGroupCheckBox: 	true
			}
		),
		plugins: totalesPorGrupoSummary,
		columns: [
			{
				header: 		'CLAVE IC_IF', 
				tooltip: 	'CLAVE IC_IF', // GROUP_ID
				dataIndex: 	'IC_IF', 
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				// width: 		315,
				hidden: 		true,
				hideable:	false,
				groupTextRenderer: function(value,unused,record,rowIndex,colIndex,dsStore){
					return  record.data['NOMBRE_IF'];
				}
			},
			{
				header: 		'EPO<br>&nbsp;',
				tooltip: 	'EPO',
				dataIndex: 	'NOMBRE_EPO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	nombreEPORenderer
			},
			{
				header: 		'PYME<br>&nbsp;',
				tooltip: 	'PYME',
				dataIndex: 	'NOMBRE_PYME',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	nombrePYMERenderer
			},
			{
				header: 		'Num. Nafin<br>Electr&oacute;nico',
				tooltip: 	'Num. Nafin Electr&oacute;nico',
				dataIndex: 	'NUMERO_NAFIN_ELECTRONICO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroNafinElectronicoRenderer
			},
			{
				header: 		'Habilitada<br>&nbsp;',
				tooltip: 	'Habilitada',
				dataIndex: 	'HABILITADA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	habilitadaRenderer
			},
			{
				header: 		'Num. Total<br>de Doctos.',
				tooltip: 	'Num. Total de Doctos.',
				dataIndex: 	'NUMERO_TOTAL_DOCUMENTOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroTotalDocumentosRenderer
			},
			{
				header: 		'Moneda<br>&nbsp;',
				tooltip: 	'Moneda',
				dataIndex: 	'NOMBRE_MONEDA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				renderer:	nombreMonedaRenderer
			},
			{
				header: 		'Monto Total<br>&nbsp;',
				tooltip: 	'Monto Total',
				dataIndex: 	'MONTO_TOTAL',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	montoTotalRenderer
			},
			{
				header: 		'C&oacute;digo Banco<br>Cuenta Clabe',
				tooltip: 	'C&oacute;digo Banco Cuenta Clabe',
				dataIndex: 	'CODIGO_BANCO_CUENTA_CLABE',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				renderer:	codigoBancoCuentaClabeRenderer
			},
			{
				header: 		'Cuenta CLABE/SWIFT<br>&nbsp;',
				tooltip: 	'Cuenta CLABE/SWIFT',
				dataIndex: 	'CUENTA_CLABE_SWIFT',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	cuentaClabeSwiftRenderer
			},
			{
				header: 		'Descripci&oacute;n<br>CECOBAN',
				tooltip: 	'Descripci&oacute;n CECOBAN',
				dataIndex: 	'DESCRIPCION_CECOBAN',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	descripcionCECOBANRenderer
			},
			{
				header: 		'Descripci&oacute;n<br>D&Oacute;LARES',
				tooltip: 	'Descripci&oacute;n D&Oacute;LARES',
				dataIndex: 	'DESCRIPCION_DOLARES',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	descripcionDOLARESRenderer
			}
		],
		stateful:	false,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'left',
			items: [
				'->',
				{
					xtype: 	'button',
					text: 	'Generar PDF',
					id: 		'btnGeneraPDF',
					iconCls: 'icoGenerarDocumento',
					disabled: true,
					handler: function(boton, evento) {
						
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						var panelMonitoreoIF 		 = Ext.getCmp("panelMonitoreoIF");
						var gridDetalleMonitoreoIF = Ext.getCmp("gridDetalleMonitoreoIF");
						
						// Generar Archivo PDF
						Ext.Ajax.request({
							url: 		'15monitordispi01ext.data.jsp',
							params: 	Ext.apply(
								gridDetalleMonitoreoIF.formParams, // panelMonitoreoIF.getForm().getValues()
								{
									informacion: 'GenerarArchivo',
									tipo: 		 'PDF'
								}
							),
							callback: procesarSuccessFailureGeneraPDF
						});
						
					}
				},
				{
					xtype: 	'button',
					text: 	'Bajar PDF',
					id: 		'btnBajaPDF',
					iconCls:	'icoBotonPDF',
					hidden: 	true
				},
				'-',
				{
					xtype: 	'button',
					text: 	'Generar Archivo',
					id: 		'btnGeneraCSV',
					iconCls:	'icoGenerarDocumento',
					disabled: true,
					handler: function(boton, evento) {
								
						boton.disable();
						boton.setIconClass('loading-indicator');
									
						var panelMonitoreoIF 		= Ext.getCmp("panelMonitoreoIF");
						var gridDetalleMonitoreoIF = Ext.getCmp("gridDetalleMonitoreoIF");
						
						// Generar Archivo CSV
						Ext.Ajax.request({
							url: 		'15monitordispi01ext.data.jsp',
							params: 	Ext.apply(
								gridDetalleMonitoreoIF.formParams, // panelMonitoreoIF.getForm().getValues()
								{
									informacion: 'GenerarArchivo',
									tipo: 		 'CSV'
								}
							),
							callback: procesarSuccessFailureGeneraCSV
						});
							
					}
				},
				{
					xtype: 	'button',
					text: 	'Bajar Archivo',
					id: 		'btnBajaCSV',
					iconCls:	'icoBotonXLS',
					hidden: 	true
				}
			]
		}
	};
	
	//------------------------------------- 1. PANEL DE MONITOREO IF ------------------------------------------

	var procesaConsultar = function(boton,evento){
		
		// Por compatibilidad con la versi�n anterior
		/*
		var comboTipoMonitoreo = Ext.getCmp("comboTipoMonitoreo");
		if( comboTipoMonitoreo.getValue() !== "F" ){
			return;	
		}
		*/
		
		// Validar toda la forma
		var panelMonitoreoIF = Ext.getCmp("panelMonitoreoIF");
		if( !panelMonitoreoIF.getForm().isValid() ){
			return;
		}
 
		// Agregar m�scara
		panelMonitoreoIF.getEl().mask('Consultando...','x-mask-loading');
		
		// Si hay una m�scara previa en el grid, suprimirla
		var gridEl = Ext.getCmp("gridDetalleMonitoreoIF").getGridEl();
		if( gridEl.isMasked()){
			gridEl.unmask();
		}
		
		// Realizar consulta
		var detalleMonitoreoIFData = Ext.StoreMgr.key('detalleMonitoreoIFDataStore');
		detalleMonitoreoIFData.load({
			params: Ext.apply(
				panelMonitoreoIF.getForm().getValues(),
				{
					informacion: 'ConsultaDetalleMonitoreoIF'
				}
			)
		});
		 
	}
	
	var procesaSelectComboIF = function(comboIF, record, index){
					
		var claveIF 			= comboIF.getValue();
		var catalogoEPOData 	= Ext.StoreMgr.key('catalogoEPODataStore');
					
		if( Ext.isEmpty(claveIF) ){
			catalogoEPOData.removeAll();
			return;
		}
 
		catalogoEPOData.load({
			params: { 
				defaultValue: 	"TODAS",
				claveIF:			claveIF
			}
		});
					
	}
	
	var procesaLimpiar = function(boton,evento){
		Ext.getCmp("panelMonitoreoIF").getForm().reset();
		Ext.getCmp("comboEPO").clearValue();
		Ext.StoreMgr.key('catalogoEPODataStore').removeAll();
	}
	
	var catalogoTipoMonitoreoData = new Ext.data.JsonStore({
		id: 					'catalogoTipoMonitoreoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispi01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoTipoMonitoreo'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboTipoMonitoreo = Ext.getCmp("comboTipoMonitoreo");
						comboTipoMonitoreo.setValue(defaultValue);
						comboTipoMonitoreo.originalValue = comboTipoMonitoreo.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	var catalogoIFData = new Ext.data.JsonStore({
		id: 					'catalogoIFDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispi01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIF'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboIF = Ext.getCmp("comboIF");
						comboIF.setValue(defaultValue);
						comboIF.originalValue = comboIF.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 					'catalogoEPODataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispi01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoEPO'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboEPO = Ext.getCmp("comboEPO");
						comboEPO.setValue(defaultValue);
						comboEPO.originalValue = comboEPO.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementosPanelMonitoreoIF = [
		// COMBO TIPO DE MONITOREO
		{
			xtype: 				'combo',
			name: 				'tipoMonitoreo',
			id: 					'comboTipoMonitoreo',
			fieldLabel: 		'Tipo de Monitoreo',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveTipoMonitoreo',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTipoMonitoreoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'65%',
			listeners:			{
				select:	function( combo, record, index ){
					
					var respuesta = new Object();
					respuesta['tipoMonitoreo'] = combo.getValue();
					monitorDispersion("MOSTRAR_PANTALLA_MONITOREO",respuesta);
					
				}
			}
		},
		// COMBO IF
		{
			xtype: 				'combo',
			name: 				'if',
			id: 					'comboIF',
			fieldLabel: 		'IF',
			allowBlank:			true,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveIF',
			emptyText: 			'Seleccione un IF...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoIFData,
			anchor:				'75%',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			width:				350,
			listeners: {
				select: procesaSelectComboIF
			}
		},
		{
			xtype: 						'compositefield',
			fieldLabel: 				'EPO',
			combineErrors: 			false,
			msgTarget: 					'side',
			items: [
				// COMBO EPO
				{
					xtype: 				'combo',
					name: 				'epo',
					id: 					'comboEPO',
					//fieldLabel: 		'EPO',
					allowBlank:			true,
					mode: 				'local', 
					displayField: 		'descripcion',
					valueField: 		'clave',
					hiddenName: 		'claveEPO',
					emptyText: 			'Debe de Seleccionar un IF...',
					forceSelection: 	true,
					triggerAction: 	'all',
					typeAhead: 			true,
					msgTarget: 			'side',
					minChars: 			1,
					store: 				catalogoEPOData,
					anchor:				'95%',
					tpl:					'<tpl for=".">' +
											'<tpl if="!Ext.isEmpty(loadMsg)">'+
											'<div class="loading-indicator">{loadMsg}</div>'+
											'</tpl>'+
											'<tpl if="Ext.isEmpty(loadMsg)">'+
											'<div class="x-combo-list-item">' +
											'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
											'</div></tpl></tpl>',
					width:				350	
				},
				{
					xtype: 				'displayfield',
					value: 				'Sin Cuenta CLABE:',
					style:				'text-align: right;',
					width: 				120
				},
				// CHECKBOX SIN CUENTA CLABE
				{
					xtype: 			'checkbox',
					name: 			'sinCuentaCLABE',
					id: 				'sinCuentaCLABE',
					//fieldLabel: 	'Sin Cuenta CLABE',
					inputValue:		'Sin Cuenta'
				}
			]
		},
		// RADIOGROUP HABILITADAS
		{
      	xtype:  		'radiogroup',
      	fieldLabel: '',
      	anchor: '95%',
      	columns: [ 0.24, 0.275, 0.25 ],
      	items: [
         	{ boxLabel: 'Habilitadas', 	name: 'radioHabilitadas', inputValue: "H"	},
         	{ boxLabel: 'No Habilitadas', name: 'radioHabilitadas', inputValue: "N"	},
         	{ boxLabel: 'Ambas', 			name: 'radioHabilitadas', inputValue: "A"	}
         ]
       }

	];
	
	var panelMonitoreoIF = new Ext.form.FormPanel({
		id: 					'panelMonitoreoIF',
		width: 				700,
		title: 				'Monitor IF',
		frame: 				true,
		collapsible: 		true,
		titleCollapse: 	true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px',
		defaults: {	
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		labelWidth: 		130,
		defaultType: 		'textfield',
		trackResetOnLoad:	true,
		items: 				elementosPanelMonitoreoIF,
		monitorValid: 		false,
		buttons: [
			{
				text: 		'Consultar',
				iconCls: 	'icoBuscar',
				handler: 	procesaConsultar
			},
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	procesaLimpiar
			}
		]
	});
	
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelMonitoreoIF,
			NE.util.getEspaciador(10),
			gridDetalleMonitoreoIF
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	monitorDispersion("INICIALIZACION",null);
	
});