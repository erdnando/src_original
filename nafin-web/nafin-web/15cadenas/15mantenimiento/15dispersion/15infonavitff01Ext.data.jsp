<%@ page 
	contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.math.BigDecimal,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("Infonavit.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// Remover atributos que pudieran haber quedado en sesión
	session.removeAttribute( "lReportes"             );
	session.removeAttribute( "lReportes.registros"   );
	session.removeAttribute( "lReportes.summaryData" );
			
	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if(  informacion.equals("CatalogoMoneda")            )	{
	
	JSONObject		jsonObj 	= new JSONObject();
	CatalogoMoneda cat 		= new CatalogoMoneda();
   cat.setCampoClave("ic_moneda");
   cat.setCampoDescripcion("cd_nombre"); 
	cat.setValoresCondicionIn("1", Integer.class);
   List catalogo = cat.getListaElementos();  
	jsonObj.put("registros", catalogo);	
   infoRegresar = jsonObj.toString();
 
} else if(informacion.equals("CatalogoEPO")) {	
    JSONObject      jsonObj 	= new JSONObject();
    CatalogoEPO     cat 		= new CatalogoEPO();
                    cat.setClave("ic_epo");
                    cat.setDescripcion("cg_razon_social");
                    cat.setOrden("cg_razon_social");
    Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
    //1647 CONSTRUCTORA TERMINAL VALLE DE MEXICO, S.A. DE C.V. - PETICION URIEL VIVEROS PREPRO
    String auxIn = 
        "1647,"+
        dispersion.getEpoInfonavit() +","+ 
        application.getInitParameter("EpoEntregaAnticipadaDeVivienda")  +","+ 
        application.getInitParameter("EpoEntregaContinuaDeVivienda");
    cat.setValoresCondicionIn(auxIn, Integer.class);
    List catalogo = cat.getListaElementos();  
    jsonObj.put("registros", catalogo);	
    infoRegresar = jsonObj.toString();
} else if (        informacion.equals("Infonavit.Consultar") || informacion.equals("Infonavit.ArchivoCSV")
						|| informacion.equals("Infonavit.ArvhivoPDF")|| informacion.equals("Infonavit.Ejecutar_Flujo") )	{
		
	String epo	= (request.getParameter("epo")		== null)?"":request.getParameter("epo");
	String moneda	= (request.getParameter("moneda")		== null)?"":request.getParameter("moneda");
	String fechaOperacion	= (request.getParameter("fechaOperacion")		== null)?"":request.getParameter("fechaOperacion");
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	int iReg = 0, iNoDoctos = 0, iNoDoctosDisp = 0, iNoDoctosError = 0;
	double dImpTotal = 0, dImpTotalDisp = 0, dImpTotalError = 0;
	AccesoDB con = new AccesoDB();
	Registros reg = new Registros();
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	String query = dispersion.getQueryDispINFONAVIT("", epo, moneda, "4", fechaOperacion);
	try{
		con.conexionDB();
		reg = con.consultarDB(query);
        //System.out.println("query:"+query);
        log.debug("query:"+query);
        
		if(	informacion.equals("Infonavit.Consultar") ) {
            resultado.put("total", new Integer(reg.getNumeroRegistros()));
            log.trace("total:"+reg.getNumeroRegistros());
            resultado.put("registros", reg.getJSONData());
            log.trace("registros:"+reg.getJSONData());
			int totalDoctos = 0;			
			int totalDoctosDis = 0;
			int totalDoctosError = 0;
			BigDecimal totalMontoDis = new BigDecimal("0");
			BigDecimal totalMontoError = new BigDecimal("0");
			BigDecimal totalMonto = new BigDecimal("0");
			while(reg.next()) {
				totalDoctos++;
				totalMonto = totalMonto.add(new BigDecimal(reg.getString("FN_IMPORTE")));		
				String estatus_cec 	= reg.getString("ic_estatus_cecoban")==null?"":reg.getString("ic_estatus_cecoban").trim();
				if("99".equals(estatus_cec)) {
					totalDoctosDis+=Integer.parseInt(reg.getString("IG_TOTAL_DOCUMENTOS"));
					totalMontoDis = totalMontoDis.add(new BigDecimal(reg.getString("FN_IMPORTE")));		
				} else {
					totalDoctosError+=Integer.parseInt(reg.getString("IG_TOTAL_DOCUMENTOS"));
					totalMontoError = totalMontoError.add(new BigDecimal(reg.getString("FN_IMPORTE")));		
				}
			}
            log.trace("totalDoctosDis:"+totalDoctosDis);
			if(totalDoctosDis>0){
				resultado.put("Concepto", 	"Total por Dispersar:" 		);
				resultado.put("totalDoctos", 	new Integer(totalDoctosDis) 		);
				resultado.put("totalMonto", 	totalMontoDis.toPlainString() 		);
			}
            log.trace("totalDoctosError:"+totalDoctosError);
			if(totalDoctosError> 0){
				resultado.put("Concepto", 	"Total con Error:" 		);
				resultado.put("totalDoctos", 	new Integer(totalDoctosError) 		);
				resultado.put("totalMonto", 	totalMontoError.toPlainString() 		);
			}
			if(totalDoctosDis > 0 && totalDoctosError > 0){
				resultado.put("Concepto", 	"Suma Total:" 		);
				resultado.put("totalDoctos", 	new Integer(totalDoctos) 		);
				resultado.put("totalMonto", 	totalMonto.toPlainString() 		);
			}
			resultado.put("totalMontoSIN_ERROR", 	totalMontoDis.toPlainString() 		);
		
		}if(	informacion.equals("Infonavit.Ejecutar_Flujo")		){
			int totalDoctos = 0;			
			int totalDoctosDis = 0;
			int totalDoctosError = 0;
			BigDecimal totalMontoDis = new BigDecimal("0");
			BigDecimal totalMontoError = new BigDecimal("0");
			BigDecimal totalMonto = new BigDecimal("0");
			while(reg.next()){
				totalDoctos++;
				totalMonto = totalMonto.add(new BigDecimal(reg.getString("FN_IMPORTE")));		
				String estatus_cec 	= reg.getString("ic_estatus_cecoban")==null?"":reg.getString("ic_estatus_cecoban").trim();
				if("99".equals(estatus_cec)){
					totalDoctosDis+=Integer.parseInt(reg.getString("IG_TOTAL_DOCUMENTOS"));
					totalMontoDis = totalMontoDis.add(new BigDecimal(reg.getString("FN_IMPORTE")));		
				}else{
					totalDoctosError+=Integer.parseInt(reg.getString("IG_TOTAL_DOCUMENTOS"));
					totalMontoError = totalMonto.add(new BigDecimal(reg.getString("FN_IMPORTE")));		
				}
			}
			
			resultado.put("numeroDocumentos", 	new Integer(totalDoctos) 		);
			resultado.put("importeTotal", 	totalMonto.toPlainString() 		);
			resultado.put("numeroDocumentosError", 	new Integer(totalDoctosError) 		);
			resultado.put("importeTotalError", 	totalMontoError.toPlainString() 		);
			resultado.put("claveEPO", 	epo 		);
			resultado.put("claveEstatus", 	"4" 		);
			resultado.put("claveMoneda", 	moneda 		);
			resultado.put("fechaVencimiento", 	"" 		);
			resultado.put("claveIF", 	"" 		);
			resultado.put("claveIfFondeo", 	"" 		);
			resultado.put("origen", 	"INFONAVIT" 		);
			resultado.put("fechaOperacion", 	fechaOperacion 		);
			resultado.put("pantallaOrigen", 	"15infonavitff01Ext.jsp" 		);			
		
		}else if(		informacion.equals("Infonavit.ArchivoCSV")		){
			String tipo = (request.getParameter("tipo")		== null)?"":request.getParameter("tipo");
			StringBuffer sbContenidoTEF = new StringBuffer();
			if(tipo.equals("normal")){
				sbContenidoTEF.append("Nombre,RFC,Fecha de Operación,Total de Documentos,Moneda,Monto,Banco,Tipo de Cuenta,Número de Cuenta\r\n");
			}
			
			while(reg.next()){
			String bancos_tef 	= reg.getString("ic_bancos_tef")==null?"":reg.getString("ic_bancos_tef").trim();
			String tipo_cuenta 	= reg.getString("ic_tipo_cuenta")==null?"":reg.getString("ic_tipo_cuenta").trim();
			String cuenta		= reg.getString("cg_cuenta")==null?"":reg.getString("cg_cuenta").trim();
				if(tipo.equals("normal")){
					sbContenidoTEF.append(
							reg.getString("cg_razon_social").replace(',',' ')+","+
							reg.getString("cg_rfc")+","+
							reg.getString("df_operacion")+","+
							reg.getString("ig_total_documentos")+","+
							reg.getString("cd_nombre")+","+
							Comunes.formatoDecimal(reg.getString("fn_importe"),2,false)+","+
							bancos_tef+","+
							tipo_cuenta+",'"+
							cuenta+"\r\n");
				}else{
					String sRFC = Comunes.strtr(reg.getString("cg_rfc"),"-","").trim();
					sRFC = (sRFC.length()==13)?sRFC:sRFC.substring(0,3)+" "+sRFC.substring(3,sRFC.length());
					sbContenidoTEF.append(
							reg.getString("fechaReg")+";"+
							reg.getString("fechaTrasf")+";"+
							reg.getString("fechaApli")+";"+
							Comunes.formatoDecimal(reg.getString("fn_importe"),2,false)+";"+
							sRFC+";"+
							reg.getString("ic_bancos_tef")+";"+
							reg.getString("ic_tipo_cuenta")+";"+
							reg.getString("cg_cuenta")+";"+
							reg.getString("refRastreo")+";"+
							reg.getString("leyendaRastreo")+"\r\n");
				}
					
			}
			String nombreArchivo = "";
			String extension = (tipo.equals("normal"))?".csv":".txt";
			CreaArchivo archivo = new CreaArchivo();
			if (archivo.make(sbContenidoTEF.toString(), strDirectorioTemp, extension))
				nombreArchivo = archivo.nombre;	
				
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		
		}else if(		informacion.equals("Infonavit.ArvhivoPDF")	){
				String nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				"",//session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),//FODEA-024-2014
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
	
				pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				int columnas = 9;
				pdfDoc.setTable(columnas, 100);			
				pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Operación ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Total de Documentos","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Banco","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Número de Cuenta","celda01",ComunesPDF.CENTER);
				
				int totalDoctos = 0;			
				int totalDoctosDis = 0;
				int totalDoctosError = 0;
				BigDecimal totalMontoDis = new BigDecimal("0");
				BigDecimal totalMontoError = new BigDecimal("0");
				BigDecimal totalMonto = new BigDecimal("0");
				
				while(reg.next()){
					totalDoctos++;
					totalMonto = totalMonto.add(new BigDecimal(reg.getString("FN_IMPORTE")));		
					String estatus_cec 	= reg.getString("ic_estatus_cecoban")==null?"":reg.getString("ic_estatus_cecoban").trim();
					if("99".equals(estatus_cec)){
						totalDoctosDis+=Integer.parseInt(reg.getString("IG_TOTAL_DOCUMENTOS"));
						totalMontoDis = totalMontoDis.add(new BigDecimal(reg.getString("FN_IMPORTE")));		
					}else{
						totalDoctosError+=Integer.parseInt(reg.getString("IG_TOTAL_DOCUMENTOS"));
						totalMontoError = totalMonto.add(new BigDecimal(reg.getString("FN_IMPORTE")));		
					}
				
					pdfDoc.setCell((reg.getString("CG_RAZON_SOCIAL") == null) ? "" : reg.getString("CG_RAZON_SOCIAL"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("CG_RFC") == null) ? "" : reg.getString("CG_RFC"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("DF_OPERACION") == null) ? "" : reg.getString("DF_OPERACION"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("IG_TOTAL_DOCUMENTOS") == null) ? "" : reg.getString("IG_TOTAL_DOCUMENTOS"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("CD_NOMBRE") == null) ? "" : reg.getString("CD_NOMBRE"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("FN_IMPORTE") == null) ? "" : "$"+Comunes.formatoDecimal(reg.getString("FN_IMPORTE"),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell((reg.getString("IC_BANCOS_TEF") == null) ? "" : reg.getString("IC_BANCOS_TEF"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("IC_TIPO_CUENTA") == null) ? "" : reg.getString("IC_TIPO_CUENTA"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("CG_CUENTA") == null) ? "" : reg.getString("CG_CUENTA"),"formas",ComunesPDF.CENTER);
				}
				
				if(totalDoctosDis>0){
					pdfDoc.setCell("Total por Dispersar:","celda01",ComunesPDF.CENTER,3);
					pdfDoc.setCell(totalDoctosDis+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMonto.toPlainString(),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,3);
				}
				if(totalDoctosError> 0){
					pdfDoc.setCell("Total con Error:","celda01",ComunesPDF.RIGHT,3);
					pdfDoc.setCell(totalDoctosError+"","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoError.toPlainString(),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,3);
				}
				if(totalDoctosDis > 0 && totalDoctosError > 0){
					pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,9);
					pdfDoc.setCell("Suma Total:","celda01",ComunesPDF.RIGHT,3);
					pdfDoc.setCell(totalDoctos+"","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMonto.toPlainString(),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,3);
				}
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
				resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				
		}
		
	}catch(Exception e){
		msg="Error: "+e;
		e.printStackTrace();
	}finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();	
		} 
	}
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>