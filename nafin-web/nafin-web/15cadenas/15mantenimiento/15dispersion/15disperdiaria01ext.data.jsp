<%@ page 
	contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoIFMonitorDispersion,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("DispersionIF.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// Obtener la fecha de hoy
	SimpleDateFormat sdf	 	= new SimpleDateFormat("dd/MM/yyyy");
	String fechaHoy 			= sdf.format(new java.util.Date());
	
	String fechaRegistro 	= fechaHoy;
	String fechaConsultada 	= "";
	
	resultado.put("fechaHoy",			fechaHoy			);
	resultado.put("fechaRegistro",	fechaRegistro	);
	resultado.put("fechaConsultada",	fechaConsultada);
	
	// Actualizar forma
	estadoSiguiente = "MOSTRAR_FORMA_CONSULTA";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if(		informacion.equals("CatalogoIF")  ){
	
	CatalogoIFMonitorDispersion cat = new CatalogoIFMonitorDispersion();
	cat.setCampoClave("ic_if"); 
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setProducto("1"); 
	cat.setDispersion("S"); 
	cat.setOrden("cg_razon_social");
	
	infoRegresar    = cat.getJSONElementos();
	
} else if(		informacion.equals("CatalogoEPO")  ){

	String claveIF = (request.getParameter("claveIF") == null)?"":request.getParameter("claveIF");
	
	if( "".equals(claveIF) ){
		
		infoRegresar = "{\"success\": true, \"total\": \"0\", \"registros\": [] }";
		
	} else {
		
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setClaveIf(claveIF); 
		cat.setHabilitado("S");
		cat.setOrden("cg_razon_social");
		List listaElementos = cat.getListaElementosMonitorDispersionIF();					
		infoRegresar        = cat.getJSONElementos(listaElementos);
	
	}

} else if(		informacion.equals("CatalogoMoneda")  ){

	String claveIF  = (request.getParameter("claveIF")	 == null)?"":request.getParameter("claveIF");
	String claveEPO = (request.getParameter("claveEPO") == null)?"":request.getParameter("claveEPO");
 
	String 	monedas  	= (!claveEPO.equals("")?"":"1,54");
	
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión.";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
 
	}
	
	if(!claveEPO.equals("")){
		
		if(dispersion.hasTarifaDispersionMonedaNacional(claveEPO)){
			monedas += (!monedas.equals(""))?",":"";
			monedas += "1";
		}
		
		if(dispersion.hasTarifaDispersionDolaresAmericanos(claveEPO)){
			monedas += (!monedas.equals(""))?",":"";
			monedas += "54";	
		}
		
	}
					
	if(!claveEPO.equals("") && monedas.equals("")){ // Se especifico una epo, pero no tiene ninguna moneda parametrizada
		monedas = "-1";// Para que no traiga ninguna moneda valida
	}
					
	if( claveIF.equals("") || claveEPO.equals("") ){
		monedas    = "-1";
	}
 
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setCondicionIn( monedas, "String.class");
	cat.setOrden("1");

	infoRegresar        = cat.getJSONElementos();
	
} else if(		informacion.equals("DispersionIF.mostrarFormaConsulta")  ){

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(		informacion.equals("DispersionIF.consultaDispersionFISOS")  ){

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Leer parametros
	String claveIF 			= (request.getParameter("claveIF")				== null)?"":request.getParameter("claveIF");
	String claveEPO 			= (request.getParameter("claveEPO")				== null)?"":request.getParameter("claveEPO");
	String claveMoneda 		= (request.getParameter("claveMoneda")			== null)?"":request.getParameter("claveMoneda");
	String fechaRegistro 	= (request.getParameter("fechaRegistro")		== null)?"":request.getParameter("fechaRegistro");
	String fechaHoy 			= (request.getParameter("fechaHoy")				== null)?"":request.getParameter("fechaHoy");
	String fechaConsultada 	= fechaRegistro;
	
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	} catch(Exception e) {
 
		msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión.";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
 
	}
	
	// Realizar consulta
	HashMap	consulta 			= dispersion.consultaDispersionFISOS( claveEPO, claveMoneda, claveIF, fechaRegistro );
	List 		registros			= (List) 	consulta.get("registros");
	List		registrosTotales	= (List) 	consulta.get("registrosTotales");
	HashMap	detalleTotales		= (HashMap) consulta.get("detalleTotales");
	
	Integer  numeroDocumentosDispersion	= (Integer) detalleTotales.get("numeroDocumentosDispersion");
	Integer 	numeroRegistros    			= (Integer) detalleTotales.get("numeroRegistros");
		
	// Determinar que botones podran ser mostrados
	boolean showBotonEjecutarInterfaseFlujoFondos = false;
	boolean showBotonExportarArchivoTEF				 = false;
	boolean showBotonGeneraArchivoTEF				 = false;
	boolean showBotonGeneraPDF							 = false;
	
	if( numeroRegistros.intValue() > 0 ) {
				
		try {
			
			// Solo se valida el horario cuando la dispersión es para el día de hoy.
			if( (new java.util.Date()).compareTo(Comunes.parseDate(fechaRegistro)) == 0){
				Horario.validarHorarioDispersion(1, claveEPO, claveIF);
			}

			if( numeroDocumentosDispersion.intValue() > 0 ){
				showBotonEjecutarInterfaseFlujoFondos = true;
			}
				
			if( !claveMoneda.equals("54") ){ 
				showBotonExportarArchivoTEF 		  = true;
				showBotonGeneraArchivoTEF	 		  = true;
			}
					
			showBotonGeneraPDF 						  = true;
		
		} catch( NafinException ne ) {
					
			msg = "Hay operaciones en proceso y/o no sea cerrado la operación de la PYME.";
				
		} //NafinException
		
	} // numeroRegistros >0
		
	// Enviar resultados
	
	JSONObject params = new JSONObject();
	params.put("claveIF",			claveIF			);
	params.put("claveEPO",			claveEPO			);
	params.put("claveMoneda",		claveMoneda		);
	params.put("fechaRegistro",	fechaRegistro	);
	params.put("fechaHoy",			fechaHoy			);
	params.put("fechaConsultada",	fechaConsultada);
	resultado.put("params", 		params 			);
	
	JSONObject dispersionFISOSData = new JSONObject();
	dispersionFISOSData.put("success",				new Boolean(true)							);
	dispersionFISOSData.put("total",					new Integer(registros.size())			);
	dispersionFISOSData.put("registros",			registros									);
	resultado.put( "dispersionFISOSData",			dispersionFISOSData 						);
	
	JSONObject totalDispersionFISOSData = new JSONObject();
	totalDispersionFISOSData.put("success",		new Boolean(true)							);
	totalDispersionFISOSData.put("total",			new Integer(registrosTotales.size()));
	totalDispersionFISOSData.put("registros",		registrosTotales							);
	resultado.put( "totalDispersionFISOSData",	totalDispersionFISOSData 				);

	resultado.put( "detalleTotales", JSONObject.fromObject(detalleTotales) ); /* numeroDocumentosDispersion, numeroRegistros */
		
	resultado.put("showBotonEjecutarInterfaseFlujoFondos",	new Boolean(showBotonEjecutarInterfaseFlujoFondos)	);
	resultado.put("showBotonExportarArchivoTEF",					new Boolean(showBotonExportarArchivoTEF)				);
	resultado.put("showBotonGeneraArchivoTEF",					new Boolean(showBotonGeneraArchivoTEF)					);
	resultado.put("showBotonGeneraPDF",								new Boolean(showBotonGeneraPDF)							);
	
	// Determinar el estado siguiente
	estadoSiguiente = "MOSTRAR_CONSULTA_DISPERSION_FISOS";
 	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(		informacion.equals("DispersionIF.mostrarConsultaDispersionFISOS")  ){
		
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("DispersionIF.ejecutarInterfaseFlujoFondos") ){

	JSONObject	resultado 						= new JSONObject();
	boolean		success	 						= true;
	String 		estadoSiguiente 				= null;
	String		msg								= null;
	String 		destino							= null;
	
	// 1. Leer parametros
	String 		claveIF 							= (request.getParameter("claveIF")							== null)?"":request.getParameter("claveIF");
	String 		claveEPO 						= (request.getParameter("claveEPO")							== null)?"":request.getParameter("claveEPO");
	String 		claveMoneda 					= (request.getParameter("claveMoneda")						== null)?"":request.getParameter("claveMoneda");
	String 		fechaRegistro 					= (request.getParameter("fechaRegistro")					== null)?"":request.getParameter("fechaRegistro");
	String 		numeroDocumentosDispersion = (request.getParameter("numeroDocumentosDispersion")	== null)?"":request.getParameter("numeroDocumentosDispersion");
	String 		importeTotalDispersion 		= (request.getParameter("importeTotalDispersion")		== null)?"":request.getParameter("importeTotalDispersion");
	String 		numeroDocumentosError 		= (request.getParameter("numeroDocumentosError")		== null)?"":request.getParameter("numeroDocumentosError");
	String 		importeTotalError 			= (request.getParameter("importeTotalError")				== null)?"":request.getParameter("importeTotalError");
		
	String 		fechaVencimiento				= "";
	String 		claveEstatus					= "";
	String 		fechaOperacion					= "";
	
	// 2. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	} catch(Exception e) {
 
		msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión.";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
 
	}
	
	// 3. Determinar el estado siguiente			
	if( claveEPO == null || !dispersion.hasEstatusDispersionOperada(claveEPO) ){
			
		msg 				 = "La EPO no tiene parametrizado el Servicio de Dispersion con Estatus: Operada.";
		// Determinar el estado siguiente
		estadoSiguiente = "ESPERAR_DECISION";
		destino			 = "";
		
	} else if( dispersion.existenOperaciones( claveEPO, claveMoneda, fechaVencimiento, claveIF, claveEstatus, fechaOperacion) ){
		
		msg 				 = "Ya fueron registradas estas operaciones.";
		// Determinar el estado siguiente
		estadoSiguiente = "ESPERAR_DECISION";
		destino			 = "";
		
	} else {
		
		// Reformatear los parametros que serán pasados a exportar interfase
		JSONObject formParams = new JSONObject();
		formParams.put("claveIF",					claveIF 							);
		formParams.put("claveEPO",					claveEPO 						);
		formParams.put("claveMoneda",				claveMoneda 					);
		formParams.put("fechaRegistro",			fechaRegistro 					);
		formParams.put("numeroDocumentos",		numeroDocumentosDispersion	);
		formParams.put("importeTotal",			importeTotalDispersion		);
		formParams.put("numeroDocumentosError",numeroDocumentosError		);
		formParams.put("importeTotalError",		importeTotalError				);
		formParams.put("origen",					"FISOS"							);
		formParams.put("pantallaOrigen",			"15disperdiaria01ext.jsp"	);
		
		resultado.put("formParams",				formParams.toString()		);
	
		estadoSiguiente = "FIN";
		destino			 = "15exportarinterfaseff01ext.jsp";
		
	}
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)			);
	resultado.put("estadoSiguiente", 	estadoSiguiente 				);
	resultado.put("msg",						msg								);
	resultado.put("destino",				destino							);
	infoRegresar = resultado.toString();
 
} else if( informacion.equals("GenerarArchivo") ){

	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	String      urlArchivo		= "";
	
	String 		nombreArchivo	= "";
	
	// Leer parametros
	String claveIF 		= (request.getParameter("claveIF")			== null)?"":request.getParameter("claveIF");
	String claveEPO 		= (request.getParameter("claveEPO")			== null)?"":request.getParameter("claveEPO");
	String claveMoneda 	= (request.getParameter("claveMoneda")		== null)?"":request.getParameter("claveMoneda");
	String fechaRegistro = (request.getParameter("fechaRegistro")	== null)?"":request.getParameter("fechaRegistro");
	String tipo 			= (request.getParameter("tipo") 				== null)?"":request.getParameter("tipo");
	
	// Preparar clases adicionales
	ConsultaDispersionFISOS consulta = new ConsultaDispersionFISOS();
	
	// HttpSession session
	String			directorioTemporal		= strDirectorioTemp; 
	String 			directorioPublicacion 	= strDirectorioPublicacion;
	
	// Crear archivo
	if(        "PDF".equals(tipo) ){
		nombreArchivo = consulta.generaArchivoPDF( directorioTemporal, directorioPublicacion, claveIF, claveEPO, claveMoneda, fechaRegistro, session );
		urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
	}
 
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();

} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>