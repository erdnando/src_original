Ext.onReady(function() {
		
			
	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			// w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth, hidden ? 0 : dom.clientWidth || me.getComputedWidth() ) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});	
	
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaDispersionIF = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						dispersionIF(resp.estadoSiguiente,resp);
					}
				);
			} else {
				dispersionIF(resp.estadoSiguiente,resp);
			}
			
		}else{
					
			var element = null;
		
			// Suprimir mascaras segun se requiera
			element = Ext.getCmp("formaConsulta").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Resetear boton Ejecutar Interfase Flujo Fondos en caso sea necesario
			element = Ext.getCmp("btnEjecutarInterfaseFlujoFondos");
			if( element.isVisible() && element.disabled ){
				element.enable();
				element.setIconClass('icoEjecutar');
			}
				
			/*
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Resetear forma
         resetWindowConfirmacionClave();
 
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			*/
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
					
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var dispersionIF = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"							   ){
			
			// Cargar Catalogo
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15disperdiariaf01ext.data.jsp',
				params: 	{
					informacion:		'DispersionIF.inicializacion'
				},
				callback: 				procesaDispersionIF
			});
 	
		} else if(			estado == "MOSTRAR_FORMA_CONSULTA"  			  ){ 
			
			// CARGAR CONTENIDO DE LOS CAMPOS
			
			// Cargar Catalogo IF Fideicomiso
			var catalogoIfFideicomisoData 	= Ext.StoreMgr.key('catalogoIfFideicomisoDataStore');
			catalogoIfFideicomisoData.load();
			// Definir Fecha de Registro
			var fechaRegistro 				= Ext.getCmp("fechaRegistro"); 
			fechaRegistro.setValue(respuesta.fechaRegistro);
			fechaRegistro.originalValue 	= fechaRegistro.getValue();
			// Definir Fecha de Hoy
			var fechaHoy      				= Ext.getCmp("fechaHoy");
			fechaHoy.setValue(respuesta.fechaHoy);
			fechaHoy.originalValue 			= fechaHoy.getValue();
			// Definir Fecha Consultada
			var fechaConsultada 				= Ext.getCmp("fechaConsultada");
			fechaConsultada.setValue(respuesta.fechaConsultada);
			fechaConsultada.originalValue = fechaConsultada.getValue();
 
			// MOSTRAR FORMA DE CONSULTA
			Ext.getCmp("formaConsulta").show();
			
			// DETERMINAR EL ESTADO SIGUIENTE
			Ext.Ajax.request({
				url: 						'15disperdiariaf01ext.data.jsp',
				params: 	{
					informacion:		'DispersionIF.mostrarFormaConsulta'
				},
				callback: 				procesaDispersionIF
			});
 
		} else if(			estado == "ESPERAR_DECISION"  					  ){ 
			
			var element = null;
		
			// Suprimir mascaras segun se requiera
			element = Ext.getCmp("formaConsulta").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Resetear boton Ejecutar Interfase Flujo Fondos en caso sea necesario
			element = Ext.getCmp("btnEjecutarInterfaseFlujoFondos");
			if( element.isVisible() && element.disabled ){
				element.enable();
				element.setIconClass('icoEjecutar');
			}
			
			/*
 
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
			
         // Resetear forma
         resetWindowConfirmacionClave();
         
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			*/
			
		} else if(			estado == "CONSULTA_DISPERSION_IF_FIDEICOMISO"  		  ){ 
			
			// Agregar mascara para indicar que se est� realizando una consulta
			var formaConsulta = Ext.getCmp('formaConsulta');
			formaConsulta.getEl().mask('Consultando...','x-mask-loading');
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 					 '15disperdiariaf01ext.data.jsp',
				params: Ext.apply(
					formaConsulta.getForm().getValues(),
					{
						informacion: 'DispersionIF.consultaDispersionIfFideicomiso'
					}
				),
				callback: 			 procesaDispersionIF
			});
			
		} else if(			estado == "MOSTRAR_CONSULTA_DISPERSION_IF_FIDEICOMISO"  ){ 
			
			var gridEl;
			var gridDispersionIfFideicomiso 		= Ext.getCmp("gridDispersionIfFideicomiso");
			var gridTotalDispersionIfFideicomiso	= Ext.getCmp("gridTotalDispersionIfFideicomiso");
			
			// SI HAY UNA M�SCARA PREVIA EN EL GRID, SUPRIMIRLA
			gridEl = gridDispersionIfFideicomiso.getGridEl();
			if( gridEl.isMasked() ){
				gridEl.unmask();
			}
			
			// SI HAY UNA M�SCARA PREVIA EN EL GRID DE TOTALES, SUPRIMIRLA
			gridEl = gridTotalDispersionIfFideicomiso.getGridEl();
			if( gridEl.isMasked() ){
				gridEl.unmask();
			}
	
			// CARGAR CONSULTA
			var dispersionIfFideicomisoData = Ext.StoreMgr.key('dispersionIfFideicomisoDataStore');
			dispersionIfFideicomisoData.loadData(respuesta.dispersionIfFideicomisoData);
			
			// CARGAR TOTALES DE LA CONSULTA
			var totalDispersionIfFideicomisoData = Ext.StoreMgr.key('totalDispersionIfFideicomisoDataStore');
			totalDispersionIfFideicomisoData.loadData(respuesta.totalDispersionIfFideicomisoData); 

			// Ajustar altura del Grid de Totales
			if( totalDispersionIfFideicomisoData.getTotalCount() 		 == 0 ){
				gridTotalDispersionIfFideicomiso.setHeight(78);
			} else if( totalDispersionIfFideicomisoData.getTotalCount() == 1 ){
				gridTotalDispersionIfFideicomiso.setHeight(96);
			} else if( totalDispersionIfFideicomisoData.getTotalCount() == 2 ){
				gridTotalDispersionIfFideicomiso.setHeight(117);
			} else {
				gridTotalDispersionIfFideicomiso.setHeight(138);
			}
			
			// COPIAR EN EL GRID LOS PARAMETROS DE LA ULTIMA CONSULTA
			gridTotalDispersionIfFideicomiso.formParams 	  = Ext.apply({},respuesta.params);
			gridTotalDispersionIfFideicomiso.detalleTotales  = Ext.apply({},respuesta.detalleTotales);

			// MOSTRAR / OCULTAR TOOLBAR DE BOTONES DEL GRID DE DETALLE DE DISPERSION FISOS
			if( 
				respuesta.showBotonGeneraArchivoErrores
					||
				respuesta.showBotonEjecutarInterfaseFlujoFondos
					||
				respuesta.showBotonExportarArchivoTEF
					||
				respuesta.showBotonGeneraArchivoTEF
					||
				respuesta.showBotonGeneraPDF 
			){
				Ext.getCmp("toolbarGridTotalDispersionIfFideicomiso").show();
			} else {
				Ext.getCmp("toolbarGridTotalDispersionIfFideicomiso").hide();
			}
			
			// CONFIGURAR BOTONES DE ACUERDO A LA CONSULTA
			var btnGeneraArchivoErrores 			= Ext.getCmp("btnGeneraArchivoErrores");
			var btnBajaArchivoErrores				= Ext.getCmp("btnBajaArchivoErrores");
			var btnEjecutarInterfaseFlujoFondos = Ext.getCmp("btnEjecutarInterfaseFlujoFondos");
			var btnExportarArchivoTEF 				= Ext.getCmp("btnExportarArchivoTEF");
			var btnGeneraArchivoTEF 				= Ext.getCmp("btnGeneraArchivoTEF");
			var btnBajaArchivoTEF 					= Ext.getCmp("btnBajaArchivoTEF");
			var btnGeneraPDF 							= Ext.getCmp("btnGeneraPDF");
			var btnBajaPDF 							= Ext.getCmp("btnBajaPDF");

			// Actualizar propiedades del grid panel	
			if( respuesta.showBotonGeneraArchivoErrores ){
				btnGeneraArchivoErrores.show();
				btnGeneraArchivoErrores.enable();
				btnGeneraArchivoErrores.setIconClass('icoGenerarArchivo');
			} else {
				btnGeneraArchivoErrores.hide();
			}
			
			if( respuesta.showBotonEjecutarInterfaseFlujoFondos ){
				btnEjecutarInterfaseFlujoFondos.show();
				btnEjecutarInterfaseFlujoFondos.enable();
				btnEjecutarInterfaseFlujoFondos.setIconClass('icoEjecutar');
			} else {
				btnEjecutarInterfaseFlujoFondos.hide();
			}
			
			/*
			// NOTA: Esta funcionalidad a quedado deprecada, 02/08/2013 01:33:29 p.m.
			if( respuesta.showBotonExportarArchivoTEF ){
				btnExportarArchivoTEF.show();
			} else {
				btnExportarArchivoTEF.hide();
			}
			*/
			
			/*
			// NOTA: Esta funcionalidad a quedado deprecada, 02/08/2013 01:33:29 p.m.
			if( respuesta.showBotonGeneraArchivoTEF   ){
				btnGeneraArchivoTEF.show();
				btnGeneraArchivoTEF.enable();
				btnGeneraArchivoTEF.setIconClass('icoGenerarArchivo');
			} else {
				btnGeneraArchivoTEF.hide();
			}
			*/
			
			if( respuesta.showBotonGeneraPDF   			){
				btnGeneraPDF.show();
				btnGeneraPDF.enable();
				btnGeneraPDF.setIconClass('icoGenerarArchivo');
			} else {
				btnGeneraPDF.hide();
			}
				
			btnBajaArchivoErrores.hide();
			btnBajaArchivoTEF.hide();
			btnBajaPDF.hide();
 
			// DETERMINAR EL ESTADO SIGUIENTE
			Ext.Ajax.request({
				url: 						'15disperdiariaf01ext.data.jsp',
				params: 	{
					informacion:		'DispersionIF.mostrarConsultaDispersionIfFideicomiso'
				},
				callback: 				procesaDispersionIF
			});
			
		} else if(			estado == "EJECUTAR_INTERFASE_FLUJO_FONDOS"    ){
				
			var gridDispersionIfFideicomiso 		= Ext.getCmp("gridDispersionIfFideicomiso");
			var gridTotalDispersionIfFideicomiso 	= Ext.getCmp("gridTotalDispersionIfFideicomiso");
			
			// Agregar mascara para indicar que se est� realizando la operacion		
			var btnEjecutarInterfaseFlujoFondos = Ext.getCmp("btnEjecutarInterfaseFlujoFondos");
			btnEjecutarInterfaseFlujoFondos.disable();
			btnEjecutarInterfaseFlujoFondos.setIconClass('loading-indicator');
			
			var formParams = Ext.apply( {},			 gridTotalDispersionIfFideicomiso.formParams		);
			formParams		= Ext.apply( formParams, gridTotalDispersionIfFideicomiso.detalleTotales	);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 					 '15disperdiariaf01ext.data.jsp',
				params: Ext.apply(
					formParams,
					{
						informacion:  'DispersionIF.ejecutarInterfaseFlujoFondos'
					}
				),
				callback: 			 procesaDispersionIF
			});
 
		} else if(			estado == "FIN"  					                 ){
 	
			var destino 	= !Ext.isEmpty(respuesta.destino)?respuesta.destino:"15disperdiariaf01ext.jsp";
 
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			
			// Agregar parametros de forma dinamica
			if( Ext.isDefined(respuesta.formParams) ){

				Ext.iterate(
					respuesta.formParams, 
					function( key, value ){
						Ext.DomHelper.insertFirst(
							forma, 
							{ 
								tag: 		'input', 
								type: 	'hidden', 
								id: 		key, 
								name: 	key, 
								value: 	value
							}
						); 
					}
				);
		
			}
				
			forma.submit();
						
		}
		
	}
 
	//-------------------------- 3. GRID DETALLE TOTALES DISPERSION FISOS ----------------------------------
	var procesarSuccessFailureGeneraArchivoErrores = function(opts, success, response) {
		
		var btnGeneraArchivoErrores = Ext.getCmp('btnGeneraArchivoErrores');
		btnGeneraArchivoErrores.setIconClass('icoGenerarArchivo');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaArchivoErrores = Ext.getCmp('btnBajaArchivoErrores');
			btnBajaArchivoErrores.show();
			btnBajaArchivoErrores.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaArchivoErrores.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
			
		} else {
			
			btnGeneraArchivoErrores.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarSuccessFailureGeneraPDF = function(opts, success, response) {
		
		var btnGeneraPDF = Ext.getCmp('btnGeneraPDF');
		btnGeneraPDF.setIconClass('icoGenerarArchivo');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaPDF = Ext.getCmp('btnBajaPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
			
		} else {
			
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarTotalDispersionIfFideicomisoData = function(store, registros, opts){
 
		var gridTotalDispersionIfFideicomiso = Ext.getCmp('gridTotalDispersionIfFideicomiso');

		if (registros != null) {
 
			if (!gridTotalDispersionIfFideicomiso.isVisible()) {
				gridTotalDispersionIfFideicomiso.show();
			}
 
			var el 					 		 = gridTotalDispersionIfFideicomiso.getGridEl();
						
			// Actualizar propiedades del grid panel
			if(store.getTotalCount() > 0) {

				el.unmask();
				
			} else {

				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
 
		}
					
	}
	
	var procesaGeneraArchivoErrores = function(boton, evento) {
						
		boton.disable();
		boton.setIconClass('loading-indicator');
						
		var gridTotalDispersionIfFideicomiso 	= Ext.getCmp("gridTotalDispersionIfFideicomiso");				
		var formParams 					= Ext.apply( {}, gridTotalDispersionIfFideicomiso.formParams );
		
		// Generar Archivo de Errores
		Ext.Ajax.request({
			url: 		'15disperdiariaf01ext.data.jsp',
			params: 	Ext.apply(
				formParams, 
				{
					informacion: 'GenerarArchivoErrores',
					tipo: 		 'CSV'
				}
			),
			callback: procesarSuccessFailureGeneraArchivoErrores
		});

	}
	
	var procesaEjecutarInterfaseFlujoFondos = function(boton, evento){

		var gridTotalDispersionIfFideicomiso = Ext.getCmp("gridTotalDispersionIfFideicomiso");		
		
		var fechaRegistroDate	= gridTotalDispersionIfFideicomiso.formParams.fechaRegistro; // Date.parseDate( gridTotalDispersionIfFideicomiso.formParams.fechaRegistro, "d/m/Y", true );
		var fechaHoyDate			= gridTotalDispersionIfFideicomiso.formParams.fechaHoy;      // Date.parseDate( gridTotalDispersionIfFideicomiso.formParams.fechaHoy,      "d/m/Y", true );
		if( fechaRegistroDate   !== fechaHoyDate ){
			Ext.Msg.alert("Mensaje","Para poder Ejecutar Interfase flujo de Fondos con las operaciones del d�a\n es necesario que la Fecha de Registro sea del d�a de Hoy.");
			return;
		}

		var fechaConsultadaDate	= gridTotalDispersionIfFideicomiso.formParams.fechaConsultada; // Date.parseDate( gridTotalDispersionIfFideicomiso.formParams.fechaConsultada, "d/m/Y", true );
		//var fechaHoyDate			= Date.parseDate( gridTotalDispersionIfFideicomiso.formParams.fechaHoy,     "d/m/Y", true );
		if( fechaConsultadaDate !== fechaHoyDate ){
			Ext.Msg.alert("Mensaje","Debe de realizar primero la Consulta correspondiente a este d�a.");
			return;
		}
		
		/*		
		if(parseInt(f.numRegistros.value) == parseInt(f.CtaNoValida.value)){
			alert("No existe registros con Numeros de Cuenta validos\npara realizar la dispersi�n.");
			return;
		}
		*/
		
		dispersionIF("EJECUTAR_INTERFASE_FLUJO_FONDOS");
 
	}
	
	var procesaExportarArchivoTEF = function(boton, evento) {
						
		throw "Esta funcionalidad ha sido deprecada, 02/08/2013 01:45:02 p.m.";
		
	}
			
	var procesaGeneraArchivoTEF = function(boton, evento) {
						
		throw "Esta funcionalidad ha sido deprecada, 02/08/2013 01:45:20 p.m.";
		
	}
	
	var procesaGeneraPDF = function(boton, evento) {
						
		boton.disable();
		boton.setIconClass('loading-indicator');
						
		var gridTotalDispersionIfFideicomiso 	= Ext.getCmp("gridTotalDispersionIfFideicomiso");				
		var formParams 					= Ext.apply( {}, gridTotalDispersionIfFideicomiso.formParams );
		
		// Generar Archivo PDF
		Ext.Ajax.request({
			url: 		'15disperdiariaf01ext.data.jsp',
			params: 	Ext.apply(
				formParams, 
				{
					informacion: 'GenerarArchivo',
					tipo: 		 'PDF'
				}
			),
			callback: procesarSuccessFailureGeneraPDF
		});

	}
 
	var rfcRenderer01 				= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;text-align:right;';
		metadata.attr += '" ';
		return value;
		
	}
	
	var nombreProveedorRenderer01	= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
		
	}
	
	var totalDocumentosRenderer01	= function( value, metadata, record, rowIndex, colIndex, store){
		
		return value;
		
	}
	
	var monedaRenderer01				= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
		
	}
	
	var montoRenderer01				= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="text-align:right;';
		metadata.attr += '" ';
		value = record.json['MONTO'];
		return value;
		
	}
	
	var bancoRenderer01 				= function( value, metadata, record, rowIndex, colIndex, store){
		
		return value;
		
	}
	
	var tipoCuentaRenderer01		= function( value, metadata, record, rowIndex, colIndex, store){
		
		return value;
		
	}
	
	var numeroCuentaRenderer01		= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
		
	}
 
	var errorRenderer01 				= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
		
	}
	
	// Crear JsonStore que se encargar� de cargar el detalle
	var totalDispersionIfFideicomisoData = new Ext.data.JsonStore({
		id:							'totalDispersionIfFideicomisoDataStore',
		name:							'totalDispersionIfFideicomisoDataStore',
		root:							'registros',
		url: 							'15disperdiariaf01ext.data.jsp',
		autoDestroy:				true,
		autoLoad: 					false,
		totalProperty: 			'total',
		messageProperty: 			'msg',
		pruneModifiedRecords: 	true, // Para que no se conserven los registros modificados en consulta anterior.
		baseParams: {
			informacion: 	'ConsultaTotalDispersionIfFideicomiso'
		},			
		fields: [
			{ name:'RFC',					type:"string" }, 
			{ name:'NOMBRE_PROVEEDOR',	type:"string" },
			{ name:'TOTAL_DOCUMENTOS',	type:"int" 	  },
			{ name:'MONEDA',				type:"string" },
			{ name:'MONTO',				type:"float", convert: function(value, record){ return Number(value.replace(/[\$,]/g,"")); } },
			{ name:'BANCO',				type:"string" },
			{ name:'TIPO_CUENTA',		type:"string" },
			{ name:'NUMERO_CUENTA',		type:"string" },
			{ name:'ESTATUS_CECOBAN',	type:"string" },
			{ name:'ERROR',				type:"string" }
		],
		listeners: { 
			beforeload: {
				fn: function( store, opts ) {
					var grid = Ext.getCmp('gridTotalDispersionIfFideicomiso');
					if (grid) {
						grid.show();
					} 			
				}
			},
			load: 	procesarTotalDispersionIfFideicomisoData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarTotalDispersionIfFideicomisoData(null, null, null);						
				}
			}
		}
	});
 
	var gridTotalDispersionIfFideicomiso = {
		frame:				true,
		store: 				totalDispersionIfFideicomisoData,
		//title:				'Detalle',
		xtype: 				'grid',
		id:					'gridTotalDispersionIfFideicomiso',
		hideHeaders:		true,
		hidden:				true,
		stripeRows: 		true,
		loadMask: 			true,
		width: 				940, 
		height:				138,
		region: 				'center',
		style: 				'margin: 0 auto',
		formParams:			{},
		detalleTotales:	{},
		// plugins: totalesGridSummary,
		columns: [
			{
				header: 		'RFC<br>&nbsp;',
				tooltip: 	'RFC',
				dataIndex: 	'RFC',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		68+134,
				hidden: 		false,
				renderer:	rfcRenderer01
			},
			/*
			{
				header: 		'Nombre del<br>Proveedor',
				tooltip: 	'Nombre del Proveedor',
				dataIndex: 	'NOMBRE_PROVEEDOR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	nombreProveedorRenderer01
			},
			*/
			{
				header: 		'Total de<br>Documentos',
				tooltip: 	'Total de Documentos',
				dataIndex: 	'TOTAL_DOCUMENTOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		80,
				hidden: 		false,
				renderer:	totalDocumentosRenderer01
			},
			{
				header: 		'Moneda<br>&nbsp;',
				tooltip: 	'Moneda',
				dataIndex: 	'MONEDA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		74,
				hidden: 		false,
				renderer:	monedaRenderer01
			},
			{
				header: 		'Monto<br>&nbsp;',
				tooltip: 	'Monto',
				dataIndex: 	'MONTO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	montoRenderer01
			},
			{
				header: 		'Banco<br>&nbsp;',
				tooltip: 	'Banco',
				dataIndex: 	'BANCO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		50,
				hidden: 		false,
				renderer:	bancoRenderer01
			},
			{
				header: 		'Tipo de<br>Cuenta',
				tooltip: 	'Tipo de Cuenta',
				dataIndex: 	'TIPO_CUENTA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		50,
				hidden: 		false,
				renderer:	tipoCuentaRenderer01
			},
			{
				header: 		'N&uacute;mero de<br>Cuenta',
				tooltip: 	'N&uacute;mero de Cuenta',
				dataIndex: 	'NUMERO_CUENTA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	numeroCuentaRenderer01
			},
			{
				header: 		'Error',
				tooltip: 	'Error',
				dataIndex: 	'ERROR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	errorRenderer01
			}
		],
		stateful:	false,
		bbar: {
			xtype: 			'toolbar',
			id:				'toolbarGridTotalDispersionIfFideicomiso',
			buttonAlign: 	'left',
			height:			60,
			items: [
				'->',
				// BUTTON: Generar Archivo de Errores
				{
					xtype: 	 	'button',
					text: 	 	'Generar Archivo<br>de Errores',
					id: 		 	'btnGeneraArchivoErrores',
					iconCls:  	'icoGenerarArchivo',
					// disabled:true,
					hidden:		true,
					handler: 	 procesaGeneraArchivoErrores,
					scale: 		'small',
               iconAlign: 	'top'
				},
				// BUTTON: Bajar Archivo de Errores
				{
					xtype: 	 	'button',
					text: 	 	'Bajar Archivo<br>de Errores',
					id: 		 	'btnBajaArchivoErrores',
					iconCls:	 	'icoBotonXLS',
					hidden: 	 	true,
					scale: 		'small',
               iconAlign: 	'top'
				},
				// BUTTON: Ejecutar Interfase Flujo de Fondos
				{
					xtype: 	 	'button',
					text: 	 	'Ejecutar Interfase<br>Flujo de Fondos',
					id: 		 	'btnEjecutarInterfaseFlujoFondos',
					iconCls:  	'icoEjecutar',
					// disabled:true,
					hidden:		true,
					handler:  	procesaEjecutarInterfaseFlujoFondos,
					scale: 		'small',
               iconAlign: 	'top'
				},
				// BUTTON: Exportar Archivo TEF
				// NOTA: Esta funcionalidad a quedado deprecada, 02/08/2013 01:31:17 p.m.
				{
					xtype: 	 	'button',
					text: 	 	'Exportar<br>Archivo TEF',
					id: 		 	'btnExportarArchivoTEF',
					iconCls:  	'icoExportar',
					// disabled:true,
					hidden:		true,
					handler:  	procesaExportarArchivoTEF,
					scale: 		'small',
               iconAlign: 	'top'
				},
				// BUTTON: Generar Archivo TEF
				// NOTA: Esta funcionalidad a quedado deprecada, 
				{
					xtype: 	 	'button',
					text: 	 	'Generar<br>Archivo TEF',
					id: 		 	'btnGeneraArchivoTEF',
					iconCls:  	'icoGenerarArchivo',
					// disabled:true,
					hidden:		true,
					handler:  	procesaGeneraArchivoTEF,
					scale: 		'small',
               iconAlign: 	'top'
				},
				// BUTTON: BAJAR ARCHIVO TEF
				// NOTA: Esta funcionalidad a quedado deprecada, 
				{
					xtype: 	 	'button',
					text: 	 	'Bajar<br>Archivo TEF',
					id: 		 	'btnBajaArchivoTEF',
					iconCls:	 	'icoTexto',
					hidden: 		true,
					scale: 		'small',
               iconAlign: 	'top'
				},
				// BUTTON: Imprimir
				{
					xtype: 	 	'button',
					text: 	 	'Generar<br>Archivo PDF',
					id: 		 	'btnGeneraPDF',
					iconCls:  	'icoGenerarArchivo',
					// disabled:true,
					hidden:		true,
					handler: 	 procesaGeneraPDF,
					scale: 		'small',
               iconAlign: 	'top'
				},
				// BUTTON: BAJAR ARCHIVO PDF
				{
					xtype: 	 	'button',
					text: 	 	'Bajar<br>Archivo PDF',
					id: 		 	'btnBajaPDF',
					iconCls:	 	'icoBotonPDF',
					hidden: 	 	true,
					scale: 		'small',
               iconAlign: 	'top'
				}
			]
		}
	};
	
	//-------------------------- 2. GRID DETALLE DISPERSION FISOS ----------------------------------
 
	var procesarDispersionIfFideicomisoData = function(store, registros, opts){
 
		var gridDispersionIfFideicomiso = Ext.getCmp('gridDispersionIfFideicomiso');

		if (registros != null) {
 
			if (!gridDispersionIfFideicomiso.isVisible()) {
				gridDispersionIfFideicomiso.show();
			}
 
			var el 					 		 = gridDispersionIfFideicomiso.getGridEl();
						
			// Actualizar propiedades del grid panel
			if(store.getTotalCount() > 0) {

				el.unmask();
				
			} else {

				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
 
		}
					
	}
 
	var rfcRenderer 					= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var nombreProveedorRenderer 	= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var totalDocumentosRenderer 	= function( value, metadata, record, rowIndex, colIndex, store){
		
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var monedaRenderer 				= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var montoRenderer					= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="text-align:right;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		value = record.json['MONTO'];
		return value;
		
	}
	
	var bancoRenderer 				= function( value, metadata, record, rowIndex, colIndex, store){
		
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var tipoCuentaRenderer 			= function( value, metadata, record, rowIndex, colIndex, store){
		
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var numeroCuentaRenderer 		= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var errorRenderer 				= function( value, metadata, record, rowIndex, colIndex, store){
		
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
 
	// Crear JsonStore que se encargar� de cargar el detalle
	var dispersionIfFideicomisoData = new Ext.data.JsonStore({
		id:							'dispersionIfFideicomisoDataStore',
		name:							'dispersionIfFideicomisoDataStore',
		root:							'registros',
		url: 							'15disperdiariaf01ext.data.jsp',
		autoDestroy:				true,
		autoLoad: 					false,
		totalProperty: 			'total',
		messageProperty: 			'msg',
		pruneModifiedRecords: 	true, // Para que no se conserven los registros modificados en consulta anterior.
		baseParams: {
			informacion: 	'ConsultaDispersionIfFideicomiso'
		},			
		fields: [
			{ name:'RFC',					type:"string" }, 
			{ name:'NOMBRE_PROVEEDOR',	type:"string" },
			{ name:'TOTAL_DOCUMENTOS',	type:"int" 	  },
			{ name:'MONEDA',				type:"string" },
			{ name:'MONTO',				type:"float", convert: function(value, record){ return Number(value.replace(/[\$,]/g,"")); } },
			{ name:'BANCO',				type:"string" },
			{ name:'TIPO_CUENTA',		type:"string" },
			{ name:'NUMERO_CUENTA',		type:"string" },
			{ name:'ERROR',				type:"string" },
			{ name:'ESTATUS_CECOBAN',	type:"string" }
		],
		listeners: { 
			beforeload: {
				fn: function( store, opts ) {
					var grid = Ext.getCmp('gridDispersionIfFideicomiso');
					if (grid) {
						grid.show();
					} 			
				}
			},
			load: 	procesarDispersionIfFideicomisoData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarDispersionIfFideicomisoData(null, null, null);						
				}
			}
		}
	});
 
	var gridDispersionIfFideicomiso = {
		frame:				true,
		store: 				dispersionIfFideicomisoData,
		//title:				'Detalle',
		xtype: 				'grid',
		id:					'gridDispersionIfFideicomiso',
		hidden:				true,
		stripeRows: 		true,
		loadMask: 			true,
		width: 				940, 
		height:				400,
		region: 				'center',
		style: 				'margin: 0 auto',
		// plugins: totalesGridSummary,
		columns: [
			{
				header: 		'RFC<br>&nbsp;',
				tooltip: 	'RFC',
				dataIndex: 	'RFC',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		68,
				hidden: 		false,
				renderer:	rfcRenderer
			},
			{
				header: 		'Nombre del<br>Proveedor',
				tooltip: 	'Nombre del Proveedor',
				dataIndex: 	'NOMBRE_PROVEEDOR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	nombreProveedorRenderer
			},
			{
				header: 		'Total de<br>Documentos',
				tooltip: 	'Total de Documentos',
				dataIndex: 	'TOTAL_DOCUMENTOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		80,
				hidden: 		false,
				renderer:	totalDocumentosRenderer
			},
			{
				header: 		'Moneda<br>&nbsp;',
				tooltip: 	'Moneda',
				dataIndex: 	'MONEDA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		74,
				hidden: 		false,
				renderer:	monedaRenderer
			},
			{
				header: 		'Monto<br>&nbsp;',
				tooltip: 	'Monto',
				dataIndex: 	'MONTO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	montoRenderer
			},
			{
				header: 		'Banco<br>&nbsp;',
				tooltip: 	'Banco',
				dataIndex: 	'BANCO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		50,
				hidden: 		false,
				renderer:	bancoRenderer
			},
			{
				header: 		'Tipo de<br>Cuenta',
				tooltip: 	'Tipo de Cuenta',
				dataIndex: 	'TIPO_CUENTA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		50,
				hidden: 		false,
				renderer:	tipoCuentaRenderer
			},
			{
				header: 		'N&uacute;mero de<br>Cuenta',
				tooltip: 	'N&uacute;mero de Cuenta',
				dataIndex: 	'NUMERO_CUENTA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	numeroCuentaRenderer
			},
			{
				header: 		'Error',
				tooltip: 	'Error',
				dataIndex: 	'ERROR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	errorRenderer
			}
		],
		stateful:	false
	};
	
	//--------------------------------- 1. FORMA DE CONSULTA ---------------------------------------
	
	var procesaConsultar = function( boton, evento ){

		var formaConsulta = Ext.getCmp("formaConsulta");
		
		var invalidForm = false;
		if( !formaConsulta.getForm().isValid() ){
			invalidForm = true;
		}
		
		// VALIDAR QUE SE HAYA SELECCIONADO UN INTERMEDIARIO FIDEICOMISO
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE SE HAYA SELECCIONADO UNA EPO
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE SE HAYA SELECCIONADO UN INTERMEDIARIO FINANCIERO 2
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE SE HAYA SELECCIONADO UNA MONEDA
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE SE HAYA ESPECIFICADO UNA FECHA DE REGISTRO
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE LA FECHA DE REGISTRO TENGA EL FORMATO DD/MM/AAAA
		// La fecha de registro es incorrecta.\nVerifique que el formato sea dd/mm/aaaa
	
		// VALIDAR QUE LA FECHA DE REGISTRO NO SEA MAYOR AL DIA ACTUAL
		var fechaRegistro			= Ext.getCmp("fechaRegistro");
		var fechaRegistroDate	= Date.parseDate( Ext.getCmp("fechaRegistro").getValue().format("d/m/Y"),	"d/m/Y", true );
		var fechaHoyDate			= Date.parseDate( Ext.getCmp("fechaHoy").getValue(), 								"d/m/Y", true );
		if( fechaRegistro.isValid() && fechaRegistroDate > fechaHoyDate ){
			invalidForm = true;
			fechaRegistro.markInvalid( "La Fecha de Registro no puede ser mayor al d�a de hoy " + fechaRegistroDate.format("d/m/Y") + "." );
		}

		if( invalidForm ){
			return; // La forma es invalida, se cancela la operacion
		}
		
		// Realizar consulta
		dispersionIF("CONSULTA_DISPERSION_IF_FIDEICOMISO",null);
 
	}
	
	var procesaLimpiar   = function( boton, evento ){
		
		Ext.getCmp("formaConsulta").getForm().reset();
		Ext.StoreMgr.key('catalogoEPODataStore').removeAll();
		Ext.StoreMgr.key('catalogoIFDataStore').removeAll();
		Ext.StoreMgr.key('catalogoMonedaDataStore').removeAll();
		Ext.getCmp("comboEPO").setNewEmptyText('Debe seleccionar un IF Fideicomiso');
		Ext.getCmp("comboIF").setNewEmptyText('Debe seleccionar un IF Fideicomiso');
		Ext.getCmp("comboMoneda").setNewEmptyText('Debe seleccionar un IF Fideicomiso');
		
	}
	
	var catalogoIfFideicomisoData = new Ext.data.JsonStore({
		id: 					'catalogoIfFideicomisoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15disperdiariaf01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIfFideicomiso'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Debido al beforeload que agrega la animacion: Cargando...
				if( !Ext.isDefined(options) || !Ext.isDefined(options.params) || !Ext.isDefined(options.params.selectedValue)){
					return;
				}
				
				var comboIfFideicomiso 	= Ext.getCmp("comboIfFideicomiso");
				
				// Si el combo que le precede no esta seleccionado agregar mensaje indicando que debe seleccionar primero un IF
				var valorComboAnterior = Ext.getCmp("concepto").getValue();
				if( Ext.isEmpty(valorComboAnterior) ){
					
					comboIfFideicomiso.setNewEmptyText("Debe seleccionar un Concepto.");
					
				// El combo anterior tiene valor seleccionado,
				} else {
					
					// Leer valor default
					var selectedValue 	= null;
					var existeParametro	= true;
					try {
						selectedValue = String(options.params.selectedValue);
					}catch(err){
						existeParametro	= false;
						selectedValue 		= null;
					}
									
					// Si se especific� un valor por default
					// El registro fue encontrado por lo que hay que seleccionarlo
					comboIfFideicomiso.setNewEmptyText("Seleccionar");
					if( existeParametro && store.findExact( 'clave', selectedValue ) >= 0 ){ 
						comboIfFideicomiso.setValue(selectedValue);	
					}
					
				}
				
				// Disparar evento de seleccion de componente
				comboIfFideicomiso.fireEvent('select',comboIfFideicomiso);
				
			},
			exception: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboIfFideicomiso 				= Ext.getCmp("comboIfFideicomiso");
						// Resetear valor en el combo IF Fideicomiso
						Ext.StoreMgr.key("catalogoIfFideicomisoDataStore").removeAll(); // Para quitar la animacion
						comboIfFideicomiso.setNewEmptyText("Seleccionar");
						// Disparar evento de seleccion de componente
						comboIfFideicomiso.fireEvent('select',comboIfFideicomiso);
					}
				);					
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 					'catalogoEPODataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15disperdiariaf01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoEPO'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
 
				// Debido al beforeload que agrega la animacion: Cargando...
				if( !Ext.isDefined(options) || !Ext.isDefined(options.params) || !Ext.isDefined(options.params.selectedValue)){
					return;
				}

				var comboEPO 				= Ext.getCmp("comboEPO");
				
				// Si el combo que le precede no esta seleccionado agregar mensaje indicando que debe seleccionar primero un IF
				var valorComboAnterior = Ext.getCmp("comboIfFideicomiso").getValue();
				if( Ext.isEmpty(valorComboAnterior) ){
					
					comboEPO.setNewEmptyText("Debe seleccionar un IF Fideicomiso");
					
				// El combo anterior tiene valor seleccionado,
				} else {
					
					// Leer valor default
					var selectedValue 	= null;
					var existeParametro	= true;
					if( store.getTotalCount() > 0 ){
						selectedValue = store.getAt(0).data['clave'];
					} else {
						existeParametro	= false;
						selectedValue 		= null;
					}
									
					// Si se especific� un valor por default
					// El registro fue encontrado por lo que hay que seleccionarlo
					comboEPO.setNewEmptyText("Seleccionar");
					if( existeParametro && store.findExact( 'clave', selectedValue ) >= 0 ){ 
						comboEPO.setValue(selectedValue);	
					} else {
						comboEPO.setNewEmptyText("No se encontr� EPO afiliada al IF FIDEICOMISO.");
					}
					
				}
				
				// Disparar evento de seleccion de componente
				comboEPO.fireEvent('select',comboEPO);
				
			},
			exception: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboEPO 				= Ext.getCmp("comboEPO");
						// Resetear valor en el combo EPO
						Ext.StoreMgr.key("catalogoEPODataStore").removeAll(); // Para quitar la animacion
						comboEPO.setNewEmptyText("Debe seleccionar un IF Fideicomiso");
						// Disparar evento de seleccion de componente
						comboEPO.fireEvent('select',comboEPO);
					}
				);					
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	var catalogoIFData = new Ext.data.JsonStore({
		id: 					'catalogoIFDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15disperdiariaf01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIF2Fideicomiso'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Debido al beforeload que agrega la animacion: Cargando...
				if( !Ext.isDefined(options) || !Ext.isDefined(options.params) || !Ext.isDefined(options.params.selectedValue)){
					return;
				}

				var comboIF 				= Ext.getCmp("comboIF");
				
				// Si el combo que le precede no esta seleccionado agregar mensaje indicando que debe seleccionar primero un IF Fideicomiso
				var valorComboAnterior 	= Ext.getCmp("comboEPO").getValue();
				if( Ext.isEmpty(valorComboAnterior) ){
					
					comboIF.setNewEmptyText("Debe seleccionar un IF Fideicomiso");
					
				// El combo anterior tiene valor seleccionado,
				} else {
					
					// Leer valor default
					var selectedValue 	= null;
					var existeParametro	= true;
					try {
						selectedValue = String(options.params.selectedValue);
					}catch(err){
						existeParametro	= false;
						selectedValue 		= null;
					}
									
					// Si se especific� un valor por default
					// El registro fue encontrado por lo que hay que seleccionarlo
					comboIF.setNewEmptyText("Seleccionar");
					if( existeParametro && store.findExact( 'clave', selectedValue ) >= 0 ){ 
						comboIF.setValue(selectedValue);	
					} 
					
				}
				
				// Disparar evento de seleccion de componente
				comboIF.fireEvent('select',comboIF);
				
			},
			exception: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboIF 				= Ext.getCmp("comboIF");
						// Resetear valor en el combo IF
						Ext.StoreMgr.key("catalogoIFDataStore").removeAll(); // Para quitar la animacion
						comboIF.setNewEmptyText("Debe seleccionar un IF Fideicomiso");
						// Disparar evento de seleccion de componente
						comboIF.fireEvent('select',comboIF);
					}
				);					
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
 
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 					'catalogoMonedaDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15disperdiariaf01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoMoneda'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Debido al beforeload que agrega la animacion: Cargando...
				if( !Ext.isDefined(options) || !Ext.isDefined(options.params) || !Ext.isDefined(options.params.selectedValue)){
					return;
				}
				
				var comboMoneda 			= Ext.getCmp("comboMoneda");
				
				// Si el combo que le precede no esta seleccionado agregar mensaje indicando que debe seleccionar primero un IF Fideicomiso 
				// que automaticamente selecciona una EPO.
				var valorComboAnterior 	= Ext.getCmp("comboEPO").getValue();
				if( Ext.isEmpty(valorComboAnterior) ){
					
					comboMoneda.setNewEmptyText("Debe seleccionar un IF Fideicomiso");
					
				// El combo anterior tiene valor seleccionado,
				} else {
				
					// Leer valor default
					var selectedValue 	= null;
					var existeParametro	= true;
					try {
						selectedValue = String(options.params.selectedValue);
					}catch(err){
						existeParametro	= false;
						selectedValue 		= null;
					}
	 
					comboMoneda.setNewEmptyText("Seleccionar");
					// Si se especific� un valor por default
					// El registro fue encontrado por lo que hay que seleccionarlo
					if( existeParametro && store.findExact( 'clave', selectedValue ) >= 0){
						comboMoneda.setValue(selectedValue);
					}
				
				}
				
				// Disparar evento de seleccion de componente
				comboMoneda.fireEvent('select',comboMoneda);
								
			},
			exception: 	 function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboMoneda 		= Ext.getCmp("comboMoneda");
						// Resetear valor en el combo Moneda
						Ext.StoreMgr.key("catalogoMonedaDataStore").removeAll(); // Para quitar la animacion
						comboMoneda.setNewEmptyText("Debe seleccionar un IF Fideicomiso");
						// Disparar evento de seleccion de componente
						comboMoneda.fireEvent('select',comboMoneda);
					}
				);					
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementosFormaConsulta = [
		// TEXFIELD CONCEPTO
		{
			xtype:		'textfield',
			id:	 		'concepto',
			name:  		'concepto',
			fieldLabel: 'Concepto',
			value: 		'Abonos',
			readOnly: 	true,
			style: 		'background: gainsboro;'
		},
		// COMBO IF FIDEICOMISO
		{
			xtype: 				'combo',
			name: 				'comboIfFideicomiso',
			id: 					'comboIfFideicomiso',
			fieldLabel: 		'IF FIDEICOMISO',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveIfFideicomiso',
			emptyText: 			'Seleccionar',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoIfFideicomisoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
			listeners:			{
				select:	function( combo, record, index ){

					var comboEPO		  = Ext.getCmp("comboEPO");
					
					var catalogoEPOData = Ext.StoreMgr.key('catalogoEPODataStore');
					catalogoEPOData.load({
						params: { 
							claveIfFideicomiso:	combo.getValue(),
							selectedValue: 		comboEPO.getValue()
						}
					});
					
				}
			}
		},
		// COMBO EPO
		{
			xtype: 				'combo',
			name: 				'comboEPO',
			id: 					'comboEPO',
			fieldLabel: 		'EPO',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPO',
			emptyText: 			'Debe seleccionar un IF Fideicomiso',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			readOnly:			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			style: 				'background: gainsboro;',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
			listeners:			{
				select:	function( combo, record, index ){

					var comboIF				= Ext.getCmp("comboIF");
						
					var catalogoIFData	= Ext.StoreMgr.key('catalogoIFDataStore');
					catalogoIFData.load({
						params: { 
							claveEPO: 		combo.getValue(),
							selectedValue: comboIF.getValue()						
						}
					});
					
				}
			}
		},
		// COMBO IF
		{
			xtype: 				'combo',
			name: 				'comboIF',
			id: 					'comboIF',
			fieldLabel: 		'IF',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveIF',
			emptyText: 			'Debe seleccionar un IF Fideicomiso',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoIFData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
			listeners:			{
				select:	function( combo, record, index ){

					var comboEPO			  = Ext.getCmp("comboEPO");
					var comboMoneda		  = Ext.getCmp("comboMoneda");
					var comboIfFideicomiso = Ext.getCmp("comboIfFideicomiso");
					
					var catalogoMonedaData = Ext.StoreMgr.key('catalogoMonedaDataStore');
					catalogoMonedaData.load({
						params: { 
							claveEPO: 				comboEPO.getValue(),
							claveIfFideicomiso:	comboIfFideicomiso.getValue(),
							selectedValue: 		comboMoneda.getValue()						
						}
					});
					
				}
			}
		},
		// COMBO MONEDA
		{
			xtype: 				'combo',
			name: 				'comboMoneda',
			id: 					'comboMoneda',
			fieldLabel: 		'Moneda',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveMoneda',
			emptyText: 			'Debe seleccionar un IF Fideicomiso',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMonedaData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		// DATE  FECHA DE REGISTRO
		{
			xtype: 				'datefield',
			name: 				'fechaRegistro',
			id: 					'fechaRegistro',
			fieldLabel: 		'Fecha de Registro',
			startDay: 			0,
			anchor: 				'-250',
			msgTarget: 			'side',
			margins: 			'0 20 0 0',
			allowBlank:			false,
			readOnly:			true,
			style: 				'background: gainsboro;'
		},
		// HIDDEN: FECHA HOY
		{
			xtype: 'hidden',
			id: 	 'fechaHoy',
			name:	 'fechaHoy'
		},
		// HIDDEN: FECHA CONSULTADA
		// NOTA: AUNQUE NO SE OCUPA ESTE VALOR SE DEJA POR COMPATIBILIDAD
		{
			xtype: 'hidden',
			id: 	 'fechaConsultada',
			name:	 'fechaConsultada'
		}
	];
	
	var formaConsulta = new Ext.form.FormPanel({
		id: 					'formaConsulta',
		width: 				500,
		title: 				'Forma',
		hidden:				true,
		frame: 				true,
		collapsible: 		true,
		titleCollapse: 	true,
		trackResetOnLoad: true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px;',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		labelWidth: 		106,
		labelAlign:			'right',
		defaultType: 		'textfield',
		items: 				elementosFormaConsulta,
		monitorValid: 		false,
		buttons: [
			{
				text: 		'Consultar',
				iconCls: 	'icoBuscar',
				handler: 	procesaConsultar
			},
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	procesaLimpiar
			}
		]
	});
	
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			formaConsulta,
			NE.util.getEspaciador(10),
			gridDispersionIfFideicomiso,
			gridTotalDispersionIfFideicomiso
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	dispersionIF("INICIALIZACION",null);
	
});