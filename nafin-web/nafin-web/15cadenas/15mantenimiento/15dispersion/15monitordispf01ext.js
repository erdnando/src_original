Ext.onReady(function() {
	
	Ext.namespace('NE.monitordispersion');
	
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaMonitorDispersion = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						monitorDispersion(resp.estadoSiguiente,resp);
					}
				);
			} else {
				monitorDispersion(resp.estadoSiguiente,resp);
			}
			
		} else {
						
			/*
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Resetear forma
         resetWindowConfirmacionClave();
 
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			*/
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
					
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var monitorDispersion = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"							   ){
			
			// Cargar Cat�logo de Tipos de Monitoreo
			var catalogoTipoMonitoreoData = Ext.StoreMgr.key('catalogoTipoMonitoreoDataStore');
			catalogoTipoMonitoreoData.load({
				params: { defaultValue: "FF" }
			});
			
			// Cargar Catalogo IF Fideicomiso
			var catalogoIfFideicomisoData 	= Ext.StoreMgr.key('catalogoIfFideicomisoDataStore');
			catalogoIfFideicomisoData.load();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispf01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.inicializacion'
				},
				callback: 				procesaMonitorDispersion
			});
 	
		} else if(			estado == "MOSTRAR_PANTALLA_MONITOREO"			  ){
									
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispf01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.mostrarPantallaMonitoreo',
					tipoMonitoreo:		respuesta.tipoMonitoreo
				},
				callback: 				procesaMonitorDispersion
			});
 			
		} else if(			estado == "ESPERAR_DECISION"  					  ){ 
			         
			/*
			
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
			
         // Resetear forma
         resetWindowConfirmacionClave();
         
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			*/
			
		} else if(			estado == "FIN"  					                 ){
 	
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"15monitordispf01ext.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			forma.submit();
						
		}
		
	}
 
	//--------------------------------- 2. PANEL DE DETALLE MONITOREO IF ---------------------------------------
 
	var procesarSuccessFailureGeneraPDF =  function(opts, success, response) {
		
		var btnGeneraPDF = Ext.getCmp('btnGeneraPDF');
		btnGeneraPDF.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaPDF = Ext.getCmp('btnBajaPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
					
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
				
			});
			
		} else {
			
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarSuccessFailureGeneraCSV =  function(opts, success, response) {
		
		var btnGeneraCSV = Ext.getCmp('btnGeneraCSV');
		btnGeneraCSV.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaCSV = Ext.getCmp('btnBajaCSV');
			btnBajaCSV.show();
			btnBajaCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaCSV.setHandler( function(boton, evento) {
					
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
				
			});
			
		} else {
			
			btnGeneraCSV.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarDetalleMonitoreoIfFideicomisoData = function(store, registros, opts){
				
		var panelMonitoreoIfFideicomiso = Ext.getCmp('panelMonitoreoIfFideicomiso');
		panelMonitoreoIfFideicomiso.el.unmask();
		
		var gridDetalleMonitoreoIfFideicomiso = Ext.getCmp('gridDetalleMonitoreoIfFideicomiso');
		
		if (registros != null) {
			
			/*
			if (!gridDetalleMonitoreoIfFideicomiso.isVisible()) {
				gridDetalleMonitoreoIfFideicomiso.show();
			}
			*/
			
			var btnGeneraPDF 				 = Ext.getCmp('btnGeneraPDF');
			var btnGeneraCSV				 = Ext.getCmp('btnGeneraCSV');
			var btnBajaPDF					 = Ext.getCmp('btnBajaPDF');
			var btnBajaCSV					 = Ext.getCmp('btnBajaCSV');		
 
			var el 					 		 = gridDetalleMonitoreoIfFideicomiso.getGridEl();
			
			// Copiar en el grid los parametros de la ultima consulta
			gridDetalleMonitoreoIfFideicomiso.formParams = Ext.apply({},opts.params);
			
			// Actualizar propiedades del grid panel
			if(store.getTotalCount() > 0) {
				
				btnGeneraPDF.enable();
				btnGeneraCSV.enable();
				btnBajaPDF.hide();
				btnBajaCSV.hide();
				
				el.unmask();
				
			} else {
				
				btnGeneraPDF.disable();
				btnGeneraCSV.disable();
				btnBajaPDF.hide();
				btnBajaCSV.hide();
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
 
		}
					
	};
	
	var nombreEPORenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var nombrePYMERenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var numeroNafinElectronicoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var habilitadaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var intermediarioFinancieroRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var numeroTotalDocumentosRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var nombreMonedaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var montoTotalRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="text-align:right;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		value = record.json['MONTO_TOTAL'];
		return value;
		
	}
	
	var codigoBancoCuentaClabeRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var cuentaClabeSwiftRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var descripcionCECOBANRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var descripcionDOLARESRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
  
	// Crear JsonStore que se encargar� de cargar el detalle
	var detalleMonitoreoIfFideicomisoData = new Ext.data.GroupingStore({
		id:					'detalleMonitoreoIfFideicomisoDataStore',
		name:					'detalleMonitoreoIfFideicomisoDataStore',
		root:					'registros',
		url: 					'15monitordispf01ext.data.jsp',
		autoDestroy:		true,
		baseParams: {
			informacion: 	'ConsultaDetalleMonitoreoIfFideicomiso'
		},			
		reader: new Ext.data.JsonReader({
			root: 			'registros',	
			totalProperty: 'total',
			fields: [
				{ name:'IC_IF_FIDEICOMISO',					type:"string" }, // GROUP_ID
				{ name:'NOMBRE_FIDEICOMISO',					type:"string" },
				{ name:'NOMBRE_EPO',								type:"string" },
				{ name:'NOMBRE_PYME',							type:"string" },
				{ name:'NUMERO_NAFIN_ELECTRONICO',			type:"string" },
				{ name:'HABILITADA',								type:"string" },
				{ name:'INTERMEDIARIO_FINANCIERO',			type:"string" },
				{ name:'NUMERO_TOTAL_DOCUMENTOS',			type:"int"    },
				{ name:'NOMBRE_MONEDA',							type:"string" },
				{ name:'MONTO_TOTAL',							type:"float", convert: function(value, record){ return Number(value.replace(/[\$,]/g,"")); } },
				{ name:'CODIGO_BANCO_CUENTA_CLABE',			type:"string" },
				{ name:'CUENTA_CLABE_SWIFT',					type:"string" },
				{ name:'DESCRIPCION_CECOBAN',					type:"string" },
				{ name:'DESCRIPCION_DOLARES',					type:"string" },
				{ name:'ESTATUS_CECOBAN',						type:"string" }	
			]
		}),
		groupField: 			'IC_IF_FIDEICOMISO', // GROUP_ID
		sortInfo: 				{ field: 'NOMBRE_FIDEICOMISO', direction: "ASC" },
		autoLoad: 				false,
		totalProperty: 		'total',
		messageProperty: 		'msg',
		pruneModifiedRecords: true, // Para que no se conserven los registros modificados en consulta anterior.
		listeners: { 
			beforeload: {
				fn: function( store, opts ) {
					var grid = Ext.getCmp('gridDetalleMonitoreoIfFideicomiso');
					if (grid) {
						grid.show();
					} 			
				}
			},
			load: 	procesarDetalleMonitoreoIfFideicomisoData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarDetalleMonitoreoIfFideicomisoData(null, null, null);						
				}
			}
		}
	});
    
	// Crear plugin que se encargar� de desplegar los totales
	var totalesPorGrupoSummary = new Ext.ux.grid.ServerSummary();
	
	var gridDetalleMonitoreoIfFideicomiso = {
		frame:			true,
		store: 			detalleMonitoreoIfFideicomisoData,
		//title:			'Detalle Monitoreo IF Fideicomiso',
		xtype: 			'grid',
		id:				'gridDetalleMonitoreoIfFideicomiso',
		hidden:			true,
		stripeRows: 	true,
		loadMask: 		true,
		width: 			940, 
		height:			480,
		region: 			'center',
		style: 			'margin: 0 auto',
		formParams:		{},
		view:		 		new Ext.grid.SelectableGroupingView(
			{
				forceFit:				false, 
				groupTextTpl: 			'{text}', // Usar group text renderer
				hideGroupedColumn: 	true,
				enableGroupingMenu:	false,
				hideGroupCheckBox: 	true
			}
		),
		plugins: totalesPorGrupoSummary,
		columns: [
			{
				header: 		'CLAVE IC_IF FIDEICOMISO', 
				tooltip: 	'CLAVE IC_IF FIDEICOMISO', // GROUP_ID
				dataIndex: 	'IC_IF_FIDEICOMISO', 
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				// width: 		315,
				hidden: 		true,
				hideable:	false,
				groupTextRenderer: function(value,unused,record,rowIndex,colIndex,dsStore){
					return  record.data['NOMBRE_FIDEICOMISO'];
				}
			},
			{
				header: 		'EPO<br>&nbsp;',
				tooltip: 	'EPO',
				dataIndex: 	'NOMBRE_EPO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	nombreEPORenderer
			},
			{
				header: 		'PYME<br>&nbsp;',
				tooltip: 	'PYME',
				dataIndex: 	'NOMBRE_PYME',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	nombrePYMERenderer
			},
			{
				header: 		'Num. Nafin<br>Electr&oacute;nico',
				tooltip: 	'Num. Nafin Electr&oacute;nico',
				dataIndex: 	'NUMERO_NAFIN_ELECTRONICO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroNafinElectronicoRenderer
			},
			{
				header: 		'Habilitada<br>&nbsp;',
				tooltip: 	'Habilitada',
				dataIndex: 	'HABILITADA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	habilitadaRenderer
			},
			{
				header: 		'Intermediario<br>Financiero ',
				tooltip: 	'Intermediario Financiero ',
				dataIndex: 	'INTERMEDIARIO_FINANCIERO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	intermediarioFinancieroRenderer 
			},
			{
				header: 		'Num. Total<br>de Doctos.',
				tooltip: 	'Num. Total de Doctos.',
				dataIndex: 	'NUMERO_TOTAL_DOCUMENTOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroTotalDocumentosRenderer
			},
			{
				header: 		'Moneda<br>&nbsp;',
				tooltip: 	'Moneda',
				dataIndex: 	'NOMBRE_MONEDA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				renderer:	nombreMonedaRenderer 
			},
			{
				header: 		'Monto Total<br>&nbsp;',
				tooltip: 	'Monto Total',
				dataIndex: 	'MONTO_TOTAL',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	montoTotalRenderer
			},
			{
				header: 		'C&oacute;digo Banco<br>Cuenta Clabe',
				tooltip: 	'C&oacute;digo Banco Cuenta Clabe',
				dataIndex: 	'CODIGO_BANCO_CUENTA_CLABE',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				renderer:	codigoBancoCuentaClabeRenderer
			},
			{
				header: 		'Cuenta CLABE/SWIFT<br>&nbsp;',
				tooltip: 	'Cuenta CLABE/SWIFT',
				dataIndex: 	'CUENTA_CLABE_SWIFT',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	cuentaClabeSwiftRenderer
			},
			{
				header: 		'Descripci&oacute;n<br>CECOBAN',
				tooltip: 	'Descripci&oacute;n CECOBAN',
				dataIndex: 	'DESCRIPCION_CECOBAN',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	descripcionCECOBANRenderer
			},
			{
				header: 		'Descripci&oacute;n<br>D&Oacute;LARES',
				tooltip: 	'Descripci&oacute;n D&Oacute;LARES',
				dataIndex: 	'DESCRIPCION_DOLARES',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	descripcionDOLARESRenderer
			}
		],
		stateful:	false,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'left',
			items: [
				'->',
				{
					xtype: 	'button',
					text: 	'Generar PDF',
					id: 		'btnGeneraPDF',
					iconCls: 'icoGenerarDocumento',
					disabled: true,
					handler: function(boton, evento) {
						
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						var panelMonitoreoIfFideicomiso 		 = Ext.getCmp("panelMonitoreoIfFideicomiso");
						var gridDetalleMonitoreoIfFideicomiso = Ext.getCmp("gridDetalleMonitoreoIfFideicomiso");
						
						// Generar Archivo PDF
						Ext.Ajax.request({
							url: 		'15monitordispf01ext.data.jsp',
							params: 	Ext.apply(
								gridDetalleMonitoreoIfFideicomiso.formParams, // panelMonitoreoIfFideicomiso.getForm().getValues()
								{
									informacion: 'GenerarArchivo',
									tipo: 		 'PDF'
								}
							),
							callback: procesarSuccessFailureGeneraPDF
						});
						
					}
				},
				{
					xtype: 	'button',
					text: 	'Bajar PDF',
					id: 		'btnBajaPDF',
					iconCls:	'icoBotonPDF',
					hidden: 	true
				},
				'-',
				{
					xtype: 	'button',
					text: 	'Generar Archivo',
					id: 		'btnGeneraCSV',
					iconCls:	'icoGenerarDocumento',
					disabled: true,
					handler: function(boton, evento) {
								
						boton.disable();
						boton.setIconClass('loading-indicator');
									
						var panelMonitoreoIfFideicomiso 		= Ext.getCmp("panelMonitoreoIfFideicomiso");
						var gridDetalleMonitoreoIfFideicomiso = Ext.getCmp("gridDetalleMonitoreoIfFideicomiso");
						
						// Generar Archivo CSV
						Ext.Ajax.request({
							url: 		'15monitordispf01ext.data.jsp',
							params: 	Ext.apply(
								gridDetalleMonitoreoIfFideicomiso.formParams, // panelMonitoreoIfFideicomiso.getForm().getValues()
								{
									informacion: 'GenerarArchivo',
									tipo: 		 'CSV'
								}
							),
							callback: procesarSuccessFailureGeneraCSV
						});
							
					}
				},
				{
					xtype: 	'button',
					text: 	'Bajar Archivo',
					id: 		'btnBajaCSV',
					iconCls:	'icoBotonXLS',
					hidden: 	true
				}
			]
		}
	};
	
	//------------------------------------- 1. PANEL DE MONITOREO IF ------------------------------------------

	var procesaConsultar = function(boton,evento){
		
		// Por compatibilidad 
		/*
		var comboTipoMonitoreo = Ext.getCmp("comboTipoMonitoreo");
		if( comboTipoMonitoreo.getValue() !== "FF" ){
			return;	
		}
		*/
		
		// Validar toda la forma
		var panelMonitoreoIfFideicomiso = Ext.getCmp("panelMonitoreoIfFideicomiso");
		if( !panelMonitoreoIfFideicomiso.getForm().isValid() ){
			return;
		}
 
		// Agregar m�scara
		panelMonitoreoIfFideicomiso.getEl().mask('Consultando...','x-mask-loading');
		
		// Si hay una m�scara previa en el grid, suprimirla
		var gridEl = Ext.getCmp("gridDetalleMonitoreoIfFideicomiso").getGridEl();
		if( gridEl.isMasked()){
			gridEl.unmask();
		}
		
		// Realizar consulta
		var detalleMonitoreoIfFideicomisoData = Ext.StoreMgr.key('detalleMonitoreoIfFideicomisoDataStore');
		detalleMonitoreoIfFideicomisoData.load({
			params: Ext.apply(
				panelMonitoreoIfFideicomiso.getForm().getValues(),
				{
					informacion: 'ConsultaDetalleMonitoreoIfFideicomiso'
				}
			)
		});
		 
	}

	var procesaLimpiar = function(boton,evento){
		Ext.getCmp("panelMonitoreoIfFideicomiso").getForm().reset();
		Ext.getCmp("comboEPO").clearValue();
		Ext.StoreMgr.key('catalogoEPODataStore').removeAll();
	}
	
	var catalogoTipoMonitoreoData = new Ext.data.JsonStore({
		id: 					'catalogoTipoMonitoreoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispf01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoTipoMonitoreo'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboTipoMonitoreo = Ext.getCmp("comboTipoMonitoreo");
						comboTipoMonitoreo.setValue(defaultValue);
						comboTipoMonitoreo.originalValue = comboTipoMonitoreo.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	var catalogoIfFideicomisoData = new Ext.data.JsonStore({
		id: 					'catalogoIfFideicomisoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispf01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIfFideicomiso'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Debido al beforeload que agrega la animacion: Cargando...
				if( !Ext.isDefined(options) || !Ext.isDefined(options.params) || !Ext.isDefined(options.params.selectedValue)){
					return;
				}
				
				var comboIfFideicomiso 	= Ext.getCmp("comboIfFideicomiso");
				
				// Si el combo que le precede no esta seleccionado agregar mensaje indicando que debe seleccionar primero un IF
				var valorComboAnterior = Ext.getCmp("comboTipoMonitoreo").getValue();
				if( Ext.isEmpty(valorComboAnterior) ){
					
					comboIfFideicomiso.setNewEmptyText("Debe seleccionar un Tipo de Monitoreo");
					
				// El combo anterior tiene valor seleccionado,
				} else {
					
					// Leer valor default
					var selectedValue 	= null;
					var existeParametro	= true;
					try {
						selectedValue = String(options.params.selectedValue);
					}catch(err){
						existeParametro	= false;
						selectedValue 		= null;
					}
									
					// Si se especific� un valor por default
					// El registro fue encontrado por lo que hay que seleccionarlo
					comboIfFideicomiso.setNewEmptyText("Seleccionar");
					if( existeParametro && store.findExact( 'clave', selectedValue ) >= 0 ){ 
						comboIfFideicomiso.setValue(selectedValue);	
					}
					
				}
				
				// Disparar evento de seleccion de componente
				comboIfFideicomiso.fireEvent('select',comboIfFideicomiso);
				
			},
			exception: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboIfFideicomiso 				= Ext.getCmp("comboIfFideicomiso");
						// Resetear valor en el combo IF Fideicomiso
						Ext.StoreMgr.key("catalogoIfFideicomisoDataStore").removeAll(); // Para quitar la animacion
						comboIfFideicomiso.setNewEmptyText("Seleccionar");
						// Disparar evento de seleccion de componente
						comboIfFideicomiso.fireEvent('select',comboIfFideicomiso);
					}
				);					
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 					'catalogoEPODataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispf01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoEPO'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
 
				// Debido al beforeload que agrega la animacion: Cargando...
				if( !Ext.isDefined(options) || !Ext.isDefined(options.params) || !Ext.isDefined(options.params.selectedValue)){
					return;
				}

				var comboEPO 				= Ext.getCmp("comboEPO");
				
				// Si el combo que le precede no esta seleccionado agregar mensaje indicando que debe seleccionar primero un IF
				var valorComboAnterior = Ext.getCmp("comboIfFideicomiso").getValue();
				if( Ext.isEmpty(valorComboAnterior) ){
					
					comboEPO.setNewEmptyText("Debe seleccionar un IF Fideicomiso");
					
				// El combo anterior tiene valor seleccionado,
				} else {
					
					// Leer valor default
					var selectedValue 	= null;
					var existeParametro	= true;
					if( store.getTotalCount() > 0 ){
						selectedValue = store.getAt(0).data['clave'];
					} else {
						existeParametro	= false;
						selectedValue 		= null;
					}
									
					// Si se especific� un valor por default
					// El registro fue encontrado por lo que hay que seleccionarlo
					comboEPO.setNewEmptyText("Seleccionar");
					if( existeParametro && store.findExact( 'clave', selectedValue ) >= 0 ){ 
						comboEPO.setValue(selectedValue);	
					} else {
						comboEPO.setNewEmptyText("No se encontr� EPO afiliada al IF FIDEICOMISO.");
					}
					
				}
				
				// Disparar evento de seleccion de componente
				comboEPO.fireEvent('select',comboEPO);
				
			},
			exception: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboEPO 				= Ext.getCmp("comboEPO");
						// Resetear valor en el combo EPO
						Ext.StoreMgr.key("catalogoEPODataStore").removeAll(); // Para quitar la animacion
						comboEPO.setNewEmptyText("Debe seleccionar un IF Fideicomiso");
						// Disparar evento de seleccion de componente
						comboEPO.fireEvent('select',comboEPO);
					}
				);					
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoIFData = new Ext.data.JsonStore({
		id: 					'catalogoIFDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispf01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIF2Fideicomiso'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Debido al beforeload que agrega la animacion: Cargando...
				if( !Ext.isDefined(options) || !Ext.isDefined(options.params) || !Ext.isDefined(options.params.selectedValue)){
					return;
				}

				var comboIF 				= Ext.getCmp("comboIF");
				
				// Si el combo que le precede no esta seleccionado agregar mensaje indicando que debe seleccionar primero un IF Fideicomiso
				var valorComboAnterior 	= Ext.getCmp("comboEPO").getValue();
				if( Ext.isEmpty(valorComboAnterior) ){
					
					comboIF.setNewEmptyText("Debe seleccionar un IF Fideicomiso");
					
				// El combo anterior tiene valor seleccionado,
				} else {
					
					// Leer valor default
					var selectedValue 	= null;
					var existeParametro	= true;
					try {
						selectedValue = String(options.params.selectedValue);
					}catch(err){
						existeParametro	= false;
						selectedValue 		= null;
					}
									
					// Si se especific� un valor por default
					// El registro fue encontrado por lo que hay que seleccionarlo
					comboIF.setNewEmptyText("Seleccionar");
					if( existeParametro && store.findExact( 'clave', selectedValue ) >= 0 ){ 
						comboIF.setValue(selectedValue);	
					} 
					
				}
				
				// Disparar evento de seleccion de componente
				comboIF.fireEvent('select',comboIF);
				
			},
			exception: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(
					proxy, type, action, optionsRequest, response, args,
					function(){
						var comboIF 				= Ext.getCmp("comboIF");
						// Resetear valor en el combo IF
						Ext.StoreMgr.key("catalogoIFDataStore").removeAll(); // Para quitar la animacion
						comboIF.setNewEmptyText("Debe seleccionar un IF Fideicomiso");
						// Disparar evento de seleccion de componente
						comboIF.fireEvent('select',comboIF);
					}
				);					
			},
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var elementosPanelMonitoreoIfFideicomiso = [
		// COMBO TIPO DE MONITOREO
		{
			xtype: 				'combo',
			name: 				'tipoMonitoreo',
			id: 					'comboTipoMonitoreo',
			fieldLabel: 		'Tipo de Monitoreo',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveTipoMonitoreo',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTipoMonitoreoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'65%',
			listeners:			{
				select:	function( combo, record, index ){
					
					var respuesta = new Object();
					respuesta['tipoMonitoreo'] = combo.getValue();
					monitorDispersion("MOSTRAR_PANTALLA_MONITOREO",respuesta);
					
				}
			}
		},
		// COMBO IF FIDEICOMISO
		{
			xtype: 				'combo',
			name: 				'comboIfFideicomiso',
			id: 					'comboIfFideicomiso',
			fieldLabel: 		'IF FIDEICOMISO',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveIfFideicomiso',
			emptyText: 			'Seleccionar',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoIfFideicomisoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
			listeners:			{
				select:	function( combo, record, index ){

					var comboEPO		  = Ext.getCmp("comboEPO");
					
					var catalogoEPOData = Ext.StoreMgr.key('catalogoEPODataStore');
					catalogoEPOData.load({
						params: { 
							claveIfFideicomiso:	combo.getValue(),
							selectedValue: 		comboEPO.getValue()
						}
					});
					
				}
			}
		},
		// COMBO EPO
		{
			xtype: 				'combo',
			name: 				'comboEPO',
			id: 					'comboEPO',
			fieldLabel: 		'EPO',
			mode: 				'local', 
			allowBlank:			true,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPO',
			emptyText: 			'Debe seleccionar un IF Fideicomiso',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			readOnly:			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			style: 				'background: gainsboro;',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
			listeners:			{
				select:	function( combo, record, index ){

					var comboIF				= Ext.getCmp("comboIF");
						
					var catalogoIFData	= Ext.StoreMgr.key('catalogoIFDataStore');
					catalogoIFData.load({
						params: { 
							claveEPO: 		combo.getValue(),
							selectedValue: comboIF.getValue()						
						}
					});
					
				}
			}
		},
		// COMBO IF
		{
			xtype: 				'combo',
			name: 				'comboIF',
			id: 					'comboIF',
			fieldLabel: 		'IF',
			mode: 				'local', 
			allowBlank:			true,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveIF',
			emptyText: 			'Debe seleccionar un IF Fideicomiso',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoIFData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		// CHECKBOX PYMES SIN CUENTA CLABE
		{
			xtype: 						'compositefield',
			fieldLabel: 				'',
			combineErrors: 			false,
			msgTarget: 					'side',
			items: [
				{
					xtype: 				'displayfield',
					value: 				'PYMES Sin Cuenta CLABE:',
					style:				'text-align: right;',
					width: 				144
				},
				// CHECKBOX SIN CUENTA CLABE
				{
					xtype: 				'checkbox',
					name: 				'pymeSinCuentaClabe',
					id: 					'pymeSinCuentaClabe',
					//fieldLabel: 		'Sin Cuenta CLABE',
					inputValue:			'Sin Cuenta'
				}
			]
		},
		// RADIOGROUP HABILITADAS
		{
      	xtype:  		'radiogroup',
      	fieldLabel: '',
      	anchor: '95%',
      	columns: [ 0.24, 0.275, 0.25 ],
      	items: [
         	{ boxLabel: 'Habilitadas', 	name: 'radioHabilitadas', inputValue: "H"	},
         	{ boxLabel: 'No Habilitadas', name: 'radioHabilitadas', inputValue: "N"	},
         	{ boxLabel: 'Ambas', 			name: 'radioHabilitadas', inputValue: "A"	}
         ]
       }

	];
	
	var panelMonitoreoIfFideicomiso = new Ext.form.FormPanel({
		id: 					'panelMonitoreoIfFideicomiso',
		width: 				700,
		title: 				'Monitor IF Fideicomiso',
		frame: 				true,
		collapsible: 		true,
		titleCollapse: 	true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px',
		defaults: {	
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		labelWidth: 		130,
		defaultType: 		'textfield',
		trackResetOnLoad:	true,
		items: 				elementosPanelMonitoreoIfFideicomiso,
		monitorValid: 		false,
		buttons: [
			{
				text: 		'Consultar',
				iconCls: 	'icoBuscar',
				handler: 	procesaConsultar
			},
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	procesaLimpiar
			}
		]
	});
	
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelMonitoreoIfFideicomiso,
			NE.util.getEspaciador(10),
			gridDetalleMonitoreoIfFideicomiso
		]
	});

	//------------------------------------- ACCIONES DE INICIALIZACION ----------------------------------------	
	monitorDispersion("INICIALIZACION",null);
	
});