<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoIFMonitorDispersion,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		java.math.BigDecimal"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("ConsultaFlujoFondos.inicializacion") )	{
	
	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
 
	String 		fechaHoy 				= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String 		fechaRegistroInicial	= fechaHoy;
	String 		fechaRegistroFinal	= fechaHoy;
	
	resultado.put("fechaHoy",					fechaHoy					);
	resultado.put("fechaRegistroInicial",	fechaRegistroInicial	);
	resultado.put("fechaRegistroFinal",		fechaRegistroFinal	);
	
	// Determinar el estado siguiente
	estadoSiguiente = "INICIALIZAR_FORMA_CONSULTA";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if ( informacion.equals("ConsultaFlujoFondos.inicializarFormaConsulta") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;

	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if( informacion.equals("CatalogoConcepto")){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Crear Catalogo	
	JSONArray  registros = new JSONArray();
	JSONObject registro	= null;
	
	registro = new JSONObject();
	registro.put("clave",			"A");
	registro.put("descripcion",	"Abonos");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"C");
	registro.put("descripcion",	"Cargos");
	registros.add( JSONObject.fromObject(registro) );
		
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)				);
	resultado.put("total",    	new Integer(registros.size()) );
	resultado.put("registros",	registros			 				);
	
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("CatalogoTipoDispersion") ){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Crear Catalogo	
	JSONArray  registros = new JSONArray();
	JSONObject registro	= null;
	
	registro = new JSONObject();
	registro.put("clave",			"E");
	registro.put("descripcion",	"EPOS");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"F");
	registro.put("descripcion",	"IF's");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"P");
	registro.put("descripcion",	"PEMEX-REF");
	registros.add( JSONObject.fromObject(registro) );
	
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)				);
	resultado.put("total",    	new Integer(registros.size()) );
	resultado.put("registros",	registros			 				);
	
	infoRegresar = resultado.toString();

} else if(  informacion.equals("CatalogoEPO")            							){

	String claveTipoDispersion = (request.getParameter("claveTipoDispersion") == null)?"":request.getParameter("claveTipoDispersion");
	
	final  String EPOS 	 		= "E";
	final  String PEMEXREF 		= "P";
	
	List   		listaElementos = null;
	CatalogoEPO catalogo 		= null;
	
	if(        EPOS.equals(claveTipoDispersion)     ){
		
		catalogo = new CatalogoEPO();
		catalogo.setClave("ic_epo");
		catalogo.setDescripcion("cg_razon_social");
		catalogo.setHabilitado("S"); 
		
		listaElementos = catalogo.getListaElementosMonitorDispersionEPO();	
		
		// Agregar la opción todos
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave("TODAS");
		elementoCatalogo.setDescripcion("TODAS");
		listaElementos.add(0,elementoCatalogo);
	
	} else if( PEMEXREF.equals(claveTipoDispersion) ){
		
		catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setOrden("2");
			
		listaElementos = catalogo.getListaElementosDistribuidoresPemexRef();
	
	}
	
	infoRegresar = catalogo.getJSONElementos(listaElementos);
	
} else if(  informacion.equals("CatalogoIF")            )	{

	CatalogoIFMonitorDispersion cat = new CatalogoIFMonitorDispersion();
	cat.setCampoClave("ic_if"); 
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setProducto("1"); 
	cat.setDispersion("S"); 
	cat.setShowCampoOperaFideicomiso(true); // F017 - 2013: Para que se envíe el contenido del campo: COMCAT_IF.CS_OPERA_FIDEICOMISO
	cat.setOperaFideicomiso("S"); // F017 - 2013: Para que también se envién los IF que operan Fideicomiso
	cat.setOrden("cg_razon_social");
	
	List listaElementos = cat.getListaElementos();	

	// Agregar la opción todos
	HashMap elementoCatalogo = new HashMap();
	elementoCatalogo.put( "clave",       			"TODOS"	);
	elementoCatalogo.put( "descripcion", 			"TODOS"	);
	elementoCatalogo.put( "opera_fideicomiso", 	"N" 		);
	listaElementos.add(0,elementoCatalogo);
				
	infoRegresar      = cat.getJSONElementos(listaElementos);
	
} else if ( informacion.equals("CatalogoEstatus")  ){
	
	String estatus = "2";
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_docto");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_docto");
	cat.setValoresCondicionIn(estatus, String.class);
	cat.setOrden("1");
	
	List listaElementos = cat.getListaElementos();	
	
	// Agregar la opción todos
	ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
	elementoCatalogo.setClave("PA");
	elementoCatalogo.setDescripcion("Pago Anticipado");
	listaElementos.add(elementoCatalogo);
				
	infoRegresar        = cat.getJSONElementos(listaElementos);
	
} else if( informacion.equals("CatalogoViaLiquidacion") ){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Crear Catalogo	
	JSONArray  registros = new JSONArray();
	JSONObject registro	= null;
	
	registro = new JSONObject();
	registro.put("clave",			"A");
	registro.put("descripcion",	"AMBOS");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"T");
	registro.put("descripcion",	"TEF");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"S");
	registro.put("descripcion",	"SPEUA");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"I");
	registro.put("descripcion",	"SPEI");
	registros.add( JSONObject.fromObject(registro) );
	
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)				);
	resultado.put("total",    	new Integer(registros.size()) );
	resultado.put("registros",	registros			 				);
	
	infoRegresar = resultado.toString();
	
} else if (	informacion.equals("ConsultaResumenOperacion")		){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	
	// Leer parametros
	String claveConcepto 			= (request.getParameter("claveConcepto")			== null)?"":request.getParameter("claveConcepto");
	String claveTipoDispersion 	= (request.getParameter("claveTipoDispersion")	== null)?"":request.getParameter("claveTipoDispersion");
	String claveIF 					= (request.getParameter("claveIF")					== null)?"":request.getParameter("claveIF");
	String ifOperaFideicomiso 		= (request.getParameter("ifOperaFideicomiso")	== null)?"":request.getParameter("ifOperaFideicomiso");
	String claveEPO 					= (request.getParameter("claveEPO")					== null)?"":request.getParameter("claveEPO");
	String claveEstatus 				= (request.getParameter("claveEstatus")			== null)?"":request.getParameter("claveEstatus");
	String claveViaLiquidacion 	= (request.getParameter("claveViaLiquidacion")	== null)?"":request.getParameter("claveViaLiquidacion");
	String fechaRegistroInicial	= (request.getParameter("fechaRegistroInicial")	== null)?"":request.getParameter("fechaRegistroInicial");
	String fechaRegistroFinal 		= (request.getParameter("fechaRegistroFinal")	== null)?"":request.getParameter("fechaRegistroFinal");

	String operacion					= (request.getParameter("operacion") 				== null)?"":request.getParameter("operacion");
	String tipo 						= "Consulta" ;
	String estatusFF 					= "";

	claveEPO	= "TODAS".equals(claveEPO)?"":claveEPO;
	claveIF	= "TODOS".equals(claveIF) ?"":claveIF;
	
	// Crear instancia del paginador
	ConsultaResumenOperacionErrorFFON paginador = new ConsultaResumenOperacionErrorFFON();
	paginador.setTipo(tipo);
	paginador.setTipoDispersion(claveTipoDispersion);
	paginador.setFechaRegistroInicial(fechaRegistroInicial);
	paginador.setFechaRegistroFinal(fechaRegistroFinal);
	paginador.setViaLiquidacion(claveViaLiquidacion);
	paginador.setClaveEPO(claveEPO);
	paginador.setClaveIF(claveIF);
	paginador.setEstatus(claveEstatus);
	paginador.setEstatusFF(estatusFF);

	// Crear instancia del query helper y el objeto de resultado
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	queryHelper.setMaximoNumeroRegistros(2500);
	
	// Leer posicion de los registros
	int start = 0;
	int limit = 0;
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {

		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		
	}catch(Exception e){
 
		String msg = "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
		
	}
	
	// Realizar consulta
	try {

		if ("NuevaConsulta".equals(operacion)) {	//Nueva consulta
			queryHelper.executePKQuery(request);   //Obtiene las llaves primarias con base a los parametros de consulta
		}
		
		JSONArray 	registros 	= new JSONArray();
		JSONObject 	registro 	= null;
		
		Registros 	consulta 	= queryHelper.getPageResultSet(request,start,limit);

		while(consulta != null && consulta.next()){

			String causaRechazoFFON		= consulta.getString("error_cen_s");
			String estatus 				= consulta.getString("estatus_descripcion");
			String estatusCECOBAN 		= consulta.getString("ic_estatus_cecoban");
			String estatusDEP 			= consulta.getString("cs_reproceso");
			String claveEPO01 			= consulta.getString("ic_epo");
			String clavePYME01 			= consulta.getString("ic_pyme");
			String claveIF01 				= consulta.getString("ic_if");
			String estatusDocumento 	= consulta.getString("ic_estatus_docto");
			String fechaVencimiento 	= consulta.getString("df_fecha_venc");
			String fechaOperacion		= consulta.getString("df_operacion");
			String claveEstatusFFON 	= consulta.getString("status_cen_i");
				
			boolean existenDocumentos 	= dispersion.getExistenDoctosRelacionados(consulta.getString("ic_flujo_fondos")) > 0?true:false;
			
			if( !"99".equals(estatusCECOBAN) && "-1".equals(claveEstatusFFON) ){
				estatus = "Error en la informaci&oacute;n de la cuenta";
			} else {
				estatus = estatus+": "+causaRechazoFFON;
			}
			
			String verDetalle = "";
			if(existenDocumentos) {
				// "<a href=\"javascript:detalleDocs('"+ consulta.getString("ic_flujo_fondos")+"', '"+ TipoDispersion+"','"+ noEstatus+"', '"+ rs_epo+"', '"+ rs_pyme+"', '"+ rs_if+"','"+ rs_estatus_doc+"','"+ rs_fecha_venc+"','"+ rs_operacion+"');\">&nbsp;Ver</a>";
				verDetalle = "javascript:detalleDocs";
			}
		
			// ENVIAR DETALLE DEL REGISTRO
			registro = new JSONObject();

			// CAMPO: EPO
			registro.put("NOMBRE_EPO",					consulta.getString("razon_social_epo")		);
			// CAMPO: IF
			registro.put("NOMBRE_IF",					consulta.getString("razon_social_if")		);
			// CAMPO: PYME
			registro.put("NOMBRE_PYME",				consulta.getString("razon_social_pyme")	);
			// CAMPO: RFC
			registro.put("RFC",							consulta.getString("cg_rfc")					);
			// CAMPO: Intermediario Financiero Fondeo
			registro.put("NOMBRE_IF_FONDEO",			consulta.getString("razon_social_iffon")	);
			// CAMPO: Banco
			registro.put("BANCO",						consulta.getString("ic_bancos_tef")			);
			// CAMPO: Tipo de Cuenta
			registro.put("TIPO_CUENTA",				consulta.getString("ic_tipo_cuenta")		);
			// CAMPO: No. de Cuenta
			registro.put("NUMERO_CUENTA",				consulta.getString("cg_cuenta")				);
			// CAMPO: Importe Total del Descuento
			registro.put("IMPORTE",						Comunes.formatoMN(consulta.getString("fn_importe")));
			// CAMPO: Estatus
			registro.put("ESTATUS",						estatus												);
			// CAMPO: Vía de Liquidación
			registro.put("VIA_LIQUIDACION",			consulta.getString("cg_via_liquidacion")	);
			// CAMPO: Folio
			registro.put("FOLIO",						consulta.getString("ic_flujo_fondos")		);
			// CAMPO: Fecha de Registro
			registro.put("FECHA_REGISTRO",			consulta.getString("df_registro")			);
			// CAMPO: Ver Detalle
			registro.put("EXISTEN_DOCUMENTOS", 		new Boolean(existenDocumentos)				);
			registro.put("VER_DETALLE",				verDetalle											);
			
			// CAMPOS DE APOYO PARA ACCION: VER DETALLE
			
			registro.put("IC_FLUJO_FONDOS",			consulta.getString("ic_flujo_fondos")		); // ic_flujo_fondos
			registro.put("CLAVE_TIPO_DISPERSION",	claveTipoDispersion								); // TipoDispersion
			registro.put("CLAVE_ESTATUS",				claveEstatus										); // noEstatus
			registro.put("CLAVE_EPO",					claveEPO01											); // rs_epo
			registro.put("CLAVE_PYME",					clavePYME01											); // rs_pyme
			registro.put("CLAVE_IF",					claveIF01											); // rs_if
			registro.put("ESTATUS_DOCUMENTO",		estatusDocumento									); // rs_estatus_doc
			registro.put("FECHA_VENCIMIENTO",		fechaVencimiento									); // rs_fecha_venc
			registro.put("FECHA_OPERACION",			fechaOperacion										); // rs_operacion
						
			registros.add(registro);

		}
		
		resultado.put( "total",			new Integer( queryHelper.getIdsSize() )	);
		resultado.put( "registros",	registros 											);
		
		// Obtener los totales
		if( "NuevaConsulta".equals(operacion) ){
			
			Registros 	totales 					= queryHelper.getResultCount(request);
			String 		totalImporte			= "0";
			String 		totalOperaciones		= "0";
			
			if( totales != null && totales.next() ){
				totalImporte 		= totales.getString("TOTAL_IMPORTE");
				totalOperaciones  = totales.getString("TOTAL_OPERACIONES");
			}
	
			resultado.put("totalImporte",	    Comunes.formatoMN(totalImporte.toString())	);
			resultado.put("totalOperaciones", new Integer(totalOperaciones)					);
 
		}
				
	} catch(Exception e) {
		
		log.error("ConsultaResumenOperacion(Exception)");
		log.error("ConsultaResumenOperacion.claveConcepto        = <" + claveConcepto        + ">");
		log.error("ConsultaResumenOperacion.claveTipoDispersion  = <" + claveTipoDispersion  + ">");
		log.error("ConsultaResumenOperacion.claveIF              = <" + claveIF              + ">");
		log.error("ConsultaResumenOperacion.ifOperaFideicomiso   = <" + ifOperaFideicomiso   + ">");
		log.error("ConsultaResumenOperacion.claveEPO             = <" + claveEPO             + ">");
		log.error("ConsultaResumenOperacion.claveEstatus         = <" + claveEstatus         + ">");
		log.error("ConsultaResumenOperacion.claveViaLiquidacion  = <" + claveViaLiquidacion  + ">");
		log.error("ConsultaResumenOperacion.fechaRegistroInicial = <" + fechaRegistroInicial + ">");
		log.error("ConsultaResumenOperacion.fechaRegistroFinal   = <" + fechaRegistroFinal   + ">");
		log.error("ConsultaResumenOperacion.operacion            = <" + operacion            + ">");
		log.error("ConsultaResumenOperacion.start                = <" + start                + ">");
		log.error("ConsultaResumenOperacion.limit                = <" + limit                + ">");
		e.printStackTrace();
		throw new AppException("Error en la paginacion", e);
		
	}	
			
	resultado.put("success",new Boolean(success));
	
	infoRegresar = resultado.toString();	
	
} else if (	informacion.equals("GenerarArchivo")		){
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	String      urlArchivo		= "";
	
	String 		nombreArchivo	= "";
	
	// Leer parametros
	String claveConcepto 			= (request.getParameter("claveConcepto")			== null)?"":request.getParameter("claveConcepto");
	String claveTipoDispersion 	= (request.getParameter("claveTipoDispersion")	== null)?"":request.getParameter("claveTipoDispersion");
	String claveIF 					= (request.getParameter("claveIF")					== null)?"":request.getParameter("claveIF");
	String ifOperaFideicomiso 		= (request.getParameter("ifOperaFideicomiso")	== null)?"":request.getParameter("ifOperaFideicomiso");
	String claveEPO 					= (request.getParameter("claveEPO")					== null)?"":request.getParameter("claveEPO");
	String claveEstatus 				= (request.getParameter("claveEstatus")			== null)?"":request.getParameter("claveEstatus");
	String claveViaLiquidacion 	= (request.getParameter("claveViaLiquidacion")	== null)?"":request.getParameter("claveViaLiquidacion");
	String fechaRegistroInicial	= (request.getParameter("fechaRegistroInicial")	== null)?"":request.getParameter("fechaRegistroInicial");
	String fechaRegistroFinal 		= (request.getParameter("fechaRegistroFinal")	== null)?"":request.getParameter("fechaRegistroFinal");
	String tipo 						= "Consulta" ;
	String estatusFF 					= "";

	String operacion					= (request.getParameter("operacion") 		== null)?"":request.getParameter("operacion");
	String tipoArchivo 				= (request.getParameter("tipoArchivo")		== null)?"":request.getParameter("tipoArchivo");
	
	claveEPO	= "TODAS".equals(claveEPO)?"":claveEPO;
	claveIF	= "TODOS".equals(claveIF) ?"":claveIF;
	
	// Crear instancia del paginador
	ConsultaResumenOperacionErrorFFON paginador = new ConsultaResumenOperacionErrorFFON();
	paginador.setTipo(tipo);
	paginador.setTipoDispersion(claveTipoDispersion);
	paginador.setFechaRegistroInicial(fechaRegistroInicial);
	paginador.setFechaRegistroFinal(fechaRegistroFinal);
	paginador.setViaLiquidacion(claveViaLiquidacion);
	paginador.setClaveEPO(claveEPO);
	paginador.setClaveIF(claveIF);
	paginador.setEstatus(claveEstatus);
	paginador.setEstatusFF(estatusFF);

	// Crear instancia del query helper y el objeto de resultado
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	// Leer posicion de los registros
	int start = 0;
	int limit = 0;
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	// HttpSession session
	String		directorioTemporal		= strDirectorioTemp; 
	String 		directorioPublicacion 	= strDirectorioPublicacion;
	
	// Crear archivo
	try {
		
		if(        "PDF".equals(tipoArchivo) ){
			
			nombreArchivo = queryHelper.getCreatePageCustomFile( request, start, limit ,directorioTemporal, "PDF" ); 
			urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
			
		} else if( "CSV".equals(tipoArchivo) ){
			
			nombreArchivo = queryHelper.getCreateCustomFile( request, directorioTemporal, "CSV" );
			urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
			
		}
		
	} catch(Throwable e) {
		
		log.error("GenerarArchivo(Exception)");
		log.error("GenerarArchivo.claveConcepto        = <" + claveConcepto        + ">");
		log.error("GenerarArchivo.claveTipoDispersion  = <" + claveTipoDispersion  + ">");
		log.error("GenerarArchivo.claveIF              = <" + claveIF              + ">");
		log.error("GenerarArchivo.ifOperaFideicomiso   = <" + ifOperaFideicomiso   + ">");
		log.error("GenerarArchivo.claveEPO             = <" + claveEPO             + ">");
		log.error("GenerarArchivo.claveEstatus         = <" + claveEstatus         + ">");
		log.error("GenerarArchivo.claveViaLiquidacion  = <" + claveViaLiquidacion  + ">");
		log.error("GenerarArchivo.fechaRegistroInicial = <" + fechaRegistroInicial + ">");
		log.error("GenerarArchivo.fechaRegistroFinal   = <" + fechaRegistroFinal   + ">");
		log.error("GenerarArchivo.operacion            = <" + operacion            + ">");
		log.error("GenerarArchivo.tipoArchivo          = <" + tipoArchivo          + ">");
		log.error("GenerarArchivo.start                = <" + start                + ">");
		log.error("GenerarArchivo.limit                = <" + limit                + ">");
		e.printStackTrace();
		
		throw new AppException("Error al generar el Archivo "+ tipoArchivo, e);
		
	}	
 
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
	
} else if (	informacion.equals("GenerarComprobanteTransferencia")		){
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	String      urlArchivo		= "";
	
	String 		nombreArchivo	= "";
	
	// Leer parametros
	String claveConcepto 			= (request.getParameter("claveConcepto")			== null)?"":request.getParameter("claveConcepto");
	String claveTipoDispersion 	= (request.getParameter("claveTipoDispersion")	== null)?"":request.getParameter("claveTipoDispersion");
	String claveIF 					= (request.getParameter("claveIF")					== null)?"":request.getParameter("claveIF");
	String ifOperaFideicomiso 		= (request.getParameter("ifOperaFideicomiso")	== null)?"":request.getParameter("ifOperaFideicomiso");
	String claveEPO 					= (request.getParameter("claveEPO")					== null)?"":request.getParameter("claveEPO");
	String claveEstatus 				= (request.getParameter("claveEstatus")			== null)?"":request.getParameter("claveEstatus");
	String claveViaLiquidacion 	= (request.getParameter("claveViaLiquidacion")	== null)?"":request.getParameter("claveViaLiquidacion");
	String fechaRegistroInicial	= (request.getParameter("fechaRegistroInicial")	== null)?"":request.getParameter("fechaRegistroInicial");
	String fechaRegistroFinal 		= (request.getParameter("fechaRegistroFinal")	== null)?"":request.getParameter("fechaRegistroFinal");
	String tipo 						= "Consulta" ;
	String estatusFF 					= "";

	String operacion					= (request.getParameter("operacion") 		== null)?"":request.getParameter("operacion");
	String tipoArchivo 				= (request.getParameter("tipoArchivo")		== null)?"":request.getParameter("tipoArchivo");
	
	claveEPO	= "TODAS".equals(claveEPO)?"":claveEPO;
	claveIF	= "TODOS".equals(claveIF) ?"":claveIF;
	
	// Crear instancia del paginador
	ConsultaResumenOperacionErrorFFON consultaResumen = new ConsultaResumenOperacionErrorFFON();
	consultaResumen.setTipo(tipo);
	consultaResumen.setTipoDispersion(claveTipoDispersion);
	consultaResumen.setFechaRegistroInicial(fechaRegistroInicial);
	consultaResumen.setFechaRegistroFinal(fechaRegistroFinal);
	consultaResumen.setViaLiquidacion(claveViaLiquidacion);
	consultaResumen.setClaveEPO(claveEPO);
	consultaResumen.setClaveIF(claveIF);
	consultaResumen.setEstatus(claveEstatus);
	consultaResumen.setEstatusFF(estatusFF);
	
	// HttpSession session
	String		directorioTemporal		= strDirectorioTemp; 
	String 		directorioPublicacion 	= strDirectorioPublicacion;
	
	String 		nombreUsuarioReporte 	= strNombreUsuario;
	
	// Crear archivo
	try {
		
		if(        "PDF".equals(tipoArchivo) ){
			
			nombreArchivo = consultaResumen.generaComprobanteTransferencia( request, directorioTemporal, directorioPublicacion, "PDF", nombreUsuarioReporte );
			urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
			
		}
 	
	} catch(Throwable e) {
		
		log.error("GenerarComprobanteTransferencia(Exception)");
		log.error("GenerarComprobanteTransferencia.claveConcepto        = <" + claveConcepto        + ">");
		log.error("GenerarComprobanteTransferencia.claveTipoDispersion  = <" + claveTipoDispersion  + ">");
		log.error("GenerarComprobanteTransferencia.claveIF              = <" + claveIF              + ">");
		log.error("GenerarComprobanteTransferencia.ifOperaFideicomiso   = <" + ifOperaFideicomiso   + ">");
		log.error("GenerarComprobanteTransferencia.claveEPO             = <" + claveEPO             + ">");
		log.error("GenerarComprobanteTransferencia.claveEstatus         = <" + claveEstatus         + ">");
		log.error("GenerarComprobanteTransferencia.claveViaLiquidacion  = <" + claveViaLiquidacion  + ">");
		log.error("GenerarComprobanteTransferencia.fechaRegistroInicial = <" + fechaRegistroInicial + ">");
		log.error("GenerarComprobanteTransferencia.fechaRegistroFinal   = <" + fechaRegistroFinal   + ">");
		log.error("GenerarComprobanteTransferencia.operacion            = <" + operacion            + ">");
		log.error("GenerarComprobanteTransferencia.tipoArchivo          = <" + tipoArchivo          + ">");
		e.printStackTrace();
 
		if( e.getCause() instanceof java.lang.OutOfMemoryError  ){
			throw new AppException("Se acabó la memoria");	
		}
		
		throw new AppException("Error al generar el Archivo "+ tipoArchivo, e);
		
	}	
 
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
	
} else if (	informacion.equals("ConsultaDetalleDocumentos")		){

	JSONObject	resultado 		= new JSONObject();
	boolean		success	 		= true;
	
	// Leer parametros
	String icFlujoFondos 		= (request.getParameter("icFlujoFondos")			== null)?"":request.getParameter("icFlujoFondos");
	String claveEpo 				= (request.getParameter("claveEpo")					== null)?"":request.getParameter("claveEpo");
	String clavePyme 				= (request.getParameter("clavePyme")				== null)?"":request.getParameter("clavePyme");
	String claveIf 				= (request.getParameter("claveIf")					== null)?"":request.getParameter("claveIf");
	String estatusDocumento 	= (request.getParameter("estatusDocumento")		== null)?"":request.getParameter("estatusDocumento");
	String claveTipoDispersion = (request.getParameter("claveTipoDispersion")	== null)?"":request.getParameter("claveTipoDispersion");
	String fechaVencimiento 	= (request.getParameter("fechaVencimiento")		== null)?"":request.getParameter("fechaVencimiento");
	String fechaOperacion 		= (request.getParameter("fechaOperacion")			== null)?"":request.getParameter("fechaOperacion");
	String claveEstatus 			= (request.getParameter("claveEstatus")			== null)?"":request.getParameter("claveEstatus");

	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {

		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		
	}catch(Exception e){
 
		String msg = "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
		
	}
	
	// Realizar consulta
	Vector datos = dispersion.getDetalleDoctos( icFlujoFondos, claveTipoDispersion, claveEstatus, claveEpo, clavePyme, claveIf, estatusDocumento, fechaVencimiento, fechaOperacion );
		
	// Variables Auxiliares
	int 			docsMN 			= 0;
	BigDecimal 	mtoMN 			= new BigDecimal("0");
	BigDecimal 	impMN 			= new BigDecimal("0");
	int 			docsUSD 			= 0;
	BigDecimal 	mtoUSD 			= new BigDecimal("0");
	BigDecimal 	impUSD 			= new BigDecimal("0");
	boolean		bNotaCred 		= false;
	
	// Enviar detalle de la consulta
	JSONArray 	registros 	= new JSONArray();
	JSONObject 	registro  	= null;
	
	for(int i = 0;i<datos.size();i++) {
			
		Vector renglon 				= (Vector)datos.get(i);
		
		String rs_documento 			= renglon.get(1).toString();
		String rs_pyme 				= renglon.get(2).toString();
		String rs_monto 				= renglon.get(3).toString();
		String rs_importe 			= renglon.get(4).toString();
		String rs_moneda 				= renglon.get(5).toString();
		String rs_dscto_especial	= renglon.get(6).toString();
		String auxNota 				= "";
		
		if("C".equals(rs_dscto_especial)) {
			if(!bNotaCred)
				bNotaCred = true; 
			auxNota 		= "*";
			rs_monto 	= "-"+rs_monto;
			rs_importe 	= "-"+rs_importe;
		}
		if("1".equals(rs_moneda)) {
			if(!"C".equals(rs_dscto_especial))
				docsMN++;
			mtoMN = mtoMN.add( new BigDecimal(rs_monto));
			impMN = impMN.add( new BigDecimal(rs_importe));
		} else if("54".equals(rs_moneda)) {
			if(!"C".equals(rs_dscto_especial))
				docsUSD++;
			mtoUSD = mtoUSD.add( new BigDecimal(rs_monto));
			impUSD = impUSD.add( new BigDecimal(rs_importe));
		}
	
		registro = new JSONObject();
		
		if(        "PA".equals(claveEstatus)      ){
			// Fecha de Aplicación
			registro.put("FECHA_APLICACION",		auxNota + " " + rs_documento);
			// PyME
			registro.put("PYME",						rs_pyme);
			// Importe del Pago
			registro.put("IMPORTE_PAGO",			"$ " + Comunes.formatoDecimal(rs_monto,2));
		} else if( !"P".equals(claveTipoDispersion) ){
			// Num. de Documento
			registro.put("NUMERO_DOCUMENTO",		auxNota + " " + rs_documento);
			// PyME
			registro.put("PYME",						rs_pyme);
			// Importe Documento
			registro.put("IMPORTE_DOCUMENTO",	"$ " + Comunes.formatoDecimal(rs_monto,2));
			// Importe Descuento
			registro.put("IMPORTE_DESCUENTO",	"$ " + Comunes.formatoDecimal(rs_importe,2));
		} else {
			// Num. de Documento
			registro.put("NUMERO_DOCUMENTO",		auxNota + " " + rs_documento);
			// PyME
			registro.put("PYME",						rs_pyme);
			// Importe Documento
			registro.put("IMPORTE_DOCUMENTO",	"$ " + Comunes.formatoDecimal(rs_monto,2));
		}
		registro.put("RENGLON",new Boolean("true"));
		
		registros.add(registro);
 
	}
	
	if( docsMN > 0 ) {
		
		registro = new JSONObject();
		
		if(        "PA".equals(claveEstatus)      ){
			// Fecha de Aplicación
			registro.put("FECHA_APLICACION",		"Total de " );
			// PyME
			registro.put("PYME",						"Documentos M. N.  " + docsMN);
			// Importe del Pago
			registro.put("IMPORTE_PAGO",			"$ " + Comunes.formatoDecimal(mtoMN.toPlainString(),2));
		} else if( !"P".equals(claveTipoDispersion) ){
			// Num. de Documento
			registro.put("NUMERO_DOCUMENTO",		"Total de ");
			// PyME
			registro.put("PYME",						"Documentos M. N.  " + docsMN);
			// Importe Documento
			registro.put("IMPORTE_DOCUMENTO",	"$ " + Comunes.formatoDecimal(mtoMN.toPlainString(),2));
			// Importe Descuento
			registro.put("IMPORTE_DESCUENTO",	"$ " + Comunes.formatoDecimal(impMN.toPlainString(),2));
		} else {
			// Num. de Documento
			registro.put("NUMERO_DOCUMENTO",		"Total de ");
			// PyME
			registro.put("PYME",						"Documentos M. N.  " + docsMN);
			// Importe Documento
			registro.put("IMPORTE_DOCUMENTO",	"$ " + Comunes.formatoDecimal(mtoMN.toPlainString(),2));
		}
		registro.put("RENGLON",new Boolean("false"));
		
		registros.add(registro);
		
	}
	
	if( docsUSD > 0 ) {
		
		registro = new JSONObject();
		
		if(        "PA".equals(claveEstatus)      ){
			// Fecha de Aplicación
			registro.put("FECHA_APLICACION",		"Total de ");
			// PyME
			registro.put("PYME",						"Documentos Dolares  " + docsUSD );
			// Importe del Pago
			registro.put("IMPORTE_PAGO",			"$ " + Comunes.formatoDecimal(mtoUSD.toPlainString(),2));
		} else if( !"P".equals(claveTipoDispersion) ){
			// Num. de Documento
			registro.put("NUMERO_DOCUMENTO",		"Total de ");
			// PyME
			registro.put("PYME",						"Documentos Dolares  " + docsUSD );
			// Importe Documento
			registro.put("IMPORTE_DOCUMENTO",	"$ " + Comunes.formatoDecimal(mtoUSD.toPlainString(),2));
			// Importe Descuento
			registro.put("IMPORTE_DESCUENTO",	"$ " + Comunes.formatoDecimal(impUSD.toPlainString(),2));
		} else {
			// Num. de Documento
			registro.put("NUMERO_DOCUMENTO",		"Total de ");
			// PyME
			registro.put("PYME",						"Documentos Dolares  " + docsUSD );
			// Importe Documento
			registro.put("IMPORTE_DOCUMENTO",	"$ " + Comunes.formatoDecimal(mtoUSD.toPlainString(),2));
		}
		registro.put("RENGLON",new Boolean("false"));
		
		registros.add(registro);
		
	}
	
	resultado.put("showLabelNotaCredito",new Boolean(bNotaCred));
	
	// Regresar resultados	
	resultado.put("total",			new Integer( registros.size() )	);
	resultado.put("registros",		registros 								);
	resultado.put("success",		new Boolean(success)					);
	
	infoRegresar = resultado.toString();
	
} else if (	informacion.equals("GenerarArchivoDetalleDocumentos")		){
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	String      urlArchivo		= "";
	
	String 		nombreArchivo	= "";
	
	// Leer parametros
	String icFlujoFondos 		= (request.getParameter("icFlujoFondos")			== null)?"":request.getParameter("icFlujoFondos");
	String claveEpo 				= (request.getParameter("claveEpo")					== null)?"":request.getParameter("claveEpo");
	String clavePyme 				= (request.getParameter("clavePyme")				== null)?"":request.getParameter("clavePyme");
	String claveIf 				= (request.getParameter("claveIf")					== null)?"":request.getParameter("claveIf");
	String estatusDocumento 	= (request.getParameter("estatusDocumento")		== null)?"":request.getParameter("estatusDocumento");
	String claveTipoDispersion = (request.getParameter("claveTipoDispersion")	== null)?"":request.getParameter("claveTipoDispersion");
	String fechaVencimiento 	= (request.getParameter("fechaVencimiento")		== null)?"":request.getParameter("fechaVencimiento");
	String fechaOperacion 		= (request.getParameter("fechaOperacion")			== null)?"":request.getParameter("fechaOperacion");
	String claveEstatus 			= (request.getParameter("claveEstatus")			== null)?"":request.getParameter("claveEstatus");
 
	String tipoArchivo 			= (request.getParameter("tipoArchivo")				== null)?"":request.getParameter("tipoArchivo");
 
	// Preparar clases adicionales
	ConsultaResumenOperacionErrorFFON 		consulta 	= new ConsultaResumenOperacionErrorFFON();
	HashMap 									 		parametros 	= new HashMap();
	parametros.put("icFlujoFondos",			icFlujoFondos			);
	parametros.put("claveEpo",					claveEpo					);
	parametros.put("clavePyme",				clavePyme				);
	parametros.put("claveIf",					claveIf					);
	parametros.put("estatusDocumento",		estatusDocumento		);
	parametros.put("claveTipoDispersion",	claveTipoDispersion	);
	parametros.put("fechaVencimiento",		fechaVencimiento		);
	parametros.put("fechaOperacion",			fechaOperacion			);
	parametros.put("claveEstatus",			claveEstatus			);

	// HttpSession session
	String		directorioTemporal		= strDirectorioTemp; 
	String 		directorioPublicacion 	= strDirectorioPublicacion;
	
	// Crear archivo
	try {
		
		if( "CSV".equals(tipoArchivo) ){
			nombreArchivo = consulta.generaArchivoCSVDetalleDocumentos( directorioTemporal, parametros );
			urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
		}
		
	} catch(Throwable e) {
		
		log.error("GenerarArchivoDetalleDocumentos(Exception)");
		log.error("GenerarArchivoDetalleDocumentos.icFlujoFondos 		= <" + icFlujoFondos       + ">");
		log.error("GenerarArchivoDetalleDocumentos.claveEpo 				= <" + claveEpo            + ">");
		log.error("GenerarArchivoDetalleDocumentos.clavePyme 				= <" + clavePyme           + ">");
		log.error("GenerarArchivoDetalleDocumentos.claveIf 				= <" + claveIf             + ">");
		log.error("GenerarArchivoDetalleDocumentos.estatusDocumento 	= <" + estatusDocumento    + ">");
		log.error("GenerarArchivoDetalleDocumentos.claveTipoDispersion = <" + claveTipoDispersion + ">");
		log.error("GenerarArchivoDetalleDocumentos.fechaVencimiento 	= <" + fechaVencimiento    + ">");
		log.error("GenerarArchivoDetalleDocumentos.fechaOperacion 		= <" + fechaOperacion      + ">");
		log.error("GenerarArchivoDetalleDocumentos.claveEstatus 			= <" + claveEstatus        + ">");
		log.error("GenerarArchivoDetalleDocumentos.tipoArchivo         = <" + tipoArchivo         + ">");
		e.printStackTrace();
 
		if( e.getCause() instanceof java.lang.OutOfMemoryError  ){
			throw new AppException("Se acabó la memoria");	
		}
		
		throw new AppException("Error al generar el Archivo "+ tipoArchivo, e);
		
	}	
 
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>