Ext.onReady(function(){
//---------------------------descargaArchivo------------------------------------
 function descargaArchivo(opts, success, response) {
	
if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');			
			var archivo = infoR.urlArchivo;
      //archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			fp.getForm().getEl().dom.action =  archivo;
			fp.getForm().getEl().dom.submit();
			Ext.getCmp('btnGenerarCSV').enable();
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------fin descargaArchivo--------------------------------

//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
	var 	fp = Ext.getCmp('formcon');
			fp.el.unmask();				
	var 	gridConsulta = Ext.getCmp('gridConsulta');	
	var 	el = gridConsulta.getGridEl();	
	if (arrRegistros != null) {
		if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
		if(store.getTotalCount() > 0) {					
				el.unmask();
				Ext.getCmp('btnGenerarCSV').enable();				
			} else {		
				Ext.getCmp('btnGenerarCSV').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
var consultaData = new Ext.data.JsonStore({
		root 			: 'registros',
		url 			: '15BitacoraCancelacionesEXT.data.jsp',
		baseParams	: {
							informacion: 'consultaGeneral'
							},
		hidden		: true,
		fields		: [	
							{	name: 'nombreepo'},	
							{	name: 'nombreIf'},	
							{	name: 'nombrePyme'},	
							{	name: 'rfc'},	
							{	name: 'banco'},	
							{	name: 'tipocuenta'},	
							{	name: 'nocuenta'},	
							{	name: 'importe'},	
							{	name: 'motivoRechazo'},
							{	name: 'verDetalle'},
							{	name: 'usuario'},							
							{	name: 'fcancelacion'},
							{	name: 'causa'}
							],		
		totalProperty 	: 'total',
		messageProperty: 'msg',
		autoLoad			: false,
		listeners		: {
			load			: procesarConsultaData,
				exception: {
						fn	: function(proxy, type, action, optionsRequest, response, args) {
								NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
								//LLama procesar consulta, para que desbloquee los componentes.
								procesarConsultaData(null, null, null);					
								}
								}
								}					
	})
//----------------------------Fin ConsultaData----------------------------------

//------------------------------------Tran-------------------------------------
var Epo = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin Tran------------------------------------

//---------------------------- procesarEpo--------------------------------
	var procesarEpo = function(store, arrRegistros, opts) {
		store.insert(0,new Epo({ 
		clave: "", 
		descripcion: "TODAS", 
		loadMsg: ""})); 		
    Ext.getCmp('cmbepo').setValue("");
		store.commitChanges(); 
	}	
//----------------------------Fin procesarEpo--------------------------------


//------------------------------Catalogo EPO------------------------------------
 var catalogoEpo = new Ext.data.JsonStore({
		id					: 'catalogoEpo',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '15BitacoraCancelacionesEXT.data.jsp',
		baseParams		: {
								informacion		: 'catalogoepo'  
								},
		totalProperty 	: 'total',
		autoLoad			: false,
		listeners		: {			
                load: procesarEpo,
								beforeload: NE.util.initMensajeCargaCombo,
								exception: NE.util.mostrarDataProxyError								
								}
	})
//----------------------------Fin Catalogo EPO----------------------------------

//------------------------------------If-------------------------------------
var If = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin If------------------------------------

//---------------------------- procesarIf--------------------------------
	var procesarIf = function(store, arrRegistros, opts) {
		store.insert(0,new If({ 
		clave: "", 
		descripcion: "TODOS", 
		loadMsg: ""})); 		
    Ext.getCmp('cmbif').setValue("");
		store.commitChanges(); 
	}	
//----------------------------Fin procesarIf--------------------------------

//------------------------------Catalogo IF-------------------------------------
 var catalogoIf = new Ext.data.JsonStore({
		id					: 'catalogoIf',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '15BitacoraCancelacionesEXT.data.jsp',
		baseParams		: {
								informacion		: 'catalogoif'  
								},
		totalProperty 	: 'total',
		autoLoad			: false,
		listeners		: {
                load: procesarIf,
								beforeload: NE.util.initMensajeCargaCombo,
								exception: NE.util.mostrarDataProxyError		
								}
	})
//------------------------------Fin Catalogo IF---------------------------------

//-------------------------Store tipoDispersion---------------------------------
	var tipoDispersion = new Ext.data.ArrayStore({
		 fields		: ['clave', 'dispersion'],
		 data 		: [
							['S','Seleccionar'],	
							['E','EPOS'],
							['F','IF�s']
						]
		})
//--------------------------Fin Store tipoDispersion----------------------------
	
//-------------------------------Elementos Forma--------------------------------
	var  elementosForma =  [
		{
		xtype				: 'combo',
		name				: 'cmbtd',
		hiddenName		: '_cmbtd',
		id					: 'cmbTipoDispersion',	
		fieldLabel		: 'Tipo de Dispersi�n', 
		mode				: 'local',	
		//emptyText		: 'Seleccionar',
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,	
		store 			: tipoDispersion,		
		valueField 		: 'clave',
		displayField 	: 'dispersion',	
    editable			: true,
		typeAhead		: true,
		//allowBlank		: false,
		listeners		: {
			select		: {
				fn			: function(combo) {
								var valor  = combo.getValue();
if (valor == 'E') { 
				Ext.getCmp('fr').reset();
				Ext.getCmp('fc').reset();
				Ext.getCmp('cmbif').reset();
				Ext.getCmp('cmbif').hide();
				Ext.getCmp('cmbepo').show();
				Ext.getCmp('fr').show();
				Ext.getCmp('fc').show();
				Ext.getCmp('btnConsultar').show();
				Ext.getCmp('btnLimpiar').show();
 } else if (valor == 'F' ) { 
				Ext.getCmp('fr').reset();
				Ext.getCmp('fc').reset();
				Ext.getCmp('cmbepo').reset();
				Ext.getCmp('cmbepo').hide();
				Ext.getCmp('cmbif').show();
				Ext.getCmp('fr').show();
				Ext.getCmp('fc').show();
				Ext.getCmp('btnConsultar').show();
				Ext.getCmp('btnLimpiar').show();										
 }
									}// FIN fn
								}//FIN select
							}//FIN listeners
		},{
		xtype				: 'combo',	
		hidden			: true,
		name				: 'cmb_epo',	
		hiddenName		: '_cmb_epo',	
		id					: 'cmbepo',	
		fieldLabel		: 'EPO', 
		mode				: 'local',
		editable			: true,	
		displayField	: 'descripcion',
		valueField 		: 'clave',
		//emptyText		: 'Todas las Epos',
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,	
		store 			: catalogoEpo,
		tpl				: NE.util.templateMensajeCargaComboConDescripcionCompleta
		},{
		xtype				: 'combo',	
		hidden			: true,
		name				: 'cmb_epo',	
		hiddenName		: '_cmb_if',	
		id					: 'cmbif',	
		fieldLabel		: 'IF', 
		mode				: 'local',
		editable			: true,
		displayField 	: 'descripcion',
		valueField 		: 'clave',
		//emptyText		: 'Todos los IFs',
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,	
		store 			: catalogoIf,
		tpl				: NE.util.templateMensajeCargaComboConDescripcionCompleta
		},{
		xtype				: 'compositefield',
		id					:'fr',
		combineErrors	: false,
		msgTarget		: 'side',
		items				:[{
							xtype			: 'datefield',
							fieldLabel	: 'Fecha de Registro',
							name			: '_fechaReg',
							id				: 'fechaRegistro',
							hiddenName	: 'fecha_registro',
							allowBlank	: true,
							startDay		: 0,
							width			: 100,
							minValue		: '01/01/1901',
							msgTarget	: 'side',
							margins		: '0 20 0 0'
							},	{
							xtype			: 'displayfield',
							value			: 'al:',
							width			: 10
							},	{
							xtype			: 'datefield',
							name			: '_fechaFinReg',
							id				: 'fechaFinRegistro',
							hiddenName	: 'fecha_fin_reg',
							allowBlank	: true,
							startDay		: 0,
							width			: 100,
							minValue		: '01/01/1901',
							msgTarget	: 'side',
							margins		: '0 20 0 0'
							}]
		},{
		xtype				: 'compositefield',
		id					:'fc',
		combineErrors	: false,
		msgTarget		: 'side',
		anchor			: '100%',
		items				:[{
							xtype			: 'datefield',
							fieldLabel	: 'Fecha de Cancelaci�n',
							name			: '_fechaCan',
							id				: 'fechaCancelacion',
							hiddenName	: 'fecha_cancelacion',
							allowBlank	: true,
							startDay		: 0,
							width			: 100,
							minValue		: '01/01/1901',
							msgTarget	: 'side',
							margins		: '0 20 0 0'
							},	{
							xtype			: 'displayfield',
							value			: 'al:',
							width			: 10
							},	{
							xtype			: 'datefield',
							name			: '_fechaFinCan',
							id				: 'fechaFinCancelacion',
							hiddenName	: 'fecha_fin_can',
							allowBlank	: true,
							startDay		: 0,
							width			: 100,
							minValue		: '01/01/1901',
							msgTarget	: 'side',
							margins		: '0 20 0 0'
							}]
		}]
//-----------------------------Fin Elementos Forma------------------------------

//--------------------------------Grid Consulta---------------------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store				:consultaData,
		id					:'gridConsulta',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Consulta - EPO',	
		clicksToEdit	:1,
		hidden			:true,
		columns			:[{
							header		:'EPO',
							tooltip		:'EPO',
							dataIndex	:'nombreepo',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'left'
							},{
							header		:'IF',
							tooltip		:'IF',
							dataIndex	:'nombreIf',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'left'				
							},{
							header		:'Pyme',
							tooltip		:'Pyme',
							dataIndex	:'nombrePyme',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'left'
							},{
							header		:'RFC',
							tooltip		:'RFC',
							dataIndex	:'rfc',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center'							
							},{
							header		:'Banco',
							tooltip		:'Banco',
							dataIndex	:'banco',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Tipo de Cuenta',
							tooltip		:'Tipo de Cuenta',
							dataIndex	:'tipocuenta',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'				
							},{
							header		:'No. Cuenta',
							tooltip		:'No. Cuenta',
							dataIndex	:'nocuenta',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Importe',
							tooltip		:'Importe',
							dataIndex	:'importe',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Motivo de Rechazo',
							tooltip		:'Motivo de Rechazo',
							dataIndex	:'motivoRechazo',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'				
							},{
							header		: 'Ver Detalle',
							tooltip		: 'Ver Detalle',
							dataIndex	: 'verDetalle',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		: 'Nombre del Usuario',
							tooltip		: 'Nombre del Usuario',
							dataIndex	: 'usuario',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		: 'Fecha de Cancelaci�n',
							tooltip		: 'Fecha Cancelaci�n',
							dataIndex	: 'fcancelacion',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center'				
							},	{
							header		: 'Causa',
							tooltip		: 'Causa',
							dataIndex	: 'causa',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							}],	
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 350,
      width				: 800,
		align				: 'center',
		frame				: false,
		bbar				: {
							store			: consultaData,
							emptyMsg		: "No hay registros.",
							items			: [
											'->','-',
											{
											xtype			: 'button',
											text			: 'Generar Archivo',					
											tooltip		:	'Generar Archivo ',
											iconCls		: 'icoXls',
											id				: 'btnGenerarCSV',
											handler		: function(boton, evento) {
																fp.el.mask('Generando archivo...', 'x-mask-loading');
																boton.setIconClass('loading-indicator');
																Ext.getCmp('btnGenerarCSV').disable();
																Ext.Ajax.request({
																	url: '15BitacoraCancelacionesEXT.data.jsp',
																	params: Ext.apply(fp.getForm().getValues(),{
																				informacion: 'GeneraArchivoCSV'			
																				}),																						
																callback: descargaArchivo
																})
															}
											}]
								}
	})
//-----------------------------Fin Grid Consulta--------------------------------
		
//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id					:'formcon',
		width				:600,
		heigth			:'auto',
		title				:'Consulta',
		layout			:'form',
		frame				:true,
		collapsible		:true,
		titleCollapse	:false,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 5px',
		labelWidth		:150,
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosForma
							],
		buttons			:[{
							text				:'Consultar',
							id					:'btnConsultar',
							iconCls			:'icoBuscar',
							//formBind			:true,
							handler			:function(boton, evento){	
                var combo = Ext.getCmp('cmbTipoDispersion').getValue();
                if (combo == 'S' || combo == ''){
                  Ext.Msg.show({
                  title: 'Tipo de Dispersion',
                  msg: 'Debe seleccionar un tipo de Dispersi�n',
                      modal: true,
                      icon: Ext.Msg.WARNING,
                      buttons: Ext.Msg.OK
                   });
                  } else { 
							
							var fechaMinReg = Ext.getCmp('fechaRegistro');
							var fechaMaxReg = Ext.getCmp('fechaFinRegistro');
							if (!Ext.isEmpty(fechaMinReg.getValue()) | !Ext.isEmpty(fechaMaxReg.getValue()) ) {
									if(Ext.isEmpty(fechaMinReg.getValue()))	{
										fechaMinReg.markInvalid('Debe capturar ambas fechas de Registro o dejarlas en blanco.');
										fechaMinReg.focus();
										return;
									}else if (Ext.isEmpty(fechaMaxReg.getValue())){
										fechaMaxReg.markInvalid('Debe capturar ambas fechas de Registro o dejarlas en blanco.');
										fechaMaxReg.focus();
										return;
									}
								}
							var fechaMin = Ext.getCmp('fechaCancelacion');
							var fechaMax = Ext.getCmp('fechaFinCancelacion');
							if (!Ext.isEmpty(fechaMin.getValue()) | !Ext.isEmpty(fechaMax.getValue()) ) {
									if(Ext.isEmpty(fechaMin.getValue()))	{
										fechaMin.markInvalid('Debe capturar ambas fechas de Cancelaci�n o dejarlas en blanco.');
										fechaMin.focus();
										return;
									}else if (Ext.isEmpty(fechaMax.getValue())){
										fechaMax.markInvalid('Debe capturar ambas fechas de Cancelaci�n o dejarlas en blanco.');
										fechaMax.focus();
										return;
									}
								}		
							if(Ext.getCmp('fechaRegistro').isValid() & Ext.getCmp('fechaFinRegistro').isValid()){							
							} else {return;}
							if(Ext.getCmp('fechaCancelacion').isValid() & Ext.getCmp('fechaFinCancelacion').isValid()){							
							} else {return;}
							fp.el.mask('Cargando...', 'x-mask-loading');	
							consultaData.load({
							params		:Ext.apply(fp.getForm().getValues(),{
							informacion	:'consultaGeneral'
											})
									}); }	
								}//fin handler 
							},{
							text				:'Limpiar',
							id					:'btnLimpiar',
							iconCls			:'icoLimpiar',
							//formBind			:false,
							handler			:function(boton, evento){
												Ext.getCmp('formcon').getForm().reset();
												Ext.getCmp('cmbif').hide();
												Ext.getCmp('cmbepo').hide();
												Ext.getCmp('gridConsulta').hide();
                        Ext.getCmp('cmbTipoDispersion').setValue("S");
												}
							}]
	})
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		: 900,
		height	: 600,
		style		:'margin:0 auto;',
		items		:[
					NE.util.getEspaciador(20),
					fp,
					NE.util.getEspaciador(20),
					gridConsulta,
					NE.util.getEspaciador(20)
					]
	})
//-----------------------------Fin Contenedor Principal-------------------------
Ext.getCmp('cmbTipoDispersion').setValue("S");
catalogoEpo.load();
catalogoIf.load();
})//-----------------------------------------------Fin Ext.onReady(function(){}