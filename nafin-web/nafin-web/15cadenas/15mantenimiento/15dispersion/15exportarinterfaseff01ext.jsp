<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%

	String numeroDocumentos 		= (request.getParameter("numeroDocumentos")			== null)? "":request.getParameter("numeroDocumentos");
	String importeTotal 				= (request.getParameter("importeTotal")				== null)? "":request.getParameter("importeTotal");
	String numeroDocumentosError	= (request.getParameter("numeroDocumentosError")	== null)?"0":request.getParameter("numeroDocumentosError");
	String importeTotalError 		= (request.getParameter("importeTotalError")			== null)? "":request.getParameter("importeTotalError");
	String claveEPO 					= (request.getParameter("claveEPO")						== null)? "":request.getParameter("claveEPO");
	String claveEstatus 				= (request.getParameter("claveEstatus")				== null)? "":request.getParameter("claveEstatus");
	String claveMoneda 				= (request.getParameter("claveMoneda")					== null)? "":request.getParameter("claveMoneda");
	String fechaVencimiento 		= (request.getParameter("fechaVencimiento")			== null)? "":request.getParameter("fechaVencimiento");
	String claveIF 					= (request.getParameter("claveIF")						== null)? "":request.getParameter("claveIF");
	String claveIfFondeo 			= (request.getParameter("claveIfFondeo")				== null)? "":request.getParameter("claveIfFondeo");
	String origen 						= (request.getParameter("origen")						== null)? "":request.getParameter("origen");
	String fechaOperacion 			= (request.getParameter("fechaOperacion")				== null)? "":request.getParameter("fechaOperacion");
	String pantallaOrigen 			= (request.getParameter("pantallaOrigen")				== null)? "":request.getParameter("pantallaOrigen");
	
%>
<%@ include file="/extjs.jspf" %>
<% if( esEsquemaExtJS ){ %>
	<%@ include file="/01principal/menu.jspf"%>
<% } %>
<script language="JavaScript">
	Ext.onReady( function() {
			
		Ext.ns('NE.exportarinterfaseff');
	
		NE.exportarinterfaseff.inputParams = {
			numeroDocumentos:         "<%= numeroDocumentos %>",
			importeTotal:         "<%= importeTotal %>",
			numeroDocumentosError:      "<%= numeroDocumentosError %>",
			importeTotalError:      "<%= importeTotalError %>",
			claveEPO:            "<%= claveEPO %>",
			claveEstatus:        "<%= claveEstatus %>",
			claveMoneda:         "<%= claveMoneda %>",
			fechaVencimiento:        "<%= fechaVencimiento %>",
			claveIF:             "<%= claveIF %>",
			claveIfFondeo:		"<%= claveIfFondeo %>",
			origen:           "<%= origen %>",
			fechaOperacion:   "<%= fechaOperacion %>",
			pantallaOrigen:	"<%= pantallaOrigen %>"
		};
		
	});
</script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/MsgCheckColumn.js"></script>
<script type="text/javascript" src="15exportarinterfaseff01ext.js?<%=session.getId()%>"></script>
<script language="JavaScript1.2" src="<%=appWebContextRoot%>/00utils/valida.js?<%=session.getId()%>"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<% } %>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
	<% } %>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
	<% } %>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>