<%@ page 
	contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoIFMonitorDispersion,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		netropology.utilerias.ElementoCatalogo,
		java.sql.PreparedStatement,
		java.sql.ResultSet,
		java.math.BigDecimal"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("MonitorDispersion.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {

		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		
	}catch(Exception e){
 
		msg = "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
		
		//success	= false;
		//resultado.put( "msg", msg);
		
	}
	
	// 2. Determinar el valor default del Catálogo de IF
	String clavesIFNafin 			 = dispersion.getIfNafin();
	String catalogoIFdefaultValue  = clavesIFNafin.replaceAll(",.*$","");
	resultado.put("catalogoIFdefaultValue",catalogoIFdefaultValue);
	
	// 3. Determinar el valor default del Catálogo de EPO
	String catalogoEPOdefaultValue = String.valueOf(dispersion.getEpoInfonavit());
	resultado.put("catalogoEPOdefaultValue",catalogoEPOdefaultValue);
	
	// 4. Determinar el estado siguiente
	estadoSiguiente = "CARGAR_CATALOGOS_FORMA";
 
	// 5. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if (        informacion.equals("MonitorDispersion.cargarCatalogosForma") )	{

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CatalogoTipoMonitoreo")            )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Crear Catalogo	
	JSONArray  registros = new JSONArray();
	JSONObject registro	= null;
	
	registro = new JSONObject();
	registro.put("clave",			"E");
	registro.put("descripcion",	"EPO's");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"F");
	registro.put("descripcion",	"IF's");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"FF");
	registro.put("descripcion",	"IF's FIDEICOMISO");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"I");
	registro.put("descripcion",	"INFONAVIT");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"P");
	registro.put("descripcion",	"PEMEX-REF");
	registros.add( JSONObject.fromObject(registro) );
	
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)				);
	resultado.put("total",    	new Integer(registros.size()) );
	resultado.put("registros",	registros			 				);
	
	infoRegresar = resultado.toString();
 
} else if (        informacion.equals("MonitorDispersion.mostrarPantallaMonitoreo") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	String tipoMonitoreo = (request.getParameter("tipoMonitoreo")		== null)?"":request.getParameter("tipoMonitoreo");
	
	// Determinar el estado siguiente
	if(        "E".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispe01ext.jsp");
	} else if( "F".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispi01ext.jsp");
	} else if( "I".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispinfo01ext.jsp");
	} else if( "P".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispPEMEX01ext.jsp");
	} else if( "FF".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispf01ext.jsp");
	} else {
		estadoSiguiente = "ESPERAR_DECISION";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CatalogoIF")            )	{
	
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
		
	}
	
	// 2. Clave(s) del IF Nafin 
	String clavesIFNafin = dispersion.getIfNafin();
	
	// 3. Consultar elementos del catalogo
	CatalogoIF cat = new CatalogoIF();
	cat.setClave("ic_if");
	cat.setDescripcion("cg_razon_social");
	cat.setListaClavesIF(clavesIFNafin); 
	List listaElementos = cat.getListaElementosMonitorDispersionINFONAVIT();
	
	// Enviar respuesta
	infoRegresar    = cat.getJSONElementos(listaElementos);
	
} else if(  informacion.equals("CatalogoEPO")            )	{

	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
 
	}
 
	// 2. Clave(s) de las EPO INFONAVIT 
	String epoINFONAVIT = String.valueOf(dispersion.getEpoInfonavit());
	
	// 3. Consultar elementos del catalogo
	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");
	cat.setHabilitado("S"); 
	cat.setListaClavesEPO(epoINFONAVIT);
	List listaElementos = cat.getListaElementosMonitorDispersionINFONAVIT();
	
	// Enviar respuesta
	infoRegresar      = cat.getJSONElementos(listaElementos);

} else if(  informacion.equals("ConsultaDetalleMonitoreoINFONAVIT")            )	{

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Leer parametros
	String cmb_tipo_monitor = (request.getParameter("claveTipoMonitoreo")	== null)?"":request.getParameter("claveTipoMonitoreo");
	String ic_if 				= (request.getParameter("claveIF")					== null)?"":request.getParameter("claveIF");
	String ic_epo 				= (request.getParameter("claveEPO")					== null)?"":request.getParameter("claveEPO");
	String chk_cuenta 		= (request.getParameter("sinCuentaCLABE")			== null)?"":request.getParameter("sinCuentaCLABE");
	String rad_hab 			= (request.getParameter("radioHabilitadas")		== null)?"":request.getParameter("radioHabilitadas");
		
	JSONArray 	registros 			= new JSONArray();
	JSONObject	registro				= null;
	JSONObject	summaryData			= new JSONObject();
		
	// 2. Obtener instancia de EJB de Dispersion
	Dispersion 	dispersion 			= null;
	try {
						
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
						
	} catch(Exception e) {
		 
		msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
				
		throw new AppException(msg);
		 
	}
	
	// 4. Variables Auxiliares
	AccesoDB				con					= new AccesoDB();
	String 				qrySentencia 		= null;
	PreparedStatement ps				 		= null;
	ResultSet			rs				 		= null;
	int 					i				 		= 0;
	
	String 				tipoColor 			= "formas";
	
	try {
		
		// Conectarse a la Base de Datos
		con.conexionDB();
 
		// Obtener el query de la consulta
		if("I".equals(cmb_tipo_monitor)) {
			qrySentencia = dispersion.getQueryDetalleMonitoreoDispersionPorInfonavit(ic_if,ic_epo,rad_hab,chk_cuenta);			
		}
		log.debug("ConsultaDetalleMonitoreoINFONAVIT.qrySentencia = <" + qrySentencia+ ">");
			
		// Realizar consulta
		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
			
		i 										= 0;
		String 		rs_if 				= "";
		String 		aux_if 				= "";
		String 		groupId 				= "";
		String 		groupIdAnterior 	= "";
		
		int 			totalDocumentos	= 0;
		BigDecimal 	totalMonto 			= new BigDecimal(0);
			
		boolean 		existetabla 		= false;
		while(rs.next()) {
				
			rs_if 					= rs.getString(1)==null?"":rs.getString(1); // Clave del IF
			String rs_nombreIF 	= rs.getString(2)==null?"":rs.getString(2); // Nombre del IF
				
			groupId 					= rs_if;
				
			if(!rs_if.equals(aux_if)) {
					
				i			= 0;
				aux_if 	= rs_if;
					
				if(existetabla){
						
					// AGREGAR LOS TOTALES AL SUMMARYDATA
					JSONArray  summaryDataRegisters = new JSONArray();
					JSONObject summaryDataRegister  = null;
						
					summaryDataRegister  = new JSONObject();
					summaryDataRegister.put( "NOMBRE_EPO", 					"Totales"												);
					summaryDataRegister.put( "NUMERO_TOTAL_DOCUMENTOS",	String.valueOf(totalDocumentos)					);
					summaryDataRegister.put( "MONTO_TOTAL", 					Comunes.formatoMN(totalMonto.toPlainString())		);
					summaryDataRegister.put( "ESTATUS_CECOBAN", 				"99"														); // PARA QUE LOS TOTALES NO SE PINTEN EN ROJO
					summaryDataRegisters.add( summaryDataRegister );
					
					summaryData.put(groupIdAnterior,summaryDataRegisters);
 
					// Resetear totales en Moneda Nacional
					totalDocumentos = 0;
					totalMonto = new BigDecimal(0);
						
				}
		
			}
				
			if(i==0) {
				existetabla = true;	
			}
				
			// Leer datos
			String rs_epo			= rs.getString(3) ==null?"":rs.getString(3);
			String rs_nafElect	= rs.getString(4) ==null?"":rs.getString(4);
			String rs_pyme			= rs.getString(5) ==null?"":rs.getString(5);
			String rs_benef		= rs.getString(6) ==null?"":rs.getString(6);
			String rs_habilitada	= rs.getString(7) ==null?"":rs.getString(7);
			String rs_totalDocs	= rs.getString(8) ==null?"":rs.getString(8);
			String rs_montoDocs	= rs.getString(9) ==null?"":rs.getString(9);
			String rs_banco		= rs.getString(10)==null?"":rs.getString(10);
			String rs_cuenta		= rs.getString(11)==null?"":rs.getString(11);
			String rs_cecoban		= rs.getString(12)==null?"":rs.getString(12);
			String rs_descCec		= rs.getString(13)==null?"":rs.getString(13);
				
			totalDocumentos 		+= Integer.parseInt(rs_totalDocs);
			totalMonto 				=  totalMonto.add(new BigDecimal(rs_montoDocs));
				
			// Determinar el tipo de color del registro presentado
			if(!"99".equals(rs_cecoban))
				tipoColor = "cuenta"; // Letras en Rojo => La cuenta no es valida
			else 
				tipoColor = "formas";
				
			// Si la cuenta no es valida, no mostrar nada en los siguientes campos 
			if("0".equals(rs_cuenta)){
				rs_banco		= "";
				rs_cuenta	= "";
				rs_cecoban	= "";
				rs_descCec	= "";
			}
 
			// AGREGAR REGISTROS ASOCIADOS AL GRUPO IF
			registro = new JSONObject();
			registro.put("IC_IF",							rs_if											); // IC_IF ( GROUP_ID )
			registro.put("NOMBRE_IF",						rs_nombreIF									); // NOMBRE_IF			
			registro.put("NOMBRE_EPO",						rs_epo										); // EPO, NOMBRE_EPO
			registro.put("NUMERO_NAFIN_ELECTRONICO",	rs_nafElect									); // Num. Nafin Electrónico, NUMERO_NAFIN_ELECTRONICO
			registro.put("NOMBRE_PYME",					rs_pyme										); // PYME, NOMBRE_PYME
			registro.put("BENEFICIARIO",					rs_benef										); // Beneficiario, BENEFICIARIO
			registro.put("HABILITADA",						"H".equals(rs_habilitada)?"Si":"No"	); // Habilitada, HABILITADA
			registro.put("NUMERO_TOTAL_DOCUMENTOS",	rs_totalDocs								); // Num. Total de Doctos., NUMERO_TOTAL_DOCUMENTOS
			registro.put("MONTO_TOTAL",					Comunes.formatoMN(rs_montoDocs)		); // Monto Total, MONTO_TOTAL
			registro.put("CODIGO_BANCO_CUENTA_CLABE",	rs_banco										); // Código Banco Cuenta Clabe, CODIGO_BANCO_CUENTA_CLABE
			registro.put("CUENTA_CLABE",					rs_cuenta									); // Cuenta CLABE, CUENTA_CLABE
			registro.put("DESCRIPCION_CECOBAN",			rs_descCec									); // Descripción CECOBAN, DESCRIPCION_CECOBAN
			registro.put("ESTATUS_CECOBAN",				rs_cecoban									); // ESTATUS_CECOBAN
			registros.add(registro);
			
			//if(!rs_if.equals(aux_if)) {
			if(false) { // <--- NOTA: NUNCA SE EJECUTA
				aux_if = rs_if;
			}
			i++;
			
			// Actualizar ID del Grupo Anterior
			groupIdAnterior = groupId;
			
		}//while(rs.next())

		if( i > 0 ) {

			// AGREGAR LOS TOTALES AL SUMMARYDATA
			JSONArray  summaryDataRegisters = new JSONArray();
			JSONObject summaryDataRegister  = null;
						
			summaryDataRegister = new JSONObject();
			summaryDataRegister.put( "NOMBRE_EPO", 					"Totales"												);
			summaryDataRegister.put( "NUMERO_TOTAL_DOCUMENTOS",	String.valueOf(totalDocumentos)					);
			summaryDataRegister.put( "MONTO_TOTAL", 					Comunes.formatoMN(totalMonto.toPlainString())		);
			summaryDataRegister.put( "ESTATUS_CECOBAN", 				"99"														); // PARA QUE LOS TOTALES NO SE PINTEN EN ROJO
			summaryDataRegisters.add( summaryDataRegister );
					
			summaryData.put(groupIdAnterior,summaryDataRegisters);
 
		}// if( i == 0 )
	
		// PONER EL REPORTE EN SESIÓN
		if( i > 0 ){
			session.setAttribute( 	 "lReportes.registros", 	registros	);
			session.setAttribute( 	 "lReportes.summaryData",	summaryData );
		} else {
			session.removeAttribute( "lReportes.registros"	 );
			session.removeAttribute( "lReportes.summaryData" );
		}
		
	} catch(Exception e) {
				
		log.error("ConsultaDetalleMonitoreoINFONAVIT(Exception)");
		log.error("ConsultaDetalleMonitoreoINFONAVIT.claveTipoMonitoreo 	= <" + cmb_tipo_monitor	+ ">");
		log.error("ConsultaDetalleMonitoreoINFONAVIT.claveIF 					= <" + ic_if				+ ">");
		log.error("ConsultaDetalleMonitoreoINFONAVIT.claveEPO 				= <" + ic_epo				+ ">");
		log.error("ConsultaDetalleMonitoreoINFONAVIT.radioHabilitadas 		= <" + rad_hab				+ ">");
		log.error("ConsultaDetalleMonitoreoINFONAVIT.sinCuentaCLABE 		= <" + chk_cuenta			+ ">");
		log.error("ConsultaDetalleMonitoreoINFONAVIT.qrySentencia         = <" + qrySentencia     + ">");
		e.printStackTrace();
				
		throw e;
				
	} finally {
				
		if( rs != null ){ try { rs.close(); } catch(Exception e){} }
		if( ps != null ){ try { ps.close(); } catch(Exception e){} }
				
		if( con.hayConexionAbierta() ){
			con.cierraConexionDB();
		}
				
	}
 
	resultado.put("success",		new Boolean(true)					);
	resultado.put("total",			new Integer(registros.size())	);
	resultado.put("registros",		registros							);
	resultado.put("summaryData",	summaryData							);
			
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("GenerarArchivo") ){
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	String      urlArchivo		= "";
	
	String 		nombreArchivo	= "";
	
	// Leer parametros
	String 		tipo 				= (request.getParameter("tipo") == null)?"":request.getParameter("tipo");
	
	// Preparar clases adicionales
	ConsultaDetalleMonitoreoINFONAVIT consulta = new ConsultaDetalleMonitoreoINFONAVIT();
	
	// Leer datos
	JSONArray 		registros 	= (JSONArray)  session.getAttribute( "lReportes.registros"   );
	JSONObject 		summaryData = (JSONObject) session.getAttribute( "lReportes.summaryData" );
	// HttpSession session
	String			directorioTemporal		= strDirectorioTemp; 
	String 			directorioPublicacion 	= strDirectorioPublicacion;
	
	// Crear archivo
	if(        "PDF".equals(tipo) ){
		nombreArchivo = consulta.generaArchivoPDF( directorioTemporal, directorioPublicacion, registros, summaryData, session );
		urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
	} else if( "CSV".equals(tipo) ){
		nombreArchivo = consulta.generaArchivoCSV( directorioTemporal, registros, summaryData );
		urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
	}
 
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
 
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>