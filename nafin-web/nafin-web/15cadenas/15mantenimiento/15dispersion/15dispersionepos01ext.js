Ext.onReady(function() {
	 
	Ext.namespace('NE.dispersionepos');
	
	//------------------------------ FUNCIONES ESPECIALES -----------------------------
	
	var formateaPorcentajeAvance = function(porcentajeAvance){
		var porcentajeFormateado  = Ext.util.Format.number( Number(porcentajeAvance), "0.00" ) + " %";
		return porcentajeFormateado;
	}
		
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaEnvioFlujoFondos = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						envioFlujoFondos(resp.estadoSiguiente,resp);
					}
				);
			} else {
				envioFlujoFondos(resp.estadoSiguiente,resp);
			}
			
		}else{
 
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Resetear forma
         resetWindowConfirmacionClave();
 
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			/*
 
			// Ocultar mascara del panel de preacuse
			element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			*/
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
 
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var envioFlujoFondos = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"								){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15dispersionepos01ext.data.jsp',
				params: 	{
					informacion:		'EnvioFlujoFondos.inicializacion'
				},
				callback: 				procesaEnvioFlujoFondos
			});
			
		} else if(			estado == "MOSTRAR_PANEL_BUSQUEDA_DISPERSIONES" ){
			
			// Configurar la Fecha Actual
			var fechaVencimiento = Ext.getCmp("fechaVencimiento");
			fechaVencimiento.setValue(respuesta.fechaVencimiento);
			fechaVencimiento.originalValue = respuesta.fechaVencimiento;
			
			// Ocultar Box con animacion de inicializacion
			Ext.getCmp('contenedorInicializacion').hide();
			// Mostrar Panel de B�squeda de Dispersiones
			Ext.getCmp('panelBusquedaDispersiones').show();
			
		} else if(			estado == "BUSCAR_DISPERSIONES_ENLISTADAS"     ){
			
			// Agregar mascara de busqueda
			var panelBusquedaDispersiones = Ext.getCmp("panelBusquedaDispersiones");
			panelBusquedaDispersiones.el.mask("Buscando...","x-mask-loading");
			
			// Realizar b�squeda
			var dispersionesEnlistadasData = Ext.StoreMgr.key('dispersionesEnlistadasDataStore');
			dispersionesEnlistadasData.load({
				params: respuesta
			});
 	
		} else if(        estado == "GUARDAR_OBSERVACIONES" 				  ){
			
			// Agregar mascara de busqueda
			var gridDetalleDispersionesEnlistadas = Ext.getCmp("gridDetalleDispersionesEnlistadas");
			gridDetalleDispersionesEnlistadas.el.mask("Guardando...","x-mask-loading");
			
			// Guardar observaciones
			Ext.Ajax.request({
				url: 					'15dispersionepos01ext.data.jsp',
				params: 	{
					informacion:				  'EnvioFlujoFondos.guardarObservaciones',
					observacionesModificadas: respuesta.observacionesModificadas
				},
				callback: 			procesaEnvioFlujoFondos
			});
			
		} else if(        estado == "EXITO_GUARDAR_OBSERVACIONES"        ){
			
			// Obtener objeto gridDetalleDispersionesEnlistadas
			var grid = Ext.getCmp("gridDetalleDispersionesEnlistadas");
			
			// Si la respuesta es exitosa, copiar el contenido de OBSERVACIONES_OPR a OBSERVACIONES_ORIG_OPR y
			// OBSERVACIONES_ORIG_SNOPR a OBSERVACIONES_SNOPR.
			
			var store = Ext.StoreMgr.key('dispersionesEnlistadasDataStore');
			store.each(
				
				function(record){
 
					// Comparar si las observaciones en la secci�n Operada / Operada Pagada sufrieron cambios
					if( record.data['OBSERVACIONES_OPR']   !== record.data['OBSERVACIONES_ORIG_OPR']   ){
						record.set('OBSERVACIONES_ORIG_OPR',  record.data['OBSERVACIONES_OPR']  );  
						// Simular commit para remover la bandera de modificaci�n
						record.modified['OBSERVACIONES_ORIG_OPR'] = undefined;
						record.modified['OBSERVACIONES_OPR'] 		= undefined;
					}
					
					// Comparar si las observaciones en la secci�n Vencido sin Operar / Pagado sin Operar sufri� cambios
					if( record.data['OBSERVACIONES_SNOPR'] !== record.data['OBSERVACIONES_ORIG_SNOPR'] ){
						record.set('OBSERVACIONES_ORIG_SNOPR',record.data['OBSERVACIONES_SNOPR']); 
						// Simular commit para remover la bandera de modificaci�n
						record.modified['OBSERVACIONES_ORIG_SNOPR'] = undefined;
						record.modified['OBSERVACIONES_SNOPR'] 	  = undefined;
					}
					
				}
				
			);
 
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
			if( grid.getEl().isMasked()){
				grid.getEl().unmask();
			}
			
			// Volver a pintar grid
			grid.getView().refresh();
 
		} else if(			estado == "CARGAR_CIFRAS_CONTROL"           ){
				
			// 1. Mostrar animacion de enviando registros...
			var gridDetalleDispersionesEnlistadas = Ext.getCmp("gridDetalleDispersionesEnlistadas");
			gridDetalleDispersionesEnlistadas.el.mask("Generando Cifras de Control...","x-mask-loading");
		
			// 2. Crear array de datos, con aquellos registros que fueron seleccionados:
			var dispersionesEnlistadasData 		  = Ext.StoreMgr.key('dispersionesEnlistadasDataStore');
			var registrosSeleccionados = new Array();
			dispersionesEnlistadasData.each(
				
				function(record){
							
					// Debe haber un grupo SELECCIONADO, con estatus NORMAL y debe estar sin MENSAJE.
					var recordDataValue  		= record.data['ENVIO_FFON_OPR'];
					var recordDataStatus 		= record.data['ENVIO_FFON_OPR_STATUS'];
					
					var grupoOprSeleccionado 	= false;
					if( recordDataValue === true &&  recordDataStatus.match(/^NORMAL[;]?$/) != null  ){
						grupoOprSeleccionado 	= true;
					}
					
					// Debe haber un grupo SELECCIONADO, con estatus NORMAL y debe estar sin MENSAJE.
					recordDataValue  				= record.data['ENVIO_FFON_SNOPR'];
					recordDataStatus 				= record.data['ENVIO_FFON_SNOPR_STATUS'];
					
					var grupoSnOprSeleccionado = false;
					if( recordDataValue === true &&  recordDataStatus.match(/^NORMAL[;]?$/) != null  ){
						grupoSnOprSeleccionado  = true;
					} 
					
					// En caso que alguno de los grupos de documentos haya sido seleccionado, agregar registro
					// a la lista
					
					if( grupoOprSeleccionado || grupoSnOprSeleccionado ){
 
						var registroSeleccionado = new Object();
						registroSeleccionado['IC_EPO']							= record.data['IC_EPO'];
						registroSeleccionado['IC_MONEDA']						= record.data['IC_MONEDA'];
						registroSeleccionado['NOMBRE_EPO']						= record.data['NOMBRE_EPO'];
						registroSeleccionado['FECHA_VENCIMIENTO']				= Ext.util.Format.date(record.data['FECHA_VENCIMIENTO'],'d/m/Y'); 
						registroSeleccionado['NOMBRE_MONEDA']					= record.data['NOMBRE_MONEDA'];
						
						if( grupoOprSeleccionado ){
							registroSeleccionado['TOTAL_DOCUMENTOS_OPR']		= record.data['TOTAL_DOCUMENTOS_OPR'];
							registroSeleccionado['MONTO_OPR']					= record.data['MONTO_OPR'];
						}else{
							registroSeleccionado['TOTAL_DOCUMENTOS_OPR']		= "0";
							registroSeleccionado['MONTO_OPR']					= "0";
						}
						
						if( grupoSnOprSeleccionado ){
							registroSeleccionado['TOTAL_DOCUMENTOS_SNOPR']	= record.data['TOTAL_DOCUMENTOS_SNOPR'];
							registroSeleccionado['MONTO_SNOPR']					= record.data['MONTO_SNOPR'];
						}else{
							registroSeleccionado['TOTAL_DOCUMENTOS_SNOPR']	= "0";
							registroSeleccionado['MONTO_SNOPR']					= "0";
						}
						
						registrosSeleccionados.push(registroSeleccionado);
					}
 
				}
				
			);
			
			// Cargar registros seleccionados en el panel de cifras de control
			var cifrasControlData = Ext.StoreMgr.key('cifrasControlDataStore');
			cifrasControlData.loadData(
			   {                                            
				    success:    true,                          
				    total:      registrosSeleccionados.length,       
				    registros:  registrosSeleccionados  
				 }                                            
			);
						
			// Suprimir animacion del panel anterior, ocultarlo junto con la forma de busqueda
			gridDetalleDispersionesEnlistadas.el.unmask();
			
			// Ocultar panel de B�squeda y Tablero de Dispersion
			Ext.getCmp('panelBusquedaDispersiones').hide();
			gridDetalleDispersionesEnlistadas.hide();
			
			// Mostrar panel de cifras de control
			Ext.getCmp('gridCifrasControl').show();
		
		} else if(			estado == "CANCELAR_ENVIO_FLUJO_FONDOS"     ){
			
			// Ocultar panel de cifras de control
			Ext.getCmp('gridCifrasControl').hide();
			
			// Remover contenido del panel
			Ext.StoreMgr.key('cifrasControlDataStore').removeAll();
			
			// Mostrar panel de B�squeda y Tablero de Dispersion
			Ext.getCmp('panelBusquedaDispersiones').show();
			Ext.getCmp('gridDetalleDispersionesEnlistadas').show();
 
		} else if(		   estado == "MOSTRAR_FORMA_VALIDAR_USUARIO"   ){
			
			resetWindowConfirmacionClave();
			showWindowConfirmacionClave();
			
		} else if(			estado == "VALIDAR_USUARIO"  					  ){ 
 
			Ext.Ajax.request({
				url: 		'15dispersionepos01ext.data.jsp',
				params: 	Ext.apply( 
					{
						informacion: 'EnvioFlujoFondos.validarUsuario'
					},
					respuesta
				),
				callback: 				procesaEnvioFlujoFondos
			});
					
			hideWindowConfirmacionClave();
 
		} else if(			estado == "FINALIZAR_ENVIO_FFON"					  ){
			
			// Poner animacion
			var gridCifrasControl = Ext.getCmp("gridCifrasControl");
			gridCifrasControl.el.mask("Enviando...","x-mask-loading");
			
			// Se env�an todos los registros seleccionados para su dispersion + contrasena.
			var contrasena = Ext.getCmp("contrasena").getValue();
			resetWindowConfirmacionClave();
			
			// Se enviar� el record id para facilitar el despliegue de resultados, se enviaran con el siguiente formato:
			var dispersionesEnlistadasData	= Ext.StoreMgr.key('dispersionesEnlistadasDataStore');
			var registrosSeleccionados 		= new Array();
			dispersionesEnlistadasData.each(
				
				function(record){
							
					// Debe haber un grupo SELECCIONADO, con estatus NORMAL y debe estar sin MENSAJE.
					var recordDataValue  		= record.data['ENVIO_FFON_OPR'];
					var recordDataStatus 		= record.data['ENVIO_FFON_OPR_STATUS'];
					
					var grupoOprSeleccionado 	= false;
					if( recordDataValue === true &&  recordDataStatus.match(/^NORMAL[;]?$/) != null  ){
						grupoOprSeleccionado 	= true;
					}
					
					// Debe haber un grupo SELECCIONADO, con estatus NORMAL y debe estar sin MENSAJE.
					recordDataValue  				= record.data['ENVIO_FFON_SNOPR'];
					recordDataStatus 				= record.data['ENVIO_FFON_SNOPR_STATUS'];
					
					var grupoSnOprSeleccionado = false;
					if( recordDataValue === true &&  recordDataStatus.match(/^NORMAL[;]?$/) != null  ){
						grupoSnOprSeleccionado  = true;
					} 
					
					// En caso que alguno de los grupos de documentos haya sido seleccionado, agregar registro
					// a la lista
					
					if( grupoOprSeleccionado ){
 
						var registroSeleccionado 							= new Object();
						registroSeleccionado['RECORD_ID']				= record.id;
						registroSeleccionado['IC_EPO']					= record.data['IC_EPO'];
						registroSeleccionado['IC_MONEDA']				= record.data['IC_MONEDA'];
						registroSeleccionado['DC_VENCIMIENTO']			= Ext.util.Format.date(record.data['FECHA_VENCIMIENTO'],'d/m/Y'); 
						registroSeleccionado['TIPO_DOCUMENTO']			= 'OPR';
						registroSeleccionado['TOTAL_DOCUMENTOS']		= String(record.data['TOTAL_DOCUMENTOS_OPR']).replace(/[,]/g,'');
						registroSeleccionado['MONTO']						= String(record.data['MONTO_OPR']).replace(/[\$,]/g,'');
												
						registrosSeleccionados.push(registroSeleccionado);
						
					}
					
					if( grupoSnOprSeleccionado ){
 
						var registroSeleccionado 								= new Object();
						registroSeleccionado['RECORD_ID']					= record.id;
						registroSeleccionado['IC_EPO']						= record.data['IC_EPO'];
						registroSeleccionado['IC_MONEDA']					= record.data['IC_MONEDA'];
						registroSeleccionado['DC_VENCIMIENTO']				= Ext.util.Format.date(record.data['FECHA_VENCIMIENTO'],'d/m/Y'); 
						registroSeleccionado['TIPO_DOCUMENTO']				= 'SNOPR';
						registroSeleccionado['TOTAL_DOCUMENTOS']			= String(record.data['TOTAL_DOCUMENTOS_SNOPR']).replace(/[,]/g,'');
						registroSeleccionado['MONTO']							= String(record.data['MONTO_SNOPR']).replace(/[\$,]/g,'');
						
						registrosSeleccionados.push(registroSeleccionado);
						
					}
 
				}
				
			);
			
			// Realizar env�o
			Ext.Ajax.request({
				url: 		'15dispersionepos01ext.data.jsp',
				params: 	{
					informacion:				'EnvioFlujoFondos.finalizarEnvioFFON',
					contrasena:	 				contrasena,
					totalRegistros:			registrosSeleccionados.length,
					registrosSeleccionados:	Ext.encode(registrosSeleccionados)
				},
				callback: 				procesaEnvioFlujoFondos
			});
			
		} else if( estado == "MUESTRA_PANTALLA_AVANCE"		) {
 
			// Ocultar animacion de env�o
			var gridCifrasControl = Ext.getCmp("gridCifrasControl");
			gridCifrasControl.el.unmask();
			
			// Mostrar panel de porcentaje de avance
			showWindowAvanceEnvioFFON( respuesta.numeroTotalEnviosFFON );
 
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 		'15dispersionepos01ext.data.jsp',
				params: 	{
					informacion: 				'EnvioFlujoFondos.muestraPantallaAvance',
					numeroTotalEnviosFFON:	respuesta.numeroTotalEnviosFFON
				},
				callback: 		 procesaEnvioFlujoFondos
			});
				
		} else if( estado == "ACTUALIZAR_AVANCE"						) {
 
			// Actualizar porcensaje de avance
			var avance								= respuesta.avance;
			var textoPorcentajeAvance 			= formateaPorcentajeAvance(respuesta.porcentajeAvance);
			var numeroEnviosFFONRealizados	= respuesta.numeroEnviosFFONRealizados;
			
 			var barrarAvanceEnvioFFON			= Ext.getCmp("barrarAvanceEnvioFFON");
			var valorEnviosFFONRealizados		= Ext.getCmp("valorEnviosFFONRealizados");
 
			barrarAvanceEnvioFFON.updateProgress( Number(avance), textoPorcentajeAvance, true);
			valorEnviosFFONRealizados.setText(numeroEnviosFFONRealizados);
			
			// En 2 segundos volver a consultar el porcentaje de avance
			var actualizarAvanceTask = new Ext.util.DelayedTask(
				function(){
					Ext.Ajax.request({
						url: 		'15dispersionepos01ext.data.jsp',
						params: 	{
							informacion: 		'EnvioFlujoFondos.actualizarAvance',
							envioFinalizado: 	respuesta.envioFinalizado 
						},
						callback: 		 procesaEnvioFlujoFondos 
					});
				},
				this
			);
			actualizarAvanceTask.delay(2000);
			
		} else if(			estado == "MOSTRAR_ENVIO_FFON_COMPLETADO"						  ){
			
			var gridDetalleDispersionesEnlistadas 	= Ext.getCmp('gridDetalleDispersionesEnlistadas');
			var dispersionesEnlistadasData			= Ext.StoreMgr.key('dispersionesEnlistadasDataStore');
			var setMessage   								= (new Ext.ux.grid.MsgCheckColumn()).setMessage;
			
			// Actualizar contenido de los campos del grid
			// utilizar record.set, ya que si no, no podr�an copiarse los cambios del data al json.
			var resultados = respuesta.resultados;
			for(var i=0;i<resultados.length;i++){
				
				var resultado 		= resultados[i];
				var record			= dispersionesEnlistadasData.getById(resultado.recordId);
				
				/*
					
					La respuesta, deber� regresar un array con los registros siguiente:
					
					resultado.recordId
					resultado.tipoDocumento
					
					resultado.success
					resultado.fechaEnvioFlujoFondos
					
					resultado.hayMensaje
					resultado.iconoMensaje
					resultado.textoMensaje
					
				*/
				
				// Actualizar estatus del registro
				if(        resultado.success === true &&  resultado.tipoDocumento === 'OPR'    ) {
					
					var fechaEnvioFlujoFondos = Ext.isEmpty(resultado.fechaEnvioFlujoFondos)?null:Date.parseDate(resultado.fechaEnvioFlujoFondos, 'd/m/Y');
					
					// Actualizar cambios
					record.set('ENVIO_FFON_OPR',  				 	true);
					record.set('ENVIO_FFON_OPR_STATUS',			 	'READONLY');
					record.set('FECHA_ENVIO_FLUJO_FONDOS_OPR', 	fechaEnvioFlujoFondos );
					record.set('VER_DETALLE_DISPERSION',			true);
					
					// Suprimir flag de registro modificado
					record.modified['ENVIO_FFON_OPR'] 						= undefined;
					record.modified['ENVIO_FFON_OPR_STATUS'] 				= undefined;
					record.modified['FECHA_ENVIO_FLUJO_FONDOS_OPR'] 	= undefined;
					record.modified['VER_DETALLE_DISPERSION'] 			= undefined;
					
				} else if( resultado.success === true &&  resultado.tipoDocumento === 'SNOPR' ) {
					
					var fechaEnvioFlujoFondos = Ext.isEmpty(resultado.fechaEnvioFlujoFondos)?null:Date.parseDate(resultado.fechaEnvioFlujoFondos, 'd/m/Y');
					
					// Actualizar cambios
					record.set('ENVIO_FFON_SNOPR',  				 	true);
					record.set('ENVIO_FFON_SNOPR_STATUS',		 	'READONLY');
					record.set('FECHA_ENVIO_FLUJO_FONDOS_SNOPR', fechaEnvioFlujoFondos );
					record.set('VER_DETALLE_DISPERSION',			true);
					
					// Suprimir flag de registro modificado
					record.modified['ENVIO_FFON_SNOPR'] 					= undefined;
					record.modified['ENVIO_FFON_SNOPR_STATUS'] 			= undefined;
					record.modified['FECHA_ENVIO_FLUJO_FONDOS_SNOPR']  = undefined;
					record.modified['VER_DETALLE_DISPERSION'] 			= undefined;
					
				}
				
				// Mostrar mensaje, en caso de haya alguno
				if( 		  resultado.hayMensaje === true && resultado.tipoDocumento === 'OPR'   ) {
					setMessage.call( undefined, record, 'ENVIO_FFON_OPR_STATUS',	resultado.iconoMensaje, resultado.textoMensaje );
				} else if( resultado.hayMensaje === true && resultado.tipoDocumento === 'SNOPR' ) {
					setMessage.call( undefined, record, 'ENVIO_FFON_SNOPR_STATUS', resultado.iconoMensaje, resultado.textoMensaje );
				}
								
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Ocultar grid de cifras de control
         Ext.getCmp('gridCifrasControl').hide();
         
         // Mostrar panel de Busqueda
         Ext.getCmp('panelBusquedaDispersiones').show();
         
         // Mostrar grid de resultados
         gridDetalleDispersionesEnlistadas.show();
         
         // Refrescar contenido del grid
         gridDetalleDispersionesEnlistadas.getView().refresh();
 	
		} else if(			estado == "ESPERAR_DECISION"  					  ){ 
			
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
			
         // Resetear forma
         resetWindowConfirmacionClave();
         
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
		} else if(			estado == "FIN"  					                 ){
 	
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "15dispersionepos01ext.jsp";
			forma.target	= "_self";
			forma.submit();
			
		}
		
	}
	
	//-------------------- 7. FORMA VER DETALLE DISPERSION -----------------
	 
	var procesaRegresarDeDetalleDispersionesEPO = function(boton,evento){
		
		// Ocultar ventana de detalle
		hideWindowDetalleDispersionesEPO();
		 
	}
	
	var procesaConsultaTotalesDetalleDispersionesEPO = function(opts, success, response) {

		var totalOperaciones 		= Ext.getCmp('totalesDetalleDispersionesEPO.TOTAL_OPERACIONES');
		var importeTotalDescuento 	= Ext.getCmp('totalesDetalleDispersionesEPO.IMPORTE_TOTAL_DESCUENTO');
			
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			var resp = Ext.util.JSON.decode(response.responseText);
			totalOperaciones.setValue(resp.totalOperaciones);
			importeTotalDescuento.setValue(resp.importeTotalDescuento);
			
		}else{
			
			totalOperaciones.setValue("0");
			importeTotalDescuento.setValue("$0.00");
			
		}
		
	}
	
	var procesarConsultaDetalleDispersionesEPO = function(store, registros, opts){

		var gridDetalleDispersionesEPO = Ext.getCmp('gridDetalleDispersionesEPO');
		var el 								 = gridDetalleDispersionesEPO.getGridEl();
		el.unmask();
		
		if (registros != null) {
			
			/*
			if (!gridDetalleDispersionesEPO.isVisible()) {
				gridDetalleDispersionesEPO.show();
			}		
			*/
			
			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
 
			var esNuevaConsulta = "NuevaConsulta" === opts.params.operacion?true:false;
 
			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
			
			// Cargar contenido de los totales
			if( esNuevaConsulta ){
				Ext.Ajax.request({
					url: 		'15dispersionepos01ext.data.jsp',
					params: 	{
						informacion: 		'ConsultaTotalesDetalleDispersionesEPO'
					},
					callback: 		 procesaConsultaTotalesDetalleDispersionesEPO 
				});
			}
			
		}
 
	}
	
	var botonesDetalleDispersionesEPO = {
		xtype: 	'container',
		hidden: 	false,
		height:	25,
		flex:	  	0,
		layout:	'hbox',
		layoutConfig: {
       	pack:		'end',
      	align:	'middle'
      },
      defaults:{
      	margins:'0 0 0 0'
      },
      items:[
      	{
				xtype: 		'button',
				text: 		'Regresar',
				iconCls: 	'icoRegresar',
				id: 			'botonRegresarDeDetalleDispersionesEPO',
				handler:    procesaRegresarDeDetalleDispersionesEPO
			}
      ]
	};
	
	var totalesDetalleDispersionesEPO = new Ext.Container({
		xtype: 	'container',
		hidden: 	false,
		height:	28,
		flex:	  	0,
		layout:	'hbox',
		layoutConfig: {
      	padding:	'5',
       	pack:		'start',
      	align:	'middle'
      },
      defaults:{
      	margins:'0 5 0 0'
      },
      items:[
      	{
         	xtype: 		'label',
         	text:  		'Total de Operaciones:'
         },
         {
         	xtype:		'textfield',
         	id:			'totalesDetalleDispersionesEPO.TOTAL_OPERACIONES',
         	readOnly: 	true,
         	style:		'text-align:right'
         },
         {
         	xtype:		'label',
         	text: 		'Importe Total Descuento:'
         },
         {
         	xtype:		'textfield',
            id:			'totalesDetalleDispersionesEPO.IMPORTE_TOTAL_DESCUENTO',
            readOnly: 	true,
            width:		174,
            style:		'text-align:right'
      	}
      ]
	});
 
	var detalleDispersionesEPOData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'detalleDispersionesEPODataStore',
		url: 		'15dispersionepos01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetalleDispersionesEPO'
		},
		idIndex: 		1,
		fields: [			
			{ name: 'NOMBRE_IF'						},
			{ name: 'NOMBRE_PYME'					},
			{ name: 'RFC_PYME'						},
			{ name: 'CLAVE_BANCO'					},
			{ name: 'TIPO_CUENTA'					},
			{ name: 'CUENTA_CLABE_SWIFT'			},
			{ name: 'IMPORTE_TOTAL_DESCUENTO', 	type: 'float', convert: function(value, record){ return Number(value.replace(/[\$, ]/g,"")); } },
			{ name: 'ESTATUS_FFON'					},
			{ name: 'VIA_LIQUIDACION'				},
			{ name: 'FOLIO'							},
			{ name: 'FECHA_REGISTRO',				type: 'date',  convert: function(value, record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
					showWindowDetalleDispersionesEPO();
				}
			},
			load: 	procesarConsultaDetalleDispersionesEPO,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDetalleDispersionesEPO(null, null, null);						
				}
			}
		}
		
	});
	
	var wordWrapRenderer02 = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;" ';
		return value;
			
	}
	
	var nombreIfRenderer02 					= wordWrapRenderer02;
	
	var nombrePymeRenderer02 				= wordWrapRenderer02;
	
	var nombreRfcPymeRenderer02 			= wordWrapRenderer02;
	
	var claveBancoRenderer02 				= wordWrapRenderer02;
	
	var tipoCuentaRenderer02 				= wordWrapRenderer02;
	
	var cuentaClabeSwiftRenderer02 		= wordWrapRenderer02;
 
	var importeTotalDescuentoRenderer02 = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align:right;" ';
		value 		  = record.json['IMPORTE_TOTAL_DESCUENTO'];
		return value;
			
	}
	
	var estatusFfonRenderer02				= wordWrapRenderer02;
	
	var viaLiquidacionRenderer02			= wordWrapRenderer02;
	
	var folioRenderer02						= wordWrapRenderer02;

	var gridDetalleDispersionesEPO = new Ext.grid.GridPanel({
		store: 	detalleDispersionesEPOData,
		id:		'gridDetalleDispersionesEPO',
		hidden: 	false,
		margins: '20 0 0 0',
		columns: [
			{
				header: 		'IF<br>&nbsp;',
				tooltip: 	'Nombre del Intermediario Financiero',
				dataIndex: 	'NOMBRE_IF',
				sortable: 	true,
				resizable: 	true,
				width: 		140,
				hidden: 		false,
				align:		'center',
				renderer:	nombreIfRenderer02
			},
			{
				header: 		'PYME<br>&nbsp;',
				tooltip: 	'Nombre del Proveedor',
				dataIndex: 	'NOMBRE_PYME',
				sortable: 	true,
				resizable: 	true,
				width: 		140,
				hidden: 		false,
				align:		'center',
				renderer:	nombrePymeRenderer02
			},
			{
				header: 		'RFC<br>&nbsp;',
				tooltip: 	'RFC del Proveedor',
				dataIndex: 	'RFC_PYME',
				sortable: 	true,
				resizable: 	true,
				width: 		90,
				hidden: 		false,
				align:		'center',
				renderer:	nombreRfcPymeRenderer02
			},
			{
				header: 		'Banco<br>&nbsp;',
				tooltip: 	'Clave del Banco',
				dataIndex: 	'CLAVE_BANCO',
				sortable: 	true,
				resizable: 	true,
				width: 		60,
				hidden: 		false,
				align:		'center',
				renderer:	claveBancoRenderer02
			},
			{
				header: 		'Tipo&nbsp;de<br>Cuenta',
				tooltip: 	'Tipo de Cuenta',
				dataIndex: 	'TIPO_CUENTA',
				sortable: 	true,
				resizable: 	true,
				width: 		74,
				hidden: 		false,
				align:		'center',
				renderer:	tipoCuentaRenderer02
			},
			{
				header: 		'Cuenta&nbsp;CLABE&nbsp;/&nbsp;SWIFT',
				tooltip: 	'Cuenta CLABE / SWIFT',
				dataIndex: 	'CUENTA_CLABE_SWIFT',
				sortable: 	true,
				resizable: 	true,
				width: 		140,
				hidden: 		false,
				align:		'center',
				renderer:	cuentaClabeSwiftRenderer02
			},
			{
				header: 		'Importe&nbsp;Total<br>Descuento',
				tooltip: 	'Importe Total Descuento',
				dataIndex: 	'IMPORTE_TOTAL_DESCUENTO',
				sortable: 	true,
				resizable: 	true,
				width: 		140,
				hidden: 		false,
				align:		'center',
				renderer:	importeTotalDescuentoRenderer02
			},
			{
				header: 		'Estatus&nbsp;FFON<br>&nbsp;',
				tooltip: 	'Estatus FFON',
				dataIndex: 	'ESTATUS_FFON',
				sortable: 	true,
				resizable: 	true,
				width: 		160,
				hidden: 		false,
				align:		'center',
				renderer:	estatusFfonRenderer02
			},
			{
				header: 		'V&iacute;a&nbsp;de<br>Liquidaci&oacute;n',
				tooltip: 	'V�a de Liquidaci�n',
				dataIndex: 	'VIA_LIQUIDACION',
				sortable: 	true,
				resizable: 	true,
				width: 		68,
				hidden: 		false,
				align:		'center',
				renderer:	viaLiquidacionRenderer02
			},
			{
				header: 		'Folio<br>&nbsp;',
				tooltip: 	'FOLIO',
				dataIndex: 	'FOLIO',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				align:		'center',
				renderer:	folioRenderer02
			},
			{
				header: 		'Fecha&nbsp;de<br>Registro',
				tooltip: 	'Fecha de Registro',
				dataIndex: 	'FECHA_REGISTRO',
				sortable: 	true,
				resizable: 	true,
				width: 		80,
				hidden: 		false,
				align:		'center',
				xtype: 		'datecolumn', 
				format: 		'd/m/Y'
			}
		],
		stripeRows: true,
		loadMask: 	true,
		height: 		256,		
		frame: 		false,
		flex:			10,
		bbar: {
			xtype: 			'paging',
			pageSize: 		15,
			buttonAlign: 	'left',
			id: 				'barraPaginacion',
			opts:				null,
			displayInfo: 	true,
			store: 			detalleDispersionesEPOData,
			displayMsg: 	'{0} - {1} de {2}',
			emptyMsg: 		"No hay registros."
		}
	});
	
	var cabeceraDetalleDispersionesEPO = new Ext.Container({
		//xtype:  'container',
		layout: 'form',
		hidden: false,
		flex:   0,
		labelWidth:	140,
		items:[
			{
				xtype: 		'displayfield',
				id:	 		'cabeceraDetalleDispersionesEPO.NOMBRE_EPO',
				fieldLabel: 'EPO'
			},
			{
				xtype: 		'displayfield',
				id:	 		'cabeceraDetalleDispersionesEPO.FECHA_VENCIMIENTO',
				fieldLabel: 'Fecha de Vencimiento'
			},
			{
				xtype: 		'displayfield',
				id:	 		'cabeceraDetalleDispersionesEPO.NOMBRE_MONEDA',
				fieldLabel: 'Moneda'
			}
		]
	});
	
	var contenedorDetalleDispersionesEPO = {
		xtype: 	'container',
		id:	 	'contenedorDetalleDispersionesEPO',
		frame:	false,
		border: 	false,
		baseCls:	'x-plain',
		title: 	'Dispersiones',
		height: 420,
		layout: {
			type:		'vbox',
			padding:	'5',
			align:	'stretch'
		},
		defaults:{
			margins:'0 0 0 0'
		},
		items: [ 
			cabeceraDetalleDispersionesEPO,
			gridDetalleDispersionesEPO,
			totalesDetalleDispersionesEPO,
			botonesDetalleDispersionesEPO
		]
	};

	var hideWindowDetalleDispersionesEPO = function(){
 
		var ventana = Ext.getCmp('windowDetalleDispersionesEPO');
		if(ventana && ventana.isVisible() ){
			// Ocultar ventana
			ventana.hide();
			// Remover detalle de la cabecera
			Ext.getCmp('cabeceraDetalleDispersionesEPO.NOMBRE_EPO').setValue('');
			Ext.getCmp('cabeceraDetalleDispersionesEPO.FECHA_VENCIMIENTO').setValue('');
			Ext.getCmp('cabeceraDetalleDispersionesEPO.NOMBRE_MONEDA').setValue('');
			// Remover contenido del grid
			var detalleDispersionesEPOData = Ext.StoreMgr.key('detalleDispersionesEPODataStore');
			detalleDispersionesEPOData.removeAll();
			// Remover detalle de los totales
			Ext.getCmp('totalesDetalleDispersionesEPO.TOTAL_OPERACIONES').setValue('');
			Ext.getCmp('totalesDetalleDispersionesEPO.IMPORTE_TOTAL_DESCUENTO').setValue('');
      }
      
	}
	
	var showWindowDetalleDispersionesEPO = function(numeroRegistros){
 
		var ventana = Ext.getCmp('windowDetalleDispersionesEPO');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Dispersiones',
				layout: 			'fit',
				width: 			900,
				height: 			430,
				minWidth: 		900,
				minHeight: 		430,
				resizable:		false,
				id: 				'windowDetalleDispersionesEPO',
				modal:			true,
				closable: 		false,
				items: [
					contenedorDetalleDispersionesEPO
				]
			}).show();
			
		}
		
	}	
	
	
	// ---------------------------- 6. PANEL AVANCE VALIDACION ------------------------------
	
	var elementosAvanceEnvioFFON = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
			text: 	'Enviando...'
		},
		{
			xtype: 'progress',
			id:	 'barrarAvanceEnvioFFON',
			name:	 'barrarAvanceEnvioFFON',
			cls:	 'center-align',
			value: 0.3756,
			text:  '37.56 %'
		},
		{
			xtype: 'panel',
			layout: 'hbox',
			border:			false,
			baseCls:			'x-plain',
			layoutConfig: {
				padding:    '5 0 0 0',
				pack:			'start',
				align:		'middle'
			},
			defaults:{
				margins:'0 5 0 0'
			},
			items: [
				{
					xtype: 	'label',
					width: 	'130',
					cls: 		'x-form-item',
					text: 	'Registros Procesados:'
				},
				{
					xtype: 	'label',
					id:		'valorEnviosFFONRealizados',
					name:		'valorEnviosFFONRealizados',
					cls: 		'x-form-item',
					style:	{
						textAlign: 'right'
					}
				}
			]
		},
		{
			xtype: 'panel',
			layout: 'hbox',
			border:			false,
			baseCls:			'x-plain',
			layoutConfig: {
				padding:    '0 0 0 0',
				pack:			'start',
				align:		'middle'
			},
			defaults:{
				margins:'0 5 0 0'
			},
			items: [
				{
					xtype: 	'label',
					width: 	'130',
					cls: 		'x-form-item',
					text: 	'Total de Registros:'
				},
				{
					xtype: 	'label',
					id:		'valorTotalRegistros',
					name:		'valorTotalRegistros',
					cls: 		'x-form-item'
				}
			]
		}
	];
	
	var panelAvanceEnvioFFON = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelAvanceEnvioFFON',
		frame: 			false,
		border:			false,
		collapsible: 	false,
		titleCollapse: false,
		bodyStyle:		'padding: 20px',
		baseCls:			'x-plain',
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		items: 			elementosAvanceEnvioFFON
	});
	
	var hideWindowAvanceEnvioFFON = function(){
		
		var ventanaAvanceEnvioFFON = Ext.getCmp('windowAvanceEnvioFFON');
		
		if(ventanaAvanceEnvioFFON && ventanaAvanceEnvioFFON.isVisible() ){
			var barrarAvanceEnvioFFON	 = Ext.getCmp("barrarAvanceEnvioFFON");
			barrarAvanceEnvioFFON.reset(); 
 
			if (ventanaAvanceEnvioFFON) {
				ventanaAvanceEnvioFFON.hide();
			}
      }
      
	}
	
	var showWindowAvanceEnvioFFON = function(numeroRegistros){
		
		var barrarAvanceEnvioFFON		= Ext.getCmp("barrarAvanceEnvioFFON");
		var valorEnviosFFONRealizados	= Ext.getCmp("valorEnviosFFONRealizados");
		var valorTotalRegistros			= Ext.getCmp("valorTotalRegistros");
 
		barrarAvanceEnvioFFON.updateProgress(0.00,"0.00 %",true);
		valorTotalRegistros.setText(numeroRegistros); 
		valorEnviosFFONRealizados.setText("0");
			
		var ventana = Ext.getCmp('windowAvanceEnvioFFON');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				resizable:		false,
				id: 				'windowAvanceEnvioFFON',
				modal:			true,
				closable: 		false,
				listeners: 		{
					show: function(component){
						var valorEnviosFFONRealizados	= Ext.getCmp("valorEnviosFFONRealizados");
						var valorTotalRegistros			= Ext.getCmp("valorTotalRegistros");
						valorEnviosFFONRealizados.setWidth(valorTotalRegistros.getWidth());
					}
				},
				items: [
					Ext.getCmp("panelAvanceEnvioFFON")
				]
			}).show();
			
		}
		
	}	
 
	//----------------------- 5. FORMA CONFIRMAR CONTRASE�A -----------------

	var resetWindowConfirmacionClave = function(){
		var panelConfirmacionClave = Ext.getCmp('panelConfirmacionClave');
		panelConfirmacionClave.getForm().reset();
	}
	
	var elementosFormaContrasena = [
		// INPUT TEXT CLAVE USUARIO
		{
			xtype: 				'textfield',
			name: 				'claveUsuario',
			id: 					'claveUsuario',
			fieldLabel: 		'Clave de Usuario',
			allowBlank: 		true,
			maxLength: 			8,	
			width: 				80,
			msgTarget: 			'side',
			//anchor:			'95%',
			margins: 			'0 20 0 0'  
		},
		// INPUT TEXT CONTRASE�A
		{
			xtype: 				'textfield',
			name: 				'contrasena',
			id: 					'contrasena',
			fieldLabel: 		'Contrase�a',
			inputType: 			'password',
			allowBlank: 		true,
			//maxLength: 		8,	
			width: 				80,
			msgTarget: 			'side',
			//anchor:			'95%',
			margins: 			'0 20 0 0'  
		},	
		// INPUT HIDDEN INDICE REGISTRO - ROW INDEX DE LA CUENTA SELECCIONADA
		{  
        xtype:	'hidden',  
        name:	'indiceRegistro',
        id: 	'indiceRegistro'
      }
			
	];
	
	var panelConfirmacionClave = new Ext.form.FormPanel({
		id: 					'panelConfirmacionClave',
		width: 				274,
		frame: 				false,
		border:				false,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 16px',
		baseCls:				'x-plain',
		labelWidth: 		100,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		items: 				elementosFormaContrasena,
		monitorValid: 		false,
		buttons: [
			{
				xtype: 		'button',
				width: 		80,
				height:		10,
				text: 		'Aceptar',
				iconCls: 	'icoAceptar',
				id: 			'botonAceptarContrasena',
				style: 		'float:right',
				handler:    function(boton, evento) {
					
					var claveUsuario = Ext.getCmp("claveUsuario").getValue();
					if(Ext.isEmpty(claveUsuario)){
						Ext.getCmp("claveUsuario").markInvalid("Este campo es requerido");
						return;
					}
					
					var contrasena	  = Ext.getCmp("contrasena").getValue();
					if(Ext.isEmpty(contrasena)){
						Ext.getCmp("contrasena").markInvalid("Este campo es requerido");
						return;
					}
					
					var respuesta = new Object();
					respuesta	  = Ext.apply( respuesta, Ext.getCmp('panelConfirmacionClave').getForm().getValues() );
					envioFlujoFondos("VALIDAR_USUARIO",respuesta);
 
				}
			},
			{
				xtype: 		'button',
				width: 		80,
				height:		10,
				text: 		'Cancelar',
				iconCls: 	'icoLimpiar',
				id: 			'botonCancelar',
				style: 		'float:right',
				handler:    function(boton, evento) {
					var ventana = Ext.getCmp('windowConfirmacionClave');
					resetWindowConfirmacionClave();
					ventana.hide();	
				}
			}
		]
	});

	var hideWindowConfirmacionClave = function(){
		
		//resetWindowConfirmacionClave();
		
		var ventanaConfirmacionClave = Ext.getCmp('windowConfirmacionClave');
		if(ventanaConfirmacionClave && ventanaConfirmacionClave.isVisible() ){
			ventanaConfirmacionClave.hide();
      }
      
	}
	
	var showWindowConfirmacionClave = function(){
		
		var ventana = Ext.getCmp('windowConfirmacionClave');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				layout: 			'fit',
				width: 			280,
				height: 			164,
				minWidth: 		280,
				minHeight: 		164,
				modal:			true,
				title: 			'Confirmaci�n de Clave',
				id: 				'windowConfirmacionClave',
				closeAction: 	'hide',
				items: [
					Ext.getCmp("panelConfirmacionClave")
				]
				/*,listeners: 		{
					hide: resetWindowConfirmacionClave
				}*/
			}).show();
		}
		
	}
							
	//--------------------------------- 4. GRID DE CIFRAS DE CONTROL --------------------------------
	
	var procesaCompletarEnvioFFON = function(boton, evento) {
		envioFlujoFondos("MOSTRAR_FORMA_VALIDAR_USUARIO",null);
	}
	
	var procesaCancelarEnvioFFON = function(boton, evento) {
		envioFlujoFondos("CANCELAR_ENVIO_FLUJO_FONDOS",null);
	}
	
	var procesarConsultaCifrasControl = function(store, registros, opts){
 
		var gridCifrasControl  = Ext.getCmp('gridCifrasControl');
 
		if (registros != null) {
			
			if (!gridCifrasControl.isVisible()) {
				gridCifrasControl.show();
			}			

			var el = gridCifrasControl.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
 
		}
		
	}
	
	var cifrasControlData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'cifrasControlDataStore',
		//url: 		'15dispersionepos01ext.data.jsp',
		//baseParams: {
		//	informacion: ''
		//},
		fields: [
			{ name: 'IC_EPO',									type: 'int'	      },
			{ name: 'IC_MONEDA',								type: 'int'	      },
			{ name: 'NOMBRE_EPO'								},
			{ name: 'FECHA_VENCIMIENTO',					type: 'date',  	convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } },
			{ name: 'NOMBRE_MONEDA'							},
			{ name: 'TOTAL_DOCUMENTOS_OPR',				type: 'int'			},
			{ name: 'MONTO_OPR'								},
			{ name: 'TOTAL_DOCUMENTOS_SNOPR',			type: 'int'			},
			{ name: 'MONTO_SNOPR'							}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload:	{
				fn: function(store, options){
					Ext.getCmp('gridCifrasControl').getGridEl().unmask();
				}
			}, 
			load: 	procesarConsultaCifrasControl,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCifrasControl(null, null, null);						
				}
			}
		}
		
	});
 
	var nombreEpoRenderer01 = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
 	
	var nombreMonedaRenderer01 = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var totalDocumentosOprRenderer01 = function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_OPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_OPR'] === 0 ){
			return '';
		}

		value = Ext.util.Format.number(value,'0,0');
		return value;
		
	}
	
	var montoOprRenderer01 = function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_OPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_OPR'] === 0 ){
			return '';
		}

		return value;
		
	}
	
	var totalDocumentosSnOprRenderer01 = function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_SNOPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_SNOPR'] === 0 ){
			return '';
		}

		value = Ext.util.Format.number(value,'0,0');
		return value;
		
	}
	
	var montoSnOprRenderer01 = function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_SNOPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_SNOPR'] === 0 ){
			return '';
		}

		return value;
		
	}
	
	var gridCifrasControlColumnHeaderGroup = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
        	  [
        	  	  { header: '&nbsp;',                										colspan: 3, align: 'center' },
        	  	  { header: 'Operada&nbsp;/&nbsp;Operada Pagada', 						colspan: 2, align: 'center' },
        	  	  { header: 'Vencido sin Operar&nbsp;/&nbsp;Pagado sin Operar',	colspan: 2, align: 'center' }
        	  ]
        ]
   });
	
	// Grid con las cuentas registradas
	var gridCifrasControl = new Ext.grid.GridPanel({
		store: 				cifrasControlData,
		id:					'gridCifrasControl',
		style: 				'margin: 0 auto;',
		hidden: 				true,
		height:				400,
		margins: 			'20 0 0 0',
		title: 				'Cifras de Control',
		autoExpandColumn: 'NOMBRE_EPO',
		stateful:			false,
		columns: [
			{
					id:			'NOMBRE_EPO',	
					header: 		'EPO<br>&nbsp;',
					tooltip: 	'EPO<br>&nbsp;',
					dataIndex: 	'NOMBRE_EPO',
					sortable: 	true,
					resizable: 	true,
					//width: 		170,
					align:		'left', 
					renderer:	nombreEpoRenderer01
			},
			{
					header: 		'Fecha<br>Vencimiento',
					tooltip: 	'Fecha Vencimiento',
					dataIndex: 	'FECHA_VENCIMIENTO',
					sortable: 	true,
					resizable: 	true,
					width: 		80,
					align:		'center',
					xtype: 		'datecolumn', 
					format: 		'd/m/Y'
			},
			{
					header: 		'Moneda<br>&nbsp;',
					tooltip: 	'Moneda',
					dataIndex: 	'NOMBRE_MONEDA',
					sortable: 	true,
					resizable: 	true,
					width: 		124,
					align:		'center', 
					renderer:	nombreMonedaRenderer01
			},
			{
					header: 		'Total&nbsp;de<br>Documentos',
					tooltip: 	'Total de Documentos',
					dataIndex: 	'TOTAL_DOCUMENTOS_OPR',
					sortable: 	true,
					resizable: 	true,
					width: 		74,
					align:		'center',
					renderer:	totalDocumentosOprRenderer01
			},
			{
					header: 		'Monto<br>&nbsp;',
					tooltip: 	'Monto',
					dataIndex: 	'MONTO_OPR',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					align:		'center',
					renderer:	montoOprRenderer01
			},
			{
					header: 		'Total&nbsp;de<br>Documentos',
					tooltip: 	'Total de Documentos',
					dataIndex: 	'TOTAL_DOCUMENTOS_SNOPR',
					sortable: 	true,
					resizable: 	true,
					width: 		74,
					align:		'center',
					renderer:	totalDocumentosSnOprRenderer01
			},
			{
					header: 		'Monto<br>&nbsp;',
					tooltip: 	'Monto',
					dataIndex: 	'MONTO_SNOPR',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					align:		'center',
					renderer:	montoSnOprRenderer01
			}
		],
		stripeRows: true,
		loadMask: 	true,
		frame: 		true,
		plugins:		gridCifrasControlColumnHeaderGroup,
		bbar: {
			xtype: 		 'toolbar',
			buttonAlign: 'left',
			items: [
				'->',
				{
					xtype: 	'button',
					text: 	'Aceptar',
					iconCls: 'icoContinuar',
					id: 		'botonCompletarEnvioFFON',
					handler: procesaCompletarEnvioFFON 
				},
				'-',
				{
					xtype: 	'button',
					iconCls: 'cancelar',
					text: 	'Cancelar',
					id: 		'botonCancelarEnvioFFON',
					handler: procesaCancelarEnvioFFON 
				}
			]
		}
	});
	
	//---------------------------- 3. GRID DETALLE DISPERSIONES ENLISTADAS --------------------------
 
	// Guardar copia de renderer original de un MsgCheckColumn
	var originalMsgCheckColumnRenderer = (new Ext.ux.grid.MsgCheckColumn()).renderer;
						
	var nombreEpoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var nombreMonedaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var totalDocumentosOprRenderer 	= function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_OPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_OPR'] === 0 ){
			return '';
		}
 
		if( 
			record.data['TOTAL_DOCUMENTOS_OPR'] 	> 		0 
				&& 
			record.data['ENVIO_FFON_OPR'] 			=== 	true 
				&& 
			record.data['ENVIO_FFON_OPR_STATUS']	=== 	'READONLY'
		){
			metadata.attr = "style='color: green;'";
		} else {
			metadata.attr = "";
		}
 
		value = Ext.util.Format.number(value,'0,0');
		return value;
				
	}
	
	var montoOprRenderer 				= function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_OPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_OPR'] === 0 ){
			return '';
		}
 
		if( 
			record.data['TOTAL_DOCUMENTOS_OPR'] 	> 		0 
				&& 
			record.data['ENVIO_FFON_OPR'] 			=== 	true 
				&& 
			record.data['ENVIO_FFON_OPR_STATUS']	=== 	'READONLY'
		){
			metadata.attr = "style='color: green;'";
		} else {
			metadata.attr = "";
		}
 
		return value;
		
	}
	
	var observacionesOprRenderer 		= function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_OPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_OPR'] === 0 ){
			return '';
		}
		
		metadata.attr = 'style="border: thin solid #3399CC;"';
		return value;
		
	}
	
	var checkboxOprRenderer 			= function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_OPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_OPR'] === 0 ){
			return '';
		
		}
		
		// Hay datos, usar renderer original
		return originalMsgCheckColumnRenderer.apply(this,arguments);
		
	}
	
	var totalDocumentosSnOprRenderer = function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_OPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_SNOPR'] === 0 ){
			return '';
		}
		
		if( 
			record.data['TOTAL_DOCUMENTOS_SNOPR'] 	> 		0 
				&& 
			record.data['ENVIO_FFON_SNOPR'] 			=== 	true 
				&& 
			record.data['ENVIO_FFON_SNOPR_STATUS']	=== 	'READONLY'
		){
			metadata.attr = "style='color: green;'";
		} else {
			metadata.attr = "";
		}
 
		value = Ext.util.Format.number(value,'0,0');
		return value;
		
	}
	
	var montoSnOprRenderer 				= function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_OPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_SNOPR'] === 0 ){
			return '';
		}
		
		if( 
			record.data['TOTAL_DOCUMENTOS_SNOPR'] 	> 		0 
				&& 
			record.data['ENVIO_FFON_SNOPR'] 			=== 	true 
				&& 
			record.data['ENVIO_FFON_SNOPR_STATUS']	=== 	'READONLY'
		){
			metadata.attr = "style='color: green;'";
		} else {
			metadata.attr = "";
		}
 
		return value;
		
	}
	
	var observacionesSnOprRenderer 	= function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_OPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_SNOPR'] === 0 ){
			return '';
		}
		
		metadata.attr = 'style="border: thin solid #3399CC;"';
		return value;
		
	}
	
	var checkboxSnOprRenderer 			= function( value, metadata, record, rowIndex, colIndex, store){
		
		// Cuando el valor del campo: TOTAL_DOCUMENTOS_OPR sea igual a cero no se mostrar� contenido
		if( record.data['TOTAL_DOCUMENTOS_SNOPR'] === 0 ){
			return '';
		}
		
		// Hay datos, usar renderer original
		return originalMsgCheckColumnRenderer.apply(this,arguments);
		
	}
									
	var procesarConsultaDispersionesEnlistadas = function(store, registros, opts){

		var gridDetalleDispersionesEnlistadas = Ext.getCmp('gridDetalleDispersionesEnlistadas');
		var el 										  = gridDetalleDispersionesEnlistadas.getGridEl();
		el.unmask();
		
		if (registros != null) {
			
			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp("botonGuardarObservaciones").disable();
				Ext.getCmp("botonEjecutarInterfaseFFON").disable();
			}else{
				Ext.getCmp("botonGuardarObservaciones").enable();
				Ext.getCmp("botonEjecutarInterfaseFFON").enable();
			}
			
		}
 
		// Limpiar M�scara en la Forma de B�squeda
		envioFlujoFondos("ESPERAR_DECISION",null);
		
	}
	
	NE.dispersionepos.toggleSelection = function(headerCheckboxStatus,tipoDocumento){
	
		var columnDataIndex;
		var columnDataStatusIndex;
		
		// Determinar los tipos de documentos que seran afectados
		if(        tipoDocumento === 'OPR'   ){
			columnDataIndex			= 'ENVIO_FFON_OPR';
			columnDataStatusIndex	= 'ENVIO_FFON_OPR_STATUS';
		} else if( tipoDocumento === 'SNOPR' ){
			columnDataIndex			= 'ENVIO_FFON_SNOPR';
			columnDataStatusIndex	= 'ENVIO_FFON_SNOPR_STATUS';
		} else {
			return;
		}
		
		// Obtener texto a firmar
		var store = Ext.StoreMgr.key('dispersionesEnlistadasDataStore');
		store.each(
			
			function(record){
				
				var recordDataStatus = record.data[columnDataStatusIndex];
				// Asignar estatus del checkbox header si se cumple que: estatus del checkbox es NORMAL, y NO TIENE MENSAJE asociado.
				if( recordDataStatus.match(/^NORMAL[;]?$/) != null ){
					record.set(columnDataIndex,headerCheckboxStatus);
				}
				
			}
			
		);
			
		// Refrescar grid
		Ext.getCmp('gridDetalleDispersionesEnlistadas').getView().refresh();
		
		//	No dar commit al store de javascript, para finalizar la operaci�n.
		// store.commitChanges();		
		
	}
	
	var procesaGuardarObservaciones  = function(boton, evento) {
		
		// 0. Observaciones con cambios
		var observacionesModificadas = new Array();
		// 1. Extraer aquellas observaciones que sufrieron cambios.
		var store = Ext.StoreMgr.key('dispersionesEnlistadasDataStore');
		store.each(
			
			function(record){
				
				// Comparar si las observaciones en la secci�n Operada / Operada Pagada sufrieron cambios
				if( record.data['OBSERVACIONES_OPR']   !== record.data['OBSERVACIONES_ORIG_OPR']   ){
					
					var registro = new Object();
					registro['IC_EPO'] 			= record.get('IC_EPO');
					registro['IC_MONEDA'] 		= record.get('IC_MONEDA');
					registro['DC_VENCIMIENTO'] = Ext.util.Format.date(record.get('FECHA_VENCIMIENTO'),'d/m/Y');
					registro['TIPO_DOCUMENTO'] = 'OPR';
					registro['OBSERVACIONES']  = record.get('OBSERVACIONES_OPR');
					
					observacionesModificadas.push(registro);
					
				}
				// Comparar si las observaciones en la secci�n Vencido sin Operar / Pagado sin Operar sufri� cambios
				if( record.data['OBSERVACIONES_SNOPR'] !== record.data['OBSERVACIONES_ORIG_SNOPR'] ){
					
					var registro = new Object();
					registro['IC_EPO'] 			= record.get('IC_EPO');
					registro['IC_MONEDA'] 		= record.get('IC_MONEDA');
					registro['DC_VENCIMIENTO'] = Ext.util.Format.date(record.get('FECHA_VENCIMIENTO'),'d/m/Y');
					registro['TIPO_DOCUMENTO'] = 'SNOPR';
					registro['OBSERVACIONES']  = record.get('OBSERVACIONES_SNOPR');
					
					observacionesModificadas.push(registro);
					
				}
				
			}
			
		);
		
		// 2. Validar que haya al menos un registro con cambios, en caso contrario, cancelar la operaci�n
		if(observacionesModificadas.length == 0 ){
			Ext.Msg.alert(
				'Mensaje','Las observaciones no han sido modificadas, se aborta la operaci�n.'
			);
			return;
		}
		
		// 3. Realizar el guardado de las observaciones que fueron modificadas.
		var respuesta = new Object();
		respuesta['observacionesModificadas'] = Ext.encode(observacionesModificadas);
		envioFlujoFondos('GUARDAR_OBSERVACIONES',respuesta);
 
	}
	
	var procesaEjecutarInterfaseFFON = function(boton, evento) {
		
		// 1. Validar que haya al menos un registro con cambios, en caso contrario, 
		//	cancelar la operaci�n.
		var haySeleccion = false;
		var store = Ext.StoreMgr.key('dispersionesEnlistadasDataStore');
		store.each(
			
			function(record){
						
				// Debe haber un grupo SELECCIONADO, con estatus NORMAL y debe estar sin MENSAJE.
				
				var recordDataValue  = record.data['ENVIO_FFON_OPR'];
				var recordDataStatus = record.data['ENVIO_FFON_OPR_STATUS'];
				
				if( recordDataValue === true &&  recordDataStatus.match(/^NORMAL[;]?$/) != null  ){
					haySeleccion = true;
					return false;
				}
				
				// Debe haber un grupo SELECCIONADO, con estatus NORMAL y debe estar sin MENSAJE.
				
				recordDataValue  		= record.data['ENVIO_FFON_SNOPR'];
				recordDataStatus 		= record.data['ENVIO_FFON_SNOPR_STATUS'];
				
				if( recordDataValue === true &&  recordDataStatus.match(/^NORMAL[;]?$/) != null  ){
					haySeleccion = true;
					return false;
				}
				
			}
			
		);
		
		// Si no se encuentra que al menos un registro, se mostrar� el siguiente mensaje de error y se cancela
		// la operacion
		if( haySeleccion === false){
			Ext.Msg.alert(
				'Mensaje','Debe seleccionar al menos un grupo'
			);
			return;
		}		
				
		// Enviar registros a la pantalla de cifras de control
		envioFlujoFondos("CARGAR_CIFRAS_CONTROL",null);
		
		/*
		
			1. Validar que haya al menos un registro con cambios, en caso contrario, 
			cancelar la operaci�n.
 
			2. Mostrar animacion de enviando registros...
					
			3. Enviar registros a la pantalla de cifras de control
 
				ESTADO => MUESTRA_CIFRAS_CONTROL
					
			1. Crear array de datos, con aquellos registros que fueron seleccionados:
					
				Solo se tomar�n aquellos doctos seleccionados, con estatus NORMAL y sin MENSAJE.
				Se extraer�n los campos siguientes:
						
					IC_EPO, 		IC_MONEDA, 		DC_VENCIMIENTO,
					NOMBRE_EPO, NOMBRE_MONEDA, 
					TOTAL_DOCUMENTOS_OPR, 	TOTAL_MONTO_OPR, 		ENVIO_FFON_OPR
					TOTAL_DOCUMENTOS_SNOPR, TOTAL_MONTO_SNOPR, 	ENVIO_FFON_SNOPR
 
			2. Cargar registros seleccionados en el panel de cifras de control
					
			3. Suprimir animacion del panel anterior, ocultarlo junto con la forma de busqueda
					
			4. Mostrar panel de cifras de control
			
		*/
	}
	
	var dispersionesEnlistadasData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'dispersionesEnlistadasDataStore',
		url: 		'15dispersionepos01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaDispersionesEnlistadas'
		},
		idIndex: 		1,
		fields: [
			{ name: 'IC_EPO',									type: 'int'	      },
			{ name: 'IC_MONEDA',								type: 'int'	      },
			{ name: 'NOMBRE_EPO'								},
			{ name: 'FECHA_ENVIO_CLIENTE',				type: 'date',  	convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } },
			{ name: 'FECHA_VENCIMIENTO',					type: 'date',  	convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } },
			{ name: 'NOMBRE_MONEDA'							},
			{ name: 'TOTAL_DOCUMENTOS_OPR',				type: 'int'			},
			{ name: 'MONTO_OPR'								},
			{ name: 'OBSERVACIONES_OPR'					},
			{ name: 'OBSERVACIONES_ORIG_OPR'				},
			{ name: 'ENVIO_FFON_OPR',						type: 'boolean'	},
			{ name: 'ENVIO_FFON_OPR_STATUS'				},
			{ name: 'FECHA_ENVIO_FLUJO_FONDOS_OPR',	type: 'date',  	convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } },
			{ name: 'TOTAL_DOCUMENTOS_SNOPR',			type: 'int'			},
			{ name: 'MONTO_SNOPR'							},
			{ name: 'OBSERVACIONES_SNOPR'					},
			{ name: 'OBSERVACIONES_ORIG_SNOPR'			},
			{ name: 'ENVIO_FFON_SNOPR',					type: 'boolean'	},
			{ name: 'ENVIO_FFON_SNOPR_STATUS'			},
			{ name: 'FECHA_ENVIO_FLUJO_FONDOS_SNOPR',	type: 'date',  	convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } },
			{ name: 'VER_DETALLE_DISPERSION',			type: 'boolean'	},
			{ name: 'LOGIN_USUARIO'							},
			{ name: 'NOMBRE_USUARIO'						}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
					var gridDetalleDispersionesEnlistadas = Ext.getCmp("gridDetalleDispersionesEnlistadas");
					if( !gridDetalleDispersionesEnlistadas.isVisible() ){
						gridDetalleDispersionesEnlistadas.show();
					}
				}
			},
			load: 	procesarConsultaDispersionesEnlistadas,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDispersionesEnlistadas(null, null, null);						
				}
			}
		}
		
	});
	
	var gridDetalleDispersionesEnlistadasColumnHeaderGroup = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
        	  [
        	  	  { header: '&nbsp;',                										colspan: 4, align: 'center' },
        	  	  { header: 'Operada&nbsp;/&nbsp;Operada Pagada', 						colspan: 4, align: 'center' },
        	  	  { header: 'Vencido sin Operar&nbsp;/&nbsp;Pagado sin Operar',	colspan: 4, align: 'center' },
        	  	  { header: '&nbsp;',                										colspan: 1, align: 'center' }
        	  ]
        ]
    });
	
	var gridDetalleDispersionesEnlistadasColModel = new Ext.grid.ColumnModel({
		columns: [
				{
					header: 		'EPO<br>&nbsp;',
					tooltip: 	'EPO<br>&nbsp;',
					dataIndex: 	'NOMBRE_EPO',
					sortable: 	true,
					resizable: 	true,
					width: 		170,
					align:		'left', 
					renderer:	nombreEpoRenderer
				},
				{
					header: 		'Fecha&nbsp;Env&iacute;o&nbsp;a<br>Cliente',
					tooltip: 	'Fecha Env�o a Cliente',
					dataIndex: 	'FECHA_ENVIO_CLIENTE',
					sortable: 	true,
					resizable: 	true,
					width: 		80,
					align:		'center',
					xtype: 		'datecolumn', 
					format: 		'd/m/Y'
				},
				{
					header: 		'Fecha<br>Vencimiento',
					tooltip: 	'Fecha Vencimiento',
					dataIndex: 	'FECHA_VENCIMIENTO',
					sortable: 	true,
					resizable: 	true,
					width: 		80,
					align:		'center',
					xtype: 		'datecolumn', 
					format: 		'd/m/Y'
				},
				{
					header: 		'Moneda<br>&nbsp;',
					tooltip: 	'Moneda',
					dataIndex: 	'NOMBRE_MONEDA',
					sortable: 	true,
					resizable: 	true,
					width: 		124,
					align:		'center', 
					renderer:	nombreMonedaRenderer
				},
				{
					header: 		'Total&nbsp;de<br>Documentos',
					tooltip: 	'Total de Documentos',
					dataIndex: 	'TOTAL_DOCUMENTOS_OPR',
					sortable: 	true,
					resizable: 	true,
					width: 		74,
					align:		'center',
					renderer:	totalDocumentosOprRenderer
				},
				{
					header: 		'Monto<br>&nbsp;',
					tooltip: 	'Monto',
					dataIndex: 	'MONTO_OPR',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					align:		'center',
					renderer:	montoOprRenderer
				},
				{
					header: 		'Observaciones<br>&nbsp;', 
					tooltip: 	'Observaciones',
					dataIndex: 	'OBSERVACIONES_OPR', 
					fixed:		true,
					sortable: 	false,	
					width: 		174,
					hiddeable: 	false,
					editor:{
						xtype:	'textarea',
						id: 		'txtAreaObservacionesOpr',
						maxLength: 1001,
						enableKeyEvents: true,
						listeners:	{
							keyup: function(field, e){
								if ( (field.getValue()).length > 1000){
									field.setValue((field.getValue()).substring(0,1000));
									Ext.Msg.alert('Observaciones','La causas de rechazo no debe sobrepasar los 1000 caracteres.');								
									field.focus();
								}
							}
						}
					},
					renderer:	observacionesOprRenderer
				},
				{
					xtype: 				'msgcheckcolumn', 
					header: 				'<input type="checkbox" name="check"  id="ENVIO_FF_HDR_OPR" onclick="NE.dispersionepos.toggleSelection(this.checked,\'OPR\');"/>',
					tooltip: 			'Enviar a Flujo Fondos',
					dataIndex: 			'ENVIO_FFON_OPR',
					dataStatusIndex: 	'ENVIO_FFON_OPR_STATUS',
					sortable: 			false,
					hideable:			false,
					resizable: 			false,
					menuDisabled:		true,
					width: 				34,
					align:				'center',
					renderer:			checkboxOprRenderer
				},
				{
					header: 		'Total&nbsp;de<br>Documentos',
					tooltip: 	'Total de Documentos',
					dataIndex: 	'TOTAL_DOCUMENTOS_SNOPR',
					sortable: 	true,
					resizable: 	true,
					width: 		74,
					align:		'center',
					renderer:	totalDocumentosSnOprRenderer
				},
				{
					header: 		'Monto<br>&nbsp;',
					tooltip: 	'Monto',
					dataIndex: 	'MONTO_SNOPR',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					align:		'center',
					renderer:	montoSnOprRenderer
				},
				{
					header: 		'Observaciones<br>&nbsp;', 
					tooltip: 	'Observaciones',
					dataIndex: 	'OBSERVACIONES_SNOPR', 
					fixed:		true,
					sortable: 	false,	
					width: 		174,
					hiddeable: 	false,
					editor:{
						xtype:	'textarea',
						id: 		'txtAreaObservacionesSnOpr',
						maxLength: 1001,
						enableKeyEvents: true,
						listeners:	{
							keyup: function(field, e){
								if ( (field.getValue()).length > 1000){
									field.setValue((field.getValue()).substring(0,1000));
									Ext.Msg.alert('Observaciones','La causas de rechazo no debe sobrepasar los 1000 caracteres.');								
									field.focus();
								}
							}
						}
					},
					renderer:	observacionesSnOprRenderer
				},
				{
					xtype: 				'msgcheckcolumn', 
					header: 				'<input type="checkbox" name="check"  id="ENVIO_FF_HDR_SNOPR" onclick="NE.dispersionepos.toggleSelection(this.checked, \'SNOPR\');"/>',
					tooltip: 			'Enviar a Flujo Fondos',
					dataIndex: 			'ENVIO_FFON_SNOPR',
					dataStatusIndex: 	'ENVIO_FFON_SNOPR_STATUS',
					sortable: 			false,
					hideable:			false,
					resizable: 			false,
					menuDisabled:		true,
					width: 				34,
					align:				'center',
					renderer:			checkboxSnOprRenderer
				},
				{
					xtype: 		'actioncolumn',
					header: 		'Detalle<br>&nbsp;',
					tooltip: 	'Detalle',
					width: 		56,
					hidden: 		false,
					align:		'center',
					items: [
						 // ACCION: VER DETALLE DISPERSION
						 {
							  getClass: function(value, metadata, record) { 
									 
									if ( record.data['VER_DETALLE_DISPERSION'] === true ) {
										 this.items[0].tooltip = 'Ver Detalle';
										 return 'icoLupa';
									} else {
										 this.items[0].tooltip = null;
										 return null;
									}
									
							  },
							  handler: function(grid, rowIndex, colIndex) {
	
							  	  	// Obtener registro
									var record = Ext.StoreMgr.key('dispersionesEnlistadasDataStore').getAt(rowIndex);
									
									// Preparar parametros de la consulta
									var icEpo								= record.get('IC_EPO');
									var icMoneda							= record.get('IC_MONEDA');
									var fechaVencimiento 				= record.get('FECHA_VENCIMIENTO');
									var fechaEnvioFlujoFondosOpr		= record.get('FECHA_ENVIO_FLUJO_FONDOS_OPR');
									var fechaEnvioFlujoFondosSnOpr	= record.get('FECHA_ENVIO_FLUJO_FONDOS_SNOPR');
									var nombreEPO							= record.get('NOMBRE_EPO');
									var nombreMoneda						= record.get('NOMBRE_MONEDA');
 
									fechaVencimiento 						= Ext.util.Format.date(fechaVencimiento,'d/m/Y');
									fechaEnvioFlujoFondosOpr			= Ext.isEmpty(fechaEnvioFlujoFondosOpr  )?'':Ext.util.Format.date(fechaEnvioFlujoFondosOpr,  'd/m/Y');
									fechaEnvioFlujoFondosSnOpr			= Ext.isEmpty(fechaEnvioFlujoFondosSnOpr)?'':Ext.util.Format.date(fechaEnvioFlujoFondosSnOpr,'d/m/Y');
 
							  	   // Agregar detalle de la informacion consultada
									Ext.getCmp('cabeceraDetalleDispersionesEPO.NOMBRE_EPO').setValue(nombreEPO);
									Ext.getCmp('cabeceraDetalleDispersionesEPO.FECHA_VENCIMIENTO').setValue(fechaVencimiento);
									Ext.getCmp('cabeceraDetalleDispersionesEPO.NOMBRE_MONEDA').setValue(nombreMoneda);
									
									// Remover contenido del grid
									var detalleDispersionesEPOData = Ext.StoreMgr.key('detalleDispersionesEPODataStore');
									detalleDispersionesEPOData.removeAll();
									
									// Remover detalle de los totales
									Ext.getCmp('totalesDetalleDispersionesEPO.TOTAL_OPERACIONES').setValue('');       
									Ext.getCmp('totalesDetalleDispersionesEPO.IMPORTE_TOTAL_DESCUENTO').setValue(''); 
 
									// Consultar cuentas registradas con el proveedor
									var detalleDispersionesEPOData   = Ext.StoreMgr.key("detalleDispersionesEPODataStore");
									detalleDispersionesEPOData.load({
										params: {
											operacion: 							'NuevaConsulta', //Generar datos para la consulta
											start: 								0,
											limit: 								15,
											icEpo:								icEpo,
											icMoneda:							icMoneda,
											fechaVencimiento:					fechaVencimiento, 
											fechaEnvioFlujoFondosOpr:		fechaEnvioFlujoFondosOpr,
											fechaEnvioFlujoFondosSnOpr:	fechaEnvioFlujoFondosSnOpr
										}
									});
									
							  }
						 }
					]
				}
			],
			isCellEditable: function(col, row) {
				
				var columnDataIndex = this.getDataIndex(col);
				if(        columnDataIndex === "OBSERVACIONES_OPR"   ){
					var store  = Ext.StoreMgr.key('dispersionesEnlistadasDataStore');
					var record = store.getAt(row);
					if( record.data['TOTAL_DOCUMENTOS_OPR'] === 0 ){
						return false;
					}
				} else if( columnDataIndex === "OBSERVACIONES_SNOPR" ){
					var store  = Ext.StoreMgr.key('dispersionesEnlistadasDataStore');
					var record = store.getAt(row);
					if( record.data['TOTAL_DOCUMENTOS_SNOPR'] === 0 ){
						return false;
					}
				}
				return Ext.grid.ColumnModel.prototype.isCellEditable.call(this, col, row);
				
			}
	});
	
	var gridDetalleDispersionesEnlistadas = {
		store: 		dispersionesEnlistadasData,
		xtype: 		'editorgrid',
		id:			'gridDetalleDispersionesEnlistadas',
		title:		'Resultados de la Consulta',
		frame:		true,
		stripeRows: true,
		loadMask: 	true,
		height: 		400,
		hidden:		true,
		style: {
			borderWidth: 0
		},
		colModel: 	gridDetalleDispersionesEnlistadasColModel,
		stateful:	false,
		plugins: 	gridDetalleDispersionesEnlistadasColumnHeaderGroup,
		bbar: {
			xtype: 		 'toolbar',
			buttonAlign: 'left',
			items: [
				'->',
				{
					xtype: 	'button',
					text: 	'Guardar',
					iconCls: 'icoGuardar',
					id: 		'botonGuardarObservaciones',
					handler: procesaGuardarObservaciones 
				},
				'-',
				{
					xtype: 	'button',
					iconCls: 'icoContinuar',
					text: 	'Ejecutar Interfase de Flujo de Fondos',
					id: 		'botonEjecutarInterfaseFFON',
					handler: procesaEjecutarInterfaseFFON 
				}
			]
		},
		generandoArchivo: false,
		recordId:	null
	};
	
	//------------------------------- 2. PANEL BUSQUEDA DISPERSIONES --------------------------------
 
	var elementosBusquedaDispersiones = [
		// DISPLAYFIELD: CONCEPTO
		{
			xtype: 		'displayfield',
			id:	 		'displayConcepto',
			cls:			'x-form-item',
			fieldLabel: 'Concepto',
			value:  		'Abonos'
		},
		// DATEFIELD: FECHA DE VENCIMIENTO
		{
			xtype: 		'datefield',
			fieldLabel: 'Fecha de Vencimiento', // (dd/mm/yyyy)
			name: 		'fechaVencimiento',
			id: 			'fechaVencimiento',
			maxlenght: 	10,
			allowBlank: false,
			anchor:		'-100',
			msgTarget:  'side',
			margins: 	'0 20 0 0'
		}
	];
	
	var panelBusquedaDispersiones = new Ext.form.FormPanel({
		id: 					'panelBusquedaDispersiones',
		width: 				400,
		title: 				'Forma de B�squeda',
		frame: 				true,
		hidden:				true,
		collapsible: 		true,
		titleCollapse: 	true,
		trackResetOnLoad: true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		renderHidden:		true, // Nota: no usar hidden: true -> hara que los componentes no se muestren del tamanio correcto
		labelWidth: 		148,
		defaultType: 		'textfield',
		items: 				elementosBusquedaDispersiones,
		monitorValid: 		false,
		buttons: [
			{
				id:		'botonBuscar',
				text: 	'Buscar',
				hidden: 	false,
				iconCls: 'icoBuscar',
				handler: function() {
 
					// Cancelar b�squeda adicionales si alguno de los campos de la forma
					// viene vacio
					var panelBusquedaDispersiones = Ext.getCmp("panelBusquedaDispersiones").getForm();
					if( !panelBusquedaDispersiones.isValid() ){
						return;
					}

					// Realizar b�squeda
					var respuesta								= new Object();
					respuesta               				= Ext.apply( respuesta, panelBusquedaDispersiones.getValues() );
					envioFlujoFondos("BUSCAR_DISPERSIONES_ENLISTADAS", respuesta);
 
				}
				
			},
			// BOTON LIMPIAR
			{
				text: 	'Limpiar',
				hidden: 	false,
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('panelBusquedaDispersiones').getForm().reset();
				}
			}
		]
	});
 
	//--------------------------------- 1. BOX DE INICIALIZACION ---------------------------------------
	
	var boxInicializacion = [
		{
			xtype: 		'box',
			id: 			'contenedorInicializacion',
			cls:			'realizandoActividad',
			style: 		'margin: 0 auto',
			width: 		16,
			height: 		16
		}
	];
 
	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			boxInicializacion,
			panelBusquedaDispersiones,
			NE.util.getEspaciador(10),
			gridDetalleDispersionesEnlistadas,
			gridCifrasControl
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	envioFlujoFondos("INICIALIZACION",null);
	
});