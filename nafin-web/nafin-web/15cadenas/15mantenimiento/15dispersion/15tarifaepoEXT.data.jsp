<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import=
	" net.sf.json.JSONArray,	  		net.sf.json.JSONObject,
	  com.netro.exception.*, 			com.netro.dispersion.*,
	  com.netro.model.catalogos.*,	netropology.utilerias.*,
	  java.util.*, java.sql.*,  	javax.naming.*,com.netro.afiliacion.*,  
	  org.apache.commons.logging.Log"
			
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
String noEpo			  = request.getParameter("_cmb_epo");
String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String operacion		= (request.getParameter("operacion")	!=null)	?	request.getParameter("operacion")	:	"";
String infoRegresar	= "";

Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);

	
if (informacion.equals("catalogoepo") )	{

	JSONObject 					jsonObj 	= new JSONObject();
	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setBancofondeo("1");
	infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("provedor")){
  String provedor	= (request.getParameter("provedor")	!=null)	?	request.getParameter("provedor")	:	"";
  String epo	= (request.getParameter("epo")	!=null)	?	request.getParameter("epo")	:	"";
  JSONObject 		jsonObj = new JSONObject();
  String clave = "";
  
  clave = dispersion.getClaveProveedorByName(provedor,epo);
  jsonObj.put("success"	,  new Boolean(true)); 
  jsonObj.put("clave", clave);	
  infoRegresar = jsonObj.toString(); 

}else if(informacion.equals("dispersion")) {
	List 				registros = new ArrayList();
	List 				tarifaNac = new ArrayList();
	List 				tarifaDol = new ArrayList();
	HashMap			beanResult= new HashMap();
	JSONObject 		jsonObj = new JSONObject();
		
	boolean disperOperada			    = false;
	boolean disperOperadaPagada	  = false;
	boolean disperPagadoSinOper 	= false;
	boolean disperVencidoSinOper	= false;
	boolean disperMonedaNacional	= false;
	boolean disperDolaresAmerica	= false;
	
	String dOperada			  ="";
	String dOperadaPagada	="";
	String dPagadoSinOper	="";
	String dVencidoSinOper="";
	String claveEpoEnFFON	="";
	String monedaNacional	="";
	String dolaresAmerica	="";
	String tipoArchivo		="";
	String nombreProvedor	="";
	String claveProvedor	="";
	
  if (operacion.equals("borrarRango")){
  log.debug("Entro a borrar rango con noEpo: "+noEpo);
  Boolean dMonNac = Boolean.valueOf(request.getParameter("dMonNac"));  
  boolean eliminaMonedaNacional = ((Boolean) dMonNac).booleanValue();
  Boolean dDolAme = Boolean.valueOf(request.getParameter("dDolAme"));  
  boolean eliminaDolaresAmerica = ((Boolean) dDolAme).booleanValue();
  if(eliminaMonedaNacional){
    dispersion.delTarifaEpo(noEpo);
  }
  if(eliminaDolaresAmerica){
    dispersion.delTarifaEpoUSD(noEpo);
  }
  jsonObj.put("borrar"	,  new Boolean(true));
  } else {
    jsonObj.put("borrar"	,  new Boolean(false)); 
  }
  
	try{
		disperOperada				  = dispersion.hasEstatusDispersionOperada(noEpo);
		disperOperadaPagada		= dispersion.hasEstatusDispersionOperadaPagada(noEpo);
		disperPagadoSinOper		= dispersion.hasEstatusDispersionPagadoSinOperar(noEpo);
		disperVencidoSinOper	= dispersion.hasEstatusDispersionVencidoSinOperar(noEpo);
		claveEpoEnFFON				= dispersion.getClaveEpoEnFFON(noEpo);
		disperMonedaNacional	= dispersion.hasTarifaDispersionMonedaNacional(noEpo);
		tarifaNac				    	= dispersion.getTarifaEpo(noEpo);
		disperDolaresAmerica	= dispersion.hasTarifaDispersionDolaresAmericanos(noEpo);
		tarifaDol					    = dispersion.getTarifaEpoUSD(noEpo);
		tipoArchivo					  = dispersion.getTipoArchivoAEnviar(noEpo);
		nombreProvedor				= dispersion.getNombreProveedor(noEpo);
		claveProvedor				  = dispersion.getClaveProveedor(noEpo);
		
	}catch(Exception e){
		log.info("Ocurrio un error durante las consultas al BEAN: "+ e);
	}
	
	if (disperOperada)
        dOperada="true";
	else 	dOperada="false";
	
	if (disperOperadaPagada)
        dOperadaPagada="true";
	else 	dOperadaPagada="false";
	
	if (disperPagadoSinOper)
        dPagadoSinOper="true";
	else 	dPagadoSinOper="false";
	
	if (disperVencidoSinOper)
        dVencidoSinOper="true";
	else 	dVencidoSinOper="false";
	
	beanResult.put("noEpo"		,noEpo);
	
	beanResult.put("disperOperada"		    ,dOperada);
	beanResult.put("disperOperadaPagada"  ,dOperadaPagada);
	beanResult.put("disperPagadoSinOper"  ,dPagadoSinOper);
	beanResult.put("disperVencidoSinOper" ,dVencidoSinOper);
	
	beanResult.put("claveEpoEnFFON",claveEpoEnFFON);
	
	if (disperMonedaNacional)
        monedaNacional="true";
  else  monedaNacional="false";	
	  
  beanResult.put("monedaNacional",monedaNacional);
	
	List      monNac  = new ArrayList();
	Iterator 	it      = null;
	String    v1      ="";
	String    v2      ="";
	String    v	      ="";  
	int sizeTarifaNac =0;
  
	try {
		sizeTarifaNac = tarifaNac.size();		
		it = tarifaNac.iterator();
		while(it.hasNext()){
			HashMap datos  = new HashMap();
			List    campos = (List)   it.next();
                  v1 = (String) campos.get(0);
                  v2 = (String) campos.get(1);
			datos.put("dias",v1);
			datos.put("tarifa",v2);
			monNac.add(datos);
		}
	} catch (Exception e){
		log.debug("ERROR Iterator List");
	}
	String sizeTarNac = String.valueOf(sizeTarifaNac);
	beanResult.put("sizeTarifaNac",sizeTarNac);
	beanResult.put("monNac",monNac);
	beanResult.put("tarifaNac",tarifaNac);
	
	if (disperDolaresAmerica)
        dolaresAmerica="true";
	else 	dolaresAmerica="false";
  
	beanResult.put("dolaresAmerica",dolaresAmerica);	
  
  List      monDolar= new ArrayList();
	Iterator 	itd      = null;
	String    v1d      ="";
	String    v2d      ="";
	String    vd       ="";    
	int sizeTarifaDol  =0;
  
  try {
		sizeTarifaDol = tarifaDol.size();		
		it = tarifaDol.iterator();
		while(it.hasNext()){
			HashMap datosd  = new HashMap();
			List    camposd = (List)   it.next();
                  v1d = (String) camposd.get(0);
                  v2d = (String) camposd.get(1);
			datosd.put("dias",v1d);
			datosd.put("tarifa",v2d);
			monDolar.add(datosd);
		}
	} catch (Exception e){
		log.debug("ERROR Iterator List");
	}
  String sizeTarDol = String.valueOf(sizeTarifaDol);
	beanResult.put("sizeTarifaDol",sizeTarDol);
	beanResult.put("monDol",monDolar);
	beanResult.put("tarifaDol",tarifaDol);
	
	beanResult.put("tipoArchivo",tipoArchivo);
	beanResult.put("nombreProvedor",nombreProvedor);
	beanResult.put("claveProvedor",claveProvedor);
	
  HashMap parametrosDispersion = dispersion.getParametrosDeDispersion(noEpo);
  HashMap correoNafin = new HashMap();
  List cN = new ArrayList();
  int sizeCuentasNafin = 0;
  
  String [] cadena = (String[])parametrosDispersion.get("CUENTAS_NAFIN");
  try {
    for(int i=0;i<cadena.length;i++){
      correoNafin = new HashMap();
      correoNafin.put("clave",cadena[i]);
      correoNafin.put("descripcion",cadena[i]);
      cN.add(correoNafin);
      sizeCuentasNafin++;
      }
  } catch(Exception e){
  log.debug("No hay correos");
  }
  String sizeCN = String.valueOf(sizeCuentasNafin);
  beanResult.put("sizeCN",sizeCN); 
  beanResult.put("CUENTAS_NAFIN",cN);
  
  HashMap correoEpo = new HashMap();
  List cE = new ArrayList();
  int sizeCuentasEpo = 0;
  String [] cadenaEpo = (String[])parametrosDispersion.get("CUENTAS_EPO");
  try {
    for(int i=0;i<cadenaEpo.length;i++){
      correoEpo = new HashMap();
      correoEpo.put("clave",cadenaEpo[i]);
      correoEpo.put("descripcion",cadenaEpo[i]);
      cE.add(correoEpo);
      sizeCuentasEpo++;
      }
  } catch(Exception e){
  log.debug("No hay correos");
  }
  String sizeCE = String.valueOf(sizeCuentasEpo);
  beanResult.put("sizeCE",sizeCE); 
  beanResult.put("CUENTAS_EPO",cE);
  
	registros.add(beanResult);
	
	jsonObj.put("success"	,  new Boolean(true)); 
	jsonObj.put("registros", registros);
  
  infoRegresar = jsonObj.toString();

} else if(informacion.equals("guardar")) {
  JSONObject 		jsonObj = new JSONObject();
	
  String claveEpo	  = (request.getParameter("claveEpo")	!=null)	?	request.getParameter("claveEpo")	:	"";
  String clavePyme	= (request.getParameter("clavePyme")	!=null)	?	request.getParameter("clavePyme")	:	"";
  String correoNaf	= (request.getParameter("correoNaf")	!=null)	?	request.getParameter("correoNaf")	:	"";
  String correoEpo	= (request.getParameter("correoEpo")	!=null)	?	request.getParameter("correoEpo")	:	"";
  String dTipoArc 	= (request.getParameter("dTipoArc")	!=null)	?	request.getParameter("dTipoArc")	:	"";
  
  Boolean dOperada = Boolean.valueOf(request.getParameter("dOperada"));  
  boolean disperOperada = ((Boolean) dOperada).booleanValue();
  
  Boolean dOperPag = Boolean.valueOf(request.getParameter("dOperPag"));  
  boolean disperOperadaPagada = ((Boolean) dOperPag).booleanValue();
    
  Boolean dPagSO = Boolean.valueOf(request.getParameter("dPagSO"));  
  boolean disperPagadoSinOper = ((Boolean) dPagSO).booleanValue();
  
  Boolean dVenSO = Boolean.valueOf(request.getParameter("dVenSO"));  
  boolean disperVencidoSinOper = ((Boolean) dVenSO).booleanValue();
  
  Boolean dMonNac = Boolean.valueOf(request.getParameter("dMonNac"));  
  boolean disperMonedaNacional = ((Boolean) dMonNac).booleanValue();
  
  Boolean dDolAme = Boolean.valueOf(request.getParameter("dDolAme"));  
  boolean disperDolaresAmerica = ((Boolean) dDolAme).booleanValue();
  
  String claveFFON 	= (request.getParameter("claveFFON")	!=null)	?	request.getParameter("claveFFON")	:	"";
  String cN;
  String cE;
  if (correoNaf.equals("")){
       cN=null;
  } else {
     cN = correoNaf.substring(0,correoNaf.length()-1);
  }
  if (correoEpo.equals("")){
       cE = null;
  } else {
     cE = correoEpo.substring(0,correoEpo.length()-1); 
  }
  String[] diasMN	  = request.getParameterValues("diasMN");	
  String[] tarifaMN	  = request.getParameterValues("tarifaMN");	
  String[] diasDol	  = request.getParameterValues("diasDol");	
  String[] tarifaDol	  = request.getParameterValues("tarifaDol");	
  
  try {
  dispersion.setParametrosDeDispersion(claveEpo,clavePyme,cN,cE);
  dispersion.setEstatusDispersionOperada(claveEpo,disperOperada);
  dispersion.setEstatusDispersionOperadaPagada(claveEpo,disperOperadaPagada);
  dispersion.setEstatusDispersionPagadoSinOperar(claveEpo,disperPagadoSinOper);
  dispersion.setEstatusDispersionVencidoSinOperar(claveEpo,disperVencidoSinOper);
  dispersion.setEstatusTarifaDispersionMonedaNacional(claveEpo,disperMonedaNacional);
  dispersion.setEstatusTarifaDispersionDolaresAmericanos(claveEpo,disperDolaresAmerica);
  dispersion.setTipoArchivoAEnviar(claveEpo,dTipoArc);
  
  if(disperMonedaNacional){
		dispersion.setTarifaEpo(claveEpo, diasMN, tarifaMN);
	}
  if(disperDolaresAmerica){
    dispersion.setTarifaEpoUSD(claveEpo, diasDol, tarifaDol);
	}
          
  dispersion.setClaveEpoEnFFON(claveEpo,claveFFON); 
  jsonObj.put("success"	,  new Boolean(true));
  jsonObj.put("guardar"	,  new Boolean(true));
  } catch (Exception e){
    System.err.println("eRROr::"+e);
    jsonObj.put("success"	,  new Boolean(false));
    jsonObj.put("guardar"	,  new Boolean(false));
  }
  
  infoRegresar = jsonObj.toString(); 
  
} else if(informacion.equals("busquedaAvanzada")) {
	Vector			vecFilas 	  = new Vector();
	List 				registros 	= new ArrayList();
	JSONObject 	jsonObj 	 	= new JSONObject();
	String 			epo			    = request.getParameter("claveEPO");
	String 			nombre		  = (request.getParameter("txtNombre") 		   !=null)	?	request.getParameter("txtNombre")		    :	"";
	String 			rfc			    = (request.getParameter("txtRFC")			     !=null)	?	request.getParameter("txtRFC")			    :	"";
	String 			proveedor	   = (request.getParameter("txtNumProveedor")!=null)	?	request.getParameter("txtNumProveedor") :	"";
	//Vector getProveedores(String ic_producto_nafin, String ic_epo, String num_pyme, String rfc_pyme, String nombre_pyme, String ic_pyme, String pantalla,String ic_if) 
	registros = getProveedores("", epo,proveedor,rfc,nombre,"","listReq","");
	 
	jsonObj.put("success"	,  new Boolean(true)); 
	jsonObj.put("registros", registros);	
  infoRegresar = jsonObj.toString();
  
} else if(informacion.equals("ConsultaProducto"))	{
	
	Registros reg = BeanAfiliacion.getProductoNafin(noEpo);
	HashMap resultado = new HashMap();
	ArrayList lista_res = new ArrayList();
	/*Barro  la informacion del Query */
	while(reg.next()) {
		resultado = new HashMap();
		String nombre_producto	= reg.getString(1) == null? "":reg.getString(1).trim();   
		String ic_producto		= reg.getString(2) == null? "":reg.getString(2);   
		String dispersion_1 		= reg.getString("CG_DISPERSION")==null?"":reg.getString("CG_DISPERSION");
		String dispNoNeg  		= reg.getString("CS_DISPERSION_NO_NEGOCIABLES")==null?"":reg.getString("CS_DISPERSION_NO_NEGOCIABLES");

		String auxDisp = "", auxDispNoNeg ="";
		if (!"".equals(dispersion) ) {
			auxDisp = "S".equals(dispersion_1)?"Si":"No";
		}
		if(!"".equals(dispNoNeg)) {
			auxDispNoNeg = "S".equals(dispNoNeg)?"Si":"No";
		}
		resultado.put("ic_producto",ic_producto);
		resultado.put("nombre_producto",nombre_producto);
		resultado.put("dispersion",auxDisp);
		resultado.put("dispNoNeg",auxDispNoNeg);
		lista_res.add(resultado);
	}
	JSONArray jsObjArray = JSONArray.fromObject(lista_res);
	infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
	

} else if(informacion.equals("actualizaDispersion"))	{

	String   ic_epo 				 	 = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo"),
	ic_producto_nafin	 	 = (request.getParameter("ic_producto") == null) ? "" : request.getParameter("ic_producto"),
	nombre_producto 	 	 = (request.getParameter("nombre_producto")==null)?"":request.getParameter("nombre_producto"),
	chk_dispersion 	 	 = (request.getParameter("chk_dispersion")==null)?"":request.getParameter("chk_dispersion"),
	chk_dispersion_no_neg = (request.getParameter("chk_dispersion_no_neg")==null)?"":request.getParameter("chk_dispersion_no_neg");
			
	BeanAfiliacion.actualizaDispersion(ic_producto_nafin, ic_epo,chk_dispersion, chk_dispersion_no_neg);
			
	JSONObject resultado = new JSONObject();
	resultado.put("success", new Boolean(true));
	resultado.put("msg", "Actualización satisfactoria");
	infoRegresar = resultado.toString();	
	
	
 
}		
		 

%>
<%=   infoRegresar %>

<%!
Vector getProveedores(String ic_producto_nafin, String ic_epo, String num_pyme, String rfc_pyme, String nombre_pyme, String ic_pyme, String pantalla,String ic_if) {
System.out.println("\n getProveedores::(E) ");
	
	AccesoDB			con				= null;
	String				condicion		= "";
	String				condicionDoc	= "";
	String				tablaDoc		= "";
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	Vector vecRows = new Vector();
	Vector vecCols = null;
	String campoProveedor = "0"; //Geag
	HashMap hash	= new HashMap();
	List	  total	= new ArrayList();
	
	try {
		con = new AccesoDB();
		con.conexionDB();
		if(!"".equals(ic_pyme)) {
			condicion = "";

			if(!"".equals(ic_epo)) {
				condicion += "    AND r.ic_epo = ?";
				campoProveedor = "r.cg_pyme_epo_interno"; //Geag
			} else {
				//Si la EPO es nula, entonces el numero de proveedor no tiene sentido
				//por lo cual se pone un valor fijo para poder hacer un distinct
				campoProveedor = "0"; //Geag
			}
			
			qrySentencia = 
				" SELECT /*+index(crn CP_COMREL_NAFIN_PK) use_nl(r p)*/"   +
//				"        p.ic_pyme, r.cg_pyme_epo_interno num_prov, p.cg_razon_social,"   +
				"        distinct p.ic_pyme, " + campoProveedor + " as num_prov, p.cg_razon_social,"   +	//Geag
				"        crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega, "   +
				"        crn.ic_nafin_electronico"   +
				"   FROM comrel_nafin crn, comrel_pyme_epo r, comcat_pyme p"   +
				"  WHERE crn.ic_epo_pyme_if = p.ic_pyme"   +
				"    AND p.ic_pyme = r.ic_pyme"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND p.ic_pyme = ?"   +
				"    AND r.ic_pyme = ?"+condicion;
//			System.out.println("\n qrySentencia: "+qrySentencia);
			if(!ic_if.equals("")){
				qrySentencia +=  
					" AND p.ic_pyme in ( select "  +
					"	      distinct "  +
					"		      cuenta.ic_pyme "  + 
					"     from  "  +
					"	      comrel_pyme_if rel, "  +
					"	      comrel_cuenta_bancaria cuenta "  +
					"     where "  +
					"	      rel.cs_vobo_if = 'S' and "  + 
					"	      rel.cs_borrado = 'N' and "  +
					"	      rel.ic_if      =  ?  and "  +
					"	      rel.ic_cuenta_bancaria = cuenta.ic_cuenta_bancaria and "  +
					"	cuenta.cs_borrado = 'N' ) ";
			}

			System.out.println(qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_pyme));
			ps.setInt(2,Integer.parseInt(ic_pyme));
			int cont = 2;
			if(!"".equals(ic_epo)){
				cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
			}
			if(!"".equals(ic_if)){ 
				cont++;ps.setString(cont,ic_if);
			}
		} else {
			condicion = "";
			
			if(!"".equals(ic_epo)) {
				condicion += "    AND pe.ic_epo = ?";
				campoProveedor = "pe.cg_pyme_epo_interno"; //Geag
			} else {
				//Si la EPO es nula, entonces el numero de proveedor no tiene sentido
				//por lo cual se pone un valor fijo para poder hacer un distinct
				campoProveedor = "0"; //Geag
			}
			if(!"".equals(num_pyme))
				condicion += "    AND cg_pyme_epo_interno LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
			if(!"".equals(rfc_pyme))
				condicion += "    AND cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
			if(!"".equals(nombre_pyme))
				condicion += "    AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
			if(!("2".equals(ic_producto_nafin)||"5".equals(ic_producto_nafin)) && !pantalla.equals("listReq") && !pantalla.equals("consSinNumProv")) {
				condicionDoc = 
					"    AND d.ic_pyme = pe.ic_pyme"   +
					"    AND d.ic_epo = pe.ic_epo";
				tablaDoc = "com_documento d,";
			}
			
			if( pantalla.equals("MantenimientoDoctos") || pantalla.equals("InformacionDocumentos") || pantalla.equals("CambioEstatus") ) {
				condicionDoc = 
					"		AND PE.cs_habilitado 			= 	'S' " +
					"		AND P.cs_habilitado 				= 	'S' " +
					"		AND P.cs_invalido 				!= 'S' " +
					"		AND d.ic_pyme 	= pe.ic_pyme			 " +
					"		AND pe.cs_num_proveedor 		= 'N'  " +  // Tiene CG_PYME_EPO_INTERNO
					"		AND d.ic_epo 	= pe.ic_epo";
				tablaDoc = "com_documento d,";
			}
			
			qrySentencia = 
				" SELECT /*+index(pe CP_COMREL_PYME_EPO_PK) index(p CP_COMCAT_PYME_PK) use_nl(pe d p)*/ " +
//				"        p.ic_pyme, pe.cg_pyme_epo_interno, p.cg_razon_social," +
				"        distinct p.ic_pyme, " + campoProveedor + " as cg_pyme_epo_interno, p.cg_razon_social AS DESCRIPCION," + //GEAG
				"        crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega," +
				"        crn.ic_nafin_electronico AS CLAVE" +
				"   FROM comrel_pyme_epo pe, "+tablaDoc+"comrel_nafin crn, comcat_pyme p" +
				"  WHERE p.ic_pyme = pe.ic_pyme" +
				"    AND p.ic_pyme = crn.ic_epo_pyme_if" +
				condicionDoc+
				"    AND crn.cg_tipo = 'P' "+condicion;
				if(!pantalla.equals("listReq") && !pantalla.equals("consSinNumProv") && !pantalla.equals("MantenimientoDoctos") && !pantalla.equals("InformacionDocumentos") && !pantalla.equals("CambioEstatus") ){
				     qrySentencia += "    AND pe.cs_habilitado = 'S'"+
				     		"    AND p.cs_habilitado = 'S'"+
				     		"    AND pe.cg_pyme_epo_interno IS NOT NULL ";
				}
				if(pantalla.equals("consSinNumProv")){
					qrySentencia += 
				     		" AND pe.cg_pyme_epo_interno IS NULL "+
				     		" AND pe.cs_num_proveedor = 'S'";
				}
				
				if(!ic_if.equals("")){
				qrySentencia +=  
					" AND p.ic_pyme in ( select "  +
					"	      distinct "  +
					"		      cuenta.ic_pyme "  + 
					"     from  "  +
					"	      comrel_pyme_if rel, "  +
					"	      comrel_cuenta_bancaria cuenta "  +
					"     where "  +
					"	      rel.cs_vobo_if = 'S' and "  + 
					"	      rel.cs_borrado = 'N' and "  +
					"	      rel.ic_if      =  ?  and "  +
					"	      rel.ic_cuenta_bancaria = cuenta.ic_cuenta_bancaria and "  +
					"	cuenta.cs_borrado = 'N' ) ";
				}
				qrySentencia += 
				
				"  GROUP BY p.ic_pyme," +
				"           pe.cg_pyme_epo_interno," +
				"           p.cg_razon_social," +
				"           RPAD (pe.cg_pyme_epo_interno, 10, ' ') || p.cg_razon_social," +
				"           crn.ic_nafin_electronico" +
				"  ORDER BY p.cg_razon_social"  ;
			//System.err.println("\n qrySentencia = < "+qrySentencia + ">"); // Debug info
			System.out.println(qrySentencia);
			System.err.println("Query: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			int cont = 0;
			if(!"".equals(ic_epo)) {
				cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
			}
			if(!"".equals(num_pyme)) {
				cont++;ps.setString(cont,num_pyme);
			}
			if(!"".equals(rfc_pyme)) {
				cont++;ps.setString(cont,rfc_pyme);
			}
			if(!"".equals(nombre_pyme)) {
				cont++;ps.setString(cont,nombre_pyme);
			}
			if(!"".equals(ic_if)) {
				cont++;ps.setString(cont,ic_if);
			}
		} // fin else
		rs = ps.executeQuery();
		while(rs.next()) {
		hash = new HashMap();
		hash.put("clave",rs.getString(5));
		hash.put("descripcion",rs.getString(5)+"   "+rs.getString(3));
		vecRows.add(hash);
		/*
			vecCols = new Vector();
			vecCols.addElement(rs.getString(1));
			vecCols.addElement(rs.getString(2));
			vecCols.addElement(rs.getString(3));
			vecCols.addElement(rs.getString(4));
			vecCols.addElement(rs.getString(5));
			vecRows.addElement(vecCols);*/
		}
		rs.close();
		if(ps!=null) ps.close();
	} catch(Exception e){
		System.out.println("getProveedores:: Exception "+e);
		e.printStackTrace();
	} finally{
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
		System.out.println("\n getProveedores::(S) ");
	}
	return vecRows;
}
%>