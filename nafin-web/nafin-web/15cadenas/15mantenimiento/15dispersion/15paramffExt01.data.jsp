<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.procesos.*,
		com.netro.dispersion.*,
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String	comboEpo 		= (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
	String	comboIF	 	= (request.getParameter("comboIF")==null)?"":request.getParameter("comboIF");
	String	viaLiquidacionD 		= (request.getParameter("liquidacion")==null)?"":request.getParameter("liquidacion");
	String	tipoDetalle 		= (request.getParameter("tipoDetalle")==null)?"":request.getParameter("tipoDetalle");
	String	desc_epo 		= (request.getParameter("desc_epo")==null)?"":request.getParameter("desc_epo");
	String	desc_if	 	= (request.getParameter("desc_if")==null)?"":request.getParameter("desc_if");
	String	via_liquidacion 		= (request.getParameter("via_liquidacion")==null)?"":request.getParameter("via_liquidacion");
	String	claveIf	 	= (request.getParameter("claveIf")==null)?"":request.getParameter("claveIf");
	String infoRegresar	=	"", consulta = "";  
	String mensaje			= "";
	
	ManejoServicio servicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	JSONObject jsonObj = new JSONObject();
	CQueryHelperRegExtJS queryHelper = null;
		ConsParamFF paginador = new ConsParamFF();
		Registros reg=null;
	
	if(informacion.equals("catalogoIF") ) {
		CatalogoIF cat = new CatalogoIF();
		cat.setCampoClave("I.ic_if");
		cat.setCampoDescripcion("I.cg_razon_social");
		cat.setOrden("I.cg_razon_social");
		List lista = cat.getListaElementosGral();
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(lista);
		infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
	
	}else if(informacion.equals("catalogoEPO") ) {
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		if(!claveIf.equals("")){
			cat.setClaveIf(claveIf);
		}
		List lista = cat.getListaElementos();
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(lista);
		infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
	}else if(informacion.equals("consultaInicial") ) {
	//	CQueryHelperRegExtJS queryHelper;
	//	ConsParamFF paginador = new ConsParamFF();
		queryHelper = new CQueryHelperRegExtJS(paginador);
		paginador.setTipoConsulta(informacion);
	//	Registros reg;
		if(informacion.equals("consultaInicial")){
			reg = queryHelper.doSearch();
			int verIf =0;
			int verEpo =0;
			verIf = paginador.refVerificaIf();
			verEpo = paginador.refVerificaEpo();
			while(reg.next()){
				String auxDescripcion = reg.getString("CD_DESCRIPCION").toString();
				reg.setObject("dist_epo","T");
				reg.setObject("dist_if","T");
				if (verIf >= 1 && auxDescripcion.equals("IF") ){
					reg.setObject("CG_TIPO","E");
					reg.setObject("GENERICA","N");
					reg.setObject("ESPECIFICA","S");
					reg.setObject("AUX_GENERICA","");
					reg.setObject("AUX_ESPECIFICA","checked");
					reg.setObject("dist_if","F");
				}
				if (verEpo >= 1  && auxDescripcion.equals("EPO") ){
					reg.setObject("CG_TIPO","E");
					reg.setObject("GENERICA","N");
					reg.setObject("ESPECIFICA","S");
					reg.setObject("AUX_GENERICA","");
					reg.setObject("AUX_ESPECIFICA","checked");
					reg.setObject("dist_epo","F");
				}
			}
			infoRegresar	=		"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}
		
	}else if(informacion.equals("guardarCambios")){
		String numeroRegistros  =(request.getParameter("numeroRegistros")!=null) ? request.getParameter("numeroRegistros"):"";
		int numRegistros = Integer.parseInt(numeroRegistros);
		String tipo[] = request.getParameterValues("tipo");
		String viaLiquidacion[] = request.getParameterValues("via_liquidacion");
		
		String Generica1[] = request.getParameterValues("Generica1");
		String Especifica1[] = request.getParameterValues("Especifica1");
		String tef1[] = request.getParameterValues("tef1");
		String ambas1[] = request.getParameterValues("ambas1");
		String spei1[] = request.getParameterValues("spei1");
		for(int i = 0; i<numRegistros; i++){
			if(Generica1[i].equals("S") || Especifica1[i].equals("S") ){
				if(Generica1[i].equals("S")){
					tipo[i] = "G";
				}else{
					tipo[i] = "E";
				}
			}
			if(tef1[i].equals("S") || ambas1[i].equals("S") || spei1[i].equals("S")){
				if(tef1[i].equals("S")){
					viaLiquidacion[i] = "T";
				}else if(ambas1[i].equals("S")){
					viaLiquidacion[i] = "A";
				}else if(spei1[i].equals("S")){
					viaLiquidacion[i] = "S";
				}
			}
		}
		int	regi = dispersion.setParamGralesFF(tipo[0],viaLiquidacion[0],tipo[1],viaLiquidacion[1],tipo[2],viaLiquidacion[2]);
		if (regi>0)
			mensaje = "Parametros actualizados satisfactoriamente.";
		else
			mensaje = "Hubo problemas para actualizar los parame_seguridadHome";	
		infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + mensaje + "\"}";
		
	}else if(informacion.equals("GuardaDetalle")){
		int numReg = dispersion.setParamGralesDetalle(tipoDetalle,comboIF,comboEpo,viaLiquidacionD);
		if (numReg>0)
			mensaje = "Parametros actualizados satisfactoriamente.";
		else
			mensaje = "Hubo problemas para actualizar los parametros.";
		infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + mensaje + "\"}";
	}else if(informacion.equals("eliminarDetalle")){
		int numReg = dispersion.setParamDetalleEliminar(tipoDetalle,desc_if,desc_epo,via_liquidacion);
		if (numReg>0)
			mensaje = "Registro eliminado";
		else
			mensaje = "Hubo problemas para actualizar los parametros.";
		infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + mensaje + "\"}";
	
	}else if(informacion.equals("ConsultarDetalle") ||  informacion.equals("ArchivoPDF")) {
		
		paginador.setTipoConsulta(informacion);
		paginador.setClaveIf(comboIF);
		paginador.setClaveEpo(comboEpo);
		paginador.setViaLiquidacion(viaLiquidacionD);
		paginador.setTipoDispercion(tipoDetalle);
		queryHelper = new CQueryHelperRegExtJS(paginador);
		if(informacion.equals("ConsultarDetalle")){
			reg = queryHelper.doSearch();
			infoRegresar	=		"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}else if(informacion.equals("ArchivoPDF") ){
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error son demasiados registros", e);
			}
		}
	}
%>
<%=infoRegresar%>



