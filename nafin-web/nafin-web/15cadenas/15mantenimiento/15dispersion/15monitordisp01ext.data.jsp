<%@ page 
	contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("MonitorDispersion.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// Remover atributos que pudieran haber quedado en sesión
	session.removeAttribute( "lReportes"             );
	session.removeAttribute( "lReportes.registros"   );
	session.removeAttribute( "lReportes.summaryData" );
			
	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if(  informacion.equals("CatalogoTipoMonitoreo")            )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Crear Catalogo	
	JSONArray  registros = new JSONArray();
	JSONObject registro	= null;
	
	registro = new JSONObject();
	registro.put("clave",			"E");
	registro.put("descripcion",	"EPO's");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"F");
	registro.put("descripcion",	"IF's");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"FF");
	registro.put("descripcion",	"IF's FIDEICOMISO");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"I");
	registro.put("descripcion",	"INFONAVIT");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"P");
	registro.put("descripcion",	"PEMEX-REF");
	registros.add( JSONObject.fromObject(registro) );
	
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)				);
	resultado.put("total",    	new Integer(registros.size()) );
	resultado.put("registros",	registros			 				);
	
	infoRegresar = resultado.toString();
 
} else if (        informacion.equals("MonitorDispersion.mostrarPantallaMonitoreo") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	String tipoMonitoreo = (request.getParameter("tipoMonitoreo")		== null)?"":request.getParameter("tipoMonitoreo");
	
	// Determinar el estado siguiente
	if(        "E".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispe01ext.jsp");
	} else if( "F".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispi01ext.jsp");
	} else if( "I".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispinfo01ext.jsp");
	} else if( "P".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispPEMEX01ext.jsp");
	} else if( "FF".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispf01ext.jsp");
	} else {
		estadoSiguiente = "ESPERAR_DECISION";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>