<%@ page language="java" %>
<%@ page import="
	java.util.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"

%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<% 

/*** OBJETOS ***/
CQueryHelperRegExtJS queryHelper;
CatalogoSimple cat;
JSONObject jsonObj;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String txt_fecha_pago   = (request.getParameter("txt_fecha_pago")==null)?"":request.getParameter("txt_fecha_pago");
String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String ic_epo				= (request.getParameter("idIc_epo")==null)?"":request.getParameter("idIc_epo");
String resultado			= null;
int start					= 0;
int limit 					= 0;
/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/

String ic_if = iNoCliente;

/*** INICIO CATALOGO EPO ***/
if(informacion.equals("catalogoEPO")){	
	CatalogoEpoDisp catalogo = new CatalogoEpoDisp();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setIc_if(ic_if);
	resultado = catalogo.getJSONElementos();		
} /*** FIN EPO ***/

/*** INICIO CONSULTA ***/
else if(informacion.equals("Consulta") || informacion.equals("ConsultaTotales")) { 
	String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
	
	try {
		start = Integer.parseInt((request.getParameter("start")==null)?"0":request.getParameter("start"));
		limit = Integer.parseInt((request.getParameter("limit")==null)?"15":request.getParameter("limit"));				
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	com.netro.cadenas.RepDispersion paginador = new com.netro.cadenas.RepDispersion();  
	paginador.setIc_epo(ic_epo);
	paginador.setDesde(txt_fecha_pago);
	paginador.setIc_if(ic_if);	
	queryHelper	= new CQueryHelperRegExtJS( paginador ); 
	
	if((informacion.equals("Consulta") && operacion.equals("Generar")) || (informacion.equals("Consulta") && operacion.equals(""))){ 
		try {
			if(operacion.equals("Generar")){
				queryHelper.executePKQuery(request);
			}
			String cadena = queryHelper.getJSONPageResultSet(request,start,limit);			
			resultado	= cadena;
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if(operacion.equals("XLS")||operacion.equals("PDF")){
		jsonObj = new JSONObject();
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,operacion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
		
	}else if(informacion.equals("ConsultaTotales")){
		Registros registros = queryHelper.doSearch();
		HashMap	datos=new HashMap();
		List reg=new ArrayList();
		float montoTransferencia = 0;
		int totaloperaciones = 0;
		
		while(registros.next()){
			//if(totaloperaciones>0){
				montoTransferencia += Float.valueOf(registros.getString("montocheque").replaceAll(" ","")).floatValue();
			//}
			totaloperaciones++;
		}
		//totaloperaciones = totaloperaciones>0 ? totaloperaciones-1 : totaloperaciones;
		datos.put("MONTOTRANSFERENCIA",Float.toString(montoTransferencia));
		datos.put("TOTALOEPERACIONES",Integer.toString(totaloperaciones));
		reg.add(datos);
		
		String resultado2 = "{\"success\": true, \"total\": \"" + reg.size() + "\", \"registros\": " + reg.toString()+"}";
		jsonObj = JSONObject.fromObject(resultado2);	
		resultado = jsonObj.toString();	
	}
} /*** FIN DE CONSULTA ***/

%>
<%= resultado%>
