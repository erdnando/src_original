<%@ page 
	contentType=
		"application/json;charset=UTF-8" 
	import="
		net.sf.json.JSONArray,net.sf.json.JSONObject, netropology.utilerias.*, com.netro.dispersion.*, 
		java.math.BigDecimal, java.util.*, javax.naming.*, com.netro.model.catalogos.* " 
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%    
String tipoDispersion 		 = (request.getParameter("_cmbtd") 			!=null)	?	request.getParameter("_cmbtd")		:	"";
String noEpo					 = (request.getParameter("_cmb_epo") 		!=null)	?	request.getParameter("_cmb_epo")		:	"";
String noIf						 = (request.getParameter("_cmb_if") 		!=null)	?	request.getParameter("_cmb_if")		:	"";
String fechaRegIni			 = (request.getParameter("_fechaReg")		!=null)	?	request.getParameter("_fechaReg")	:	"";
String fechaRegFin 			 = (request.getParameter("_fechaFinReg") 	!=null)	?	request.getParameter("_fechaFinReg"):	"";
String fechaCancelacionIni  = (request.getParameter("_fechaCan") 		!=null)	?	request.getParameter("_fechaCan")	:	"";
String fechaCancelacionFin  = (request.getParameter("_fechaFinCan") 	!=null)	?	request.getParameter("_fechaFinCan"):	"";
String informacion			 = (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String infoRegresar			 = "";

if (informacion.equals("catalogoepo") )	{
	JSONObject 					jsonObj 	= new JSONObject();
	CatalogoEpoDispersion 	catEpo 	= new CatalogoEpoDispersion();
	catEpo.setCampoClave("ic_epo");
	catEpo.setCampoDescripcion("cg_razon_social");
	List lis = catEpo.getListaElementos(); 
	jsonObj.put("registros", lis);	
  infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("catalogoif")){	
	JSONObject 					jsonObj 	= new JSONObject();
	CatalogoIfDispersion 	catIf 	= new CatalogoIfDispersion();
	catIf.setCampoClave("ic_if");
	catIf.setCampoDescripcion("cg_razon_social");
	List lis = catIf.getListaElementos();
	jsonObj.put("registros", lis);	
  infoRegresar = jsonObj.toString();
	
} else if (informacion.equals("consultaGeneral") || informacion.equals("GeneraArchivoCSV") ){
 
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	StringBuffer 				contenidoArchivo 	= new StringBuffer();
	CreaArchivo 				archivo 				= new CreaArchivo();
	JSONObject 					jsonObj 				= new JSONObject();
	HashMap 						datos 				= new HashMap();
	List 							info 					= new ArrayList();	
	List 							registros			= new ArrayList();
	Iterator 					it 					= null;
	String  epo="", nif="", nombrePyme="", rfc="", banco="", tipocuenta="", nocuenta ="", importe="",  causa="", usuario="", 
				fcancelacion="",motivoRechazo="",verDetalle="";


	try {
		info=dispersion.consultaBitacoraCan(tipoDispersion,noEpo,noIf,fechaRegIni,fechaRegFin,fechaCancelacionIni,fechaCancelacionFin);
		it = info.iterator();
		
		while(it.hasNext()) {
			List 	campos 	= (List) it.next();
			datos 			= new HashMap();
			epo 				= (String) campos.get(0);
			nif 				= (String) campos.get(1);
			nombrePyme 		= (String) campos.get(2);
			rfc 				= (String) campos.get(3);
			banco 			= (String) campos.get(4);
			tipocuenta 		= (String) campos.get(5);
			nocuenta 		= (String) campos.get(6);
			importe			= (String) campos.get(7);
			causa 			= (String) campos.get(8);
			usuario 			= (String) campos.get(9);
			fcancelacion 	= (String) campos.get(10);			
			motivoRechazo 	= (String) campos.get(13);
			verDetalle 		= "Detalle depurado por proceso interno ";		 
			datos.put		("nombreepo"	, epo 		);
			datos.put		("nombreIf"		, nif 		);
			datos.put		("nombrePyme"	, nombrePyme);
			datos.put		("rfc"			, rfc 		);
			datos.put		("banco"			, banco 		);
			datos.put		("tipocuenta"	, tipocuenta);
			datos.put		("nocuenta"		, nocuenta	);
			datos.put		("importe"		, importe	);
			datos.put		("causa"			, causa 		);
			datos.put		("usuario"		, usuario 	);
			datos.put		("fcancelacion", fcancelacion);			
			datos.put		("motivoRechazo", motivoRechazo);
			datos.put		("verDetalle"	, verDetalle );	
			registros.add	(datos);			
			}
		jsonObj.put("success"	,  new Boolean(true)); 
		jsonObj.put("registros"	, registros);
	//	infoRegresar = jsonObj.toString();
		
		if(informacion.equals("GeneraArchivoCSV")){
    BigDecimal newc = new BigDecimal("0");
    BigDecimal suma = new BigDecimal("0");
    BigDecimal res  = new BigDecimal("0");

				
		contenidoArchivo.append("EPO"+","+"IF"+","+"Pyme"+","+"RFC"+","+"Banco"+","+"Tipo de Cuenta"+","+"No Cuenta"+
				","+"Importe"+","+"Motivo de rechazo"+","+"Ver Detalle"+","+"Usuario"+","+"Fecha de Cancelacion"+","+"Causa"+"\n");
				
			try {
				info	=dispersion.consultaBitacoraCan(tipoDispersion,noEpo,noIf,fechaRegIni,fechaRegFin,fechaCancelacionIni,fechaCancelacionFin);
				it 	= info.iterator();
				
				while(it.hasNext()) {
					List campos 	= (List) it.next();
					epo 				= (String) campos.get(0);
					nif 				= (String) campos.get(1);
					nombrePyme 		= (String) campos.get(2);
					rfc 				= (String) campos.get(3);
					banco 			= (String) campos.get(4);
					tipocuenta 		= (String) campos.get(5);
          try {
            nocuenta 		= (String) campos.get(6);
            if (nocuenta.equals("")){
              nocuenta = " ";
              }
          } catch (Exception e){
            nocuenta = " ";
          }
					importe 			= (String) campos.get(7);
					causa 			= (String) campos.get(8);
					usuario 			= (String) campos.get(9);
					fcancelacion	= (String) campos.get(10);			
					motivoRechazo 	= (String) campos.get(13);
					verDetalle 		= "Detalle depurado por proceso interno "; 
					
				// newc= new BigDecimal(nocuenta);
				// res=suma.add(newc);
				 //BigDecimal res  = new BigDecimal("0");
         
					contenidoArchivo.append(epo.replace				(',',' ')+",");
					contenidoArchivo.append(nif.replace				(',',' ')+",");
					contenidoArchivo.append(nombrePyme.replace	(',',' ')+",");
					contenidoArchivo.append(rfc.replace				(',',' ')+",");
					contenidoArchivo.append(banco.replace			(',',' ')+",");
					contenidoArchivo.append(tipocuenta.replace	(',',' ')+",");
					//contenidoArchivo.append("\"\"\""+res+"\"\"\"");
          contenidoArchivo.append("\'"+nocuenta);
					contenidoArchivo.append(",");
					contenidoArchivo.append(importe.replace		(',',' ')+",");
					contenidoArchivo.append(motivoRechazo.replace(',',' ')+",");
					contenidoArchivo.append(verDetalle.replace	(',',' ')+",");
					contenidoArchivo.append(usuario.replace		(',',' ')+",");
					contenidoArchivo.append(fcancelacion.replace	(',',' ')+",");
					contenidoArchivo.append(causa.replace			(',',' ')+",");
					contenidoArchivo.append("\n");
          
					}//fin while
			System.err.println("Prepared file");
			boolean crear =	archivo.make(contenidoArchivo.toString(),strDirectorioTemp,".csv");
			jsonObj.put("success"	,  new Boolean(true)); 
			jsonObj.put("urlArchivo", strDirecVirtualTemp+archivo.nombre);		
			//infoRegresar = jsonObj.toString();
      System.err.println("infoRegresar: "+infoRegresar);
      
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}	
		}
	} catch (Exception e){
     System.err.println("Error al generar el archivo CSV :: AppException "+e);
	}	
  	infoRegresar = jsonObj.toString(); System.err.println("info: "+infoRegresar);
}
%>
<%=    infoRegresar%>