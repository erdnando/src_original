<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<% if( esEsquemaExtJS ){ %>
	<%@ include file="/01principal/menu.jspf"%>
<% } %>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/MsgCheckColumn.js"></script>
<script type="text/javascript" src="15consultaff01ext.js?<%=session.getId()%>"></script>
<script language="JavaScript1.2" src="<%=appWebContextRoot%>/00utils/valida.js?<%=session.getId()%>"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<% } %>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
	<% } %>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
	<% } %>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>