<%@ page 
	contentType=
		"application/json;charset=UTF-8" 
	import="
		net.sf.json.JSONArray,net.sf.json.JSONObject, netropology.utilerias.*, com.netro.dispersion.*, 
		java.util.*, javax.naming.*, com.netro.model.catalogos.*, org.apache.commons.logging.Log " 
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<% 

String guardarMensaje	= (request.getParameter("guardarMensaje")	!=null)  ?  request.getParameter("guardarMensaje") :"";
String mensaje				= (request.getParameter("mensaje") 			!=null)	?	request.getParameter("mensaje")		   :"";
String informacion		= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String infoRegresar		= "";
boolean	datosGuardados;

if (informacion.equals("vistaPrevia")){
	
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	
	if(mensaje.length() > 3072){
		mensaje = mensaje.substring(0,3072);
	}
		datosGuardados = false;

	String 	mensaje_correo	= (request.getParameter("mensaje")	== null)?"":request.getParameter("mensaje");
	
	String 				codigoHTML 	= "";
	TraductorDeCorreo traductor 	= new TraductorDeCorreo();
	ArrayList			lista 		= new ArrayList();
	ArrayList			listaUSD		= new ArrayList();
	HashMap				registro		= new HashMap();
	HashMap				registroUSD	= new HashMap();
	
	registro.put("INTERMEDIARIO",	"BANCO1");
	registro.put("MONTO",			"$ 224,555.82");
	lista.add(registro);
	traductor.setTotalIntermediariosConOperacion("$ 224,555.82");
	
	registroUSD.put("INTERMEDIARIO",	"BANCO2");
	registroUSD.put("MONTO",			"$ 54,261.12");
	listaUSD.add(registroUSD);
	traductor.setTotalIntermediariosConOperacionUSD("$ 54,261.12");
	
	traductor.setRazonSocialEPO("CADENA PRODUCTIVA");
	traductor.setListaDeIntermediariosConOperacion(lista);
	traductor.setMontoDocumentosNoOperados("$59,356.02");
	traductor.setTotalADispersar("$283,911.84");
	traductor.setFechaDeVencimiento("27 de mayo de 2008");
	traductor.setFechaADepositar("26 de mayo de 2008");
	traductor.setTotalNoDispersado("$9,747.75");
	
	traductor.setListaDeIntermediariosConOperacionUSD(listaUSD);
	traductor.setMontoDocumentosNoOperadosUSD("$4,791.02");
	traductor.setTotalADispersarUSD("$33,761.14");
	traductor.setTotalNoDispersadoUSD("$7,752.23");
	
	codigoHTML 	= traductor.convierteEnCodigoHTML(mensaje_correo);
	
	String salto = codigoHTML.replaceAll("\n", " ");
	String retorno = salto.replaceAll("\r", " ");
	String tilde = retorno.replaceAll("'", "\"");
	infoRegresar = tilde;

} else if(informacion.equals("guardar")){
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	
	if(mensaje.length() > 3072){
		mensaje = mensaje.substring(0,3072);
	}
	
	if(guardarMensaje.equals("true")){
		dispersion.setMensajeDedispersion(mensaje);
		datosGuardados = true;
		log.debug("datosGuardados : "+datosGuardados);
	
	mensaje = dispersion.getMensajeDedispersion();
	mensaje = escapaComillasDobles(mensaje);
	mensaje = mensaje.replaceAll("\r\n","\\\\n"); 
	}
} else if(informacion.equals("obtener msj")){
	JSONObject 					jsonObj 				= new JSONObject();
	Dispersion 		dispersion 	= ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
	
	String 	msj;
	msj = dispersion.getMensajeDedispersion();
	if(msj == null || msj.equals("") ){
	msj =	"[1]"  +
			"\n"  +
			"Les informo sobre el detalle de los montos a dispersar derivados de la operación de los proveedores "  +
			"del [1] en el sistema de Nafin Electrónico."  +
			"\n"  +
			"Con Intermediarios Financieros M.N.:\n"  +
			"\n"  +
			"[2]\n"  +
			"[3]\n"  +
			"\n"  +
			"Con Intermediarios Financieros USD:\n"  +
			"\n"  +
			"[4]\n"  +
			"[5]\n"  +
			"\n"  +
			"De la parte correspondiente de los Documentos No operados el importe a dispersar es\n"  +
			"\n"  +
			"[6] M.N.\n"  +
			"[7] USD\n"  +
			"\n"  +
			"en un total de documentos correspondientes a proveedores.\n"  +
			"\n"  +
			"El importe a dispersar para el día [10] es por:\n"  +
			"\n"  +
			"[8] M.N.\n"  +
			"[9] USD\n"  +
			"\n"  +
			"Que deberá estar depositado el día [11] en la cuenta especificada en el addendum al contrato de "  +
			"Cadenas Productivas para el servicio de dispersión. La leyenda con la que deberá llegar la "  +
			"transferencia o depósito es: CADENAPRODUCTIVA-DP [10].\n"  +
			"\n"  +
			"Total del pago a proveedores que NO se dispersará (detalle se muestra en el archivo anexo):\n"  +
			"\n"  +
			"[12] M.N.\n"  +
			"[13] USD\n"  +
			"\n"  +
			"Saludos cordiales\n"  +
			"\n"  +
			"Dirección de Canales Alternos\n"  +
			"Nacional Financiera, S.N.C.\n"  +
			"Tel.(0155) 5325 6000 Ext. 8622\n"  +
			"Fax (0155) 5325 6677 Ext. 8622\n"  +
			"2008, Año de la Educación Física y el Deporte ";
	}
	
	jsonObj.put("success"	,  new Boolean(true)); 
	jsonObj.put("msj"	, msj);
	infoRegresar = jsonObj.toString();
	log.debug("msj: "+ infoRegresar);
}
%>
<%=infoRegresar%>
<%!
	public String escapaComillasDobles(String mensaje){
		
		if(mensaje == null || mensaje.trim().equals("")){
			return "";
		}
		
		StringBuffer buffer=new StringBuffer();
		for(int cursor=0;cursor<mensaje.length();cursor++){
			char caracter = mensaje.charAt(cursor);
			if(caracter == '"'){
				buffer.append("\\");
			}
			buffer.append(caracter);
		}
		System.err.println("buffer.toString() = <"+buffer.toString()+">");
		return buffer.toString();
	}
%>