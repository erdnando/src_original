<%@ page 
	contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoEPO,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		com.netro.descuento.*,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		netropology.utilerias.ElementoCatalogo,
		java.sql.PreparedStatement,
		java.sql.ResultSet,
		java.math.BigDecimal"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("MonitorDispersion.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if(  informacion.equals("CatalogoTipoMonitoreo")            )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Crear Catalogo	
	JSONArray  registros = new JSONArray();
	JSONObject registro	= null;
	
	registro = new JSONObject();
	registro.put("clave",			"E");
	registro.put("descripcion",	"EPO's");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"F");
	registro.put("descripcion",	"IF's");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"FF");
	registro.put("descripcion",	"IF's FIDEICOMISO");
	registros.add( JSONObject.fromObject(registro) );
		
	registro = new JSONObject();
	registro.put("clave",			"I");
	registro.put("descripcion",	"INFONAVIT");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"P");
	registro.put("descripcion",	"PEMEX-REF");
	registros.add( JSONObject.fromObject(registro) );
	
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)				);
	resultado.put("total",    	new Integer(registros.size()) );
	resultado.put("registros",	registros			 				);
	
	infoRegresar = resultado.toString();
 
} else if (        informacion.equals("MonitorDispersion.mostrarPantallaMonitoreo") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	String tipoMonitoreo = (request.getParameter("tipoMonitoreo")		== null)?"":request.getParameter("tipoMonitoreo");
	
	// Determinar el estado siguiente
	if(        "E".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispe01ext.jsp");
	} else if( "F".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispi01ext.jsp");
	} else if( "I".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispinfo01ext.jsp");
	} else if( "P".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispPEMEX01ext.jsp");
	} else if( "FF".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispf01ext.jsp");
	} else {
		estadoSiguiente = "ESPERAR_DECISION";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CatalogoEPO")            							){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");
	cat.setHabilitado("S"); 
	List listaElementos = cat.getListaElementosMonitorDispersionEPO();	
	
	// Agregar la opción todos
	ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
	elementoCatalogo.setClave("TODAS");
	elementoCatalogo.setDescripcion("TODAS");
	listaElementos.add(0,elementoCatalogo);
				
	infoRegresar      = cat.getJSONElementos(listaElementos);

} else if(  informacion.equals("ConsultaDetalleMonitoreoEPO") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// 1. Leer parametros de la consulta
	String 		cmb_tipo_monitor 	= ( request.getParameter("claveTipoMonitoreo")	== null)?"":request.getParameter("claveTipoMonitoreo");
	String 		ic_epo 				= ( request.getParameter("claveEPO") 				== null)?"":request.getParameter("claveEPO");
	String 		txt_fecha_ven_de 	= ( request.getParameter("fechaVencimientoDe")	== null)?"":request.getParameter("fechaVencimientoDe");
	String 		txt_fecha_ven_a 	= ( request.getParameter("fechaVencimientoA")	== null)?"":request.getParameter("fechaVencimientoA");
 
	ic_epo = "TODAS".equals(ic_epo)?"":ic_epo;
			
	JSONArray 	registros 					= new JSONArray();
	JSONObject	registro						= null;
	JSONObject	summaryData					= new JSONObject();
	JSONObject  groupCheckBoxData			= new JSONObject();
	boolean 		habilitarSeleccionEPO	= false;
		
	// 2. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
					
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
					
	} catch(Exception e) {
	 
		msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
			
		throw new AppException(msg);
	 
	}
	
	// 3. Obtener instancia de EJB de Seleccion Documento
	ISeleccionDocumento seleccionDocumento = null;
	try {
					
		seleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);
					
	} catch(Exception e) {
	 
		msg	= "Ocurrió un error al obtener instancia del EJB de Seleccion de Documentos";
		log.error(msg);
		e.printStackTrace();
			
		throw new AppException(msg);
	 
	}
	
	// Variables Auxiliares
	AccesoDB 			con 			 						= new AccesoDB();
	PreparedStatement ps 			 						= null;
	ResultSet			rs				 						= null;
	String 				qrySentencia 						= null;
	int					i 										= 0;
	String 				tipoColor 							= "formas";
	
	// Variables Auxiliares utilizadas para determinar si hay aun hay pendiende documentos de ser 
	// enviados al tablero de dispersion
	String 				claveEPOAnterior 					= "";
	String 				fechaVencimientoAnterior		= "";
			
	boolean 				hayDocumentosMXNOperados 		= false;
	boolean 				hayDocumentosMXNSinOperar 		= false;
	boolean 				hayDocumentosUSDOperados 		= false;
	boolean  			hayDocumentosUSDSinOperar 		= false;
			
	final int   		PESOS									= 1;
	final int 			DOLARES								= 54;
			
	final int 			OPERADA 								= 4; 
	final int 			OPERADA_PAGADA 					= 11;
	final int 			VENCIDO_SIN_OPERAR 				= 9;
	final int 			PAGADO_SIN_OPERAR 				= 10; 
						
	boolean				isFirstRecord						= true;		
	boolean				hasRecords							= false;
			
	boolean 				esRangoFechaVencimientoUnDia	= false;
	boolean 				todasLasEPO						 	= false;
	boolean				existenParametrosDeDispersion = false;
	
	try {
			
		// Conectarse a la Base de Datos
		con.conexionDB();
		
		// 4. Determinar si el rango de fechas de vencimiento a consultar abarca un solo día
		SimpleDateFormat 	dateFormat 	= new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		Calendar 			aux 			= dateFormat.getCalendar();
		
		dateFormat.parse(txt_fecha_ven_de); 	
		Calendar fechaVenDe = new GregorianCalendar(aux.get(Calendar.YEAR),aux.get(Calendar.MONTH),aux.get(Calendar.DAY_OF_MONTH));
 
		dateFormat.parse(txt_fecha_ven_a); 	
		Calendar fechaVenA  = new GregorianCalendar(aux.get(Calendar.YEAR),aux.get(Calendar.MONTH),aux.get(Calendar.DAY_OF_MONTH));
 
		esRangoFechaVencimientoUnDia 	= fechaVenA.equals(fechaVenDe)?true:false;

		// 5. Determinar si se seleccionaron todas las EPOs
		todasLasEPO						  	= "".equals(ic_epo)?true:false;
 
		existenParametrosDeDispersion = !todasLasEPO && esRangoFechaVencimientoUnDia?dispersion.existenParametrosDeDispersion(ic_epo):false;
		
		// 6. Habilitar seleccion de EPOs
		habilitarSeleccionEPO 					 = esRangoFechaVencimientoUnDia && todasLasEPO;
		
		// 7. Obtener la descripcion de las monedas
		String descripcionMonedaNacional 	 = dispersion.getDescripcionMoneda("1");
		String descripcionDolaresAmericanos  = dispersion.getDescripcionMoneda("54");
		
		// 8. Obtener query
		if("E".equals(cmb_tipo_monitor)) {

			qrySentencia = dispersion.getQueryMonitoreoDispersionPorEPO(
									ic_epo, 
									(!"".equals(ic_epo)?seleccionDocumento.operaFechaVencPyme(ic_epo):""), 
									txt_fecha_ven_de, 
									txt_fecha_ven_a
								);
			
		} 		
		log.debug("ConsultaDetalleMonitoreoEPO.qrySentencia = <" + qrySentencia+ ">");
		
		// 9.Realizar consulta
		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
					
		boolean 		existetabla 			= false;
		String 		groupIdAnterior		= "";
		String 		groupId					= "";
		int 			totalDocumentos 		= 0;
		BigDecimal 	totalMonto 				= new BigDecimal(0);
		int 			totalDocumentosUSD 	= 0;
		BigDecimal 	totalMontoUSD 			= new BigDecimal(0);
		List 			lReportes 				= new ArrayList();
		RepDispEpo 	repDispEpo 				= new RepDispEpo();
		int r=0;
		while(rs.next() && r<4700) {r++;
			
			// DETERMINAR QUE EPOS YA ENVIARON TODOS SUS GRUPOS DE DOCUMENTOS A FLUJO DE FONDOS.
			// SOLO PARA CUANDO SE HAN SELECCIONADOS TODAS LAS EPOS Y SE HA ESPECIFICADO UN RANGO DE 
			// FECHAS DE VENCIMIENTO DE UN SOLO DIA
			if( esRangoFechaVencimientoUnDia && todasLasEPO ){
				
				// Se agrega validacion de envío al tablero de dispersion de todas las operaciones
				String	claveEPO							= rs.getString(13) == null?"":rs.getString(13);
				int 		claveMoneda						= rs.getInt("ic_moneda");
				int 		claveEstatusDocumento		= rs.getInt("ic_estatus_docto");
				String 	fechaVencimiento				= rs.getString(6)  == null?"":rs.getString(6);
	 
				boolean	esEPONueva						= !claveEPO.equals(claveEPOAnterior)						 ?true:false;
				boolean 	evaluarEnvioFlujoFondos 	= !claveEPO.equals(claveEPOAnterior) && !isFirstRecord ?true:false; 
	 
				// Evaluar envio a flujo fondos de la EPO anterior
				if( evaluarEnvioFlujoFondos ){
					
					boolean flujoFondosCompletado = hayDocumentosMXNOperados || hayDocumentosMXNSinOperar || hayDocumentosUSDOperados || hayDocumentosUSDSinOperar;
 				
					if(        hayDocumentosMXNOperados  && !dispersion.seEnvioAFlujoFondos(claveEPOAnterior, "1",  fechaVencimientoAnterior, "OPR")   ){
						flujoFondosCompletado = false;
					} else if( hayDocumentosMXNSinOperar && !dispersion.seEnvioAFlujoFondos(claveEPOAnterior, "1",  fechaVencimientoAnterior, "SNOPR") ){
						flujoFondosCompletado = false;
					} else if( hayDocumentosUSDOperados  && !dispersion.seEnvioAFlujoFondos(claveEPOAnterior, "54", fechaVencimientoAnterior, "OPR")   ){
						flujoFondosCompletado = false;
					} else if( hayDocumentosUSDSinOperar && !dispersion.seEnvioAFlujoFondos(claveEPOAnterior, "54", fechaVencimientoAnterior, "SNOPR") ){
						flujoFondosCompletado = false;
					} 
					
					groupCheckBoxData.put(claveEPOAnterior+":"+fechaVencimientoAnterior,flujoFondosCompletado?"true;readonly":"false;normal");
					
				}
 
				// Resetear de busqueda de documentos
				if(esEPONueva){
					hayDocumentosMXNOperados 	= false;
					hayDocumentosMXNSinOperar 	= false;
					hayDocumentosUSDOperados 	= false;
					hayDocumentosUSDSinOperar 	= false;					
				}
				
				// Determinar tipo de documento encontrado
				switch( claveMoneda ){    
					case PESOS:
						if(        claveEstatusDocumento == OPERADA            || claveEstatusDocumento == OPERADA_PAGADA    ){
							hayDocumentosMXNOperados  = true;
						} else if( claveEstatusDocumento == VENCIDO_SIN_OPERAR || claveEstatusDocumento == PAGADO_SIN_OPERAR ){
							hayDocumentosMXNSinOperar = true;
						}
						break;
					case DOLARES:
						if(        claveEstatusDocumento == OPERADA            || claveEstatusDocumento == OPERADA_PAGADA    ){
							hayDocumentosUSDOperados  = true;
						} else if( claveEstatusDocumento == VENCIDO_SIN_OPERAR || claveEstatusDocumento == PAGADO_SIN_OPERAR ){
							hayDocumentosUSDSinOperar = true;
						}
						break;
				}
 
				// Guardar copia de la clave de la EPO anterior
				claveEPOAnterior 				= claveEPO;
				// Guardar copia de la fecha de vencimiento anterior
				fechaVencimientoAnterior 	= fechaVencimiento;
 
				isFirstRecord 					= false;
				hasRecords						= true;
				
			}
		
			// INICIO CONSULTA DE REGISTROS
			
			String 	rs_ic_epo							= rs.getString(13)			== null?"":rs.getString(13);
			String 	rs_ic_pyme							= rs.getString("ic_pyme")	== null?"":rs.getString("ic_pyme");
			String 	rs_epo								= rs.getString(1)				== null?"":rs.getString(1);
			String 	rs_fechaVenc						= rs.getString(6)				== null?"":rs.getString(6);
			String 	rs_cecoban							= rs.getString(11)			== null?"":rs.getString(11);
			String 	rs_ic_moneda						= rs.getString("ic_moneda")== null?"":rs.getString("ic_moneda");
			groupId											= rs_ic_epo+":"+rs_fechaVenc;
			
			boolean 	esCuentaEnMonedaNacional		= rs_ic_moneda.equals("1")	?true:false;
			boolean	esCuentaEnDolaresAmericanos 	= rs_ic_moneda.equals("54")?true:false;
			String 	nombreMoneda						= rs_ic_moneda.equals("1") ?descripcionMonedaNacional:(rs_ic_moneda.equals("54")?descripcionDolaresAmericanos:"");
 
			// Se detectó un nuevo grupo EPO, FECHA_VENCIMIENTO, por lo que se calcularán los totales del grupo anterior.
			if(!groupIdAnterior.equals(groupId)) {
				
				i 					 = 0;
				if(existetabla){
 
					// AGREGAR LOS TOTALES AL SUMMARYDATA
					JSONArray  summaryDataRegisters = new JSONArray();
					JSONObject summaryDataRegister  = null;
					
					summaryDataRegister  = new JSONObject();
					summaryDataRegister.put( "NUMERO_NAFIN_ELECTRONICO", 	"Total M.N."											);
					summaryDataRegister.put( "NUMERO_TOTAL_DOCUMENTOS",	String.valueOf(totalDocumentos)					);
					summaryDataRegister.put( "MONTO_TOTAL", 					Comunes.formatoMN(totalMonto.toPlainString())		);
					summaryDataRegister.put( "ESTATUS_CECOBAN", 				"99"														); // PARA QUE LOS TOTALES NO SE PINTEN EN ROJO
					summaryDataRegisters.add( summaryDataRegister );
					
					summaryDataRegister  = new JSONObject();
					summaryDataRegister.put( "NUMERO_NAFIN_ELECTRONICO",	"Total USD"												);
					summaryDataRegister.put( "NUMERO_TOTAL_DOCUMENTOS",   String.valueOf(totalDocumentosUSD)				);
					summaryDataRegister.put( "MONTO_TOTAL", 					Comunes.formatoMN(totalMontoUSD.toPlainString())	);
					summaryDataRegister.put( "ESTATUS_CECOBAN", 				"99"														); // PARA QUE LOS TOTALES NO SE PINTEN EN ROJO
					summaryDataRegisters.add( summaryDataRegister );
					
					summaryData.put(groupIdAnterior,summaryDataRegisters);
 
					totalDocumentos 		= 0;
					totalMonto 				= new BigDecimal(0);
					totalDocumentosUSD 	= 0;
					totalMontoUSD 			= new BigDecimal(0);
					
					lReportes.add(repDispEpo);
					
				}
				
			}
			
			// Es el primer registro del grupo EPO, Fecha Vencimiento
			if( i == 0 ){
				
				repDispEpo 	= new RepDispEpo( rs_ic_epo, rs_epo, rs_fechaVenc, rs_cecoban );
				existetabla = true;
 	
			}

			String rs_nafElect		= rs.getString(2)==null?"":rs.getString(2);
			String rs_pyme				= rs.getString(3)==null?"":rs.getString(3);
			String rs_if				= rs.getString(4)==null?"":rs.getString(4);
			String rs_estatus			= rs.getString(5)==null?"":rs.getString(5);
			String rs_totalDocs		= rs.getString(7)==null?"":rs.getString(7);
			String rs_montoDocs		= rs.getString(8)==null?"":rs.getString(8);
			String rs_banco			= rs.getString(9)==null?"":rs.getString(9);
			String rs_cuenta			= rs.getString(10)==null?"":rs.getString(10);
			String rs_descCec			= rs.getString(12)==null?"":rs.getString(12);
			String ic_estatus_docto = rs.getString("ic_estatus_docto") == null?"":rs.getString("ic_estatus_docto");

			if(esCuentaEnMonedaNacional){
				totalDocumentos 		+= Integer.parseInt(rs_totalDocs);
				totalMonto 				=  totalMonto.add(new BigDecimal(rs_montoDocs));
			}
			if(esCuentaEnDolaresAmericanos){
				totalDocumentosUSD 	+= Integer.parseInt(rs_totalDocs);
				totalMontoUSD 			=  totalMontoUSD.add(new BigDecimal(rs_montoDocs));
			}
			
			if(!"99".equals(rs_cecoban))
				tipoColor = "cuenta";
			else
				tipoColor = "formas";
			if("0".equals(rs_cuenta)){
				rs_banco	= "";
				rs_cuenta	= "";
				rs_cecoban	= "";
				rs_descCec	= "";
			}
			
			List lDatos		= new ArrayList();
			List lNombres	= new ArrayList();
			
			lDatos.add(rs_totalDocs);
			lDatos.add(rs_montoDocs);
			lDatos.add(rs_ic_pyme);
			lDatos.add(rs_cecoban);
			lDatos.add(rs_ic_moneda);
			lDatos.add(ic_estatus_docto);
			
			lNombres.add("Num. Total Doctos");
			lNombres.add("Monto Total");
			lNombres.add("ic_pyme");
			lNombres.add("rs_cecoban");
			lNombres.add("rs_ic_moneda");
			lNombres.add("ic_estatus_docto");

			if(!"".equals(rs_pyme)) {
				lDatos.add(0, rs_pyme);
				lNombres.add(0, "PYME");
				if("99".equals(rs_cecoban)) {
					repDispEpo.addRepPymesConDisp(lDatos);
					repDispEpo.setNomRepPymesConDisp(lNombres);
				} else {
					repDispEpo.addRepPymesSinDisp(lDatos);
					repDispEpo.setNomRepPymesSinDisp(lNombres);
				}
			} else if(!"".equals(rs_if)) {
				lDatos.add(0, rs_if);
				lNombres.add(0, "IF");
				repDispEpo.addRepPagoIf(lDatos);
				repDispEpo.setNomRepPagoIf(lNombres);
			}

			// AGREGAR REGISTROS ASOCIADOS AL GRUPO EPO, Fecha de Vencimiento
			registro = new JSONObject();
			registro.put("GROUP_ID",						groupId													); // GROUP_ID
			registro.put("IC_EPO",							rs_ic_epo												); // IC_EPO
			registro.put("NOMBRE_EPO",						rs_epo													); // NOMBRE_EPO
			registro.put("NUMERO_NAFIN_ELECTRONICO",	rs_nafElect												); // Num. Nafin Electrónico
			registro.put("NOMBRE_IF",						rs_if														); // IF
			registro.put("NOMBRE_PYME",					rs_pyme													); // PYME
			registro.put("ESTATUS",							rs_estatus												); // Estatus
			registro.put("FECHA_VENCIMIENTO",			rs_fechaVenc											); // Fecha de Vencimiento
			registro.put("NUMERO_TOTAL_DOCUMENTOS",	rs_totalDocs											); // Num. Total de Doctos.
			registro.put("NOMBRE_MONEDA",					nombreMoneda											); // Moneda
			registro.put("MONTO_TOTAL",					Comunes.formatoMN(rs_montoDocs)					); // Monto Total
			registro.put("CODIGO_BANCO_CUENTA_CLABE",	esCuentaEnDolaresAmericanos?"N/A":rs_banco	); // Código Banco Cuenta Clabe
			registro.put("CUENTA_CLABE_SWIFT",			rs_cuenta												); // Cuenta CLABE/SWIFT
			registro.put("DESCRIPCION_CECOBAN",			esCuentaEnDolaresAmericanos?"N/A":rs_descCec	); // Descripción CECOBAN
			registro.put("DESCRIPCION_DOLARES",			esCuentaEnMonedaNacional	?"N/A":rs_descCec	); // Descripción DÓLARES 
			registro.put("ESTATUS_CECOBAN",				rs_cecoban												); // 
			registros.add(registro);
			
			i++;
			
			// El GroupID actual se convierte en el anterior
			groupIdAnterior = groupId;
				
		}//while(rs.next())
		
		// DETERMINAR QUE EPOS YA ENVIARON TODOS SUS GRUPOS DE DOCUMENTOS A FLUJO DE FONDOS.
		// SOLO PARA CUANDO SE HAN SELECCIONADOS TODAS LAS EPOS Y SE HA ESPECIFICADO UN RANGO DE 
		// FECHAS DE VENCIMIENTO DE UN SOLO DIA
		if( esRangoFechaVencimientoUnDia && todasLasEPO ){

			// Evaluar envío flujo fondos de la última EPO
			if( hasRecords ){
					
				boolean flujoFondosCompletado = hayDocumentosMXNOperados || hayDocumentosMXNSinOperar || hayDocumentosUSDOperados || hayDocumentosUSDSinOperar;
 
				if(        hayDocumentosMXNOperados  && !dispersion.seEnvioAFlujoFondos(claveEPOAnterior, "1",  fechaVencimientoAnterior, "OPR")   ){
					flujoFondosCompletado = false;
				} else if( hayDocumentosMXNSinOperar && !dispersion.seEnvioAFlujoFondos(claveEPOAnterior, "1",  fechaVencimientoAnterior, "SNOPR") ){
					flujoFondosCompletado = false;
				} else if( hayDocumentosUSDOperados  && !dispersion.seEnvioAFlujoFondos(claveEPOAnterior, "54", fechaVencimientoAnterior, "OPR")   ){
					flujoFondosCompletado = false;
				} else if( hayDocumentosUSDSinOperar && !dispersion.seEnvioAFlujoFondos(claveEPOAnterior, "54", fechaVencimientoAnterior, "SNOPR") ){
					flujoFondosCompletado = false;
				} 
					
				groupCheckBoxData.put(claveEPOAnterior+":"+fechaVencimientoAnterior,flujoFondosCompletado?"true;readonly":"false;normal");
				
			}
 
		}
		
		// AGREGAR TOTALES DEL ÚLTIMO GRUPO
		if(i > 0 ) {
			
			JSONArray  summaryDataRegisters = new JSONArray();
			JSONObject summaryDataRegister  = null;
					
			summaryDataRegister  = new JSONObject();
			summaryDataRegister.put( "NUMERO_NAFIN_ELECTRONICO", 	"Total M.N."											);
			summaryDataRegister.put( "NUMERO_TOTAL_DOCUMENTOS",	String.valueOf(totalDocumentos)					);
			summaryDataRegister.put( "MONTO_TOTAL", 					Comunes.formatoMN(totalMonto.toPlainString())		);
			summaryDataRegister.put( "ESTATUS_CECOBAN", 				"99"														); // PARA QUE LOS TOTALES NO SE PINTEN EN ROJO
			summaryDataRegisters.add( summaryDataRegister );
					
			summaryDataRegister  = new JSONObject();
			summaryDataRegister.put( "NUMERO_NAFIN_ELECTRONICO",	"Total USD"												);
			summaryDataRegister.put( "NUMERO_TOTAL_DOCUMENTOS",   String.valueOf(totalDocumentosUSD)				);
			summaryDataRegister.put( "MONTO_TOTAL", 					Comunes.formatoMN(totalMontoUSD.toPlainString())	);
			summaryDataRegister.put( "ESTATUS_CECOBAN", 				"99"														); // PARA QUE LOS TOTALES NO SE PINTEN EN ROJO
			summaryDataRegisters.add( summaryDataRegister );
					
			summaryData.put(groupIdAnterior,summaryDataRegisters);

			lReportes.add(repDispEpo);
			
		}
 
		// PONER EL REPORTE EN SESIÓN
		if( i > 0 ) {
			session.setAttribute( "lReportes", 					lReportes   );
			session.setAttribute( "lReportes.registros",		registros   );
			session.setAttribute( "lReportes.summaryData",	summaryData );
		} 
		
	} catch(Exception e) {
		
		log.error("ConsultaDetalleMonitoreoEPO(Exception)");
		log.error("ConsultaDetalleMonitoreoEPO.cmb_tipo_monitor = <" + cmb_tipo_monitor + ">");
		log.error("ConsultaDetalleMonitoreoEPO.ic_epo           = <" + ic_epo           + ">");
		log.error("ConsultaDetalleMonitoreoEPO.txt_fecha_ven_de = <" + txt_fecha_ven_de + ">");
		log.error("ConsultaDetalleMonitoreoEPO.txt_fecha_ven_a  = <" + txt_fecha_ven_a  + ">");
		log.error("ConsultaDetalleMonitoreoEPO.qrySentencia     = <" + qrySentencia     + ">");
		e.printStackTrace();
		
		throw e;
		
	} finally {
		
		if( rs != null ){ try { rs.close(); } catch(Exception e){} }
		if( ps != null ){ try { ps.close(); } catch(Exception e){} }
		
		if( con.hayConexionAbierta() ){
			con.cierraConexionDB();
		}
		
	}
	
	resultado.put("success",								new Boolean(true)									);
	resultado.put("total",    								new Integer(registros.size()) 				);
	resultado.put("registros",								registros											);
	resultado.put("summaryData",							summaryData											);
	resultado.put("groupCheckBoxData",				 	groupCheckBoxData									);
	resultado.put("hideGroupCheckBox",				 	new Boolean(!habilitarSeleccionEPO)			);
	resultado.put("esRangoFechaVencimientoUnDia", 	new Boolean(esRangoFechaVencimientoUnDia)	);
	resultado.put("todasLasEPO",						 	new Boolean(todasLasEPO)						);
	resultado.put("existenParametrosDeDispersion",	new Boolean(existenParametrosDeDispersion));
	
	infoRegresar      = resultado.toString();

} else if( informacion.equals("GenerarArchivo") ){
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	String      urlArchivo		= "";
	
	String 		nombreArchivo	= "";
	
	// Leer parametros
	String 		tipo 				= (request.getParameter("tipo") == null)?"":request.getParameter("tipo");
	
	// Preparar clases adicionales
	ConsultaDetalleMonitoreoEPO consulta = new ConsultaDetalleMonitoreoEPO();
	
	// Leer datos
	JSONArray 		registros 	= (JSONArray)  session.getAttribute( "lReportes.registros"   );
	JSONObject 		summaryData = (JSONObject) session.getAttribute( "lReportes.summaryData" );
	// HttpSession session
	String			directorioTemporal		= strDirectorioTemp; 
	String 			directorioPublicacion 	= strDirectorioPublicacion;
	
	// Crear archivo
	if(        "PDF".equals(tipo) ){
		nombreArchivo = consulta.generaArchivoPDF( directorioTemporal, directorioPublicacion, registros, summaryData, session );
		urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
	} else if( "CSV".equals(tipo) ){
		nombreArchivo = consulta.generaArchivoCSV( directorioTemporal, registros, summaryData );
		urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
	}
 
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();

} else if( informacion.equals("GenerarReporteDispersionEPO") ){
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	String      urlArchivo		= "";
	
	// Leer parametros
	String 			tipo 							= (request.getParameter("tipo")		== null)?"":request.getParameter("tipo");
	
	// Preparar clases adicionales
	CreaArchivo					archivo 			= new CreaArchivo();
	NotificaDispersionEPO	notificaciones = new NotificaDispersionEPO();
	
	// Generar Archivo
	List 				reportesEPO 				= (ArrayList) session.getAttribute("lReportes");  
	// HttpSession session
	String			directorioTemporal		= strDirectorioTemp; 
	String 			directorioPublicacion 	= strDirectorioPublicacion;
	String 			nombreArchivo				= archivo.nombreArchivo(); 
	DispersionBean dispersionBean 			= new DispersionBean(); 
	if(        "PDF".equals(tipo) ){
		notificaciones.generaArchivoAdjuntoPDF(reportesEPO, session, directorioTemporal, directorioPublicacion, nombreArchivo, dispersionBean );
		urlArchivo = strDirecVirtualTemp + nombreArchivo + ".pdf";
	} else if( "XLS".equals(tipo) ){
		notificaciones.generaArchivoAdjuntoXLS(reportesEPO, session, directorioTemporal, directorioPublicacion, nombreArchivo, dispersionBean );
		urlArchivo = strDirecVirtualTemp + nombreArchivo + ".xls";
	}
 
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("MonitorDispersion.enviarCorreoMasivoCliente") ){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;

	// Leer parametros de entrada
	String  numeroTotalDeNotificaciones  = (request.getParameter("numeroTotalDeNotificaciones") == null)?"0":request.getParameter("numeroTotalDeNotificaciones");
	String  notificacionesRealizadas     = "0";
	String  porcentajeAvance           	 = "0.00";	
	
	// Revisar si hay un thread de una opracion anterior, y si es así detenerlo.
	NotificacionMasivaDispersionEPOThread 	thread = (NotificacionMasivaDispersionEPOThread) session.getAttribute("15ENVIARCORREOMASIVO_THREAD");
	if(thread != null){
		thread.setRequestStop(true);
		// Esperar a que el thread finalice
		while(thread.isRunning() && !thread.isCompleted()){
			Thread.sleep(500);
		}
      session.removeAttribute("15ENVIARCORREOMASIVO_THREAD");
	}
	
	// Enviar detalle del envío de las notificaciones
	resultado.put("numeroTotalDeNotificaciones",	numeroTotalDeNotificaciones );
	resultado.put("notificacionesRealizadas",		notificacionesRealizadas	 );
	resultado.put("porcentajeAvance",				porcentajeAvance				 );
	resultado.put("envioFinalizado",					new Boolean(false)	 		 );
	
	// Determinar el estado siguiente
	if( Integer.parseInt(numeroTotalDeNotificaciones) == 0 ){
		estadoSiguiente = "ENVIO_CORREO_MASIVO_FINALIZADO"; 
		resultado.put("resultadoOperacion","No se especificó ninguna EPO.");
	} else {
		estadoSiguiente = "ENVIO_CORREO_MASIVO_MOSTRAR_AVANCE";
	}
 
	// 2. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("MonitorDispersion.envioCorreoMasivoMostrarAvance") ){ 

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// 1. Determinar el estado siguiente
	estadoSiguiente = "ENVIO_CORREO_MASIVO_LANZAR_THREAD";
	
	// 2. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("MonitorDispersion.envioCorreoMasivoLanzarThread") ){ 
	
	JSONObject	resultado 					= new JSONObject();
	boolean		success	 					= true;
	String 		estadoSiguiente 			= null;
	String		msg							= null;

	// Leer parametros
	String[]	   eposSeleccionadas 		=  null;
	if( request.getParameter("eposSeleccionadas") == null){
		eposSeleccionadas 					= new String[0];
	}else {
		JSONArray auxArray = JSONArray.fromObject(request.getParameter("eposSeleccionadas"));
		eposSeleccionadas  = new String[auxArray.size()];
		for(int i=0;i<auxArray.size();i++){
			eposSeleccionadas[i] = auxArray.getString(i);
		}
	}
	//String[]	   eposSeleccionadas 		= (request.getParameterValues("eposSeleccionadas") == null)?new String[0]:request.getParameterValues("eposSeleccionadas");
	String 		fechaDeVencimiento 		= (request.getParameter("fechaVencimiento")		   == null)?""           :request.getParameter("fechaVencimiento");

	int 	 		totalNotificaciones 		= eposSeleccionadas.length;
	List 			reportesEPO 				= (ArrayList) session.getAttribute("lReportes");
	String 		loginDelUsuario			= iNoUsuario;
	String 		nombreDelUsuario			= (String)    session.getAttribute("strNombreUsuario");
	String 		correoUsuario				= (String)    session.getAttribute("strCorreoUsuario");
	// HttpSession session
	String		directorioTemporal		= strDirectorioTemp; 
	String 		directorioPublicacion 	= strDirectorioPublicacion;
	
	// Obtener EJB de Dispersion
	Dispersion 		dispersion 				= ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	
	// Recortar variables que pudieran causar problemas
	loginDelUsuario						= loginDelUsuario.length() >12 ?loginDelUsuario.substring(0,12)  :loginDelUsuario;
	nombreDelUsuario						= nombreDelUsuario.length()>100?nombreDelUsuario.substring(0,100):nombreDelUsuario;
 
	// Crear un thread nuevo
	NotificacionMasivaDispersionEPOThread thread = new NotificacionMasivaDispersionEPOThread();
	thread.setEposSeleccionadas(Arrays.asList(eposSeleccionadas));
	thread.setReportesEPO(reportesEPO);
	thread.setLoginDelUsuario(loginDelUsuario);             
	thread.setNombreDelUsuario(nombreDelUsuario);           
	thread.setCorreoUsuario(correoUsuario);         
	thread.setFechaDeVencimiento(fechaDeVencimiento);               
	thread.setSession(session);                
	thread.setDirectorioTemporal(directorioTemporal);               
	thread.setDirectorioPublicacion(directorioPublicacion);         
	thread.setTotalNotificaciones(totalNotificaciones);                
	thread.setDispersionBean(new DispersionBean()); 
	thread.setNotificaDispersionEPO(new NotificaDispersionEPO());
	// Lanzar proceso de sincronizacion
	thread.setRunning(true);
	new Thread(thread).start();
	// Poner el thread en sesion
	session.setAttribute("15ENVIARCORREOMASIVO_THREAD",thread);
 
	// Enviar detalle del envío de las notificaciones
	resultado.put("numeroTotalDeNotificaciones",	String.valueOf( totalNotificaciones )	);
	resultado.put("notificacionesRealizadas",		"0"	 											);
	resultado.put("porcentajeAvance",				"0"	 											);
	resultado.put("envioFinalizado",					new Boolean(false)	 						);
	
	// Determinar el estado siguiente
	estadoSiguiente = "ENVIO_CORREO_MASIVO_ACTUALIZAR_AVANCE"; 
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("MonitorDispersion.envioCorreoMasivoActualizarAvance") ){ // actualizarAvance
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// LEER PARAMETROS
	boolean                  envioFinalizado = "true".equals(request.getParameter("envioFinalizado"))?true:false;

	// LEER THREAD DE SESION
	NotificacionMasivaDispersionEPOThread thread          = (NotificacionMasivaDispersionEPOThread) session.getAttribute("15ENVIARCORREOMASIVO_THREAD");
	
	// SI EL USUARIO YA PUDO VER QUE SE HA COMPLETADO EL 100 DE AVANCE, ENVIAR EL DETALLE DE LA OPERACION
	if( envioFinalizado ){
		
		// USAR THREAD PARA LEER LO QUE HACE FALTA
		String 	mensajes 				= thread.getMensajes();
		boolean 	notificacionExitosa	= mensajes.length() == 0?true:false;
		// notificacionExitosa = true;
		// Si todas las operaciones fueron exitosas, ocultar animacion y mostrar:
		if( notificacionExitosa ){
			resultado.put("resultadoOperacion", "El envío de correos se realizó exitosamente."       );
		// En caso de que haya errores(longitud del buffer > 0), mostrar mensaje y detalle de estos:
		} else {
			resultado.put("resultadoOperacion", "Se presentarón problemas al realizar la operación." );
			resultado.put("detalleOperacion",   mensajes                                             );
		}
				
		// Remover thread de sesion
		session.removeAttribute("15ENVIARCORREOMASIVO_THREAD");
		
	// ENVIAR PORCENTAJE DE AVANCE	
	} else {
		
		// Enviar porcentaje de avance
		resultado.put("numeroTotalDeNotificaciones",	new Integer(thread.getNumberOfRegisters())	);
		resultado.put("notificacionesRealizadas",		new Integer(thread.getProcessedRegisters())	);
		resultado.put("porcentajeAvance",				new Double(thread.getPercent())					);
		resultado.put("envioFinalizado",             new Boolean(thread.isCompleted())				);
		
	}
	
	// DETERMINAR EL ESTADO SIGUIENTE
	if( envioFinalizado ){
		estadoSiguiente = "ENVIO_CORREO_MASIVO_FINALIZADO";
	} else {
		estadoSiguiente = "ENVIO_CORREO_MASIVO_ACTUALIZAR_AVANCE"; 
	}
	
	// ENVIAR RESULTADO DE LA OPERACION
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("MonitorDispersion.envioCorreoMasivoFinalizado") ){ // envioFinalizado
 
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// DETERMINAR EL ESTADO SIGUIENTE
	estadoSiguiente = "ESPERAR_DECISION";
		
	// ENVIAR RESULTADO DE LA OPERACION
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("MonitorDispersion.enviarCorreoCliente") ){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// LEER PARAMETROS
	String 		claveEPO 					= (request.getParameter("claveEPO")		 == null)?"":request.getParameter("claveEPO");
	String 		fechaReporte 				= (request.getParameter("fechaReporte") == null)?"":request.getParameter("fechaReporte");
	
	String 		directorioPublicacion	= strDirectorioPublicacion;
	String 		directorioTemporal	 	= strDirectorioTemp; 
	String 		nombreDelUsuario			= (String) session.getAttribute("strNombreUsuario");
	String 		correoUsuario				= (String) session.getAttribute("strCorreoUsuario");
	List 			reportesEPO 				= (ArrayList) session.getAttribute("lReportes");
	RepDispEpo 	reporteEPO 					= (RepDispEpo) reportesEPO.get(0);
	
	nombreDelUsuario							= nombreDelUsuario.length()>100?nombreDelUsuario.substring(0,100):nombreDelUsuario;
	
	// GENERAR REPORTE
	NotificaDispersionEPO notificaDispersionEPO = new NotificaDispersionEPO();
	HashMap mensaje = notificaDispersionEPO.construyeMensaje( 
		claveEPO, 
		fechaReporte, 
		session,
		reporteEPO, 
		directorioPublicacion, 
		directorioTemporal,
		nombreDelUsuario,
		correoUsuario
	);
 
	// LEER PARAMETROS DEL REPORTE
	String mensajeWeb				= (String) mensaje.get("mensajeWeb");
	String tipoArchivo			= (String) mensaje.get("tipoArchivo");
	String nombreArchivoPDF		= (String) mensaje.get("nombreArchivoPDF");
	String tamanoArchivoPDF		= (String) mensaje.get("tamanoArchivoPDF");
	String nombreArchivoXLS		= (String) mensaje.get("nombreArchivoXLS");
	String tamanoArchivoXLS		= (String) mensaje.get("tamanoArchivoXLS");
	String nombreRemitente 		= (String) session.getAttribute("strNombreUsuario");
	String listaDeCuentasEPO	= (String) mensaje.get("listaDeCuentasEPO");
	String listaDeCuentasNAFIN = (String) mensaje.get("listaDeCuentasNAFIN");
	String asunto					= (String) mensaje.get("tituloMensaje");
		
	// CONSTRUIR CUERPO DEL CORREO	
	StringBuffer mensajeCorreo = new StringBuffer(1024);
	mensajeCorreo.append(
		"			<div style='width:550px;text-align:justify;font-family:helvetica;font-weigth:bolder;font-size:12px;padding-left:7px;'>"  +
		"			<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"border-collapse:collapse;margin: 0px; padding: 0px;\" >"  +
		"				<tr>"  +
		"					<td class=\"textos\" style=\"color:#9099AE;\" >Datos&nbsp;adjuntos:</td>"  +
		"					<td class=\"textos\" style=\"vertical-align:top;text-align:justified;padding-top:0px;width:440px;word-wrap: break-word;\"><span style=\"padding-left:0px;padding-right:0px;\">"
	);
	if(tipoArchivo.equals("P") || tipoArchivo.equals("A")){
		mensajeCorreo.append(
			"&nbsp;<img src=\"/nafin/00utils/gif/pdfdoc.png\"></span>"
		);
		mensajeCorreo.append(
			nombreArchivoPDF
		);
		mensajeCorreo.append(
			"<br>&nbsp;("
		);
		mensajeCorreo.append(
			tamanoArchivoPDF
		);
		mensajeCorreo.append(
			")"
		);
	}
	if(tipoArchivo.equals("A")){
		mensajeCorreo.append(
			"<br>"
		);
	}
	if(tipoArchivo.equals("E") || tipoArchivo.equals("A")){
		mensajeCorreo.append(
			"&nbsp;<img src=\"/nafin/00utils/gif/xlsdoc.png\"></span>"
		);
		mensajeCorreo.append(
			nombreArchivoXLS
		);
		mensajeCorreo.append(
			"<br>&nbsp;("
		);
		mensajeCorreo.append(
			tamanoArchivoXLS
		);
		mensajeCorreo.append(
			")"
		);
	}
	mensajeCorreo.append(
		"					</td>"  +
		"				</tr>"  +
		"			</table>"  +
		"			<hr width=\"100%\" >"  +
		"			&nbsp;<br>"  +
		"			<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"border-collapse:collapse;margin: 0px; padding: 0px;\" >"  +
		"				<tr>"  +
		"					<td class=\"textos\" align=\"left\" style=\"font-weight:bold;vertical-align:top;padding-right:3px;\">De:</td>"  +
		"					<td class=\"textos\" style=\"width:500px;word-wrap: break-word;vertical-align:top;text-align:justified;\">"
	);
	mensajeCorreo.append(
		nombreRemitente
	);
	mensajeCorreo.append(
		"              </td>"  +
		"				</tr>"  +
		"				<tr>"  +
		"					<td class=\"textos\" align=\"left\" style=\"font-weight:bold;vertical-align:top;padding-right:3px;\">Para:</td>"  +
		"					<td class=\"textos\" style=\"width:500px;word-wrap: break-word;vertical-align:top;text-align:justified;\">"
	);
	mensajeCorreo.append(
		 listaDeCuentasEPO
	);
	mensajeCorreo.append(
		"              </td>"  +
		"				</tr>"  +
		"				<tr>"  +
		"					<td class=\"textos\" align=\"left\" style=\"font-weight:bold;vertical-align:top;padding-right:3px;\">CC:</td>"  +
		"					<td class=\"textos\" style=\"width:500px;word-wrap: break-word;vertical-align:top;text-align:justified;\">"
	);
	mensajeCorreo.append(
		 listaDeCuentasNAFIN
	);
	mensajeCorreo.append(
		"              </td>"  +
		"				</tr>"  +
		"				<tr>"  +
		"					<td class=\"textos\" align=\"left\" style=\"font-weight:bold;vertical-align:top;padding-right:3px;\">Asunto:</td>"  +
		"					<td class=\"textos\" style=\"width:500px;vertical-align:top;text-align:justified;\">"
	);
	mensajeCorreo.append(
		 asunto
	);
	mensajeCorreo.append(
		"              </td>"  +
		"				</tr>"  +
		"			</table>"  +
		"		</div>"  
	);	
	mensajeCorreo.append(
		mensajeWeb
	);
	mensajeCorreo.append(
		"			<div style='width:550px;text-align:justify;font-family:helvetica;font-weigth:bolder;font-size:12px;padding-left:7px;'>" +
		"				&nbsp; " +
		"			</div>    "
	);
	resultado.put("mensajeCorreo",mensajeCorreo.toString());
	
	// DETERMINAR EL ESTADO SIGUIENTE
	estadoSiguiente = "ENVIAR_CORREO_CLIENTE_MOSTRAR_VISTA_PREVIA";
		
	// ENVIAR RESULTADO DE LA OPERACION
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("MonitorDispersion.enviarCorreoClienteMostrarVistaPrevia") ){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// DETERMINAR EL ESTADO SIGUIENTE
	estadoSiguiente = "ESPERAR_DECISION";
		
	// ENVIAR RESULTADO DE LA OPERACION
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("MonitorDispersion.envioCorreoClienteFinalizar") ){
 
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// LEER PARAMETROS
	String 		claveEPO 			= (request.getParameter("claveEPO")		 == null)?"":request.getParameter("claveEPO");
	String 		fechaReporte 		= (request.getParameter("fechaReporte") == null)?"":request.getParameter("fechaReporte");
	
	// ENVIAR CORREO
	NotificaDispersionEPO notificaDispersionEPO = new NotificaDispersionEPO();
		
	StringBuffer 			mensajes 				= new StringBuffer();
	List 						lReportes 				= (ArrayList)session.getAttribute("lReportes");
		
	notificaDispersionEPO.enlistarDispersionEPO(
		claveEPO,
		(RepDispEpo)lReportes.get(0),
		iNoUsuario,
		(String) session.getAttribute("strNombreUsuario"),
		(String) session.getAttribute("strCorreoUsuario"),
		fechaReporte,
		session,
		strDirectorioTemp,
		strDirectorioPublicacion, 
		new DispersionBean(),
		mensajes
	);
		
	// DETERMINAR ESTATUS DE LA OPERACION
	String error = mensajes.toString();
		
	boolean hayErrorEnvioCorreo = !error.equals("")?true:false;
	
	// PREPARAR MENSAJE
	if(hayErrorEnvioCorreo){
		msg = error;
	} else { 
		msg = "El correo ha sido enviado";
	}
	
	// DETERMINAR SI EL GRID CON EL DETALLE DEL MONITOREO EPO SERÁ ACTUALIZADO
	if(hayErrorEnvioCorreo){
		resultado.put("updateGridDetalleMonitoreoEPO",new Boolean(false));
	} else { 
		resultado.put("updateGridDetalleMonitoreoEPO",new Boolean(true));
	}
	
	// DETERMINAR EL ESTADO SIGUIENTE
	if(hayErrorEnvioCorreo){
		estadoSiguiente = "ESPERAR_DECISION";
	} else {
		estadoSiguiente = "REGRESAR_A_PANTALLA_MONITOREO";
	}
		
	// ENVIAR RESULTADO DE LA OPERACION
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
 			
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>