Ext.ns('NE.dispersion');

NE.dispersion.GridNoDisp = Ext.extend(Ext.grid.GridPanel,{
	initComponent : function(){
		 Ext.apply(this, {
			margins: '0 0 0 0',
			viewConfig: {
				templates: {
					cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
						'</td>'
					)
				}
			},
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 250,
			width: 865,
			style: 'margin:0 auto;',
			title: '',
			frame: true
		});
		
		NE.dispersion.GridNoDisp.superclass.initComponent.call(this);
	}
});

Ext.onReady(function() {

	var ObjGral = {
		hayDocumentosNoOperadosMN : null,
		hayDocumentosNoOperadosUSD : null,
		hayTarifasParametrizadas : null,
		hayTarifasParametrizadasUSD : null,
		hayDocumentosOperadosMN : null,
		hayDocumentosOperadosUSD : null,
		descripcionMonedaNacional : null,
		descripcionDolaresAmericanos : null,
		hayTarifaA : null,
		diasTarifaA : null,	
		hayTarifaB : null,
		diasTarifaB : null,
		hayTarifaC : null,
		diasTarifaC : null,
		hayTarifaAUSD : null,
		diasTarifaAUSD : null,
		hayTarifaBUSD : null,
		diasTarifaBUSD : null,
		hayTarifaCUSD : null,
		diasTarifaCUSD : null,
		sFechaHoy : null,
		fechaVencDe : null,
		fechaVencA : null,
		nombreEPO : null
	}
	
	var ObjFlags = {
		flagDetalleDisp : false,
		flagDetalleNoDisp : false
	}

//HANDLERS----------------------------------------------------------------------
	var procesarSuccessDatosIniciales = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
				
				var txtFechaVencDe = Ext.getCmp('txtFechaVencDe').setValue(resp.fechaDeHoy);
				var txtFechaVencA = Ext.getCmp('txtFechaVencA').setValue(resp.fechaDeHoy);

				pnl.el.unmask();
			
		}else{
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var consultarMovimientos = function(boton, evento) {
		pnl.el.mask('Consultando...', 'x-mask-loading');
		
		Ext.Ajax.request({
			url: '15disperconsultaEPOext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ConsultarMovimientos'
			}),
			callback: procesarSuccessCosultaMov
		});  
	}

	//LLAMADA REALIZADA UNA VEZ HECHA LA CONSULTA DEL PRIMER GRID(Detalle de Documentos Operados por Intermediario Financiero)
	var procesarSuccessCosultaMov = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var infoComsionTpl = [];
			
			ObjGral.hayTarifasParametrizadas = resp.hayTarifasParametrizadas;
			ObjGral.hayTarifasParametrizadasUSD = resp.hayTarifasParametrizadasUSD;
			ObjGral.hayDocumentosOperadosMN = resp.hayDocumentosOperadosMN;
			ObjGral.hayDocumentosOperadosUSD = resp.hayDocumentosOperadosUSD;
			ObjGral.descripcionMonedaNacional =resp.descripcionMonedaNacional;
			ObjGral.descripcionDolaresAmericanos = resp.descripcionDolaresAmericanos;
			ObjGral.sFechaHoy = resp.sFechaHoy;
			ObjGral.fechaVencDe = resp.fechaVencDe;
			ObjGral.fechaVencA = resp.fechaVencA;
			ObjGral.nombreEPO = resp.nombreEPO;
			
			
			Ext.getCmp('btnGenerarArchivo').show();
			Ext.getCmp('btnImprimirPDF').show();
			Ext.getCmp('btnGenerarArchivo').enable();
			Ext.getCmp('btnImprimirPDF').enable();
			Ext.getCmp('btnAbrirCSV').hide();
			Ext.getCmp('btnAbrirPDF').hide();
			
			var txtEncabezado = '<table class="formas" width=800>'+
									'<tr><td colspan="2" align="right">Fecha de Emisi�n: <b>'+ObjGral.sFechaHoy+'</b></td></tr>'+
									'<tr><td colspan="2" align="left">EPO: <b>'+ObjGral.nombreEPO+'</b></td></tr>'+
									'<tr><td align="center">Fecha de Inicio</td><td align="center">Fecha de T�rmino</td></tr>'+
									'<tr><td align="center"><b>'+ObjGral.fechaVencDe+'</b></td><td align="center"><b>'+ObjGral.fechaVencA+'</b></td></tr>'+
									'<tr><td colspan="2" align="left">&nbsp;</td></tr>'+
									'</table>';
									
			var txtComisiones = '';
			if(ObjGral.hayTarifasParametrizadas || ObjGral.hayTarifasParametrizadasUSD){
				txtComisiones ='<table class="formas">'+
									'<tr><td colspan="2">Clasificaci�n de Documentos de Acuerdo a su Vencimiento</td></tr>'+
									'<tr><td colspan="2">&nbsp;</td></tr>'+
									'<tr>';
				
				if(ObjGral.hayTarifasParametrizadas) txtComisiones += '<td align="center"><b>'+ObjGral.descripcionMonedaNacional+'</b></td>';
				if(ObjGral.hayTarifasParametrizadasUSD) txtComisiones += '<td align="center"><b>'+ObjGral.descripcionDolaresAmericanos+'</b></td>';
				txtComisiones += '</tr>';
				
				if(ObjGral.hayTarifasParametrizadas){
					ObjGral.hayTarifaA = resp.hayTarifaA;
					ObjGral.diasTarifaA = resp.diasTarifaA;	
					ObjGral.hayTarifaB = resp.hayTarifaB;
					ObjGral.diasTarifaB = resp.diasTarifaB;
					ObjGral.hayTarifaC = resp.hayTarifaC;
					ObjGral.diasTarifaC = resp.diasTarifaC;
					
					
					
				}
				if(ObjGral.hayTarifasParametrizadasUSD){
					ObjGral.hayTarifaAUSD = resp.hayTarifaAUSD;
					ObjGral.diasTarifaAUSD = resp.diasTarifaAUSD;	
					ObjGral.hayTarifaBUSD = resp.hayTarifaBUSD;
					ObjGral.diasTarifaBUSD = resp.diasTarifaBUSD;
					ObjGral.hayTarifaCUSD = resp.hayTarifaCUSD;
					ObjGral.diasTarifaCUSD = resp.diasTarifaCUSD;
				}
				
				if(ObjGral.hayTarifaA && ObjGral.hayTarifaAUSD){
					txtComisiones += '<tr>';
					if(ObjGral.hayTarifaA) txtComisiones += '<td align="left">a) '+ObjGral.diasTarifaA+'</td>';
					if(ObjGral.hayTarifaA) txtComisiones += '<td align="left">a) '+ObjGral.diasTarifaAUSD+'</td>';
					txtComisiones += '</tr>';
				}
				if(ObjGral.hayTarifaB && ObjGral.hayTarifaBUSD){
					txtComisiones += '<tr>';
					if(ObjGral.hayTarifaB) txtComisiones += '<td align="left">b) '+ObjGral.diasTarifaB+'</td>';
					if(ObjGral.hayTarifaBUSD) txtComisiones += '<td align="left">b) '+ObjGral.diasTarifaBUSD+'</td>';
					txtComisiones += '</tr>';
				}
				if(ObjGral.hayTarifaC && ObjGral.hayTarifaCUSD){
					txtComisiones += '<tr>';
					if(ObjGral.hayTarifaC) txtComisiones += '<td align="left">c) '+ObjGral.diasTarifaC+'</td>';
					if(ObjGral.hayTarifaCUSD) txtComisiones += '<td align="left">c) '+ObjGral.diasTarifaCUSD+'</td>';
					txtComisiones += '</tr>';
				}

			}

			if(ObjGral.hayTarifasParametrizadas && ObjGral.hayTarifasParametrizadasUSD){
				txtComisiones += '</table>';
				var objClasifDocto = Ext.getCmp('pnlClasifDocto');
				objClasifDocto.html = txtComisiones;
				
				var pnlEncabezado = Ext.getCmp('pnlEncabezado');
				pnlEncabezado.html = txtEncabezado;
			}
			storeConsMovData.loadData(resp);
			storeConsMovTotalData.loadData(resp);
			
			var groupB = new Ext.ux.grid.ColumnHeaderGroup({
				rows: [
					[
						{header: ' ', colspan: 1, align: 'center'},
						{header: ObjGral.diasTarifaA, colspan: 2, align: 'center'},
						{header: ObjGral.diasTarifaB, colspan: 2, align: 'center'},
						{header: ObjGral.diasTarifaC, colspan: 2, align: 'center'},
						{header: 'Total No Operados', colspan: 2, align: 'center'}
						
					]
				]
			});
			
			var groupC = new Ext.ux.grid.ColumnHeaderGroup({
				rows: [
					[
						{header: ' ', colspan: 1, align: 'center'},
						{header: ObjGral.diasTarifaAUSD, colspan: 2, align: 'center'},
						{header: ObjGral.diasTarifaBUSD, colspan: 2, align: 'center'},
						{header: ObjGral.diasTarifaCUSD, colspan: 2, align: 'center'},
						{header: 'Total No Operados', colspan: 2, align: 'center'}
						
					]
				]
			});
			
			var objGridB = Ext.getCmp('gridB1');
			
			if (objGridB) {
				objGridB.show();
			} else {
				gridB = new NE.dispersion.GridNoDisp({
					id: 'gridB1',
					store: storeDoctoNoOperMnData,
					cm: colModelGridB,
					plugins: groupB,
					bbar: {
						xtype: 'toolbar',
						items: [
							'->',
							'-',
							{
							text: 'Ver Totales',
							id: 'btnTotalesB',
							handler: function(btn){
									if(gridBTot.isVisible()){
										btn.setText('Ver Totales')
										gridBTot.setVisible(false);
									}else{
										btn.setText('Ocultar Totales')
										gridBTot.setVisible(true);
									}
								}
							}
						]
					}
				});
			}
			
			var objGridC = Ext.getCmp('gridC1');
			
			if (objGridC) {
				objGridC.show();
			} else {
				gridC = new NE.dispersion.GridNoDisp({
					id: 'gridC1',
					store: storeDoctoNoOperUsdData,
					cm: colModelGridB,
					plugins: groupC,
					bbar: {
						xtype: 'toolbar',
						items: [
							'->',
							'-',
							{
							text: 'Ver Totales',
							id: 'btnTotalesC',
							handler: function(btn){
									if(gridCTot.isVisible()){
										btn.setText('Ver Totales')
										gridCTot.setVisible(false);
									}else{
										btn.setText('Ocultar Totales')
										gridCTot.setVisible(true);
									}
								}
							}
						]
					}
				});
			}

			
			storeDoctoNoOperMnData.loadData(resp);
			storeNoOperMNtotalData.loadData(resp);
			
			storeDoctoNoOperUsdData.loadData(resp);
			storeNoOperUSDtotalData.loadData(resp);
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		pnl.el.unmask();
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var gridColumnMod = gridA.getColumnModel();
		
		gridA.show();
		
		if (arrRegistros != null) {
			if (!gridA.isVisible()) {
				contenedorPrincipalCmp.add(panelDoctosOpe);
				contenedorPrincipalCmp.doLayout();
			}
			
			var el = gridA.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
					el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}	
	}
	
	
	var procesarConsultaTotalesData = function(store, arrRegistros, opts) {
		gridATot.hide();		
	}
	
	var procesarConsultaBData = function(store, arrRegistros, opts) {
		
		var panelAllInfo = Ext.getCmp('pnlAllInfo');
		var gridColumnMod = gridB.getColumnModel();
		
		gridB.show();
		
		if (arrRegistros != null) {
			if (!gridB.isVisible()) {
				pnlDoctosNoOpeMn.add(gridB);
				pnlDoctosNoOpeMn.doLayout();
				
				panelAllInfo.add(pnlDoctosNoOpeMn);
				panelAllInfo.add(NE.util.getEspaciador(10));
				panelAllInfo.doLayout();
				
				
			}
			
			var el = gridB.getGridEl();
			if(store.getTotalCount() > 0) {
				var btnTotalesB = Ext.getCmp('btnTotalesB');
				btnTotalesB.enable();
				ObjGral.hayDocumentosNoOperadosMN = true;
				el.unmask();
			}else {
				var btnTotalesB = Ext.getCmp('btnTotalesB');
				btnTotalesB.disable();
				ObjGral.hayDocumentosNoOperadosMN = false;
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarConsultaBTotData = function(store, arrRegistros, opts) {
		
		if (arrRegistros != null) {
			if (!gridBTot.isVisible()) {
				pnlDoctosNoOpeMn.add(gridBTot);
				pnlDoctosNoOpeMn.doLayout();
			}

		}
	}
	
	var procesarConsultaCData = function(store, arrRegistros, opts) {
		
		var panelAllInfo = Ext.getCmp('pnlAllInfo');
		var gridColumnMod = gridC.getColumnModel();
		
		var txtLeyenda = '<table class="formas" width=860>'+
							'<tr style="padding-left:20px;padding-right:20px;">'+
							'<td class="formas" style="text-align:justify;margin: 0px; padding: 0px;">'+
								'* Para determinar el per&iacute;odo de vigencia de los documentos, se '+
								'calcular&aacute; la diferiencia en d�as entre la fecha de vencimiento y '+
								'la fecha de alta de los mismos en Cadenas Productivas. En el caso de '+
								'existir m&aacute;s de un documento con la misma fecha de vencimiento '+
								'para el mismo proveedor, se tomar&aacute; como periodo de vigencia el '+
								'm&iacute;nimo del grupo de documentos. '+
							'</td>'+
							'</tr>'+
							'</table>';
							
		gridC.show();
		
		if (arrRegistros != null) {
			if (!gridC.isVisible()) {
				pnlDoctosNoOpeUsd.add(gridC);
				pnlDoctosNoOpeUsd.doLayout();
				
				panelAllInfo.add(pnlDoctosNoOpeUsd);
				panelAllInfo.add(NE.util.getEspaciador(10));
				panelAllInfo.add(new Ext.Panel({
					id: 'pnlLeyenda',
					frame: false,
					border: false,
					html: txtLeyenda
				}));
				
				panelAllInfo.doLayout();
			}
			
			var el = gridC.getGridEl();
			if(store.getTotalCount() > 0) {
				var btnTotalesC = Ext.getCmp('btnTotalesC');
				btnTotalesC.enable();
				btnTotalesC.enable();
				ObjGral.hayDocumentosNoOperadosUSD = true;
				el.unmask();
			}else {
				var btnTotalesC = Ext.getCmp('btnTotalesC');
				btnTotalesC.disable();
				ObjGral.hayDocumentosNoOperadosUSD = false;
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarConsultaCTotData = function(store, arrRegistros, opts) {
		var panelAllInfo = Ext.getCmp('pnlAllInfo');
		
		if (arrRegistros != null) {
			if (!gridCTot.isVisible()) {
				pnlDoctosNoOpeUsd.add(gridCTot);
				pnlDoctosNoOpeUsd.doLayout();		
			}
			
			var el = gridCTot.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarSuccessGenerarArchivoCSV = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var tipoArchivo =resp.tipoArchivo;
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			
					
			btnGenerarArchivo.setIconClass('');
			var btnAbrirCSV = Ext.getCmp('btnAbrirCSV');
			btnAbrirCSV.show();
			btnAbrirCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnAbrirCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = resp.urlArchivo;
				forma.submit();
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessImprimirPDF = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var tipoArchivo =resp.tipoArchivo;
			var btnGenerarArchivo = Ext.getCmp('btnImprimirPDF');
			
					
			btnGenerarArchivo.setIconClass('');
			var btnAbrirPDF = Ext.getCmp('btnAbrirPDF');
			btnAbrirPDF.show();
			btnAbrirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnAbrirPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = resp.urlArchivo;
				forma.submit();
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessDetalleDocto = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			
			storeDoctoDispData.loadData(resp);
			storeDoctoNoDispData.loadData(resp);
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var mostrarDetalleDispersion = function(){
		var ventana = Ext.getCmp('winDetalleDoctos');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				title: '',
				layout: 'form',
				modal: true,
				width: 800,
				height: 580,
				minWidth: 400,
				minHeight: 300,
				x: 100,
				y: 100,
				buttonAlign: 'right',
				id: 'winDetalleDoctos',
				closeAction: 	'hide',
				autoScroll: true,
				items: [gridDoctoDisp, gridDoctoNoDisp],
				buttons:[
					{
						text: 'Regresar',
						id: 'btnRegresarWin',
						handler: function(btn){
							Ext.getCmp('winDetalleDoctos').hide();
						}
						
					}
				]
			}).show();
		}
	}
	
	var procesarConsultaDetalleDisp = function(store, arrRegistros, opts) {
		
		if (arrRegistros != null) {
			
			ObjFlags.flagDetalleDisp = true;
			mostrarDetalleDispersion();
			
			var el = gridDoctoDisp.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarConsultaDetalleNoDisp = function(store, arrRegistros, opts) {
		
		if (arrRegistros != null) {
			
			ObjFlags.flagDetalleNoDisp = true;
			mostrarDetalleDispersion();
			
			var el = gridDoctoNoDisp.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}

	}
	
	var procesarConsultaDetalleDoctos = function(store, arrRegistros, opts) {
		if(gridDoctoDisp.getStore().getTotalCount()>0){
			gridDoctoDisp.getGridEl().unmask();		 
		}
		if(gridDoctoNoDisp.getStore().getTotalCount()>0){
			gridDoctoNoDisp.getGridEl().unmask();		 
		}
		if (arrRegistros != null) {
			
			var ventana = Ext.getCmp('winDetallePop');
			if (ventana) {
				ventana.show();
			} else {
				new Ext.Window({
					title: '',
					layout: 'fit',
					width: 500,
					height: 230,
					minWidth: 400,
					minHeight: 300,
					x: 90,
					y: 90,
					buttonAlign: 'right',
					id: 'winDetallePop',
					closeAction: 	'hide',
					autoScroll: true,
					items: [gridDetallePop],
					buttons:[
						{
							text: 'Regresar',
							id: 'btnRegresarWin',
							handler: function(btn){
								Ext.getCmp('winDetallePop').hide();
							}
							
						}
					]
				}).show();
			}
			
			var el = gridDetallePop.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}

	}


//STORES------------------------------------------------------------------------
	var storeConsMovData = new Ext.data.GroupingStore({	 
		root : 'registros',
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
				{name: 'INTERMEDIARIO'},
				{name: 'MONTO'},
				{name: 'NUMERO'},
				{name: 'MONEDA'}							
			]
		}),
		groupField: 'INTERMEDIARIO',
		sortInfo:{field: 'INTERMEDIARIO', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}		
	});
	
	var storeConsMovTotalData = new Ext.data.JsonStore({
		root : 'registrosTotales',		
		fields: [
			{name: 'TITULO_MONEDA'},
			{name: 'TOTAL_DOCTOS'},
			{name: 'MONTO_DOCTOS'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesData(null, null, null);
				}
			}
		}
		
	});
	
	var storeDoctoNoOperMnData = new Ext.data.JsonStore({
		root : 'registrosB',		
		fields: [
			{name: 'fechaVencimiento'},
			{name: 'iNoPymePV1'},
			{name: 'bMtoPV1'},
			{name: 'iNoPymePV2'},
			{name: 'bMtoPV2'},
			{name: 'iNoPymePV3'},
			{name: 'bMtoPV3'},
			{name: 'iTotPymes'},
			{name: 'bSumTotMtoV'}
			//{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaBData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaBData(null, null, null);
				}
			}
		}
		
	});
	
	var storeNoOperMNtotalData = new Ext.data.JsonStore({
		root : 'registrosBTot',		
		fields: [
			{name: 'titulo'},
			{name: 'iTotPymePV1'},
			{name: 'bMtoTotPV1'},
			{name: 'iTotPymePV2'},
			{name: 'bMtoTotPV2'},
			{name: 'iTotPymePV3'},
			{name: 'bMtoTotPV3'},
			{name: 'iSumTotPymes'},
			{name: 'bSumTotMtoH'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaBTotData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaBTotData(null, null, null);
				}
			}
		}
		
	});
	
	var storeDoctoNoOperUsdData = new Ext.data.JsonStore({
		root : 'registrosC',		
		fields: [
			{name: 'fechaVencimiento'},
			{name: 'iNoPymePV1'},
			{name: 'bMtoPV1'},
			{name: 'iNoPymePV2'},
			{name: 'bMtoPV2'},
			{name: 'iNoPymePV3'},
			{name: 'bMtoPV3'},
			{name: 'iTotPymes'},
			{name: 'bSumTotMtoV'}
			//{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaCData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCData(null, null, null);
				}
			}
		}
		
	});
	
	var storeNoOperUSDtotalData = new Ext.data.JsonStore({
		root : 'registrosCTot',		
		fields: [
			{name: 'titulo'},
			{name: 'iTotPymePV1'},
			{name: 'bMtoTotPV1'},
			{name: 'iTotPymePV2'},
			{name: 'bMtoTotPV2'},
			{name: 'iTotPymePV3'},
			{name: 'bMtoTotPV3'},
			{name: 'iSumTotPymes'},
			{name: 'bSumTotMtoH'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaCTotData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCTotData(null, null, null);
				}
			}
		}
	});
	
	var storeDoctoDispData  = new Ext.data.JsonStore({
		root : 'registrosDisp',		
		fields: [
			{name: 'NOMIF'},
			{name: 'NOMPYME'},
			{name: 'RFC'},
			{name: 'FECVENC'},
			{name: 'TOTALDOCTOS'},
			{name: 'NOMMONEDA'},
			{name: 'FNIMPORTE'},
			{name: 'NOMBANCO'},
			{name: 'TIPOCUENTA'},
			{name: 'CUENTA'},
			{name: 'ESTATUSFLUJO'},
			{name: 'FOLIO'}
		],
		totalProperty : 'totalDisp',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDetalleDisp,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDetalleDisp(null, null, null);
				}
			}
		}
	});
	
	var storeDoctoNoDispData  = new Ext.data.JsonStore({
		root : 'registrosNoDisp',		
		fields: [
			{name: 'NOMIF'},
			{name: 'NOMPYME'},
			{name: 'RFC'},
			{name: 'FECVENC'},
			{name: 'TOTALDOCTOS'},
			{name: 'NOMMONEDA'},
			{name: 'FNIMPORTE'},
			{name: 'NOMBANCO'},
			{name: 'TIPOCUENTA'},
			{name: 'CUENTA'}
		],
		totalProperty : 'totalNoDisp',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDetalleNoDisp,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDetalleNoDisp(null, null, null);
				}
			}
		}
	});
	
	
	var storeDetalleDoctoPopData = new Ext.data.JsonStore({
		url : '15disperconsultaEPOext.data.jsp',
		root : 'registros',
		baseParams: {
			informacion: 'GeneraPopUpDetalle'
		},
		fields: [
			{name: 'NUMERO_DE_DOCUMENTO'},
			{name: 'PYME'},
			{name: 'IMPORTE_DOCUMENTO'},
			{name: 'IMPORTE_DESCUENTO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDetalleDoctos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDetalleDoctos(null, null, null);
				}
			}
		}
	});
//COMPONENTES-------------------------------------------------------------------	
	var elementosForma = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'txtFechaVencDe',
					id: 'txtFechaVencDe',
					allowBlank: false,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaVencA',
					margins: '0 20 0 0'
					
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txtFechaVencA',
					id: 'txtFechaVencA',
					allowBlank: false,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaVencDe',
					margins: '0 20 0 0'
					
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		}
	];
	
	var group = new Ext.ux.grid.ColumnHeaderGroup({
        rows: [
					[
						{header: 'Documentos Operados', colspan: 4, align: 'center'}
					]
				]
    });
	 
	
	
//CONTENEDORES------------------------------------------------------------------
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: 'margin:0 auto;',
		title: 'Consulta de Movimientos',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				id:'btnConsultar',
				formBind: true,
				handler: consultarMovimientos
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href="15disperconsultaEPOext.jsp";
				}
				
			}
		]
	});
	
	var gridA = new Ext.grid.GridPanel({
		id: 'gridA1',
		store: storeConsMovData,
		margins: '0 0 0 0',
		region: 'west',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header : 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex : 'INTERMEDIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align: 'left'	
			},
			{
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 200,
				sortable : true,
				renderer:  function (valor, columna, registro){
							if(valor=='1')
								valor = ObjGral.descripcionMonedaNacional;
								columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
								
							if(valor=='54')
								valor = ObjGral.descripcionDolaresAmericanos;
								columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
								
							return valor;
						}
			},
			{
				header : 'N�mero',
				tooltip: 'N�mero',
				dataIndex : 'NUMERO',
				width : 150,
				sortable : true
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		view: new Ext.grid.GroupingView({
			//forceFit:true,
			groupTextTpl: '{text}'					 
		}),
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 300,
		width: 519,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		plugins: group,
		//region:'west'
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
				text: 'Ver Totales',
				id: 'btnTotalesA',
				handler: function(btn){
						if(gridATot.isVisible()){
							btn.setText('Ver Totales')
							gridATot.setVisible(false);
						}else{
							btn.setText('Ocultar Totales')
							gridATot.setVisible(true);
						}
					}
				}
			]
		}
	});
	
	var gridATot = new Ext.grid.GridPanel({
		id: 'gridATot1',
		store: storeConsMovTotalData,
		margins: '0 0 0 0',
		style: 'margin:0 auto;',
		colspan: 2,
		hideHeaders: true,
		hidden: true,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header : 'Titulo',
				tooltip: 'Titulo',
				dataIndex : 'TITULO_MONEDA',
				width : 200,
				sortable : true,
				align: 'left',
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Total Documentos',
				tooltip: 'Total Documentos',
				dataIndex : 'TOTAL_DOCTOS',
				width : 150,
				sortable : true
			},
			{
				header : 'Monto Doctos',
				tooltip: 'Monto Doctos',
				dataIndex : 'MONTO_DOCTOS',
				width : 150,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 90,
		width: 519,
		style: 'margin:0 auto;',
		title: '',
		frame: true
	});


	var colModelGridB = new Ext.grid.ColumnModel([
			{
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'fechaVencimiento',
				width : 100,
				sortable : true,
				hidden: false,
				align: 'left'	
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iNoPymePV1',
				width : 94,
				sortable : true
				
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'bMtoPV1',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iNoPymePV2',
				width : 94,
				sortable : true
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'bMtoPV2',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iNoPymePV3',
				width : 94,
				sortable : true
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'bMtoPV3',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iTotPymes',
				width : 94,
				sortable : true
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'bSumTotMtoV',
				width : 89,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		]);
		
	var gridB = new NE.dispersion.GridNoDisp();

	
	var gridBTot = new Ext.grid.GridPanel({
		id: 'gridBTot1',
		store: storeNoOperMNtotalData,
		margins: '0 0 0 0',
		hideHeaders: true,
		hidden: true,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header : 'titulo',
				tooltip: 'titulo',
				dataIndex : 'titulo',
				width : 100,
				sortable : true,
				hidden: false,
				align: 'left'	
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iTotPymePV1',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: function (valor, columna, registro){
					if('Tarifas:' == registro.data['titulo']){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
						return Ext.util.Format.number(valor,'$0,0.00');
					}else	if('Totales:' != registro.data['titulo']){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
						return Ext.util.Format.number(valor,'$0,0.00 MN');
					}else{
						return valor;
					}
				}
				
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'bMtoTotPV1',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iTotPymePV2',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: function (valor, columna, registro){
					if('Tarifas:' == registro.data['titulo']){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
						return Ext.util.Format.number(valor,'$0,0.00');
					}else{
						return valor;
					}
				}
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'bMtoTotPV2',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iTotPymePV3',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: function (valor, columna, registro){
					if('Tarifas:' == registro.data['titulo']){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
						return Ext.util.Format.number(valor,'$0,0.00');
					}else{
						return valor;
					}
				}
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'bMtoTotPV3',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iTotPymePV4',
				width : 94,
				align: 'right',
				sortable : true
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'bMtoTotPV4',
				width : 89,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 120,
		width: 865,
		style: 'margin:0 auto;',
		title: '',
		frame: true
	});
	
		
	var gridC = new NE.dispersion.GridNoDisp();
	
	
	var gridCTot = new Ext.grid.GridPanel({
		id: 'gridCTot1',
		store: storeNoOperUSDtotalData,
		margins: '0 0 0 0',
		hideHeaders: true,
		hidden: true,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header : 'titulo',
				tooltip: 'titulo',
				dataIndex : 'titulo',
				width : 100,
				sortable : true,
				hidden: false,
				align: 'left'	
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iTotPymePV1',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: function (valor, columna, registro){
					
					if('Tarifas:' == registro.data['titulo']){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
						return Ext.util.Format.number(valor,'$0,0.00');
					}else if('Totales:' != registro.data['titulo']){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
						return Ext.util.Format.number(valor,'$0,0.00 USD');
					}else{
						return valor;
					}
				}
				
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'bMtoTotPV1',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iTotPymePV2',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: function (valor, columna, registro){
					if('Tarifas:' == registro.data['titulo']){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
						return Ext.util.Format.number(valor,'$0,0.00');
					}else{
						return valor;
					}
				}
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				align: 'right',
				dataIndex : 'bMtoTotPV2',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iTotPymePV3',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: function (valor, columna, registro){
					if('Tarifas:' == registro.data['titulo']){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
						return Ext.util.Format.number(valor,'$0,0.00');
					}else{
						return valor;
					}
				}
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				align: 'right',
				dataIndex : 'bMtoTotPV3',
				width : 94,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Num. Proveedores',
				tooltip: 'N�mero de Proveedores',
				dataIndex : 'iTotPymePV4',
				width : 94,
				align: 'right',
				sortable : true
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				align: 'right',
				dataIndex : 'bMtoTotPV4',
				width : 89,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 120,
		width: 865,
		style: 'margin:0 auto;',
		title: '',
		frame: true
	});
	
	var gridDoctoDisp = new Ext.grid.GridPanel({
		id: 'gridDoctoDisp1',
		store: storeDoctoDispData,
		margins: '0 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMIF',
				width : 150,
				sortable : true,
				align: 'left',
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMPYME',
				width : 150,
				sortable : true,
				align: 'left',
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
				
			},
			{
				header : 'RFC',
				tooltip: 'RFC',
				dataIndex : 'RFC',
				width : 100,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECVENC',
				width : 100,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'No. Documentos',
				tooltip: 'Total de Documentos',
				dataIndex : 'TOTALDOCTOS',
				width : 100,
				sortable : true
			},
			{
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'NOMMONEDA',
				width : 100,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'FNIMPORTE',
				width : 100,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Banco',
				tooltip: 'Banco',
				dataIndex : 'NOMBANCO',
				width : 120,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Tipo Cuenta',
				tooltip: 'Tipo Cuenta',
				dataIndex : 'TIPOCUENTA',
				width : 94,
				sortable : true
			},
			{
				header : 'N�mero de Cuenta',
				tooltip: 'N�mero de Cuenta',
				dataIndex : 'CUENTA',
				width : 150,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Estatus Flujo Fondos',
				tooltip: 'Estatus Flujo Fondos',
				dataIndex : 'ESTATUSFLUJO',
				width : 120,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'No. Folio',
				tooltip: 'No. Folio Flujo de Fondos',
				dataIndex : 'FOLIO',
				width : 120,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				xtype:	'actioncolumn',
				header : 'Detalle', tooltip: 'Detalle',
				dataIndex : 'FOLIO',
				width:	65,	align: 'center', hidden: false,
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						},
						handler:	function(grid, rowIndex, colIndex, item, event) {
							grid.getGridEl().mask('Consultado...','x-mask-loading');
							var registro = grid.getStore().getAt(rowIndex);
							var clave = registro.data['FOLIO'];
					
							storeDetalleDoctoPopData.load({
								params:{
									clave : clave,
									tipoDocto: 'dispersado'
								}
							});
						}
					}
				]
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 250,
		//width: 600,
		style: 'margin:0 auto;',
		title: 'Documentos Dispersados',
		frame: true
	});
	
	var gridDoctoNoDisp = new Ext.grid.GridPanel({
		id: 'gridDoctoNoDisp1',
		store: storeDoctoNoDispData,
		margins: '0 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMIF',
				width : 150,
				sortable : true,
				align: 'left',
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMPYME',
				width : 150,
				sortable : true,
				align: 'left',
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
				
			},
			{
				header : 'RFC',
				tooltip: 'RFC',
				dataIndex : 'RFC',
				width : 100,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECVENC',
				width : 100,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'No. Documentos',
				tooltip: 'Total de Documentos',
				dataIndex : 'TOTALDOCTOS',
				width : 100,
				sortable : true
			},
			{
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'NOMMONEDA',
				width : 100,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'FNIMPORTE',
				width : 100,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Banco',
				tooltip: 'Banco',
				dataIndex : 'NOMBANCO',
				width : 120,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Tipo Cuenta',
				tooltip: 'Tipo Cuenta',
				dataIndex : 'TIPOCUENTA',
				width : 94,
				sortable : true
			},
			{
				header : 'N�mero de Cuenta',
				tooltip: 'N�mero de Cuenta',
				dataIndex : 'CUENTA',
				width : 150,
				sortable : true,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				xtype:	'actioncolumn',
				header : 'Detalle', tooltip: 'Detalle',
				dataIndex : 'CUENTA',
				width:	65,	align: 'center', hidden: false,
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						},
						handler:	function(grid, rowIndex, colIndex, item, event) {
							grid.getGridEl().mask('Consultado...','x-mask-loading');
							var registro = grid.getStore().getAt(rowIndex);
							var clave = registro.data['CUENTA'];
					
							storeDetalleDoctoPopData.load({
								params:{
									clave : clave,
									tipoDocto: 'no_dispersado'
								}
							});
						}
					}
				]
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 250,
		//width: 600,
		style: 'margin:0 auto;',
		title: 'Documentos no Dispersados',
		frame: true
	});
	
	var gridDetallePop = new Ext.grid.GridPanel({
		id: 'gridDetallePop1',
		store: storeDetalleDoctoPopData,
		margins: '0 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header : 'Num. de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUMERO_DE_DOCUMENTO',
				width : 100,
				sortable : true,
				align: 'left',
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'PYME',
				tooltip: 'PYME',
				dataIndex : 'PYME',
				width : 150,
				sortable : true,
				align: 'left',
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
				
			},
			{
				header : 'Importe Documento',
				tooltip: 'Importe Documento',
				dataIndex : 'IMPORTE_DOCUMENTO',
				width : 100,
				sortable : true,
				align: 'right',
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header : 'Importe Descuento',
				tooltip: 'Importe Descuento',
				dataIndex : 'IMPORTE_DESCUENTO',
				width : 100,
				sortable : true,
				align: 'right',
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		style: 'margin:0 auto;',
		title: '',
		frame: true
	});
	
	
	var panelDoctosOpe = new Ext.Panel({
		id: 'pnlAllInfo',
		frame: true,
		border: false,
		title: '',
		width: 890,

		items: [
			{
				xtype: 'panel',
				id: 'pnlEncabezado',
				frame: false,
				border: false,
				html: ''
			},
			{
				xtype: 'panel',
				layout: 'table',
				//height: 700,
				layoutConfig: {columns:2},
				defaults: {
					  bodyStyle:'padding:0px'
				 },
				frame: true,
				title: 'Detalle de Documentos Operados por Intermediario Financiero',
				collapsible: true,
				items:[
					gridA,
					{
						xtype: 'panel',
						id: 'pnlClasifDocto',
						align: 'top',
						height: 250,
						//width: 250,
						html: ' '
					},
					gridATot
				
				]
			},
			NE.util.getEspaciador(10)
		],
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
				text: 'Generar Archivo',
				id: 'btnGenerarArchivo',
				handler: function(btn){
						if( !ObjGral.hayDocumentosOperadosMN && !ObjGral.hayDocumentosNoOperadosMN && !ObjGral.hayDocumentosOperadosUSD && !ObjGral.hayDocumentosNoOperadosUSD ){
							Ext.MessageBox.alert('Aviso','No se encontraron registros');
						}else{
							var arrDoctoOperGral = [];
							var arrDoctoNoOperMn = [];
							var arrDoctoNoOperMnTot = [];
							var arrDoctoNoOperUsd = [];
							var arrDoctoNoOperUsdTot = [];
							var arrDoctoOperTotalGral = [];
							
							btn.setIconClass('loading-indicator');
							btn.disable();
							
							storeConsMovData.each(function(record){
								arrDoctoOperGral.push(record.data);
							});
							arrDoctoOperGral = Ext.encode(arrDoctoOperGral);
							
							storeConsMovTotalData.each(function(record){
								arrDoctoOperTotalGral.push(record.data);
							});
							arrDoctoOperTotalGral = Ext.encode(arrDoctoOperTotalGral);
							
							storeDoctoNoOperMnData.each(function(record){
								arrDoctoNoOperMn.push(record.data);
							});
							arrDoctoNoOperMn = Ext.encode(arrDoctoNoOperMn);
							
							storeNoOperMNtotalData.each(function(record){
								arrDoctoNoOperMnTot.push(record.data);
							});
							arrDoctoNoOperMnTot = Ext.encode(arrDoctoNoOperMnTot);
							
							storeDoctoNoOperUsdData.each(function(record){
								arrDoctoNoOperUsd.push(record.data);
							});
							arrDoctoNoOperUsd = Ext.encode(arrDoctoNoOperUsd);
							
							storeNoOperUSDtotalData.each(function(record){
								arrDoctoNoOperUsdTot.push(record.data);
							});
							arrDoctoNoOperUsdTot = Ext.encode(arrDoctoNoOperUsdTot);
	
							
							Ext.Ajax.request({
								url: '15disperconsultaEPOext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'GenerarArchivoCSV',
										arrDoctoOperGral : arrDoctoOperGral,
										arrDoctoOperTotalGral : arrDoctoOperTotalGral,
										arrDoctoNoOperMn : arrDoctoNoOperMn,
										arrDoctoNoOperMnTot : arrDoctoNoOperMnTot,
										arrDoctoNoOperUsd : arrDoctoNoOperUsd,
										arrDoctoNoOperUsdTot : arrDoctoNoOperUsdTot,
										objGral : Ext.encode(ObjGral)
									}),
								callback: procesarSuccessGenerarArchivoCSV
							});
						}
					}
				},
				{
					text: 'Abrir CSV',
					id: 'btnAbrirCSV',
					hidden: true
				},
				'-',
				{
				text: 'Imprimir PDF',
				id: 'btnImprimirPDF',
				handler: function(btn){
						if( !ObjGral.hayDocumentosOperadosMN && !ObjGral.hayDocumentosNoOperadosMN && !ObjGral.hayDocumentosOperadosUSD && !ObjGral.hayDocumentosNoOperadosUSD ){
							Ext.MessageBox.alert('Aviso','No se encontraron registros');
						}else{
							var arrDoctoOperGral = [];
							var arrDoctoNoOperMn = [];
							var arrDoctoNoOperMnTot = [];
							var arrDoctoNoOperUsd = [];
							var arrDoctoNoOperUsdTot = [];
							
							btn.setIconClass('loading-indicator');
							btn.disable();
							
							storeConsMovData.each(function(record){
								arrDoctoOperGral.push(record.data);
							});
							arrDoctoOperGral = Ext.encode(arrDoctoOperGral);
							
							storeDoctoNoOperMnData.each(function(record){
								arrDoctoNoOperMn.push(record.data);
							});
							arrDoctoNoOperMn = Ext.encode(arrDoctoNoOperMn);
							
							storeNoOperMNtotalData.each(function(record){
								arrDoctoNoOperMnTot.push(record.data);
							});
							arrDoctoNoOperMnTot = Ext.encode(arrDoctoNoOperMnTot);
							
							storeDoctoNoOperUsdData.each(function(record){
								arrDoctoNoOperUsd.push(record.data);
							});
							arrDoctoNoOperUsd = Ext.encode(arrDoctoNoOperUsd);
							
							storeNoOperUSDtotalData.each(function(record){
								arrDoctoNoOperUsdTot.push(record.data);
							});
							arrDoctoNoOperUsdTot = Ext.encode(arrDoctoNoOperUsdTot);
							
							Ext.Ajax.request({
								url: '15disperconsultaEPOext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'ImprimirPDF',
										arrDoctoOperGral : arrDoctoOperGral,
										arrDoctoNoOperMn : arrDoctoNoOperMn,
										arrDoctoNoOperMnTot : arrDoctoNoOperMnTot,
										arrDoctoNoOperUsd : arrDoctoNoOperUsd,
										arrDoctoNoOperUsdTot : arrDoctoNoOperUsdTot,
										objGral : Ext.encode(ObjGral)
									}),
								callback: procesarSuccessImprimirPDF
							});
						}
						
					}
				},
				{
					text: 'Abrir PDF',
					id: 'btnAbrirPDF',
					hidden: true
				},
				'-',
				{
				text: 'Detalle',
				id: 'btnDetalle',
				handler: function(btn){
						if( !ObjGral.hayDocumentosOperadosMN && !ObjGral.hayDocumentosNoOperadosMN && !ObjGral.hayDocumentosOperadosUSD && !ObjGral.hayDocumentosNoOperadosUSD ){
							Ext.MessageBox.alert('Aviso','No se encontraron registros');
						}else{
							Ext.Ajax.request({
								url: '15disperconsultaEPOext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'DetalleDoctos'
									}),
								callback: procesarSuccessDetalleDocto
							});
						}
					}
				}
			]
		}
	})
	
	var pnlDoctosNoOpeMn = new Ext.Panel({
		frame: true,
		title: 'Detalle de Documentos no Operados M.N. por Periodo de Vigencia*',
		collapsible: true
		/*items: [
			gridB
		]*/
	})

	var pnlDoctosNoOpeUsd = new Ext.Panel({
		frame: true,
		title: 'Detalle de Documentos no Operados USD por Periodo de Vigencia*',
		collapsible: true
		/*items: [
			gridC
		]*/
	})
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20)
		]
	});
	
	
	pnl.el.mask('Cargando...','x-mask-loading');
	
	Ext.Ajax.request({
		url: '15disperconsultaEPOext.data.jsp',
		params: {
			informacion: 'DatosIniciales'
		},
		callback: procesarSuccessDatosIniciales
	});  
	
});