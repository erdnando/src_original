Ext.onReady(function() {
		
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaInterfaseFlujoFondos = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						interfaseFlujoFondos(resp.estadoSiguiente,resp);
					}
				);
			} else {
				interfaseFlujoFondos(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			var element;
			
			element = Ext.getCmp("botonAceptarCifrasControl");
			if( element.iconCls === 'loading-indicator'){
				element.enable();
				element.setIconClass('icoAceptar');
			}
			
			element = Ext.getCmp("botonAceptarContrasena");
			if( element.iconCls === 'loading-indicator'){
				element.enable();
				element.setIconClass('icoAceptar');
			}
			
			/*
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Resetear forma
         resetWindowConfirmacionClave();
 
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			*/
								
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var interfaseFlujoFondos = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"							   ){
 
			// Cargar Catalogo
			var formParams = Ext.apply( {}, NE.exportarinterfaseff.inputParams);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15exportarinterfaseff01ext.data.jsp',
				params: 	Ext.apply(
					formParams,
					{
						informacion:		'InterfaseFlujoFondos.inicializacion'
					}
				),
				callback: 				procesaInterfaseFlujoFondos
			});
 
		} else if(		   estado == "MOSTRAR_CIFRAS_CONTROL"				  ){
			
			// Actualizar la altura del panel de cifras de control
			var panelCifrasControl = Ext.getCmp("panelCifrasControl");
			panelCifrasControl.show();
			
			// Cargar Cifras de Control
			var cifrasControlData = Ext.StoreMgr.key('cifrasControlDataStore');
			cifrasControlData.loadData(respuesta.cifrasControlDataArray);
 
			if( cifrasControlData.getTotalCount() > 2 ){
				panelCifrasControl.setHeight(220);
			} else {
				panelCifrasControl.setHeight(176);
			}
 
				} else if(			estado == "CANCELAR_EJECUCION"					  ){
			
			// Cargar Catalogo
			var formParams = Ext.apply( {}, NE.exportarinterfaseff.inputParams);

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15exportarinterfaseff01ext.data.jsp',
				params: 	Ext.apply(
					formParams,
					{
						informacion:		'InterfaseFlujoFondos.cancelarEjecucion'
					}
				),
				callback: 				procesaInterfaseFlujoFondos
			});
 	
		} else if(		   estado == "ACEPTAR_CIFRAS_CONTROL"				  ){
		
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url:	  		'15exportarinterfaseff01ext.data.jsp',
				params: {
					informacion: 'InterfaseFlujoFondos.aceptarCifrasControl'
				},
				callback:	procesaInterfaseFlujoFondos
			});
			
		} else if(		   estado == "SOLICITAR_CONFIRMACION"				  ){
			
			// Ocultar panel de Crifras de Control
			Ext.getCmp('panelCifrasControl').hide();
			
			// Resetear boton Aceptar Cifras de Control
			var botonAceptarCifrasControl = Ext.getCmp("botonAceptarCifrasControl");
			botonAceptarCifrasControl.enable();
			botonAceptarCifrasControl.setIconClass('icoAceptar');
			
			// Mostrar panel de Solicitud de Clave de Confirmacion
			Ext.getCmp('panelConfirmacionClave').show();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url:	  		'15exportarinterfaseff01ext.data.jsp',
				params: {
					informacion: 'InterfaseFlujoFondos.solicitarConfirmacion'
				},
				callback:	procesaInterfaseFlujoFondos
			});
			
		} else if(			estado == "LLENA_ENCABEZADO_FLUJO_FONDOS" 	  ){
 
			// Cargar Catalogo
			var formParams = Ext.apply( respuesta, NE.exportarinterfaseff.inputParams);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15exportarinterfaseff01ext.data.jsp',
				params: 	Ext.apply(
					formParams,
					{
						informacion:		'InterfaseFlujoFondos.llenaEncabezadoFlujoFondos'
					}
				),
				callback: 				procesaInterfaseFlujoFondos
			})
		
		} else if(			estado == "ESPERAR_DECISION"  					  ){ 
 
		} else if(			estado == "FIN"  					                 ){
 	
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"15exportarinterfaseff01ext.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			
			// Agregar parametros de forma dinamica
			if( Ext.isDefined(respuesta.formParams) ){

				Ext.iterate(
					respuesta.formParams, 
					function( key, value ){
						Ext.DomHelper.insertFirst(
							forma, 
							{ 
								tag: 		'input', 
								type: 	'hidden', 
								id: 		key, 
								name: 	key, 
								value: 	value
							}
						); 
					}
				);
		
			}
			
			forma.submit();
						
		}
		
	}
	
	// 2. ----------------------- FORMA CONFIRMAR CONTRASEÑA -----------------------
	
	var procesaAceptar01 = function(boton, evento) {
					
		var claveUsuario = Ext.getCmp("claveUsuario").getValue();
		if(Ext.isEmpty(claveUsuario)){
			Ext.getCmp("claveUsuario").markInvalid("Este campo es requerido");
			return;
		}
					
		var contrasena	  = Ext.getCmp("contrasena").getValue();
		if(Ext.isEmpty(contrasena)){
			Ext.getCmp("contrasena").markInvalid("Este campo es requerido");
			return;
		}
					
		var respuesta = new Object();
		respuesta	  = Ext.apply( respuesta, Ext.getCmp('panelConfirmacionClave').getForm().getValues() );
		
		// Agregar animacion de operacion en proceso...
		boton.disable();
		boton.setIconClass('loading-indicator');
		
		interfaseFlujoFondos("LLENA_ENCABEZADO_FLUJO_FONDOS",respuesta);
 
	}
				
	var procesaCancelar01 = function(boton, evento) {
		
		// Agregar animacion de operacion en proceso...
		boton.disable();
		boton.setIconClass('loading-indicator');
		
		// Cancelar ejecucion	
		interfaseFlujoFondos("CANCELAR_EJECUCION",null);
		
	}
	
	var elementosFormaContrasena = [
		// INPUT TEXT CLAVE USUARIO
		{
			xtype: 				'textfield',
			name: 				'claveUsuario',
			id: 					'claveUsuario',
			fieldLabel: 		'Clave de Usuario',
			allowBlank: 		true,
			maxLength: 			8,	
			width: 				80,
			msgTarget: 			'side',
			//anchor:			'95%',
			margins: 			'0 20 0 0'  
		},
		// INPUT TEXT CONTRASEÑA
		{
			xtype: 				'textfield',
			name: 				'contrasena',
			id: 					'contrasena',
			fieldLabel: 		'Contraseña',
			inputType: 			'password',
			allowBlank: 		true,
			//maxLength: 		8,	
			width: 				80,
			msgTarget: 			'side',
			//anchor:			'95%',
			margins: 			'0 20 0 0'  
		}
			
	];
	
	var panelConfirmacionClave = new Ext.form.FormPanel({
		id: 					'panelConfirmacionClave',
		title:				'Confirmación de Clave',
		width: 				274,
		hidden:				true,
		frame: 				true,
		border:				true,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 16px',
		style: 				'margin: 0 auto;',
		labelWidth: 		100,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		items: 				elementosFormaContrasena,
		monitorValid: 		false,
		buttons: [
			{
				xtype: 		'button',
				width: 		80,
				height:		10,
				text: 		'Aceptar',
				iconCls: 	'icoAceptar',
				id: 			'botonAceptarContrasena',
				style: 		'float:right',
				handler:    procesaAceptar01
			},
			{
				xtype: 		'button',
				width: 		80,
				height:		10,
				text: 		'Cancelar',
				iconCls: 	'icoCancelar',
				id: 			'botonCancelarContrasena',
				style: 		'float:right',
				handler:    procesaCancelar01
			}
		]
	});
 
	// 1. -------------------------------- PANEL CIFRAS DE CONTROL ----------------------------------
	
	var procesaCancelar = function(boton, evento) {
		
		// Agregar animacion de operacion en proceso...
		boton.disable();
		boton.setIconClass('loading-indicator');
		
		// Cancelar ejecucion	
		interfaseFlujoFondos("CANCELAR_EJECUCION",null);
		
	}
	
	var procesaAceptar = function(boton, evento) {
		
		// Agregar animacion de operacion en proceso...
		boton.disable();
		boton.setIconClass('loading-indicator');
		
		// Aceptar Cifras de Control	
		interfaseFlujoFondos("ACEPTAR_CIFRAS_CONTROL",null);
		
	}
					
	var procesarConsultaCifrasControl = function(store, registros, opts){
		
		var gridCifrasControl 			= Ext.getCmp('gridCifrasControl');
		
		if (registros != null) {
			
			var el 							= gridCifrasControl.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('Se presentó un error al leer los parámetros', 'x-mask');
				
			}
			
		}
		
	}
	
	var cifrasControlData = new Ext.data.ArrayStore({
			
		storeId: 'cifrasControlDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  	mapping: 0 },
			{ name: 'CONTENIDO', 		mapping: 1 },
			{ name: 'TIPO_CONTENIDO', 	mapping: 2 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaCifrasControl,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCifrasControl(null, null, null);						
				}
			}
		}
		
	});
	
	var renderContenido = function(value, metaData, record, rowIndex, colIndex, store) {
		
		if(        record.get('TIPO_CONTENIDO') == 'NUMERO_ENTERO' ){
			metaData.style += 'text-align: center !important';
		} else if( record.get('TIPO_CONTENIDO') == 'MONTO' 		  ){
			metaData.style += 'text-align: right  !important';
		}
		return value;
		
	}
	
	var elementosCifrasControl = [
		{
			xtype: 	'label',
			id:	 	'labelCifrasControl',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		100,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
				width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '5%'
						},
						region:	'west'
					},
					{
						store: 			cifrasControlData,
						xtype: 			'grid',
						id:				'gridCifrasControl',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '90%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		200,
								hidden: 		false,
								hideable:	false,
								fixed:		true,
								align: 		'right'
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								align:		'right',
								renderer: 	renderContenido
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '5%'
						},
						region:	'east'
					}
			]
		}
	];
	
	var panelCifrasControl = new Ext.Panel({
		id: 					'panelCifrasControl',
		width: 				430,
		height:				220,
		title: 				'Pre-Acuse',
		hidden:				'true',
		frame: 				true,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 6px;',
		style: 				'margin: 0 auto;',
		labelWidth: 		100,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side'
		},
		items: 				elementosCifrasControl,
		monitorValid: 		false,
		buttonAlign:		'center',
		buttons: [
			{
					xtype: 		'button',
					text: 		'Aceptar',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoAceptar',
					id: 			'botonAceptarCifrasControl',
					handler:    procesaAceptar
			},
			{
					xtype: 		'button',
					text: 		'Cancelar',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoCancelar',
					id: 			'botonCancelar',
					handler:    procesaCancelar
			}
		]
	});
	
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelCifrasControl,
			panelConfirmacionClave
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	interfaseFlujoFondos("INICIALIZACION",null);
	
});