Ext.onReady(function() {
	
	Ext.namespace('NE.monitordispersion');
	
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaMonitorDispersion = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						monitorDispersion(resp.estadoSiguiente,resp);
					}
				);
			} else {
				monitorDispersion(resp.estadoSiguiente,resp);
			}
			
		} else {
 
			// Resetear boton de enviar correo a cliente
         resetBotonEnviarCorreoCliente(false);
         
         // Ocultar ventana de Avance de Notificacion Masiva
         hideWindowAvanceNotificacion();
         
         // Resetear boton de vista previa de enviar correo
         resetBotonEnviarACliente();
         
			/*
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Resetear forma
         resetWindowConfirmacionClave();
 
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			*/
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
					
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var monitorDispersion = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"							   ){
			
			// Cargar Cat�logo de Tipos de Monitoreo
			var catalogoTipoMonitoreoData = Ext.StoreMgr.key('catalogoTipoMonitoreoDataStore');
			catalogoTipoMonitoreoData.load({
				params: { defaultValue: "E" }
			});
			
			// Cargar Cat�logo de EPOs
			var catalogoEPOData = Ext.StoreMgr.key('catalogoEPODataStore');
			catalogoEPOData.load({
				params: { defaultValue: "TODAS" }
			});
 
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispe01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.inicializacion'
				},
				callback: 				procesaMonitorDispersion
			});
 	
		} else if(			estado == "MOSTRAR_PANTALLA_MONITOREO"			  ){
									
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispe01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.mostrarPantallaMonitoreo',
					tipoMonitoreo:		respuesta.tipoMonitoreo
				},
				callback: 				procesaMonitorDispersion
			});
 	
		} else if(			estado == "ENVIAR_CORREO_MASIVO_CLIENTE"		  ){
			 
			// Bloquear boton de env�o de correos
			var btnEnviarACliente = Ext.getCmp('btnEnviarACliente');
			btnEnviarACliente.disable();
			btnEnviarACliente.setIconClass('loading-indicator');
			
			// Guardar copia de los parametros a enviar
			btnEnviarACliente.formParams = {};
			btnEnviarACliente.formParams['eposSeleccionadas'] = respuesta.eposSeleccionadas;
			btnEnviarACliente.formParams['fechaVencimiento']  = respuesta.fechaVencimiento;
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispe01ext.data.jsp',
				params: 	{
					informacion:						'MonitorDispersion.enviarCorreoMasivoCliente',
					numeroTotalDeNotificaciones:	respuesta.eposSeleccionadas.length
				},
				callback: 				procesaMonitorDispersion
			});
			
		} else if( estado == "ENVIO_CORREO_MASIVO_MOSTRAR_AVANCE"		) {
 
			// INICIALIZAR PANTALLA CON LOS PORCENTAJES DE AVANCE
			Ext.getCmp("notificacionesRealizadas").setText(respuesta.notificacionesRealizadas);
			Ext.getCmp("numeroTotalNotificaciones").setText(respuesta.numeroTotalDeNotificaciones);
 
			// MOSTRAR PANEL DE PORCENTAJE DE AVANCE
			showWindowAvanceNotificacion(respuesta.numeroTotalDeNotificaciones);
 
			// DETERMINAR EL ESTADO SIGUIENTE
			Ext.Ajax.request({
				url: 		'15monitordispe01ext.data.jsp',
				params: 	{
					informacion:			'MonitorDispersion.envioCorreoMasivoMostrarAvance'
				},
				callback: 				   procesaMonitorDispersion
			});
				
		} else if( estado == "ENVIO_CORREO_MASIVO_LANZAR_THREAD"			) {
				
			var btnEnviarACliente = Ext.getCmp('btnEnviarACliente');
				
			// PREPARAR ARGUMENTOS
				
			// Extraer la lista de EPOS  a las cuales se les notificar�
			var eposSeleccionadas    	= btnEnviarACliente.formParams['eposSeleccionadas'];
				
			// Obtener la fecha de vencimiento a la cual corresponde el reporte
			var fechaVencimiento		 	= btnEnviarACliente.formParams['fechaVencimiento'];
 
			// Lanzar Thread
			Ext.Ajax.request({
				url: 		'15monitordispe01ext.data.jsp',
				params: 	{
					informacion:			'MonitorDispersion.envioCorreoMasivoLanzarThread',
					eposSeleccionadas: 	Ext.encode(eposSeleccionadas),
					fechaVencimiento:		fechaVencimiento
				},
				callback: 				   procesaMonitorDispersion
			});
 
		} else if( estado == "ENVIO_CORREO_MASIVO_ACTUALIZAR_AVANCE"  ){
 
			var notificacionesRealizadas  = Ext.getCmp("notificacionesRealizadas");
			var numeroTotalNotificaciones = Ext.getCmp("numeroTotalNotificaciones");
			var barrarAvanceNotificacion  = Ext.getCmp("barrarAvanceNotificacion");
			
			// ACTUALIZAR PORCENSAJE DE AVANCE
			var porcentaje					= respuesta.porcentajeAvance;
			var porcentajeFormateado	= formateaPorcentajeAvance(respuesta.porcentajeAvance);
			var numeroTotal 				= respuesta.numeroTotalDeNotificaciones;
			var notificaciones 			= respuesta.notificacionesRealizadas;
 
			barrarAvanceNotificacion.updateProgress(porcentaje,porcentajeFormateado,true);
			numeroTotalNotificaciones.setText(numeroTotal); 
			notificacionesRealizadas.setText(notificaciones);
 
			// EN 2 SEGUNDOS VOLVER A CONSULTAR EL PORCENTAJE DE AVANCE
			var actualizarAvanceTask = new Ext.util.DelayedTask(
				function(){
					Ext.Ajax.request({
						url: 		 '15monitordispe01ext.data.jsp',
						params: 	{
							informacion: 		'MonitorDispersion.envioCorreoMasivoActualizarAvance',
							envioFinalizado: 	respuesta.envioFinalizado 
						},
						callback: procesaMonitorDispersion 
					});
				},
				this
			);
			actualizarAvanceTask.delay(2000);
			
		} else if( estado == "ENVIO_CORREO_MASIVO_FINALIZADO"   ){
 
			var resultadoOperacion  				  	= Ext.getCmp("resultadoOperacion");
			var textoDetalleResultadosNotificacion = Ext.getCmp("textoDetalleResultadosNotificacion");
			
			var panelResultadosNotificacion		  	= Ext.getCmp("panelResultadosNotificacion");
			var panelMonitoreoEPO					  	= Ext.getCmp("panelMonitoreoEPO");
			var gridDetalleMonitoreoEPO			  	= Ext.getCmp("gridDetalleMonitoreoEPO");
			
			// CARGAR RESULTADOS
			resultadoOperacion.setText(respuesta.resultadoOperacion);
			textoDetalleResultadosNotificacion.setRawValue(respuesta.detalleOperacion,false);
				
			// MOSTRAR PANEL DE RESULTADOS
			
			// Ocultar forma de Consulta
			panelMonitoreoEPO.hide();
			// Ocultar grid con el detalle de monitoreo EPO
			gridDetalleMonitoreoEPO.hide();
			// Ocultar panel de porcentaje de avance
			hideWindowAvanceNotificacion();
 
			// Mostrar panel de resultados
			if( !Ext.isEmpty(respuesta.detalleOperacion) ){
				panelResultadosNotificacion.setWidth( 700 );
				panelResultadosNotificacion.setHeight( 433 );
				textoDetalleResultadosNotificacion.show();
				panelResultadosNotificacion.show();
			} else {
				panelResultadosNotificacion.setWidth( 350 );
				panelResultadosNotificacion.setHeight( 108 );
				textoDetalleResultadosNotificacion.hide();
				panelResultadosNotificacion.show();				
			}
			panelResultadosNotificacion.doLayout(true);
			panelResultadosNotificacion.syncSize(true);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 		'15monitordispe01ext.data.jsp',
				params: 	{
					informacion:			'MonitorDispersion.envioCorreoMasivoFinalizado'
				},
				callback: 				   procesaMonitorDispersion
			});
			
		/*
		// NOTA: Version mejorada que muestra los resultados en un grid.
		} else if( estado == "ENVIO_CORREO_MASIVO_FINALIZADO"   ){
				
			var resultadoOperacion  				  = Ext.getCmp("resultadoOperacion");
			var gridDetalleResultadosNotificacion = Ext.getCmp("gridDetalleResultadosNotificacion");
			var detalleResultadosNotificacionData = Ext.StoreMgr.key("detalleResultadosNotificacionDataStore");
			
			var panelResultadosNotificacion		  = Ext.getCmp("panelResultadosNotificacion");
			var panelMonitoreoEPO					  = Ext.getCmp("panelMonitoreoEPO");
			var gridDetalleMonitoreoEPO			  = Ext.getCmp("gridDetalleMonitoreoEPO");
			
			// CARGAR RESULTADOS
			Ext.getCmp("resultadoOperacion").setText(respuesta.resultadoOperacion);
			detalleResultadosNotificacionData.loadData(respuesta.detalleOperacion);
				
			// MOSTRAR PANEL DE RESULTADOS
			
			// Ocultar forma de Consulta
			panelMonitoreoEPO.hide();
			// Ocultar grid con el detalle de monitoreo EPO
			gridDetalleMonitoreoEPO.hide();
			// Ocultar panel de porcentaje de avance
			hideWindowAvanceNotificacion();
 
			// Mostrar panel de resultados
			if( detalleResultadosNotificacionData.getTotalCount() > 0 ){
				panelResultadosNotificacion.setWidth( 700 );
				panelResultadosNotificacion.setHeight( 433 );
				panelResultadosNotificacion.show();
				gridDetalleResultadosNotificacion.show();
			} else {
				panelResultadosNotificacion.setWidth( 350 );
				panelResultadosNotificacion.setHeight( 108 );
				panelResultadosNotificacion.show();
				gridDetalleResultadosNotificacion.hide();
			}
			panelResultadosNotificacion.doLayout(true);
			panelResultadosNotificacion.syncSize(true);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 		'15monitordispe01ext.data.jsp',
				params: 	{
					informacion:			'MonitorDispersion.envioCorreoMasivoFinalizado'
				},
				callback: 				   procesaMonitorDispersion
			});
			
		*/
		
		} else if(        estado == "REGRESAR_A_PANTALLA_MONITOREO" 	  ){

			var panelResultadosNotificacion		  	= Ext.getCmp("panelResultadosNotificacion");
			var panelMonitoreoEPO					  	= Ext.getCmp("panelMonitoreoEPO");
			var gridDetalleMonitoreoEPO			  	= Ext.getCmp("gridDetalleMonitoreoEPO"); 			
 
			// MOSTRAR FORMA DE CONSULTA CON EL GRID DE RESULTADOS
			
			// Ocultar panel de vista previa si est� visible
			hideWindowEnviarCorreoCliente();
			// Ocultar panel de resultados de la notificacion masiva
			hidePanelResultadosNotificacion();
			// Ocultar forma de Consulta
			panelMonitoreoEPO.show();
			// Ocultar grid con el detalle de monitoreo EPO
			gridDetalleMonitoreoEPO.show();
 
			// Actualizar grid detalle monitoreo
			// Realizar consulta
			if( respuesta.updateGridDetalleMonitoreoEPO ){
				
				var formParams = Ext.apply({}, gridDetalleMonitoreoEPO.formParams );
				
				var detalleMonitoreoEPOData = Ext.StoreMgr.key('detalleMonitoreoEPODataStore');
				detalleMonitoreoEPOData.load({
					params: Ext.apply(
						formParams,
						{
							informacion: 'ConsultaDetalleMonitoreoEPO'
						}
					)
				});
				
			}
			
			// Nota: Por simplicidad, se omite determinar el estado siguiente en el servidor
			
		} else if( 		   estado == "ENVIAR_CORREO_CLIENTE"				  ){
			
			// Bloquear boton de env�o de correos
			var btnEnviarACliente = Ext.getCmp('btnEnviarACliente');
			btnEnviarACliente.disable();
			btnEnviarACliente.setIconClass('loading-indicator');

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispe01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.enviarCorreoCliente',
					claveEPO:			respuesta.claveEPO,
					fechaReporte:		respuesta.fechaReporte
				},
				callback: 				procesaMonitorDispersion
			});
			
		} else if(			estado == "ENVIAR_CORREO_CLIENTE_MOSTRAR_VISTA_PREVIA" ){
 
			// Mostrar cuerpo del correo
			var mensajeCorreo = Ext.getCmp("mensajeCorreo");
			mensajeCorreo.setValue(respuesta.mensajeCorreo);
			
			// Mostrar vista previa
			showWindowEnviarCorreoCliente();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispe01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.enviarCorreoClienteMostrarVistaPrevia'
				},
				callback: 				procesaMonitorDispersion
			});
			
		} else if(			estado == "ENVIO_CORREO_CLIENTE_FINALIZAR" 				){
			
			// Bloquear boton de env�o de correos
			var btnEnviarCorreoCliente = Ext.getCmp('btnEnviarCorreoCliente');
			btnEnviarCorreoCliente.disable();
			btnEnviarCorreoCliente.setIconClass('loading-indicator');
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordispe01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.envioCorreoClienteFinalizar',
					claveEPO:			respuesta.claveEPO,
					fechaReporte:		respuesta.fechaReporte
				},
				callback: 				procesaMonitorDispersion
			});
			
		} else if(			estado == "ESPERAR_DECISION"  					  			){ 
 
			// Resetear boton de enviar correo a cliente
         resetBotonEnviarCorreoCliente(false);
         
         // Ocultar ventana de Avance de Notificacion Masiva
         hideWindowAvanceNotificacion();
         
          // Resetear boton de vista previa de enviar correo
         resetBotonEnviarACliente();
         
			/*
			
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
			
         // Resetear forma
         resetWindowConfirmacionClave();
         
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			*/
			
		} else if(			estado == "FIN"  					                 ){
 	
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"15monitordispe01ext.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			forma.submit();
						
		}
		
	}
	
	//-------------------------- PANEL RESULTADOS NOTIFICACION ----------------------------
	
	var procesaAceptarResultadosNotificacion  = function(boton, evento){
		var respuesta = new Object();
		respuesta['updateGridDetalleMonitoreoEPO'] = true;
		monitorDispersion("REGRESAR_A_PANTALLA_MONITOREO", respuesta );
	}
	
	var procesarDetalleResultadosNotificacion = function( store, registros, opts){

		var gridDetalleResultadosNotificacion	= Ext.getCmp('gridDetalleResultadosNotificacion');
		var el 											= gridDetalleResultadosNotificacion.getGridEl();
		el.unmask();
		
		if ( registros != null ){
			
			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
 		
	}
	
	var detalleResultadosNotificacionData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'detalleResultadosNotificacionDataStore',
		fields: [
			{ name: 'DETALLE_RESULTADOS' }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarDetalleResultadosNotificacion,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDetalleResultadosNotificacion(null, null, null);						
				}
			}
		}
		
	});
	
	var detalleResultadosRenderer = function( value, metadata, record, rowIndex, colIndex, store){
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;"';
		return value;		
	}
	
	var elementosResultadosNotificacion = [
		// LABEL: CON EL MENSAJE DE LOS RESULTADOS DE LA OPERACION
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			id:		'resultadoOperacion',
			style: {
				textAlign: 		'center',
				paddingBottom: '15px'
			},
			text: 	''
		},
		// TEXTAREA: CON EL DETALLE DE LOS RESULTADOS DE LA OPERACION
		{
			xtype:		'textarea',
			id:			'textoDetalleResultadosNotificacion',
			name:			'textoDetalleResultadosNotificacion',
			height:		300,
			style: {
				width:	'100%'
			},
			readOnly: 	true				
		}
		/*
		{
			store: 				detalleResultadosNotificacionData,
			xtype: 				'grid',
			id:					'gridDetalleResultadosNotificacion',
			stripeRows: 		true,
			loadMask: 			true,
			height: 				300,
			hideHeaders: 		true,
			region: 				'center',
			style: {
				borderWidth: 1
			},
			viewConfig: {
				forceFit: true
			},
			columns: [
				new Ext.grid.RowNumberer(),
				{
					header: 		'detalleResultados',
					tooltip: 	'detalleResultados',
					dataIndex: 	'DETALLE_RESULTADOS',
					sortable: 	true,
					resizable: 	true,
					hidden: 		false,
					hideable:	false,
					align:		'left',
					renderer: 	detalleResultadosRenderer
				}
			]
		}
		*/
	];
	
	var panelResultadosNotificacion = {
		title:			'Resultados de la Notificaci�n Masiva',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosNotificacion',
		collapsible: 	false,
		titleCollapse: false,
		bodyStyle: 		'padding: 16px',
		width: 			700, //949,
		height:			108,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosResultadosNotificacion,
		monitorValid: 	false,
		buttonAlign: 	'center',
		buttons: [
			{
				xtype: 		'button',
				text: 		'Aceptar',
				id: 			'btnAceptarResultadosNotificacion',
				iconCls: 	'icoAceptar',
				handler:		procesaAceptarResultadosNotificacion
			}
		]
	}
	
	var hidePanelResultadosNotificacion = function(){
		
		var panelResultadosNotificacion = Ext.getCmp("panelResultadosNotificacion");
		// Ocultar panel de resultados de la notificacion masiva
		if( panelResultadosNotificacion && panelResultadosNotificacion.isVisible() ){
			panelResultadosNotificacion.hide();
		}
		
	}
			
	//-------------------------- PANEL AVANCE NOTIFICACION ----------------------------
	
	var formateaPorcentajeAvance = function(porcentajeAvance){
		var porcentajeFormateado = Ext.util.Format.number( Number(porcentajeAvance), "0.00" ) + " %";
		return porcentajeFormateado;
	}
		
	var elementosAvanceNotificacion = [
		{
			xtype:  'panel',
			layout: {
				type: 'hbox',
				pack: 'center'
			},
			baseCls: 'x-plain',
			items: [
				{
					xtype: 	'label',
					width: 	'180',
					cls: 		'x-form-item',
					text: 	'N�mero Total de Notificaciones:'
				},
				{
					xtype: 	'label',
					width: 	'44',
					id:		'numeroTotalNotificaciones',
					name:		'numeroTotalNotificaciones',
					cls: 		'x-form-item',
					style:	{
						textAlign: 'right'
					}
				}
			]
		},
		{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				pack: 'center'
			},
			baseCls: 		'x-plain',
			items: [
				{
					xtype: 	'label',
					width: 	'180',
					cls: 		'x-form-item',
					text: 	'Notificaciones Realizadas:'
				},
				{
					xtype: 	'label',
					width: 	'44',
					id:		'notificacionesRealizadas',
					name:		'notificacionesRealizadas',
					cls: 		'x-form-item',
					style:	{
						textAlign: 		'right',
						paddingBottom: '30px'
					}
				}
			]
		},
		{
			xtype: 'progress',
			id:	 'barrarAvanceNotificacion',
			name:	 'barrarAvanceNotificacion',
			cls:	 'center-align',
			value: 0.0,
			text:  '0.00 %'
		}
		
	];
	
	var panelAvanceNotificacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelAvanceNotificacion',
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		items: 			elementosAvanceNotificacion,
		frame: 			false,
		border:			false,
		baseCls: 		'x-plain'
	});
	
	var showWindowAvanceNotificacion = function(numeroRegistros){
		
		var barrarAvanceNotificacion	= Ext.getCmp("barrarAvanceNotificacion");
		var notificacionesRealizadas	= Ext.getCmp("notificacionesRealizadas");
		var numeroTotalNotificaciones	= Ext.getCmp("numeroTotalNotificaciones");
 
		barrarAvanceNotificacion.updateProgress(0.00,"0.00 %",true);
		numeroTotalNotificaciones.setText(numeroRegistros); 
		notificacionesRealizadas.setText("0");
			
		var ventana = Ext.getCmp('windowAvanceNotificacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				resizable:		false,
				id: 				'windowAvanceNotificacion',
				modal:			true,
				closable: 		false,
				listeners: 		{
					show: function(component){
						var notificacionesRealizadas	= Ext.getCmp("notificacionesRealizadas");
						var numeroTotalNotificaciones	= Ext.getCmp("numeroTotalNotificaciones");
						notificacionesRealizadas.setWidth(numeroTotalNotificaciones.getWidth());
					}
				},
				items: [
					Ext.getCmp("panelAvanceNotificacion")
				]
			}).show();
			
		}
		
	}	
	
	var hideWindowAvanceNotificacion = function(){
		
		var ventanaAvanceNotificacion = Ext.getCmp('windowAvanceNotificacion');
		
		if(ventanaAvanceNotificacion && ventanaAvanceNotificacion.isVisible() ){
			
			var barrarAvanceNotificacion	= Ext.getCmp("barrarAvanceNotificacion");
			var notificacionesRealizadas	= Ext.getCmp("notificacionesRealizadas");
			var numeroTotalNotificaciones	= Ext.getCmp("numeroTotalNotificaciones");
			
			barrarAvanceNotificacion.reset();
			notificacionesRealizadas.setText("");
			numeroTotalNotificaciones.setText("");
			
			if (ventanaAvanceNotificacion) {
				ventanaAvanceNotificacion.hide();
			}
      }
      
	}
	
	//---------------------------------- 4. VENTANA REPORTE DISPERSION EPO ----------------------------------------
    
	var procesarSuccessFailureDispersionEPOPDF =  function(opts, success, response) {
		
		var btnGererarPDFDispersionEPO = Ext.getCmp('btnGererarPDFDispersionEPO');
		btnGererarPDFDispersionEPO.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajarPDFDispersionEPO = Ext.getCmp('btnBajarPDFDispersionEPO');
			btnBajarPDFDispersionEPO.enable();
			btnBajarPDFDispersionEPO.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFDispersionEPO.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(/^\/nafin/,'');
				// Insertar archivo
				Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					}
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
			});
			
		} else {
			
			btnGererarPDFDispersionEPO.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarSuccessFailureDispersionEPOExcel =  function(opts, success, response) {
		
		var btnGererarExcelDispersionEPO = Ext.getCmp('btnGererarExcelDispersionEPO');
		btnGererarExcelDispersionEPO.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajarExcelDispersionEPO = Ext.getCmp('btnBajarExcelDispersionEPO');
			btnBajarExcelDispersionEPO.enable();
			btnBajarExcelDispersionEPO.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarExcelDispersionEPO.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(/^\/nafin/,'');
				// Insertar archivo
				Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					}
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
			});
			
		} else {
			
			btnGererarExcelDispersionEPO.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var elementosReporteDispersionEPO = [
		{
			xtype: 		'button',
			text: 		'Generar PDF',
			id: 			'btnGererarPDFDispersionEPO',
			iconCls: 	'icoGenerarDocumento',
			handler: function(boton, evento) {
				
				boton.disable();
				boton.setIconClass('loading-indicator');
						
				var panelMonitoreoEPO 		 = Ext.getCmp("panelMonitoreoEPO");
				var gridDetalleMonitoreoEPO = Ext.getCmp("gridDetalleMonitoreoEPO");
				
				var formParams = Ext.apply({}, gridDetalleMonitoreoEPO.formParams );
				
				// Generar Archivo Excel
				Ext.Ajax.request({
					url: 		'15monitordispe01ext.data.jsp',
					params: 	Ext.apply(
						formParams, // panelMonitoreoEPO.getForm().getValues()
						{
							informacion: 'GenerarReporteDispersionEPO',
							tipo: 		 'PDF'
						}
					),
					callback: procesarSuccessFailureDispersionEPOPDF
				});

			},
			colspan: 2,
			width:   170,
			height:  25
		},	
		{
			xtype: 		'button',
			text: 		'Generar Excel',
			id: 			'btnGererarExcelDispersionEPO',
			iconCls: 	'icoGenerarDocumento',
			handler: function(boton, evento) {

				boton.disable();
				boton.setIconClass('loading-indicator');
						
				var panelMonitoreoEPO 		 = Ext.getCmp("panelMonitoreoEPO");
				var gridDetalleMonitoreoEPO = Ext.getCmp("gridDetalleMonitoreoEPO");
				
				var formParams = Ext.apply({}, gridDetalleMonitoreoEPO.formParams );
				
				// Generar Archivo Excel
				Ext.Ajax.request({
					url: 		'15monitordispe01ext.data.jsp',
					params: 	Ext.apply(
						formParams, // panelMonitoreoEPO.getForm().getValues()
						{
							informacion: 'GenerarReporteDispersionEPO',
							tipo: 		 'XLS'
						}
					),
					callback: procesarSuccessFailureDispersionEPOExcel
				});
				
			},
			colspan: 2,
			width:   170,
			height:  25
		},
		{
			xtype: 		'button',
			scale:	 	'small',
         iconAlign: 	'top',
         text: 		'Bajar PDF',
         id: 			'btnBajarPDFDispersionEPO',
			iconCls: 	'icoBotonPDF',
			disabled:	true
		},
		{
			xtype: 		'button',
			scale:	 	'small',
         iconAlign: 	'top',
         text: 		'Bajar Excel',
         id: 			'btnBajarExcelDispersionEPO',
			iconCls: 	'icoBotonXLS',
			disabled:	true
		}
	];
	
	var panelReporteDispersionEPO = new Ext.Panel({
		id: 					'panelReporteDispersionEPO',
		//width: 				170,
		frame: 				false,
		border:				false,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 16px',
		layout:				'table',
		defaultType: 		'button',
		baseCls: 			'x-plain',
		menu: 				undefined,
		split: 				false,
		layoutConfig: {
		  columns: 2
		},
		items: 				elementosReporteDispersionEPO,
		defaults: {               // defaults are applied to items, not the container
			width: '100%'
    	},
		monitorValid: 		false
	});

	var resetWindowReporteDispersionEPO = function(){
		
		var btnGererarExcelDispersionEPO = Ext.getCmp('btnGererarExcelDispersionEPO');
		var btnGererarPDFDispersionEPO 	= Ext.getCmp('btnGererarPDFDispersionEPO');
		var btnBajarExcelDispersionEPO 	= Ext.getCmp('btnBajarExcelDispersionEPO');
		var btnBajarPDFDispersionEPO 		= Ext.getCmp('btnBajarPDFDispersionEPO');
		
		btnGererarPDFDispersionEPO.setIconClass('icoGenerarDocumento');
		btnGererarExcelDispersionEPO.setIconClass('icoGenerarDocumento');
		
		btnGererarExcelDispersionEPO.enable();
		btnGererarPDFDispersionEPO.enable();
		btnBajarExcelDispersionEPO.disable();
		btnBajarPDFDispersionEPO.disable();
		
	}
	
	var hideWindowReporteDispersionEPO = function(){
		
		//resetWindowReporteDispersionEPO();
		var ventanaReporteDispersionEPO = Ext.getCmp('windowReporteDispersionEPO');
		if( ventanaReporteDispersionEPO && ventanaReporteDispersionEPO.isVisible() ){
			ventanaReporteDispersionEPO.hide();
      }
      
	}
	
	var showWindowReporteDispersionEPO = function(){
		
		var ventana = Ext.getCmp('windowReporteDispersionEPO');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				//layout: 		'fit',
				width: 			216,
				height: 			Ext.isIE7?167:152,
				minWidth: 		216,
				minHeight: 		Ext.isIE7?167:152,
				modal:			true,
				title: 			'Reporte de Dispersion EPO',
				id: 				'windowReporteDispersionEPO',
				closeAction: 	'hide',
				items: [
					Ext.getCmp("panelReporteDispersionEPO")
				],
				listeners: 		{
					hide: resetWindowReporteDispersionEPO
				}
			}).show();
		}
		
	}
	
	//----------------------------- 3. VENTANA ENVIAR CORREO A CLIENTE ----------------------------------------
    	
	var resetMensajeCorreo = function() {
		Ext.getCmp('mensajeCorreo').setValue('');
	}
	
	var resetBotonEnviarCorreoCliente = function(forceReset) {
 
		var btnEnviarCorreoCliente = Ext.getCmp('btnEnviarCorreoCliente');
		if( forceReset || btnEnviarCorreoCliente.disabled ){
			btnEnviarCorreoCliente.setIconClass('icoBotonEnviarCorreo');
			btnEnviarCorreoCliente.enable();
		}
		
	}
	
	var procesaEnviarCorreoCliente = function(boton, evento) {
		
		var gridDetalleMonitoreoEPO 	= Ext.getCmp("gridDetalleMonitoreoEPO");
		
		var claveEPO 			  			= gridDetalleMonitoreoEPO.formParams.claveEPO;
		var fechaReporte 	 	  			= gridDetalleMonitoreoEPO.formParams.fechaVencimientoDe;
							
		var respuesta = new Object();
		respuesta.claveEPO 	  = claveEPO;
		respuesta.fechaReporte = fechaReporte;
		monitorDispersion("ENVIO_CORREO_CLIENTE_FINALIZAR",respuesta);
		
	}
				
	var elementosEnviarCorreoCliente = [
		{
			xtype:	'displayfield',
			id:   	'mensajeCorreo',
			html: 	''
		}
	];
	
	var panelEnviarCorreoCliente = new Ext.Panel({
		id: 					'panelEnviarCorreoCliente',
		//width: 			170,
		height:				450,
		frame: 				false,
		border:				false,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 16px; overflow-y: scroll;',
		items: 				elementosEnviarCorreoCliente,
		defaults: {               // defaults are applied to items, not the container
			width: '100%'
    	},
		monitorValid: 		false,
		buttons: [
			{
				xtype: 			'button',
				text: 			'Enviar',
				id: 				'btnEnviarCorreoCliente',
				iconCls: 		'icoBotonEnviarCorreo',
				handler: 		procesaEnviarCorreoCliente
			}	
		]
	});

	var resetWindowEnviarCorreoCliente = function(){
		resetMensajeCorreo();
		resetBotonEnviarCorreoCliente(true);
	}
	
	var hideWindowEnviarCorreoCliente = function(){
		
		//resetWindowEnviarCorreoCliente();
		var ventanaEnviarCorreoCliente = Ext.getCmp('windowEnviarCorreoCliente');
		if( ventanaEnviarCorreoCliente && ventanaEnviarCorreoCliente.isVisible() ){
			ventanaEnviarCorreoCliente.hide();
      }
      
	}
	
	var showWindowEnviarCorreoCliente = function(){
		
		var ventana = Ext.getCmp('windowEnviarCorreoCliente');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				//layout: 		'fit',
				resizable:		false,
				width: 			640,
				height: 			480,
				minWidth: 		640,
				minHeight: 		480,
				modal:			true,
				title: 			'Vista Previa',
				id: 				'windowEnviarCorreoCliente',
				closeAction: 	'hide',
				items: [
					Ext.getCmp("panelEnviarCorreoCliente")
				],
				listeners: 		{
					hide: resetWindowEnviarCorreoCliente
				}
			}).show();
		}
		
	}

	//--------------------------------- 2. PANEL DE DETALLE MONITOREO EPO ---------------------------------------
 
	var resetBotonEnviarACliente = function(){
		var btnEnviarACliente = Ext.getCmp('btnEnviarACliente');
		if( btnEnviarACliente.disabled ){
			btnEnviarACliente.setIconClass('icoBotonEditarCorreo');
			btnEnviarACliente.enable();
			btnEnviarACliente.formParams = {};
		}
	}
		
	var procesarSuccessFailureGeneraPDF =  function(opts, success, response) {
		
		var btnGeneraPDF = Ext.getCmp('btnGeneraPDF');
		btnGeneraPDF.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaPDF = Ext.getCmp('btnBajaPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(/^\/nafin/,'');
				// Insertar archivo
				Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					}
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
			});
			
		} else {
			
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarSuccessFailureGeneraCSV =  function(opts, success, response) {
		
		var btnGeneraCSV = Ext.getCmp('btnGeneraCSV');
		btnGeneraCSV.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaCSV = Ext.getCmp('btnBajaCSV');
			btnBajaCSV.show();
			btnBajaCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaCSV.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(/^\/nafin/,'');
				// Insertar archivo
				Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					}
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
			});
			
		} else {
			
			btnGeneraCSV.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesaEnviarACliente = function(boton, evento) {
						
		var gridDetalleMonitoreoEPO = Ext.getCmp("gridDetalleMonitoreoEPO");
						
		// 1. Validar rango de fechas que corresponda a un s�lo d�a
		var esRangoFechaVencimientoUnDia = gridDetalleMonitoreoEPO.responseParams.esRangoFechaVencimientoUnDia;
		if( esRangoFechaVencimientoUnDia !== true ){
			Ext.Msg.alert("Aviso","Para usar esta caracter�stica, se requiere que la fecha de vencimiento abarque un s�lo d�a.");
			return;
		}
				 
		// 2. Determinar si se trata de TODAS las EPO o de una EPO.
		var todasLasEPO = gridDetalleMonitoreoEPO.responseParams.todasLasEPO;
							
		// 3. Se seleccionaron Todas las EPO
		if( todasLasEPO === true ){
								
			var eposSeleccionadas 		= gridDetalleMonitoreoEPO.getView().getSelectedGroupList(); // IC_EPO:FECHA_VENCIMIENTO
			var hayEPOsSeleccionadas   = eposSeleccionadas.length > 0?true:false;
			
			if( !hayEPOsSeleccionadas ){
						
				Ext.Msg.alert("Aviso","Favor de seleccionar una Cadena Productiva para realizar el env�o del detalle de Dispersiones.");
				return;
							
			} else {
				
				// Filtrar las lista EPO seleccionadas para que solo se env�e la clave de la EPO
				for(var i=0;i<eposSeleccionadas.length;i++){
					eposSeleccionadas[i] = eposSeleccionadas[i].replace(/:.*$/,""); 
				}
				
				var respuesta = new Object();
				respuesta.eposSeleccionadas = eposSeleccionadas;
				respuesta.fechaVencimiento  = gridDetalleMonitoreoEPO.formParams.fechaVencimientoDe;
				monitorDispersion("ENVIAR_CORREO_MASIVO_CLIENTE",respuesta);
									
			}
								
		// 4. Se seleccion� una EPO
		} else { 
								
			var existenParametrosDeDispersion = gridDetalleMonitoreoEPO.responseParams.existenParametrosDeDispersion;
						
			if( existenParametrosDeDispersion === true ){
								
				var claveEPO 			  = gridDetalleMonitoreoEPO.formParams.claveEPO;
				var fechaReporte 	 	  = gridDetalleMonitoreoEPO.formParams.fechaVencimientoDe;
							
				var respuesta = new Object();
				respuesta.claveEPO 	  = claveEPO;
				respuesta.fechaReporte = fechaReporte;
				monitorDispersion("ENVIAR_CORREO_CLIENTE",respuesta);
	
			} else {
							
				Ext.Msg.alert("Aviso","Falta parametrizar el proveedor y/o las cuentas de correo para el servicio de dispersi�n de la EPO en cuesti�n.");
				return;
								
			}
			
		}	
		
	}
	
	var procesarDetalleMonitoreoEPOData = function(store, registros, opts){
				
		var panelMonitoreoEPO = Ext.getCmp('panelMonitoreoEPO');
		panelMonitoreoEPO.el.unmask();
		
		var gridDetalleMonitoreoEPO = Ext.getCmp('gridDetalleMonitoreoEPO');
		
		if (registros != null) {
			
			/*
			if (!gridDetalleMonitoreoEPO.isVisible()) {
				gridDetalleMonitoreoEPO.show();
			}
			*/
			
			var btnReporteDispersionEPO = Ext.getCmp('btnReporteDispersionEPO');
			var btnEnviarACliente 		 = Ext.getCmp('btnEnviarACliente');
			var btnGeneraPDF 				 = Ext.getCmp('btnGeneraPDF');
			var btnGeneraCSV				 = Ext.getCmp('btnGeneraCSV');
			var btnBajaPDF					 = Ext.getCmp('btnBajaPDF');
			var btnBajaCSV					 = Ext.getCmp('btnBajaCSV');		
 
			var el 					 		 = gridDetalleMonitoreoEPO.getGridEl();
			
			// Copiar en el grid los parametros de la ultima consulta
			gridDetalleMonitoreoEPO.formParams = Ext.apply({},opts.params);
			// Copiar en el grid parametros adicionales asociados a la consulta
			gridDetalleMonitoreoEPO.responseParams = {};
			gridDetalleMonitoreoEPO.responseParams.esRangoFechaVencimientoUnDia	= store.reader.jsonData.esRangoFechaVencimientoUnDia;
			gridDetalleMonitoreoEPO.responseParams.todasLasEPO 						= store.reader.jsonData.todasLasEPO;
			gridDetalleMonitoreoEPO.responseParams.existenParametrosDeDispersion = store.reader.jsonData.existenParametrosDeDispersion;
			// Resetear copia de parametros de consulta en el boton de enviar a cliente
			btnEnviarACliente.formParams = {};
			
			// Actualizar propiedades del grid panel
			if(store.getTotalCount() > 0) {
				
				btnReporteDispersionEPO.enable();
				btnEnviarACliente.enable();
				btnGeneraPDF.enable();
				btnGeneraCSV.enable();
				btnBajaPDF.hide();
				btnBajaCSV.hide();
				
				el.unmask();
				
			} else {
				
				btnReporteDispersionEPO.disable();
				btnEnviarACliente.disable();
				btnGeneraPDF.disable();
				btnGeneraCSV.disable();
				btnBajaPDF.hide();
				btnBajaCSV.hide();
				
				el.unmask();
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
 
		}
					
	};
	
	var numeroNafinElectronicoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var nombreIFRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var nombrePYMERenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var estatusRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var fechaVencimientoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var numeroTotalDocumentosRenderer  = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var nombreMonedaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var montoTotalRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="text-align:right;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		value = record.json['MONTO_TOTAL'];
		return value;
		
	}
		
	var codigoBancoCuentaClabeRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var cuentaClabeSwiftRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr = 'style="color: red;" ';
		}
		return value;
		
	}
	
	var descripcionCECOBANRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
	
	var descripcionDOLARESRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
		
	}
  
	// Crear JsonStore que se encargar� de cargar el detalle
	var detalleMonitoreoEPOData = new Ext.data.GroupingStore({
		id:					'detalleMonitoreoEPODataStore',
		name:					'detalleMonitoreoEPODataStore',
		root:					'registros',
		url: 					'15monitordispe01ext.data.jsp',
		autoDestroy:		true,
		baseParams: {
			informacion: 	'ConsultaDetalleMonitoreoEPO'
		},			
		reader: new Ext.data.JsonReader({
			root: 			'registros',	
			totalProperty: 'total',
			fields: [
				{ name:"GROUP_ID",							type:"string" }, // IC_EPO + ":" + FECHA_VENCIMIENTO
				{ name:'IC_EPO',								type:"string" },
				{ name:'NOMBRE_EPO',							type:"string" },
				{ name:'NUMERO_NAFIN_ELECTRONICO',		type:"string" },
				{ name:'NOMBRE_IF',							type:"string" },
				{ name:'NOMBRE_PYME',						type:"string" },
				{ name:'ESTATUS',								type:"string" },
				{ name:'FECHA_VENCIMIENTO',				type:"string" },
				{ name:'NUMERO_TOTAL_DOCUMENTOS',		type:"int"    },
				{ name:'NOMBRE_MONEDA',						type:"string" },
				{ name:'MONTO_TOTAL',						type:"float", convert: function(value, record){ return Number(value.replace(/[\$,]/g,"")); } },
				{ name:'CODIGO_BANCO_CUENTA_CLABE',		type:"string" },
				{ name:'CUENTA_CLABE_SWIFT',				type:"string" },
				{ name:'DESCRIPCION_CECOBAN',				type:"string" },
				{ name:'DESCRIPCION_DOLARES',				type:"string" },
				{ name:'ESTATUS_CECOBAN',					type:"string" }
			]
		}),
		groupField: 			'GROUP_ID', 
		sortInfo: 				{ field: 'NOMBRE_EPO', direction: "ASC" },
		autoLoad: 				false,
		totalProperty: 		'total',
		messageProperty: 		'msg',
		pruneModifiedRecords: true, // Para que no se conserven los registros modificados en consulta anterior.
		listeners: { 
			beforeload: {
				fn: function( store, opts ) {
					var grid = Ext.getCmp('gridDetalleMonitoreoEPO');
					if (grid) {
						grid.show();
					} 			
				}
			},
			load: 	procesarDetalleMonitoreoEPOData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarDetalleMonitoreoEPOData(null, null, null);						
				}
			}
		}
	});
    
	// Crear plugin que se encargar� de desplegar los totales
	var totalesPorGrupoSummary = new Ext.ux.grid.ServerSummary();
	
	var gridDetalleMonitoreoEPO = {
		frame:			true,
		store: 			detalleMonitoreoEPOData,
		//title:			'Detalle Monitoreo EPO',
		xtype: 			'grid',
		id:				'gridDetalleMonitoreoEPO',
		hidden:			true,
		stripeRows: 	true,
		loadMask: 		true,
		width: 			940, 
		height:			480,
		region: 			'center',
		style: 			'margin: 0 auto',
		formParams:		{},
		responseParams: {},
		view:		 		new Ext.grid.SelectableGroupingView(
			// TODO: Se podr�a mejorar el checkbox de seleccion de grupos de tal manera que si se recarga y el estatus del objeto original
			// es checkbox = false, readonly=false y el usuario lo tiene seleccionado, entonces que aparezca seleccionado
			// Esto se puede hacer en la peticion de recarga en el servidor.
			{
				forceFit:				false, 
				groupTextTpl: 			'{text}', // Usar group text renderer
				hideGroupedColumn: 	true,
				enableGroupingMenu:	false,
				groupSelectionText:	'Enviar a Cliente',
				startGroup: new Ext.XTemplate( 
					'<div id="{groupId}" class="x-grid-group {cls} {clsSelectedCheckBox}" >', 
                	 '<div id="{groupId}-hd" class="x-grid-group-hd" style="{style}">'+
                    		'<div class="x-grid-group-title" >{text}<br/>', 
                    			'<table cellspacing="0" cellpadding="0" border="0" >',
                    				'<tr>',
                    				'<td id="{groupId}-checkboxhd" width="90" class="x-grid-checkboxhd {clsReadOnlyCheckBox}" style="padding-left:0;">',
                    						'Enviar a Cliente',
                    					'</td>',
                    				'</tr>',
                    			'</table>',
                    		'</div>',
                    '</div>',
                '<div id="{groupId}-bd" class="x-grid-group-body">'
            )
			}),
		plugins: totalesPorGrupoSummary,
		columns: [
			{
				header: 		'CLAVE IC_EPO FECHA_VENCIMIENTO',
				tooltip: 	'CLAVE IC_EPO FECHA_VENCIMIENTO',
				dataIndex: 	'GROUP_ID', // IC_EPO + ":" + FECHA_VENCIMIENTO 
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				// width: 		315,
				hidden: 		true,
				hideable:	false,
				groupTextRenderer: function( value, metadata, record, rowIndex, colIndex, store){
					return record.data['NOMBRE_EPO'];
				}
			},
			{
				header: 		'Num. Nafin<br>Electr&oacute;nico',
				tooltip: 	'Num. Nafin Electr&oacute;nico',
				dataIndex: 	'NUMERO_NAFIN_ELECTRONICO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroNafinElectronicoRenderer
			},
			{
				header: 		'IF<br>&nbsp;',
				tooltip: 	'IF',
				dataIndex: 	'NOMBRE_IF',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	nombreIFRenderer
			},
			{
				header: 		'PYME<br>&nbsp;',
				tooltip: 	'PYME',
				dataIndex: 	'NOMBRE_PYME',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	nombrePYMERenderer
			},
			{
				header: 		'Estatus<br>&nbsp;',
				tooltip: 	'Estatus',
				dataIndex: 	'ESTATUS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	estatusRenderer
			},
			{
				header: 		'Fecha de<br>Vencimiento',
				tooltip: 	'Fecha de Vencimiento',
				dataIndex: 	'FECHA_VENCIMIENTO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	fechaVencimientoRenderer
			},
			{
				header: 		'Num. Total<br>de Doctos.',
				tooltip: 	'Num. Total de Doctos.',
				dataIndex: 	'NUMERO_TOTAL_DOCUMENTOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroTotalDocumentosRenderer
			},
			{
				header: 		'Moneda<br>&nbsp;',
				tooltip: 	'Moneda',
				dataIndex: 	'NOMBRE_MONEDA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				renderer:	nombreMonedaRenderer
			},
			{
				header: 		'Monto Total<br>&nbsp;',
				tooltip: 	'Monto Total',
				dataIndex: 	'MONTO_TOTAL',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	montoTotalRenderer
			},
			{
				header: 		'C&oacute;digo Banco<br>Cuenta Clabe',
				tooltip: 	'C&oacute;digo Banco Cuenta Clabe',
				dataIndex: 	'CODIGO_BANCO_CUENTA_CLABE',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				renderer:	codigoBancoCuentaClabeRenderer
			},
			{
				header: 		'Cuenta CLABE/SWIFT<br>&nbsp;',
				tooltip: 	'Cuenta CLABE/SWIFT',
				dataIndex: 	'CUENTA_CLABE_SWIFT',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		134,
				hidden: 		false,
				renderer:	cuentaClabeSwiftRenderer
			},
			{
				header: 		'Descripci&oacute;n<br>CECOBAN',
				tooltip: 	'Descripci&oacute;n CECOBAN',
				dataIndex: 	'DESCRIPCION_CECOBAN',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	descripcionCECOBANRenderer
			},
			{
				header: 		'Descripci&oacute;n<br>D&Oacute;LARES',
				tooltip: 	'Descripci&oacute;n D&Oacute;LARES',
				dataIndex: 	'DESCRIPCION_DOLARES',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	descripcionDOLARESRenderer
			}
		],
		stateful:	false,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'left',
			items: [
				'->',
				{
					xtype: 	'button',
					text: 	'Reporte Dispersion EPO',
					id: 		'btnReporteDispersionEPO',
					iconCls: 'icoBotonReporte',
					handler: function(boton, evento) {
						showWindowReporteDispersionEPO();
					}
				},	
				{
					xtype: 		'button',
					text: 		'Enviar a Cliente',
					id: 			'btnEnviarACliente',
					iconCls: 	'icoBotonEditarCorreo',
					handler: 	procesaEnviarACliente,
					formParams:	{}
				},
				'-',
				{
					xtype: 	'button',
					text: 	'Generar PDF',
					id: 		'btnGeneraPDF',
					iconCls: 'icoGenerarDocumento',
					handler: function(boton, evento) {
						
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						var panelMonitoreoEPO 		 = Ext.getCmp("panelMonitoreoEPO");
						var gridDetalleMonitoreoEPO = Ext.getCmp("gridDetalleMonitoreoEPO");
						
						var formParams = Ext.apply({}, gridDetalleMonitoreoEPO.formParams );
						
						// Generar Archivo PDF
						Ext.Ajax.request({
							url: 		'15monitordispe01ext.data.jsp',
							params: 	Ext.apply(
								formParams, // panelMonitoreoEPO.getForm().getValues()
								{
									informacion: 'GenerarArchivo',
									tipo: 		 'PDF'
								}								
							),
							callback: procesarSuccessFailureGeneraPDF
						});
						
					}
				},
				{
					xtype: 	'button',
					text: 	'Bajar PDF',
					id: 		'btnBajaPDF',
					iconCls:	'icoBotonPDF',
					hidden: 	true
				},
				'-',
				{
					xtype: 	'button',
					text: 	'Generar Archivo',
					id: 		'btnGeneraCSV',
					iconCls:	'icoGenerarDocumento',
					handler: function(boton, evento) {
								
						boton.disable();
						boton.setIconClass('loading-indicator');
									
						var panelMonitoreoEPO 		 = Ext.getCmp("panelMonitoreoEPO");
						var gridDetalleMonitoreoEPO = Ext.getCmp("gridDetalleMonitoreoEPO");
						
						var formParams = Ext.apply({}, gridDetalleMonitoreoEPO.formParams );
						
						// Generar Archivo CSV
						Ext.Ajax.request({
							url: 		'15monitordispe01ext.data.jsp',
							params: 	Ext.apply(
								formParams, // panelMonitoreoEPO.getForm().getValues()
								{
									informacion: 'GenerarArchivo',
									tipo: 		 'CSV'
								}								
							),
							callback: procesarSuccessFailureGeneraCSV
						});
							
					}
				},
				{
					xtype: 	'button',
					text: 	'Bajar Archivo',
					id: 		'btnBajaCSV',
					iconCls:	'icoBotonXLS',
					hidden: 	true
				}
			]
		}
	};
	
	//------------------------------------- 1. PANEL DE MONITOREO EPO ------------------------------------------

	var procesaConsultar = function(boton,evento){
		
		// Por compatibilidad con la versi�n anterior
		var comboTipoMonitoreo = Ext.getCmp("comboTipoMonitoreo");
		if( comboTipoMonitoreo.getValue() !== "E" ){
			return;	
		}
		
		// Validar toda la forma
		var panelMonitoreoEPO = Ext.getCmp("panelMonitoreoEPO");
		if( !panelMonitoreoEPO.getForm().isValid() ){
			return;
		}
 
		/* 
			Nota: las siguientes validaciones ya fueron incluidas en la validacion
			'rangofecha':
				"Debe capturar un rango para la fecha de vencimiento."
				"La primera fecha en el rango debe ser menor a la segunda"
		*/
		
		// Agregar m�scara
		panelMonitoreoEPO.getEl().mask('Consultando...','x-mask-loading');
		
		// Si hay una m�scara previa en el grid, suprimirla
		var gridEl = Ext.getCmp("gridDetalleMonitoreoEPO").getGridEl();
		if( gridEl.isMasked()){
			gridEl.unmask();
		}
		
		// Realizar consulta
		var detalleMonitoreoEPOData = Ext.StoreMgr.key('detalleMonitoreoEPODataStore');
		detalleMonitoreoEPOData.load({
			params: Ext.apply(
				{
					informacion: 'ConsultaDetalleMonitoreoEPO'
				},
				panelMonitoreoEPO.getForm().getValues()
			)
		});
		 
	}
	
	var procesaLimpiar = function(boton,evento){
		Ext.getCmp("panelMonitoreoEPO").getForm().reset();
		Ext.getCmp("fechaVencimientoDe").setMaxValue('');
		Ext.getCmp("fechaVencimientoA").setMinValue('');
	}
	
	var catalogoTipoMonitoreoData = new Ext.data.JsonStore({
		id: 					'catalogoTipoMonitoreoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispe01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoTipoMonitoreo'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboTipoMonitoreo = Ext.getCmp("comboTipoMonitoreo");
						comboTipoMonitoreo.setValue(defaultValue);
						comboTipoMonitoreo.originalValue = comboTipoMonitoreo.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 					'catalogoEPODataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordispe01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoEPO'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboEPO = Ext.getCmp("comboEPO");
						comboEPO.setValue(defaultValue);
						comboEPO.originalValue = comboEPO.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementosPanelMonitoreoEPO = [
		// COMBO TIPO DE MONITOREO
		{
			xtype: 				'combo',
			name: 				'tipoMonitoreo',
			id: 					'comboTipoMonitoreo',
			fieldLabel: 		'Tipo de Monitoreo',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveTipoMonitoreo',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTipoMonitoreoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'65%',
			listeners:			{
				select:	function( combo, record, index ){
					
					var respuesta = new Object();
					respuesta['tipoMonitoreo'] = combo.getValue();
					monitorDispersion("MOSTRAR_PANTALLA_MONITOREO",respuesta);
					
				}
			}
		},
		// COMBO EPO
		{
			xtype: 				'combo',
			name: 				'epo',
			id: 					'comboEPO',
			fieldLabel: 		'EPO',
			allowBlank:			false,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPO',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			anchor:				'95%',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>'
		},
		// DATERANGE FECHA DE VENCIMIENTO
		{
			xtype: 						'compositefield',
			fieldLabel: 				'Fecha de Vencimiento',
			combineErrors: 			false,
			msgTarget: 					'side',
			items: [
				{
					xtype: 				'datefield',
					name: 				'fechaVencimientoDe',
					id: 					'fechaVencimientoDe',
					allowBlank: 		true,
					startDay: 			0,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoFinFecha: 	'fechaVencimientoA',
					margins: 			'0 20 0 0',
					allowBlank:			false
				},{
					xtype: 				'displayfield',
					value: 				'a',
					width: 				20
				},{
					xtype: 				'datefield',
					name: 				'fechaVencimientoA',
					id: 					'fechaVencimientoA',
					allowBlank: 		true,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoInicioFecha: 'fechaVencimientoDe',
					margins: 			'0 20 0 0',
					allowBlank:			false
				}
			]
		}
	];
	
	var panelMonitoreoEPO = new Ext.form.FormPanel({
		id: 					'panelMonitoreoEPO',
		width: 				500,
		title: 				'Monitor EPO',
		frame: 				true,
		collapsible: 		true,
		titleCollapse: 	true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px',
		defaults: {	
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		labelWidth: 		130,
		defaultType: 		'textfield',
		trackResetOnLoad:	true,
		items: 				elementosPanelMonitoreoEPO,
		monitorValid: 		false,
		buttons: [
			{
				text: 		'Consultar',
				iconCls: 	'icoBuscar',
				handler: 	procesaConsultar
			},
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	procesaLimpiar
			}
		]
	});
	
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelMonitoreoEPO,
			NE.util.getEspaciador(10),
			gridDetalleMonitoreoEPO,
			panelResultadosNotificacion
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	monitorDispersion("INICIALIZACION",null);

});