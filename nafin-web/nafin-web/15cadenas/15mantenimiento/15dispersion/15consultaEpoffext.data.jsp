<%@ page contentType="application/json;charset=UTF-8" import="   
   java.util.*, java.sql.*,   
	netropology.utilerias.*,   
	com.netro.exception.*,  
	javax.naming.*, 
	net.sf.json.JSONArray,   
	net.sf.json.JSONObject" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<% 

String fechaRegIni            = request.getParameter("dFechaRegIni");         if (fechaRegIni            == null) { fechaRegIni           = ""; }
String fechaRegFin            = request.getParameter("dFechaRegFin");         if (fechaRegFin            == null) { fechaRegFin           = ""; }
String tipoDispersion         = "E";
String viaLiq                 = "A";

String	ic_flujo_fondos 	= (request.getParameter("ic_flujo_fondos")  == null)?"":request.getParameter("ic_flujo_fondos"),
			ic_epo 				= (request.getParameter("ic_epo")           == null)?"":request.getParameter("ic_epo"),
			ic_pyme				= (request.getParameter("ic_pyme")          == null)?"":request.getParameter("ic_pyme"),
			ic_estatus_docto 	= (request.getParameter("ic_estatus_docto") == null)?"":request.getParameter("ic_estatus_docto"),
			df_fecha_venc 		= (request.getParameter("df_fecha_venc")    == null)?"":request.getParameter("df_fecha_venc"),
			df_operacion 		= (request.getParameter("df_operacion")     == null)?"":request.getParameter("df_operacion");


int start                     = 0;
int limit                     = 0;
JSONObject resultado 	      = new JSONObject();

String informacion            =(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion              =(request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String infoRegresar           = "";

if (informacion.equals("Consulta") || informacion.equals("ConsultaTotales") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")|| informacion.equals("ArchivoPDFCT")) {
   com.netro.cadenas.ConsDispersionEPO paginador = new com.netro.cadenas.ConsDispersionEPO();
	paginador.setTipoDispersion(tipoDispersion);
	paginador.setFechaRegIni(fechaRegIni);
	paginador.setFechaRegFin(fechaRegFin);
	paginador.setViaLiq(viaLiq);
	paginador.setIcEpo(iNoCliente);
	paginador.setIcIf("");
	paginador.setEstatus("2");
	paginador.setEstatysFF("1");
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";
	
	if(informacion.equals("Consulta")) {
	 try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);															   		
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		
				resultado = JSONObject.fromObject(consulta);
				infoRegresar = resultado.toString();
	}
	else if (informacion.equals("ConsultaTotales") ) {
		
		queryHelper = new CQueryHelperRegExtJS(paginador); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion	
	}
	else if (informacion.equals("ArchivoCSV")) {
	 try {
	  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
	  JSONObject jsonObj = new JSONObject();
	  jsonObj.put("success", new Boolean(true));
	  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	  infoRegresar = jsonObj.toString();
	  } 
	  catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
	  }
	}
	else if (informacion.equals("ArchivoPDF")) {
	 try {
	  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"PDF");
	  JSONObject jsonObj = new JSONObject();
	  jsonObj.put("success", new Boolean(true));
	  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	  infoRegresar = jsonObj.toString();
	  } 
	  catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
	  }
	}
	else if (informacion.equals("ArchivoPDFCT")) {
	 try {
	  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"PDF_ComprobanteTrans");
	  JSONObject jsonObj = new JSONObject();
	  jsonObj.put("success", new Boolean(true));
	  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	  infoRegresar = jsonObj.toString();
	  } 
	  catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
	  }
	}
}
if (informacion.equals("ConsultaDetalle") || informacion.equals("ConsultaDetalleTotales") || informacion.equals("ArchivoDetalleCSV")) {
   com.netro.cadenas.ConsDispersionEPODet paginador = new com.netro.cadenas.ConsDispersionEPODet();
	paginador.setIcFlujoFondos(ic_flujo_fondos);
	paginador.setIcEpo(ic_epo);
	paginador.setIcPyme(ic_pyme);
	paginador.setIcEstatusDocto(ic_estatus_docto);
	paginador.setDfFechaVenc(df_fecha_venc);
	paginador.setDfOperacion(df_operacion);				
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";
	
	if(informacion.equals("ConsultaDetalle")) {
	 try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);															   		
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		
				resultado = JSONObject.fromObject(consulta);
				infoRegresar = resultado.toString();
	}
	else if (informacion.equals("ConsultaDetalleTotales") ) {
		
		queryHelper = new CQueryHelperRegExtJS(paginador); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion
	}
	else if (informacion.equals("ArchivoDetalleCSV")) {
	 try {
	  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
	  JSONObject jsonObj = new JSONObject();
	  jsonObj.put("success", new Boolean(true));
	  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	  infoRegresar = jsonObj.toString();
	  } 
	  catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
	  }
	}
}

%>
<%=infoRegresar%>
