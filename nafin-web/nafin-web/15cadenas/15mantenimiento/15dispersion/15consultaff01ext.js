Ext.onReady(function() {
		
	Ext.namespace('NE.consultaff');
			
	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaConsultaFlujoFondos = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						consultaFlujoFondos(resp.estadoSiguiente,resp);
					}
				);
			} else {
				consultaFlujoFondos(resp.estadoSiguiente,resp);
			}
			
		} else {
			
			var element;
			
			// Suprimir m�scara del Grid de Resumen de operaciones
			element = Ext.getCmp("gridResumenOperacion").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			/*
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Resetear forma
         resetWindowConfirmacionClave();
 
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			*/
					
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var consultaFlujoFondos = function(estado, respuesta ){
 
		
		if(			estado == "INICIALIZACION"							   ){
			
			// Debido a un bug en el layout de los composite fields para IE7
			if( Ext.isIE7 ) Ext.getCmp("panelFormaConsulta").hide();

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15consultaff01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaFlujoFondos.inicializacion'
				},
				callback: 				procesaConsultaFlujoFondos
			});
			
		} else if(	estado == "INICIALIZAR_FORMA_CONSULTA"		   	){		
			
			// Mostrar panel con la forma de consulta
			Ext.getCmp("panelFormaConsulta").show();
			
			// Cargar Catalogo Concepto
			var catalogoConceptoData       	= Ext.StoreMgr.key('catalogoConceptoDataStore')
			catalogoConceptoData.load();
			
			// Cargar Catalogo Tipo de Dispersion
			var catalogoTipoDispersionData 	= Ext.StoreMgr.key('catalogoTipoDispersionDataStore');
			catalogoTipoDispersionData.load();
 
			// Borrar Contenido del Catalogo IF
			Ext.StoreMgr.key('catalogoIFDataStore').removeAll();

			// Borrar Contenido Campo Hidden ifOperaFideicomiso
			var ifOperaFideicomiso	            = Ext.getCmp("ifOperaFideicomiso");
			ifOperaFideicomiso.setValue('');
			
			// Borrar Contenido del Catalogo EPO
			Ext.StoreMgr.key('catalogoEPODataStore').removeAll();

			// Borrar Contenido del Catalogo Estatus
			Ext.StoreMgr.key('catalogoEstatusDataStore').removeAll();

			// Cargar Catalogo Via de Liquidacion
			var catalogoViaLiquidacionData 	= Ext.StoreMgr.key('catalogoViaLiquidacionDataStore');
			catalogoViaLiquidacionData.load();

			var fechaHoy 				            = Ext.getCmp("fechaHoy");
			fechaHoy.setValue(	               respuesta.fechaHoy 	  );
			fechaHoy.originalValue	            = fechaHoy.getValue();
			
			var fechaRegistroInicial 				= Ext.getCmp("fechaRegistroInicial");
			fechaRegistroInicial.setValue(	   respuesta.fechaRegistroInicial 	  );
			fechaRegistroInicial.originalValue	= fechaRegistroInicial.getValue();
			
			var fechaRegistroFinal					= Ext.getCmp("fechaRegistroFinal"  );
			fechaRegistroFinal.setValue(	      respuesta.fechaRegistroFinal		  );
			fechaRegistroFinal.originalValue		= fechaRegistroFinal.getValue();
	
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15consultaff01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaFlujoFondos.inicializarFormaConsulta'
				},
				callback: 				procesaConsultaFlujoFondos
			});
 	
		} else if(			estado == "ESPERAR_DECISION"  					  ){ 

		} else if(			estado == "FIN"  					                 ){
 	
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"15consultaff01ext.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			forma.submit();
						
		}
		
	}
	
	//-------------------- 3. WINDOW DETALLE DOCUMENTOS -----------------
	 
	var changeColumnVisibility = function(columnId,columnModel,visible){
		var columnIndex = columnModel.getIndexById(columnId);
		columnModel.config[columnIndex].hideable = visible;
		columnModel.setHidden( columnIndex, !visible );
	}
	
	var reconfigureColumnsDetalleDocumentos = function(params){
		
		var columnModel = Ext.getCmp("gridDetalleDocumentos").getColumnModel();
		
		if(        "PA" === params.claveEstatus        ) {
			// Fecha de Aplicaci�n	: visible
			changeColumnVisibility('columnFechaAplicacion',	columnModel,true	);
			// Num. de Documento		: hidden
			changeColumnVisibility('columnNumeroDocumento',	columnModel,false	);
			// PyME						: visible
			changeColumnVisibility('columnPyme',				columnModel,true	);
			// Importe del Pago		: visible
			changeColumnVisibility('columnImportePago',		columnModel,true	);
			// Importe Documento		: hidden
			changeColumnVisibility('columnImporteDocumento',columnModel,false	);
			// Importe Descuento		: hidden
			changeColumnVisibility('columnImporteDescuento',columnModel,false	);
		} else if( "P" !== params.claveTipoDispersion ) {
			// Fecha de Aplicaci�n	: hidden
			changeColumnVisibility('columnFechaAplicacion',	columnModel,false	);
			// Num. de Documento		: visible
			changeColumnVisibility('columnNumeroDocumento',	columnModel,true	);
			// PyME						: visible
			changeColumnVisibility('columnPyme',				columnModel,true	);
			// Importe del Pago		: hidden
			changeColumnVisibility('columnImportePago',		columnModel,false	);
			// Importe Documento		: visible
			changeColumnVisibility('columnImporteDocumento',columnModel,true	);
			// Importe Descuento		: visible
			changeColumnVisibility('columnImporteDescuento',columnModel,true	);
		} else {
			// Fecha de Aplicaci�n	: hidden
			changeColumnVisibility('columnFechaAplicacion',	columnModel,false	);
			// Num. de Documento		: visible
			changeColumnVisibility('columnNumeroDocumento',	columnModel,true	);
			// PyME						: visible
			changeColumnVisibility('columnPyme',				columnModel,true	);
			// Importe del Pago		: hidden
			changeColumnVisibility('columnImportePago',		columnModel,false	);
			// Importe Documento		: visible
			changeColumnVisibility('columnImporteDocumento',columnModel,true	);
			// Importe Descuento		: hidden
			changeColumnVisibility('columnImporteDescuento',columnModel,false	);
		}	
		
	}

	var procesarSuccessFailureGenerarArchivoDetalleCSV =  function(opts, success, response) {
		
		var btnGenerarArchivoDetalleCSV = Ext.getCmp('btnGenerarArchivoDetalleCSV');
		btnGenerarArchivoDetalleCSV.setIconClass('icoGenerarArchivo');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajarDetalleCSV = Ext.getCmp('btnBajarDetalleCSV');
			btnBajarDetalleCSV.show();
			btnBajarDetalleCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarDetalleCSV.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot) ),'');
				// Insertar archivo
				Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					}
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
			});
			
		} else {
			
			btnGenerarArchivoDetalleCSV.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesaGenerarArchivoDetalleCSV = function(boton, evento) {
						
		boton.disable();
		boton.setIconClass('loading-indicator');
 
		var formParams = Ext.apply({}, Ext.getCmp("gridDetalleDocumentos").formParams);
						
		// Generar Archivo CSV
		Ext.Ajax.request({
			url: 		'15consultaff01ext.data.jsp',
			params: 	Ext.apply(
				formParams, 
				{
					informacion: 'GenerarArchivoDetalleDocumentos',
					tipoArchivo: 'CSV'
				}
			),
			callback: procesarSuccessFailureGenerarArchivoDetalleCSV
		});
						
	}
			
	var procesaCerrar = function(boton,evento){
		
		// Ocultar ventana de detalle
		hideWindowDetalleDocumentos();
		 
	}
		
	var procesarConsultaDetalleDocumentos = function(store, registros, opts){

		var gridDetalleDocumentos = Ext.getCmp('gridDetalleDocumentos');
		var el 						  = gridDetalleDocumentos.getGridEl();
		el.unmask();
		
		if (registros != null) {
			
			/*
			if (!gridDetalleDocumentos.isVisible()) {
				gridDetalleDocumentos.show();
			}		
			*/
			
			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro.', 'x-mask');
			} else {
				// Habilitar boton para descargar archivo
				Ext.getCmp("btnGenerarArchivoDetalleCSV").enable();
			}
			
			// Copiar los formParams
			gridDetalleDocumentos.formParams    = Ext.apply({},opts.params);
			
			// Reconfigurar las columnas que ser�n mostradas
			reconfigureColumnsDetalleDocumentos(gridDetalleDocumentos.formParams);
			
			// Mostrar ocultar label sobre notas de credito
			var jsonData = store.reader.jsonData;
			if( jsonData.showLabelNotaCredito ){
				Ext.getCmp("detalleNotaCredito").show();
			} else {
				Ext.getCmp("detalleNotaCredito").hide();
			}
 
		}
 
	}
	
	var detalleDocumentosData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'detalleDocumentosDataStore',
		url: 		'15consultaff01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetalleDocumentos'
		},
		idIndex: 		1,
		fields: [	
			{ name: "FECHA_APLICACION", 	type: "string" },//type: 'date',  convert: function(value, record){ return ( record.json['RENGLON'] === 'false' || Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } 	}, // Fecha de Aplicaci�n
			{ name: "NUMERO_DOCUMENTO", 	type: "string" 																																		}, // Num. de Documento
			{ name: "PYME", 					type: "string" 																																		}, // PyME
			{ name: "IMPORTE_PAGO", 		type: 'float', convert: function(value, record){ return Number(value.replace(/[\$, ]/g,'')); } 								}, // Importe del Pago
			{ name: "IMPORTE_DOCUMENTO", 	type: 'float', convert: function(value, record){ return Number(value.replace(/[\$, ]/g,'')); } 								}, // Importe Documento
			{ name: "IMPORTE_DESCUENTO", 	type: 'float', convert: function(value, record){ return Number(value.replace(/[\$, ]/g,'')); } 								}, // Importe Descuento
			{ name: "RENGLON",            type: 'boolean' } // Indica si la columna corresponde con un renglon comun de datos, o un registro de totales
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
					showWindowDetalleDocumentos();
				}
			},
			load: 	procesarConsultaDetalleDocumentos,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDetalleDocumentos(null, null, null);						
				}
			}
		}
		
	});
	

	var fechaAplicacionRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align: left;';
		if( record.data['RENGLON'] === false ){
			metadata.attr += ' background: #f1f2f4;';
		}
		metadata.attr += '" ';
		value         = Ext.isEmpty(value)?'':value.format("d/m/Y");
		return value;
			
	}
	
	var numeroDocumentoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align: left;';
		if( record.data['RENGLON'] === false ){
			metadata.attr += ' background: #f1f2f4;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var pymeRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align: left;';
		if( record.data['RENGLON'] === false ){
			metadata.attr += ' background: #f1f2f4;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var importePagoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align: right;" ';
		value 		  = record.json['IMPORTE_PAGO'];
		return value;
			
	}
 
	var importeDocumentoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align: right;" ';
		value 		  = record.json['IMPORTE_DOCUMENTO'];
		return value;
			
	}
	
	var importeDescuentoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align: right;" ';
		value 		  = record.json['IMPORTE_DESCUENTO'];
		return value;
			
	}
	
	var gridDetalleDocumentosColModel = new Ext.grid.ColumnModel({
		columns: [
			{
				header: 		'Fecha de Aplicaci�n',
				tooltip: 	'Fecha de Aplicaci�n',
				dataIndex: 	'FECHA_APLICACION',
				id:			'columnFechaAplicacion',
				sortable: 	false,
				resizable: 	true,
				width: 		80,
				hidden: 		false,
				align:		'center',
				renderer:	fechaAplicacionRenderer
			},
			{
				header: 		'Num. de Documento',
				tooltip: 	'Num. de Documento',
				dataIndex: 	'NUMERO_DOCUMENTO',
				id:			'columnNumeroDocumento',
				sortable: 	false,
				resizable: 	true,
				width: 		124,
				hidden: 		false,
				align:		'center',
				renderer:	numeroDocumentoRenderer
			},
			{
				header: 		'PyME',
				tooltip: 	'PyME',
				dataIndex: 	'PYME',
				id:			'columnPyme',
				sortable: 	false,
				resizable: 	true,
				width: 		116,
				hidden: 		false,
				align:		'center',
				renderer:	pymeRenderer
			},
			{
				header: 		'Importe del Pago',
				tooltip: 	'Importe del Pago',
				dataIndex: 	'IMPORTE_PAGO',
				id:			'columnImportePago',
				sortable: 	false,
				resizable: 	true,
				width: 		120,
				hidden: 		false,
				align:		'center',
				renderer:	importePagoRenderer
			},
			{
				header: 		'Importe Documento',
				tooltip: 	'Importe Documento',
				dataIndex: 	'IMPORTE_DOCUMENTO',
				id:			'columnImporteDocumento',
				sortable: 	false,
				resizable: 	true,
				width: 		120,
				hidden: 		false,
				align:		'center',
				renderer:	importeDocumentoRenderer
			},
			{
				header: 		'Importe Descuento',
				tooltip: 	'Importe Descuento',
				dataIndex: 	'IMPORTE_DESCUENTO',
				id:			'columnImporteDescuento',
				sortable: 	false,
				resizable: 	true,
				width: 		120,
				hidden: 		false,
				align:		'center',
				renderer:	importeDescuentoRenderer
			}
		]
	});
	
	var gridDetalleDocumentos = new Ext.grid.GridPanel({
		store: 		detalleDocumentosData,
		id:			'gridDetalleDocumentos',
		hidden: 		false,
		margins: 	'20 0 0 0',
		colModel: 	gridDetalleDocumentosColModel,
		formParams:	{},
		stripeRows: true,
		loadMask: 	true,
		height: 		316,		
		frame: 		false,
		flex:			10
	});
 
	var detalleNotaCredito = new Ext.Container({
		//xtype: 	'container',
		hidden: 	true,
		id:		'detalleNotaCredito',
		name:		'detalleNotaCredito',
		height:	28,
		flex:	  	0,
		layout:	'form',
      items:[
      	{
				xtype: 	'label',
				id:	 	'labelNotaCredito',
				cls:		'x-form-item',
				style: 	'font-weight:normal;text-align:left;margin:0px;',
				html:  	'(*) Nota de Cr&eacute;dito'
			}
      ]
	});
		
	var panelDetalleDocumentos = new Ext.Panel({
		//xtype: 	'panel',
		id:	 	'panelDetalleDocumentos',
		frame:	false,
		border: 	false,
		baseCls:	'x-plain',
		height: 420,
		layout: {
			type:		'vbox',
			padding:	'5',
			align:	'stretch'
		},
		defaults:{
			margins:'0 0 0 0'
		},
		items: [ 
			gridDetalleDocumentos,
			detalleNotaCredito
		],
		buttons: [
			{
				xtype: 		'button',
				text: 		'Generar Archivo',
				id: 			'btnGenerarArchivoDetalleCSV',
				iconCls:		'icoGenerarArchivo',
				handler: 	procesaGenerarArchivoDetalleCSV
			},
			{
				xtype: 		'button',
				text: 		'Bajar CSV',
				id: 			'btnBajarDetalleCSV',
				iconCls:		'icoBotonXLS',
				hidden: 		true
			},
			{
				text: 		'Cerrar',
				iconCls: 	'icoRegresar',
				handler: 	procesaCerrar
			}
		]
	});
	
	var hideWindowDetalleDocumentos = function(){
 
		var ventana = Ext.getCmp('windowDetalleDocumentos');
		if(ventana && ventana.isVisible() ){
			// Ocultar ventana
			ventana.hide();
			// Remover contenido del grid
			var detalleDocumentosData = Ext.StoreMgr.key('detalleDocumentosDataStore');
			detalleDocumentosData.removeAll();
			// Ocultar Boton Bajar CSV
			Ext.getCmp("btnBajarDetalleCSV").hide();
			// Ocultar Label Nota de Credito
			Ext.getCmp("detalleNotaCredito").hide();
			// Remover los parametros de la consulta anterior
			Ext.getCmp("gridDetalleDocumentos").formParams = {};
      }
      
	}
	
	var showWindowDetalleDocumentos = function(numeroRegistros){
 
		var ventana = Ext.getCmp('windowDetalleDocumentos');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Nafin Electr�nico',
				layout: 			'fit',
				width: 			650,
				height: 			430,
				minWidth: 		650,
				minHeight: 		430,
				resizable:		false,
				id: 				'windowDetalleDocumentos',
				modal:			true,
				closable: 		false,
				items: [
					panelDetalleDocumentos 
				]
			}).show();
			
		}
		
	}	
	
	//--------------------------------- 2. GRID RESUMEN OPERACION ---------------------------------------

	var ocultaColumnaIfFondeo = function(){
		var columnModel = Ext.getCmp("gridResumenOperacion").getColumnModel();
		var columnIndex = columnModel.getIndexById('columnNombreIfFondeo');
		columnModel.config[columnIndex].hideable = false;
		columnModel.setHidden( columnIndex, true );
	}
	
	var muestraColumnaIfFondeo = function(){
		var columnModel = Ext.getCmp("gridResumenOperacion").getColumnModel();
		var columnIndex = columnModel.getIndexById('columnNombreIfFondeo');
		columnModel.config[columnIndex].hideable = true;
		columnModel.setHidden( columnIndex, false );
	}
	
	var procesarSuccessFailureGenerarComprobanteTransferencia = function(opts, success, response) {
		
		var btnGenerarComprobanteTransferencia = Ext.getCmp('btnGenerarComprobanteTransferencia');
		btnGenerarComprobanteTransferencia.setIconClass('icoGenerarArchivo');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaComprobante = Ext.getCmp('btnBajaComprobante');
			btnBajaComprobante.show();
			btnBajaComprobante.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaComprobante.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
			
		} else {
			
			btnGenerarComprobanteTransferencia.enable();
			NE.util.mostrarConnError(response,opts);
			
		}

	}
	
	var procesarSuccessFailureGenerarArchivoPDF = function(opts, success, response) {
		
		var btnGenerarArchivoPDF = Ext.getCmp('btnGenerarArchivoPDF');
		btnGenerarArchivoPDF.setIconClass('icoGenerarArchivo');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaPDF = Ext.getCmp('btnBajaPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
			
		} else {
			
			btnGenerarArchivoPDF.enable();
			NE.util.mostrarConnError(response,opts);
			
		}

	}
	
	var procesarSuccessFailureGenerarArchivoCSV = function(opts, success, response) {
		
		var btnGenerarArchivoCSV = Ext.getCmp('btnGenerarArchivoCSV');
		btnGenerarArchivoCSV.setIconClass('icoGenerarArchivo');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaCSV = Ext.getCmp('btnBajaCSV');
			btnBajaCSV.show();
			btnBajaCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaCSV.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
			
		} else {
			
			btnGenerarArchivoCSV.enable();
			NE.util.mostrarConnError(response,opts);
			
		}

	}
	
	var procesarConsultaResumenOperacion = function(store, registros, opts){

		var panelFormaConsulta   = Ext.getCmp('panelFormaConsulta');
		panelFormaConsulta.el.unmask();
		
		var gridResumenOperacion = Ext.getCmp('gridResumenOperacion');
		var el 						 = gridResumenOperacion.getEl();
		el.unmask();
		
		if (registros != null) {
			
			if(store.getTotalCount() == 0) {
				el.mask('No existe alguna operaci&oacute;n por el momento Aceptada, En Proceso o Rechazada.', 'x-mask');
			}
			
			// Copiar los formParams
			gridResumenOperacion.formParams = Ext.apply({},opts.params);
				
			var esNuevaConsulta = "NuevaConsulta" === opts.params.operacion?true:false;
 
			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
			
			// Si es consulta nueva cargar totales
			if( esNuevaConsulta ){
				
				var jsonData = store.reader.jsonData;

				var totalOperaciones = Ext.getCmp("totalOperaciones");
				var totalImporte 	   = Ext.getCmp("totalImporte");
				
				totalOperaciones.setValue( jsonData.totalOperaciones );
				totalImporte.setValue(     jsonData.totalImporte     );
				
			}
			
			// Ocultar/Mostrar columna IF Fondeo dependiendo del tipo de dispersion seleccionada
			var esTipoDispersionIF = opts.params.claveTipoDispersion === "F"?true:false;

			if(        esNuevaConsulta &&  esTipoDispersionIF ){
				muestraColumnaIfFondeo();
			} else if( esNuevaConsulta && !esTipoDispersionIF ){
				ocultaColumnaIfFondeo();
			}
			
			if( esNuevaConsulta && store.getTotalCount() > 0 ){
				Ext.getCmp("btnGenerarComprobanteTransferencia").enable();
				Ext.getCmp("btnBajaComprobante").hide();
				Ext.getCmp("btnGenerarArchivoPDF").enable();
				Ext.getCmp("btnBajaPDF").hide();
				Ext.getCmp("btnGenerarArchivoCSV").enable();
				Ext.getCmp("btnBajaCSV").hide();
			} else if( esNuevaConsulta ) {
				Ext.getCmp("btnGenerarComprobanteTransferencia").disable();
				Ext.getCmp("btnBajaComprobante").hide();
				Ext.getCmp("btnGenerarArchivoPDF").disable();
				Ext.getCmp("btnBajaPDF").hide();
				Ext.getCmp("btnGenerarArchivoCSV").disable();
				Ext.getCmp("btnBajaCSV").hide();
			}
			
		} else { // Restaurar el estado original del Grid
			
			var store = Ext.StoreMgr.key('resumenOperacionDataStore');
			if(store.getTotalCount() == 0) {
				el.mask('No existe alguna operaci&oacute;n por el momento Aceptada, En Proceso o Rechazada.', 'x-mask');
			}
			
		}

	}
	
	NE.consultaff.procesaVerDetalle = function( rowIndex ){
		
		var record = Ext.StoreMgr.key('resumenOperacionDataStore').getAt(rowIndex);
		
		var icFlujoFondos			= record.data['IC_FLUJO_FONDOS'];
		var claveTipoDispersion	= record.data['CLAVE_TIPO_DISPERSION'];
		var claveEstatus			= record.data['CLAVE_ESTATUS'];
		var claveEpo				= record.data['CLAVE_EPO'];
		var clavePyme				= record.data['CLAVE_PYME'];
		var claveIf					= record.data['CLAVE_IF'];
		var estatusDocumento		= record.data['ESTATUS_DOCUMENTO'];
		var fechaVencimiento		= record.data['FECHA_VENCIMIENTO'];
		var fechaOperacion		= record.data['FECHA_OPERACION'];
 				
		// Deshabilitar boton de descarga archivo
		Ext.getCmp("btnGenerarArchivoDetalleCSV").disable();
		
		// Cargar contenido del Detalle
		var detalleDocumentosData = Ext.StoreMgr.key("detalleDocumentosDataStore");
		detalleDocumentosData.load({
			params: {
				icFlujoFondos: 		icFlujoFondos,
				claveTipoDispersion: claveTipoDispersion,
				claveEstatus: 			claveEstatus,
				claveEpo: 				claveEpo,
				clavePyme: 				clavePyme,
				claveIf: 				claveIf,
				estatusDocumento: 	estatusDocumento,
				fechaVencimiento: 	fechaVencimiento,
				fechaOperacion: 		fechaOperacion
			}
		});

	}
	
	var procesaGenerarComprobanteTransferencia = function( boton, evento ){
		
		boton.disable();
		boton.setIconClass('loading-indicator');
 
		var formParams = Ext.apply({}, Ext.getCmp("gridResumenOperacion").formParams);
	
		// Generar Comprobante de Transferencia
		Ext.Ajax.request({
			url: 		'15consultaff01ext.data.jsp',
			params: 	Ext.apply(
				formParams, 
				{
					informacion: 'GenerarComprobanteTransferencia',
					tipoArchivo: 'PDF'
				}
			),
			callback: procesarSuccessFailureGenerarComprobanteTransferencia
		});
		
	}
	
	var procesaGenerarArchivoPDF = function( boton, evento ){
		
		boton.disable();
		boton.setIconClass('loading-indicator');
 
		var formParams = Ext.apply({}, Ext.getCmp("gridResumenOperacion").formParams);
						
		// Generar Archivo PDF
		Ext.Ajax.request({
			url: 		'15consultaff01ext.data.jsp',
			params: 	Ext.apply(
				formParams, 
				{
					informacion:     'GenerarArchivo',
					tipoArchivo:      'PDF',
					totalOperaciones: Ext.getCmp("totalOperaciones").getValue(),
					totalImporte:     Ext.getCmp("totalImporte").getValue()
				}
			),
			callback: procesarSuccessFailureGenerarArchivoPDF
		});
		
	}
	
	var procesaGenerarArchivoCSV = function( boton, evento ){
		
		boton.disable();
		boton.setIconClass('loading-indicator');
 
		var formParams = Ext.apply({}, Ext.getCmp("gridResumenOperacion").formParams);
						
		// Generar Archivo CSV
		Ext.Ajax.request({
			url: 		'15consultaff01ext.data.jsp',
			params: 	Ext.apply(
				formParams, 
				{
					informacion: 'GenerarArchivo', 
					tipoArchivo: 'CSV'
				}
			),
			callback: procesarSuccessFailureGenerarArchivoCSV
		});
		
	}

	var resumenOperacionData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'resumenOperacionDataStore',
		url: 		'15consultaff01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaResumenOperacion'
		},
		idIndex: 		1,
		fields: [
			{ name: 'NOMBRE_EPO', 						type: 'string'  },
			{ name: 'NOMBRE_IF', 						type: 'string'  },
			{ name: 'NOMBRE_PYME', 						type: 'string'  },
			{ name: 'RFC', 								type: 'string'  },
			{ name: 'NOMBRE_IF_FONDEO', 				type: 'string'  },
			{ name: 'BANCO', 								type: 'string'  },
			{ name: 'TIPO_CUENTA', 						type: 'string'  },
			{ name: 'NUMERO_CUENTA', 					type: 'string'  },
			{ name: 'IMPORTE', 							type: 'float', convert: function(value, record){ return Number(value.replace(/[\$, ]/g,'')); } },
			{ name: 'ESTATUS', 							type: 'string'  },
			{ name: 'VIA_LIQUIDACION', 				type: 'string'  },
			{ name: 'FOLIO', 								type: 'string'  },
			{ name: 'FECHA_REGISTRO', 					type: 'date',  convert: function(value, record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } },
			{ name: 'VER_DETALLE', 						type: 'string'  },
			{ name: 'EXISTEN_DOCUMENTOS', 			type: 'boolean' },
			{ name: 'IC_FLUJO_FONDOS', 				type: 'string'  },
			{ name: 'CLAVE_TIPO_DISPERSION', 		type: 'string'  },
			{ name: 'CLAVE_ESTATUS', 					type: 'string'  },
			{ name: 'CLAVE_EPO', 						type: 'string'  },
			{ name: 'CLAVE_PYME', 						type: 'string'  },
			{ name: 'CLAVE_IF', 							type: 'string'  },
			{ name: 'ESTATUS_DOCUMENTO', 				type: 'string'  },
			{ name: 'FECHA_VENCIMIENTO', 				type: 'string'  },
			{ name: 'FECHA_OPERACION', 				type: 'string'  }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
					
					var gridEl = Ext.getCmp("gridResumenOperacion").getEl();
					if( gridEl.isMasked() ){
						gridEl.unmask();
					}
						
					var gridResumenOperacion = Ext.getCmp("gridResumenOperacion");
					if( !gridResumenOperacion.isVisible() ){
						gridResumenOperacion.show();
						// Debido a un Bug por multiples bottom-bars, se ocultan los botones de Generacion de Archivos
						Ext.getCmp("btnBajaPDF").hide();
						Ext.getCmp("btnBajaCSV").hide();
					}
				}
			},
			load: 	procesarConsultaResumenOperacion,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaResumenOperacion(null, null, null);						
				}
			}
		}
		
	});
	
	var nombreEpoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var nombreIfRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var nombrePymeRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var rfcRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var intermediarioFinancieroFondeoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var bancoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var tipoCuentaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var numeroCuentaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var importeRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;text-align:right;';
		metadata.attr += '" ';
		value 			= record.json['IMPORTE'];
		return value;
			
	}

	var estatusRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var viaLiquidacionRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var folioRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
			
	}
	
	var fechaRegistroRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr =  'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		value         = Ext.isEmpty(value)?'':value.format("d/m/Y");
		return value;
			
	}

	var verDetalleRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		if( record.data["EXISTEN_DOCUMENTOS"] === true ){
			
			value = 
				"<a href=\"javascript:NE.consultaff.procesaVerDetalle(" + rowIndex + ");\"> "  +
				"   <img border=\"0\" src=\"" + Ext.BLANK_IMAGE_URL + "\" class=\"icoLupa\" ext:qtip=\"Ver Detalle\" align=\"middle\" >Ver"  +
				"    "  +
				"</a> ";
 
		} else {
			
			metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
			metadata.attr += '" ';
		}
		
		return value;
			
	}

	var gridResumenOperacionColModel = new Ext.grid.ColumnModel({
		columns: [
			{
				header: 		'EPO<br>&nbsp;',
				tooltip: 	'EPO',
				dataIndex: 	'NOMBRE_EPO',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				align:		'center', 
				renderer:	nombreEpoRenderer,
				editable:	false
			},
			{
				header: 		'IF<br>&nbsp;',
				tooltip: 	'IF',
				dataIndex: 	'NOMBRE_IF',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				align:		'center', 
				renderer:	nombreIfRenderer,
				editable:	false
			},
			{
				header: 		'PYME<br>&nbsp;',
				tooltip: 	'PYME',
				dataIndex: 	'NOMBRE_PYME',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				align:		'center', 
				renderer:	nombrePymeRenderer,
				editable:	false
			},
			{
				header: 		'RFC<br>&nbsp;',
				tooltip: 	'RFC',
				dataIndex: 	'RFC',
				sortable: 	true,
				resizable: 	true,
				width: 		60,
				align:		'center', 
				renderer:	rfcRenderer,
				editable:	false
			},
			{
				header: 		'Intermediario<br>Financiero Fondeo',
				tooltip: 	'Intermediario Financiero Fondeo',
				id:			'columnNombreIfFondeo',
				dataIndex: 	'NOMBRE_IF_FONDEO',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				align:		'center', 
				renderer:	intermediarioFinancieroFondeoRenderer,
				editable:	false
			},
			{
				header: 		'Banco<br>&nbsp;',
				tooltip: 	'Banco',
				dataIndex: 	'BANCO',
				sortable: 	true,
				resizable: 	true,
				width: 		60,
				align:		'center', 
				renderer:	bancoRenderer,
				editable:	false
			},
			{
				header: 		'Tipo de<br>Cuenta',
				tooltip: 	'Tipo de Cuenta',
				dataIndex: 	'TIPO_CUENTA',
				sortable: 	true,
				resizable: 	true,
				width: 		60,
				align:		'center', 
				renderer:	tipoCuentaRenderer,
				editable:	false
			},
			{
				header: 		'No. de<br>Cuenta',
				tooltip: 	'No. de Cuenta',
				dataIndex: 	'NUMERO_CUENTA',
				sortable: 	true,
				resizable: 	true,
				width: 		124,
				align:		'center', 
				renderer:	numeroCuentaRenderer,
				editable:	false
			},
			{
				header: 		'Importe Total<br>del Descuento',
				tooltip: 	'Importe Total del Descuento',
				dataIndex: 	'IMPORTE',
				sortable: 	true,
				resizable: 	true,
				width: 		120,
				align:		'center', 
				renderer:	importeRenderer,
				editable:	false
			},
			{
				header: 		'Estatus<br>&nbsp;',
				tooltip: 	'Estatus',
				dataIndex: 	'ESTATUS',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				align:		'center', 
				renderer:	estatusRenderer,
				editable:	false
			},
			{
				header: 		'V�a de<br>Liquidaci�n',
				tooltip: 	'V�a de Liquidaci�n',
				dataIndex: 	'VIA_LIQUIDACION',
				sortable: 	true,
				resizable: 	true,
				width: 		80,
				align:		'center', 
				renderer:	viaLiquidacionRenderer,
				editable:	false
			},
			{
				header: 		'Folio<br>&nbsp;',
				tooltip: 	'Folio',
				dataIndex: 	'FOLIO',
				sortable: 	true,
				resizable: 	true,
				width: 		124,
				align:		'center', 
				renderer:	folioRenderer,
				editable:	false
			},
			{
				header: 		'Fecha de<br>Registro',
				tooltip: 	'Fecha de Registro',
				dataIndex: 	'FECHA_REGISTRO',
				sortable: 	true,
				resizable: 	true,
				width: 		80,
				align:		'center', 
				renderer:	fechaRegistroRenderer,
				editable:	false
			},
			{
				header: 		'Ver<br>Detalle',
				tooltip: 	'Ver Detalle',
				dataIndex: 	'VER_DETALLE',
				sortable: 	false,
				resizable: 	false,
				width: 		60,
				align:		'center', 
				renderer:	verDetalleRenderer,
				editable:	false
			}
		]		
	});
	
	var gridResumenOperacion = {
		store: 		resumenOperacionData,
		xtype: 		'grid',
		id:			'gridResumenOperacion',
		title:		'Resumen',
		frame:		true,
		formParams:	{},
		stripeRows: true,
		loadMask: 	true,
		width:		940,
		height: 		464,
		hidden:		true,
		style: {
			borderWidth: 0
		},
		bodyBorder: false,
		colModel: 	gridResumenOperacionColModel,
		stateful:	false,
		bbar: {
			xtype:	'container',
			height:	56,
         layout: {
				type:		'vbox',
				padding:	'0',
				align:	'stretch'
			},
			defaults:{
				margins: '0 0 0 0'
			},
			items:[
				{
					xtype: 'toolbar',
					height: 28,
					items: [
						{
							xtype: 'box',
							cls:   'xtb-text',
							html:  'Total de Operaciones: '
						},
						// TEXTFIELD Total de Operaciones
						{ 
							xtype: 			'textfield',
							name: 			'totalOperaciones',
							id: 				'totalOperaciones',
							readOnly:		true,
							hidden: 			false,
							maxLength: 		9,
							width:			126,
							selectOnFocus: true,
							submitValue: 	true,
							style: {
								textAlign: 'right'
							}
						},
						{
							xtype: 'box',
							cls:   'xtb-text',
							width: 100,
							style: 'text-align: right',
							html:  'Importe Total: '
						},
						// TEXTFIELD Importe Total de Operaciones
						{ 
							xtype: 			'textfield',
							name: 			'totalImporte',
							id: 				'totalImporte',
							readOnly:		true,
							hidden: 			false,
							// maxLength: 	9,
							msgTarget: 		'side',
							width:			184,
							selectOnFocus: true,
							submitValue: 	true,
							style: {
								textAlign: 'right'
							}
						},
						'->',
						'-',
						// BUTTON: Generar Comprobante de Transferencia
						{
							xtype: 		'button',
							text: 		'Generar Comprobante de Transferencia',
							iconCls: 	'icoGenerarArchivo',
							id: 			'btnGenerarComprobanteTransferencia', 
							disabled:	true,
							handler: 	procesaGenerarComprobanteTransferencia 
						},
						// BUTTON: BAJAR Comprobante
						{
							xtype: 	 	'button',
							text: 	 	'Bajar Comprobante',
							id: 		 	'btnBajaComprobante',
							iconCls:	 	'icoBotonPDF',
							hidden: 	 	true
						}
					]
				},
				{
					xtype: 			'paging',
					pageSize: 		15,
					buttonAlign: 	'left',
					id: 				'barraPaginacion',
					opts:				null,
					displayInfo: 	true,
					store: 			resumenOperacionData,
					displayMsg: 	'{0} - {1} de {2}',
					emptyMsg: 		"No hay registros.",
					items:  [
						'->',
						'-',
						// BUTTON: Imprimir
						{
							xtype: 	 	'button',
							text: 	 	'Generar Archivo PDF',
							id: 		 	'btnGenerarArchivoPDF',
							iconCls:  	'icoGenerarArchivo',
							disabled:	true,
							handler: 	procesaGenerarArchivoPDF
						},
						// BUTTON: Bajar PDF
						{
							xtype: 	 	'button',
							text: 	 	'Bajar PDF',
							id: 		 	'btnBajaPDF',
							iconCls:	 	'icoBotonPDF',
							hidden: 	 	true
						},
						// BUTTON: Generar Archivo
						{
							xtype: 		'button',
							text: 		'Generar Archivo',
							id: 			'btnGenerarArchivoCSV',
							iconCls:		'icoGenerarArchivo', 
							disabled:	true,
							handler: 	procesaGenerarArchivoCSV
						},
						// BUTTON: Bajar PDF
						{
							xtype: 		'button',
							text: 		'Bajar CSV',
							id: 			'btnBajaCSV',
							iconCls:		'icoBotonXLS',
							hidden: 		true
						}
					]
				} 
			]
      }
	};
	 
	//--------------------------------- 1. PANEL FORMA CONSULTA ---------------------------------------
	
	var procesaConsultar = function( boton, evento ){
		
		var panelFormaConsulta = Ext.getCmp("panelFormaConsulta");
		
		var invalidForm = false;
		if( !panelFormaConsulta.getForm().isValid() ){
			invalidForm = true;
		}
 
		// VALIDAR QUE SE HAYA SELECCIONADO UN CONCEPTO
		// 'Debe de seleccionar un Concepto'
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE NO SE HAYA SELECCIONADO EL CONCEPTO CARGOS
		var comboConcepto = Ext.getCmp("comboConcepto");
		if( comboConcepto.isValid() && comboConcepto.getValue() === 'C' ){
			invalidForm = true;
			comboConcepto.markInvalid('El concepto de cargos por el momento no est� disponible.');
		}
			
		// VALIDAR QUE SE HAYA SELECCIONADO UN TIPO DE DISPERSION
		// 'Debe de seleccionar un Tipo de Dispersi�n'
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE SE HAYA SELECCIONADO UNA V�A DE LIQUIDACI�N
		// 'Debe de seleccionar la V�a de Liquidaci�n'
		// Nota: La validacion ya se realiza en la forma

		// VALIDAR QUE SE HAYA ESPECIFICADO EL RANGO DE FECHAS DE REGISTRO
		// 'Debe de capturar la fecha de registro final'
		// 'Debe de capturar la fecha de registro inicial'
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE LAS FECHAS DE REGISTRO TENGAN EL FORMATO DD/MM/AAAA
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE LAS FECHA DE REGISTRO FINAL SEA MAYOR O IGUAL A FECHA DE REGISTRO INICIAL
		// 'La fecha de registro final debe ser mayor o igual a la inicial'
		// Nota: La validacion ya se realiza en la forma
		
		if( invalidForm ){
			return; // La forma es invalida, se cancela la operacion
		}
		
		// Aplicar m�scara de consulta
		var panelFormaConsulta = Ext.getCmp("panelFormaConsulta");
		panelFormaConsulta.el.mask("Consultando...","x-mask-loading");
		
		// Realizar consulta
		var resumenOperacionData = Ext.StoreMgr.key("resumenOperacionDataStore");
		resumenOperacionData.load({
			params: Ext.apply(Ext.getCmp("panelFormaConsulta").getForm().getValues(),{
				operacion: 	'NuevaConsulta', //Generar datos para la consulta
				start: 		0,
				limit: 		15
			})
		});	

	}
	
	var procesaLimpiar = function( boton, evento ){

		Ext.getCmp("comboIF").originalValue      = '';
		Ext.getCmp("comboEPO").originalValue     = '';
		Ext.getCmp("comboEstatus").originalValue = '';
		
		Ext.getCmp("panelFormaConsulta").getForm().reset();
		Ext.getCmp("fechaRegistroInicial").setMaxValue('');
		Ext.getCmp("fechaRegistroFinal").setMinValue('');
		Ext.getCmp("comboIF").hide();
		Ext.getCmp("displayOperaFideicomiso").hide();
		Ext.getCmp("comboEPO").hide();
		Ext.getCmp("comboEstatus").hide();

	}
	
	var catalogoConceptoData = new Ext.data.JsonStore({
		id: 					'catalogoConceptoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15consultaff01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoConcepto'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboConcepto = Ext.getCmp("comboConcepto");
						comboConcepto.setValue(defaultValue);
						comboConcepto.originalValue = comboConcepto.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoTipoDispersionData = new Ext.data.JsonStore({
		id: 					'catalogoTipoDispersionDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15consultaff01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoTipoDispersion'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboTipoDispersion = Ext.getCmp("comboTipoDispersion");
						comboTipoDispersion.setValue(defaultValue);
						comboTipoDispersion.originalValue = comboTipoDispersion.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoIFData = new Ext.data.JsonStore({
		id: 					'catalogoIFDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg', 'opera_fideicomiso' ],
		url: 					'15consultaff01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIF'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboIF = Ext.getCmp("comboIF");
						comboIF.setValue(defaultValue);
						comboIF.originalValue = comboIF.getValue();
						// Para que se actualice ifOperaFideicomiso
						comboIF.fireEvent("select",comboIF,store.getAt(index),index); 
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 					'catalogoEPODataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15consultaff01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoEPO'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// En caso se haya seleccinado el tipo de dispersion PEMEX-REF,
				// especificar el valor default
				var overrideDefaultValue = null;
				if( Ext.isDefined(options.params) && options.params.claveTipoDispersion === "P" && records.length > 0 ){
					overrideDefaultValue = records[0].data['clave'];
				}
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = !Ext.isEmpty(overrideDefaultValue)?String(overrideDefaultValue):String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
						
				// Solo para el Tipo de Dispersion: PEMEX-REF, se informar�, cuando no se haya 
				// encontrado ninguna EPO.
				if( Ext.isDefined(options.params) && options.params.claveTipoDispersion === "P" && records.length === 0 ){
					Ext.getCmp("comboEPO").setNewEmptyText("No se encontr� ninguna EPO");
				} 
				
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboEPO = Ext.getCmp("comboEPO");
						comboEPO.setValue(defaultValue);
						comboEPO.originalValue = comboEPO.getValue();
					}
									
				} 
 
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatusData = new Ext.data.JsonStore({
		id: 					'catalogoEstatusDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg' ],
		url: 					'15consultaff01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoEstatus'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Se deber� seleccionar por default el primer valor que arroje la consulta
				var overrideDefaultValue = null;
				if( records.length > 0 ){
					overrideDefaultValue = records[0].data['clave'];
				}
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = !Ext.isEmpty(overrideDefaultValue)?String(overrideDefaultValue):String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboEstatus = Ext.getCmp("comboEstatus");
						comboEstatus.setValue(defaultValue);
						comboEstatus.originalValue = comboEstatus.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoViaLiquidacionData = new Ext.data.JsonStore({
		id: 					'catalogoViaLiquidacionDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15consultaff01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoViaLiquidacion'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboViaLiquidacion = Ext.getCmp("comboViaLiquidacion");
						comboViaLiquidacion.setValue(defaultValue);
						comboViaLiquidacion.originalValue = comboViaLiquidacion.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var elementosPanelFormaConsulta = [
		// COMBO CONCEPTO
		{
			xtype: 				'combo',
			name: 				'comboConcepto',
			id: 					'comboConcepto',
			fieldLabel: 		'Concepto',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveConcepto',
			emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoConceptoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		// COMBO TIPO DE DISPERSION
		{
			xtype: 				'combo',
			name: 				'comboTipoDispersion',
			id: 					'comboTipoDispersion',
			fieldLabel: 		'Tipo de Dispersi�n',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveTipoDispersion',
			emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTipoDispersionData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
			listeners:			{
				select:	function( combo, record, index ){

					var comboIF	 					 = Ext.getCmp("comboIF");
					var displayOperaFideicomiso = Ext.getCmp("displayOperaFideicomiso");
					var ifOperaFideicomiso		 = Ext.getCmp("ifOperaFideicomiso");
					var comboEPO 					 = Ext.getCmp("comboEPO");
					var comboEstatus				 = Ext.getCmp("comboEstatus");
					
					if( 		  combo.getValue() === "E" ){ // EPO'S
						
						// Ocultar Combo IF
						comboIF.hide();
						comboIF.originalValue = '';
						comboIF.reset();
						
						// Ocultar Label IF Opera Fideicomiso para Desarrollo de Proveedores
						displayOperaFideicomiso.hide();
						
						// Borrar contenido Campo Hidden ifOperaFideicomiso
						ifOperaFideicomiso.reset();

						// Mostrar Combo EPO
						comboEPO.setNewEmptyText("Seleccionar...");
						comboEPO.originalValue = '';
						comboEPO.reset();
						comboEPO.show();
						// Cargar Contenido del Catalogo EPO
						var catalogoEPOData           	= Ext.StoreMgr.key('catalogoEPODataStore');
						catalogoEPOData.load({
							params: { 
								claveTipoDispersion: combo.getValue(),
								defaultValue:        "TODAS"						
							}
						});

						// Ocultar Combo Estatus
						comboEstatus.hide();
						comboEstatus.originalValue = '';
						comboEstatus.reset();
						
					} else if( combo.getValue() === "F" ){ // IF'S
						
						// Mostrar Combo IF
						comboIF.originalValue = '';
						comboIF.reset();
						comboIF.show();
						// Cargar Contenido del Catalogo IF
						var catalogoIFData 					= Ext.StoreMgr.key('catalogoIFDataStore');
						catalogoIFData.load({
							params: { 
								defaultValue: "TODOS"						
							}
						});
						
						// Ocultar Label IF Opera Fideicomiso para Desarrollo de Proveedores
						displayOperaFideicomiso.hide();
						
						// Borrar contenido Campo Hidden ifOperaFideicomiso
						ifOperaFideicomiso.reset();
						
						// Ocultar Combo EPO
						comboEPO.hide();
						comboEPO.originalValue = '';
						comboEPO.reset();
						
						// Ocultar Combo Estatus
						comboEstatus.hide();
						comboEstatus.originalValue = '';
						comboEstatus.reset();
						
					} else if( combo.getValue() === "P" ){ // PEMEX-REF
						
						// Ocultar Combo IF
						comboIF.hide();
						comboIF.originalValue = '';
						comboIF.reset();
						
						// Ocultar Label IF Opera Fideicomiso para Desarrollo de Proveedores
						displayOperaFideicomiso.hide();
						
						// Borrar contenido Campo Hidden ifOperaFideicomiso
						ifOperaFideicomiso.reset();

						// Mostrar Combo EPO
						comboEPO.setNewEmptyText("Seleccionar...");
						comboEPO.originalValue = '';
						comboEPO.reset();
						comboEPO.show();
						// Cargar Contenido del Catalogo EPO
						var catalogoEPOData           	= Ext.StoreMgr.key('catalogoEPODataStore');
						catalogoEPOData.load({
							params: { 
								claveTipoDispersion: combo.getValue(),
								defaultValue:        "TODAS"						
							}
						});

						// Mostrar Combo Estatus
						comboEstatus.originalValue = '';
						comboEstatus.reset();
						comboEstatus.show();
						// Cargar Contenido del Catalogo Estatus
						var catalogoEstatusData           	= Ext.StoreMgr.key('catalogoEstatusDataStore');
						catalogoEstatusData.load();
						
					}
				
				}
			}
		},
		// COMBO IF
		{
			xtype: 				'combo',
			hidden:				true,
			name: 				'comboIF',
			id: 					'comboIF',
			width:				310,
			fieldLabel: 		'IF',
			mode: 				'local', 
			allowBlank:			true,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveIF',
			//emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoIFData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			/*anchor:				'-20',*/
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
			listeners: {
				select:	function( combo, record, index ){

					var displayOperaFideicomiso = Ext.getCmp("displayOperaFideicomiso");
					var ifOperaFideicomiso		 = Ext.getCmp("ifOperaFideicomiso");
					if( 			record.data['opera_fideicomiso'] === "S" ){
						displayOperaFideicomiso.show();
						ifOperaFideicomiso.setValue("S");
					} else {
						displayOperaFideicomiso.hide();
						ifOperaFideicomiso.setValue("N");
					}
				
				}
			}
		},
		// DISPLAYFIELD IF OPERA FIDEICOMISO
		{
			xtype: 				'displayfield',
			id:					'displayOperaFideicomiso',
			name:					'displayOperaFideicomiso',	
			hidden:				true,
			value: 				'IF Opera Fideicomiso para Desarrollo de Proveedores',
			style:				'text-align: left; padding:0; padding-bottom: 2;'
		},
		// HIDDEN ifOperaFideicomiso
		{
			xtype: 'hidden',
			id:	 'ifOperaFideicomiso',
			name:	 'ifOperaFideicomiso'
		},
		// COMBO EPO
		{
			xtype: 				'combo',
			hidden:				true,
			name: 				'comboEPO',
			id: 					'comboEPO',
			width:				310,
			fieldLabel: 		'EPO',
			mode: 				'local', 
			allowBlank:			true,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPO',
			//emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			/*anchor:				'-20',*/
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		// COMBO ESTATUS
		{
			xtype: 				'combo',
			hidden:				true,
			name: 				'comboEstatus',
			id: 					'comboEstatus',
			width:				310,
			fieldLabel: 		'Estatus',
			mode: 				'local', 
			allowBlank:			true,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEstatus',
			//emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEstatusData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			/*anchor:				'-20',*/
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		// COMBO V�A DE LIQUIDACI�N
		{
			xtype: 				'combo',
			name: 				'comboViaLiquidacion',
			id: 					'comboViaLiquidacion',
			fieldLabel: 		'V�a de Liquidaci�n',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveViaLiquidacion',
			emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoViaLiquidacionData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		// DATERANGE: FECHA DE REGISTRO
		{
			xtype: 						'compositefield',
			fieldLabel: 				'Fecha de Registro',
			combineErrors: 			false,
			msgTarget: 					'side',
			items: [
				{
					xtype: 				'datefield',
					name: 				'fechaRegistroInicial',
					id: 					'fechaRegistroInicial',
					allowBlank: 		false,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoFinFecha: 	'fechaRegistroFinal',
					margins: 			'0 20 0 0',
					allowBlank:			false,
					disabledDates: 	["../../0000"],
					disabledDatesText: "La fecha no es v�lida"
				},{
					xtype: 				'displayfield',
					value: 				'a',
					width: 				20
				},{
					xtype: 				'datefield',
					name: 				'fechaRegistroFinal',
					id: 					'fechaRegistroFinal',
					allowBlank: 		false,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoInicioFecha: 'fechaRegistroInicial',
					margins: 			'0 20 0 0',
					allowBlank:			false,
					disabledDates: 	["../../0000"],
					disabledDatesText: "La fecha no es v�lida"
				}
			]
		},
		// HIDDEN FECHA HOY
		{
			xtype: 'hidden',
			id:	 'fechaHoy',
			name:	 'fechaHoy'
		}
	];
	
	var panelFormaConsulta = new Ext.form.FormPanel({
		id: 				'panelFormaConsulta',
		layout:			'form',
		width: 			474,
		title: 			'Forma de Consulta',
		hidden:			Ext.isIE7?false:true, // Debido a un bug en el layout de los composite fields para IE7
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto; ', 
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	110,
		defaultType: 	'textfield',
		items: 			elementosPanelFormaConsulta,
		monitorValid: 	false,
		buttons: [
			{
				text: 		'Consultar',
				iconCls: 	'icoBuscar',
				handler: 	procesaConsultar
			},
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	procesaLimpiar
			}
		]
	});
	
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelFormaConsulta,
			NE.util.getEspaciador(10),
			gridResumenOperacion
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	consultaFlujoFondos("INICIALIZACION",null);
	
});