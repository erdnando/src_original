Ext.onReady(function() {

	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);

	var grupoHeaders = 
	[
		{header: ' ',								colspan: 1, align: 'center'},
		{header: 'Documentos Operados', 		colspan: 2, align: 'center'},
		{header: 'Proveedores Dispersados', colspan: 3, align: 'center'}
	];

	var group = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [grupoHeaders]
	});	

/***** Descargar archivos PDF o CSV *****/  
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			formaPrincipal.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			formaPrincipal.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnImprimir').setIconClass('icoPdf');
		Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
	}
	
/**********		Cat�logos			**********/ 
	 var procesarCatalogoIF = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){	
			var ic_if = Ext.getCmp('no_if_id');
			Ext.getCmp('no_if_id').setValue('');
			if(ic_if.getValue() == ''){
				var newRecord = new recordType({
					clave: 		 '',
					descripcion: 'Seleccionar ....'
				});							
				store.insert(0,newRecord);
				store.commitChanges();
				ic_if.setValue('');			
			}						
		}
  };

	var procesarCatalogoEPO = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){	
			var ic_epo = Ext.getCmp('no_epo_id');
			Ext.getCmp('no_epo_id').setValue('');
			if(ic_epo.getValue() == ''){
				var newRecord = new recordType({
					clave: 		 '',
					descripcion: 'Seleccionar ....'
				});							
				store.insert(0, newRecord);
				store.commitChanges();
				ic_epo.setValue('');			
			}						
		}
  };	
  
/**********		Se crea el Store de los Cat�logos			**********/ 
	var catalogoIF = new Ext.data.JsonStore({
		id:		'catalogoIF',
		root:		'registros',
		fields:	['clave', 'descripcion', 'loadMsg'],
		url:		'15resumenIfext.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty :	'total',
		autoLoad: 			true,		
		listeners: {	
		load: procesarCatalogoIF,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});

	var catalogoEPO = new Ext.data.JsonStore({
		id:		'catalogoEPO',
		root:		'registros',
		fields:	['clave', 'descripcion', 'loadMsg'],
		url:		'15resumenIfext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty :	'total',
		autoLoad: 			false,		
		listeners: {	
		load: procesarCatalogoEPO,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});

/**********		Proceso para generar el Grid Resumen Dispersi�n			**********/  
	var procesarConsultaData = function(store, arrRegistros, opts){
		var formaPrincipal = Ext.getCmp('formaPrincipal');				
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		el.unmask();
		if (arrRegistros != null) {		
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0) {
				/***** Lleno los campos *****/
				Ext.getCmp('fecha_emision_id').setValue(jsonData.fecha_emision);
				Ext.getCmp('fecha_inicio_id').setValue(jsonData.fecha_inicio);
				Ext.getCmp('fecha_fin_id').setValue(jsonData.fecha_fin);	
				Ext.getCmp('usuario_id').setValue(jsonData.usuario);
				Ext.getCmp('gridConsulta').setTitle(jsonData.titulo);	//	Asigno el t�tulo al grid
				/***** Lleno los campos ocultos *****/
				Ext.getCmp('if_id').setValue(Ext.getCmp('no_if_id').getValue());
				Ext.getCmp('epo_id').setValue(Ext.getCmp('no_epo_id').getValue());
				Ext.getCmp('numero_id').setValue(jsonData.numeroTot);
				Ext.getCmp('monto_id').setValue(jsonData.montoTot);
				Ext.getCmp('proveedor_id').setValue(jsonData.proveedorTot);
				Ext.getCmp('docto_id').setValue(jsonData.docTot);
				Ext.getCmp('monto_dis_id').setValue(jsonData.montoDispTot);
				Ext.getCmp('moneda_id').setValue(jsonData.tipo_moneda);
				/***** Consulto los totales  *****/
				consultaTotal.load({
					params : Ext.apply({
						numero_tot:		jsonData.numeroTot,
						proveedor_tot:	jsonData.proveedorTot,
						docto_tot:		jsonData.docTot,
						monto_tot:		jsonData.montoTot,
						monto_dis_tot:	jsonData.montoDispTot
					})
				});			
			} else {	
				formaPrincipal.el.unmask();
				Ext.getCmp('formaAvisos').show();
				Ext.getCmp('formaDispersion').hide();				
			}
		}
	};

/**********		Proceso para generar el Grid de Totales			**********/  
	var procesarConsultaTotal = function(store, arrRegistros, opts){
		var formaPrincipal = Ext.getCmp('formaPrincipal');	
		var gridTotal = Ext.getCmp('gridTotal');	
		var el = gridTotal.getGridEl();	
		formaPrincipal.el.unmask();
		el.unmask();
		if (arrRegistros != null) {		
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0) {
				Ext.getCmp('formaAvisos').hide();				
				Ext.getCmp('formaDispersion').show();	
			} else {	
				Ext.getCmp('formaAvisos').show();
				Ext.getCmp('formaDispersion').hide();	
			}
		}
	};
	
/**********		Se crea el store del Grid Resumen Dispersi�n		**********/
	var consultaData = new Ext.data.JsonStore({
		root:					'registros',
		url:					'15resumenIfext.data.jsp',
		baseParams:			{		
			informacion:	'ConsultaDatosGrid'		
		},						
		fields: [			
			{	name: 'FECHA'					},
			{	name: 'NUMERO'					},
			{	name: 'MONTO_OPERADOS'		},
			{	name: 'NUMERO_PROVEEDOR'	},
			{	name: 'NUMERO_DOC'			},
			{	name: 'MONTO_DISPERSADOS'	}
		],						
		totalProperty:		'total',
		messageProperty:	'msg',
		autoLoad:			false,
		listeners:			{
			load:				procesarConsultaData,
			exception:		{
				fn: 			function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}								
	});

/**********		Se crea el store del Grid Totales		**********/
	var consultaTotal = new Ext.data.JsonStore({
		root:					'registros',
		url:					'15resumenIfext.data.jsp',
		baseParams:			{		
			informacion:	'ConsultaTotales'		
		},						
		fields: [
			{	name: 'TOTAL'						},
			{	name: 'NUMERO_TOT'				},
			{	name: 'MONTO_OPERADOS_TOT'		},
			{	name: 'NUMERO_PROVEEDOR_TOT'	},
			{	name: 'NUMERO_DOC_TOT'			},
			{	name: 'MONTO_DISPERSADOS_TOT'	}
		],						
		totalProperty:		'total',
		messageProperty:	'msg',
		autoLoad:			false,
		listeners:			{
			load:				procesarConsultaTotal,
			exception:		{
				fn: 			function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotal(null, null, null);					
				}
			}
		}								
	});
	
/**********		Se crea el grid Resumen Dispersi�n		**********/
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		width:				600,
		id:					'gridConsulta',	
		title:				'&nbsp;',
		style:				'margin:0 auto;',		
		align:				'center',
		frame:				false,
		border:				true,
		hidden:				false,
		displayInfo:		true,	
		loadMask:			true,
		stripeRows:			true,
		autoHeight:			true,
		store:				consultaData,
		plugins: 			group,
		columns:				[
			{
				width:		100,
				header:		'Fecha',
				dataIndex:	'FECHA',			
				align:		'center',
				sortable:	true,		
				resizable:	false		
				
			},{
				width:		97,	
				header:		'N�mero',
				dataIndex:	'NUMERO',			
				align:		'center',	
				sortable:	true,		
				resizable:	false				
			},{
				width:		100,	
				header:		'Monto',
				dataIndex:	'MONTO_OPERADOS',			
				align:		'right',
				sortable:	true,		
				resizable:	false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			},{
				width:		100,
				header:		'No. Proveedor',			
				align:		'center',	
				dataIndex:	'NUMERO_PROVEEDOR',
				sortable:	true,		
				resizable:	false				
			},{
				width:		100,	
				header:		'No. Docto',
				dataIndex:	'NUMERO_DOC',				
				align:		'center',
				sortable:	true,		
				resizable:	false				
			},{
				width:		100,
				header:		'Monto',
				dataIndex:	'MONTO_DISPERSADOS',				
				align:		'right',
				sortable:	true,		
				resizable:	false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')
			}
		]
	});

/**********		Se crea el grid Totales		**********/
	var gridTotal = new Ext.grid.EditorGridPanel({	
		width:				600,
		height:				80,
		id:					'gridTotal',	
		style:				'margin:0 auto;',		
		align:				'center',
		frame:				false,
		border:				true,
		hidden:				false,
		displayInfo:		true,	
		loadMask:			true,
		stripeRows:			true,
		autoHeight:			true,
		store:				consultaTotal,
		columns:				[
			{
				width:		100,
				header:		'&nbsp;',
				dataIndex:	'TOTAL',			
				align:		'center',
				sortable:	true,		
				resizable:	false		
				
			},{
				width:		97,	
				header:		'N�mero',
				dataIndex:	'NUMERO_TOT',			
				align:		'center',	
				sortable:	true,		
				resizable:	false				
			},{
				width:		100,	
				header:		'Monto',
				dataIndex:	'MONTO_OPERADOS_TOT',			
				align:		'right',
				sortable:	true,		
				resizable:	false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')					
			},{
				width:		100,
				header:		'No. Proveedor',			
				align:		'center',	
				dataIndex:	'NUMERO_PROVEEDOR_TOT',
				sortable:	true,		
				resizable:	false				
			},{
				width:		100,	
				header:		'No. Docto',
				dataIndex:	'NUMERO_DOC_TOT',				
				align:		'center',
				sortable:	true,		
				resizable:	false				
			},{
				width:		100,
				header:		'Monto',
				dataIndex:	'MONTO_DISPERSADOS_TOT',				
				align:		'right',
				sortable:	true,		
				resizable:	false,
				renderer: 	Ext.util.Format.numberRenderer('$0,0.00')	
			}
		],
		bbar: {
			items: [
				'->','-',
				{
					xtype: 	'button',
					text: 	'Generar Archivo',	
					id: 		'btnGenerarCSV',				
					tooltip:	'Generar Archivo',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15resumenIfext.data.jsp',
							params: Ext.apply({
								informacion: 	'GenerarArchivo',
													
								fecha: 			Ext.getCmp('fecha_emision_id').getValue(),
								usuario: 		Ext.getCmp('usuario_id').getValue(),
								ic_if: 			Ext.getCmp('if_id').getValue(),
								ic_epo: 			Ext.getCmp('epo_id').getValue(),
								fecha_ini: 		Ext.getCmp('fecha_inicio_id').getValue(),
								fecha_fin: 		Ext.getCmp('fecha_fin_id').getValue(),
								nombre_if: 		Ext.getCmp('nombre_if_id').getValue(),
								nombre_epo:		Ext.getCmp('nombre_epo_id').getValue(),
													
								numero_tot: 	Ext.getCmp('numero_id').getValue(),
								monto_tot: 		Ext.getCmp('monto_id').getValue(),
								proveedor_tot: Ext.getCmp('proveedor_id').getValue(),
								docto_tot: 		Ext.getCmp('docto_id').getValue(),
								monto_dis_tot: Ext.getCmp('monto_dis_id').getValue(),
								tipo_moneda:	Ext.getCmp('moneda_id').getValue()
																
							}),
							callback: descargaArchivo
						});
					}
				},
				'-',
				{
					xtype: 	'button',
					text: 	'Imprimir',
					id: 		'btnImprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15resumenIfext.data.jsp',
							params: Ext.apply({  
								informacion: 	'Imprimir',
													
								fecha: 			Ext.getCmp('fecha_emision_id').getValue(),
								usuario: 		Ext.getCmp('usuario_id').getValue(),
								ic_if: 			Ext.getCmp('if_id').getValue(),
								ic_epo: 			Ext.getCmp('epo_id').getValue(),
								fecha_ini: 		Ext.getCmp('fecha_inicio_id').getValue(),
								fecha_fin: 		Ext.getCmp('fecha_fin_id').getValue(),
								nombre_if: 		Ext.getCmp('nombre_if_id').getValue(),
								nombre_epo:		Ext.getCmp('nombre_epo_id').getValue(),
													
								numero_tot: 	Ext.getCmp('numero_id').getValue(),
								monto_tot: 		Ext.getCmp('monto_id').getValue(),
								proveedor_tot: Ext.getCmp('proveedor_id').getValue(),
								docto_tot: 		Ext.getCmp('docto_id').getValue(),
								monto_dis_tot: Ext.getCmp('monto_dis_id').getValue(),
								tipo_moneda:	Ext.getCmp('moneda_id').getValue()
							}),
							callback: descargaArchivo
						});
					}
				}				
			]
		}
	});

/**********		Form Panel Avisos		**********/
	var formaAvisos = new Ext.form.FormPanel({
		width:					500,
		id:						'formaAvisos',
		layout:					'form',
		bodyPadding:			'20 20 20 20',
		style:					'margin: 0px auto 0px auto;',
		frame:					true,
		hidden:			 		true,
		autoHeight:				true,
		items: [{
			width: 				500,
			xtype:				'panel',
			id: 					'titulo',
			layout: 				'table',
			border: 				false,
			layoutConfig:		{ columns: 3 },
			defaults: 			{ align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	
					width: 		485,
					colspan: 	'3',
					border: 		false,
					frame: 		true,
					html:		  '<div align="center"><br><b>No se encontraron documentos operados y dispersados.</b><br>&nbsp;</div>'
				}
			]
		}]
	});
	
/**********		Form Panel Resumen Dispersi�n		**********/
	var formaDispersion = new Ext.form.FormPanel({
		width:			620,	
		labelWidth:		135,
		id:				'formaDispersion',				
		layout:			'form',
		bodyPadding:	'20 20 20 20',
		style:			'margin: 0px auto 0px auto;',
		frame:			true,
		border:			true,
		hidden:			true,
		autoHeight:		true,
		defaults:		{   
			msgTarget:	'side'
		},						
		items: [{
			xtype:  'panel',
			layout: 'table',
			border: true,
			width:  450,
			layoutConfig: { columns: 2},
			defaults: {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:  120,
				frame:  false,
				border: false,
				html:   '<div align="left"> &nbsp;&nbsp;&nbsp; Fecha de emisi�n: </div>'
			},{
				width:  300,
				xtype:  'displayfield',
				id:     'fecha_emision_id',
				name:   'fecha_emision',
				value:  new Date()
			}]
		},{
			xtype:  'panel',
			layout: 'table',
			border: true,
			width:  450,
			layoutConfig: { columns: 2},
			defaults: {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:  120,
				frame:  false,
				border: false,
				html:   '<div align="left"> &nbsp;&nbsp;&nbsp; Nombre IF: </div>'
			},{
				width:  330,
				xtype:  'displayfield',
				id:     'nombre_if_id',
				name:   'nombre_if',
				value:  '&nbsp;'
			}]
		},{
			xtype:  'panel',
			layout: 'table',
			border: true,
			width:  450,
			layoutConfig: { columns: 2},
			defaults: {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:  120,
				frame:  false,
				border: false,
				html:   '<div align="left"> &nbsp;&nbsp;&nbsp; Nombre EPO: </div>'
			},{
				width:  330,
				xtype:  'displayfield',
				id:     'nombre_epo_id',
				name:   'nombre_epo',
				value:  '&nbsp;'
			}]
		},{
			xtype:  'panel',
			layout: 'table',
			border: true,
			width:  450,
			layoutConfig: { columns: 4},
			defaults: {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:  120,
				frame:  false,
				border: false,
				html:   '<div align="left"> &nbsp;&nbsp;&nbsp; Fecha de inicio: <br></div>'
			},{
				width:  100,
				xtype:  'displayfield',
				id:     'fecha_inicio_id',
				name:   'fecha_inicio',
				value:  '&nbsp;'
			},{
				width:  120,
				frame:  false,
				border: false,
				html:   '<div align="left"> &nbsp;&nbsp;&nbsp; Fecha de t�rmino: <br></div>'
			},{
				width:  100,
				xtype:  'displayfield',
				id:     'fecha_fin_id',
				name:   'fecha_fin',
				value:  '&nbsp;'
			}]
		},{
			xtype:  'panel',
			layout: 'table',
			border: true,
			width:  450,
			layoutConfig: { columns: 1},
			defaults: {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:  100,
				xtype:  'displayfield',
				value:  '&nbsp;'
			}]
		}, gridConsulta,  gridTotal,
		{					
			width: 			350,
			xtype:			'displayfield',
			id:				'usuario_id',
			name:				'usuario',
			labelAlign: 	'right'
		},
		{ width: 350,	xtype: 'displayfield', 	id: 'if_id', 			name: 'if_hid', 			hidden: true },
		{ width: 350, 	xtype: 'displayfield', 	id: 'epo_id', 			name: 'epo_hid', 			hidden: true },
		{ width: 350, 	xtype: 'displayfield', 	id: 'docto_id', 		name: 'docto_hid', 		hidden: true },
		{ width: 350, 	xtype: 'displayfield', 	id: 'numero_id', 		name: 'numero_hid', 		hidden: true },
		{ width: 350, 	xtype: 'displayfield', 	id: 'monto_id', 		name: 'monto_hid', 		hidden: true },
		{ width: 350, 	xtype: 'displayfield', 	id: 'proveedor_id', 	name: 'proveedor_hid',	hidden: true },
		{ width: 350, 	xtype: 'displayfield',	id: 'monto_dis_id', 	name: 'monto_dis_hid',	hidden: true },
		{ width: 350, 	xtype: 'displayfield',	id: 'moneda_id', 		name: 'moneda_hid',		hidden: true }
		]
	});

/**********		Form Panel principal		**********/
	var formaPrincipal = new Ext.form.FormPanel({
		width:					500,	
		labelWidth:				60,
		id:						'formaPrincipal',
		title:					'Resumen IFs',				
		layout:					'form',
		bodyPadding:			'20 20 20 20',
		style:					'margin: 0px auto 0px auto;',
		frame:					true,
		autoHeight:				true,
		monitorValid:        true,
		defaults:				{   
			msgTarget:			'side'
		},
		items: [{
			width:				400,
			xtype:				'combo',	
			id:					'no_if_id',	 
			name:					'no_if',
			fieldLabel:			'&nbsp; IF',			
			mode:					'local',
			emptyText:			'Seleccione...',	
			triggerAction:		'all',	
			displayField:		'descripcion',	
			valueField:			'clave',		
			forceSelection:	true,
			allowBlank:			false,	
			store:				catalogoIF,		
			listeners: {
				select: {
					fn: function(combo){
						if( Ext.getCmp('no_if_id').getValue() != '' ){
							catalogoEPO.load({
								params: Ext.apply({
									ic_if: combo.getValue()
								})	
							});
						}
						var value = combo.getValue();
						var valueField = combo.valueField;
						var record;
						combo.getStore().each(function(r){
							if(r.data[valueField] == value){
								record = r;
								return false;
							}
						});
						Ext.getCmp('nombre_if_id').setValue(record.get(combo.displayField));						
					}
				}
			}
		},{
			width: 				400,
			xtype:				'combo',	
			id:					'no_epo_id',	 
			name:					'no_epo',
			fieldLabel:			'&nbsp; EPO',			
			mode:					'local',
			emptyText:			'Seleccione...',	
			triggerAction:		'all',
			displayField:		'descripcion',	
			valueField:			'clave',	
			forceSelection:	true,
			allowBlank:			false,
			store:				catalogoEPO,
			listeners: {
				select: {
					fn: function(combo){
						if( Ext.getCmp('no_if_id').getValue() == '' ){
							Ext.getCmp('no_epo_id').setValue('');
							Ext.getCmp('no_epo_id').markInvalid('Debe de seleccionar un IF');
						}
						var value = combo.getValue();
						var valueField = combo.valueField;
						var record;
						combo.getStore().each(function(r){
							if(r.data[valueField] == value){
								record = r;
								return false;
							}
						});
						Ext.getCmp('nombre_epo_id').setValue(record.get(combo.displayField));
					}
				}
			}
		},{
			xtype:				'compositefield',
			combineErrors:		false,
			items: [{
				width: 			170,
				xtype:			'datefield',
				id:				'periodo_inicio_id',
				name:				'periodo_inicio',
				fieldLabel:		'&nbsp; Periodo',
				minValue:		'01/01/1901',
				vtype:			'rangofecha',
				campoFinFecha:	'periodo_fin_id' ,
				msgTarget:		'side',
				anchor:			'90%',
				allowBlank:		false,
				value: 			new Date(),
				startDay:		1
			},{					
				width:			5,
				xtype:			'displayfield',
				id:				'display0_id',
				name:				'display0',
				value:			'&nbsp;',
				msgTarget:		'side',
				anchor:			'90%'
			},{					
				width:			35,
				xtype:			'displayfield',
				id:				'display1_id',
				name:				'display1',
				value:			'&nbsp; al:',
				msgTarget:		'side',
				anchor:			'90%'
			},{					
				width: 			175,
				xtype:			'datefield',
				id:				'periodo_fin_id',
				name:				'periodo_fin',
				minValue:		'01/01/1901',
				vtype:			'rangofecha',
				startDateField:'periodo_inicio_id',
				msgTarget:		'side',
				anchor:			'90%',
				allowBlank:		false,
				value: 			new Date(),
				startDay:		1
			}]
		},{					
			width: 				170,
			xtype:				'datefield',
			id:					'fecha_id',
			name:					'fecha',
			disabled:			true,
			hidden:				true,
			value: 				new Date()
		}],
		buttons: [{
			id:					'btnConsultar',
			text:					'Consultar',				
			iconCls: 			'icoBuscar',
			tooltip: 			'Consulta resumen dispersi�n',
			handler:				function(boton, evento){
				if( Ext.getCmp('no_if_id').getValue() == '' ){
					Ext.getCmp('no_if_id').markInvalid('Este campo es obligatorio');
				} else if( Ext.getCmp('no_epo_id').getValue() == '' ){
					Ext.getCmp('no_epo_id').markInvalid('Este campo es obligatorio');
				} else if(Ext.getCmp('formaPrincipal').getForm().isValid()){
					Ext.getCmp('formaAvisos').hide();
					Ext.getCmp('formaDispersion').hide();
					formaPrincipal.el.mask('Enviando...', 'x-mask-loading');	
					consultaData.load({
						params: Ext.apply({
							ic_if: 			Ext.getCmp('no_if_id').getValue(),
							ic_epo: 			Ext.getCmp('no_epo_id').getValue(),
							fecha_ini:		Ext.getCmp('periodo_inicio_id').getValue(),
							fecha_fin:		Ext.getCmp('periodo_fin_id').getValue(),
							fecha:			Ext.getCmp('fecha_id').getValue()
						})
					});
				}
			}
		},{
			id:					'btnLimpiar',
			text:					'Limpiar',				
			iconCls: 			'icoLimpiar',
			tooltip: 			'Limpiar form principal',
			handler:				function(boton, evento){
				Ext.getCmp('formaPrincipal').getForm().reset();
				Ext.getCmp('formaDispersion').getForm().reset();
				Ext.getCmp('formaDispersion').hide();
				Ext.getCmp('formaAvisos').hide();
			}
		}]
	});

/**********		Contenedor Principal		**********/
	var pnl = new Ext.Container({
		width:	949,	
		id:		'contenedorPrincipal',
		applyTo:	'areaContenido',
		items:	[
			NE.util.getEspaciador(20),
			formaPrincipal,
			NE.util.getEspaciador(20),
			formaDispersion,
			formaAvisos
		]
	});
	/***** Inicializo *****/
	//Ext.getCmp('formaDispersion').hide();	
	
});