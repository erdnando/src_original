Ext.onReady(function() {
		
	Ext.namespace('NE.monitorff');
			
	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaMonitorFlujoFondos = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						monitorFlujoFondos(resp.estadoSiguiente,resp);
					}
				);
			} else {
				monitorFlujoFondos(resp.estadoSiguiente,resp);
			}
			
		} else {
			
			var element;
			
			// Suprimir m�scara del Grid de Resumen de operaciones
			element = Ext.getCmp("gridResumenOperacion").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			/*
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Resetear forma
         resetWindowConfirmacionClave();
 
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			*/
					
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var monitorFlujoFondos = function(estado, respuesta ){
 
		
		if(			estado == "INICIALIZACION"							   ){
			
			// Debido a un bug en el layout de los composite fields para IE7
			if( Ext.isIE7 ) Ext.getCmp("panelFormaConsulta").hide();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitorff01ext.data.jsp',
				params: 	{
					informacion:		'MonitorFlujoFondos.inicializacion'
				},
				callback: 				procesaMonitorFlujoFondos
			});
			
		} else if(	estado == "INICIALIZAR_FORMA_CONSULTA"		   	){		
			
			// Mostrar panel con la forma de consulta
			Ext.getCmp("panelFormaConsulta").show();
			
			// Cargar Catalogo Concepto
			var catalogoConceptoData       	= Ext.StoreMgr.key('catalogoConceptoDataStore')
			catalogoConceptoData.load();
			
			// Cargar Catalogo Tipo de Dispersion
			var catalogoTipoDispersionData 	= Ext.StoreMgr.key('catalogoTipoDispersionDataStore');
			catalogoTipoDispersionData.load();
			
			// Borrar Contenido del Catalogo EPO
			var catalogoEPOData           	= Ext.StoreMgr.key('catalogoEPODataStore');
			catalogoEPOData.load({
				params: { 
					defaultValue: "TODAS"						
				}
			});
			
			// Borrar Contenido del Catalogo IF
			var catalogoIFData 					= Ext.StoreMgr.key('catalogoIFDataStore');
			catalogoIFData.load({
				params: { 
					defaultValue: "TODOS"						
				}
			});

			var fechaHoy 				= Ext.getCmp("fechaHoy");
			fechaHoy.setValue(	respuesta.fechaHoy 	);
			fechaHoy.originalValue	= fechaHoy.getValue();
			
			var fechaRegistroInicial 				= Ext.getCmp("fechaRegistroInicial");
			fechaRegistroInicial.setValue(	respuesta.fechaRegistroInicial 	);
			fechaRegistroInicial.originalValue	= fechaRegistroInicial.getValue();
			
			var fechaRegistroFinal					= Ext.getCmp("fechaRegistroFinal");
			fechaRegistroFinal.setValue(	   respuesta.fechaRegistroFinal		);
			fechaRegistroFinal.originalValue		= fechaRegistroFinal.getValue();
	
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitorff01ext.data.jsp',
				params: 	{
					informacion:		'MonitorFlujoFondos.inicializarFormaConsulta'
				},
				callback: 				procesaMonitorFlujoFondos
			});
 	
		} else if(			estado == "ESPERAR_DECISION"  					  ){ 
 
		} else if(			estado == "MOSTRAR_RESULTADO_OPERACIONES"  	  ){ 
						
			// Actualizar datos en grid
			var gridResumenOperacion = Ext.getCmp("gridResumenOperacion");
			gridResumenOperacion.getEl().unmask();
			
			Ext.Msg.alert(
				'Mensaje',
				respuesta.mensaje,
				function(btn, text){
						
					if( respuesta.actualizarResumenOperaciones ){
						Ext.getCmp('barraPaginacion').doRefresh();
					}
					
					// Determinar el estado siguiente
					Ext.Ajax.request({
						url: 						'15monitorff01ext.data.jsp',
						params: 	{
							informacion:		'MonitorFlujoFondos.mostrarResultadoOperaciones'
						},
						callback: 				procesaMonitorFlujoFondos
					});
				}
				
			);
					
		} else if(			estado == "REPROCESAR_OPERACIONES"				  ){
			
			// Agregar m�scara de reprocesamiento
			var gridEl = Ext.getCmp('gridResumenOperacion').getEl();
			gridEl.mask("Reprocesando Operaciones...","x-mask-loading");
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitorff01ext.data.jsp',
				params: 	{
					informacion:		'MonitorFlujoFondos.reprocesarOperaciones',
					reprocesar:			respuesta.reprocesar
				},
				callback: 				procesaMonitorFlujoFondos
			});
			
		} else if(			estado == "CANCELAR_OPERACIONES"					  ){
			
			// Agregar m�scara de reprocesamiento
			var gridEl = Ext.getCmp('gridResumenOperacion').getEl();
			gridEl.mask("Cancelando Operaciones...","x-mask-loading");
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitorff01ext.data.jsp',
				params: 	{
					informacion:		'MonitorFlujoFondos.cancelarOperaciones',
					cancelar:			respuesta.cancelar
				},
				callback: 				procesaMonitorFlujoFondos
			});
			
		} else if(			estado == "FIN"  					                 ){
 	
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"15monitorff01ext.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			forma.submit();
						
		}
		
	}
	
	//-------------------- 3. WINDOW DETALLE MATRIZ DISPERSION -----------------
	 
	var procesarSuccessFailureGeneraCSV =  function(opts, success, response) {
		
		var btnGenerarArchivoCSV = Ext.getCmp('btnGenerarArchivoCSV');
		btnGenerarArchivoCSV.setIconClass('icoGenerarArchivo');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot) ),'');
				// Insertar archivo
				Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					}
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
			});
			
		} else {
			
			btnGenerarArchivoCSV.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesaGenerarArchivoCSV = function(boton, evento) {
						
		boton.disable();
		boton.setIconClass('loading-indicator');
 
		var formParams = Ext.apply({}, Ext.getCmp("gridDetalleMatrizDispersion").formParams);
						
		// Generar Archivo CSV
		Ext.Ajax.request({
			url: 		'15monitorff01ext.data.jsp',
			params: 	Ext.apply(
				formParams, 
				{
					informacion: 'GenerarArchivoMatrizDispersion',
					tipo: 		 'CSV'
				}
			),
			callback: procesarSuccessFailureGeneraCSV
		});
						
	}
			
	var procesaCerrar = function(boton,evento){
		
		// Ocultar ventana de detalle
		hideWindowDetalleMatrizDispersion();
		 
	}
		
	var procesarConsultaDetalleMatrizDispersion = function(store, registros, opts){

		var gridDetalleMatrizDispersion = Ext.getCmp('gridDetalleMatrizDispersion');
		var el 								 = gridDetalleMatrizDispersion.getGridEl();
		el.unmask();
		
		if (registros != null) {
			
			/*
			if (!gridDetalleMatrizDispersion.isVisible()) {
				gridDetalleMatrizDispersion.show();
			}		
			*/
			
			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro.', 'x-mask');
			} else {
				// Habilitar boton para descargar archivo
				Ext.getCmp("btnGenerarArchivoCSV").enable();
			}
			// Copiar los formParams
			gridDetalleMatrizDispersion.formParams = Ext.apply({},opts.params);
 
		}
 
	}
	
	var detalleMatrizDispersionData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'detalleMatrizDispersionDataStore',
		url: 		'15monitorff01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetalleMatrizDispersion'
		},
		idIndex: 		1,
		fields: [	
			{ name: 'NUMERO_CAMPO'				},
			{ name: 'DESCRIPCION'				},
			{ name: 'VALOR'						}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
					showWindowDetalleMatrizDispersion();
				}
			},
			load: 	procesarConsultaDetalleMatrizDispersion,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDetalleMatrizDispersion(null, null, null);						
				}
			}
		}
		
	});
	
	var descripcionRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align: left;" ';
		return value;
			
	}
	
	var valorRenderer       = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align: left;" ';
		return value;
			
	}
	
	var gridDetalleMatrizDispersion = new Ext.grid.GridPanel({
		store: 	detalleMatrizDispersionData,
		id:		'gridDetalleMatrizDispersion',
		hidden: 	false,
		margins: '20 0 0 0',
		columns: [
			{
				header: 		'Num. de<br>Campo',
				tooltip: 	'N�mero de Campo',
				dataIndex: 	'NUMERO_CAMPO',
				sortable: 	false,
				resizable: 	true,
				width: 		60,
				hidden: 		false,
				align:		'center'
			},
			{
				header: 		'Descripci�n<br>&nbsp;',
				tooltip: 	'Descripci�n',
				dataIndex: 	'DESCRIPCION',
				sortable: 	false,
				resizable: 	true,
				width: 		180,
				hidden: 		false,
				align:		'center',
				renderer:	descripcionRenderer
			},
			{
				header: 		'Valor<br>&nbsp;',
				tooltip: 	'Valor',
				dataIndex: 	'VALOR',
				sortable: 	false,
				resizable: 	true,
				width: 		360,
				hidden: 		false,
				align:		'center',
				renderer:	valorRenderer
			}
		],
		formParams:	{},
		stripeRows: true,
		loadMask: 	true,
		height: 		316,		
		frame: 		false,
		flex:			10
	});

	var detalleOperacion = new Ext.Container({
		//xtype: 	'container',
		hidden: 	false,
		height:	38,
		flex:	  	0,
		layout:	'form',
      items:[
      	{
				xtype: 	'label',
				id:	 	'labelDetalleOperacion',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:center;margin:15px;',
				html:  	'Detalle de Operaci�n: '
			}
      ]
	});
 
	var panelDetalleMatrizDispersion = new Ext.Panel({
		//xtype: 	'panel',
		id:	 	'panelDetalleMatrizDispersion',
		frame:	false,
		border: 	false,
		baseCls:	'x-plain',
		height: 420,
		layout: {
			type:		'vbox',
			padding:	'5',
			align:	'stretch'
		},
		defaults:{
			margins:'0 0 0 0'
		},
		items: [ 
			detalleOperacion,
			gridDetalleMatrizDispersion
		],
		buttons: [
			{
				xtype: 		'button',
				text: 		'Generar Archivo',
				id: 			'btnGenerarArchivoCSV',
				iconCls:		'icoGenerarArchivo',
				handler: 	procesaGenerarArchivoCSV
			},
			{
				xtype: 		'button',
				text: 		'Bajar CSV',
				id: 			'btnBajarCSV',
				iconCls:		'icoBotonXLS',
				hidden: 		true
			},
			{
				text: 		'Cerrar',
				iconCls: 	'icoRegresar',
				handler: 	procesaCerrar
			}
		]
	});
	
	var hideWindowDetalleMatrizDispersion = function(){
 
		var ventana = Ext.getCmp('windowDetalleMatrizDispersion');
		if(ventana && ventana.isVisible() ){
			// Ocultar ventana
			ventana.hide();
			// Remover contenido del grid
			var detalleMatrizDispersionData = Ext.StoreMgr.key('detalleMatrizDispersionDataStore');
			detalleMatrizDispersionData.removeAll();
			// Ocultar Boton Bajar CSV
			Ext.getCmp("btnBajarCSV").hide();
			// Remover los parametros de la consulta anterior
			Ext.getCmp("gridDetalleMatrizDispersion").formParams = {};
      }
      
	}
	
	var showWindowDetalleMatrizDispersion = function(numeroRegistros){
 
		var ventana = Ext.getCmp('windowDetalleMatrizDispersion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Nafin Electr�nico',
				layout: 			'fit',
				width: 			650,
				height: 			430,
				minWidth: 		650,
				minHeight: 		430,
				resizable:		false,
				id: 				'windowDetalleMatrizDispersion',
				modal:			true,
				closable: 		false,
				items: [
					panelDetalleMatrizDispersion 
				]
			}).show();
			
		}
		
	}	
	
	//--------------------------------- 2. GRID RESUMEN OPERACION ---------------------------------------

	var ocultaColumnaIfFondeo = function(){
		var columnModel = Ext.getCmp("gridResumenOperacion").getColumnModel();
		var columnIndex = columnModel.getIndexById('columnNombreIfFondeo');
		columnModel.config[columnIndex].hideable = false;
		columnModel.setHidden( columnIndex, true );
	}
	
	var muestraColumnaIfFondeo = function(){
		var columnModel = Ext.getCmp("gridResumenOperacion").getColumnModel();
		var columnIndex = columnModel.getIndexById('columnNombreIfFondeo');
		columnModel.config[columnIndex].hideable = true;
		columnModel.setHidden( columnIndex, false );
	}
	
	var procesarConsultaResumenOperacion = function(store, registros, opts){

		var panelFormaConsulta   = Ext.getCmp('panelFormaConsulta');
		panelFormaConsulta.el.unmask();
		
		var gridResumenOperacion = Ext.getCmp('gridResumenOperacion');
		var el 						 = gridResumenOperacion.getEl();
		el.unmask();
		
		if (registros != null) {
			
			if(store.getTotalCount() == 0) {
				el.mask('No hay ninguna operaci�n con Error.', 'x-mask');
			}
			
			var esNuevaConsulta = "NuevaConsulta" === opts.params.operacion?true:false;
 
			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
			
			// Si es consulta nueva cargar totales
			if( esNuevaConsulta ){
				
				var jsonData = store.reader.jsonData;
				
				var totalRegistrosProcesados 	= Ext.getCmp("totalRegistrosProcesados");
				var totalRegistrosAceptados 	= Ext.getCmp("totalRegistrosAceptados");
				var totalRegistrosRechazados 	= Ext.getCmp("totalRegistrosRechazados");
				
				totalRegistrosProcesados.setValue( jsonData.totalRegistrosProcesados );
				totalRegistrosAceptados.setValue(  jsonData.totalRegistrosAceptados  );
				totalRegistrosRechazados.setValue( jsonData.totalRegistrosRechazados );
				
			}
			
			// Ocultar/Mostrar columna IF Fondeo dependiendo del tipo de dispersion seleccionada
			var esTipoDispersionIF = opts.params.claveTipoDispersion === "F"?true:false;

			if(        esNuevaConsulta &&  esTipoDispersionIF ){
				 muestraColumnaIfFondeo();
			} else if( esNuevaConsulta && !esTipoDispersionIF ){
				ocultaColumnaIfFondeo();
			}
			
		} else {
			
			var store = Ext.StoreMgr.key('resumenOperacionDataStore');
			if(store.getTotalCount() == 0) {
				el.mask('No hay ninguna operaci�n con Error.', 'x-mask');
			}
			
		}

	}
	
	NE.monitorff.procesaVerDetalle = function( rowIndex ){
		
		var record = Ext.StoreMgr.key('resumenOperacionDataStore').getAt(rowIndex);
		
		var icFlujoFondos = record.data['IC_FLUJO_FONDOS'];
		var tablaDetalle	= record.data['TABLA_DETALLE'];
		
		// Mostrar Detalle de la Operacion
		Ext.getCmp("labelDetalleOperacion").setText( 'Detalle de Operaci�n: ' + icFlujoFondos );
		
		// Deshabilitar boton de descarga archivo
		Ext.getCmp("btnGenerarArchivoCSV").disable();
				
		// Cargar contenido del Detalle
		var detalleMatrizDispersionData = Ext.StoreMgr.key("detalleMatrizDispersionDataStore");
		detalleMatrizDispersionData.load({
			params: {
				icFlujoFondos: icFlujoFondos,
				tablaDetalle:  tablaDetalle
			}
		});
		
	}
	
	var procesaReprocesar = function( boton, evento ){
		
		var reprocesar = new Array();
		
		var fechaHoy   = Ext.getCmp("fechaHoy").getValue();
		
		var resumenOperacionData = Ext.StoreMgr.key('resumenOperacionDataStore');
		var abortarOperacionMsg	 = "";
		resumenOperacionData.each(
			function(record){
				
				var valor 			= record.data["VALOR_SELECCIONAR"];
				var estatus 		= valor.substring(0,2); // Se conserva el bug de la versi�n original
				var fechaRegistro = valor.substring(valor.lastIndexOf('|')+1, valor.length);
				
				if(record.data["SELECCIONAR"] === true ){
					
					if( datecomp(fechaRegistro, fechaHoy) !== 0 ){
						abortarOperacionMsg = "S�lo se pueden reprocesar operaciones del d�a.";
						return false;
					} else if( estatus !== "99" ) {
						abortarOperacionMsg = "Los registros en rojo no pueden ser reprocesados\nhasta que tengan una cuenta v�lida.";
						return false;
					}
					reprocesar.push( record.data["VALOR_SELECCIONAR"] );
					
				}
				
			}
		);
		
		// Si se encontr� alg�n registro que no cumple con el criterio, abortar la operacion.
		if( !Ext.isEmpty(abortarOperacionMsg) ){
			Ext.Msg.alert( "Aviso", abortarOperacionMsg );
			return;
		// No se seleccion� ning�n registro, abortar la operacion.
		} else if( reprocesar.length === 0 ){
			Ext.Msg.alert( "Aviso", "Debe de seleccionar al menos un registro para Reprocesar.");
			return;
		}
		
		var respuesta 			= new Object();
		respuesta.reprocesar = reprocesar;
		monitorFlujoFondos('REPROCESAR_OPERACIONES',respuesta);
 
	}
	
	var procesaCancelar = function( boton, evento ){
		
		var cancelar = new Array();
				
		var resumenOperacionData = Ext.StoreMgr.key('resumenOperacionDataStore');
		var abortarOperacionMsg	 = "";
		resumenOperacionData.each(
			function(record){
				
				if(record.data["SELECCIONAR"] === true ){
					
					if( Ext.isEmpty( record.data["CAUSA"] ) ){
						abortarOperacionMsg = "Debe de capturar una causa para poder cancelar la operaci�n.";
						return false;
					} else {
						cancelar.push( record.data["VALOR_SELECCIONAR"] + "|" + record.data["CAUSA"] );
					}
					
				}
				
			}
		);
		
		// Si se encontr� alg�n registro que no cumple con el criterio, abortar la operacion.
		if( !Ext.isEmpty(abortarOperacionMsg) ){
			Ext.Msg.alert( "Aviso", abortarOperacionMsg );
			return;
		// No se seleccion� ning�n registro, abortar la operacion.
		} else if( cancelar.length === 0 ){
			Ext.Msg.alert( "Aviso", "Debe de seleccionar al menos un registro para Cancelar.");
			return;
		}
		
		var respuesta 			= new Object();
		respuesta.cancelar = cancelar;
		monitorFlujoFondos('CANCELAR_OPERACIONES',respuesta);

	}
	
	var resumenOperacionData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'resumenOperacionDataStore',
		url: 		'15monitorff01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaResumenOperacion'
		},
		idIndex: 		1,
		fields: [
			{ name: 'NOMBRE_EPO', 						type: 'string'	      },
			{ name: 'NOMBRE_IF', 						type: 'string'	      },
			{ name: 'NOMBRE_PYME', 						type: 'string'	      },
			{ name: 'RFC', 								type: 'string'	      },
			{ name: 'NOMBRE_IF_FONDEO', 				type: 'string'	      },
			{ name: 'BANCO', 								type: 'string'	      },
			{ name: 'TIPO_CUENTA', 						type: 'string'	      },
			{ name: 'NUMERO_CUENTA', 					type: 'string'	      },
			{ name: 'IMPORTE', 							type: 'float', convert: function(value, record){ return Number(value.replace(/[\$, ]/g,"")); } },
			{ name: 'MOTIVO_RECHAZO', 					type: 'string'	      },
			{ name: 'EXISTE_DETALLE', 					type: 'boolean'      },
			{ name: 'VER_DETALLE', 						type: 'string'	      },
			{ name: 'SELECCIONAR', 						type: 'boolean'      },
			{ name: 'VALOR_SELECCIONAR',				type: 'string'			},
			{ name: 'CAUSA', 								type: 'string'	      },
			{ name: 'ESTATUS_CECOBAN', 				type: 'string'	      },
			{ name: 'ESTATUS_FFON', 					type: 'string'	      },
			{ name: 'IC_FLUJO_FONDOS',					type:	'string'			},
			{ name: 'TABLA_DETALLE',					type:	'string'			}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
					
					var gridEl = Ext.getCmp("gridResumenOperacion").getEl();
					if( gridEl.isMasked() ){
						gridEl.unmask();
					}
						
					var gridResumenOperacion = Ext.getCmp("gridResumenOperacion");
					if( !gridResumenOperacion.isVisible() ){
						gridResumenOperacion.show();
					}
				}
			},
			load: 	procesarConsultaResumenOperacion,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaResumenOperacion(null, null, null);						
				}
			}
		}
		
	});
	
	var nombreEpoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var nombreIfRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var nombrePymeRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var rfcRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var nombreIfFondeoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var bancoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var tipoCuentaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var numeroCuentaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var importeRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;text-align:right;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		value 			= record.json['IMPORTE'];
		return value;
			
	}
	
	var motivoRechazoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		if( record.data['ESTATUS_CECOBAN'] !== "99" ){
			metadata.attr += 'color: red;';
		}
		metadata.attr += '" ';
		return value;
			
	}
	
	var verDetalleRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		if( record.data["EXISTE_DETALLE"] === true ){
			
			value = 
				"<a href=\"javascript:NE.monitorff.procesaVerDetalle(" + rowIndex + ");\"> "  +
				"   <img border=\"0\" src=\"" + Ext.BLANK_IMAGE_URL + "\" class=\"icoLupa\" ext:qtip=\"Ver Detalle\" align=\"middle\" >Ver"  +
				"    "  +
				"</a> ";
 
		} else {
			
			metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
			if( record.data['ESTATUS_CECOBAN'] !== "99" ){
				metadata.attr += 'color: red;';
			}
			metadata.attr += '" ';
		}
		
		return value;
			
	}
	
	var originalCheckColumnRenderer = (new Ext.grid.CheckColumn()).renderer;
	
	var seleccionarRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
      var v;
      
      if( "60" === record.data['ESTATUS_FFON'] || "9999" === record.data['ESTATUS_FFON'] ) {
   		v = '';
   	} else {
   		v = originalCheckColumnRenderer.apply(this,arguments);
   	}
   	return v;
			
	}
	
	var causaRenderer = function( value, metadata, record, rowIndex, colIndex, store){

		var v;
      
   	if( "60" === record.data['ESTATUS_FFON'] || "9999" === record.data['ESTATUS_FFON'] ) {
   		v = '';
   	} else {
   		v = NE.util.colorCampoEdit( value, metadata, record );
   		metadata.style += 'text-align:left;';
   	}
   	return v;
			
	}
	
	var gridResumenOperacionColModel = new Ext.grid.ColumnModel({
		columns: [
			{
				header: 		'EPO<br>&nbsp;',
				tooltip: 	'EPO',
				dataIndex: 	'NOMBRE_EPO',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				align:		'center', 
				renderer:	nombreEpoRenderer,
				editable:	false
			},
			{
				header: 		'IF<br>&nbsp;',
				tooltip: 	'IF',
				dataIndex: 	'NOMBRE_IF',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				align:		'center', 
				renderer:	nombreIfRenderer,
				editable:	false
			},
			{
				header: 		'PYME<br>&nbsp;',
				tooltip: 	'PYME',
				dataIndex: 	'NOMBRE_PYME',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				align:		'center', 
				renderer:	nombrePymeRenderer,
				editable:	false
			},
			{
				header: 		'RFC<br>&nbsp;',
				tooltip: 	'RFC',
				dataIndex: 	'RFC',
				sortable: 	true,
				resizable: 	true,
				width: 		60,
				align:		'center', 
				renderer:	rfcRenderer,
				editable:	false
			},
			{
				header: 		'IF Fondeo<br>&nbsp;',
				tooltip: 	'Intermediario Financiero Fondeo',
				id:			'columnNombreIfFondeo',
				dataIndex: 	'NOMBRE_IF_FONDEO',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				align:		'center', 
				renderer:	nombreIfFondeoRenderer,
				editable:	false
			},
			{
				header: 		'Banco<br>&nbsp;',
				tooltip: 	'Banco',
				dataIndex: 	'BANCO',
				sortable: 	true,
				resizable: 	true,
				width: 		60,
				align:		'center', 
				renderer:	bancoRenderer,
				editable:	false
			},
			{
				header: 		'Tipo de<br>Cuenta',
				tooltip: 	'Tipo de Cuenta',
				dataIndex: 	'TIPO_CUENTA',
				sortable: 	true,
				resizable: 	true,
				width: 		60,
				align:		'center', 
				renderer:	tipoCuentaRenderer,
				editable:	false
			},
			{
				header: 		'No. de<br>Cuenta',
				tooltip: 	'No. de Cuenta',
				dataIndex: 	'NUMERO_CUENTA',
				sortable: 	true,
				resizable: 	true,
				width: 		124,
				align:		'center', 
				renderer:	numeroCuentaRenderer,
				editable:	false
			},
			{
				header: 		'Importe<br>&nbsp;',
				tooltip: 	'Importe',
				dataIndex: 	'IMPORTE',
				sortable: 	true,
				resizable: 	true,
				width: 		120,
				align:		'center', 
				renderer:	importeRenderer,
				editable:	false
			},
			{
				header: 		'Motivo de<br>Rechazo',
				tooltip: 	'Motivo de Rechazo',
				dataIndex: 	'MOTIVO_RECHAZO',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				align:		'center', 
				renderer:	motivoRechazoRenderer,
				editable:	false
			},
			{
				header: 		'Ver<br>Detalle',
				tooltip: 	'Ver Detalle',
				dataIndex: 	'VER_DETALLE',
				sortable: 	false,
				resizable: 	false,
				width: 		120,
				align:		'center', 
				renderer:	verDetalleRenderer,
				editable:	false
			},
			{
				xtype:		'checkcolumn',
				header: 		'Seleccionar<br>&nbsp;',
				tooltip: 	'Seleccionar',
				dataIndex:	'SELECCIONAR',
				sortable: 	true,
				resizable: 	false,
				width: 		80,
				align:		'center', 
				renderer:	seleccionarRenderer,
				processEvent: function(name, e, grid, rowIndex, colIndex){
					  if (name == 'mousedown') {
							var record = grid.store.getAt(rowIndex);
							if( "60" === record.data['ESTATUS_FFON'] || "9999" === record.data['ESTATUS_FFON'] ) {
								return false; // Cancel click on void cell.
							}
							record.set(this.dataIndex, !record.data[this.dataIndex]);
							return false; // Cancel row selection.
					  } else {
							return Ext.grid.ActionColumn.superclass.processEvent.apply(this, arguments);
					  }
				 }
				//, editable:	false
			},
			{
				header: 		'Causa<br>&nbsp;',
				tooltip: 	'Causa',
				dataIndex: 	'CAUSA',
				sortable: 	true,
				resizable: 	true,
				width: 		120,
				align:		'center', 
				editor: {
					xtype:	'textarea',
					id: 		'textAreaCausa',
					maxLength: 256,
					enableKeyEvents: true,
					listeners:	{
						keyup: function(field, e){      // S�lo se permite como m�ximo 255 caracteres.
							if ( (field.getValue()).length > 255){
								field.setValue((field.getValue()).substring(0,255));							
								field.focus();
							}
						},
						keypress: function( field, e ){ // Deshabilitar la captura del salto de l�nea.
							if( e.getKey() == 13 ){
								e.stopEvent();
								if(Ext.isIE) {e.browserEvent.keyCode = 0;}
							}
						}
					}
				},
				renderer:	causaRenderer
				//, editable:	true
			}
		],
		isCellEditable: function(col, row) {
			
			var columnDataIndex = this.getDataIndex(col);
			if(        columnDataIndex === "SELECCIONAR"   ){
				
				var store  = Ext.StoreMgr.key('resumenOperacionDataStore');
				var record = store.getAt(row);
				if( "60" === record.data['ESTATUS_FFON'] || "9999" === record.data['ESTATUS_FFON'] ) {
					return false;
				} else {
					return true;	
				}
				
			} else if( columnDataIndex === "CAUSA" 		  ){
				
				var store  = Ext.StoreMgr.key('resumenOperacionDataStore');
				var record = store.getAt(row);
				if( "60" === record.data['ESTATUS_FFON'] || "9999" === record.data['ESTATUS_FFON'] ) {
					return false;
				} else {
					return true;
				}
				
			}
			
			return Ext.grid.ColumnModel.prototype.isCellEditable.call(this, col, row);
			
		}
		
	});
	
	var gridResumenOperacion = {
		store: 		resumenOperacionData,
		xtype: 		'editorgrid',
		id:			'gridResumenOperacion',
		title:		'Resumen',
		frame:		true,
		stripeRows: true,
		loadMask: 	true,
		width:		940,
		height: 		464,
		hidden:		true,
		style: {
			borderWidth: 0
		},
		colModel: 	gridResumenOperacionColModel,
		stateful:	false,
		tbar: {
			xtype: 		'container',
			layout:		'form',
			labelWidth: 174,
			//height:	28,
			//width:		400,
			items: [
				NE.util.getEspaciador(10),
				// TEXTFIELD Total de Registros Procesados
				{ 
					xtype: 			'textfield',
					fieldLabel:		'Total de Registros Procesados',
					name: 			'totalRegistrosProcesados',
					id: 				'totalRegistrosProcesados',
					readOnly:		true,
					hidden: 			false,
					maxLength: 		9,
					msgTarget: 		'side',
					width:			126,
					style: {
						textAlign: 'left'
					}
				},
				// TEXTFIELD Total de Registros Aceptados
				{ 
					xtype: 			'textfield',
					fieldLabel:		'Total de Registros Aceptados',
					name: 			'totalRegistrosAceptados',
					id: 				'totalRegistrosAceptados',
					readOnly:		true,
					hidden: 			false,
					maxLength: 		9,
					msgTarget: 		'side',
					width:			126,
					style: {
						textAlign: 'left'
					}
				},
				// TEXTFIELD Total de Registros Rechazados
				{ 
					xtype: 			'textfield',
					fieldLabel:		'Total de Registros Rechazados',
					name: 			'totalRegistrosRechazados',
					id: 				'totalRegistrosRechazados',
					readOnly:		true,
					hidden: 			false,
					maxLength: 		9,	
					msgTarget: 		'side',
					width:			126,
					style: {
						textAlign: 'left'
					}
				},
				NE.util.getEspaciador(10)
			]
		},
		bbar: {
			xtype: 			'paging',
			pageSize: 		15,
			buttonAlign: 	'left',
			id: 				'barraPaginacion',
			opts:				null,
			displayInfo: 	true,
			store: 			resumenOperacionData,
			displayMsg: 	'{0} - {1} de {2}',
			emptyMsg: 		"No hay registros.",
			items:  [
				'->',
				'-',
				{
					xtype: 	'button',
					text: 	'Reprocesar',
					iconCls: 'icoReprocesar',
					id: 		'botonReprocesar',
					handler: procesaReprocesar 
				},
				'-',
				{
					xtype: 	'button',
					text: 	'Cancelar',
					iconCls: 'icoCancelar',
					id: 		'botonCancelar',
					handler: procesaCancelar 
				}
			]
		} 
	};
	 
	//--------------------------------- 1. PANEL FORMA CONSULTA ---------------------------------------
	
	var procesaConsultar = function( boton, evento ){
		
		var panelFormaConsulta = Ext.getCmp("panelFormaConsulta");
		
		var invalidForm = false;
		if( !panelFormaConsulta.getForm().isValid() ){
			invalidForm = true;
		}
 
		// VALIDAR QUE SE HAYA SELECCIONADO UN CONCEPTO
		// 'Debe de seleccionar un Concepto'
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE NO SE HAYA SELECCIONADO EL CONCEPTO CARGOS
		var comboConcepto = Ext.getCmp("comboConcepto");
		if( comboConcepto.isValid() && comboConcepto.getValue() === 'C' ){
			invalidForm = true;
			comboConcepto.markInvalid('El concepto de cargos por el momento no esta disponible.');
		}
			
		// VALIDAR QUE SE HAYA SELECCIONADO UN TIPO DE DISPERSION
		// 'Debe de seleccionar un Tipo de Dispersi�n'
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDACION ADICIONAL: DEPENDIENDO DEL TIPO DE DISPERSION, VALIDAR QUE
		// EL COMBO EPO O IF TENGAN VALOR
		var comboTipoDispersion = Ext.getCmp("comboTipoDispersion");
		if( 			comboTipoDispersion.isValid() && comboTipoDispersion.getValue() === 'E' ){
			
			var comboEPO = Ext.getCmp("comboEPO");
			if( Ext.isEmpty( comboEPO.getValue() ) ){
				invalidForm = true;
				comboEPO.markInvalid("Este campo es obligatorio");	
			}
			
		} else if(	comboTipoDispersion.isValid() && comboTipoDispersion.getValue() === 'F' ){
			
			var comboIF = Ext.getCmp("comboIF");
			if( Ext.isEmpty( comboIF.getValue() ) ){
				invalidForm = true;
				comboIF.markInvalid("Este campo es obligatorio");	
			}
			
		}
		
		// VALIDAR QUE SE HAYA ESPECIFICADO EL RANGO DE FECHAS DE REGISTRO
		// 'Debe de capturar la fecha de registro final'
		// 'Debe de capturar la fecha de registro inicial'
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE LAS FECHAS DE REGISTRO TENGAN EL FORMATO DD/MM/AAAA
		// Nota: La validacion ya se realiza en la forma
		
		// VALIDAR QUE LAS FECHA DE REGISTRO FINAL SEA MAYOR O IGUAL A FECHA DE REGISTRO INICIAL
		// 'La fecha de registro final debe ser mayor o igual a la inicial'
		// Nota: La validacion ya se realiza en la forma
		
		if( invalidForm ){
			return; // La forma es invalida, se cancela la operacion
		}
		
		// Aplicar m�scara de consulta
		var panelFormaConsulta = Ext.getCmp("panelFormaConsulta");
		panelFormaConsulta.el.mask("Consultando...","x-mask-loading");
		
		// Realizar consulta
		var resumenOperacionData = Ext.StoreMgr.key("resumenOperacionDataStore");
		resumenOperacionData.load({
			params: Ext.apply(Ext.getCmp("panelFormaConsulta").getForm().getValues(),{
				operacion: 	'NuevaConsulta', //Generar datos para la consulta
				start: 		0,
				limit: 		15
			})
		});	

	}
	
	var procesaLimpiar = function(boton, evento ){
		
		Ext.getCmp("panelFormaConsulta").getForm().reset();
		Ext.getCmp("fechaRegistroInicial").setMaxValue('');
		Ext.getCmp("fechaRegistroFinal").setMinValue('');
		Ext.getCmp("comboEPO").hide();
		Ext.getCmp("comboIF").hide();
		Ext.getCmp("displayOperaFideicomiso").hide();

	}
	
	var catalogoConceptoData = new Ext.data.JsonStore({
		id: 					'catalogoConceptoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitorff01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoConcepto'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboConcepto = Ext.getCmp("comboConcepto");
						comboConcepto.setValue(defaultValue);
						comboConcepto.originalValue = comboConcepto.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoTipoDispersionData = new Ext.data.JsonStore({
		id: 					'catalogoTipoDispersionDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitorff01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoTipoDispersion'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboTipoDispersion = Ext.getCmp("comboTipoDispersion");
						comboTipoDispersion.setValue(defaultValue);
						comboTipoDispersion.originalValue = comboTipoDispersion.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 					'catalogoEPODataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitorff01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoEPO'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboEPO = Ext.getCmp("comboEPO");
						comboEPO.setValue(defaultValue);
						comboEPO.originalValue = comboEPO.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoIFData = new Ext.data.JsonStore({
		id: 					'catalogoIFDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg', 'opera_fideicomiso' ],
		url: 					'15monitorff01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIF'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						var comboIF = Ext.getCmp("comboIF");
						comboIF.setValue(defaultValue);
						comboIF.originalValue = comboIF.getValue();
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementosPanelFormaConsulta = [
		// COMBO CONCEPTO
		{
			xtype: 				'combo',
			name: 				'comboConcepto',
			id: 					'comboConcepto',
			fieldLabel: 		'Concepto',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveConcepto',
			emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoConceptoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		// COMBO TIPO DE DISPERSION
		{
			xtype: 				'combo',
			name: 				'comboTipoDispersion',
			id: 					'comboTipoDispersion',
			fieldLabel: 		'Tipo de Dispersi�n',
			mode: 				'local', 
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveTipoDispersion',
			emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTipoDispersionData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
			listeners:			{
				select:	function( combo, record, index ){

					var comboIF	 					 = Ext.getCmp("comboIF");
					var comboEPO 					 = Ext.getCmp("comboEPO");
					var displayOperaFideicomiso = Ext.getCmp("displayOperaFideicomiso");
					var ifOperaFideicomiso		 = Ext.getCmp("ifOperaFideicomiso");
					
					if( 			combo.getValue() === "E" ){
						
						comboIF.hide();
						displayOperaFideicomiso.hide();
						comboIF.reset();
						ifOperaFideicomiso.reset();

						comboEPO.reset();
						comboEPO.show();

					} else if( 	combo.getValue() === "F" ){
						
						comboEPO.hide();
						comboEPO.reset();
						
						comboIF.reset();
						ifOperaFideicomiso.reset();
						displayOperaFideicomiso.hide();
						comboIF.show();

					}
				
				}
			}
		},
		// COMBO EPO
		{
			xtype: 				'combo',
			hidden:				true,
			name: 				'comboEPO',
			id: 					'comboEPO',
			width:				310,
			fieldLabel: 		'EPO',
			mode: 				'local', 
			allowBlank:			true,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPO',
			//emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			/*anchor:				'-20',*/
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		// COMBO IF
		{
			xtype: 				'combo',
			hidden:				true,
			name: 				'comboIF',
			id: 					'comboIF',
			width:				310,
			fieldLabel: 		'IF',
			mode: 				'local', 
			allowBlank:			true,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveIF',
			//emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoIFData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			/*anchor:				'-20',*/
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
			listeners: {
				select:	function( combo, record, index ){

					var displayOperaFideicomiso = Ext.getCmp("displayOperaFideicomiso");
					var ifOperaFideicomiso		 = Ext.getCmp("ifOperaFideicomiso");
					if( 			record.data['opera_fideicomiso'] === "S" ){
						displayOperaFideicomiso.show();
						ifOperaFideicomiso.setValue("S");
					} else {
						displayOperaFideicomiso.hide();
						ifOperaFideicomiso.setValue("N");
					}
				
				}
			}
		},
		// DISPLAYFIELD IF OPERA FIDEICOMISO
		{
			xtype: 				'displayfield',
			id:					'displayOperaFideicomiso',
			name:					'displayOperaFideicomiso',	
			hidden:				true,
			value: 				'IF Opera Fideicomiso para Desarrollo de Proveedores',
			style:				'text-align: left; padding:0; padding-bottom: 2;'
		},
		// DATERANGE: FECHA DE REGISTRO
		{
			xtype: 						'compositefield',
			fieldLabel: 				'Fecha de Registro',
			combineErrors: 			false,
			msgTarget: 					'side',
			items: [
				{
					xtype: 				'datefield',
					name: 				'fechaRegistroInicial',
					id: 					'fechaRegistroInicial',
					allowBlank: 		false,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoFinFecha: 	'fechaRegistroFinal',
					margins: 			'0 20 0 0',
					allowBlank:			false,
					disabledDates: 	["../../0000"],
					disabledDatesText: "La fecha no es v�lida"
				},{
					xtype: 				'displayfield',
					value: 				'a',
					width: 				20
				},{
					xtype: 				'datefield',
					name: 				'fechaRegistroFinal',
					id: 					'fechaRegistroFinal',
					allowBlank: 		false,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoInicioFecha: 'fechaRegistroInicial',
					margins: 			'0 20 0 0',
					allowBlank:			false,
					disabledDates: 	["../../0000"],
					disabledDatesText: "La fecha no es v�lida"
				}
			]
		},
		{
			xtype: 'hidden',
			id:	 'fechaHoy',
			name:	 'fechaHoy'
		},
		{
			xtype: 'hidden',
			id:	 'ifOperaFideicomiso',
			name:	 'ifOperaFideicomiso'
		}
	];
	
	var panelFormaConsulta = new Ext.form.FormPanel({
		id: 				'panelFormaConsulta',
		layout:			'form',
		width: 			474,
		title: 			'Forma de Consulta',
		hidden:			Ext.isIE7?false:true, // Debido a un bug en el layout de los composite fields para IE7
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto; ', 
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	110,
		defaultType: 	'textfield',
		items: 			elementosPanelFormaConsulta,
		monitorValid: 	false,
		buttons: [
			{
				text: 		'Consultar',
				iconCls: 	'icoBuscar',
				handler: 	procesaConsultar
			},
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	procesaLimpiar
			}
		]
	});
	
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelFormaConsulta,
			NE.util.getEspaciador(10),
			gridResumenOperacion
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	monitorFlujoFondos("INICIALIZACION",null);
	
});