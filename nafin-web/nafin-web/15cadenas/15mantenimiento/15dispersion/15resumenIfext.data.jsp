<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject, 
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.dispersion.*,
		com.netro.model.catalogos.*,	
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion 	= request.getParameter("informacion")   == null?"":(String)request.getParameter("informacion");
String ic_if 		 	= request.getParameter("ic_if")         == null?"":(String)request.getParameter("ic_if");
String ic_epo 		 	= request.getParameter("ic_epo")        == null?"":(String)request.getParameter("ic_epo");
String fecha_ini		= request.getParameter("fecha_ini")     == null?"":(String)request.getParameter("fecha_ini");
String fecha_fin		= request.getParameter("fecha_fin")     == null?"":(String)request.getParameter("fecha_fin");
String fecha			= request.getParameter("fecha")		    == null?"":(String)request.getParameter("fecha");
String usuario_log	= request.getParameter("usuario")	    == null?"":(String)request.getParameter("usuario");
String nombre_if	 	= request.getParameter("nombre_if")     == null?"":(String)request.getParameter("nombre_if");
String nombre_epo	 	= request.getParameter("nombre_epo")    == null?"":(String)request.getParameter("nombre_epo");
String tipo_moneda 	= request.getParameter("tipo_moneda")   == null?"":(String)request.getParameter("tipo_moneda");
String numero_tot	 	= request.getParameter("numero_tot")    == null?"":(String)request.getParameter("numero_tot");
String monto_tot		= request.getParameter("monto_tot")     == null?"":(String)request.getParameter("monto_tot");
String proveedor_tot	= request.getParameter("proveedor_tot") == null?"":(String)request.getParameter("proveedor_tot");
String docto_tot		= request.getParameter("docto_tot")     == null?"":(String)request.getParameter("docto_tot");
String monto_dis_tot	= request.getParameter("monto_dis_tot") == null?"":(String)request.getParameter("monto_dis_tot");

String nombreArchivo = "", consulta = "", titulo = "", infoRegresar = "";
HashMap datos = new HashMap();

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("catalogoIF")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COMCAT_IF");
	cat.setCampoClave("IC_IF");
	cat.setCampoDescripcion("CG_RAZON_SOCIAL");
	cat.setOrden("CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();	
	
} else if(informacion.equals("catalogoEPO")){

	CatalogoEpoResumenIf cat = new CatalogoEpoResumenIf();
	cat.setClave("E.IC_EPO");
	cat.setDescripcion("E.CG_RAZON_SOCIAL");
	cat.setIcIf(Integer.parseInt(ic_if));
	infoRegresar = cat.getJSONElementos();	

} else if(informacion.equals("ConsultaDatosGrid")){
		
	Registros registros 					= new Registros();
	JSONObject resultado 				= new JSONObject();	
	ConsultaDispersion paginador 		= new ConsultaDispersion();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String monto 							= "";
	String usuario 						= "";
	int numeroTot = 0, proveedorTot = 0, docTot = 0;
	double montoTot = 0, montoDispTot = 0;
	
	/***** Convierto el formato de las fechas *****/
	fecha = fecha.substring(8,10) + "/" + fecha.substring(5,7) + "/" + fecha.substring(0,4);	
	if( !fecha_ini.equals("") && !fecha_fin.equals("") ){
		fecha_ini = fecha_ini.substring(8,10) + "/" + fecha_ini.substring(5,7) + "/" + fecha_ini.substring(0,4);
		fecha_fin = fecha_fin.substring(8,10) + "/" + fecha_fin.substring(5,7) + "/" + fecha_fin.substring(0,4);
	}
	usuario = "Elaboró: " + strNombreUsuario;	
	
	paginador.setIcIf(Integer.parseInt(ic_if));
	paginador.setIcEpo(Integer.parseInt(ic_epo));
	paginador.setFechaInicio(fecha_ini);
	paginador.setFechaFin(fecha_fin);
	paginador.setIcMoneda(1);
	
	registros = queryHelper.doSearch();
	
	if(registros.getNumeroRegistros() > 0){
	
		while(registros.next()) { 
			/***** Calculo los totales *****/
			numeroTot 		+= Integer.parseInt(registros.getString("NUMERO"));
			montoTot 		+= Double.parseDouble(registros.getString("MONTO_OPERADOS"));
			proveedorTot 	+= Integer.parseInt(registros.getString("NUMERO_PROVEEDOR"));
			docTot 			+= Integer.parseInt(registros.getString("NUMERO_DOC"));
			montoDispTot	+= Double.parseDouble(registros.getString("MONTO_DISPERSADOS"));
			
		}
	
		titulo = "RESUMEN DISPERSIÓN M. N.";
		
		resultado.put("success", 			new Boolean(true)						);
		resultado.put("usuario",			usuario		 							);
		resultado.put("titulo",				titulo		 							);
		resultado.put("fecha_emision",	fecha			 							);
		resultado.put("fecha_inicio", 	fecha_ini								);
		resultado.put("fecha_fin", 		fecha_fin								);
		resultado.put("tipo_moneda", 		"1"										);
		resultado.put("numeroTot", 		""+numeroTot							);
		resultado.put("montoTot", 			""+montoTot								);
		resultado.put("proveedorTot", 	""+proveedorTot						);
		resultado.put("docTot", 			""+docTot								);
		resultado.put("montoDispTot",		""+montoDispTot						);
		resultado.put("total", 				""+registros.getNumeroRegistros());
		resultado.put("registros", 		registros.getJSONData()				);
		
	} else {
		
		paginador.setIcMoneda(54);
		registros = queryHelper.doSearch();
		
		if(registros.getNumeroRegistros() > 0){

			while(registros.next()) { 
				/***** Calculo los totales *****/
				numeroTot		+= Integer.parseInt(registros.getString("NUMERO"));
				montoTot 		+= Double.parseDouble(registros.getString("MONTO_OPERADOS"));
				proveedorTot	+= Integer.parseInt(registros.getString("NUMERO_PROVEEDOR"));
				docTot 			+= Integer.parseInt(registros.getString("NUMERO_DOC"));
				montoDispTot	+= Double.parseDouble(registros.getString("MONTO_DISPERSADOS"));
			
			}
			
			titulo = "RESUMEN DISPERSIÓN USD";
			
			resultado.put("success", 			new Boolean(true)						);
			resultado.put("fecha_emision",	fecha			 							);
			resultado.put("usuario",			usuario		 							);
			resultado.put("titulo",				titulo		 							);
			resultado.put("fecha_inicio", 	fecha_ini								);
			resultado.put("fecha_fin", 		fecha_fin								);
			resultado.put("tipo_moneda", 		"54"										);
			resultado.put("numeroTot", 		""+numeroTot							);
			resultado.put("montoTot", 			""+montoTot								);
			resultado.put("proveedorTot", 	""+proveedorTot						);
			resultado.put("docTot", 			""+docTot								);
			resultado.put("montoDispTot",		""+montoDispTot						);
			resultado.put("total", 				""+registros.getNumeroRegistros());
			resultado.put("registros", 		registros.getJSONData()				);		
		} else{
			resultado.put("success", 	new Boolean(true)	);
			resultado.put("total", 	 	"0"					);
			resultado.put("registros", ""						);		
		}
	}	
	
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("ConsultaTotales")){

	JSONObject resultado = new JSONObject();	
	String cadena = "[{\"TOTAL\":\"Total\",\"NUMERO_TOT\":\"" + numero_tot +
			 "\",\"MONTO_OPERADOS_TOT\":\"" + monto_tot + "\",\"NUMERO_PROVEEDOR_TOT\":\"" + 
			 proveedor_tot + "\",\"NUMERO_DOC_TOT\":\"" + docto_tot +
			 "\",\"MONTO_DISPERSADOS_TOT\":\"" + monto_dis_tot + "\"}]";
	
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 	 	"1"					);
	resultado.put("registros", cadena				);	
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("Imprimir") || informacion.equals("GenerarArchivo") ){

	Registros registros 					= new Registros();
	JSONObject jsonObj 					= new JSONObject();
	String usuario 						= "";
	int numeroTot = 0, proveedorTot = 0, docTot = 0;
	double montoTot = 0, montoDispTot = 0;	
	
	if( tipo_moneda.equals("1") ){
		titulo = "RESUMEN DISPERSIÓN M. N.";
	} else{
		titulo = "RESUMEN DISPERSIÓN USD";
	}
	
	ConsultaDispersion paginador = new ConsultaDispersion();
	paginador.setIcIf(Integer.parseInt(ic_if));
	paginador.setIcEpo(Integer.parseInt(ic_epo));
	paginador.setIcMoneda(Integer.parseInt(tipo_moneda));
	paginador.setFechaEmision(fecha);
	paginador.setNombreIf(nombre_if);
	paginador.setNombreEpo(nombre_epo);
	paginador.setFechaInicio(fecha_ini);
	paginador.setFechaFin(fecha_fin);
	paginador.setUsuario(strNombreUsuario);
	paginador.setTitulo(titulo);
	paginador.setNumeroTot(Integer.parseInt(numero_tot));
	paginador.setMontoTot(Double.parseDouble(monto_tot));
	paginador.setProveedorTot(Integer.parseInt(proveedor_tot));
	paginador.setDocTot(Integer.parseInt(docto_tot));
	paginador.setMontoDispTot(Double.parseDouble(monto_dis_tot));
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	try {
		if( informacion.equals("Imprimir") ){
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		} else if( informacion.equals("GenerarArchivo") ){
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
		}
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo", e);
	}	
	infoRegresar = jsonObj.toString();
	
}
	
%>
<%=infoRegresar%>