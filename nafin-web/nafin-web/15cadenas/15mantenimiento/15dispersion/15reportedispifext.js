Ext.onReady(function() {

var ic_if =  Ext.getDom('ic_if').value;

   var parametroGeneral = {
      ic_if          :  Ext.getDom('ic_if').value,
      msgNotFoundIF  :  '<div style="color:#0B4C5F;font-weight:bold;font-size:16px;text-align:center;margin:30px auto">Servicio no disponible para IF</div>'
   }
   
//------------------------- VALIDA PERFIL IF -----------------------------------
   var validaIF = {      
      frmMsgNotFoundIF  : new Ext.form.FormPanel({
         xtype          :  'form',
         id             :  'frmMsgNotFoundIF',
         style          :  'margin:50px auto; border: solid 1px #0B4C5F',
         collapsible    :  false,
         frame          :  true,
         hidden         :  true,
         width          :  600,
         height         :  100,   
         titleCollapse  :  false,
         bodyStyle      :  'padding: 6px',
         defaults       :  {
            msgTarget      :  'side',
            anchor         :  '-20'
         },
         items :  [{
            xtype    :  'label',
            html     :  parametroGeneral.msgNotFoundIF
         }]
      }),
   
      muestraMsgNotFoundIF : function() {
//         if(parametroGeneral.ic_if!='35'){
//            Ext.getCmp('forma').hide();
//            this.frmMsgNotFoundIF.show();
//         }else{
            catalogoNombreEPO.load();
//         }
      }
   }
//------------------------ TERMINA VALIDA PERFIL IF ----------------------------
	var busqAvanzadaSelect=0;
	Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);
	//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
	var procesarCatalogoNombreEPO = function(store, arrRegistros, opts) {
		//Ext.getCmp("idIc_epo").setValue((ic_if));		
	}
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		var grid = Ext.getCmp('grid');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var jsonData = store.reader.jsonData;	
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
	
			var gridTotales = Ext.getCmp('gridTotales');
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				gridTotales.show();	
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnBajarPDF').hide();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);			
		}
	}
	var bajarContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			alert("error");
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15reportedispifext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOMPROVEEDOR'},
			{name: 'NUMPROVEEDOR'},
			{name: 'NUMTRANS'},
			{name: 'FECHAPAGO'},
			{name: 'MONTOTRANS'},
			{name: 'MONEDA'},
			{name: 'FECHFAC'},
			{name: 'MONTOFAC'},
			{name: 'FECHAOPERACION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	var resumenTotalesData = new Ext.data.JsonStore({		
		root : 'registros',
		url : '15reportedispifext.data.jsp',
		baseParams: {
			informacion: 'ConsultaTotales',
			operacion: 'Generar'
		},
		fields: [
			{name: 'MONTOTRANSFERENCIA',  type: 'float',mapping: 'MONTOTRANSFERENCIA'},
			{name: 'TOTALOEPERACIONES',  type: 'float',mapping: 'TOTALOEPERACIONES'}
		],
		totalProperty : 'total', 
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	var catalogoNombreEPO = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15reportedispifext.data.jsp',
		listeners: {
			load: procesarCatalogoNombreEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false
	});
	
	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion','ic_pyme','loadMsg'],
		url : '15reportedispifext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		columns: [{
				header: 'Nombre Proveedor', tooltip: 'Nombre del Proveedor',
				dataIndex: 'NOMPROVEEDOR',
				sortable: true,
				width: 120, resizable: true,
				align: 'center'
			},{
				header: 'Num Proveedor', tooltip: 'Numero del Proveedor',
				dataIndex: 'NUMPROVEEDOR',
				sortable: true,
				width: 90, resizable: true,
				align: 'center'
			},{
				header: 'Num Transferencia', tooltip: 'Numero de Transferencia',
				dataIndex: 'NUMTRANS',
				sortable: true,
				width: 90, resizable: true,
				align: 'center'
			},{
				header: 'Fecha de Pago', tooltip: 'Fecha de Pago',
				dataIndex: 'FECHAPAGO',
				sortable: true,
				width: 90, resizable: true,
				align: 'center',
            renderer: function(value) {
					return value.replace(/-/gi, "/");
            }
			},{
				header: 'Monto total de la transferencia', tooltip: 'Monto total de la transferencia',
				dataIndex: 'MONTOTRANS',
				sortable: true,
				width: 90, resizable: true,
				align: 'center',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.usMoney(record.get('MONTOTRANS'));
            }
			},{
				header: 'Moneda', tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 140, resizable: true,
				align: 'center'
			},{
				header: 'Fecha de la factura', tooltip: 'Fecha de la factura',
				dataIndex: 'FECHFAC',
				sortable: true,
				width: 90, resizable: true,
				align: 'center',
				renderer: function(value){
					return value.replace(/-/gi, "/");
				}
			},{
				header: 'Monto de la factura', tooltip: 'Monto de la factura',
				dataIndex: 'MONTOFAC',
				sortable: true,
				width: 130, resizable: true,
				align: 'center',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.usMoney(record.get('MONTOFAC'));
            }
			},{
				header: 'Fecha de operacion', tooltip: 'Fecha de operacion',
				dataIndex: 'FECHAOPERACION',
				sortable: true,
				width: 90, resizable: true,
				align: 'center',
				renderer: function(value){
					return value.replace(/-/gi, "/");
				}
			}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 766,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		bbar: {
		xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '15reportedispifext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'XLS',
								informacion: 'Consulta'
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '15reportedispifext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'PDF',
								informacion: 'Consulta'
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
	};
	var gridTotales = new Ext.grid.EditorGridPanel({
		id: 'gridTotales',
		hidden: true,
		align: 'center',
		store: resumenTotalesData,
			columns: [
				{
					header: 'No. Operaciones',
					dataIndex: 'TOTALOEPERACIONES',
					width: 150,
					align: 'center'
				},	
				{
					header: 'Monto total transferencias',
					dataIndex: 'MONTOTRANSFERENCIA',
					width: 250,
					align: 'center',
					renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
						 return Ext.util.Format.usMoney(record.get('MONTOTRANSFERENCIA'));
					}			
				}			
			],
			height: 70,		
			width: 410,
			style: 'margin:0 auto',
			frame: false
		});
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'idIc_epo',
			id: 'id_Ic_epo',
			hiddenName : 'idIc_epo',
			fieldLabel: 'Nombre de la EPO',
			emptyText: 'Seleccione EPO',
			store: catalogoNombreEPO,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			allowBlank : false,
			minChars : 1,
			width:300
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Pago',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'txt_fecha_pago',
					id: 'idtxt_fecha_pago',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					formBind: true
				}
			]
		}
	];
	//Forma para hacer la busqueda filtrada
	var fp = new Ext.form.FormPanel({
		xtype: 'form',
		id: 'forma',
		style: ' margin:0 auto;',
		title:'Reporte Dispersion',
		collapsible: true,
		frame:true,
		width: 600,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					var cmpForma = Ext.getCmp('forma');
					var _ic_moneda = Ext.getCmp("id_Ic_epo");
					var _txt_fecha_pago = Ext.getCmp("idtxt_fecha_pago");
				
					if (Ext.isEmpty(_ic_moneda.getValue()) ) {
							_ic_moneda.markInvalid('Por favor, una EPO...');
							return;
					}else if (Ext.isEmpty(_txt_fecha_pago.getValue()) ) {
							_txt_fecha_pago.markInvalid('Por favor, una Fecha de Pago...');
							return;
					}
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():"";
					consultaDataGrid.load({
						params: Ext.apply(paramSubmit,{
							operacion: 'Generar',
							start:0,
							limit:15							
						})
					});
					fp.el.mask('Enviando...', 'x-mask-loading');
					//grid.hide();	
					resumenTotalesData.load({params: Ext.apply(fp.getForm().getValues(),{})})
				
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15reportedispifext.jsp';
				}
			}
		]
	});//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
         validaIF.frmMsgNotFoundIF,
			fp,
			NE.util.getEspaciador(10),
			grid,
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});
   
   validaIF.muestraMsgNotFoundIF();
});
