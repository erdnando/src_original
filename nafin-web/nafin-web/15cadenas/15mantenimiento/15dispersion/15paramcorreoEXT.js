Ext.onReady(function(){

//-------------------------Obtiene mensaje--------------------------------------
var m = Ext.Ajax.request({ 		
							url:'15paramcorreoEXT.data.jsp',
							params :{
								informacion:'obtener msj'
							},
							scope:this,
							success : function(response) { 							
			var infoR 	= Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.msj;
							var men = Ext.getCmp ('textarea');
							men.setValue(archivo);
							}
						});
//-------------------------Fin Obtiene mensaje----------------------------------


//------------------------------Ventana------------------------------------------
var ventana = new Ext.Window({
	
	resizable	:true,
	id 			:'ventana',
	layout		:'anchor',
	title			:"Vista Previa",
	autoScroll : true,
	closable		:false,
	width			:700,
	height		:500,
	items			:[
		{
		xtype		: 'displayfield',
		id			: 'displayvista'
		}
	],
	bbar			: {
		xtype		: 'toolbar',
		buttonAlign		:'center',	
				buttons	: ['-',{	
							xtype: 'button',
							text: 'Regresar',
							iconCls: 'icoLimpiar',
							id: 'btnCerrar',
							handler: function(){
									Ext.getCmp('ventana').hide();
									fp.el.unmask();
								} 
							}]
	}
})
//---------------------------------Fin Ventana----------------------------------

//-------------------------------Elementos Forma--------------------------------
	var  elementosForma =  [
	{ 
	xtype: 'compositefield',
  // labelWidth: 120,
   items: [
		{
		xtype			: 'textarea',							
		id				: 'textarea',
		name			: '_mensaje',
		width			: 350,
		height 		: 300
		},{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 1
		},{
		xtype			: 'displayfield',
		value			: '<br><br><b>Claves de Texto</b><br><br>[1] Raz�n Social EPO<br>[2] Lista de Intermediarios con Operaci�n M.N.<br>[3] Total Intermediarios con Operaci�n M.N.<br>[4] Lista de Intermediarios con Operaci�n USD<br>[5] Total Intermediarios con Operaci�n USD<br>[6] Monto Documentos No Operados M.N.<br>[7] Monto Documentos No Operados USD<br>[8] Total a Dispersar M.N.<br>[9] Total a Dispersar USD<br>[10] Fecha de Vencimiento<br>[11] Fecha a Depositar<br>[12] Total No Dispersado M.N.<br>[13] Total No Dispersado USD',						
		width			: 275,
		height 		: 200
		}
		]
	},{ 
	xtype: 'compositefield',
   labelWidth: 120,
   items: [				
		{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 170
		},	{
		xtype			: 'button',
		text			:'Guardar',
		id				:'btnGuardar',
		hidden		: false,
		width 		: 80,
		iconCls		:'icoGuardar',
		formBind		:true,
		handler		:function(boton, evento){
						var ta = Ext.getCmp('textarea');
						if (ta.getValue()==""){
							Ext.MessageBox.show({
								title: 'Guardar',
								msg: 'El mensaje de correo no puede estar vacio!',
								buttons: Ext.MessageBox.OK
							});
						} else {
							fp.el.mask('Guardando mensaje...', 'x-mask-loading');
							Ext.Ajax.request({ 		
							url:'15paramcorreoEXT.data.jsp',
							params :{
								guardarMensaje : true,
								informacion:'guardar',
								mensaje :ta.getValue()
							},
							scope:this,
							success : function(response) { 
								Ext.Msg.alert(boton.getText(),'La operaci�n se realiz� con �xito');
								fp.el.unmask();
							}
						});
						}
			}
		},{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 3
		},{
		xtype			: 'button',
		text			:'Vista Previa',
		id				:'btnVista',
		hidden		: false,
		iconCls		:'icoBuscar',
		formBind		:true,
		handler		:function(boton, evento){
						var ta = Ext.getCmp('textarea');
						if (ta.getValue()==""){
							Ext.MessageBox.show({
							title: 'Vista Previa',
							msg: 'No se puede generar vista previa sin datos!',
							buttons: Ext.MessageBox.OK//icon: Ext.get('icons').dom.value
							});
						} else {
							fp.el.mask('Generando Vista Previa...', 'x-mask-loading');
							Ext.Ajax.request({ 		
							url:'15paramcorreoEXT.data.jsp',
							params :{
								informacion:'vistaPrevia',
								mensaje : ta.getValue()
								},
							scope:this,
							success : function(response, opts) {
							fp.el.unmask();
							Ext.getCmp('displayvista').setValue(response.responseText);
								}
							});
							ventana.show();
						}
			}
		}
		]
	}]
//-----------------------------Fin Elementos Forma------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id					:'formcon',
		width				: 663,
		heigth			:'auto',
		title				:'Parametrizaci�n Correo',
		layout			:'form',
		frame				:true,
		collapsible		:true,
		titleCollapse	:false,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 5px',
		labelWidth		:1,
		monitorValid	:true,	
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosForma
							]
	})
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		:'auto',
		height	:'auto',
		style		:'margin:0 auto;',
		items		:[
					NE.util.getEspaciador(20),
					fp,
					NE.util.getEspaciador(20)
					]
	})
//-----------------------------Fin Contenedor Principal-------------------------

})//-----------------------------------------------Fin Ext.onReady(function(){}