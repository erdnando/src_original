Ext.onReady(function() {
	
	Ext.namespace('NE.monitordispersion');
	
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaMonitorDispersion = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						monitorDispersion(resp.estadoSiguiente,resp);
					}
				);
			} else {
				monitorDispersion(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			/*
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
         
         // Resetear forma
         resetWindowConfirmacionClave();
 
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			*/
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
					
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var monitorDispersion = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"							   ){
			
			// Cargar Catalogo de Tipos de Monitoreo
			var catalogoTipoMonitoreoData = Ext.StoreMgr.key('catalogoTipoMonitoreoDataStore');
			catalogoTipoMonitoreoData.load();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordisp01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.inicializacion'
				},
				callback: 				procesaMonitorDispersion
			});
 	
		} else if(			estado == "MOSTRAR_PANTALLA_MONITOREO"			  ){
									
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15monitordisp01ext.data.jsp',
				params: 	{
					informacion:		'MonitorDispersion.mostrarPantallaMonitoreo',
					tipoMonitoreo:		respuesta.tipoMonitoreo
				},
				callback: 				procesaMonitorDispersion
			});
 	
		} else if(			estado == "ESPERAR_DECISION"  					  ){ 
			
			/*
			
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("panelBusquedaDispersiones").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Ocultar mascara del grid de detalle de las dispersiones enlistadas
         element = Ext.getCmp("gridDetalleDispersionesEnlistadas").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceEnvioFFON();
			
         // Resetear forma
         resetWindowConfirmacionClave();
         
         // Ocultar mascara del grid de cifras de control
         element = Ext.getCmp("gridCifrasControl").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			*/
			
		} else if(			estado == "FIN"  					                 ){
 	
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"15monitordisp01ext.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			forma.submit();
						
		}
		
	}
	
	
	//--------------------------------- 1. PANEL DE SELECCION DE MONITOR ---------------------------------------

	var catalogoTipoMonitoreoData = new Ext.data.JsonStore({
		id: 					'catalogoTipoMonitoreoDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15monitordisp01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoTipoMonitoreo'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("comboTipoMonitoreo").setValue(defaultValue);
					}
									
				} 
								
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	var elementosPanelSeleccionMonitoreo = [
		// COMBO TIPO DE MONITOREO
		{
			xtype: 				'combo',
			name: 				'tipoMonitoreo',
			id: 					'comboTipoMonitoreo',
			fieldLabel: 		'Tipo de Monitoreo',
			mode: 				'local',
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveTipoMonitoreo',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTipoMonitoreoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%',
			listeners:			{
				select:	function( combo, record, index ){
					
					var respuesta = new Object();
					respuesta['tipoMonitoreo'] = combo.getValue();
					monitorDispersion("MOSTRAR_PANTALLA_MONITOREO",respuesta);
					
				}
			}
		}
	];
	
	var panelSeleccionMonitoreo = new Ext.form.FormPanel({
		id: 				'panelSeleccionMonitoreo',
		width: 			400,
		title: 			'Selecci�n de Monitor',
		frame: 			true,
		collapsible: 	false,
		titleCollapse: false,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		defaultType: 	'textfield',
		items: 			elementosPanelSeleccionMonitoreo,
		monitorValid: 	false
	});
	
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelSeleccionMonitoreo
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	monitorDispersion("INICIALIZACION",null);
	
});