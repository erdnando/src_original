<%@ page 
	contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoIFMonitorDispersion,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		netropology.utilerias.ElementoCatalogo,
		java.sql.PreparedStatement,
		java.sql.ResultSet,
		java.math.BigDecimal"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("MonitorDispersion.inicializacion") )	{

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Determinar el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CatalogoTipoMonitoreo")            )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Crear Catalogo	
	JSONArray  registros = new JSONArray();
	JSONObject registro	= null;
	
	registro = new JSONObject();
	registro.put("clave",			"E");
	registro.put("descripcion",	"EPO's");
	registros.add( JSONObject.fromObject(registro) );

	registro = new JSONObject();
	registro.put("clave",			"F");
	registro.put("descripcion",	"IF's");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"FF");
	registro.put("descripcion",	"IF's FIDEICOMISO");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"I");
	registro.put("descripcion",	"INFONAVIT");
	registros.add( JSONObject.fromObject(registro) );
	
	registro = new JSONObject();
	registro.put("clave",			"P");
	registro.put("descripcion",	"PEMEX-REF");
	registros.add( JSONObject.fromObject(registro) );
	
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)				);
	resultado.put("total",    	new Integer(registros.size()) );
	resultado.put("registros",	registros			 				);
	
	infoRegresar = resultado.toString();
 
} else if (        informacion.equals("MonitorDispersion.mostrarPantallaMonitoreo") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	String tipoMonitoreo = (request.getParameter("tipoMonitoreo")		== null)?"":request.getParameter("tipoMonitoreo");
	
	// Determinar el estado siguiente
	if(        "E".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispe01ext.jsp");
	} else if( "F".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispi01ext.jsp");
	} else if( "I".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispinfo01ext.jsp");
	} else if( "P".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispPEMEX01ext.jsp");
	} else if( "FF".equals(tipoMonitoreo) ){
		estadoSiguiente = "FIN";
		resultado.put("destino","15monitordispf01ext.jsp");
	} else {
		estadoSiguiente = "ESPERAR_DECISION";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
		
} else if(  informacion.equals("CatalogoEPO")            )	{
 
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		throw new AppException(msg);
 
	}
	
	// 2. Consultar catalogo de registros
	List catalogo = dispersion.getDetalleCatalogoEposMonitoreoPEMEXREF();
	
	JSONObject consulta  = new JSONObject();
	JSONArray  registros = new JSONArray();
	for(int i=0;i<catalogo.size();i++){
		HashMap registro = (HashMap) catalogo.get(i);
		registros.add( JSONObject.fromObject(registro) );
	}
	
	consulta.put("success",  new Boolean(true)				);
	consulta.put("total", 	 new Integer(registros.size()));
	consulta.put("registros",registros							);
	
	infoRegresar = consulta.toString();
	
} else if( informacion.equals("ConsultaDetalleMonitoreoPEMEXREF") ){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Leer parametros
	String cmb_tipo_monitor	= (request.getParameter("claveTipoMonitoreo")	== null)?"":request.getParameter("claveTipoMonitoreo");
	String ic_epo 				= (request.getParameter("claveEPO")					== null)?"":request.getParameter("claveEPO");
	String txt_fecha_ven_de	= (request.getParameter("fechaVencimientoDe")	== null)?"":request.getParameter("fechaVencimientoDe");
	String txt_fecha_ven_a 	= (request.getParameter("fechaVencimientoA")		== null)?"":request.getParameter("fechaVencimientoA");
 
	JSONArray 	registros 			= new JSONArray();
	JSONObject	registro				= null;
	JSONObject	summaryData			= new JSONObject();
		
	// 2. Obtener instancia de EJB de Dispersion
	Dispersion 	dispersion 			= null;
	try {
						
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
						
	} catch(Exception e) {
		 
		msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
				
		throw new AppException(msg);
		 
	}
	
	// 3. Variables Auxiliares
	AccesoDB				con					= new AccesoDB();
	String 				qrySentencia 		= null;
	PreparedStatement ps				 		= null;
	ResultSet			rs				 		= null;
	int 					i				 		= 0;
	
	String 				tipoColor 			= "formas";
	
	try {
		
		// Conectarse a la Base de Datos
		con.conexionDB();
 
		// Obtener el query de la consulta
		if("P".equals(cmb_tipo_monitor)) {
				
			qrySentencia = dispersion.getQueryDetalleMonitoreoPemexRef(
				ic_epo
			);
				
		} //if("E".equals(cmb_tipo_monitor))
		log.debug("ConsultaDetalleMonitoreoPEMEXREF.qrySentencia = <" + qrySentencia+ ">");

		// Realizar consulta
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1, 4);
		ps.setInt(2, 4);
		ps.setInt(3, 5);
		ps.setString(4, "E");
		ps.setInt(5, 1);
		ps.setInt(6, 1);
		ps.setInt(7, 4);
		ps.setInt(8, 40);
		ps.setInt(9, 2);
		ps.setString(10, txt_fecha_ven_de);
		ps.setString(11, txt_fecha_ven_a);
		ps.setInt(12, 1);
		if(!"".equals(ic_epo)) 
			ps.setInt(13, Integer.parseInt(ic_epo));
		rs = ps.executeQuery();

		String 		groupId 				= "";
		String 		groupIdAnterior 	= "";
		
		int 			totalDocumentos 	= 0;
		BigDecimal 	totalMonto 			= new BigDecimal(0);
			
		boolean 		existetabla 		= false;
		while(rs.next()) {
				
			// INICIO CONSULTA DE REGISTROS
			String rs_ic_epo		= rs.getString(13)== null?"":rs.getString(13);
			String rs_fechaVenc	= rs.getString(6)	== null?"":rs.getString(6);
			groupId					= rs_ic_epo+":"+rs_fechaVenc;
				
			// Se detectó un nuevo grupo EPO, FECHA_VENCIMIENTO, por lo que se calcularán los totales del grupo anterior.
			if(!groupIdAnterior.equals(groupId)) {
					
				i = 0;
				if(existetabla){
						
					// AGREGAR LOS TOTALES AL SUMMARYDATA
					JSONArray  summaryDataRegisters = new JSONArray();
					JSONObject summaryDataRegister  = null;
					
					summaryDataRegister  = new JSONObject();
					summaryDataRegister.put( "NUMERO_NAFIN_ELECTRONICO", 	"Totales"											);
					summaryDataRegister.put( "NUMERO_TOTAL_DOCUMENTOS",	String.valueOf(totalDocumentos)				);
					summaryDataRegister.put( "MONTO_TOTAL", 					Comunes.formatoMN(totalMonto.toPlainString())	);
					summaryDataRegister.put( "ESTATUS_CECOBAN", 				"99"													); // PARA QUE LOS TOTALES NO SE PINTEN EN ROJO
					summaryDataRegisters.add( summaryDataRegister );
					
					summaryData.put(groupIdAnterior,summaryDataRegisters);
 
					totalDocumentos 	= 0;
					totalMonto 			= new BigDecimal(0);
						
				}
					
			}
				
			// Es el primer registro del grupo EPO, Fecha Vencimiento
			if( i == 0 ) {
				existetabla = true;
			}
				
			String rs_epo			=  rs.getString(1)	== null?"":rs.getString(1);
			String rs_nafElect	=  rs.getString(2)	== null?"":rs.getString(2);
			String rs_pyme			=  rs.getString(3)	== null?"":rs.getString(3);
			String rs_if			=  rs.getString(4)	== null?"":rs.getString(4);
			String rs_estatus		=  rs.getString(5)	== null?"":rs.getString(5);
			String rs_totalDocs	=  rs.getString(7)	== null?"":rs.getString(7);
			String rs_montoDocs	=  rs.getString(8)	== null?"":rs.getString(8);
			String rs_banco		=  rs.getString(9)	== null?"":rs.getString(9);
			String rs_cuenta		=  rs.getString(10)	== null?"":rs.getString(10);
			String rs_cecoban		=  rs.getString(11)	== null?"":rs.getString(11);
			String rs_descCec		=  rs.getString(12)	== null?"":rs.getString(12);
			totalDocumentos 		+= Integer.parseInt(rs_totalDocs);
			totalMonto 		 		=  totalMonto.add(new BigDecimal(rs_montoDocs));
				
			if(!"99".equals(rs_cecoban))
				tipoColor = "cuenta";
			else 
				tipoColor = "formas";
			if("0".equals(rs_cuenta)){
				rs_banco	= "";
				rs_cuenta	= "";
				rs_cecoban	= "";
				rs_descCec	= "";
			}
				
			// AGREGAR REGISTROS ASOCIADOS AL GRUPO EPO, Fecha de Vencimiento
			registro = new JSONObject();
			registro.put("GROUP_ID",						groupId									); // GROUP_ID
			registro.put("IC_EPO",							rs_ic_epo								); // clave_EPO, IC_EPO
			registro.put("NOMBRE_EPO",						rs_epo									); // EPO, NOMBRE_EPO
			registro.put("NUMERO_NAFIN_ELECTRONICO",	rs_nafElect								); // Num. Nafin Electrónico 
			registro.put("NOMBRE_IF",						rs_if										); // IF
			registro.put("NOMBRE_PYME",					rs_pyme									); // PYME
			registro.put("ESTATUS",							rs_estatus								); // Estatus
			registro.put("FECHA_VENCIMIENTO",			rs_fechaVenc							); // Fecha de Vencimiento
			registro.put("NUMERO_TOTAL_DOCUMENTOS",	rs_totalDocs							); // Num. Total de Doctos.
			registro.put("MONTO_TOTAL",					Comunes.formatoMN(rs_montoDocs)	); // Monto Total
			registro.put("CODIGO_BANCO_CUENTA_CLABE",	rs_banco									); // Código Banco Cuenta Clabe
			registro.put("CUENTA_CLABE",					rs_cuenta								); // Cuenta CLABE
			registro.put("DESCRIPCION_CECOBAN",			rs_descCec								); // Descripción CECOBAN
			registro.put("ESTATUS_CECOBAN",				rs_cecoban								); // ESTATUS_CECOBAN
			registros.add(registro); // 
 
			i++;
	
			// El GroupID actual se convierte en el anterior
			groupIdAnterior = groupId;
			
		}//while(rs.next())
		
		// AGREGAR TOTALES DEL ÚLTIMO GRUPO
		if( i > 0 ) {
			
			JSONArray  summaryDataRegisters = new JSONArray();
			JSONObject summaryDataRegister  = null;
					
			summaryDataRegister = new JSONObject();
			summaryDataRegister.put( "NUMERO_NAFIN_ELECTRONICO", 	"Totales"												);
			summaryDataRegister.put( "NUMERO_TOTAL_DOCUMENTOS",	String.valueOf(totalDocumentos)					);
			summaryDataRegister.put( "MONTO_TOTAL", 					Comunes.formatoMN(totalMonto.toPlainString())		);
			summaryDataRegister.put( "ESTATUS_CECOBAN", 				"99"														); // PARA QUE LOS TOTALES NO SE PINTEN EN ROJO
			summaryDataRegisters.add( summaryDataRegister );
					
			summaryData.put(groupIdAnterior,summaryDataRegisters);
 
		}

		// PONER EL REPORTE EN SESIÓN
		if( i > 0 ){
			session.setAttribute( 	 "lReportes.registros", 	registros	);
			session.setAttribute( 	 "lReportes.summaryData",	summaryData );
		} else {
			session.removeAttribute( "lReportes.registros"	 );
			session.removeAttribute( "lReportes.summaryData" );
		}
	
	} catch(Exception e) {
				
		log.error("ConsultaDetalleMonitoreoPEMEXREF(Exception)");
		log.error("ConsultaDetalleMonitoreoPEMEXREF.claveTipoMonitoreo 	= <" + cmb_tipo_monitor	+ ">");
		log.error("ConsultaDetalleMonitoreoPEMEXREF.claveEPO 					= <" + ic_epo				+ ">");
		log.error("ConsultaDetalleMonitoreoPEMEXREF.fechaVencimientoDe 	= <" + txt_fecha_ven_de	+ ">");
		log.error("ConsultaDetalleMonitoreoPEMEXREF.fechaVencimientoA 		= <" + txt_fecha_ven_a	+ ">");
		log.error("ConsultaDetalleMonitoreoPEMEXREF.qrySentencia 			= <" + qrySentencia		+ ">");
		e.printStackTrace();
				
		throw e;
				
	} finally {
				
		if( rs != null ){ try { rs.close(); } catch(Exception e){} }
		if( ps != null ){ try { ps.close(); } catch(Exception e){} }
				
		if( con.hayConexionAbierta() ){
			con.cierraConexionDB();
		}
				
	}
 
	resultado.put("success",		new Boolean(true)					);
	resultado.put("total",			new Integer(registros.size())	);
	resultado.put("registros",		registros							);
	resultado.put("summaryData",	summaryData							);
			
	infoRegresar = resultado.toString();
		
} else if( informacion.equals("GenerarArchivo") ){
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	String      urlArchivo		= "";
	
	String 		nombreArchivo	= "";
	
	// Leer parametros
	String 		tipo 				= (request.getParameter("tipo") == null)?"":request.getParameter("tipo");
	
	// Preparar clases adicionales
	ConsultaDetalleMonitoreoPEMEXREF consulta = new ConsultaDetalleMonitoreoPEMEXREF();
	
	// Leer datos
	JSONArray 		registros 	= (JSONArray)  session.getAttribute( "lReportes.registros"   );
	JSONObject 		summaryData = (JSONObject) session.getAttribute( "lReportes.summaryData" );
	// HttpSession session
	String			directorioTemporal		= strDirectorioTemp; 
	String 			directorioPublicacion 	= strDirectorioPublicacion;
	
	// Crear archivo
	if(        "PDF".equals(tipo) ){
		nombreArchivo = consulta.generaArchivoPDF( directorioTemporal, directorioPublicacion, registros, summaryData, session );
		urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
	} else if( "CSV".equals(tipo) ){
		nombreArchivo = consulta.generaArchivoCSV( directorioTemporal, registros, summaryData );
		urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
	}
 
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();

} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>