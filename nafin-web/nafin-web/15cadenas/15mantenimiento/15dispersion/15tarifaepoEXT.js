Ext.onReady(function(){

  var operada;
  var operadaPagada;
  var pagadaSinOper;
  var vencidoSinOper;
  var claveEpoEnFFON;
  var monedaNacional;
  var sizeTarifaNac;
  var dolaresAmerica;
  var sizeTarifaDol;
  var tipoArchivo;
  var nombreProvedor;
  var claveProvedor;
  var sizeCN;
  var sizeCE;
  var mN;
  var dA;
  var valorComboPyme="";
  var cambioTextfield=0;
  var cargado=0;
  var btN=0;
  var btD=0;
  var fila1=false;
  var fila2=false;
  var fila3=false;
  var fila4=false;
  var fila5=false;
  var fila6=false;
  var total,ic_producto,nombre_producto,chk_dispersion,chk_doctos ;
  
  	var procesarSuccessFailureActualizarDispersion = function(opts, success, response)    {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('',Ext.util.JSON.decode(response.responseText).msg);	
			Ext.getCmp('winModProd').hide();
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(), {
					informacion: 'ConsultaProducto'
				})						
			});
		}else {
			Ext.Msg.alert('','Ocurrio un error durante la actualizacion de datos.');	
		}
	}
	
	
	var procesarActualizaDispersion  = function(grid, rowIndex, colIndex, item, event) {
		var record = grid.getStore().getAt(rowIndex);
		
		ic_producto 		= '';
		nombre_producto   = '';
		chk_dispersion		= '';
		chk_doctos			= '';
		if(record.get('ic_producto')!=''){
			ic_producto 		= record.get('ic_producto');
			nombre_producto   = record.get('nombre_producto');
			chk_dispersion		= record.get('dispersion');
			chk_doctos			= record.get('dispNoNeg');
		}
		
		var ventana = Ext.getCmp('winModProd');
		if (ventana) {
			fpModProducto.getForm().reset();
			ventana.show(); 
			Ext.getCmp('var_prod').setValue(nombre_producto);
			//checks
			var check = chk_dispersion;
			if(check != null && check == 'Si'){	
				Ext.getCmp('id_dispersion').setValue(true); 
			}else {
				Ext.getCmp('id_dispersion').setValue(false); 
			}
			check = chk_doctos;
			if(check != null && check == 'Si'){	
				Ext.getCmp('id_nonegociables').setValue(true); 
			}else {
				Ext.getCmp('id_nonegociables').setValue(false); 
			}
			//checks
		} 	else  	{
			new Ext.Window({
					title			: 'Modificar',
					id				: 'winModProd',
					layout		: 'fit',
					heigth		:	400,										
					width			:	450,
					minWidth		:	400,
					minHeight	: 	200,					
					modal			: true,
					closeAction	: 'hide',
					items			: [fpModProducto]
				}).show();   
				
				Ext.getCmp('var_prod').setValue(nombre_producto);
				//checks
			var check = chk_dispersion;
			if(check != null && check == 'Si'){	
				Ext.getCmp('id_dispersion').setValue(true); 
			}else {
				Ext.getCmp('id_dispersion').setValue(false); 
			}
			check = chk_doctos;
			if(check != null && check == 'Si'){	
				Ext.getCmp('id_nonegociables').setValue(true); 
			}else {
				Ext.getCmp('id_nonegociables').setValue(false); 
			}
			//checks
		}
	}
	
	var elementosModProducto = [
		{
			xtype			: 'panel',
			layout		: 'table',	
			width			: 450,
			autoHeight	: true,//height		: 300,
			layoutConfig:{ columns: 3 },
			defaults		: {
				frame:false, 
				align:'center',
				border: true,
				width:100, 
				bodyStyle:'padding:2px,'
			},
			items:[
			
				{	width:200,frame:true,	border:false,	html:'<div align="center">Producto<br>&nbsp;</div>'	},
				{	width:100,frame:true,	border:false,	html:'<div align="center">Dispersi�n<br>&nbsp;</div>'	},
				{	width:150,frame:true,	border:false,	html:'<div align="center">Dispersi�n Doctos. No <br> Negociables</div>'	},
				{	xtype: 'displayfield',  border: true,  id : 'var_prod', width:200},
				{	border:true, xtype: 'checkbox', id: 'id_dispersion',   name: 'dispersion', style: {  width: '115%',margin:10,marginBottom: '10px'} },
				{	border:true, xtype: 'checkbox', id: 'id_nonegociables',name: 'no_negocia', style: {  width: '115%',margin:10,marginBottom: '10px'} }
			]//items
		},
		{
			xtype: 		'panel',
			defaults: {	msgTarget: 'side',	anchor: '-20'	},
			bodyStyle: 	'padding: 18px; padding-right:2px;padding-left:66px;',
			layout: 'hbox',
			items: [
				{	xtype: 'displayfield',	width: 170	},
				{	xtype: 'button',			text: 'Aceptar',	id: 'btnAceptar',	width:90,
					handler: function(boton, evento) 
						{
							var disp = Ext.getCmp('id_dispersion');
							if (disp.getValue() == true) { 
								disp = 'S';
							}else {
								disp = 'N';
							}
							var disp_no_neg = Ext.getCmp('id_nonegociables');
							if (disp_no_neg.getValue() == true) { 
								disp_no_neg = 'S';
							}else {
								disp_no_neg = 'N';
							}
							
							
							Ext.Ajax.request({
									url: '15tarifaepoEXT.data.jsp',
									params: {
											informacion: 'actualizaDispersion',
											ic_epo : Ext.getCmp('cmbepo').getValue(),
											ic_producto : ic_producto,
											nombre_producto : nombre_producto,
											chk_dispersion : disp,
											chk_dispersion_no_neg :  disp_no_neg
									},
									callback: procesarSuccessFailureActualizarDispersion
							});
						}
				},	
				{	xtype: 'displayfield',	width: 10	},
				{	xtype: 'button',			text: 'Cancelar',	width:90,
					handler: function(){					
						Ext.getCmp('winModProd').hide();
					}				
				}
			]
		}			  		
	];
	var fpModProducto = new Ext.form.FormPanel({
		id				: 'fModProducto',
      frame			: false,
		width			: 450,
		autoHeight	: true,
		layout		: 'form',
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		items			: elementosModProducto,
		monitorValid: true		
	});
	
	
    var consultaData = new Ext.data.JsonStore ({		
		root:'registros',	
		url:'15tarifaepoEXT.data.jsp',	
		totalProperty: 'total',		
		fields: [	
			{name: 'nombre_producto'}, 
			{name: 'ic_producto'}, 					
			{name: 'dispersion'},
			{name: 'dispNoNeg'},
			{name: 'producto'}
		],
		messageProperty: 'msg',
		autoLoad: false,
		listeners: { exception: NE.util.mostrarDataProxyError	}		
	});
	
  	var gridConsul = new Ext.grid.GridPanel ({
		title      : '',
		id 		  : 'gridConsul',
		store 	  : consultaData,
		style 	  : 'margin:0 auto;',
		stripeRows : true,
		loadMask	  : false,
		hidden	  : false,		
		width		  : 455,
		height	  : 150,	
		frame		  : false, 
		header	  : true,
		columns    : [
			{
				header		: 'Producto',
				tooltip		: 'Producto',			
				dataIndex	: 'nombre_producto',	
				sortable		:true,	
				resizable	:true,	
				width			:192,
				renderer	: function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('nombre_producto') != ''){
						nombre_producto = record.get('nombre_producto');
						return value;
					}
				}
			},		
			{
				header		: 'Dispersi�n',
				tooltip		: 'Dispersi�n',			
				dataIndex	: 'dispersion',	
				sortable		: true,	
				resizable	: true,	
				width			:80,
				align			: 'center'	
			},
			{
				header		: 'Doctos. No <br> Negociables',
				tooltip		: 'Doctos. No <br> Negociables',
				dataIndex	: 'dispNoNeg',	
				sortable		: true,
				resizable	: true,	
				width			: 80, 
				align			: 'center'				
			},
			{
				dataIndex:	'ic_producto',	
				hidden	: true
			},
			{
				xtype		: 'actioncolumn',
				header 	: 'Modificar', 
				tooltip	: 'Modificar',
				width		:  80,	
				align		: 'center', 
				hidden	: false,
				
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Modificar';
							return 'iconoLupa';
						},
						handler: procesarActualizaDispersion
					}
				]
			}
		]		
	});
	
	
//-----------------------------respuestaGuardar------------------------------
function respuestaGuardar(opts, success, response) {
  if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
      Ext.Msg.show({
          title: 'Guardar',
          msg: 'La informaci�n se almacen� con �xito.',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function (){
               //window.location  = "/nafin/15cadenas/15mantenimiento/15dispersion/15tarifaepoEXT.jsp "; 
              }
          });
      Ext.Ajax.request({
      url: '15tarifaepoEXT.data.jsp',
      params: Ext.apply(fp.getForm().getValues(), {
        informacion: 'dispersion'
        }),
      callback:procesarInformacion 								
      });
     fila1=false;
     fila2=false;
     fila3=false;
     fila4=false;
     fila5=false;
     fila6=false;
     dA=false;
     mN=false;
      
  } else {
    NE.util.mostrarConnError(response,opts);
     fila1=false;
     fila2=false;
     fila3=false;
     fila4=false;
     fila5=false;
     fila6=false;
  }
};
//---------------------------Fin respuestaGuardar----------------------------

//*******************************************************  procesaCarga
var procesaCarga = function() {
  Ext.Ajax.request({
  url: '15tarifaepoEXT.data.jsp',	
  params : {
    informacion: 'provedor',
    provedor   : Ext.getCmp('nombre').getValue(),
    epo        : Ext.getCmp('cmbepo').getValue()
    },
  success : function(response) {
   var info = Ext.util.JSON.decode(response.responseText);
   valorComboPyme = info.clave;
   if (valorComboPyme == ""){   } else { cargado=1; }
  }
});	
};
//*******************************************************  procesaCarga

//*******************************************************  procesarGuardar
var procesarGuardar = function() {
 
  var sN = "";
  var grid = Ext.getCmp('gridNafin');
  var store = grid.getStore();  
  store.each(function(record) {
    sN = sN + record.data['descripcion']+",";
  });
  
  var sEp = "";
  var gridEp = Ext.getCmp('gridEpo');
  var storeEp = gridEp.getStore();  
  storeEp.each(function(record) {
    sEp = sEp + record.data['descripcion']+",";
  });
  
  var dMN = new Array();
  var tMN = new Array();
  if (Ext.getCmp('nacional').getValue()){
    var grid = Ext.getCmp('gridNacional');
    var store = grid.getStore();
    store.each(function(record) {
      dMN.push(record.data['dos']);
      tMN.push(record.data['cuatro']);
    });
  } else {
    var grid = Ext.getCmp('gridNacional');
    var store = grid.getStore();
    if (sizeTarifaNac == 1){
    for(var x=0; x<1; x++){
      var record = grid.getStore().getAt(x);
      var fieldNameDia = grid.getColumnModel().getDataIndex(1); // Get field name
      var dia = record.get(fieldNameDia);
      var fieldNameTar = grid.getColumnModel().getDataIndex(3); // Get field name
      var tar = record.get(fieldNameTar);
      dMN.push(dia);
      tMN.push(tar);
      }
    }
    if (sizeTarifaNac == 2){
    for(var x=0; x<2; x++){
      var record = grid.getStore().getAt(x);
      var fieldNameDia = grid.getColumnModel().getDataIndex(1); // Get field name
      var dia = record.get(fieldNameDia);
      var fieldNameTar = grid.getColumnModel().getDataIndex(3); // Get field name
      var tar = record.get(fieldNameTar);
      dMN.push(dia);
      tMN.push(tar);
      }
    }
    if (sizeTarifaNac == 3){
    for(var x=0; x<3; x++){
      var record = grid.getStore().getAt(x);
      var fieldNameDia = grid.getColumnModel().getDataIndex(1); // Get field name
      var dia = record.get(fieldNameDia);
      var fieldNameTar = grid.getColumnModel().getDataIndex(3); // Get field name
      var tar = record.get(fieldNameTar);
      dMN.push(dia);
      tMN.push(tar);
      }
    }  
  }
  
  
  var dD = new Array();
  var tD = new Array();
  
  if (Ext.getCmp('dolares').getValue()){
    var grid = Ext.getCmp('gridDolares');
    var store = grid.getStore();
    store.each(function(record) {
      dD.push(record.data['dos']);
      tD.push(record.data['cuatro']);
    });
  } else {
    var grid = Ext.getCmp('gridDolares');
    var store = grid.getStore();
    if (sizeTarifaDol == 1){
    for(var x=0; x<1; x++){
      var record = grid.getStore().getAt(x);
      var fieldNameDia = grid.getColumnModel().getDataIndex(1); // Get field name
      var dia = record.get(fieldNameDia);
      var fieldNameTar = grid.getColumnModel().getDataIndex(3); // Get field name
      var tar = record.get(fieldNameTar);
      dD.push(dia);
      tD.push(tar);
      }
    }
    if (sizeTarifaDol == 2){
    for(var x=0; x<2; x++){
      var record = grid.getStore().getAt(x);
      var fieldNameDia = grid.getColumnModel().getDataIndex(1); // Get field name
      var dia = record.get(fieldNameDia);
      var fieldNameTar = grid.getColumnModel().getDataIndex(3); // Get field name
      var tar = record.get(fieldNameTar);
      dD.push(dia);
      tD.push(tar);
      }
    }
    if (sizeTarifaDol == 3){
    for(var x=0; x<3; x++){
      var record = grid.getStore().getAt(x);
      var fieldNameDia = grid.getColumnModel().getDataIndex(1); // Get field name
      var dia = record.get(fieldNameDia);
      var fieldNameTar = grid.getColumnModel().getDataIndex(3); // Get field name
      var tar = record.get(fieldNameTar);
      dD.push(dia);
      tD.push(tar);
      }
    }  
  }
  
  
  if (cargado == 1){
  Ext.Ajax.request({
    url: '15tarifaepoEXT.data.jsp',
    params: {
      informacion: 'guardar',
      claveEpo : Ext.getCmp('cmbepo').getValue(),
      clavePyme : valorComboPyme,
      correoNaf: sN,
      correoEpo: sEp,
      dOperada : Ext.getCmp('operada').getValue(),
      dOperPag : Ext.getCmp('operadapagada').getValue(),
      dPagSO   : Ext.getCmp('pagado').getValue(),
      dVenSO   : Ext.getCmp('vencido').getValue(),
      dMonNac  : Ext.getCmp('nacional').getValue(),
      dDolAme  : Ext.getCmp('dolares').getValue(),
      dTipoArc : Ext.getCmp('cmbarc').getValue(),
      claveFFON: Ext.getCmp('clave').getValue(),
      diasMN   : dMN,
      tarifaMN : tMN,
      diasDol  : dD,
      tarifaDol: tD
    },
    callback: respuestaGuardar
    });
  }
}
//*******************************************************  procesarGuardar

//-----------------------------procesarInformacion------------------------------
	function procesarInformacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			 operada				  =info.registros[0].disperOperada;
			 operadaPagada		=info.registros[0].disperOperadaPagada;
			 pagadaSinOper		=info.registros[0].disperPagadoSinOper; 
			 vencidoSinOper	=info.registros[0].disperVencidoSinOper;
			 claveEpoEnFFON	=info.registros[0].claveEpoEnFFON;
			 monedaNacional	=info.registros[0].monedaNacional; 
			 sizeTarifaNac		=info.registros[0].sizeTarifaNac;
			 dolaresAmerica	=info.registros[0].dolaresAmerica;
       sizeTarifaDol		=info.registros[0].sizeTarifaDol;
			 tipoArchivo		  =info.registros[0].tipoArchivo;
			 nombreProvedor	=info.registros[0].nombreProvedor;
			 claveProvedor		=info.registros[0].claveProvedor;
       sizeCN          =info.registros[0].sizeCN;
       sizeCE          =info.registros[0].sizeCE;
			 valorComboPyme  =info.registros[0].claveProvedor;  
       
        btN=sizeTarifaNac;
        btD=sizeTarifaDol;
  
			if(operada == "true")
				Ext.getCmp('operada').setValue(true);
			else 
				Ext.getCmp('operada').setValue(false);
			
			if(operadaPagada == "true")
				Ext.getCmp('operadapagada').setValue(true);
			else 
				Ext.getCmp('operadapagada').setValue(false);
				
			if(pagadaSinOper == "true")
				Ext.getCmp('pagado').setValue(true);
			else 
				Ext.getCmp('pagado').setValue(false);
			
			if(vencidoSinOper == "true")
				Ext.getCmp('vencido').setValue(true);
			else 
				Ext.getCmp('vencido').setValue(false);	
				
			Ext.getCmp('clave').setValue(claveEpoEnFFON);	
			
    //Store Nacional
 //   if(monedaNacional == "true"){
    if (sizeTarifaNac==0){
        var miData = new Array();
          var registro = [];
          registro.push("a) Hasta");
          registro.push("");
          registro.push("D�as Inclusive ");
          registro.push("");
          miData.push(registro);
          
        var grid = Ext.getCmp('gridNacional');
        var store = grid.getStore();
        store.loadData(miData);
      
      }//if (sizeTarifaNac==0)
      if (sizeTarifaNac>0){
          var miData = new Array();
          var registro = [];
          registro.push("a) Hasta");
          registro.push(info.registros[0].monNac[0].dias);
          registro.push("D�as Inclusive ");
          registro.push(info.registros[0].monNac[0].tarifa);
          miData.push(registro);
          var d1=info.registros[0].monNac[0].dias;
          var df = parseInt(d1); df=df+1;
          var registro = [];
          registro.push("b) De los "+df+ " a los");
          registro.push("");
          registro.push("D�as");
          registro.push("");
          miData.push(registro);
        var grid = Ext.getCmp('gridNacional');
        var store = grid.getStore();
        store.loadData(miData);
    }//if (sizeTarifaNac>0){
    if (sizeTarifaNac==2 || sizeTarifaNac > 2){
      var miData = new Array();
          var registro = [];
          registro.push("a) Hasta");
          registro.push(info.registros[0].monNac[0].dias);
          registro.push("D�as Inclusive ");
          registro.push(info.registros[0].monNac[0].tarifa);
          miData.push(registro);
          var d2  = info.registros[0].monNac[1].dias;
          var t2  = info.registros[0].monNac[1].tarifa;
          var f	  = parseInt(d2); f=f+1;
          var registro = [];
          registro.push("b) De los "+df+ " a los");
          registro.push(d2);
          registro.push("D�as");
          registro.push(t2);
          miData.push(registro);
          /*var registro = [];
          registro.push("c) De los ");
          registro.push(f);
          registro.push("en adelante");
          registro.push("");
          miData.push(registro);*/
        var grid = Ext.getCmp('gridNacional');
        var store = grid.getStore();
        store.loadData(miData);
//*        Ext.getCmp('txtCausa').setReadOnly(true);
//*        Ext.getCmp('txtCausa').setDisabled(true);
    }//fin if (sizeTarifaNac==2)
    if (false){//sizeTarifaNac==3){ 
      var miData = new Array();
          var registro = [];
          registro.push("a) Hasta");
          registro.push(info.registros[0].monNac[0].dias);
          registro.push("D�as Inclusive ");
          registro.push(info.registros[0].monNac[0].tarifa);
          miData.push(registro);
          var d2  = info.registros[0].monNac[1].dias;
          var t2  = info.registros[0].monNac[1].tarifa;
          var t3=info.registros[0].monNac[2].tarifa;
          var f	  = parseInt(d2); f=f+1;
          var registro = [];
          registro.push("b) De los "+df+ " a los");
          registro.push(d2);
          registro.push("D�as");
          registro.push(t2);
          miData.push(registro);
          var registro = [];
          registro.push("c) De los ");
          registro.push(f);
          registro.push("en adelante");
          registro.push(t3);
          miData.push(registro);
        var grid = Ext.getCmp('gridNacional');
        var store = grid.getStore();
        store.loadData(miData);
//*        Ext.getCmp('txtCausa').setReadOnly(true);
//*        Ext.getCmp('txtCausa').setDisabled(true);
    }//fin if (sizeTarifaNac==3)

    if(monedaNacional == "true"){
				Ext.getCmp('nacional').setValue(true);
        Ext.getCmp('gridNacional').show();
        Ext.getCmp('gridLeyenda').show();
        Ext.getCmp('cgn').show();
        Ext.getCmp('cgl').show();
    }else { //si no es true MN
				Ext.getCmp('nacional').setValue(false);
        Ext.getCmp('gridNacional').hide();
        Ext.getCmp('gridLeyenda').hide();
        Ext.getCmp('cgn').hide();
        Ext.getCmp('cgl').hide();
    }
 
       
    //store Dolar
//    if(dolaresAmerica == "true"){
    	if (sizeTarifaDol==0){
        var miData = new Array();
        var registro = [];
        registro.push("a) Hasta");
        registro.push("");
        registro.push("D�as Inclusive ");
        registro.push("");
        miData.push(registro);
          
        var grid = Ext.getCmp('gridDolares');
        var store = grid.getStore();
        store.loadData(miData);
      }//if (sizeTarifaDol==0)
      if (sizeTarifaDol>0){
      var miData = new Array();
          var registro = [];
          registro.push("a) Hasta");
          registro.push(info.registros[0].monDol[0].dias);
          registro.push("D�as Inclusive ");
          registro.push(info.registros[0].monDol[0].tarifa);
          miData.push(registro);
          var d1=info.registros[0].monDol[0].dias;
          var df = parseInt(d1); df=df+1;
          var registro = [];
          registro.push("b) De los "+df+ " a los");
          registro.push("");
          registro.push("D�as");
          registro.push("");
          miData.push(registro);
        var grid = Ext.getCmp('gridDolares');
        var store = grid.getStore();
        store.loadData(miData);
      }//if (sizeTarifaDol>0){
      if (sizeTarifaDol==2 || sizeTarifaDol > 2){
        var miData = new Array();
        var registro = [];
        registro.push("a) Hasta");
        registro.push(info.registros[0].monDol[0].dias);
        registro.push("D�as Inclusive ");
        registro.push(info.registros[0].monDol[0].tarifa);
        miData.push(registro);
        var d2  = info.registros[0].monDol[1].dias;
        var t2  = info.registros[0].monDol[1].tarifa;
        var f	  = parseInt(d2); f=f+1;
        var registro = [];
        registro.push("b) De los "+df+ " a los");
        registro.push(d2);
        registro.push("D�as");
        registro.push(t2);
        miData.push(registro);
        /*var registro = [];
        registro.push("c) De los ");
        registro.push(f);
        registro.push("en adelante");
        registro.push("");
        miData.push(registro);*/
        var grid = Ext.getCmp('gridDolares');
        var store = grid.getStore();
        store.loadData(miData);
//*        Ext.getCmp('txtCausaDol').setReadOnly(true);
//*        Ext.getCmp('txtCausaDol').setDisabled(true);
      }//fin if (sizeTarifaDol==2)
      if (false){//sizeTarifaDol==3){
        var miData = new Array();
        var registro = [];
        registro.push("a) Hasta");
        registro.push(info.registros[0].monDol[0].dias);
        registro.push("D�as Inclusive ");
        registro.push(info.registros[0].monDol[0].tarifa);
        miData.push(registro);
        var d2  = info.registros[0].monDol[1].dias;
        var t2  = info.registros[0].monDol[1].tarifa;
        var t3=info.registros[0].monDol[2].tarifa;
        var f	  = parseInt(d2); f=f+1;
        var registro = [];
        registro.push("b) De los "+df+ " a los");
        registro.push(d2);
        registro.push("D�as");
        registro.push(t2);
        miData.push(registro);
        var registro = [];
        registro.push("c) De los ");
        registro.push(f);
        registro.push("en adelante");
        registro.push(t3);
        miData.push(registro);
        var grid = Ext.getCmp('gridDolares');
        var store = grid.getStore();
        store.loadData(miData);
//*        Ext.getCmp('txtCausaDol').setReadOnly(true);
//*        Ext.getCmp('txtCausaDol').setDisabled(true);
    }//fin if (sizeTarifaDol==3)     

    if(dolaresAmerica == "true"){
				Ext.getCmp('dolares').setValue(true);
        Ext.getCmp('gridDolares').show();
        Ext.getCmp('gridLeyendaDol').show();
        Ext.getCmp('cgd').show();
        Ext.getCmp('cgld').show();
    }else { //si no es true MN
				Ext.getCmp('dolares').setValue(false);
        Ext.getCmp('gridDolares').hide();
        Ext.getCmp('gridLeyendaDol').hide();
        Ext.getCmp('cgd').hide();
        Ext.getCmp('cgld').hide();
    }
    
			if(tipoArchivo == "")
				Ext.getCmp('cmbarc').setValue('S');
			else if(tipoArchivo == "P")
				Ext.getCmp('cmbarc').setValue('P');
				else if(tipoArchivo == "E")
					Ext.getCmp('cmbarc').setValue('E');
					else if(tipoArchivo == "A")
						Ext.getCmp('cmbarc').setValue('A');
		
			Ext.getCmp('nombre').setValue(nombreProvedor);
		  
      //Store Correo Nafin
      var tamCN = parseInt(sizeCN,10);
      if (tamCN > 0){
        var miData = new Array();
        for ( x =0 ; x < tamCN; x++){
          var registro = [];
          registro.push(info.registros[0].CUENTAS_NAFIN[x].descripcion);
          registro.push(info.registros[0].CUENTAS_NAFIN[x].clave);
          miData.push(registro);
          }
        var grid = Ext.getCmp('gridNafin');
        var store = grid.getStore();
        store.loadData(miData);
      } else {
          var grid = Ext.getCmp('gridNafin');
          var store = grid.getStore();
          store.loadData('');
      } // FIN Store Correo Nafin

      //Store Correo Epo
      var tamCE = parseInt(sizeCE,10);
      if (tamCE > 0){
        var miData = new Array();
        for ( x =0 ; x < tamCE; x++){
          var registro = [];
          registro.push(info.registros[0].CUENTAS_EPO[x].descripcion);
          registro.push(info.registros[0].CUENTAS_EPO[x].clave);
          miData.push(registro);
          }       
          var grid = Ext.getCmp('gridEpo');
          var store = grid.getStore();
          store.loadData(miData);
      } else {
          var grid = Ext.getCmp('gridEpo');
          var store = grid.getStore();
          store.loadData('');
      } // FIN Store Correo Epo
    if (info.borrar){
      Ext.Msg.show({
          title: 'Eliminar',
          msg: 'La informaci�n se elimin� con �xito.',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function (){}
          });
    }
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
//---------------------------Fin procesarInformacion2----------------------------

//-------------------------Store tipoArchivo---------------------------------
	var tipoArchivo = new Ext.data.ArrayStore({
		 fields		: ['clave', 'descripcion'],
		 data 		: [
							['S','Seleccionar'],	
							['P','Archivo PDF'],
							['E','Excel'],
							['A','Ambos']
							]
		});
//--------------------------Fin Store tipoArchivo----------------------------

//-------------------------Store correoNafin---------------------------------
	var correoNafin = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'myStore',
     idIndex: 0, 
		 fields		: [
              {name : 'descripcion'},
              {name : 'clave' }
              ]
		});
//--------------------------Fin Store correoNafin----------------------------

//-------------------------Store infoNacional---------------------------------
	var infoNacional = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'myStore',
     idIndex: 0, 
		 fields		: [
              {name : 'uno'},
              {name : 'dos' },
              {name : 'tres'},
              {name : 'cuatro'}
              ]
		});
//--------------------------Fin Store infoNacional----------------------------

//-------------------------Store infoDolares---------------------------------
	var infoDolares = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'myStore',
     idIndex: 0, 
		 fields		: [
              {name : 'uno'},
              {name : 'dos' },
              {name : 'tres'},
              {name : 'cuatro'}
              ]
		});
//--------------------------Fin Store infoDolares----------------------------

//-------------------------Store correoEpo---------------------------------
	var correoEpo = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'myStore',
     idIndex: 0, 
		 fields		: [
              {name : 'descripcion'},
              {name : 'clave' }
              ]
		});
//--------------------------Fin Store correoEpo----------------------------

//-------------------------Store seleccione---------------------------------
	var tipo = new Ext.data.ArrayStore({
		 fields		: ['clave', 'descripcion'],
		 data 		: [
							['S','Seleccione Proveedor']
							]
		});
//--------------------------Fin Store tipoArchivo----------------------------

//---------------------------- procesarCatalogo--------------------------------
	var procesarCatalogo = function(store, arrRegistros, opts) {
		if(arrRegistros.length==0){
			Ext.Msg.show({
			title: 'Busqueda Avanzada',
			msg: 'No existe informaci�n con los criterios determinados.',
			modal: true,
			icon: Ext.Msg.INFO,
			buttons: Ext.Msg.OK,
			fn: function (){
				}
			});
		}
	}	
//----------------------------Fin procesarCatalogo--------------------------------


//------------------------catalogoBusquedaAvanzada----------------------------
var catalogoBusquedaAvanzada = new Ext.data.JsonStore({
		id					: 'catalogoBusquedaAvanzada',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '15tarifaepoEXT.data.jsp',
		baseParams	: {
								informacion: 'busquedaAvanzada'
								},
		totalProperty : 'total',
		autoLoad			: false,
		listeners		: {
			load			: procesarCatalogo,
								exception: NE.util.mostrarDataProxyError,
								beforeload: NE.util.initMensajeCargaCombo
								}
	})
//------------------------FIN catalogoBusquedaAvanzada----------------------------

//------------------------------Catalogo EPO------------------------------------
 var catalogoEpo = new Ext.data.JsonStore({
		id					: 'catalogoEpo',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '15tarifaepoEXT.data.jsp',
		baseParams		: {
								informacion		: 'catalogoepo'  
								},
		totalProperty 	: 'total',
		autoLoad			: false,
		listeners		: {			
								beforeload: NE.util.initMensajeCargaCombo,
								exception: NE.util.mostrarDataProxyError								
								}
	})
//----------------------------Fin Catalogo EPO----------------------------------

//------------------------grupo-------------------------------------------------
var grupo = new Ext.ux.grid.ColumnHeaderGroup({
  rows: [
    [
     // {header: 'Intermediario Financiero', colspan: 1, align: 'center'},
      {header: 'Clasificaci�n D�as de Vigencia', colspan: 3, align: 'center'},
      {header: 'Tarifa en M.N.', colspan: 1, align: 'center'}
     // {header: 'Desplegar numero de <br>documentos en vencimiento', colspan: 2, align: 'center'} 
    ]
  ]
});
//------------------------ fin grupo--------------------------------------------

//------------------------grupoDolar-------------------------------------------------
var grupoDolar = new Ext.ux.grid.ColumnHeaderGroup({
  rows: [
    [
     // {header: 'Intermediario Financiero', colspan: 1, align: 'center'},
      {header: 'Clasificaci�n D�as de Vigencia', colspan: 3, align: 'center'},
      {header: 'Tarifa en USD', colspan: 1, align: 'center'}
     // {header: 'Desplegar numero de <br>documentos en vencimiento', colspan: 2, align: 'center'} 
    ]
  ]
});
//------------------------ fin grupoDolar--------------------------------------------

var gridNacional = new Ext.grid.EditorGridPanel({
		id: 'gridNacional',
		store: infoNacional,
    plugins: grupo,
    clicksToEdit: 1,
		height: 100,
		width: 357,
    hidden:true,
		columns: [
				{
				width : 100 ,
				dataIndex: 'uno',
				sortable: false,
				resizable: false,
        hideable:false,
				align: 'left'
			},{
				width : 50,
				dataIndex: 'dos',
				sortable: false,
				resizable: false,
        hideable:false,
				align: 'center',
        editor: {
          xtype: 'numberfield',
          height: 10,
          id: 'txtCausa',
          decimalPrecision: 0,
          minValue: 0,
          //regex		:/^([1-9])/ ,
         // regex: /[1-9-]*/
          //readOnly: true,
          //disabled: true,
          //allowBlank	: false,
          listeners: {
              blur: function(field) {
                  if (field.getValue() === "") {
                     
                  } else {
                //      Ext.getCmp('txtCausa').setReadOnly(true);
                //      Ext.getCmp('txtCausa').setDisabled(true);
                  }
              },
              specialkey: function(field, e) {
                  if (e.getKey() === e.ENTER) {
                      if (field.getValue() === "") {
                        //  procesarCausaVacia();
                      } else{}
                //  Ext.getCmp('txtCausa').setReadOnly(true);
                //  Ext.getCmp('txtCausa').setDisabled(true);
                  }
              }
          }//fin listener
                },
                renderer: function(value, metadata, registro, rowIndex, colIndex) {
                   // metadata.attr = 'style="border: thin solid #3399CC;  color: black;"';
                    return value;
                }
			},{
				width : 100 ,
				dataIndex: 'tres',
				sortable: false,
				resizable: false,
        hideable:false,
				align: 'left'
			},{
				width : 100 ,
				dataIndex: 'cuatro',
				sortable: false,
				resizable: false,
        hideable:false,
				align: 'right',
        editor: {
          xtype: 'numberfield',
          height: 10,
          width : 30 ,
          id: 'txtCausa2',
          maxLength: 12,
          minValue: 0,
          //readOnly: true,
          //disabled: true,
          //allowBlank	: false,
          listeners: {
              invalid: function(field,msg){
                Ext.Msg.show({
                title: 'Tarifa en M.N.',
                msg: 'El tama�o maximo para este campo es de 12',
                modal: true,
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK,
                fn: function (){}
                });
              },
              blur: function(field) {
                  if (field.getValue() === "") {
                     
                  } else {
                   //   Ext.getCmp('txtCausa2').setReadOnly(true);
                   //   Ext.getCmp('txtCausa2').setDisabled(true);
                  }
              },
              specialkey: function(field, e) {
                  if (e.getKey() === e.ENTER) {
                      if (field.getValue() === "") {
                        //  procesarCausaVacia();
                      } else{}
                 // Ext.getCmp('txtCausa2').setReadOnly(true);
                //  Ext.getCmp('txtCausa2').setDisabled(true);
                  }
              }
          }//fin listener
                },renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
                /*
                renderer: function(value, metadata, registro, rowIndex, colIndex) {
                   // metadata.attr = 'style="border: thin solid #3399CC;  color: black;"';
                   //return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
                   
                   // return "$ "+value;
                }*/
			}
		],
    listeners : {/*
    cellclick: function(grid, rowIndex, columnIndex, e) {
              if (sizeTarifaNac==1 && rowIndex==0 && columnIndex==1){
                  
                  Ext.getCmp('txtCausa').setReadOnly(true);
                  Ext.getCmp('txtCausa').setDisabled(true);
                } else {
                  Ext.getCmp('txtCausa').setReadOnly(false);
                  Ext.getCmp('txtCausa').setDisabled(false);
                } 
                
                if (sizeTarifaNac==1 && rowIndex==1 && columnIndex==1){
                  
                  Ext.getCmp('txtCausa').setReadOnly(false);
                  Ext.getCmp('txtCausa').setDisabled(false);
                }
              if (sizeTarifaNac > 1){ 
                Ext.getCmp('txtCausa').setReadOnly(true);
                Ext.getCmp('txtCausa').setDisabled(true);
              }  
                var record = grid.getStore().getAt(rowIndex);  // Get the Record
                var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                var data = record.get(fieldName);
                
            //    Ext.getCmp('validaNafin').setValue(rowIndex);
            },*/
			beforeedit: function(e){
					if( sizeTarifaNac==0 && e.row==0 && e.column==1 ){
						e.cancel=false;
					} else if( sizeTarifaNac==0 && e.row==0 && e.column==3 ){
						e.cancel=false;
					} else if( sizeTarifaNac==1 && e.row==0 && e.column==1 ){
						e.cancel=true;
					} else if( sizeTarifaNac==1 && e.row==0 && e.column==3 ){
						e.cancel=false;
					} else if( sizeTarifaNac==1 && e.row==1 && e.column==1 ){
						e.cancel=false;
					} else if( sizeTarifaNac==1 && e.row==1 && e.column==3 ){
						e.cancel=false;
					} else if( sizeTarifaNac==2 && e.row==0 && e.column==1 ){
						e.cancel=true;
					} else if( sizeTarifaNac==2 && e.row==0 && e.column==3 ){
						e.cancel=false;
					} else if( sizeTarifaNac==2 && e.row==1 && e.column==1 ){
						e.cancel=true;
					} else if( sizeTarifaNac==2 && e.row==1 && e.column==3 ){
						e.cancel=false;
					} else if( sizeTarifaNac==3 && e.row==0 && e.column==1 ){
						e.cancel=true;
					} else if( sizeTarifaNac==3 && e.row==1 && e.column==1 ){
						e.cancel=true;
					}
					//	grid.stopEditing(true);
					
				}
    }
	});//fin gridNAcional

var gridLeyenda = new Ext.grid.EditorGridPanel({
		id: 'gridLeyenda',
		store: infoNacional,
    hidden:true,
    title: '<div align="center"> * Cero si no aplica costo de comisi�n </div>',
    //plugins: grupo,
    //clicksToEdit: 1,
		height: 27,
		width: 357,
		columns: [
		]
	});//fin gridLeyenda


var gridDolares = new Ext.grid.EditorGridPanel({
		id: 'gridDolares',
		store: infoDolares,
    plugins: grupoDolar,
    clicksToEdit: 1,
		height: 100,
		width: 357,
    hidden:true,
		columns: [
				{
				width : 100 ,
				dataIndex: 'uno',
				sortable: false,
				resizable: false,
        hideable:false,
				align: 'left'
			},{
				width : 50,
				dataIndex: 'dos',
				sortable: false,
				resizable: false,
        hideable:false,
				align: 'center',
        editor: {
          xtype: 'numberfield',
          height: 10,
          id: 'txtCausaDol',
          decimalPrecision: 0,
          minValue: 0,
          listeners: {
              blur: function(field) {
                  if (field.getValue() === "") {
                  } else {
                  }
              },
              specialkey: function(field, e) {
                  if (e.getKey() === e.ENTER) {
                      if (field.getValue() === "") {
                      } else{}
                  }
              }
          }//fin listener
                },
                renderer: function(value, metadata, registro, rowIndex, colIndex) {
                    return value;
                }
			},{
				width : 100 ,
				dataIndex: 'tres',
				sortable: false,
				resizable: false,
        hideable:false,
				align: 'left'
			},{
				width : 100 ,
				dataIndex: 'cuatro',
				sortable: false,
				resizable: false,
        hideable:false,
				align: 'right',
        editor: {
          xtype: 'numberfield',
          height: 10,
          width : 30 ,
          id: 'txtCausaDol2',
          minValue: 0,
          maxLength:12,
          listeners: {
          invalid: function(field,msg){
                Ext.Msg.show({
                title: 'Tarifa en USD',
                msg: 'El tama�o maximo para este campo es de 12',
                modal: true,
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK,
                fn: function (){}
                });
              },
              blur: function(field) {
                  if (field.getValue() === "") {
                  } else {
                  }
              },
              specialkey: function(field, e) {
                  if (e.getKey() === e.ENTER) {
                      if (field.getValue() === "") {
                      } else{}
                  }
              }
          }//fin listener
                },renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			}
		],
    listeners : {/*
    cellclick: function(grid, rowIndex, columnIndex, e) {
              if (sizeTarifaDol==1 && rowIndex==0 && columnIndex==1){
                  
                  Ext.getCmp('txtCausaDol').setReadOnly(true);
                  Ext.getCmp('txtCausaDol').setDisabled(true);
                } else {
                  Ext.getCmp('txtCausaDol').setReadOnly(false);
                  Ext.getCmp('txtCausaDol').setDisabled(false);
                } 
                
                if (sizeTarifaDol==1 && rowIndex==1 && columnIndex==1){
                  
                  Ext.getCmp('txtCausaDol').setReadOnly(false);
                  Ext.getCmp('txtCausaDol').setDisabled(false);
                }
              if (sizeTarifaDol > 1){ 
                Ext.getCmp('txtCausaDol').setReadOnly(true);
                Ext.getCmp('txtCausaDol').setDisabled(true);
              }  
                var record = grid.getStore().getAt(rowIndex);  // Get the Record
                var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                var data = record.get(fieldName);
                
            }*/
		beforeedit: function(e){
					if( sizeTarifaDol==0 && e.row==0 && e.column==1 ){
						e.cancel=false;
					} else if( sizeTarifaDol==0 && e.row==0 && e.column==3 ){
						e.cancel=false;
					} else if( sizeTarifaDol==1 && e.row==0 && e.column==1 ){
						e.cancel=true;
					} else if( sizeTarifaDol==1 && e.row==0 && e.column==3 ){
						e.cancel=false;
					} else if( sizeTarifaDol==1 && e.row==1 && e.column==1 ){
						e.cancel=false;
					} else if( sizeTarifaDol==1 && e.row==1 && e.column==3 ){
						e.cancel=false;
					} else if( sizeTarifaDol==2 && e.row==0 && e.column==1 ){
						e.cancel=true;
					} else if( sizeTarifaDol==2 && e.row==0 && e.column==3 ){
						e.cancel=false;
					} else if( sizeTarifaDol==2 && e.row==1 && e.column==1 ){
						e.cancel=true;
					} else if( sizeTarifaDol==2 && e.row==1 && e.column==3 ){
						e.cancel=false;
					} else if( sizeTarifaDol==3 && e.row==0 && e.column==1 ){
						e.cancel=true;
					} else if( sizeTarifaDol==3 && e.row==1 && e.column==1 ){
						e.cancel=true;
					}
					//	grid.stopEditing(true);
					
				}
    }
	});//fin gridDolares

var gridLeyendaDol = new Ext.grid.EditorGridPanel({
		id: 'gridLeyendaDol',
		store: infoDolares,
    hidden:true,
    title: '<div align="center"> * Cero si no aplica costo de comisi�n </div>',
		height: 27,
		width: 357,
		columns: [
		]
	});//fin gridLeyendaDol

//
var gridNafin = new Ext.grid.GridPanel({
		id: 'gridNafin',
		store: correoNafin,
		height: 100,
		width: 200,
		columns: [
				{
				//header: 'Descripci�n',
				//tooltip: 'Descripci�n',
				width : 180 ,
				dataIndex: 'descripcion',
				sortable: false,
				resizable: false,
        hideable:false,
				align: 'left'
			}
		],
    listeners : {
    cellclick: function(grid, rowIndex, columnIndex, e) {
                var record = grid.getStore().getAt(rowIndex);  // Get the Record
                var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                var data = record.get(fieldName);
                
                Ext.getCmp('validaNafin').setValue(rowIndex);
            }
    }
	});

var gridEpo = new Ext.grid.GridPanel({
		id: 'gridEpo',
		store: correoEpo,
		height: 100,
		width: 200,
		columns: [
				{
				//header: 'Descripci�n',
				//tooltip: 'Descripci�n',
				width : 180 ,
				dataIndex: 'descripcion',
				sortable: false,
				resizable: false,
        hideable:false,
				align: 'left'
			}
		],listeners : {
    cellclick: function(grid, rowIndex, columnIndex, e) {
                var record = grid.getStore().getAt(rowIndex);  // Get the Record
                var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                var data = record.get(fieldName);                
                Ext.getCmp('validaEpo').setValue(rowIndex);
            }
    }
	});
//
//------------------------elementosBusquedaAvanzada-----------------------------
	var elementosBusquedaAvanzada = [		
		{
			xtype   :'form',
			id      :'fpWinBusca',
			frame   : true,
			//labelAlign : 'right',
			border  : false,
			style   : 'margin: 0 auto',
			bodyStyle:'padding:10px',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			labelWidth: 130,
			items:[
				{
					xtype :'displayfield',
					frame :true,
					border: false,
					value :'Utilice el * para b�squeda gen�rica'
				},{
					xtype     :'textfield',
					fieldLabel: 'Nombre',
					name      :			'_txtNombre',
					id        :			'txtNombre',
					hiddenName:		'txt_nombre',
					allowBlank:	true
				},{
					xtype     :		'textfield',
					fieldLabel: 'RFC',
					name      :			'_txtRFC',
					id        :			'txtRFC',
					hiddenName:		'txt_RFC',
					maxLength:	20,
					allowBlank:	true,
          regex		:/^([A-Z|a-z|&amp;]{4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/ ,
          regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
          'en el formato NNNN-AAMMDD-XXX donde:<br>'+
          'NNNN:son las iniciales del nombre de la empresa<br>'+
          'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
          'XXX:es la homoclave'
				},
				{
					xtype:		'textfield',
					fieldLabel: 'N�mero de Proveedor',
					name:			'_txtNumProveedor',
					id:			'txtNumProveedor',
					hiddenName:		'txt_num_proveedor',
					maxLength:	20,
					allowBlank:	true
				}
			],
			buttonAlign:'center',
			buttons:[
				{
        text:'Busca',
		  id: 'botonBuscaAva',
        iconCls:'icoBuscar',
        handler: function(boton) {
            if (Ext.getCmp('txtRFC').isValid()){
            Ext.getCmp("cmbseleccion").hide();
            Ext.getCmp("cmbNumNE").reset();
            Ext.getCmp("cmbNumNE").show();
						var epo = Ext.getCmp("cmbepo").getValue();
						var nombre = Ext.getCmp("txtNombre").getValue();
						var rfc = Ext.getCmp("txtRFC").getValue();
						var proveedor = Ext.getCmp("txtNumProveedor").getValue();
						catalogoBusquedaAvanzada.load({
							params: {
								claveEPO: epo,
								txtNombre: nombre,
								txtRFC: rfc,
								txtNumProveedor: proveedor
							}
						});
            } else {
				}
					}
				},{
					text:'Cancelar',
					iconCls: 'icoCancelar',
					handler: function() {
						Ext.getCmp('fpWinBusca').getForm().reset();
						Ext.getCmp('fpWinBuscaB').getForm().reset();
						Ext.getCmp('winBusqueda').hide();
            Ext.getCmp("cmbseleccion").show();
            Ext.getCmp("cmbNumNE").reset();
            Ext.getCmp("cmbNumNE").hide();
            Ext.getCmp("btnAceptar").hide();
					}
				}
			]
		},{
			xtype:'form',
			id:'fpWinBuscaB',
			frame: true,
			border: false,
			labelAlign : 'left',
			style: 'margin: 0 auto',
			bodyStyle:'padding:10px',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			labelWidth: 130,
			items:[
      {
					xtype   : 'combo',
					id      :	'cmbseleccion',
					name    : 'cmbs',
					hiddenName : '_cmbs',
					fieldLabel: 'Nombre',
          mode     : 'local',
          forceSelection:true,
					triggerAction : 'all',
          typeAhead		: true,
          minChars 		: 1,	
          editable			: false,
          store    : tipo,
          value: 'S',
          valueField: 'clave',
          displayField: 'descripcion'
				},{
					xtype   : 'combo',
					id      :	'cmbNumNE',
					name    : 'ic_pyme',
					hiddenName : 'ic_pyme',
					fieldLabel: 'Nombre',
					emptyText: 'Seleccione Proveedor',
					displayField: 'descripcion',
					valueField: 'clave',
					triggerAction : 'all',
					forceSelection:true,
					//allowBlank: false,
          //msgTarget: 'side',
          //style				:'margin:0 auto;',
					typeAhead: true,
					mode     : 'local',
					minChars : 1,
          hidden :true,
					store    : catalogoBusquedaAvanzada,
					tpl       : NE.util.templateMensajeCargaCombo,
					listeners	: {
					select		: {
						fn			: function(combo) {
								 var valor  = combo.getValue();
								 Ext.getCmp('btnAceptar').show();
							}// FIN fn
						}//FIN select
					}//FIN listeners
				}
			],
			buttonAlign:'center',
			buttons:[
				{
					text:'Aceptar',
					iconCls:'aceptar',
					id: 'btnAceptar',
					hidden: true,
					handler: function() {
              var nombre = Ext.getCmp('nombre');
              var combo  = Ext.getCmp('cmbNumNE').getRawValue();            
              var solonom  = combo.split("   ");  
				  nombre.setValue(solonom[1]);
              valorComboPyme = Ext.getCmp('cmbNumNE').getValue();
              cambioTextfield=1;
            Ext.getCmp('fpWinBusca').getForm().reset();
						Ext.getCmp('fpWinBuscaB').getForm().reset();
						Ext.getCmp('winBusqueda').hide();	
            Ext.getCmp("cmbseleccion").show();
            Ext.getCmp("cmbNumNE").reset();
            Ext.getCmp("cmbNumNE").hide();
            Ext.getCmp("btnAceptar").hide();
            
					}
				},{
					text:'Cancelar',
					iconCls: 'icoCancelar',
					handler: function() {
						Ext.getCmp('fpWinBusca').getForm().reset();
						Ext.getCmp('fpWinBuscaB').getForm().reset();
						Ext.getCmp('winBusqueda').hide();	
            Ext.getCmp("cmbseleccion").show();
            Ext.getCmp("cmbNumNE").reset();
            Ext.getCmp("cmbNumNE").hide();
            Ext.getCmp("btnAceptar").hide();
					}
				}
			]
		}
	];
////



//-------------------------------Elementos Forma--------------------------------
	var  elementosForma =  [
		{
		xtype				: 'combo',
		name				: '_cmb_epo',
		hiddenName		: '_cmb_epo',
		id					: 'cmbepo',	
		fieldLabel		: 'EPO', 
		mode				: 'local',	
		emptyText		: 'Seleccione una EPO',
		forceSelection  : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,
		tpl				: NE.util.templateMensajeCargaCombo,
		store 			: catalogoEpo,
		displayField	: 'descripcion',
		valueField 		: 'clave',
		listeners		: {
			select		: {
				fn			: function(combo) {
								Ext.getCmp('operada').setValue(false);
								Ext.getCmp('operadapagada').setValue(false);
								Ext.getCmp('vencido').setValue(false);
								Ext.getCmp('pagado').setValue(false);
								Ext.getCmp('clave').setValue('');
								Ext.getCmp('nacional').setValue(false);
								Ext.getCmp('dolares').setValue(false);
								Ext.getCmp('nombre').setValue('');
                Ext.getCmp('es1').show();
                Ext.getCmp('es2').show();
                Ext.getCmp('es3').show();
                Ext.getCmp('es4').show();
                Ext.getCmp('es6').show();
                Ext.getCmp('es7').show();
                Ext.getCmp('es8').show();                
                Ext.getCmp('es9').show();                
                Ext.getCmp('es10').show();
                Ext.getCmp('es11').show();
                Ext.getCmp('es12').show();
                Ext.getCmp('es13').show();                
                Ext.getCmp('es14').show();
                Ext.getCmp('es15').show();
                Ext.getCmp('es16').show();
					 Ext.getCmp('es17').show();
                
						Ext.Ajax.request({
							url: '15tarifaepoEXT.data.jsp',
							params: Ext.apply(fp.getForm().getValues(), {
								informacion: 'dispersion'
							}),
							callback:procesarInformacion 								
						});
												
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(), {
								informacion: 'ConsultaProducto'
								
							})						
						});
						
				
					}
				}
			}
		},{
		xtype: 'compositefield',
    id:'es1',
		combineErrors: false,
		msgTarget: 'side',
    hidden : true,
		items: [
				{
				xtype: 'displayfield',
				value:'Estatus a Dispersar',
				width : 150
				},	{
					xtype: 'checkbox',
					boxLabel: 'Operada',
					name: 'operada',
					id: 'operada',
					msgTarget: 'side',
					width : 150
					
				},	{
					xtype: 'checkbox',
					boxLabel: 'Operada Pagada',
					name: 'operadapagada',
					id: 'operadapagada',
					msgTarget: 'side'
				}]			
			},{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
    hidden : true,
    id:'es2',
		items: [
				{
				xtype: 'displayfield',
				value:'',
				width : 150
				},	{
					xtype: 'checkbox',
					boxLabel: 'Vencido sin Operar',
					name: 'vencido',
					id: 'vencido',
					msgTarget: 'side',
					width : 150
					
				},	{
					xtype: 'checkbox',
					boxLabel: 'Pagado sin Operar',
					name: 'pagado',
					id: 'pagado',
					msgTarget: 'side'
				}]			
			},{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
      hidden : true,
      id:'es3',
			items: [
				{
				xtype: 'displayfield',
				value:'Clave EPO en FFON:',
				width : 150
				},
				{
				xtype: 'textfield',
				value	:'',
				id:'clave',
				width : 280
				}
			]
			},{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
    hidden : true,
     id:'es4',
		items: [
				{
				xtype: 'displayfield',
				value:'Tipo de Moneda y Tarifa:',
				width : 150
				},	{
					xtype: 'checkbox',
					boxLabel: 'Moneda Nacional',
					name: 'nacional',
					id: 'nacional',
					msgTarget: 'side',
					width : 150,
          listeners: {
            check: function(){
              var check = Ext.getCmp('nacional').getValue();
              
              if (check){
                  Ext.getCmp('gridNacional').show();
                  Ext.getCmp('gridLeyenda').show();
                  Ext.getCmp('cgn').show();
                  Ext.getCmp('cgl').show();
                
                             
              } else {
                Ext.getCmp('gridNacional').hide();
                Ext.getCmp('gridLeyenda').hide();
                Ext.getCmp('cgn').hide();
                Ext.getCmp('cgl').hide();
                               
              }
            }
          }
				}]	
			},{
		xtype: 'compositefield',
    id: 'cgn',
		combineErrors: false,
		msgTarget: 'side',
    hidden:true,
		items: [{
				xtype: 'displayfield',
				width : 150,
				height: 1			
				},gridNacional]	
		},{
		xtype: 'compositefield',
    id: 'cgl',
		combineErrors: false,
		msgTarget: 'side',
    hidden:true,
		items: [{
				xtype: 'displayfield',
				width : 150,
				height: 1			
				},gridLeyenda]	
		},
		{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
    hidden : true,
    id:'es6',
		items: [
				{
				xtype: 'displayfield',
				value:'',
				width : 150
				},	{
					xtype: 'checkbox',
					boxLabel: 'D�lares Americanos',
					name: 'dolares',
					id: 'dolares',
					msgTarget: 'side',
					width : 150,
          listeners: {
            check: function(){
              var check = Ext.getCmp('dolares').getValue();
              
              if (check){
                Ext.getCmp('gridDolares').show();
                Ext.getCmp('gridLeyendaDol').show();
                Ext.getCmp('cgd').show();
                Ext.getCmp('cgld').show();
                             
              } else {
                Ext.getCmp('gridDolares').hide();
                Ext.getCmp('gridLeyendaDol').hide();
                Ext.getCmp('cgd').hide();
                Ext.getCmp('cgld').hide();
              }
            }
          }
				}]	
			},{
		xtype: 'compositefield',
    id: 'cgd',
		combineErrors: false,
		msgTarget: 'side',
    hidden:true,
		items: [{
				xtype: 'displayfield',
				width : 150,
				height: 1			
				},gridDolares]	
		},		
		{
			xtype: 'compositefield',
			 hidden : true,
			id:'es17',
			items: [
			{
				xtype: 'displayfield',
				width : 150 
			},
				gridConsul
			]
		 },
		{
		xtype: 'compositefield',
    id: 'cgld',
		combineErrors: false,
		msgTarget: 'side',
    hidden:true,
		items: [{
				xtype: 'displayfield',
				width : 150,
				height: 1			
				},gridLeyendaDol]	
		},{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
    hidden : true,
    id: 'es7',
		items: [
		{
				xtype: 'displayfield',
				value:'Tipo de Archivo a Enviar:',
				width : 150
		},{
		xtype				: 'combo',
		name				: '_cmb_arc',
		hiddenName	: '_cmb_arc',
		id					: 'cmbarc',
		mode				: 'local',	
		forceSelection : true,	
		triggerAction  : 'all',	
		editable			 : false,
		minChars 		: 1,
		store 			: tipoArchivo,
		displayField	: 'descripcion',
		valueField 		: 'clave'
				}]
		},{
		xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
      hidden : true,
      id: 'es8',
			items: [
				{
				xtype: 'displayfield',
				value:'Nombre de Proveedor:',
				width : 150
				},{
				xtype: 'textfield',
				id:'nombre',
				width : 280
				}
			]
		},{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
    hidden : true,
    id:'es9',
		items: [
			{
			xtype: 'displayfield',
			value:'',
			width : 150
			},	{
			xtype : 'button',
			text  : 'B�squeda Avanzada',
			name  : '_btnBusquedaAvanzada',
			id    : 'btnBusquedaAvanzada',
			hiddenName: 'btn_busqueda_avanzada',
			iconCls   : 'icoBuscar',
			handler   : function(boton, evento){
                  Ext.getCmp('winBusqueda').show();
                }
			}]
		},{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
    hidden : true,
    id:'es10',
		items: [
			{
			xtype: 'displayfield',
			value:'',
			width : 200
			},{
			xtype: 'button',
			text:'Guardar',
			iconCls:'icoGuardar',
			id: 'btnGuardar',
			handler: function() {
        var statusMonMx = Ext.getCmp('nacional').getValue();
        var statusMonDol= Ext.getCmp('dolares').getValue();
       
       
      if (statusMonMx == true | statusMonDol == true ){
      
        if (statusMonDol == true){
         var storeDolar = gridDolares.getStore();
         var numReg =0;
          storeDolar.each(function(record) {
          numReg++;
          });  
          if (numReg == 1){
          var record0 = gridDolares.getStore().getAt(0);  // Get the Record
          var dias0=record0.data['dos'];
          var tarifa0=record0.data['cuatro'];
             
           if (tarifa0 =="0"){}
           else if(tarifa0 =="" ){
          Ext.Msg.show({ title: 'Dolar Americano', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
            fn: function (){
              gridDolares.startEditing(0,3);
              }
            });
          }
          if(dias0 =="0"){} else if(dias0 ==""){
            Ext.Msg.show({ title: 'Dolar Americano', msg: 'Debe capturar los dias.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridDolares.startEditing(0,1);
                }
            });
          }
          if(tarifa0 !="" && dias0 !=""){fila1=true;} 
          if(tarifa0 =="0" && dias0 =="0"){fila1=true;}
          if(tarifa0 !="" && dias0 =="0"){fila1=true;}
          if(tarifa0 =="0" && dias0 !=""){fila1=true;}
          
          if(fila1) {dA=true;}
          
        } else if (numReg==2){
            var record0 = gridDolares.getStore().getAt(0);  // Get the Record
            var dias0=record0.data['dos'];
            var record1 = gridDolares.getStore().getAt(1);  // Get the Record
            var dias1=record1.data['dos'];
            var d0 = parseInt(dias0); 
            d0=d0+1;
            var d1 = parseInt(dias1);
            var tarifa0=record0.data['cuatro'];
            var tarifa1=record1.data['cuatro'];
            
             
            if(dias1 =="0"){
            Ext.Msg.show({ title: 'Dolar Americano', msg: 'El dia debe ser mayor que el dia anterior.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
            fn: function (){
//*              Ext.getCmp('txtCausaDol').setReadOnly(false);
//*              Ext.getCmp('txtCausaDol').setDisabled(false);
              gridDolares.startEditing(1,1);
                }
            });
            } else if(dias1 ==""){
            Ext.Msg.show({ title: 'Dolar Americano', msg: 'Debe capturar los dias.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
            fn: function (){
//*              Ext.getCmp('txtCausaDol').setReadOnly(false);
//*              Ext.getCmp('txtCausaDol').setDisabled(false);
              gridDolares.startEditing(1,1);
                }
            });
           } else   if(d1 <= d0){
             Ext.Msg.show({ title: 'Dolar Americano', msg: 'El dia debe ser mayor que el dia anterior.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridDolares.startEditing(1,1);
                  }
              });
           } else if(tarifa1 =="0" ){} else if(tarifa1 =="" ){
            Ext.Msg.show({ title: 'Dolar Americano', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridDolares.startEditing(1,3);
                }
              });
            }
          
          if(tarifa0 =="0" ){} else if(tarifa0 =="" ){
            Ext.Msg.show({ title: 'Dolar Americano', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridDolares.startEditing(0,3);
                }
              });
            }  
          if(tarifa0 !="" && dias0 !=""){fila1=true;} 
          if(tarifa0 =="0" && dias0 =="0"){fila1=true;} 
          if(tarifa0 !="" && dias0 =="0"){fila1=true;} 
          if(tarifa0 =="0" && dias0 !=""){fila1=true;} 
          
          if(tarifa1 !="" && dias1 !=""){
            if(d0 < d1){fila2=true;}           
          } 
          if(tarifa1 =="0" && dias1 !=""){
           if(d0 < d1){fila2=true;} 
          } 
          
           if(fila1 && fila2) {dA=true;}

        }  else if (numReg==3){
            var record0 = gridDolares.getStore().getAt(0);  // Get the Record
            var record1 = gridDolares.getStore().getAt(1);  // Get the Record
            var record3 = gridDolares.getStore().getAt(2);  // Get the Record
            var tarifa0=record0.data['cuatro'];
            var tarifa1=record1.data['cuatro'];
            var tarifa2=record3.data['cuatro'];
            
             if(tarifa2 =="0" ){} else if(tarifa2 =="" ){
            Ext.Msg.show({ title: 'Dolar Americano', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridDolares.startEditing(2,3);
                }
              });
            } 
            if(tarifa1 =="0" ){} else if(tarifa1 =="" ){
            Ext.Msg.show({ title: 'Dolar Americano', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridDolares.startEditing(1,3);
                }
              });
            } 
            if(tarifa0 =="0" ){}else if(tarifa0 =="" ){
            Ext.Msg.show({ title: 'Dolar Americano', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridDolares.startEditing(0,3);
                }
              });
            } 
            if(tarifa0 =="0" ){fila1=true;} 
            if(tarifa0 !="" ){fila1=true;} 
            if(tarifa1 =="0" ){fila2=true;} 
            if(tarifa1 !="" ){fila2=true;} 
            if(tarifa2 =="0" ){fila3=true;} 
            if(tarifa2 !="" ){fila3=true;} 
            
             if(fila1 && fila2 && fila3) {dA=true;}
        }
       } else { dA=true;}
       
       if (statusMonMx == true){
       //   nuevos Cambios 
       var storeNacional = gridNacional.getStore();
       var numReg =0;
        storeNacional.each(function(record) {
        numReg++;
        });  
        if (numReg == 1){
        var record0 = gridNacional.getStore().getAt(0);  // Get the Record
        var dias0=record0.data['dos'];
        var tarifa0=record0.data['cuatro'];
           
           if (tarifa0 =="0"){}
           else if(tarifa0 =="" ){
          Ext.Msg.show({ title: 'Moneda Nacional', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
            fn: function (){
              gridNacional.startEditing(0,3);
              }
            });
          }
          if(dias0 =="0"){} else if(dias0 ==""){
            Ext.Msg.show({ title: 'Moneda Nacional', msg: 'Debe capturar los dias.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridNacional.startEditing(0,1);
                }
            });
          }
          if(tarifa0 !="" && dias0 !=""){fila4=true;} 
          if(tarifa0 =="0" && dias0 =="0"){fila4=true;}
          if(tarifa0 !="" && dias0 =="0"){fila4=true;}
          if(tarifa0 =="0" && dias0 !=""){fila4=true;}
          
          if(fila4) {mN=true;}

        } else if (numReg==2){
            var record0 = gridNacional.getStore().getAt(0);  // Get the Record
            var dias0=record0.data['dos'];
            var record1 = gridNacional.getStore().getAt(1);  // Get the Record
            var dias1=record1.data['dos'];
            var d0 = parseInt(dias0); 
            d0=d0+1;
            var d1 = parseInt(dias1);
            var tarifa0=record0.data['cuatro'];
            var tarifa1=record1.data['cuatro'];
            
             
            if(dias1 =="0"){
            Ext.Msg.show({ title: 'Moneda Nacional', msg: 'El dia debe ser mayor que el dia anterior.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
            fn: function (){
//*              Ext.getCmp('txtCausa').setReadOnly(false);
//*              Ext.getCmp('txtCausa').setDisabled(false);
              gridNacional.startEditing(1,1);
                }
            });
            } else if(dias1 ==""){
            Ext.Msg.show({ title: 'Moneda Nacional', msg: 'Debe capturar los dias.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
            fn: function (){
//*              Ext.getCmp('txtCausa').setReadOnly(false);
//*              Ext.getCmp('txtCausa').setDisabled(false);
              gridNacional.startEditing(1,1);
                }
            });
           } else   if(d1 <= d0){
             Ext.Msg.show({ title: 'Moneda Nacional', msg: 'El dia debe ser mayor que el dia anterior.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridNacional.startEditing(1,1);
                  }
              });
           } else if(tarifa1 =="0" ){} else if(tarifa1 =="" ){
            Ext.Msg.show({ title: 'Moneda Nacional', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridNacional.startEditing(1,3);
                }
              });
            }
          
          if(tarifa0 =="0" ){} else if(tarifa0 =="" ){
            Ext.Msg.show({ title: 'Moneda Nacional', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridNacional.startEditing(0,3);
                }
              });
            }  
          if(tarifa0 !="" && dias0 !=""){fila4=true;} 
          if(tarifa0 =="0" && dias0 =="0"){fila4=true;} 
          if(tarifa0 !="" && dias0 =="0"){fila4=true;} 
          if(tarifa0 =="0" && dias0 !=""){fila4=true;} 
          
          if(tarifa1 !="" && dias1 !=""){
            if(d0 < d1){fila5=true;}           
          } 
          if(tarifa1 =="0" && dias1 !=""){
           if(d0 < d1){fila5=true;} 
          } 
          
           if(fila4 && fila5) {mN=true;}
           
            
        }  else if (numReg==3){
            var record0 = gridNacional.getStore().getAt(0);  // Get the Record
            var record1 = gridNacional.getStore().getAt(1);  // Get the Record
            var record3 = gridNacional.getStore().getAt(2);  // Get the Record
            var tarifa0=record0.data['cuatro'];
            var tarifa1=record1.data['cuatro'];
            var tarifa2=record3.data['cuatro'];
            
             if(tarifa2 =="0" ){} else if(tarifa2 =="" ){
            Ext.Msg.show({ title: 'Moneda Nacional', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridNacional.startEditing(2,3);
                }
              });
            } 
            if(tarifa1 =="0" ){} else if(tarifa1 =="" ){
            Ext.Msg.show({ title: 'Moneda Nacional', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridNacional.startEditing(1,3);
                }
              });
            } 
            if(tarifa0 =="0" ){}else if(tarifa0 =="" ){
            Ext.Msg.show({ title: 'Moneda Nacional', msg: 'Debe capturar la tarifa.', modal: true, icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
              fn: function (){
                gridNacional.startEditing(0,3);
                }
              });
            } 
        if(tarifa0 =="0" ){fila4=true;} 
        if(tarifa0 !="" ){fila4=true;} 
        if(tarifa1 =="0" ){fila5=true;} 
        if(tarifa1 !="" ){fila5=true;} 
        if(tarifa2 =="0" ){fila6=true;} 
        if(tarifa2 !="" ){fila6=true;} 
         
        if(fila4 && fila5 && fila6) {mN=true;}    
           
        }
        //   FIN nuevos Cambios 
       } else { mN=true; }
       
      } else { //fin si MN o MD true
          Ext.Msg.show({
          title: 'Guardar',
          msg: 'Es obligatorio seleccionar al menos un Tipo de Moneda',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
      }
        
        if (Ext.getCmp('cmbarc').getValue() == "S"){
               Ext.Msg.show({
                title: 'Tipo de Archivo',
                msg: 'Es obligatorio seleccionar al menos un Tipo de Archivo a Enviar',
                modal: true,
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
              });
            } 
            
        if (Ext.getCmp('nombre').getValue() == ""){
            Ext.Msg.show({
            title: 'Provedor',
            msg: 'Debe seleccionar un Proveedor.',
            modal: true,
            icon: Ext.Msg.WARNING,
            buttons: Ext.Msg.OK,
            fn: function (){
                  var p = Ext.getCmp('nombre');	
                   p.focus();
                  }
            });
        } else {
          Ext.Ajax.request({
                  url: '15tarifaepoEXT.data.jsp',	
                  params : {
                    informacion: 'provedor',
                    provedor   : Ext.getCmp('nombre').getValue(),
                    epo        : Ext.getCmp('cmbepo').getValue()
                    },
                  success : function(response) {
                   var info = Ext.util.JSON.decode(response.responseText);
                   valorComboPyme = info.clave;
                   if (valorComboPyme == ""){
                        Ext.Msg.show({
                        title: 'Provedor',
                        msg: 'El Nombre del Proveedor proporcionado no es valido.',
                        modal: true,
                        icon: Ext.Msg.WARNING,
                        buttons: Ext.Msg.OK,
                        fn: function (){
                              var p = Ext.getCmp('nombre');	
                               p.focus();
                              }
                        });
                   } else { cargado=1; }
                  }
                });	
        }//fin provedor

//obliga estatus        
        if (Ext.getCmp('operada').getValue() || Ext.getCmp('operadapagada').getValue() || Ext.getCmp('pagado').getValue() || Ext.getCmp('vencido').getValue() ){
             
              if(mN && dA && Ext.getCmp('cmbarc').getValue() != "S" && Ext.getCmp('nombre').getValue() != "" ){
                
                 if ( cargado == 1){
                  procesarGuardar();
                 
                 } else { procesaCarga();}
               }
            //****Fin llamada Proceso Guardar
            } else {
              Ext.Msg.show({
              title: 'Guardar',
              msg: 'Es obligatorio seleccionar al menos un estatus a dispersar.',
              modal: true,
              icon: Ext.Msg.WARNING,
              buttons: Ext.Msg.OK
              });
            }
            
				}//fin handlear guardar
			},{
			xtype: 'button',
			text:'Borrar Rango',
			iconCls:'icoCancelar',
			id: 'btnBorrar',
      
			handler: function() {
          var checaTarifaMx  = Ext.getCmp('nacional').getValue();
          var checaTarifaDol = Ext.getCmp('dolares').getValue();
          
          if (checaTarifaMx | checaTarifaDol ){
          
            if (btN > 0 || btD >0){
             Ext.Msg.show({
              title: 'Borrar Rango',
              msg: '�Esta seguro de borrar la clasificaci�n dias de vigencia?',
              modal: true,
              icon: Ext.Msg.QUESTION,
              buttons: Ext.Msg.OKCANCEL,
              fn: function (btn){ 
                  if (btn == "ok" || btn == "OK" || btn == "Si" || btn == "Si" ||  btn == "Ok"  )
                  Ext.Ajax.request({
                    url: '15tarifaepoEXT.data.jsp',
                    params: Ext.apply(fp.getForm().getValues(), {
                      informacion: 'dispersion',
                      operacion: 'borrarRango',
                      dMonNac: Ext.getCmp('nacional').getValue(),
                      dDolAme: Ext.getCmp('dolares').getValue()
                      }),
                    callback:procesarInformacion 								
                  });
                 }
              });
            } else {
               Ext.Msg.show({
              title: 'Borrar Rango',
              msg: 'No hay ninguna tarifa especificada que se pueda borrar',
              modal: true,
              icon: Ext.Msg.INFO,
              buttons: Ext.Msg.OKCANCEL,
              fn: function (){ }
              });
            }
          } else {
                Ext.Msg.show({
                  title: 'Borrar Rango',
                  msg: 'No hay ninguna tarifa especificada que se pueda borrar',
                  modal: true,
                  icon: Ext.Msg.INFO,
                  buttons: Ext.Msg.OK
                }); 
          }
     
				}//fin handlar borrar
			},{
			xtype: 'button',
			text:'Limpiar',
			iconCls:'icoLimpiar',
			id: 'btnLimpiar',
			handler: function() {
        window.location  = "/nafin/15cadenas/15mantenimiento/15dispersion/15tarifaepoEXT.jsp";
				}//fin handlear limpiar
			}]
		},{
      xtype: 'displayfield',
			value:'Cuentas de Correo NAFIN',
			width : 200,
      hidden : true,
      id:'es11'
  },{
    xtype: 'compositefield',
		combineErrors: false,
		//msgTarget: 'side',
    hidden : true,
    id:'es12',
		items: [
        {
        xtype     : 'textfield',
        id : 'correoNafin',
        vtype: 'email',
        msgTarget: 'side',
        width : 200
        },{
        xtype     : 'displayfield',
        id : 'disp',
        width : 10
        },{
        xtype: 'button',
        text:'Agregar',
        iconCls:'icoAgregar',
        id: 'agregaNafin',
        width : 100,
        handler: function() {
        
        var correoNafin = Ext.getCmp('correoNafin').getValue();
        
        var ereg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        var testResult = ereg.test(correoNafin);        
      
      if (Ext.getCmp('correoNafin').isValid() & Ext.getCmp('correoNafin').getValue() !="" ){
      var grid = Ext.getCmp('gridNafin');
      var store = grid.getStore();
      var cmp=0; 
      var uno=1;
      var cuenta=0;
      store.each(function(record) {
        cuenta++;
      });
      if (store.getCount()== 0){
        TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"}
        ]);
        var record = new TaskLocation({
            clave : Ext.id(),
            descripcion: correoNafin
        });    
        store.add(record);
        store.commitChanges();
        Ext.getCmp('correoNafin').setValue('');
        uno++;
      } else {
      store.each(function(record) {
        var existe = record.data['descripcion'];
        var nuevo  =  correoNafin;
        if (nuevo != existe){           
            cmp=0;
        }else {
          cmp++;
          uno++;
        }
      }); }
      if (cmp > 0){ 
      Ext.Msg.show({
          title: 'Cuentas de Correo Nafin',
          msg: 'La direccion de correo proporcionada ya existe en la lista,<br>por lo que no sera agregada nuevamente',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoNafin');		
                  correo.focus();
              }
          });
      } else { }//nuevamodificacion
      if (uno == 1 & cmp == 0 ){
       TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"}
        ]);
        var record = new TaskLocation({
            clave : Ext.id(),
            descripcion: correoNafin
        });    
        store.add(record);
        store.commitChanges();
        Ext.getCmp('correoNafin').setValue('');
      } else { 
          if(cuenta==0) {} else { 
          Ext.Msg.show({
          title: 'Cuentas de Correo Nafin',
          msg: 'La direccion de correo proporcionada ya existe en la lista,<br>por lo que no sera agregada nuevamente',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoNafin');		
                  correo.focus();
              }
          }); }
            } //nuevamodificacion
        } else{          
          if (correoNafin == "" ){
              Ext.Msg.show({
              title: 'Cuentas de Correo Nafin',
              msg: 'Debe especificar una cuenta de correo',
              modal: true,
              icon: Ext.Msg.WARNING,
              buttons: Ext.Msg.OK,
              fn: function (){
                  var correo = Ext.getCmp('correoNafin');		
                      correo.focus();
                  }
              });
              }
            if ( ! Ext.getCmp('correoNafin').isValid() ){
              Ext.Msg.show({
              title: 'Cuentas de Correo Nafin',
              msg: 'La cuenta de correo proporcionada <br> no es valida, por lo que no sera agregada.',
              modal: true,
              icon: Ext.Msg.WARNING,
              buttons: Ext.Msg.OK,
              fn: function (){
                  var correo = Ext.getCmp('correoNafin');		
                      correo.focus();
                  }
              });
            }  
           }// FIN ELSE
          }//FIN HANDLER
        },{
        xtype: 'button',
        text:'Quitar',
        iconCls:'icoCancelar',
        id: 'quitaNafin',
        width : 100,
        handler: function() {
   
      var grid = Ext.getCmp('gridNafin');
      var store = grid.getStore();
      var indice = Ext.getCmp('validaNafin').getValue();
      var filas=0;
      store.each(function(record) {
        filas++;
      });
     if(filas>0){ 
       if (indice !=""){
        var rec = grid.getStore().getAt(indice);
        store.remove(rec);
        store.commitChanges();
        Ext.getCmp('validaNafin').setValue("");        
      } else {
          Ext.Msg.show({
          title: 'Cuentas de Correo Nafin',
          msg: 'Debe especificar una cuenta de correo',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
      }
    } else {
      Ext.Msg.show({
          title: 'Cuentas de Correo Nafin',
          msg: 'No hay ninguna direccion de correo valida que se pueda borrar.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
      }//fin else 
      }//fin handler
        },{
        xtype : 'textfield',
        id : 'validaNafin',
        msgTarget: 'side',
        width : 50,
        hidden: true
        }
			]
  },{
    xtype: 'compositefield',
    hidden : true,
    id:'es13',
		items: [{
      xtype: 'displayfield',
			width : 215 
      },gridNafin
      ]
  },{
    xtype: 'displayfield',
    value:'Cuentas de Correo EPO',
    width : 200,
    hidden : true,
    id:'es14'
  },{
    xtype: 'compositefield',
    hidden : true,
    combineErrors: false,
    id: 'es15',
		items: [
        {
        xtype     : 'textfield',
        id : 'correoEpo',
        vtype : 'email',
        msgTarget: 'side',
        width : 200
        },{
        xtype     : 'displayfield',
        id : 'disp',
        width : 10
        },{
        xtype: 'button',
        text:'Agregar',
        iconCls:'icoAgregar',
        id: 'agregaEpp',
        width : 100,
        handler: function() {
        var correoIf = Ext.getCmp('correoEpo').getValue(); 
     if (Ext.getCmp('correoEpo').isValid() & Ext.getCmp('correoEpo').getValue() !="" ){
     var grid = Ext.getCmp('gridEpo');
      var store = grid.getStore();
      var cmp=0; 
      var uno=1;
      var cuenta=0;
      store.each(function(record) {
        cuenta++;
      });
      
      if (store.getCount()== 0){
        TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"}
        ]);
        var record = new TaskLocation({
            clave : Ext.id(),
            descripcion: correoIf
        });    
        store.add(record);
        store.commitChanges();
        Ext.getCmp('correoEpo').setValue('');
        uno++;
      } else {
      store.each(function(record) {
        var existe = record.data['descripcion'];
        var nuevo  =  correoIf;
        if (nuevo != existe){           
            cmp=0;
        }else {
          cmp++;
          uno++;
        }
      }); }
      if (cmp > 0){ 
      Ext.Msg.show({
          title: 'Cuentas de Correo Epo',
          msg: 'La direccion de correo proporcionada ya existe en la lista,<br>por lo que no sera agregada nuevamente',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoEpo');		
                  correo.focus();
              }
          });
      } else {}//nuevamodificacion1
      if (uno == 1 & cmp == 0 ){
       TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"}
        ]);
        var record = new TaskLocation({
            clave : Ext.id(),
            descripcion: correoIf
        });    
        store.add(record);
        store.commitChanges();
        Ext.getCmp('correoEpo').setValue('');
      } else { if(cuenta==0) {} else { 
          Ext.Msg.show({
          title: 'Cuentas de Correo Epo',
          msg: 'La direccion de correo proporcionada ya existe en la lista,<br>por lo que no sera agregada nuevamente',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoEpo');		
                  correo.focus();
              }
          }); }
          }//nuevamodificacion2
     } else {
        if (correoIf == "" ){
          Ext.Msg.show({
          title: 'Cuentas de Correo Epo',
          msg: 'Debe especificar una cuenta de correo',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoEpo');		
                  correo.focus();
              }
          });
          }
        if ( ! Ext.getCmp('correoEpo').isValid() ){
          Ext.Msg.show({
          title: 'Cuentas de Correo Epo',
          msg: 'La cuenta de correo proporcionada <br> no es valida, por lo que no sera agregada.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('correoEpo');		
                  correo.focus();
              }
          });
        }
      }// FIN ELSE
      }//FIN HANDLER
        },{
        xtype: 'button',
        text:'Quitar',
        iconCls:'icoCancelar',
        id: 'quitaEpo',
        width : 100,
        handler: function() {
      var grid = Ext.getCmp('gridEpo');
      var store = grid.getStore();
      var indice = Ext.getCmp('validaEpo').getValue(); 
      var filas=0;
      store.each(function(record) {
        filas++;
      });
     if(filas>0){ 
      if (indice !=""){
        var rec = grid.getStore().getAt(indice);
        store.remove(rec);
        store.commitChanges();
        Ext.getCmp('validaEpo').setValue("");        
      } else {
          Ext.Msg.show({
          title: 'Cuentas de Correo Epo',
          msg: 'Debe especificar una cuenta de correo',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
      } 
    } else {
      Ext.Msg.show({
          title: 'Cuentas de Correo Epo',
          msg: 'No hay ninguna direccion de correo valida que se pueda borrar.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
        }//fin else
      }//fin handler
        },{
        xtype : 'textfield',
        id : 'validaEpo',
        msgTarget: 'side',
        width : 50,
        hidden: true
        }
			]
  },
  {
    xtype: 'compositefield',
    hidden : true,
    id:'es16',
		items: [{
      xtype: 'displayfield',
			width : 215 
      },gridEpo
      ]
    }		 
]
//-----------------------------Fin Elementos Forma------------------------------

//------------------------------ventanaBusqueda---------------------------------
	var ventanaBusqueda = new Ext.Window({
		id    : 'winBusqueda',
    title : 'B�squeda Avanzada',
		width : 500,
		height: 340,
		modal : true,
		closeAction: 'hide',
		items:[
			elementosBusquedaAvanzada
		],
    listeners: {
      hide: function(p){
        Ext.getCmp('fpWinBusca').getForm().reset();
        Ext.getCmp('fpWinBuscaB').getForm().reset();
        Ext.getCmp('winBusqueda').hide();
        Ext.getCmp("cmbseleccion").show();
        Ext.getCmp("cmbNumNE").reset();
        Ext.getCmp("cmbNumNE").hide();
        Ext.getCmp("btnAceptar").hide();
      }
    }
	});
//------------------------------FIN ventanaBusqueda---------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id					:'formcon',
		width				: 750,
		//height			: 100,
		title				:'Consulta',
		//layout			:'form',
		frame				:true,
		collapsible		:true,
		titleCollapse	:false,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 6px',
		labelWidth		:50,
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosForma
							]	
})
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		: 949,
		style		:'margin:0 auto;',
		items		:[
					NE.util.getEspaciador(20),
					fp
					]
})//-----------------------------Fin Contenedor Principal-------------------------

catalogoEpo.load(); 

})//-----------------------------------------------Fin Ext.onReady(function(){}