
Ext.onReady(function() 
{

var ic_flujo_fondos;
var ic_epo;
var ic_pyme;
var ic_estatus_docto;
var df_fecha_venc;
var df_operacion;

//--------------------------------- HANDLERS -------------------------------	    
  var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) { gridGeneral.show(); }
			
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		var btnBajarPDF = Ext.getCmp('btnBajarPDF');
	   var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		var btnBajarPDFCT = Ext.getCmp('btnBajarPDFCT');
	   var btnGenerarPDFCT = Ext.getCmp('btnGenerarPDFCT');
		var btnTotales = Ext.getCmp('btnTotales');
		
		var el = gridGeneral.getGridEl();		
		
		if(store.getTotalCount() > 0) {
		   btnTotales.show();
		   btnGenerarArchivo.show();
		   btnGenerarPDF.show();
			btnGenerarPDFCT.show();
		   btnGenerarArchivo.enable();
		   btnGenerarPDF.enable();
			btnGenerarPDFCT.enable();
		   btnBajarArchivo.hide();
		   btnBajarPDF.hide();
			btnBajarPDFCT.hide();		
			
  			el.unmask();
		} else {
		   btnTotales.hide();
		   btnGenerarArchivo.hide();
		   btnGenerarPDF.hide();
			btnGenerarPDFCT.hide();
		   btnBajarArchivo.hide();
		   btnBajarPDF.hide();
			btnBajarPDFCT.hide();
		  
		  btnGenerarArchivo.disable();
		  btnGenerarPDF.disable();
		  btnGenerarPDFCT.disable();
		  el.mask('No existe alguna operaci�n por el momento Aceptada, En Proceso o Rechazada.', 'x-mask'); 
		  }
	   }
  }
  
  var procesarConsultaDataDetalle = function(store, arrRegistros, opts) {
		var gridDetalle = Ext.getCmp('gridDetalle');
		if (arrRegistros != null) {
			if (!gridDetalle.isVisible()) { gridDetalle.show(); }
			
			var btnBajarArchivoD = Ext.getCmp('btnBajarArchivoD');
		   var btnGenerarArchivoD = Ext.getCmp('btnGenerarArchivoD');
			
			var el = gridDetalle.getGridEl();
			var store = consultaDataDetalle;
			if (store.getTotalCount()>0) {
			  btnGenerarArchivoD.enable();
			  btnBajarArchivoD.hide();
			  el.unmask(); 
		   } else { el.mask('No se encontro ning�n registro', 'x-mask'); }
	   }
  }
  
  var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) 
  {
	 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	 btnGenerarArchivo.setIconClass('');
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		btnBajarArchivo.show();
		btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivo.setHandler(function(boton, evento) 
		{
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	  } 
	 else 
	 {
		btnGenerarArchivo.enable();
		NE.util.mostrarConnError(response,opts);
    }
  }
  
  var procesarSuccessFailureGenerarArchivoD =  function(opts, success, response) 
  {
	 var btnGenerarArchivoD = Ext.getCmp('btnGenerarArchivoD');
	 btnGenerarArchivoD.setIconClass('');
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
		var btnBajarArchivoD = Ext.getCmp('btnBajarArchivoD');
		btnBajarArchivoD.show();
		btnBajarArchivoD.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivoD.setHandler(function(boton, evento) 
		{
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	 } 
	 else 
	 {
		btnGenerarArchivoD.enable();
		NE.util.mostrarConnError(response,opts);
    }
  }
  
  var procesarSuccessFailureGenerarPDF = function (opts, success, response) 
  {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarPDF.setHandler(function (boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}
		else 
		{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
  }
  
  var procesarSuccessFailureGenerarPDFCT = function (opts, success, response) 
  {
		var btnGenerarPDFCT = Ext.getCmp('btnGenerarPDFCT');
		btnGenerarPDFCT.setIconClass('');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			var btnBajarPDFCT = Ext.getCmp('btnBajarPDFCT');
			btnBajarPDFCT.show();
			btnBajarPDFCT.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarPDFCT.setHandler(function (boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}
		else 
		{
			btnGenerarPDFCT.enable();
			NE.util.mostrarConnError(response,opts);
		}
  }
  
  var procesarDetalle = function(grid, rowIndex, colIndex, item, event)
  {  
	  var registro = grid.getStore().getAt(rowIndex);
	  ic_flujo_fondos = registro.get('IC_FLUJO_FONDOS');
	  ic_epo = registro.get('IC_EPO');
	  ic_pyme = registro.get('IC_PYME');
	  ic_estatus_docto = registro.get('IC_ESTATUS_DOCTO');
	  df_fecha_venc = registro.get('DF_FECHA_VENC');
	  df_operacion = registro.get('DF_OPERACION');
	  
	  consultaDataDetalle.load({
				          params:{
					              operacion: 'Generar', //Generar datos para la consulta
					              start: 0,
					              limit: 15,
							
							        ic_flujo_fondos : ic_flujo_fondos,
					              ic_epo : ic_epo,
						           ic_pyme : ic_pyme,
						           ic_estatus_docto : ic_estatus_docto,
						           df_fecha_venc : df_fecha_venc,
						           df_operacion : df_operacion
				                 }
							        });
  new Ext.Window({
					  //layout: 'fit',
					  modal: true,
					  width: 640,
					  height: 325,
					  id: 'ventanaDetalle',
					  //closeAction: 'hide',
					  items: [ 
					          gridDetalle,
		                   gridDetalleTotales
								],
					  title: 'Detalle de Dispersion EPO'
				    }).show().setTitle();
  }
	
//-------------------------------- STORES -----------------------------------
  var consultaData = new Ext.data.JsonStore
  ({
			root: 'registros',
			url: '15consultaEpoffext.data.jsp',
			baseParams: { informacion: 'Consulta' },
			fields: [
			         {name: 'CG_RAZON_SOCIAL'},
						{name: 'CRSI'},
						{name: 'CRSP'},
						{name: 'CG_RFC'},
						{name: 'IC_BANCOS_TEF'},
						{name: 'IC_TIPO_CUENTA'},
						{name: 'CG_CUENTA'},
						{name: 'FN_IMPORTE'},
						{name: 'ESTATUS'},
						{name: 'CG_VIA_LIQUIDACION'},
						{name: 'IC_FLUJO_FONDOS'},
						{name: 'DF_REGISTRO'},
						
						//Variables para obtener Detalle
						{name: 'IC_PYME'},
						{name: 'IC_EPO'},
						{name: 'DF_FECHA_VENC'},
						{name: 'DF_OPERACION'},
						{name: 'IC_ESTATUS_DOCTO'}
			        ],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
					load: procesarConsultaData,
					exception: {
								fn: function(proxy,type,action,optionsRequest,response,args){
										NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
										procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
								}
					}
			}
  }); 
  
  var totalesData = new Ext.data.JsonStore({
			root: 'registros',
			url: '15consultaEpoffext.data.jsp',
			baseParams: { informacion: 'ConsultaTotales' },
			fields: [
						{name: 'TOTALOPERACION'},
						{name: 'TOTALIMPORTETD'}
			        ],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
						exception: NE.util.mostrarDataProxyError
			}
	});
	
  var consultaDataDetalle = new Ext.data.JsonStore
  ({
			root: 'registros',
			url: '15consultaEpoffext.data.jsp',
			baseParams: { informacion: 'ConsultaDetalle' },
			fields: [
			         {name: 'IG_NUMERO_DOCTO'},
						{name: 'PYME'},
						{name: 'MONTO'},
						{name: 'IMPORTE'}
			        ],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
					load: procesarConsultaDataDetalle,
					exception: {
								fn: function(proxy,type,action,optionsRequest,response,args){
										NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
										procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
								}
					}
			}
  });
  
  var totalesDataDetalle = new Ext.data.JsonStore({
			root: 'registros',
			url: '15consultaEpoffext.data.jsp',
			baseParams: { informacion: 'ConsultaDetalleTotales' },
			fields: [
			         {name: 'TOTALREGISTROS'},  
						{name: 'TOTALIMPORTEDOCUMENTO'},
						{name: 'TOTALIMPORTEDESCUENTO'}
			        ],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
						exception: NE.util.mostrarDataProxyError
			}
	});
  	
//---------------------------------COMPONENTES-------------------------------
  var elementosForma =
  [{
         xtype: 'compositefield',
	      fieldLabel: 'Fecha de Registro',
			combineErrors: false,
			msgTarget: 'side',
			items: [
           {
			   // Fecha Inicio
			   xtype: 'datefield',
            name: 'dFechaRegIni',
            id: 'dFechaRegIni',
            vtype: 'rangofecha',
            campoFinFecho: 'dFechaRegFin',
				allowBlank: false,
				value: new Date(),
				width: 100,
				margins: '0 20 0 0'
           },
			  {
					xtype: 'displayfield',
					value: 'a',
					width: 20
			  },
		     {
			   // Fecha Final
			   xtype: 'datefield',
            name: 'dFechaRegFin',
            id: 'dFechaRegFin',
             vtype: 'rangofecha',
             campoInicioFecha: 'dFechaRegIni',
				allowBlank: false,
				value: new Date(),
				width: 100,
				margins: '0 20 0 0'
           }    
                ]
  }];
  
  var gridGeneral = new Ext.grid.GridPanel
  ({
		store: consultaData,
		stateful: true,
		hidden: true,
      id: 'gridGeneral',
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		title: '',
		frame: true,
		columns: [		
			{
				header: 'EPO',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'left',
				hidden: false,
				dataIndex: 'CG_RAZON_SOCIAL'
			},
			{
				header: 'IF',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'left',
				hidden: false,
				dataIndex: 'CRSI'
			},
			{
				header: 'PYME',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'left',
				hidden: false,
				dataIndex: 'CRSP'
			},
			{
				header: 'RFC',
				sortable: true,
				resizable: true,
			   width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'CG_RFC'
			},
			{
				header: 'Banco',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: false,
				dataIndex: 'IC_BANCOS_TEF'
			},
			{
				header: 'Tipo de Cuenta',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: false,
				dataIndex: 'IC_TIPO_CUENTA'
			},
			{
				header: 'No. de Cuenta',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'CG_CUENTA'
			},
			{
				header: 'Importe Total del Descuento',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'right',
				hidden: false,
				dataIndex: 'FN_IMPORTE',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Estatus',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'center',
				hidden: false,
				dataIndex: 'ESTATUS'
			},
			{
				header: 'V�a de Liquidaci�n',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: false,
				dataIndex: 'CG_VIA_LIQUIDACION'
			},
			{
				header: 'Folio',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'IC_FLUJO_FONDOS'
			},
			{
				header: 'Fecha de Registro',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'right',
				hidden: false,
				dataIndex: 'DF_REGISTRO'
			},
			{
				xtype: 'actioncolumn',
				header: 'Ver Detalle',
				tooltip: 'Ver Detalle',
				dataIndex: '',
            width: 130,
				align: 'center',
				items: [
					     {
						   getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {								
							 this.items[0].tooltip = 'Ver';								
							 return 'iconoLupa';											
						   },
						  handler: procesarDetalle
					     }
				       ]	
			}
		],
			bbar: 
	      {		
			 xtype: 'paging',
			 pageSize: 15,
			 buttonAlign: 'left',
			 id: 'barraPaginacion',
			 displayInfo: true,
			 store: consultaData,
			 displayMsg: '{0} - {1} de {2}',
			 emptyMsg: 'No hay registros.',
			 items: [
			       '->',
					 '-',
							{
								xtype: 'button',
								text: 'Totales',
								id: 'btnTotales',
								hidden: false,
								handler: function (boton,evento) {
											totalesData.load({ params: Ext.apply(fp.getForm().getValues()) });
											var gridTotales = Ext.getCmp('gridTotales');
											if(!gridTotales.isVisible()){
													gridTotales.show();
													gridTotales.el.dom.scrollIntoView();
											};
								}
							},
					 '-',
					 {
						xtype: 'button',
						text: 'Generar Comprobante de Transferencia',
						id: 'btnGenerarPDFCT',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '15consultaEpoffext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{ informacion: 'ArchivoPDFCT'}),
							callback: procesarSuccessFailureGenerarPDFCT});
						}
					 },
					 {
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDFCT',
						hidden: true
					 },
					 '-',
			       {
					  xtype: 'button',
					  text: 'Generar Archivo',
					  id: 'btnGenerarArchivo',
					  handler: function(boton, evento) 
					  {
					   boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
						url:'15consultaEpoffext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{ informacion: 'ArchivoCSV'}),
						callback: procesarSuccessFailureGenerarArchivo});
					  }
				    },
					 {
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarArchivo',
						hidden: true
					 },
					 '-',
					 {
						xtype: 'button',
						text: 'Generar PDF',
						id: 'btnGenerarPDF',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '15consultaEpoffext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{ informacion: 'ArchivoPDF'}),
							callback: procesarSuccessFailureGenerarPDF});
						}
					},
					{
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDF',
						hidden: true
					},
					'-'
					  ]
			 }
  });

  var gridTotales = {
	xtype: 'grid',
	store: totalesData,
	id: 'gridTotales',
	style: ' margin:0 auto;',
	hidden: true,
	columns: [
			    {
					header: 'Total de Operaciones',
					dataIndex: 'TOTALOPERACION',
					align: 'center',
					width: 150,
					renderer: Ext.util.Format.numberRenderer('0,000')
				 },
				 {
					header: 'Importe Total del Descuento',
					dataIndex: 'TOTALIMPORTETD',
					width: 200,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				 }
				],
		width: 940,
		height: 93,
		title: 'Totales',
		tools: [
					{
						id: 'close',
						handler: function(evento, toolEl, panel, tc){
						var gGridTotales = Ext.getCmp('gridTotales');
			         gGridTotales.hide();
						}
					}
		],
		frame: false
  };
  
  var gridDetalle =
  {   
      xtype: 'grid',
		store: consultaDataDetalle,
      id: 'gridDetalle',
		hidden: true,
		stateful: true,
		columns: [		
			{
				header: 'Num. de Documento',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'left',
				hidden: false,
				dataindex: 'IC_NUMERO_DOCTO'
			},
			{
				header: 'PyME	',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'left',
				hidden: false,
				dataindex: 'PYME'
			},
			{
				header: 'Importe Documento',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'left',
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				dataindex: 'MONTO'
			},
			{
				header: 'Importe Descuento',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'left',
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				dataindex: 'IMPORTE'
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 620,
		frame: true,
			bbar: 
	      {
			 xtype: 'paging',
			 pageSize: 15,
			 buttonAlign: 'left',
			 id: 'barraPaginacionD',
			 displayInfo: true,
			 store: consultaDataDetalle,
			 displayMsg: '{0} - {1} de {2}',
			 emptyMsg: 'No hay registros.',
			 items: [
			       '->',
			       '-',
							{
								xtype: 'button',
								text: 'Totales',
								id: 'btnTotalesD',
								hidden: false,
								handler: function (boton,evento) {
											totalesDataDetalle.load({ params: Ext.apply(fp.getForm().getValues()) });
											var gridDetalleTotales = Ext.getCmp('gridDetalleTotales');
											if(!gridDetalleTotales.isVisible()){
													gridDetalleTotales.show();
													gridDetalleTotales.el.dom.scrollIntoView();
											};
								}
							},
					 '-',
			       {
					  xtype: 'button',
					  text: 'Generar Archivo',
					  id: 'btnGenerarArchivoD',
					  handler: function(boton, evento) 
					  {
					   boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
						url:'15consultaEpoffext.data.jsp',
						params: Ext.apply//(fp.getForm().getValues(),
						({
						        informacion: 'ArchivoDetalleCSV',
								  ic_flujo_fondos : ic_flujo_fondos,
					           ic_epo : ic_epo,
						        ic_pyme : ic_pyme,
						        ic_estatus_docto : ic_estatus_docto,
						        df_fecha_venc : df_fecha_venc,
						        df_operacion : df_operacion
						}),
						callback: procesarSuccessFailureGenerarArchivoD});
					  }
				    },
					 {
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarArchivoD',
						hidden: true
					 }
					  ]
			 }
  };  

  var gridDetalleTotales = {
	xtype: 'grid',
	store: totalesDataDetalle,
	id: 'gridDetalleTotales',
	style: ' margin:0 auto;',
	hidden: true,
	columns: [
	          {
					header: 'Total de Documentos M. N.',
					dataIndex: 'TOTALREGISTROS',
					align: 'center',
					width: 150,
					renderer: Ext.util.Format.numberRenderer('0,000')
				 },
			    {
					header: 'Total Importe Documento',
					dataIndex: 'TOTALIMPORTEDOCUMENTO',
					align: 'right',
					width: 150,
					renderer: Ext.util.Format.numberRenderer('0,000')
				 },
				 {
					header: 'Total Importe Descuento',
					dataIndex: 'TOTALIMPORTEDESCUENTO',
					width: 150,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				 }
				],
		width: 620,
		height: 93,
		title: 'Totales',
		tools: [
					{
						id: 'close',
						handler: function(evento, toolEl, panel, tc){
						var gGridDetalleTotales = Ext.getCmp('gridDetalleTotales');
			         gGridDetalleTotales.hide();
						}
					}
		],
		frame: false
  };
  
  var fp = new Ext.form.FormPanel
  ({
		width: 500,
		labelWidth: 120,
		collapsible: true,
		title: 'Consulta Dispersion EPO',
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle:'padding:6px',
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
			  {
			   var gGridTotales = Ext.getCmp('gridTotales');
			   gGridTotales.hide();
			   consultaData.load({ params: Ext.apply(fp.getForm().getValues(),{
				//modalidad_plazo:modo,
									operacion: 'Generar', //Generar datos para la consulta
									start: 0,
									limit: 15})
							         });
			  }
			 },
			 {
			  //Bot�n Limpiar
			  xtype: 'button',
			  text: 'Limpiar',
			  name: 'btnLimpiar',
			  hidden: false,
			  //formBind: true,
			  iconCls: 'icoLimpiar',
			  handler: function(boton, evento) 
			  {
			   window.location = '15consultaEpoffext.jsp'
			  }
			 }
		  ]
  });

//-------------------------------- PRINCIPAL -----------------------------------
  var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 949,
	  items: 
	    [ 
	     fp,
	     NE.util.getEspaciador(20),
		  gridGeneral,
		  gridTotales
	    ]
  });

});