<%@ page 
	contentType=
		"application/json;charset=UTF-8" 
	import="
		net.sf.json.JSONArray,net.sf.json.JSONObject, netropology.utilerias.*, com.netro.cadenas.*, 
		java.util.*, javax.naming.*, com.netro.model.catalogos.*, org.apache.commons.logging.Log " 
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%><%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%    
String noEpo					  = (request.getParameter("_cmb_epo") 	!=null)	?	request.getParameter("_cmb_epo")		:	"";
String informacion			= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String operacion 				= (request.getParameter("operacion") 	!= null) ? 	request.getParameter("operacion")	: 	"";	
String infoRegresar			= "", consulta="";
int start=0, limit=0;

if (informacion.equals("catalogoepo") )	{
	JSONObject 					   jsonObj 	= new JSONObject();
	CatalogoEpoDispersion 	catEpo 	= new CatalogoEpoDispersion();
	catEpo.setCampoClave("ic_epo");
	catEpo.setCampoDescripcion("cg_razon_social");
	List lis = catEpo.getListaElementos(); 
	jsonObj.put("registros", lis);	
   infoRegresar = jsonObj.toString();
	
} else if (informacion.equals("Consultar") || informacion.equals("GeneraArchivoPDF")){
	
	JSONObject 					jsonObj 	= new JSONObject();
	BloqueoServicioConsulta  paginadorext	= new BloqueoServicioConsulta();
	
	paginadorext.setIcIf(noEpo);
  CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginadorext);
  if(informacion.equals("Consultar") ||  informacion.equals("GeneraArchivoPDF")){
  try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));										
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
  if(informacion.equals("Consultar") ){
	try {
		if (operacion.equals("Generar")) {	
			queryHelper.executePKQuery(request); 
		}
	consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
	jsonObj = JSONObject.fromObject(consulta);
  } else if(informacion.equals("GeneraArchivoPDF") ){
  try {
					String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					//infoRegresar = jsonObj.toString();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
			}//fin pdf
  }
  infoRegresar = jsonObj.toString();
  log.debug("jsonObj: "+jsonObj);
}
%>
<%=    infoRegresar%>