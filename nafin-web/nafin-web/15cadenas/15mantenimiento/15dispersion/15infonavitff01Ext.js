Ext.onReady(function() {
var montoTotal='';

	//----------------------------------- HANDLERS ------------------------------------
	//---------------------------descargaArchivo------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
//-----------------------------procesarConsultaData-----------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = store.reader.jsonData;
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			if(store.getTotalCount() > 0) {//////////Verifica que existan registros
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
				montoTotal=jsonData.totalMontoSIN_ERROR;
				var resutlados =[
					[	jsonData.Concepto,
						jsonData.totalDoctos,
						Ext.util.Format.usMoney(( jsonData.totalMonto )) 
					]
				];
				consultaTotalesParcialesData.loadData(resutlados);
				el.unmask();					
			} else {	
				//gridTotales.hide();
				gridTotalesParciales.hide();
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var procesarConsultaTotalesParcialesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = store.reader.jsonData;					
		var gridTotales = Ext.getCmp('gridTotalesParciales');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}								
			if(store.getTotalCount() > 0) {	
				el.unmask();
				
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}	
	
//---------------------------fin descargaArchivo--------------------------------

//--------------------------------- 1. STORES ----------------------------------

	var catalogoMoneda = new Ext.data.JsonStore({
		id: 					'catalogoMoneda',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15infonavitff01Ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoMoneda'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoEPO = new Ext.data.JsonStore({
		id: 					'catalogoEpo',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					'15infonavitff01Ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoEPO'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var consultaData = new Ext.data.JsonStore({
		root 			: 'registros',
		url 			: '15infonavitff01Ext.data.jsp',
		baseParams	: {
							informacion: 'Infonavit.Consultar'
							},
		fields		: [	
							{	name: 'CG_RAZON_SOCIAL'},	
							{	name: 'CG_RFC'},	
							{	name: 'DF_OPERACION'},	
							{	name: 'IG_TOTAL_DOCUMENTOS'},	
							{	name: 'CD_NOMBRE'},	
							{	name: 'FN_IMPORTE'},	
							{	name: 'IC_BANCOS_TEF'},	
							{	name: 'IC_TIPO_CUENTA'},	
							{	name: 'CG_CUENTA'},
							{	name: 'verDetalle'},
							{	name: 'usuario'},							
							{	name: 'fcancelacion'},
							{	name: 'causa'}
							],		
		totalProperty 	: 'total',
		messageProperty: 'msg',
		autoLoad			: false,
		listeners		: {
			load			: procesarConsultaData,
				exception: {
						fn	: function(proxy, type, action, optionsRequest, response, args) {
								NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
								//LLama procesar consulta, para que desbloquee los componentes.
								procesarConsultaData(null, null, null);					
						}
				}
		}					
	});
	var rt = Ext.data.Record.create([
		{	name: 'TITULO'},
		{	name: 'TOTAL_DOCTOS'},
		{	name:	'TOTAL_MONTO'}
	]);
	var consultaTotalesParcialesData=  new Ext.data.Store({
		 reader: new Ext.data.ArrayReader(
			  {
					idIndex: 1  // id for each record will be the first element
			  },
			  rt // recordType
		 ),
		 listeners: {
				load: procesarConsultaTotalesParcialesData,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaTotalesData(null, null, null);					
					}
				}
			}		
	});	
	
//------------------------------------------------------------------------------
//-------------------ELEMENTOS-------------------------------------------------
	var elementosForma = [
		//CONCEPTO
		{
			xtype: 'textfield',
			name:'concepto',
			id :'txtConcepto',
			fieldLabel :'Concepto',
			value:'Abonos',
			readOnly:true,
			style: 		'background: gainsboro;'
		},
		// COMBO EPO
		{
			xtype: 				'combo',
			name: 				'epo',
			id: 					'cboEpo',
			fieldLabel: 		'EPO',
			mode: 				'local',
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'epo',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPO,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%'
			
		},
		// COMBO MONEDA
		{
			xtype: 				'combo',
			name: 				'moneda',
			id: 					'cboMoneda',
			fieldLabel: 		'Moneda',
			mode: 				'local',
			allowBlank:			false,
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'moneda',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMoneda,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%'
			
		},
		//Fecha de Operaci�n
		{
			xtype:'datefield',
			name: 'fechaOperacion',
			id : 'txtFechaOperacion',
			fieldLabel:'Fecha de Operaci�n',
			allowBlank:			false,
			value: new Date()
			
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 				'forma',
		width: 			500,
		//title: 			'Selecci�n de Monitor',
		frame: 			true,
		collapsible: 	false,
		titleCollapse: false,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		items: 			elementosForma,
		buttons: [
		{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,	
				handler: function(boton, evento) {
				fp.el.mask('Procesando...', 'x-mask-loading');
				gridTotalesParciales.hide()
					consultaData.load({
						params		:Ext.apply(fp.getForm().getValues(),{
						informacion	:'Infonavit.Consultar'
						})
					});	
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				tabIndex			: 8,
				handler: function() {
					//window.location = '15consultacontratos01Ext.jsp';					
					gridConsulta.hide();
					gridTotalesParciales.hide();
					fp.getForm().reset();
					
				}
			}
		],
		monitorValid: 	true
	});
	//--------------------------------Grid Consulta---------------------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,
		columns			:[{
							header		:'Nombre',
							tooltip		:'Nombre',
							dataIndex	:'CG_RAZON_SOCIAL',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'RFC',
							tooltip		:'RFC',
							dataIndex	:'CG_RFC',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'				
							},{
							header		:'Fecha de<br>Operaci�n',
							tooltip		:'Fecha de<br>Operaci�n',
							dataIndex	:'DF_OPERACION',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Total de<br>Documentos',
							tooltip		:'Total de<br>Documentos',
							dataIndex	:'IG_TOTAL_DOCUMENTOS',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Moneda',
							tooltip		:'Moneda',
							dataIndex	:'CD_NOMBRE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'				
							},{
							header		:'Monto',
							tooltip		:'Monto',
							dataIndex	:'FN_IMPORTE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer		: function(v){
								return '<div align=right>'+Ext.util.Format.usMoney(v)+'</div>';
							}
							},{
							header		:'Banco',
							tooltip		:'Banco',
							dataIndex	:'IC_BANCOS_TEF',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Tipo de Cuenta',
							tooltip		:'Tipo de Cuenta',
							dataIndex	:'IC_TIPO_CUENTA',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'				
							},{
							header		: 'N&uacute;mero de Cuenta',
							tooltip		: 'N&uacute;mero de Cuenta',
							dataIndex	: 'CG_CUENTA',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center'
							}],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 430,
		width: 900,
		align: 'center',
		frame: false
		
	});
	var gridTotalesParciales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesParcialesData,
		id: 'gridTotalesParciales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		//title: 'Totales Parciales',	
		align: 'center',
		hidden: true,//true,
		columns: [	
			{
				header: '',
				tooltip: '',
				dataIndex: 'TITULO',
				sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Total de Documentos',
				tooltip: 'Total de Documentos',
				dataIndex: 'TOTAL_DOCTOS',
				sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center'
			},{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'TOTAL_MONTO',
				sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
							return '<div align="RIGHT">'+ value+'</div>';
				 }
			}
		],	
		stripeRows: true,
		loadMask: true,
		height: 150,
		width: 900,		
		frame: true	,
		bbar				: {
							store			: consultaData,
							emptyMsg		: "No hay registros.",
							items			: [
											'->','-',
											{
											xtype			: 'button',
											text			: 'Ejecutar Interfase flujo de Fondos',
											tooltip		: 'Ejecutar Interfase flujo de Fondos',
											iconCls		: 'icoEjecutar',
											id				: 'btnEjecutar',
											handler		: function(boton, evento) {
												
												
											boton.setIconClass('loading-indicator');
												Ext.Ajax.request({
																	url: '15infonavitff01Ext.data.jsp',
																	params: Ext.apply(fp.getForm().getValues(),{
																				informacion: 'Infonavit.Ejecutar_Flujo'
																				}),
																	success : function(response) {
																			boton.setIconClass('icoEjecutar');    
																			var infoR=(Ext.util.JSON.decode(response.responseText));
																			var numeroDocumentos = infoR.numeroDocumentos;///////////////////////
																			var importeTotal = infoR.importeTotal;///////////////////////
																			var numeroDocumentosError = infoR.numeroDocumentosError;///////////////////////
																			var importeTotalError = infoR.importeTotalError;///////////////////////
																			var claveEPO = infoR.claveEPO;///////////////////////
																			var claveEstatus = infoR.claveEstatus;
																			var claveMoneda = infoR.claveMoneda;
																			var claveIF = infoR.claveIF;
																			var claveIfFondeo = infoR.claveIfFondeo;
																			var origen = infoR.origen;
																			var fechaOperacion = infoR.fechaOperacion;
																			var pantallaOrigen = infoR.pantallaOrigen;
																			var parametros = '?numeroDocumentos='+numeroDocumentos+'&importeTotal='+importeTotal+'&numeroDocumentosError='+numeroDocumentosError+'&importeTotalError='+importeTotalError+'&claveEPO='+claveEPO+'&claveEstatus='+claveEstatus+'&claveMoneda='+claveMoneda+'&claveIF='+claveIF+'&claveIfFondeo='+claveIfFondeo+'&origen='+origen+'&fechaOperacion='+fechaOperacion+'&pantallaOrigen='+pantallaOrigen;
																			window.location = '15exportarinterfaseff01ext.jsp'+parametros;
										
																	}
																})
															}
											},
											{
											xtype			: 'button',
											text			: 'Exportar Archivo TEF',
											
											tooltip		: 'Exportar Archivo TEF',
											iconCls		: 'icoExportar',
											id				: 'btnExportarTEF',
											handler		: function(boton, evento) {
															if(montoTotal < 50000) {
																fp.el.mask('Generando archivo...', 'x-mask-loading');
																boton.setIconClass('loading-indicator');
																Ext.getCmp('btnExportarTEF').disable();
																Ext.Ajax.request({
																	url: '15infonavitff01Ext.data.jsp',
																	params: Ext.apply(fp.getForm().getValues(),{
																				informacion: 'Infonavit.ArchivoCSV',
																				tipo:'TEF'
																				}),
																	success : function(response) {
																		fp.el.unmask();
																		boton.setIconClass('icoExportar');     
																		boton.setDisabled(false);
										
																	},																															
																callback: procesarDescargaArchivos
																})
															}
															 else {
																Ext.Msg.alert('','El Monto corresponde a envio v�a SPEI.');
																return;
															}
														}	
															
											},
											{
											xtype			: 'button',
											text			: 'Generar Archivo',					
											tooltip		:	'Generar Archivo ',
											iconCls		: 'icoXls',
											id				: 'btnArchivoCSV',
											handler		: function(boton, evento) {
																fp.el.mask('Generando archivo...', 'x-mask-loading');
																boton.setIconClass('loading-indicator');
																Ext.getCmp('btnArchivoCSV').disable();
																Ext.Ajax.request({
																	url: '15infonavitff01Ext.data.jsp',
																	params: Ext.apply(fp.getForm().getValues(),{
																				informacion: 'Infonavit.ArchivoCSV',	
																				tipo:'normal'
																				}),
																	success : function(response) {
																		fp.el.unmask();
																		boton.setIconClass('icoXls');     
																		boton.setDisabled(false);
										
																	},																						
																callback: procesarDescargaArchivos
																})
															}
											},
											{
												xtype: 'button',
												text: 'Imprimir',					
												tooltip:	'Imprimir',
												iconCls: 'icoPdf',
												id: 'btnArchivoPDF',
												handler: function(boton, evento) {
													var barraPaginacionA = Ext.getCmp("barraPaginacion");	
													boton.setIconClass('loading-indicator');
														Ext.Ajax.request({
														url: '15infonavitff01Ext.data.jsp',
														params: Ext.apply(fp.getForm().getValues(),{							
															informacion: 'Infonavit.ArvhivoPDF'
														}),
														success : function(response) {
															boton.setIconClass('icoPdf');     
															boton.setDisabled(false);
							
														},
														callback: procesarDescargaArchivos
													});						
												}
											}]
								}
		
	});	
	
//-----------------------------Fin Grid Consulta--------------------------------
	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 947,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,gridTotalesParciales,
			NE.util.getEspaciador(20),	
			//fpBotones,
			NE.util.getEspaciador(20)			
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
		// Cargar Catlogo EPO
			//var catalogoEPOData = Ext.StoreMgr.key('catalogoEPO');
			catalogoEPO.load();
			// Cargar Catalogo Moneda
			//var catalogoMonedaData = Ext.StoreMgr.key('catalogoMoneda');
			catalogoMoneda.load();
	
});