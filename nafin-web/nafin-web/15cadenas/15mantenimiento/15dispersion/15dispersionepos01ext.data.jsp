<%@ page 
	contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("EnvioFlujoFondos.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
 
	// Revisar si hay un thread de una opracion anterior, y si es así detenerlo.
	EnvioFlujoFondosEPOThread thread = (EnvioFlujoFondosEPOThread) session.getAttribute("15DISPERSIONEPOS_THREAD");
	if(thread != null){
		thread.setRequestStop(true);
		// Esperar a que el thread finalice
		while(thread.isRunning() && !thread.isCompleted()){
			Thread.sleep(500);
		}
      session.removeAttribute("15DISPERSIONEPOS_THREAD");
	}
	
	// Enviar valor de la fecha actual
	resultado.put("fechaVencimiento",Fecha.getFechaActual());
	// Determinar el estado siguiente
	estadoSiguiente = "MOSTRAR_PANEL_BUSQUEDA_DISPERSIONES";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else if(  informacion.equals("ConsultaDispersionesEnlistadas")        )  {
	
	JSONObject		resultado 			= new JSONObject();
	boolean			success   			= true;
	
	// Leer parametros
	String 			fechaVencimiento 	= (request.getParameter("fechaVencimiento") == null)?"":request.getParameter("fechaVencimiento");
	
	// Obtener EJB de Dispersion
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	
	// Consultar detalle de las dispersiones enlistadas
	List consulta = dispersion.getDetalleDispersionesEnlistadas(fechaVencimiento);

	// Convertir respuesta a formato JSON
	JSONArray registros = new JSONArray();
	for(int i=0;i<consulta.size();i++){
		
		HashMap registro = (HashMap) consulta.get(i);
		registros.add(
			JSONObject.fromObject(registro)
		);
		
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)					);
	resultado.put("total",    	String.valueOf(registros.size()) );
	resultado.put("registros",	registros			 					);
	
	infoRegresar = resultado.toString();

} else if(  informacion.equals("EnvioFlujoFondos.guardarObservaciones") )  {
	
	JSONObject		resultado 			= new JSONObject();
	boolean			success   			= true;
	String 			estadoSiguiente 	= null;
	String			msg					= null;
 
	// Leer parametros
	String observacionesModificadas 	= (request.getParameter("observacionesModificadas") == null)?"":request.getParameter("observacionesModificadas");
	String loginDelUsuario				= iNoUsuario;
	String nombreDelUsuario				= (String) session.getAttribute("strNombreUsuario");
 
	// Recortar variables que pudieran causar problemas
	loginDelUsuario						= loginDelUsuario.length() >12 ?loginDelUsuario.substring(0,12)  :loginDelUsuario;
	nombreDelUsuario						= nombreDelUsuario.length()>100?nombreDelUsuario.substring(0,100):nombreDelUsuario;
 
	// Convertir JSONArray a Array normal
	List		 observacionesList 		= new ArrayList();
	JSONArray auxArrayObservaciones 	= JSONArray.fromObject(observacionesModificadas);
	for(int i=0;i<auxArrayObservaciones.size();i++){
		
		JSONObject 	auxObservacion = auxArrayObservaciones.getJSONObject(i);
		
		HashMap 		registro 		= new HashMap();
		registro.put("IC_EPO",			auxObservacion.getString("IC_EPO")			);
		registro.put("IC_MONEDA",		auxObservacion.getString("IC_MONEDA")		);
		registro.put("DC_VENCIMIENTO",auxObservacion.getString("DC_VENCIMIENTO"));
		registro.put("TIPO_DOCUMENTO",auxObservacion.getString("TIPO_DOCUMENTO"));
		registro.put("OBSERVACIONES",	auxObservacion.getString("OBSERVACIONES")	);
		
		observacionesList.add(registro);
		
	}
	
	// Obtener instancia del EJB de Dispersion
	Dispersion  dispersion 		= null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		log.error("EnvioFlujoFondos.guardarObservaciones");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
 
	}
 
	// Actualizar observaciones
	dispersion.actualizaObservacionesTableroDisperionEPO(observacionesList,loginDelUsuario,nombreDelUsuario);
	 
	// Determinar el estado siguiente
	estadoSiguiente = "EXITO_GUARDAR_OBSERVACIONES";
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("EnvioFlujoFondos.validarUsuario") ){
	
	// Inicializar respuesta
	JSONObject	resultado 		= new JSONObject();
	boolean		success			= true;
	
	// Leer parametros
	String 		claveUsuario 	= (request.getParameter("claveUsuario") == null)?"":request.getParameter("claveUsuario");
	String 		contrasena 		= (request.getParameter("contrasena")	 == null)?"":request.getParameter("contrasena");
 
	String 		loginUsuario	= iNoUsuario;
	
	// Obtener instancia del EJB de Dispersion
	Dispersion  dispersion 		= null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		log.error("EnvioFlujoFondos.validarUsuario");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
 
	}
 
	boolean esUsuarioCorrecto = false;
	try {
		
		// Verificar usuario
		if( !loginUsuario.equals(claveUsuario) ){
			
			resultado.put( "msg", "La Clave de Usuario proporcionada no corresponde con la que se ha firmado.");
			esUsuarioCorrecto = false;
			
		} else {
			
			esUsuarioCorrecto = dispersion.esUsuarioCorrecto(loginUsuario, claveUsuario, contrasena);
			
			// Si el usuario es incorrecto la siguiente seccion no se ejecuta porque se lanza una excepcion
			// solo se agrega el mensaje por compatibilidad con la version anterior
			if(!esUsuarioCorrecto){
				resultado.put( "msg", "La confirmación de la clave y contraseña es incorrecta, el proceso ha sido cancelado.");
			}
			
		}
 
	} catch (NafinException ne) {
		
		log.error("EnvioFlujoFondos.validarUsuario(Exception)");
		log.error("EnvioFlujoFondos.validarUsuario.claveUsuario = <" + claveUsuario + ">");
		log.error("EnvioFlujoFondos.validarUsuario.contrasena   = <" + contrasena   + ">");
		ne.printStackTrace();
		
		esUsuarioCorrecto = false;
		resultado.put( "msg", ne.getMessage() );
 
	} catch(Exception e) {
		
		log.error("EnvioFlujoFondos.validarUsuario(Exception)");
		log.error("EnvioFlujoFondos.validarUsuario.claveUsuario = <" + claveUsuario + ">");
		log.error("EnvioFlujoFondos.validarUsuario.contrasena   = <" + contrasena   + ">");
		e.printStackTrace();
		
		throw new AppException("Ocurrió un error al validar usuario.");
		
	} finally {
 
		resultado.put("esUsuarioCorrecto", new Boolean(esUsuarioCorrecto));
		
	}
	
	// Determinar el Estado Siguiente
	String estadoSiguiente = esUsuarioCorrecto?"FINALIZAR_ENVIO_FFON":"ESPERAR_DECISION";
	resultado.put("estadoSiguiente", estadoSiguiente);
	
	// Enviar respuesta
 	resultado.put("success", 						new Boolean(success));
 	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("EnvioFlujoFondos.finalizarEnvioFFON") )  {

	// Inicializar respuesta
	JSONObject	resultado 			= new JSONObject();
	boolean		success				= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// Leer parametros
	String contrasena 				= (request.getParameter("contrasena")				  == null)?"" 	 :request.getParameter("contrasena");
	String registrosSeleccionados = (request.getParameter("registrosSeleccionados") == null)?"[]" :request.getParameter("registrosSeleccionados");
	String totalRegistros 			= (request.getParameter("totalRegistros")			  == null)?"0"  :request.getParameter("totalRegistros");			
 
	String 		loginDelUsuario	= iNoUsuario;
	String 		nombreDelUsuario	= (String)    session.getAttribute("strNombreUsuario");
	
	// Obtener instancia del EJB de Dispersion
	Dispersion  dispersion 			= null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		log.error("EnvioFlujoFondos.finalizarEnvioFFON");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
 
	}
	
	// Revalidar contraseña
	boolean esUsuarioCorrecto 		= false;
	String  loginUsuario		  		= iNoUsuario;
	try {
		
		// Verificar usuario
		esUsuarioCorrecto = dispersion.esUsuarioCorrecto(loginUsuario, loginUsuario, contrasena);
			
		// Si el usuario es incorrecto la siguiente seccion no se ejecuta porque se lanza una excepcion
		// solo se agrega el mensaje por compatibilidad con la version anterior
		if(!esUsuarioCorrecto){
			resultado.put( "msg", "La confirmación de la clave y contraseña es incorrecta, el proceso ha sido cancelado.");
		}
 
	} catch (NafinException ne) {
		
		log.error("EnvioFlujoFondos.finalizarEnvioFFON(Exception)");
		log.error("EnvioFlujoFondos.finalizarEnvioFFON.contrasena   = <" + contrasena   + ">");
		ne.printStackTrace();
		
		esUsuarioCorrecto = false;
		resultado.put( "msg", ne.getMessage() );
 
		throw new AppException("Ocurrió un error al validar usuario."); // La validacion debería ser exitosa
		
	} catch(Exception e) {
		
		log.error("EnvioFlujoFondos.finalizarEnvioFFON(Exception)");
		log.error("EnvioFlujoFondos.finalizarEnvioFFON.contrasena   = <" + contrasena   + ">");
		e.printStackTrace();
		
		throw new AppException("Ocurrió un error al validar usuario.");
		
	}
	
	// Preparar registros leídos
	JSONArray registrosDispersionEPO = JSONArray.fromObject(registrosSeleccionados);
	
	// Recortar variables que pudieran causar problemas
	loginDelUsuario						= loginDelUsuario.length() >12 ?loginDelUsuario.substring(0,12)  :loginDelUsuario;
	nombreDelUsuario						= nombreDelUsuario.length()>100?nombreDelUsuario.substring(0,100):nombreDelUsuario;
 
	// Crear un thread nuevo
	EnvioFlujoFondosEPOThread thread = new EnvioFlujoFondosEPOThread();
	thread.setLoginDelUsuario(loginDelUsuario);             
	thread.setNombreDelUsuario(nombreDelUsuario);    
	thread.setDispersion(dispersion); 
	thread.setNumeroTotalEnviosFFON( Integer.parseInt( totalRegistros ) );
	thread.setRegistrosDispersionEPO(registrosDispersionEPO);
 
	// Lanzar proceso de sincronizacion
	thread.setRunning(true);
	new Thread(thread).start();
	// Poner el thread en sesion
	session.setAttribute("15DISPERSIONEPOS_THREAD",thread);
 
	// Enviar detalle del envío de las notificaciones
	resultado.put("numeroTotalEnviosFFON",			String.valueOf( totalRegistros )			);
	resultado.put("numeroEnviosFFONRealizados",	"0"	 											);
	resultado.put("avance",								"0"	 											);
	resultado.put("porcentajeAvance",				"0"	 											);
	resultado.put("envioFinalizado",					new Boolean(false)	 						);
	
	// Determinar el estado siguiente
	estadoSiguiente = "MUESTRA_PANTALLA_AVANCE"; 
						
	// Enviar el total de envios a Flujo de Fondos
	resultado.put("totalRegistros",		totalRegistros			);
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();

} else if( informacion.equals("EnvioFlujoFondos.muestraPantallaAvance") ){
	
	JSONObject	resultado 					= new JSONObject();
	boolean		success	 					= true;
	String 		estadoSiguiente 			= null;
	String		msg							= null;
	
	String 		numeroTotalEnviosFFON 	= (request.getParameter("numeroTotalEnviosFFON") == null)?"0":request.getParameter("numeroTotalEnviosFFON");
	
	resultado.put("numeroTotalEnviosFFON",			numeroTotalEnviosFFON	);
	resultado.put("numeroEnviosFFONRealizados",	"0"	 						);
	resultado.put("avance",								"0"	 						);
	resultado.put("porcentajeAvance",				"0"	 						);
	resultado.put("envioFinalizado",					new Boolean(false)		);
	
	// Determinar el estado siguiente
	estadoSiguiente = "ACTUALIZAR_AVANCE";
	
	// Enviar resultado de la operacion
	resultado.put("success", 							new Boolean(success)		);
	resultado.put("estadoSiguiente", 				estadoSiguiente 			);
	resultado.put("msg",									msg							);
	
	infoRegresar = resultado.toString();
 
} else if( informacion.equals("EnvioFlujoFondos.actualizarAvance") 		){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// LEER PARAMETROS
	boolean		envioFinalizado 			= "true".equals(request.getParameter("envioFinalizado"))?true:false;

	// LEER THREAD DE SESION
	EnvioFlujoFondosEPOThread thread  	= (EnvioFlujoFondosEPOThread) session.getAttribute("15DISPERSIONEPOS_THREAD");
	
	// UNA VEZ QUE AE HA MOSTRADO QUE SE HA LLEGADO AL 100%, PRESENTAR RESULTADOS
	if(        envioFinalizado && !thread.hasError() ){ // El envio a flujo de fondos fue exitoso
	
		// msg = "La operación ha sido completada."; // Agregar diferentes mensajes dependiendo de si hay algunos registros que fallaron.
		
		// Enviar detalle de los envios realizados
		JSONArray resultados = new JSONArray();
		ArrayList registros = thread.getResultados();
		for(int i=0;i<registros.size();i++){
			HashMap 		auxRegistro = (HashMap) registros.get(i);
			JSONObject 	registro 	= JSONObject.fromObject(auxRegistro);
			resultados.add(registro);
		}
		resultado.put( "resultados", resultados );
			
		// Remover thread de sesion
		session.removeAttribute("15DISPERSIONEPOS_THREAD");
		
		// Determinar el estado siguiente
		estadoSiguiente = "MOSTRAR_ENVIO_FFON_COMPLETADO";
		
	// EN CASO DE QUE HAYA FALLADO EL THREAD, MOSTRAR DETALLE DEL ERROR Y REINICIALIZAR LA PANTALLA
	} else if( envioFinalizado &&  thread.hasError() ){ 
	
		// Mostrar mensaje de error mediante un alert.
		msg = thread.getMensajes();
			
		// Remover thread de sesion
		session.removeAttribute("15DISPERSIONEPOS_THREAD");
		
		// DETERMINAR EL ESTADO SIGUIENTE
		estadoSiguiente = "FIN";
			
	// ENVIAR PORCENTAJE DE AVANCE ACTUALIZADO
	} else {
		
		// ENVIAR PORCENTAJE DE AVANCE
		resultado.put("numeroTotalEnviosFFON",			new Integer(thread.getNumberOfRegisters())	);
		resultado.put("numeroEnviosFFONRealizados",	new Integer(thread.getProcessedRegisters())	);
		resultado.put("avance",								new Double(thread.getPercent()/100.0f)			);
		resultado.put("porcentajeAvance",				new Double(thread.getPercent())					);
		resultado.put("envioFinalizado",             new Boolean(thread.isCompleted())				);
			
		// DETERMINAR EL ESTADO SIGUIENTE
		estadoSiguiente = "ACTUALIZAR_AVANCE";
		
	}
 
	// ENVIAR RESULTADO DE LA OPERACION
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("ConsultaDetalleDispersionesEPO") ) {
	
	JSONObject	resultado 					= new JSONObject();
	boolean		success	 					= true;
	
	// Leer parametros
	String claveEpo 							= (request.getParameter("icEpo")								== null)?"":request.getParameter("icEpo");
	String claveMoneda 						= (request.getParameter("icMoneda")							== null)?"":request.getParameter("icMoneda");
	String fechaVencimiento 				= (request.getParameter("fechaVencimiento")				== null)?"":request.getParameter("fechaVencimiento");
	String fechaEnvioFlujoFondosOpr 		= (request.getParameter("fechaEnvioFlujoFondosOpr")	== null)?"":request.getParameter("fechaEnvioFlujoFondosOpr");
	String fechaEnvioFlujoFondosSnOpr 	= (request.getParameter("fechaEnvioFlujoFondosSnOpr")	== null)?"":request.getParameter("fechaEnvioFlujoFondosSnOpr");
	
	String operacion							= (request.getParameter("operacion") 						== null)?"":request.getParameter("operacion");
	
	// Validar que al menos un grupo de documentos haya sido enviado a flujo de fondos
	if( fechaEnvioFlujoFondosOpr.matches("\\s*") && fechaEnvioFlujoFondosSnOpr.matches("\\s*") ){
		throw new AppException("Se requiere que al menos un grupo de documentos haya sido enviado a flujo de fondos.");
	}
 		
	// Crear instancia del paginador
	ConsultaDispersionEPOEnFFON paginador = new ConsultaDispersionEPOEnFFON();
	paginador.setClaveEpo(claveEpo);
	paginador.setClaveMoneda(claveMoneda);
	paginador.setFechaVencimiento(fechaVencimiento);
	paginador.setFechaEnvioFlujoFondosOpr(fechaEnvioFlujoFondosOpr);
	paginador.setFechaEnvioFlujoFondosSnOpr(fechaEnvioFlujoFondosSnOpr);
 
	// Crear instancia del query helper y el objeto de resultado
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	// Leer posicion de los registros
	int start = 0;
	int limit = 0;
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	// Realizar consulta
	try {
		
		if ("NuevaConsulta".equals(operacion)) {	//Nueva consulta
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		
		JSONArray 	registros 	= new JSONArray();
		JSONObject 	registro 	= null;
		
		Registros consulta	= queryHelper.getPageResultSet(request,start,limit);	
		
		while( consulta != null && consulta.next() ){
			
			// Leer campos: IMPORTE_TOTAL_DESCUENTO
			String importeTotalDescuento 	= consulta.getString("IMPORTE_TOTAL_DESCUENTO");
			importeTotalDescuento  			= importeTotalDescuento == null || importeTotalDescuento.matches("\\s*")?"0":importeTotalDescuento;
			importeTotalDescuento			= Comunes.formatoMN(importeTotalDescuento);
 
			// Leer campos: ESTATUS_FFON
			String estatusFFON				= consulta.getString("ESTATUS_FFON");
			String estatusCECOBAN			= consulta.getString("ESTATUS_CECOBAN");
			String claveEstatusFFON			= consulta.getString("CLAVE_ESTATUS_FFON");
			String causaRechazoFFON 		= consulta.getString("CAUSA_RECHAZO_FFON");
			if( !"99".equals(estatusCECOBAN) && "-1".equals(claveEstatusFFON) ){
				estatusFFON = "Error en la informaci&oacute;n de la cuenta";
			} else {
				estatusFFON = estatusFFON + ": " + causaRechazoFFON;
			}
				
			// ENVIAR DETALLE DEL REGISTRO
			registro = new JSONObject();
			
			registro.put("NOMBRE_IF",					 consulta.getString("NOMBRE_IF")					);
			registro.put("NOMBRE_PYME",				 consulta.getString("NOMBRE_PYME")				);
			registro.put("RFC_PYME",					 consulta.getString("RFC_PYME")					);
			registro.put("CLAVE_BANCO",				 consulta.getString("CLAVE_BANCO")				);
			registro.put("TIPO_CUENTA",				 consulta.getString("TIPO_CUENTA")				);
			registro.put("CUENTA_CLABE_SWIFT",		 consulta.getString("CUENTA_CLABE_SWIFT")		);
			registro.put("IMPORTE_TOTAL_DESCUENTO", importeTotalDescuento								);
			registro.put("ESTATUS_FFON",				 estatusFFON											);
			registro.put("VIA_LIQUIDACION",			 consulta.getString("VIA_LIQUIDACION")			);
			registro.put("FOLIO",						 consulta.getString("FOLIO")						);
			registro.put("FECHA_REGISTRO",			 consulta.getString("FECHA_REGISTRO")			);
					
			registros.add(registro);
			
		}
		
		resultado.put( "total",			new Integer( queryHelper.getIdsSize() )	);
		resultado.put( "registros",	registros 											);
		 
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}	
  
	resultado.put("success",new Boolean(success));
	
	infoRegresar = resultado.toString();	
	
} else if (informacion.equals("ConsultaTotalesDetalleDispersionesEPO")) {
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;

	//Debe ser llamado despues de Consulta
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion				
	String 		totales 				= queryHelper.getJSONResultCount(request); //los saca de sesion
	
	JSONObject 	resultadoTotales	= JSONObject.fromObject(totales);
	JSONArray   registrosTotales	= resultadoTotales.getJSONArray("registros"); 
 
	if( registrosTotales.size() > 0 ){
				
		JSONObject 	registroTotal   			= registrosTotales.getJSONObject(0);
		String 		totalOperaciones 		 	= registroTotal.getString("TOTAL_OPERACIONES");
		String 		importeTotalDescuento 	= registroTotal.getString("IMPORTE_TOTAL_DESCUENTO");
				
		resultado.put("totalOperaciones",		      Comunes.formatoDecimal(totalOperaciones,      0, true) );
		resultado.put("importeTotalDescuento",	"$" + Comunes.formatoDecimal(importeTotalDescuento, 2, true) );
				
	} else {
				
		resultado.put("totalOperaciones",		"0"		);
		resultado.put("importeTotalDescuento",	"$0.00"	);
				
	}
 
	// ENVIAR RESULTADO DE LA OPERACION
	resultado.put("success", 				new Boolean(success)	);
	infoRegresar = resultado.toString();

} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>