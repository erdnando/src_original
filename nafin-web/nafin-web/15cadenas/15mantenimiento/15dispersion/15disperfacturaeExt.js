/*
* Migraci�n : FODEA-XX-2014
* PANTALLA 	: Administraci�n / Dispersi�n / Resumen EPO's
* Autor		: Jhonatan Pacheco Hern�ndez
*/
Ext.onReady(function(){

//Variables Globales
var _URL_ = '15disperfacturaeExt.data.jsp'

var esTarifaEnMonedaNacionalValida = ''; 
var esTarifaEnDolaresAmericanosValida = ''; 
	
var txt_dias1; 
var txt_dias2; 
var txt_dias1USD; 
var txt_dias2USD; 
//------------------------------------------------------------------------------
//-------------------------------HANDLER's--------------------------------------
//------------------------------------------------------------------------------
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};						
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarMonedasValidaData = function (opts, success, response)  {
		var panel = Ext.getCmp('clasificacion');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var mensaje = (Ext.util.JSON.decode(response.responseText).mensaje);
			var data = (Ext.util.JSON.decode(response.responseText).data);
			
			if (mensaje !=""){
				Ext.Msg.alert('',mensaje);
				//Ocultar todos los elementos 
					var panel = Ext.getCmp('panelTabla').hide();
					var grid1 = Ext.getCmp('grid_01').hide();	
					var gridMN = Ext.getCmp('gridDetalleDoctoOperMN').hide();	//
					var gridMNTotales = Ext.getCmp('gridDetalleDoctoOperMNTotales').hide();
					var gridUSD = Ext.getCmp('gridDetalleDoctoOperUSD').hide();	
					var gridUSDTotales = Ext.getCmp('gridDetalleDoctoOperUSDTotales').hide();				
					var panelTablaMensaje = Ext.getCmp('panelTablaMensaje').hide();
					var bntConsultar = Ext.getCmp('bntConsultar').hide();
			}
			if(data != ""){
				esTarifaEnMonedaNacionalValida = (Ext.util.JSON.decode(response.responseText).esTarifaEnMonedaNacionalValida);
				if(esTarifaEnMonedaNacionalValida){
					txt_dias1 = (Ext.util.JSON.decode(response.responseText).txt_dias1);
					txt_dias2 = (Ext.util.JSON.decode(response.responseText).txt_dias2);
				}
				esTarifaEnDolaresAmericanosValida = (Ext.util.JSON.decode(response.responseText).esTarifaEnDolaresAmericanosValida);
				if(esTarifaEnDolaresAmericanosValida){
					txt_dias1USD= (Ext.util.JSON.decode(response.responseText).txt_dias1USD);
					txt_dias2USD= (Ext.util.JSON.decode(response.responseText).txt_dias2USD);
				}
				var bntConsultar = Ext.getCmp('bntConsultar').show();
				panel.show();
				panel.setValue(data);
			}
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsulta = function (opts, success, response)  {
		var panelFormaConsulta   = Ext.getCmp('forma');
		panelFormaConsulta.el.unmask();
		var panel = Ext.getCmp('panelTabla');	
			
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if(Ext.util.JSON.decode(response.responseText).data !=''){
				Ext.Msg.alert('',Ext.util.JSON.decode(response.responseText).data);
				panel.hide();
			}else{
				
				//Actualizar los datos del contenedor
				var fechaEmision = Ext.util.JSON.decode(response.responseText).Fecha_emision;
				Ext.getCmp('fecha_emision').setValue('<div align=\"right\" width=\"100%\">Fecha de Emisi�n : '+fechaEmision+' </div>')
				var nombreEPO =Ext.util.JSON.decode(response.responseText).nombreEPO;
				Ext.getCmp('nombre_epo').setValue('EPO:&nbsp;<b>'+nombreEPO+'</b>');
				//Fecha Inicio
				Ext.getCmp('fecha_reg_inicio').setValue('<div align=\"center\" width=\"100%\">'+Ext.util.Format.date(Ext.getCmp('txtFechaPeriodoMin').getValue(),'d/M/Y')+'</div>');
				//fecha fin
				Ext.getCmp('fecha_reg_fin').setValue('<div align=\"center\" width=\"100%\">'+Ext.util.Format.date(Ext.getCmp('txtFechaPeriodoMax').getValue(),'d/M/Y')+'</div>');
				//Mostramos el panel
				if (!panel.isVisible()) {
					panel.show();
				}
				//Cargar el Store grid1StoreData
				var grid1ControlData = Ext.StoreMgr.key('grid1StoreData');
				grid1ControlData.loadData(Ext.util.JSON.decode(response.responseText).cifrasGrid1DataArray);
				
				//Cargar el Store grid2StoreData
				//
				var gridDetalleDoctoOperMNSData = Ext.StoreMgr.key('gridDetalleDoctoOperMNStoreData');
				gridDetalleDoctoOperMNStoreData.loadData(Ext.util.JSON.decode(response.responseText).cifrasGridDetalleMNDataArray);
				var gridDetalleDoctoOperMNTotalesStoreData = Ext.StoreMgr.key('gridDetalleDoctoOperMNTotalesStoreData');
				if(gridDetalleDoctoOperMNSData.getCount()>0){
					gridDetalleDoctoOperMNTotalesStoreData.loadData(Ext.util.JSON.decode(response.responseText).cifrasGridDetalleMNTotalesDataArray);
					var gridTotales = Ext.getCmp('gridDetalleDoctoOperMNTotales');	
					if (!gridTotales.isVisible()) {
						gridTotales.show();
					}
				}
				//Carga el Sotre grid 2
				var gridDetalleDoctoOperUSDSData = Ext.StoreMgr.key('gridDetalleDoctoOperUSDStoreData');
				gridDetalleDoctoOperUSDStoreData.loadData(Ext.util.JSON.decode(response.responseText).cifrasGridDetalleUSDDataArray);
				var gridDetalleDoctoOperUSDTotalesStoreData = Ext.StoreMgr.key('gridDetalleDoctoOperUSDTotalesStoreData');
				
				if(gridDetalleDoctoOperUSDSData.getCount()>0){
					gridDetalleDoctoOperUSDTotalesStoreData.loadData(Ext.util.JSON.decode(response.responseText).cifrasGridDetalleUSDTotalesDataArray);
					var gridTotalesUSD = Ext.getCmp('gridDetalleDoctoOperUSDTotales');	
					if (!gridTotalesUSD.isVisible()) {
						gridTotalesUSD.show();
					}
				}
				if(Ext.util.JSON.decode(response.responseText).esTarifaEnMonedaNacionalValida){//
					var grid = Ext.getCmp('gridDetalleDoctoOperMN');
					columnHeaderGird2.config.rows[0][1].header =Ext.util.JSON.decode(response.responseText).textoHeaderMN_1;
					columnHeaderGird2.config.rows[0][2].header =Ext.util.JSON.decode(response.responseText).textoHeaderMN_2;
					grid.getView().refresh(true);
					grid.show();	
				}else{
					var grid = Ext.getCmp('gridDetalleDoctoOperMN').hide();
				}
				if(Ext.util.JSON.decode(response.responseText).esTarifaEnDolaresAmericanosValida){//
					var grid = Ext.getCmp('gridDetalleDoctoOperUSD')
					columnHeaderGirdUSD.config.rows[0][1].header ='Hasta '+txt_dias1USD+' D�as Inclusive';
					columnHeaderGirdUSD.config.rows[0][2].header ='De los '+(parseInt(txt_dias1USD)+1)+' a los '+txt_dias2USD+' D�as';
					grid.getView().refresh(true);
					grid.show();	
				}else{
					var grid = Ext.getCmp('gridDetalleDoctoOperUSD').hide();	
				}
				//Actualizamos la info del panel 
				Ext.getCmp('uusarioElaboro').setValue('<div align=\"center\" width=\"100%\">Elabor&oacute;:&nbsp;<u><b>'+Ext.util.JSON.decode(response.responseText).strNombreUsuario+'</b></div><br><br>');
				//Mostrar mensaje y botones		
				
				var panelTablaMensaje = Ext.getCmp('panelTablaMensaje').show();
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarGrid1Data = function(store, arrRegistros, opts) 	{
		var grid1 = Ext.getCmp('grid_01');	
		var el = grid1.getGridEl();	
		var jsonData = store.reader.jsonData;	
		if (arrRegistros != null) {
			if (!grid1.isVisible()) {
				grid1.show();
			}				
			if(store.getTotalCount() > 0) {	
				el.unmask();
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');	
			}
		}
	}
	var procesarGridDetalleDoctoOperMNData = function(store, arrRegistros, opts) 	{
		var grid = Ext.getCmp('gridDetalleDoctoOperMN');	
		var el = grid.getGridEl();	
		var jsonData = store.reader.jsonData;	
		
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}				
			if(store.getTotalCount() > 0) {	
			
				el.unmask();
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');	
			}
		}
	}
	var procesarGridDetalleDoctoOperUSDData = function(store, arrRegistros, opts) 	{
		var grid = Ext.getCmp('gridDetalleDoctoOperUSD');	
		var el = grid.getGridEl();	
		var jsonData = store.reader.jsonData;
		
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}				
			if(store.getTotalCount() > 0) {	
			
				el.unmask();
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');	
			}
		}
	}

//------------------------------------------------------------------------------
//---------------------------------STORE's--------------------------------------
//------------------------------------------------------------------------------
var catalogoEPOData = new Ext.data.JsonStore({
		id: 					'catalogoEPODataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion', 'loadMsg'],
		url: 					_URL_,
		baseParams: {
			informacion: 	'CatalogoEPO'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

var grid1StoreData = new Ext.data.ArrayStore({
    autoDestroy: true,
    storeId: 'grid1StoreData',
    idIndex: 0,
    fields: [
       {name: 'COL0',mapping:0},
       {name: 'COL1',mapping:1},
       {name: 'COL2',mapping:2},
       {name: 'COL3',mapping:3}
    ],
	 listeners: {
			load: procesarGrid1Data,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarGrid1Data(null, null, null);					
				}
			}
		}
});
var gridDetalleDoctoOperMNStoreData = new Ext.data.ArrayStore({
    autoDestroy: true,
    storeId: 'gridDetalleDoctoOperMNStoreData',
    idIndex: 0,
    fields: [
       {name: 'COL0',mapping:0},
       {name: 'COL1',mapping:1},
       {name: 'COL2',mapping:2},
       {name: 'COL3',mapping:3},
		 {name: 'COL4',mapping:4},
       {name: 'COL5',mapping:5},
       {name: 'COL6',mapping:6}
    ],
	 listeners: {
			load: procesarGridDetalleDoctoOperMNData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarGridDetalleDoctoOperMNData(null, null, null);					
				}
			}
		}
});
var gridDetalleDoctoOperMNStoreDataTotales = new Ext.data.ArrayStore({
    autoDestroy: true,
    storeId: 'gridDetalleDoctoOperMNTotalesStoreData',
    idIndex: 0,
    fields: [
       {name: 'COL0',mapping:0},
       {name: 'COL1',mapping:1},
       {name: 'COL2',mapping:2},
       {name: 'COL3',mapping:3},
		 {name: 'COL4',mapping:4},
       {name: 'COL5',mapping:5},
       {name: 'COL6',mapping:6}
    ]
});	
var gridDetalleDoctoOperUSDStoreData = new Ext.data.ArrayStore({
    autoDestroy: true,
    storeId: 'gridDetalleDoctoOperUSDStoreData',
    idIndex: 0,
    fields: [
       {name: 'COL0',mapping:0},
       {name: 'COL1',mapping:1},
       {name: 'COL2',mapping:2},
       {name: 'COL3',mapping:3},
		 {name: 'COL4',mapping:4},
       {name: 'COL5',mapping:5},
       {name: 'COL6',mapping:6}
    ],
	 listeners: {
			load: procesarGridDetalleDoctoOperUSDData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarGridDetalleDoctoOperUSDData(null, null, null);					
				}
			}
		}
});
var gridDetalleDoctoOperUSDStoreDataTotales = new Ext.data.ArrayStore({
    autoDestroy: true,
    storeId: 'gridDetalleDoctoOperUSDTotalesStoreData',
    idIndex: 0,
    fields: [
       {name: 'COL0',mapping:0},
       {name: 'COL1',mapping:1},
       {name: 'COL2',mapping:2},
       {name: 'COL3',mapping:3},
		 {name: 'COL4',mapping:4},
       {name: 'COL5',mapping:5},
       {name: 'COL6',mapping:6}
    ]
});	
//------------------------------------------------------------------------------
//------------------------Elementos Visuales------------------------------------
//------------------------------------------------------------------------------
	var elementosForma = [
		{
			xtype 		: 'combo',
			id				: 'cboEpo',
			name			: 'cboepo',
			allowBlank	: false,
			hiddenName	: 'epo',
			fieldLabel	: 'EPO',
			displayField: 'descripcion',
			valueField	: 'clave',
			mode			: 'local',
			emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'-20',
			listeners	: {
				select :function(combo){
					//TarifaMondedasValidaDATA
					var panel = Ext.getCmp('clasificacion');
					panel.hide();
					Ext.Ajax.request({
							url: _URL_,
							params: Ext.apply(fp.getForm().getValues(),{ 
									informacion: 'TarifaMondedasValidaDATA'
								}
							),
							callback: procesarMonedasValidaData
					});
					
				}
			}
			
		},
		{
				xtype: 'compositefield',
				fieldLabel: 'Periodo (Fecha)',
				combineErrors: false,
				msgTarget: 'side',
				
				items: [
					{
						xtype: 'datefield',
						name: 'fechaPeriodoMin',
						id: 'txtFechaPeriodoMin',
						allowBlank: false,
						startDay: 0,
						vtype: 'rangofecha',
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoFinFecha: 'txtFechaPeriodoMax',
						tabIndex			: 9,
						margins: '0 20 0 0',  //necesario para mostrar el icono de error
						value : new Date()
					},
					{
						xtype: 'displayfield',
						value: 'al',
						width: 15,margins:{top:0, right:10, bottom:0, left:-10}
					},
					{
						xtype: 'datefield',
						name: 'fechaPeriodoMax',
						id: 'txtFechaPeriodoMax',
						allowBlank: false,
						startDay: 1,
						vtype: 'rangofecha',
						width: 100,
						tabIndex			: 10,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoInicioFecha: 'txtFechaPeriodoMin',
						margins: '0 20 0 0',  //necesario para mostrar el icono de error}
						value : new Date()
					}
				]
			},
			{
				xtype : 'displayfield',
				id : 'clasificacion',
				hidden :true,
				hideLabel: true,
				value : ''
				
			}
			
	];
	

	var fp = new Ext.form.FormPanel({
		id: 				'forma',
		layout:			'form',
		width: 			474,
		title: 			'Forma de Consulta',
		//hidden:			Ext.isIE7?false:true, // Debido a un bug en el layout de los composite fields para IE7
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto; ', 
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	110,
		defaultType: 	'textfield',
		items: 			elementosForma,
		monitorValid: 	true,
		buttons: [
			{
				text: 		'Consultar',
				id : 'bntConsultar',
				iconCls: 	'icoBuscar',
				formBind : true,
				handler: 	function( boton, evento ){
					var panelFormaConsulta = Ext.getCmp("forma");
					var invalidForm = false;
					if( !panelFormaConsulta.getForm().isValid() ){
						invalidForm = true;
					}
					
					if( invalidForm ){
						return; // La forma es invalida, se cancela la operacion
					}
					// Aplicar m�scara de consulta
					var panelFormaConsulta = Ext.getCmp("forma");
					panelFormaConsulta.el.mask("Consultando...","x-mask-loading");
					var epo = Ext.getCmp('cboEpo').getRawValue();
					//Ocultar todos los elementos 
					var panel = Ext.getCmp('panelTabla').hide();
					var grid1 = Ext.getCmp('grid_01').hide();	
					var gridMN = Ext.getCmp('gridDetalleDoctoOperMN').hide();	
					var gridMNTotales = Ext.getCmp('gridDetalleDoctoOperMNTotales').hide();
					var gridUSD = Ext.getCmp('gridDetalleDoctoOperUSD').hide();	
					var gridUSDTotales = Ext.getCmp('gridDetalleDoctoOperUSDTotales').hide();
					var panelTablaMensaje = Ext.getCmp('panelTablaMensaje').hide();
					// Realizar consulta
					
					Ext.Ajax.request({
							url: _URL_,
							params: Ext.apply(fp.getForm().getValues(),{ 
									informacion: 'Consultar_Detalle',
									esTarifaEnMonedaNacionalValida		:	esTarifaEnMonedaNacionalValida,
									esTarifaEnDolaresAmericanosValida	:	esTarifaEnDolaresAmericanosValida,
									txt_dias1									:	txt_dias1,
									txt_dias2									:	txt_dias2,
									txt_dias1USD								:	txt_dias1USD,
									txt_dias2USD								:	txt_dias2USD,
									nombreEPO									:	epo
								}
							),
							callback: procesarConsulta
					});
			
				}
			},
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar'
				,handler: 	function(boton){
					//Ocultar todos los elementos 
					var panel = Ext.getCmp('panelTabla').hide();
					var grid1 = Ext.getCmp('grid_01').hide();	
					var gridMN = Ext.getCmp('gridDetalleDoctoOperMN').hide();	//
					var gridMNTotales = Ext.getCmp('gridDetalleDoctoOperMNTotales').hide();
					var gridUSD = Ext.getCmp('gridDetalleDoctoOperUSD').hide();	
					var gridUSDTotales = Ext.getCmp('gridDetalleDoctoOperUSDTotales').hide();				
					var panelTablaMensaje = Ext.getCmp('panelTablaMensaje').hide();
					fp.getForm().reset();
				}
			}
		]
	});

	
	var contenedor =new Ext.Container ({
		layout: 'table',
		id: 'panelTabla',
		width: 600,
		heigth: 'auto',
		style: 'margin: 0 auto',
		bodyStyle:'padding:10px',
		hidden:true,
		layoutConfig: {
			columns: 9
		},
		items: [	
			{
				xtype: 'displayfield',
				id: 'fecha_emision',
				value :'<div align=\"right\" width=\"100%\">Fecha de Emisi�n : 18/11/2014 </div>',
				width: 650,
				hideLabel: true,
				colspan: 9
				
			},
			{
				xtype: 'displayfield',
				id: 'nombre_epo',
				value :'EPO:&nbsp;<b>nombreEPO</b>',
				hideLabel: true,
				colspan: 9
			},
			{	
				xtype: 'displayfield',
				value: ''
			},
			{
				xtype: 'displayfield',
				html :'<div align=\"center\" width=\"100%\">Fecha de Inicio</div>',
				hideLabel: true
			},
			{	
				xtype: 'displayfield',
				value: ''
			},
			{
				xtype: 'displayfield',
				value :'<div align=\"center\" width=\"100%\">T&eacute;rmino</div>',
				hideLabel: true
			},
			{	
				xtype: 'displayfield',
				value: '',
				colspan : 5
			},
			{	
				xtype: 'displayfield',
				value: ''
			},
			{
				xtype: 'displayfield',
				id	:'fecha_reg_inicio',
				html :'<div align=\"center\" width=\"100%\">'+Ext.util.Format.date(Ext.getCmp('txtFechaPeriodoMin').getValue(),'d/M/Y')+'</div>',
				hideLabel: true
			},
			{	
				xtype: 'displayfield',
				value: ''
			},
			{
				xtype: 'displayfield',
				id	: 'fecha_reg_fin',
				html :'<div align=\"center\" width=\"100%\">'+Ext.util.Format.date(Ext.getCmp('txtFechaPeriodoMax').getValue(),'d/M/Y')+'</div>',
				hideLabel: true
			},
			{	
				xtype: 'displayfield',
				value: '',
				colspan : 5
			},
			{	
				xtype: 'displayfield',
				value: '<br>'//,
				
			}
		]
  });


  var columnHeaderGird1 = new Ext.ux.grid.ColumnHeaderGroup({
	  rows: [
		 [
			{header: 'Intermediario Financiero', colspan: 1, align: 'center'},
			{header: 'Documentos Operados', colspan: 3, align: 'center'}
		 ]
	  ]
	});

	var girdConsulta_01 = new Ext.grid.EditorGridPanel({
		id:'grid_01',
		width :580, 
		autoHeight: 	true,
		store:grid1StoreData,
		margins:'20 0 0 0',		
		style	:'margin:0 auto;',
		title	:'Detalle Documentos Operados por Intermediario Financiero',	
		plugins: columnHeaderGird1,
		hidden:true,
		stripeRows: 	true,
		loadMask: 		true,		
		columns :[
			{
				//header:'',
				tooltip:'Intermediario Financiero',
				dataIndex	:'COL0',
				width			:250,			
				resizable	:true,
				hideable 	: false,
				align			:'left'
			},
			{
				header:'Moneda',
				dataIndex	:'COL1',
				width			:120,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},
			{
				header:'N�mero',
				dataIndex	:'COL2',
				width			:80,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},
			{
				header:'Monto',
				dataIndex	:'COL3',
				sortable	:true,
				hideable 	: false,
				width			:120,			
				resizable	:true,
				align			:'center',
				renderer:function(value){
					return value;
					}
			}
		]
  });
   var columnHeaderGird2 = new Ext.ux.grid.ColumnHeaderGroup({
	  rows: [
		 [
			{header: 'Fecha de<br>Vencimiento', colspan: 1, align: 'center'},
			{header: 'Hasta '+txt_dias1+' D�as Inclusive', colspan: 2, align: 'center',id:'head1'},
			{header: 'De los '+(txt_dias1+1)+' a los '+txt_dias2+' D�as', colspan: 2, align: 'center'},
			{header: 'Total No Operados', colspan: 2, align: 'center'}
		 ]
	  ]
	});
	var gridDetalleDoctoOperMN = new Ext.grid.GridPanel({
		id:'gridDetalleDoctoOperMN',
		width :700,
		//autoHeight: 	true,
		height : 300,
		store	: gridDetalleDoctoOperMNStoreData,
		margins:'20 0 0 0',		
		style	:'margin:0 auto;',
		title	:'Detalle Documentos no Operados M.N. por periodo de Vigencia*',	
		plugins: columnHeaderGird2,
		hidden:true,
		stripeRows: 	true,
		loadMask: 		true,
		viewConfig : {
			//forceFit:true,
			autoFill: 		true,
			scrollOffset:	0
		},
		columns :[
			{
				//header:'',
				tooltip:'Fecha de Vencimiento',
				dataIndex	:'COL0',
				width			:150,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL1',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL2',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL3',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL4',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL5',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL6',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			}
		]
	});
	var gridDetalleDoctoOperMNTotales = new Ext.grid.GridPanel({
		id:'gridDetalleDoctoOperMNTotales',
		width :700,
		autoHeight: 	true,
		hideHeaders  : true,
		store	: gridDetalleDoctoOperMNStoreDataTotales,
		margins:'20 0 0 0',		
		style	:'margin:0 auto;',
		hidden:true,
		stripeRows: 	true,
		loadMask: 		true,
		viewConfig : {
			//forceFit:true,
			autoFill: 		true,
			scrollOffset:	0
		},
		columns :[
			{
				//header:'',
				tooltip:'Fecha de Vencimiento',
				dataIndex	:'COL0',
				width			:150,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL1',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL2',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL3',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL4',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL5',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL6',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			}
		]
	});
var columnHeaderGirdUSD = new Ext.ux.grid.ColumnHeaderGroup({
	  rows: [
		 [
			{header: 'Fecha de<br>Vencimiento', colspan: 1, align: 'center'},
			{header: 'Hasta '+txt_dias1+' D�as Inclusive', colspan: 2, align: 'center'},
			{header: 'De los '+(txt_dias1+1)+' a los '+txt_dias2+' D�as', colspan: 2, align: 'center'},
			{header: 'Total No Operados', colspan: 2, align: 'center'}
		 ]
	  ]
	});
	var gridDetalleDoctoOperUSD = new Ext.grid.GridPanel({
		id:'gridDetalleDoctoOperUSD',
		width :700,
		//autoHeight: 	true,
		height : 300,
		store	: gridDetalleDoctoOperUSDStoreData,
		margins:'20 0 0 0',		
		style	:'margin:0 auto;',
		title	:'Detalle Documentos no Operados USD por periodo de Vigencia*',	
		plugins: columnHeaderGirdUSD,
		hidden:true,
		stripeRows: 	true,
		loadMask: 		true,
		viewConfig : {
			//forceFit:true,
			autoFill: 		true,
			scrollOffset:	0
		},
		columns :[
			{
				//header:'',
				tooltip:'Fecha de Vencimiento',
				dataIndex	:'COL0',
				width			:150,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL1',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL2',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL3',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL4',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL5',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL6',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			}
		]
	});
	var gridDetalleDoctoOperUSDTotales = new Ext.grid.GridPanel({
		id:'gridDetalleDoctoOperUSDTotales',
		width :700,
		autoHeight: 	true,
		hideHeaders  : true,
		store	: gridDetalleDoctoOperUSDStoreDataTotales,
		margins:'20 0 0 0',		
		style	:'margin:0 auto;',
		hidden:true,
		stripeRows: 	true,
		loadMask: 		true,
		viewConfig : {
			//forceFit:true,
			autoFill: 		true,
			scrollOffset:	0
		},
		columns :[
			{
				//header:'',
				tooltip:'Fecha de Vencimiento',
				dataIndex	:'COL0',
				width			:150,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL1',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL2',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL3',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL4',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			},{
				header:'N�mero de<br>Proveedores',
				tooltip:'N�mero de Proveedores',
				dataIndex	:'COL5',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'center'
			},{
				header:'Monto',
				tooltip:'Monto',
				dataIndex	:'COL6',
				width			:100,			
				resizable	:true,
				hideable 	: false,
				align			:'right',
				renderer: function(value){
					return value;
				}
			}
		]
	});
		var mesajePie =new Ext.Container ({
		layout: 'table',
		id: 'panelTablaMensaje',
		width: 700,
		heigth: 'auto',
		style: 'margin: 0 auto',
		bodyStyle:'padding:10px',
		hidden:true,
		layoutConfig: {
			columns: 9
		},
		items: [	
			{
				xtype: 'displayfield',
				value :'<div align=\"justify\" width=\"100%\">* Para determinar el per&iacute;odo de vigencia de los documentos, se calcular&aacute; la diferiencia en d&iacute;as entre la fecha de vencimiento y la fecha de alta de los mismos en Cadenas Productivas. En el caso de existir m&aacute;s de un documento con la misma fecha de vencimiento para el mismo proveedor, se tomar&aacute; como periodo de vigencia el m&iacute;nimo del grupo de documentos.</div><br><br>',
				width: 650,
				hideLabel: true,
				colspan: 9
				
			},
			{
				xtype: 'displayfield',
				id: 'uusarioElaboro',
				value :'<div align=\"center\" width=\"100%\">Elabor&oacute;:&nbsp;<u><b>strNombreUsuario</b></div><br><br>',
				hideLabel: true,
				colspan: 9
			},
			{	
				xtype: 'displayfield',
				value: 'hola',
				value :'<div align=\"center\" width=\"600px\">&nbsp;</div>',
				colspan: 6
			},
			{
				xtype: 'button',
				text:'Generar Detalle',
				tooltip: 'Generar Detalle',
				id: 'btnGenerarDetalle',
				iconCls: 'icoXls',
				handler: function(boton, evento){							
					Ext.Ajax.request({
						url: _URL_,
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Generar_Archivo_Detalle'
						}),
						callback: descargaArchivo
					});
				}
			},
			{
				xtype: 'button',
				text:'Generar Archivo',
				tooltip: 'Generar Archivo',
				id: 'btnGenerarArchivo',
				iconCls: 'icoXls',
				handler: function(boton, evento){	
					var epo = Ext.getCmp('cboEpo').getRawValue();
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: _URL_,
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Generar_Archivo',
						esTarifaEnMonedaNacionalValida		:	esTarifaEnMonedaNacionalValida,
						esTarifaEnDolaresAmericanosValida	:	esTarifaEnDolaresAmericanosValida,
						txt_dias1									:	txt_dias1,
						txt_dias2									:	txt_dias2,
						txt_dias1USD								:	txt_dias1USD,
						txt_dias2USD								:	txt_dias2USD,
						nombreEPO									:	epo
						}),success:function(){
								boton.enable();
								boton.setIconClass('icoXls');
							},
						callback: descargaArchivo
					});
				}
			},
			{
				xtype: 'button',
				text:'Imprimir',
				tooltip: 'Imprimir',
				id: 'btnImprimir',
				iconCls: 'icoPdf',
				handler: function(boton, evento){							
					var epo = Ext.getCmp('cboEpo').getRawValue();
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: _URL_,
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Generar_Archivo',
						esTarifaEnMonedaNacionalValida		:	esTarifaEnMonedaNacionalValida,
						esTarifaEnDolaresAmericanosValida	:	esTarifaEnDolaresAmericanosValida,
						txt_dias1									:	txt_dias1,
						txt_dias2									:	txt_dias2,
						txt_dias1USD								:	txt_dias1USD,
						txt_dias2USD								:	txt_dias2USD,
						nombreEPO									:	epo,
						tipo											:	'PDF'
						}),success:function(){
								boton.enable();
								boton.setIconClass('icoPdf');
							},
						callback: descargaArchivo
					});
				}
			}
		]
  });

	//--------------------------------------- CONTENEDOR PRINCIPAL ---------------------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: 	[
			fp	,NE.util.getEspaciador(10)	,contenedor//,tablaTitulosGrid1
			//,NE.util.getEspaciador(10)	
			,girdConsulta_01
			,NE.util.getEspaciador(10)	
			,gridDetalleDoctoOperMN,gridDetalleDoctoOperMNTotales
			,NE.util.getEspaciador(10)	
			,gridDetalleDoctoOperUSD,gridDetalleDoctoOperUSDTotales
			,NE.util.getEspaciador(10)	
			,gridDetalleDoctoOperUSD,gridDetalleDoctoOperUSDTotales
			,NE.util.getEspaciador(5)	
			,mesajePie
		],
		columns:[
			{}
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------
	catalogoEPOData.load();
});