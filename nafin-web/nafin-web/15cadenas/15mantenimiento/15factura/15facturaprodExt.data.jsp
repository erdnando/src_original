<%@ page contentType="application/json;charset=UTF-8" 
	import="
		java.util.*,
		java.text.*,	
		netropology.utilerias.*,	
		com.netro.cadenas.*,
		com.netro.dispersion.*, 
		com.netro.model.catalogos.*,	
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		javax.naming.Context,
		org.apache.commons.logging.Log"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");

String cmb_mesini	= (request.getParameter("cmb_mesini")		== null)?"":request.getParameter("cmb_mesini");
String cmb_anyoini	= (request.getParameter("cmb_anyoini")		== null)?"":request.getParameter("cmb_anyoini");

String cmb_mesfin	= (request.getParameter("cmb_mesfin")		== null)?"":request.getParameter("cmb_mesfin");
String cmb_anyofin	= (request.getParameter("cmb_anyofin")		== null)?"":request.getParameter("cmb_anyofin");

String tipo_factura	= (request.getParameter("tipo_factura")		== null)?"":request.getParameter("tipo_factura");
String ic_producto_nafin	= (request.getParameter("ic_producto_nafin")		== null)?"":request.getParameter("ic_producto_nafin");
String ic_epo	= (request.getParameter("ic_epo")		== null)?"":request.getParameter("ic_epo");
String ic_if	= (request.getParameter("ic_if")		== null)?"":request.getParameter("ic_if");
String cmb_factura	= (request.getParameter("cmb_factura")		== null)?"":request.getParameter("cmb_factura");
String nombre_pyme	= (request.getParameter("nombre_pyme")		== null)?"":request.getParameter("nombre_pyme");
String rfc_prov	= (request.getParameter("rfc_prov")		== null)?"":request.getParameter("rfc_prov");
String numProveedor	= (request.getParameter("numProveedor")		== null)?"":request.getParameter("numProveedor");

String txt_nafelec	= (request.getParameter("txt_nafelec")		== null)?"":request.getParameter("txt_nafelec");
String operacion	= (request.getParameter("operacion")		== null)?"":request.getParameter("operacion");
String cg_usuario			= (request.getParameter("cg_usuario")==null)?"":request.getParameter("cg_usuario");
String nomBoton			= (request.getParameter("nomBoton")==null)?"":request.getParameter("nomBoton");

String numeroEnLetra			= (request.getParameter("numeroEnLetra")==null)?"":request.getParameter("numeroEnLetra");
String tituloGridC			= (request.getParameter("tituloGridC")==null)?"":request.getParameter("tituloGridC");

String infoRegresar	= "", mensaje ="",  consulta ="",fecha_inicio  ="", fecha_fin ="";
String	meses[] 	= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	
 Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
 
String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String	diaHoy	= fechaHoy.substring(0, 2);	
String	mesHoy	= fechaHoy.substring(3, 5);
String	anyoHoy	= fechaHoy.substring(6, 10);

HashMap info = new HashMap();
JSONArray regis = new JSONArray();
JSONObject jsonObj = new JSONObject();
int  start= 0, limit =0;
ConFacturas  pag  =  new ConFacturas();


//Rutina para obtener iniciales del usuario
	StringBuffer sbIniciales = new StringBuffer();
	if(cg_usuario.equals("")) {
		List lUsuario = Comunes.explode(" ", strNombreUsuario);
		for(int i=0; i<lUsuario.size(); i++){
		 	cg_usuario= (String)lUsuario.get(i);
			cg_usuario=cg_usuario.substring(0,1);
			sbIniciales.append(cg_usuario);
		}
	} else {
		sbIniciales.append(cg_usuario);
	}	
if ( informacion.equals("valoresIniciales") )	{	

jsonObj.put("success", new Boolean(true));
jsonObj.put("mesHoy", mesHoy);	
jsonObj.put("anyoHoy", anyoHoy);	
jsonObj.put("fechaHoy", fechaHoy); 
jsonObj.put("sbIniciales", sbIniciales.toString()); 
infoRegresar = jsonObj.toString();	

}else if ( informacion.equals("catalogoAnio") )	{
	
	int		comboAnyo[]	= new int[5];
	for(int a=0;a<5;a++) {
		comboAnyo[a] = Integer.parseInt(anyoHoy) - a;
	}		
	for(int i=0;i<5;i++) {
		String valAnyo = new Integer(comboAnyo[i]).toString();
		
		info = new HashMap();
		info.put("clave", valAnyo);
		info.put("descripcion", valAnyo );
		regis.add(info);

	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + regis.size() + "\", \"registros\": " + regis.toString()+"}";

}else  if ( informacion.equals("catalogoProducto") )	{

	String valor = "1,4"; 
	if(tipo_factura.equals("C")) {  valor ="4"; }
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_producto_nafin");
	catalogo.setCampoDescripcion("IC_NOMBRE");
	catalogo.setTabla("COMCAT_PRODUCTO_NAFIN");		
	catalogo.setOrden("ic_producto_nafin");
	catalogo.setValoresCondicionIn(valor, Integer.class);
	infoRegresar = catalogo.getJSONElementos();	

}else  if ( informacion.equals("catalogoMoneda") )	{
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	

}else  if ( informacion.equals("catalogoIVA") )	{
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("fn_porcentaje_iva");
	catalogo.setCampoDescripcion("fn_porcentaje_iva");
	catalogo.setTabla("comcat_iva");		
	catalogo.setOrden("fn_porcentaje_iva");	
	infoRegresar = catalogo.getJSONElementos();	


}else  if ( informacion.equals("catalogoEPOxProd") )	{

	CatalogoEPOxProd catalogo = new CatalogoEPOxProd();
	catalogo.setClave("e.ic_epo");
	catalogo.setDescripcion("e.cg_razon_social");
	catalogo.setProducto(ic_producto_nafin);	
	catalogo.setIntermediario("");	
	catalogo.setPyme("");	
	catalogo.setOrden("e.cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();		


}else  if ( informacion.equals("catalogoIF") )	{
 
   CatalogoIF catalogo = new CatalogoIF(); 
	catalogo.setClave("ic_if");
	catalogo.setDescripcion("cg_razon_social");
	catalogo.setClaveEpo(ic_epo);
	catalogo.setG_producto(ic_producto_nafin);
	catalogo.setG_piso("1"); 
	catalogo.setEstatusAfiliacion("S");  
   catalogo.setOrden("cg_razon_social");
	
	infoRegresar = catalogo.getJSONElementos(); 


}else  if ( informacion.equals("catalogoPyme") )	{


	List datos = pag.getCatalogobusqueda(  nombre_pyme,   rfc_prov,   numProveedor, ic_epo , ic_producto_nafin  ); 
	
	regis = regis.fromObject(datos);

	infoRegresar =  "{\"success\": true, \"total\": \"" + regis.size() + "\", \"registros\": " + regis.toString()+"}";
	
}else  if ( informacion.equals("NombrePyme") )	{

	String nombrePyme ="";
	List datos =  pag.getBusquedaPyme( txt_nafelec ); 
	if(datos.size()>0){
		nombrePyme = datos.get(1).toString();
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("nombrePyme", nombrePyme);		
	infoRegresar = jsonObj.toString();
 

}else  if ( informacion.equals("Consultar") ||  informacion.equals("Resumen_Facturas")  
        ||  informacion.equals("FacturasGeneral") ||   informacion.equals("GenerarTodas_Facturas")   || informacion.equals("GenerarTodas_Facturas_TXT")   )	{
	
	String rs_ic_pyme ="", txt_iniPeriodo ="", txt_finPeriodo ="",  title ="", mesAnio ="", mesFactura = "", anyoFactura = "",  ic_pyme ="";
	Fecha fecha = new Fecha();
	Calendar mesAnyoFinal 	= new GregorianCalendar();
	Calendar mesAnyoInicial		= new GregorianCalendar();
	int diaMaxMes   =0;  
	if(!txt_nafelec.equals("")){
		List datos = pag.getBusquedaPyme( txt_nafelec ); 
		ic_pyme = datos.get(0).toString();
	}
	
	if(!cmb_mesini.equals("") &&  !cmb_anyoini.equals("")  ){	//Fecha Inicial	
		mesAnyoInicial.set(Integer.parseInt(cmb_anyoini), Integer.parseInt(cmb_mesini)-1, 1);
		fecha_inicio = new java.text.SimpleDateFormat("dd/MM/yyyy").format(mesAnyoInicial.getTime());
				
		//Fecha Final
		mesAnyoFinal.set(Integer.parseInt(cmb_anyofin), Integer.parseInt(cmb_mesfin)-1, 1);
		diaMaxMes = mesAnyoFinal.getActualMaximum(Calendar.DAY_OF_MONTH);
		fecha_fin = diaMaxMes+"/"+cmb_mesfin+"/"+cmb_anyofin;
	}	
	pag.setIc_epo (ic_epo);
	pag.setIc_if (ic_if);
	pag.setTxt_nafelec (txt_nafelec);
	pag.setTipo_factura (tipo_factura);
	pag.setFecha_seleccion_de (fecha_inicio);
	pag.setFecha_seleccion_a(fecha_fin);
	pag.setIc_producto_nafin (ic_producto_nafin);
	pag.setIc_pyme (ic_pyme);	
	pag.setInformacion (informacion);
	pag.setCmb_factura (cmb_factura);
				
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pag);
		
	if(informacion.equals("Consultar") && tipo_factura.equals("T")  ) {	
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
							
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
				
			Registros registros = queryHelper.getPageResultSet(request,start,limit);
			while(registros.next()){
				rs_ic_pyme =registros.getString("ic_pyme");	
				mesAnio =registros.getString("MES_ANIO");	 
			}
		
		 consulta	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
	   	
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		String fechaInicio ="";		
		if(!mesAnio.equals("") ) { 
			 mesFactura = mesAnio.substring(0,2);
			 anyoFactura = mesAnio.substring(3,7);		
			fechaInicio ="01/"+mesFactura+"/"+anyoFactura; 
		   fecha_fin  =  fecha.getUltimoDiaDelMes(fechaInicio, "dd/MM/yyyy");			
			//String dia_fin  =  fecha.getUltimoDiaDelMes(fechaInicio, "dd");	
			String dia_fin  =  fecha_fin.substring(0,2);
                        title ="México D.F. a "+dia_fin+" de "+meses[Integer.parseInt(mesFactura)-1]+" del "+anyoFactura; 
		}
				
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("fecha_inicio", fechaInicio);
		jsonObj.put("fecha_fin", fecha_fin);
		jsonObj.put("rs_ic_pyme", rs_ic_pyme);
		jsonObj.put("title", title);
		
	}else  if(informacion.equals("Consultar") && tipo_factura.equals("C")  ) {	
	
		Registros reg	=	queryHelper.doSearch();
		while(reg.next()){
			mesAnio =reg.getString("MES_ANIO");	 
		}		
			
		if(!mesAnio.equals("") ) { 
			 mesFactura = mesAnio.substring(0,2);
			 anyoFactura = mesAnio.substring(3,7);	
			 String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			  String diaActual    = fechaActual.substring(0,2);			
			title ="México D.F. a "+diaActual+" de "+meses[Integer.parseInt(mesFactura)-1]+" del "+anyoFactura; 
		}
		
		
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);	
		jsonObj.put("title", title);
		infoRegresar = jsonObj.toString(); 
	
	
	}else  if ( informacion.equals("Resumen_Facturas") )	{

		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	
				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}		
		jsonObj.put("nomBoton", nomBoton);		
		
	}else  if ( informacion.equals("FacturasGeneral") )	{
	
		String datosPyme			= (request.getParameter("datosPyme")==null)?"":request.getParameter("datosPyme");
		String datosTotales[] = request.getParameterValues("datosTotales");	
							
		try {
				String nombreArchivo = pag.facturasGeneralPDF(request,  strDirectorioTemp, datosPyme , datosTotales,  tituloGridC , numeroEnLetra);
			  
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}		
			jsonObj.put("nomBoton", nomBoton);	
			
	}else  if ( informacion.equals("GenerarTodas_Facturas") )	{ 
		try {
			
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	
				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}		
		jsonObj.put("nomBoton", nomBoton);	
	
	
	}else  if ( informacion.equals("GenerarTodas_Facturas_TXT") )	{  
		try {
			
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "TXT");	
				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}		
		jsonObj.put("nomBoton", nomBoton);	
		
	}
	
	infoRegresar = jsonObj.toString();	
	
}else  if ( informacion.equals("constTotales") )	{
	  
	HashMap datos  = new HashMap();
	JSONArray registros = new JSONArray();	
	double totalMontoOperado = 0;
	double totalInteresCobrado = 0;
	double totalMontoOperadoSirac = 0;
	double totalInteresCobradoSirac = 0;
	consulta ="";
	String rs_montoOperado		= "",rs_tasa= "",  rs_interesCobrado	= "";
	Vector regTotales = new Vector();
	
	registros = new JSONArray();	
	regTotales = new Vector();		
	
	if(  tipo_factura.equals("T")  ) {
	
		fecha_fin	= (request.getParameter("fecha_fin")		== null)?"":request.getParameter("fecha_fin");
		fecha_inicio	= (request.getParameter("fecha_inicio")		== null)?"":request.getParameter("fecha_inicio");
		String rs_ic_pyme	= (request.getParameter("rs_ic_pyme")		== null)?"":request.getParameter("rs_ic_pyme");

		regTotales = dispersion.getDatosFactura(rs_ic_pyme,fecha_inicio,fecha_fin,ic_producto_nafin,ic_epo,ic_if,"N");
		 
			totalMontoOperado =0;
			totalInteresCobrado =0;
									
		for(int r=0;r<regTotales.size();r++) {
			Vector  renglones = (Vector)regTotales.get(r);
			rs_montoOperado		= (String)renglones.get(0);
			rs_tasa				= (String)renglones.get(1);
			rs_interesCobrado	= (String)renglones.get(2);
			
			totalMontoOperado += Double.parseDouble(rs_montoOperado);
			totalInteresCobrado += Double.parseDouble(rs_interesCobrado);
									
			datos  = new HashMap();
			datos.put("TIPO_DATO","");
			datos.put("DESCRIPCION","");
			datos.put("MONTO_OPERADO",rs_montoOperado);
			datos.put("VALOR_TASA",rs_tasa);
			datos.put("INTERES_COBRADO",rs_interesCobrado);
			registros.add(datos);	
		}
		datos  = new HashMap();
		datos.put("TIPO_DATO","");
		datos.put("DESCRIPCION","TOTAL OPERADO");
		datos.put("MONTO_OPERADO",Double.toString (totalMontoOperado));
		datos.put("VALOR_TASA","");
		datos.put("INTERES_COBRADO",Double.toString (totalInteresCobrado));
		registros.add(datos);
		
		
		datos  = new HashMap();
		datos.put("TIPO_DATO","");
		datos.put("DESCRIPCION","IVA");
		datos.put("MONTO_OPERADO","0");
		datos.put("VALOR_TASA","");
		datos.put("INTERES_COBRADO","0");
		registros.add(datos);
		
		datos  = new HashMap();
		datos.put("TIPO_DATO","");
		datos.put("DESCRIPCION","GRAN TOTAL");
		datos.put("MONTO_OPERADO",Double.toString (totalMontoOperado));
		datos.put("VALOR_TASA","");
		datos.put("INTERES_COBRADO",Double.toString (totalInteresCobrado));
		registros.add(datos);
		
		numeroEnLetra = ConvierteCadena.numeroALetra(totalInteresCobrado);
		String centavos = Comunes.formatoDecimal(new Double(totalInteresCobrado).toString(),2,false);
		int indexOfDot = centavos.indexOf('.');
		centavos = centavos.substring(indexOfDot+1,centavos.length());
		numeroEnLetra = "("+numeroEnLetra+" PESOS "+centavos+"/100 M.N.)";
					
				
	}else  if(  tipo_factura.equals("C")  ) {
	
	String  ic_moneda	= (request.getParameter("ic_moneda")		== null)?"":request.getParameter("ic_moneda");
	String fn_monto_autorizado	= (request.getParameter("fn_monto_autorizado")		== null)?"":request.getParameter("fn_monto_autorizado");
	String fn_tasa_comision	= (request.getParameter("fn_tasa_comision")		== null)?"":request.getParameter("fn_tasa_comision");
	String fn_porcentaje_iva	= (request.getParameter("fn_porcentaje_iva")		== null)?"":request.getParameter("fn_porcentaje_iva");
	double totalAutorizado = 0;
	double monto_iva = 0;
	
	System.out.println("fn_monto_autorizado === "+fn_monto_autorizado); 
	System.out.println("fn_tasa_comision === "+fn_tasa_comision); 

	
	totalAutorizado= ( Double.parseDouble(fn_monto_autorizado) * Double.parseDouble(fn_tasa_comision))/100;
	monto_iva=(Double.parseDouble(fn_porcentaje_iva) * totalAutorizado)/100;
	
	double granTotal = monto_iva + totalAutorizado;
	
	System.out.println("totalAutorizado === "+totalAutorizado); 
						
	datos  = new HashMap();
	datos.put("TIPO_DATO","");
	datos.put("DESCRIPCION","");
	datos.put("MONTO_OPERADO",fn_monto_autorizado);
	datos.put("VALOR_TASA",fn_tasa_comision);
	datos.put("INTERES_COBRADO",Double.toString (totalAutorizado) );
	registros.add(datos);	
	
	datos  = new HashMap();
	datos.put("TIPO_DATO","LETRA");
	datos.put("DESCRIPCION","");
	datos.put("MONTO_OPERADO","");
	datos.put("VALOR_TASA","IVA");
	datos.put("INTERES_COBRADO",Double.toString (monto_iva) );
	registros.add(datos);
	
	datos  = new HashMap();
	datos.put("TIPO_DATO","LETRA");
	datos.put("DESCRIPCION","");
	datos.put("MONTO_OPERADO","");
	datos.put("VALOR_TASA","GRAN TOTAL ");
	datos.put("INTERES_COBRADO",Double.toString (granTotal) );
	registros.add(datos);
	
	
	numeroEnLetra = ConvierteCadena.numeroALetra(granTotal);
	String centavos = Comunes.formatoDecimal(new Double(granTotal).toString(),2,false);
	int indexOfDot = centavos.indexOf('.');
	centavos = centavos.substring(indexOfDot+1,centavos.length());
	String auxNumLetra1 = " PESOS ";
	String auxNumLetra2 = "M.N.";
	if("54".equals(ic_moneda)){
		auxNumLetra1 = "  ";
		auxNumLetra2 = "U.S.D.";
	}
	numeroEnLetra = "("+numeroEnLetra+auxNumLetra1+centavos+"/100 "+auxNumLetra2+")";
								
	}		
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("relleno", numeroEnLetra);
	jsonObj.put("lb_cg_usuario", cg_usuario);
	
	infoRegresar = jsonObj.toString(); 

}

%>

<%=infoRegresar %>