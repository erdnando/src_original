Ext.onReady(function() {

	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
					
			var infoR = Ext.JSON.decode(response.responseText);
			
			if(infoR.nomBoton=='btnTodasTXT') {
				Ext.ComponentQuery.query('#btnTodasTXT')[0].enable();			
				Ext.ComponentQuery.query('#btnTodasTXT')[0].setIconCls('icoTxt');
			}else if(infoR.nomBoton=='btnResumen') {
				Ext.ComponentQuery.query('#btnResumen')[0].enable();			
				Ext.ComponentQuery.query('#btnResumen')[0].setIconCls('icoBotonXLS');
			}else if(infoR.nomBoton=='btnGenerarFac') {			
				Ext.ComponentQuery.query('#btnGenerarFac')[0].enable();
				Ext.ComponentQuery.query('#btnGenerarFac')[0].setIconCls('icoPdf');
			}else if(infoR.nomBoton=='btnGenerarTodas') {			
				Ext.ComponentQuery.query('#btnGenerarTodas')[0].enable();
				Ext.ComponentQuery.query('#btnGenerarTodas')[0].setIconCls('icoPdf');
			}
			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();							
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var datosPyme;
	var datosTotales = [];	
	
	var cancelar =  function() { 
		datosPyme;
		datosTotales = [];	
	}
	
	var  panelBotones = Ext.create( 'Ext.form.Panel', {      
      itemId: 'panelBotones',
      frame: false,
		border: false,
      bodyPadding: '12 6 12 6',
      width: 690,
      style: 'margin: 0px auto 0px auto;',
		hidden: true, 
		items:[	
			{
				xtype: 'displayfield',
				fieldLabel: '',
				itemId: 'lb_cg_usuario1',
				name: 'lb_cg_usuario',			
				width: 300,	
				hidden:true,
				maxLength: 20
			},	
			{
				xtype: 'button',
				text: 'Generar Todas las Facturas en TXT',			
				iconCls: 'icoTxt',
				id: 'btnTodasTXT',				
				handler: function(boton, evento) {
						
					Ext.ComponentQuery.query('#btnTodasTXT')[0].disable();
					Ext.ComponentQuery.query('#btnTodasTXT')[0].setIconCls('x-mask-msg-text');							
					var  forma =   Ext.ComponentQuery.query('#forma')[0];
					
					Ext.Ajax.request({
						url: '15facturaprodExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{					
							nomBoton:'btnTodasTXT',
							informacion:'GenerarTodas_Facturas_TXT',
							cmb_mesini:forma.query('#cmb_mesini1')[0].getValue(),
							cmb_anyoini:forma.query('#cmb_anyoini1')[0].getValue(),
							cmb_mesfin:forma.query('#cmb_mesfin1')[0].getValue(),
							cmb_anyofin:forma.query('#cmb_anyofin1')[0].getValue(),			
							ic_producto_nafin:forma.query('#ic_producto_nafin1')[0].getValue(),
							ic_epo:forma.query('#ic_epo1')[0].getValue(),
							ic_if:forma.query('#ic_if1')[0].getValue(),
							cmb_factura:forma.query('#cmb_factura1')[0].getValue(),
							txt_nafelec:forma.query('#txt_nafelec1')[0].getValue(),
							tipo_factura: forma.query('#tipo_factura_C')[0].getValue()
						}),
						callback: procesarDescargaArchivos
					});
				}
			},
			{
				xtype: 'button',
				text: 'Resumen de Facturas',			
				iconCls: 'icoBotonXLS',
				id: 'btnResumen',				
				handler: function(boton, evento) {
				
					Ext.ComponentQuery.query('#btnResumen')[0].disable();
					Ext.ComponentQuery.query('#btnResumen')[0].setIconCls('x-mask-msg-text');
					var  fp =   Ext.ComponentQuery.query('#forma')[0];		
					
					Ext.Ajax.request({
						url: '15facturaprodExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							nomBoton:'btnResumen',
							informacion:'Resumen_Facturas',
							tipo_factura: Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()
						}),
						callback: procesarDescargaArchivos
					});					
				}
			},
			{
				xtype: 'button',
				text: 'Generar Factura',			
				iconCls: 'icoPdf',
				id: 'btnGenerarFac',				
				handler: function(boton, evento) {
				
				//	Ext.ComponentQuery.query('#btnGenerarFac')[0].disable();
				//	Ext.ComponentQuery.query('#btnGenerarFac')[0].setIconCls('x-mask-msg-text');
					var  fp =   Ext.ComponentQuery.query('#forma')[0];			
					
					cancelar(); //limpia valores 
					
					//para el encabezado
					var gridConsulta =  Ext.ComponentQuery.query('#gridConsulta')[0];		
					var storeC = gridConsulta.getStore();	
					storeC.each(function(record) {  			
						datosPyme=record.data['CG_RAZON_SOCIAL']+'|'+record.data['NO_CLIENTE'] +'|'+record.data['DOMICILIO'] +'|'+record.data['RFC'] +'|'+record.data['MES_ANIO'] ;
					});
					
					if(Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()=='C') {   // Comisiones  
						//para el totales
						var gridTotalesC =  Ext.ComponentQuery.query('#gridTotalesC')[0];		
						var storeT = gridTotalesC.getStore();	
						storeT.each(function(record) {  			
							datosTotales.push(record.data['DESCRIPCION']+'|'+record.data['MONTO_OPERADO']+'|'+record.data['VALOR_TASA']+'|'+record.data['INTERES_COBRADO'] +'|'+record.data['TIPO_DATO']    );					
						});
					
					}else  if(Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()=='T') {   // Transacciones  
						//para el totales
						var gridTotales =  Ext.ComponentQuery.query('#gridTotales')[0];		
						var storeT = gridTotales.getStore();	
						storeT.each(function(record) {  			
							datosTotales.push(record.data['DESCRIPCION']+'|'+record.data['MONTO_OPERADO']+'|'+record.data['VALOR_TASA']+'|'+record.data['INTERES_COBRADO'] +'|'+record.data['TIPO_DATO']    );					
						});
					
					}
					
					
					Ext.Ajax.request({
						url: '15facturaprodExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{					
							nomBoton:'btnGenerarFac',
							informacion:'FacturasGeneral',
							datosPyme:datosPyme,
							datosTotales:datosTotales,
							tipo_factura:Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue(),
							numeroEnLetra: Ext.ComponentQuery.query('#numeroEnLetra')[0].getValue(),
							tituloGridC: Ext.ComponentQuery.query('#tituloGridC')[0].getValue()
						}),
						callback: procesarDescargaArchivos
					});										
				}
			},
			{
				xtype: 'button',
				text: 'Generar Todas las Facturas',			
				iconCls: 'icoPdf',
				id: 'btnGenerarTodas',				
				handler: function(boton, evento) {
					
					Ext.ComponentQuery.query('#btnGenerarTodas')[0].disable();
					Ext.ComponentQuery.query('#btnGenerarTodas')[0].setIconCls('x-mask-msg-text');
					var  forma =   Ext.ComponentQuery.query('#forma')[0];
					
					Ext.Ajax.request({
						url: '15facturaprodExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{					
							nomBoton:'btnGenerarTodas',
							informacion:'GenerarTodas_Facturas',
							cmb_mesini:forma.query('#cmb_mesini1')[0].getValue(),
							cmb_anyoini:forma.query('#cmb_anyoini1')[0].getValue(),
							cmb_mesfin:forma.query('#cmb_mesfin1')[0].getValue(),
							cmb_anyofin:forma.query('#cmb_anyofin1')[0].getValue(),			
							ic_producto_nafin:forma.query('#ic_producto_nafin1')[0].getValue(),
							ic_epo:forma.query('#ic_epo1')[0].getValue(),
							ic_if:forma.query('#ic_if1')[0].getValue(),
							cmb_factura:forma.query('#cmb_factura1')[0].getValue(),
							txt_nafelec:forma.query('#txt_nafelec1')[0].getValue(),
							tipo_factura: Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()
						}),
						callback: procesarDescargaArchivos
					});
				}
			},
			{
				xtype: 'button',
				text: 'Salir',			
				iconCls: 'icoLimpiar',
				id: 'btnSalir',
				hidden:true,
				handler: function(boton, evento) {
					window.location  = "15facturaprodExt.jsp"; 					
				}
			}			
		]
	});
	
	
	//Funci�n para deshabilitar o habilitar columnas 
	function findColumnByDataIndex(grid, dataIndex) {
		var selector = "gridcolumn[dataIndex=" + dataIndex + "]";
		return grid.down(selector);
	};
	
	
		var procesarConsTotales = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			var  fp =   Ext.ComponentQuery.query('#forma')[0];
			main.el.unmask();
			
			if(store.getTotalCount() > 0) {		
				
				Ext.ComponentQuery.query('#panelBotones')[0].show();					
				
				if(Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()=='C') {   // Comisiones  
				
					Ext.ComponentQuery.query('#gridTotalesC')[0].show();
					Ext.ComponentQuery.query('#numeroEnLetraC')[0].setValue(json.relleno);
															
					Ext.ComponentQuery.query('#btnTodasTXT')[0].hide();		
					Ext.ComponentQuery.query('#btnResumen')[0].hide();						
					Ext.ComponentQuery.query('#btnGenerarTodas')[0].hide();
					Ext.ComponentQuery.query('#btnSalir')[0].show();					
					Ext.ComponentQuery.query('#btnGenerarFac')[0].setText('Imprimir Factura');
					Ext.ComponentQuery.query('#lb_cg_usuario1')[0].show();						
					Ext.ComponentQuery.query('#lb_cg_usuario1')[0].setValue("* "+json.lb_cg_usuario);
					
					Ext.ComponentQuery.query('#gridTotalesC')[0].getView().setAutoScroll(false);
					
				}else  if(Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()=='T') {   //Transacciones
				
					Ext.ComponentQuery.query('#gridTotales')[0].show();	
					Ext.ComponentQuery.query('#numeroEnLetra')[0].setValue(json.relleno);
									
				  	Ext.ComponentQuery.query('#MONTO_OPERADO')[0].setText('MONTO OPERADO');
					Ext.ComponentQuery.query('#VALOR_TASA')[0].setText('TASA');
					Ext.ComponentQuery.query('#INTERES_COBRADO')[0].setText('INTERES COBRADO');
										
					Ext.ComponentQuery.query('#btnTodasTXT')[0].show();		
					Ext.ComponentQuery.query('#btnResumen')[0].show();						
					Ext.ComponentQuery.query('#btnGenerarTodas')[0].show();
					Ext.ComponentQuery.query('#btnSalir')[0].hide();					
					Ext.ComponentQuery.query('#btnGenerarFac')[0].setText('Generar Factura');					
					Ext.ComponentQuery.query('#lb_cg_usuario1')[0].hide();	
					Ext.ComponentQuery.query('#gridTotales')[0].getView().setAutoScroll(false);
				}						
			}else  {				
				Ext.ComponentQuery.query('#gridTotales')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				
			}

		}
	};
		
	Ext.define('LisTotales', {
		extend: 'Ext.data.Model',
		fields: [						
			{ name: 'DESCRIPCION'},
			{ name: 'MONTO_OPERADO'},
			{ name: 'VALOR_TASA'},
			{ name: 'INTERES_COBRADO'},
			{ name: 'TIPO_DATO'}			
		 ]
	});


	var constTotalesData = Ext.create('Ext.data.Store', {
		model: 'LisTotales',
		storeId:'constTotalesData',		
		proxy: {
			type: 'ajax',
			url: '15facturaprodExt.data.jsp',
			reader: 		 { type: 'json',  root: 'registros' 	},
			extraParams: { informacion: 'constTotales' 	},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners:   { exception: NE.util.mostrarProxyAjaxError 	}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsTotales
		}
	});
	
	
	var  gridTotales = Ext.create('Ext.grid.Panel',{		   
		itemId: 'gridTotales',		
		store: constTotalesData, 			
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            '',
		border:           true,
		stripeRows:       true,
		autoScroll:       false,
		hidden:           true,
		frame:            false,	
		width: 500,	
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		selType: 'cellmodel',		
		columns: [
			{
				header: '',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 100,
				resizable: false,
				align: 'left',
				itemId:'DESCRIPCION'
         },	
			{
				header: 'MONTO OPERADO',
				dataIndex: 'MONTO_OPERADO',
				sortable: true,
				width: 130,
				resizable: false,
				align: 'right',
				itemId:'MONTO_OPERADO',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
         },	
			{
				header: 'TASA',
				dataIndex: 'VALOR_TASA',
				sortable: true,
				width: 100,
				resizable: false,
				align: 'center',
				itemId:'VALOR_TASA',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					if(  record.data['TIPO_DATO'] =='LETRA')  { 
						return value;
					}else  {
						return Ext.util.Format.number(value, '0.0000%'); 
					}
				}				
         },
			{
				header: 'INTERES COBRADO',
				dataIndex: 'INTERES_COBRADO',
				sortable: true,
				width: 150, 
				resizable: false,
				align: 'right',
				itemId:'INTERES_COBRADO',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
         }
		],
		stripeRows: true,
		loadMask: true,			
		frame: false,
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'bottom',
			items: [
				{
					xtype: 'displayfield',
					itemId: 'numeroEnLetra'
				}
			]
		}]
	});
	
	var  gridTotalesC = Ext.create('Ext.grid.Panel',{		   
		itemId: 'gridTotalesC',		
		store: constTotalesData, 			
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            '',
		border:           true,
		stripeRows:       true,
		autoScroll:       false,
		hidden:           true,
		frame:            false,	
		width: 410,	
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		selType: 'cellmodel',		
		columns: [		
			{
				header: 'IMPORTE AUTORIZADO',
				dataIndex: 'MONTO_OPERADO',
				sortable: true,
				width: 150,
				resizable: false,
				align: 'right',
				itemId:'MONTO_OPERADO',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
         },	
			{
				header: 'COMISION',
				dataIndex: 'VALOR_TASA',
				sortable: true,
				width: 100,
				resizable: false,
				align: 'center',
				itemId:'VALOR_TASA',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					if(  record.data['TIPO_DATO'] =='LETRA')  { 
						return value;
					}else  {
						return Ext.util.Format.number(value, '0.0000%'); 
					}
				}				
         },
			{
				header: 'IMPORTE',
				dataIndex: 'INTERES_COBRADO',
				sortable: true,
				width: 150, 
				resizable: false,
				align: 'right',
				itemId:'INTERES_COBRADO',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
         }
		],
		stripeRows: true,
		loadMask: true,			
		frame: false,
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'bottom',
			items: [
				{
					xtype: 'displayfield',
					itemId: 'numeroEnLetraC'
				}
			]
		}]
	});
	
	
	
	var procesarConsultar = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			var  fp =   Ext.ComponentQuery.query('#forma')[0];
			main.el.unmask();
			
			Ext.ComponentQuery.query('#gridConsulta')[0].show();			
			
			if(store.getTotalCount() > 0) {				
			   Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(true);				
		      Ext.ComponentQuery.query('#gridConsulta')[0].setTitle(json.title);
				
				Ext.ComponentQuery.query('#tituloGridC')[0].setValue(json.title);
				var  fpC =   Ext.ComponentQuery.query('#formaComisiones')[0];
	
				constTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{							
						informacion: 'constTotales',
						fecha_inicio:json.fecha_inicio,
						fecha_fin:json.fecha_fin,
						rs_ic_pyme:json.rs_ic_pyme,
						tipo_factura: Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue(),
						ic_moneda:fpC.query('#ic_moneda1')[0].getValue(),
						fn_monto_autorizado:fpC.query('#fn_monto_autorizado1')[0].getValue(),
						fn_tasa_comision:fpC.query('#fn_tasa_comision1')[0].getValue(),	
						fn_porcentaje_iva:fpC.query('#fn_porcentaje_iva1')[0].getValue(),
						cg_usuario:fpC.query('#cg_usuario1')[0].getValue()
						
					})
				});				
			}else  {
			   Ext.ComponentQuery.query('#gridConsulta')[0].setTitle('');
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().getEl().mask('No se encontrar�n documentos operados y no operados dispersados.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(false);
			}

		}
	};
	
	Ext.define('LisDoctos', {
		extend: 'Ext.data.Model',
		fields: [						
			{ name: 'CG_RAZON_SOCIAL'},
			{ name: 'IC_PYME'},
			{ name: 'NO_CLIENTE'},
			{ name: 'DOMICILIO'},
			{ name: 'RFC'},
			{ name: 'MES_ANIO'}
		 ]
	});


	var consultaData = Ext.create('Ext.data.Store', {
		model: 'LisDoctos',
		storeId:'consultaData',
		pageSize: 1,
		proxy: {
			type: 'ajax',
			url: '15facturaprodExt.data.jsp',
			reader: 		 { 
				type: 'json', 
				root: 'registros' 	
			},
			extraParams: { 				
				informacion: 'Consultar'
			},
			totalProperty:   'total',
			messageProperty: 'msg'					
		},
		autoLoad: false,		
		listeners: {
			beforeload:	{fn: function(store, options){
				var  fp =   Ext.ComponentQuery.query('#forma')[0];
				var  fpFac =   Ext.ComponentQuery.query('#formaFactura')[0];
				
				var proxy= store.getProxy();
				proxy.setExtraParam('ic_producto_nafin', fp.query('#ic_producto_nafin1')[0].getValue() );
				proxy.setExtraParam('cmb_mesini', fp.query('#cmb_mesini1')[0].getValue() );   
				proxy.setExtraParam('cmb_anyoini', fp.query('#cmb_anyoini1')[0].getValue() );  
				proxy.setExtraParam('cmb_mesfin', fp.query('#cmb_mesfin1')[0].getValue() );
				proxy.setExtraParam('cmb_anyofin', fp.query('#cmb_anyofin1')[0].getValue() );
				proxy.setExtraParam('ic_epo', fp.query('#ic_epo1')[0].getValue() );
				proxy.setExtraParam('ic_if', fp.query('#ic_if1')[0].getValue() );
				proxy.setExtraParam('cmb_factura', fp.query('#cmb_factura1')[0].getValue() );
				proxy.setExtraParam('txt_nafelec', fp.query('#txt_nafelec1')[0].getValue() );
				proxy.setExtraParam('txt_nombre_pyme', fp.query('#txt_nombre_pyme1')[0].getValue() );
				proxy.setExtraParam('tipo_factura', fp.query('#tipo_factura_C')[0].getValue() );	
				
			}},
			load: procesarConsultar,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultar(null, null, null);						
				}
			}
		}		
	});


	function cambiosRenderer(val){
    return '<div style="white-space:normal !important;">'+ val +'</div>';
	}
	
	var  gridConsulta = Ext.create('Ext.grid.Panel',{		   
		itemId: 'gridConsulta',		
		store: consultaData, 			
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            'M�xico D.F. a ',
		border:           true,
		stripeRows:       true,
		autoScroll:       true,
		hidden:           true,
		frame:            false,			
		width: 800,
		height: 150,
		enableColumnMove: false,
		enableColumnHide: false,
		columns: [			
			{
				header: 'Nombre o Raz�n Social',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,
				width: 250,
				resizable: false,
				align: 'left',
				renderer:cambiosRenderer	
         },
			{
				header: 'No. Cliente',
				dataIndex: 'NO_CLIENTE',
				sortable: true,
				width: 100,
				resizable: false,
				align: 'center'
         },
			{
				header: 'Domicilio',
				dataIndex: 'DOMICILIO',
				sortable: true,
				width: 300,				
				resizable: false, 
				align: 'left',
				renderer:cambiosRenderer	
         }	,
			{
				header: 'Clave del RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 120,
				resizable: false,
				align: 'center'
         }	
		],
		bbar: [
			Ext.create('Ext.PagingToolbar',{
				store:      Ext.data.StoreManager.lookup('consultaData'),				
				itemId:     'barraPaginacion',
				displayMsg: '{0} - {1} de {2}',
				emptyMsg:   'No se encontraron registros',
				displayInfo:true,
				border:     false
			})	
		]
	});
	

	
	function procesarNombrePyme(opts, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {

			var  jsonData = Ext.JSON.decode(response.responseText);
		
			if(jsonData.nombrePyme!='') {
				Ext.ComponentQuery.query('#txt_nombre_pyme1')[0].setValue(jsonData.nombrePyme); 
			}else  {
				Ext.Msg.alert(	'Mensaje','<center> El No. Nafin Electr�nico no existe </center>');
			}
						
		} else {
			
			NE.util.mostrarConnError(response,opts);
		}
	}	

	function procesaValoresIniciales(opts, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {

			var  jsonData = Ext.JSON.decode(response.responseText);
			var  fp =   Ext.ComponentQuery.query('#forma')[0];
			fp.el.unmask();		
					
			if (jsonData != null){							
				var  forma =   Ext.ComponentQuery.query('#forma')[0];
				forma.query('#cmb_mesini1')[0].setValue(jsonData.mesHoy);			
				forma.query('#cmb_anyoini1')[0].setValue(jsonData.anyoHoy);
				forma.query('#cmb_mesfin1')[0].setValue(jsonData.mesHoy);
				forma.query('#cmb_anyofin1')[0].setValue(jsonData.anyoHoy);						
				forma.query('#df_factura1')[0].setValue(jsonData.fechaHoy);			
				
				var  forComi =   Ext.ComponentQuery.query('#formaComisiones')[0];
				forComi.query('#cg_usuario1')[0].setValue(jsonData.sbIniciales);	
				forComi.query('#fn_tasa_comision1')[0].setValue('1');	
			}
			
		} else {
			
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
		
	var  catalogoPyme = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15facturaprodExt.data.jsp',  
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoPyme'
			},
			listeners: {					
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	//---Catalogo  Anio	
	var  catalogoAnio = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15facturaprodExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoAnio'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	//---Catalogo  Producto
	var  catalogoProducto = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15facturaprodExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoProducto'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	//---Catalogo Epo x Producto
	var  catalogoEPOxProd = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15facturaprodExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoEPOxProd'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
		//---Catalogo IF
	var  catalogoIF = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15facturaprodExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoIF'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
		//---Catalogo Moneda
	var  catalogoMoneda = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15facturaprodExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoMoneda'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
			//---Catalogo I.V.A.
	var  catalogoIVA = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15facturaprodExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoIVA'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	//---Catalogo Tipo Factura
	var catTipoFact = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],	 
		autoLoad: false	  
	});
	
	catTipoFact.loadData(	[
		['T','Transacciones'],
		['C','Comisiones']			
	]);
	
	//---Catalogo Pyme
	var catPyme = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],	 
		autoLoad: false	  
	});
	
	catPyme.loadData(	[
	   ['','Seleccionar'],	
		['T','Todas']			
	]);
	
	//---Catalogo Meses
	var catMeses1 = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],	 
		autoLoad: false	  
	});
	
	catMeses1.loadData(	[		
		['01','Enero'],
		['02','Febrero'],
		['03','Marzo'],
		['04','Abril'],
		['05','Mayo'],
		['06','Junio'],
		['07','Julio'],
		['08','Agosto'],
		['09','Septiembre'],
		['10','Octubre'],
		['11','Noviembre'],
		['12','Diciembre']
	]);
	
	
	//---Catalogo Meses
	var catMeses = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],	 
		autoLoad: false	  
	});
	
	
	
	catMeses.loadData(	[		
		['01','Enero'],
		['02','Febrero'],
		['03','Marzo'],
		['04','Abril'],
		['05','Mayo'],
		['06','Junio'],
		['07','Julio'],
		['08','Agosto'],
		['09','Septiembre'],
		['10','Octubre'],
		['11','Noviembre'],
		['12','Diciembre']
	]);
	

	var  elemFormaComisiones= [
		{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			itemId: 'ic_moneda1',
			name: 'ic_moneda',
			hiddenName: 'ic_moneda',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 300,
			store: catalogoMoneda,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave'
		},
		{
			xtype: 'bigdecimal',
			fieldLabel: 'Monto de L�nea Autorizada',
			itemId: 'fn_monto_autorizado1',
			name: 'fn_monto_autorizado',
		   minValue:			'0.00',
        	maxValue:			'999,999,999,999,999,999.99',
        	maxText:  		'El monto no cumple con la precisi�n de 18 enteros 2 decimales',
        	format:			'$0,000.00' ,
			width: 300			
		},		
		{
			xtype: 'bigdecimal',
			fieldLabel: 'Tasa de Comisi�n',
			itemId: 'fn_tasa_comision1',
			name: 'fn_tasa_comision',
			minValue:			'0.00',
        	maxValue:			'999,999,999,999,999,999.99',
        	format:			'0,000.00' ,
			width: 300			
		},		
		{
			xtype: 'combo',
			fieldLabel: 'I.V.A',
			itemId: 'fn_porcentaje_iva1',
			name: 'fn_porcentaje_iva',
			hiddenName: 'fn_porcentaje_iva',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 300,
			store: catalogoIVA,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Iniciales del Usuario',
			itemId: 'cg_usuario1',
			name: 'cg_usuario',
			maxLength: 30,
			width: 300				
		}	
	];
	


	var fpComisiones = Ext.create('Ext.form.Panel',	{		
		itemId: 'formaComisiones',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: '',
      width: 620,
		hidden: true, 
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 150
		},		
		items:elemFormaComisiones, 
		buttons: [		
			{
				text: 'Generar',
				id: 'btnGenerarComi',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(){
				
					var  fpC =   Ext.ComponentQuery.query('#formaComisiones')[0];
					if(Ext.isEmpty(Ext.ComponentQuery.query('#ic_moneda1')[0].getValue())){
						Ext.ComponentQuery.query('#ic_moneda1')[0].markInvalid('Debe seleccionar una moneda');
						Ext.ComponentQuery.query('#ic_moneda1')[0].focus();						
						return;
					}	
					
					if(Ext.isEmpty(Ext.ComponentQuery.query('#fn_monto_autorizado1')[0].getValue())  ||  parseFloat(Ext.ComponentQuery.query('#fn_monto_autorizado1')[0].getValue())==0  ){
						Ext.ComponentQuery.query('#fn_monto_autorizado1')[0].markInvalid('Debe capturar o elegir el monto de l�nea, para poder generar la factura');
						Ext.ComponentQuery.query('#fn_monto_autorizado1')[0].focus();						
						return;
					}	
					
					if( Ext.isEmpty(Ext.ComponentQuery.query('#fn_tasa_comision1')[0].getValue())){
						Ext.ComponentQuery.query('#fn_tasa_comision1')[0].markInvalid('La tasa debe ser positiva');
						Ext.ComponentQuery.query('#fn_tasa_comision1')[0].focus();						
						return;
						
					}else if( !Ext.isEmpty(Ext.ComponentQuery.query('#fn_tasa_comision1')[0].getValue())){
					
						if( parseFloat(Ext.ComponentQuery.query('#fn_tasa_comision1')[0].getValue())>100 ){						
							Ext.ComponentQuery.query('#fn_tasa_comision1')[0].markInvalid('La tasa no debe ser mayor a 100');
							Ext.ComponentQuery.query('#fn_tasa_comision1')[0].focus();						
							return;							
						}
					
					}
					if( Ext.isEmpty(Ext.ComponentQuery.query('#fn_porcentaje_iva1')[0].getValue())){
						Ext.ComponentQuery.query('#fn_porcentaje_iva1')[0].markInvalid('Debe seleccionar un porcentaje');
						Ext.ComponentQuery.query('#fn_porcentaje_iva1')[0].focus();						
						return;
					}	
					
					
					var importe = (parseFloat(Ext.ComponentQuery.query('#fn_monto_autorizado1')[0].getValue()) * parseFloat(Ext.ComponentQuery.query('#fn_tasa_comision1')[0].getValue()))/100;
					var iva = (importe * parseFloat(Ext.ComponentQuery.query('#fn_porcentaje_iva1')[0].getValue() ))/100;
					var total = importe + iva;
					
					if(total >= 1000000000) { //mil millones
						Ext.Msg.alert(	'Mensaje','<center> No se permiten totales <br> iguales o mayores a $1000,000,000 </center>');
						return;
					}	
					 
					main.el.mask('Procesando Consulta...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fpC.getForm().getValues(),{								
							tipo_factura: Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()						
						})
					});	
					
					if(Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()=='C') {   // Comisiones  
						Ext.ComponentQuery.query('#formaComisiones')[0].hide();
					}
						
				
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimCom',
				iconCls: 'icoLimpiar',	
				formBind: true,
				handler: function(){
				
					Ext.ComponentQuery.query('#formaComisiones')[0].hide();	
					Ext.ComponentQuery.query('#gridTotales')[0].hide();	
					Ext.ComponentQuery.query('#gridTotalesC')[0].hide();	
					Ext.ComponentQuery.query('#gridConsulta')[0].hide();	
					Ext.ComponentQuery.query('#panelBotones')[0].hide();		
					Ext.ComponentQuery.query('#ic_producto_nafin1')[0].setValue('');
					Ext.ComponentQuery.query('#txt_nafelec1')[0].setValue('');
					Ext.ComponentQuery.query('#txt_nombre_pyme1')[0].setValue('');
				
				}
			}
		]
	});
	


	var  elementosForma = [
		{ xtype: 'textfield',	itemId: 'tituloGridC', hidden:  true },
		{ xtype: 'textfield',	itemId: 'tipo_factura_C', hidden:  true },
		{
			xtype: 'container',			
			layout: 'hbox', 
			width: 600,
			fieldLabel:'Per�odo (Mes - A�o)',
			itemId: 'ic_perido',
			hidden:true,
			items: [
				{
					xtype: 'displayfield',
					value: 'Per�odo (Mes - A�o):', 
					width: 124
				},
				{
					xtype: 'combo',
					//fieldLabel: 'Per�odo (Mes - A�o)',
					itemId: 'cmb_mesini1',
					name: 'cmb_mesini',
					hiddenName: 'cmb_mesini',
					mode: 'local',
					autoLoad: false,
					forceSelection: true,
					hidden: false, 
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,			
					store: catMeses,
					emptyText: 'Mes',
					queryMode: 'local',
					allowBlank: true,
					displayField: 'descripcion',
					valueField: 'clave',
					width: 90 
				},
				{
					xtype: 'displayfield',
					value: '&nbsp;  &nbsp;', 
					width: 20 
				},
				{
					xtype: 'combo',
					fieldLabel: '',
					itemId: 'cmb_anyoini1',
					name: 'cmb_anyoini',
					hiddenName: 'cmb_anyoini',
					mode: 'local',
					autoLoad: false,
					forceSelection: true,
					hidden: false, 
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,			
					store: catalogoAnio,
					emptyText: 'A�o',
					queryMode: 'local',
					allowBlank: true,
					displayField: 'descripcion',
					valueField: 'clave',
					width: 90 
				},
				{
					xtype: 'displayfield',
					value: '&nbsp; a  &nbsp;', 
					width: 30
				},
				{
					xtype: 'combo',
					fieldLabel: '',
					itemId: 'cmb_mesfin1',
					name: 'cmb_mesfin',
					hiddenName: 'cmb_mesfin',
					mode: 'local',
					autoLoad: false,
					forceSelection: true,
					hidden: false, 
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,			
					store: catMeses1,
					emptyText: 'Mes',
					queryMode: 'local',
					allowBlank: true,
					displayField: 'descripcion',
					valueField: 'clave',
					width: 90 
				},				
				{
					xtype: 'displayfield',
					value: '&nbsp;  &nbsp;', 
					width: 20
				},
				{
					xtype: 'combo',
					fieldLabel: '',
					itemId: 'cmb_anyofin1',
					name: 'cmb_anyofin',
					hiddenName: 'cmb_anyofin',
					mode: 'local',
					autoLoad: false,
					forceSelection: true,
					hidden: false, 
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,			
					store: catalogoAnio,
					emptyText: 'A�o',
					queryMode: 'local',
					allowBlank: true,
					displayField: 'descripcion',
					valueField: 'clave',
					width: 90 
				}				
			]
		},	
		{
			xtype: 'datefield',					
			fieldLabel: 'Fecha de Factura',
			name: 'df_factura',
			itemId: 'df_factura1',
			//vtype: 'rangoFechas',					
			allowBlank: true,
			startDay: 1,
			hidden: true, 
			msgTarget: 'side',				
			minValue: '01/01/1901',					
			margins: '0 20 0 0'	
		},
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			itemId: 'ic_producto_nafin1',
			name: 'ic_producto_nafin',
			hiddenName: 'ic_producto_nafin',
			mode: 'local',
			width: 350,
			autoLoad: false,
			forceSelection: true,
			hidden: true, 
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,			
			store: catalogoProducto,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',			
			listeners:	{					
				select:	{
					fn: function(combo){
					
						Ext.ComponentQuery.query('#gridTotales')[0].hide();	
						Ext.ComponentQuery.query('#gridTotalesC')[0].hide();	
						Ext.ComponentQuery.query('#gridConsulta')[0].hide();	
						Ext.ComponentQuery.query('#panelBotones')[0].hide();	
						Ext.ComponentQuery.query('#btnBuscaA')[0].enable();
					
						catalogoEPOxProd.load({
							params: 	{
								ic_producto_nafin: combo.getValue()
							}
						});
					}					
				}
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'EPO',
			itemId: 'ic_epo1',
			name: 'ic_epo',
			hiddenName: 'ic_epo',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,
			hidden: true, 
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			store: catalogoEPOxProd,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',			
			listeners:	{					
				select:	{
					fn: function(combo){
						var  forma =   Ext.ComponentQuery.query('#forma')[0];
						var ic_producto_nafin =  forma.query('#ic_producto_nafin1')[0].getValue();						
						
						catalogoIF.load({
							params: 	{
								ic_producto_nafin: ic_producto_nafin,
								ic_epo: combo.getValue()
							}
						});
					}					
				}
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'IF',
			itemId: 'ic_if1',
			name: 'ic_if',
			hiddenName: 'ic_if',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,
			hidden: true, 
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,	
			width: 500,
			store: catalogoIF,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave'			
		},
		{
			xtype: 'combo',
			fieldLabel: 'PYME',
			itemId: 'cmb_factura1',
			name: 'cmb_factura',
			hiddenName: 'cmb_factura',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,
			hidden: true, 
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,			
			store: catPyme,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',
			listeners:	{					
				select:	{
					fn: function(combo){
						if(combo.getValue()=='T'){
							Ext.ComponentQuery.query('#txt_nafelec1')[0].disable();
							Ext.ComponentQuery.query('#btnBuscaA')[0].disable();
							Ext.ComponentQuery.query('#txt_nafelec1')[0].setValue('');
							Ext.ComponentQuery.query('#txt_nombre_pyme1')[0].setValue('');
						}else  {
							Ext.ComponentQuery.query('#txt_nafelec1')[0].enable();
							Ext.ComponentQuery.query('#btnBuscaA')[0].enable();
						}
					}
				}
			}
		},
		{
			xtype: 'container',			
			layout: 'hbox', 
			width: 600,
			fieldLabel:'No. Nafin Electr�nico',
			itemId: 'busqueda_naf',
			hidden:true,
			items: [ 
				{
					xtype: 'numberfield',
					fieldLabel: 'No. Nafin Electr�nico',
					itemId: 'txt_nafelec1',
					name: 'txt_nafelec',
					maxLength: 30,
					width: 230,
					listeners:{
						blur: function(field){ 	
						
						Ext.Ajax.request({
							url: '15facturaprodExt.data.jsp',
							params: {
								informacion: "NombrePyme",
								txt_nafelec:field.value
							},							
							callback: procesarNombrePyme
						});
						
						}
					}				
				},
				{
					xtype: 'displayfield',
					value: '&nbsp;  &nbsp;', 
					width: 10
				},					
				{
					xtype:	'button',
					id:		'btnBuscaA',
					iconCls:	'icoBuscar',
					text:		'Busq. Avanzada',
					disabled:true,
					handler: function(boton, evento) {
					
						new Ext.Window({
							modal: true,
							width: 400,
							resizable: false,
							closable:true,
							id: 'winBuscaA',
							autoDestroy:false,
							closeAction: 'destroy',
							items: [	
								{
									xtype:'displayfield',
									frame:true,
									border: false,
									value:'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilice el * para b�squeda gen�rica'
								},
								{
									xtype:'form',
									id:'fpWinBusca',										
									frame: true,
									border: false,
									style: 'margin: 0 auto',
									bodyStyle:'padding:10px',
									defaults: {		msgTarget: 'side', 	anchor: '-20' 	},
									labelWidth: 30,
									items:[
										{
											xtype: 'textfield',
											fieldLabel: 'Nombre',
											itemId: 'nombre_pyme1',
											name: 'nombre_pyme',			
											width: 100,												
											maxLength: 25
										},
										{
											xtype: 'textfield',
											fieldLabel: 'RFC',
											itemId: 'rfc_prov1',
											name: 'rfc_prov',			
											width: 100,												
											maxLength: 25
										},
										{
											xtype: 'textfield',
											fieldLabel: 'No. Proveedor',
											itemId: 'numProveedor1',
											name: 'numProveedor',			
											width: 100,
											hidden:true,
											maxLength: 25
										}
									],
									buttonAlign:'center',
									buttons:[
										{
											text:'Buscar',
											iconCls:'icoBuscar',
											handler: function(boton) {
												
												var  forma =   Ext.ComponentQuery.query('#forma')[0];
												var  fpBus =   Ext.ComponentQuery.query('#fpWinBusca')[0];
												
												catalogoPyme.load({
													params: 	{
														ic_epo: forma.query('#ic_epo1')[0].getValue(),
														ic_producto_nafin: forma.query('#ic_producto_nafin1')[0].getValue(),
														nombre_pyme: fpBus.query('#nombre_pyme1')[0].getValue(),
														rfc_prov: fpBus.query('#rfc_prov1')[0].getValue(),
														numProveedor: fpBus.query('#numProveedor1')[0].getValue()
														
													}
												});
																								
												Ext.ComponentQuery.query('#ic_pyme1')[0].show();
												Ext.ComponentQuery.query('#btn_Aceptar')[0].show();												
																								
											}
										},
										{
											text:'Cancelar',
											iconCls: 'icoRechazar',
											handler: function() {
												Ext.getCmp('winBuscaA').hide();
												Ext.getCmp('winBuscaA').destroy();
											}
										}
									]
								},
								{
									xtype:'form',
									frame: true,
									id:'fpWinBuscaB',
									style: 'margin: 0 auto',
									bodyStyle:'padding:10px',
									monitorValid: true,
									defaults: { msgTarget: 'side', anchor: '-20' },
									labelWidth: 80,
									items:[
										{
											xtype: 'combo',
											fieldLabel: '',
											itemId: 'ic_pyme1',
											name: 'ic_pyme',
											hiddenName: 'ic_pyme',
											mode: 'local',
											autoLoad: false,
											forceSelection: true,
											hidden: true, 
											triggerAction : 'all',
											typeAhead: true,
											minChars : 1,	
											width: 500,
											store: catalogoPyme,
											emptyText: 'Seleccione...',
											queryMode: 'local',
											allowBlank: true,
											displayField: 'descripcion',
											valueField: 'clave'			
										}
									],
									buttonAlign:'center',
									buttons:[
										{
											text:'Aceptar',
											iconCls:'aceptar',
											itemId: 'btn_Aceptar',
											formBind:true,
											hidden: true, 
											handler: function() {
											
												if (!Ext.isEmpty(Ext.ComponentQuery.query('#ic_pyme1')[0].getValue())){
											
													var disp =Ext.ComponentQuery.query('#ic_pyme1')[0].getValue();
													var cveP = disp.substr(0,disp.indexOf(" "));
													var desc = disp.slice(disp.indexOf(" ")+1);
																										
													Ext.ComponentQuery.query('#txt_nafelec1')[0].setValue(cveP);
													Ext.ComponentQuery.query('#txt_nombre_pyme1')[0].setValue(desc);
													Ext.getCmp('winBuscaA').hide();													
												   Ext.getCmp('winBuscaA').destroy();
												
												}
											}
										}, 										
										{
											text:'Cancelar',
											iconCls: 'icoRechazar',
											handler: function() {	
												Ext.getCmp('winBuscaA').hide();	
												Ext.getCmp('winBuscaA').destroy();
											}
									}
								]
							}								
								
						]			
					}).show().setTitle('B�squeda Avanzada');	
					
						var  forma =   Ext.ComponentQuery.query('#forma')[0];
						var  fpBus =   Ext.ComponentQuery.query('#fpWinBusca')[0];
						
						if(!Ext.isEmpty(Ext.ComponentQuery.query('#ic_epo1')[0].getValue())){							
							fpBus.query('#numProveedor1')[0].show();
						}else  {
							fpBus.query('#numProveedor1')[0].hide();						
						}					
					}
				}
				
			]
			
		}, 		
		{
			xtype: 'container',			
			layout: 'hbox', 
			width: 600,
			fieldLabel:'No. Nafin Electr�nico',
			itemId: 'busqueda_naf_1',		
			items: [ 
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					value:'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				},
				{
					xtype: 'displayfield',
					fieldLabel: '',
					itemId: 'txt_nombre_pyme1',
					name: 'txt_nombre_pyme',			
					width: 500,						
					maxLength: 20
				}
			]			
		}
	];
	var fpFac = Ext.create('Ext.form.Panel',	{		
		itemId: 'formaFactura',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: '',
      width: 300,
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 120
		},		
		items:[
			{
				xtype: 'combo',
				fieldLabel: 'Tipo de Factura',
				itemId: 'tipo_factura1',
				name: 'tipo_factura',
				hiddenName: 'tipo_factura',
				mode: 'local',
				autoLoad: false,
				forceSelection: true,
				hidden: false, 
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,			
				store: catTipoFact,
				emptyText: 'Seleccione...',
				queryMode: 'local',
				allowBlank: true,
				displayField: 'descripcion',
				valueField: 'clave',
				listeners:	{					
					select:	{
						fn: function(combo){						
							
							
							Ext.ComponentQuery.query('#formaFactura')[0].hide();
							
							var  forma =   Ext.ComponentQuery.query('#forma')[0];
							
							forma.show();
							forma.query('#ic_producto_nafin1')[0].getValue();
							forma.query('#ic_producto_nafin1')[0].show();
							forma.query('#busqueda_naf')[0].show();
							
							forma.query('#tipo_factura_C')[0].setValue(combo.getValue());
							
							if(combo.getValue()=='T') {
								forma.query('#ic_perido')[0].show();						
								forma.query('#ic_epo1')[0].show();
								forma.query('#ic_if1')[0].show();
								forma.query('#cmb_factura1')[0].show();								
								forma.query('#df_factura1')[0].hide();								
								
								Ext.ComponentQuery.query('#formaComisiones')[0].hide();	
								
							}else  if(combo.getValue()=='C') {
							
								forma.query('#df_factura1')[0].show();								
								forma.query('#ic_perido')[0].hide();						
								forma.query('#ic_epo1')[0].hide();
								forma.query('#ic_if1')[0].hide();
								forma.query('#cmb_factura1')[0].hide();
								
								
								forma.query('#ic_perido')[0].hide();						
								forma.query('#ic_epo1')[0].hide();
								forma.query('#ic_if1')[0].hide();
								forma.query('#cmb_factura1')[0].hide();								
								forma.query('#btnLimpiar')[0].hide();																	
							}
							
							Ext.ComponentQuery.query('#gridTotales')[0].hide();
							Ext.ComponentQuery.query('#gridTotalesC')[0].hide();							
							Ext.ComponentQuery.query('#gridConsulta')[0].hide();	
							Ext.ComponentQuery.query('#panelBotones')[0].hide();						
							Ext.ComponentQuery.query('#btnBuscaA')[0].disable();
								
							
							catalogoProducto.load({
								params: 	{
									tipo_factura:combo.getValue()
								}
							});
							
							fp.el.mask('Inicializado valores...', 'x-mask-loading');		
							Ext.Ajax.request({
								url: '15facturaprodExt.data.jsp',
								params: {
									informacion: "valoresIniciales"			
								},
								callback: procesaValoresIniciales
							});
	
						}
					}
				}
			}		
		]
	}); 
	
	var fp = Ext.create('Ext.form.Panel',	{		
		itemId: 'forma',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: '',
      width: 620,
		hidden: true, 
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 120
		},		
		items:elementosForma, 
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(){
				
					var  fp =   Ext.ComponentQuery.query('#forma')[0];
						
					// Validaciones para Tipo de Transacciones
					if(Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()=='T' ){					
						
						if(Ext.isEmpty(Ext.ComponentQuery.query('#ic_producto_nafin1')[0].getValue())){
							Ext.ComponentQuery.query('#ic_producto_nafin1')[0].markInvalid('Debe elegir un producto.');
							Ext.ComponentQuery.query('#ic_producto_nafin1')[0].focus();						
							return;
						}
					
						if(Ext.isEmpty(Ext.ComponentQuery.query('#ic_epo1')[0].getValue())){
							Ext.ComponentQuery.query('#ic_epo1')[0].markInvalid('Debe elegir una EPO.');
							Ext.ComponentQuery.query('#ic_epo1')[0].focus();						
							return;
						}
						if(Ext.isEmpty(Ext.ComponentQuery.query('#ic_if1')[0].getValue())){
							Ext.ComponentQuery.query('#ic_if1')[0].markInvalid('Debe elegir un IF.');
							Ext.ComponentQuery.query('#ic_if1')[0].focus();						
							return;
						}	
						
						if(Ext.isEmpty(Ext.ComponentQuery.query('#cmb_factura1')[0].getValue())  &&  Ext.isEmpty(Ext.ComponentQuery.query('#txt_nafelec1')[0].getValue())   ){
							Ext.ComponentQuery.query('#cmb_factura1')[0].markInvalid('Desea generar las facturas para todos los clientes de la EPO seleccionada');
							Ext.ComponentQuery.query('#cmb_factura1')[0].focus();						
							return;
						}	
						
						Ext.ComponentQuery.query('#gridTotales')[0].hide();	
						Ext.ComponentQuery.query('#gridTotalesC')[0].hide();							
						Ext.ComponentQuery.query('#gridConsulta')[0].hide();	
						Ext.ComponentQuery.query('#panelBotones')[0].hide();	
					
						main.el.mask('Procesando Consulta...', 'x-mask-loading');
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar',							  
								start:0,
								limit:1,
								tipo_factura: Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()		
							})
						});	
						
					}else if(Ext.ComponentQuery.query('#tipo_factura_C')[0].getValue()=='C' ){  // Validaciones para Tipo de Factura Comisiones
						
						if(Ext.isEmpty(Ext.ComponentQuery.query('#df_factura1')[0].getValue())){
							Ext.ComponentQuery.query('#df_factura1')[0].markInvalid('Debe capturar la fecha de factura');
							return;
						}	
					
						if(Ext.isEmpty(Ext.ComponentQuery.query('#ic_producto_nafin1')[0].getValue())){
							Ext.ComponentQuery.query('#ic_producto_nafin1')[0].markInvalid('Debe elegir un producto.');
							Ext.ComponentQuery.query('#ic_producto_nafin1')[0].focus();						
							return;
						}				
					
						if(Ext.isEmpty(Ext.ComponentQuery.query('#txt_nafelec1')[0].getValue())){
							Ext.ComponentQuery.query('#txt_nafelec1')[0].markInvalid('Debe elegir o capturar el nafin electr�nico del proveedor');
											
							return;
						}				
												
						var  forComi =   Ext.ComponentQuery.query('#formaComisiones')[0];
					
							forComi.query('#ic_moneda1')[0].setValue('');	
							forComi.query('#fn_monto_autorizado1')[0].setValue('');	
							forComi.query('#fn_tasa_comision1')[0].setValue('1');	
							forComi.query('#fn_porcentaje_iva1')[0].setValue('');	
							forComi.query('#cg_usuario1')[0].setValue('');	
												
							Ext.Ajax.request({
								url: '15facturaprodExt.data.jsp',
								params: {
									informacion: "valoresIniciales"			
								},
								callback: procesaValoresIniciales
							});
							
						Ext.ComponentQuery.query('#formaComisiones')[0].show();		
						
					}
					
					Ext.ComponentQuery.query('#gridTotales')[0].hide();	
					Ext.ComponentQuery.query('#gridTotalesC')[0].hide();	
					Ext.ComponentQuery.query('#gridConsulta')[0].hide();	
					Ext.ComponentQuery.query('#panelBotones')[0].hide();						
					Ext.ComponentQuery.query('#btnBuscaA')[0].disable();		
					
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',	
				formBind: true,
				handler: function(){
					//window.location  = "15facturaprodExt.jsp"; 	
					Ext.ComponentQuery.query('#cmb_mesini1')[0].setValue('');
					Ext.ComponentQuery.query('#cmb_anyoini1')[0].setValue('');
					Ext.ComponentQuery.query('#cmb_mesfin1')[0].setValue('');
					Ext.ComponentQuery.query('#cmb_anyofin1')[0].setValue('');				
					Ext.ComponentQuery.query('#ic_producto_nafin1')[0].setValue('');
					Ext.ComponentQuery.query('#ic_epo1')[0].setValue('');
					Ext.ComponentQuery.query('#ic_if1')[0].setValue('');
					Ext.ComponentQuery.query('#cmb_factura1')[0].setValue('');
					Ext.ComponentQuery.query('#txt_nafelec1')[0].setValue('');
					Ext.ComponentQuery.query('#txt_nombre_pyme1')[0].setValue('');
					
					Ext.ComponentQuery.query('#gridTotales')[0].hide();	
					Ext.ComponentQuery.query('#gridTotalesC')[0].hide();	
					Ext.ComponentQuery.query('#gridConsulta')[0].hide();	
					Ext.ComponentQuery.query('#panelBotones')[0].hide();						
					Ext.ComponentQuery.query('#btnBuscaA')[0].disable();
					
					Ext.ComponentQuery.query('#formaComisiones')[0].hide();		

					Ext.Ajax.request({
						url: '15facturaprodExt.data.jsp',
						params: {
							informacion: "valoresIniciales"			
						},
						callback: procesaValoresIniciales
					});
	
	
				}
			}
		]
	});
	
	var main = Ext.create('Ext.container.Container', {
			id: 'contenedorPrincipal',
			renderTo: 'areaContenido',
			width: 900,
			style: 'margin:0 auto;',
			items: [
			fpFac,
			fp	,
			fpComisiones, 
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),
			gridTotales,
			gridTotalesC,
			NE.util.getEspaciador(20),
			panelBotones
			]
		});
	
		catalogoAnio.load();		
		catalogoMoneda.load();
		catalogoIVA.load();
		
	 
	
});
	
	
	