<!DOCTYPE html>
<%@page contentType="text/html;charset=windows-1252"%>
<%@ page import="java.util.*, netropology.utilerias.*"	
errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<% String version = (String)session.getAttribute("version"); %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs4.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%if(version!=null){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>

<script type="text/javascript" src="15facturaprodExt.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(version!=null){%>
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%if(version!=null){%>
	 <%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
	 <%}%>
	<div id="areaContenido" style="width:700px">
	</div>
	</div>
	</div>
	<div id="areaLeyenda"></div>
	 <%if(version!=null){%>
	 <%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'></form>

</body>

</html>
