Ext.onReady(function() {

//---------------------------procesarArchivos------------------------------------
	function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnArchivoCSV').setIconClass('icoXls');  
			Ext.getCmp('btnArchivoPDF').setIconClass('icoPdf');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			Ext.getCmp('btnArchivoCSV').enable();
			Ext.getCmp('btnArchivoPDF').enable();
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------fin procesarArchivos-------------------------------

//-----------------------------procesarConsultaData-----------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
	var fp = Ext.getCmp('forma');
		fp.el.unmask();							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
				}				
			if(store.getTotalCount() > 0) {		
				el.unmask();
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
			} else {		
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15lineasfondeoxifEXT.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'NOMBRE'},
			{	name: 'NUMERO_LINEA_FONDEO'},
			{	name: 'CODIGO_FINANCIERA'},
			{	name: 'TIPO_FINANCIERA'},
			{	name: 'CODIGO_AGENCIA'},
			{	name: 'CODIGO_SUB_APLICACION'},
			{	name: 'MONTO_ASIGNADO'},
			{	name: 'MONTO_UTILIZADO'},
			{	name: 'MONTO_COMPROMETIDO'},
			{	name: 'DISPONIBLE'},
			{	name: 'ADICIONADO_POR'},
			{	name: 'FECHA_ADICION'},
			{	name: 'MODIFICADO_POR'},
			{	name: 'FECHA_MODIFICACION'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
//----------------------------Fin ConsultaData----------------------------------

//------------------------------------Todas-------------------------------------
var Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin Todas------------------------------------

//----------------------------Fin ProcesarCadena--------------------------------
	var procesarCadena = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "-1", 
		descripcion: "SeleccionarIF", 
		loadMsg: ""}));
		
		store.insert(1,new Todas({ 
		clave: "0", 
		descripcion: "Todos", 
		loadMsg: ""})); 		

		store.commitChanges(); 
	}	
//----------------------------Fin ProcesarCadena--------------------------------
	
//------------------------------Catalogo IF-------------------------------------
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15lineasfondeoxifEXT.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {	
			load: procesarCadena,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
//------------------------------Fin Catalogo IF---------------------------------

//-------------------------------Elementos Forma--------------------------------	
	var elementosForma =  [	
		{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			name: 'ic_if',
			id: 'ic_if1',		
			mode: 'local',			
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_if',
			emptyText: 'Seleccionar IF...',
			autoLoad: false,
			forceSelection: true,
			//allowBlank		: false,
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoIF,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120,
			listeners	: {
			select		: {
				fn			: function(combo) {
								var valor  = combo.getValue();
				 if (valor == '-1'){/*
				 Ext.getCmp('forma').getForm().reset();
				 Ext.Msg.show({
						title: 'Intermediario Financiero',
						msg: 'Debe seleccionar un IF',
						modal: true,
						icon: Ext.Msg.WARNING,
						buttons: Ext.Msg.OK
					});*/
          Ext.getCmp('btnConsulta').setDisabled(true);
				} else {
          Ext.getCmp('btnConsulta').setDisabled(false);
          }
			}// FIN fn
		}//FIN select
	}//FIN listeners
}];
//-----------------------------Fin Elementos Forma------------------------------

//--------------------------------Grid Consulta---------------------------------	
var gridConsulta = new Ext.grid.EditorGridPanel({	
		store				:consultaData,
		id					:'gridConsulta',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Consulta ',	
		clicksToEdit	:1,
		hidden			:true,
		columns			:[{
							header		:'Nombre IF',
							tooltip		:'Nombre IF',
							dataIndex	:'NOMBRE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,
							align			:'left',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},
							{
							header		:'N�mero L�nea Fondeo',
							tooltip		:'N�mero L�nea Fondeo',
							dataIndex	:'NUMERO_LINEA_FONDEO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},
							{
							header		:'C�digo Financiera',
							tooltip		:'C�digo Financiera',
							dataIndex	:'CODIGO_FINANCIERA',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Descripci�n de l�nea de cr�dito',
							tooltip		:'Descripci�n de l�nea de cr�dito',
							dataIndex	:'TIPO_FINANCIERA',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'C�digo agencia',
							tooltip		:'C�digo agencia',
							dataIndex	:'CODIGO_AGENCIA',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'C�digo Subaplicaci�n',
							tooltip		:'C�digo Subaplicaci�n',
							dataIndex	:'CODIGO_SUB_APLICACION',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Monto asignado',
							tooltip		:'Monto asignado',
							dataIndex	:'MONTO_ASIGNADO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Monto utilizado',
							tooltip		:'Monto utilizado',
							dataIndex	:'MONTO_UTILIZADO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Monto Comprometido',
							tooltip		:'Monto Comprometido',
							dataIndex	:'MONTO_COMPROMETIDO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,	
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Disponible',
							tooltip		:'Disponible',
							dataIndex	:'DISPONIBLE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Adicionado por',
							tooltip		:'Adicionado por',
							dataIndex	:'ADICIONADO_POR',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Fecha adici�n',
							tooltip		:'Fecha adici�n',
							dataIndex	:'FECHA_ADICION',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Modificado por',
							tooltip		:'Modificado por',
							dataIndex	:'MODIFICADO_POR',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Fecha modificaci�n',
							tooltip		:'Fecha modificaci�n',
							dataIndex	:'FECHA_MODIFICACION',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							}
		],			
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 350,
      width				: 800,
		align				: 'center',
		frame				: false,
		bbar				: {

			store			: consultaData,
				emptyMsg		: "No hay registros.",
				items: [	
				'->','-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',					
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV', 
					handler: function(boton, evento) {
					fp.el.mask('Generando archivo...', 'x-mask-loading');
					//boton.setIconClass('loading-indicator');
					Ext.getCmp('btnArchivoCSV').disable();
						Ext.Ajax.request({
							url: '15lineasfondeoxifEXT.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'						
							}),
							callback: procesarArchivos
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
					fp.el.mask('Generando archivo...', 'x-mask-loading');
					//boton.setIconClass('loading-indicator');
					Ext.getCmp('btnArchivoPDF').disable();
						Ext.Ajax.request({
							url: '15lineasfondeoxifEXT.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF',
							start:0,
							limit: 15
							}),
							callback: procesarArchivos
						});
					}
				}	
			]
		}
	});	
//-----------------------------Fin Grid Consulta--------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de B�squeda',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsulta',
				iconCls: 'icoBuscar',
        disabled:true,
				//formBind: true,
				handler: function (boton, evento){
					fp.el.mask('Cargando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar',
							operacion : 'Generar',
							start:0,
							limit:15	
						})
					});
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function (boton, evento){
					Ext.getCmp('forma').getForm().reset();
					Ext.getCmp('gridConsulta').hide();
          Ext.getCmp('btnConsulta').setDisabled(true);
				}
			}
		]
	});
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});	
//-----------------------------Fin Contenedor Principal-------------------------

catalogoIF.load();

});//-----------------------------------------------Fin Ext.onReady(function(){}