Ext.onReady(function() {


//**************************textoCompleto
var textoCompleto = function (value, metadata, record, rowIndex, colIndex, store){
  metadata.attr = 'style="white-space: normal; word-wrap: break-word; "';
  return value;
};
//**************************textoCompleto

//---------------------------procesarDetalle------------------------------------
	function procesarDetalle(opts, success, response) {
	if (success == true ){
		var infoR = Ext.util.JSON.decode(response.responseText);
		var archivo = infoR.urlArchivo;
		archivo = archivo.replace('/nafin','');
		var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
	}
	}
//---------------------------fin procesarDetalle--------------------------------

//---------------------------procesarArchivos------------------------------------
	function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnArchivoCSV').setIconClass('icoXls');  
			Ext.getCmp('btnArchivoPDF').setIconClass('icoPdf');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			Ext.getCmp('btnArchivoCSV').enable();
			Ext.getCmp('btnArchivoPDF').enable();
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------fin procesarArchivos--------------------------------

//**************************************procesarVerCuentas*****************************			
var procesarVerCuentas= function(grid, rowIndex, colIndex, item, event) {
  var registro = grid.getStore().getAt(rowIndex);
  var clavePyme = registro.get('CODIGO_CLIENTE'); 
 // var naE = registro.get('ELECT'); 
 // var nombreEPO = registro.get('RAZONSOCIAL'); 
 // var tituloC = 'N@E:'+ naE +' '+nombreEPO;
 // var tituloF ='';
  
  consCuentasData.load({
        params: {
				informacion: 'verDetalleGrid',		
				clavePyme: clavePyme
			}
    });
     
  new Ext.Window({					
    width: 450,
    height: 650,
    modal: true,
    minWidth : 450,
    minHeight : 650,
    closeAction: 'hide',
          resizable: true,
					//autoScroll :true,
					constrain: true,
					closable:false,
    id: 'Vercuentas',
    items: [
      {
      xtype	: 'compositefield',
      items	:[	{
          xtype			: 'displayfield',
          value			: '',
          width			: 120
          },{
          xtype			: 'displayfield',
          value			: '<br> <img src="/nafin/00archivos/15cadenas/15archcadenas/logos/nafinsa.gif" height="65" width="175" alt="" border="0"><br>'
          }]
      },
      gridCuentas					
    ],
    bbar: {
      xtype: 'toolbar',	buttonAlign:'center',	
      buttons: ['-',
        {	xtype: 'button',	text: 'Cerrar',	iconCls: 'icoLimpiar', 	id: 'btnCerrar',
          handler: function(){																		
            Ext.getCmp('Vercuentas').hide();
          } 
      },
        '-',
        {
          xtype: 'button',
								text: 'Imprimir',
								tooltip:	'Imprimir',
								id: 'btnImprimirC',
								iconCls: 'icoPdf',
								handler: function(boton, evento) {
                var gridCuentas = Ext.getCmp('gridCuentas');
                var registro = gridCuentas.getStore().getAt(0);
                var clavePyme = registro.get('clave');   
               
									Ext.Ajax.request({
										url: '15clientesiracEXT.data.jsp',
										params:	{
											clavePyme: clavePyme,
											informacion: 'verDetalle',
											operacion: 'imprimeDetalle',
											start :0,
											limit: 15										
										}					
										,callback: procesarDetalle
									});
								}
        }	
      ]
    }
  }).show()
};
//*********************************procesarVerCuentas*****************************

//**************************procesarConCuentasData
var procesarConCuentasData = function(store,arrRegistros,opts){
	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros != null){			
			var gridCuentas = Ext.getCmp('gridCuentas');
			if (!gridCuentas.isVisible()) {
				gridCuentas.show();
			}	
			var el = gridCuentas.getGridEl();			
			var info = store.reader.jsonData;			
			if(store.getTotalCount()>0){				
				Ext.getCmp('btnImprimirC').enable();
			}else{	}
		}
	}
//**************************procesarConCuentasData

//-----------------------------procesarConsultaData-----------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
	var fp = Ext.getCmp('forma');
		fp.el.unmask();							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
				}				
			if(store.getTotalCount() > 0) {		
				el.unmask();
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
			} else {		
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//--------------------------Fin procesarConsultaData----------------------------
	

//******************* consCuentasData
	var consCuentasData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15clientesiracEXT.data.jsp',
		baseParams: {
			informacion: 'verDetalleGrid'			
		},		
		fields: [				
			{name : 'clave' },
      {name : 'descricpion'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConCuentasData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConCuentasData(null, null, null);					
				}
			}
		}					
	});
//****************** consCuentasData

//-------------------------------ConsultaData-----------------------------------
var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15clientesiracEXT.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'CODIGO_CLIENTE'},
			{	name: 'NUMERO_IDENTIFICACION'},
			{	name: 'NOMBRES'},
			{	name: 'PRIMER_APELLIDO'},
			{	name: 'SEGUNDO_APELLIDO'},
			{	name: 'RAZON_SOCIAL'},
			{	name: 'CODIGO_BLOQUEO'},
			{	name: ''}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	})
//----------------------------Fin ConsultaData----------------------------------
	
//---------------------------------radios-------------------------------
 var radios = [
  {
  xtype      : 'radio', 
  id			 : 'f',
  boxLabel   : 'F�sica',
  name       : 'rbGroup',
  checked    :true,
  inputValue : 'F',
  handler 	 : function() {
           var m=Ext.getCmp('m');
           if(!m.getValue()){
           Ext.getCmp('razon').disable();
      Ext.getCmp('apaterno').enable();
      Ext.getCmp('amaterno').enable();
      Ext.getCmp('nombre').enable();
      Ext.getCmp('rfcp').reset()
      Ext.getCmp('rfcpm').reset()
      Ext.getCmp('razon').reset()
      Ext.getCmp('apaterno').reset()
      Ext.getCmp('amaterno').reset()
      Ext.getCmp('nombre').reset()
      Ext.getCmp('gridConsulta').hide()
      Ext.getCmp('rfcp').setVisible(true)
      Ext.getCmp('rfcpm').setVisible(false)
          }
              }
  },
  {
    xtype          : 'radio', 
  id					: 'm',
    boxLabel       : 'Moral',
    name           : 'rbGroup',
    inputValue     : 'M',
  handler 	 : function() {
      var f=Ext.getCmp('f');
           if(!f.getValue()){
           Ext.getCmp('razon').enable();
      Ext.getCmp('apaterno').disable();
      Ext.getCmp('amaterno').disable();
      Ext.getCmp('nombre').disable();
      Ext.getCmp('rfcp').reset()
      Ext.getCmp('rfcpm').reset()
      Ext.getCmp('razon').reset()
      Ext.getCmp('apaterno').reset()
      Ext.getCmp('amaterno').reset()
      Ext.getCmp('nombre').reset()
      Ext.getCmp('gridConsulta').hide()
      Ext.getCmp('rfcp').setVisible(false)
      Ext.getCmp('rfcpm').setVisible(true)
      }
         }
  }
];
//---------------------------------radios-------------------------------    
    
//-------------------------------Elementos Forma--------------------------------	
var elementosForma =  [
			{
				xtype			: 'numberfield',
				id          : 'baseoperacion',
				name			: '_base_oper',
				fieldLabel  : 'No. SIRAC',
				maxLength	: 10,
				width			: 150,				
				margins		: '0 20 0 0'
			},{
			
		xtype				: 'compositefield',
		items				:[	{
							xtype			: 'displayfield',
							value			: '',
							width			: 100
			},{
							xtype			: 'displayfield',
							value			: 'Utilice el * para la b�squeda gen�rica '
			}
		]
			},{
				xtype			: 'textfield',
				id          : 'rfcp',
				name			: '_rfc',
				fieldLabel  : 'RFC',
				//maxLength	: 15,
        //hidden  : true,
				width			: 150,
				margins		: '0 20 0 0',
				allowBlank	: true,
				regex			:/^([A-Z|a-z|&amp;]{4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/ ,
				regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
				'en el formato NNNN-AAMMDD-XXX donde:<br>'+
				'NNNN:son las iniciales del nombre de la empresa<br>'+
				'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
				'XXX:es la homoclave'
			},{
				xtype			: 'textfield',
				id          : 'rfcpm',
				name			: '_rfc',
				fieldLabel  : 'RFC',
				//maxLength	: 15,
        hidden  : true,
				width			: 150,
				margins		: '0 20 0 0',
				allowBlank	: true,
				regex			:/^([A-Z|a-z|&amp;]{3})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/ ,
				regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
				'en el formato NNN-AAMMDD-XXX donde:<br>'+
				'NNN:son las iniciales del nombre de la empresa<br>'+
				'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
				'XXX:es la homoclave'
			},{
			
		xtype				: 'compositefield',
		id					: 'composite',
		items				:[	{
							xtype			: 'displayfield',
							id          : 'tpersona',
							name			: '_tpersona',
							fieldLabel  : 'Tipo de Persona '
							}, {
				xtype		:'radiogroup',
				id			:'radioGroup',
				items		:radios,
				columns	:2,
				allowBlank	: false
							}]
			},{
				xtype			: 'textfield',
				id          : 'razon',
				disabled :true,
				name			: '_razon',
				fieldLabel  : 'Raz�n Social',
				maxLength	: 20,
				width			: 150,
				margins		: '0 20 0 0',
				allowBlank	: true
			},{
				xtype			: 'textfield',
				id          : 'apaterno',
				name			: '_ap',
				fieldLabel  : 'Apellido Paterno',
				maxLength	: 20,
				width			: 150,
				margins		: '0 20 0 0',
				allowBlank	: true
			},{
				xtype			: 'textfield',
				id          : 'amaterno',
				name			: '_am',
				fieldLabel  : 'Apellido Materno ',
				maxLength	: 20,
				width			: 150,
				margins		: '0 20 0 0',
				allowBlank	: true
			},{
				xtype			: 'textfield',
				id          : 'nombre',
				name			: '_nom',
				fieldLabel  : 'Nombre',
				maxLength	: 20,
				width			: 150,
				margins		: '0 20 0 0',
				allowBlank	: true
			}
	];
//-----------------------------Fin Elementos Forma------------------------------

//***************gridCuentas
var gridCuentas = {
  //xtype: 'editorgrid',
  xtype: 'grid',
  store: consCuentasData,
  id: 'gridCuentas',		
  hidden: false,
  //title: 'Usuarios',	
  columns: [			
   {
    header		:'Nombre del Campo',
    dataIndex: 'descricpion',
    width : 200,
    align: 'center',
    sortable: false,
		resizable: false,
    renderer:function(value){
      return "<div align='left'>"+value+"</div>";
      }
    },{
    header	:'Valor',
    dataIndex: 'clave',
    width : 200,
    align: 'center',
    sortable: false,
		resizable: false,
    renderer: textoCompleto
  }],
  stripeRows: true,
  //loadMask: true,
  height: 530,
  width: 440,		
  frame: true
};
//*****************fin gridCuentas

//--------------------------------Grid Consulta---------------------------------
var gridConsulta = new Ext.grid.EditorGridPanel({	
		store				:consultaData,
		id					:'gridConsulta',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Consulta ',	
		clicksToEdit	:1,
		hidden			:true,
		columns			:[{
							header		:'C�digo Cliente',
							tooltip		:'C�digo Cliente',
							dataIndex	:'CODIGO_CLIENTE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},
							{
							header		:'RFC',
							tooltip		:'RFC',
							dataIndex	:'NUMERO_IDENTIFICACION',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},
							{
							header		:'Nombre',
							tooltip		:'Nombre',
							dataIndex	:'NOMBRES',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'Apellido Paterno',
							tooltip		:'Apellido Paterno',
							dataIndex	:'PRIMER_APELLIDO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'Apellido Materno',
							tooltip		:'Apellido Materno',
							dataIndex	:'SEGUNDO_APELLIDO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'Raz�n social',
							tooltip		:'Raz�n social',
							dataIndex	:'RAZON_SOCIAL',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'C�digo de Bloqueo',
							tooltip		:'C�digo de Bloqueo',
							dataIndex	:'CODIGO_BLOQUEO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
				xtype		: 'actioncolumn',
				header	: 'Detalle',
				tooltip	: 'Detalle',	
				width		: 80,							
				align		: 'center',
				items		: [
					{
					getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							
							return 'iconoLupa';
						},
						
						handler: procesarVerCuentas
					}
				]
			}
		],			
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 400,
      width				: 800,
		align				: 'center',
		frame				: false,
		bbar				: {

			store			: consultaData,
				emptyMsg		: "No hay registros.",
				items: [	
				'->','-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',					
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV', 
					handler: function(boton, evento) {
					fp.el.mask('Generando archivo...', 'x-mask-loading');
					boton.setIconClass('loading-indicator');
					Ext.getCmp('btnArchivoCSV').disable();
						Ext.Ajax.request({
							url: '15clientesiracEXT.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'						
							}),
							callback: procesarArchivos
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
					fp.el.mask('Generando archivo...', 'x-mask-loading');
					boton.setIconClass('loading-indicator');
					Ext.getCmp('btnArchivoPDF').disable();
						Ext.Ajax.request({
							url: '15clientesiracEXT.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF',
							start:0,
							limit: 15
							}),
							callback: procesarArchivos
						});
					}
				}	
			]
		}
	});
//-----------------------------Fin Grid Consulta--------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		heigth			:'auto', 
		title: 'Criterios de B�squeda',
		layout			:'form',  
		frame: true,		
		collapsible		:true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		monitorValid: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsulta',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function (boton, evento){
				//baseoperacion,rfcp,razon,apaterno,amaterno,nombre
				var fisica = Ext.getCmp('f').getValue(); 
				var moral  = Ext.getCmp('m').getValue(); 
				if (fisica | moral){
					var noSirac = Ext.getCmp('baseoperacion').getValue();
					var rfc		= Ext.getCmp('rfcp').getValue();
          var rfcm	= Ext.getCmp('rfcpm').getValue();
					var rS		= Ext.getCmp('razon').getValue();
					var aP		= Ext.getCmp('apaterno').getValue();
					var aM		= Ext.getCmp('amaterno').getValue();
					var nom		= Ext.getCmp('nombre').getValue();

					//noSirac != ""
					if (noSirac != "" || rfcm !="" || rfc != "" || rS != "" || aP != "" || aM != "" || nom != "" ){
				   fp.el.mask('Cargando...', 'x-mask-loading');			
					 consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar',
							operacion : 'Generar',
							start:0,
							limit:15	
						})
					 });
					}//FIN VERIFICACION DE CAMPOS
					else {
					 Ext.Msg.show({
						title: 'Consultar',
						msg: 'Por lo menos debe ingresar un criterio de Busqueda',
						modal: true,
						icon: Ext.Msg.WARNING,
						buttons: Ext.Msg.OK
					})	
					}
				}//FIN FISICA | MORAL
				}//FIN HANDLER
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				//formBind: true,
				handler: function (boton, evento){
					Ext.getCmp('gridConsulta').hide()
					Ext.getCmp('radioGroup').reset()
					Ext.getCmp('baseoperacion').reset();
					Ext.getCmp('rfcp').reset()
					Ext.getCmp('razon').reset()
					Ext.getCmp('apaterno').reset()
					Ext.getCmp('amaterno').reset()
					Ext.getCmp('nombre').reset()
					Ext.getCmp('razon').disable()
					Ext.getCmp('apaterno').enable()
					Ext.getCmp('amaterno').enable()
					Ext.getCmp('nombre').enable()
					Ext.getCmp('forma').getForm().reset()
				}
			}
		]
	});
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',		
		width: 949,
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	})
//-----------------------------Fin Contenedor Principal-------------------------

});//-----------------------------------------------Fin Ext.onReady(function(){}