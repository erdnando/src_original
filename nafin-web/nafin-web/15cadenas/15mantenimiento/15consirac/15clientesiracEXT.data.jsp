<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log, 
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
	String informacion 	= (request.getParameter("informacion")	!=null)?request.getParameter("informacion")	:""; 
	String operacion 		= (request.getParameter("operacion")	!=null)?request.getParameter("operacion")		:""; 
	
	String noSirac 		= (request.getParameter("_base_oper") 	== null) ? "" : request.getParameter("_base_oper");
	String tipoPersona 	= (request.getParameter("rbGroup") 	  	== null) ? "" : request.getParameter("rbGroup");
	String rfc 				= (request.getParameter("_rfc") 			== null) ? "" : request.getParameter("_rfc");
	String razonSocial 	= (request.getParameter("_razon")		== null) ? "" : request.getParameter("_razon");
	String pApellido 		= (request.getParameter("_ap") 			== null) ? "" : request.getParameter("_ap");
	String sApellido 		= (request.getParameter("_am") 			== null) ? "" : request.getParameter("_am");
	String nombre 			= (request.getParameter("_nom") 			== null) ? "" : request.getParameter("_nom");
	
	ConsClientesSiracEXT paginador = new ConsClientesSiracEXT();	
	
	int  start= 0, limit =0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();

if(informacion.equals("Consultar")  ||  informacion.equals("verDetalle") ||
			informacion.equals("ArchivoCSV") ||  informacion.equals("ArchivoPDF")  ) {
		
		rfc= rfc.toUpperCase();
		razonSocial= razonSocial.toUpperCase();
		pApellido= pApellido.toUpperCase();
		sApellido= sApellido.toUpperCase();
		nombre= nombre.toUpperCase();
		
		paginador.setNoSirac(noSirac);
		paginador.setTipoPersona(tipoPersona);
		paginador.setNumeroIdentificacion(rfc);
		paginador.setRazonSocial(razonSocial);
		paginador.setPApellido(pApellido);
		paginador.setSApellido(sApellido);
		paginador.setNombre(nombre);
			
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
		if(informacion.equals("Consultar")  ||  informacion.equals("ArchivoPDF") )  {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));						
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			if(informacion.equals("Consultar") ){
				try {
					if (operacion.equals("Generar")) {	
						queryHelper.executePKQuery(request); 
					}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
				} catch(Exception e) {
					throw new AppException("Error en la paginacion", e);
				}
			jsonObj = JSONObject.fromObject(consulta);
				
			}else if(informacion.equals("ArchivoPDF") ){
				try {
					String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
					infoRegresar = jsonObj.toString();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
			}
			
		}else  if(informacion.equals("ArchivoCSV") )  {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		
    }else  if (informacion.equals ("verDetalle"))  {
	
		int valor = Integer.parseInt(request.getParameter("clavePyme"));
	
		com.netro.cadenas.BaseOperacionSirac  paginadord	= new com.netro.cadenas.BaseOperacionSirac();
		CQueryHelperRegExtJS queryHelperextdet = new CQueryHelperRegExtJS( paginadord);
		paginadord.setBaseOperacion(valor);
		
		consulta = queryHelperextdet.getJSONPageResultSet(request,0,15);
		jsonObj = JSONObject.fromObject(consulta);
		
		Registros reg = queryHelperextdet.doSearch();
		
	
		int z=0;
		while (reg.next() & z<1){//consula especifica
			
			try {
			String fecNac = reg.getString("FECHA_DE_NACIMIENTO");
			if (fecNac !=null)
				reg.setObject("FECHA_DE_NACIMIENTO",fecNac.toString());
			else reg.setObject("FECHA_DE_NACIMIENTO","");
			} catch (Exception e){
				log.debug("no existe columna");
			}
			
			try {
			String fecAd = reg.getString("FECHA_ADICION");
			if (fecAd != null)
				reg.setObject("FECHA_ADICION",fecAd.toString());
			else	reg.setObject("FECHA_ADICION",""); 
			}catch (Exception e){
				log.debug("no existe columna");
			}
			
		
		try {
			String fecMod = reg.getString("FECHA_MODIFICACION");
			if (fecMod != null)
				reg.setObject("FECHA_MODIFICACION",fecMod.toString());
			else reg.setObject("FECHA_MODIFICACION","");
			}catch (Exception e){
				log.debug("no existe columna");
			}

			z++;
		}//fin consulta especifica
		consulta = 	"{\"success\": true,\"registros\": "+ reg.getJSONData()+"}";
		jsonObj = JSONObject.fromObject(consulta); 
		
	
	if (operacion.equals("imprimeDetalle")) {	
		int valordetalle = Integer.parseInt(request.getParameter("clavePyme"));

			try {
			String nombreArchivo = queryHelperextdet.getCreatePageCustomFile(request, 0,  15, strDirectorioTemp, "PDF");
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
		}
	infoRegresar = jsonObj.toString();
	
} else if (informacion.equals("verDetalleGrid")){
  int valor = Integer.parseInt(request.getParameter("clavePyme"));
		
    com.netro.cadenas.BaseOperacionSirac  paginadordG	= new com.netro.cadenas.BaseOperacionSirac();
		CQueryHelperRegExtJS queryHelperext = new CQueryHelperRegExtJS( paginadordG);
		paginadordG.setBaseOperacion(valor);
		
		consulta = queryHelperext.getJSONPageResultSet(request,0,15);
		jsonObj = JSONObject.fromObject(consulta);
		
    List principal = new ArrayList();
    HashMap mio = new HashMap(); 
    
		Registros reg = queryHelperext.doSearch();
		int z=0;
		while (reg.next() & z<1){//consula especifica
    
    try {
			String fecNac = reg.getString("FECHA_DE_NACIMIENTO");
			if (fecNac !=null)
				reg.setObject("FECHA_DE_NACIMIENTO",fecNac.toString());
			else reg.setObject("FECHA_DE_NACIMIENTO","");
    } catch (Exception e){
      log.debug("no existe columna");
    }
      
    try {
			String fecAd = reg.getString("FECHA_ADICION");
			if (fecAd != null)
				reg.setObject("FECHA_ADICION",fecAd.toString());
			else	reg.setObject("FECHA_ADICION",""); 
    }catch (Exception e){
      log.debug("no existe columna");
    }
      
		try {
			String fecMod = reg.getString("FECHA_MODIFICACION");
			if (fecMod != null)
				reg.setObject("FECHA_MODIFICACION",fecMod.toString());
			else reg.setObject("FECHA_MODIFICACION","");
    }catch (Exception e){
      log.debug("no existe columna");
    }
    	
    mio.put("descricpion","\"Código Cliente\"");  
    mio.put("clave",reg.getString("CODIGO_CLIENTE"));
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"RFC\"");
    mio.put("clave","\"<div align='left'>"+reg.getString("NUMERO_IDENTIFICACION")+"</div>\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"Nombre\"");
    mio.put("clave","\"<div align='left'>"+reg.getString("NOMBRES")+"</div>\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"Apellido Paterno\"");
    mio.put("clave","\"<div align='left'>"+reg.getString("PRIMER_APELLIDO")+"</div>\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"Apellido Materno\"");
    mio.put("clave","\"<div align='left'>"+reg.getString("SEGUNDO_APELLIDO")+"</div>\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"Sexo\"");
    mio.put("clave","\""+reg.getString("SEXO")+"\"");
    principal.add(mio);
    
    String fN="";
    try {
      mio = new HashMap();
      mio.put("descricpion","\"Fecha de Nacimiento\"");
      fN= reg.getString("FECHA_DE_NACIMIENTO");
      if (fN != null){
        String newFecha=(String)reg.getString("FECHA_DE_NACIMIENTO");
        String[] fechaArray = newFecha.split(" ");
        String miFecha ="";
        String miHora  ="";
        for (int zz=0; zz<fechaArray.length; zz++){
            if (zz==0)			miFecha = fechaArray[zz];
            if (zz==1)			miHora  = fechaArray[zz];
          }
        mio.put("clave","\""+miFecha+" 00:00:00.0"+"\"");  
      } else {
        mio.put("clave","\""+" "+"\"");
      }
    } catch (Exception e){
      mio.put("clave","\""+" "+"\"");
      log.debug("no existe columna");
    }
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"Razón social\"");
    mio.put("clave","\""+reg.getString("RAZON_SOCIAL")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"Adicionado por\"");
    mio.put("clave","\""+reg.getString("ADICIONADO_POR")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"Fecha de Adición\"");
    mio.put("clave","\""+reg.getString("FECHA_ADICION")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"Modificado Por\"");
    mio.put("clave","\""+reg.getString("MODIFICADO_POR")+"\"");
    principal.add(mio);
    
    String fM="";
    try {
      mio = new HashMap();
      mio.put("descricpion","\"Fecha de modificación\"");
			fM = reg.getString("FECHA_MODIFICACION");      
			if (fM != null){
        mio.put("clave","\""+reg.getString("FECHA_MODIFICACION")+"\"");
        }
			else { 
         mio.put("clave","\""+" "+"\"");
      }
    }catch (Exception e){
        mio.put("clave","\""+" "+"\"");
      log.debug("no existe columna");
    }    
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"Código departamento\"");
    mio.put("clave","\""+reg.getString("CODIGO_DEPARTAMENTO")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"Código municipio\"");
    mio.put("clave","\""+reg.getString("CODIGO_MUNICIPIO")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"Código país\"");
    mio.put("clave","\""+reg.getString("CODIGO_PAIS")+"\"");
    principal.add(mio);    
     
    mio = new HashMap();
    mio.put("descricpion","\"Código tipo de Identificación\"");
    mio.put("clave","\""+reg.getString("CCODIGO_TIPO_IDENTIFICACION_R")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"No empleados\"");
    mio.put("clave","\""+reg.getString("NO_EMPLEADOS")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"Ventas netas\"");
    mio.put("clave","\""+reg.getString("VENTAS_NETAS")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"Código Subsector\"");
    mio.put("clave","\""+reg.getString("CODIGO_SUBSECTOR")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"Código Rama\"");
    mio.put("clave","\""+reg.getString("CODIGO_RAMA")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"Código clase\"");
    mio.put("clave","\""+reg.getString("CODIGO_CLASE")+"\"");
    principal.add(mio);
      
    mio = new HashMap();
    mio.put("descricpion","\"Código bloqueo\"");
    mio.put("clave","\""+reg.getString("CODIGO_BLOQUEO")+"\"");
    principal.add(mio);
    
    z++;
		}//fin consulta especifica
    
    consulta = 	"{\"success\": true,\"registros\": "+principal+"}";
		jsonObj = JSONObject.fromObject(consulta); 
    infoRegresar = jsonObj.toString();
 }

%>


<%=infoRegresar%>

