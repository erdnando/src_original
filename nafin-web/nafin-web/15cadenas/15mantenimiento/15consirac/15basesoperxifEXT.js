Ext.onReady(function() {

//---------------------------procesarDetalle------------------------------------
	function procesarDetalle(opts, success, response) {
	if (success == true ){
		var infoR = Ext.util.JSON.decode(response.responseText);
		var archivo = infoR.urlArchivo;
		archivo = archivo.replace('/nafin','');
		var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
	}
	}
//---------------------------fin procesarDetalle--------------------------------

//**************************************procesarVerCuentas*****************************			
var procesarVerCuentas= function(grid, rowIndex, colIndex, item, event) {
  var registro = grid.getStore().getAt(rowIndex);
  var clavePyme = registro.get('CODIGO_BASE_OPERACION'); 
  var naE = registro.get('ELECT'); 
  var nombreEPO = registro.get('RAZONSOCIAL'); 
  var tituloC = 'N@E:'+ naE +' '+nombreEPO;
  var tituloF ='';
  
  consCuentasData.load({
        params: {
				informacion: 'verDetalleGrid',		
				clavePyme: clavePyme
			}
    });
     
  new Ext.Window({					
    width: 450,
    height: 790,
    modal: true,
    minWidth : 450,
    minHeight : 790,
    closeAction: 'hide',
          resizable: true,
					//autoScroll :true,
					constrain: true,
					closable:false,
    id: 'Vercuentas',
    items: [
      {
      xtype	: 'compositefield',
      items	:[	{
          xtype			: 'displayfield',
          value			: '',
          width			: 120
          },{
          xtype			: 'displayfield',
          value			: '<br> <img src="/nafin/00archivos/15cadenas/15archcadenas/logos/nafinsa.gif" height="65" width="175" alt="" border="0"><br>'
          }]
      },
      gridCuentas					
    ],
    bbar: {
      xtype: 'toolbar',	buttonAlign:'center',	
      buttons: ['-',
        {	xtype: 'button',	text: 'Cerrar',	iconCls: 'icoLimpiar', 	id: 'btnCerrar',
          handler: function(){																		
            Ext.getCmp('Vercuentas').hide();
            if (Ext.getCmp('Vercuentas').isVisible()){
              Ext.getCmp('Vercuentas').hide();
            }
          } 
      },
        '-',
        {
          xtype: 'button',
								text: 'Imprimir',
								tooltip:	'Imprimir',
								id: 'btnImprimirC',
								iconCls: 'icoPdf',
								handler: function(boton, evento) {
                var gridCuentas = Ext.getCmp('gridCuentas');
                var registro = gridCuentas.getStore().getAt(0);
                var clavePyme = registro.get('clave');   
               
									Ext.Ajax.request({
										url: '15basesoperxifEXT.data.jsp',
										params:	{
											clavePyme: clavePyme,
											informacion: 'verDetalle',
											operacion: 'imprimeDetalle',
											start :0,
											limit: 15,
											tipoAfiliado:'E',
											tituloF:tituloF,
											tituloC:tituloC											
										}					
										,callback: procesarDetalle
									});
								}
        }	
      ]
    }
  }).show()
};
//*********************************

//---------------------------procesarArchivos------------------------------------
	function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnArchivoCSV').setIconClass('icoXls');  
			Ext.getCmp('btnArchivoPDF').setIconClass('icoPdf');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			Ext.getCmp('btnArchivoCSV').enable();
			Ext.getCmp('btnArchivoPDF').enable();
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------fin procesarArchivos--------------------------------

//**************************
var textoCompleto = function (value, metadata, record, rowIndex, colIndex, store){
  metadata.attr = 'style="white-space: normal; word-wrap: break-word; "';
  return value;
};
//**************************

var procesarConCuentasData = function(store,arrRegistros,opts){
	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros != null){			
			var gridCuentas = Ext.getCmp('gridCuentas');
			if (!gridCuentas.isVisible()) {
				gridCuentas.show();
			}	
			var el = gridCuentas.getGridEl();			
			var info = store.reader.jsonData;				
			//gridCuentas.setTitle(info.tituloC);
			
			if(store.getTotalCount()>0){				
				Ext.getCmp('btnImprimirC').enable();
			}else{
			//	Ext.getCmp('btnImprimirC').disable();
//				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}

//*******************
	var consCuentasData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15basesoperxifEXT.data.jsp',
		baseParams: {
			informacion: 'verDetalleGrid'			
		},		
		fields: [				
			{name : 'clave' },
      {name : 'descricpion'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConCuentasData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConCuentasData(null, null, null);					
				}
			}
		}					
	});
//******************

//***************
var gridCuentas = {
  //xtype: 'editorgrid',
  xtype: 'grid',
  store: consCuentasData,
  id: 'gridCuentas',		
  hidden: false,
  //title: 'Usuarios',	
  columns: [			
   {
    header		:'Nombre del Campo',
    dataIndex: 'descricpion',
    width : 200,
    align: 'center',
    sortable: false,
		resizable: false,
    renderer:function(value){
      return "<div align='left'>"+value+"</div>";
      }
    },{
    header	:'Valor',
    dataIndex: 'clave',
    width : 200,
    align: 'center',
    sortable: false,
		resizable: false,
    renderer: textoCompleto
  }],
  stripeRows: true,
  //loadMask: true,
  height: 660,
  width: 440,		
  frame: true
};
//*****************

//------------------------------------Todas-------------------------------------
var Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin Todas------------------------------------

//----------------------------Fin ProcesarCadena--------------------------------
	var procesarCadena = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "-1", 
		descripcion: "Seleccionar IF", 
		loadMsg: ""}));

		store.commitChanges(); 
	}	
//----------------------------Fin ProcesarCadena--------------------------------

//-----------------------------procesarConsultaData-----------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
	var fp = Ext.getCmp('forma');
		fp.el.unmask();							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
				}				
			if(store.getTotalCount() > 0) {		
				el.unmask();
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
			} else {		
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15basesoperxifEXT.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'NOMBRE'},
			{	name: 'CODIGO_BASE_OPERACION'},
			{	name: 'UNODES'},
			{	name: 'CODIGO_PRODUCTO_BANCO'},
			{	name: 'DESDOS'},
			{	name: 'DIAS_MINIMOS_PRIMER_PAGO'},
			{	name: 'OPERA_FACTORAJE'},
			{	name: ''}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	})
//----------------------------Fin ConsultaData----------------------------------
	
//------------------------------Catalogo IF-------------------------------------
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15basesoperxifEXT.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			load: procesarCadena,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
//------------------------------Fin Catalogo IF---------------------------------

//-----------------------------Fin Elementos Detalle----------------------------
var  elementosDetalle =  []
//-----------------------------Fin Elementos Detalle-----------------------------

//-------------------------------Elementos Forma--------------------------------	
var elementosForma =  [	
		{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			name: 'ic_if',
			id: 'ic_if1',		
			mode: 'local',
			//allowBlank		: false,
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_if',
			emptyText: 'Seleccionar IF...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoIF,
			tpl: NE.util.templateMensajeCargaCombo,
			width: 120,
			listeners	: {
			select		: {
				fn			: function(combo) {
								var valor  = combo.getValue();
				 if (valor == '-1'){
         Ext.getCmp('btnConsulta').setDisabled(true);			
				} else {
          Ext.getCmp('btnConsulta').setDisabled(false);
        }
			}// FIN fn
		}//FIN select
	}//FIN listeners
		},{
				xtype			: 'numberfield',
				id          : 'baseoperacion',
				name			: '_base_oper',
				hiddenName	: '_base_oper',
				fieldLabel  : ' No. Base de operaci�n',
				maxLength	: 10,
				anchor		: '50%',
				//width			: 'auto',
				margins		: '0 20 0 0',
				allowBlank	: true
			}
	]
//-----------------------------Fin Elementos Forma------------------------------

//--------------------------------Grid Consulta---------------------------------
var gridConsulta = new Ext.grid.EditorGridPanel({	
		store				:consultaData,
		id					:'gridConsulta',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Consulta ',	
		clicksToEdit	:1,
		hidden			:true,
		columns			:[{
							header		:'Nombre IF',
							tooltip		:'Nombre IF',
							dataIndex	:'NOMBRE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'left',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},
							{
							header		:'C�digo base Operaci�n',
							tooltip		:'C�digo base Operaci�n',
							dataIndex	:'CODIGO_BASE_OPERACION',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},
							{
							header		:'Descripci�n Bases de Operaci�n',
							tooltip		:'Descripci�n Bases de Operaci�n',
							dataIndex	:'UNODES',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'left',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'C�digo producto Banco',
							tooltip		:'C�digo producto Banco',
							dataIndex	:'CODIGO_PRODUCTO_BANCO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Descripci�n producto Banco',
							tooltip		:'Descripci�n producto Banco',
							dataIndex	:'DESDOS',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'D�as m�nimos primer pago',
							tooltip		:'D�as m�nimos primer pago',
							dataIndex	:'DIAS_MINIMOS_PRIMER_PAGO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Opera factoraje',
							tooltip		:'Opera factoraje',
							dataIndex	:'OPERA_FACTORAJE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
				xtype		: 'actioncolumn',
				header	: 'Detalle',
				tooltip	: 'Detalle',	
				width		: 80,							
				align		: 'center',
				items		: [
					{
					getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';							
							return 'iconoLupa';
						},						
						handler: procesarVerCuentas
					}
				]
			}
		],			
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 350,
    width				: 800,
		align				: 'center',
		frame				: false,
		bbar				: {
				emptyMsg		: "No hay registros.",
				items: [	
				'->','-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',					
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV', 
					handler: function(boton, evento) {
					fp.el.mask('Generando archivo...', 'x-mask-loading');
					boton.setIconClass('loading-indicator');
					Ext.getCmp('btnArchivoCSV').disable();
						Ext.Ajax.request({
							url: '15basesoperxifEXT.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'						
							}),
							callback: procesarArchivos
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
					fp.el.mask('Generando archivo...', 'x-mask-loading');
					boton.setIconClass('loading-indicator');
					Ext.getCmp('btnArchivoPDF').disable();
						Ext.Ajax.request({
							url: '15basesoperxifEXT.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF',
							start:0,
							limit: 15
							}),
							callback: procesarArchivos
						});
					}
				}	
			]
		}
	});
//-----------------------------Fin Grid Consulta--------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de B�squeda',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsulta',
        disabled:true,
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function (boton, evento){
					fp.el.mask('Cargando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar',
							operacion : 'Generar',
							start:0,
							limit:15	
						})
					});
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function (boton, evento){
					Ext.getCmp('forma').getForm().reset();
					Ext.getCmp('gridConsulta').hide();
          Ext.getCmp('btnConsulta').setDisabled(true);
				}
			}
		]
	});
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	})
//-----------------------------Fin Contenedor Principal-------------------------
catalogoIF.load();
});//-----------------------------------------------Fin Ext.onReady(function(){}