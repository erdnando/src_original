<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		com.netro.afiliacion.*,
		com.netro.cadenas.*,
		com.netro.model.catalogos.*,
		netropology.utilerias.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		org.apache.commons.logging.Log "
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String operacion = (request.getParameter("operacion") == null) ? ""   : request.getParameter("operacion");
	
	String ic_if = (request.getParameter("ic_if") != null)?request.getParameter("ic_if"):"";
	int start=0, limit=15;

	JSONObject jsonObj = new JSONObject();
	String infoRegresar = "", consulta="";
	JSONObject 	resultado	= new JSONObject();

 if(informacion.equals("catalogoIF")) {
	//Thread.sleep(1500);
	//int x = 5/0;
	CatalogoIF	catIf 	= new CatalogoIF();
	catIf.setClave("ic_if");
	catIf.setDescripcion("'(' || ic_financiera || ') ' || cg_razon_social");
	catIf.setG_orden("ic_financiera");
	List lis = catIf.getListaElementosGral();
	jsonObj.put("registros", lis);
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString();

}else if(informacion.equals("Consultar")  ||   
			informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")  ) {
	
	ConsLineafondeIfEXT  paginadorext	= new ConsLineafondeIfEXT();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginadorext);
	paginadorext.setIc_if(ic_if);
		
	if(informacion.equals("Consultar")  ||  informacion.equals("ArchivoPDF") )  {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));							
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	}
	
	if(informacion.equals("Consultar") ){
	
	try {
		if (operacion.equals("Generar")) {	
			queryHelper.executePKQuery(request); 
		}
		//consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
		Registros reg = new Registros();
				// Edito los registros 
		reg = queryHelper.getPageResultSet(request,start,limit);
		while(reg.next()){
			String  monto_asignado = (reg.getString("MONTO_ASIGNADO") == null) ? "" : reg.getString("MONTO_ASIGNADO");
			reg.setObject("MONTO_ASIGNADO",monto_asignado);
			String  monto_utilizado = (reg.getString("MONTO_UTILIZADO") == null) ? "" : reg.getString("MONTO_UTILIZADO");
			reg.setObject("MONTO_UTILIZADO",monto_utilizado);
			String  monto_comprometido = (reg.getString("MONTO_COMPROMETIDO") == null) ? "" : reg.getString("MONTO_COMPROMETIDO");
			reg.setObject("MONTO_COMPROMETIDO",monto_comprometido);
			String  disponible = (reg.getString("DISPONIBLE") == null) ? "" : reg.getString("DISPONIBLE");
			reg.setObject("DISPONIBLE",disponible);
											
		}	
		consulta =  "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + reg.getJSONData()+"}";
		jsonObj = JSONObject.fromObject(consulta);
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
	//jsonObj = JSONObject.fromObject(consulta);
	//log.debug("jsonObj ------> "+ jsonObj);
			
	}else if(informacion.equals("ArchivoPDF") ){
	//Thread.sleep(1500);
	//int x = 5/0;
	try {
		String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
		//log.debug("nombre Archivo -- - - > " + nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo PDF", e);
	}
	
	}else  if(informacion.equals("ArchivoCSV") )  {
	//Thread.sleep(1500);
	//int x = 5/0;
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
infoRegresar = jsonObj.toString();
 }

%>
<%=infoRegresar%>
