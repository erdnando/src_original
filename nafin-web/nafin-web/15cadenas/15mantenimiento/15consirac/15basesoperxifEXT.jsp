<!DOCTYPE  HTML>
<%@ page contentType="text/html;charset=windows-1252" import="java.util.*, netropology.utilerias.*"	errorPage="/00utils/error.jsp"%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf"%>
<% String version = (String)session.getAttribute("version"); %>

<html>
  <head>
		<%@ include file="/extjs.jspf" %>
		<%if(version!=null){%>
		<%@ include file="/01principal/menu.jspf"%>
		<%}%>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
		<script type="text/javascript" src="15basesoperxifEXT.js?<%=session.getId()%>"></script> 
    <title>ADMIN NAFIN / Consultas en SIRAC/ Bases de Operaciones por IF</title>
  </head>
  
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%if(version!=null){%>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<%}%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%if(version!=null){%>
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<%}%>
			<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
		</div>
	</div>
	<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<%}%>
	<form id='formAux' name="formAux" target='_new'></form>
	</body>
</html>
