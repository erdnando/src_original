<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		com.netro.afiliacion.*,
		com.netro.cadenas.*,
		com.netro.model.catalogos.*,
		netropology.utilerias.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		org.apache.commons.logging.Log "
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
	String informacion 	= (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String operacion 		= (request.getParameter("operacion") 	!= null) ? request.getParameter("operacion")	  :"";
	String intermediario	= (request.getParameter("ic_if") 		!= null) ? request.getParameter("ic_if")		  :"";
	String baseOperacion = (request.getParameter("_base_oper") 	!= null) ? request.getParameter("_base_oper")  :"";
	int start=0, limit=15;

	JSONObject jsonObj  = new JSONObject();
	String infoRegresar = "", consulta="";

 if(informacion.equals("catalogoIF")) {
	CatalogoIF	catIf 	= new CatalogoIF();
	catIf.setClave("ic_if");
	catIf.setDescripcion("cg_razon_social");
	catIf.setG_orden("2");
	List lis = catIf.getListaElementosGral();
	jsonObj.put("registros", lis);
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString();

}else if(informacion.equals("Consultar")  || informacion.equals("verDetalle") ||
			informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")  ) {
	
	ParamBaseOperIfEXT  paginadorext	= new ParamBaseOperIfEXT();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginadorext);
	paginadorext.setIntermediario(intermediario);
	paginadorext.setBaseOperacion(baseOperacion);
	
	if(informacion.equals("Consultar")  ||  informacion.equals("ArchivoPDF") )  {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));							
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	}
	
	if(informacion.equals("Consultar") ){
	try {
		if (operacion.equals("Generar")) {	
			queryHelper.executePKQuery(request); 
		}
		consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
	jsonObj = JSONObject.fromObject(consulta);
	}else if(informacion.equals("ArchivoPDF") ){
	try {
		String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo PDF", e);
	}
	
	}else  if(informacion.equals("ArchivoCSV") )  {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	
	}else if (informacion.equals ("verDetalle")){
		int valor = Integer.parseInt(request.getParameter("clavePyme"));
		
		BaseOperacion  paginador	= new BaseOperacion();
		CQueryHelperRegExtJS queryHelperext = new CQueryHelperRegExtJS( paginador);
		paginador.setBaseOperacion(valor);
		
		consulta = queryHelperext.getJSONPageResultSet(request,start,limit);
		jsonObj = JSONObject.fromObject(consulta);
		//log.debug("jsonObj ------> "+ jsonObj);
		
		Registros reg = queryHelperext.doSearch();
		int z=0;
		while (reg.next() & z<1){//consula especifica
	
		String limMaxPro = reg.getString("LIMITE_MAXIMO_PRODUCTO");
		//log.debug("LIMITE_MAXIMO_PRODUCTO:"+limMaxPro);		
		String newLimitPro = Comunes.formatoDecimal(limMaxPro,2);
		//log.debug("newLimitPro:"+newLimitPro);		
		reg.setObject("LIMITE_MAXIMO_PRODUCTO",newLimitPro.toString());
		
		String limMaxCli = reg.getString("LIMITE_MAXIMO_CLIENTE");
		//log.debug("LIMITE_MAXIMO_CLIENTE:"+limMaxCli);		
		String newLimitCli = Comunes.formatoDecimal(limMaxCli,2);
		//log.debug("newLimitCli:"+newLimitCli);		
		reg.setObject("LIMITE_MAXIMO_CLIENTE",newLimitCli.toString());		
		
		z++;
		}//fin consulta especifica
		consulta = 	"{\"success\": true,\"registros\": "+ reg.getJSONData()+"}";
		jsonObj = JSONObject.fromObject(consulta); 
		//log.debug("jsonObj ------> "+ jsonObj);
		
	if (operacion.equals("imprimeDetalle")) {	
		int valordetalle = Integer.parseInt(request.getParameter("clavePyme"));
		try {
			String nombreArchivo = queryHelperext.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}	
	}

infoRegresar = jsonObj.toString();

 } else if (informacion.equals("verDetalleGrid")){
  int valor = Integer.parseInt(request.getParameter("clavePyme"));
		
		BaseOperacion  paginador	= new BaseOperacion();
		CQueryHelperRegExtJS queryHelperext = new CQueryHelperRegExtJS( paginador);
		paginador.setBaseOperacion(valor);
		
		consulta = queryHelperext.getJSONPageResultSet(request,start,limit);
		jsonObj = JSONObject.fromObject(consulta);
		//log.debug("jsonObj ------> "+ jsonObj);
		
    List principal = new ArrayList();
    HashMap mio = new HashMap();
    String operacionTit = "\"CodigoBaseOperacion\"";    
    
		Registros reg = queryHelperext.doSearch();
		int z=0;
		while (reg.next() & z<1){//consula especifica
   // mio = new HashMap();
    
		String limMaxPro = reg.getString("LIMITE_MAXIMO_PRODUCTO");
		String newLimitPro = Comunes.formatoDecimal(limMaxPro,2);
		reg.setObject("LIMITE_MAXIMO_PRODUCTO",newLimitPro.toString());
		
		String limMaxCli = reg.getString("LIMITE_MAXIMO_CLIENTE");
		String newLimitCli = Comunes.formatoDecimal(limMaxCli,2);
		reg.setObject("LIMITE_MAXIMO_CLIENTE",newLimitCli.toString());	
    
    String des = reg.getString("DESCRIPCI");
    String newdes = des.replace	('\"',' ');
		reg.setObject("DESCRIPCI",newdes.toString());
    
    mio.put("descricpion","\"CÓDIGO BASE OPERACIÓN\"");  
    mio.put("clave",reg.getString("CODIGO_BASE_OPER"));
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"CÓDIGO EMPRESA\"");
    mio.put("clave",reg.getString("CODIGO_EMP"));
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"CÓDIGO PRODUCTO BANCO\"");
    mio.put("clave",reg.getString("CODIGO_PROC_BANCO"));
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"DESCRIPCIÓN PRODUCTO BANCO\"");
    mio.put("clave","\""+reg.getString("DESCRIPCION")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"AÑO\"");
    mio.put("clave",reg.getString("ANIO"));
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"MODALIDAD PRODUCTO\"");
    mio.put("clave",reg.getString("MODALIDAD_PRODUCTO"));
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"CÓDIGO GRUPO PARAMETROS\"");
    mio.put("clave",reg.getString("CODIGO_GRUPO_PARAMETROS"));
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"TIPO CERTIFICADOS\"");
    mio.put("clave","\""+reg.getString("TIPO_CERTIFICADO")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"TIPO USO TASA\"");
    mio.put("clave","\""+reg.getString("TIPO_USO_TASA")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"VIGENTE\"");
    mio.put("clave","\""+reg.getString("VIGENTE")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"CÓDIGO GRUPO MORA\"");
    mio.put("clave",reg.getString("CODIGO_GRUPO_MORA"));
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"RELACIÓN MATEMÁTICA MORA\"");
    mio.put("clave","\""+reg.getString("RELACION_MATEMATICA_MORA")+"\"");
    principal.add(mio);
      
    mio = new HashMap();
    mio.put("descricpion","\"SPREAD MORA\"");
    mio.put("clave",reg.getString("SPREAD_MORA"));
    principal.add(mio);
      
    mio = new HashMap();
    mio.put("descricpion","\"LIMITE MÁXIMO PRODUCTO\"");
    mio.put("clave","\"<div align='right'>"+reg.getString("LIMITE_MAXIMO_PRODUCTO")+"</div>\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"LIMITE MÁXIMO CLIENTE\"");
    mio.put("clave","\"<div align='right'>"+reg.getString("LIMITE_MAXIMO_CLIENTE")+"</div>\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"ADICIONADO POR\"");
    mio.put("clave","\""+reg.getString("ADICIONADO_PX")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"FECHA ADICIÓN\"");
    mio.put("clave","\""+reg.getString("FECHA_ADICION")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"MODIFICADO POR\"");
    mio.put("clave","\""+reg.getString("MODIFICADO_POR")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"FECHA MODIFICACIÓN\"");
    mio.put("clave","\""+reg.getString("FECHA_MOD")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"DESCRIPCIÓN\"");
    mio.put("clave","\"<div align='left'>"+reg.getString("DESCRIPCI")+"</div>\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"DIAS MINIMOS PRIMER PAGO\"");
    mio.put("clave","\""+reg.getString("DIAS_MINIMOS_PRIMER_PAGO")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"OPERA FACTORAJE\"");
    mio.put("clave","\""+reg.getString("OPERA_FACTORAJE")+"\"");
    principal.add(mio);

    mio = new HashMap();
    mio.put("descricpion","\"VALIDA LIMITE MÁXIMO CLIENTE\"");
    mio.put("clave","\""+reg.getString("VALIDA_LIMITE_MAXIMO_CLIENTE")+"\"");
    principal.add(mio);
    
    mio = new HashMap();
    mio.put("descricpion","\"CON TRANSFERENCIA\"");
    mio.put("clave","\""+reg.getString("CON_TRANSFERENCIA")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"EMERGENTE\"");
    mio.put("clave","\""+reg.getString("EMERGENTE")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"TIPO BASE OPERACIÓN\"");
    mio.put("clave","\""+reg.getString("TIPO_B_OPERACION")+"\"");
    principal.add(mio);
     
    mio = new HashMap();
    mio.put("descricpion","\"APLICA MODALIDAD\"");
    mio.put("clave","\""+reg.getString("APLICA_MODALIDAD")+"\"");
    principal.add(mio);
    
    z++;
		}//fin consulta especifica
    //principal.add(mio);
		//consulta = 	"{\"success\": true,\"registros\": "+ reg.getJSONData()+"}";
    consulta = 	"{\"success\": true,\"registros\": "+principal+"}";
		jsonObj = JSONObject.fromObject(consulta); 
		//log.debug("jsonObj ------> "+ jsonObj);
    infoRegresar = jsonObj.toString();
 }
%>
<%=infoRegresar%>
