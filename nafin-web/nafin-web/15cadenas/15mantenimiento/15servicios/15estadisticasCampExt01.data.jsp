<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.servicios.*,
		javax.naming.Context,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
String informacion = (request.getParameter("informacion")!=null)?(String)request.getParameter("informacion"):"";
String campana = (request.getParameter("campana")!=null)?(String)request.getParameter("campana"):"";
String epo = (request.getParameter("epo")!=null)?(String)request.getParameter("epo"):"";
String operacion = request.getParameter("operacion") == null?"": request.getParameter("operacion");
String accion = request.getParameter("accion") == null?"": request.getParameter("accion");
String selCadena = request.getParameter("selCadena") == null?"": request.getParameter("selCadena");
String cmbRespuesta = request.getParameter("cmbRespuesta") == null?"": request.getParameter("cmbRespuesta");
String infoRegresar="", consulta="",  consulta2	="";
JSONObject jsonObj = new JSONObject();

Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB",Servicios.class);
EstadisticasCamp clase = new EstadisticasCamp();

if(informacion.equals("catalogoCampana")){

	JSONArray catCamp= clase.getCatCamp("1");
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("total", new Integer(catCamp.size()));
	jsonObj.put("registros", catCamp);
	infoRegresar= jsonObj.toString();

} else if(informacion.equals("catalogoEstatus")){

	JSONObject	resultado	= new JSONObject();
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	registro.put("clave",			"1");
	registro.put("descripcion",	"Todos");
	registros.add(registro);
	registro.put("clave",			"2");
	registro.put("descripcion",	"Si");
	registros.add(registro);
	registro.put("clave",			"3");
	registro.put("descripcion",	"No");
	registros.add(registro);
	registro.put("clave",			"4");
	registro.put("descripcion",	"Sin Contestar");
	registros.add(registro);
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
	infoRegresar = resultado.toString();

}else if(informacion.equals("catalogoEPO")){

	JSONArray catEpo= clase.getCatEPO(campana);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("total", new Integer(catEpo.size()));
	jsonObj.put("registros", catEpo);
	infoRegresar= jsonObj.toString();
}else if(informacion.equals("fechaPublicacion")){

	HashMap catFecha= clase.getFechaPub(campana);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("fecha_in", catFecha.get("fecha_in"));
	jsonObj.put("fecha_fin", catFecha.get("fecha_fin"));
	infoRegresar= jsonObj.toString();

}else if(informacion.equals("consultar")||informacion.equals("consultarTotales")||informacion.equals("consultarD")||informacion.equals("consultarTotalesD")||informacion.equals("GenerarArchivoCSV")||informacion.equals("GenerarArchivoCSVD")||informacion.equals("GenerarArchivoCSVDetalle")){
	int start = 0;
	int limit = 0;
	EstadisticasCamp paginador = new EstadisticasCamp();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	paginador.setTxtCampana(campana);
	paginador.setTxtEpo(epo);
	paginador.setTxtRespuesta(cmbRespuesta);
	paginador.setTxtAccion(accion);
	JSONObject 	resultado	= new JSONObject();
	if(informacion.equals("consultar")||informacion.equals("consultarD")){
	
		try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try{
			if(operacion.equals("Generar")){
				queryHelper.executePKQuery(request);
			}
			
			if(informacion.equals("consultar")){
				consulta2	= queryHelper.getJSONPageResultSet(request,start,limit);	
			}
			if(informacion.equals("consultarD")){
				Registros registros = queryHelper.getPageResultSet(request,start,limit);
				consulta2	=	"{\"success\": true, \"total\": \"" + registros.getNumeroRegistros() + "\", \"registros\": " + registros.getJSONData()+"}";

			}
			resultado = JSONObject.fromObject(consulta2);
			infoRegresar = resultado.toString();
			
		}catch(Exception e){
			throw new AppException("Error en la paginación",e);
		}
	}else if(informacion.equals("consultarTotales")||informacion.equals("consultarTotalesD")) {

		try{
			consulta = queryHelper.getJSONResultCount(request); 
			jsonObj = JSONObject.fromObject(consulta);
			
		} catch(Exception e){
			log.warn("-----> Error en consulta totales: " + e);
		}
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("GenerarArchivoCSV")){
		
		paginador.setTxtSelCadena(selCadena);
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if(informacion.equals("GenerarArchivoCSVD")){
	
		paginador.setTxtSelCadena(selCadena);
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if(informacion.equals("GenerarArchivoCSVDetalle")){
		paginador.setTxtSelCadena(selCadena);
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
}

%>

<%=infoRegresar%>

