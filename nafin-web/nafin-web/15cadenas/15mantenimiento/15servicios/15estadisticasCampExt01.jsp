<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf"%>


<html>
<head>
<title>Nafi@net - Autorizaciones de Solicitudes</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>

<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script language="JavaScript" src="../../../00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="15estadisticasCampExt01.js?<%=session.getId()%>"></script> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/01if/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
<%}%>
		 <div id="areaContenido"></div>                                                                                          
<%if(esEsquemaExtJS){%>
	</div>
	</div>
	<%@ include file="/01principal/01if/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'>
	
	<input type="hidden" id="iTipoPerfil" name=" " value="<%=iTipoPerfil%>"/>	
</form>
</body>
</html>
