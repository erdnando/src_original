<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,		
		javax.naming.Context,
		com.netro.servicios.*,
		netropology.utilerias.usuarios.*,
		org.apache.commons.logging.Log,		
		com.jspsmart.upload.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String infoRegresar="",  mensaje ="",  mensajeEnviado = "", momentoEnvioS  ="", tipoInformacionS ="";

String informacion = (request.getParameter("informacion")!=null)?(String)request.getParameter("informacion"):"";
String claveUsuario = (request.getParameter("claveUsuario")!=null)?(String)request.getParameter("claveUsuario"):"";
String nafinElectronico = (request.getParameter("nafinElectronico")!=null)?(String)request.getParameter("nafinElectronico"):"";
String razonSocial = (request.getParameter("razonSocial")!=null)?(String)request.getParameter("razonSocial"):"";
String rfc = (request.getParameter("rfc")!=null)?(String)request.getParameter("rfc"):"";
String numeroSirac = (request.getParameter("numeroSirac")!=null)?(String)request.getParameter("numeroSirac"):"";
String icPyme = (request.getParameter("icPyme")!=null)?(String)request.getParameter("icPyme"):"";
String recibeFactoraje = (request.getParameter("recibeFactoraje")!=null)?(String)request.getParameter("recibeFactoraje"):"";
String recibeFactoraje2 = (request.getParameter("recibeFactoraje2")!=null)?(String)request.getParameter("recibeFactoraje2"):"";
String email = (request.getParameter("email")!=null)?(String)request.getParameter("email"):"";
String confirmacionCorreo = (request.getParameter("confirmacionCorreo")!=null)?(String)request.getParameter("confirmacionCorreo"):"";
String nuevoCelular = (request.getParameter("nuevoCelular")!=null)?(String)request.getParameter("nuevoCelular"):"";
String confirmacionCelular = (request.getParameter("confirmacionCelular")!=null)?(String)request.getParameter("confirmacionCelular"):"";
String momentoEnvio_0 = (request.getParameter("momentoEnvio_0")!=null)?(String)request.getParameter("momentoEnvio_0"):"";
String momentoEnvio_1 = (request.getParameter("momentoEnvio_1")!=null)?(String)request.getParameter("momentoEnvio_1"):"";
String tipoInformacion_0 = (request.getParameter("tipoInformacion_0")!=null)?(String)request.getParameter("tipoInformacion_0"):"";
String tipoInformacion_1 = (request.getParameter("tipoInformacion_1")!=null)?(String)request.getParameter("tipoInformacion_1"):"";
String compCelular = (request.getParameter("compCelular")!=null)?(String)request.getParameter("compCelular"):"";
String claveConfirmacion = (request.getParameter("claveConfirmacion")!=null)?(String)request.getParameter("claveConfirmacion"):"";
String claveConfirmacionSesion = (request.getParameter("claveConfirmacionSesion")!=null)?(String)request.getParameter("claveConfirmacionSesion"):"";

String msgError ="";		 
		
if(recibeFactoraje.equals("on")) {  recibeFactoraje = "S";    }
if(recibeFactoraje2.equals("on")) {  recibeFactoraje = "S";    }

if(momentoEnvio_0.equals("S")  &&  momentoEnvio_1.equals("S") ) {
	momentoEnvioS = "A";
} else if(momentoEnvio_0.equals("S")  &&  momentoEnvio_1.equals("") ) {
	momentoEnvioS = "P";
}else if(momentoEnvio_0.equals("")  &&  momentoEnvio_1.equals("S") ) {
	momentoEnvioS = "V";
}
	
if(tipoInformacion_0.equals("S")  &&  tipoInformacion_1.equals("S") ) {
	tipoInformacionS = "A";
} else if(tipoInformacion_0.equals("S")  &&  tipoInformacion_1.equals("") ) {
	tipoInformacionS = "M";
}else if(tipoInformacion_0.equals("")  &&  tipoInformacion_1.equals("S") ) {
	tipoInformacionS = "N";
}


Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB",Servicios.class);						

JSONObject jsonObj = new JSONObject();
HashMap	datos = new HashMap();
JSONArray registros = new JSONArray();

if(informacion.equals("valoresIniciales")){
	
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();  

}else  if(informacion.equals("catalogoPYME")){

	ConsultaPymes consultaPymes = new ConsultaPymes();
	List listaPymes = consultaPymes.getListaPymes(claveUsuario, nafinElectronico, razonSocial, rfc, numeroSirac);
	jsonObj.put("registros", listaPymes);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();  

}else  if(informacion.equals("Consultar")){
		
	HashMap  resultado = servicios.getInformacionPyme(icPyme);
	registros.add(resultado);	
	
	String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();

}else  if(informacion.equals("obtenerDatosPyme")){

	List datosPyme = servicios.getDatosPymeFactorajeMovil(icPyme);
	
	nuevoCelular = (((String)datosPyme.get(1)).equals("")?"":"044 "+datosPyme.get(1));
	email = (String)datosPyme.get(0); 

	if(datosPyme.size()==0) {
		mensaje = "No se encontró información de contacto de la pyme.";
	}
	jsonObj.put("success",  new Boolean(true));
	jsonObj.put("email",  email);
	jsonObj.put("numeroCelular",  nuevoCelular); 
	jsonObj.put("mensaje",  mensaje);
	infoRegresar = jsonObj.toString();  


}else  if(informacion.equals("Activar_Servicio") || informacion.equals("Aceptar") ){   
	
	if(informacion.equals("Activar_Servicio") ){  
		mensajeEnviado = "Le ha sido enviado a su celular registrado el Código de Verificación <br>para continuar el registro.";
	}else  	if(informacion.equals("Aceptar") ){  
		mensajeEnviado = "Información guardada exitosamente.";
	}
	
	String diasPrevios ="";
	
	try{
		
	   String icTerminos = servicios.getVersionTerminosCondiciones("1");
		diasPrevios = servicios.getDiasPreviosVencimiento();
					
		UtilUsr utilUsr = new UtilUsr();
		Usuario usuario = utilUsr.getUsuario(iNoUsuario);
		String nombreUsuario = usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
								
		List datosRegistroFactorajeMovil = new ArrayList();
				
		datosRegistroFactorajeMovil.add(icPyme);
		datosRegistroFactorajeMovil.add("1");
		datosRegistroFactorajeMovil.add(recibeFactoraje);
		datosRegistroFactorajeMovil.add(email);
		datosRegistroFactorajeMovil.add(confirmacionCorreo);
		datosRegistroFactorajeMovil.add(nuevoCelular);
		datosRegistroFactorajeMovil.add(confirmacionCelular);
		datosRegistroFactorajeMovil.add(strNombreUsuario);
		datosRegistroFactorajeMovil.add(iNoUsuario);
		datosRegistroFactorajeMovil.add(icTerminos);
		datosRegistroFactorajeMovil.add("");		
							
		servicios.guardarRegistroFactorajeMovil(datosRegistroFactorajeMovil);
				
		List datosPyme = servicios.getDatosPymeFactorajeMovil(icPyme);
				
		String numeroCelular = (String)datosPyme.get(1);
				
		Comunes validacion = new Comunes();
		claveConfirmacionSesion = validacion.cadenaAleatoria(6);
	
		System.out.println("claveConfirmacionSesion  ==="+claveConfirmacionSesion);
		
		SMS sms = new SMS();
		sms.enviarCodigoVerificacion(numeroCelular, claveConfirmacion.toLowerCase());	
			
	
	
	} catch(AppException e) { 
		e.printStackTrace();						 
		msgError= e.getMessage(); 
	}	
	if(msgError.equals("")) {
		jsonObj.put("success",  new Boolean(true));
		jsonObj.put("mensajeEnviado",  mensajeEnviado);
		jsonObj.put("claveConfirmacionSesion",  claveConfirmacionSesion);	
		jsonObj.put("diasPrevios",  diasPrevios);	
		jsonObj.put("informacion",  informacion);			
	}else  {
		jsonObj.put("success",  new Boolean(false));		
		jsonObj.put("msg",msgError  );
	}		
   infoRegresar = jsonObj.toString();  	
	
	
}else  if(informacion.equals("Confirmar")){	
	
	String codigo ="correcto";	
	try {
	
		System.out.println("claveConfirmacionSesion=====" +claveConfirmacionSesion);
		System.out.println("claveConfirmacion=====" +claveConfirmacion);
	
		if(!claveConfirmacionSesion.equals(claveConfirmacion)){
			mensajeEnviado = "El C�digo de Confirmaci�n no es el correcto."; 
			codigo = "incorrecto";
		}else { 
	
			mensajeEnviado = "El servicio ha sido activado"; 
			List datosActivacionFactorajeMovil = new ArrayList();
			datosActivacionFactorajeMovil.add(icPyme);
			datosActivacionFactorajeMovil.add("1");
			datosActivacionFactorajeMovil.add(momentoEnvioS);
			datosActivacionFactorajeMovil.add(tipoInformacionS);
			datosActivacionFactorajeMovil.add(compCelular.toUpperCase());
						
			servicios.activarServicioFactorajeMovil(datosActivacionFactorajeMovil);
						
			List datosPyme = servicios.getDatosPymeFactorajeMovil(icPyme);
			String numeroCelular = (String)datosPyme.get(1);
				
			SMS sms = new SMS();
			sms.enviarRegistroExitoso(numeroCelular);
		}
	} catch(AppException e) {
		e.printStackTrace();		 
		msgError =  e.getMessage();		
	} 
	if(msgError.equals("")) {
		jsonObj.put("success",  new Boolean(true));
		jsonObj.put("mensajeEnviado",  mensajeEnviado);
		jsonObj.put("codigo", codigo );
		jsonObj.put("informacion",  informacion);			
	}else  {
		jsonObj.put("success",  new Boolean(false));
		jsonObj.put("msg", msgError);
	}
	
	infoRegresar = jsonObj.toString();  	

}

%>
<%=infoRegresar%>