<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.servicios.*,
		javax.naming.Context,
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoEPO,
		com.jspsmart.upload.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
String informacion = (request.getParameter("informacion")!=null)?(String)request.getParameter("informacion"):"";
String infoRegresar="", consulta="";
JSONObject jsonObj = new JSONObject();

Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB",Servicios.class);

if(informacion.equals("catTipoPub")){

	String tipoAfiliado = (((String)request.getParameter("tipoAfi")).equals(""))?"P":request.getParameter("tipoAfi");	
	JSONObject	resultado	= new JSONObject();
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	if(tipoAfiliado.equals("P")){
		registro.put("clave",			"M");
		registro.put("descripcion",	"Mensaje");
		registros.add(registro);
		registro 	= new JSONObject();
		registro.put("clave",			"C");
		registro.put("descripcion",	"Campaña");
		registros.add(registro);
	
	}else{
		registro.put("clave",			"M");
		registro.put("descripcion",	"Mensaje");
		registros.add(registro);
	}
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
	infoRegresar = resultado.toString();

}else if(informacion.equals("catalogoAfiliado")){
	
	String tipoPub = (((String)request.getParameter("tipoPub")).equals(""))?"M":request.getParameter("tipoPub");	
	MensajeParam  mensaje_param = new MensajeParam();
	JSONObject	resultado	= new JSONObject();
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	if(!tipoPub.equals("C")){
		if(!"EPO".equals(strTipoUsuario)){
			registro.put("clave",			"E");
			registro.put("descripcion",	mensaje_param.getMensaje("15admmeninitra01.EPO",sesIdiomaUsuario));
			registros.add(registro);
		
		}
		registro.put("clave",			"I");
		registro.put("descripcion",	mensaje_param.getMensaje("15admmeninitra01.Ifs",sesIdiomaUsuario));
		registros.add(registro);
		registro 	= new JSONObject();
		registro.put("clave",			"P");
		registro.put("descripcion",	mensaje_param.getMensaje("15admmeninitra01.Proveedores",sesIdiomaUsuario));
		registros.add(registro);
	}else{
		registro.put("clave",			"P");
		registro.put("descripcion",	mensaje_param.getMensaje("15admmeninitra01.Proveedores",sesIdiomaUsuario));
		registros.add(registro);	}
		resultado.put("success", 	new Boolean(true)	);
		resultado.put("total", 		new Integer(registros.size())	);
		resultado.put("registros", registros			);
		infoRegresar = resultado.toString();

}else if(informacion.equals("catalogoBancoFon")){

	JSONObject	resultado	= new JSONObject();
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	if(!iTipoPerfil.equals("8")){
		registro.put("clave",			"1");
		registro.put("descripcion",	"NAFIN");
		registros.add(registro);
		registro.put("clave",			"2");
		registro.put("descripcion",	"BANCOMEXT");
		registros.add(registro);
	}else{
		registro.put("clave",			"2");
		registro.put("descripcion",	"BANCOMEXT");
		registros.add(registro);
	}
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
	infoRegresar = resultado.toString();

} else if(informacion.equals("CatalogoEpoData")){
	
	String tipoAfiliado = (((String)request.getParameter("tipoAfi")).equals(""))?"P":request.getParameter("tipoAfi");
	if(tipoAfiliado.equals("E")||tipoAfiliado.equals("P")){
		if(!"EPO".equals(strTipoUsuario)){
			CatalogoEPO catalogo = new CatalogoEPO();
			catalogo.setCampoClave("ic_epo");
			catalogo.setCampoDescripcion("cg_razon_social");
			catalogo.setOrden("cg_razon_social");
			infoRegresar = catalogo.getJSONElementos();
		}
	}
}else if(informacion.equals("CatalogoIFData")){
	
	String tipoAfiliado = (((String)request.getParameter("tipoAfi")).equals(""))?"P":request.getParameter("tipoAfi");
	String tipoPub = (((String)request.getParameter("tipoPub")).equals(""))?"M":request.getParameter("tipoPub");
	if((tipoAfiliado.equals("I")||tipoAfiliado.equals("P"))&&!tipoPub.equals("C")){
		if(!"EPO".equals(strTipoUsuario)){
			CatalogoIF catalogo = new CatalogoIF();
			catalogo.setCampoClave("I.ic_if");
			catalogo.setCampoDescripcion("I.cg_razon_social");
			catalogo.setOrden("2");
			List lista = catalogo.getListaElementosGral();
			JSONArray jsObjArray = new JSONArray();
			jsObjArray = JSONArray.fromObject(lista);
			infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
	
		}else{
			CatalogoIF catalogo = new CatalogoIF();
			catalogo.setCampoClave("I.ic_if");
			catalogo.setCampoDescripcion("I.cg_razon_social");
			catalogo.setClaveEpo(iNoCliente);
			catalogo.setOrden("2");
			List lista = catalogo.getListaElementosGral();
			JSONArray jsObjArray = new JSONArray();
			jsObjArray = JSONArray.fromObject(lista);
			infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
	
		}
		
	}	
	
}else if(informacion.equals("Consultar")){
	
	String selectCadenaDestinoEpo = request.getParameter("selectEpo");
	String cadenaDestinoEpo[]=selectCadenaDestinoEpo.split(",");
	String selectCadenaDestinoIf = request.getParameter("selectIf");
	String cadenaDestinoIf[]=selectCadenaDestinoIf.split(",");
	String tipoAfiliado = (((String)request.getParameter("tipoAfi")).equals(""))?"P":request.getParameter("tipoAfi");	
	String tipoPub = (((String)request.getParameter("tipoPub")).equals(""))?"M":request.getParameter("tipoPub");
	String txtFechaDesde = (request.getParameter("dfFechaPubA")!=null)?request.getParameter("dfFechaPubA"):"";
	String txtFechaHasta = (request.getParameter("dfFechaPubB")!=null)?request.getParameter("dfFechaPubB"):"";
	String txtContenido = (request.getParameter("contenido")!=null)?request.getParameter("contenido"):"";
	String txtTitulo = (request.getParameter("titulo")!=null)?request.getParameter("titulo"):"";
	String txtNomCampaña = (request.getParameter("nomCampaña")!=null)?request.getParameter("nomCampaña"):"";
	String txtArchivo = (request.getParameter("cargaImagen")!=null)?request.getParameter("cargaImagen"):"";
	MensajeAfiliados paginado = new MensajeAfiliados();
	CQueryHelperRegExtJS queryHelper1 = new CQueryHelperRegExtJS( paginado);
	paginado.setTxtFechaDesde(txtFechaDesde);
	paginado.setTxtFechaHasta(txtFechaHasta);
	if(!tipoPub.equals("C")){
		paginado.setTxtTitulo(txtTitulo);
	}else{
		paginado.setTxtTitulo(txtNomCampaña);
	}
	paginado.setTxtContenido(txtContenido);
	paginado.setTipoAfiliado(tipoAfiliado);
	if(!selectCadenaDestinoEpo.equals("")){
		paginado.setCadenaDestino(cadenaDestinoEpo);
	}
	if(!selectCadenaDestinoIf.equals("")){
		paginado.setIfDestino(cadenaDestinoIf);
	}
	try {
		Registros reg	=	queryHelper1.doSearch();
		if (informacion.equals("Consultar") )  {
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);			
		}
	} catch(Exception e) {
		throw new AppException("Error al obtener los datos", e);
	}
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("valoresIniciales")){

	String tipoAfiliado = (((String)request.getParameter("tipoAfi")).equals(""))?"P":request.getParameter("tipoAfi");	
	String tipoPub = (((String)request.getParameter("tipoPub")).equals(""))?"M":request.getParameter("tipoPub");
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("tipoPublicacion", tipoPub);
	jsonObj.put("tipoAfiliado", tipoAfiliado);
	jsonObj.put("iTipoPerfil", iTipoPerfil);
	infoRegresar = jsonObj.toString();	
}else if(informacion.equals("eliminaMensaje")){

	String IC_MENSAJE_INI = request.getParameter("IC_MENSAJE_INI") == null?"":request.getParameter("IC_MENSAJE_INI");
	servicios.bajaMensajeIni(IC_MENSAJE_INI);
	MensajeParam  mensaje_param = new MensajeParam();
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje_param.getMensaje("15admmeninitra01.MensajeEliminado",sesIdiomaUsuario) );
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("modificaMensaje")){

	MensajeParam  mensaje_param = new MensajeParam();
	String ic_mensajeM = request.getParameter("ic_mensajeM") == null?"":request.getParameter("ic_mensajeM");
	String tituloM = request.getParameter("tituloM") == null?"":request.getParameter("tituloM");
	String contenidoM = request.getParameter("contenidoM") == null?"":request.getParameter("contenidoM");
	String dfFechaModA = request.getParameter("dfFechaModA") == null?"":request.getParameter("dfFechaModA");
	String dfFechaModB = request.getParameter("dfFechaModB") == null?"":request.getParameter("dfFechaModB");
	 servicios.modificacionMensajeIni(ic_mensajeM,dfFechaModA,dfFechaModB,tituloM,contenidoM);
	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje_param.getMensaje("15admmeninitra01.DatosFueronActualizados",sesIdiomaUsuario) );
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("cargaArchivo")){

	String icMensajeIni = (request.getParameter("icMensajeIni")!=null)?(String)request.getParameter("icMensajeIni"):"";
	MensajeParam  mensaje_param = new MensajeParam();
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String ruta_archivo			= "";
	String mensaje ="";
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	String nombreArchivo = "";
	jsonObj = new JSONObject();
	try {
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(500000);
		myUpload.upload();
		if(myUpload.getFiles ().getCount()>0)  
      {  
          nombreArchivo =  myUpload.getFiles (). getFile (0).getFileName ();
       }
		 jsonObj.put("success", new Boolean(true));
	} catch(Exception e) {
		jsonObj.put("success", new Boolean(false));
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 500000) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 8 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
		
	}
		if(!"".equals(nombreArchivo)){
			ruta_archivo = strDirTrabajo + "15archcadenas/mensIni" +icMensajeIni  + "/";
			java.io.File fdPath1 = new java.io.File(ruta_archivo);
			if(!fdPath1.exists()) {
				if (!fdPath1.mkdir()) {
					mensaje = mensaje_param.getMensaje("15admmeninitra01.NoPudoCrearDirectorio",sesIdiomaUsuario) + ruta_archivo;
					jsonObj.put("success", new Boolean(false));
				}
			}
			ruta_archivo += "imagenes/";
			java.io.File fdPath2 = new java.io.File(ruta_archivo);
			if(!fdPath2.exists()) {
				if (!fdPath2.mkdir()) {
					mensaje = mensaje_param.getMensaje("15admmeninitra01.NoPudoCrearDirectorio",sesIdiomaUsuario) + ruta_archivo;
					jsonObj.put("success", new Boolean(false));
				}
			}
			myUpload.save(ruta_archivo);
		}
	if(mensaje.equals("")){
		mensaje="Sus datos fueron enviados exitosamente....!!!";
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje);
	jsonObj.put("icMensajeIni",icMensajeIni);
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("vistaPrevia")){
	MensajeParam  mensaje_param = new MensajeParam();
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String ruta_archivo			= "";
	String mensaje ="";
	int 	icMensajeIni	= 0;
	
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	String nombreArchivo = "";
	try {
			myUpload.initialize(pageContext);
			myUpload.setTotalMaxFileSize(500000);
			myUpload.upload();
			if(myUpload.getFiles ().getCount()>0)  
			{  
				nombreArchivo =  myUpload.getFiles (). getFile (0).getFileName ();
			}
		} catch(Exception e) {
			success		= false;
			log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
			e.printStackTrace();
			if(myUpload.getSize() > 500000) {
				throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 8 MB.");
			} else {
				throw new AppException("Problemas al Subir el Archivo."); 
			}
			
		}
		if(!"".equals(nombreArchivo)){
			ruta_archivo = strDirTrabajo + "15archcadenas/tmp_vistaprevia/";
			java.io.File fdPath1 = new java.io.File(ruta_archivo);
			if(!fdPath1.exists()) {
				if (!fdPath1.mkdir()) {
					mensaje = mensaje_param.getMensaje("No se pudo crear el directorio:",sesIdiomaUsuario) + ruta_archivo;
				}
			}
			ruta_archivo += "imagenes/";
			java.io.File fdPath2 = new java.io.File(ruta_archivo);
			if(!fdPath2.exists()) {
				if (!fdPath2.mkdir()) {
					mensaje = mensaje_param.getMensaje("No se pudo crear el directorio:",sesIdiomaUsuario) + ruta_archivo;
				}
			}
			myUpload.save(ruta_archivo);
		}
	String strDirecVirtualTempAux= strDirecVirtualTrabajo +"15archcadenas/tmp_vistaprevia/imagenes/" ;
	JSONObject jsonObj3 = new JSONObject();
	jsonObj3.put("success", new Boolean(true));
	jsonObj3.put("urlArchivo", strDirecVirtualTempAux+nombreArchivo);
	jsonObj3.put("nombreArchivo", nombreArchivo);
	infoRegresar = jsonObj3.toString();
}else if(informacion.equals("regresar")){
	String nombreArch = (request.getParameter("nombreArch")!=null)?(String)request.getParameter("nombreArch"):"";
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String ruta_archivo			= "";
	String mensaje ="";
	int 	icMensajeIni	= 0;
	String rutarchivo = strDirTrabajo + "15archcadenas/tmp_vistaprevia/imagenes/"+nombreArch;
	java.io.File feliminar = new java.io.File(rutarchivo);
	if(feliminar.exists()){
		log.debug("Se eliminara archivo");
		if(feliminar.delete())
			log.debug("Se eliminó archivo");
	}
	rutarchivo = strDirTrabajo + "15archcadenas/tmp_vistaprevia/imagenes";
	feliminar = new java.io.File(rutarchivo);
	if(feliminar.exists()){
		log.debug("Se eliminara carpeta imagenes");
		if(feliminar.delete())
			log.debug("Se eliminó carpeta imagenes");
	}
	rutarchivo = strDirTrabajo + "15archcadenas/tmp_vistaprevia";
	feliminar = new java.io.File(rutarchivo);
	if(feliminar.exists()){
		log.debug("Se eliminara carpeta tmp_vistaprevia");
		if(feliminar.delete())
			log.debug("Se eliminó carpeta tmp_vistaprevia");
	}
	JSONObject jsonObj3 = new JSONObject();
	jsonObj3.put("success", new Boolean(true));
	jsonObj3.put("mensaje","Se eliminó carpeta tmp_vistaprevia" );
	infoRegresar = jsonObj3.toString();
}else if(informacion.equals("Confirmacion")){
	String nombreArch = (request.getParameter("nombreArch")!=null)?(String)request.getParameter("nombreArch"):"";
	MensajeParam  mensaje_param = new MensajeParam();
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String ruta_archivo			= "";
	String mensaje ="";
	int 	icMensajeIni	= 0;   
	String selectCadenaDestinoEpo = request.getParameter("selectEpo");  
	String cadenaDestinoEpo[]=selectCadenaDestinoEpo.split(",");
	String selectCadenaDestinoIf = request.getParameter("selectIf");
	String cadenaDestinoIf[]=selectCadenaDestinoIf.split(",");
	String tipoAfiliado = (((String)request.getParameter("tipoAfi")).equals(""))?"P":request.getParameter("tipoAfi");	
	String tipoPub = (((String)request.getParameter("tipoPub")).equals(""))?"M":request.getParameter("tipoPub");
	String txtFechaDesde = (request.getParameter("dfFechaPubA")!=null)?request.getParameter("dfFechaPubA"):"";
	String txtFechaHasta = (request.getParameter("dfFechaPubB")!=null)?request.getParameter("dfFechaPubB"):"";
	String txtContenido = (request.getParameter("contenido")!=null)?request.getParameter("contenido"):"";
	String txtTitulo = (request.getParameter("titulo")!=null)?request.getParameter("titulo"):"";
	String txtNomCampaña = (request.getParameter("nomCampaña")!=null)?request.getParameter("nomCampaña"):"";
	if(tipoPub.equals("C")){
		txtTitulo = txtNomCampaña;
	}
	icMensajeIni = servicios.guardaMensajeIni(tipoAfiliado,cadenaDestinoEpo,cadenaDestinoIf,txtFechaDesde,txtFechaHasta,txtTitulo,txtContenido,nombreArch,tipoPub);
		
	if(icMensajeIni!=0){
		mensaje="Sus datos fueron enviados exitosamente....!!!";
	}
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje);
	jsonObj.put("icMensajeIni",String.valueOf(icMensajeIni));
	infoRegresar = jsonObj.toString();
}

%>

<%=infoRegresar%>

