Ext.onReady(function() {
	var iTipoPerfil =  Ext.getDom('iTipoPerfil').value;
	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);

/*--------------------------------- HANDLERS -------------------------------*/
	function procesaValoresFecha(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
			 var fechaPubIni =jsonData.fecha_in;
			 var fechaPubFin =jsonData.fecha_fin;
			 Ext.getCmp('dfFechaPubA').setValue(fechaPubIni);
			 Ext.getCmp('dfFechaPubB').setValue(fechaPubFin);
			 Ext.getCmp('dfFechaPubAux').setValue(fechaPubIni);
			 Ext.getCmp('dfFechaPubBAux').setValue(fechaPubFin);
			}
		} else {
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	
	}
	function mostrarArchivoCSV(opts, success, response) {//mostrarArchivoCSVDetalle
		Ext.getCmp('btnGenerarArchivo').enable();
		var btnImprimirCSV = Ext.getCmp('btnGenerarArchivo');
		btnImprimirCSV.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function mostrarArchivoCSVDetalle(opts, success, response) {//mostrarArchivoCSVDetalle
		Ext.getCmp('btnGenerarArchivoVistaD').enable();
		var btnImprimirCSV = Ext.getCmp('btnGenerarArchivoVistaD');
		btnImprimirCSV.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function mostrarArchivoCSVD(opts, success, response) {
		Ext.getCmp('btnGenerarArchivoD').enable();
		var btnImprimirCSVD= Ext.getCmp('btnGenerarArchivoD');
		btnImprimirCSVD.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
	var procesarConsultaDataD = function (store, arrRegistros, opts) {
		var fpCons = Ext.getCmp('formaD');
		fpCons.el.unmask();
		var mask = new Ext.LoadMask(Ext.get('VerDetalle'), {msg:'Cargando...'});
		mask.hide();
		if(arrRegistros!= null) {
			var gridConsultaD = Ext.getCmp('gridConsultaD');
			if(!gridConsultaD.isVisible()) {
				gridConsultaD.show();
			}
			var el = gridConsultaD.getGridEl();  
			var btnGenerarArchivoD = Ext.getCmp('btnGenerarArchivoVistaD');
			if(store.getTotalCount() > 0) {
				el.unmask();
				btnGenerarArchivoD.enable();
				consultaTotalesD.load({
					params: Ext.apply({
						informacion: 'consultarTotalesD',
						start:0,
						limit:15,
						campana:Ext.getCmp('tfNumCampAux').getValue(),
						epo:Ext.getCmp('tfNumEpoAux').getValue(),
						accion:'verDetalle'
					})
				});
			}else {
				el.mask('No se encontr� ning�n registro','x-mask');
				btnGenerarArchivoD.disable();
				Ext.getCmp('gridConsultaTotalesD').hide();
			}
		}
	}
	var procesarConsultaData = function (store, arrRegistros, opts) {
		var fpCons = Ext.getCmp('forma');
		fpCons.el.unmask();
		if(arrRegistros!= null) {
			var gridConsulta = Ext.getCmp('gridConsulta');
			if(!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
			var cm = gridConsulta.getColumnModel();
			var estatus = Ext.getCmp('cmbEstatusResp').getValue();
			if(estatus=='2'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SI'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO_CONTE'), true);

			}else if(estatus=='3'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SI'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO_CONTE'), true);

			}else if(estatus=='4'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SI'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO_CONTE'), false);
			}else{
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SI'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO_CONTE'), false);
			}
			
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnGenerarArchivoD = Ext.getCmp('btnGenerarArchivoD');	
			var el = gridConsulta.getGridEl();
			
			if(store.getTotalCount() > 0) {
				consultaTotales.load({
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'consultarTotales',
					start:0,
					limit:15,
					accion:'Total'
				})
			});
				btnGenerarArchivoD.enable();
				btnGenerarArchivo.enable();
				el.unmask();
			}else {
				btnGenerarArchivoD.disable();
				btnGenerarArchivo.disable();
				el.mask('No se encontr� ning�n registro','x-mask');
				Ext.getCmp('gridConsultaTotales').hide();
			}
		}
	}
	var procesarDetalle= function(grid, rowIndex, colIndex, item, event) {
		var camp = Ext.getCmp('cmbCampana').getRawValue();
		var registro = grid.getStore().getAt(rowIndex);
		var MSG_INI = registro.get('MSG_INI');  
		var RZSOCIAL = registro.get('RZSOCIAL');  
		var TOTAL_PYMES = registro.get('TOTAL_PYMES');  
		var SI = registro.get('SI');  
		var NO = registro.get('NO');  
		var EPO = registro.get('EPO');  
		var TOTAL_NO_CONTE = registro.get('TOTAL_NO_CONTE');  
		var tituloF ='EPO: '+RZSOCIAL+'	CAMPA�A:	'+camp;
		storeCatRespuesta.load();
		Ext.getCmp('tfNumEpoAux').setValue(EPO);
		Ext.getCmp('tfNumCampAux').setValue(MSG_INI);
		var gridConsultaD = Ext.getCmp('gridConsultaD');
		if(!gridConsultaD.isVisible()) {
			gridConsultaD.show();
		}
		ConsultaDataD.load({
			params: Ext.apply({
				informacion: 'consultarD',
				start: 0,
				cmbRespuesta:Ext.getCmp('cmbRespuestas').getValue(),
				accion:'verDetalle',
				limit: 15,
				epo:EPO,
				operacion:'Generar',
				campana:MSG_INI
			})
		});
		var ProcesarDetalle = Ext.getCmp('VerDetalle');
		if(ProcesarDetalle){
			ProcesarDetalle.show();
		}else{
		new Ext.Window({	
					modal: true,
					width: 770,
					closeAction: 'hide',
					resizable: false,
					closable:false,
					autoHeight: true,
					id: 'VerDetalle',
					items: [	
						NE.util.getEspaciador(10),
						fpD,
						NE.util.getEspaciador(10),
						gridConsultaD,
						NE.util.getEspaciador(10),
						gridConsultaTotalesD
					],
					bbar: {
						xtype: 'toolbar',	buttonAlign:'center',	
						buttons: ['-',
							{	xtype: 'button',	text: 'Cerrar',	iconCls: 'icoLimpiar', 	id: 'btnCerrar',
								handler: function(){																		
									Ext.getCmp('VerDetalle').hide ();
								} 
							},
							{
								xtype: 'button',
								text: 'Generar Archivo',
								id: 'btnGenerarArchivoVistaD',
								iconCls: 'icoXls',
								handler: function(boton, evento) {
									var selCadena = "";
									var i=0;
									selCadena = camp+'|'+RZSOCIAL +'|'+TOTAL_PYMES+'|'+TOTAL_PYMES+'|'+SI+'|'+NO+'|'+TOTAL_NO_CONTE+'|'+EPO+'|'+MSG_INI+'@';
									boton.disable();
									boton.setIconClass('loading-indicator');
									Ext.Ajax.request({
										url:'15estadisticasCampExt01.data.jsp',
										params: Ext.apply({					
											informacion:'GenerarArchivoCSVDetalle',
											selCadena : selCadena,
											accion :'AD',
											tipo:'CSV',
											campana:Ext.getCmp('tfNumCampAux').getValue(MSG_INI)
										
										}),
										callback: mostrarArchivoCSVDetalle
									});		
								}
							}
						]
					}
				}).show().setTitle(tituloF);
				var mask = new Ext.LoadMask(Ext.get('VerDetalle'), {msg:'Cargando...'});
				mask.show();
			}
	}
	var procesarConsultaTotalesD = function(store, arrRegistros, opts) 	{
		
		var gridConsultaTotalesD = Ext.getCmp('gridConsultaTotalesD');	
		var el = gridConsultaTotalesD.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsultaTotalesD.isVisible()) {
				gridConsultaTotalesD.show();
			}
			if(store.getTotalCount() > 0) {					
				el.unmask();				
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var procesarConsultaTotales = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaTotales = Ext.getCmp('gridConsultaTotales');	
		var el = gridConsultaTotales.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsultaTotales.isVisible()) {
				gridConsultaTotales.show();
			}
			var cm = gridConsultaTotales.getColumnModel();
			var estatus = Ext.getCmp('cmbEstatusResp').getValue();
			if(estatus=='2'){
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_SI'), false);	
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO'), true);
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO_CONTES'), true);

			}else if(estatus=='3'){
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_SI'), true);	
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO'), false);
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO_CONTES'), true);

			}else if(estatus=='4'){
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_SI'), true);	
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO'), true);
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO_CONTES'), false);
			}else{
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_SI'), false);	
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO'), false);
				gridConsultaTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_NO_CONTES'), false);
			}
			
			if(store.getTotalCount() > 0) {					
				el.unmask();				
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	//-------------------------------- STORES -----------------------------------
	var ConsultaDataD = new Ext.data.JsonStore({
		root: 'registros',
		url: '15estadisticasCampExt01.data.jsp',
		baseParams: {
			informacion: 'consultarD'
		},
		fields: [
					{name: 'ICPYME'},
					{name: 'NUMPYME'},
					{name: 'PYME'},
					{name: 'RFC'},
					{name: 'CONTACTO'},
					{name: 'MAIL'},
					{name: 'EDO'},
					{name: 'TEL'},
					{name: 'RESPUESTA'},
					{name: 'FECHA'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
						Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues()),
						campana: Ext.getCmp('cmbCampana').getValue()
						
						});
					}},
			load: procesarConsultaDataD,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaDataD(null,null,null);
				}
			}
		}
	});
	var ConsultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15estadisticasCampExt01.data.jsp',
		baseParams: {
			informacion: 'consultar'
		},
		fields: [
					{name: 'MSG_INI'},
					{name: 'RZSOCIAL'},
					{name: 'TOTAL_PYMES'},
					{name: 'SI'},
					{name: 'NO'},
					{name: 'EPO'},
					{name: 'TOTAL_NO_CONTE'},
					{name: 'SEL'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
						Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues()),
						campana: Ext.getCmp('cmbCampana').getValue()
						
						});
					}},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});
	var consultaTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '15estadisticasCampExt01.data.jsp',
		baseParams: {
			informacion: 'consultarTotales'			
		},
		hidden: true,
		fields: [					
			{	name: 'TOTAL_PYME'},
			{	name: 'TOTAL_SI'}, 
			{	name: 'TOTAL_NO'}, 
			{	name: 'TOTAL_NO_CONTES'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotales,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotales(null, null, null);					
				}
			}
		}					
	});
	var consultaTotalesD = new Ext.data.JsonStore({
		root : 'registros',
		url : '15estadisticasCampExt01.data.jsp',
		baseParams: {
			informacion: 'consultarTotalesD'			
		},
		hidden: true,
		fields: [					
			{	name: 'TOTAL'},
			{	name: 'TOTAL_S'}, 
			{	name: 'TOTAL_N'}, 
			{	name: 'TOTAL_NC'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesD,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesD(null, null, null);					
				}
			}
		}					
	});
	var procesarCatEpo = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cveEPO = Ext.getCmp('cmbEpo');
			if(cveEPO.getValue()==''){
				var newRecord = new recordType({
					clave:'0',
					descripcion:'Todas las EPOs'
				});
				store.insert(0,newRecord);
				store.commitChanges();
				cveEPO.setValue('');
			}
		}
  }
  var storeCatRespuesta = new Ext.data.JsonStore({
		id: 'catRespuesta',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15estadisticasCampExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var storeCatCampana = new Ext.data.JsonStore({
		id: 'catCampana',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15estadisticasCampExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoCampana'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	var storeCatEstatus = new Ext.data.JsonStore({
		id: 'catEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15estadisticasCampExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	var storeCatEPO = new Ext.data.JsonStore({
		id: 'catEPO',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15estadisticasCampExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load:procesarCatEpo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
//----------------------------ELEMENTOS----------------------------------

	var selectModel = new Ext.grid.CheckboxSelectionModel( {
	  	listeners: {
			rowdeselect: function(selectModel, rowIndex, record) {	// cuando se quita la selecci�n
				record.data['SEL']='N';
				record.commit();				
			},
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //Se activa cuando una fila est� seleccionada, return false para cancelar.		
				record.data['SEL']='S';	
				record.commit();
			}		
		}
	});
	var gridConsultaD = new Ext.grid.EditorGridPanel({
		id : 'gridConsultaD',
		store: ConsultaDataD,
		emptyMsg: "No hay registros.",		
		title: 'Consulta RESPUESTA',
		hidden:	true,
		frame: true,
		loadMask: true,
		stripeRows: true,
		style: 'margin: 0 auto;',
		columns: [
			{
				header: 'N�m. Pyme',
				dataIndex: 'NUMPYME',
				sortable: true,
				resizable: true,
				align: 'center',
				width: 100
			},{
				header: '<center>PYME</center>',
				dataIndex: 'PYME',
				width: 200,
				sortable: true,
				resizable: true,
				align: 'left'
			},{
				header: 'RFC',
				dataIndex: 'RFC',
				width: 100,
				sortable: true,
				resizable: true,
				hidden:false,
				align: 'center'
			},{
				header: '<center>Contacto</center>',
				dataIndex: 'CONTACTO',
				width: 200,
				sortable: true,
				resizable: true,
				hidden:false,
				align: 'left'
			},{
				header: '<center>E-mail</center>',
				dataIndex: 'MAIL',
				width: 150,	
				sortable: true,
				resizable: true,
				hidden:false,
				align: 'left'
			},{
				header: '<center>Estado</center>',
				dataIndex: 'EDO',
				width: 100,
				sortable: true,
				resizable: true,
				hidden:false,
				align: 'left'
			},{
				header: 'Tel�fono',
				dataIndex: 'TEL',
				width: 100,
				sortable: true,
				resizable: true,
				hidden:false,
				align: 'center'
			},{
				header: 'Respuesta',
				dataIndex: 'RESPUESTA',
				width: 100,	
				sortable: true,
				resizable: true,
				hidden:false,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('RESPUESTA') =='S' ) {
						return 'Si';
					}else if(registro.get('RESPUESTA') =='N' ) {
						return 'No';
					}else{
						return 'No contestada';
					}				
				}
			},{
				header: 'Fecha de Respuesta',
				dataIndex: 'FECHA',   
				width: 100,	
				sortable: true,
				resizable: true,
				hidden:false,
				align: 'center'
			}
		],
		width: 750,
		height: 350
	});
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id : 'gridConsulta',
		store: ConsultaData,
		emptyMsg: "No hay registros.",		
		title: 'Consulta',
		hidden:	false,
		sm: selectModel,
		frame: true,
		loadMask: true,
		stripeRows: true,
		style: 'margin: 0 auto;',
		hidden:true,
		columns: [
			selectModel,
			{
				header: 'EPO',
				dataIndex: 'RZSOCIAL',
				sortable: true,
				resizable: true,
				align: 'left',
				width: 200
			},{
				header: 'Total de Pymes',
				dataIndex: 'TOTAL_PYMES',
				width: 100,
				sortable: true,
				resizable: true,
				align: 'center'
			},{
				header: '<center>Total de pymes que<br>respondieron que si</center>',
				dataIndex: 'SI',
				width: 150,	
				sortable: true,
				resizable: true,
				hidden:true,
				align: 'center'
			},{
				header: '<center>Total de pymes que<br>respondieron que no</center>',
				dataIndex: 'NO',
				width: 150,	
				sortable: true,
				resizable: true,
				hidden:true,
				align: 'center'
			},{
				header: '<center>Total de pymes que<br>no han contestado</center>',
				dataIndex: 'TOTAL_NO_CONTE',
				width: 150,	
				sortable: true,
				resizable: true,
				hidden:true,
				align: 'center'
			},{
				xtype		: 'actioncolumn',
				header: 'Ver detalle',
				id			: 'dtnEliminar',
				width		:  100,	
				sortable: true,
				resizable: true,
				align		: 'center', 
				hidden	: false,
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'icoBuscar';
						},
						handler:procesarDetalle
					}
				]
		  }
		],
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: ConsultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No se encontro ning�n registro",
			items: [
				'->',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						var countChecksE =0;
						var store = gridConsulta.getStore();		
						store.each(function(record) {
							if(record.data['SEL']=='S')  {
								countChecksE++;
							}
						});
						if (countChecksE==0){
							Ext.Msg.alert(boton.text,'Favor de seleccionar la(s) cadena(s) a procesar');
								return;
							}
							var selCadena = "";
							var i=0;
							var cam = Ext.getCmp('cmbCampana').getRawValue();
							store.each(function(record) {
								if (record.data['SEL']=='S'){
									if(i==0){
										selCadena += cam+'|'+record.data['RZSOCIAL'] +'|'+record.data['TOTAL_PYMES']+'|'+record.data['TOTAL_PYMES']+'|'+record.data['SI']+'|'+record.data['NO']+'|'+record.data['TOTAL_NO_CONTE']+'|'+record.data['EPO']+'|'+record.data['MSG_INI']+'@';
									}else{
										selCadena += cam+'|'+record.data['RZSOCIAL'] +'|'+record.data['TOTAL_PYMES']+'|'+record.data['TOTAL_PYMES']+'|'+record.data['SI']+'|'+record.data['NO']+'|'+record.data['TOTAL_NO_CONTE']+'|'+record.data['EPO']+'|'+record.data['MSG_INI']+'@';

									}
									i++;
								}
							});
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url:'15estadisticasCampExt01.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{					
								informacion:'GenerarArchivoCSV',
								selCadena : selCadena,
								accion :'A',
								tipo:'CSV'
							}),
							callback: mostrarArchivoCSV
						});		
					}
				},				
				{
					xtype: 'button',
					text: 'Generar Archivo con Detalle',
					iconCls:	'icoXls',
					id: 'btnGenerarArchivoD',
					handler: function(boton, evento) {
						var countChecksE =0;
						var store = gridConsulta.getStore();		
						store.each(function(record) {
							if(record.data['SEL']=='S')  {
								countChecksE++;
							}
						});
						if (countChecksE==0){
							Ext.Msg.alert(boton.text,'Favor de seleccionar la(s) cadena(s) a procesar');
								return;
							}
							var selCadena = "";
							var i=0;
							var cam = Ext.getCmp('cmbCampana').getRawValue();
							store.each(function(record) {
							if (record.data['SEL']=='S'){
								if(i==0){
									selCadena += cam+'|'+record.data['RZSOCIAL'] +'|'+record.data['TOTAL_PYMES']+'|'+record.data['TOTAL_PYMES']+'|'+record.data['SI']+'|'+record.data['NO']+'|'+record.data['TOTAL_NO_CONTE']+'|'+record.data['EPO']+'|'+record.data['MSG_INI']+'@';
								}else{
									selCadena += cam+'|'+record.data['RZSOCIAL'] +'|'+record.data['TOTAL_PYMES']+'|'+record.data['TOTAL_PYMES']+'|'+record.data['SI']+'|'+record.data['NO']+'|'+record.data['TOTAL_NO_CONTE']+'|'+record.data['EPO']+'|'+record.data['MSG_INI']+'@';

								}
								i++;
							}
						});
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url:'15estadisticasCampExt01.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{					
								informacion:'GenerarArchivoCSVD',
								selCadena : selCadena,
								accion :'AD',
								tipo:'CSV'
								
							}),
							callback: mostrarArchivoCSVD
						});		
					}
				}			
			]
		},	
		width: 630,
		height: 410
	});
	var gridConsultaTotalesD = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesD,
		id: 'gridConsultaTotalesD',
		emptyMsg: "No hay registros.",	
		title: 'Total detalle',
		hidden: true,
		frame: true,
		loadMask: true,
		stripeRows: true,
		style: 'margin:0 auto;',
		autoHeight: true,
		columns: [	
			{
				header: 'PYMES Si<br>interesadas',
				tooltip: 'PYMES Si interesadas',
				dataIndex: 'TOTAL_S',
				sortable: true,
				width: 100,			
				resizable: true,					
				align: 'center'
			},
			{
				header: 'PYMES No<br>interesadas',
				tooltip: 'PYMES No interesadas',
				dataIndex: 'TOTAL_N',
				sortable: true,
				width:  100,
				hidden:false,
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'PYMES Sin<br>contestar',
				tooltip: 'PYMES Sin contestar',
				dataIndex: 'TOTAL_NC',
				sortable: true,
				width: 100,	
				hidden:false,
				resizable: true,					
				align: 'center'
			},
			{
				header: 'Total',
				tooltip: 'Total',
				dataIndex: 'TOTAL',
				sortable: true,
				width:  100,
				hidden:false,
				resizable: true,					
				align: 'center'
			}
		],		
		width: 600
	});	
	var gridConsultaTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotales,
		id: 'gridConsultaTotales',
		emptyMsg: "No hay registros.",	
		title: 'Total',
		hidden: true,
		frame: true,
		loadMask: true,
		stripeRows: true,
		style: 'margin:0 auto;',
		autoHeight: true,//150,
		columns: [	
			{
				header: 'Total de Pymes',
				tooltip: 'Total de Pymes',
				dataIndex: 'TOTAL_PYME',
				sortable: true,
				width: 100,			
				resizable: true,					
				align: 'center'
			},
			{
				header: 'Total de Pymes que<br>respondieron que si',
				tooltip: 'Total de Pymes que respondieron que si',
				dataIndex: 'TOTAL_SI',
				sortable: true,
				width:  150,
				hidden:true,
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Total de Pymes que<br>respondieron que no',
				tooltip: 'Total de Pymes que respondieron que no',
				dataIndex: 'TOTAL_NO',
				sortable: true,
				width: 150,	
				hidden:true,
				resizable: true,					
				align: 'center'
			},
			{
				header: 'Total de Pymes que<br>no han contestado',
				tooltip: 'Total de Pymes que no han contestado',
				dataIndex: 'TOTAL_NO_CONTES',
				sortable: true,
				width:  150,
				hidden:true,
				resizable: true,					
				align: 'center'
			}
		],		
		width: 600
	});	
	var elementosFormaD =  [	
		 {
			xtype: 'combo',
			name: 'cmbRespuesta',
			id: 'cmbRespuestas',
			fieldLabel: 'Respuesta',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cmbRespuesta',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : storeCatRespuesta,
			width: 270,
			tpl : NE.util.templateMensajeCargaCombo
	  },
	  {
			xtype: 'textfield',
			name: 'numEpoAux',
			id: 'tfNumEpoAux',
			allowBlank: true,
			width: 100,
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',			
			valueField: 'clave',
			hiddenName : 'numEpoAux',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			minChars : 1,
			maskRe:/[0-9]/
		},
	  {
			xtype: 'textfield',
			name: 'numCampAux',
			id: 'tfNumCampAux',
			allowBlank: true,
			width: 100,
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',			
			valueField: 'clave',
			hiddenName : 'numCampAux',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			minChars : 1,
			maskRe:/[0-9]/
		}
	];
	var elementosForma =  [	
	  {
			xtype: 'combo',
			name: 'campana',
			id: 'cmbCampana',
			fieldLabel: 'Campa�a',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'campana',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : storeCatCampana,
			width: 270,
			listeners: {
				select: {
					fn: function(combo) {
						Ext.Ajax.request({
							url: '15estadisticasCampExt01.data.jsp',
							params:Ext.apply(fp.getForm().getValues(),{
								informacion: 'fechaPublicacion'
							}),
							callback: procesaValoresFecha
						});
						Ext.getCmp('cmbEstatusResp').setValue('');
						Ext.getCmp('cmbEpo').setValue('');
						storeCatEPO.load({ params: Ext.apply(fp.getForm().getValues())});								
					}
				}
			},
			tpl : NE.util.templateMensajeCargaCombo
	  },
	  {
			xtype: 'combo',
			name: 'estatusResp',
			id: 'cmbEstatusResp',
			fieldLabel: 'Estatus de Respuesta',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'estatusResp',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : storeCatEstatus,
			width: 270,
			tpl : NE.util.templateMensajeCargaCombo
	  },
	  {
			xtype: 'compositefield',
			fieldLabel: 'Fecha de publicaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'displayfield',
					value: 'De',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'dfFechaPubA',
					id: 'dfFechaPubA',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'dfFechaPubB',
					startDay: 0,
					width: 100,
					maxLength: 10,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 25
				},
				{
					xtype: 'datefield',
					name: 'dfFechaPubB',
					id: 'dfFechaPubB',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoInicioFecha: 'dfFechaPubA',
					startDay: 1,
					width: 100,
					maxLength: 10,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			hidden:true,
			items: [
				{
					xtype: 'datefield',
					name: 'dfFechaPubAux',
					id: 'dfFechaPubAux',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'dfFechaPubBAux',
					startDay: 0,
					width: 100,
					maxLength: 10,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'datefield',
					name: 'dfFechaPubBAux',
					id: 'dfFechaPubBAux',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoInicioFecha: 'dfFechaPubAux',
					startDay: 1,
					width: 100,
					maxLength: 10,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'  
				}
			]
	   },
	  {
			xtype: 'combo',
			name: 'epo',
			id: 'cmbEpo',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'epo',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : storeCatEPO,
			width: 270
	  }
	];
	var fpD = new Ext.form.FormPanel({
		id: 'formaD',
		width: 500,
		title: '',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		labelWidth: 150,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaD,
		monitorValid: true,
		buttons: [
			{
				text : 'Consultar',
				id   : 'btnConsultar',
				iconCls :'icoBuscar',
				formBind: true,	
				handler: function (boton, evento){
					var cmbResp = Ext.getCmp('cmbRespuestas');
					if(Ext.isEmpty(cmbResp.getValue())){
						camp.markInvalid("Debe Seleccionar una Respuesta");
						return;
					}
					fpD.el.mask('Consultando...', 'x-mask-loading');
					ConsultaDataD.load({
						params: Ext.apply({
							informacion: 'consultarD',
							cmbRespuesta:Ext.getCmp('cmbRespuestas').getValue(),
							start: 0,
							limit: 15,
							campana:Ext.getCmp('tfNumCampAux').getValue(),
							epo:Ext.getCmp('tfNumEpoAux').getValue(),
							accion:'verDetalle',
							operacion:'Generar'
						})
					});
				}
			}
		]
	});
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: '<center>Estad�sticas Env�o Campa�as</center>',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		labelWidth: 150,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text : 'Consultar',
				id   : 'btnConsultar',
				iconCls :'icoBuscar',
				formBind: true,	
				handler: function (boton, evento){
					var camp = Ext.getCmp('cmbCampana');
					var fechaIni = Ext.getCmp('dfFechaPubA');
					var fechaFin = Ext.getCmp('dfFechaPubB');
					var fechaInAux= Ext.getCmp('dfFechaPubAux').getValue();
					var fechaFinAux= Ext.getCmp('dfFechaPubBAux').getValue();
					var fechaIni1  = Ext.util.Format.date(Ext.getCmp('dfFechaPubA').getValue(),'d/m/Y'); 	
					var fechaFinal1  = Ext.util.Format.date(Ext.getCmp('dfFechaPubB').getValue(),'d/m/Y'); 	
					var fechaIni2  = Ext.util.Format.date(Ext.getCmp('dfFechaPubAux').getValue(),'d/m/Y'); 
					var fechaFinal2  = Ext.util.Format.date(Ext.getCmp('dfFechaPubBAux').getValue(),'d/m/Y'); 	
					if(Ext.isEmpty(fechaIni.getValue())&&Ext.isEmpty(fechaFin.getValue())){
						Ext.getCmp('dfFechaPubA').setValue(fechaInAux);
						Ext.getCmp('dfFechaPubB').setValue(fechaFinAux);
					}else if((datecomp(fechaIni1,fechaIni2)==2) || datecomp(fechaFinal1,fechaFinal2)==1){
						Ext.Msg.show({
								//title: 'Crear Grupo',
								msg: 'Las fechas no coinciden con las de la publicaci�n de la campa�a',
								modal: true,
								icon: Ext.Msg.WARNING,
								buttons: Ext.Msg.OK,
								fn: function peticionAjax(btn){
										if (btn == 'ok'){
											Ext.getCmp('dfFechaPubA').setValue(fechaInAux);
											Ext.getCmp('dfFechaPubB').setValue(fechaFinAux);
										}
									}
								});
						return;
					}
					if(!Ext.isEmpty(fechaIni.getValue()) || !Ext.isEmpty(fechaFin.getValue())){
						if(Ext.isEmpty(fechaIni.getValue())){
							Ext.getCmp('dfFechaPubA').setValue(fechaInAux);
						}
						if(Ext.isEmpty(fechaFin.getValue())){
							Ext.getCmp('dfFechaPubB').setValue(fechaFinAux);
						}
					}
					if(Ext.isEmpty(camp.getValue())){
						camp.markInvalid("Debe Seleccionar una Campa�a");
						return;
					}
					fp.el.mask('Consultando...', 'x-mask-loading');
					ConsultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'consultar',
							start: 0,
							accion:'consultaG',
							limit: 15,
							operacion:'Generar',
							cmbRespuesta:Ext.getCmp('cmbEstatusResp').getValue()
						})
					});
				}
			},
			{
				text : 'Regresar',
				id   : 'btnRegresar',
				iconCls :'icoRegresar',
				handler: function (boton, evento){
					window.location.href='15estadisticasCampExt01.jsp';
				}
			}
		]
	});
//-------------------------------- PRINCIPAL -----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),	
			gridConsulta,
			NE.util.getEspaciador(20),	  
			gridConsultaTotales
		]
	});
//-------------------------------- INICIO -----------------------------------
	storeCatCampana.load({ params: Ext.apply(fp.getForm().getValues())});
	storeCatEstatus.load();

});