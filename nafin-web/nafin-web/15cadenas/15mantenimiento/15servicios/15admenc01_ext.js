var pregSeleccionadaI = "";
var pregSeleccionadaE = "";
var pregSeleccionadaP = "";
var pregTituloEncSelecI = "";
var pregTituloEncSelecE = ""
var pregTituloEncSelecP = ""
var tipoAfiliadoGral = "I";

var plantSeleccionadaI = "";
var plantSeleccionadaE = "";
var plantSeleccionadaP = "";

function asignaSeleccion (valorSelec, tituloSelec, afiliado){
	
	if(afiliado=='I'){
		pregSeleccionada = valorSelec;
		pregTituloEncSelecI = tituloSelec;
	}
	if(afiliado=='E'){
		pregSeleccionadaE = valorSelec;
		pregTituloEncSelecE = tituloSelec;
	}
	if(afiliado=='P'){
		pregSeleccionadaP = valorSelec;
		pregTituloEncSelecP = tituloSelec;
	}
	
}

function asignaPlantilla(valorSelec, tituloSelec, afiliado){
	if(afiliado=='I'){
		plantSeleccionadaI = valorSelec;
	}
	if(afiliado=='E'){
		plantSeleccionadaE = valorSelec;
	}
	if(afiliado=='P'){
		plantSeleccionadaP = valorSelec;
	}
}

Ext.onReady(function() {
Ext.override(Ext.dd.DragTracker, {
    onMouseMove: function (e, target) {
        var isIE9 = Ext.isIE && (/msie 9/.test(navigator.userAgent.toLowerCase())) && document.documentMode != 6;
        if (this.active && Ext.isIE && !isIE9 && !e.browserEvent.button) {
            e.preventDefault();
            this.onMouseUp(e);
            return;
        }
        e.preventDefault();
        var xy = e.getXY(), s = this.startXY;
        this.lastXY = xy;
        if (!this.active) {
            if (Math.abs(s[0] - xy[0]) > this.tolerance || Math.abs(s[1] - xy[1]) > this.tolerance) {
                this.triggerStart(e);
            } else {
                return;
            }
        }
        this.fireEvent('mousemove', this, e);
        this.onDrag(e);
        this.fireEvent('drag', this, e);
    }
});



//-----------------------------------------------------------------------------
var reseteaTodo = function(tipoAfil, opcion){
	if(tipoAfil=='I'){
		if(opcion=='Alta'){
			fpAltaI.show();
			fpAltaI.getForm().reset();
			if(fpRespuestasI!=null)fpRespuestasI.destroy();
			if(fpPreguntasI!=null)fpPreguntasI.destroy();
			fpConsultaI.reset()
			fpConsultaI.hide()
			if(gridRespuestasI!=null)gridRespuestasI.hide();
			fpRespIndI.destroy();
			fpMuestraRI.destroy();
			fpMuestraRCI.destroy();
		}
	}
}


var fnNumeroPreguntasPlant = function(numPreg, numPregOld){
	fpModPreguntasL.generarCamposMod(fpModPreguntasL, numPreg, numPregOld, 'S');
}

var fnNumeroPreguntas = function(newVal){
	if(fpRespuestasL!=null){
		fpRespuestasL.destroy();
		fpRespuestasL = null;
	}
	fpPreguntasL = new NE.encuestas.FormPreguntasEnc({numeroPreguntas:newVal, tipoAfiliado:'L'});
	tabEncAltaPlant.add(fpPreguntasL);
	tabEncAltaPlant.doLayout();
	
	fpPreguntasL.setHandlerBtnCancelar(function(){
		fpAltaPlantilla.show();
		fpAltaPlantilla.getForm().reset();
		fpPreguntasL.destroy();
	});
	
	fpPreguntasL.setHandlerBtnSiguiente(function(){
			
			fpRespuestasL = new NE.encuestas.FormRespEnc({formaPreguntas:fpPreguntasL, numOjetos:1, tipoAfiliado:'L'});
			fpAltaPlantilla.hide();
			fpPreguntasL.hide();
			tabEncAltaPlant.add(fpRespuestasL);
			tabEncAltaPlant.doLayout();
			
			fpRespuestasL.setHandlerBtnCancelar(function(){
				fpAltaPlantilla.show();
				fpAltaPlantilla.getForm().reset();
				fpPreguntasL.destroy();
				fpRespuestasL.destroy();
			});
			
			fpRespuestasL.setHandlerBtnSiguiente(function(){
				
				Ext.Ajax.request({
					url: '15admencu01_ext.data.jsp',
					params:Ext.apply(fpAltaPlantilla.getForm().getValues(), Ext.apply(fpPreguntasL.getForm().getValues(), fpRespuestasL.getForm().getValues()),{
						informacion: 'GuardarPlantilla',
						tipoAfiliado: 'L'
					}),
					callback: processSuccessFailureGuardaPlant
				});
			
			});
			
			fpRespuestasL.setHandlerBtnVistaPrevia(function(){
				var objDisabled = fpRespuestasL.find('disabled',true);
				for(var i=0; i<objDisabled.length; i++){
					objDisabled[i].setValue('');
					objDisabled[i].enable();
				}
				
				var titulo = Ext.getCmp('fp_titulo1L');
				fpVistaPreviaL = new NE.encuestas.FormVistaPrevEnc({formaPreguntas:fpPreguntasL, numOjetos:1, tipoAfiliado:'L', titulo:titulo.getValue()});

				new Ext.Window({
					modal: true,
					resizable: false,
					layout: 'form',
					x: 100,
					width: 810,
					id: 'winVistaPrevI',
					closable: true,
					closeAction: 'close',
					items: [ fpVistaPreviaL ],
					title: 'Vista Previa'
				}).show();
			});
			
		});
}

var fnUsaPlantillas = function(valSeleccion){
	
	if(valSeleccion=='S'){
		
		if(tipoAfiliadoGral=='I'){
			if(gridEncPlantillaI!=null){
				gridEncPlantillaI.destroy();
				gridEncPlantillaI = null;
			}
			gridEncPlantillaI = new NE.encuestas.GridEncPlantillas({tipoAfiliado:tipoAfiliadoGral, fnLoadStore:procesarConsultaEncPlantilla, fnVistaPrevia:muestraVistaPrevPlant });
			var storeEncPlantillasData = Ext.StoreMgr.key('storeEncPlantData1'+tipoAfiliadoGral);
			storeEncPlantillasData.load({
				params:{
					//informacion: 'ConsultaEncuestas',
					tipoAfiliado: tipoAfiliadoGral
				}
			});
		}
		if(tipoAfiliadoGral=='E'){
			if(gridEncPlantillaE!=null){
				gridEncPlantillaE.destroy();
				gridEncPlantillaE = null;
			}
			gridEncPlantillaE = new NE.encuestas.GridEncPlantillas({tipoAfiliado:tipoAfiliadoGral, fnLoadStore:procesarConsultaEncPlantilla, fnVistaPrevia:muestraVistaPrevPlant });
			var storeEncPlantillasData = Ext.StoreMgr.key('storeEncPlantData1'+tipoAfiliadoGral);
			storeEncPlantillasData.load({
				params:{
					//informacion: 'ConsultaEncuestas',
					tipoAfiliado: tipoAfiliadoGral
				}
			});
		}
		if(tipoAfiliadoGral=='P'){
			if(gridEncPlantillaP!=null){
				gridEncPlantillaP.destroy();
				gridEncPlantillaP = null;
			}
			gridEncPlantillaP = new NE.encuestas.GridEncPlantillas({tipoAfiliado:tipoAfiliadoGral, fnLoadStore:procesarConsultaEncPlantilla, fnVistaPrevia:muestraVistaPrevPlant });
			var storeEncPlantillasData = Ext.StoreMgr.key('storeEncPlantData1'+tipoAfiliadoGral);
			storeEncPlantillasData.load({
				params:{
					//informacion: 'ConsultaEncuestas',
					tipoAfiliado: tipoAfiliadoGral
				}
			});
		}

	}
	
	if(valSeleccion=='N'){
		if(tipoAfiliadoGral=='I'){
			if(gridEncPlantillaI!=null){
				gridEncPlantillaI.destroy();
				gridEncPlantillaI = null;
			}
		}
		if(tipoAfiliadoGral=='P'){
			if(gridEncPlantillaP!=null){
				gridEncPlantillaP.destroy();
				gridEncPlantillaP = null;
			}
		}
		if(tipoAfiliadoGral=='E'){
			if(gridEncPlantillaE!=null){
				gridEncPlantillaE.destroy();
				gridEncPlantillaE = null;
			}
		}
	}
}

var muestraVistaPrevPlant = function(cveEncuesta){
	pnl.el.mask('Generando Vista Previa...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '15admencu01_ext.data.jsp',
		params:{
			informacion: 'obtenerInfoPlantilla',
			cveEncuesta: cveEncuesta,
			tipoAfiliado:tipoAfiliadoGral
		},
		callback: processSuccessFailureVistaPrevPlant
	});
}
	
var processSuccessFailureVistaPrevPlant = function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = Ext.util.JSON.decode(response.responseText);
		var objInfoEncuesta = resp.dataInfoEnc;
		var infoParam = objInfoEncuesta.INFOGRAL;
		var infoPreguntas = objInfoEncuesta.INFOPREGUNTAS;
		var infoRespuestas = objInfoEncuesta.INFORESPUESTAS;
		var tabEncuesta = null;
		var fpModEncuesta = null;
		var fp_cveEncuesta = '';
		
		if(tipoAfiliadoGral=='I'){
			tabEncuesta=tabEncIF;
			fp_cveEncuesta = infoParam.fp_cveEncuesta_mI;
		}
		if(tipoAfiliadoGral=='P'){
			tabEncuesta=tabEncPYME;
			fp_cveEncuesta = infoParam.fp_cveEncuesta_mP;
		}
		if(tipoAfiliadoGral=='E'){
			tabEncuesta=tabEncEPO;
			fp_cveEncuesta = infoParam.fp_cveEncuesta_mE;
		}
		if(tipoAfiliadoGral=='L'){
			tabEncuesta=tabEncAltaPlant;
			fp_cveEncuesta = infoParam.fp_cveEncuesta_mL;
		}
		
		
		if(tipoAfiliadoGral!=''){

				fpModPlantillaL = new NE.encuestas.FormModificaPlantilla({tipoAfiliado:tipoAfiliadoGral, claveEncuesta:fp_cveEncuesta, fnNumPreguntas:fnNumeroPreguntas, hidden:true});
				fpModPlantillaL.getForm().setValues(infoParam);
				//fpModPlantillaL.show();
				
				tabEncuesta.add(fpModPlantillaL);
				
				fpModPreguntasL = new NE.encuestas.FormPreguntasEnc({tipoAfiliado:tipoAfiliadoGral, modif: 's', objDataPreg:infoPreguntas, esPlantilla:'S', hidden:true});
				//fpModPreguntasL.show();
				tabEncuesta.add(fpModPreguntasL);
				tabEncuesta.doLayout();
				
				var task = new Ext.util.DelayedTask(function(){
					fpModRespuestasL = new NE.encuestas.FormRespEnc({formaPreguntas:fpModPreguntasL, numOjetos:1, tipoAfiliado:tipoAfiliadoGral, modif:'s', objDataResp:infoRespuestas, hidden:true});
					fpModPlantillaL.hide();
					fpModPreguntasL.hide();
					
					tabEncuesta.add(fpModRespuestasL);
					tabEncuesta.doLayout();
					
					fpVistaPreviaL = new NE.encuestas.FormVistaPrevEnc({formaPreguntas:fpModPreguntasL, numOjetos:1, tipoAfiliado:tipoAfiliadoGral, titulo:'seee', modif:'s'});
					fpVistaPreviaL.show();
					
					var taskResp = new Ext.util.DelayedTask(function(){
						var titulo = '';
						if(tipoAfiliadoGral=='I'){ titulo = Ext.getCmp('fp_titulo_m1I');}
						if(tipoAfiliadoGral=='P'){titulo = Ext.getCmp('fp_titulo_m1P');}
						if(tipoAfiliadoGral=='E'){titulo = Ext.getCmp('fp_titulo_m1E');}
						if(tipoAfiliadoGral=='L'){titulo = Ext.getCmp('fp_titulo_m1L');}
						
						fpVistaPreviaL = new NE.encuestas.FormVistaPrevEnc({formaPreguntas:fpModPreguntasL, numOjetos:1, tipoAfiliado:tipoAfiliadoGral, titulo:titulo.getValue(), modif:'s'});
						
						new Ext.Window({
							modal: true,
							resizable: false,
							layout: 'form',
							x: 100,
							width: 810,
							id: 'winVistaPrevLI',
							closable: true,
							closeAction: 'close',
							items: [ fpVistaPreviaL ],
							title: 'Vista Previa'
						}).show();
						
						fpModPlantillaL.destroy();
						fpModPlantillaL = null;
						fpModPreguntasL.destroy();
						fpModPreguntasL = null;
						fpModRespuestasL.destroy();
						fpModRespuestasL = null;
						pnl.el.unmask();
					});
					taskResp.delay(500);
				});
				task.delay(500);
		}
		
	} else {
		pnl.el.unmask();
		NE.util.mostrarConnError(response,opts);
	}
};


var processSuccessFailureGuardarEncPlant = function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
		Ext.MessageBox.alert('aviso', 'Encuesta guardada con �xito', function(){
			if(tipoAfiliadoGral=='I'){
				fpAltaI.getForm().reset();
				Ext.StoreMgr.key('storeAfiliadosDataI').removeAll();
			}
			if(tipoAfiliadoGral=='E'){
				fpAltaE.getForm().reset();
				Ext.StoreMgr.key('storeAfiliadosDataE').removeAll();
			}
			if(tipoAfiliadoGral=='P'){
				fpAltaP.getForm().reset();
				Ext.StoreMgr.key('storeAfiliadosDataP').removeAll();
			}
		});
		window.location = '15admenc01_ext.jsp';	
		
	} else {
		pnl.el.unmask();
		NE.util.mostrarConnError(response,opts);
	}
};

var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
	//var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	//btnGenerarArchivo.setIconClass('');
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = Ext.util.JSON.decode(response.responseText)
		
		var archivo = resp.urlArchivo;				
		archivo = archivo.replace('/nafin','');
		var params = {nombreArchivo: archivo};
		
		//if(tipoAfiliadoGral=='I'){
			fpArchivo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpArchivo.getForm().getEl().dom.submit();
		//}
		/*
		if(tipoAfiliadoGral=='E'){
			fpRespIndE.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpRespIndE.getForm().getEl().dom.submit();
		}
		if(tipoAfiliadoGral=='P'){
			fpRespIndP.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpRespIndP.getForm().getEl().dom.submit();
		}
		*/

	} else {
		//btnGenerarArchivo.enable();
		NE.util.mostrarConnError(response,opts);
	}
}

var processSuccessFailureGuardaEnc = function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			Ext.MessageBox.alert('aviso', 'Encuesta guardada con �xito', function(){
				//window.location.href='15admenc01_ext.jsp';
				
				if(tipoAfiliadoGral=='I'){
					fpAltaI.show();
					fpAltaI.getForm().reset();
					
					fpPreguntasI.destroy();
					fpRespuestasI.destroy();
				}
				if(tipoAfiliadoGral=='E'){
					fpAltaE.show();
					fpAltaE.getForm().reset();
					
					fpPreguntasE.destroy();
					fpRespuestasE.destroy();
				}
				if(tipoAfiliadoGral=='P'){
					fpAltaP.show();
					fpAltaP.getForm().reset();
					
					fpPreguntasP.destroy();
					fpRespuestasP.destroy();
				}
				Ext.StoreMgr.key('storeAfiliadosData'+tipoAfiliadoGral).removeAll();
				window.location = '15admenc01_ext.jsp';		
				
			});
			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

var processSuccessFailureGuardaModifEnc = function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			if(Ext.getCmp('winGuardarComo')){
				Ext.getCmp('winGuardarComo').close();
			}
			
			Ext.MessageBox.alert('aviso', 'La encuesta ha sido modificada con �xito', function(){
				//window.location.href='15admenc01_ext.jsp';
				
				if(tipoAfiliadoGral=='I'){
					fpConsultaI.show();
					fpConsultaI.getForm().reset();
					
					fpModEncuestaI.destroy();
					fpModPreguntasI.destroy();
					fpModRespuestasI.destroy();
				}
				if(tipoAfiliadoGral=='E'){
					fpConsultaE.show();
					fpConsultaE.getForm().reset();
					
					fpModEncuestaE.destroy();
					fpModPreguntasE.destroy();
					fpModRespuestasE.destroy();
				}
				if(tipoAfiliadoGral=='P'){
					fpConsultaP.show();
					fpConsultaP.getForm().reset();
					
					fpModEncuestaP.destroy();
					fpModPreguntasP.destroy();
					fpModRespuestasP.destroy();
				}
				if(tipoAfiliadoGral=='L'){
					fpConsultaL.show();
					fpConsultaL.getForm().reset();
					
					fpModPlantillaL.destroy();
					fpModPreguntasL.destroy();
					fpModRespuestasL.destroy();
				}
				
				window.location = '15admenc01_ext.jsp';	
				
			});
			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};


var processSuccessFailureModificaEnc = function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = Ext.util.JSON.decode(response.responseText);
		var objInfoEncuesta = resp.dataInfoEnc;
		var infoParam = objInfoEncuesta.INFOGRAL;
		var infoPreguntas = objInfoEncuesta.INFOPREGUNTAS;
		var infoRespuestas = objInfoEncuesta.INFORESPUESTAS;
		var tabEncuesta = null;
		var fpModEncuesta = null;
		
		if(tipoAfiliadoGral=='I'){tabEncuesta=tabEncIF;}
		if(tipoAfiliadoGral=='P'){tabEncuesta=tabEncPYME;}
		if(tipoAfiliadoGral=='E'){tabEncuesta=tabEncEPO;}
		
		if(tipoAfiliadoGral=='I'){
			
			//if(fpModEncuestaI==null){
				fpConsultaI.hide();
				gridEncuestasI.hide();
				
				fpModEncuestaI = new NE.encuestas.FormModificaEncuesta({tipoAfiliado:tipoAfiliadoGral, claveEncuesta:infoParam.fp_cveEncuesta_mI});
				fpModEncuestaI.getForm().setValues(infoParam);
				
				fpModPreguntasI = new NE.encuestas.FormPreguntasEnc({tipoAfiliado:tipoAfiliadoGral, modif: 's', objDataPreg:infoPreguntas});
				fpModPreguntasI.setHandlerBtnCancelar(function(){
					fpConsultaI.show();
					fpModEncuestaI.destroy();
					fpModPreguntasI.destroy();
				});
				fpModPreguntasI.setHandlerBtnSiguiente(function(){
					fpModRespuestasI = new NE.encuestas.FormRespEnc({formaPreguntas:fpModPreguntasI, numOjetos:1, tipoAfiliado:'I', modif:'s', objDataResp:infoRespuestas});
					fpModEncuestaI.hide();
					fpModPreguntasI.hide();
					
					tabEncIF.add(fpModRespuestasI);
					tabEncIF.doLayout();
					
					
					fpModRespuestasI.setHandlerBtnVistaPrevia(function(){
						var objDisabled = fpModRespuestasI.find('disabled',true);
						for(var i=0; i<objDisabled.length; i++){
							objDisabled[i].setValue('');
							objDisabled[i].enable();
						}
						
						var titulo = Ext.getCmp('fp_titulo_m1I');
							fpVistaPreviaI = new NE.encuestas.FormVistaPrevEnc({formaPreguntas:fpModPreguntasI, numOjetos:1, tipoAfiliado:'I', titulo:titulo.getValue(), modif:'s'});
			
							new Ext.Window({
								modal: true,
								resizable: false,
								layout: 'form',
								x: 100,
								width: 810,
								id: 'winVistaPrevI',
								closable: true,
								closeAction: 'close',
								items: [ fpVistaPreviaI ],
								title: 'Vista Previa'
							}).show();
					});
					
					fpModRespuestasI.setHandlerBtnCancelar(function(){
						fpConsultaI.show();
						fpModEncuestaI.destroy();
						fpModPreguntasI.destroy();
						fpModRespuestasI.destroy();
					});
					fpModRespuestasI.setHandlerBtnSiguiente(function(){
						Ext.Msg.confirm("Mensaje de p�gina web","�Est� seguro que desea modificar la encuesta?",
								function(res){
									if(res=='no' || res == 'NO'){
										return;
									}else{
										var objDisabled = fpModRespuestasI.find('disabled',true);
										for(var i=0; i<objDisabled.length; i++){
											objDisabled[i].setValue('');
											objDisabled[i].enable();
										}
									
										Ext.Ajax.request({
											url: '15admencu01_ext.data.jsp',
											params:Ext.apply(fpModEncuestaI.getForm().getValues(), Ext.apply(fpModPreguntasI.getForm().getValues(), fpModRespuestasI.getForm().getValues()),{
												informacion: 'ModificarEncuesta',
												tipoAfiliado: 'I',
												cveEncuesta: fpModEncuestaI.claveEncuesta,
												numPregReal: fpModRespuestasI.numPregReal
											}),
											callback: processSuccessFailureGuardaModifEnc
										});
									}
							});
						
						
					});
				});
				tabEncuesta.add(fpModEncuestaI);
				tabEncuesta.add(fpModPreguntasI);
				tabEncuesta.doLayout();
			//}
		}
		
		if(tipoAfiliadoGral=='E'){
			//if(fpModEncuestaE==null){
				fpConsultaE.hide();
				gridEncuestasE.hide();
				
				fpModEncuestaE = new NE.encuestas.FormModificaEncuesta({tipoAfiliado:tipoAfiliadoGral, claveEncuesta:infoParam.fp_cveEncuesta_mE});
				fpModEncuestaE.getForm().setValues(infoParam);
				
				fpModPreguntasE = new NE.encuestas.FormPreguntasEnc({tipoAfiliado:tipoAfiliadoGral, modif: 's', objDataPreg:infoPreguntas});
				fpModPreguntasE.setHandlerBtnCancelar(function(){
					fpConsultaE.show();
					fpModEncuestaE.destroy();
					fpModPreguntasE.destroy();
				});
				fpModPreguntasE.setHandlerBtnSiguiente(function(){
					fpModRespuestasE = new NE.encuestas.FormRespEnc({formaPreguntas:fpModPreguntasE, numOjetos:1, tipoAfiliado:'E', modif:'s', objDataResp:infoRespuestas});
					fpModEncuestaE.hide();
					fpModPreguntasE.hide();
					
					tabEncEPO.add(fpModRespuestasE);
					tabEncEPO.doLayout();
					
					
					fpModRespuestasE.setHandlerBtnVistaPrevia(function(){
						var objDisabled = fpModRespuestasE.find('disabled',true);
						for(var i=0; i<objDisabled.length; i++){
							objDisabled[i].setValue('');
							objDisabled[i].enable();
						}
						
						var titulo = Ext.getCmp('fp_titulo_m1E');
							fpVistaPreviaE = new NE.encuestas.FormVistaPrevEnc({formaPreguntas:fpModPreguntasE, numOjetos:1, tipoAfiliado:'E', titulo:titulo.getValue(), modif:'s'});
			
							new Ext.Window({
								modal: true,
								resizable: false,
								layout: 'form',
								x: 100,
								width: 810,
								id: 'winVistaPrevI',
								closable: true,
								closeAction: 'close',
								items: [ fpVistaPreviaE ],
								title: 'Vista Previa'
							}).show();
					});
					
					fpModRespuestasE.setHandlerBtnCancelar(function(){
						fpConsultaE.show();
						fpModEncuestaE.destroy();
						fpModPreguntasE.destroy();
						fpModRespuestasE.destroy();
					});
					fpModRespuestasE.setHandlerBtnSiguiente(function(){
						Ext.Msg.confirm("Mensaje de p�gina web","�Est� seguro que desea modificar la encuesta?",
						function(res){
							if(res=='no' || res == 'NO'){
								return;
							}else{
								Ext.Ajax.request({
									url: '15admencu01_ext.data.jsp',
									params:Ext.apply(fpModEncuestaE.getForm().getValues(), Ext.apply(fpModPreguntasE.getForm().getValues(), fpModRespuestasE.getForm().getValues()),{
										informacion: 'ModificarEncuesta',
										tipoAfiliado: 'E',
										cveEncuesta: fpModEncuestaE.claveEncuesta,
										numPregReal: fpModRespuestasE.numPregReal
									}),
									callback: processSuccessFailureGuardaModifEnc
								});
							}
						});
					});
				});
				tabEncuesta.add(fpModEncuestaE);
				tabEncuesta.add(fpModPreguntasE);
				tabEncuesta.doLayout();
			//}
		}
		
		if(tipoAfiliadoGral=='P'){
			//if(fpModEncuestaP==null){
				fpConsultaP.hide();
				gridEncuestasP.hide();
				
				fpModEncuestaP = new NE.encuestas.FormModificaEncuesta({tipoAfiliado:tipoAfiliadoGral, claveEncuesta:infoParam.fp_cveEncuesta_mP});
				fpModEncuestaP.getForm().setValues(infoParam);
				
				fpModPreguntasP = new NE.encuestas.FormPreguntasEnc({tipoAfiliado:tipoAfiliadoGral, modif: 's', objDataPreg:infoPreguntas});
				fpModPreguntasP.setHandlerBtnCancelar(function(){
					fpConsultaP.show();
					fpModEncuestaP.destroy();
					fpModPreguntasP.destroy();
				});
				fpModPreguntasP.setHandlerBtnSiguiente(function(){
					fpModRespuestasP = new NE.encuestas.FormRespEnc({formaPreguntas:fpModPreguntasP, numOjetos:1, tipoAfiliado:'P', modif:'s', objDataResp:infoRespuestas});
					fpModEncuestaP.hide();
					fpModPreguntasP.hide();
					
					tabEncPYME.add(fpModRespuestasP);
					tabEncPYME.doLayout();
					
					
					fpModRespuestasP.setHandlerBtnVistaPrevia(function(){
						var objDisabled = fpModRespuestasP.find('disabled',true);
						for(var i=0; i<objDisabled.length; i++){
							objDisabled[i].setValue('');
							objDisabled[i].enable();
						}
						
						var titulo = Ext.getCmp('fp_titulo_m1P');
							fpVistaPreviaP = new NE.encuestas.FormVistaPrevEnc({formaPreguntas:fpModPreguntasP, numOjetos:1, tipoAfiliado:'P', titulo:titulo.getValue(), modif:'s'});
			
							new Ext.Window({
								modal: true,
								resizable: false,
								layout: 'form',
								x: 100,
								width: 810,
								id: 'winVistaPrevI',
								closable: true,
								closeAction: 'close',
								items: [ fpVistaPreviaP ],
								title: 'Vista Previa'
							}).show();
					});
					
					fpModRespuestasP.setHandlerBtnCancelar(function(){
						fpConsultaP.show();
						fpModEncuestaP.destroy();
						fpModPreguntasP.destroy();
						fpModRespuestasP.destroy();
					});
					fpModRespuestasP.setHandlerBtnSiguiente(function(){
						Ext.Msg.confirm("Mensaje de p�gina web","�Est� seguro que desea modificar la encuesta?",
						function(res){
							if(res=='no' || res == 'NO'){
								return;
							}else{
								Ext.Ajax.request({
									url: '15admencu01_ext.data.jsp',
									params:Ext.apply(fpModEncuestaP.getForm().getValues(), Ext.apply(fpModPreguntasP.getForm().getValues(), fpModRespuestasP.getForm().getValues()),{
										informacion: 'ModificarEncuesta',
										tipoAfiliado: 'P',
										cveEncuesta: fpModEncuestaP.claveEncuesta,
										numPregReal: fpModRespuestasP.numPregReal
									}),
									callback: processSuccessFailureGuardaModifEnc
								});
							}
						});
					});
				});
				tabEncuesta.add(fpModEncuestaP);
				tabEncuesta.add(fpModPreguntasP);
				tabEncuesta.doLayout();
			//s}
		}
			
		
	
	} else {
		NE.util.mostrarConnError(response,opts);
	}
};

var procesarConsultaEncData = function(store, arrRegistros, opts) {
		//fpConsultaCargaMas.el.unmask();
		//var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var gridEncuestas = null;
		var tabEncuesta = null;
		if(tipoAfiliadoGral=='I'){gridEncuestas=gridEncuestasI; tabEncuesta=tabEncIF;}
		if(tipoAfiliadoGral=='P'){gridEncuestas=gridEncuestasP; tabEncuesta=tabEncPYME;}
		if(tipoAfiliadoGral=='E'){gridEncuestas=gridEncuestasE; tabEncuesta=tabEncEPO;}
		
		gridEncuestas.show();
		if (arrRegistros != null) {
			if (!gridEncuestas.isVisible()) {
				tabEncuesta.add(NE.util.getEspaciador(10));
				tabEncuesta.add(gridEncuestas);
				tabEncuesta.doLayout();
			}
			
			var el = gridEncuestas.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};
	
var muestraEncModificar = function(cveEncuesta){
	Ext.Ajax.request({
		url: '15admencu01_ext.data.jsp',
		params:{
			informacion: 'obtenerInfoEncuesta',
			cveEncuesta: cveEncuesta,
			tipoAfiliado:tipoAfiliadoGral
		},
		callback: processSuccessFailureModificaEnc
	});
}
	
var procesarConsultaRespData = function(store, arrRegistros, opts) {
		//pregSeleccionada='';
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var gridRespuestas = null;
		
		if (arrRegistros != null) {
		
			if(tipoAfiliadoGral=='I'){
				gridRespuestas=gridRespuestasI;
				pregSeleccionada = '';
			}
			if(tipoAfiliadoGral=='P'){
				gridRespuestas=gridRespuestasP;
				pregSeleccionadaP = '';
			}
			if(tipoAfiliadoGral=='E'){
				gridRespuestas=gridRespuestasE;
				pregSeleccionadaE = '';
			}
		
			gridRespuestas.show();
			if (!gridRespuestas.isVisible()) {

			}
			
			var el = gridRespuestas.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};
	
var processSuccessFailureConsRespInd = function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			
			var dataRespInd = resp.registros;
			
			if(tipoAfiliadoGral=='I'){
				if(fpMuestraRI!=null){
					fpMuestraRI.destroy();
				}else{
					tabEncIF.add(NE.util.getEspaciador(20));
				}
				
				fpMuestraRI = new NE.encuestas.FormMuestraRespInd({tipoAfiliado:tipoAfiliadoGral, dataRespInd:dataRespInd, tituloEnc:pregTituloEncSelecI});
				fpMuestraRI.setHandlerBtnImprimir(function(){
					var cboAfil = Ext.getCmp('fp_consAfiliado1'+tipoAfiliadoGral);
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespIndividual',
							tipoAfiliado: tipoAfiliadoGral,
							claveAfiliado:cboAfil.getValue(),
							cveEncuesta:pregSeleccionada,
							tipoArch:'PDF'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
				});
				
				fpMuestraRI.setHandlerBtnArchivo(function(){
					var cboAfil = Ext.getCmp('fp_consAfiliado1'+tipoAfiliadoGral);
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespIndividual',
							tipoAfiliado: tipoAfiliadoGral,
							claveAfiliado:cboAfil.getValue(),
							cveEncuesta:pregSeleccionada,
							tipoArch:'CSV'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
				});
				
				tabEncIF.add(fpMuestraRI);
				tabEncIF.doLayout();
			}
			if(tipoAfiliadoGral=='E'){
				if(fpMuestraRE!=null){
					fpMuestraRE.destroy();
				}else{
					tabEncEPO.add(NE.util.getEspaciador(20));
				}
				
				fpMuestraRE = new NE.encuestas.FormMuestraRespInd({tipoAfiliado:tipoAfiliadoGral, dataRespInd:dataRespInd, tituloEnc:pregTituloEncSelecE});
				fpMuestraRE.setHandlerBtnImprimir(function(){
					var cboAfil = Ext.getCmp('fp_consAfiliado1'+tipoAfiliadoGral);
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespIndividual',
							tipoAfiliado: tipoAfiliadoGral,
							claveAfiliado:cboAfil.getValue(),
							cveEncuesta:pregSeleccionadaE,
							tipoArch:'PDF'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
				});
				
				fpMuestraRE.setHandlerBtnArchivo(function(){
					var cboAfil = Ext.getCmp('fp_consAfiliado1'+tipoAfiliadoGral);
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespIndividual',
							tipoAfiliado: tipoAfiliadoGral,
							claveAfiliado:cboAfil.getValue(),
							cveEncuesta:pregSeleccionadaE,
							tipoArch:'CSV'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
				});
				
				tabEncEPO.add(fpMuestraRE);
				tabEncEPO.doLayout();
			}
			if(tipoAfiliadoGral=='P'){
				if(fpMuestraRP!=null){
					fpMuestraRP.destroy();
				}else{
					tabEncPYME.add(NE.util.getEspaciador(20));
				}
				
				fpMuestraRP = new NE.encuestas.FormMuestraRespInd({tipoAfiliado:tipoAfiliadoGral, dataRespInd:dataRespInd, tituloEnc:pregTituloEncSelecP});
				fpMuestraRP.setHandlerBtnImprimir(function(){
					var cboAfil = Ext.getCmp('fp_consAfiliado1'+tipoAfiliadoGral);
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespIndividual',
							tipoAfiliado: tipoAfiliadoGral,
							claveAfiliado:cboAfil.getValue(),
							cveEncuesta:pregSeleccionadaP,
							tipoArch:'PDF'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
				});
				
				fpMuestraRP.setHandlerBtnArchivo(function(){
					var cboAfil = Ext.getCmp('fp_consAfiliado1'+tipoAfiliadoGral);
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespIndividual',
							tipoAfiliado: tipoAfiliadoGral,
							claveAfiliado:cboAfil.getValue(),
							cveEncuesta:pregSeleccionadaP,
							tipoArch:'CSV'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
				});
				
				tabEncPYME.add(fpMuestraRP);
				tabEncPYME.doLayout();
			}


		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	

var processSuccessFailureConsRespCon = function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			
			var dataRespCon = resp.registros;
			
			
			if(tipoAfiliadoGral=='I'){
				if(fpMuestraRCI!=null){
					fpMuestraRCI.destroy();
				}else{
					tabEncIF.add(NE.util.getEspaciador(20));
				}
				
				fpMuestraRCI = new NE.encuestas.FormMuestraRespCon({tipoAfiliado:tipoAfiliadoGral, dataRespCon:dataRespCon, tituloEnc:pregTituloEncSelecI});
				fpMuestraRCI.setHandlerBtnRegresar(function(){
					gridRespuestasI.show();
					fpMuestraRCI.destroy();
				});
				fpMuestraRCI.setHandlerBtnImprimir(function(){
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespConsolidada',
							tipoAfiliado: tipoAfiliadoGral,
							cveEncuesta:pregSeleccionada,
							tipoArch:'PDF'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
				});
				
				fpMuestraRCI.setHandlerBtnArchivo(function(){
					
					var forma = document.formAux;
					forma.num_encuesta.value=pregSeleccionada;
					forma.tipoEncuesta.value=tipoAfiliadoGral;
					forma.action = "15admresp03strxls.jsp";
					forma.submit();
					
					/*
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespConsolidada',
							tipoAfiliado: tipoAfiliadoGral,
							cveEncuesta:pregSeleccionada,
							tipoArch:'CSV'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
					*/
				});
				
				
				
				
				tabEncIF.add(fpMuestraRCI);
				tabEncIF.doLayout();
				gridRespuestasI.hide();
			}
			if(tipoAfiliadoGral=='E'){
				if(fpMuestraRCE!=null){
					fpMuestraRCE.destroy();
				}else{
					tabEncEPO.add(NE.util.getEspaciador(20));
				}
				
				fpMuestraRCE = new NE.encuestas.FormMuestraRespCon({tipoAfiliado:tipoAfiliadoGral, dataRespCon:dataRespCon, tituloEnc:pregTituloEncSelecE});
				fpMuestraRCE.setHandlerBtnRegresar(function(){
					gridRespuestasE.show();
					fpMuestraRCE.destroy();
				});
				
				fpMuestraRCE.setHandlerBtnImprimir(function(){
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespConsolidada',
							tipoAfiliado: tipoAfiliadoGral,
							cveEncuesta:pregSeleccionadaE,
							tipoArch:'PDF'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
				});
				
				fpMuestraRCE.setHandlerBtnArchivo(function(){
					var forma = document.formAux;
					forma.num_encuesta.value=pregSeleccionadaE;
					forma.tipoEncuesta.value=tipoAfiliadoGral;
					forma.action = "15admresp03strxls.jsp";
					forma.submit();
					/*
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespConsolidada',
							tipoAfiliado: tipoAfiliadoGral,
							cveEncuesta:pregSeleccionadaE,
							tipoArch:'CSV'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
					*/
				});
				
				tabEncEPO.add(fpMuestraRCE);
				tabEncEPO.doLayout();
				gridRespuestasE.hide();
			}
			if(tipoAfiliadoGral=='P'){
				if(fpMuestraRCP!=null){
					fpMuestraRCP.destroy();
				}else{
					tabEncPYME.add(NE.util.getEspaciador(20));
				}
				
				fpMuestraRCP = new NE.encuestas.FormMuestraRespCon({tipoAfiliado:tipoAfiliadoGral, dataRespCon:dataRespCon, tituloEnc:pregTituloEncSelecP});
				fpMuestraRCP.setHandlerBtnRegresar(function(){
					gridRespuestasP.show();
					fpMuestraRCP.destroy();
				});
				
				fpMuestraRCP.setHandlerBtnImprimir(function(){
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespConsolidada',
							tipoAfiliado: tipoAfiliadoGral,
							cveEncuesta:pregSeleccionadaP,
							tipoArch:'PDF'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
				});
				
				fpMuestraRCP.setHandlerBtnArchivo(function(){
					var forma = document.formAux;
					forma.num_encuesta.value=pregSeleccionadaP;
					forma.tipoEncuesta.value=tipoAfiliadoGral;
					forma.action = "15admresp03strxls.jsp";
					forma.submit();
					
					/*
					Ext.Ajax.request({
						url: '15admencu01_ext.data.jsp',
						params:{
							informacion: 'ArchivoRespConsolidada',
							tipoAfiliado: tipoAfiliadoGral,
							cveEncuesta:pregSeleccionadaP,
							tipoArch:'CSV'
							
						},
						callback: procesarSuccessFailureGenerarArchivo
					});
					*/
				});
				
				tabEncPYME.add(fpMuestraRCP);
				tabEncPYME.doLayout();
				gridRespuestasP.hide();
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

var fnBtnRespIndividual = function(){

	var pregSelec = '';
	
	if(tipoAfiliadoGral=='I' && pregSeleccionada != ""){ pregSelec = pregSeleccionada; }
	if(tipoAfiliadoGral=='E' && pregSeleccionadaE != ""){ pregSelec = pregSeleccionadaE; }
	if(tipoAfiliadoGral=='P' && pregSeleccionadaP != ""){ pregSelec = pregSeleccionadaP; }
	
	if(pregSelec != ''){
		
		var fpRespInd = null;
		
		if(tipoAfiliadoGral=='I'){
			if(fpRespIndI!=null){
				fpRespIndI.destroy();
			}
			
			fpRespIndI = new NE.encuestas.FormRespInd({tipoAfiliado:tipoAfiliadoGral, claveEncuesta: pregSelec });
			fpRespIndI.setHandlerBtnRegresar(function(){
				gridRespuestasI.show();
				fpRespIndI.destroy();
				if(fpMuestraRI)fpMuestraRI.destroy();
			});
			
			tabEncIF.add(fpRespIndI);
			tabEncIF.doLayout();
			
			fpRespInd = fpRespIndI;
			gridRespuestasI.hide();
			
		}
		
		if(tipoAfiliadoGral=='E'){
			if(fpRespIndE!=null){
				fpRespIndE.destroy();
			}
			
			fpRespIndE = new NE.encuestas.FormRespInd({tipoAfiliado:tipoAfiliadoGral, claveEncuesta: pregSelec });
			fpRespIndE.setHandlerBtnRegresar(function(){
				gridRespuestasE.show();
				fpRespIndE.destroy();
				if(fpMuestraRE)fpMuestraRE.destroy();
			});
			
			tabEncEPO.add(fpRespIndE);
			tabEncEPO.doLayout();
			
			fpRespInd = fpRespIndE;
			gridRespuestasE.hide();
		}
		if(tipoAfiliadoGral=='P'){
			if(fpRespIndP!=null){
				fpRespIndP.destroy();
			}
			
			fpRespIndP = new NE.encuestas.FormRespInd({tipoAfiliado:tipoAfiliadoGral, claveEncuesta: pregSelec });
			fpRespIndP.setHandlerBtnRegresar(function(){
				gridRespuestasP.show();
				fpRespIndP.destroy();
				if(fpMuestraRP)fpMuestraRP.destroy();
			});
			
			tabEncPYME.add(fpRespIndP);
			tabEncPYME.doLayout();
			
			fpRespInd = fpRespIndP;
			gridRespuestasP.hide();
		}
		
		fpRespInd.show();
		
		fpRespInd.setHandlerBtnAceptar(function(){
			var cboAfil = Ext.getCmp('fp_consAfiliado1'+tipoAfiliadoGral);
			Ext.Ajax.request({
				url: '15admencu01_ext.data.jsp',
				params:{
					informacion: 'ConsultaRespIndividual',
					tipoAfiliado: tipoAfiliadoGral,
					claveAfiliado: cboAfil.getValue(),
					cveEncuesta: pregSelec
					
				},
				callback: processSuccessFailureConsRespInd
			});
		});
	}else{
		Ext.MessageBox.alert('Aviso', 'Debe seleccionar una opci�n');
	}
}

var fnBtnRespConsolidada = function(){
	var pregSelec = '';
	
	if(tipoAfiliadoGral=='I' && pregSeleccionada != ""){ pregSelec = pregSeleccionada; }
	if(tipoAfiliadoGral=='E' && pregSeleccionadaE != ""){ pregSelec = pregSeleccionadaE; }
	if(tipoAfiliadoGral=='P' && pregSeleccionadaP != ""){ pregSelec = pregSeleccionadaP; }
	
	if(pregSelec!=''){
		Ext.Ajax.request({
				url: '15admencu01_ext.data.jsp',
				params:{
					informacion: 'ConsultaRespConsolidada',
					tipoAfiliado: tipoAfiliadoGral,
					cveEncuesta:pregSelec
					
				},
				callback: processSuccessFailureConsRespCon
		});
	}else{
		Ext.MessageBox.alert('Aviso', 'Debe seleccionar una opci�n');
	}
}


var processSuccessFailureGuardaPlant = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.MessageBox.alert('aviso', 'Plantilla guardada con �xito', function(){
				if(tipoAfiliadoGral=='L'){
					fpPreguntasL.destroy();
					fpRespuestasL.destroy();
					fpAltaPlantilla.show();
					fpAltaPlantilla.getForm().reset();
				}
			});
			window.location = '15admenc01_ext.jsp';	
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

var procesarConsultaPlantData = function(store, arrRegistros, opts) {
		
		var gridEncuestas = null;
		var tabEncuesta = null;
		//if(tipoAfiliadoGral=='I'){gridEncuestas=gridEncuestasI; tabEncuesta=tabEncIF;}
		if(tipoAfiliadoGral=='L'){gridEncuestas=gridPlantillaL; tabEncuesta=tabEncAltaPlant;}
		
		
		gridEncuestas.show();
		if (arrRegistros != null) {
			if (!gridEncuestas.isVisible()) {
				tabEncuesta.add(NE.util.getEspaciador(10));
				tabEncuesta.add(gridEncuestas);
				tabEncuesta.doLayout();
			}
			
			var el = gridEncuestas.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

var muestraPlantillaModificar = function(cveEncuesta){
	Ext.Ajax.request({
		url: '15admencu01_ext.data.jsp',
		params:{
			informacion: 'obtenerInfoPlantilla',
			cveEncuesta: cveEncuesta,
			tipoAfiliado:tipoAfiliadoGral
		},
		callback: processSuccessFailureModificaPlant
	});
}

var processSuccessFailureModificaPlant = function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = Ext.util.JSON.decode(response.responseText);
		var objInfoEncuesta = resp.dataInfoEnc;
		var infoParam = objInfoEncuesta.INFOGRAL;
		var infoPreguntas = objInfoEncuesta.INFOPREGUNTAS;
		var infoRespuestas = objInfoEncuesta.INFORESPUESTAS;
		var tabEncuesta = null;
		var fpModEncuesta = null;
		
		if(tipoAfiliadoGral=='I'){tabEncuesta=tabEncIF;}
		if(tipoAfiliadoGral=='P'){tabEncuesta=tabEncPYME;}
		if(tipoAfiliadoGral=='E'){tabEncuesta=tabEncEPO;}
		if(tipoAfiliadoGral=='L'){tabEncuesta=tabEncAltaPlant;}
		
		
		if(tipoAfiliadoGral=='L'){
			//if(fpModEncuestaP==null){
				fpConsultaL.hide();
				gridPlantillaL.hide();
				
				//fnNumeroPreguntas
				fpModPlantillaL = new NE.encuestas.FormModificaPlantilla({tipoAfiliado:tipoAfiliadoGral, claveEncuesta:infoParam.fp_cveEncuesta_mL, fnNumPreguntas:fnNumeroPreguntasPlant});
				fpModPlantillaL.getForm().setValues(infoParam);
				
				fpModPreguntasL = new NE.encuestas.FormPreguntasEnc({tipoAfiliado:tipoAfiliadoGral, modif: 's', objDataPreg:infoPreguntas, esPlantilla:'S'});
				fpModPreguntasL.setHandlerBtnCancelar(function(){
					fpConsultaL.show();
					fpModPlantillaL.destroy();
					fpModPreguntasL.destroy();
				});
				fpModPreguntasL.setHandlerBtnSiguiente(function(){
					fpModRespuestasL = new NE.encuestas.FormRespEnc({formaPreguntas:fpModPreguntasL, numOjetos:1, tipoAfiliado:'L', modif:'s', esPlantilla:'S', objDataResp:infoRespuestas});
					fpModPlantillaL.hide();
					fpModPreguntasL.hide();
					
					tabEncAltaPlant.add(fpModRespuestasL);
					tabEncAltaPlant.doLayout();
					
					
					fpModRespuestasL.setHandlerBtnVistaPrevia(function(){
						var objDisabled = fpModRespuestasL.find('disabled',true);
						for(var i=0; i<objDisabled.length; i++){
							objDisabled[i].setValue('');
							objDisabled[i].enable();
						}
						
						var titulo = Ext.getCmp('fp_titulo_m1L');
							fpVistaPreviaL = new NE.encuestas.FormVistaPrevEnc({formaPreguntas:fpModPreguntasL, numOjetos:1, tipoAfiliado:'L', titulo:titulo.getValue(), modif:'s'});
			
							new Ext.Window({
								modal: true,
								resizable: false,
								layout: 'form',
								x: 100,
								width: 810,
								id: 'winVistaPrevI',
								closable: true,
								closeAction: 'close',
								items: [ fpVistaPreviaL ],
								title: 'Vista Previa'
							}).show();
					});
					
					fpModRespuestasL.setHandlerBtnCancelar(function(){
						fpConsultaL.show();
						fpModPlantillaL.destroy();
						fpModPreguntasL.destroy();
						fpModRespuestasL.destroy();
					});
					fpModRespuestasL.setHandlerBtnSiguiente(function(){
						Ext.Ajax.request({
							url: '15admencu01_ext.data.jsp',
							params:Ext.apply(fpModPlantillaL.getForm().getValues(), Ext.apply(fpModPreguntasL.getForm().getValues(), fpModRespuestasL.getForm().getValues()),{
								informacion: 'ModificarPlantilla',
								tipoAfiliado: 'L',
								cveEncuesta: fpModPlantillaL.claveEncuesta,
								numPregReal: fpModRespuestasL.numPregReal
							}),
							callback: processSuccessFailureGuardaModifEnc
						});
					});
					
					fpModRespuestasL.setHandlerBtnGuardarComo(function(){
						new Ext.Window({
							modal: true,
							resizable: false,
							layout: 'form',
							x: 100,
							width: 630,
							id: 'winGuardarComo',
							closable: true,
							closeAction: 'close',
							items: [
								NE.util.getEspaciador(20),
								{
								xtype: 'compositefield',
								fieldLabel: 'Nombre',
								combineErrors: false,
								msgTarget: 'side',
								items: [
									{
										xtype:			'textfield',
										name:				'tituloPlantGuardComo',
										id:				'tituloPlantGuardComo1',
										allowBlank:		false,
										startDay:		0,
										width:			300,
										msgTarget:		'side',
										margins:			'0 20 0 0'  //necesario para mostrar el icono de error
									},
									{
										xtype: 'button',
										text: 'Aceptar',
										name: 'btnAceptTituloG',
										id: 'btnAceptTituloG1',
										width: 90,
										handler: function(){
											if(Ext.getCmp('tituloPlantGuardComo1').getValue()==''){
												Ext.Msg.alert('Aviso', 'Es necesario capturar el nombre de la plantilla');
											}else{
												
												fpModPlantillaL.getForm().setValues([{id:'fp_titulo_m1L', value:Ext.getCmp('tituloPlantGuardComo1').getValue()}]);
												
												Ext.Ajax.request({
													url: '15admencu01_ext.data.jsp',
													params:Ext.apply(fpModPlantillaL.getForm().getValues(), Ext.apply(fpModPreguntasL.getForm().getValues(), fpModRespuestasL.getForm().getValues()),{
														informacion: 'ModificarPlantilla',
														tipoAfiliado: 'L',
														guardarComo : 'S',
														cveEncuesta: fpModPlantillaL.claveEncuesta,
														numPregReal: fpModRespuestasL.numPregReal
													}),
													callback: processSuccessFailureGuardaModifEnc
												});
											}
										}
									},
									{
										xtype: 'button',
										text: 'Cancelar',
										name: 'btnCancTituloG',
										id: 'btnCancTituloG1',
										width: 90,
										handler: function(){
											Ext.getCmp('winGuardarComo').close();
										}
									}
								]
							},NE.util.getEspaciador(20)],
							title: '<p align="center">Guardar Como</p>'
						}).show();
						
						/*
						Ext.Ajax.request({
							url: '15admencu01_ext.data.jsp',
							params:Ext.apply(fpModPlantillaL.getForm().getValues(), Ext.apply(fpModPreguntasL.getForm().getValues(), fpModRespuestasL.getForm().getValues()),{
								informacion: 'GuardarComoPlantilla',
								tipoAfiliado: 'L',
								cveEncuesta: fpModPlantillaL.claveEncuesta,
								numPregReal: fpModRespuestasL.numPregReal
							}),
							callback: processSuccessFailureGuardaModifEnc
						});*/
					});
					
					
				});
				tabEncuesta.add(fpModPlantillaL);
				tabEncuesta.add(fpModPreguntasL);
				tabEncuesta.doLayout();
			//s}
		}
			
		
	
	} else {
		NE.util.mostrarConnError(response,opts);
	}
};

var procesarConsultaEncPlantilla = function(store, arrRegistros, opts) {
		//pregSeleccionada='';
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var gridEncPlant = null;
		
		if (arrRegistros != null) {
		
			if(tipoAfiliadoGral=='I'){
				gridEncPlant = gridEncPlantillaI;
				var pnlGridEnc = '';
				pnlGridEnc = fpAltaI.find('id','pnlGridEncPlantI');
				pnlGridEnc[0].add(gridEncPlantillaI);
				pnlGridEnc[0].doLayout();

			}
			if(tipoAfiliadoGral=='P'){
				gridEncPlant = gridEncPlantillaP;
				var pnlGridEnc = '';
				pnlGridEnc = fpAltaP.find('id','pnlGridEncPlantP');
				pnlGridEnc[0].add(gridEncPlantillaP);
				pnlGridEnc[0].doLayout();
			}
			if(tipoAfiliadoGral=='E'){
				gridEncPlant = gridEncPlantillaE;
				var pnlGridEnc = '';
				pnlGridEnc = fpAltaE.find('id','pnlGridEncPlantE');
				pnlGridEnc[0].add(gridEncPlantillaE);
				pnlGridEnc[0].doLayout();
			}

			
			var el = gridEncPlant.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

//------------------------------------------------------------------------------

	var seleOpcion = function(tipoAfil, opcion, presionado){
		if(tipoAfil=='I'){
			//tipoAfiliadoGral = tipoAfil;
			
			if(opcion=='Alta'){
				fpAltaI.show();
				fpAltaI.getForm().reset();
				if(fpRespuestasI!=null)fpRespuestasI.destroy();
				if(fpPreguntasI!=null)fpPreguntasI.destroy();
				
				fpConsultaI.getForm().reset();
				fpConsultaI.hide();
				if(gridEncuestasI!=null)gridEncuestasI.hide();
				if(fpModEncuestaI!=null)fpModEncuestaI.destroy();
				if(fpModPreguntasI!=null)fpModPreguntasI.destroy();
				if(fpModRespuestasI!=null)fpModRespuestasI.destroy();
				
				gridRespuestasI.hide();
				if(fpRespIndI!=null)fpRespIndI.destroy();
				if(fpMuestraRI!=null)fpMuestraRI.destroy();
				if(fpMuestraRCI!=null)fpMuestraRCI.destroy();
			}
			if(opcion=='Enc'){
				fpConsultaI.show();
				
				fpAltaI.hide();
				fpAltaI.getForm().reset();
				if(fpRespuestasI!=null)fpRespuestasI.destroy();
				if(fpPreguntasI!=null)fpPreguntasI.destroy();
				
				fpConsultaI.getForm().reset();
				if(gridEncuestasI!=null)gridEncuestasI.hide();
				if(fpModEncuestaI!=null)fpModEncuestaI.destroy();
				if(fpModPreguntasI!=null)fpModPreguntasI.destroy();
				if(fpModRespuestasI!=null)fpModRespuestasI.destroy();
				
				gridRespuestasI.hide();
				if(fpRespIndI!=null)fpRespIndI.destroy();
				if(fpMuestraRI!=null)fpMuestraRI.destroy();
				if(fpMuestraRCI!=null)fpMuestraRCI.destroy();
				
			}
			if(opcion=='Resp'){
				gridRespuestasI.show();
				
				fpAltaI.hide();
				fpAltaI.getForm().reset();
				if(fpRespuestasI!=null)fpRespuestasI.destroy();
				if(fpPreguntasI!=null)fpPreguntasI.destroy();
				
				fpConsultaI.getForm().reset();
				fpConsultaI.hide();
				if(gridEncuestasI!=null)gridEncuestasI.hide();
				if(fpModEncuestaI!=null)fpModEncuestaI.destroy();
				if(fpModPreguntasI!=null)fpModPreguntasI.destroy();
				if(fpModRespuestasI!=null)fpModRespuestasI.destroy();
				
				
				if(fpRespIndI!=null)fpRespIndI.destroy();
				if(fpMuestraRI!=null)fpMuestraRI.destroy();
				if(fpMuestraRCI!=null)fpMuestraRCI.destroy();
				
				var storeRespData = Ext.StoreMgr.key('storeRespData1'+tipoAfil);
				storeRespData.load({
					params:{
						tipoAfiliado:tipoAfil
					}
				});
			}
		}else if(tipoAfil=='P'){
			//tipoAfiliadoGral = tipoAfil;
			if(opcion=='Alta'){
				fpAltaP.show();
				fpAltaP.getForm().reset();
				if(fpRespuestasP!=null)fpRespuestasP.destroy();
				if(fpPreguntasP!=null)fpPreguntasP.destroy();
				fpConsultaP.getForm().reset();
				fpConsultaP.hide();
				if(gridEncuestasP!=null)gridEncuestasP.hide();
				if(fpModEncuestaP!=null)fpModEncuestaP.destroy();
				if(fpModPreguntasP!=null)fpModPreguntasP.destroy();
				if(fpModRespuestasP!=null)fpModRespuestasP.destroy();
				
				gridRespuestasP.hide();
				if(fpRespIndP!=null)fpRespIndP.destroy();
				if(fpMuestraRP!=null)fpMuestraRP.destroy();
				if(fpMuestraRCP!=null)fpMuestraRCP.destroy();
			}
			if(opcion=='Enc'){
				fpConsultaP.show();
				if(gridEncuestasP!=null)gridEncuestasP.hide();
				fpAltaP.hide();
				fpAltaP.getForm().reset();
				if(fpRespuestasP!=null)fpRespuestasP.destroy();
				if(fpPreguntasP!=null)fpPreguntasP.destroy();
				fpConsultaP.getForm().reset();
				if(fpModEncuestaP!=null)fpModEncuestaP.destroy();
				if(fpModPreguntasP!=null)fpModPreguntasP.destroy();
				if(fpModRespuestasP!=null)fpModRespuestasP.destroy();
				
				gridRespuestasP.hide();
				if(fpRespIndP!=null)fpRespIndP.destroy();
				if(fpMuestraRP!=null)fpMuestraRP.destroy();
				if(fpMuestraRCP!=null)fpMuestraRCP.destroy();
				
			}
			if(opcion=='Resp'){
				gridRespuestasP.show();
				fpAltaP.hide();
				fpAltaP.getForm().reset();
				if(fpRespuestasP!=null)fpRespuestasP.destroy();
				if(fpPreguntasP!=null)fpPreguntasP.destroy();
				
				fpConsultaP.getForm().reset();
				fpConsultaP.hide();
				if(gridEncuestasP!=null)gridEncuestasP.hide();
				if(fpModEncuestaP!=null)fpModEncuestaP.destroy();
				if(fpModPreguntasP!=null)fpModPreguntasP.destroy();
				if(fpModRespuestasP!=null)fpModRespuestasP.destroy();
				
				if(fpRespIndP!=null)fpRespIndP.destroy();
				if(fpMuestraRP!=null)fpMuestraRP.destroy();
				if(fpMuestraRCP!=null)fpMuestraRCP.destroy();
				
				var storeRespData = Ext.StoreMgr.key('storeRespData1'+tipoAfil);
				storeRespData.load({
					params:{
						tipoAfiliado:tipoAfil
					}
				});
			}
		
		}else if(tipoAfil=='E'){
			if(opcion=='Alta'){
				fpAltaE.show();
				fpAltaE.getForm().reset();
				if(fpRespuestasE!=null)fpRespuestasE.destroy();
				if(fpPreguntasE!=null)fpPreguntasE.destroy();
				fpConsultaE.getForm().reset();
				fpConsultaE.hide();
				if(gridEncuestasE!=null)gridEncuestasE.hide();
				if(fpModEncuestaE!=null)fpModEncuestaE.destroy();
				if(fpModPreguntasE!=null)fpModPreguntasE.destroy();
				if(fpModRespuestasE!=null)fpModRespuestasE.destroy();
				
				gridRespuestasE.hide();
				if(fpRespIndE!=null)fpRespIndE.destroy();
				if(fpMuestraRE!=null)fpMuestraRE.destroy();
				if(fpMuestraRCE!=null)fpMuestraRCE.destroy();
			}
			if(opcion=='Enc'){
				fpConsultaE.show();
				if(gridEncuestasE!=null)gridEncuestasE.hide();
				fpAltaE.hide();
				fpAltaE.getForm().reset();
				if(fpRespuestasE!=null)fpRespuestasE.destroy();
				if(fpPreguntasE!=null)fpPreguntasE.destroy();
				fpConsultaE.getForm().reset();
				if(fpModEncuestaE!=null)fpModEncuestaE.destroy();
				if(fpModPreguntasE!=null)fpModPreguntasE.destroy();
				if(fpModRespuestasE!=null)fpModRespuestasE.destroy();
				
				gridRespuestasE.hide();
				if(fpRespIndE!=null)fpRespIndE.destroy();
				if(fpMuestraRE!=null)fpMuestraRE.destroy();
				if(fpMuestraRCE!=null)fpMuestraRCE.destroy();
				
			}
			if(opcion=='Resp'){
				gridRespuestasE.show();
				fpAltaE.hide();
				fpAltaE.getForm().reset();
				if(fpRespuestasE!=null)fpRespuestasE.destroy();
				if(fpPreguntasE!=null)fpPreguntasE.destroy();
				
				fpConsultaE.getForm().reset();
				fpConsultaE.hide();
				if(gridEncuestasE!=null)gridEncuestasE.hide();
				if(fpModEncuestaE!=null)fpModEncuestaE.destroy();
				if(fpModPreguntasE!=null)fpModPreguntasE.destroy();
				if(fpModRespuestasE!=null)fpModRespuestasE.destroy();
				
				if(fpRespIndE!=null)fpRespIndE.destroy();
				if(fpMuestraRE!=null)fpMuestraRE.destroy();
				if(fpMuestraRCE!=null)fpMuestraRCE.destroy();
				
				var storeRespData = Ext.StoreMgr.key('storeRespData1'+tipoAfil);
				storeRespData.load({
					params:{
						tipoAfiliado:tipoAfil
					}
				});
			}
		}else if(tipoAfil=='L'){
			//tipoAfiliadoGral = tipoAfil;
			if(opcion=='Alta'){
				fpAltaPlantilla.show();
				fpAltaPlantilla.getForm().reset();
				if(fpRespuestasL!=null)fpRespuestasL.destroy();
				if(fpPreguntasL!=null)fpPreguntasL.destroy();
				fpConsultaL.getForm().reset();
				fpConsultaL.hide();
				if(gridPlantillaL!=null)gridPlantillaL.hide();
				if(fpModPlantillaL!=null)fpModPlantillaL.destroy();
				if(fpModPreguntasL!=null)fpModPreguntasL.destroy();
				if(fpModRespuestasL!=null)fpModRespuestasL.destroy();
				
				//gridRespuestasL.hide();
				/*
				if(fpRespIndP!=null)fpRespIndP.destroy();
				if(fpMuestraRP!=null)fpMuestraRP.destroy();
				if(fpMuestraRCP!=null)fpMuestraRCP.destroy();
				*/
			}
			if(opcion=='Enc'){
				fpConsultaL.show();
				if(gridPlantillaL!=null)gridPlantillaL.hide();
				fpAltaPlantilla.hide();
				fpAltaPlantilla.getForm().reset();
				if(fpRespuestasL!=null)fpRespuestasL.destroy();
				if(fpPreguntasL!=null)fpPreguntasL.destroy();
				fpConsultaL.getForm().reset();
				if(fpModPlantillaL!=null)fpModPlantillaL.destroy();
				if(fpModPreguntasL!=null)fpModPreguntasL.destroy();
				if(fpModRespuestasL!=null)fpModRespuestasL.destroy();
				
				//gridRespuestasL.hide();
				/*
				if(fpRespIndP!=null)fpRespIndP.destroy();
				if(fpMuestraRP!=null)fpMuestraRP.destroy();
				if(fpMuestraRCP!=null)fpMuestraRCP.destroy();
				*/
			}
		}
	}


//------------------------------------------------------------------------------
	

/*var storeRespData = new Ext.data.JsonStore({
		url : '15admencu01_ext.data.jsp',
		id: 'storeRespData1',
		baseParams:{
			informacion: 'ConsultaRespuestas'
		},
		root : 'registros',
		fields: [
			{name: 'CVEENCUESTA'},
			{name: 'FECPUBLIC'},
			{name: 'TITULO'},
			{name: 'CONTRESP'},
			{name: 'SELECCIONAR'},
			{name: 'NUMPREGUNTAS'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaRespData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaRespData(null, null, null);
				}
			}
		}
	});*/
	


//------------------------------------------------------------------------------
	//--ALTA----
	var fpAltaI = new NE.encuestas.FormAltaEncuestas({tipoAfiliado:'I', fnUsaPlantilla:fnUsaPlantillas});
	var fpRespuestasI;
	var fpPreguntasI;
	var fpVistaPreviaI;
	var gridEncPlantillaI = null
	
	//--CONSULTA-MODIFICACION---
	var fpConsultaI = new NE.encuestas.FormConsultaEncuestas({hidden: true, tipoAfiliado:'I'});
	var gridEncuestasI = null;
	var fpModEncuestaI = null;
	var fpModPreguntasI = null;
	var fpModRespuestasI = null;
	
	//--RESPUESTAS--
	var gridRespuestasI = new NE.encuestas.GridEncRespGral({hidden: true, tipoAfiliado:'I', fnLoadStore:procesarConsultaRespData});
	gridRespuestasI.setHandlerBtnRespInd(fnBtnRespIndividual);
	gridRespuestasI.setHandlerBtnRespCon(fnBtnRespConsolidada);
	var fpRespIndI = null;
	var fpMuestraRI = null;
	var fpMuestraRCI = null;
	
//-----
	//--ALTA----
	var fpAltaP = new NE.encuestas.FormAltaEncuestas({tipoAfiliado:'P',fnUsaPlantilla:fnUsaPlantillas});
	var fpRespuestasP;
	var fpPreguntasP;
	var gridEncPlantillaP = null;
	
	//--CONSULTA-MODIFICACION---
	var fpConsultaP = new NE.encuestas.FormConsultaEncuestas({hidden: true, tipoAfiliado:'P'});
	var gridEncuestasP = null;
	var fpModEncuestaP = null;
	var fpModPreguntasP = null;
	var fpModRespuestasP = null;
	
	//--RESPUESTAS--
	var gridRespuestasP = new NE.encuestas.GridEncRespGral({hidden: true, tipoAfiliado:'P', fnLoadStore:procesarConsultaRespData});
	gridRespuestasP.setHandlerBtnRespInd(fnBtnRespIndividual);
	gridRespuestasP.setHandlerBtnRespCon(fnBtnRespConsolidada);
	var fpRespIndP = null;
	var fpMuestraRP = null;
	var fpMuestraRCP = null;

//----
	//--ALTA----
	var fpAltaE = new NE.encuestas.FormAltaEncuestas({tipoAfiliado:'E', fnUsaPlantilla:fnUsaPlantillas});
	var fpRespuestasE;
	var fpPreguntasE;
	var gridEncPlantillaE = null
	
	//--CONSULTA--
	var fpConsultaE = new NE.encuestas.FormConsultaEncuestas({hidden: true, tipoAfiliado:'E'});
	var gridEncuestasE = null;
	var fpModEncuestaE = null;
	var fpModPreguntasE = null;
	var fpModRespuestasE = null;
	
	//--RESPUESTAS--
	var gridRespuestasE = new NE.encuestas.GridEncRespGral({hidden: true, tipoAfiliado:'E', fnLoadStore:procesarConsultaRespData});
	gridRespuestasE.setHandlerBtnRespInd(fnBtnRespIndividual);
	gridRespuestasE.setHandlerBtnRespCon(fnBtnRespConsolidada);
	var fpRespIndE = null;
	var fpMuestraRE = null;
	var fpMuestraRCE = null;
	
//----
	//--ALTA PLANTILLAS----
	var fpAltaPlantilla = new NE.encuestas.FormAltaPlantilla({tipoAfiliado:'L', width: 0, fnNumPreguntas:fnNumeroPreguntas});
	var fpRespuestasL;
	var fpPreguntasL;
	var fpVistaPreviaL;
	
	var fpConsultaL = new NE.encuestas.FormConsultaPlantillas({hidden: true, tipoAfiliado:'L'});
	var gridPlantillaL = null;
	var fpModPlantillaL = null;
	var fpModPreguntasL = null;
	var fpModRespuestasL = null;


fpAltaI.setHandlerBtnSiguiente(function(){
	var numPreguntas = Ext.getCmp('fp_numPreguntas1I');
	if(Ext.getCmp('fp_selecAfiliados1I').getValue()==''){
		Ext.MessageBox.alert('Mensaje','No hay afiliados agregados');
	}else{
		fpPreguntasI = new NE.encuestas.FormPreguntasEnc({numeroPreguntas:numPreguntas.getValue(), tipoAfiliado:'I'});
		fpAltaI.hide();
		tabEncIF.add(fpPreguntasI);
		tabEncIF.doLayout();
		
		
		fpPreguntasI.setHandlerBtnCancelar(function(){
			fpAltaI.show();
			fpPreguntasI.destroy();
		});
		fpPreguntasI.setHandlerBtnSiguiente(function(){
			fpRespuestasI = new NE.encuestas.FormRespEnc({formaPreguntas:fpPreguntasI, numOjetos:1, tipoAfiliado:'I'});
			fpAltaI.hide();
			fpPreguntasI.hide();
			tabEncIF.add(fpRespuestasI);
			tabEncIF.doLayout();
			
			fpRespuestasI.setHandlerBtnCancelar(function(){
				fpAltaI.show();
				fpPreguntasI.destroy();
				fpRespuestasI.destroy();
			});
			
			fpRespuestasI.setHandlerBtnSiguiente(function(){
				var objDisabled = fpRespuestasI.find('disabled',true);
				for(var i=0; i<objDisabled.length; i++){
					objDisabled[i].setValue('');
					objDisabled[i].enable();
				}
				
				Ext.Ajax.request({
					url: '15admencu01_ext.data.jsp',
					params:Ext.apply(fpAltaI.getForm().getValues(), Ext.apply(fpPreguntasI.getForm().getValues(), fpRespuestasI.getForm().getValues()),{
						informacion: 'GuardarEncuesta',
						tipoAfiliado: 'I',
						numPregReal: fpRespuestasI.numPregReal
					}),
					callback: processSuccessFailureGuardaEnc
				});
			});
			
			fpRespuestasI.setHandlerBtnVistaPrevia(function(){
				var objDisabled = fpRespuestasI.find('disabled',true);
				for(var i=0; i<objDisabled.length; i++){
					objDisabled[i].setValue('');
					objDisabled[i].enable();
				}
				
				var titulo = Ext.getCmp('fp_titulo1I');
				fpVistaPreviaI = new NE.encuestas.FormVistaPrevEnc({formaPreguntas:fpPreguntasI, numOjetos:1, tipoAfiliado:'I', titulo:titulo.getValue()});

				new Ext.Window({
					modal: true,
					resizable: false,
					layout: 'form',
					x: 100,
					width: 810,
					id: 'winVistaPrevI',
					closable: true,
					closeAction: 'close',
					items: [ fpVistaPreviaI ],
					title: 'Vista Previa'
				}).show();
			});
		});
	}
});

fpAltaI.setHandlerBtnGuardar(function(){
	var numPreguntas = Ext.getCmp('fp_numPreguntas1I');
	if(Ext.getCmp('fp_selecAfiliados1I').getValue()==''){
		Ext.MessageBox.alert('Mensaje','No hay afiliados agregados');
	}else if(plantSeleccionadaI==''){
		Ext.MessageBox.alert('Mensaje','Seleccione una plantilla');
	}else{
		
		Ext.Ajax.request({
			url: '15admencu01_ext.data.jsp',
			params:Ext.apply(fpAltaI.getForm().getValues(),{
				informacion: 'GuardarEncuestaPlant',
				tipoAfiliado: tipoAfiliadoGral,
				cvePlantilla: plantSeleccionadaI
			}),
			callback: processSuccessFailureGuardarEncPlant
		});
		
	}
});

fpAltaP.setHandlerBtnGuardar(function(){
	var numPreguntas = Ext.getCmp('fp_numPreguntas1P');
	if(Ext.getCmp('fp_selecAfiliados1P').getValue()==''){
		Ext.MessageBox.alert('Mensaje','No hay afiliados agregados');
	}else if(plantSeleccionadaP==''){
		Ext.MessageBox.alert('Mensaje','Seleccione una plantilla');
	}else{
		
		Ext.Ajax.request({
			url: '15admencu01_ext.data.jsp',
			params:Ext.apply(fpAltaP.getForm().getValues(),{
				informacion: 'GuardarEncuestaPlant',
				tipoAfiliado: tipoAfiliadoGral,
				cvePlantilla: plantSeleccionadaP
			}),
			callback: processSuccessFailureGuardarEncPlant
		});
		
	}
});

fpAltaE.setHandlerBtnGuardar(function(){
	var numPreguntas = Ext.getCmp('fp_numPreguntas1E');
	if(Ext.getCmp('fp_selecAfiliados1E').getValue()==''){
		Ext.MessageBox.alert('Mensaje','No hay afiliados agregados');
	}else if(plantSeleccionadaE==''){
		Ext.MessageBox.alert('Mensaje','Seleccione una plantilla');
	}else{
		
		Ext.Ajax.request({
			url: '15admencu01_ext.data.jsp',
			params:Ext.apply(fpAltaE.getForm().getValues(),{
				informacion: 'GuardarEncuestaPlant',
				tipoAfiliado: tipoAfiliadoGral,
				cvePlantilla: plantSeleccionadaE
			}),
			callback: processSuccessFailureGuardarEncPlant
		});
		
	}
});

fpAltaP.setHandlerBtnSiguiente(function(){
	var numPreguntas = Ext.getCmp('fp_numPreguntas1P');
	if(Ext.getCmp('fp_selecAfiliados1P').getValue()==''){
		Ext.MessageBox.alert('Mensaje','No hay afiliados agregados');
	}else{
		fpPreguntasP = new NE.encuestas.FormPreguntasEnc({numeroPreguntas:numPreguntas.getValue(), tipoAfiliado:'P'});
		fpAltaP.hide();
		tabEncPYME.add(fpPreguntasP);
		tabEncPYME	.doLayout();
		
		fpPreguntasP.setHandlerBtnSiguiente(function(){
			fpRespuestasP = new NE.encuestas.FormRespEnc({formaPreguntas:fpPreguntasP, numOjetos:1, tipoAfiliado:'P'});
			fpAltaP.hide();
			fpPreguntasP.hide();
			tabEncPYME.add(fpRespuestasP);
			tabEncPYME.doLayout();
			

			fpRespuestasP.setHandlerBtnCancelar(function(){
				fpAltaP.show();
				fpPreguntasP.destroy();
				fpRespuestasP.destroy();
			});
			
			fpRespuestasP.setHandlerBtnSiguiente(function(){
				
				Ext.Ajax.request({
					url: '15admencu01_ext.data.jsp',
					params:Ext.apply(fpAltaP.getForm().getValues(), Ext.apply(fpPreguntasP.getForm().getValues(), fpRespuestasP.getForm().getValues()),{
						informacion: 'GuardarEncuesta',
						tipoAfiliado: 'P'
					}),
					callback: processSuccessFailureGuardaEnc
				});
			
			});
			
			fpRespuestasP.setHandlerBtnVistaPrevia(function(){
				var objDisabled = fpRespuestasP.find('disabled',true);
				for(var i=0; i<objDisabled.length; i++){
					objDisabled[i].setValue('');
					objDisabled[i].enable();
				}
				
				var titulo = Ext.getCmp('fp_titulo1P');
				fpVistaPreviaP = new NE.encuestas.FormVistaPrevEnc({formaPreguntas:fpPreguntasP, numOjetos:1, tipoAfiliado:'P', titulo:titulo.getValue()});

				new Ext.Window({
					modal: true,
					resizable: false,
					layout: 'form',
					x: 100,
					width: 810,
					id: 'winVistaPrevI',
					closable: true,
					closeAction: 'close',
					items: [ fpVistaPreviaP ],
					title: 'Vista Previa'
				}).show();
			});
			
		});
	}
});



fpAltaE.setHandlerBtnSiguiente(function(){
	var numPreguntas = Ext.getCmp('fp_numPreguntas1E');
	if(Ext.getCmp('fp_selecAfiliados1E').getValue()==''){
		Ext.MessageBox.alert('Mensaje','No hay afiliados agregados');
	}else{
		fpPreguntasE = new NE.encuestas.FormPreguntasEnc({numeroPreguntas:numPreguntas.getValue(), tipoAfiliado:'E'});
		fpAltaE.hide();
		tabEncEPO.add(fpPreguntasE);
		tabEncEPO.doLayout();
		
		fpPreguntasE.setHandlerBtnSiguiente(function(){
			fpRespuestasE = new NE.encuestas.FormRespEnc({formaPreguntas:fpPreguntasE, numOjetos:1, tipoAfiliado:'E'});
			fpAltaE.hide();
			fpPreguntasE.hide();
			tabEncEPO.add(fpRespuestasE);
			tabEncEPO.doLayout();
			
			fpRespuestasE.setHandlerBtnCancelar(function(){
				fpAltaE.show();
				fpPreguntasE.destroy();
				fpRespuestasE.destroy();
			});
			
			fpRespuestasE.setHandlerBtnSiguiente(function(){
				
				Ext.Ajax.request({
					url: '15admencu01_ext.data.jsp',
					params:Ext.apply(fpAltaE.getForm().getValues(), Ext.apply(fpPreguntasE.getForm().getValues(), fpRespuestasE.getForm().getValues()),{
						informacion: 'GuardarEncuesta',
						tipoAfiliado: 'E'
					}),
					callback: processSuccessFailureGuardaEnc
				});
			
			});
			
			fpRespuestasE.setHandlerBtnVistaPrevia(function(){
				var objDisabled = fpRespuestasE.find('disabled',true);
				for(var i=0; i<objDisabled.length; i++){
					objDisabled[i].setValue('');
					objDisabled[i].enable();
				}
				
				var titulo = Ext.getCmp('fp_titulo1E');
				fpVistaPreviaE = new NE.encuestas.FormVistaPrevEnc({formaPreguntas:fpPreguntasE, numOjetos:1, tipoAfiliado:'E', titulo:titulo.getValue()});

				new Ext.Window({
					modal: true,
					resizable: false,
					layout: 'form',
					x: 100,
					width: 810,
					id: 'winVistaPrevI',
					closable: true,
					closeAction: 'close',
					items: [ fpVistaPreviaE ],
					title: 'Vista Previa'
				}).show();
			});
			
		});
	}
});

fpConsultaI.setHandlerBtnConsultar(function(){
		var continua = true;
		fpConsultaI.getForm().items.each(function(itm){ 
			if(!itm.isValid()){
				continua = false;
			}
		}); 
		
		if(continua){
			if(gridEncuestasI==null){
				gridEncuestasI =	new NE.encuestas.GridEncuestas({tipoAfiliado:'I', fnLoadStore:procesarConsultaEncData, fnModificacion:muestraEncModificar});
			}
			
			var storeEncuestasData = Ext.StoreMgr.key('storeEncuestasData1I');
			storeEncuestasData.load({
				params:Ext.apply(fpConsultaI.getForm().getValues(),{
					informacion: 'ConsultaEncuestas',
					tipoAfiliado: 'I'
				})
			});
		}
	
});

fpConsultaE.setHandlerBtnConsultar(function(){
		var continua = true;
		fpConsultaE.getForm().items.each(function(itm){ 
			if(!itm.isValid()){
				continua = false;
			}
		});
		
		if(continua){
			if(gridEncuestasE==null){
				gridEncuestasE =	new NE.encuestas.GridEncuestas({tipoAfiliado:'E', fnLoadStore:procesarConsultaEncData, fnModificacion:muestraEncModificar});
			}
			
			var storeEncuestasData = Ext.StoreMgr.key('storeEncuestasData1E');
			storeEncuestasData.load({
				params:Ext.apply(fpConsultaE.getForm().getValues(),{
					informacion: 'ConsultaEncuestas',
					tipoAfiliado: 'E'
				})
			});
		}
});

fpConsultaP.setHandlerBtnConsultar(function(){
		var continua = true;
		fpConsultaP.getForm().items.each(function(itm){ 
			if(!itm.isValid()){
				continua = false;
			}
		});
		
		if(continua){
			if(gridEncuestasP==null){
				gridEncuestasP =	new NE.encuestas.GridEncuestas({tipoAfiliado:'P', fnLoadStore:procesarConsultaEncData, fnModificacion:muestraEncModificar});
			}
			
			var storeEncuestasData = Ext.StoreMgr.key('storeEncuestasData1P');
			storeEncuestasData.load({
				params:Ext.apply(fpConsultaP.getForm().getValues(),{
					informacion: 'ConsultaEncuestas',
					tipoAfiliado: 'P'
				})
			});
		}
});


fpConsultaL.setHandlerBtnConsultar(function(){
		
		if(gridPlantillaL==null){
			gridPlantillaL =	new NE.encuestas.GridPlantillas({tipoAfiliado:'L', fnLoadStore:procesarConsultaPlantData, fnModificacion:muestraPlantillaModificar});
		}
		
		var storePlantillaData = Ext.StoreMgr.key('storePlantillasData1L');
		storePlantillaData.load({
			params:Ext.apply(fpConsultaL.getForm().getValues(),{
				informacion: 'ConsultaPlantillas',
				tipoAfiliado: 'L'
			})
		});
});

//------------------------------------------------------------------------------









//------------------------------------------------------------------------------
	//TAB UNO
	var tabEncIF = new Ext.Panel({
		title:'Encuestas IF',
		frame: false,
		id:'tabEncIF1',
		width: '500',
		height: '550',
		tipoAfiliado:'I',
		autoScroll: true,
		items:[NE.util.getEspaciador(20),
				fpAltaI,
				fpConsultaI,
				gridRespuestasI
				//NE.util.getEspaciador(20)
				],
		tbar:[
			'-',
			{
            text:'Alta',
				enableToggle:true,
				pressed: true,
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoAgregarForma',
            scope:this,
				toggleGroup: 'brnIF',
				toggleHandler: function(btn, pressed){
               if(pressed) seleOpcion('I','Alta',pressed);
            }
        },
		  '-',
        {
            text:'Consulta',
				enableToggle:true,
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoBuscar',
				toggleGroup: 'brnIF',
            toggleHandler: function(btn, pressed){
					if(pressed) seleOpcion('I','Enc',pressed);
				},
            scope:this
        },
        '-',
        {
            enableToggle:true,
            text:'Respuestas',
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoEditarForma',
				toggleGroup: 'brnIF',
            scope:this,
            toggleHandler: function(btn, pressed){
               if(pressed) seleOpcion('I','Resp',pressed);
            }
        }]
	});
	
	//TAB DOS
	var tabEncPYME = new Ext.Panel({
		title:'Encuestas PYME',
		frame: false,
		id:'tabEncPYME1',
		height: 550,
		tipoAfiliado:'P',
		autoScroll: true,
		items:[NE.util.getEspaciador(20),
				fpAltaP,
				fpConsultaP,
				gridRespuestasP
				//NE.util.getEspaciador(20)
				],
		tbar:[
			'-',
			{
            text:'Alta',
				enableToggle:true,
				pressed: true,
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoAgregarForma',
            scope:this,
				toggleGroup: 'brnPYME',
				toggleHandler: function(btn, pressed){
               if(pressed) seleOpcion('P','Alta',pressed);
            }
        },
		  '-',
        {
            text:'Consulta',
				enableToggle:true,
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoBuscar',
				toggleGroup: 'brnPYME',
            toggleHandler: function(btn, pressed){
					if(pressed) seleOpcion('P','Enc',pressed);
				},
            scope:this
        },
        '-',
        {
            enableToggle:true,
            text:'Respuestas',
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoEditarForma',
				toggleGroup: 'brnPYME',
            scope:this,
            toggleHandler: function(btn, pressed){
               if(pressed) seleOpcion('P','Resp',pressed);
            }
        }]
	});
	
	//TAB TRES
	var tabEncEPO = new Ext.Panel({
		title:'Encuestas EPO',
		frame: false,
		id:'tabEncEPO1',
		height: 550,
		tipoAfiliado:'E',
		autoScroll: true,
		items:[NE.util.getEspaciador(20),
				fpAltaE,
				fpConsultaE,
				gridRespuestasE
				//NE.util.getEspaciador(20)
				],
		tbar:[
			'-',
			{
            text:'Alta',
				enableToggle:true,
				pressed: true,
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoAgregarForma',
            scope:this,
				toggleGroup: 'brnEPO',
				toggleHandler: function(btn, pressed){
               if(pressed) seleOpcion('E','Alta',pressed);
            }
        },
		  '-',
        {
            text:'Consulta',
				enableToggle:true,
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoBuscar',
				toggleGroup: 'brnEPO',
            toggleHandler: function(btn, pressed){
					if(pressed) seleOpcion('E','Enc',pressed);
				},
            scope:this
        },
        '-',
        {
            enableToggle:true,
            text:'Respuestas',
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoEditarForma',
				toggleGroup: 'brnEPO',
            scope:this,
            toggleHandler: function(btn, pressed){
               if(pressed) seleOpcion('E','Resp',pressed);
            }
        }]
	});
	
	//TAB CUATRO
	var tabEncAltaPlant = new Ext.Panel({
		title:'Alta Plantillas',
		frame: false,
		id:'tabEncAltaPlant1',
		height: 550,
		tipoAfiliado:'L',
		autoScroll: true,
		items:[NE.util.getEspaciador(20),
				fpAltaPlantilla,
				fpConsultaL
				//gridRespuestasL
				//NE.util.getEspaciador(20)
				],
		tbar:[
			'-',
			{
            text:'Alta',
				enableToggle:true,
				pressed: true,
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoAgregarForma',
            scope:this,
				toggleGroup: 'brnEPO',
				toggleHandler: function(btn, pressed){
               if(pressed) seleOpcion('L','Alta',pressed);
            }
        },
		  '-',
        {
            text:'Consulta',
				enableToggle:true,
            tooltip: {title:'Prueba',text:'En construcci�n'},
            iconCls: 'icoBuscar',
				toggleGroup: 'brnEPO',
            toggleHandler: function(btn, pressed){
					if(pressed) seleOpcion('L','Enc',pressed);
				},
            scope:this
        }]
	});
	
	//TAB PRINCIPAL
	var tabGralReqMensual = new Ext.TabPanel({
		id: 'tabGralReqMensual1',
		border: true,
		frame: true,
		activeTab: 0,
		style:			'margin:0 auto;',
		items:[ tabEncIF, tabEncPYME, tabEncEPO, tabEncAltaPlant],
		listeners:{
			tabchange: function(ObjTab, newTabPanel){
				tipoAfiliadoGral= newTabPanel.tipoAfiliado;
				//pregSeleccionada = '';
			
			}
		}
	});
	
	var fpArchivo = new Ext.form.FormPanel({
		id: 'fpArchivo1',
		width: 600,
		title: '',
		frame: true,
		hidden: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;'
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fpArchivo,
			NE.util.getEspaciador(10),
			tabGralReqMensual,
			NE.util.getEspaciador(10)
		]
	});


});
