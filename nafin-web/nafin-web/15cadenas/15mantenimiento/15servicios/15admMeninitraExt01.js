Ext.onReady(function() {
	var iTipoPerfil =  Ext.getDom('iTipoPerfil').value;
	var Af = '';
	var Pu = '';
//-----------------------------procesarConsultaData-----------------------------
	var procesarConsultaData = function(store,arrRegistros,opts){
	var fp = Ext.getCmp('formaInicial');
		fp.el.unmask();
			if(arrRegistros!=null){
				var grid = Ext.getCmp('gridConsulta');
				if (!grid.isVisible()) {
					grid.show();
					fpInicial.hide();
				}
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
	}
	function procesaValoresIniciales(opts, success, response) {
			fpModifica.hide();
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var pub = jsonData.tipoPublicacion;
				var afi = jsonData.tipoAfiliado;
				var fond = jsonData.banFondeo;
				var iTipoPerfil = jsonData.iTipoPerfil;
				if(afi=='E' || afi=='P' ){
					storeCatEpoData.load({params : Ext.apply({tipoAfi :Ext.getCmp('cmbTipoAfi').getValue()})} );
					Ext.getCmp('isSelectEpo').show();
				}else{
					Ext.getCmp('isSelectEpo').hide();
				}
				if((afi=='I' || afi=='P')&& pub!='C'){
					storeCatIFData.load({params : Ext.apply({tipoAfi :Ext.getCmp('cmbTipoAfi').getValue(),tipoPub : Ext.getCmp('cmbTipoPub').getValue()})} );
					Ext.getCmp('isSelectIf').show();
				}else{
					Ext.getCmp('isSelectIf').hide();
				}
				if( pub!='C'){
					Ext.getCmp('txtTituloG').show();
					Ext.getCmp('txtContenido').show();
				}else{
					Ext.getCmp('txtTituloG').show();
					Ext.getCmp('txtContenido').hide();
				}
				if( pub =='C'){
					Ext.getCmp('panelDer').show();
					Ext.getCmp('txtNomCampa�aG').show();
					Ext.getCmp('txtTituloG').hide();
				}else if(pub !='C'){
					Ext.getCmp('txtNomCampa�aG').hide();
					Ext.getCmp('panelDer').show();
				}
				catTipoPub.load({ params: Ext.apply({tipoAfi :Ext.getCmp('cmbTipoAfi').getValue()})});
				catTipoAfi.load({ params: Ext.apply({tipoPub : Ext.getCmp('cmbTipoPub').getValue()})});
				//storeCatBancoFon.load();
				pnl.el.unmask();
									
			}
		} else {
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	
	}
	function procesaValoresCarga(opts, success, response) {
		fpModifica.hide();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				
				var pub = jsonData.tipoPublicacion;
				var afi = jsonData.tipoAfiliado;
				var fond = jsonData.banFondeo;
				if(afi=='E' || afi=='P' ){
					storeCatEpoData.load({params : Ext.apply({tipoAfi :Ext.getCmp('cmbTipoAfi').getValue()})} );
					Ext.getCmp('isSelectEpo').show();
				}else{
					Ext.getCmp('isSelectEpo').hide();
				}
				if((afi=='I' || afi=='P')&& pub!='C'){
					storeCatIFData.load({params : Ext.apply({tipoAfi :Ext.getCmp('cmbTipoAfi').getValue(),tipoPub : Ext.getCmp('cmbTipoPub').getValue()})} );
					Ext.getCmp('isSelectIf').show();
				}else{
					Ext.getCmp('isSelectIf').hide();
				}
				if( pub!='C'){
					Ext.getCmp('txtTituloG').show();
					Ext.getCmp('txtContenido').show();
				}else{
					Ext.getCmp('txtTituloG').show();
					Ext.getCmp('txtContenido').hide();
				}
				if( pub =='C'){
					Ext.getCmp('panelDer').show();
					Ext.getCmp('txtNomCampa�aG').show();
					Ext.getCmp('txtTituloG').hide();
				}else if(pub !='C'){
					Ext.getCmp('txtNomCampa�aG').hide();
					Ext.getCmp('panelDer').show();
				}
			
				catTipoPub.load({ params: Ext.apply({tipoAfi :Ext.getCmp('cmbTipoAfi').getValue()})});
				catTipoAfi.load({ params: Ext.apply({tipoPub : Ext.getCmp('cmbTipoPub').getValue()})});
				//storeCatBancoFon.load();
								
									
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesaRegresa(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				Ext.getCmp('vistaPrevia').destroy();
			}
				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function TransmiteBorrar(opts, success, response) {
		var grid = Ext.getCmp('gridConsulta');
		grid.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var mensaje = jsonData.mensaje;
				Ext.MessageBox.alert('Mensaje',mensaje);
				ConsultaData.load({ params: Ext.apply(fpInicial.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
			}
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	function TransmiteConfirmar1(opts, success, response) {
		var formaInicial = Ext.getCmp('formaInicial');
		formaInicial.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var mensaje = jsonData.mensaje;
				Ext.MessageBox.alert('Mensaje',mensaje);
				fpInicial.getForm().reset();
				Ext.Ajax.request({
					url: '15admMeninitraExt01.data.jsp',
					params:Ext.apply(fpInicial.getForm().getValues(),{
						informacion: 'valoresIniciales'
						}),
					callback: procesaValoresIniciales
				});
			}
		} else {
			formaInicial.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
	function TransmiteConfirmar(opts, success, response) {
		var formaInicial = Ext.getCmp('formaInicial');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var mensaje = jsonData.mensaje;
				var ic_mensaje = jsonData.icMensajeIni;
				var arch = Ext.getCmp('fufCargaImagen').getValue();
				var forma = formaInicial.getForm();
				if(ic_mensaje!='0'&&arch!=''){
					forma.submit({
						url: '15admMeninitraExt01.data.jsp?informacion=cargaArchivo&icMensajeIni='+ic_mensaje,
						success:function(form, action, resp) {
							TransmiteConfirmar1(null,  true,  action.response );
						},
						failure: function(form, action) {
							 TransmiteConfirmar1(null,  true, action.response );
						}
					});
				}else{
					formaInicial.el.unmask();
					Ext.MessageBox.alert('Mensaje',mensaje);
					fpInicial.getForm().reset();
					Ext.Ajax.request({
						url: '15admMeninitraExt01.data.jsp',
						params:Ext.apply(fpInicial.getForm().getValues(),{
							informacion: 'valoresIniciales'
							}),
						callback: procesaValoresIniciales
					});
				}
			}
		} else {
			formaInicial.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function TransmiteVistaPrevia(opts, success, response) {
		var formaInicial = Ext.getCmp('formaInicial')
		formaInicial.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var urlArchivo = jsonData.urlArchivo;
				
				var nombreArchivo = jsonData.nombreArchivo;
				mensajes2 = ['<table width="590" align="center" cellpadding="0" cellspacing="0" border="0" bordercolor="#A5B8BF" class="formas" >'+			
				'<tr align="center" >	<td width="590" height="390" align="center"  ><img src='+urlArchivo+' align="center" ></td></tr>'+		
				'<tr><td colspan="5">&nbsp;</td></tr>'+
				'</table>'];
				var ventana = Ext.getCmp('vistaPrevia');
				if (ventana) {
					ventana.show();
				}else {	
					new Ext.Window({			
						width: 715,
						height: 460,
						resizable: false,
						closable:false,
						closeAction: 'hide',
						id: 'vistaPrevia',
						align: 'center',
						items:[
							{ 
								xtype: 'panel',
								id: 'Consentimiento',
								width: 700,
								height: 400,
								hidden: false,
								align: 'center',
								autoScroll: true,  
								html:		mensajes2.join(''), 
								cls:		'x-form-item',
								style: { 
									width:   		'100%', 
									textAlign: 		'center'
								} 
							}
						],
					buttons: [
					{
						xtype: 'button',
						text: 'Regresar',
						id: 'btnRegresar',
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '15admMeninitraExt01.data.jsp',
								params: Ext.apply({
									informacion: 'regresar',
									nombreArch: nombreArchivo
								})
								,callback: procesaRegresa
							});
						}	
					}
				],
				title: 'Vista Previa'						
				}).show();
			}
		}
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	function procesarSuccessFailureModificar(opts, success, response) {
		var fpModifica = Ext.getCmp('formaModifica');
		fpModifica.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				fpModifica.hide();
				var mensaje = jsonData.mensaje;
				Ext.MessageBox.alert('Mensaje',mensaje);
				var formaModifica=Ext.getCmp('formaModifica');
				formaModifica.getForm().reset();
				ConsultaData.load({ params: Ext.apply(fpInicial.getForm().getValues(),{
									informacion: 'Consultar'
								})
						});
			}
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	
	var procesarEliminar = function(grid, rowIndex, colIndex, item, event) {
		var grid = Ext.getCmp('gridConsulta');
		var registro = grid.getStore().getAt(rowIndex);
		var IC_MENSAJE_INI = registro.get('IC_MENSAJE_INI'); 
		grid.el.mask('Eliminando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '15admMeninitraExt01.data.jsp',
			params: Ext.apply({
				informacion: 'eliminaMensaje',
				IC_MENSAJE_INI:IC_MENSAJE_INI
			}),
			callback: TransmiteBorrar
		});
	}
	var procesarModificar = function(grid, rowIndex, colIndex, item, event) {
		Ext.getCmp('gridConsulta').hide();
		fpModifica.show();
		
		var grid = Ext.getCmp('gridConsulta');
		var registro = grid.getStore().getAt(rowIndex);
		var IC_MENSAJE_INI = registro.get('IC_MENSAJE_INI'); 
		var FECHA_INI = registro.get('FECHA_INI'); 
		var FECHA_FIN = registro.get('FECHA_FIN'); 
		var CG_TITULO = registro.get('CG_TITULO'); 
		var CG_CONTENIDO = registro.get('CG_CONTENIDO');
		Ext.getCmp('txtIc_mensaje').setValue(IC_MENSAJE_INI);
		Ext.getCmp('txtTituloMod').setValue(CG_TITULO);
		Ext.getCmp('txtContenidoM').setValue(CG_CONTENIDO);
		Ext.getCmp('dfFechaModA').setValue(FECHA_INI);
		Ext.getCmp('dfFechaModB').setValue(FECHA_FIN);
		
	}

//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
	
	var procesarTipoP = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			if(Pu==''||(Af=='E'||Af=='I'||Af=='P')){
				Ext.getCmp('cmbTipoPub').setValue('M');
			}
			
		}
	}
	var procesarTipoA= function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			if(Af==''||(Pu=='M'||Pu=='C')){
				Ext.getCmp('cmbTipoAfi').setValue('P');
			}
			
		}
	}
	/*var procesarBanF= function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			if(iTipoPerfil=='8'){
					Ext.getCmp('cmbBancoFon').setValue('2');
			}else{
					Ext.getCmp('cmbBancoFon').setValue('1');
			}
		}
	}*/
	var catTipoPub = new Ext.data.JsonStore({
		id: 'catTipoPub',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admMeninitraExt01.data.jsp',
		baseParams: {
			informacion: 'catTipoPub'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load:procesarTipoP,
			exception: NE.util.mostrarDataProxyError
		}		
	});
	var catTipoAfi = new Ext.data.JsonStore({
		id: 'catTipoAfi',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admMeninitraExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoAfiliado'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			
			load:procesarTipoA,
			exception: NE.util.mostrarDataProxyError
		}		
	});
	/*var storeCatBancoFon = new Ext.data.JsonStore({
		id: 'catBancoFon',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admMeninitraExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoBancoFon'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load:procesarBanF,
			exception: NE.util.mostrarDataProxyError
		}		
	});*/
	
	
	var storeCatEpoData = new Ext.data.JsonStore({
		id: 'CatEpoData',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15admMeninitraExt01.data.jsp',
		baseParams: {
			informacion: 'CatalogoEpoData'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});
	var storeCatEpoBData = new Ext.data.JsonStore({
		id: 'storeCatEpoBData',
		root : 'registrosB',
		url : '15admMeninitraExt01.data.jsp',
		fields : ['clave', 'descripcion','loadMsg'],
		baseParams: {
			informacion: 'storeCatEpoBData'
		},
		totalProperty : 'totalB',
		autoLoad: false
	});
	var storeCatIFData = new Ext.data.JsonStore({
		id: 'CatIFData',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15admMeninitraExt01.data.jsp',
		baseParams: {
			informacion: 'CatalogoIFData'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});
	var storeCatIFBData = new Ext.data.JsonStore({
		id: 'storeCatIFBData',
		root : 'registrosB',
		url : '15admMeninitraExt01.data.jsp',
		fields : ['clave', 'descripcion','loadMsg'],
		baseParams: {
			informacion: 'storeCatIFBData'
		},
		totalProperty : 'totalB',
		autoLoad: false
	});
	
	var ConsultaData = new Ext.data.JsonStore({
	root: 'registros',
	url: '15admMeninitraExt01.data.jsp',
	baseParams: {
		informacion: 'Consultar'
	},
	fields: [
				{name: 'IC_MENSAJE_INI'},
				{name: 'FECHA_INI'},
				{name: 'FECHA_FIN'},
				{name: 'CG_TITULO'},
				{name: 'CG_CONTENIDO'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, { params: Ext.apply(fpInicial.getForm().getValues())});
						}	},
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
	
//----------------------------Fin ConsultaData----------------------------------

//-------------------------------elementosForma---------------------------------	
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id : 'gridConsulta',
		store: ConsultaData,
		emptyMsg: "No hay registros.",		
		title: 'Consulta',
		hidden:	true,
		frame: true,
		loadMask: true,
		stripeRows: true,
		style: 'margin: 0 auto;',
		columns: [
			{
				header: 'Fecha de Publicaci�n',
				dataIndex: 'FECHA_INI',
				sortable: true,
				resizable: true,
				align: 'center',
				width: 100
			},{
				header: 'Fecha de vigencia',
				dataIndex: 'FECHA_FIN',
				width: 100,
				sortable: true,
				resizable: true,
				align: 'center'
			},{
				header: 'T�tulo',
				dataIndex: 'CG_TITULO',
				width: 250,	
				sortable: true,
				resizable: true,
				align: 'left'
			},{
				header: 'Mensaje Inicial',
				dataIndex: 'CG_CONTENIDO',
				width: 400,	
				sortable: true,
				resizable: true,
				align: 'LEFT'
			},{
				xtype		: 'actioncolumn',
				header: 'Seleccionar',
				tooltip: 'ELIMINAR ',
				id			: 'dtnEliminar',
				width		:  100,	
				sortable: true,
				resizable: true,
				align		: 'center', 
				hidden	: false,
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Modifica';
							return 'modificar';
						},
						handler:procesarModificar
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Elimina';
							return 'borrar';
						},
						handler:procesarEliminar
					}
				]
			}
		],
		buttons: [
					{
						xtype: 'button',
						text: 'Regresar',
						id: 'btnRegresaConsultar',
						handler: function(boton, evento) {
							window.location.href='15admMeninitraExt01.jsp';
						}	
					}
		],
		width: 930,
		height: 410
	});

	var elementosFormaInicial =  [
		{
			xtype: 'combo',
			name: 'tipoPub',
			id: 'cmbTipoPub',
			fieldLabel: 'Tipo de Publicaci�n',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'tipoPub',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catTipoPub,
			width: 270,
			
			listeners: {
				select: {
					fn: function(combo) {
						var tipoAfili = Ext.getCmp('cmbTipoAfi');	
						catTipoAfi.load({
							params: {
								tipoPub:combo.getValue()
							}
						});
						Pu = Ext.getCmp('cmbTipoAfi').getValue();
						Ext.Ajax.request({
							url: '15admMeninitraExt01.data.jsp',
							params:Ext.apply(fpInicial.getForm().getValues(),{
								informacion: 'valoresIniciales'
							}),
							callback: procesaValoresCarga
						});
						tipoAfili.setValue('');								
					}
				}
			},
			tpl : NE.util.templateMensajeCargaCombo
	  },
	  {
			xtype: 'combo',
			name: 'tipoAfi',
			id: 'cmbTipoAfi',
			hiddenName : 'tipoAfi',
			fieldLabel: 'Tipo de Afiliado',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',
			emptyText: 'Preveedores',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catTipoAfi,
			width: 270,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var tipoPub = Ext.getCmp('cmbTipoPub');
						catTipoPub.load({
							params: {
								tipoAfi:combo.getValue()
							}
						});
						 Af = Ext.getCmp('cmbTipoAfi').getValue();
						Ext.Ajax.request({
							url: '15admMeninitraExt01.data.jsp',
							params:Ext.apply(fpInicial.getForm().getValues(),{
								informacion: 'valoresIniciales'
							}),
							callback: procesaValoresCarga
						});
					}
				}
			}	
	  },
	 /* {
			xtype: 'combo',
			name: 'bancoFon',
			id: 'cmbBancoFon',
			fieldLabel: 'Banco de Fondeo',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'bancoFon',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : storeCatBancoFon,
			width: 270,
			tpl : NE.util.templateMensajeCargaCombo
	  },*/
	  {
			xtype: 'itemselector',
			name: 'selectEpo',
			id: 'isSelectEpo',
			fieldLabel: 'EPO',
			imagePath: '/nafin/00utils/extjs/ux/images/',
			drawUpIcon:false, 
			drawDownIcon:false, 
			drawTopIcon:false, 
			drawBotIcon:false,
			border: false,
			multiselects: [{
				 width: 286,
				 height: 150,
				 legend: 'Disponibles',
				 store: storeCatEpoData,
				 displayField: 'descripcion',
				 valueField: 'clave'
			},{
				 width: 286,
				 height: 150,
				 legend: 'Seleccionados',
				 store: storeCatEpoBData,
				 allowBlank: false,
			    displayField: 'descripcion',
				 valueField: 'clave'
			}]
	 },
	 {
			xtype: 'itemselector',
			name: 'selectIf',
			id: 'isSelectIf',
			fieldLabel: 'IF',
			imagePath: '/nafin/00utils/extjs/ux/images/',
			drawUpIcon:false, 
			drawDownIcon:false, 
			drawTopIcon:false, 
			drawBotIcon:false,
			border: false,
			multiselects: [{
				 width: 286,
				 height: 150,
				 legend: 'Disponibles',
				 store: storeCatIFData,
				 displayField: 'descripcion',
				 valueField: 'clave'
			},{
				 width: 286,
				 height: 150,
				 legend: 'Seleccionados',
				 store: storeCatIFBData,
				 allowBlank: false,
			    displayField: 'descripcion',
				 valueField: 'clave'
			}]
	 },
	
	 {
			xtype: 'compositefield',
			fieldLabel: 'Fecha de publicaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'dfFechaPubA',
					id: 'dfFechaPubA',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'dfFechaPubB',
					startDay: 0,
					width: 100,
					maxLength: 10,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 60
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 10
				},
				{
					xtype: 'displayfield',
					value: 'Hasta',
					width: 50
				},
				{
					xtype: 'datefield',
					name: 'dfFechaPubB',
					id: 'dfFechaPubB',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoInicioFecha: 'dfFechaPubA',
					startDay: 1,
					width: 100,
					maxLength: 10,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 100
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'T�tulo',
			id: 'txtTituloG',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'titulo',
					id: 'txtTitulo',
					fieldLabel: 'T�tulo',					
					allowBlank: true,
					startDay: 0,
					maxLength: 35,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 400,
					msgTarget: 'side',						
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			id: 'txtNomCampa�aG',
			items: [
				{
					xtype: 'textfield',
					name: 'nomCampa�a',
					id: 'txtNomCampa�a',
					fieldLabel: 'Nombre de Campa�a',		
					allowBlank: true,
					startDay: 0,
					maxLength: 35,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 400,
					msgTarget: 'side',						
					margins: '0 20 0 0'
				}
			]
		},
		{
					xtype: 'textarea',
					fieldLabel: 'Contenido',
					id: 'txtContenido',
					name: 'contenido',
					allowBlank: true,
					anchor: '90%',
					maxLength: 2000
		},
		{
			xtype: 'panel',
			layout:'column',
			items:[{
					xtype: 'container',
					id:	'panelIzq',
					labelWidth:150,
					columnWidth:.75,
					defaults: {	msgTarget: 'side',	anchor: '-40'	},
					layout: 'form',
					items: [
						{
							xtype: 		'fileuploadfield',
							id: 			'fufCargaImagen',
							name: 			'cargaImagen',
							emptyText: 	'Seleccione...',
							fieldLabel: 	"Carga Imagen", 
							buttonText: 	null,
							buttonCfg: {
								iconCls: 	'upload-icon'
							},
							anchor: 		'-20'
						}
					]
					},
					{
						xtype: 'container',
						id:	'panelDer',
						columnWidth:.25,
						labelWidth:1,
						layout: 'form',
						defaults: {	msgTarget: 'side',	anchor: '-100'	},
						items: [{
									xtype: 'button',
									text: 'Vista Previa',
									name: 'btnVistaPrevia',
									handler: function (boton, evento){
										var arch = Ext.getCmp('fufCargaImagen');
										if( Ext.isEmpty( arch.getValue() ) ){
											arch.markInvalid("Debe capturar la ruta de la imagen");
											return;
										}
										var myRegex = /^.+\.([Pp][Nn][Gg])$/;
										var nombreArchivo1 	= Ext.getCmp('fufCargaImagen').getValue();
										if( !myRegex.test(nombreArchivo1) ) {
											arch.markInvalid("El tipo de archivo de carga solo debe ser .PNG");
											return;
										}else{
												var imagen = Ext.getCmp('fufCargaImagen').getValue();
												var fpIn = fpInicial.getForm();	
												fpInicial.el.mask("Procesando", 'x-mask-loading');
												fpIn.submit({
													url: '15admMeninitraExt01.data.jsp?informacion=vistaPrevia',
													success:function(form, action, resp) {
														TransmiteVistaPrevia(null,  true,  action.response );
													},
													failure: function(form, action) {
														TransmiteVistaPrevia(null,  true, action.response );
													}
												});
										}
								}
						}]
					}
					]
		}
		
		
	];
	
	var elementosFormaModifica =  [
		{
			xtype: 'textfield',
			name: 'ic_mensajeM',
			id: 'txtIc_mensaje',
			allowBlank: true,
			startDay: 0,
			maxLength: 25,	
			width: 100,
			msgTarget: 'side',
			hidden:true,
			margins: '0 20 0 0'
		},
	 {
			xtype: 'compositefield',
			fieldLabel: 'T�tulo',
			id: 'txtTituloGMod',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'tituloM',
					id: 'txtTituloMod',
					fieldLabel: 'T�tulo',					
					allowBlank: true,
					startDay: 0,
					maxLength: 35,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 400,
					msgTarget: 'side',						
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Publicar ',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'displayfield',
					value: 'Desde',
					width: 30
				},
				{
					xtype: 'datefield',
					name: 'dfFechaModA',
					id: 'dfFechaModA',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'dfFechaModB',
					startDay: 0,
					width: 100,
					maxLength: 10,
					//msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 60
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 10
				},
				{
					xtype: 'displayfield',
					value: 'Hasta',
					width: 50
				},
				{
					xtype: 'datefield',
					name: 'dfFechaModB',
					id: 'dfFechaModB',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoInicioFecha: 'dfFechaModA',
					startDay: 1,
					width: 100,
					maxLength: 10,
					//msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 100
				}
			]
		},
	
		{
			xtype: 'textarea',
			fieldLabel: 'Contenido',
			id: 'txtContenidoM',
			name: 'contenidoM',
			maxLength: 2000,
			allowBlank: true,
			anchor: '90%'
		}
		
		
	];
//-----------------------------Fin elementosForma-------------------------------

//--------------------------------gridConsulta----------------------------------	

//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------Panel Consulta---------------------------------
	
	var fpInicial = new Ext.form.FormPanel({
		id: 'formaInicial',
		width: 800,
		title: '<center>Mensaje Inicial</center>',
		frame: true,		
		titleCollapse: false,
		collapsible: true,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 150,
		defaultType:	'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaInicial,
		monitorValid: true,
		buttons: [
			
			{
				text : 'Confirmar',
				id   : 'btnConfirmar',
				iconCls :'icoAceptar',
				formBind: true,	
				handler: function (boton, evento){
					var arch = Ext.getCmp('fufCargaImagen').getValue();
					
					var tipoAfiliado = Ext.getCmp('cmbTipoAfi').getValue();
					var tipoPub = Ext.getCmp('cmbTipoPub').getValue();
					
					var fechaIni = Ext.getCmp('dfFechaPubA');
					var fechaFin = Ext.getCmp('dfFechaPubB');
					
					var titulo = Ext.getCmp('txtTitulo');
					var contenidoIni = Ext.getCmp('txtContenido');
					var nomCamp= Ext.getCmp('txtNomCampa�a');
					var cEpo = 0, cIF = 0;
					if(tipoAfiliado=='E' || tipoAfiliado=='P' ){
						storeCatEpoBData.each(
							function(record){
								cEpo++;	
							}
						);	
					}
					if((tipoAfiliado=='I'||tipoAfiliado=='P')&&tipoPub!='C'){
						storeCatIFBData.each(
							function(record){
								cIF++;	
							}
						);	
						
					}
					if(cEpo==0&&cIF==0){
						Ext.Msg.show({
								//title: 'Crear Grupo',
								msg: 'Debe seleccionar por lo menos un afiliado',
								modal: true,
								icon: Ext.Msg.WARNING,
								buttons: Ext.Msg.OK,
								fn: function (){ }
								});
								return;
						
					}
					if(Ext.isEmpty(fechaIni.getValue())&&Ext.isEmpty(fechaFin.getValue())){
						Ext.Msg.show({
								//title: 'Crear Grupo',
								msg: 'Debe capturar la fecha de publicaci�n',
								modal: true,
								icon: Ext.Msg.WARNING,
								buttons: Ext.Msg.OK,
								fn: function (){ }
								});
								return;
					}
					if(!Ext.isEmpty(fechaIni.getValue()) || !Ext.isEmpty(fechaFin.getValue())){
						var dtFechaActual = Ext.util.Format.date( new Date());
						var dtFechaDInicio = Ext.util.Format.date( fechaIni.getValue());
						if(datecomp(dtFechaDInicio,dtFechaActual) ==2){
							fechaIni.markInvalid('La fecha de publicaci�n "desde" no debe ser menor a la fecha actual.');
							fechaIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaIni.getValue())){
							fechaIni.markInvalid('El valor de la fecha de publicaci�n inicial es requerido');
							fechaIni.focus();
							return;
						}
					if(Ext.isEmpty(fechaFin.getValue())){
						fechaFin.markInvalid('El valor de la fecha de publicaci�n final  es requerido');
						fechaFin.focus();
						return;
					}
					
					
				 }
					if(tipoPub!='C'){
						if(Ext.isEmpty(titulo.getValue())){
							Ext.Msg.show({
								//title: 'Crear Grupo',
								msg: 'Debe capturar el  t�tulo del mensaje',
								modal: true,
								icon: Ext.Msg.WARNING,
								buttons: Ext.Msg.OK,
								fn: function (){ }
								});
								return;
						}
						if(Ext.isEmpty(contenidoIni.getValue())){
							Ext.Msg.show({
								//title: 'Crear Grupo',
								msg: 'Debe capturar el contenido del mensaje',
								modal: true,
								icon: Ext.Msg.WARNING,
								buttons: Ext.Msg.OK,
								fn: function (){ }
								});
								return;
						}
						
					}else{
						if(Ext.isEmpty(nomCamp.getValue())){
							Ext.Msg.show({
								//title: 'Crear Grupo',
								msg: 'Debe capturar el nombre de la campa�a',
								modal: true,
								icon: Ext.Msg.WARNING,
								buttons: Ext.Msg.OK,
								fn: function (){ }
								});
							return;
						}
						var nombreArchivo 	= Ext.getCmp("fufCargaImagen");
						var myRegex = /^.+\.([Pp][Nn][Gg])$/;
						var nombreArchivo1 	= Ext.getCmp('fufCargaImagen').getValue();
						if( !Ext.isEmpty( nombreArchivo.getValue() ) ){
							if( !myRegex.test(nombreArchivo1) ) {
								nombreArchivo.markInvalid("El tipo de archivo de carga solo debe ser .PNG");
								return;
							}
						}
					}
					Ext.Msg.show({
						title: 'Confirmar',
						msg: '�Est� seguro de querer enviar su informaci�n?',
						buttons: Ext.Msg.OKCANCEL,
						fn: function peticionAjax(btn){
							if (btn == 'ok'){
								fpInicial.el.mask('Confirmando...', 'x-mask-loading');
								Ext.Ajax.request({
									url : '15admMeninitraExt01.data.jsp',
									params: Ext.apply(fpInicial.getForm().getValues(),{
										informacion:'Confirmacion',
										nombreArch:arch
									}),
									callback: TransmiteConfirmar
								});
							}   
						}
					});
				}
			},
			{
				text : 'Limpiar',
				id   : 'btnLimpiar',
				iconCls :'icoLimpiar',
				handler: function (boton, evento){
					window.location.href='15admMeninitraExt01.jsp';
				}
			},
			{
				text : 'Consultar',
				id   : 'btnConsultar',  
				iconCls :'icoBuscar',
				formBind: true,	
				handler: function (boton, evento){
					var fechaIni = Ext.getCmp('dfFechaPubA');
					var fechaFin = Ext.getCmp('dfFechaPubB');
					
					if(!Ext.isEmpty(fechaIni.getValue()) || !Ext.isEmpty(fechaFin.getValue())){
					
						if(Ext.isEmpty(fechaIni.getValue())){
							fechaIni.markInvalid('El valor de la fecha de publicaci�n inicial es requerido');
							fechaIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaFin.getValue())){
							fechaFin.markInvalid('El valor de la fecha de publicaci�n final  es requerido');
							fechaFin.focus();
							return;
						}
					
					
					}
					fpInicial.el.mask('Consultando...', 'x-mask-loading');
						ConsultaData.load({ params: Ext.apply(fpInicial.getForm().getValues(),{
							informacion: 'Consultar'
									})
						});
				}   
			}
		]
	});
	
	var fpModifica = new Ext.form.FormPanel({
		id: 'formaModifica',
		width: 800,
		title: '<center>Modificaci�n de Mensajes</center>',
		frame: true,		
		titleCollapse: true,
		collapsible: true,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 150,
		defaultType:	'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaModifica,
		monitorValid: true,
		buttons: [
			{
				text : 'Modificar',
				id   : 'btnModificar',
				iconCls :'icoModificar',
				formBind: true,
				handler: function (boton, evento){
					var fechaIniM = Ext.getCmp('dfFechaModA');
					var fechaFinM = Ext.getCmp('dfFechaModB');
					if(!Ext.isEmpty(fechaIniM.getValue()) || !Ext.isEmpty(fechaFinM.getValue())){
						if(Ext.isEmpty(fechaIniM.getValue())){
							fechaIniM.markInvalid('El valor de la fecha a publicar inicial es requerido');
							fechaIniM.focus();
							return;
						}
					if(Ext.isEmpty(fechaFinM.getValue())){
						fechaFinM.markInvalid('El valor de la fecha a publicar final  es requerido');
						fechaFinM.focus();
						return;
					}
				}
					fpModifica.el.mask("Procesando", 'x-mask-loading');
					Ext.Ajax.request({
						url: '15admMeninitraExt01.data.jsp',
						params: Ext.apply(fpModifica.getForm().getValues(),{
							informacion: 'modificaMensaje'
							
						}),
						callback: procesarSuccessFailureModificar
					});
				}
			},
			{
				text : 'Cancelar',
				id   : 'btnCancelar',
				iconCls :'icoCancelar',
				handler: function (boton, evento){
					Ext.getCmp('gridConsulta').show();
					fpModifica.hide();
				}
			}
		]
	});
	var panelVista = new Ext.Panel({
		id: 'panelVista',
		height: 'auto',
		hidden: false,
		align: 'center',
		autoScroll: true
	});

//----------------------------Contenedor Principal------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 		'auto',
		disabled: 	false,
		items: [
			NE.util.getEspaciador(20),	
			fpInicial,
			fpModifica,
			gridConsulta
		]
	});	
//-----------------------------Fin Contenedor Principal-------------------------
	
	pnl.el.mask('Cargando...');
	Ext.Ajax.request({
		url: '15admMeninitraExt01.data.jsp',
		params:Ext.apply(fpInicial.getForm().getValues(),{
			informacion: 'valoresIniciales'
		}),
		callback: procesaValoresIniciales
	});

});//-----------------------------------------------Fin Ext.onReady(function(){}