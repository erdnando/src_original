<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.servicios.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoIFEncuestas,
		com.netro.model.catalogos.CatalogoPYMEEncuestas,
		com.netro.model.catalogos.CatalogoEPOEncuestas,
		com.netro.model.catalogos.CatalogoEncuestas,
		com.netro.model.catalogos.CatalogoEncRespAfil,
		com.netro.model.catalogos.CatalogoEPO,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String tipoAfiliado = (request.getParameter("tipoAfiliado")!=null)?request.getParameter("tipoAfiliado"):"";
String infoRegresar	=	"";

Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB",Servicios.class);


if (informacion.equals("CatalogoTipoAfil")){
		//ElementoCatalogo
		JSONArray jsObjArray = new JSONArray();
		if("I".equals(tipoAfiliado)){
			List lstCatTipoAfil = new ArrayList();
			HashMap  hm = new HashMap();
			hm.put("clave", "B");
			hm.put("descripcion", "Intermediario Financiero Bancario");
			lstCatTipoAfil.add(hm);
			hm = new HashMap();
			hm.put("clave", "NB");
			hm.put("descripcion", "Intermediario Financiero No Bancario");
			lstCatTipoAfil.add(hm);
			hm = new HashMap();
			hm.put("clave", "FB");
			hm.put("descripcion", "Intermediario Financiero Bancario/No Bancario");
			lstCatTipoAfil.add(hm);
			jsObjArray = JSONArray.fromObject(lstCatTipoAfil);
			
			infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		
		}
		if("P".equals(tipoAfiliado)){
			List lstCatTipoAfil = new ArrayList();
			HashMap  hm = new HashMap();
			hm.put("clave", "P");
			hm.put("descripcion", "proveedores");
			lstCatTipoAfil.add(hm);
			hm = new HashMap();
			hm.put("clave", "D");
			hm.put("descripcion", "distribuidores");
			lstCatTipoAfil.add(hm);
			jsObjArray = JSONArray.fromObject(lstCatTipoAfil);
			
			infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		}
		if("E".equals(tipoAfiliado)){
			CatalogoSimple catalogo = new CatalogoSimple();
			catalogo.setCampoClave("ic_tipo_epo");
			catalogo.setCampoDescripcion("cg_descripcion");
			catalogo.setTabla("comcat_tipo_epo");
			catalogo.setOrden("ic_tipo_epo");
			infoRegresar = catalogo.getJSONElementos();
		}
		
		

} else if (informacion.equals("CatalogoCadenas")){
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setOrden("cg_razon_social");
		infoRegresar = catalogo.getJSONElementos();
		
} else if (informacion.equals("CatalogoAfiliados")){
		
	String catTipoAfil = (request.getParameter("catTipoAfil")!=null)?request.getParameter("catTipoAfil"):"";
	String cveEpo = (request.getParameter("cveEpo")!=null)?request.getParameter("cveEpo"):"";
	//String cveEncuesta = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	
	System.out.println("catTipoAfil===="+catTipoAfil);
	System.out.println("cveEpo===="+cveEpo);
	
	if("I".equals(tipoAfiliado)){
		
		CatalogoIFEncuestas catalogo = new CatalogoIFEncuestas();
		catalogo.setCampoClave("ic_if");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setTipoBanco(catTipoAfil);
		catalogo.setOrden("cg_razon_social");
		infoRegresar = catalogo.getJSONElementos();
	}
	if("P".equals(tipoAfiliado)){
		
		CatalogoPYMEEncuestas catalogo = new CatalogoPYMEEncuestas();
		catalogo.setCampoClave("ic_pyme");
		catalogo.setCampoDescripcion("cg_razon_social||' '|| t1.cg_apmat ||' '|| t1.cg_appat ||' '|| t1.cg_nombre");
		catalogo.setTipoCliente(catTipoAfil);
		catalogo.setCveEpo(cveEpo);
		//catalogo.setOrden("descripcion");
		infoRegresar = catalogo.getJSONElementos();
	}
	if("E".equals(tipoAfiliado)){
		CatalogoEPOEncuestas catalogo = new CatalogoEPOEncuestas();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setOrden("cg_razon_social");
		catalogo.setTipoEpo(catTipoAfil);
		infoRegresar = catalogo.getJSONElementos();
	}
			
} else if (informacion.equals("CatalogoTipoPregunta")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_tipo_respuesta");
		catalogo.setCampoDescripcion("cd_tipo_respuesta");
		catalogo.setTabla("comcat_tipo_respuesta");
		catalogo.setOrden("ic_tipo_respuesta");
		infoRegresar = catalogo.getJSONElementos();
		
} else if (informacion.equals("CatalogoEncuestas")){
		CatalogoEncuestas catalogo = new CatalogoEncuestas();
		catalogo.setCampoClave("ic_encuesta");
		catalogo.setCampoDescripcion("cg_titulo");
		catalogo.setOrden("ce.ic_encuesta");
		catalogo.setTipoEncuesta(tipoAfiliado);
		infoRegresar = catalogo.getJSONElementos();
		
		

} else if (informacion.equals("CatalogoEncRespAfil")){
		String cveEncuesta = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
		
		CatalogoEncRespAfil catalogo = new CatalogoEncRespAfil();
		if("I".equals(tipoAfiliado))
			catalogo.setCampoClave("ic_if");
		if("E".equals(tipoAfiliado))
			catalogo.setCampoClave("ic_epo");
		if("P".equals(tipoAfiliado))
			catalogo.setCampoClave("ic_pyme");
		catalogo.setCampoDescripcion("cg_razon_social");
		//catalogo.setOrden("ce.ic_encuesta");
		catalogo.setTipoEncuesta(tipoAfiliado);
		catalogo.setCveEncuesta(cveEncuesta);
		infoRegresar = catalogo.getJSONElementos();
		
} else if (informacion.equals("CatalogoPlantillas")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_plantilla");
		catalogo.setCampoDescripcion("cg_titulo");
		catalogo.setTabla("com_plantilla");
		//catalogo.setOrden("ic_tipo_respuesta");
		infoRegresar = catalogo.getJSONElementos();
		
		

} else if (informacion.equals("GuardarEncuesta")){
	HashMap dataAltaEnc = new HashMap();
	List lstPreguntas = new ArrayList();
	List lstRespuestas = new ArrayList();
	
	
	
	String numPregReal = (request.getParameter("numPregReal")!=null)?request.getParameter("numPregReal"):"";
	//Parametros de la Forma de Alta de Encuesta
	String fp_titulo = (request.getParameter("fp_titulo"+tipoAfiliado)!=null)?request.getParameter("fp_titulo"+tipoAfiliado):"";
	String fp_fec_desde = (request.getParameter("fp_fec_desde"+tipoAfiliado)!=null)?request.getParameter("fp_fec_desde"+tipoAfiliado):"";
	String fp_fec_hasta = (request.getParameter("fp_fec_hasta"+tipoAfiliado)!=null)?request.getParameter("fp_fec_hasta"+tipoAfiliado):"";
	String fp_cadena = (request.getParameter("fp_cadena"+tipoAfiliado)!=null)?request.getParameter("fp_cadena"+tipoAfiliado):"";
	String fp_tipoAfiliado = (request.getParameter("fp_tipoAfiliado"+tipoAfiliado)!=null)?request.getParameter("fp_tipoAfiliado"+tipoAfiliado):"";
	String fp_respObligada = (request.getParameter("fp_respObligada"+tipoAfiliado)!=null)?request.getParameter("fp_respObligada"+tipoAfiliado):"";
	String fp_diasResp = (request.getParameter("fp_diasResp"+tipoAfiliado)!=null)?request.getParameter("fp_diasResp"+tipoAfiliado):"0";
	String fp_instrucciones = (request.getParameter("fp_instrucciones"+tipoAfiliado)!=null)?request.getParameter("fp_instrucciones"+tipoAfiliado):"";
	String fp_numPreguntas = (request.getParameter("fp_numPreguntas"+tipoAfiliado)!=null)?request.getParameter("fp_numPreguntas"+tipoAfiliado):"0";
	String fp_selecAfiliados = (request.getParameter("fp_selecAfiliados"+tipoAfiliado)!=null)?request.getParameter("fp_selecAfiliados"+tipoAfiliado):"";
	
	
	dataAltaEnc.put("fp_titulo",fp_titulo);
	dataAltaEnc.put("fp_fec_desde",fp_fec_desde);
	dataAltaEnc.put("fp_fec_hasta",fp_fec_hasta);
	dataAltaEnc.put("fp_tipoAfiliado",fp_tipoAfiliado);
	dataAltaEnc.put("fp_cadena",fp_cadena);
	dataAltaEnc.put("fp_respObligada",fp_respObligada);
	dataAltaEnc.put("fp_diasResp",fp_diasResp);
	dataAltaEnc.put("fp_instrucciones",fp_instrucciones);
	dataAltaEnc.put("fp_numPreguntas",fp_numPreguntas);
	dataAltaEnc.put("fp_selecAfiliados",fp_selecAfiliados);
	
	/*
	System.out.println("fp_titulo====="+fp_titulo);
	System.out.println("fp_fec_desde====="+fp_fec_desde);
	System.out.println("fp_fec_hasta====="+fp_fec_hasta);
	System.out.println("fp_cadena====="+fp_cadena);
	System.out.println("fp_tipoAfiliado====="+fp_tipoAfiliado);
	System.out.println("fp_respObligada====="+fp_respObligada);
	System.out.println("fp_diasResp====="+fp_diasResp);
	System.out.println("fp_instrucciones====="+fp_instrucciones);
	System.out.println("fp_numPreguntas====="+fp_numPreguntas);
	System.out.println("fp_selecAfiliados====="+fp_selecAfiliados);
	*/
	
	
	//Parametros de la Forma Preguntas de Encuesta
	String fpPregunta = "";
	String fpTipoPreg = "";
	String fpTipoResp = "";
	String fpNumOpc = "";
	 
	if(Integer.parseInt(fp_numPreguntas)>0){
		HashMap dataPreguntas = null;
		int numPregunta = 0;
		int numRubro = 0;
		
		//SE OBTIENE LISTADO DE PREGUNTAS PARAMTERIZADAS
		for(int x=1; x<=Integer.parseInt(fp_numPreguntas); x++){
			
			fpPregunta = (request.getParameter("fpPregunta"+x+tipoAfiliado)!=null)?request.getParameter("fpPregunta"+x+tipoAfiliado):"";
			fpTipoPreg = (request.getParameter("fpTipoPreg"+x+tipoAfiliado)!=null)?request.getParameter("fpTipoPreg"+x+tipoAfiliado):"";
			fpTipoResp = (request.getParameter("fpTipoResp"+x+tipoAfiliado)!=null)?request.getParameter("fpTipoResp"+x+tipoAfiliado):"";
			fpNumOpc = (request.getParameter("fpNumOpc"+x+tipoAfiliado)!=null)?request.getParameter("fpNumOpc"+x+tipoAfiliado):"";
			String fpRubro[] = request.getParameterValues("fpRubro"+x+tipoAfiliado);
			
			if(fpRubro!=null){
				for(int r=0; r<fpRubro.length; r++){
					numRubro++;
					dataPreguntas = new HashMap();
					dataPreguntas.put("ORDENPREG",String.valueOf(++numPregunta));
					dataPreguntas.put("PREGUNTA",fpRubro[r]);
					dataPreguntas.put("TIPOPREG","N");
					dataPreguntas.put("TIPORESP","4");
					dataPreguntas.put("NUMOPC","0");
					dataPreguntas.put("NUMRUBRO",String.valueOf(numRubro));
					dataPreguntas.put("NUMPREG","0");
					
					lstPreguntas.add(dataPreguntas);
				}
			}
			
			dataPreguntas = new HashMap();
			dataPreguntas.put("ORDENPREG",String.valueOf(++numPregunta));
			dataPreguntas.put("PREGUNTA",fpPregunta);
			dataPreguntas.put("TIPOPREG",fpTipoPreg);
			dataPreguntas.put("TIPORESP",fpTipoResp);
			dataPreguntas.put("NUMOPC",fpNumOpc);
			dataPreguntas.put("NUMRUBRO",String.valueOf(numRubro));
			dataPreguntas.put("NUMPREG",String.valueOf(x));
			
			lstPreguntas.add(dataPreguntas);
			
		}
		
		
		HashMap dataRespuestas = new HashMap();
		//SE OBTIENE LISTADO DE RESPUESTAS PARAMTERIZADAS
		for(int x=1; x<=Integer.parseInt(fp_numPreguntas); x++){
			
			String fprNumPreg = (request.getParameter(x+"_numPreg_"+tipoAfiliado)!=null)?request.getParameter(x+"_numPreg_"+tipoAfiliado):"";
			String fprNumOpc = (request.getParameter(x+"_numOpc_"+tipoAfiliado)!=null)?request.getParameter(x+"_numOpc_"+tipoAfiliado):"0";
			String fprTipoResp = (request.getParameter(x+"_tipoResp_"+tipoAfiliado)!=null)?request.getParameter(x+"_tipoResp_"+tipoAfiliado):"";
			
			System.out.println("fprNumOpc============"+fprNumOpc);
			
			
			
			
			
			int numOpciones = Integer.parseInt(fprNumOpc);
			for(int y=0; y<numOpciones;y++){
				dataRespuestas = new HashMap();
				
				String fprResp = (request.getParameter(x+"_resp_"+y+tipoAfiliado)!=null)?request.getParameter(x+"_resp_"+y+tipoAfiliado):"";
				String fprPregExc = (request.getParameter(x+"_pregExclu_"+y+tipoAfiliado)!=null)?request.getParameter(x+"_pregExclu_"+y+tipoAfiliado):"";
				String fprPond = (request.getParameter(x+"_pond_0"+tipoAfiliado)!=null)?request.getParameter(x+"_pond_0"+tipoAfiliado):"0";
				
				System.out.println("fprResp============"+fprResp);
				
				dataRespuestas.put("NUMPREG", fprNumPreg);
				dataRespuestas.put("NUMOPC",fprNumOpc);
				dataRespuestas.put("TIPORESP",fprTipoResp);
				dataRespuestas.put("RESPUESTA",fprResp);
				dataRespuestas.put("PREGEXCLU",fprPregExc);
				dataRespuestas.put("PONDERACION",fprPond);
				
				lstRespuestas.add(dataRespuestas);
			}
			
			
			
			
		}
		
		
	}
	
	
	String cveEncuestas = servicios.guardaEncuestaGral(dataAltaEnc, lstPreguntas,lstRespuestas, tipoAfiliado);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();	
	
} else if (informacion.equals("ConsultaEncuestas")){
	JSONArray jsObjArray = new JSONArray();
	List lstEncuestas = new ArrayList();
	
	String fp_consTipoAfil = (request.getParameter("fp_consTipoAfil"+tipoAfiliado)!=null)?request.getParameter("fp_consTipoAfil"+tipoAfiliado):"";
	String fp_consCadena = (request.getParameter("fp_consCadena"+tipoAfiliado)!=null)?request.getParameter("fp_consCadena"+tipoAfiliado):"";
	String fp_consEncuesta = (request.getParameter("fp_consEncuesta"+tipoAfiliado)!=null)?request.getParameter("fp_consEncuesta"+tipoAfiliado):"";
	String fp_cons_fec_desde = (request.getParameter("fp_cons_fec_desde"+tipoAfiliado)!=null)?request.getParameter("fp_cons_fec_desde"+tipoAfiliado):"";
	String fp_cons_fec_hasta = (request.getParameter("fp_cons_fec_hasta"+tipoAfiliado)!=null)?request.getParameter("fp_cons_fec_hasta"+tipoAfiliado):"";
	//cveEncuesta, String cveEpo, String cveTipoAfil, String fechaIni,
	//String fechaFin, String tipoAfiliado
	
	lstEncuestas = servicios.consultaEncuestas(fp_consEncuesta,fp_consCadena,fp_consTipoAfil,fp_cons_fec_desde,fp_cons_fec_hasta,tipoAfiliado);
	
	jsObjArray = JSONArray.fromObject(lstEncuestas);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";


} else if (informacion.equals("obtenerInfoEncuesta")){

	HashMap hmInfoEnc = new HashMap();
	String cveEncuesta  = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	
	hmInfoEnc = servicios.consultaInfoEncuesta(cveEncuesta, tipoAfiliado);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("dataInfoEnc", hmInfoEnc);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();


} else if (informacion.equals("BorrarEncuesta")){
	
	String cveEncuesta = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	
	servicios.borraEncuesta(cveEncuesta, "");
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();	
	
} else if (informacion.equals("ModificarEncuesta")){
	HashMap dataAltaEnc = new HashMap();
	List lstPreguntas = new ArrayList();
	List lstRespuestas = new ArrayList();
	String tipoAfil = tipoAfiliado;
	
	
	String cveEncuesta = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	String numPregReal = (request.getParameter("numPregReal")!=null)?request.getParameter("numPregReal"):"";
	
	//Parametros de la Forma de Alta de Encuesta
	String fp_titulo = (request.getParameter("fp_titulo_m"+tipoAfiliado)!=null)?request.getParameter("fp_titulo_m"+tipoAfiliado):"";
	String fp_fec_desde = (request.getParameter("fp_fec_desde_m"+tipoAfiliado)!=null)?request.getParameter("fp_fec_desde_m"+tipoAfiliado):"";
	String fp_fec_hasta = (request.getParameter("fp_fec_hasta_m"+tipoAfiliado)!=null)?request.getParameter("fp_fec_hasta_m"+tipoAfiliado):"";
	String fp_cadena = (request.getParameter("fp_cadena_m"+tipoAfiliado)!=null)?request.getParameter("fp_cadena_m"+tipoAfiliado):"";
	String fp_tipoAfiliado = (request.getParameter("fp_tipoAfiliado_m"+tipoAfiliado)!=null)?request.getParameter("fp_tipoAfiliado_m"+tipoAfiliado):"";
	String fp_respObligada = (request.getParameter("fp_respObligada_m"+tipoAfiliado)!=null)?request.getParameter("fp_respObligada_m"+tipoAfiliado):"";
	String fp_diasResp = (request.getParameter("fp_diasResp_m"+tipoAfiliado)!=null)?request.getParameter("fp_diasResp_m"+tipoAfiliado):"0";
	String fp_instrucciones = (request.getParameter("fp_instrucciones_m"+tipoAfiliado)!=null)?request.getParameter("fp_instrucciones_m"+tipoAfiliado):"";
	String fp_numPreguntas = (request.getParameter("fp_numPreguntas_m"+tipoAfiliado)!=null)?request.getParameter("fp_numPreguntas_m"+tipoAfiliado):"0";
	String fp_selecAfiliados = (request.getParameter("fp_selecAfiliados_m"+tipoAfiliado)!=null)?request.getParameter("fp_selecAfiliados_m"+tipoAfiliado):"";
	
	dataAltaEnc.put("fp_titulo",fp_titulo);
	dataAltaEnc.put("fp_fec_desde",fp_fec_desde);
	dataAltaEnc.put("fp_fec_hasta",fp_fec_hasta);
	dataAltaEnc.put("fp_tipoAfiliado",fp_tipoAfiliado);
	dataAltaEnc.put("fp_respObligada",fp_respObligada);
	dataAltaEnc.put("fp_diasResp",fp_diasResp);
	dataAltaEnc.put("fp_instrucciones",fp_instrucciones);
	dataAltaEnc.put("fp_numPreguntas",fp_numPreguntas);
	dataAltaEnc.put("fp_selecAfiliados",fp_selecAfiliados);
	
	//Parametros de la Forma Preguntas de Encuesta
	String fpPregunta = "";
	String fpTipoPreg = "";
	String fpTipoResp = "";
	String fpNumOpc = "";
	 
	if(Integer.parseInt(numPregReal)>0){
		HashMap dataPreguntas = null;
		int numPregunta = 0;
		int numRubro = 0;
		tipoAfiliado = tipoAfiliado+"s" ;
		
		//SE OBTIENE LISTADO DE PREGUNTAS PARAMTERIZADAS
		for(int x=1; x<Integer.parseInt(numPregReal); x++){
			
			fpPregunta = (request.getParameter("fpPregunta"+x+tipoAfiliado)!=null)?request.getParameter("fpPregunta"+x+tipoAfiliado):"";
			fpTipoPreg = (request.getParameter("fpTipoPreg"+x+tipoAfiliado)!=null)?request.getParameter("fpTipoPreg"+x+tipoAfiliado):"";
			fpTipoResp = (request.getParameter("fpTipoResp"+x+tipoAfiliado)!=null)?request.getParameter("fpTipoResp"+x+tipoAfiliado):"";
			fpNumOpc = (request.getParameter("fpNumOpc"+x+tipoAfiliado)!=null)?request.getParameter("fpNumOpc"+x+tipoAfiliado):"";
			String fpRubro[] = request.getParameterValues("fpRubro"+x+tipoAfiliado);
			
			if(fpRubro!=null){
				for(int r=0; r<fpRubro.length; r++){
					numRubro++;
					dataPreguntas = new HashMap();
					dataPreguntas.put("ORDENPREG",String.valueOf(++numPregunta));
					dataPreguntas.put("PREGUNTA",fpRubro[r]);
					dataPreguntas.put("TIPOPREG","N");
					dataPreguntas.put("TIPORESP","4");
					dataPreguntas.put("NUMOPC","0");
					dataPreguntas.put("NUMRUBRO",String.valueOf(numRubro));
					dataPreguntas.put("NUMPREG","0");
					
					lstPreguntas.add(dataPreguntas);
				}
			}
			
			dataPreguntas = new HashMap();
			dataPreguntas.put("ORDENPREG",String.valueOf(++numPregunta));
			dataPreguntas.put("PREGUNTA",fpPregunta);
			dataPreguntas.put("TIPOPREG",fpTipoPreg);
			dataPreguntas.put("TIPORESP",fpTipoResp);
			dataPreguntas.put("NUMOPC",fpNumOpc);
			dataPreguntas.put("NUMRUBRO",String.valueOf(numRubro));
			dataPreguntas.put("NUMPREG",String.valueOf(x));
			
			lstPreguntas.add(dataPreguntas);
			
		}
		
		
		//SE OBTIENE LISTADO DE RESPUESTAS PARAMTERIZADAS
		HashMap dataRespuestas = new HashMap();
		for(int x=1; x<Integer.parseInt(numPregReal); x++){
			
			String fprNumPreg = (request.getParameter(x+"_numPreg_"+tipoAfil)!=null)?request.getParameter(x+"_numPreg_"+tipoAfil):"";
			String fprNumOpc = (request.getParameter(x+"_numOpc_"+tipoAfil)!=null)?request.getParameter(x+"_numOpc_"+tipoAfil):"0";
			String fprTipoResp = (request.getParameter(x+"_tipoResp_"+tipoAfil)!=null)?request.getParameter(x+"_tipoResp_"+tipoAfil):"";
			
			int numOpciones = Integer.parseInt(fprNumOpc);
			for(int y=0; y<numOpciones;y++){
				dataRespuestas = new HashMap();
				
				String fprResp = (request.getParameter(x+"_resp_"+y+tipoAfil)!=null)?request.getParameter(x+"_resp_"+y+tipoAfil):"";
				String fprPregExc = (request.getParameter(x+"_pregExclu_"+y+tipoAfil)!=null)?request.getParameter(x+"_pregExclu_"+y+tipoAfil):"";
				String fprPond = (request.getParameter(x+"_pond_0"+tipoAfil)!=null)?request.getParameter(x+"_pond_0"+tipoAfil):"0";
				
				dataRespuestas.put("NUMPREG", fprNumPreg);
				dataRespuestas.put("NUMOPC",fprNumOpc);
				dataRespuestas.put("TIPORESP",fprTipoResp);
				dataRespuestas.put("RESPUESTA",fprResp);
				dataRespuestas.put("PREGEXCLU",fprPregExc);
				dataRespuestas.put("PONDERACION",fprPond);
				
				lstRespuestas.add(dataRespuestas);
			}
		}
	}
	
	
	String cveEncuestas = servicios.modificacionEncuestaGral(dataAltaEnc, lstPreguntas,lstRespuestas, cveEncuesta);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();	
	
} else if (informacion.equals("ConsultaRespuestas")){
	JSONArray jsObjArray = new JSONArray();
	List lstRespuestas = new ArrayList();

	lstRespuestas = servicios.consultaRespuestas(tipoAfiliado);
	
	jsObjArray = JSONArray.fromObject(lstRespuestas);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

} else if (informacion.equals("ConsultaRespIndividual")){
	JSONArray jsObjArray = new JSONArray();
	List lstRespuestas = new ArrayList();
	String cveEncuesta = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	String cveAfiliado = (request.getParameter("claveAfiliado")!=null)?request.getParameter("claveAfiliado"):"";
	
	lstRespuestas = servicios.consultaRespIndividual(cveEncuesta, cveAfiliado, tipoAfiliado);
	
	jsObjArray = JSONArray.fromObject(lstRespuestas);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

} else if (informacion.equals("ConsultaRespConsolidada")){
	JSONArray jsObjArray = new JSONArray();
	List lstRespuestas = new ArrayList();
	String cveEncuesta = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	String cveAfiliado = (request.getParameter("claveAfiliado")!=null)?request.getParameter("claveAfiliado"):"";
	
	lstRespuestas = servicios.consultaRespConsolidada(cveEncuesta, cveAfiliado, tipoAfiliado);
	
	jsObjArray = JSONArray.fromObject(lstRespuestas);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

} else if (informacion.equals("ArchivoRespIndividual")){
	String cveEncuesta = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	String cveAfiliado = (request.getParameter("claveAfiliado")!=null)?request.getParameter("claveAfiliado"):"";
	String tipoArch = (request.getParameter("tipoArch")!=null)?request.getParameter("tipoArch"):"";
	HashMap hmSession = new HashMap();
	
	hmSession.put("strPais",(String)session.getAttribute("strPais"));
	hmSession.put("iNoNafinElectronico",session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico").toString());
	hmSession.put("sesExterno",(String)session.getAttribute("sesExterno"));
	hmSession.put("strNombre",(String) session.getAttribute("strNombre"));
	hmSession.put("strNombreUsuario",(String) session.getAttribute("strNombreUsuario"));
	hmSession.put("strLogo",(String)session.getAttribute("strLogo"));
	hmSession.put("strDirectorioPublicacion",(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	GeneraArchivoEncuestas generaArch = new GeneraArchivoEncuestas();
	String nombreArchivo = "";
	if("PDF".equals(tipoArch)){
		nombreArchivo = generaArch.generaRespIndPDF(hmSession,strDirectorioTemp, cveEncuesta, cveAfiliado, tipoAfiliado);
	}else{
		nombreArchivo = generaArch.generaRespIndCSV(hmSession,strDirectorioTemp, cveEncuesta, cveAfiliado, tipoAfiliado);
	}
	
			
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();	

} else if (informacion.equals("ArchivoRespConsolidada")){
	String cveEncuesta = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	String cveAfiliado = (request.getParameter("claveAfiliado")!=null)?request.getParameter("claveAfiliado"):"";
	String tipoArch = (request.getParameter("tipoArch")!=null)?request.getParameter("tipoArch"):"";
	HashMap hmSession = new HashMap();
	
	hmSession.put("strPais",(String)session.getAttribute("strPais"));
	hmSession.put("iNoNafinElectronico",session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico").toString());
	hmSession.put("sesExterno",(String)session.getAttribute("sesExterno"));
	hmSession.put("strNombre",(String) session.getAttribute("strNombre"));
	hmSession.put("strNombreUsuario",(String) session.getAttribute("strNombreUsuario"));
	hmSession.put("strLogo",(String)session.getAttribute("strLogo"));
	hmSession.put("strDirectorioPublicacion",(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	GeneraArchivoEncuestas generaArch = new GeneraArchivoEncuestas();
	String nombreArchivo = "";
	
	if("PDF".equals(tipoArch)){
		nombreArchivo = generaArch.generaRespConsPDF(hmSession,strDirectorioTemp, cveEncuesta, cveAfiliado, tipoAfiliado);
	}else{
		nombreArchivo = generaArch.generaRespConsCSV(hmSession,strDirectorioTemp, cveEncuesta, cveAfiliado, tipoAfiliado);
	}
			
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();	

} else if (informacion.equals("GuardarPlantilla")){
	HashMap dataAltaEnc = new HashMap();
	List lstPreguntas = new ArrayList();
	List lstRespuestas = new ArrayList();
	
	
	
	String numPregReal = (request.getParameter("numPregReal")!=null)?request.getParameter("numPregReal"):"";
	//Parametros de la Forma de Alta de Encuesta
	String fp_titulo = (request.getParameter("fp_titulo"+tipoAfiliado)!=null)?request.getParameter("fp_titulo"+tipoAfiliado):"";
	String fp_instrucciones = (request.getParameter("fp_instrucciones"+tipoAfiliado)!=null)?request.getParameter("fp_instrucciones"+tipoAfiliado):"";
	String fp_numPreguntas = (request.getParameter("fp_numPreguntas"+tipoAfiliado)!=null)?request.getParameter("fp_numPreguntas"+tipoAfiliado):"0";
	
	
	dataAltaEnc.put("fp_titulo",fp_titulo);
	dataAltaEnc.put("fp_instrucciones",fp_instrucciones);
	dataAltaEnc.put("fp_numPreguntas",fp_numPreguntas);
	dataAltaEnc.put("fp_usuario",strLogin);
	
	
	//Parametros de la Forma Preguntas de Encuesta
	String fpPregunta = "";
	String fpTipoPreg = "";
	String fpTipoResp = "";
	String fpNumOpc = "";
	 
	if(Integer.parseInt(fp_numPreguntas)>0){
		HashMap dataPreguntas = null;
		int numPregunta = 0;
		int numRubro = 0;
		
		//SE OBTIENE LISTADO DE PREGUNTAS PARAMTERIZADAS
		for(int x=1; x<=Integer.parseInt(fp_numPreguntas); x++){
			
			fpPregunta = (request.getParameter("fpPregunta"+x+tipoAfiliado)!=null)?request.getParameter("fpPregunta"+x+tipoAfiliado):"";
			fpTipoPreg = (request.getParameter("fpTipoPreg"+x+tipoAfiliado)!=null)?request.getParameter("fpTipoPreg"+x+tipoAfiliado):"";
			fpTipoResp = (request.getParameter("fpTipoResp"+x+tipoAfiliado)!=null)?request.getParameter("fpTipoResp"+x+tipoAfiliado):"";
			fpNumOpc = (request.getParameter("fpNumOpc"+x+tipoAfiliado)!=null)?request.getParameter("fpNumOpc"+x+tipoAfiliado):"";
			String fpRubro[] = request.getParameterValues("fpRubro"+x+tipoAfiliado);
			
			if(fpRubro!=null){
				for(int r=0; r<fpRubro.length; r++){
					numRubro++;
					dataPreguntas = new HashMap();
					dataPreguntas.put("ORDENPREG",String.valueOf(++numPregunta));
					dataPreguntas.put("PREGUNTA",fpRubro[r]);
					dataPreguntas.put("TIPOPREG","N");
					dataPreguntas.put("TIPORESP","4");
					dataPreguntas.put("NUMOPC","0");
					dataPreguntas.put("NUMRUBRO",String.valueOf(numRubro));
					dataPreguntas.put("NUMPREG","0");
					
					lstPreguntas.add(dataPreguntas);
				}
			}
			
			dataPreguntas = new HashMap();
			dataPreguntas.put("ORDENPREG",String.valueOf(++numPregunta));
			dataPreguntas.put("PREGUNTA",fpPregunta);
			dataPreguntas.put("TIPOPREG",fpTipoPreg);
			dataPreguntas.put("TIPORESP",fpTipoResp);
			dataPreguntas.put("NUMOPC",fpNumOpc);
			dataPreguntas.put("NUMRUBRO",String.valueOf(numRubro));
			dataPreguntas.put("NUMPREG",String.valueOf(x));
			
			lstPreguntas.add(dataPreguntas);
			
		}
		
		
		HashMap dataRespuestas = new HashMap();
		//SE OBTIENE LISTADO DE RESPUESTAS PARAMTERIZADAS
		for(int x=1; x<=Integer.parseInt(fp_numPreguntas); x++){
			
			String fprNumPreg = (request.getParameter(x+"_numPreg_"+tipoAfiliado)!=null)?request.getParameter(x+"_numPreg_"+tipoAfiliado):"";
			String fprNumOpc = (request.getParameter(x+"_numOpc_"+tipoAfiliado)!=null)?request.getParameter(x+"_numOpc_"+tipoAfiliado):"0";
			String fprTipoResp = (request.getParameter(x+"_tipoResp_"+tipoAfiliado)!=null)?request.getParameter(x+"_tipoResp_"+tipoAfiliado):"";
			
			System.out.println("fprNumOpc============"+fprNumOpc);

			int numOpciones = Integer.parseInt(fprNumOpc);
			for(int y=0; y<numOpciones;y++){
				dataRespuestas = new HashMap();
				
				String fprResp = (request.getParameter(x+"_resp_"+y+tipoAfiliado)!=null)?request.getParameter(x+"_resp_"+y+tipoAfiliado):"";
				String fprPregExc = (request.getParameter(x+"_pregExclu_"+y+tipoAfiliado)!=null)?request.getParameter(x+"_pregExclu_"+y+tipoAfiliado):"";
				String fprPond = (request.getParameter(x+"_pond_0"+tipoAfiliado)!=null)?request.getParameter(x+"_pond_0"+tipoAfiliado):"0";
				
				System.out.println("fprResp============"+fprResp);
				
				dataRespuestas.put("NUMPREG", fprNumPreg);
				dataRespuestas.put("NUMOPC",fprNumOpc);
				dataRespuestas.put("TIPORESP",fprTipoResp);
				dataRespuestas.put("RESPUESTA",fprResp);
				dataRespuestas.put("PREGEXCLU",fprPregExc);
				dataRespuestas.put("PONDERACION",fprPond);
				
				lstRespuestas.add(dataRespuestas);
			}
		}
	}
	
	
	String cveEncuestas = servicios.guardaPlantillaGral(dataAltaEnc, lstPreguntas,lstRespuestas, tipoAfiliado);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();	
	
} else if (informacion.equals("ConsultaPlantillas")){
	JSONArray jsObjArray = new JSONArray();
	List lstEncuestas = new ArrayList();
	
	String fp_consEncuesta = (request.getParameter("fp_consPlantilla"+tipoAfiliado)!=null)?request.getParameter("fp_consPlantilla"+tipoAfiliado):"";
	String fp_cons_fec_desde = (request.getParameter("fp_cons_fec_desde"+tipoAfiliado)!=null)?request.getParameter("fp_cons_fec_desde"+tipoAfiliado):"";
	String fp_cons_fec_hasta = (request.getParameter("fp_cons_fec_hasta"+tipoAfiliado)!=null)?request.getParameter("fp_cons_fec_hasta"+tipoAfiliado):"";
	
	
	lstEncuestas = servicios.consultaPlantillas(fp_consEncuesta,fp_cons_fec_desde,fp_cons_fec_hasta);
	
	jsObjArray = JSONArray.fromObject(lstEncuestas);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

} else if (informacion.equals("BorrarPlantilla")){
	
	String cveEncuesta = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	
	servicios.borraPlantilla(cveEncuesta);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();	
	
} else if (informacion.equals("obtenerInfoPlantilla")){

	HashMap hmInfoEnc = new HashMap();
	String cveEncuesta  = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	
	hmInfoEnc = servicios.consultaInfoPlantilla(cveEncuesta, tipoAfiliado);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("dataInfoEnc", hmInfoEnc);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();


} else if (informacion.equals("ModificarPlantilla")){
	HashMap dataAltaEnc = new HashMap();
	List lstPreguntas = new ArrayList();
	List lstRespuestas = new ArrayList();
	String tipoAfil = tipoAfiliado;
	
	String guardarComo = (request.getParameter("guardarComo")!=null)?request.getParameter("guardarComo"):"";
	String cveEncuesta = (request.getParameter("cveEncuesta")!=null)?request.getParameter("cveEncuesta"):"";
	String numPregReal = (request.getParameter("numPregReal")!=null)?request.getParameter("numPregReal"):"";
	
	System.out.println("numPregReal===="+numPregReal);
	
	//Parametros de la Forma de Alta de Encuesta
	String fp_titulo = (request.getParameter("fp_titulo_m"+tipoAfiliado)!=null)?request.getParameter("fp_titulo_m"+tipoAfiliado):"";
	//String fp_fec_desde = (request.getParameter("fp_fec_desde_m"+tipoAfiliado)!=null)?request.getParameter("fp_fec_desde_m"+tipoAfiliado):"";
	//String fp_fec_hasta = (request.getParameter("fp_fec_hasta_m"+tipoAfiliado)!=null)?request.getParameter("fp_fec_hasta_m"+tipoAfiliado):"";
	//String fp_cadena = (request.getParameter("fp_cadena_m"+tipoAfiliado)!=null)?request.getParameter("fp_cadena_m"+tipoAfiliado):"";
	//String fp_tipoAfiliado = (request.getParameter("fp_tipoAfiliado_m"+tipoAfiliado)!=null)?request.getParameter("fp_tipoAfiliado_m"+tipoAfiliado):"";
	//String fp_respObligada = (request.getParameter("fp_respObligada_m"+tipoAfiliado)!=null)?request.getParameter("fp_respObligada_m"+tipoAfiliado):"";
	//String fp_diasResp = (request.getParameter("fp_diasResp_m"+tipoAfiliado)!=null)?request.getParameter("fp_diasResp_m"+tipoAfiliado):"0";
	String fp_instrucciones = (request.getParameter("fp_instrucciones_m"+tipoAfiliado)!=null)?request.getParameter("fp_instrucciones_m"+tipoAfiliado):"";
	String fp_numPreguntas = (request.getParameter("fp_numPreguntas_m"+tipoAfiliado)!=null)?request.getParameter("fp_numPreguntas_m"+tipoAfiliado):"0";
	//String fp_selecAfiliados = (request.getParameter("fp_selecAfiliados_m"+tipoAfiliado)!=null)?request.getParameter("fp_selecAfiliados_m"+tipoAfiliado):"";
	String fpHideNumeros[] = request.getParameterValues("fpHideNumeros"+tipoAfiliado);
	
	
	dataAltaEnc.put("fp_titulo",fp_titulo);
	//dataAltaEnc.put("fp_fec_desde",fp_fec_desde);
	//dataAltaEnc.put("fp_fec_hasta",fp_fec_hasta);
	//dataAltaEnc.put("fp_tipoAfiliado",fp_tipoAfiliado);
	//dataAltaEnc.put("fp_respObligada",fp_respObligada);
	//dataAltaEnc.put("fp_diasResp",fp_diasResp);
	dataAltaEnc.put("fp_instrucciones",fp_instrucciones);
	dataAltaEnc.put("fp_numPreguntas",fp_numPreguntas);
	//dataAltaEnc.put("fp_selecAfiliados",fp_selecAfiliados);
	
	//Parametros de la Forma Preguntas de Encuesta
	String fpPregunta = "";
	String fpTipoPreg = "";
	String fpTipoResp = "";
	String fpNumOpc = "";
	String fpNumPreg = "";
	
	if(fpHideNumeros!=null){
		int numPregHide = 0;
		HashMap dataPreguntas = null;
		int numPregunta = 0;
		int numRubro = 0;
		tipoAfiliado = tipoAfiliado+"s" ;
		
		for(int z=0; z<fpHideNumeros.length; z++){
			numPregHide = Integer.parseInt(fpHideNumeros[z]);
			int x = numPregHide;
	//if(Integer.parseInt(numPregReal)>0){
		
		
		//SE OBTIENE LISTADO DE PREGUNTAS PARAMTERIZADAS
		//for(int x=1; x<Integer.parseInt(numPregReal); x++){
			
			fpPregunta = (request.getParameter("fpPregunta"+x+tipoAfiliado)!=null)?request.getParameter("fpPregunta"+x+tipoAfiliado):"";
			fpTipoPreg = (request.getParameter("fpTipoPreg"+x+tipoAfiliado)!=null)?request.getParameter("fpTipoPreg"+x+tipoAfiliado):"";
			fpTipoResp = (request.getParameter("fpTipoResp"+x+tipoAfiliado)!=null)?request.getParameter("fpTipoResp"+x+tipoAfiliado):"";
			fpNumOpc = (request.getParameter("fpNumOpc"+x+tipoAfiliado)!=null)?request.getParameter("fpNumOpc"+x+tipoAfiliado):"";
			String fpRubro[] = request.getParameterValues("fpRubro"+x+tipoAfiliado);
			
			fpNumPreg = (request.getParameter("fpNumPreg"+x+tipoAfiliado)!=null)?request.getParameter("fpNumPreg"+x+tipoAfiliado):"";
			System.out.println("fpNumPreg======================== "+fpNumPreg);
			
			if(fpRubro!=null){
				for(int r=0; r<fpRubro.length; r++){
					numRubro++;
					dataPreguntas = new HashMap();
					dataPreguntas.put("ORDENPREG",String.valueOf(++numPregunta));
					dataPreguntas.put("PREGUNTA",fpRubro[r]);
					dataPreguntas.put("TIPOPREG","N");
					dataPreguntas.put("TIPORESP","4");
					dataPreguntas.put("NUMOPC","0");
					dataPreguntas.put("NUMRUBRO",String.valueOf(numRubro));
					dataPreguntas.put("NUMPREG","0");
					
					lstPreguntas.add(dataPreguntas);
				}
			}
			
			dataPreguntas = new HashMap();
			dataPreguntas.put("ORDENPREG",String.valueOf(++numPregunta));
			dataPreguntas.put("PREGUNTA",fpPregunta);
			dataPreguntas.put("TIPOPREG",fpTipoPreg);
			dataPreguntas.put("TIPORESP",fpTipoResp);
			dataPreguntas.put("NUMOPC",fpNumOpc);
			dataPreguntas.put("NUMRUBRO",String.valueOf(numRubro));
			dataPreguntas.put("NUMPREG",String.valueOf(z+1));
			
			lstPreguntas.add(dataPreguntas);
			
		}
		
		
		//SE OBTIENE LISTADO DE RESPUESTAS PARAMTERIZADAS
		HashMap dataRespuestas = new HashMap();
		for(int x=1; x<Integer.parseInt(numPregReal); x++){
			
			String fprNumPreg = (request.getParameter(x+"_numPreg_"+tipoAfil)!=null)?request.getParameter(x+"_numPreg_"+tipoAfil):"";
			String fprNumOpc = (request.getParameter(x+"_numOpc_"+tipoAfil)!=null)?request.getParameter(x+"_numOpc_"+tipoAfil):"0";
			String fprTipoResp = (request.getParameter(x+"_tipoResp_"+tipoAfil)!=null)?request.getParameter(x+"_tipoResp_"+tipoAfil):"";
			
			int numOpciones = Integer.parseInt(fprNumOpc);
			for(int y=0; y<numOpciones;y++){
				dataRespuestas = new HashMap();
				
				String fprResp = (request.getParameter(x+"_resp_"+y+tipoAfil)!=null)?request.getParameter(x+"_resp_"+y+tipoAfil):"";
				String fprPregExc = (request.getParameter(x+"_pregExclu_"+y+tipoAfil)!=null)?request.getParameter(x+"_pregExclu_"+y+tipoAfil):"";
				String fprPond = (request.getParameter(x+"_pond_0"+tipoAfil)!=null)?request.getParameter(x+"_pond_0"+tipoAfil):"0";
				
				dataRespuestas.put("NUMPREG", fprNumPreg);
				dataRespuestas.put("NUMOPC",fprNumOpc);
				dataRespuestas.put("TIPORESP",fprTipoResp);
				dataRespuestas.put("RESPUESTA",fprResp);
				dataRespuestas.put("PREGEXCLU",fprPregExc);
				dataRespuestas.put("PONDERACION",fprPond);
				
				lstRespuestas.add(dataRespuestas);
			}
		}
	}
	
	String cveEncuestas = servicios.modificacionPlantillaGral(dataAltaEnc, lstPreguntas,lstRespuestas, cveEncuesta, strLogin, guardarComo);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();	
	
} else if (informacion.equals("GuardarEncuestaPlant")){
	HashMap dataAltaEnc = new HashMap();
	List lstPreguntas = new ArrayList();
	List lstRespuestas = new ArrayList();
	
	String cvePlantilla = (request.getParameter("cvePlantilla")!=null)?request.getParameter("cvePlantilla"):"";
	
	//Parametros de la Forma de Alta de Encuesta
	String fp_titulo = (request.getParameter("fp_titulo"+tipoAfiliado)!=null)?request.getParameter("fp_titulo"+tipoAfiliado):"";
	String fp_fec_desde = (request.getParameter("fp_fec_desde"+tipoAfiliado)!=null)?request.getParameter("fp_fec_desde"+tipoAfiliado):"";
	String fp_fec_hasta = (request.getParameter("fp_fec_hasta"+tipoAfiliado)!=null)?request.getParameter("fp_fec_hasta"+tipoAfiliado):"";
	String fp_cadena = (request.getParameter("fp_cadena"+tipoAfiliado)!=null)?request.getParameter("fp_cadena"+tipoAfiliado):"";
	String fp_tipoAfiliado = (request.getParameter("fp_tipoAfiliado"+tipoAfiliado)!=null)?request.getParameter("fp_tipoAfiliado"+tipoAfiliado):"";
	String fp_respObligada = (request.getParameter("fp_respObligada"+tipoAfiliado)!=null)?request.getParameter("fp_respObligada"+tipoAfiliado):"";
	String fp_diasResp = (request.getParameter("fp_diasResp"+tipoAfiliado)!=null)?request.getParameter("fp_diasResp"+tipoAfiliado):"0";
	String fp_instrucciones = (request.getParameter("fp_instrucciones"+tipoAfiliado)!=null)?request.getParameter("fp_instrucciones"+tipoAfiliado):"";
	String fp_numPreguntas = (request.getParameter("fp_numPreguntas"+tipoAfiliado)!=null)?request.getParameter("fp_numPreguntas"+tipoAfiliado):"0";
	String fp_selecAfiliados = (request.getParameter("fp_selecAfiliados"+tipoAfiliado)!=null)?request.getParameter("fp_selecAfiliados"+tipoAfiliado):"";
	
	
	dataAltaEnc.put("fp_titulo",fp_titulo);
	dataAltaEnc.put("fp_fec_desde",fp_fec_desde);
	dataAltaEnc.put("fp_fec_hasta",fp_fec_hasta);
	dataAltaEnc.put("fp_tipoAfiliado",fp_tipoAfiliado);
	dataAltaEnc.put("fp_cadena",fp_cadena);
	dataAltaEnc.put("fp_respObligada",fp_respObligada);
	dataAltaEnc.put("fp_diasResp",fp_diasResp);
	dataAltaEnc.put("fp_instrucciones",fp_instrucciones);
	dataAltaEnc.put("fp_numPreguntas",fp_numPreguntas);
	dataAltaEnc.put("fp_selecAfiliados",fp_selecAfiliados);
	
	
	String cveEncuestas = servicios.guardaEncuestaPlant(dataAltaEnc, cvePlantilla, tipoAfiliado);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();	

} else if (informacion.equals("GuardaFechaFinal")){

	String fecha_final = (request.getParameter("fecha_final") != null) ? request.getParameter("fecha_final") : "";
	String ic_encuesta = (request.getParameter("ic_encuesta") != null) ? request.getParameter("ic_encuesta") : "";
	
	JSONObject jsonObj = new JSONObject();
	
	try{

		servicios.guardaFechaFinalEncuesta( fecha_final, ic_encuesta);
		jsonObj.put("success", new Boolean(true));
	} catch(Exception ne) {
		ne.printStackTrace();	
		jsonObj.put("success", new Boolean(false));
	}
	
	infoRegresar = jsonObj.toString();	
}


%>

<%=infoRegresar%>

