Ext.onReady(function() {


	
	function procesarComfirmaActivaServicio(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
				
			if(resp.informacion=='Activar_Servicio') {
				Ext.MessageBox.alert("Mensaje",resp.mensajeEnviado);			
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('formaCombo').hide();
				
				Ext.getCmp('div1').hide();	
				Ext.getCmp('div2').hide();	
				Ext.getCmp('div3').hide();	
				Ext.getCmp('div4').hide();	
				Ext.getCmp('div5').hide();	
				Ext.getCmp('div6').hide();	
				
				Ext.getCmp('btnAceptar').hide();	
				Ext.getCmp('btnActivarSer').hide();	
											
				Ext.getCmp('div7').show();	
				Ext.getCmp('div7_1').show();	
				Ext.getCmp('div8').show();	
				Ext.getCmp('div9').show();	
				
				Ext.getCmp('btnConfirmar').show();	3
				Ext.getCmp('btnRegresar').show();	
				
				
				Ext.getCmp('id_layout_momentoEnvio').update(resp.diasPrevios+'  d�as h�biles previos a su vencimiento.');
				Ext.getCmp('claveConfirmacionSesion').setValue(resp.claveConfirmacionSesion);	
				
			}else 	if(resp.informacion=='Confirmar') {
						
				if(resp.codigo=='correcto') {
				
					Ext.MessageBox.alert('Mensaje',resp.mensajeEnviado,
						function(){ 					
							window.location ='15factorajemovilbuscapymeExt.jsp';
					});			
				}else  {
					Ext.MessageBox.alert('Mensaje',resp.mensajeEnviado);
				}			
			
			}else 	if(resp.informacion=='Aceptar') {
			
				Ext.MessageBox.alert('Mensaje',resp.mensajeEnviado,
					function(){ 					
						Ext.Ajax.request({
							url: '15factorajemovilbuscapyme.data.jsp',
							params: {
								informacion: "obtenerDatosPyme",
								icPyme:Ext.getCmp('icPyme1').getValue() 
							},
							callback: procesaDatosPyme
						});
				});
				
			}
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var datos1 = {
		xtype			: 'fieldset',
		id 			: 'datos1',
		border		: false,
		labelWidth	: 180,
		heigth:	500,
		width	: 500,
		items			: 
		[	
			{
				xtype: 'radio',
				name: 'recibeFactoraje',
				id: 'recibeFactoraje',
				fieldLabel: 'Si desea recibir el servicio',
				allowBlank: true,
				startDay: 0,
				width: 100,
				height:20,
				msgTarget: 'side',
				vtype: 'recibeFactoraje', 
				checked: false,
				campoFinFecha: 'recibeFactoraje',
				margins: '0 20 0 0',
				listeners:{
					check:	function(radio){
						if(radio.checked){
							Ext.getCmp('email_1').setValue('');
							Ext.getCmp('confirmacionCorreo1').setValue('');
							Ext.getCmp('nuevoCelular1').setValue('');
							Ext.getCmp('confirmacionCelular1').setValue('');
							Ext.getCmp('recibeFactoraje2').setValue(false);
							
							Ext.getCmp('email_1').enable();
							Ext.getCmp('confirmacionCorreo1').enable();
							Ext.getCmp('nuevoCelular1').enable();
							Ext.getCmp('confirmacionCelular1').enable();
							
						}
					}
				}
			}			
		]
	};
	
	
	var datos2 = {
		xtype			: 'fieldset',
		id 			: 'datos2',
		border		: false,
		labelWidth	: 400,
		heigth:	500,
		width	: 500,
		items			: 
		[	
			{
				xtype: 'displayfield',
				style: 'text-align:left;',
				fieldLabel: 'Ingresa los siguentes datos',
				text: '-'
			},
			{
				xtype: 'label',
				id: 'correoRegistrado',
				style: 'text-align:left;',
				fieldLabel: '',
				text: ''
			},
			{
				xtype: 'displayfield',
				style: 'text-align:left;',
				fieldLabel: 'Si el correo no es correcto favor de actualizarlo',
				text: '-'
			}
		]
	};
	
	var datos3 = {
		xtype			: 'fieldset',
		id 			: 'datos3',
		border		: false,
		labelWidth	: 140,
		width	: 500,
		heigth:	500,
		items			: 
		[	
			{
				xtype			: 'textfield',
				id				: 'email_1',
				name			: 'email',
				fieldLabel: 'Correo Nuevo',
				allowBlank	: true,
				maxLength	: 100,
				width			: 200,						
				msgTarget: 'side',
				margins: '0 20 0 0',
				vtype: 'email'
			},					
			{
				xtype			: 'textfield',
				id				: 'confirmacionCorreo1',
				name			: 'confirmacionCorreo',
				fieldLabel: 'Confirmaci�n de Correo',
				allowBlank	: true,
				maxLength	: 100,
				width			: 200,						
				msgTarget: 'side',
				margins: '0 20 0 0',
				vtype: 'email'
			}
		]
	};
	
	var datos4 = {
		xtype			: 'fieldset',
		id 			: 'datos4',
		border		: false,
		labelWidth	: 400,
		heigth:	500,
		width	: 500,
		items			: 
		[	
			{
				xtype: 'label',				
				id: 'CelularRegistrado',
				fieldLabel: '',
				text: ''
			},
			{
				xtype: 'displayfield',
				style: 'text-align:left;',
				fieldLabel: 'Si el n�mero celular no es correcto favor de actualizarlo',
				text: '-'
			}
		]
	};
	
	
	var datos5 = {
		xtype			: 'fieldset',
		id 			: 'datos5',
		border		: false,
		labelWidth	: 200,
		heigth:	500,
		width	: 500,
		items			: 
		[	
			{
				xtype			: 'numberfield',
				id          : 'nuevoCelular1',
				name			: 'nuevoCelular',
				fieldLabel  : 'Tel�fono Celular (10 d�gitos): 044  ',
				allowBlank	: true,
				width			: 200,
				msgTarget: 'side',
				margins: '0 20 0 0',
				maxLength	: 10,
				minLength 	: 10
			},
			{
				xtype			: 'numberfield',
				id          : 'confirmacionCelular1',
				name			: 'confirmacionCelular',
				fieldLabel  : 'Confirmaci�n (10 d�gitos): 044',
				allowBlank	: true,
				width			: 200,
				msgTarget: 'side',
				margins: '0 20 0 0',
				maxLength	: 10,
				minLength 	: 10
			}
		]
	};
	
	var datos6 = {
		xtype			: 'fieldset',
		id 			: 'datos6',
		border		: false,
		labelWidth	: 180,
		heigth:	500,
		width	: 500,
		items			: 
		[	 
			{
				xtype: 'radio',
				name: 'recibeFactoraje2',
				id: 'recibeFactoraje2',
				fieldLabel: 'No le interesa el servicio',
				allowBlank: true,
				startDay: 0,
				width: 100,
				height:20,
				msgTarget: 'side',
				vtype: 'recibeFactoraje2', 
				checked: false,
				campoFinFecha: 'recibeFactoraje2',
				margins: '0 20 0 0',
				listeners:{
					check:	function(radio){
						if(radio.checked){
							Ext.getCmp('email_1').setValue('');
							Ext.getCmp('confirmacionCorreo1').setValue('');
							Ext.getCmp('nuevoCelular1').setValue('');
							Ext.getCmp('confirmacionCelular1').setValue('');
							Ext.getCmp('recibeFactoraje').setValue(false);
							
							Ext.getCmp('email_1').disable();
							Ext.getCmp('confirmacionCorreo1').disable();
							Ext.getCmp('nuevoCelular1').disable();
							Ext.getCmp('confirmacionCelular1').disable();
							
						}
					}
				}
			}
		]
	};

	var datos7_izq = {
		xtype		: 'fieldset',
		id 		: 'datos7_izq',
		border	: false,
		labelWidth	: 1,
		width: 500,
		items		: 
		[		
			{
				xtype: 'displayfield',
				value: 'Indique el momento en el que desea recibir los mensajes:',
				width: 400
			},	
			{
				xtype: 'compositefield',
				fieldLabel: '',
				combineErrors: false,
				msgTarget: 'side',				
				width: 300,
				items: [					
					{
						xtype: 'checkbox',
						boxLabel:'Cuando me hayan publicado documentos.',
						name:'momentoEnvio_0',
						id:'id_momentoEnvio_0',
						inputValue:'S',
						width: 350,
						height:20,
						msgTarget: 'side',
						anchor : '-25',
						colspan:1
					}
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: '',
				combineErrors: false,
				msgTarget: 'side',				
				width: 300,
				items: [	
					{
						xtype: 'checkbox',
						boxLabel:'',
						name:'momentoEnvio_1',
						id:'id_momentoEnvio_1',
						inputValue:'S',
						width: 30,
						height:20,
						msgTarget: 'side',
						anchor : '-25',
						colspan:1
					},
					{
						xtype: 'displayfield',
						id: 'id_layout_momentoEnvio',
						style: 'text-align:left;',
						fieldLabel: '',
						text: ''						
					}					
				]
			}		
		]
	};
	
	
	var datos7_der = {
		xtype		: 'fieldset',
		id 		: 'datos7_der',
		border	: false,
		labelWidth	: 1,
		width: 500,
		items		: 
		[		
			{
				xtype: 'displayfield',
				value: 'Indique el tipo de informaci�n que desea recibir:',
				width: 400
			},	
			{
				xtype: 'compositefield',
				fieldLabel: '',
				combineErrors: false,
				msgTarget: 'side',
				id: 'camp3',   
				width: 200,
				items: [					
					{
						xtype: 'checkbox',
						boxLabel:'Monto',
						name:'tipoInformacion_0',
						id:'id_tipoInformacion_0',
						inputValue:'S',
						width: 350,
						height:20,
						msgTarget: 'side',
						anchor : '-25',
						colspan:1
					}
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: '',
				combineErrors: false,
				msgTarget: 'side',
				id: 'camp4',   
				width: 250,
				items: [	
					{
						xtype: 'checkbox',
						boxLabel:'N�mero de documentos',
						name:'tipoInformacion_1',
						id:'id_tipoInformacion_1',
						inputValue:'S',
						width: 350,
						height:20,
						msgTarget: 'side',
						anchor : '-25',
						colspan:1
					}					
				]
			}		
		]
	};
	


	var dataCelular = new Ext.data.SimpleStore({
		fields: ['clave','descripcion'  ],	 
		data : [ 
			['Iusacell','Iusacell'],
			['Movistar','Movistar'],
			['Nextel','Nextel'],
			['Telcel','Telcel'],
			['Unefon','Unefon']
		]
	});
	
	var datos8 =  {
		xtype		: 'fieldset',
		id 		: 'datos8',
		border	: false,
		labelWidth	: 200,
		items		: [
			{
				xtype				: 'combo',
				id					: 'compCelular1',
				name				: 'compCelular',
				hiddenName 		:'compCelular', 
				fieldLabel		: 'Tipo de Afiliado',
				width				: 150,
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	:'descripcion',	
				emptyText		: 'Seleccionar  ',
				store				: dataCelular								
			},				
			{
				xtype: 'textfield',
				fieldLabel: 'Ingresar C�digo de Confirmaci�n',		
				name: 'claveConfirmacion',
				id: 'claveConfirmacion1',
				allowBlank: true,
				maxLength: 6,
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0'
			}			
		]
	};
	
	var datos9 =  {
		xtype		: 'fieldset',
		id 		: 'datos9',
		border	: false,
		labelWidth	: 1,
		items		: [
			{
				xtype: 'displayfield',
				value: 'Si no ha recibido su c�digo de verificaci�n:',
				width: 350
			},
			{
				xtype: 'displayfield',
				value: '1. Regrese y valide que su n�mero de celular es correcto.',
				width: 350
			},
			{
				xtype: 'displayfield',
				value: '2. Dir�jase a la pantalla Administraci�n - Servicios - Env�o de promociones <br> para completar su registro.',
				width: 500
			},
			{
				xtype: 'displayfield',
				value: '3. Llame al 01 800 6234672 para mayor referencia.',
				width: 350
			}
		]
	};
	
 
	function procesaDatosPyme(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){				
								
				if (jsonData.mensaje ==''){
					Ext.getCmp('correoRegistrado').update('<b>Correo Registrado: '+jsonData.email+'</b>');
					Ext.getCmp('CelularRegistrado').update('<b>Celular Registrado: '+jsonData.numeroCelular+'</b>');
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('formaCombo').hide();
					//Ext.getCmp('forma').hide();										
					Ext.getCmp('div0').hide();	
					Ext.getCmp('div1').show();	
					Ext.getCmp('div2').show();	
					Ext.getCmp('div3').show();	
					Ext.getCmp('div4').show();	
					Ext.getCmp('div5').show();	
					Ext.getCmp('div6').show();	
					
					Ext.getCmp('div7').hide();	
					Ext.getCmp('div7_1').hide();	
					Ext.getCmp('div8').hide();	
					Ext.getCmp('div9').hide();	
					 
					Ext.getCmp('btnRegresar').hide();	
					Ext.getCmp('btnConfirmar').hide();	
					
					Ext.getCmp('btnAceptar').show();	
					Ext.getCmp('btnActivarSer').show();	
					
					Ext.getCmp('btnCancelar').hide();	
					Ext.getCmp('btnConsultar').hide();	
					
				}else  {
					Ext.MessageBox.alert("Mensaje",jsonData.mensaje);				
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	// ------------------- Consulta  -------------------

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
					
			if(store.getTotalCount() > 0) {
				el.unmask();					
			} else {					
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15factorajemovilbuscapyme.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'RFC'},
			{	name: 'CALLE'},	
			{	name: 'COLONIA'},	
			{	name: 'CODIGO_POSTAL'},	
			{	name: 'TELEFONO'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});

	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Calle y N�mero',
				tooltip: 'Calle y N�mero',
				dataIndex: 'CALLE',
				sortable: true,
				width: 300,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Colonia',
				tooltip: 'Colonia',
				dataIndex: 'COLONIA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'C.P.',
				tooltip: 'C.P.',
				dataIndex: 'CODIGO_POSTAL',
				sortable: true,
				width: 120,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tel�fono',
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO',
				sortable: true,
				width: 120,			
				resizable: true,				
				align: 'center'	
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 170,
		width: 850,
		align: 'center',
		frame: false,
		bbar: {					
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Siguiente',					
					tooltip:	'Siguiente',					
					id: 'btnSiguiente',
					handler: function(boton, evento) {					
					
						Ext.Ajax.request({
							url: '15factorajemovilbuscapyme.data.jsp',
							params: {
								informacion: "obtenerDatosPyme",
								icPyme:Ext.getCmp('icPyme1').getValue()
							},
							callback: procesaDatosPyme
						});
						
					}
				}
			]
		}
	});	
	
	
	//**********************criterios de busqueda******************+
	var procesarcatalogoPymeData= function(store, records, oprion){
	
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){			
			Ext.getCmp('formaCombo').show();
		}
		
  }
	
	var catalogoPYME = new Ext.data.JsonStore ({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15factorajemovilbuscapyme.data.jsp',
		baseParams: {	
			informacion: 'catalogoPYME'	
		}, 
		totalProperty : 'total',
		totalProperty : 'total',
		autoLoad: false,
		listeners: 	{ 	
			load: procesarcatalogoPymeData,
			exception: NE.util.mostrarDataProxyError,	
			beforeload: NE.util.initMensajeCargaCombo	
		}
	});
	
	var fpCombo = new Ext.form.FormPanel({
		id					: 'formaCombo',
		layout			: 'form',
		hidden			: true,
		width				: 500,
		style				: ' margin:0 auto;',
		frame				: false,		
		collapsible		: false,
		titleCollapse	: false	,		
		bodyStyle		: 'padding: 8px',
		labelWidth			: 70,
		monitorValid: true,
		defaults: { 	msgTarget: 'side', 	anchor: '-20'
		},		
		items: [  
			{
				xtype: 'combo',
				name: 'icPyme',
				id: 'icPyme1',
				fieldLabel: 'Proveedor',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'icPyme',
				emptyText: 'Seleccione...',					
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : catalogoPYME,
				tpl : NE.util.templateMensajeCargaCombo,
				listeners: {
					select: {
						fn: function(combo) {
							var icPyme = combo.getValue();
							consultaData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									icPyme:icPyme								
								})
							});								
						}
					}
				}				
			}  
		],
		monitorValid: true	
	});
	
	var  elementosForma =  [
	
	];
	
	var datos0 = {
		xtype			: 'fieldset',
		id 			: 'datos0',
		border		: false,
		labelWidth	: 180,
		heigth:	500,
		width	: 500,
		items			: 
		[	
				{
			xtype: 'numberfield',
			fieldLabel: 'Clave de Usuario',		
			name: 'claveUsuario',
			id: 'claveUsuario1',
			allowBlank: true,
			maxLength: 10,
			width: 220,
			msgTarget: 'side',
			margins: '0 20 0 0'
		}, 		
		{
			xtype: 'numberfield',
			fieldLabel: 'N@E Pyme',		
			name: 'nafinElectronico',
			id: 'nafinElectronico1',
			allowBlank: true,
			maxLength: 38,
			width: 220,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Nombre',		
			name: 'razonSocial',
			id: 'razonSocial1',
			allowBlank: true,
			maxLength: 100,
			width: 220,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'RFC',		
			name: 'rfc',
			id: 'rfc1',
			allowBlank: true,
			maxLength: 20,
			width: 220,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'N�mero SIRAC',		
			name: 'numeroSirac',
			id: 'numeroSirac1',
			allowBlank: true,
			maxLength: 12,
			width: 220,
			msgTarget: 'side',
			margins: '0 20 0 0'
		}
		]
		
		}
	
	var fp = new Ext.form.FormPanel({
		id					: 'forma',
		layout			: 'form',
		width				: 500,
		style				: ' margin:0 auto;',
		frame				: true,		
		collapsible		: false,
		titleCollapse	: false	,		
		bodyStyle		: 'padding: 8px',
		//labelWidth			: 150,
		monitorValid: true,
		defaults: { 	msgTarget: 'side', 	anchor: '-20'
		},
		title: 'Utilice el * para b�squeda gen�rica. ',
		items: [  
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div0',
				align    : 'center',
				width		: 500,
				labelWidth	: 150,
				items		: [datos0 ]
			},	
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div1',
				align    : 'center',
				width		: 500,
				labelWidth	: 150,
				hidden:true,
				items		: [datos1 ]
			},		
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div2',
				align    : 'center',
				hidden:true,
				width		: 500,
				items		: [datos2 ]
			},
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div3',
				align    : 'center',
				width		: 500,
				hidden:true,
				items		: [datos3 ]
			},
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div4',
				align    : 'center',
				hidden:true,
				width		: 500,
				items		: [datos4 ]
			},
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div5',
				align    : 'center',
				hidden:true,
				width		: 500,
				items		: [datos5 ]
			},
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div6',
				align    : 'center',
				hidden:true,
				width		: 500,
				items		: [datos6 ]
			},			
			//*****+++++++++
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div7',
				align    : 'center',
				hidden:true,
				width		: 500,
				items		: [datos7_izq ]
			},
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div7_1',
				align    : 'center',
				hidden:true,
				width		: 500,
				items		: [datos7_der ]
			},
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div8',
				align    : 'center',
				width		: 500,
				hidden:true,
				items		: [datos8]
			},
			{
				layout	: 'hbox',
				title		: '',
				id			: 'div9',
				align    : 'center',
				hidden:true,
				width		: 500,
				items		: [datos9]
			},
			{ 	xtype: 'textfield',  hidden: true, id: 'claveConfirmacionSesion', 	value: '' }		
		],
		monitorValid: true,
		buttons: [		
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,	
				handler: function(boton, evento) {	
				
					var icPyme= Ext.getCmp('icPyme1');
					icPyme.setValue('');
						
					var claveUsuario= Ext.getCmp('claveUsuario1');
					var nafinElectronico= Ext.getCmp('nafinElectronico1');
					var razonSocial= Ext.getCmp('razonSocial1');
					var rfc= Ext.getCmp('rfc1');
					var numeroSirac= Ext.getCmp('numeroSirac1');
					
					Ext.getCmp('gridConsulta').hide();
					 
					if( Ext.isEmpty(claveUsuario.getValue()) && Ext.isEmpty(nafinElectronico.getValue())   
						 && Ext.isEmpty(razonSocial.getValue()) &&  Ext.isEmpty(rfc.getValue())  
						 &&  Ext.isEmpty(numeroSirac.getValue())  ){						
							Ext.MessageBox.alert("Mensaje","por favor sea m�s espec�fico en la b�squeda");
							return;
					}
					
					catalogoPYME.load({ 					
						params: Ext.apply(Ext.getCmp('forma').getForm().getValues())																														
					});							
					
					
					
				}
			},
			{
				text: 'Cancelar',
				id: 'btnCancelar',
				iconCls: 'icoLimpiar',	
				hidden	  : false,
				handler: function(boton, evento) {	
					window.location ='15factorajemovilbuscapymeExt.jsp';	
				}
			},
			
			//pantalla 2
			{
				text: 'Activar Servicio',
				id: 'btnActivarSer',
				iconCls: 'icoAceptar',
				hidden:true,
				handler: function(boton, evento) {	
				
					var recibeFactoraje= Ext.getCmp('recibeFactoraje');
					var recibeFactoraje2= Ext.getCmp('recibeFactoraje2');
				
					if(recibeFactoraje.getValue()==false && recibeFactoraje2.getValue()==false  ){	 					
						Ext.MessageBox.alert("Mensaje","Debe de seleccionar una opci�n del servicio de factoraje movil ");
						return;
					}
					
					if(recibeFactoraje.getValue()==true){
					
						var email= Ext.getCmp('email_1');
						var confirmacionCorreo= Ext.getCmp('confirmacionCorreo1');
						var nuevoCelular= Ext.getCmp('nuevoCelular1');
						var confirmacionCelular= Ext.getCmp('confirmacionCelular1');					
					
						if( Ext.isEmpty(email.getValue())   ){						
							email.markInvalid('Este campo es obigatorio');
							return;
						}
						if( Ext.isEmpty(confirmacionCorreo.getValue() )  ){						
							confirmacionCorreo.markInvalid('Este campo es obigatorio');
							return;
						}
						
						if ( !Ext.isEmpty(email.getValue()) ||  !Ext.isEmpty(confirmacionCorreo.getValue())  ){
							if ( email.getValue() !=confirmacionCorreo.getValue()   ){
								confirmacionCorreo.markInvalid('El correo de confirmaci�n no es correcto');					
								return;
							}
						}
									
						if( Ext.isEmpty(nuevoCelular.getValue())   ){						
							nuevoCelular.markInvalid('Este campo es obigatorio');
							return;
						}
						if( Ext.isEmpty(confirmacionCelular.getValue())   ){						
							confirmacionCelular.markInvalid('Este campo es obigatorio');
							return;
						}	
						
						if ( !Ext.isEmpty(nuevoCelular.getValue()) ||  !Ext.isEmpty(confirmacionCelular.getValue())  ){
							if ( nuevoCelular.getValue() !=confirmacionCelular.getValue()   ){
								confirmacionCelular.markInvalid('El tel�no Celular  y confirmaci�n debe ser iguales');					
								return;
							}
						}
										 
					
						pnl.el.mask('Procesando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '15factorajemovilbuscapyme.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Activar_Servicio',
								icPyme:Ext.getCmp('icPyme1').getValue()
							}),					
							callback: procesarComfirmaActivaServicio
						});	
					
					}else  {
						Ext.MessageBox.alert("Mensaje","Debe aceptar el servicio para poder activarlo ");
						return;						
					}
				}
			},
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoAceptar',	
				hidden	  : true,
				
				handler: function(boton, evento) {	
								
					var recibeFactoraje= Ext.getCmp('recibeFactoraje');
					var recibeFactoraje2= Ext.getCmp('recibeFactoraje2');
				
					if(recibeFactoraje.getValue()==false && recibeFactoraje2.getValue()==false  ){	 					
						Ext.MessageBox.alert("Mensaje","Debe de seleccionar una opci�n del servicio de factoraje movil ");
						return;
					}
					
					var email= Ext.getCmp('email_1');
					var confirmacionCorreo= Ext.getCmp('confirmacionCorreo1');
					var nuevoCelular= Ext.getCmp('nuevoCelular1');
					var confirmacionCelular= Ext.getCmp('confirmacionCelular1');	
					
					if(recibeFactoraje.getValue()==true){
					
						if( Ext.isEmpty(email.getValue())   ){						
							email.markInvalid('Este campo es obigatorio');
							return;
						}
						if( Ext.isEmpty(confirmacionCorreo.getValue() )  ){						
							confirmacionCorreo.markInvalid('Este campo es obigatorio');
							return;
						}
						
						if ( !Ext.isEmpty(email.getValue()) ||  !Ext.isEmpty(confirmacionCorreo.getValue())  ){
							if ( email.getValue() !=confirmacionCorreo.getValue()   ){
								confirmacionCorreo.markInvalid('El correo de confirmaci�n no es correcto');					
								return;
							}
						}
									
						if( Ext.isEmpty(nuevoCelular.getValue())   ){						
							nuevoCelular.markInvalid('Este campo es obigatorio');
							return;
						}
						if( Ext.isEmpty(confirmacionCelular.getValue())   ){						
							confirmacionCelular.markInvalid('Este campo es obigatorio');
							return;
						}	
						
						if ( !Ext.isEmpty(nuevoCelular.getValue()) ||  !Ext.isEmpty(confirmacionCelular.getValue())  ){
							if ( nuevoCelular.getValue() !=confirmacionCelular.getValue()   ){
								confirmacionCelular.markInvalid('El tel�no Celular  y confirmaci�n debe ser iguales');					
								return;
							}
						}
										 
					
						pnl.el.mask('Procesando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '15factorajemovilbuscapyme.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Activar_Servicio',
								icPyme:Ext.getCmp('icPyme1').getValue()
							}),					
							callback: procesarComfirmaActivaServicio
						});	
					
					}else {										
					
						pnl.el.mask('Procesando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '15factorajemovilbuscapyme.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Aceptar',
								icPyme:Ext.getCmp('icPyme1').getValue()
							}),					
							callback: procesarComfirmaActivaServicio
						});
					}
					
					
				}
			},
			
			// Pantalla 3 
			{
				text: 'Regresar',
				id: 'btnRegresar',
				iconCls: 'icoAceptar',	
				hidden : true,
				handler: function(boton, evento) {
		
					Ext.getCmp('recibeFactoraje').setValue(false);
					Ext.getCmp('recibeFactoraje2').setValue(false);
					
					Ext.getCmp('email_1').setValue('');
					Ext.getCmp('confirmacionCorreo1').setValue('');
					Ext.getCmp('nuevoCelular1').setValue('');
					Ext.getCmp('confirmacionCelular1').setValue('');	
					
					Ext.Ajax.request({
						url: '15factorajemovilbuscapyme.data.jsp',
						params: {
							informacion: "obtenerDatosPyme",
							icPyme:Ext.getCmp('icPyme1').getValue() 
						},
						callback: procesaDatosPyme
					});
					
				}
			},
			{
				text: 'Confirmar',
				id: 'btnConfirmar',
				iconCls: 'icoAceptar',	
				hidden	  : true,
				handler: function(boton, evento) {	
				
					var id_momentoEnvio_0= Ext.getCmp('id_momentoEnvio_0');
					var id_momentoEnvio_1= Ext.getCmp('id_momentoEnvio_1');
					
					var id_tipoInformacion_0= Ext.getCmp('id_tipoInformacion_0');
					var id_tipoInformacion_1= Ext.getCmp('id_tipoInformacion_1');
					
					var compCelular1= Ext.getCmp('compCelular1');
					var claveConfirmacion1= Ext.getCmp('claveConfirmacion1');
									
					
					if( id_momentoEnvio_0.getValue()==false &&  id_momentoEnvio_1.getValue()==false  ){						
						Ext.MessageBox.alert("Mensaje","Debe de seleccionar alguna opci�n del  momento en el que desea recibir los mensajes");
						return;						
					}
					
					if( id_tipoInformacion_0.getValue()==false &&  id_tipoInformacion_1.getValue()==false  ){						
						Ext.MessageBox.alert("Mensaje","Debe de seleccionar alguna opci�n del tipo de informaci�n que desea recibir");
						return;						
					}
								
					
					if( Ext.isEmpty(compCelular1.getValue() )  ){						
						compCelular1.markInvalid('Este campo es obigatorio');
						return;
					}
					
					if( Ext.isEmpty(claveConfirmacion1.getValue() )  ){						
						claveConfirmacion1.markInvalid('Este campo es obigatorio');
						return;
					}
					
					pnl.el.mask('Procesando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '15factorajemovilbuscapyme.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Confirmar',
							icPyme:Ext.getCmp('icPyme1').getValue(),
							claveConfirmacionSesion:Ext.getCmp('claveConfirmacionSesion').getValue()	
						}),					
						callback: procesarComfirmaActivaServicio
					});
					
				}
			}	
		]
	});
			
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 900,
		height: 'auto',
		style: 'margin:0 auto;',	
		items: [
			NE.util.getEspaciador(20),					
			fp,
			fpCombo,
			NE.util.getEspaciador(20),	
			gridConsulta			
		]
	});


});