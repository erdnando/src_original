Ext.ns('NE.encuestas');

function datecomp(date1,date2) {
  var exito,dd1,mm1,aa1,dd2,mm2,aa2,pos,tempdate;
  var d1,m1,a1,d2,m2,a2;

  if(!((isdate(date1)) && (isdate(date2))))
    exito = -1;
  else
  {
   pos = date1.indexOf("/",0);
   d1 = date1.substring(0,pos);
   date1 = date1.substring(pos+1,date1.length);
   pos = date1.indexOf("/",0);
   m1 = date1.substring(0,pos);
   a1 = date1.substring(pos+1,date1.length);
   pos = date2.indexOf("/",0);
   d2 = date2.substring(0,pos);
   date2 = date2.substring(pos+1,date2.length);
   pos = date2.indexOf("/",0);
   m2 = date2.substring(0,pos); 
   a2 = date2.substring(pos+1,date2.length);
   dd1 = parseInt(d1,10);
   mm1 = parseInt(m1,10);
   aa1 = parseInt(a1,10);
   dd2 = parseInt(d2,10);
   mm2 = parseInt(m2,10);
   aa2 = parseInt(a2,10);


 if((aa1 >= 0) && (aa1 <= 99))
   aa1 += 1900;

 if((aa2 >= 0) && (aa2 <= 99))
   aa2 += 1900;

 if(aa1 >= aa2)
    {
   if(aa1 == aa2)
      {
        if(mm1 >= mm2)
        {
    if(mm1 == mm2)
          {
            if(dd1 >= dd2)
            {
     if(dd1 ==  dd2)
                exito = 0; //son fechas iguales
              else
                exito = 1; //es mayor por dias
            }
            else // es menor por dias
     exito = 2;
          }
          else //es mayor por meses
            exito = 1;
        }
        else //es menor por meses
          exito = 2;
      }
      else //es mayor por anios
        exito = 1;
    }
    else //es menor por anios
      exito = 2;
  }
  return(exito);
}

function isdate(strValue) {
//Para validar la fecha en el formato mm/dd/aaaa
//escribir en la variable strFormatoFecha = "US"

//Para validar la fecha en el formato dd/mm/aaaa
//escribir en la variable strFormatoFecha = ""
  var strFormatoFecha = "";
  var strFecha;
  var arrFecha;
  var strDia;
  var strMes;
  var strAnio;
  var intDia;
  var intMes;
  var intAnio;
  var booFound = false;
 // var cmpFecha = objFecha;
  var arrSeparador = new Array("-","/",".");
  var elementos;
  var error = 0;
  strFecha = strValue;
/////////////////////////////////////////////////
 // if (strFecha.length < 1)
 // { return true; }
//JAVIER	
//  alert(strValue);
  if (strFecha.length < 11 && strFecha.length > 5)
  { 
/////////////////////////////////////////////////
  for (elementos = 0; elementos < arrSeparador.length; elementos++)
  {
    if (strFecha.indexOf(arrSeparador[elementos]) != -1)
    {
      arrFecha = strFecha.split(arrSeparador[elementos]);
// Si los elementos del arreglo son diferente de 3 elementos ejemplo: 01/01/200/34/23/4
      if (arrFecha.length != 3)
      {
        error = 1;
        return false;
      }

      else
      {
        strDia = arrFecha[0];
        strMes = arrFecha[1];
        strAnio = arrFecha[2];
		if(strAnio.length != 4)
			return false;
      }
      booFound = true;
    }

  }

//alert(strDia+" "+strMes+" "+strAnio)

/////////////////////////////////////////////////

  if (booFound == false)
  {
    if (strFecha.length>5)
    {
      strDia = strFecha.substr(0, 2);
      strMes = strFecha.substr(2, 2);
      strAnio = strFecha.substr(4);
    }
  }
/////////////////////////////////////////////////
// Si el a�o es de 2 digitos agrega 20 ejemplo: si es 01 entonces 2001
  if (strAnio.length == 2)
  { strAnio = '20' + strAnio; }

// Si el a�o es 0000
  if (parseInt(strAnio) <= 1900 )
  { return false }

/////////////////////////////////////////////////

  // Formato (mm/dd/aaaa)
  if (strFormatoFecha == "US")
  {
    strTemp = strDia;
    strDia = strMes;
    strMes = strTemp;
  }



/////////////////////////////////////////////////

  intDia = parseInt(strDia, 10);

  if (isNaN(intDia))
  {
    error = 2;
    return false;
  }

/////////////////////////////////////////////////

  intMes = parseInt(strMes, 10);

  if (isNaN(intMes))
  {
    error = 3;
    return false;
  }


/////////////////////////////////////////////////

  intAnio = parseInt(strAnio, 10);

  if (isNaN(intAnio))
  {
    error = 4;
    return false;
  }

/////////////////////////////////////////////////

  if (intMes>12 || intMes<1)
  {
    error = 5;
    return false;
  }

/////////////////////////////////////////////////

  if ((intMes == 1 || intMes == 3 || intMes == 5 || intMes == 7 || intMes == 8 || intMes == 10 || intMes == 12) && (intDia > 31 || intDia < 1))
  {
    error = 6;
    return false;
  }

/////////////////////////////////////////////////

  if ((intMes == 4 || intMes == 6 || intMes == 9 || intMes == 11) && (intDia > 30 || intDia < 1))
  {
    error = 7;
    return false;
  }

/////////////////////////////////////////////////

  if (intMes == 2)
  {
    if (intDia < 1)
    {
      error = 8;
      return false;
    }

    if (esAnioBisiesto(intAnio) == true)
    {
      if (intDia > 29)
      {
        error = 9;
        return false;
      }
    }

    else
    {
      if (intDia > 28)
      {
        error = 10;
        return false;
      }
    }
  }
/////////////////////////////////////////////////
return true;
}

else
{ return false }

}

NE.encuestas.FormAltaPlantilla = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	fnNumPreguntas: null,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpAltaPlantilla'+this.tipoAfiliado,
			width: 810,
			title: '<p align="center">Plantillas</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',  
			defaultType: 'textfield',
			labelWidth: 150,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(this)
			//buttons: this.generarBotones(this)
		});
		
		NE.encuestas.FormAltaPlantilla.superclass.initComponent.call(this);
	},
	generarCampos: function(fp){
		var afiliado = fp.tipoAfiliado;
		var fnNumPreg = fp.fnNumPreguntas;
		return [
				{
					xtype:'textfield',
					name:'fp_titulo'+afiliado,
					id:'fp_titulo1'+afiliado,
					fieldLabel:'T�tulo',
					maxLength:80,
					anchor: '80%',
					allowBlank: false,
					regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
					regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
				},
				{
					xtype: 'textarea',
					id: 'fp_instrucciones1'+afiliado,
					name: 'fp_instrucciones'+afiliado,
					allowBlank: false,
					fieldLabel: "Observaciones",
					anchor: '80%',
					//maxLength:300,
					regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
					regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
				},
				{
					xtype: 'numberfield',
					id: 'fp_numPreguntas1'+afiliado,
					name: 'fp_numPreguntas'+afiliado,
					allowDecimals: false,
					maxLength: 3,
					allowBlank: false,
					fieldLabel: "N�mero de Preguntas",
					anchor: '30%',
					autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3'},
					listeners:{
						change: function(obj, newVal, oldVal){
							fnNumPreg(newVal);
						}
					}
				}
				
			]
	},
	generarBotones: function(fp){
		var afiliado = fp.tipoAfiliado;
		return [
				{
					text: 'Siguiente',
					id: 'btnAltaAcept'+afiliado,
					formBind: true
				},
				{
					text: 'Deshacer',
					id: 'btnAltaDeshacer'+afiliado,
					handler: function(){
						fp.getForm().reset();
					}
				}
			]
	},
	setHandlerBtnSiguiente: function(fn){
		var btnLoginAcept = Ext.getCmp('btnAltaAcept'+this.tipoAfiliado)
			btnLoginAcept.setHandler(fn);
	}
});



//----------------------------------------------------------------------------

NE.encuestas.FormAltaEncuestas = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	fnUsaPlantilla: null,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpAltaEncuestas'+this.tipoAfiliado,
			width: 700,
			title: '<p align="center">Encuesta</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',  
			defaultType: 'textfield',
			labelWidth: 150,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(this),
			buttons: this.generarBotones(this)
		});
		
		NE.encuestas.FormAltaEncuestas.superclass.initComponent.call(this);
	},
	generarCampos: function(obj){
		var afiliado = obj.tipoAfiliado;
		var  storeAfiliadosData  = new Ext.data.JsonStore({
										id:				'storeAfiliadosData'+afiliado,
										root:				'registros',
										fields:			['clave', 'descripcion', 'loadMsg'],
										url:				'15admencu01_ext.data.jsp',
										baseParams:		{	informacion:	'CatalogoAfiliados', tipoAfiliado: afiliado},
										totalProperty:	'total',
										autoLoad:		false,
										listeners: {
											exception: NE.util.mostrarDataProxyError,
											beforeload: NE.util.initMensajeCargaCombo
										}
									});
	
		return [
				{
								xtype:'radiogroup',	id:'radioGp'+afiliado,	anchor:'50%',	style:'margin:0 auto;',	columns:2,
								fieldLabel: 'Usar Plantilla',
								items:[
									{boxLabel: 'Si', name: 'usaPlantilla'+afiliado, inputValue: 'S', checked: false,
										listeners:{
											check:	function(radio){
															//recargar(0);
															if (radio.checked){
																Ext.getCmp('btnAltaAcept'+afiliado).hide();
																Ext.getCmp('btnAltaGuardar'+afiliado).show();
																Ext.getCmp('fp_numPreguntas1'+afiliado).hide();
																Ext.getCmp('fp_numPreguntas1'+afiliado).allowBlank=true;
																obj.fnUsaPlantilla('S');
																
															}
															//actualizaCombos();
														}
										}
									},{boxLabel: 'No', name: 'usaPlantilla'+afiliado, inputValue: 'N',checked: true,
										listeners:{
											check:	function(radio){
															//recargar(0);
															if (radio.checked){
																Ext.getCmp('btnAltaAcept'+afiliado).show();
																Ext.getCmp('btnAltaGuardar'+afiliado).hide();
																Ext.getCmp('fp_numPreguntas1'+afiliado).show();
																Ext.getCmp('fp_numPreguntas1'+afiliado).allowBlank=false;
																obj.fnUsaPlantilla('N');
															}
															//actualizaCombos();
														}
										}
									}
								]
							},
				{
					xtype:'textfield',
					name:'fp_titulo'+afiliado,
					id:'fp_titulo1'+afiliado,
					fieldLabel:'T�tulo',
					maxLength:80,
					anchor: '80%',
					allowBlank: false,
					regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
					regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
				},
				{
					xtype: 'compositefield',
					fieldLabel: 'Publicar Desde',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:			'datefield',
							name:				'fp_fec_desde'+afiliado,
							id:				'fp_fec_desde1'+afiliado,
							allowBlank:		false,
							startDay:		0,
							width:			100,
							msgTarget:		'side',
							vtype:			'rangofecha', 
							campoFinFecha:	'fp_fec_hasta1'+afiliado,
							margins:			'0 20 0 0',  //necesario para mostrar el icono de error
							regexText:"La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
							regex: /.*[1-9][0-9]{3}$/
						},
						{
							xtype: 'displayfield',
							value: 'hasta:',
							width: 30
						},
						{
							xtype:				'datefield',
							name:					'fp_fec_hasta'+afiliado,
							id:					'fp_fec_hasta1'+afiliado,
							allowBlank:			false,
							startDay:			1,
							width:				100,
							msgTarget:			'side',
							vtype:				'rangofecha', 
							campoInicioFecha:	'fp_fec_desde1'+afiliado,
							margins:				'0 20 0 0',  //necesario para mostrar el icono de error
							regexText:"La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
							regex: /.*[1-9][0-9]{3}$/
							}
					]
				},
				{
					xtype:			'combo',
					id:				'fp_cadena1'+afiliado,
					name:				'fp_cadena'+afiliado,
					hiddenName:		'fp_cadena'+afiliado,
					fieldLabel:		'Cadena',
					emptyText:		'Seleccionar. . .',
					mode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					forceSelection:true,
					typeAhead:		true,
					triggerAction:	'all',
					minChars:		1,
					cafiliado: afiliado,
					hidden: (afiliado=='P')?false:true,
					allowBlank: (afiliado=='P')?false:true,
					store:			new Ext.data.JsonStore({
										id:				'storeCatCadenasData'+afiliado,
										root:				'registros',
										fields:			['clave', 'descripcion', 'loadMsg'],
										url:				'15admencu01_ext.data.jsp',
										baseParams:		{	informacion:	'CatalogoCadenas', tipoAfiliado: afiliado	},
										totalProperty:	'total',
										autoLoad:		true,
										listeners: {
											exception: NE.util.mostrarDataProxyError,
											beforeload: NE.util.initMensajeCargaCombo
										}
									}),
					tpl:				NE.util.templateMensajeCargaCombo,
					listeners:{
						change: function(obj, newVal, oldVal){
							var vTipoAfil = Ext.getCmp('fp_tipoAfiliado1'+obj.cafiliado).getValue();
							
							if(newVal!=''){
								if(vTipoAfil!='' && obj.cafiliado=='P' ){
									storeAfiliadosData.load({
										params:{
											catTipoAfil: vTipoAfil,
											cveEpo: newVal
										}
									});
								}
							}
						}
					}
				},
				{
					xtype:			'combo',
					id:				'fp_tipoAfiliado1'+afiliado,
					name:				'fp_tipoAfiliado'+afiliado,
					hiddenName:		'fp_tipoAfiliado'+afiliado,
					fieldLabel:		'Tipo de Afiliado',
					emptyText:		'Seleccionar. . .',
					mode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					forceSelection:true,
					typeAhead:		true,
					triggerAction:	'all',
					minChars:		1,
					cafiliado: afiliado,
					allowBlank: false,
					store:			new Ext.data.JsonStore({
										id:				'storeCatTipoAfilData'+afiliado,
										root:				'registros',
										fields:			['clave', 'descripcion', 'loadMsg'],
										url:				'15admencu01_ext.data.jsp',
										baseParams:		{	informacion:	'CatalogoTipoAfil', tipoAfiliado: afiliado},
										totalProperty:	'total',
										autoLoad:		true,
										afiliado: afiliado,
										listeners: {
											exception: NE.util.mostrarDataProxyError,
											beforeload: NE.util.initMensajeCargaCombo,
											load: function(store, arrRegistros, opts){
												if(store.getTotalCount()>0 &&  Ext.isEmpty(arrRegistros[0].data.loadMsg)){
													 if(store.afiliado=='E'){
														 Todas = Ext.data.Record.create([ 
															{name: "clave",         type: "string"}, 
															{name: "descripcion",   type: "string"}, 
															{name: "loadMsg",       type: "string"}
														]);
	
														
														 store.insert(0,new Todas({ 
															clave: "-", 
															descripcion: "Todos", 
															loadMsg: ""})); 
														store.commitChanges(); 
													}
													
												}
											}
										}
									}),
					tpl:				NE.util.templateMensajeCargaCombo,
					listeners:{
						change: function(obj, newVal, oldVal){
							var vCadena = Ext.getCmp('fp_cadena1'+obj.cafiliado).getValue();
							newVal = newVal=="-"?"":newVal;
							if(newVal!=''){
								if(obj.cafiliado!='P'){
									storeAfiliadosData.load({
										params:{
											catTipoAfil: newVal
										}
									});
								}
								
								if(vCadena!='' && obj.cafiliado=='P' ){
									storeAfiliadosData.load({
										params:{
											catTipoAfil: newVal,
											cveEpo: vCadena
										}
									});
								}
							}else{
								if(obj.cafiliado=='E'){
									storeAfiliadosData.load({
										params:{
											catTipoAfil: newVal
										}
									});
								}
							}
						}
					}
				},
				{
					xtype: 'itemselector',
					name: 'fp_selecAfiliados'+afiliado,
					id: 'fp_selecAfiliados1'+afiliado,
					fieldLabel: '',
					imagePath: '/nafin/00utils/extjs/ux/images/',
					drawUpIcon:false, 
					drawDownIcon:false, 
					drawTopIcon:false, 
					drawBotIcon:false,
					border: false,
					allowBlank: false,
					//margins: '20 20 20 20',
					multiselects: [{
						 width: 250,
						 height: 100,
						 legend: '',
						 id: 'fp_afiliados1'+afiliado,
						 store: storeAfiliadosData,
						 //disabled: true,
						 displayField: 'descripcion',
						 valueField: 'clave'
					},{
						 width: 250,
						 height: 100,
						 legend: '',
						 id: 'fp_afiliados_add1'+afiliado,
						 store: new Ext.data.JsonStore({
										id:				'storeAfiliadosBData'+afiliado,
										root:				'registros',
										fields:			['clave', 'descripcion', 'loadMsg'],
										url:				'15admencu01_ext.data.jsp',
										baseParams:		{	informacion:	'CatalogoAfiliadosB', tipoAfiliado: afiliado},
										totalProperty:	'total',
										autoLoad:		false,
										listeners: {
											exception: NE.util.mostrarDataProxyError,
											beforeload: NE.util.initMensajeCargaCombo
										}
									}),
						 //disabled: true,
						 allowBlank: false,
						 displayField: 'descripcion',
						 valueField: 'clave'
						 
					}]
				},
				{
					xtype: 'compositefield',
					fieldLabel: '',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
						 xtype: 'radio',
						 fieldLabel: '',
						 boxLabel: 'Obligatoria',
						 name: 'fp_respObligada'+afiliado,
						 inputValue: 'S',
						 checked: true,
						 listeners:{
							check: function(obj, checked){
								if(checked){
									var objDias = Ext.getCmp('fp_diasResp1'+afiliado);
									objDias.enable();
									objDias.allowBlank= false;
								}
							}
						 }
						},
						{
							xtype: 'displayfield',
							value: '(Solicitar la respuesta ',
							width: 115
						},
						{
							xtype: 'numberfield',
							id: 'fp_diasResp1'+afiliado,
							name: 'fp_diasResp'+afiliado,
							allowDecimals: false,
							allowBlank: false,
							maxLength: 3,
							width:30
						},
						{
							xtype: 'displayfield',
							value: ' d�as h�biles previos al vencimiento)',
							width: 200
						}
					]
				},
				{
					xtype:'panel',
					layout: 'form',
					items:[
						{
							xtype: 'radio',
							fieldLabel: '',
							boxLabel: 'No obligatoria',
							labelSeparator: ':',
							name: 'fp_respObligada'+afiliado,
							inputValue: 'N',
							listeners: {
								check: function(obj, checked){
									if(checked){
										var objDias = Ext.getCmp('fp_diasResp1'+afiliado);
										objDias.setValue('');
										objDias.disable();
										objDias.allowBlank= true;
									}
								}	
							}
						}
					]
				},
				{
					xtype: 'textarea',
					id: 'fp_instrucciones1'+afiliado,
					name: 'fp_instrucciones'+afiliado,
					allowBlank: false,
					fieldLabel: "Instrucciones"
				},
				{
					xtype: 'numberfield',
					id: 'fp_numPreguntas1'+afiliado,
					name: 'fp_numPreguntas'+afiliado,
					allowDecimals: false,
					maxLength: 3,
					allowBlank: false,
					fieldLabel: "N�mero de Preguntas",
					anchor: '30%',
					autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3'}
				},
				{
					xtype:'panel',
					id: 'pnlGridEncPlant'+afiliado,
					anchor: '100%',
					items:[NE.util.getEspaciador(10)]
				}
				
			]
	},
	generarBotones: function(fp){
		var afiliado = fp.tipoAfiliado;
		return [
				{
					text: 'Guardar',
					id: 'btnAltaGuardar'+afiliado,
					hidden: true,
					formBind: true
				},
				{
					text: 'Siguiente',
					id: 'btnAltaAcept'+afiliado,
					formBind: true
				},
				{
					text: 'Deshacer',
					id: 'btnAltaDeshacer'+afiliado,
					handler: function(){
						fp.getForm().reset();
						Ext.StoreMgr.key('storeAfiliadosData'+afiliado).removeAll();
						
					}
				}
			]
	},
	setHandlerBtnSiguiente: function(fn){
		var btnLoginAcept = Ext.getCmp('btnAltaAcept'+this.tipoAfiliado)
			btnLoginAcept.setHandler(fn);
	},
	setHandlerBtnGuardar: function(fn){
		var btnGuardarEncPlant = Ext.getCmp('btnAltaGuardar'+this.tipoAfiliado)
			btnGuardarEncPlant.setHandler(fn);
	}
});


NE.encuestas.GridEncPlantillas = Ext.extend(Ext.grid.EditorGridPanel,{
	tipoAfiliado: null,
	fnLoadStore:null,
	fnVistaPrevia:null,
	//claveEncuesta: null,
	initComponent : function(){
		Ext.apply(this, {
			id: 'gridEncPlantillas1'+this.tipoAfiliado,
			store: new Ext.data.JsonStore({
				url : '15admencu01_ext.data.jsp',
				id: 'storeEncPlantData1'+this.tipoAfiliado,
				afiliado:this.tipoAfiliado,
				baseParams:{
					informacion: 'ConsultaPlantillas'
				},
				root : 'registros',
				fields: [
					{name: 'CVEPLANTILLA'},
					{name: 'TITULO'},
					{name: 'VISTAPREV'},
					{name: 'SELECCION'}
					
				],
				totalProperty : 'total',
				messageProperty: 'msg',
				autoLoad: false,
				listeners: {
					load: this.fnLoadStore,
					exception: {
						fn: function(proxy, type, action, optionsRequest, response, args) {
							NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
							this.fnLoadStore(null, null, null);
						}
					}
				}
			}),
			style:	'margin:0 auto;',
			margins: '20 0 0 0',
			viewConfig: {
				templates: {
					cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
						'</td>'
					)
				}
			},
			columns: [
				{//2
					header: '<p align="center">T�tulo</p>',
					tooltip: 'T�tulo',
					dataIndex: 'TITULO',
					sortable: true,
					hideable: false,
					width: 200,
					align: 'left',
					hidden: false
					//renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{
					xtype: 		'actioncolumn',
					header: 		'Seleccionar',
					tooltip: 	'Vista Previa',
					width: 		120,
					align:		'center',
					tipoAfil: this.tipoAfiliado,
					hidden: 		false,
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								this.items[0].tooltip = 'Ver';
								return 'icoLupa';
							},
							handler: function(grid, rowIndex, colIndex) {
								var registro = Ext.StoreMgr.key('storeEncPlantData1'+grid.tipoAfiliado).getAt(rowIndex);
								grid.fnVistaPrevia(registro.data['CVEPLANTILLA']);
							}
						}
					]
				},
				{
					header: 'Seleccionar',
					tooltip: 'Seleccionar',
					dataIndex: 'CVEPLANTILLA',
					sortable: true,
					width: 100,
					align: 'center',
					hidden: false,
					renderer:function(val, cell, record, rowIndex, colIndex, store){ 
						var retval = '<input type="radio" name="myRadioButtonPlant'+store.afiliado+'" value="'+val+'" onclick="asignaPlantilla(\''+val+'\', '+'\''+record.data["TITULO"]+'\', '+'\''+store.afiliado+'\')" >'; 
						return retval;
					 }
				}
			],
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 200,
			width: 420,
			style: 'margin:0 auto;',
			title: '',
			frame: true
	
		});
		
		NE.encuestas.GridEncPlantillas.superclass.initComponent.call(this);
	}
});


NE.encuestas.FormPreguntasEnc = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	numeroPreguntas: 1,
	numeroPregMax: 0,
	modif: '',
	objDataPreg:null,
	esPlantilla:'N',
	collapsible: true,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpPreguntasEnc'+this.tipoAfiliado+this.modif,
			width: 810,
			height:400,
			title: '<p align="center">Preguntas</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			//bodyStyle: 'padding-left:5px;,padding-top:10px;,padding:6px;,text-align:left',
			//baseCls:'x-plain',
			defaultType: 'textarea',
			labelWidth: 150,
			autoScroll: true,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			//items : this.generarCampos(this, this.tipoAfiliado, this.numeroPreguntas),
			listeners:{
				afterrender: function(obj){
					//alert(obj.generarCampos);
					if(obj.modif==''){
						obj.generarCampos(obj, obj.tipoAfiliado, obj.numeroPreguntas, 0);
					}else if(obj.modif=='s'){
						obj.generarCamposMod(obj, 0, 0, 'N');
					}
				}
			},
			buttons: this.generarBotones(this, this.tipoAfiliado)
		});
		
		NE.encuestas.FormPreguntasEnc.superclass.initComponent.call(this);
	},
	generarCamposMod: function(fp, newNumPreg, oldNumPreg, agregarPreg){
		var afiliado = fp.tipoAfiliado;
		var modif = fp.modif;
		var lstDataPreg = fp.objDataPreg;
		var esPlantilla = fp.esPlantilla;
		
		if(agregarPreg!='S'){
			if(lstDataPreg.length>0){
				var p = lstDataPreg.length;
				var pregAsoc = 1;
				for(var i=0; i<lstDataPreg.length; i++){
					var dataPreg = lstDataPreg[i];
					var vOrdenRenglon =  dataPreg.IGORDENPREG;
					var vNumPregunta =  dataPreg.IGNUMPREG;
					var vTipoPreg = dataPreg.TIPOPREG; 
					var vTipoResp = dataPreg.TIPORESP; 
					var vCgPregunta = dataPreg.CGPREGUNTA;
					var vNumOpc = dataPreg.IGNUMOPC;
					fp.numeroPregMax = vNumPregunta;
					if(i==0){
						var filaCabPregunta = new Ext.Panel({
							id: 'pnlPreg'+i+afiliado+modif,
							identificador: 'renglon',
							identificador2: 'renglonPregunta',
							frame: false,
							layout:'table',
							//width: 600,
							layoutConfig: {columns:5},
							indiceObj: i,
							items: [
								{
									xtype:'panel',
									frame: false,
									width:260,
									//height: (i==1)?100:60,
									layout:'hbox',
									title:'<p align="center">Preguntas<br><br></p>'
								},
								{
									xtype:'panel',
									frame: false,
									width:150,
									//height: (i==1)?100:60,
									layout:'anchor',
									border: false,
									style: 'margin:0 auto;',
									title:'<p align="center">Tipo de Pregunta <br><br></p>'
								},
								{
									xtype:'panel',
									frame: false,
									width:150,
									//height: (i==1)?100:60,
									layout:'anchor',
									border: false,
									style: 'margin:0 auto;',
									title:'<p align="center">Tipo de Respuesta<br><br></p>'
								},
								{
									xtype:'panel',
									frame: false,
									width:100,
									//height: (i==1)?100:60,
									title:'<p align="center">N�mero de<br>opciones</p>',
									style: 'margin:0 auto;'
								},
								{
									xtype:'panel',
									frame: false,
									width:100,
									hidden: (esPlantilla=='S')?false:true,
									//height: (i==1)?100:60,
									title:'<p align="center">Seleccionar<br><br></p>',
									style: 'margin:0 auto;'
								}
							]
						});
						
						//fp = Ext.getCmp('fpPreguntasEnc'+this.tipoAfiliado)
						fp.add(filaCabPregunta);
						fp.doLayout();
					}//else{
						var filaPregunta;
						if(dataPreg.TIPORESP=='4'){
							filaPregunta = new Ext.Panel({
								id: 'pnlPreg'+p+afiliado+modif,
								identificador: 'renglon',
								frame: false,
								layout:'table',
								//width: 600,
								layoutConfig: {columns:5},
								preguntaAsoc: 'pnlPreg'+pregAsoc+afiliado+modif,
								indiceObj: p,
								items: [
									{
										xtype:'panel',
										frame: true,
										width:260,
										height: 40,
										layout:'hbox',
										title:'',
										items:[
											{
											xtype: 'displayfield',
											value: '',
											width: 20
											},
											{
												xtype: 'textfield',
												id: 'fpPregunta1'+p+afiliado+modif,
												name: 'fpRubro'+pregAsoc+afiliado+modif,
												fieldLabel: '',
												allowBlank: false,
												maxLength: 300,
												value: vCgPregunta,
												//height: 50,
												width: 320
											}]
									},
									{
										xtype:'panel',
										frame: true,
										width:150,
										height: 40,
										layout:'anchor',
										border: false,
										style: 'margin:0 auto;',
										title:'',
										items:[
													{
											xtype: 'displayfield',
											value: ' '
											}
												]
									},
									{
										xtype:'panel',
										frame: true,
										width:150,
										height: 40,
										layout:'hbox',
										border: false,
										style: 'margin:0 auto;',
										title:'',
										items:[
													{
														xtype:			'textfield',
														id:				'fpTextResp1'+p+afiliado+modif,
														name:				'fpTextResp1'+p+afiliado+modif,
														//style: 'margin:0 auto;',
														value: 'Rubro',
														allowBlank: false,
														readOnly: true,
														anchor: '100%',
														consecutivo: i
													},
													{
														xtype: 'hidden',
														id: 'fpTipoResp1'+p+afiliado+modif,
														name: 'fpTipoResp'+p+afiliado+modif,
														value: 4
													}
												]
									},
									{
										xtype:'panel',
										frame: true,
										width:100,
										height: 40,
										title: '',
										style: 'margin:0 auto;',
										items:[
											/*{
												xtype: 'hiddenfield',
												id: 'fpNumOpc1'+indice+afiliado,
												name: 'fpNumOpc'+indice+afiliado,
												allowBlank: true,
												disabled: true,
												allowDecimals: false,
												width:20
											},*/
											{
												xtype:'button',
												name:'btnEliminaReg'+p+afiliado+modif,
												iconCls: 'borrar',
												consecutivo: p,
												width:23,
												handler: function(obj){
													//alert('pnlPreg'+i+afiliado+modif);
													fp.remove(Ext.getCmp('pnlPreg'+obj.consecutivo+afiliado+modif));
													fp.doLayout();
												}
											}
										]
									},
									{
										xtype:'panel',
										frame: true,
										width:100,
										height: 40,
										layout:'anchor',
										border: false,
										style: 'margin:0 auto;',
										title:'',
										items:[
													{
											xtype: 'displayfield',
											value: ' '
											}
												]
									}
								]
							});
							p++;
						}else{
							pregAsoc++;
							filaPregunta = new Ext.Panel({
								id: 'pnlPreg'+vNumPregunta+afiliado+modif,
								identificador: 'renglon',
								identificador2: 'renglonPregunta',
								frame: false,
								layout:'table',
								//width: 600,
								layoutConfig: {columns:5},
								indiceObj: vNumPregunta,
								items: [
									{
										xtype:'panel',
										frame: true,
										width:260,
										height: 60,
										layout:'hbox',
										//title:(i==1)?'<p align="center">Preguntas<br><br></p>':'',
										title:'',
										items:[
											{
												xtype: 'hidden',
												id: 'fpHideNumeros1'+vNumPregunta+afiliado+modif,
												name: 'fpHideNumeros'+afiliado,
												value: vNumPregunta
											},
											{
												xtype: 'displayfield',
												id: 'numPregFila'+vNumPregunta+afiliado+modif,
												value: vNumPregunta,
												width: 20
											},
											{
												xtype: 'hidden',
												id: 'fpNumPreg1'+vNumPregunta+afiliado+modif,
												name: 'fpNumPreg'+vNumPregunta+afiliado+modif,
												value: vNumPregunta
											},
											{
												xtype: 'textarea',
												id: 'fpPregunta1'+vNumPregunta+afiliado+modif,
												name: 'fpPregunta'+vNumPregunta+afiliado+modif,
												fieldLabel: vNumPregunta,
												allowBlank: false,
												maxLength: 300,
												height: 50,
												value: vCgPregunta,
												width: 320
											}]
									},
									{
										xtype:'panel',
										frame: true,
										width:150,
										height: 60,
										layout:'anchor',
										border: false,
										style: 'margin:0 auto;',
										//title:(i==1)?'<p align="center">Tipo de Pregunta <br><br></p>':'',
										title:'',
										items:[
													{
														xtype:			'combo',
														id:				'fpTipoPreg1'+vNumPregunta+afiliado+modif,
														name:				'fpTipoPreg'+vNumPregunta+afiliado+modif,
														hiddenName:		'fpTipoPreg'+vNumPregunta+afiliado+modif,
														fieldLabel:		'',
														style: 'margin:0 auto;',
														emptyText:		'Seleccionar. . .',
														mode:				'local',
														displayField:	'descripcion',
														valueField:		'clave',
														forceSelection:true,
														typeAhead:		true,
														triggerAction:	'all',
														minChars:		1,
														allowBlank: false,
														anchor: '100%',
														consecutivo: vNumPregunta,
														modif: modif,
														valueIni: vTipoPreg,
														value: (vTipoPreg!='')?vTipoPreg:'N',
														store: new Ext.data.ArrayStore({
																		  id:	'storeCatTipoPreg'+vNumPregunta+afiliado+modif,
																		  consecutivo: vNumPregunta,
																		  modif: modif,
																		  fields: [
																				'clave',
																				'descripcion'
																		  ],
																		  data: [['N', 'Normal'], ['E', 'Excluyente']]
																	}),
														//tpl:NE.util.templateMensajeCargaCombo,
														listeners:{
															change: function(obj, newVal, oldVal){
																var tipoResp = Ext.getCmp('fpTipoResp1'+obj.consecutivo+afiliado+obj.modif);
																var numResp = Ext.getCmp('fpNumOpc1'+obj.consecutivo+afiliado.modif);
																if(newVal=='E'){
																	tipoResp.setValue(3);
																	numResp.setValue(2);
																}
				
															}
														}
													}
												]
									},
									{
										xtype:'panel',
										frame: true,
										width:150,
										height: 60,
										layout:'anchor',
										border: false,
										style: 'margin:0 auto;',
										//title:(i==1)?'<p align="center">Tipo de Respuesta<br><br></p>':'',
										title:'',
										items:[
													{
														xtype:			'combo',
														id:				'fpTipoResp1'+vNumPregunta+afiliado+modif,
														name:				'fpTipoResp'+vNumPregunta+afiliado+modif,
														hiddenName:		'fpTipoResp'+vNumPregunta+afiliado+modif,
														fieldLabel:		'',
														style: 'margin:0 auto;',
														emptyText:		'Seleccionar. . .',
														mode:				'local',
														displayField:	'descripcion',
														valueField:		'clave',
														forceSelection:true,
														typeAhead:		true,
														triggerAction:	'all',
														minChars:		1,
														allowBlank: false,
														anchor: '100%',
														consecutivo: vNumPregunta,
														valueIni: vTipoResp,
														modif: modif,
														store:			new Ext.data.JsonStore({
																			id:				'storeCatTipoResp'+vNumPregunta+afiliado+modif,
																			root:				'registros',
																			fields:			['clave', 'descripcion', 'loadMsg'],
																			url:				'15admencu01_ext.data.jsp',
																			baseParams:		{	informacion:	'CatalogoTipoPregunta'	},
																			totalProperty:	'total',
																			autoLoad:		true,
																			consecutivo: vNumPregunta,
																			modif: modif,
																			valueIni: vTipoResp,
																			listeners: {
																				exception: NE.util.mostrarDataProxyError,
																				beforeload: NE.util.initMensajeCargaCombo,
																				load: function(store, records, option){
																					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
																						var catTipoResp = Ext.getCmp('fpTipoResp1'+store.consecutivo+afiliado+store.modif);
																						if(catTipoResp.getValue()==''){
																							if(store.valueIni!=''){
																								catTipoResp.setValue(store.valueIni);
																							}else{
																								catTipoResp.setValue(1);
																							}
																						}
																					}
																			  } 
																			}
																		}),
														tpl:NE.util.templateMensajeCargaCombo,
														listeners:{
															change: function(obj, newlVal, oldVal){
																var tipoPreg = Ext.getCmp('fpTipoPreg1'+obj.consecutivo+afiliado+obj.modif);
																var numResp = Ext.getCmp('fpNumOpc1'+obj.consecutivo+afiliado+obj.modif);
																var renglones = fp.find('identificador','renglon');
																
																if(newlVal==1){
																	numResp.setValue(1);
																}
																if(newlVal==3){
																	numResp.setValue(2);
																}
																if(newlVal!=3){
																	tipoPreg.setValue('N');
																}
																
																for(var index=0; index<renglones.length; index++){
																	if(renglones[index].id == 'pnlPreg'+obj.consecutivo+afiliado+obj.modif){
																		if(newlVal==4){
																			numResp.setValue(1);
																			fp.insert(index, fp.agregaRubro(fp, p, afiliado, obj.consecutivo, 'pnlPreg'+obj.consecutivo+afiliado+obj.modif));
																			fp.doLayout();
																			p++;
																			obj.setValue(1);
																		}
																	}
																}; 
															}
														}
													}
												]
									},
									{
										xtype:'panel',
										frame: true,
										width:100,
										height: 60,
										//title:(i==1)?'<p align="center">N�mero de<br>opciones</p>':'',
										title:'',
										style: 'margin:0 auto;',
										items:[
											{
												xtype: 'numberfield',
												id: 'fpNumOpc1'+vNumPregunta+afiliado+modif,
												name: 'fpNumOpc'+vNumPregunta+afiliado+modif,
												allowBlank: false,
												allowDecimals: false,
												valueIni: vNumOpc,
												value: vNumOpc,
												maxLength:2,
												autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '2'},
												width:20,
												consecutivo: vNumPregunta,
												modif: modif,
												listeners:{
													blur: function(obj){
														var vTipoResp = Ext.getCmp('fpTipoResp1'+obj.consecutivo+afiliado+obj.modif).getValue();
														
														if(vTipoResp=='1' && (obj.getValue()=='' || Number(obj.getValue()))>1){
															Ext.MessageBox.alert('Aviso', 'El n�mero m�ximo de opciones deber� ser 1 para el tipo de respuesta Libre', function(){
																obj.setValue(1);
															});
														}else if(vTipoResp=='3' && (obj.getValue()=='' || Number(obj.getValue()))<2){
															Ext.MessageBox.alert('Aviso', 'El n�mero m�nimo de opciones deber� ser 2 para el tipo de respuesta Opci�n Excluyente', function(){
																obj.setValue(2);
															});
															
														}else if(obj.getValue()=='' || Number(obj.getValue())<=0){
															obj.setValue(1);
														}
														
													}
												}
											}
										]
									},
									{
										xtype:'panel',
										frame: true,
										width:100,
										height: 60,
										hidden: (esPlantilla=='S')?false:true,
										title:'',
										style: 'margin:0 auto;',
										items:[
											{
												xtype: 'checkbox',
												id: 'fpSelecccion1'+vNumPregunta+afiliado+modif,
												name: 'fpSelecccion'+vNumPregunta+afiliado+modif,
												//valueIni: vNumOpc,
												width:300,
												consecutivo: vNumPregunta,
												modif: modif
										  }
										]
									}
								]
							});
						}
						
						//fp = Ext.getCmp('fpPreguntasEnc'+this.tipoAfiliado)
						fp.add(filaPregunta);
						fp.doLayout();
					}
				//}
			}
		
		}else{
			
			for(oldNumPreg; oldNumPreg<newNumPreg; oldNumPreg++){
			var vNumPregunta = oldNumPreg+1;
			filaPregunta = new Ext.Panel({
					id: 'pnlPreg'+vNumPregunta+afiliado+modif,
					identificador: 'renglon',
					identificador2: 'renglonPregunta',
					frame: false,
					layout:'table',
					//width: 600,
					layoutConfig: {columns:5},
					indiceObj: vNumPregunta,
					items: [
						{
							xtype:'panel',
							frame: true,
							width:260,
							height: 60,
							layout:'hbox',
							//title:(i==1)?'<p align="center">Preguntas<br><br></p>':'',
							title:'',
							items:[
								{
									xtype: 'hidden',
									id: 'fpHideNumeros1'+vNumPregunta+afiliado+modif,
									name: 'fpHideNumeros'+afiliado,
									value: vNumPregunta
								},
								{
									xtype: 'displayfield',
									id: 'numPregFila'+vNumPregunta+afiliado+modif,
									value: vNumPregunta,
									width: 20
								},
								{
									xtype: 'hidden',
									id: 'fpNumPreg1'+vNumPregunta+afiliado+modif,
									name: 'fpNumPreg'+vNumPregunta+afiliado+modif,
									value: vNumPregunta
								},
								{
									xtype: 'textarea',
									id: 'fpPregunta1'+vNumPregunta+afiliado+modif,
									name: 'fpPregunta'+vNumPregunta+afiliado+modif,
									fieldLabel: vNumPregunta,
									allowBlank: false,
									maxLength: 300,
									height: 50,
									//value: vCgPregunta,
									width: 320
								}]
						},
						{
							xtype:'panel',
							frame: true,
							width:150,
							height: 60,
							layout:'anchor',
							border: false,
							style: 'margin:0 auto;',
							//title:(i==1)?'<p align="center">Tipo de Pregunta <br><br></p>':'',
							title:'',
							items:[
										{
											xtype:			'combo',
											id:				'fpTipoPreg1'+vNumPregunta+afiliado+modif,
											name:				'fpTipoPreg'+vNumPregunta+afiliado+modif,
											hiddenName:		'fpTipoPreg'+vNumPregunta+afiliado+modif,
											fieldLabel:		'',
											style: 'margin:0 auto;',
											emptyText:		'Seleccionar. . .',
											mode:				'local',
											displayField:	'descripcion',
											valueField:		'clave',
											forceSelection:true,
											typeAhead:		true,
											triggerAction:	'all',
											minChars:		1,
											allowBlank: false,
											anchor: '100%',
											consecutivo: vNumPregunta,
											modif: modif,
											//valueIni: vTipoPreg,
											//value: (vTipoPreg!='')?vTipoPreg:'N',
											value: 'N',
											store: new Ext.data.ArrayStore({
															  id:	'storeCatTipoPreg'+vNumPregunta+afiliado+modif,
															  consecutivo: vNumPregunta,
															  modif: modif,
															  fields: [
																	'clave',
																	'descripcion'
															  ],
															  data: [['N', 'Normal'], ['E', 'Excluyente']]
														}),
											//tpl:NE.util.templateMensajeCargaCombo,
											listeners:{
												change: function(obj, newVal, oldVal){
													var tipoResp = Ext.getCmp('fpTipoResp1'+obj.consecutivo+afiliado+obj.modif);
													var numResp = Ext.getCmp('fpNumOpc1'+obj.consecutivo+afiliado.modif);
													if(newVal=='E'){
														tipoResp.setValue(3);
														numResp.setValue(2);
													}
	
												}
											}
										}
									]
						},
						{
							xtype:'panel',
							frame: true,
							width:150,
							height: 60,
							layout:'anchor',
							border: false,
							style: 'margin:0 auto;',
							//title:(i==1)?'<p align="center">Tipo de Respuesta<br><br></p>':'',
							title:'',
							items:[
										{
											xtype:			'combo',
											id:				'fpTipoResp1'+vNumPregunta+afiliado+modif,
											name:				'fpTipoResp'+vNumPregunta+afiliado+modif,
											hiddenName:		'fpTipoResp'+vNumPregunta+afiliado+modif,
											fieldLabel:		'',
											style: 'margin:0 auto;',
											emptyText:		'Seleccionar. . .',
											mode:				'local',
											displayField:	'descripcion',
											valueField:		'clave',
											forceSelection:true,
											typeAhead:		true,
											triggerAction:	'all',
											minChars:		1,
											allowBlank: false,
											anchor: '100%',
											consecutivo: vNumPregunta,
											//valueIni: vTipoResp,
											modif: modif,
											store:			new Ext.data.JsonStore({
																id:				'storeCatTipoResp'+vNumPregunta+afiliado+modif,
																root:				'registros',
																fields:			['clave', 'descripcion', 'loadMsg'],
																url:				'15admencu01_ext.data.jsp',
																baseParams:		{	informacion:	'CatalogoTipoPregunta'	},
																totalProperty:	'total',
																autoLoad:		true,
																consecutivo: vNumPregunta,
																modif: modif,
																//valueIni: vTipoResp,
																listeners: {
																	exception: NE.util.mostrarDataProxyError,
																	beforeload: NE.util.initMensajeCargaCombo,
																	load: function(store, records, option){
																		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
																			var catTipoResp = Ext.getCmp('fpTipoResp1'+store.consecutivo+afiliado+store.modif);
																			if(catTipoResp.getValue()==''){
																				catTipoResp.setValue(1);
																			}
																		}
																  } 
																}
															}),
											tpl:NE.util.templateMensajeCargaCombo,
											listeners:{
												change: function(obj, newlVal, oldVal){
													var tipoPreg = Ext.getCmp('fpTipoPreg1'+obj.consecutivo+afiliado+obj.modif);
													var numResp = Ext.getCmp('fpNumOpc1'+obj.consecutivo+afiliado+obj.modif);
													var renglones = fp.find('identificador','renglon');
													
													if(newlVal==1){
														numResp.setValue(1);
													}
													if(newlVal==3){
														numResp.setValue(2);
													}
													if(newlVal!=3){
														tipoPreg.setValue('N');
													}
													
													for(var index=0; index<renglones.length; index++){
														if(renglones[index].id == 'pnlPreg'+obj.consecutivo+afiliado+obj.modif){
															if(newlVal==4){
																numResp.setValue(1);
																fp.insert(index, fp.agregaRubro(fp, p, afiliado, obj.consecutivo, 'pnlPreg'+obj.consecutivo+afiliado+obj.modif));
																fp.doLayout();
																p++;
																obj.setValue(1);
															}
														}
													}; 
												}
											}
										}
									]
						},
						{
							xtype:'panel',
							frame: true,
							width:100,
							height: 60,
							//title:(i==1)?'<p align="center">N�mero de<br>opciones</p>':'',
							title:'',
							style: 'margin:0 auto;',
							items:[
								{
									xtype: 'numberfield',
									id: 'fpNumOpc1'+vNumPregunta+afiliado+modif,
									name: 'fpNumOpc'+vNumPregunta+afiliado+modif,
									allowBlank: false,
									allowDecimals: false,
									//valueIni: vNumOpc,
									value: 1,
									maxLength:2,
									autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '2'},
									width:20,
									consecutivo: vNumPregunta,
									modif: modif,
									listeners:{
										blur: function(obj){
											var vTipoResp = Ext.getCmp('fpTipoResp1'+obj.consecutivo+afiliado+obj.modif).getValue();
											
											if(vTipoResp=='1' && (obj.getValue()=='' || Number(obj.getValue()))>1){
												Ext.MessageBox.alert('Aviso', 'El n�mero m�ximo de opciones deber� ser 1 para el tipo de respuesta Libre', function(){
													obj.setValue(1);
												});
											}else if(vTipoResp=='3' && (obj.getValue()=='' || Number(obj.getValue()))<2){
												Ext.MessageBox.alert('Aviso', 'El n�mero m�nimo de opciones deber� ser 2 para el tipo de respuesta Opci�n Excluyente', function(){
													obj.setValue(2);
												});
												
											}else if(obj.getValue()=='' || Number(obj.getValue())<=0){
												obj.setValue(1);
											}
											
										}
									}
								}
							]
						},
						{
							xtype:'panel',
							frame: true,
							width:100,
							height: 60,
							hidden: (esPlantilla=='S')?false:true,
							title:'',
							style: 'margin:0 auto;',
							items:[
								{
									xtype: 'checkbox',
									id: 'fpSelecccion1'+vNumPregunta+afiliado+modif,
									name: 'fpSelecccion'+vNumPregunta+afiliado+modif,
									//valueIni: vNumOpc,
									width:300,
									consecutivo: vNumPregunta,
									modif: modif
							  }
							]
						}
					]
				});
				fp.add(filaPregunta);
				fp.doLayout();
			}
		
		}
		
	},
	generarCampos: function(fp, afiliado, numPreguntas, oldNumPreg){
		var i = 0;
		if(oldNumPreg>0){
			i = Number(oldNumPreg)+1;
		}
		
		for(var i; i<=numPreguntas; i++){
			if(i==0){
				var filaPregunta = new Ext.Panel({
					id: 'pnlPreg'+i+afiliado,
					identificador: 'renglon',
					identificador2: 'renglonPregunta',
					frame: false,
					layout:'table',
					//width: 600,
					layoutConfig: {columns:4},
					indiceObj: i,
					items: [
						{
							xtype:'panel',
							frame: false,
							width:360,
							//height: (i==1)?100:60,
							layout:'hbox',
							title:'<p align="center">Preguntas<br><br></p>'
						},
						{
							xtype:'panel',
							frame: false,
							width:150,
							//height: (i==1)?100:60,
							layout:'anchor',
							border: false,
							style: 'margin:0 auto;',
							title:'<p align="center">Tipo de Pregunta <br><br></p>'
						},
						{
							xtype:'panel',
							frame: false,
							width:150,
							//height: (i==1)?100:60,
							layout:'anchor',
							border: false,
							style: 'margin:0 auto;',
							title:'<p align="center">Tipo de Respuesta<br><br></p>'
						},
						{
							xtype:'panel',
							frame: false,
							width:100,
							//height: (i==1)?100:60,
							title:'<p align="center">N�mero de<br>opciones</p>',
							style: 'margin:0 auto;'
						}
					]
				});
				
				//fp = Ext.getCmp('fpPreguntasEnc'+this.tipoAfiliado)
				fp.add(filaPregunta);
				fp.doLayout();
			}else{
				var filaPregunta = new Ext.Panel({
					id: 'pnlPreg'+i+afiliado,
					identificador: 'renglon',
					identificador2: 'renglonPregunta',
					frame: false,
					layout:'table',
					//width: 600,
					layoutConfig: {columns:4},
					indiceObj: i,
					items: [
						{
							xtype:'panel',
							frame: true,
							width:360,
							height: 60,
							layout:'hbox',
							//title:(i==1)?'<p align="center">Preguntas<br><br></p>':'',
							title:'',
							items:[
								{
									xtype: 'displayfield',
									value: i,
									width: 20
								},
								{
									xtype: 'hidden',
									id: 'fpNumPreg1'+i+afiliado,
									name: 'fpNumPreg'+i+afiliado,
									value: i
								},
								{
									xtype: 'textarea',
									id: 'fpPregunta1'+i+afiliado,
									name: 'fpPregunta'+i+afiliado,
									fieldLabel: i,
									allowBlank: false,
									maxLength: 300,
									height: 50,
									width: 320
								}]
						},
						{
							xtype:'panel',
							frame: true,
							width:150,
							height: 60,
							layout:'anchor',
							border: false,
							style: 'margin:0 auto;',
							title:'',
							items:[
										{
											xtype:			'combo',
											id:				'fpTipoPreg1'+i+afiliado,
											name:				'fpTipoPreg'+i+afiliado,
											hiddenName:		'fpTipoPreg'+i+afiliado,
											fieldLabel:		'',
											style: 'margin:0 auto;',
											emptyText:		'Seleccionar. . .',
											mode:				'local',
											displayField:	'descripcion',
											valueField:		'clave',
											forceSelection:true,
											typeAhead:		true,
											triggerAction:	'all',
											minChars:		1,
											allowBlank: false,
											anchor: '100%',
											consecutivo: i,
											value: 'N',
											store: new Ext.data.ArrayStore({
															  id:	'storeCatTipoPreg'+i+afiliado,
															  consecutivo: i,
															  fields: [
																	'clave',
																	'descripcion'
															  ],
															  data: [['N', 'Normal'], ['E', 'Excluyente']]
														}),
											listeners:{
												change: function(obj, newVal, oldVal){
													var tipoResp = Ext.getCmp('fpTipoResp1'+obj.consecutivo+afiliado);
													var numResp = Ext.getCmp('fpNumOpc1'+obj.consecutivo+afiliado);
													if(newVal=='E'){
														tipoResp.setValue(3);
														numResp.setValue(2);
													}
												}
											}
										}
									]
						},
						{
							xtype:'panel',
							frame: true,
							width:150,
							height: 60,
							layout:'anchor',
							border: false,
							style: 'margin:0 auto;',
							//title:(i==1)?'<p align="center">Tipo de Respuesta<br><br></p>':'',
							title:'',
							items:[
										{
											xtype:			'combo',
											id:				'fpTipoResp1'+i+afiliado,
											name:				'fpTipoResp'+i+afiliado,
											hiddenName:		'fpTipoResp'+i+afiliado,
											fieldLabel:		'',
											style: 'margin:0 auto;',
											emptyText:		'Seleccionar. . .',
											mode:				'local',
											displayField:	'descripcion',
											valueField:		'clave',
											forceSelection:true,
											typeAhead:		true,
											triggerAction:	'all',
											minChars:		1,
											allowBlank: false,
											anchor: '100%',
											consecutivo: i,
											store:			new Ext.data.JsonStore({
																id:				'storeCatTipoResp'+i+afiliado,
																root:				'registros',
																fields:			['clave', 'descripcion', 'loadMsg'],
																url:				'15admencu01_ext.data.jsp',
																baseParams:		{	informacion:	'CatalogoTipoPregunta'	},
																totalProperty:	'total',
																autoLoad:		true,
																consecutivo: i,
																listeners: {
																	exception: NE.util.mostrarDataProxyError,
																	beforeload: NE.util.initMensajeCargaCombo,
																	load: function(store, records, option){
																		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
																			var catTipoResp = Ext.getCmp('fpTipoResp1'+store.consecutivo+afiliado);
																			if(catTipoResp.getValue()==''){
																				catTipoResp.setValue(1);
																			}
																		}
																  } 
																}
															}),
											tpl:NE.util.templateMensajeCargaCombo,
											listeners:{
												change: function(obj, newlVal, oldVal){
													var tipoPreg = Ext.getCmp('fpTipoPreg1'+obj.consecutivo+afiliado);
													var numResp = Ext.getCmp('fpNumOpc1'+obj.consecutivo+afiliado);
													var renglones = fp.find('identificador','renglon');
													
													if(newlVal==1){
														numResp.setValue(1);
													}
													if(newlVal==3){
														numResp.setValue(2);
													}
													if(newlVal!=3){
														tipoPreg.setValue('N');
													}
													
													for(var index=0; index<renglones.length; index++){
														if(renglones[index].id == 'pnlPreg'+obj.consecutivo+afiliado){
															if(newlVal==4){
																numResp.setValue(1);
																fp.insert(index, fp.agregaRubro(fp, i, afiliado, obj.consecutivo, 'pnlPreg'+obj.consecutivo+afiliado));
																fp.doLayout();
																i++;
																obj.setValue(1);
															}
														}
													}; 
												}
											}
										}
									]
						},
						{
							xtype:'panel',
							frame: true,
							width:100,
							height: 60,
							//title:(i==1)?'<p align="center">N�mero de<br>opciones</p>':'',
							title:'',
							style: 'margin:0 auto;',
							items:[
								{
									xtype: 'numberfield',
									id: 'fpNumOpc1'+i+afiliado,
									name: 'fpNumOpc'+i+afiliado,
									allowBlank: false,
									allowDecimals: false,
									value: 1,
									maxLength:2,
									autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '2'},
									width:20,
									consecutivo: i,
									listeners:{
										blur: function(obj){
											var vTipoResp = Ext.getCmp('fpTipoResp1'+obj.consecutivo+afiliado).getValue();
											
											if(vTipoResp=='1' && (obj.getValue()=='' || Number(obj.getValue()))>1){
												Ext.MessageBox.alert('Aviso', 'El n�mero m�ximo de opciones deber� ser 1 para el tipo de respuesta Libre', function(){
													obj.setValue(1);
												});
											}else if(vTipoResp=='3' && (obj.getValue()=='' || Number(obj.getValue()))<2){
												Ext.MessageBox.alert('Aviso', 'El n�mero m�nimo de opciones deber� ser 2 para el tipo de respuesta Opci�n Excluyente', function(){
													obj.setValue(2);
												});
												
											}else if(obj.getValue()=='' || Number(obj.getValue())<=0){
												obj.setValue(1);
											}
											
										}
									}
								}
							]
						}
					]
				});
				
				//fp = Ext.getCmp('fpPreguntasEnc'+this.tipoAfiliado)
				fp.add(filaPregunta);
				fp.doLayout();
			}
		}
	},
	agregaRubro: function(fp, indice, afiliado, iRenglon, idRenglonAsoc){
		var modif = fp.modif;
		//indice++;
		var filaRubro = new Ext.Panel({
				id: 'pnlPreg'+indice+afiliado+modif,
				identificador: 'renglon',
				frame: false,
				layout:'table',
				//width: 600,
				layoutConfig: {columns: 5},
				preguntaAsoc: idRenglonAsoc,
				indiceObj: indice,
				items: [
					{
						xtype:'panel',
						frame: true,
						width: (modif=='s')?260:360,
						height: 40,
						layout:'hbox',
						title:'',
						items:[
							{
							xtype: 'displayfield',
							value: '',
							width: 20
							},
							{
								xtype: 'textfield',
								id: 'fpPregunta1'+indice+afiliado+modif,
								name: 'fpRubro'+iRenglon+afiliado+modif,
								fieldLabel: '',
								allowBlank: false,
								maxLength: 300,
								//height: 50,
								width: 320
							}]
					},
					{
						xtype:'panel',
						frame: true,
						width:150,
						height: 40,
						layout:'anchor',
						border: false,
						style: 'margin:0 auto;',
						title:(indice==1)?'<p align="center">Tipo de Pregunta <br><br></p>':'',
						items:[
									{
							xtype: 'displayfield',
							value: ' '
							}
								]
					},
					{
						xtype:'panel',
						frame: true,
						width:150,
						height: 40,
						layout:'hbox',
						border: false,
						style: 'margin:0 auto;',
						title:'',
						items:[
									{
										xtype:			'textfield',
										id:				'fpTextResp1'+indice+afiliado+modif,
										name:				'fpTextResp1'+indice+afiliado+modif,
										//style: 'margin:0 auto;',
										value: 'Rubro',
										allowBlank: false,
										readOnly: true,
										anchor: '100%',
										consecutivo: indice
									},
									{
										xtype: 'hidden',
										id: 'fpTipoResp1'+indice+afiliado+modif,
										name: 'fpTipoResp'+indice+afiliado+modif,
										value: 4
									}
								]
					},
					{
						xtype:'panel',
						frame: true,
						width:(modif=='s')?100:150,
						height: 40,
						title: '',
						style: 'margin:0 auto;',
						items:[
							/*{
								xtype: 'hiddenfield',
								id: 'fpNumOpc1'+indice+afiliado,
								name: 'fpNumOpc'+indice+afiliado,
								allowBlank: true,
								disabled: true,
								allowDecimals: false,
								width:20
							},*/
							{
								xtype:'button',
								name:'btnEliminaReg'+indice+afiliado+modif,
								iconCls: 'borrar',
								width:23,
								handler: function(){
									fp.remove(Ext.getCmp('pnlPreg'+indice+afiliado+modif));
									fp.doLayout();
								}
							}
						]
					},
					{
						xtype:'panel',
						frame: true,
						width:100,
						height: 40,
						layout:'anchor',
						border: false,
						hidden: (fp.esPlantilla=='S')?false:true,
						style: 'margin:0 auto;',
						title: '',
						items:[
									{
							xtype: 'displayfield',
							value: ' '
							}
								]
					}
				]
			});
			return filaRubro;
	},
	generarBotones: function(fpa,afiliado){
		var afiliado = fpa.tipoAfiliado;
		var modif = fpa.modif;
		var esPlantilla = fpa.esPlantilla;
		
		return [
				{
					text: 'Siguiente',
					id: 'btnSiguientePreg'+afiliado,
					formBind: true
				},
				{
					text: 'Cancelar',
					id: 'btnCancelarPreg'+afiliado
				},
				{
					text: 'Eliminar',
					id: 'btnEliminarPreg'+afiliado,
					handler: function(){
						//var fpPreg = Ext.getCmp('fpPreguntasEncLs');
						var arrayRenglones = fpa.find('identificador2','renglonPregunta');
						var contFila = 0;
						for(var r=1; r<arrayRenglones.length;r++){
							var indice = arrayRenglones[r].indiceObj;
							var idRubros = arrayRenglones[r].id;
							//var arrayRubro = fpPreguntas.find('preguntaAsoc',idRubros);
							
							var objSeleccion = arrayRenglones[r].find('id','fpSelecccion1'+indice+afiliado+modif);
							var objNumPregFila = arrayRenglones[r].find('id','numPregFila'+indice+afiliado+modif);
							var objfpNumPreg = arrayRenglones[r].find('id','fpNumPreg1'+indice+afiliado+modif);
							
							//alert(objfpNumPreg[0].getValue());
							
							if(objSeleccion[0].checked){
								arrayRenglones[r].destroy();
								var numPregunstas = Ext.getCmp('fp_numPreguntas_m1L').getValue();
								Ext.getCmp('fp_numPreguntas_m1L').setValue(Number(numPregunstas)-1);
							}else{
								contFila++;
								objNumPregFila[0].setValue(contFila);
							}
						}
					}
				}
			]
	},
	setHandlerBtnSiguiente: function(fn){
		var btnLoginAcept = Ext.getCmp('btnSiguientePreg'+this.tipoAfiliado)
			btnLoginAcept.setHandler(fn);
	},
	setHandlerBtnCancelar: function(fn){
		var btnLoginAcept = Ext.getCmp('btnCancelarPreg'+this.tipoAfiliado)
			btnLoginAcept.setHandler(fn);
	}
});



NE.encuestas.FormRespEnc = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	formaPreguntas: null,
	objDataResp: null,
	//numOjetos: 1,
	numPregReal: 0,
	esPlantilla: 'N',
	modif: '',
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpRespEnc'+this.tipoAfiliado,
			width: 810,
			height:400,
			title: '<p align="center">Configurador de respuestas</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			//bodyStyle: 'padding-left:5px;,padding-top:10px;,padding:6px;,text-align:left',
			//baseCls:'x-plain',
			defaultType: 'textarea',
			autoScroll: true,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			buttons: this.generarBotones(this, this.tipoAfiliado),
			//items : this.generarCampos(this, this.tipoAfiliado, this.numeroPreguntas),
			listeners:{
				afterrender: function(obj){
					//alert(obj.generarCampos);
					obj.generarCampos(this.formaPreguntas, obj, obj.tipoAfiliado);
					//obj.generarBotones(obj, obj.tipoAfiliado);
				}
			}
			//buttons: this.generarBotones(this, this.tipoAfiliado)
		});
		
		NE.encuestas.FormRespEnc.superclass.initComponent.call(this);
	},
	generarCampos: function(fpPreguntas, fpResp, afiliado){
		var objFilaResp = null;
		var fieldRubroInicial = null;
		var fieldRubroActual = null;
		var modif = fpResp.modif;
		var objResp = fpResp.objDataResp
		var arrayRenglones = fpPreguntas.find('identificador2','renglonPregunta');
		
		
		fpResp.numPregReal = arrayRenglones.length;
		var totalPreg = Number(fpResp.numPregReal)-1;
		
		//alert(arrayRenglones.length);
		//var indResp = 0;
		for(var r=1; r<arrayRenglones.length;r++){
			fieldRubroInicial = null;
			fieldRubroActual = null;
			var indice = arrayRenglones[r].indiceObj;
			var idRubros = arrayRenglones[r].id;
			var arrayRubro = fpPreguntas.find('preguntaAsoc',idRubros);
			
			//rubros--------------------------------------------------------------
			if(arrayRubro && arrayRubro.length >0){
				
				for(var y =0; y<arrayRubro.length; y++){
					var fieldRubroExtra = null
					var indiceR = arrayRubro[y].indiceObj;
					var objPreguntaR = arrayRubro[y].find('id','fpPregunta1'+indiceR+afiliado+modif);
					var objTipoRespR = arrayRubro[y].find('id','fpTipoResp1'+indiceR+afiliado+modif);
					
					if(y==0){
						fieldRubroInicial = new Ext.form.FieldSet({
												title: objPreguntaR[0].getValue(),
												collapsible: true,
												autoHeight:true,
												defaultType: 'textfield'
										});
						fieldRubroActual = fieldRubroInicial;
					}else{
						fieldRubroExtra = new Ext.form.FieldSet({
													title: objPreguntaR[0].getValue(),
													collapsible: true,
													autoHeight:true,
													//defaults: {width: 210},
													defaultType: 'textfield'
											});
						fieldRubroActual.add(fieldRubroExtra);
						fieldRubroInicial = fieldRubroActual;
						fieldRubroActual = fieldRubroExtra;
					}

					
				}
				
			}
			
			//preguntas-----------------------------------------------------------
			
			var objNumPreg = arrayRenglones[r].find('id','fpNumPreg1'+indice+afiliado+modif);
			var objPregunta = arrayRenglones[r].find('id','fpPregunta1'+indice+afiliado+modif);
			var objTipoPreg = arrayRenglones[r].find('id','fpTipoPreg1'+indice+afiliado+modif);
			var objTipoResp = arrayRenglones[r].find('id','fpTipoResp1'+indice+afiliado+modif);
			var objNumOpc = arrayRenglones[r].find('id','fpNumOpc1'+indice+afiliado+modif);
			
			var objNumPregF = arrayRenglones[r].find('id','numPregFila'+indice+afiliado+modif);
			
			var nump = (fpPreguntas.esPlantilla=='S')?objNumPregF[0].getValue():objNumPreg[0].getValue();
			
			
			var fieldPregunta = new Ext.form.FieldSet({
						title: nump+') '+objPregunta[0].getValue(),
						collapsible: true,
						autoHeight:true,
						labelWidth: (objTipoResp[0].getValue()==5)?110:10,
						defaultType: 'textfield',
						items:[
							{xtype:'hidden', name: objNumPreg[0].getValue()+'_numPreg_'+afiliado, value:nump },
							{xtype:'hidden', name: objNumPreg[0].getValue()+'_numOpc_'+afiliado, value:objNumOpc[0].getValue()},
							{xtype:'hidden', name: objNumPreg[0].getValue()+'_tipoResp_'+afiliado, value:objTipoResp[0].getValue()}
						]
				});
			//se genera fila 
			
			var nTipoPreg = objTipoPreg[0].getValue();
			var oTipoPreg = objTipoPreg[0].valueIni;
			
			var nTipoResp = Number(objTipoResp[0].getValue());
			var oTipoResp = Number(objTipoResp[0].valueIni);
			
			var nNumOpciones = Number(objNumOpc[0].getValue());
			var oNumOpciones = Number(objNumOpc[0].valueIni);
			
			var vnumPreg = objNumPreg[0].getValue();
			var existCambio = false;
			

			if(nTipoPreg!=oTipoPreg || nTipoResp!=oTipoResp || nNumOpciones!=oNumOpciones){
				existCambio = true;
			}
			
			var imagen = '';
			if(nTipoResp=='2')imagen ='<div class="icoCheck" style="float:left;width:15px;">&#160</div>';
			if(nTipoResp=='3')imagen ='<div class="icoRadio" style="float:left;width:15px;">&#160</div>';
			for(var z=0; z<nNumOpciones; z++){
				var objData=null;;
				if(modif == 's' && !existCambio){
					objData = eval("objResp.p"+vnumPreg);
					objData = objData[z];
					//indResp++;
				}
			
				if(objTipoResp[0].getValue()==3  && objTipoPreg[0].getValue()=='E'){
					fieldPregunta.add([
						{
							xtype: 'compositefield',
							fieldLabel: '',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'displayfield',
									value: imagen,
									width: 20
								},
								{
									xtype: 'textfield',
									id:  objNumPreg[0].getValue()+'_resp1_'+z+afiliado,
									name:  objNumPreg[0].getValue()+'_resp_'+z+afiliado,
									maxLength:300,
									width: '400',
									value: ((objData==null)?'':objData.CGRESPUESTA),
									allowBlank: false,
									regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
									regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
									
								},
								{
									xtype: 'displayfield',
									value: 'Preguntas Excluyentes:',
									width: 150
								},
								{
									xtype: 'textfield',
									id:  objNumPreg[0].getValue()+'_pregExclu1_'+z+afiliado,
									name:  objNumPreg[0].getValue()+'_pregExclu_'+z+afiliado,
									maxLength:7,
									width: '50',
									value: ((objData==null)?'':objData.CGEXCLUTENTE),
									allowBlank: true,
									regexText:"Favor de ingresar correctamente el rango de preguntas. Rangos correctos: [4], [1-4] ",
									regex: /(^(((\d){1,3}))((-){1}(\d){1,3})$)|(^((\d){1,3})$)/,
									autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7'},
									indice: indice,
									listeners:{
										change: function(obj, newVal, oldVal){
											if(obj.isValid()){
												if(oldVal!=newVal && newVal!=''){
														var index = newVal.search("-");
														var rang1=0;
														var rang2=0;
														
														if(index != -1){
															rang1 = newVal.substring(0,index);
															rang2 = newVal.substring(index+1);
															if(Number(rang1)<=obj.indice){
																Ext.Msg.alert('Validaci�n','S�lo se permite parametrizar preguntas posteriores a la Pregunta Excluyente', function(){
																	obj.setValue('');
																	obj.focus();
																});
															}else															
															if(Number(rang1)>Number(rang2)){
																Ext.Msg.alert('Validaci�n','N�mero de pregunta inicial tiene que ser menor a la final', function(){
																	obj.setValue('');
																	obj.focus();
																});
															}else
															if(Number(rang1)>totalPreg){
																Ext.Msg.alert('Validaci�n','N�mero de pregunta ('+rang1+') no existe', function(){
																	obj.setValue('');
																	obj.focus();
																});
															}else
															if(Number(rang2)>totalPreg){
																Ext.Msg.alert('Validaci�n','N�mero de pregunta ('+rang2+') no existe', function(){
																	obj.setValue('');
																	obj.focus();
																});
															}else{
																for(var w=rang1; w<=rang2; w++){
																	var objTipoPregInt = arrayRenglones[w].find('id','fpTipoPreg1'+arrayRenglones[w].indiceObj+afiliado+modif);
																	if(objTipoPregInt[0].getValue()=='E'){
																		Ext.Msg.alert('Validaci�n','Favor de verificar est� excluyendo preguntas parametrizadas como Respuestas Excluyentes('+arrayRenglones[w].indiceObj+')', function(){
																			obj.setValue('');
																			obj.focus();
																		});
																	}
																}
															
															}
															
														}else{
															rang1 = newVal;
															if(Number(rang1)<=obj.indice){
																Ext.Msg.alert('Validaci�n','S�lo se permite parametrizar preguntas posteriores a la Pregunta Excluyente', function(){
																	obj.setValue('');
																	obj.focus();
																});
															}else
															if(Number(rang1)>totalPreg){
																Ext.Msg.alert('Validaci�n','N�mero de pregunta ('+rang1+') no existe', function(){
																	obj.setValue('');
																	obj.focus();
																});
															}else{
																//for(var w=rang1; w<=rang2; w++){
																	var w=rang1;
																	var objTipoPregInt = arrayRenglones[w].find('id','fpTipoPreg1'+arrayRenglones[w].indiceObj+afiliado+modif);
																	if(objTipoPregInt[0].getValue()=='E'){
																		Ext.Msg.alert('Validaci�n','Favor de verificar est� excluyendo preguntas parametrizadas como Respuestas Excluyentes('+arrayRenglones[w].indiceObj+')', function(){
																			obj.setValue('');
																			obj.focus();
																		});
																	}
																//}
															
															}
														}
												}
											}
										}
									}
								}
							]
						}
					]);
							
					
				}else{
					//alert(objTipoResp[0].getValue());
					if(objTipoResp[0].getValue()==5 ){
						if(z==0){
							fieldPregunta.add(new Ext.form.NumberField({
								id:  objNumPreg[0].getValue()+'_pond1_'+z+afiliado,
								name:  objNumPreg[0].getValue()+'_pond_'+z+afiliado,
								fieldLabel: 'Ponderaci�n hasta',
								maxLength:4,
								width: '30',
								value: ((objData==null)?'':objData.IGPONDERACION),
								allowDecimal: false,
								allowBlank: false,
								autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '4'}
							}));
						}
					}
					fieldPregunta.add(
						[
						{
							xtype: 'compositefield',
							fieldLabel: '',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'displayfield',
									value: imagen,
									width: (nTipoResp=='2' || nTipoResp=='3')?20:0
								},
								{
									xtype: 'textfield',
									id:  objNumPreg[0].getValue()+'_resp1_'+z+afiliado,
									name:  objNumPreg[0].getValue()+'_resp_'+z+afiliado,
									maxLength:300,
									width: '400',
									value: (nTipoResp=='1'?'Respuesta abierta':(objData==null?'':objData.CGRESPUESTA)),
									allowBlank: (nTipoResp=='1')?true:false,
									disabled: (nTipoResp=='1')?true:false,
									regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
									regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
								}
							]
						}
					]
					);
				}
			}
			fieldPregunta.doLayout();
			
			
			
			objFilaResp =  new Ext.Panel({
				id: 'pnlResp'+r+afiliado,
				identificador: 'renglon',
				frame: false
				//layout:'table',
				//width: 600,
			});
			
			
			
			if(fieldRubroInicial!=null){
				fieldRubroActual.add(fieldPregunta);
				fieldRubroActual.doLayout();
				
				objFilaResp.add(fieldRubroInicial);
				objFilaResp.doLayout();
				
				fpResp.add(objFilaResp);
				fpResp.doLayout();
			}else{
				objFilaResp.add(fieldPregunta);
				objFilaResp.doLayout();
				
				fpResp.add(objFilaResp);
				fpResp.doLayout();
			
			}
		}
		
	},
	generarBotones: function(fpa,afiliado){
		return [
				{
					text: 'Vista Previa',
					id: 'btnVistaPrevia'+afiliado,
					formBind: true
				},
				{
					text: 'Terminar',
					id: 'btnTerminarResp'+afiliado,
					formBind: true
				},
				{
					text: 'Cancelar',
					id: 'btnCancelarResp'+afiliado
				},
				{
					text: 'Guardar Como',
					hidden: (fpa.esPlantilla=='S')?false:true,
					formBind: true,
					id: 'btnGuardComoResp'+afiliado
				}
			]
	},
	setHandlerBtnSiguiente: function(fn){
		var btnTerminar = Ext.getCmp('btnTerminarResp'+this.tipoAfiliado)
			btnTerminar.setHandler(fn);
	},
	setHandlerBtnCancelar: function(fn){
		var btnCancelar = Ext.getCmp('btnCancelarResp'+this.tipoAfiliado)
			btnCancelar.setHandler(fn);
	},
	setHandlerBtnVistaPrevia: function(fn){
		var btnVista = Ext.getCmp('btnVistaPrevia'+this.tipoAfiliado)
			btnVista.setHandler(fn);
	},
	setHandlerBtnGuardarComo: function(fn){
		var btnGuardarComo = Ext.getCmp('btnGuardComoResp'+this.tipoAfiliado)
			btnGuardarComo.setHandler(fn);
	}
});


NE.encuestas.FormVistaPrevEnc = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	formaPreguntas: null,
	formaRespuestas: null,
	titulo: null,
	objDataResp: null,
	//numOjetos: 1,
	numPregReal: 0,
	modif: '',
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpVistaprevEnc'+this.tipoAfiliado,
			width: 810,
			height:400,
			title: this.titulo,
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,padding:6px;,text-align:left',
			autoScroll: true,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			//buttons: this.generarBotones(this, this.tipoAfiliado),
			listeners:{
				afterrender: function(obj){
					obj.generarCampos(this.formaPreguntas, obj, obj.tipoAfiliado);
				}
			}
		});
		
		NE.encuestas.FormVistaPrevEnc.superclass.initComponent.call(this);
	},
	generarCampos: function(fpPreguntas, fpResp, afiliado){
		var objFilaResp = null;
		var fieldRubroInicial = null;
		var fieldRubroActual = null;
		var modif = fpResp.modif;
		var arrayRenglones = fpPreguntas.find('identificador2','renglonPregunta');
		
		fpResp.numPregReal = arrayRenglones.length;
		var totalPreg = Number(fpResp.numPregReal)-1;
		
		for(var r=1; r<arrayRenglones.length;r++){
			fieldRubroInicial = null;
			fieldRubroActual = null;
			var indice = arrayRenglones[r].indiceObj;
			var idRubros = arrayRenglones[r].id;
			var arrayRubro = fpPreguntas.find('preguntaAsoc',idRubros);
			
			//rubros--------------------------------------------------------------
			if(arrayRubro && arrayRubro.length >0){
				
				for(var y =0; y<arrayRubro.length; y++){
					var fieldRubroExtra = null
					var indiceR = arrayRubro[y].indiceObj;
					var objPreguntaR = arrayRubro[y].find('id','fpPregunta1'+indiceR+afiliado+modif);
					var objTipoRespR = arrayRubro[y].find('id','fpTipoResp1'+indiceR+afiliado+modif);
					
					if(y==0){
						fieldRubroInicial = new Ext.form.FieldSet({
												title: objPreguntaR[0].getValue(),
												collapsible: true,
												autoHeight:true,
												defaultType: 'textfield'
										});
						fieldRubroActual = fieldRubroInicial;
					}else{
						fieldRubroExtra = new Ext.form.FieldSet({
													title: objPreguntaR[0].getValue(),
													collapsible: true,
													autoHeight:true,
													//defaults: {width: 210},
													defaultType: 'textfield'
											});
						fieldRubroActual.add(fieldRubroExtra);
						fieldRubroInicial = fieldRubroActual;
						fieldRubroActual = fieldRubroExtra;
					}
				}
			}
			
			//preguntas-----------------------------------------------------------
			
			var objNumPreg = arrayRenglones[r].find('id','fpNumPreg1'+indice+afiliado+modif);
			var objPregunta = arrayRenglones[r].find('id','fpPregunta1'+indice+afiliado+modif);
			var objTipoPreg = arrayRenglones[r].find('id','fpTipoPreg1'+indice+afiliado+modif);
			var objTipoResp = arrayRenglones[r].find('id','fpTipoResp1'+indice+afiliado+modif);
			var objNumOpc = arrayRenglones[r].find('id','fpNumOpc1'+indice+afiliado+modif);
			
			//se genera fila 
			var nTipoPreg = objTipoPreg[0].getValue();
			var oTipoPreg = objTipoPreg[0].valueIni;
			
			var nTipoResp = Number(objTipoResp[0].getValue());
			var oTipoResp = Number(objTipoResp[0].valueIni);
			
			var nNumOpciones = Number(objNumOpc[0].getValue());
			var oNumOpciones = Number(objNumOpc[0].valueIni);
			
			var vnumPreg = objNumPreg[0].getValue();
			
			var fieldPregunta = new Ext.form.FieldSet({
					title: objNumPreg[0].getValue()+') '+objPregunta[0].getValue(),
					collapsible: true,
					autoHeight:true,
					//labelWidth: 110,
					layout: (nTipoResp=='2' || nTipoResp=='3')?'table':'form',
					layoutConfig: {columns:2}
			});
			
			
			for(var z=0; z<nNumOpciones; z++){

				if(objTipoResp[0].getValue()==3 || objTipoResp[0].getValue()==2){
					
					//objNumPreg[0].getValue()+'_resp1_'+z+afiliado
					//objNumPreg[0].getValue()+'_pregExclu1_'+z+afiliado
					var valorRadio = Ext.getCmp(objNumPreg[0].getValue()+'_resp1_'+z+afiliado);
					
					//if(z==0){
						if(objTipoResp[0].getValue()==3){
							var valorExclu = '';
							if(nTipoPreg=='E'){
								valorExclu = Ext.getCmp(objNumPreg[0].getValue()+'_pregExclu1_'+z+afiliado).getValue();
							}
							
							fieldPregunta.add(
								new Ext.Panel({
										frame:false,
										autoHeight:true,
										//layout: 'form',
										labelWidth: 50,
										title:'',
										items:							
											[
												{
													xtype: 'radio',
													id: objNumPreg[0].getValue()+'radioGrupo+'+z+afiliado,
													name: 'radioGrupo',
													width:300,
													boxLabel: valorRadio.getValue(),
													valExcluyente: valorExclu,
													listeners:{
														check:function(obj, checked){
																if(checked){
																		//se habilitaan todas las respuestas
																		for(var w=1; w<=totalPreg; w++){
																			var respPrev = Ext.getCmp('pnlResp'+w+afiliado);
																			respPrev.enable();
																		}
																		//se inhabitan respuestas si existe un rango capturado de preguntas excluyentes
																		if(obj.valExcluyente!=''){
																				var index = (obj.valExcluyente).search("-");
																				var rang1=0;
																				var rang2=0;
																				
																				if(index != -1){
																					rang1 = (obj.valExcluyente).substring(0,index);
																					rang2 = (obj.valExcluyente).substring(index+1);
																					
																					for(var w=rang1; w<=rang2; w++){
																						var respPrev = Ext.getCmp('pnlResp'+w+afiliado);
																						respPrev.disable();
																					}

																					
																				}else{
																					rang1 = obj.valExcluyente;
																					var w=rang1;
																					var respPrev = Ext.getCmp('pnlResp'+w+afiliado);
																					respPrev.disable();
																				}
																		}
																}
															
														}
													}
													//columns: 2
											  }
											]
							})
							);
						}else if(objTipoResp[0].getValue()==2){
							fieldPregunta.add(
							new Ext.Panel({
										frame:false,
										autoHeight:true,
										title:'',
										//layout: 'form',
										labelWidth: 50,
										items:	[
												{
													xtype: 'checkbox',
													id: objNumPreg[0].getValue()+'checkGrupo',
													boxLabel: valorRadio.getValue(),
													width:300
													//columns: 2
											  }
											]
								})
							);
						}
					
				}else{
					
					var valorCheck = Ext.getCmp(objNumPreg[0].getValue()+'_resp1_'+z+afiliado);
					
					
					if(objTipoResp[0].getValue()==5 ){
						var valorSilder = Ext.getCmp(objNumPreg[0].getValue()+'_pond1_'+'0'+afiliado);
						
						/*
						Ext.define('AlwaysVisibleTip', {
							 extend: 'Ext.slider.Tip',
							 init: function(slider) {
										  var me = this;
										  me.callParent(arguments);
										  slider.removeListener('dragend', me.hide);
										  slider.on({
												scope: me,
												change: me.onSlide,
												afterrender: {
													 fn: function() {
														  me.onSlide(slider, null, slider.thumbs[0]);
													 },
													 delay: 100
												}
										  });
									 }
						});
						
						
						var slider = Ext.create('Ext.slider.Single', {
							 animate: false,
							 plugins: [Ext.create('AlwaysVisibleTip')],
							 width: 200,
							 value: 50,
							 increment: 10,
							 minValue: 0,
							 maxValue: 100
						});
						*/
						

						var timeSliderLabel = new Ext.form.Label({
							  id:'txtSliderx'+objNumPreg[0].getValue()+'_resp1_'+z+afiliado,
							  html: '0',
							  style: 'margin:3px 5px 0px 5px;'
						 });
						
						//var slider = new Ext.form.SliderField({
						var slider = new Ext.Slider({
							id:'sliderx'+objNumPreg[0].getValue()+'_resp1_'+z+afiliado,
							idTxt:'txtSliderx'+objNumPreg[0].getValue()+'_resp1_'+z+afiliado,
							fieldLabel: valorCheck.getValue(),
							value: 0,
							width:300,
							plugins: new Ext.slider.Tip({
								getText: function(thumb){
									 return String.format('{0}', thumb.value);
								}
						  }),
							listeners:{
								change:function(obj, newVal, oldVal){
									Ext.getCmp(obj.idTxt).setText(newVal);
								}
							}
						});
						
						slider.setMaxValue(valorSilder.getValue());
						
						fieldPregunta.add(
							new Ext.Panel({
										frame:false,
										autoHeight:true,
										title:'',
										layout: 'table',
										layoutConfig: {columns:2},
										labelWidth: 110,
										items:	
											[
												{
													xtype:'panel',
													layout:'form',
													items:[slider]
												},
												{
													xtype:'panel',
													layout:'form',
													frame:true,
													width:50,
													bodyStyle:'backgroundColor:white;',
													items:[timeSliderLabel]
												}
													
												
											]
							})
						);
					}else{
						fieldPregunta.add(
							new Ext.Panel({
										frame:false,
										autoHeight:true,
										title:'',
										layout: 'form',
										labelWidth: 110,
										items:
										[
											{
												xtype: 'textarea',
												fieldLabel: '',
												value: '',
												width:600
										  }
										]
							})
						);
					}
				}
			}
			fieldPregunta.doLayout();
			
			
			
			objFilaResp =  new Ext.Panel({
				id: 'pnlResp'+r+afiliado,
				identificador: 'renglon',
				frame: false
				//layout:'table',
				//width: 600,
			});
			
			
			
			if(fieldRubroInicial!=null){
				fieldRubroActual.add(fieldPregunta);
				fieldRubroActual.doLayout();
				
				objFilaResp.add(fieldRubroInicial);
				objFilaResp.doLayout();
				
				fpResp.add(objFilaResp);
				fpResp.doLayout();
			}else{
				objFilaResp.add(fieldPregunta);
				objFilaResp.doLayout();
				
				fpResp.add(objFilaResp);
				fpResp.doLayout();
			
			}
		}
	}
});


//--------------------------CONSULTAS-------------------------------------------

NE.encuestas.FormConsultaEncuestas = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpConsultaEncuestas'+this.tipoAfiliado,
			width: 700,
			//height:400,
			title: '<p align="center">Consulta Encuesta '+((this.tipoAfiliado=='I')?'IF':(this.tipoAfiliado=='P'?'PYME':'EPO'))+'</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',  
			defaultType: 'textfield',
			labelWidth: 150,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(this.tipoAfiliado),
			buttons: this.generarBotones(this.tipoAfiliado)
		});
		
		NE.encuestas.FormConsultaEncuestas.superclass.initComponent.call(this);
	},
	generarCampos: function(afiliado){
		return [
				
				{
					xtype:			'combo',
					id:				'fp_consTipoAfil1'+afiliado,
					name:				'fp_consTipoAfil'+afiliado,
					hiddenName:		'fp_consTipoAfil'+afiliado,
					fieldLabel:		'Tipo de Afiliado',
					emptyText:		'Seleccionar. . .',
					mode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					forceSelection:true,
					typeAhead:		true,
					triggerAction:	'all',
					minChars:		1,
					hidden: (afiliado=='P' || afiliado=='E')?false:true,
					store:			new Ext.data.JsonStore({
										id:				'storeConsTipoAfilData'+afiliado,
										root:				'registros',
										fields:			['clave', 'descripcion', 'loadMsg'],
										url:				'15admencu01_ext.data.jsp',
										baseParams:		{	informacion:	'CatalogoTipoAfil', tipoAfiliado: afiliado	},
										totalProperty:	'total',
										autoLoad:		true,
										listeners: {
											exception: NE.util.mostrarDataProxyError,
											beforeload: NE.util.initMensajeCargaCombo
										}
									}),
					tpl:				NE.util.templateMensajeCargaCombo
				},
				{
					xtype:			'combo',
					id:				'fp_consCadena1'+afiliado,
					name:				'fp_consCadena'+afiliado,
					hiddenName:		'fp_consCadena'+afiliado,
					fieldLabel:		'Cadena',
					emptyText:		'Seleccionar. . .',
					mode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					forceSelection:true,
					typeAhead:		true,
					triggerAction:	'all',
					minChars:		1,
					hidden: (afiliado=='P' || afiliado=='E')?false:true,
					store:			new Ext.data.JsonStore({
										id:				'storeConsCadenasData'+afiliado,
										root:				'registros',
										fields:			['clave', 'descripcion', 'loadMsg'],
										url:				'15admencu01_ext.data.jsp',
										baseParams:		{	informacion:	'CatalogoCadenas', tipoAfiliado: afiliado	},
										totalProperty:	'total',
										autoLoad:		true,
										listeners: {
											exception: NE.util.mostrarDataProxyError,
											beforeload: NE.util.initMensajeCargaCombo
										}
									}),
					tpl:				NE.util.templateMensajeCargaCombo
				},
				{
					xtype:			'combo',
					id:				'fp_consEncuesta1'+afiliado,
					name:				'fp_consEncuesta'+afiliado,
					hiddenName:		'fp_consEncuesta'+afiliado,
					fieldLabel:		'T�tulo de la Encuesta',
					emptyText:		'Seleccionar. . .',
					mode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					forceSelection:true,
					typeAhead:		true,
					triggerAction:	'all',
					minChars:		1,
					hidden: false,
					store:			new Ext.data.JsonStore({
										id:				'storeConsEncuestasData'+afiliado,
										root:				'registros',
										fields:			['clave', 'descripcion', 'loadMsg'],
										url:				'15admencu01_ext.data.jsp',
										baseParams:		{	informacion:	'CatalogoEncuestas', tipoAfiliado: afiliado},
										totalProperty:	'total',
										autoLoad:		true,
										listeners: {
											exception: NE.util.mostrarDataProxyError,
											beforeload: NE.util.initMensajeCargaCombo
										}
									}),
					tpl:				NE.util.templateMensajeCargaCombo
				},
				{
					xtype: 'compositefield',
					fieldLabel: 'Fecha de Publicaci�n',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:			'datefield',
							name:				'fp_cons_fec_desde'+afiliado,
							id:				'fp_cons_fec_desde1'+afiliado,
							//allowBlank:		false,
							startDay:		0,
							width:			90,
							msgTarget:		'side',
							vtype:			'rangofecha', 
							campoFinFecha:	'fp_cons_fec_hasta1'+afiliado,
							margins:			'0 20 0 0',  //necesario para mostrar el icono de error
							regexText:"La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
							regex: /.*[1-9][0-9]{3}$/
						},
						{
							xtype: 'displayfield',
							value: 'hasta:',
							width: 30
						},
						{
							xtype:				'datefield',
							name:					'fp_cons_fec_hasta'+afiliado,
							id:					'fp_cons_fec_hasta1'+afiliado,
							//allowBlank:			false,
							startDay:			1,
							width:				90,
							msgTarget:			'side',
							vtype:				'rangofecha', 
							campoInicioFecha:	'fp_cons_fec_desde1'+afiliado,
							margins:				'0 20 0 0',  //necesario para mostrar el icono de error
							regexText:"La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
							regex: /.*[1-9][0-9]{3}$/
							}
					]
				},
				{
					xtype: 'displayfield',
					value: ' ',
					width: 25
				}
			]
	},
	generarBotones: function(afiliado){
		return [
				{
					text: 'Consultar',
					id: 'btnConsConsultar'+afiliado
					//formBind: true
				}
				/*
				{
					text: 'Regresar',
					id: 'btnConsRegresar'+afiliado,
					formBind: true
				}
				*/
			]
	},
	setHandlerBtnConsultar: function(fn){
		var btnConsultar = Ext.getCmp('btnConsConsultar'+this.tipoAfiliado)
			btnConsultar.setHandler(fn);
	}
});


	var procesarSuccessFailureGuardar =  function(opts, success, response) {		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true)  {			
			Ext.MessageBox.alert('Cambios Guardados','La actualizaci�n ha sido realizada con �xito');			
				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	

NE.encuestas.GridEncuestas = Ext.extend(Ext.grid.EditorGridPanel,{
	tipoAfiliado: null,
	fnLoadStore:null,
	fnModificacion:null,
	//claveEncuesta: null,
	initComponent : function(){
	var storeGrid =  new Ext.data.JsonStore({
				url : '15admencu01_ext.data.jsp',
				id: 'storeEncuestasData1'+this.tipoAfiliado,
				baseParams:{
					informacion: 'consultaEncuestas'
				},
				root : 'registros',
				fields: [
					{name: 'CVEENCUESTA'},
					{name: 'TITULO'},
					{name: 'FECHAINI',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'FECHAFIN',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'OBLIGATORIO'},
					{name: 'NUMPREGUNTAS'},
					{name: 'NUMRESPUESTAS'},
					{name: 'INSTRUCCIONES'}					
				],
				totalProperty : 'total',
				messageProperty: 'msg',
				autoLoad: false,
				listeners: {
					load: this.fnLoadStore,
					exception: {
						fn: function(proxy, type, action, optionsRequest, response, args) {
							NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
							this.fnLoadStore(null, null, null);
						}
					}
				}
			});			
		Ext.apply(this, {
			id: 'gridEncuestas1'+this.tipoAfiliado,			
			style:	'margin:0 auto;',
			autoDestroy: true, 
			margins: '20 0 0 0',
			store:storeGrid,
			viewConfig: {
				templates: {
					cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
						'</td>'
					)
				}
			},
			columns: [
				{//1
					header: 'N�mero de Encuesta',
					tooltip: 'N�mero de Encuesta',
					dataIndex: 'CVEENCUESTA',
					sortable: true,
					width: 150,
					resizable: true,
					align: 'center',
					hidden: false
				},
				{//2
					header: '<p align="center">T�tulo</p>',
					tooltip: 'T�tulo',
					dataIndex: 'TITULO',
					sortable: true,
					hideable: false,
					width: 150,
					align: 'left',
					hidden: false
					//renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{
					header: 'Desde',
					tooltip: 'Desde',
					dataIndex: 'FECHAINI',
					sortable: true,
					width: 80,
					align: 'center',
					renderer: Ext.util.Format.dateRenderer('d/m/Y'),	
					hidden: false
				},
				{
					header: 'Hasta',
					tooltip: 'Hasta',
					dataIndex: 'FECHAFIN',
					sortable: true,
					width: 100,
					align: 'center',
					hidden: false,
					renderer: Ext.util.Format.dateRenderer('d/m/Y'),							
					editor: new Ext.form.DateField({
							startDay: 0
					})
				},
				{
					header: 'N�mero de preguntas',
					tooltip: 'N�mero de Preguntas',
					dataIndex: 'NUMPREGUNTAS',
					sortable: true,
					width: 80,
					align: 'center',
					hidden: false
				},
				{
					header: '<p align="center">Instrucciones</p>',
					tooltip: 'Instrucciones',
					dataIndex: 'INSTRUCCIONES',
					sortable: true,
					width: 150,
					align: 'left',
					hidden: false
				},
				{
					header: 'Obligatoria',
					tooltip: 'Obligatoria',
					dataIndex: 'OBLIGATORIO',
					sortable: true,
					width: 80,
					align: 'center',
					hidden: false
				},
				{
					xtype: 		'actioncolumn',
					header: 		'Seleccionar',
					tooltip: 	'Seleccionar',
					width: 		120,
					align:		'center',
					tipoAfil: this.tipoAfiliado,
					hidden: 		false,
					/*renderer: function(valor, metadata, registro, rowindex, colindex, store) {
						valor='Eliminar';
						return valor;
					},*/
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								this.items[0].tooltip = 'Eliminar';
								return 'borrar';
							},
							handler: function(grid, rowIndex, colIndex) {
								var registro = Ext.StoreMgr.key('storeEncuestasData1'+grid.tipoAfiliado).getAt(rowIndex);
								Ext.Msg.confirm("Mensaje de p�gina web","�Est� seguro que desea eliminar esta encuesta?",
								function(res){
									if(res=='no' || res == 'NO'){
										return;
									}else{
										Ext.Ajax.request({
											url: '15admencu01_ext.data.jsp',
											params:{
												informacion: 'BorrarEncuesta',
												cveEncuesta: registro.data['CVEENCUESTA']
											},
											callback: function(opts, success, response) {
												//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
												
												if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
													//gridEncuestasI.hide
													Ext.MessageBox.alert('aviso', 'La encuesta ha sido borrada', function(){
														grid.hide();
														window.location = '15admenc01_ext.jsp';	
													});
												
												} else {
													NE.util.mostrarConnError(response,opts);
												}
											}
										});
									}
								});
								
							}
						},
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								
								if(registro.data['NUMRESPUESTAS']!='0')
									return '';
								else{
									this.items[1].tooltip = 'Modificar';
									return 'modificar';
								}
							},
							handler: function(grid, rowIndex, colIndex) {
								var registro = (grid.getStore()).getAt(rowIndex);
								
								if(registro.data['NUMRESPUESTAS']=='0'){
									grid.fnModificacion(registro.data['CVEENCUESTA']);
								}
							}
						}
					]
				}
			],
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 340,
			width: 930,
			style: 'margin:0 auto;',
			title: '',
			frame: true,
			
			//*********************************************************************+
			bbar: {
				xtype: 'toolbar',
				items: [
					'->',
						{
							text: 'Guardar',
							handler: function(boton, evento) {
								var fecha_final = [];
								var ic_encuesta = [];
								var totalEr= 0;
								
								var modificados = storeGrid.getModifiedRecords();
								
								if (modificados.length > 0) {
									Ext.each(modificados, function(record) {	
										
										var fechaIni  = Ext.util.Format.date(record.data['FECHAINI'],'d/m/Y'); 	
										var fechaFinal  = Ext.util.Format.date(record.data['FECHAFIN'],'d/m/Y'); 	
										
										if (fechaIni!=''  && fechaFinal!='' ){
											var resultado = datecomp(fechaFinal, fechaIni);
													
											if (resultado==2)	{
												totalEr++;
												Ext.MessageBox.alert("Mensaje","La fecha  deber� ser mayor a la fecha de  Desde");
												return false;
											}else {
												fecha_final += Ext.util.Format.date(record.data['FECHAFIN'],'d/m/Y') +'|'; 				
												ic_encuesta +=record.data['CVEENCUESTA']+'|'; 	
											}
										}
																			
									});
								}
								if(totalEr==0)  {
									Ext.Ajax.request({
										url : '15admencu01_ext.data.jsp',
										params : {
											fecha_final : fecha_final,
											ic_encuesta: ic_encuesta,
											informacion: 'GuardaFechaFinal'										
										},
										callback: procesarSuccessFailureGuardar
									});
								
								}
							
							}
						}
				]
			}
		});
		
		NE.encuestas.GridEncuestas.superclass.initComponent.call(this);
	}
});


NE.encuestas.FormModificaEncuesta = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	claveEncuesta: null,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpModifiEncuestas'+this.tipoAfiliado,
			width: 810,
			collapsible: true,
			title: '<p align="center">Modificaci�n Encuesta</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',  
			defaultType: 'textfield',
			labelWidth: 150,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(this.tipoAfiliado)
			//buttons: this.generarBotones(this.tipoAfiliado)
		});
		
		NE.encuestas.FormModificaEncuesta.superclass.initComponent.call(this);
	},
	generarCampos: function(afiliado){
	
		return [
				{
					xtype:'textfield',
					name:'fp_titulo_m'+afiliado,
					id:'fp_titulo_m1'+afiliado,
					fieldLabel:'T�tulo',
					maxLength:80,
					anchor: '80%',
					allowBlank: false,
					regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
					regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
				},
				{
					xtype: 'compositefield',
					fieldLabel: 'Publicar Desde',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:			'datefield',
							name:				'fp_fec_desde_m'+afiliado,
							id:				'fp_fec_desde_m1'+afiliado,
							allowBlank:		false,
							startDay:		0,
							width:			100,
							msgTarget:		'side',
							vtype:			'rangofecha', 
							campoFinFecha:	'fp_fec_hasta_m1'+afiliado,
							margins:			'0 20 0 0',  //necesario para mostrar el icono de error
							regexText:"La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
							regex: /.*[1-9][0-9]{3}$/
						},
						{
							xtype: 'displayfield',
							value: 'hasta:',
							width: 30
						},
						{
							xtype:				'datefield',
							name:					'fp_fec_hasta_m'+afiliado,
							id:					'fp_fec_hasta_m1'+afiliado,
							allowBlank:			false,
							startDay:			1,
							width:				100,
							msgTarget:			'side',
							vtype:				'rangofecha', 
							campoInicioFecha:	'fp_fec_desde_m1'+afiliado,
							margins:				'0 20 0 0',  //necesario para mostrar el icono de error
							regexText:"La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
							regex: /.*[1-9][0-9]{3}$/
							}
					]
				},
				{
					xtype: 'textarea',
					id: 'fp_instrucciones_m1'+afiliado,
					name: 'fp_instrucciones_m'+afiliado,
					allowBlank: false,
					fieldLabel: "Instrucciones"
				},
				{
					xtype: 'compositefield',
					fieldLabel: '',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
						 xtype: 'radio',
						 fieldLabel: '',
						 boxLabel: 'Obligatoria',
						 name: 'fp_respObligada_m'+afiliado,
						 inputValue: 'S',
						 listeners:{
							check: function(obj, checked){
								if(checked){
									var objDias = Ext.getCmp('fp_diasResp_m1'+afiliado);
									objDias.enable();
									objDias.allowBlank= false;
								}
							}
						 }
						},
						{
							xtype: 'displayfield',
							value: '(Solicitar la respuesta ',
							width: 115
						},
						{
							xtype: 'numberfield',
							id: 'fp_diasResp_m1'+afiliado,
							name: 'fp_diasResp_m'+afiliado,
							allowDecimals: false,
							maxLength: 3,
							width:30
						},
						{
							xtype: 'displayfield',
							value: ' d�as h�biles previos al vencimiento)',
							width: 200
						}
					]
				},
				{
					xtype:'panel',
					layout: 'form',
					items:[
						{
							xtype: 'radio',
							fieldLabel: '',
							boxLabel: 'No obligatoria',
							labelSeparator: ':',
							name: 'fp_respObligada_m'+afiliado,
							inputValue: 'N',
							listeners: {
								check: function(obj, checked){
									if(checked){
										var objDias = Ext.getCmp('fp_diasResp_m1'+afiliado);
										objDias.setValue('');
										objDias.disable();
										objDias.allowBlank= true;
									}
								}	
							}
						}
					]
				}
				
			]
	}
});


NE.encuestas.FormConsultaPlantillas = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpConsultaPlantillas'+this.tipoAfiliado,
			width: 700,
			//height:400,
			title: '<p align="center">Consulta Plantillas</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',  
			defaultType: 'textfield',
			labelWidth: 150,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(this.tipoAfiliado),
			buttons: this.generarBotones(this.tipoAfiliado)
		});
		
		NE.encuestas.FormConsultaPlantillas.superclass.initComponent.call(this);
	},
	generarCampos: function(afiliado){
		return [
		
				{
					xtype:			'combo',
					id:				'fp_consPlantilla1'+afiliado,
					name:				'fp_consPlantilla'+afiliado,
					hiddenName:		'fp_consPlantilla'+afiliado,
					fieldLabel:		'T�tulo de la Encuesta',
					emptyText:		'Seleccionar. . .',
					mode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					forceSelection:true,
					typeAhead:		true,
					triggerAction:	'all',
					minChars:		1,
					hidden: false,
					store:			new Ext.data.JsonStore({
										id:				'storeConsPlantillasData'+afiliado,
										root:				'registros',
										fields:			['clave', 'descripcion', 'loadMsg'],
										url:				'15admencu01_ext.data.jsp',
										baseParams:		{	informacion:	'CatalogoPlantillas', tipoAfiliado: afiliado},
										totalProperty:	'total',
										autoLoad:		true,
										listeners: {
											exception: NE.util.mostrarDataProxyError,
											beforeload: NE.util.initMensajeCargaCombo
										}
									}),
					tpl:				NE.util.templateMensajeCargaCombo
				},
				{
					xtype: 'compositefield',
					fieldLabel: 'Fecha de Registro',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:			'datefield',
							name:				'fp_cons_fec_desde'+afiliado,
							id:				'fp_cons_fec_desde1'+afiliado,
							allowBlank:		false,
							startDay:		0,
							width:			90,
							msgTarget:		'side',
							vtype:			'rangofecha', 
							campoFinFecha:	'fp_cons_fec_hasta1'+afiliado,
							margins:			'0 20 0 0',  //necesario para mostrar el icono de error
							regexText:"La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
							regex: /.*[1-9][0-9]{3}$/
						},
						{
							xtype: 'displayfield',
							value: 'a',
							width: 25
						},
						{
							xtype:				'datefield',
							name:					'fp_cons_fec_hasta'+afiliado,
							id:					'fp_cons_fec_hasta1'+afiliado,
							allowBlank:			false,
							startDay:			1,
							width:				90,
							msgTarget:			'side',
							vtype:				'rangofecha', 
							campoInicioFecha:	'fp_cons_fec_desde1'+afiliado,
							margins:				'0 20 0 0',  //necesario para mostrar el icono de error
							regexText:"La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
							regex: /.*[1-9][0-9]{3}$/
							}
					]
				},
				{
					xtype: 'displayfield',
					value: ' ',
					width: 25
				}
			]
	},
	generarBotones: function(afiliado){
		return [
				{
					text: 'Consultar',
					id: 'btnConsConsultar'+afiliado
					//formBind: true
				}
				/*
				{
					text: 'Regresar',
					id: 'btnConsRegresar'+afiliado,
					formBind: true
				}
				*/
			]
	},
	setHandlerBtnConsultar: function(fn){
		var btnConsultar = Ext.getCmp('btnConsConsultar'+this.tipoAfiliado)
			btnConsultar.setHandler(fn);
	}
});


NE.encuestas.GridPlantillas = Ext.extend(Ext.grid.EditorGridPanel,{
	tipoAfiliado: null,
	fnLoadStore:null,
	fnModificacion:null,
	//claveEncuesta: null,
	initComponent : function(){
		Ext.apply(this, {
			id: 'gridPlantillas1'+this.tipoAfiliado,
			store: new Ext.data.JsonStore({
				url : '15admencu01_ext.data.jsp',
				id: 'storePlantillasData1'+this.tipoAfiliado,
				baseParams:{
					informacion: 'ConsultaPlantillas'
				},
				root : 'registros',
				fields: [
					{name: 'CVEPLANTILLA'},
					{name: 'TITULO'},
					{name: 'NUMPREGUNTAS'},
					{name: 'SELECCIONAR'},
					{name: 'INSTRUCCIONES'},
					{name: 'USUARIOALTA'},
					{name: 'FECPUBLIC'},
					{name: 'USUARIOMOD'},
					{name: 'FECMOD'}
					
				],
				totalProperty : 'total',
				messageProperty: 'msg',
				autoLoad: false,
				listeners: {
					load: this.fnLoadStore,
					exception: {
						fn: function(proxy, type, action, optionsRequest, response, args) {
							NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
							this.fnLoadStore(null, null, null);
						}
					}
				}
			}),
			style:	'margin:0 auto;',
			margins: '20 0 0 0',
			viewConfig: {
				templates: {
					cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
						'</td>'
					)
				}
			},
			columns: [
				{//1
					header: 'N�mero de Encuesta',
					tooltip: 'N�mero de Encuesta',
					dataIndex: 'CVEPLANTILLA',
					sortable: true,
					width: 150,
					resizable: true,
					align: 'center',
					hidden: false
				},
				{//2
					header: '<p align="center">T�tulo</p>',
					tooltip: 'T�tulo',
					dataIndex: 'TITULO',
					sortable: true,
					hideable: false,
					width: 150,
					align: 'left',
					hidden: false
					//renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{
					header: 'N�mero de preguntas',
					tooltip: 'N�mero de Preguntas',
					dataIndex: 'NUMPREGUNTAS',
					sortable: true,
					width: 80,
					align: 'center',
					hidden: false
				},
				{
					xtype: 		'actioncolumn',
					header: 		'Seleccionar',
					tooltip: 	'Seleccionar',
					width: 		120,
					align:		'center',
					tipoAfil: this.tipoAfiliado,
					hidden: 		false,
					/*renderer: function(valor, metadata, registro, rowindex, colindex, store) {
						valor='Eliminar';
						return valor;
					},*/
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								this.items[0].tooltip = 'Eliminar';
								return 'borrar';
							},
							handler: function(grid, rowIndex, colIndex) {
								var registro = Ext.StoreMgr.key('storePlantillasData1'+grid.tipoAfiliado).getAt(rowIndex);
								
								Ext.Ajax.request({
									url: '15admencu01_ext.data.jsp',
									params:{
										informacion: 'BorrarPlantilla',
										cveEncuesta: registro.data['CVEPLANTILLA']
									},
									callback: function(opts, success, response) {
										//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
										
										if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
											//gridEncuestasI.hide
											Ext.MessageBox.alert('aviso', 'La encuesta ha sido borrada', function(){
												grid.hide();
											});
										
										} else {
											NE.util.mostrarConnError(response,opts);
										}
									}
								});
								
							}
						},
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								this.items[1].tooltip = 'Modificar';
								return 'modificar';
							},
							handler: function(grid, rowIndex, colIndex) {
								var registro = (grid.getStore()).getAt(rowIndex);
								
								//if(registro.data['NUMRESPUESTAS']=='0'){
									grid.fnModificacion(registro.data['CVEPLANTILLA']);
								//}
							}
						}
					]
				},
				{
					header: '<p align="center">Instrucciones</p>',
					tooltip: 'Instrucciones',
					dataIndex: 'INSTRUCCIONES',
					sortable: true,
					width: 150,
					align: 'left',
					hidden: false
				},
				{
					header: '<p align="center">Usuario Alta</p>',
					tooltip: 'Usuario Alta',
					dataIndex: 'USUARIOALTA',
					sortable: true,
					width: 150,
					align: 'left',
					hidden: false
				},
				{
					header: 'Desde',
					tooltip: 'Desde',
					dataIndex: 'FECPUBLIC',
					sortable: true,
					width: 80,
					align: 'center',
					hidden: false
				},
				{
					header: '<p align="center">Usuario Modifico</p>',
					tooltip: 'Usuario Modifico',
					dataIndex: 'USUARIOMOD',
					sortable: true,
					width: 150,
					align: 'left',
					hidden: false
				},
				{
					header: 'Fecha Modifico',
					tooltip: 'Fecha Modifico',
					dataIndex: 'FECMOD',
					sortable: true,
					width: 80,
					align: 'center',
					hidden: false
				}
			],
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 340,
			width: 930,
			style: 'margin:0 auto;',
			title: '',
			frame: true
	
		});
		
		NE.encuestas.GridEncuestas.superclass.initComponent.call(this);
	}
});


//-------------------------RESPUESTAS-------------------------------------------

NE.encuestas.GridEncRespGral = Ext.extend(Ext.grid.GridPanel,{
	tipoAfiliado: null,
	fnLoadStore:null,
	//claveEncuesta: null,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'gridEncRespGral'+this.tipoAfiliado,
			store: new Ext.data.JsonStore({
				url : '15admencu01_ext.data.jsp',
				id: 'storeRespData1'+this.tipoAfiliado,
				baseParams:{
					informacion: 'ConsultaRespuestas'
				},
				root : 'registros',
				afiliado: this.tipoAfiliado,
				fields: [
					{name: 'CVEENCUESTA'},
					{name: 'FECPUBLIC'},
					{name: 'TITULO'},
					{name: 'CONTRESP'},
					{name: 'SELECCIONAR'},
					{name: 'NUMPREGUNTAS'}
				],
				totalProperty : 'total',
				messageProperty: 'msg',
				autoLoad: false,
				listeners: {
					load: this.fnLoadStore,
					exception: {
						fn: function(proxy, type, action, optionsRequest, response, args) {
							NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
							this.fnLoadStore(null, null, null);
						}
					}
				}
			}),
			style:	'margin:0 auto;',
			margins: '20 0 0 0',
			viewConfig: {
				templates: {
					cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
						'</td>'
					)
				}
			},
			columns: [
				{//1
					header: 'Fecha de publicaci�n',
					tooltip: 'Fecha de publicaci�n',
					dataIndex: 'FECPUBLIC',
					sortable: true,
					width: 150,
					resizable: true,
					align: 'center',
					hidden: false
				},
				{//2
					header: '<p align="center">T�tulo</p>',
					tooltip: 'T�tulo',
					dataIndex: 'TITULO',
					sortable: true,
					hideable: false,
					width: 300,
					align: 'left',
					hidden: false
					//renderer: Ext.util.Format.dateRenderer('d/m/Y')
				},
				{
					header: 'Respuestas Obtenidas',
					tooltip: 'Respuestas Obtenidas',
					dataIndex: 'CONTRESP',
					sortable: true,
					width: 100,
					align: 'center',
					hidden: false
				},
				{
					header: 'Seleccionar',
					tooltip: 'Seleccionar',
					dataIndex: 'CVEENCUESTA',
					sortable: true,
					width: 100,
					align: 'center',
					hidden: false,
					renderer:function(val, cell, record, rowIndex, colIndex, store){ 
						var retval = '<input type="radio" name="myRadioButton'+store.afiliado+'" value="'+val+'" onclick="asignaSeleccion(\''+val+'\', '+'\''+record.data["TITULO"]+'\', '+'\''+store.afiliado+'\')" >'; 
						return retval;
					 }
					 
					/*editor: {
						 xtype: 'radio',
						 name: 'radioResp'
						 /*enableKeyEvents: true,
						 listeners:{
							keypress: function(field, event){							
								var val = field.getValue();
								if(event.keyCode !=8 && event.keyCode !=46){//ignorar backspace and delete
									if (val.length >= 255 ){
										event.stopEvent();
									}
								}
							}
						 }
					},*/
				}
			],
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 340,
			width: 700,
			style: 'margin:0 auto;',
			title: '',
			frame: true,
			bbar: {
				xtype: 'toolbar',
				items: [
					'->',
					'-',
					{
						text: 'Ver Respuesta Individual',
						id: 'btnRespInd'+this.tipoAfiliado
					},
					'-',
					{
						text: 'Ver Respuesta Consolidada',
						id: 'btnRespCon'+this.tipoAfiliado
					}
				]
			}
		});
		
		NE.encuestas.GridEncRespGral.superclass.initComponent.call(this);
	},
	setHandlerBtnRespInd: function(fn){
		var btnRespInd = Ext.getCmp('btnRespInd'+this.tipoAfiliado)
			btnRespInd.setHandler(fn);
	},
	setHandlerBtnRespCon: function(fn){
		var btnRespCon = Ext.getCmp('btnRespCon'+this.tipoAfiliado)
			btnRespCon.setHandler(fn);
	}
});


NE.encuestas.FormRespInd = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	claveEncuesta: null,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpRespInd'+this.tipoAfiliado,
			width: 700,
			//height:400,
			title: '<p align="center">Respuesta Individual</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',  
			defaultType: 'textfield',
			labelWidth: 100,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(this.tipoAfiliado, this.claveEncuesta),
			buttons: this.generarBotones(this.tipoAfiliado)
		});
		
		NE.encuestas.FormRespInd.superclass.initComponent.call(this);
	},
	generarCampos: function(afiliado, cveEncuesta){
		var fieldLabel = "";
		if(afiliado=='I') fieldLabel = 'IF';
		if(afiliado=='P') fieldLabel = 'PYME';
		if(afiliado=='E') fieldLabel = 'EPO';
		
		return [
				
				{
					xtype:			'combo',
					id:				'fp_consAfiliado1'+afiliado,
					name:				'fp_consAfiliado'+afiliado,
					hiddenName:		'fp_consAfiliado'+afiliado,
					fieldLabel:		fieldLabel,
					emptyText:		'Seleccionar. . .',
					mode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					forceSelection:true,
					typeAhead:		true,
					triggerAction:	'all',
					allowBlank: false,
					minChars:		1,
					//hidden: (afiliado=='P' || afiliado=='E')?false:true,
					store:			new Ext.data.JsonStore({
										id:				'storeAfiliaIndData'+afiliado,
										root:				'registros',
										fields:			['clave', 'descripcion', 'loadMsg'],
										url:				'15admencu01_ext.data.jsp',
										baseParams:		{	informacion:	'CatalogoEncRespAfil', tipoAfiliado: afiliado, cveEncuesta:cveEncuesta	},
										totalProperty:	'total',
										autoLoad:		true,
										listeners: {
											exception: NE.util.mostrarDataProxyError,
											beforeload: NE.util.initMensajeCargaCombo
										}
									}),
					tpl:				NE.util.templateMensajeCargaCombo
				}
			]
	},
	generarBotones: function(afiliado){
		return [
				{
					text: 'Aceptar',
					id: 'btnAceptarRespInd'+afiliado,
					formBind: true
				},
				{
					text: 'Regresar',
					id: 'btnRegresarRespInd'+afiliado
					//formBind: true
				}
				
			]
	},
	setHandlerBtnAceptar: function(fn){
		var btnConsultar = Ext.getCmp('btnAceptarRespInd'+this.tipoAfiliado)
			btnConsultar.setHandler(fn);
	},
	setHandlerBtnRegresar: function(fn){
		var btnRegresar = Ext.getCmp('btnRegresarRespInd'+this.tipoAfiliado)
			btnRegresar.setHandler(fn);
	}
});


NE.encuestas.FormMuestraRespInd = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	dataRespInd: null,
	tituloEnc: '',
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpMuestraRespInd'+this.tipoAfiliado,
			width: 700,
			height:'350',
			title: this.tituloEnc,
			frame: true,   
			border: true, 
			autoScroll: true,
			//monitorValid: true,
			defaultType: 'displayfield',
			autoScroll: true,
			style: 'margin:0 auto;',
			buttons: this.generarBotones(this, this.tipoAfiliado),
			listeners:{
				afterrender: function(obj){
					obj.generarCampos(obj, obj.tipoAfiliado, obj.dataRespInd);
				}
			}
			//buttons: this.generarBotones(this, this.tipoAfiliado)
		});
		
		NE.encuestas.FormMuestraRespInd.superclass.initComponent.call(this);
	},
	generarCampos: function(fpMuestraResp, afiliado, dataRespInd){
		var pnlPreg = null;

		if(dataRespInd.length>0){
			for(var x=0; x<dataRespInd.length; x++){
					//preguntas de la encuesta
					var sPreg = dataRespInd[x];
					//alert(sPreg.IGNUMPREG+'--'+sPreg.TIPORESP+'--'+sPreg.CGPREGUNTA);
					
					pnlPreg = new Ext.Panel({
							title: ((sPreg.TIPORESP=='4')?'':(sPreg.IGNUMPREG+') '))+sPreg.CGPREGUNTA,
							//width: 700,
							anchor: '100%',
							frame: (sPreg.TIPORESP=='4')?false:true,
							layout: 'table',
							layoutConfig: {columns:2}
					});
					
					//respuestas de preguntas
					var objResp = sPreg.LSTRESP;
					if(objResp!=null && objResp.length>0){
						for(var y=0; y<objResp.length; y++){
							var sResp = objResp[y];
							var tipResp = sResp.TIPORESP;
							var imagen = '';
							var texto = '';
							var colspan = '';
							var width = '';
							
							if(tipResp=='1'){
								colspan = 2;
								width = 680;
								texto = sResp.CGCONTESTARESP;
							}else if(tipResp=='2'){
								colspan = 1;
								width = 340;
								imagen ='<div class="icoCheck" style="float:left;width:15px;">&#160</div>';
								texto = sResp.CGRESPUESTA;
							}else if(tipResp=='3'){
								colspan = 1;
								width = 340;
								imagen ='<div class="icoRadio" style="float:left;width:15px;">&#160</div>';
								texto = sResp.CGRESPUESTA;
							}
							else if(tipResp=='5'){
								colspan = 1;
								width = 340;
								texto = sResp.CGRESPUESTA+' = '+sResp.CGCONTESTARESP;
							}else{
								texto = sResp.CGRESPUESTA;
							}
							
							
							pnlPreg.add(
									new Ext.Panel({
										colspan: colspan,
										width: width,
										frame:true,
										bodyStyle:'backgroundColor:white;',
										title:'',
										items:new Ext.form.DisplayField({
											width: (tipResp=='2' || tipResp=='3' || tipResp=='5')?335:670,
											style: 'margin:0 auto;',
											value: imagen+texto
											//colspan:(tipResp=='1')?2:1
										})
									})
							);
						}
					}
					
					fpMuestraResp.add(pnlPreg);
					fpMuestraResp.doLayout();
					
			}
		}
		
	},
	generarBotones: function(fpa,afiliado){
		return [
				{
					text: 'Imprimir',
					id: 'btnImprimirRespInd'+afiliado
					//formBind: true
				},
				{
					text: 'Generar Archivo',
					id: 'btnArchivoRespInd'+afiliado
				}
			]
	},
	setHandlerBtnImprimir: function(fn){
		var btnImprimir = Ext.getCmp('btnImprimirRespInd'+this.tipoAfiliado)
			btnImprimir.setHandler(fn);
	},
	setHandlerBtnArchivo: function(fn){
		var btnArchivo = Ext.getCmp('btnArchivoRespInd'+this.tipoAfiliado)
			btnArchivo.setHandler(fn);
	}
});

NE.encuestas.FormMuestraRespCon = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	dataRespCon: null,
	tituloEnc: '',
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpMuestraRespCon'+this.tipoAfiliado,
			width: 700,
			height:'400',
			title: this.tituloEnc,
			frame: true,   
			border: true, 
			autoScroll: true,
			//monitorValid: true,
			defaultType: 'displayfield',
			autoScroll: true,
			style: 'margin:0 auto;',
			buttons: this.generarBotones(this, this.tipoAfiliado),
			listeners:{
				afterrender: function(obj){
					obj.generarCampos(obj, obj.tipoAfiliado, obj.dataRespCon);
				}
			}
		});
		
		NE.encuestas.FormMuestraRespCon.superclass.initComponent.call(this);
	},
	generarCampos: function(fpMuestraResp, afiliado, dataRespCon){
		var pnlRubro = null;
		var pnlPreg = null;

		if(dataRespCon.length>0){
			var encabezado = new Ext.Panel({
					title: '',
					//width: 700,
					anchor: '100%',
					frame: true,
					layout: 'form'
			});
						
			encabezado.add(
				new Ext.Panel({
					anchor:'100%',
					frame:false,
					layout:'table',
					layoutConfig: {columns:2},
					title:'',
					items:[
						{
							xtype:'panel',
							frame:true,
							width: 555,
							html: "Pregunta"
						},
						{
							xtype:'panel',
							frame:true,
							
							width: 95,
							html: 'Total Resp.'
						}
					]
				})
			);
			fpMuestraResp.add(encabezado);
			
			for(var x=0; x<dataRespCon.length; x++){
					var icRubro = '0';
					//preguntas de la encuesta
					var sPreg = dataRespCon[x];
				
						pnlPreg = new Ext.Panel({
								title: ((sPreg.TIPORESP=='4')?'':(sPreg.IGNUMPREG+') '))+sPreg.CGPREGUNTA,
								//width: 700,
								anchor: '100%',
								frame: (sPreg.TIPORESP=='4')?false:true,
								layout: 'form'
						});
					
					
						//respuestas de preguntas
						var objResp = sPreg.LSTRESP;
						if(objResp!=null && objResp.length>0){
							for(var y=0; y<objResp.length; y++){
								var sResp = objResp[y];
								var lstRespPond = sResp.LSTRESPPOND;
								
								if(sResp.TIPORESP=='5'){
									if(lstRespPond!=null && lstRespPond.length>0){
										for(var z=0; z<lstRespPond.length; z++){
											var sRespPond = lstRespPond[z];
											if(z==0){
												pnlPreg.add(
													new Ext.Panel({
														//colspan:(tipResp=='2' || tipResp=='3')?1:2,
														//width: (tipResp=='2' || tipResp=='3')?340:680,
														anchor:'100%',
														frame:false,
														layout:'table',
														layoutConfig: {columns:2},
														title:'',
														items:[
															{
																xtype:'panel',
																frame:true,
																width: 650,
																colspan: 2,
																//bodyStyle:'backgroundColor:white;',
																html: sResp.CGRESPUESTA
															}
														]
													})
												);
											}
											
											pnlPreg.add(
													new Ext.Panel({
														//colspan:(tipResp=='2' || tipResp=='3')?1:2,
														//width: (tipResp=='2' || tipResp=='3')?340:680,
														anchor:'100%',
														frame:false,
														layout:'table',
														layoutConfig: {columns:2},
														title:'',
														items:[
															{
																xtype:'panel',
																frame:true,
																width: 555,
																bodyStyle:'backgroundColor:white;',
																html: sRespPond.CONTESTARESP
															},
															{
																xtype:'panel',
																frame:true,
																bodyStyle:'backgroundColor:white;',
																width: 95,
																html: sRespPond.CONTRESP
															}
														]
													})
												);
											
										}
									}
								}else{
									pnlPreg.add(
										new Ext.Panel({
											//colspan:(tipResp=='2' || tipResp=='3')?1:2,
											//width: (tipResp=='2' || tipResp=='3')?340:680,
											anchor:'100%',
											frame:false,
											layout:'table',
											layoutConfig: {columns:2},
											title:'',
											items:[
												{
													xtype:'panel',
													frame:true,
													width: 555,
													bodyStyle:'backgroundColor:white;',
													html: sResp.CGRESPUESTA
												},
												{
													xtype:'panel',
													frame:true,
													bodyStyle:'backgroundColor:white;',
													width: 95,
													html: sResp.CONTRESP
												}
											]
										})
									);
								}
							}
						}
						
						fpMuestraResp.add(pnlPreg);
						fpMuestraResp.doLayout();
				//}
			}
		}
		
	},
	generarBotones: function(fpa,afiliado){
		return [
				{
					text: 'Regresar',
					id: 'btnRegresarRespCon'+afiliado
					//formBind: true
				},
				{
					text: 'Imprimir',
					id: 'btnImprimirRespCon'+afiliado
					//formBind: true
				},
				{
					text: 'Generar Archivo',
					id: 'btnArchivoRespCon'+afiliado
				}
			]
	},
	setHandlerBtnRegresar: function(fn){
		var btnRegresar = Ext.getCmp('btnRegresarRespCon'+this.tipoAfiliado)
			btnRegresar.setHandler(fn);
	},
	setHandlerBtnImprimir: function(fn){
		var btnImprimir = Ext.getCmp('btnImprimirRespCon'+this.tipoAfiliado)
			btnImprimir.setHandler(fn);
	},
	setHandlerBtnArchivo: function(fn){
		var btnArchivo = Ext.getCmp('btnArchivoRespCon'+this.tipoAfiliado)
			btnArchivo.setHandler(fn);
	}
});

NE.encuestas.FormModificaPlantilla = Ext.extend(Ext.form.FormPanel,{
	tipoAfiliado: null,
	fnNumPreguntas: null,
	claveEncuesta: null,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpModifPlantilla'+this.tipoAfiliado,
			width: 810,
			title: '<p align="center">Modificaci�n Plantillas</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',  
			defaultType: 'textfield',
			labelWidth: 150,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(this)
			//items : this.generarCampos(this.tipoAfiliado)
			//buttons: this.generarBotones(this)
		});
		
		NE.encuestas.FormModificaPlantilla.superclass.initComponent.call(this);
	},
	generarCampos: function(fp){
		var afiliado = fp.tipoAfiliado;
		var fnNumPreg = fp.fnNumPreguntas;
	
		return [
				{
					xtype:'textfield',
					name:'fp_titulo_m'+afiliado,
					id:'fp_titulo_m1'+afiliado,
					fieldLabel:'T�tulo',
					maxLength:80,
					anchor: '80%',
					allowBlank: false,
					regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
					regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
				},
				{
					xtype: 'textarea',
					id: 'fp_instrucciones_m1'+afiliado,
					name: 'fp_instrucciones_m'+afiliado,
					allowBlank: false,
					fieldLabel: "Instrucciones"
				},
				{
					xtype: 'numberfield',
					id: 'fp_numPreguntas_m1'+afiliado,
					name: 'fp_numPreguntas_m'+afiliado,
					allowDecimals: false,
					maxLength: 3,
					allowBlank: false,
					fieldLabel: "N�mero de Preguntas",
					anchor: '30%',
					autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3'},
					listeners:{
						change: function(obj, newVal, oldVal){
							if( (oldVal=='' || oldVal=='0') && newVal!='' && Number(newVal)>0 ){
									fnNumPreg( newVal, oldVal);
							}else if( Number(newVal)>Number(oldVal)){
									fnNumPreg( newVal, oldVal);;
							}else if( Number(newVal)<Number(oldVal)){
									Ext.Msg.alert('Aviso', 'El n�mero de preguntas no puede ser menor al actual', function(){
										obj.setValue(oldVal);
									});
							}
							//fnNumPreg(newVal);
						}
					}
				}
				
			]
	}
});