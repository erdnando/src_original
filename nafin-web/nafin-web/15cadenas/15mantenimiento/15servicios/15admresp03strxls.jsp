<%@page contentType		= "text/html;charset=Cp1252"
			import			= "java.sql.*" 
			import			= "java.util.*" 
			import			= "java.text.*" 
			import			= "java.math.*" 
			import      	= "java.io.*"
			import			= "com.netro.zip.*"
			import			= "netropology.utilerias.*" 
			import			= "netropology.utilerias.usuarios.*" 
			import			= "com.netro.exception.*"
			import			= "com.netro.dispersion.*" 
			import			= "com.netro.cadenas.*"
			import			= "javax.naming.*"
         import		   = "com.netro.fondosjr.ConsultaAmortizacionesPendientesPago"
			import		   = "com.netro.fondojr.*"
         import		   = "java.math.BigDecimal"
         import		   = "org.apache.commons.beanutils.PropertyUtils"
			pageEncoding	= "Cp1252"
			
%><%--@ include file			= "/15cadenas/015secsession.jspf" --%><jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" /><%

	//El siguiente c�digo debe ser lo primero en ejecutarse, el cual determina si la sesi�n est� activa. 
	String gsCveUsuario = (String) session.getAttribute("sesCveUsuario");
	if(gsCveUsuario == null){
		out.println("");
		return;
	}
	
	String strDirectorioPublicacion 	= (String)application.getAttribute("strDirectorioPublicacion");
	String directorioPlantilla			= strDirectorioPublicacion + "00archivos/plantillas/15cadenas/";
	String directorioTemporal			= strDirectorioPublicacion + "00tmp/15cadenas/";
   
   String sesIdiomaUsuario          = (session.getAttribute("sesIdiomaUsuario") == null)?"ES":(String)session.getAttribute("sesIdiomaUsuario");

	//Parametros de la pantalla Actual	
	String num_encuesta 		= (request.getParameter("num_encuesta") 	== null)?"":request.getParameter("num_encuesta");
	String tipoEncuesta 		= request.getParameter("tipoEncuesta") 	== null ?"":request.getParameter("tipoEncuesta");
	
	System.out.println("num_encuesta==="+num_encuesta);
	System.out.println("tipoEncuesta==="+tipoEncuesta);
 
	// Enviar cabecera del contenido de la informacion enviada
	response.setDateHeader("Expires", 				0);
	response.setHeader("Pragma", 						"noCache");
	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-Disposition", 	"attachment;filename=Encuesta_Consolidadas y Libres.xlsx");
 
   // Conectarse a la Base de Datos 
	AccesoDB 				con 				= new AccesoDB();
	StringBuffer			query				= new StringBuffer();
	
	ResultSet 				rs 				= null;
	PreparedStatement 	ps 				= null;
	
	ResultSet 				rs1 				= null;
	PreparedStatement 	ps1 				= null;
	
	String 					nombreEncuesta	= "";
	
	BufferedWriter 		xml 				= null;
	OutputStreamWriter 	writer 			= null;
	CreaArchivo 			archivo 			= new CreaArchivo();
	
	String					hoja1 			= null;
	String					hoja2 			= null;
	String  					archivoXLSX 	= null;
	int 						rowNum 			= 0;
	
	final int				ESTILO_NORMAL	= 0;
	final int 				SUBTITULO		= 1;
	final int				TITULO			= 2;
	
   try{
		
		nombreEncuesta = getDescripcionEncuesta(num_encuesta);
		
		// 1. CONSULTAR LOS DATOS (RESPUESTA CONSOLIDADA)
		con.conexionDB();
		
		// 1.1 CONSULTAR LOS RUBROS DE LA ENCUESTA												
		query.append( 
							"	SELECT cp.cg_pregunta,  cp.ic_rubro FROM com_pregunta cp  "+
							" WHERE CP.ic_encuesta = ? "+
							"	and cp.ic_tipo_respuesta = ? "+
							"  ORDER BY cp.ic_pregunta "
						);

		ps = con.queryPrecompilado(query.toString());
		ps.setLong(1, Long.parseLong(num_encuesta));
		ps.setLong(2, 4);
		rs = ps.executeQuery();
		
		// 1.2 PREPARAR QUERY PARA CONSULTAR LA PREGUNTAS CON OPCION MULTIPLE Y OPCION EXCLUYENTE
		query.setLength(0);
		query.append( 
							"SELECT cp.ic_pregunta AS ic_pregunta, cp.cg_pregunta AS cg_pregunta, " +
							"       cp.ig_num_pregunta as ig_num_pregunta , cp.ic_tipo_respuesta AS tipo_resp " +
							"       ,cp.IC_NOPREGUNTA  as IC_NOPREGUNTA "+
							"  FROM com_encuesta cea, com_pregunta cp " +
							" WHERE cea.ic_encuesta = cp.ic_encuesta " +
							"   AND cea.ic_encuesta = ? " +		
							"   AND cp.ic_rubro = ? "+
							"   AND cp.ic_tipo_respuesta in(2,3,5) " + // <---- Respustas de Opcion Multiple y Opcion Excluyente
							"	ORDER BY cp.ic_pregunta "
						);
		ps1 = con.queryPrecompilado(query.toString());
 
		List 			datos 		   = new ArrayList();	
		List			datos2		   = new ArrayList();
      String      num_pregunta 	= "";
      String      desc_pregunta 	= "";
      List 			repuestasR 	   = new ArrayList();
      List 			repuestasSR    = new ArrayList();
 
		while (rs.next()){
			
			String 	tipo_resp 		= "";
			int 		numeracion 		= 0;
			String  	ic_pregunta 	= "";
			String  	rubro 			= "";
			String 	cg_pregunta 	= "";
			String  	IC_NOPREGUNTA 	= "";
		
			datos 		= new ArrayList();	
			rubro 		= rs.getString("ic_rubro")		== null?"":rs.getString("ic_rubro");
			cg_pregunta = rs.getString("cg_pregunta")	== null?"":rs.getString("cg_pregunta");
			
			// 1.2.1 PARA CADA UNO DE LOS RUBROS CONSULTAR LAS PREGUNTAS CON OPCION MULTIPLE Y OPCION EXCLUYENTE 
			ps1.clearParameters();
			ps1.setLong(1, Long.parseLong(num_encuesta));
			ps1.setLong(2, Long.parseLong(rubro));
			rs1 = ps1.executeQuery();
		
			datos.add(cg_pregunta); // <-- Agregar descripcion del Rubro
			
			while (rs1.next()){	
			
				datos2 			= new ArrayList();
				
				ic_pregunta 	= rs1.getString("ic_pregunta")		== null?"":rs1.getString("ic_pregunta");	
				num_pregunta 	= rs1.getString("ig_num_pregunta")	== null?"":rs1.getString("ig_num_pregunta");
				desc_pregunta 	= rs1.getString("cg_pregunta")		== null?"":rs1.getString("cg_pregunta");
				tipo_resp 		= rs1.getString("tipo_resp")			== null?"":rs1.getString("tipo_resp");
				IC_NOPREGUNTA 	= rs1.getString("IC_NOPREGUNTA")		== null?"":rs1.getString("IC_NOPREGUNTA");	
	 
				datos2.add(ic_pregunta);			
				datos2.add(num_pregunta);
				datos2.add(desc_pregunta);
				datos2.add(tipo_resp);
				datos2.add(IC_NOPREGUNTA);		
				
				datos.add(datos2);				
			}
			
			rs1.close();
			
			repuestasR.add(datos);
			
		}
		
		ps1.close();
		
		rs.close();
		ps.close();
		
		// 1.3. SI NO SE ENCONTRO NINGUN RUBRO: CONSULTAR DIRECTAMENTE LAS PREGUNTAS CON OPCION MULTIPLE Y OPCION EXCLUYENTE 
		if(repuestasR.size() == 0) {
		
         query.setLength(0);
			query.append(
								"SELECT cp.ic_pregunta AS ic_pregunta, cp.cg_pregunta AS cg_pregunta, " +
								"       cp.ig_num_pregunta as ig_num_pregunta , cp.ic_tipo_respuesta AS tipo_resp " +
								"       ,cp.IC_NOPREGUNTA  as IC_NOPREGUNTA "+
								"  FROM com_encuesta cea, com_pregunta cp " +
								" WHERE cea.ic_encuesta = cp.ic_encuesta " +
								"   AND cea.ic_encuesta = ? " +														
								"   AND cp.ic_tipo_respuesta in(2,3,5) " + // <---- Respustas de Opcion Multiple y Opcion Excluyente
								"	ORDER BY cp.ic_pregunta "
							);
 
			String 	tipo_resp 		= "";
			int 		numeracion 		= 0;
			String  	ic_pregunta 	= "";   
			String  	rubro 			= "";  
			String  	cg_pregunta 	= ""; 
			String  	IC_NOPREGUNTA 	= "";
		
			ps = con.queryPrecompilado(query.toString());
			ps.setLong(1, Long.parseLong(num_encuesta));				
			rs = ps.executeQuery();
			 
			while (rs.next()) {	
			
				datos2 			= new ArrayList();
				
				ic_pregunta 	= rs.getString("ic_pregunta")			== null?"":rs.getString("ic_pregunta");
				num_pregunta 	= rs.getString("ig_num_pregunta")	== null?"":rs.getString("ig_num_pregunta");
				desc_pregunta 	= rs.getString("cg_pregunta")			== null?"":rs.getString("cg_pregunta");
				tipo_resp 		= rs.getString("tipo_resp")			== null?"":rs.getString("tipo_resp");	
				IC_NOPREGUNTA 	= rs.getString("IC_NOPREGUNTA")		== null?"":rs.getString("IC_NOPREGUNTA");
	 
				datos2.add(ic_pregunta);			
				datos2.add(num_pregunta);
				datos2.add(desc_pregunta);
				datos2.add(tipo_resp);
				datos2.add(IC_NOPREGUNTA);	
				
				datos.add(datos2);					
			}
		
			rs.close();
			ps.close();			
			 
			repuestasSR.add(datos);	
	 
		}
 
		// 1.4 CREAR PRIMERA HOJA DEL ARCHIVO XLSX
		try {
				
			// Crear Archivo correspondiente a la hoja 1: => Respuesta consolidada
			hoja1		= archivo.nombreArchivo()+".xml";
			writer 	= new OutputStreamWriter(new FileOutputStream(directorioTemporal+hoja1),"UTF8"); 
			xml 	   = new BufferedWriter(writer);
			rowNum 	= 0;
			
			// Definir Cabecera
			xml.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			xml.write("<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">");
			xml.write("<sheetData>");
			
			rowNum++;
			xml.write("<row r=\""+String.valueOf(rowNum)+"\" spans=\"1:2\" >");			
			writeCellContent(xml, nombreEncuesta, TITULO, "A"+rowNum);
			xml.write("</row>");
	
			// xml.write("\n" + mensaje_param.getMensaje("15admenc02.Pregunta",sesIdiomaUsuario) + "," + mensaje_param.getMensaje("15admenc02.TotalResp",sesIdiomaUsuario));
			rowNum++;
			xml.write("<row r=\""+String.valueOf(rowNum)+"\">");			
			writeCellContent(xml, mensaje_param.getMensaje("15admenc02.Pregunta", sesIdiomaUsuario),  SUBTITULO, "A"+rowNum);
			writeCellContent(xml, mensaje_param.getMensaje("15admenc02.TotalResp",sesIdiomaUsuario),  SUBTITULO, "B"+rowNum);			
			xml.write("</row>");
				
			int iCurrRow = 0;
			
			// 1.4.1 PARA CADA UNO DE LOS RUBROS MOSTRAR LAS PREGUNTAS CON OPCION MULTIPLE Y OPCION EXCLUYENTE
			if(repuestasR.size()>0){
				
				// Definir query para consultar las respuestas
				query.setLength(0);
				query.append(
											"select cr.cg_respuesta as cg_respuesta, nvl(totr.countresp,0) as countresp " +
											"from com_encuesta cea, com_pregunta cp, com_respuesta cr, " +
											"	( " +
											"	SELECT  COUNT (DISTINCT (ic_usuario)) AS countresp, " +
											"             cea.ic_encuesta AS ic_encuesta, ccr.ic_respuesta as ic_respuesta " +
											"        FROM com_contesta_respuesta ccr, comrel_encuesta_afiliado cea " +
											"       WHERE ccr.IC_ENCUESTA_AFIL = cea.IC_ENCUESTA_AFIL  " +
											"       and cea.ic_encuesta = ? " +
											"    GROUP BY cea.ic_encuesta, ccr.ic_respuesta " +
											"    ) totr " +
											"where cea.ic_encuesta = cp.ic_encuesta " +
											"AND cr.ic_encuesta = totr.ic_encuesta(+) " +
											"and cr.ic_respuesta = totr.ic_respuesta(+)  " +
											"and cp.ic_pregunta = cr.ic_pregunta " +
											"and cea.ic_encuesta = ? " +
											"and cr.ic_pregunta = ? " +
											"and cr.ic_tipo_respuesta in(?,?,?) "// <---- Respustas de Opcion Multiple y Opcion Excluyente
										);
				ps = con.queryPrecompilado(query.toString());
					
				// Para cada Rubro:
				for(int x=0; x<repuestasR.size();x++){
												
					String tipo_resp 		= "";  
					String ic_pregunta 	= "";   
					String rubro 			= "";  
					String cg_pregunta 	= ""; 
					String IC_NOPREGUNTA = "";
					
					// Leer rubros y sus preguntas asociadas
					List informacion = (ArrayList)repuestasR.get(x);
					
					if(informacion.size()>1){ // Si el rubro tiene preguntas
														
						String  Rubro  = (String)informacion.get(0);
						
						// Mostrar Descripcion del Rubro
						if(!Rubro.equals("")){ 
							// xml.write("\n"+ Rubro.replace(',', ' '));
							rowNum++;
							xml.write("<row r=\""+String.valueOf(rowNum)+"\" spans=\"1:2\" >");
							writeCellContent(xml, Rubro, TITULO, "A"+rowNum);
							xml.write("</row>");
						}
 
						// Desplegar cada una de las preguntas asociadas al Rubro en cuestion
						for(int y=1; y<informacion.size();y++){
							
							List resdatos = (ArrayList)informacion.get(y);
							
							ic_pregunta  	= (String)resdatos.get(0);
							num_pregunta  	= (String)resdatos.get(1);
							desc_pregunta  = (String)resdatos.get(2);
							tipo_resp  		= (String)resdatos.get(3);
							IC_NOPREGUNTA  = (String)resdatos.get(4);
														
							// Agregar descripcion de la Pregunta de Respuesta Libre 
							if(tipo_resp.equals("2") || tipo_resp.equals("3") || tipo_resp.equals("4") ){ 
								if(tipo_resp.equals("4")){ 
									//xml.write("\n"+ desc_pregunta.replace(',', ' '));
									rowNum++;
									xml.write("<row r=\""+String.valueOf(rowNum)+"\"  spans=\"1:2\" >");
									writeCellContent(xml, desc_pregunta, SUBTITULO, "A"+rowNum);
									xml.write("</row>");
								}else{
									//xml.write( "\n"+IC_NOPREGUNTA +") "+ desc_pregunta.replace(',', ' '));
									rowNum++;
									xml.write("<row r=\""+String.valueOf(rowNum)+"\"  spans=\"1:2\" >");
									writeCellContent(xml, (IC_NOPREGUNTA +") "+ desc_pregunta), SUBTITULO, "A"+rowNum);
									xml.write("</row>");
								}
							}
																
							// Consultar respuestas a la pregunta en cuestion
							ps.clearParameters();
							ps.setLong(1,Long.parseLong(num_encuesta));
							ps.setLong(2,Long.parseLong(num_encuesta));
							ps.setLong(3,Long.parseLong(ic_pregunta));
							ps.setLong(4,Long.parseLong("2"));
							ps.setLong(5,Long.parseLong("3"));
							ps.setLong(6,Long.parseLong("5"));
							rs = ps.executeQuery();
														
							while(rs.next()){
																		
								String	cg_respuesta2 	= rs.getString("cg_respuesta")	== null?"":rs.getString("cg_respuesta");
								String	countresp 		= rs.getString("countresp")		== null?"":rs.getString("countresp");
					
								//xml.write("\n"+cg_respuesta2.replaceAll("\\r"," ").replaceAll("\\n", " ").replace('\"', ' ').replace('\\', ' ').replace(',', ' ') + "," + rs.getString("countresp"));
								rowNum++;
								xml.write("<row r=\""+String.valueOf(rowNum)+"\">");
								writeCellContent(xml, cg_respuesta2, ESTILO_NORMAL, "A"+rowNum);
								writeCellContent(xml, countresp, 	 ESTILO_NORMAL, "B"+rowNum);  
								xml.write("</row>");
				
								if(iCurrRow % 70 == 0){
									xml.flush();
								}
								
							}
							
							rs.close();
 
						}
					}
				}
				
				ps.close();
				
			}
			
			// 1.5. SI NO SE ENCONTRO NINGUN RUBRO: MOSTRAR LAS PREGUNTAS CON SUS RESPUESTAS DIRECTAMENTE
			if(repuestasSR.size()>0){
				
				// Definir query para consultar las respuestas
				query.setLength(0);
				query.append(
											"select cr.cg_respuesta as cg_respuesta, nvl(totr.countresp,0) as countresp, cr.ic_respuesta " +
											"from com_encuesta cea, com_pregunta cp, com_respuesta cr, " +
											"	( " +
											"	SELECT  COUNT (DISTINCT (ic_usuario)) AS countresp, " +
											"             cea.ic_encuesta AS ic_encuesta, ccr.ic_respuesta as ic_respuesta " +
											"        FROM com_contesta_respuesta ccr, comrel_encuesta_afiliado cea " +
											"       WHERE ccr.IC_ENCUESTA_AFIL = cea.IC_ENCUESTA_AFIL  " +
											"       and cea.ic_encuesta = ? " +
											"    GROUP BY cea.ic_encuesta, ccr.ic_respuesta " +
											"    ) totr " +
											"where cea.ic_encuesta = cp.ic_encuesta " +
											"AND cr.ic_encuesta = totr.ic_encuesta(+) " +
											"and cr.ic_respuesta = totr.ic_respuesta(+)  " +
											"and cp.ic_pregunta = cr.ic_pregunta " +
											"and cea.ic_encuesta = ? " +
											"and cr.ic_pregunta = ? " +
											"and cr.ic_tipo_respuesta in(?,?,?) "// <---- Respustas de Opcion Multiple y Opcion Excluyente
										);
				ps = con.queryPrecompilado(query.toString());
				
				// Para cada pregunta
				for(int x=0; x<repuestasSR.size();x++){
													
					String tipo_resp 		= "";  
					String ic_pregunta 	= "";   
					String rubro 			= "";  
					String cg_pregunta 	= ""; 
					String IC_NOPREGUNTA = "";
				
					List informacion = (ArrayList)repuestasSR.get(x);
													
					// Desplegar cada una de las preguntas de libre respuesta 	
					for(int y=0; y<informacion.size();y++){
					
						List resdatos = (ArrayList) informacion.get(y);
						
						ic_pregunta  	= (String)resdatos.get(0);
						num_pregunta  	= (String)resdatos.get(1);
						desc_pregunta  = (String)resdatos.get(2);
						tipo_resp  		= (String)resdatos.get(3);
						IC_NOPREGUNTA  = (String)resdatos.get(4);
													
						// Agregar descripcion de la Pregunta
						// xml.write("\n"+IC_NOPREGUNTA +") "+ desc_pregunta.replace(',', ' '));
						rowNum++;
						xml.write("<row r=\""+String.valueOf(rowNum)+"\" spans=\"1:2\" >");
						writeCellContent(xml, (IC_NOPREGUNTA +") "+ desc_pregunta), SUBTITULO, "A"+rowNum);
						xml.write("</row>");
							
						// Consultar respuestas a la pregunta en cuestion
						ps.clearParameters();
						ps.setLong(1,Long.parseLong(num_encuesta));
						ps.setLong(2,Long.parseLong(num_encuesta));
						ps.setLong(3,Long.parseLong(ic_pregunta));
						ps.setLong(4,Long.parseLong("2"));
						ps.setLong(5,Long.parseLong("3"));
						ps.setLong(6,Long.parseLong("5"));
						rs = ps.executeQuery();
				
						while(rs.next()){
				
							String	cg_respuesta2 	= rs.getString("cg_respuesta") == null?"":rs.getString("cg_respuesta");
							String	countresp 		= rs.getString("countresp")    == null?"":rs.getString("countresp");
              String	cveRespuesta 		= rs.getString("ic_respuesta")    == null?"":rs.getString("ic_respuesta");
				
							//xml.write("\n"+cg_respuesta2.replaceAll("\\r"," ").replaceAll("\\n", " ").replace('\"', ' ').replace('\\', ' ').replace(',', ' ') + "," + rs.getString("countresp"));
							rowNum++;
							xml.write("<row r=\""+String.valueOf(rowNum)+"\">");
							writeCellContent(xml, cg_respuesta2, ESTILO_NORMAL, "A"+rowNum);
							writeCellContent(xml, countresp, 	 ESTILO_NORMAL, "B"+rowNum);
							xml.write("</row>");
              
              if("5".equals(tipo_resp)){
                String strSQL = "SELECT   COUNT (DISTINCT (ic_usuario)) AS countresp,  " +
                "                 cea.ic_encuesta AS ic_encuesta,  " +
                "                 ccr.ic_respuesta AS ic_respuesta, " +
                "                 ccr.cg_contesta_respuesta as cg_contesta_respuesta " +
                "            FROM com_contesta_respuesta ccr, comrel_encuesta_afiliado cea  " +
                "           WHERE ccr.ic_encuesta_afil = cea.ic_encuesta_afil  " +
                "             AND cea.ic_encuesta = ? " +
                "             AND ccr.ic_respuesta = ? " +
                "        GROUP BY cea.ic_encuesta, ccr.ic_respuesta, ccr.cg_contesta_respuesta ";
                
                PreparedStatement psr = null;
                ResultSet rsr = null;
                psr = con.queryPrecompilado(strSQL);
                psr.setLong(1,Long.parseLong(num_encuesta));
                psr.setLong(2,Long.parseLong(cveRespuesta));
                rsr = psr.executeQuery();
                
                while(rsr.next()){
                    rowNum++;
                    xml.write("<row r=\""+String.valueOf(rowNum)+"\">");
                    writeCellContent(xml, rsr.getString("cg_contesta_respuesta"), ESTILO_NORMAL, "A"+rowNum);
                    writeCellContent(xml, rsr.getString("countresp"), 	 ESTILO_NORMAL, "B"+rowNum);
                    xml.write("</row>");
                }
                rsr.close();
                psr.close();
              }
							
							if(iCurrRow % 70 == 0){
								xml.flush();
							}
							
						}
														
						rs.close();
						
					}
					
				}
				
				ps.close();
							
			}

			// Si no hay registros mostrar mensaje 
			if(repuestasR.size()==0 && repuestasSR.size()==0 ){ 
				// xml.write("No existen Registros");
				rowNum++;
				xml.write("<row r=\""+String.valueOf(rowNum)+"\">");
				writeCellContent(xml, "No existen Registros", ESTILO_NORMAL, "A"+rowNum);
				xml.write("</row>");
			}
			
			xml.write("</sheetData>");
			xml.write("</worksheet>");
			
		} catch (Exception e) {
			System.out.println("15admresp03strxls.jsp(Exception)");
			e.printStackTrace();
			if(xml 	!= null )	try{xml.close();}catch(Exception sub){};
			throw e;
		}finally{
			if(xml 	!= null )	try{xml.close();}catch(Exception sub){};	
		}
 
		// 2. CONSULTAR LOS DATOS (RESPUESTA LIBRE)
		datos 		   = new ArrayList();	
		datos2		   = new ArrayList();
      num_pregunta 	= "";
      desc_pregunta 	= "";
      repuestasR 	   = new ArrayList();
      repuestasSR    = new ArrayList();
				
		// 2.1. CONSULTAR LOS RUBROS DE LA ENCUESTA
		query.setLength(0);
		query.append(
							"	SELECT cp.cg_pregunta,  cp.ic_rubro FROM com_pregunta cp  "+
							" WHERE CP.ic_encuesta = ? "+
							"	and cp.ic_tipo_respuesta = ? "+
							"  ORDER BY cp.ic_pregunta "
						);
 
		ps = con.queryPrecompilado(query.toString());
		ps.setLong(1, Long.parseLong(num_encuesta));
		ps.setLong(2, 4);// Preguntas de Respuesta Libre
		rs = ps.executeQuery();	 
		
		// 2.2 PREPARAR QUERY PARA CONSULTAR LA PREGUNTAS CON RESPUESTA LIBRE
		query.setLength(0);
		query.append(
							"SELECT cp.ic_pregunta AS ic_pregunta, cp.cg_pregunta AS cg_pregunta, " +
							"       cp.ig_num_pregunta as ig_num_pregunta , cp.ic_tipo_respuesta AS tipo_resp " +
							"       ,cp.IC_NOPREGUNTA  as IC_NOPREGUNTA "+
							"  FROM com_encuesta cea, com_pregunta cp " +
							" WHERE cea.ic_encuesta = cp.ic_encuesta " +
							"   AND cea.ic_encuesta = ? " +		
							"   AND cp.ic_rubro = ? "+
							"   AND cp.ic_tipo_respuesta in (1) " + // <---- Preguntas de respuesta libre
							"	ORDER BY cp.ic_pregunta "
						);		
		ps1 = con.queryPrecompilado(query.toString());
	
		while (rs.next()){
		
			String 	tipo_resp 		= "";
			int 		numeracion 		= 0;
			String  	ic_pregunta 	= "";   
			String  	rubro 			= "";  
			String  	cg_pregunta 	= ""; 
			String  	IC_NOPREGUNTA 	= "";
		
			datos 		= new ArrayList();	
			rubro 		= rs.getString("ic_rubro")		== null?"":rs.getString("ic_rubro");
			cg_pregunta = rs.getString("cg_pregunta")	== null?"":rs.getString("cg_pregunta");		
		
			// 2.2.1 PARA CADA UNO DE LOS RUBROS CONSULTAR LAS PREGUNTAS DE RESPUESTA LIBRE
			ps1.clearParameters();
			ps1.setLong(1, Long.parseLong(num_encuesta));
			ps1.setLong(2, Long.parseLong(rubro));
			rs1 = ps1.executeQuery();
			 
			datos.add(cg_pregunta); // <-- Agregar descripcion del Rubro
			 
			while (rs1.next()){								
				
				datos2 				= new ArrayList();	
				
				ic_pregunta 		= rs1.getString("ic_pregunta")		== null?"":rs1.getString("ic_pregunta");	
				num_pregunta 		= rs1.getString("ig_num_pregunta")	== null?"":rs1.getString("ig_num_pregunta");
				desc_pregunta 		= rs1.getString("cg_pregunta")		== null?"":rs1.getString("cg_pregunta");
				tipo_resp 			= rs1.getString("tipo_resp")			== null?"":rs1.getString("tipo_resp");
				IC_NOPREGUNTA 		= rs1.getString("IC_NOPREGUNTA")		== null?"":rs1.getString("IC_NOPREGUNTA");	
	 
				datos2.add(ic_pregunta);			
				datos2.add(num_pregunta);
				datos2.add(desc_pregunta);
				datos2.add(tipo_resp);
				datos2.add(IC_NOPREGUNTA);
				
				datos.add(datos2);		
				
			}
		
			rs1.close();
					 
			repuestasR.add(datos);
			
		}
		
		ps1.close();
	
		rs.close();
		ps.close();		
	 
		// 2.3. SI NO SE ENCONTRO NINGUN RUBRO: CONSULTAR LAS PREGUNTAS DE RESPUESTA LIBRE DIRECTAMENTE
		if(repuestasR.size() == 0) {
		
			query.setLength(0);
			query.append( 
								"SELECT cp.ic_pregunta AS ic_pregunta, cp.cg_pregunta AS cg_pregunta, " +
								"       cp.ig_num_pregunta as ig_num_pregunta , cp.ic_tipo_respuesta AS tipo_resp " +
								"       ,cp.IC_NOPREGUNTA  as IC_NOPREGUNTA "+
								"  FROM com_encuesta cea, com_pregunta cp " +
								" WHERE cea.ic_encuesta = cp.ic_encuesta " +
								"   AND cea.ic_encuesta = ? " +														
								"   AND cp.ic_tipo_respuesta in (1) " + // <---- Preguntas de respuesta libre
								"	ORDER BY cp.ic_pregunta "
							);
 
			String 	tipo_resp 		= "";
			int 		numeracion 		= 0;
			String  	ic_pregunta 	= "";
			String 	rubro 			= "";
			String 	cg_pregunta 	= "";
			String 	IC_NOPREGUNTA 	= "";
		
			ps = con.queryPrecompilado(query.toString());
			ps.setLong(1, Long.parseLong(num_encuesta));				
			rs = ps.executeQuery();
			 
			while (rs.next()) {
			
				datos2 			= new ArrayList();
				
				ic_pregunta 	= rs.getString("ic_pregunta")			== null?"":rs.getString("ic_pregunta");
				num_pregunta 	= rs.getString("ig_num_pregunta")	== null?"":rs.getString("ig_num_pregunta");
				desc_pregunta 	= rs.getString("cg_pregunta")			== null?"":rs.getString("cg_pregunta");
				tipo_resp 		= rs.getString("tipo_resp")			== null?"":rs.getString("tipo_resp");	
				IC_NOPREGUNTA 	= rs.getString("IC_NOPREGUNTA")		== null?"":rs.getString("IC_NOPREGUNTA");
					
				datos2.add(ic_pregunta);			
				datos2.add(num_pregunta);
				datos2.add(desc_pregunta);
				datos2.add(tipo_resp);
				datos2.add(IC_NOPREGUNTA);
				
				datos.add(datos2);
				
			}
		
			rs.close();
			ps.close();			
			 
			repuestasSR.add(datos);	
	 
		}
 
		// 2.4 CREAR SEGUNDA HOJA DEL ARCHIVO XLSX
		try {
			
			// Crear Archivo correspondiente a la hoja 2: => Respuestas Libres
			hoja2		= archivo.nombreArchivo()+".xml";
			writer 	= new OutputStreamWriter(new FileOutputStream(directorioTemporal+hoja2),"UTF8"); 
			xml 	   = new BufferedWriter(writer);
			rowNum 	= 0;
			
			// Definir cabecera
			xml.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			xml.write("<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">");
			xml.write("<sheetData>");
			
			// Si la encuesta fue publicada para Pyme, mostrar tambien el nombre de la EPO
			if("P".equals(tipoEncuesta)){
				rowNum++;
				xml.write("<row r=\""+String.valueOf(rowNum)+"\" spans=\"1:3\" >");
				writeCellContent(xml, getNombreEpoEncuestaPyme(num_encuesta), TITULO, "A"+rowNum);
				xml.write("</row>");
			}
			
			// xml.write(nombreEncuesta);
			rowNum++;
			xml.write("<row r=\""+String.valueOf(rowNum)+"\" spans=\"1:3\" >");
			writeCellContent(xml, nombreEncuesta, TITULO, "A"+rowNum);
			xml.write("</row>");
											
			// 2.4.1 PARA CADA UNO DE LOS RUBROS MOSTRAR LAS PREGUNTAS CON SUS RESPUESTAS LIBRES
			if(repuestasR.size()>0){
				
				// Definir query para consultar las respuestas
				query.setLength(0);
				query.append(
					"SELECT   																								"  +
					" DECODE( CEA.CG_AFILIADO, 																		"  +
					"   'E', (select CG_RAZON_SOCIAL from COMCAT_EPO where IC_EPO = CEA.IC_EPO), 		"  +
					"   'I', (select CG_RAZON_SOCIAL from COMCAT_IF where IC_IF = CEA.IC_IF), 			"  +
					"   'P', (select CG_RAZON_SOCIAL from COMCAT_PYME where IC_PYME = CEA.IC_PYME) 	"  +
					" ) AS ENCUESTADO, 											"  +
					" CCR.CG_CONTESTA_RESPUESTA AS CG_RESPUESTA 			"  +
					"FROM                   									"  +
					"   COM_ENCUESTA  CE,  										"  +
					"   COM_PREGUNTA  CP,   									"  +
					"   COM_RESPUESTA CR, 										"  +
					"   COM_CONTESTA_RESPUESTA CCR,  						"  +
					"   COMREL_ENCUESTA_AFILIADO CEA 						"  +
					"WHERE                  									"  +
					"    CE.IC_ENCUESTA        = CP.IC_ENCUESTA  		"  +
					"AND CP.IC_PREGUNTA        = CR.IC_PREGUNTA  		"  +
					"AND CE.IC_ENCUESTA        = ?     						"  +
					"AND CR.IC_PREGUNTA        = ?     						"  +
					"AND CR.IC_TIPO_RESPUESTA IN (?)  						"  + // <--- Preguntas de respuesta libre
					"AND CR.IC_RESPUESTA       = CCR.IC_RESPUESTA 		"  +
					"AND CCR.IC_ENCUESTA_AFIL  = CEA.IC_ENCUESTA_AFIL 	"  +
					"ORDER BY ENCUESTADO 										"
				);
				ps = con.queryPrecompilado(query.toString());
				
				// Para cada Rubro
				for(int x=0; x<repuestasR.size();x++){
													
					String tipo_resp 		= "";  
					String ic_pregunta 	= "";   
					String rubro 			= "";  
					String cg_pregunta 	= ""; 
					String IC_NOPREGUNTA = "";
													
					// Leer rubros y sus preguntas asociadas
					List informacion 		= (ArrayList)repuestasR.get(x);
					
					if(informacion.size()>1){	// Si el rubro tiene preguntas de libre respuesta	
												
						String  Rubro  = (String)informacion.get(0);		
														
						// Mostrar Descripcion del Rubro 
						if(!Rubro.equals("")){ 
							// xml.write("\n"+ Rubro);
							rowNum++;
							xml.write("<row r=\""+String.valueOf(rowNum)+"\" spans=\"1:3\" >");
							writeCellContent(xml, Rubro, TITULO, "A"+rowNum);
							xml.write("</row>");
						}
						
						// Desplegar cada una de las preguntas de libre respuesta asociadas al Rubro en cuestion
						for(int y=1; y<informacion.size(); y++){
									
							List resdatos 	= (ArrayList)informacion.get(y);
										
							ic_pregunta  	= (String)resdatos.get(0);
							num_pregunta  	= (String)resdatos.get(1);
							desc_pregunta  = (String)resdatos.get(2);
							tipo_resp  		= (String)resdatos.get(3);
							IC_NOPREGUNTA  = (String)resdatos.get(4);
															
							// Agregar descripcion de la Pregunta de Respuesta Libre 
							//xml.write("\n" + IC_NOPREGUNTA + ") " + desc_pregunta );
							rowNum++;
							xml.write("<row r=\""+String.valueOf(rowNum)+"\" spans=\"1:3\" >");
							writeCellContent(xml, (IC_NOPREGUNTA + ") " + desc_pregunta), SUBTITULO, "A"+rowNum);
							xml.write("</row>");
				 
							// Consultar respuestas a la pregunta en cuestion
							ps.clearParameters();
							ps.setLong(1, Long.parseLong(num_encuesta));
							ps.setLong(2, Long.parseLong(ic_pregunta));
							ps.setLong(3, Long.parseLong("1"));
							rs = ps.executeQuery();
							
							while(rs.next()){
								
								String	encuestado 		= rs.getString("ENCUESTADO")   == null?"":rs.getString("ENCUESTADO").trim();
								String	cg_respuesta2 	= rs.getString("CG_RESPUESTA") == null?"":rs.getString("CG_RESPUESTA").trim();
																
								if( "".equals(cg_respuesta2) || "null".equalsIgnoreCase(cg_respuesta2)) continue;
								//cg_respuesta2 		= "null".equalsIgnoreCase(cg_respuesta2)?"El usuario no especific� ninguna respuesta valida.":cg_respuesta2;
																	
								//xml.write("\n" + encuestado +","+ cg_respuesta2);
								rowNum++;
								xml.write("<row r=\""+String.valueOf(rowNum)+"\"  spans=\"1:3\" >");
								writeCellContent(xml, encuestado,    ESTILO_NORMAL, "B"+rowNum);
								writeCellContent(xml, cg_respuesta2, ESTILO_NORMAL, "C"+rowNum);
								xml.write("</row>");
								
							}
															
							rs.close();
	 
						}
					}
				}
					
				ps.close();
					
			} 
	
			// 2.4.2 SI NO SE ENCONTRO NINGUN RUBRO: MOSTRAR LAS PREGUNTAS CON SUS RESPUESTAS LIBRES DIRECTAMENTE
			if(repuestasSR.size()>0){
					
				// Definir query para consultar las respuestas
				query.setLength(0);
				query.append(
									"SELECT   																								"  +
									" DECODE( CEA.CG_AFILIADO, 																		"  +
									"   'E', (select CG_RAZON_SOCIAL from COMCAT_EPO where IC_EPO = CEA.IC_EPO), 		"  +
									"   'I', (select CG_RAZON_SOCIAL from COMCAT_IF where IC_IF = CEA.IC_IF), 			"  +
									"   'P', (select CG_RAZON_SOCIAL from COMCAT_PYME where IC_PYME = CEA.IC_PYME) 	"  +
									" ) AS ENCUESTADO, 											"  +
									" CCR.CG_CONTESTA_RESPUESTA AS CG_RESPUESTA 			"  +
									"FROM                   									"  +
									"   COM_ENCUESTA  CE,  										"  +
									"   COM_PREGUNTA  CP,   									"  +
									"   COM_RESPUESTA CR, 										"  +
									"   COM_CONTESTA_RESPUESTA CCR,  						"  +
									"   COMREL_ENCUESTA_AFILIADO CEA 						"  +
									"WHERE                  									"  +
									"    CE.IC_ENCUESTA        = CP.IC_ENCUESTA  		"  +
									"AND CP.IC_PREGUNTA        = CR.IC_PREGUNTA  		"  +
									"AND CE.IC_ENCUESTA        = ?     						"  +
									"AND CR.IC_PREGUNTA        = ?     						"  +
									"AND CR.IC_TIPO_RESPUESTA IN (?)  						"  + // <--- Preguntas de respuesta libre
									"AND CR.IC_RESPUESTA       = CCR.IC_RESPUESTA 		"  +
									"AND CCR.IC_ENCUESTA_AFIL  = CEA.IC_ENCUESTA_AFIL 	"  +
									"ORDER BY ENCUESTADO 										"
								);
				ps = con.queryPrecompilado(query.toString());
				
				// Para cada Pregunta
				for(int x=0; x<repuestasSR.size();x++){
													
					String 	tipo_resp 		= "";  
					String 	ic_pregunta 	= "";   
					String 	rubro 			= "";  
					String 	cg_pregunta 	= ""; 
					String 	IC_NOPREGUNTA 	= "";
					
					List 		informacion 	= (ArrayList) repuestasSR.get(x);
													
					// Desplegar cada una de las preguntas de libre respuesta 
					for(int y=0; y<informacion.size();y++){
						
						List resdatos 	= (ArrayList)informacion.get(y);
							
						ic_pregunta  	= (String)resdatos.get(0);
						num_pregunta  	= (String)resdatos.get(1);
						desc_pregunta  = (String)resdatos.get(2);
						tipo_resp  		= (String)resdatos.get(3);
						IC_NOPREGUNTA  = (String)resdatos.get(4);
														
						// Agregar descripcion de la Pregunta de Respuesta Libre 
						//xml.write("\n" + IC_NOPREGUNTA +") "+ desc_pregunta );
						rowNum++;
						xml.write("<row r=\""+String.valueOf(rowNum)+"\" spans=\"1:3\" >");
						writeCellContent(xml, (IC_NOPREGUNTA +") "+ desc_pregunta), SUBTITULO, "A"+rowNum);
						xml.write("</row>");
														
						// Consultar respuestas a la pregunta en cuestion
						ps.clearParameters();
						ps.setLong(1,Long.parseLong(num_encuesta));
						ps.setLong(2,Long.parseLong(ic_pregunta));
						ps.setLong(3,Long.parseLong("1"));
						rs = ps.executeQuery();
					
						while(rs.next()){
							
							String	encuestado 		= rs.getString("ENCUESTADO")   == null?"":rs.getString("ENCUESTADO").trim();
							String	cg_respuesta2 	= rs.getString("CG_RESPUESTA") == null?"":rs.getString("CG_RESPUESTA").trim();
															
							if( "".equals(cg_respuesta2) || "null".equalsIgnoreCase(cg_respuesta2)) continue;
							//cg_respuesta2 		= "null".equalsIgnoreCase(cg_respuesta2)?"El usuario no especific� ninguna respuesta valida.":cg_respuesta2;
								
							// xml.write("\n"+encuestado +","+ cg_respuesta2);
							rowNum++;
							xml.write("<row r=\""+String.valueOf(rowNum)+"\">");
							writeCellContent(xml, encuestado, 		ESTILO_NORMAL, "B"+rowNum);
							writeCellContent(xml, cg_respuesta2, 	ESTILO_NORMAL, "C"+rowNum);
							xml.write("</row>");
						}
														
						rs.close();
							
					}
				}
					
				ps.close();	
					
			}
			
			// Si no hay registros mostrar mensaje 
			if( repuestasR.size() == 0 && repuestasSR.size() == 0 ){ 
				// xml.write("No existen Registros");
				rowNum++;
				xml.write("<row r=\""+String.valueOf(rowNum)+"\">");
				writeCellContent(xml, "No existen Registros", ESTILO_NORMAL, "A"+rowNum);
				xml.write("</row>");
			}
		
			xml.write("</sheetData>");
			xml.write("</worksheet>");
			
		} catch (Exception e) {
			System.out.println("15admresp03strxls.jsp(Exception)");
			e.printStackTrace();
			if(xml 	!= null )	try{xml.close();}catch(Exception sub){};
			throw e;
		} finally {
			// Cerrar archivo
			if(xml 	!= null )	try{xml.close();}catch(Exception e){};
		}
		
		// 3. AGREGAR LAS HOJAS NUEVAS A LA PLANTILLA
		List 	lista 			= new ArrayList();
		
		HashMap updatedFile1 = new HashMap();
		updatedFile1.put("NAME", "xl/worksheets/sheet1.xml");
		updatedFile1.put("PATH", directorioTemporal+hoja1);
		lista.add(updatedFile1);
		
		HashMap updatedFile2 = new HashMap();
		updatedFile2.put("NAME", "xl/worksheets/sheet2.xml");
		updatedFile2.put("PATH", directorioTemporal+hoja2);
		lista.add(updatedFile2);
 
		archivoXLSX				= archivo.nombreArchivo()+".xlsx";
		ComunesZIP.updateFiles(directorioPlantilla+"15admresp03.template.xlsx", lista, directorioTemporal+archivoXLSX);
  
		// 4. REALIZAR EL STREAMING DEL ARCHIVO
		OutputStream 			stream 	= null;
		PrintStream 			info 		= null;
		File 						f 			= null;
		BufferedInputStream 	in 		= null; 
		byte[] 					buffer 	= new byte[1024 * 256];  
		int 						r 			= 0; 
			
		try { 
				
			f 			= new File(directorioTemporal+archivoXLSX);
			in 		= new BufferedInputStream(new FileInputStream(f));
			stream 	= response.getOutputStream();
					
			info		= new PrintStream(stream);
			while ((r = in.read(buffer, 0, buffer.length)) != -1) { 
				info.write(buffer, 0, r); 
				stream.flush();
			}
			stream.flush();
				
		} catch(Exception e) {
			e.printStackTrace();		
		} finally {
			if(stream != null) 	stream.close();
			if(in != null) 		in.close(); 
		} 
		
	}catch(Exception e){
		
		System.out.println("15admresp03strxls.jsp(Exception)");
		System.out.println("15admresp03strxls.jsp.num_encuesta = <"+num_encuesta+">");
		System.out.println("15admresp03strxls.jsp.tipoEncuesta = <"+tipoEncuesta+">");
 
		e.printStackTrace();
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Encuesta_Consolidadas y Libres.xlsx");
		String mensaje = "<html>"+
								"	<head>"+
								"		<title>Mensaje del Sistema</title>"+
								"	</head>"+
								"	<body>"+
								"	El Sistema est� experimentando dificultades t�cnicas. Intente m�s tarde."+
								"	</body>"+
								"</html>";
		out.println(mensaje);
		
	}finally{
		
		if(ps != null)  try { ps.close();  }catch(Exception e){}
		if(rs != null)	 try { rs.close();  }catch(Exception e){}  
		
		if(ps1 != null) try { ps1.close(); }catch(Exception e){}
		if(rs1 != null) try { rs1.close(); }catch(Exception e){}
		
		if(con.hayConexionAbierta()) 
			con.cierraConexionDB();
	}
	
%><%!
	
	public String getDescripcionEncuesta(String claveEncuesta)
		throws AppException{
		
		System.out.println("15admresp03strxls.jsp::getTipoFondeo(E)");
		 
		 AccesoDB 				con  				= new AccesoDB();
		 StringBuffer 			qrySentencia 	= new StringBuffer();
		 ResultSet 				rs 				= null;
		 PreparedStatement 	ps		 			= null;
		 
		 String					descripcion		= "";

		 try {
			 
		 	con.conexionDB();

			qrySentencia.append(
					"SELECT                   "  +
					"	CG_TITULO AS ENCUESTA  "  +
					"FROM                     "  +
					"	COM_ENCUESTA           "  +
					"WHERE						  "  +	
					"	 IC_ENCUESTA       = ? "
			);
			
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setLong(1,Long.parseLong(claveEncuesta));
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				descripcion = rs.getString("ENCUESTA");
			}
			descripcion = descripcion == null || descripcion.trim().equals("")?"":descripcion.trim();
			
		 } catch(Exception e) {
			System.out.println("15admresp03strxls.jsp::getTipoFondeo(Exception)");
			System.out.println("15admresp03strxls.jsp::getTipoFondeo.claveEncuesta = <"+claveEncuesta+">");
			e.printStackTrace();
			throw new AppException("Error al obtener la descripcion de la encuesta");
		 } finally {
		 	if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("15admresp03strxls.jsp::getTipoFondeo(S)");
		 }
		 return descripcion;
	}
	
	public String getNombreEpoEncuestaPyme(String claveEncuesta)
		throws AppException{
		
		System.out.println("15admresp03strxls.jsp::getNombreEpoEncuestaPyme(E)");
		 
		 AccesoDB 				con  				= new AccesoDB();
		 StringBuffer 			qrySentencia 	= new StringBuffer();
		 ResultSet 				rs 				= null;
		 PreparedStatement 	ps		 			= null;
		 
		 String					nombreEpo		= "";

		 try {
			 
		 	con.conexionDB();

			qrySentencia.append(
					"SELECT                              "  +
					"	EPO.CG_RAZON_SOCIAL AS NOMBRE_EPO "  +
					"FROM                                "  +
					"	COMREL_ENCUESTA_AFILIADO REL,     "  +
					"	COMCAT_EPO               EPO      "  +
					"WHERE                               "  +
					"	REL.IC_EPO      = EPO.IC_EPO AND  "  + 
					"	REL.IC_ENCUESTA = ?               "
			);
			
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setLong(1,Long.parseLong(claveEncuesta));
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				nombreEpo = rs.getString("NOMBRE_EPO");
			}
			nombreEpo = nombreEpo == null || nombreEpo.trim().equals("")?"":nombreEpo.trim();
			
		 } catch(Exception e) {
			System.out.println("15admresp03strxls.jsp::getNombreEpoEncuestaPyme(Exception)");
			System.out.println("15admresp03strxls.jsp::getNombreEpoEncuestaPyme.claveEncuesta = <"+claveEncuesta+">");
			e.printStackTrace();
			throw new AppException("Error al Nombre de la EPO");
		 } finally {
		 	if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("15admresp03strxls.jsp::getNombreEpoEncuestaPyme(S)");
		 }
		 return nombreEpo;
	}
	
	public StringBuffer escapaCaracteresEspeciales(String consulta){
		
		StringBuffer resultado = new StringBuffer();
		
		if( consulta == null || consulta.length() == 0){
			return resultado;
		}
		
		for(int i=0;i<consulta.length();i++){
			char c = consulta.charAt(i);
			if(c == '<'){
				resultado.append("&lt;");
			}else if(c == '>'){
				resultado.append("&gt;");
			}else if(c == '"'){
				resultado.append("&quot;");
			}else if(c == '&'){
				resultado.append("&amp;");
			}else{
				resultado.append(c);
			}
		}
		
		return resultado;
		
	}
	
	/*
	
			xml.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			xml.write("<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">");
			xml.write("<sheetData>");
			
			rowNum++;
			xml.write("<row r=\""+String.valueOf(rowNum)+"\">");
			writeCellContent(xml, Rubro, TITULO, "A"+rowNum,"1:3");
			xml.write("</row>");
							
			xml.write("</sheetData>");
			xml.write("</worksheet>");
	*/
	public void writeCellContent(BufferedWriter xml, String cellContent, int style, String columnName) 
      throws Exception{

		if(style ==  0){ // ESTILO_NORMAL
			xml.write("<c r=\"");
			xml.write(columnName);
			xml.write("\" t=\"inlineStr\" >"); // <--- Sin estilo
		}else if(style ==  1){ // SUBTITULO
			xml.write("<c r=\"");
			xml.write(columnName);
			xml.write("\" t=\"inlineStr\" s=\"6\" >");
		}else if(style ==  2){ // TITULO
			xml.write("<c r=\"");
			xml.write(columnName);
			xml.write("\" t=\"inlineStr\" s=\"5\" >");
		}
		
		xml.write("<is>");
		xml.write("<t>");
		xml.write(escapaCaracteresEspeciales(cellContent).toString());
		xml.write("</t>");
		xml.write("</is>");
		xml.write("</c>");
		
	}
	
%>