
Ext.onReady(function() {
	
	
	var noSolicitud = [];	
	var productos = [];
		
	var cancelar =  function() {
		noSolicitud = [];		
		productos = [];
	}
	
	
	function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function transmiteProceso(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
				
			Ext.getCmp('mensajeA').show();			
			Ext.getCmp('mensaje').setValue(info.mensaje);
						
			Ext.getCmp('forma').hide();
			Ext.getCmp('gridConsulta').hide();
			Ext.getCmp('fpBotones').hide();
			Ext.getCmp('fpBotonesAcuse').show();
			
			Ext.getCmp('solicitudes').setValue(info.solicitudes);		
			fp.el.mask('Enviando...', 'x-mask-loading');			
			consProcesadosData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'ConsultarProcesados',
					solicitudes:info.solicitudes
				})
			});
					
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	var procesarCancelar = function() {
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();		
		var noRegistros = 0;
			
		cancelar();
		store.each(function(record) {				
			if(record.data['SELECCION']=='S' ){	
				noRegistros++;				
				noSolicitud.push(record.data['NU_SOLICITUD']);
				productos.push(record.data['PRODUCTO']);					
			}
		});
				
		if(noRegistros ==0) {
			Ext.MessageBox.alert('Error de validaci�n','Es necesario seleccionar un registro para continuar');
			return;		
		}
		
		if(noRegistros >100) {
			Ext.MessageBox.alert('Error de validaci�n','Favor de seleccionar un m�ximo de 100 registros para cancelar');
			return;		
		}
		
		Ext.Msg.show({
			title:	"Confirmaci�n",
			msg:		'Este proceso cambiara el estatus de las solicitudes seleccionadas �Desea continuar?',
			buttons:	Ext.Msg.OKCANCEL,
			fn: function resultMsj(btn){
				if(btn =='ok'){
				
					Ext.Ajax.request({
						url: '15canccont.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'procesarCancelar',
							noSolicitud:noSolicitud,
							productos:productos,
							noRegistros:noRegistros
						})
						,callback: transmiteProceso
					});
				}
			}
		});	
	}
	
	var fpBotonesAcuse = {
		xtype:     	'panel',
		id: 'fpBotonesAcuse',	
		hidden:true,
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [				
			{
				text: 'Imprimir',
				iconCls: 'icoPdf',
				xtype: 'button',
				id: 'btnImprimir',
				handler: function(boton, evento) {
					Ext.Ajax.request({
						url: '15canccont.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'archivo'								
						}),
						callback: procesarArchivos
					});
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				text: 'Regresar',
				iconCls: 'icoLimpiar',
				xtype: 'button',
				id: 'btnRegresar',				
				handler: function() {
					window.location = '15canccontExt.jsp';
				}
			}
		]
	};
	
	var mensajeA = new Ext.form.FormPanel({
		id: 'mensajeA',
		width: 900,
		style: 'margin:0 auto;',
		title: '',
		hidden: true,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 0,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items:[
			{ 	xtype: 'displayfield',  id: 'mensaje', 	value: '' }			
		]
	});
	
	//****************GRID DE REGISTROS PROCESADOS **********************************	
	var procesarConsProData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridProcesados = Ext.getCmp('gridProcesados');	
		var el = gridProcesados.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridProcesados.isVisible()) {
				gridProcesados.show();
			}	
			//edito el titulo de la columna  
			var jsonData = store.reader.jsonData;		
			var cm = gridProcesados.getColumnModel();			
			var el = gridProcesados.getGridEl();
			
			var barraPagina = Ext.getCmp('barraPaginaP');								
			Ext.getCmp('barraPaginaP').setText('Total Solicitudes Canceladas: '+jsonData.numRegistros);
			
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {			
				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consProcesadosData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15canccont.data.jsp',
		baseParams: {
			informacion: 'ConsultarProcesados'
		},		
		fields: [				
			{  name: 'PRODUCTO'},
			{  name: 'NOMBREPRODUCTO'},
			{  name: 'INTERMEDIARIO'},
			{  name: 'NOMBRE_EPO'},
			{  name: 'NOMBRE_PYME'},
			{  name: 'NU_SOLICITUD'},
			{  name: 'MONEDA'},
			{  name: 'TIPO_FACTORAJE'},
			{  name: 'FECHA_OPERACION'},
			{  name: 'NUM_PRESTAMO'},
			{  name: 'MONTO_OPERAR'},			
			{  name: 'ESTATUS_ACTUAL'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsProData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsProData(null, null, null);					
				}
			}
		}		
	});
	
	
	var gridProcesados = new Ext.grid.EditorGridPanel({	
		store: consProcesadosData,
		id: 'gridProcesados',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Producto',
				tooltip: 'Producto',
				dataIndex: 'NOMBREPRODUCTO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},
			{
				header: 'Intermediario',
				tooltip: 'Intermediario',
				dataIndex: 'INTERMEDIARIO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'N�mero Solicitud',
				tooltip: 'N�mero Solicitud',
				dataIndex: 'NU_SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},
			{
				header: 'Tipo de Factoraje',
				tooltip: 'Tipo de Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Fecha Operaci�n SIRAC',
				tooltip: 'Fecha Operaci�n SIRAC',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'N�mero de Pr�stamo',
				tooltip: 'N�mero de Pr�stamo',
				dataIndex: 'NUM_PRESTAMO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPERAR',
				sortable: true,
				width: 150,			
				resizable: true,					
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Estatus Actual',
				tooltip: 'Estatus Actual',
				dataIndex: 'ESTATUS_ACTUAL',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			}
		],					
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false,
			bbar: {
			xtype: 'toolbar',
			id: 'barraPaginaP',		
			items: [	
				{
					xtype: 'label',
					id:    'barraPaginaP',
					html:  'Total Solicitudes:',
					style: 'font-weight:bold;padding-bottom:10pt;text-align:center'
				}
			]
		}
	});
	
	
	//******************* CONSULTA  PRINCIPAL **************************************
	var procesarConsData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			//edito el titulo de la columna  
			var jsonData = store.reader.jsonData;		
			var cm = gridConsulta.getColumnModel();			
			var el = gridConsulta.getGridEl();
			
			var barraPagina = Ext.getCmp('barraPagina');								
			Ext.getCmp('barraPagina').setText('Total Solicitudes: '+jsonData.numRegistros);					
			
			if(store.getTotalCount() > 0) {	
				Ext.getCmp('btnCancelar').enable();
				el.unmask();					
			} else {			
					Ext.getCmp('btnCancelar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15canccont.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [				
			{  name: 'PRODUCTO'},
			{  name: 'NOMBREPRODUCTO'},
			{  name: 'INTERMEDIARIO'},
			{  name: 'NOMBRE_EPO'},
			{  name: 'NOMBRE_PYME'},
			{  name: 'NU_SOLICITUD'},
			{  name: 'MONEDA'},
			{  name: 'TIPO_FACTORAJE'},
			{  name: 'FECHA_OPERACION'},
			{  name: 'NUM_PRESTAMO'},
			{  name: 'MONTO_OPERAR'},			
			{  name: 'ESTATUS_ACTUAL'},			
			{  name: 'SELECCION'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsData(null, null, null);					
				}
			}
		}		
	});
	
	// *****************para realizar validaciones al seleccionar 			
	var selectModel = new Ext.grid.CheckboxSelectionModel( {
		checkOnly: true,
		listeners: {
			//Para quitar lo selecccionado en el documentos ******************************************
			rowdeselect: function(selectModel, rowIndex, record) {	 
				
				record.data['SELECCION']='N';				
				record.commit();				
			},	
			//Para selecccionar los documentos ******************************************
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //Se activa cuando una fila est� seleccionada, return false para cancelar.
				
				record.data['SELECCION']='S';				
				record.commit();				
			}
		}
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Producto',
				tooltip: 'Producto',
				dataIndex: 'NOMBREPRODUCTO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},
			{
				header: 'Intermediario',
				tooltip: 'Intermediario',
				dataIndex: 'INTERMEDIARIO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'N�mero Solicitud',
				tooltip: 'N�mero Solicitud',
				dataIndex: 'NU_SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},
			{
				header: 'Tipo de Factoraje',
				tooltip: 'Tipo de Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Fecha Operaci�n SIRAC',
				tooltip: 'Fecha Operaci�n SIRAC',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'N�mero de Pr�stamo',
				tooltip: 'N�mero de Pr�stamo',
				dataIndex: 'NUM_PRESTAMO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPERAR',
				sortable: true,
				width: 150,			
				resizable: true,					
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Estatus Actual',
				tooltip: 'Estatus Actual',
				dataIndex: 'ESTATUS_ACTUAL',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			selectModel
		],	
		sm:selectModel,
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,	
		bbar: {
			xtype: 'toolbar',
			id: 'barraPagina',		
			items: [	
				{
					xtype: 'label',
					id:    'barraPagina',
					html:  'Total Solicitudes:',
					style: 'font-weight:bold;padding-bottom:10pt;text-align:center'
				},
				'->',
				'-',
				{
					text: 'Cancelar',
					iconCls: 'cancelar',
					xtype: 'button',
					id: 'btnCancelar',	
					handler:procesarCancelar
				}
			]
		}
	});
	
		
	
	//***************Criterios de Busqueda*****************
	var catalogoProducto = new Ext.data.JsonStore({
		id: 'catalogoProducto',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15canccont.data.jsp',
		baseParams: {
			informacion: 'catalogoProducto'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15canccont.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma =  [
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			name: 'ic_producto',
			id: 'ic_producto1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_producto',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoProducto,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120
		},
		{
			xtype: 'compositefield',			
			fieldLabel: 'N�mero de Pr�stamo ',
			combineErrors: false,
			msgTarget: 'side',		
			items: [	
				{
					xtype: 'textarea',
					fieldLabel: 'N�mero de Pr�stamo ',		
					name: 'ig_numero_prestamo',
					id: 'ig_numero_prestamo1',
					allowBlank: true,					
					width: 200,
					msgTarget: 'side',
					maxLength	: 100,
					margins: '0 20 0 0'
				}
			]
		},			
		{
			xtype: 'combo',
			fieldLabel: 'Nombre del Intermediario',
			name: 'ic_if',
			id: 'ic_if1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_if',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoIF,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120
		},
		{
			xtype: 'compositefield',			
			fieldLabel: 'Fecha de Operaci�n',
			combineErrors: false,
			msgTarget: 'side',		
			items: [				
				{
					xtype: 'datefield',
					name: 'df_operacion',
					id: 'df_operacion',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',							
					margins: '0 20 0 0' //necesario para mostrar el icono de error
				}
			]
		},
		{ 	xtype: 'textfield', hidden: true,  id: 'solicitudes', 	value: '' }
	];
	
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: 'margin:0 auto;',
		title: 'Cancelaci�n X Contingencia -CAPTURA',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 160,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [		
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
					
					var ic_producto = Ext.getCmp("ic_producto1");
					if (Ext.isEmpty(ic_producto.getValue()) ){
						ic_producto.markInvalid('Debe de seleccionar un Producto');
						return;
					}
					
					var df_operacion = Ext.getCmp("df_operacion");	
					var df_operacion_ = Ext.util.Format.date(df_operacion.getValue(),'d/m/Y');

					if(!Ext.isEmpty(df_operacion.getValue())){
						if(!isdate(df_operacion_)) { 
							df_operacion.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_operacion.focus();
							return;
						}
					}
					var ig_numero_prestamo = Ext.getCmp("ig_numero_prestamo1").getValue();
					ig_numero_prestamo.split("\n");		
					
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar',
							ig_numero_prestamo:ig_numero_prestamo
						})
					});
					
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',				
				//formBind: true,				
				handler: function(boton, evento) {	
						document.location.href  = 	'15canccontExt.jsp';
				}
			}
		]
	});	


	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [			
			{
				xtype: 'button',				
				text: '<h1><b>Captura</h1></b>',	
				id: 'btnCap',					
				handler: function() {
					window.location = '15canccontExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Consulta',			
				id: 'btnCons2',					
				handler: function() {
					window.location = '15canccontbExt.jsp';
				}
			}
		]
	};

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fpBotones,
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,	
			gridProcesados,
			mensajeA,
			fpBotonesAcuse,
			NE.util.getEspaciador(20)
		]
	});
	
	catalogoProducto.load();
	catalogoIF.load();

});	
