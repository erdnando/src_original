Ext.onReady(function(){

//-------------Handlers-------------
	
	var escondeBoton =0;
	var estatusActualApi;
	var estatusActualProducto;
	
	var esconderBtn= function(){
		if(escondeBoton>0){
			Ext.getCmp('btnGenerarArchivo').enable();
		}else{
			Ext.getCmp('btnGenerarArchivo').disable();
		}
	}
				
	var procesarTodos = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "T", 
		descripcion: "Todos", 
		loadMsg: ""})); 
		store.commitChanges(); 

		Ext.getCmp('idComboProducto').setValue('T');
		Ext.getCmp('idComboApi').setValue('T');
	
		estatusActualApi = Ext.getCmp('idComboApi').getValue();
		estatusActualProducto = Ext.getCmp('idComboProducto').getValue();
	}


	var procesarEncabezados = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			var gridProyecto = Ext.getCmp('gridProyecto');
			var gridDs = Ext.getCmp('gridDisposiciones');
			var gridC = Ext.getCmp('gridContratos');
			var gridPres = Ext.getCmp('gridPrestamos');
			
			setEncabezados(gridProyecto,info);
			setEncabezados(gridDs,info);
			setEncabezados(gridC,info);
			setEncabezados(gridPres,info);
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}

	
	var setEncabezados= function(grid, info){
		var cm = grid.getColumnModel();
		
		grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_DESCPROD1'),'<center>'+info.campo1+'</center>');	
		grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_DESCPROD2'),'<center>'+info.campo2+'</center>');	
		grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_DESCPROD3'),'<center>'+info.campo3+'</center>');	
		grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_DESCPROD4'),'<center>'+info.campo4+'</center>');	
		grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_DESCPROD5'),'<center>'+info.campo5+'</center>');	
		grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_DESCPROD6'),'<center>'+info.campo6+'</center>');
		grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_DESCPROD7'),'<center>'+info.campo7+'</center>');	
		grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_DESCPROD8'),'<center>'+info.campo8+'</center>');	
		grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_DESCPROD9'),'<center>'+info.campo9+'</center>');	
		grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CG_DESCPROD10'),'<center>'+info.campo10+'</center>');
		
	}


	var procesarDatosDS = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('gridDisposiciones');	
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
				escondeBoton ++;
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
				esconderBtn();
			}
	}
	
	
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('gridProyecto');	
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				escondeBoton ++;
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			esconderBtn();
		}
	}
	
	var procesarDatosC = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('gridContratos');	
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				escondeBoton ++;
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
				}
				esconderBtn();
			}
	}
	
	
	var procesarDatosPre = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('gridPrestamos');	
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				escondeBoton ++;
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			esconderBtn();
		}
	}
	
	
	var procesarInicio = function (){
		consultaDataGrid.load({
		params: {comboProducto:"T",
					comboApi:"PY" }
		});
					
		consultaDataGridCN.load({
		params: {comboProducto:"T",
					comboApi:"CN" }
		});
					
		consultaDataGridPR.load({
		params: {comboProducto:"T",
					comboApi:"PR" }
		});
					
					
		consultaDataGridDS.load({
		params: {comboProducto:"T",
					comboApi:"DS" }
		});
		
	}
	

	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton = Ext.getCmp('btnGenerarArchivo');
			boton.enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var limpiar = function(grid){
		var cm = grid.getColumnModel();
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD1'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD2'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD3'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('IG_PARAM_CAMPO'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD4'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD5'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD6'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD7'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD8'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD9'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD10'), true);
	}


	var mostrar = function(grid){
		var cm = grid.getColumnModel();
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD1'), false);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD2'), false);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD3'), false);
		grid.getColumnModel().setHidden(cm.findColumnIndex('IG_PARAM_CAMPO'), true);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD4'), false);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD5'), false);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD6'), false);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD7'), false);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD8'), false);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD9'), false);
		grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESCPROD10'), false);
	}


	var getNombreColumna = function(valor){
		if (valor =='1A')
			return 'CG_DESCPROD1'
		
		if (valor =='1B')
			return 'CG_DESCPROD2'
		
		if (valor =='1C')
			return 'CG_DESCPROD3,IG_PARAM_CAMPO'
				
		if (valor =='1D')
			return 'CG_DESCPROD4'
		
		if (valor =='2A')
			return 'CG_DESCPROD5'
		
		if (valor =='0')
			return 'CG_DESCPROD6'
				
		if (valor =='5A')
			return 'CG_DESCPROD7'
		
		if (valor =='4A')
			return 'CG_DESCPROD8'
		
		if (valor =='4B')
			return 'CG_DESCPROD9'
		
		if (valor =='8')
			return 'CG_DESCPROD10'
	}


	var muestraApis = function()
	{
		var comboApi= Ext.getCmp('idComboApi');
		var gridR = Ext.getCmp('gridProyecto');
		var gridContratos = Ext.getCmp('gridContratos');
		var gridPrestamos = Ext.getCmp('gridPrestamos');
		var gridDisposiciones = Ext.getCmp('gridDisposiciones');
		var cm = gridR.getColumnModel();
		
		if(comboApi.getValue() == "T" ){
			consultaDataGrid.load({
				params: {comboProducto:Ext.getCmp('idComboProducto').getValue(),
							comboApi:"PY" }
			});
			gridR.show();
			
			consultaDataGridCN.load({
				params: {comboProducto:Ext.getCmp('idComboProducto').getValue(),
							comboApi:"CN" }
			});
			gridContratos.show();
			
			consultaDataGridPR.load({
				params: {comboProducto:Ext.getCmp('idComboProducto').getValue(),
							comboApi:"PR" }
			});
			gridPrestamos.show();
			
			consultaDataGridDS.load({
				params: {comboProducto:Ext.getCmp('idComboProducto').getValue(),
							comboApi:"DS" }
			});
			gridDisposiciones.show();
		}
		else{
			gridR.hide();
			gridContratos.hide();
			gridPrestamos.hide();
			gridDisposiciones.hide();
			if(comboApi.getValue()=="PY"){
				consultaDataGrid.load({
					params: {comboProducto:Ext.getCmp('idComboProducto').getValue(),
								comboApi:"PY" }
				});
				gridR.show();						
			}else if(comboApi.getValue()=="CN"){
				consultaDataGridCN.load({
					params: {comboProducto:Ext.getCmp('idComboProducto').getValue(),
								comboApi:"CN" }
				});
				gridContratos.show();
			}
			else if(comboApi.getValue()=="PR"){
				consultaDataGridPR.load({
					params: {comboProducto:Ext.getCmp('idComboProducto').getValue(),
								comboApi:"PR" }
				});
				gridPrestamos.show();				
			}
			else if(comboApi.getValue()=="DS"){
				consultaDataGridDS.load({
					params: {comboProducto:Ext.getCmp('idComboProducto').getValue(),
								comboApi:"DS" }
				});
				gridDisposiciones.show();
			}
		}
	} 



//-------------Stores----------------

	var CatalogoApis= new Ext.data.JsonStore({
		id: 'catApis',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15matrizapisExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoApis'
		},
		autoLoad: false, 
		listeners:
		{
		 load: procesarTodos,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	  });

	  
	Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
		

	var CatalogoProducto= new Ext.data.JsonStore
	  ({
			id: 'catProducto',
			root : 'registros',
			fields : ['clave', 'descripcion', 'loadMsg'],
			url : '15matrizapisExt.data.jsp',
			baseParams: 
			{
			 informacion: 'CatalogoProducto'
			},
			autoLoad: false,
			listeners:
			{
			 load: procesarTodos,
			 exception: NE.util.mostrarDataProxyError,
			 beforeload: NE.util.initMensajeCargaCombo
			}
	  });


	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15matrizapisExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'NUMEROPARAMETRO'},
			{name: 'NOMBREPARAMETRO'},
			{name: 'DESCPARAMETRO'},
			{name: 'CG_DESCPROD1'},
			{name: 'CG_DESCPROD2'},
			{name: 'CG_DESCPROD3'},
			{name: 'CG_DESCPROD4'},
			{name: 'CG_DESCPROD5'},
			{name: 'CG_DESCPROD6'},
			{name: 'CG_DESCPROD7'},
			{name: 'CG_DESCPROD8'},
			{name: 'CG_DESCPROD9'},
			{name: 'CG_DESCPROD10'},
			{name: 'IG_PARAM_CAMPO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	
	var consultaDataGridPR = new Ext.data.JsonStore({
		root : 'registros',
		url : '15matrizapisExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'NUMEROPARAMETRO'},
			{name: 'NOMBREPARAMETRO'},
			{name: 'DESCPARAMETRO'},
			{name: 'CG_DESCPROD1'},
			{name: 'CG_DESCPROD2'},
			{name: 'CG_DESCPROD3'},
			{name: 'CG_DESCPROD4'},
			{name: 'CG_DESCPROD5'},
			{name: 'CG_DESCPROD6'},
			{name: 'CG_DESCPROD7'},
			{name: 'CG_DESCPROD8'},
			{name: 'CG_DESCPROD9'},
			{name: 'CG_DESCPROD10'},
			{name: 'IG_PARAM_CAMPO'}		
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load:procesarDatosPre,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	


    var consultaDataGridCN = new Ext.data.JsonStore({
		root : 'registros',
		url : '15matrizapisExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'NUMEROPARAMETRO'},
			{name: 'NOMBREPARAMETRO'},
			{name: 'DESCPARAMETRO'},
			{name: 'CG_DESCPROD1'},
			{name: 'CG_DESCPROD2'},
			{name: 'CG_DESCPROD3'},
			{name: 'CG_DESCPROD4'},
			{name: 'CG_DESCPROD5'},
			{name: 'CG_DESCPROD6'},
			{name: 'CG_DESCPROD7'},
			{name: 'CG_DESCPROD8'},
			{name: 'CG_DESCPROD9'},
			{name: 'CG_DESCPROD10'},
			{name: 'IG_PARAM_CAMPO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load:procesarDatosC,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	
	var consultaDataGridDS = new Ext.data.JsonStore({
		root : 'registros',
		url : '15matrizapisExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'NUMEROPARAMETRO'},
			{name: 'NOMBREPARAMETRO'},
			{name: 'DESCPARAMETRO'},
			{name: 'CG_DESCPROD1'},
			{name: 'CG_DESCPROD2'},
			{name: 'CG_DESCPROD3'},
			{name: 'CG_DESCPROD4'},
			{name: 'CG_DESCPROD5'},
			{name: 'CG_DESCPROD6'},
			{name: 'CG_DESCPROD7'},
			{name: 'CG_DESCPROD8'},
			{name: 'CG_DESCPROD9'},
			{name: 'CG_DESCPROD10'},
			{name: 'IG_PARAM_CAMPO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load:procesarDatosDS,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
//--------------Componentes------------------
	var elementosForma = 
	[	
		{xtype: 'combo',
		fieldLabel: 'API',
		forceSelection: true,
		autoSelect: true,
		name:'claveApi',
		id:'idComboApi',
		displayField: 'descripcion',
		allowBlank: true,
		editable:true,
		valueField: 'clave',
		mode: 'local',
		triggerAction: 'all',
		autoLoad: false,
		typeAhead: true,
		minChars: 1,
		store: CatalogoApis,  
		width:120,
		
		listeners:{
			select: function(combo){
				if(estatusActualApi != Ext.getCmp('idComboApi').getValue()){
					estatusActualApi = Ext.getCmp('idComboApi').getValue();
					escondeBoton=0; 
					muestraApis();
				}
			}   
		}
	},{
	xtype: 'combo',
		fieldLabel: 'Producto',
		forceSelection: true,
		autoSelect: true,
		name:'claveProducto',
		id:'idComboProducto',
		displayField: 'descripcion',
		allowBlank: true,
		editable:true,
		valueField: 'clave',
		mode: 'local',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		store: CatalogoProducto,  
		width:215,
		listeners:{
			select: function(combo){
				if(estatusActualProducto != Ext.getCmp('idComboProducto').getValue()){
					estatusActualProducto = Ext.getCmp('idComboProducto').getValue();
					escondeBoton=0;
					var gridContratos = Ext.getCmp('gridContratos');
					var gridPrestamos = Ext.getCmp('gridPrestamos');
					var gridDisposiciones = Ext.getCmp('gridDisposiciones');
					
					var combopro= Ext.getCmp('idComboProducto');
					var gridR = Ext.getCmp('gridProyecto');
					var cm = gridR.getColumnModel();
					var cm2 = gridContratos.getColumnModel();
					var cm3 = gridPrestamos.getColumnModel();
					var cm4 = gridDisposiciones.getColumnModel();
					if(combopro.getValue()=='T'){
						mostrar(gridR);
						mostrar(gridContratos);
						mostrar(gridPrestamos);
						mostrar(gridDisposiciones);
	
					}else{
						limpiar(gridR);
						limpiar(gridContratos);
						limpiar(gridPrestamos);
						limpiar(gridDisposiciones);

						if(combopro.getValue() != '6A'){
							
							var nom = getNombreColumna(combopro.getValue());
							if(!combopro.getValue()=='1C'){
								gridR.getColumnModel().setHidden(cm.findColumnIndex(nom), false);
								gridContratos.getColumnModel().setHidden(cm2.findColumnIndex(nom), false);
								gridPrestamos.getColumnModel().setHidden(cm3.findColumnIndex(nom), false);
								gridDisposiciones.getColumnModel().setHidden(cm4.findColumnIndex(nom), false);
							}else{
								var campos = nom.split(',');
								if(campos.length>0){
									for(var i = 0; i<campos.length;i++){
										nom = campos[i];
										gridR.getColumnModel().setHidden(cm.findColumnIndex(nom), false);
										gridContratos.getColumnModel().setHidden(cm2.findColumnIndex(nom), false);
										gridPrestamos.getColumnModel().setHidden(cm3.findColumnIndex(nom), false);
										gridDisposiciones.getColumnModel().setHidden(cm4.findColumnIndex(nom), false);
									}
									
								}
							}
						}
					}
					muestraApis();
				}
			}
		}   
	
	}
	]



	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
		style: 'margin:0 auto;',	   
		height: 90,
		width: 430
	});



	var grid = new Ext.grid.GridPanel( {
		store: consultaDataGrid,
		title: '<center>Proyectos</center>',
		style: 'margin:0 auto;',
		id: 'gridProyecto',
		columns: [
			{
				header: 'N�mero<BR>de<BR>Par�metro ',
				tooltip: 'N�mero de Par�metro',
				dataIndex: 'NUMEROPARAMETRO',
				width: 70,
				align: 'center'				
			},{
				header: '<center>Nombre</center>',
				dataIndex: 'NOMBREPARAMETRO',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 150
			},{
				header: '<center>Nombre del Dato<BR>en la Forma</center>',
				dataIndex: 'DESCPARAMETRO',
				width: 150,
				tooltip: 'Nombre del dato en la forma',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},					
				align: 'left'				
			},{
				header: 'Campo1',
				dataIndex: 'CG_DESCPROD1',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo2',
				dataIndex: 'CG_DESCPROD2',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo3',
				dataIndex: 'CG_DESCPROD3',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: '<center>Campos Parametrizables<br>por Default</center>',
				dataIndex: 'IG_PARAM_CAMPO',
				align: 'center',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								if(value==''){
									return 'N/A';
								}else{
									metadata.attr = 'ext:qtip="' + value + '"';
									return value;
								}
								
							},				
				width: 150
			},{
				header: 'Campo4',
				dataIndex: 'CG_DESCPROD4',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo5',
				dataIndex: 'CG_DESCPROD5',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo6',
				dataIndex: 'CG_DESCPROD6',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo7',
				dataIndex: 'CG_DESCPROD7',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo8',
				dataIndex: 'CG_DESCPROD8',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo9',
				dataIndex: 'CG_DESCPROD9',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo10',
				dataIndex: 'CG_DESCPROD10',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			}
			],
			loadMask: true,
			style: 'margin:0 auto;',
			height: 500,
			width: 800,
			frame: true
	});



	var grid2 = new Ext.grid.GridPanel( {
		store: consultaDataGridCN,
		title: '<center>Contratos</center>',
		style: 'margin:0 auto;',
		id: 'gridContratos',
		columns: [
			{
				header: 'N�mero<BR>de<BR>Par�metro ',
				dataIndex: 'NUMEROPARAMETRO',
				tooltip: 'N�mero de Par�metro',
				width: 70,
				align: 'center'				
			},{
				header: '<center>Nombre</center>',
				dataIndex: 'NOMBREPARAMETRO',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 150
			},
				{
				header: '<center>Nombre del Dato<BR>en la Forma</center>',
				tooltip: 'Nombre del dato en la forma',
				dataIndex: 'DESCPARAMETRO',
				width: 150,
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},	
				align: 'left'				
			},{
				header: 'Campo1',
				dataIndex: 'CG_DESCPROD1',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo2',
				dataIndex: 'CG_DESCPROD2',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo3',
				dataIndex: 'CG_DESCPROD3',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},
				width: 380
			},{
				header: '<center>Campos Parametrizables<br>por Default</center>',
				dataIndex: 'IG_PARAM_CAMPO',
				align: 'center',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								if(value==''){
									return 'N/A';
								}else{
									metadata.attr = 'ext:qtip="' + value + '"';
									return value;
								}
								
							},				
				width: 150
			},{
				header: 'Campo4',
				dataIndex: 'CG_DESCPROD4',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo5',
				dataIndex: 'CG_DESCPROD5',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo6',
				dataIndex: 'CG_DESCPROD6',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo7',
				dataIndex: 'CG_DESCPROD7',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo8',
				dataIndex: 'CG_DESCPROD8',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo9',
				dataIndex: 'CG_DESCPROD9',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo10',
				dataIndex: 'CG_DESCPROD10',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			}
			],
			loadMask: true,
			style: 'margin:0 auto;',
			height: 500,
			width: 800,
			frame: true
	});


	var grid3 = new Ext.grid.GridPanel( {
		store: consultaDataGridPR,
		title: '<center>Prestamos</center>',
		style: 'margin:0 auto;',
		id: 'gridPrestamos',
		columns: [
			{
				header: 'N�mero<BR>de<BR>Par�metro ',
				dataIndex: 'NUMEROPARAMETRO',
				tooltip: 'N�mero de Par�metro',
				width: 70,
				align: 'center'				
			},{
				header: '<center>Nombre</center>',
				dataIndex: 'NOMBREPARAMETRO',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 150
			},{
				header: '<center>Nombre del Dato<BR>en la Forma</center>',
				dataIndex: 'DESCPARAMETRO',
				tooltip: 'Nombre del dato en la forma',
				width: 150,
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;				
							},
				align: 'left'				
			},{
				header: 'Campo1',
				dataIndex: 'CG_DESCPROD1',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo2',
				dataIndex: 'CG_DESCPROD2',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo3',
				dataIndex: 'CG_DESCPROD3',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: '<center>Campos Parametrizables<br>por Default</center>',
				dataIndex: 'IG_PARAM_CAMPO',
				align: 'center',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								if(value==''){
									return 'N/A';
								}else{
									metadata.attr = 'ext:qtip="' + value + '"';
									return value;
								}
								
							},				
				width: 150
			},{
				header: 'Campo4',
				dataIndex: 'CG_DESCPROD4',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo5',
				dataIndex: 'CG_DESCPROD5',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo6',
				dataIndex: 'CG_DESCPROD6',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo7',
				dataIndex: 'CG_DESCPROD7',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo8',
				dataIndex: 'CG_DESCPROD8',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo9',
				dataIndex: 'CG_DESCPROD9',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo10',
				dataIndex: 'CG_DESCPROD10',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			}
			],
			loadMask: true,
			style: 'margin:0 auto;',
			height: 500,
			width: 800,
			frame: true
	});


	var grid4 = new Ext.grid.GridPanel( {
		store: consultaDataGridDS,
		title: '<center>Disposiciones</center>',
		style: 'margin:0 auto;',
		id: 'gridDisposiciones',
		columns: [
			{
				header: 'N�mero<BR>de<BR>Par�metro ',
				dataIndex: 'NUMEROPARAMETRO',
				tooltip: 'N�mero de Par�metro',
				width: 70,
				align: 'center'				
			},{
				header: '<center>Nombre</center>',
				dataIndex: 'NOMBREPARAMETRO',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 150
			},{
				header: '<center>Nombre del Dato<BR>en la Forma</center>',
				dataIndex: 'DESCPARAMETRO',
				tooltip: 'Nombre del dato en la forma',
				width: 150,
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;				
							},
				align: 'left'				
			},{
				header: 'Campo1',
				dataIndex: 'CG_DESCPROD1',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},
				width: 380
			},{
				header: 'Campo2',
				dataIndex: 'CG_DESCPROD2',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo3',
				dataIndex: 'CG_DESCPROD3',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: '<center>Campos Parametrizables<br>por Default</center>',
				dataIndex: 'IG_PARAM_CAMPO',
				align: 'center',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								if(value==''){
									return 'N/A';
								}else{
									metadata.attr = 'ext:qtip="' + value + '"';
									return value;
								}
								
							},				
				width: 150
			},{
				header: 'Campo4',
				dataIndex: 'CG_DESCPROD4',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo5',
				dataIndex: 'CG_DESCPROD5',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo6',
				dataIndex: 'CG_DESCPROD6',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo7',
				dataIndex: 'CG_DESCPROD7',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo8',
				dataIndex: 'CG_DESCPROD8',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo9',
				dataIndex: 'CG_DESCPROD9',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			},{
				header: 'Campo10',
				dataIndex: 'CG_DESCPROD10',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								//metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
								return value;
							},				
				width: 380
			}
			],
			loadMask: true,
			style: 'margin:0 auto;',
			height: 500,
			width: 800,
			frame: true
	});


	var botonCSV={
		xtype:'button',
		id: 'btnGenerarArchivo',
		width: 50,
		weight: 20,
		text: ' Generar Archivo',
		style: 'margin:0 auto;',
		iconCls: 'icoXls',
		handler: function(boton, evento) {												
			boton.disable();
			Ext.Ajax.request({
				url: '15matrizapisExt.data.jsp',
				params: {comboProducto:Ext.getCmp('idComboProducto').getValue(),
							comboApi:Ext.getCmp('idComboApi').getValue(),
							informacion:'ArchivoCSV'
				},	
				callback: procesarDescargaArchivos
			});						
		}	
	};


	//Contenedor Principal
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [fp,NE.util.getEspaciador(10),grid
		,NE.util.getEspaciador(10),grid2
		,NE.util.getEspaciador(10),grid3
		,NE.util.getEspaciador(10),grid4
		,NE.util.getEspaciador(15),botonCSV ]
	});

	
	CatalogoApis.load();
	CatalogoProducto.load();
	
	Ext.Ajax.request({
		url: '15matrizapisExt.data.jsp',
		params: {
			informacion: 'ConsultaEncabezados'
		},
		callback: procesarEncabezados
	});	
	
	procesarInicio();

});