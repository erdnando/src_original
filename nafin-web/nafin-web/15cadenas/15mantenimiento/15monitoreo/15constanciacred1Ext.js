Ext.onReady(function() {

	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(infoR.mensaje !='')  {
				Ext.MessageBox.alert('Mensaje',infoR.mensaje);
			}else {
				var archivo = infoR.urlArchivo;				
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		
	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('txtfechaIni').setValue(info.fechaHoy);
			Ext.getCmp('txtfechaFin').setValue(info.fechaHoy);
						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var elementosForma =[
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha',
			combineErrors: false,
			msgTarget: 'side',
			width: 550,
			items: [
				{
					xtype: 'displayfield',
					value: 'De',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txtfechaIni',
					id: 'txtfechaIni',
					allowBlank: true,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtfechaFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'Hasta',
					width: 40
				},
				{
					xtype: 'datefield',
					name: 'txtfechaFin',
					id: 'txtfechaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 					
					campoInicioFecha: 'txtfechaIni',
					margins: '0 20 0 0' 
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 20
				}
			]
		}
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 550,
		style: 'margin:0 auto;',
		title: 'Total del d�a',
		hidden: false,
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [	
			{
				text: 'Generar Archivo',
				id: 'btnGenArc',									
				formBind: true,				
				handler: function(boton, evento) {		
				
					var txtfechaIni = Ext.getCmp("txtfechaIni");
					var txtfechaFin = Ext.getCmp("txtfechaFin");	
					
					if(Ext.isEmpty(txtfechaIni.getValue()) ||  Ext.isEmpty(txtfechaFin.getValue())){
						if(Ext.isEmpty(txtfechaIni.getValue())){
							txtfechaIni.markInvalid('Debe capturar ambas fechas');
							txtfechaIni.focus();
							return;
						}
						if(Ext.isEmpty(txtfechaFin.getValue())){
							txtfechaFin.markInvalid('Debe capturar ambas fechas');
							txtfechaFin.focus();
							return;
						}
					}
					
					
					var txtfechaIni_ = Ext.util.Format.date(txtfechaIni.getValue(),'d/m/Y');
					var txtfechaFin_ = Ext.util.Format.date(txtfechaFin.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(txtfechaIni.getValue())){
						if(!isdate(txtfechaIni_)) { 
							txtfechaIni.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txtfechaIni.focus();
							return;
						}
					}
					if( !Ext.isEmpty(txtfechaFin.getValue())){
						if(!isdate(txtfechaFin_)) { 
							txtfechaFin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txtfechaFin.focus();
							return;
						}
					}

					
					Ext.Ajax.request({
						url: '15constanciacredExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Total_dia'
						}),							
						callback: descargaArchivo
					});	
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',									
				formBind: true,				
				handler: function(boton, evento) {	
					document.location.href  = 	'15constanciacredExt.jsp?pantalla=Total_dia';
				}
			}
		]
	});	


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),				
			fp,
			NE.util.getEspaciador(20),			
			NE.util.getEspaciador(20)
		]
	});

	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url : '15constanciacredExt.data.jsp',
		params: {
			informacion: "valoresIniciales"			
		},
		callback: procesaValoresIniciales
	});	

	
});