<%@ page contentType="application/json;charset=UTF-8"
	import="
		com.netro.model.catalogos.*,		
		com.netro.cadenas.*,		
		java.sql.*,	
		java.util.*,
		netropology.utilerias.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"				
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String ic_producto = (request.getParameter("ic_producto") != null)?request.getParameter("ic_producto"):"";
String ic_if = (request.getParameter("ic_if") != null)?request.getParameter("ic_if"):"";
String ic_estatus = (request.getParameter("ic_estatus") != null)?request.getParameter("ic_estatus"):"";
String tipoArchivo = (request.getParameter("tipoArchivo") != null)?request.getParameter("tipoArchivo"):"";
String infoRegresar = "", consulta = "", estatusCombo = "1,2,3,4";

if(ic_producto.equals("0")||ic_producto.equals("")){
estatusCombo = "1,2,3,4,12";
}

JSONObject jsonObj = new JSONObject();
HashMap datos =  new HashMap();
JSONArray  registros  = new JSONArray();
List registrosImp  = new ArrayList();
List registrosImpT  = new ArrayList();

HashMap datosTotales =  new HashMap();
JSONArray  registrosTotales  = new JSONArray();


interfasesMonitorResOperBean paginador =   new interfasesMonitorResOperBean();
paginador.setIc_producto(ic_producto);

if(informacion.equals("catalogoProducto")) {
	
	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_producto_nafin");
	catalogo.setCampoDescripcion("ic_nombre");
	catalogo.setTabla("comcat_producto_nafin");
	catalogo.setOrden("ic_producto_nafin");
	catalogo.setValoresCondicionIn("0,1,4", Integer.class);
	infoRegresar = catalogo.getJSONElementos();
	
} else  if(informacion.equals("catalogoIF")) {

	String ids = paginador.getIntermediarios(ic_producto);
	if(ids.equals("")) { 	ids = "0"; 	}

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setTabla("comcat_if");
	catalogo.setOrden("ic_if");
	catalogo.setValoresCondicionIn(ids, Integer.class);
	infoRegresar = catalogo.getJSONElementos();

} else  if(informacion.equals("catalogoEstatus")) {


	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_estatus_solic");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_estatus_solic");
	catalogo.setOrden("cd_descripcion");
	catalogo.setValoresCondicionIn(estatusCombo, Integer.class);
	infoRegresar = catalogo.getJSONElementos();

} else  if(informacion.equals("Consulta") || informacion.equals("ConsultaTotales") || informacion.equals("Archivos") ) {
	Vector vecColumnas = null;
	Vector vecFilas = paginador.getConsultaOperaciones(ic_producto,ic_if,ic_estatus);
	int totalDocMN = 0, totalDocDL = 0;
	double totalMN = 0, totalDL = 0;
	double totalOpMN = 0, totalOpDL = 0;
	
	for (int i=0;i<vecFilas.size();i++){
	
		vecColumnas = (Vector) vecFilas.get(i);
		String IcIf 	= (String)vecColumnas.get(0);
		String NomIf 	= (String)vecColumnas.get(1);
		String Estatus 	= (String)vecColumnas.get(2);
		String IcMoneda = (String)vecColumnas.get(3);
		String Moneda 	= (String)vecColumnas.get(4);
		String NumSol 	= (String)vecColumnas.get(5);
		String Monto 	= (String)vecColumnas.get(6);
		String MontoOp 	= (String)vecColumnas.get(7);
		String Modalidad = "";
		if("8".equals(ic_producto))
			Modalidad 	= (String)vecColumnas.get(8);

		if(IcMoneda.equals("1")){
			totalMN += Double.parseDouble(Monto);
			totalOpMN += Double.parseDouble(MontoOp);
			totalDocMN += Integer.parseInt(NumSol);
		} else {
			totalDL += Double.parseDouble(Monto);
			totalOpDL += Double.parseDouble(MontoOp);
			totalDocDL += Integer.parseInt(NumSol);
		}
	
		datos =  new HashMap();
		datos.put("MODALIDAD", Modalidad);
		datos.put("CLAVE_IF", IcIf);
		datos.put("NOMBRE_IF", NomIf);
		datos.put("ESTATUS", Estatus);
		datos.put("MONEDA", Moneda);
		datos.put("MONTO_DOCTO", Monto);
		datos.put("MONTO_OPERADO", MontoOp);
		datos.put("TOTAL_SOLICITUDES", NumSol);
		registros.add(datos);
		registrosImp.add(datos);
	}
	
	datosTotales =  new HashMap();
	datosTotales.put("VACIO",  "");
	datosTotales.put("MONEDA",  "Total Moneda Nacional");
	datosTotales.put("MONTO_DOCTO",  Double.toString (totalMN));
	datosTotales.put("MONTO_OPERADO",  Double.toString (totalOpMN));
	datosTotales.put("TOTAL_SOLICITUDES",  String.valueOf(totalDocMN));
	registrosTotales.add(datosTotales);
	registrosImpT.add(datosTotales);
	
	
	datosTotales =  new HashMap();
	datosTotales.put("VACIO",  "");
	datosTotales.put("MONEDA",  "Total Dólares");
	datosTotales.put("MONTO_DOCTO",  Double.toString (totalDL));
	datosTotales.put("MONTO_OPERADO",  Double.toString (totalOpDL));
	datosTotales.put("TOTAL_SOLICITUDES", String.valueOf(totalDocDL));
	registrosTotales.add(datosTotales);	
	registrosImpT.add(datosTotales);
	
	if(informacion.equals("Consulta")) {
		jsonObj = new JSONObject();
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("ic_producto", ic_producto);	
	
	}else if(informacion.equals("ConsultaTotales")) {
		jsonObj = new JSONObject();
		consulta =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("ic_producto", ic_producto);	
			
	}else  if(informacion.equals("Archivos")) {	
		try {
			String nombreArchivo = paginador.impDescargaArchivo(request, registrosImp,registrosImpT, strDirectorioTemp, tipoArchivo);				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		}		
		
	}
	infoRegresar = jsonObj.toString();

	
}

%>
<%=infoRegresar%>

