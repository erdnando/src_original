<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.parametrosgrales.*,
		com.netro.cadenas.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
	String infoRegresar = "", consulta =""; 
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	JSONObject 	jsonObj	= new JSONObject();
	
	if (informacion.equals("CatalogoApis")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_api");
		cat.setCampoClave("cc_api");
		cat.setCampoDescripcion("cg_nombre");
		cat.setOrden("ig_orden");
		
		infoRegresar=cat.getJSONElementos(); 
	
	}else if (informacion.equals("CatalogoProducto")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_productos_api");
		cat.setCampoClave("cc_producto");
		cat.setCampoDescripcion("cg_nombre");
		
		List lis= new ArrayList ();
		lis.add("1A");
		lis.add("1B");
		lis.add("1C");
		lis.add("1D");
		lis.add("2A");
		lis.add("0");
		lis.add("5A");
		lis.add("4A");
		lis.add("4B");
		lis.add("8");
		lis.add("6A");
		
		cat.setValoresCondicionIn(lis);   
		cat.setOrden("ig_orden");		
		
		infoRegresar=cat.getJSONElementos();
		
	}else if(informacion.equals("ConsultaEncabezados")){
		HashMap hm = new HashMap();
		ParametrosGrales BeanParametrosGrales  = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
		
		hm =(HashMap)BeanParametrosGrales.getEncabezadosMatrizApis(); 
			
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("campo1", hm.get("1A"));
		jsonObj.put("campo2", hm.get("1B"));
		jsonObj.put("campo3", hm.get("1C"));
		jsonObj.put("campo4", hm.get("1D"));
		jsonObj.put("campo5", hm.get("2A"));
		jsonObj.put("campo6", hm.get("0"));
		jsonObj.put("campo7", hm.get("5A"));
		jsonObj.put("campo8", hm.get("4A"));
		jsonObj.put("campo9", hm.get("4B"));
		jsonObj.put("campo10", hm.get("8"));
		
		infoRegresar = jsonObj.toString();
	
	}else if (informacion.equals("ConsultaGrid") || informacion.equals("ArchivoCSV") ){
	
		String sTipoInterfaz		  = (request.getParameter("comboApi")==null)?"":request.getParameter("comboApi");
		String sNombreProducto	  = (request.getParameter("comboProducto")==null)?"":request.getParameter("comboProducto");
	
		ConsMatrizApis paginador = new ConsMatrizApis();
	
		paginador.setsTipoInterfaz(sTipoInterfaz);
		paginador.setsNombreProducto(sNombreProducto);
	
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	 
		if (informacion.equals("ConsultaGrid") ){ 
			try {
				Registros reg	=	queryHelper.doSearch();
				consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
				jsonObj.put("success", new Boolean(true));
				jsonObj = JSONObject.fromObject(consulta);
			}catch(Exception e) {
				throw new AppException("Error en la paginación", e);
			}
		}
		else  if(informacion.equals("ArchivoCSV") )  {	
				
				try {
					String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
				}catch(Throwable e) {
					throw new AppException("Error al generar el archivo CSV", e);
				}
		}
	infoRegresar = jsonObj.toString();
	}
	
%>

<%= infoRegresar %>