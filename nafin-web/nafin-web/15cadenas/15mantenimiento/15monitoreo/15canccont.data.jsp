<%@ page contentType="application/json;charset=UTF-8"
	import="
		com.netro.model.catalogos.*,
		com.netro.cadenas.*, 
		com.netro.descuento.*,
		netropology.utilerias.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"				
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String ic_producto = (request.getParameter("ic_producto") != null)?request.getParameter("ic_producto"):"";
	String ic_if = (request.getParameter("ic_if") != null)?request.getParameter("ic_if"):"";
	String df_operacion = (request.getParameter("df_operacion") != null)?request.getParameter("df_operacion"):"";
	String ig_numero_prestamo = (request.getParameter("ig_numero_prestamo") != null)?request.getParameter("ig_numero_prestamo"):"";
	String solicitudes = (request.getParameter("solicitudes") != null)?request.getParameter("solicitudes"):"";
	
	//Quitar el signo de coma al final de la cadena
	if(!ig_numero_prestamo.equals("")){
		int tamanio2 =	ig_numero_prestamo.length();
		String valor =  ig_numero_prestamo.substring(tamanio2-1, tamanio2);
		System.out.println("valor "+valor);
		if(valor.equals(",")){
		ig_numero_prestamo =ig_numero_prestamo.substring(0,tamanio2-1);
		}
	}
		
	JSONObject jsonObj = new JSONObject();
	String infoRegresar = "", consulta = "",  nombreArchivo ="";
	int numRegistros=0;
	
	ConsCanceXConti  paginador = new ConsCanceXConti();	
	paginador.setIc_producto(ic_producto);
	paginador.setIc_if(ic_if);
	paginador.setIg_numero_prestamo(ig_numero_prestamo);
	paginador.setDf_operacion(df_operacion);
	paginador.setSolicitudes(solicitudes);	
	if(informacion.equals("Consultar")) {
		paginador.setPantalla("Consulta");
	}else  if(informacion.equals("ConsultarProcesados")  || informacion.equals("archivo") ) {
		paginador.setPantalla("Procesados");
	}
	
if(informacion.equals("catalogoProducto")) {

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_producto_nafin");
	catalogo.setCampoDescripcion("ic_nombre");
	catalogo.setTabla("comcat_producto_nafin");
	catalogo.setValoresCondicionIn("0,1,4", Integer.class);
	catalogo.setOrden("ic_nombre");
	infoRegresar = catalogo.getJSONElementos();
	
} else  if(informacion.equals("catalogoIF")) {

	CatalogoIF catalogo = new CatalogoIF(); 
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");	
	catalogo.setG_orden("cg_razon_social");	
	infoRegresar = catalogo.getJSONElementos();
	
} else  if(informacion.equals("Consultar")    ||  informacion.equals("ConsultarProcesados")   ||  informacion.equals("archivo")  ) {
		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	
	if( informacion.equals("Consultar")    ||  informacion.equals("ConsultarProcesados")  ) {
		try {
			Registros reg	=	queryHelper.doSearch();
			numRegistros = reg.getNumeroRegistros();
			
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("numRegistros", String.valueOf(numRegistros));		
		}	catch(Exception e) {
			throw new AppException("Error en la consulta", e);
		}			
		infoRegresar = jsonObj.toString();
	
	} else  if(informacion.equals("archivo")) {

	try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
		infoRegresar = jsonObj.toString();
	
	}
} else  if(informacion.equals("procesarCancelar")) {

	String noSolicitud[] = request.getParameterValues("noSolicitud");
	String productos[] = request.getParameterValues("productos");
	String noRegistros = (request.getParameter("noRegistros") != null)?request.getParameter("noRegistros"):"";
	StringBuffer solic = new StringBuffer("");
	for(int i=0; i<Integer.parseInt(noRegistros); i++) {
		solic.append(noSolicitud[i]+",");
	}
	solic.deleteCharAt(solic.length()-1);	
	String  mensaje =   paginador.procesarCancela(Integer.parseInt(noRegistros), noSolicitud,  ic_producto,  productos );

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", mensaje);
	jsonObj.put("noRegistros", noRegistros);
	jsonObj.put("solicitudes", solic.toString());	
	infoRegresar = jsonObj.toString();
	
}
	
%>
<%=infoRegresar%>
	