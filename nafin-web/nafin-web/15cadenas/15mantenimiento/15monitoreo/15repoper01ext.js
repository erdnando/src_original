Ext.onReady(function(){

//--------------------------------HANDLERS-----------------------------------
	var consultaInicial = function(){
		var combo = Ext.getCmp('clave_epo1');
		var boton = Ext.getCmp('btnConsultar')
		var fecha = Ext.getCmp('fecha_operacion');
		var fecha_ = Ext.util.Format.date(fecha.getValue(),'d/m/Y');
		
		grupos.config.rows[0][0].header = 'Otorgamiento: ' + combo.lastSelectionText;
		
		if(combo.getValue() == ''){
			combo.markInvalid('Debe seleccionar al menos una EPO');
			boton.enable();
		
		}if(fecha.isValid(false) || fecha.getRawValue() == ''){
			if(isdate(fecha_) || fecha.getRawValue() == ''){
				consultaInicialData.load({
					params: {
						informacion: 'consultaInicial',
						cveEpo: combo.getValue()
					}
				});
			}else{
				fecha.markInvalid("La fecha es incorrecta. Verifique que el formato sea dd/mm/aaaa");
				fecha.focus();
				boton.enable();
			}
		}else{
			boton.enable();
		}
	}

	//procesarConsultaInicialData
	var procesarConsultaInicialData = function(store, arrRegistros, opts) 	{
		var combo = Ext.getCmp('clave_epo1');
		if (arrRegistros != null) {		
			if(store.getTotalCount() > 0) {
				var record = consultaInicialData.getAt(0);
				grupos.config.rows[1][0].header = 'Banco de retiro: ' +record.get('CG_BANCO_RETIRO') +
							' Cuenta de Retiro: ' + record.get('CG_CTA_RETIRO');
				consultaData.load({
					params: {
						informacion: 'realizarConsulta',
						cveEpo: combo.getValue(),
						fechaSel: Ext.getCmp("fecha_operacion").getRawValue()
					}
				});
			} else {							
				
			}
		}
	}

	var procesarConsultasData = function(store, arrRegistros, opts) 	{
		var fecha = Ext.getCmp("fecha_operacion").getRawValue();
		if (fecha == ''){
			var fechaActual = new Ext.form.DateField();
			fechaActual.setValue(new Date());
			fecha = fechaActual.getRawValue();
		}
		var gridPrincipal = Ext.getCmp('grid');	
		gridPrincipal.setTitle("Movimiento del d�a " + fecha);
		Ext.getCmp("btnPDF").setIconClass('icoPdf');
		Ext.getCmp("btnXLS").setIconClass('icoXls');
		Ext.getCmp("btnSTAN").setIconClass('icoTxt16');
		Ext.getCmp("btnPDF").show();
		Ext.getCmp("btnXLS").show();
		Ext.getCmp("btnSTAN").show();
		var fp = Ext.getCmp('forma');
		fp.el.unmask();							
		
		var el = gridPrincipal.getGridEl();	
		var combo = Ext.getCmp('clave_epo1');
		if (arrRegistros != null) {
			Ext.getCmp('btnConsultar').enable();
			consultaTotalesData.load({
				params: {
					informacion: 'ConsultarTotales',
					cveEpo: combo.getValue(),
					fechaSel: Ext.getCmp("fecha_operacion").getRawValue()
				}
			});
			if (!gridPrincipal.isVisible()) {				
				gridPrincipal.show();				
			}								
			if(store.getTotalCount() > 0) {				
				Ext.getCmp("btnPDF").enable();				
				Ext.getCmp("btnXLS").enable();				
				Ext.getCmp("btnSTAN").enable();
				Ext.getCmp("btnConsultar").enable();
				var fecha = Ext.getCmp("fecha_operacion").getRawValue();
				gridPrincipal.setTitle("Movimiento del d�a " + fecha);
				el.unmask();					
			} else {
				Ext.getCmp("btnPDF").disable();
				Ext.getCmp("btnXLS").disable();
				Ext.getCmp("btnSTAN").disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var record = consultaTotalesData.getAt(0);
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}								
			if(store.getTotalCount() > 0) {
				if(record.get('TOTAL_MN') != "0" || record.get('TOTAL_DLS') != "0"){
					el.unmask();
				}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
									
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var procesarSuccessFailureArchivo = function(opts, success, response){
		var btnGenerarPdf = Ext.getCmp('btnPDF');
		var btnGenerarXLS = Ext.getCmp('btnXLS');
		var btnGenerarSTAN = Ext.getCmp('btnSTAN')
		btnGenerarPdf.disable();
		btnGenerarXLS.disable();
		btnGenerarSTAN.disable();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			btnGenerarPdf.setIconClass('icoPdf');
			btnGenerarXLS.setIconClass('icoXls');
			btnGenerarSTAN.setIconClass('icoTxt16');			
			btnGenerarPdf.enable();
			btnGenerarXLS.enable();
			btnGenerarSTAN.enable();
		}else{
			btnGenerarPdf.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureArchivoXLS = function(opts, success, response){
		var btnGenerarXLS = Ext.getCmp('btnXLS');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarXLS = Ext.getCmp('btnBajarXLS');
			btnGenerarXLS.hide();
			btnBajarXLS.show();
			btnBajarXLS.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarXLS.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});		
		}else{
			btnGenerarXLS.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureArchivoSTAN = function(opts, success, response){
		var btnGenerarSTAN = Ext.getCmp('btnSTAN');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarSTAN = Ext.getCmp('btnBajarSTAN');
			btnGenerarSTAN.hide();
			btnBajarSTAN.show();
			btnBajarSTAN.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarSTAN.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});		
		}else{
			btnGenerarSTAN.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//---------------------------------STORES------------------------------------
	
	//Store para llenar ComboBox EPO
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPOStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15repoper01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var consultaInicialData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15repoper01ext.data.jsp',
		baseParams: {
			informacion: 'consultaInicial'
		},		
		fields: [	
		   {	name: 'CG_BANCO_RETIRO' },
			{	name: 'CG_CTA_RETIRO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaInicialData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaInicialData(null, null, null);					
				}
			}
		}		
	});

	
	//Store para llenar Grid.
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15repoper01ext.data.jsp',
		baseParams: {
			informacion: 'realizarConsulta'
		},
		fields: [
					{name: 'CG_BANCO_RETIRO'},
					{name: 'CG_CTA_RETIRO'},
					{name: 'RAZON_PYME'},
					{name: 'CD_NOMBRE'},
					{name: 'NC_PYME'},
					{name: 'BANCO_PYME'},
					{name: 'SUC_PYME'},
					{name: 'PLAZA_PYME'},
					{name: 'IMPORTE_PYME'},
					{name: 'RAZON_BEN'},				
					{name: 'NC_BEN'},
					{name: 'BANCO_BEN'},
					{name: 'SUC_BEN'},
					{name: 'PLAZA_BEN'},
					{name: 'IMPORTE_BEN'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultasData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});
	
	var consultaTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15repoper01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},		
		fields: [	
		   {	name: 'TOTAL_MN' },
			{	name: 'IMP_TOTAL_DES_MN'},
			{	name: 'IMP_TOTAL_BEN_MN'},
			{	name: 'TOTAL_DLS' },
			{	name: 'IMP_TOTAL_DES_DLS'},
			{	name: 'IMP_TOTAL_BEN_DLS'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesData(null, null, null);					
				}
			}
		}		
	});

//------------------------------COMPONENTES----------------------------------
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'clave_epo',
			id: 'clave_epo1',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_epo',
			emptyText: 'Seleccione...',
			autoSelect :true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'datefield',
			name: 'fecha_operacion',
			id: 'fecha_operacion',
			allowBlank: true,
			editable: true,
			fieldLabel: 'Fecha de operaci�n',
			startDay: 0,
			anchor: '40%',
			msgTardet: 'side',
			margins: '0 20 0 0'
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 120,
		labelAlign : 'right',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid: true,
		buttons: [		
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('clave_epo1').reset();
					grid.hide();
					gridTotales.hide();
					Ext.getCmp('fecha_operacion').reset();
				}
			},
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: false,
				handler: function(boton, evento){
					boton.disable();
					consultaInicial();
					grid.show();
				}
			}
		]
	});
	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		id: 'grupoHeaders',
		rows: [
			[
				{header: 'Otorgamiento', colspan: 13, align: 'center'}
			],
			[
				{header: 'Banco de Retiro:			Cuenta de Retiro:', colspan: 13, align: 'left'}			
			],
			[
				{header: 'Desarrollador', colspan: 7, align: 'center'},
				{header: 'Beneficiario', colspan: 6, align: 'center'}
			]
		]
	});
	
	//Grid resultados tiene setTitle
	var grid = new Ext.grid.GridPanel({
		id: 'grid',
		store: consultaData,
		height: 350,
		hidden: true,
		margins: '20 0 0 0',
		align: 'center',
		plugins:	grupos,
		style: 'margin:0 auto',
		 loadMask: true,
		title: 'Movimiento para el d�a',
		columns:[
			{
				header: 'Nombre',
				tooltip: 'Nombre',
				dataIndex: 'RAZON_PYME',
				sortable: true,
				resizable: true,
				align: 'left'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'CD_NOMBRE',
				sortable: true,
				resizable: true,
				align: 'left'
			},			
			{
				header: 'No. de Cuenta',
				tooltip: 'No. de Cuenta',
				dataIndex: 'NC_PYME',
				sortable: true,
				resizable: true,
				align: 'center'
			},			
			{
				header: 'Dep�sito en',
				tooltip: 'Dep�sito en',
				dataIndex: 'BANCO_PYME',
				sortable: true,
				resizable: true,
				align: 'left'
			},			
			{
				header: 'Sucursal',
				tooltip: 'Sucursal',
				dataIndex: 'SUC_PYME',
				sortable: true,
				resizable: true,
				align: 'center'
			},			
			{
				header: 'Plaza',
				tooltip: 'Plaza',
				dataIndex: 'PLAZA_PYME',
				sortable: true,
				resizable: true,
				align: 'left'
			},			
			{
				header: 'Importe',
				tooltip: 'Importe',
				dataIndex: 'IMPORTE_PYME',
				sortable: true,
				resizable: true,
				align: 'right',
				renderer:  Ext.util.Format.numberRenderer('$ 0,0.00')			
			},			
			{
				header: 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex: 'RAZON_BEN',
				sortable: true,
				resizable: true,
				align: 'left',
				renderer: function(val, meta, record, rowIndex, colIndex, store) {
					if(val == 'SIN')
						return ''
					else
						return val
				}
			},			
			{
				header: 'Cta. Beneficiario',
				tooltip: 'Cta. Beneficiario',
				dataIndex: 'NC_BEN',
				sortable: true,
				resizable: true,
				align: 'center',
				renderer: function(val, meta, record, rowIndex, colIndex, store) {
					if(val == 'SIN')
						return ''
					else
						return val
				}
			},			
			{
				header: 'Dep�sito en',
				tooltip: 'Dep�sito en',
				dataIndex: 'BANCO_BEN',
				sortable: true,
				resizable: true,
				align: 'left',
				renderer: function(val, meta, record, rowIndex, colIndex, store) {
					if(val == 'SIN')
						return ''
					else
						return val
				}
			},			
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUC_BEN',
				sortable: true,
				resizable: true,
				align: 'center',
				renderer: function(val, meta, record, rowIndex, colIndex, store) {
					if(val == 'SIN')
						return ''
					else
						return val
				}
			},			
			{
				header: 'Plaza Beneficiario',
				tooltip: 'Plaza Beneficiario',
				dataIndex: 'PLAZA_BEN',
				sortable: true,
				resizable: true,
				align: 'left',
				renderer: function(val, meta, record, rowIndex, colIndex, store) {
					if(val == 'SIN')
						return ''
					else
						return val
				}
			},			
			{
				header: 'Importe Beneficiario',
				tooltip: 'Importe Beneficiario',
		//		width: 160,
				dataIndex: 'IMPORTE_BEN',
				sortable: true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			}
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnPDF',
					iconCls: 'icoPdf',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15repoper01ext.data.jsp',
							params:{
								informacion: 'generarArchivoPDF',
								cveEpo: Ext.getCmp('clave_epo1').getValue(),
								fechaSel: Ext.getCmp("fecha_operacion").getRawValue()
							},
							callback: procesarSuccessFailureArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnXLS',
					iconCls: 'icoXls',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15repoper01ext.data.jsp',
							params:{
								informacion: 'generarArchivoXLS',
								cveEpo: Ext.getCmp('clave_epo1').getValue(),
								fechaSel: Ext.getCmp("fecha_operacion").getRawValue()
							},
							callback: procesarSuccessFailureArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Reporte STAN',
					id: 'btnSTAN',
					iconCls: 'icoTxt16',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15repoper01ext.data.jsp',
							params:{
								informacion: 'generarArchivoSTAN',
								cveEpo: Ext.getCmp('clave_epo1').getValue(),
								fechaSel: Ext.getCmp("fecha_operacion").getRawValue()
							},
							callback: procesarSuccessFailureArchivo
						});
					}
				}
			]
		}
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '',	
		align: 'left',
		hidden: true,
		columns: [	
			{
				header: 'Total MN',
				tooltip: 'Total MN',
				dataIndex: 'TOTAL_MN',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},		
			{
				header: 'Importe Total Desarrollador MN',
				tooltip: 'Importe Total Desarrollador MN',
				dataIndex: 'IMP_TOTAL_DES_MN',
				sortable: true,
				width: 160,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},		
			{
				header: 'Importe Total Beneficiario MN',
				tooltip: 'Importe Total Beneficiario MN',
				dataIndex: 'IMP_TOTAL_BEN_MN',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},	
			{
				header: 'Total DLS',
				tooltip: 'Total DLS',
				dataIndex: 'TOTAL_DLS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},		
			{
				header: 'Importe Total Desarrollador DLS',
				tooltip: 'Importe Total Desarrollador DLS',
				dataIndex: 'IMP_TOTAL_DES_DLS',
				sortable: true,
				width: 160,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},	
			{
				header: 'Importe Total Beneficiario DLS',
				tooltip: 'Importe Total Beneficiario DLS ',
				dataIndex: 'IMP_TOTAL_BEN_DLS',
				sortable: true,
				width: 160,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			}	
		],	
		stripeRows: true,
		loadMask: true,
		height: 80,
		frame: true	
	});
		

//-------------------------COMPONENTE PRINCIPAL------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 950,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid,
			gridTotales
		]
	});
	
	catalogoEPO.load();

});