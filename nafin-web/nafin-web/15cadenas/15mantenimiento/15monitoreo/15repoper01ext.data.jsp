<%@ page contentType="application/json;charset=UTF-8" 
	import="  java.sql.ResultSet, 
	java.util.*,
	java.text.*,
	java.math.*,
	com.netro.procesos.*,
	com.netro.model.catalogos.CatalogoSimple,
	com.netro.cadenas.*,   
	net.sf.json.*,
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%  
String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String cveEpo = (request.getParameter("cveEpo") !=null)?request.getParameter("cveEpo"):"";
String nomEpo = (request.getParameter("nomEpo") !=null)?request.getParameter("nomEpo"):"";
String fechaSel = (request.getParameter("fechaSel") !=null)?request.getParameter("fechaSel"):"";
String infoRegresar = "";

//Permite cargar valores de usuario y fecha
if (informacion.equals("catalogoEPO")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_epo");
	cat.setCampoClave("IC_EPO");	
	cat.setCampoDescripcion("CG_RAZON_SOCIAL");
	cat.setOrden("CG_RAZON_SOCIAL");	
	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("consultaInicial") ||
		informacion.equals("realizarConsulta") || 
		informacion.equals("ConsultarTotales") || 
		informacion.equals("generarArchivoPDF") || 
		informacion.equals("generarArchivoXLS") || 
		informacion.equals("generarArchivoSTAN")){
		
	CQueryHelperRegExtJS queryHelper;
	ConsMovimientosDia paginador = new ConsMovimientosDia();
	paginador.setIdEPO(cveEpo);	
	paginador.setFecha(fechaSel);
		
	if(informacion.equals("consultaInicial")){
		try{
			paginador.setTipoConsulta("1");
			queryHelper = new CQueryHelperRegExtJS(paginador);
			Registros reg = queryHelper.doSearch();
			infoRegresar = "{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		//	System.out.println(infoRegresar);
		}catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
		
	}else	if(informacion.equals("realizarConsulta")){
		try{
			paginador.setTipoConsulta("2");
			queryHelper = new CQueryHelperRegExtJS(paginador);
			Registros reg = queryHelper.doSearch();
			infoRegresar = "{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
	
	}else if(informacion.equals("ConsultarTotales")){
		int numRegistrosMN = 0;
		int numRegistrosDLS = 0;
		BigDecimal total_desarrolladorMN = new BigDecimal("0.00");
		BigDecimal total_beneficiarioMN = new BigDecimal("0.00");
		BigDecimal total_desarrolladorDLS = new BigDecimal("0.00");
		BigDecimal total_beneficiarioDLS = new BigDecimal("0.00");
		
		paginador.setTipoConsulta("2");
		queryHelper = new CQueryHelperRegExtJS(paginador);
		Registros reg	=	queryHelper.doSearch();
		while (reg.next()) {	
			if(reg.getString("IC_MONEDA").equals("1")){
				numRegistrosMN++;
				String importeDes = reg.getString("IMPORTE_PYME");
				total_desarrolladorMN = total_desarrolladorMN.add(new BigDecimal(importeDes));
				String importeBen = reg.getString("IMPORTE_BEN");
				total_beneficiarioMN = total_desarrolladorMN.add(new BigDecimal(importeBen));
			}else{
				numRegistrosDLS++;
				String importeDes = reg.getString("IMPORTE_PYME");
				total_desarrolladorDLS = total_desarrolladorDLS.add(new BigDecimal(importeDes));
				String importeBen = reg.getString("IMPORTE_BEN");
				total_beneficiarioDLS = total_beneficiarioDLS.add(new BigDecimal(importeBen));
			}
		}
		JSONArray registros = new JSONArray();
		JSONObject jsonObj = new JSONObject();
		HashMap datos = new HashMap();
		datos.put("TOTAL_MN", String.valueOf(numRegistrosMN));
		datos.put("IMP_TOTAL_DES_MN", total_desarrolladorMN.toPlainString());
		datos.put("IMP_TOTAL_BEN_MN", total_beneficiarioMN.toPlainString());
		datos.put("TOTAL_DLS", String.valueOf(numRegistrosDLS));
		datos.put("IMP_TOTAL_DES_DLS", total_desarrolladorDLS.toPlainString());
		datos.put("IMP_TOTAL_BEN_DLS", total_beneficiarioDLS.toPlainString());
		registros.add(datos);
		infoRegresar = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

	}else if(informacion.equals("generarArchivoPDF")){
		paginador.setTipoConsulta("2");
		queryHelper = new CQueryHelperRegExtJS(paginador);
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
	
	}else if(informacion.equals("generarArchivoXLS")){
		paginador.setTipoConsulta("2");
		queryHelper = new CQueryHelperRegExtJS(paginador);
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CVS");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
	
	}else if(informacion.equals("generarArchivoSTAN")){
		paginador.setTipoConsulta("2");
		queryHelper = new CQueryHelperRegExtJS(paginador);
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "STAN");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
	}
}
%>
<%=  infoRegresar%>
