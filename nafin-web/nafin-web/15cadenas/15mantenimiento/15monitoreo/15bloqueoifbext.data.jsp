<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		java.lang.String,
		com.netro.cadenas.*,
		com.netro.exception.*,
		com.netro.procesos.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.descuento.*,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoIFBloqueo,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if(informacion.equals("catalogoProductos") ) {
	List l = new ArrayList();
	l.add("0");
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COMCAT_PRODUCTO_NAFIN");
	cat.setCampoClave("ic_producto_nafin");
	cat.setCampoDescripcion("ic_nombre");
	cat.setOrden("ic_nombre");
	cat.setValoresCondicionIn(l);
	
	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoIFs") ) {
	CatalogoIFBloqueo cat = new CatalogoIFBloqueo();
	cat.setClave("ic_if");
	cat.setDescripcion("cg_razon_social");

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("Consultar") ) {
	int start = 0;
	int limit = 0;
	
	String operacion					=	(request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	String ic_producto 	= request.getParameter("claveProducto")==null?"":request.getParameter("claveProducto");
	String ic_if		=	(request.getParameter("claveIF") == null) ? "": request.getParameter("claveIF");
	
	
	BloqueoIf paginador = new BloqueoIf();
	paginador.setCve_producto(ic_producto);
	paginador.setCve_if(ic_if);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));

		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	
	try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}

			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		
}else if(informacion.equals("Actualizar")){
	int start = 0;
	int limit = 0;
	StringBuffer acuses = new StringBuffer();
	
	String [] v = request.getParameterValues("ar");	
	String operacion		=	(request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	String ic_producto 	= request.getParameter("claveProducto")==null?"":request.getParameter("claveProducto");
	String ic_if		=	(request.getParameter("claveIF") == null) ? "": request.getParameter("claveIF");
	
	for(int i=0;i<v.length;i++){
		acuses.append("'"+v[i].toString()+"'"+", ");
	}
	
	acuses = acuses.delete(acuses.length()-2,acuses.length());
	UpDateBloqueoIf.upDateBI(acuses.toString());			//Se Realiza la actualización de los datos 
	
	BloqueoIf paginador = new BloqueoIf();
	paginador.setCve_producto(ic_producto);
	paginador.setCve_if(ic_if);
	paginador.setAcuses(acuses.toString());
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));

		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	
	try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}

			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	
}else if(informacion.equals("ArchivoCSV") ) {

	String acuse	=	(request.getParameter("acuse") == null)?"":request.getParameter("acuse");
	
	AutorizacionSolicitud autorizacionSolicitud = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB",AutorizacionSolicitud.class);
	
	String sContenidoArchivo = autorizacionSolicitud.getDatosSolicitudCreditoxAcuse(acuse);
	String nombreArchivo="";
	
	if (!sContenidoArchivo.equals("")) {	
		CreaArchivo archivo = new CreaArchivo();
		if(!archivo.make(sContenidoArchivo.toString(), strDirectorioTemp, ".csv"))
			out.print("Error al generar el archivo.");
		else
			nombreArchivo = archivo.nombre;								
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>
