<%@ page contentType="application/json;charset=UTF-8"
	import="
		com.netro.model.catalogos.*,
		java.util.*,
		com.netro.cadenas.*, 
		com.netro.descuento.*,
		netropology.utilerias.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"				
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String ic_producto = (request.getParameter("ic_producto") != null)?request.getParameter("ic_producto"):"";
	String ic_if = (request.getParameter("ic_if") != null)?request.getParameter("ic_if"):"";
	String fechaOperacDe = (request.getParameter("fechaOperacDe") != null)?request.getParameter("fechaOperacDe"):"";
	String fechaOperacA = (request.getParameter("fechaOperacA") != null)?request.getParameter("fechaOperacA"):"";
	String claveSolicitud = (request.getParameter("claveSolicitud") != null)?request.getParameter("claveSolicitud"):"";
	String interfase = (request.getParameter("interfase") != null)?request.getParameter("interfase"):"";
	String logAdicional = (request.getParameter("logAdicional") != null)?request.getParameter("logAdicional"):"";
	String version = (String)session.getAttribute("version");
	if(version==null){ 	version=""; 	}
	
	JSONObject jsonObj = new JSONObject();
	String infoRegresar = "", consulta = "";
	
	ConsMonitorXProcesos  paginador = new ConsMonitorXProcesos();

if(informacion.equals("valoresIniciales")) {
	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("version", version);		
	infoRegresar = jsonObj.toString();

}else  if(informacion.equals("catalogoProducto")) {

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_producto_nafin");
	catalogo.setCampoDescripcion("ic_nombre");
	catalogo.setTabla("comcat_producto_nafin");
	catalogo.setValoresCondicionIn("0,1,4", Integer.class);
	catalogo.setOrden("ic_producto_nafin");
	infoRegresar = catalogo.getJSONElementos();
	
} else  if(informacion.equals("catalogoIF")) {

	List catIF = paginador.getCatalogoIF();
	jsonObj.put("registros", catIF);
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString();

} else  if(informacion.equals("Consultar") || informacion.equals("ArchivoPDF") || informacion.equals("ArchivoCSV") ) {

	
	paginador.setIc_producto(ic_producto);
	paginador.setIc_if(ic_if);
	paginador.setFechaOperacDe(fechaOperacDe);
	paginador.setFechaOperacA(fechaOperacA);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	int    numRegistros = 0;
	
	if(informacion.equals("Consultar") ){
		try {
			Registros reg	=	queryHelper.doSearch();
			numRegistros = reg.getNumeroRegistros();
			
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
			throw new AppException("Error en la consulta", e);
		}
		jsonObj.put("ic_producto", ic_producto);
		jsonObj.put("numRegistros", String.valueOf(numRegistros));
		infoRegresar = jsonObj.toString();
	
	}else  if ( informacion.equals("ArchivoPDF") ) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}		
	}else  if ( informacion.equals("ArchivoCSV") ) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}		
	}
	
	infoRegresar = jsonObj.toString();
	
 }else  if ( informacion.equals("ArchivoVerPrestamo") ) {

	String nombreArchivo = paginador.impVerPrestamoo( request, strDirectorioTemp ,  ic_producto,  claveSolicitud,   interfase,  logAdicional ) ;
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();

 }else  if ( informacion.equals("ArchivoVerOrigen") ) {
 
	AutorizacionSolicitud autorizacionSolicitud = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB",AutorizacionSolicitud.class);
	CreaArchivo creaArchivo = new CreaArchivo();

	String sContenidoArchivo = autorizacionSolicitud.getDatosSolicitudCredito(claveSolicitud);

	creaArchivo.make(sContenidoArchivo.toString(), strDirectorioTemp, ".csv");
	String nombreArchivo = creaArchivo.getNombre();
			
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();
}	

%>
<%=infoRegresar%>
