 <!DOCTYPE html>
  <%@ page import="java.util.*, netropology.utilerias.*"	errorPage="/00utils/error.jsp"%>
<%@page contentType="text/html;charset=windows-1252"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf"%>

<%
 String ic_producto = (request.getParameter("ic_producto")!=null)?request.getParameter("ic_producto"):"";
 String tipoProceso = (request.getParameter("tipoProceso")!=null)?request.getParameter("tipoProceso"):"Prestamos";
 String tituloTipoProceso = (request.getParameter("tituloTipoProceso")!=null)?request.getParameter("tituloTipoProceso"):"Préstamos";
 String monitor = (request.getParameter("monitor") != null)?request.getParameter("monitor"):"";
 String version = (String)session.getAttribute("version"); 
%>
	
<%if(esEsquemaExtJS) {%>
<html>
   <head>	   
      <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">      
      <%@ include file="/extjs4.jspf" %> 
		<%@ include file="/01principal/menu.jspf"%>
		<script type="text/javascript" src="15monprocext.js?<%=session.getId()%>"></script>      
      <title>Nafinet</title>
   </head>   
   
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<div id="areaContenido"></div>
		</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
	<form id='formParametros' name="formParametros">
	<input type="hidden" id="ic_producto" name="ic_producto" value="<%=ic_producto%>"/>	
	<input type="hidden" id="tipoProceso" name="tipoProceso" value="<%=tipoProceso%>"/>
	<input type="hidden" id="tituloTipoProceso" name="tituloTipoProceso" value="<%=tituloTipoProceso%>"/>
	<input type="hidden" id="monitor" name="monitor" value="<%=monitor%>"/>
	</form>	
</body>
</html>
<%}%>