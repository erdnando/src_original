Ext.onReady(function() {
		
	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);	
		
	/**********CRITERIOS DE BUSQUEDA ***************/		

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: 'Captura',			
				id: 'btnCaptura',					
				handler: function() {
					window.location = '15bloqueoifExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<b>Administraci�n de Operaciones</b>',			
				id: 'btnConsulta',					
				handler: function() {
					window.location = '15bloqueoifbext.jsp';
				}
			}	
		]
	});	
	
	var procesarCatProductosData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var claveProd = Ext.getCmp('_cmb_prod');
			
			if(claveProd.getValue()==''){	
				var newRe = new recordType({
					clave:'',
					descripcion:'Seleccionar Producto'
				});
				
				store.insert(0,newRe);
				store.commitChanges();
				claveProd.setValue('');
				Ext.getCmp('_cmb_prod').setValue('0');
			}
			
		}
  }
  
  var procesarCatIFsData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var claveIf = Ext.getCmp('_cmb_if');
			
			if(claveIf.getValue()==''){	
				var newRe = new recordType({
					clave:'',
					descripcion:'Seleccionar IF'
				});
				
				store.insert(0,newRe);
				store.commitChanges();
				claveIf.setValue('');
				//Ext.getCmp('_cmb_if').setValue('');
			}
			
		}
  }

	//descargar el  Archivo
	function procesarSuccessFailureManual(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	var descargaArchivo = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var acuse = registro.get('ACUSE');
	
		Ext.Ajax.request({
			url: '15bloqueoifbext.data.jsp',
			params: {
				informacion: 'ArchivoCSV',
				acuse: acuse
			},
			callback: procesarSuccessFailureManual
		});
	}

	var procesarConsultaRegistros = function(store, registros, opts){
		var forma = Ext.getCmp('forma');
		forma.el.unmask();

		if (registros != null) {

			var grid  = Ext.getCmp('gridConsulta');

			if (!grid.isVisible()) {
				grid.show();
			}

			//Ext.getCmp('barraPaginacion').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGuardar').enable();
				Ext.getCmp('btnCancelar').enable();
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
				Ext.getCmp('btnGuardar').disable();
				Ext.getCmp('btnCancelar').disable();
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}
	}
	
	var procesarRegistrosProcesados = function(store, registros, opts){
		if (registros != null) {
			fpBotones.hide();
			Ext.getCmp("gridConsulta").hide();
			Ext.getCmp("forma").hide();
			Ext.getCmp("gridProcesados").show();
			var el = gridCambios.getGridEl();
			var gr = Ext.getCmp("gridConsulta");
			gr.el.unmask();
			Ext.getCmp("gridProcesados").el.unmask();
			if(store.getTotalCount() > 0) {
				//Ext.getCmp('btnAceptar').enable();
			} else {
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
				//Ext.getCmp('btnAceptar').disable();
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}else{
			var el = gridCambios.getGridEl();
			var gr = Ext.getCmp("gridConsulta");
			gr.el.unmask();
			Ext.getCmp("gridConsulta").hide();
		}
	}

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - -

	var catalogoProductosData = new Ext.data.JsonStore({
		id:				'catalogoProductosDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15bloqueoifbext.data.jsp',
		baseParams:		{	informacion: 'catalogoProductos'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: procesarCatProductosData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoIFsData = new Ext.data.JsonStore({
		id:				'catalogoIFsDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15bloqueoifbext.data.jsp',
		baseParams:		{	informacion: 'catalogoIFs'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: procesarCatIFsData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});


//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'15bloqueoifbext.data.jsp',
			baseParams: {	informacion:	'ConsultaRegistros'	},
			fields: [
				{ name: 'RAZON_SOCIAL'},
				{ name: 'ACUSE'},
				{ name: 'DF_CARGA'},
				{ name: 'NOMBREMONEDA'},
				{ name: 'IMPORTETOTALDSCTO'},
				{ name: 'NUMOPERACIONES'},
				{ name: 'PROC'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null,null);
					}
				}
			}
	});
	
	//Este store es para los datos que seran cargados en el grid de registros procesados
		var registrosProcesadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosProcesadosDataStore',
			url: 			'15bloqueoifbext.data.jsp',
			baseParams: {	informacion:	'ConsultaRegistros'	},
			fields: [
				{ name: 'RAZON_SOCIAL'},
				{ name: 'ACUSE'},
				{ name: 'DF_CARGA'},
				{ name: 'NOMBREMONEDA'},
				{ name: 'IMPORTETOTALDSCTO'},
				{ name: 'NUMOPERACIONES'},
				{ name: 'PROCESAR'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				load: 	procesarRegistrosProcesados,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarRegistrosProcesados(null, null,null);
					}
				}
			}
	});
	
//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - -

	var procesaConsulta = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){
		 if(  estadoSiguiente == "CONSULTAR" ){
			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');
			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("gridConsulta").hide();
			Ext.StoreMgr.key('registrosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Consultar',
								operacion: 'Generar',
								start: 0,
								limit: 20
							}
				)
			});
		}else if(	estadoSiguiente == "ACTUALIZAR"){
			var gr = Ext.getCmp("gridConsulta");
			gr.el.mask('Procesando...','x-mask-loading');
			var j=0;
		/*	var v = new Array();
			var v = sm.getSelections();*/
			var a = new Array();
		/*	for(var i=0; i<v.length; i++){	//Se obtiene el numero de acuse de cada record seleccionado
				a[i]=v[i].get('ACUSE');
			}*/
			
			var grConsulta = Ext.getCmp('gridConsulta'); //Store de grid 
			for(i=0; i<grConsulta.getStore().getTotalCount() ;i++ ){
				var registro = grConsulta.getStore().getAt(i);
				if(registro.get('PROCESAR')==true){
					a[j]= registro.get('ACUSE');
					j++;
				}
			}
			
			Ext.StoreMgr.key('registrosProcesadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion:	'Actualizar',
								ar:a,
								operacion: 'Generar',
								start: 0,
								limit: 20
							}
				)
			});
		}else if(	estadoSiguiente == "LIMPIAR"){
			//Ext.getCmp('forma').getForm().reset();
			//grid.hide();
			window.location = '15bloqueoifbext.jsp';
		}
	}	

//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

//Columna de checkbox
	var sm = new Ext.grid.CheckboxSelectionModel({
		header : '',
		checkOnly:true,
		hideable:true,
		singleSelect:false,
		id:'chkIR',
		width:32	  			
	});


//Elementos del grid de la consulta
		var grid = new Ext.grid.GridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			title:		undefined,
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			width: 		892,
			frame: 		false,
			//sm: sm,
			columns: [
				{
					header: 		'IF',
					tooltip: 	'Nombre de la IF',
					dataIndex: 	'RAZON_SOCIAL',
					sortable: 	true,
					align:		'left',
					resizable: 	true,
					width: 		140,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Acuse',
					tooltip: 	'N�mero de acuse',
					dataIndex: 	'ACUSE',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		115,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Fecha y Hora de env�o',
					tooltip: 	'Fecha y hora en que se envio',
					dataIndex: 	'DF_CARGA',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		125,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'No. Operaciones',
					tooltip: 	'Numero de operaciones',
					dataIndex: 	'NUMOPERACIONES',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		95,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Moneda',
					tooltip: 	'Tipo de moneda de cambio',
					dataIndex: 	'NOMBREMONEDA',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Monto Total del Descuento',
					tooltip: 	'Monto total',
					dataIndex: 	'IMPORTETOTALDSCTO',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		155,
					hidden: 		false,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
				},{
	      xtype: 'actioncolumn',
				header: 'Ver archivo',
				tooltip: 'Descargar archivo excel',
				width: 80,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoBuscar';										
						},	
						handler: descargaArchivo
					}
				]				
			},{
					xtype: 'checkcolumn',				
					header:'Procesar',
					tooltip: 'Procesar',
					dataIndex : 'PROCESAR',
					width : 55,
					align: 'center',
					sortable : false			
				}
			],bbar: {
			buttonAlign: 	'right',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Aceptar',
					width:	100,
					iconCls:	'icoAceptar',
					id: 		'btnGuardar',
					handler: function(boton, evento) {
					   var b = false;
						var ac= sm.getCount();		//Se obtiene el numero de checkbox chequeados
						var grConsulta = Ext.getCmp('gridConsulta'); //Store de grid 
						for(i=0; i<grConsulta.getStore().getTotalCount() ;i++ ){
							var registro = grConsulta.getStore().getAt(i);
							if(registro.get('PROCESAR')==true){
								b = true;
								break;
							}
						}
						if(b){
							Ext.Msg.confirm('Confirmaci�n', '�Est� seguro de querer enviar su informaci�n?', function(btn){
								if(btn=='yes'){
									accionConsulta("ACTUALIZAR", null);
								}
							});
						}
						else{
							Ext.Msg.alert('Aviso','Por favor seleccione al menos un bloque de operaciones a procesar');
						}
					}
				},'-',
				{
					xtype:	'button',
					text:		'Cancelar',
					iconCls:	'icoRechazar',
					width:	100,
					id: 		'btnCancelar',
					handler: function(boton, evento) {
						accionConsulta("LIMPIAR",null);
					}
				},'-'
			]
		}
	});
	
	var grupo = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Los Cambios Se Realizaron Con �xito', colspan: 7, align: 'center'}
				],
				[
					{header: 'A continuaci�n se muestran los cambios realizados', colspan: 7, align: 'center'}
				]
			]
		});

//Elementos del grid que carga los registros procesados
		var gridCambios = new Ext.grid.GridPanel({
			store: 		registrosProcesadosData,
			id:			'gridProcesados',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			align:		'center',
			stripeRows: true,
			loadMask: 	true,
			height: 		260,
			width: 		869,
			frame: 		true,
			plugins:		grupo,
			columns: [
				{
					header: 		'IF',
					tooltip: 	'Nombre de la IF',
					dataIndex: 	'RAZON_SOCIAL',
					sortable: 	true,
					align:		'left',
					resizable: 	true,
					width: 		140,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Acuse',
					tooltip: 	'N�mero de acuse',
					dataIndex: 	'ACUSE',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		115,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Fecha y Hora de env�o',
					tooltip: 	'Fecha y hora en que se envio',
					dataIndex: 	'DF_CARGA',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		125,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'No. Operaciones',
					tooltip: 	'Numero de operaciones',
					dataIndex: 	'NUMOPERACIONES',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		95,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Moneda',
					tooltip: 	'Tipo de moneda de cambio',
					dataIndex: 	'NOMBREMONEDA',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Monto Total del Descuento',
					tooltip: 	'Monto total',
					dataIndex: 	'IMPORTETOTALDSCTO',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		155,
					hidden: 		false,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
				},{
					header: 		'Estatus',
					tooltip: 	'Estatus de la IF',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		100,
					hidden: 		false,
					hideable:	false,
					renderer:  function (val, meta, record, rowIndex, colIndex, store){
							return 'Seleccionada IF';
							}
				}
			],bbar: {
			buttonAlign: 	'right',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Regresar',
					iconCls:	'icoRegresar',
					width:	100,
					id: 		'btnAceptar',
					handler: function(boton, evento) {
							var g=Ext.getCmp('gridProcesados');
							g.hide();
							var f  = Ext.getCmp('forma');
							if (!f.isVisible()) {
								f.show();
							}
							fpBotones.show();
					}
				},'-'
			]
		}
	});

	var elementosFormaConsulta = [
				{
					xtype: 'combo',
					name: 'cmb_prod',
					id: '_cmb_prod',
					fieldLabel: 'Producto',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'claveProducto',
					//emptyText: 'Producto',
					forceSelection : true,
					triggerAction : 'all',
					editable: false,
					typeAhead: true,
					minChars : 1,
					store: 		catalogoProductosData,
					anchor:	'95%'
				},{
					xtype: 'combo',
					name: 'cmb_if',
					id: '_cmb_if',
					fieldLabel: 'IF',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'claveIF',
					emptyText: 'Todas las IF',
					forceSelection : true,
					triggerAction : 'all',
					editable: true,
					typeAhead: true,
					minChars : 1,
					store: 		catalogoIFsData,
					anchor:	'95%'
					}
			];
			
			var fp = new Ext.form.FormPanel({
			id:				'forma',
			width:			570,
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			labelWidth:		150,
			items:			elementosFormaConsulta,
			monitorValid:	true,
			buttons: [
				{
				text:		'Consultar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:false,
				handler: function(boton, evento) {
									var prod=Ext.getCmp('_cmb_prod');
									if(prod.getValue()==""){
										Ext.Msg.alert('Aviso','Por Favor Seleccione un Producto');
									}
									else{
										accionConsulta("CONSULTAR",null)
									}
								}//fin handler
				},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR",null);
							}
				}
			]
		});
		
//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	930,
		//height: 	'auto',
		align: 'center',
		items: 	[
			fpBotones,
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(5),
			gridCambios
		]

	});	
	
	Ext.StoreMgr.key('catalogoProductosDataStore').load();
	Ext.StoreMgr.key('catalogoIFsDataStore').load();
		
});