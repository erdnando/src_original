<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.sql.*,		
		java.util.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*, 	
		com.netro.descuento.*, 
		netropology.utilerias.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"				
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String fechaHoy		= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());	
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String ic_producto = (request.getParameter("ic_producto") != null)?request.getParameter("ic_producto"):"";
	String ic_if = (request.getParameter("ic_if") != null)?request.getParameter("ic_if"):"";
	String ic_epo = (request.getParameter("ic_epo") != null)?request.getParameter("ic_epo"):"";
	String ic_moneda = (request.getParameter("ic_moneda") != null)?request.getParameter("ic_moneda"):"";
	String fecha_de = (request.getParameter("fecha_de") != null)?request.getParameter("fecha_de"):fechaHoy;
	String fecha_a = (request.getParameter("fecha_a") != null)?request.getParameter("fecha_a"):fechaHoy;
	String tipoA = (request.getParameter("tipoA") != null)?request.getParameter("tipoA"):"";	
	String fecha = (request.getParameter("fecha") != null)?request.getParameter("fecha"):fechaHoy;
	String boton = (request.getParameter("boton") != null)?request.getParameter("boton"):"";
	String infoRegresar = "", consulta = "", tipo_piso ="", fechaConvenio=  "", fechaPeriodo ="",nombreArchivo = "", mensaje="";

	JSONObject jsonObj = new JSONObject();
	HashMap datosT = new HashMap();
	JSONArray registros = new JSONArray();
	
	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB",AutorizacionDescuento.class);

	if(!ic_if.equals("") && !ic_producto.equals("") ){
		Vector vecFilas = new Vector();
		vecFilas = BeanAutDescuento.getFechasConvenioyDescuento(ic_if,ic_producto);
		if(vecFilas.size()>0)
			fechaConvenio = (String)vecFilas.get(0);

		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		StringTokenizer stk = new StringTokenizer(fecha_de, "/");
		fechaPeriodo = new Integer(stk.nextToken())+" de "+meses[new Integer(stk.nextToken()).intValue()-1]+" de "+stk.nextToken();
		stk = new StringTokenizer(fecha_a, "/");
		fechaPeriodo = " a "+new Integer(stk.nextToken())+" de "+meses[new Integer(stk.nextToken()).intValue()-1]+" de "+stk.nextToken();
	}
				
	ConsDeposito paginador = new ConsDeposito();
	
	if(!ic_if.equals(""))  {
		tipo_piso  = paginador.getTipoPiso(ic_if );
	}

if(informacion.equals("valoresIniciales")) {

	jsonObj.put("success",  new Boolean(true));
	jsonObj.put("fechaHoy", fechaHoy);	
	jsonObj.put("tipo_piso", tipo_piso);	
	infoRegresar = jsonObj.toString();
		
}else  if(informacion.equals("catProducto")) {

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_producto_nafin");
	catalogo.setCampoDescripcion("ic_nombre");
	catalogo.setTabla("comcat_producto_nafin");
	catalogo.setValoresCondicionIn("1,2,4", Integer.class);
	catalogo.setOrden("ic_producto_nafin");
	infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("catalogoIF")) {

	List catIF = paginador.getCatalogoIF();
	jsonObj.put("registros", catIF);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();
	
}else  if(informacion.equals("catalogoEPO") && !ic_producto.equals("") ) {

	CatalogoEPOxProd catalogo = new CatalogoEPOxProd(); 
	catalogo.setClave("E.ic_epo");
	catalogo.setDescripcion("E.cg_razon_social");
	catalogo.setOrden("E.cg_razon_social");
	catalogo.setIntermediario(ic_if);
	catalogo.setProducto(ic_producto);
	catalogo.setHabilitado("'H','S'");
	catalogo.setPyme("");
	infoRegresar = catalogo.getJSONElementos();	

}else if(informacion.equals("catMoneda")) {

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	catalogo.setOrden("ic_moneda");
	infoRegresar = catalogo.getJSONElementos();


}else if(informacion.equals("Consultar")  || informacion.equals("Archivos")   || informacion.equals("ConsTotales")  ) {

	List datos  =paginador.getEncabezado(ic_producto,  ic_if,  ic_epo, ic_moneda, fecha_de, fecha_a, fechaConvenio,  fechaPeriodo );
	
	HashMap info = (HashMap)datos.get(0);
	String titulo= info.get("TITULO").toString();
	String mensajeA=  info.get("MENSAJE_A").toString();
	String mensajeB= info.get("MENSAJE_B").toString();
	String mensajeC= info.get("MENSAJE_C").toString();
	String mensajeCA= info.get("MENSAJE_CA").toString();
	String mensajeD= info.get("MENSAJE_D").toString();
	String mensajeDA= info.get("MENSAJE_DA").toString();
	String encabezado= info.get("ENCABEZADO").toString();
	String total= info.get("TOTAL").toString();
	String importe= info.get("IMPORTE").toString();
	String total2= info.get("TOTAL2").toString();
	String nombre_if_e= info.get("NOMBRE_IF_E").toString();
	String nombre_if_p= info.get("NOMBRE_IF_P").toString();
	String puesto= info.get("PUESTO").toString();
	fechaPeriodo= info.get("FECHA_PERIODO").toString();
	fechaConvenio= info.get("FECHA_CONVENIO").toString();
	double total1=0,  totalRemanente = 0;

	
	String mensajePie  = "<table width='900' cellpadding='3' cellspacing='0' >"+
								"<tr> "+
								" <td class='titulos' align='center'><br>Facultado por "+nombre_if_e+".<br>"+	puesto+" "+nombre_if_p+"</td>"+
								"</tr>"+
								"</table>";
	
	String mensajeEncabezado  =" <table  width='900'  cellpadding='2' cellspacing='1' border='0'>"+
										" <tr> <td class='titulos' align='center'>Formato de constancia de depósito  de"+titulo+"<br>"+ "Cadenas Productivas </td>  </tr>"+
										" <tr>" +
										" <td class='formas'><div align='justify'>De conformidad con el convenio que "+nombre_if_e+" y en la fecha que posteriormente se señala, celebró con Nacional Financiera, S.N.C., bajo protesta de decir verdad, se hace constar que para todos los efectos legales, nos asumimos como depositarios de los título (s) de crédito y/o documentos electrónico (s) "+
										" sin derecho a honorarios y asumiendo la responsabilidad civil y penal correspondiente al carácter de depositario judicial, el (los) título (s) de crédito y/o documento (s) electrónico (s) en que se consignan los derechos de crédito derivados de las operaciones de "+ mensajeA+", celebradas con los clientes que se describen en el cuadro siguiente, en donde se especifica la fecha de presentación, importe total y nombre de la empresa acreditada.<br><br>"+
										" La información de el (los) "+total+" documento (s) en "+(ic_moneda.equals("1")?"Moneda Nacional":"Dólares")+ "  por un importe de $"+Comunes.formatoDecimal(importe,2,true)+" citado (s), están registrados en el sistema denominado Nafin Electrónico a disposición de Nacional Financiera, S.N.C., cuando ésta los requiera.<br><br>"+
										" Tratándose de el (los) título (s) de crédito electrónico (s), éstos se encuentran debidamente transmitido (s) en propiedad y con nuestra responsabilidad a favor de Nacional Financiera, S.N.C. En el caso de el (los) documento (s) electrónico (s), en este acto cedemos a dicha Institución de Banca de Desarrollo, los derechos de crédito que se derivan de los mismos.<br><br>"+
										" En términos del artículo 2044 del Código Civil para el Distrito Federal, la cesión a que se refiere el párrafo anterior, la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, por el tiempo en que permanezcan vigentes los adeudos y hasta su liquidación total.<br><br>"+
										" Así mismo y en términos de lo estipulado  en el Convenio antes mencionado, nos obligamos a efectuar el cobro de (los) título (s) de crédito electrónico y/o ejercer los derechos de cobro consignados en el (los) documentos objeto de "+mensajeB+", así como ejecutar todos los actos que sean necesarios para que el (los) documento (s) señalados conserven su valor y demás derechos que les correspondan.</div>"+
										" </td>"+
										" </tr>"+
										" <tr> <td class='formas'>&nbsp;</td></tr>"+
										" <tr> "+
										" <td align='center'> "+
										" <table cellpadding='0' cellspacing='0' border='0'>"+
										" <tr>"+
										" <td class='formas'><b>Nombre del Intermediario Financiero</b></td>"+
										" <td class='formas' align='center'>"+nombre_if_e+"</td>"+
										" </tr>"+
										" <tr>" +
										" <td>&nbsp;</td>"+
										" <td width='120' valign='top'><hr width='210' size='1' noshade></td>"+
										" </tr>"+
										" <tr>"+
										" <td class='formas'><b>Fecha del Convenio de "+mensajeC +"</b></td>"+
										" <td class='formas' align='center'>"+fechaConvenio+"</td>"+
										" </tr>"+
										" <tr>"+
										" <td>&nbsp;</td>"+
										" <td width='120' valign='top'><hr width='210' size='1' noshade></td>"+
										"  </tr>"+
										" <tr>"+
										"  <td class='formas'><b>Periodo de Consulta de "+mensajeD+"</td>"+
										" <td class='formas' align='center'>México D.F. " +fechaPeriodo+"</td>"+
										" </tr>"+
										" </table>";
  
	paginador.setIc_producto(ic_producto);
	paginador.setIc_if(ic_if);
	paginador.setIc_epo(ic_epo);
	paginador.setIc_moneda(ic_moneda);
	paginador.setFecha_de(fecha_de);
	paginador.setFecha_a(fecha_a);
	paginador.setTipo_piso(tipo_piso);
	paginador.setMensajeEncabezado(encabezado);
	paginador.setPuesto(puesto);
	paginador.setNombre_if_p(nombre_if_p);
	paginador.setNombre_if_e(nombre_if_e);
	paginador.setTotal(total);
	paginador.setTotal2(total2);
	paginador.setFechaPeriodo(fechaPeriodo);
	paginador.setFechaConvenio(fechaConvenio); 
	paginador.setMensajeA(mensajeA);
	paginador.setMensajeB(mensajeB);
	paginador.setMensajeC(mensajeC);
	paginador.setMensajeD(mensajeD);
	paginador.setTitulo(titulo);	
	paginador.setImporte(importe);	
	
	
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
 
 
	if (informacion.equals("Consultar") ){ 
		try {
			Registros reg	=	queryHelper.doSearch();
				
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);		 
			jsonObj.put("mensajeEncabezado", mensajeEncabezado);
			jsonObj.put("mensajePie", mensajePie);
			
		}	catch(Exception e) {
			throw new AppException("Error en la consulta", e);
		}
	}else if (informacion.equals("Archivos") ){ 
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoA);				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
			jsonObj.put("tipoArchivo", tipoA);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ConsTotales") ){ 
	
		try {
			Registros reg	=	queryHelper.doSearch();
			while (reg.next()) {
				String impor = (reg.getString("IMPORTE_DOCTO") == null) ? "" : reg.getString("IMPORTE_DOCTO");				
				String capPermanente = (reg.getString("CAPTURA_REMANENTE") == null) ? "" : reg.getString("CAPTURA_REMANENTE");				
				total1=total1+ Double.parseDouble(impor);			
				if(!"".equals(capPermanente)){
					totalRemanente += Double.parseDouble(capPermanente);
				}				
			}
			datosT = new HashMap();
			datosT.put("TOTAL_DOCTOS", total);
			datosT.put("TOTAL_IMP_DOCTOS",  Double.toString (total1) );
			datosT.put("TOTAL_IMP_DESC", total2 );
			if(boton.equals("CapturaR")) {			
				datosT.put("TOTAL_REMANENTE","");
			}else  {
				datosT.put("TOTAL_REMANENTE", Double.toString (totalRemanente));
			}
			registros.add(datosT);			
					
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + registros.toString()+ "}";
			
		}	catch(Exception e) {
			throw new AppException("Error en la consulta", e);
		}			
		jsonObj.put("success", new Boolean(true));
		jsonObj = JSONObject.fromObject(consulta);
	}
	
	jsonObj.put("ic_producto", ic_producto);
	jsonObj.put("tipo_piso", tipo_piso);
	jsonObj.put("boton", boton);
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("Concentrado_dia") ){ 
	

	try {
		nombreArchivo = paginador.generarArchivo(fecha, strDirectorioTemp);										
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo Concentrado del dia", e);
	}
	
	if(nombreArchivo.equals(""))  { mensaje="No se Encontró Ningún Registro";  }
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);			
	infoRegresar = jsonObj.toString();	


}else if (informacion.equals("Total_dia") ){ 
	
	try {
		nombreArchivo = paginador.generarArchivoD( ic_producto, ic_moneda, fecha,  strDirectorioTemp);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo Concentrado del dia", e);
	}
	
	if(nombreArchivo.equals(""))  { mensaje="No se Encontró Ningún Registro";  }
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);			
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("grabarRemanente") ){ 

	String ic_docto_sel[] = request.getParameterValues("ic_docto_sel");
	String fn_remanente[] = request.getParameterValues("fn_remanente");
	String noRegistros = (request.getParameter("noRegistros") != null) ? request.getParameter("noRegistros") : "";
	
	mensaje =   paginador.grabarRemanente( Integer.parseInt(noRegistros),  ic_docto_sel ,  fn_remanente );
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje);
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("ConsNacional")  ||  informacion.equals("ConsDolar") ){ 

	List datos =  paginador.consConfirmacion(fecha_de , fecha_a, ic_moneda  );
	consulta= datos.get(0).toString();	
	String numRegistrosMN= datos.get(1).toString();	
	String numRegistrosDL= datos.get(2).toString();	
		
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("numRegistrosMN",numRegistrosMN);
	jsonObj.put("numRegistrosDL",numRegistrosDL);
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("AceptarConfirmacion") ){ 

	String ic_ifMN[] = request.getParameterValues("ic_ifMN");
	String ic_ifDL[] = request.getParameterValues("ic_ifDL");
	String confirmaOperacionMN = (request.getParameter("confirmaOperacionMN") != null)?request.getParameter("confirmaOperacionMN"):"";
	String confirmaOperacionDL = (request.getParameter("confirmaOperacionDL") != null)?request.getParameter("confirmaOperacionDL"):"";
	String numRegMN = (request.getParameter("numRegMN") != null)?request.getParameter("numRegMN"):"";
	String numRegDL = (request.getParameter("numRegDL") != null)?request.getParameter("numRegDL"):"";

	mensaje = paginador.AceptaConfirmacion(fecha_de , ic_ifMN,  ic_ifDL ,  numRegMN,  numRegDL,  confirmaOperacionMN,  confirmaOperacionDL );
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje);
	infoRegresar = jsonObj.toString();	

}

%>
<%=infoRegresar%>
