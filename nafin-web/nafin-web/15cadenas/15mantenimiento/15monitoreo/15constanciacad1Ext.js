Ext.onReady(function() {

	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnGenArc').setIconClass('');
			if(infoR.mensaje !='')  {
				Ext.MessageBox.alert('Mensaje',infoR.mensaje);
			}else {
				var archivo = infoR.urlArchivo;				
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		
	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('fecha').setValue(info.fechaHoy);
						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
  
	var procesarCatProducto= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_producto1 = Ext.getCmp('ic_producto1');
			if(ic_producto1.getValue()==''){
				ic_producto1.setValue(records[0].data['clave']);
			}
		}
  }
  
	var catProducto = new Ext.data.JsonStore({
		id: 'catProducto',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15constanciacadExt.data.jsp',
		baseParams: {
			informacion: 'catProducto'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatProducto,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catMoneda = new Ext.data.JsonStore({
		id: 'catMoneda',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15constanciacadExt.data.jsp',
		baseParams: {
			informacion: 'catMoneda'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma =[
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			name: 'ic_producto',
			id: 'ic_producto1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_producto',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catProducto,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120		
		},	
		{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			name: 'ic_moneda',
			id: 'ic_moneda1',				
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_moneda',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catMoneda,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha',			
			msgTarget: 'side',
			width: 330,
			combineErrors: false,
			items: [
				{	
					fieldLabel: 'Fecha',
					xtype: 'datefield',
					name: 'fecha',
					id: 'fecha',					
					width: 100,
					msgTarget: 'side',								
					margins: '0 20 0 0' 
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		}
	];	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 330,
		style: 'margin:0 auto;',
		title: 'Total del d�a',
		hidden: false,
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [	
			{
				text: 'Generar Archivo ',
				id: 'btnGenArc',									
				formBind: true,				
				handler: function(boton, evento) {
				
					var ic_producto = Ext.getCmp("ic_producto1");
					if (Ext.isEmpty(ic_producto.getValue()) ){
						ic_producto.markInvalid('Debe de seleccionar un Producto');
						return;
					}
					
					var ic_moneda = Ext.getCmp("ic_moneda1");
					if (Ext.isEmpty(ic_moneda.getValue()) ){
						ic_moneda.markInvalid('Debe de seleccionar una Moneda');
						return;
					}
					var fecha = Ext.getCmp("fecha");
					if (Ext.isEmpty(fecha.getValue()) ){
						fecha.markInvalid('Debe de seleccionar una Fecha');
						return;
					}
					
					var fecha_ = Ext.util.Format.date(fecha.getValue(),'d/m/Y');

					if(!Ext.isEmpty(fecha.getValue())){
						if(!isdate(fecha_)) { 
							fecha.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fecha.focus();
							return;
						}
					}
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '15constanciacadExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Total_dia'
						}),							
						callback: descargaArchivo
					});	
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',									
				formBind: true,				
				handler: function(boton, evento) {	
					document.location.href  = 	'15constanciacadExt.jsp?pantalla=Total_dia';
				}
			}
		]
	});	


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),				
			fp,
			NE.util.getEspaciador(20),			
			NE.util.getEspaciador(20)
		]
	});

	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url : '15constanciacadExt.data.jsp',
		params: {
			informacion: "valoresIniciales"			
		},
		callback: procesaValoresIniciales
	});	

	catProducto.load();
	catMoneda.load();
	
});