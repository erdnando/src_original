Ext.onReady(function() {

	var catPrincipal = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','Cadenas'],			
			['2','Cr�dito Electr�nico']	
		 ]
	});
	
	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'tipoMonitoreo',	
			id: 'tipoMonitoreo1',	
			fieldLabel: 'Seleccionar', 
			mode: 'local',	
			hiddenName : 'tipoMonitoreo',	
			emptyText: 'Cadenas',
			forceSelection : true,	triggerAction : 'all',	typeAhead: true,
			minChars : 1,	
			store : catPrincipal,	
			displayField : 'descripcion',	valueField : 'clave',
			listeners: {
				select: {
					fn: function(combo) {
						
					}
				}
			}
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 400,
		style: 'margin:0 auto;',
		title: 'Constancia de Dep�sito',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [		
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {					
					var tipoMonitoreo1 = Ext.getCmp("tipoMonitoreo1");
					if(tipoMonitoreo1.getValue()=='1' || tipoMonitoreo1.getValue()=='') {
						document.location.href  = 	'15constanciacadExt.jsp';						
					}else if(tipoMonitoreo1.getValue()=='2') {
						document.location.href  = 	'15constanciacredExt.jsp';						
					}
				}
			}
		]
	});	


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),				
			fp,		
			NE.util.getEspaciador(20),			
			NE.util.getEspaciador(20)
		]
	});

	
});