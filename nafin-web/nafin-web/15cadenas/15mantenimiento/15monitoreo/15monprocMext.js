
Ext.onReady(function() {

	var verRegistos  = function (grid,rowIndex,colIndex,item){
		var registro = grid.getStore().getAt(rowIndex);
		var ic_producto1 = Ext.getCmp('ic_producto1').getValue();					
		var tipoProceso = registro.get('PROCESOS');	
		
		document.location.href  = "15monproc1ext.jsp?ic_producto="+ic_producto1+"&tipoProceso="+tipoProceso+"&monitor=SI";
		
	}
	
//***************GRID DEL MONITOR**************************


	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();
                
                var fpBotonesMonitor = Ext.getCmp('fpBotonesMonitor');
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
                        if (!fpBotonesMonitor.isVisible()) {
				fpBotonesMonitor.show();
			}
                        
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;		
						
			if(store.getTotalCount() > 0) {	
				gridConsulta.setTitle('<div><div style="float:left">Registros a Procesar:   '+jsonData.rproc+'</div>');
			
				el.unmask();					
			} else {				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15monproc.data.jsp',
		baseParams: {
			informacion: 'ConsultaMonitor'
		},		
		fields: [	
			{	name: 'PROCESOS'},	
			{	name: 'REGISTROS_PROCESADOS'},	
			{	name: 'REGISTROS_RECHAZADOS'},	
			{	name: 'REGISTROS_CONCLUIDOS'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});

	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Procesos',
				tooltip: 'Procesos',
				dataIndex: 'PROCESOS',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},			
			{
				xtype: 'actioncolumn',
				header: 'Registros Procesados',
				tooltip: 'Registros Procesados',
				dataIndex: 'REGISTROS_PROCESADOS',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer: function(value,metaData,registro,rowIndex,colIndex,store){
					return value;						
				}
			},
			{
				xtype: 'actioncolumn',
				header: 'Registros Rechazados',
				tooltip: 'Registros Rechazados',
				dataIndex: 'REGISTROS_RECHAZADOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
					renderer: function(value,metaData,registro,rowIndex,colIndex,store){
					if(value!='0') {
						return '<span style="color:red;">' + value + '</span>';
					}else {					
						return value;
					}	
				},
				items: [
					{
						getClass: function(value,metadata,registro,rowIndex,colIndex,store){
							if(value!='0') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';						
							}						
						}
						,handler: verRegistos
					}										
				]
			},
			{
				xtype: 'actioncolumn',
				header: 'Registros Concluidos',
				tooltip: 'Registros Concluidos',
				dataIndex: 'REGISTROS_CONCLUIDOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(value,metaData,registro,rowIndex,colIndex,store){
					return value;
				}
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 220,
		width: 610,
		align: 'center',
		frame: false
	});

	function resetMonitor(opts, success, response) {
            consultaData.reload();
        }
        
	function consultaPausa(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var clave = infoR.clave;
                        var descripcion = infoR.descripcion;
                        var ic_producto = infoR.ic_producto;
                        var btnPausaTmp= Ext.getCmp('btnPausaProy');
			if(clave=='CREDE' ){
                            if(descripcion=='N' ){
                                btnPausaTmp.setText('Pausar Prestamos');
                            }
                            if(descripcion=='S' ){
                                btnPausaTmp.setText('Reiniciar Prestamos');
                            }
                        }
			if(clave== 'DSCTOE' ){
                            if(descripcion=='N' ){
                                btnPausaTmp.setText('Pausar Prestamos');
                            }
                            if(descripcion=='S' ){
                                btnPausaTmp.setText('Reiniciar Prestamos');
                            }
                        }
			if(clave=='DISTR' ){
                            if(descripcion=='N' ){
                                btnPausaTmp.setText('Pausar Prestamos');
                            }
                            if(descripcion=='S' ){
                                btnPausaTmp.setText('Reiniciar Prestamos');
                            }
                        }
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

// ***************+Criterios de Busquea********************
	
	var catalogoProducto = new Ext.data.JsonStore({
		id: 'catalogoProducto',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15monproc.data.jsp',
		baseParams: {
			informacion: 'catalogoProducto'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});

		
	var elementosForma =  [
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			name: 'ic_producto',
			id: 'ic_producto1',
			allowBlank: true,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_producto',
			emptyText: 'Seleccionar...',
			forceSelection: false, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoProducto,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120,
			listeners: {
				select:{ 
					fn:function (combo) {		
					
						fp.el.mask('Enviando...', 'x-mask-loading');			
							consultaData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ConsultaMonitor'							
								})
							});
                                                                                Ext.Ajax.request({
                            url: '15monproc.data.jsp',
                            params: Ext.apply(fp.getForm().getValues(),{
                                informacion: 'ConsultaPausa'
                            }),
                            callback: consultaPausa
                        });
						}
					}
				}
			}			
		];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: 'margin:0 auto;',
		title: '',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 160,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [		
			{
				text: 'Consultar/Actualizar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
				
					var ic_producto = Ext.getCmp('ic_producto1');
					if (Ext.isEmpty(ic_producto.getValue()) ){
						ic_producto.markInvalid('El campo es obigatorio');
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ConsultaMonitor'							
						})
					});
				}
			}
		]
	});
		
	
	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: 'Concluidos por IF',			
				id: 'btnCons1',					
				handler: function() {
					window.location = '15monconcifext.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',				
				text: '<h1><b>Monitor</h1></b>',	
				id: 'btnCap',					
				handler: function() {
					window.location = '15monprocext.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Resumen de Operaciones',			
				id: 'btnCons2',					
				handler: function() {
					window.location = '15monresoperext.jsp';
				}
			}
		]
	};

	var fpBotonesMonitor = {
		xtype:     	'panel',
		id: 'fpBotonesMonitor',
                frame:     	false,
                border:    	false,
                bodyStyle: 	'background:transparent;',
                layout:    	'hbox',
                style: 		'margin: 0 auto',
                width:			650,
                height: 		75,
                hidden: true,
                layoutConfig: {
                    align:      'middle',
                    pack:       'center'
                },			  
		items: [{
                    xtype: 'button',
                    text: 'Pausar Prestamos',
                    id: 'btnPausaProy',
                    handler: function(boton, evento) {
                        var btnPauTmp= Ext.getCmp('btnPausaProy');
                        var words = btnPauTmp.getText().split(" ");
                        var word=words[0];
                        Ext.Msg.confirm('Interfaz de Prestamos', '�Est� Seguro de '+word+' la Interfaz de Prestamos? : ',
                            function(botonConf) {
                                if (botonConf == 'ok' || botonConf == 'yes') {
                                    Ext.Ajax.request({
                                        url: '15monproc.data.jsp',
                                        params: Ext.apply(fp.getForm().getValues(),{
                                            informacion: 'PausaInterfaz'
                                        }),
                                        callback: consultaPausa
                                    });
                                }
                            }
                        );
                    }
                },{
                    xtype: 'box',
                    width: 20
                },{
                    xtype: 'button',
                    text: 'Reset Monitor',
                    id: 'btnResetMon',
                    handler: function(boton, evento) {
                        Ext.Msg.confirm('Reset Monitor', '�Est� seguro de hacer el Reset del Monitor? : ',
                            function(botonConf) {
                                if (botonConf == 'ok' || botonConf == 'yes') {
                                    Ext.Ajax.request({
                                        url: '15monproc.data.jsp',
                                        params: Ext.apply(fp.getForm().getValues(),{
                                            informacion: 'ResetMonitor'
                                        }),
                                        callback: resetMonitor
                                    });                                
                                }
                            }
                        );
                    }
                }]
	};


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fpBotones,
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,			
			NE.util.getEspaciador(20),
                        fpBotonesMonitor,
                        NE.util.getEspaciador(20)
		]
	});
	
	catalogoProducto.load();
	
});	
