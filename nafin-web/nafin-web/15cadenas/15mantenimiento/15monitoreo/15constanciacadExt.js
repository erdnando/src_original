
	function selecCONFIR_DL(check, rowIndex, colIds){
		var  gridDolar = Ext.getCmp('gridDolar');	
		var store = gridDolar.getStore();
		var reg = gridDolar.getStore().getAt(rowIndex);
	 
		if(check.checked==true)  {
		reg.set('CONFIRMA', "S");				
		}else  {
			reg.set('CONFIRMA', "N"); 
		}	
		store.commitChanges();	
	}
	
	function selecCONFIR_MN(check, rowIndex, colIds){
		var  gridNacional = Ext.getCmp('gridNacional');	
		var store = gridNacional.getStore();
		var reg = gridNacional.getStore().getAt(rowIndex);
		if(check.checked==true)  {
			reg.set('CONFIRMA', "S");				
		}else  {
			reg.set('CONFIRMA', "N"); 
		}	
		store.commitChanges();	
	}
	
Ext.onReady(function() {


	var ic_docto_sel = [];
	var fn_remanente = [];
	
	var cancelar =  function() { 
		ic_docto_sel = [];
		fn_remanente = [];
	}


	//********************CONSULTA  Confirmaci�n de Operaciones del d�a *******************************************

	function repCofirmacion(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			if(info.mensaje !='')  {
				Ext.MessageBox.alert('Mensaje',info.mensaje,
					function(){
						fp.el.mask('Enviando...', 'x-mask-loading');			
						consNacionalData.load({
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ConsNacional',
								ic_moneda:'1'
							})
						});
						consDolarData.load({
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ConsDolar',
								ic_moneda:'54'
							})
						});
					
					}
				);				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	var proConfirmacion = function() {
		
		var  gridNacional = Ext.getCmp('gridNacional');
		var storeMN = gridNacional.getStore();		
		
		var  gridDolar = Ext.getCmp('gridDolar');
		var storeDL = gridDolar.getStore();	
		
		var ic_ifMN = [];
		var ic_ifDL = [];
		var confirmaOperacionMN ="N";
		var confirmaOperacionDL ="N";
		var numRegMN =0;
		var numRegDL =0;
				
		storeMN.each(function(record) {	
			if(record.data['IC_IF']!=''){
				ic_ifMN.push(record.data['IC_IF']);
				numRegMN++;
			}
			confirmaOperacionMN= record.data['CONFIRMA'];								
		});
		
		storeDL.each(function(record) {	
			if(record.data['IC_IF']!=''){
				ic_ifDL.push(record.data['IC_IF']);
				numRegDL++;
			}
			confirmaOperacionDL= record.data['CONFIRMA'];								
		});
		
	
		Ext.Ajax.request({
			url: '15constanciacadExt.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'AceptarConfirmacion',	
				ic_ifMN:ic_ifMN,
				ic_ifDL:ic_ifDL,
				numRegMN:numRegMN,
				numRegDL:numRegDL,
				confirmaOperacionMN:confirmaOperacionMN,
				confirmaOperacionDL:confirmaOperacionDL
			}),
			callback: repCofirmacion
		});					
	}
	

	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },	
		hidden: true,
		items: [			
			{
				xtype: 'button',				
				text: '<h1><b>Aceptar</h1></b>',	
				id: 'btnAceptar',
				handler:proConfirmacion
			},
			{ xtype: 'textfield',	id: 'numRegistrosMN' , hidden: true	},
			{ xtype: 'textfield',	id: 'numRegistrosDL' , hidden: true	}
			
		]
	};
	
	var procConsMNacionalData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridNacional = Ext.getCmp('gridNacional');	
		var el = gridNacional.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridNacional.isVisible()) {
				gridNacional.show();
			}	
						
			var cm = gridNacional.getColumnModel();
			var el = gridNacional.getGridEl();	
			var info = store.reader.jsonData;		
			if(info.numRegistrosMN=='0') {
				gridNacional.hide();
			}			
			Ext.getCmp('numRegistrosMN').setValue(info.numRegistrosMN);
						
			if(Ext.getCmp('numRegistrosMN').getValue()!='0' ||  Ext.getCmp('numRegistrosDL').getValue() !='0') {
				Ext.getCmp('fpBotones').show();	
			}
			if(Ext.getCmp('numRegistrosMN').getValue()=='0' && Ext.getCmp('numRegistrosDL').getValue() =='0') {
				Ext.getCmp('fpBotones').hide();	
			}
			
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consNacionalData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15constanciacadExt.data.jsp',
		baseParams: {
			informacion: 'ConsNacional'
		},		
		fields: [	
			{	name: 'IC_IF'},
			{	name: 'NOMBRE_IF'},
			{	name: 'NUM_OPERACIONES'},
			{	name: 'MONTO'},
			{	name: 'CHECKBOX'},
			{	name: 'CONFIRMA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procConsMNacionalData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procConsMNacionalData(null, null, null);					
				}
			}
		}		
	});
	
	var gridNacional = new Ext.grid.EditorGridPanel({	
		store: consNacionalData,
		id: 'gridNacional',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		hidden: true,
		title:'Moneda: Moneda Nacional',
		columns: [			
			{
				header: 'Nombre IF',				
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 250,			
				resizable: true,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'N�mero de Operaciones',				
				dataIndex: 'NUM_OPERACIONES',
				sortable: true,
				width: 240,			
				resizable: true,
				align: 'center'				
			},
			{
				header: 'Monto',				
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,
				align: 'right',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					var checked= '';
					if(record.data['CONFIRMA']=='S') {  checked='checked'; }
					if(record.data['CHECKBOX']=='SI'){
            		return '<input id="confirmaOperacionMN"'+ rowIndex + 'value="DL" type="checkbox" '+checked+' onclick="selecCONFIR_MN(this,'+rowIndex +','+colIndex+');"/>';				
            	}else{
            		return value;
            	}
            }				
			}		
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 170,
		width: 650,
		align: 'center',
		frame: false
	});
	
	
	
	var procConsDolarData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridDolar = Ext.getCmp('gridDolar');	
		var el = gridDolar.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridDolar.isVisible()) {
				gridDolar.show();
			}	
						
			var cm = gridDolar.getColumnModel();
			var el = gridDolar.getGridEl();	
			var info = store.reader.jsonData;		
			if(info.numRegistrosDL=='0') {
				gridDolar.hide();
			}
			
			Ext.getCmp('numRegistrosDL').setValue(info.numRegistrosDL);
			
			if(Ext.getCmp('numRegistrosDL').getValue()!='0' || Ext.getCmp('numRegistrosMN').getValue()!='0') {
				Ext.getCmp('fpBotones').show();	
			}
			if(Ext.getCmp('numRegistrosMN').getValue()=='0' && Ext.getCmp('numRegistrosDL').getValue() =='0') {
				Ext.getCmp('fpBotones').hide();	
			}
			
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consDolarData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15constanciacadExt.data.jsp',
		baseParams: {
			informacion: 'ConsDolar'
		},		
		fields: [
			{	name: 'IC_IF'},
			{	name: 'NOMBRE_IF'},
			{	name: 'NUM_OPERACIONES'},
			{	name: 'MONTO'},
			{	name: 'CHECKBOX'},
			{	name: 'CONFIRMA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procConsDolarData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procConsDolarData(null, null, null);					
				}
			}
		}		
	});
	
	var gridDolar = new Ext.grid.EditorGridPanel({	
		store: consDolarData,
		id: 'gridDolar',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		title:'Moneda: D�lares',
		hidden: true,		
		columns: [			
			{
				header: 'Nombre IF',				
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 250,			
				resizable: true,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'N�mero de Operaciones',				
				dataIndex: 'NUM_OPERACIONES',
				sortable: true,
				width: 240,			
				resizable: true,
				align: 'center'				
			},
			{
				header: 'Monto',				
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,
				align: 'right',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					var checked= '';
					if(record.data['CONFIRMA']=='S') {  checked='checked'; }
					if(record.data['CHECKBOX']=='SI'){
            		return '<input id="confirmaOperacionDL"'+ rowIndex + 'value="DL" type="checkbox" '+checked+' onclick="selecCONFIR_DL(this,'+rowIndex +','+colIndex+');"/>';
            	}else{
            		return value;
            	}
            }				
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 170,
		width: 650,
		align: 'center',
		frame: false
		
	});
	
		
	//********************GRABAR Captura de Remanente **********************************************
	
	function respuestaProceso(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			if(info.mensaje !='')  {
				Ext.MessageBox.alert('Mensaje',info.mensaje,
					function(){
						
						Ext.getCmp('btnGrabar').hide();
						
						fp.el.mask('Enviando...', 'x-mask-loading');			
							consultaData.load({
								params: Ext.apply(fp.getForm().getValues(),{							
									informacion: 'Consultar',
									boton:'Consulta'
								})
							});
						}
					);				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	var grabarRemanente = function() {
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();		
		var noRegistros = 0;
		cancelar();
		
		store.each(function(record) {	
			if(record.data['CAPTURA_REMANENTE_2']!='' ){	
				noRegistros++;			
				ic_docto_sel.push(record.data['IC_DOCUMENTO']);
				fn_remanente.push(record.data['CAPTURA_REMANENTE_2']);					
			}
		});
		
		Ext.Ajax.request({
			url: '15constanciacadExt.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'grabarRemanente',	
				noRegistros:noRegistros,
				ic_docto_sel:ic_docto_sel,
				fn_remanente:fn_remanente
			}),
			callback: respuestaProceso
		});					
	}

//********************DESCARGA ARCHIVO **********************************************
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(infoR.tipoArchivo=='PDF') {
				Ext.getCmp('btnArchivoPDF').setIconClass('icoPdf');
			}
			if(infoR.tipoArchivo=='CSV') {
				Ext.getCmp('btnArchivoCSV').setIconClass('icoXls');
			}	
			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//********************CONSULTA **********************************************


	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
						
			var cm = gridConsulta.getColumnModel();
			var el = gridConsulta.getGridEl();	
			var info = store.reader.jsonData;		
			
			gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_EMISOR'), false);	
			gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('EMPRESA_ACREDITADA'), false);
			gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DESCRIPCION_DOCTO'), false);
			gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('LUGAR_SUSCRIPCION'), false);
			gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_SUSCRIPCION'), false);
			gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_PRESTAMO'), false);	
			gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE_DOCTO'), false);
						
			if(info.ic_producto=='1') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_DESCUENTO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE_DESC'), false);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VTO_DESC'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TASA_DESC'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VTO_CREDITO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TASA_CREDITO'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PERIODICIDAD'), false);
				if(info.tipo_piso=='1' && info.boton =='Consulta') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAPTURA_REMANENTE'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAPTURA_REMANENTE_2'), true);
				}	
				if(info.tipo_piso=='1' && info.boton =='CapturaR') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAPTURA_REMANENTE_2'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAPTURA_REMANENTE'), true);					
				}	
			}else if(info.ic_producto=='2' ) {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_DESCUENTO'), true);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE_DESC'), true);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VTO_DESC'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TASA_DESC'), false);						
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAPTURA_REMANENTE'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VTO_CREDITO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TASA_CREDITO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PERIODICIDAD'), false);
			
			} else if(  info.ic_producto=='4' ) {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_DESCUENTO'), true);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE_DESC'), true);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VTO_DESC'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TASA_DESC'), true);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAPTURA_REMANENTE'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VTO_CREDITO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TASA_CREDITO'), true);
			   gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE_FINANCIAMIENTO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PERIODICIDAD'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PERIODICIDAD_CAPITAL'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PERIODICIDAD_INTERES'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TASA_INTERES'), false);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('NUM_PRESTAMO'),'N�mero de Pr�stamo Nafin');
			}	
			
			if(store.getTotalCount() > 0) {	
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
				Ext.getCmp('mensaje1').show();
				Ext.getCmp('mensajeEncabezado').setValue(info.mensajeEncabezado);
				Ext.getCmp('mensaje2').show();
				Ext.getCmp('mensajePie').setValue(info.mensajePie);
				if(info.tipo_piso=='1' && info.boton =='CapturaR') {
					Ext.getCmp('btnGrabar').show();
				}
				consTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{							
						informacion: 'ConsTotales',
						boton:info.boton
					})
				});
					
				el.unmask();					
			} else {	
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				Ext.getCmp('btnGrabar').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
			
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15constanciacadExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			
			{	name: 'IC_DOCUMENTO'},	
			{	name: 'NOMBRE_EMISOR'},	
			{	name: 'EMPRESA_ACREDITADA'},	
			{	name: 'DESCRIPCION_DOCTO'},	
			{	name: 'LUGAR_SUSCRIPCION'},	
			{	name: 'FECHA_SUSCRIPCION'},	
			{	name: 'FECHA_DESCUENTO'},	
			{	name: 'IMPORTE_DOCTO'},	
			{	name: 'IMPORTE_DESC'},	
			{	name: 'PERIODICIDAD'},	
			{	name: 'FECHA_VTO_DESC'},	
			{	name: 'TASA_DESC'},	
			{	name: 'NUM_PRESTAMO'},	
			{	name: 'CAPTURA_REMANENTE'},
			{  name: 'CAPTURA_REMANENTE_2'},
			{	name: 'FECHA_VTO_CREDITO'},	
			{	name: 'TASA_CREDITO'},
			{	name: 'IMPORTE_FINANCIAMIENTO'},
			{	name: 'PERIODICIDAD_CAPITAL'},	
			{	name: 'PERIODICIDAD_INTERES'},
			{	name: 'FECHA_VENC'},	
			{	name: 'TASA_INTERES'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	
	var calculaRemanente =  function() { 
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		var totalRemanente = 0;
		store.each(function(record) {				
			if(record.data['CAPTURA_REMANENTE_2']!=''  ){	
				totalRemanente +=record.data['CAPTURA_REMANENTE_2'];
			}
		});
		
		var  gridTotales = Ext.getCmp('gridTotales');
		var storeT = gridTotales.getStore();	
		storeT.each(function(record) {		
			record.data['TOTAL_REMANENTE'] = totalRemanente;
			record.commit();	
		});		
			
	}

	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consultar',
		clicksToEdit: 1,
		hidden: true,		
		columns: [			
			{
				header: 'Nombre del Emisor',				
				dataIndex: 'NOMBRE_EMISOR',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Nombre de la Empresa Acreditada',				
				dataIndex: 'EMPRESA_ACREDITADA',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
				
			},
			{
				header: 'Descripci�n del Documento',				
				dataIndex: 'DESCRIPCION_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'left'				
			},
			{
				header: 'Lugar Suscripci�n',				
				dataIndex: 'LUGAR_SUSCRIPCION',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'left'				
			},
			{
				header: 'Fecha Suscripci�n',				
				dataIndex: 'FECHA_SUSCRIPCION',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center'				
			},
			{
				header: 'Fecha Descuento',				
				dataIndex: 'FECHA_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center'				
			},
			{
				header: 'Importe Documento',				
				dataIndex: 'IMPORTE_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				hidden: true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},			
			{
				header: 'Fecha Vto. Cr�dito',				
				dataIndex: 'FECHA_VTO_CREDITO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				hidden: true								
			},
			{
				header: 'Tasa Cr�dito',				
				dataIndex: 'TASA_CREDITO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				hidden: true,
				renderer: Ext.util.Format.numberRenderer('0,0.00')
			},			
			{
				header: 'Importe Descuento',				
				dataIndex: 'IMPORTE_DESC',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				hidden: true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'Importe del Financiamiento',				
				dataIndex: 'IMPORTE_FINANCIAMIENTO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				hidden: true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{
				header: 'Periodicidad de Pago de Capital',				
				dataIndex: 'PERIODICIDAD_CAPITAL',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center'							
			},
			{
				header: 'Periodicidad Pago de Inter�s',				
				dataIndex: 'PERIODICIDAD_INTERES',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center'							
			},
			
			{
				header: 'Periodicidad pago Capital e Inter�s',				
				dataIndex: 'PERIODICIDAD',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center'							
			},
			{
				header: 'Fecha Vencimiento',				
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center'							
			},
			{
				header: 'Tasa de Inter�s',				
				dataIndex: 'TASA_INTERES',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},			
			{
				header: 'Fecha Vto. Descuento',				
				dataIndex: 'FECHA_VTO_DESC',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center'							
			},
			{
				header: 'Tasa Descuento',				
				dataIndex: 'TASA_DESC',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'right'							
			},
			{
				header: 'N�mero de Pr�stamo',				
				dataIndex: 'NUM_PRESTAMO',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center'							
			},
			{
				header: 'Captura de Remanente',				
				dataIndex: 'CAPTURA_REMANENTE',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(value =='0') {
						return '';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Captura de Remanente',				
				dataIndex: 'CAPTURA_REMANENTE_2',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'center',				
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: false
				},
				renderer:function(value,metadata,registro){
					var valor;
					if(value=='0') { 	valor = ''; 	}
					if(value!='0') { 	valor = value; 	}
					
					return NE.util.colorCampoEdit(valor,metadata,registro);									
				}	
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		listeners: {	
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
				
				var  impDesc=	record.data['IMPORTE_DESC'];
				var  impRema=	record.data['CAPTURA_REMANENTE_2'];
				
				
				if(campo == 'CAPTURA_REMANENTE_2'){					
					if( parseFloat(impRema)<=0)  {
						Ext.MessageBox.alert('Mensaje','El monto capturado debe ser mayor a cero.');	
						record.data['CAPTURA_REMANENTE_2']='';
						record.commit();	
						return;
					
					}else if( parseFloat(impRema)>parseFloat(impDesc))  {
						Ext.MessageBox.alert('Mensaje','El monto capturado no debe ser mayor al importe de Descuento.');
						record.data['CAPTURA_REMANENTE_2']='';
						record.commit();	
						return;
					}else  {
						calculaRemanente ();
					}
				}
									
			}				
		},
		bbar: {
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Grabar',					
					tooltip:	'Grabar',
					iconCls: 'icoAceptar',
					id: 'btnGrabar',
					hidden: true,
					handler:grabarRemanente
				},
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {	
						
						boton.setIconClass('loading-indicator');	
						
						Ext.Ajax.request({
							url: '15constanciacadExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'Archivos',
								tipoA:'CSV'
							}),							
							callback: descargaArchivo
						});						
					}
				},
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {		
					
						boton.setIconClass('loading-indicator');							
						Ext.Ajax.request({
							url: '15constanciacadExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'Archivos',
								tipoA:'PDF'
							}),							
							callback: descargaArchivo
						});						
					}
				}
			]
		}
	});
	
		
	//********************CONSULTA  TOTALES *******************************************

	var procConsTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}	
						
			var cm = gridTotales.getColumnModel();
			var el = gridTotales.getGridEl();	
			var info = store.reader.jsonData;		
						
			if(info.ic_producto=='1'  &&  info.tipo_piso=='1') {
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_REMANENTE'), false);			
			}else  {
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_REMANENTE'), true);		
			}
			
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15constanciacadExt.data.jsp',
		baseParams: {
			informacion: 'ConsTotales'
		},		
		fields: [	
			{	name: 'TOTAL_DOCTOS'},
			{	name: 'TOTAL_IMP_DOCTOS'},
			{	name: 'TOTAL_IMP_DESC'},
			{	name: 'TOTAL_REMANENTE'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procConsTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procConsTotalesData(null, null, null);					
				}
			}
		}		
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		hidden: true,		
		columns: [			
			{
				header: 'Total de Documentos',				
				dataIndex: 'TOTAL_DOCTOS',
				sortable: true,
				width: 150,			
				resizable: true,
				align: 'center'				
			},
			{
				header: 'Total Importe Documento',				
				dataIndex: 'TOTAL_IMP_DOCTOS',
				sortable: true,
				width: 150,			
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'Total Importe Descuento',				
				dataIndex: 'TOTAL_IMP_DESC',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Remanente:',				
				dataIndex: 'TOTAL_REMANENTE',
				sortable: true,
				width: 150,			
				resizable: true,					
				hidden: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 80,
		width: 900,
		align: 'center',
		frame: false
	});
	

	var mensaje1 = new Ext.Container({
		layout: 'table',		
		style: 'margin:0 auto;',
		id: 'mensaje1',	
		hidden: true,
	   width: '900',
		heigth:'auto',		
		items:[
			{ 	xtype: 'displayfield',  id: 'mensajeEncabezado', 	value: '' }			
		]
	});
	
	var mensaje2 = new Ext.Container({
		layout: 'table',		
		style: 'margin:0 auto;',
		id: 'mensaje2',	
		hidden: true,
	   width: '900',
		heigth:'auto',		
		items:[
			{ 	xtype: 'displayfield',  id: 'mensajePie', 	value: '' }			
		]
	});
	
	
	//********************CRITERIOS DE BUSQUEDA ********************************
	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var info = Ext.util.JSON.decode(response.responseText);
			
			var fp= Ext.getCmp('forma');
			var  ic_producto= Ext.getCmp('ic_producto1').getValue();
			
			if(ic_producto=='') {
				Ext.getCmp('fecha_de').setValue(info.fechaHoy);
				Ext.getCmp('fecha_a').setValue(info.fechaHoy);	
			}
				
			Ext.getCmp('fecha_hoy').setValue(info.fechaHoy);	
			
			if(ic_producto=='1' &&  info.tipo_piso=='1') {			
				Ext.getCmp('btnCapturaRem').show();
			}else  {
				Ext.getCmp('btnCapturaRem').hide();
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var catMoneda = new Ext.data.JsonStore({
		id: 'catMoneda',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15constanciacadExt.data.jsp',
		baseParams: {
			informacion: 'catMoneda'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});

	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15constanciacadExt.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15constanciacadExt.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catProducto = new Ext.data.JsonStore({
		id: 'catProducto',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15constanciacadExt.data.jsp',
		baseParams: {
			informacion: 'catProducto'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma = [
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			name: 'ic_producto',
			id: 'ic_producto1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_producto',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catProducto,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120,
			listeners: {
				select:{ 
					fn:function (combo) {	
					
						catalogoEPO.load({
							params : Ext.apply(fp.getForm().getValues(),{
								ic_producto: combo.getValue()								
							})
						});
						
						var  ic_if= Ext.getCmp('ic_if1').getValue();
						
						Ext.Ajax.request({
							url : '15constanciacadExt.data.jsp',
							params: {
								informacion: "valoresIniciales",	
								ic_producto: combo.getValue(),
								ic_if:ic_if
							},
							callback: procesaValoresIniciales
						});
						
					}
				}
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'IF',
			name: 'ic_if',
			id: 'ic_if1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_if',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoIF,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120,
			listeners: {
				select:{ 
					fn:function (combo) {	
					
						catalogoEPO.load({
							params : Ext.apply(fp.getForm().getValues(),{
								ic_if: combo.getValue()								
							})
						});
						
						var  ic_producto= Ext.getCmp('ic_producto1').getValue();
						
						Ext.Ajax.request({
							url : '15constanciacadExt.data.jsp',
							params: {
								informacion: "valoresIniciales",	
								ic_if: combo.getValue(),
								ic_producto:ic_producto
							},
							callback: procesaValoresIniciales
						});
					
					}
				}
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la EPO',
			name: 'ic_epo',
			id: 'ic_epo1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_epo',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoEPO,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120
		},
		{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			name: 'ic_moneda',
			id: 'ic_moneda1',					
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_moneda',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catMoneda,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Emisi�n',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_de',
					id: 'fecha_de',
					allowBlank: true,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_a',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_a',
					id: 'fecha_a',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 					
					campoInicioFecha: 'fecha_de',
					margins: '0 20 0 0' 
				}
			]
		},
		{
			xtype: 'textfield',
			name: 'fecha_hoy',
			id: 'fecha_hoy',
			allowBlank: true,
			startDay: 1,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 750,
		style: 'margin:0 auto;',
		title: 'Cadenas ',
		hidden: false,
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 160,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [	
			{
				text: 'Confirmaci�n de Operaciones del d�a',
				id: 'btnConfirma',									
				formBind: true,				
				handler: function(boton, evento) {	
				
					var ic_producto = Ext.getCmp("ic_producto1");
					var ic_if = Ext.getCmp("ic_if1");
					var ic_moneda = Ext.getCmp("ic_moneda1");
					var ic_epo = Ext.getCmp("ic_epo1");
					
					if (     !Ext.isEmpty(ic_producto.getValue()) || !Ext.isEmpty(ic_if.getValue()) 
							|| !Ext.isEmpty(ic_moneda.getValue())  
							|| !Ext.isEmpty(ic_epo.getValue())  ){
						Ext.MessageBox.alert('Mensaje','Solo de debe capturar la Fecha');
						return;
					}
										
					var fecha_de = Ext.getCmp("fecha_de");
					var fecha_a = Ext.getCmp("fecha_a");	
					var fecha_hoy = Ext.getCmp("fecha_hoy");
					
					if(Ext.isEmpty(fecha_de.getValue()) ||  Ext.isEmpty(fecha_a.getValue())){
						if(Ext.isEmpty(fecha_de.getValue())){
							fecha_de.markInvalid('Debe capturar ambas fechas');
							fecha_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_a.getValue())){
							fecha_a.markInvalid('Debe capturar ambas fechas');
							fecha_a.focus();
							return;
						}
					}
					
					var fecha_de_ = Ext.util.Format.date(fecha_de.getValue(),'d/m/Y');
					var fecha_a_ = Ext.util.Format.date(fecha_a.getValue(),'d/m/Y');					
					
					if(!Ext.isEmpty(fecha_de.getValue())){
						if(!isdate(fecha_de_)) { 
							fecha_de.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fecha_de.focus();
						return;
						}
					}
					
					if( !Ext.isEmpty(fecha_a.getValue())){
						if(!isdate(fecha_a_)) { 
							fecha_a.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fecha_a.focus();
							return;
						}
					}					
					
					var resta_de = datecomp(Ext.util.Format.date(fecha_de.getValue(),'d/m/Y'),fecha_hoy.getValue());
					var resta_a = datecomp(Ext.util.Format.date(fecha_a.getValue(),'d/m/Y'),fecha_hoy.getValue());
					var resta_igual = datecomp( Ext.util.Format.date( fecha_de.getValue(),'d/m/Y'), Ext.util.Format.date(fecha_a.getValue(),'d/m/Y') );
					//  0 si son iguales.
					//  1 si la primera es mayor que la segunda
					//  2 si la segunda mayor que la primera
					// -1 si son fechas invalidas
	
					if(resta_de==1) {
						Ext.MessageBox.alert("Mensaje","La fecha debe ser menor o igual a la fecha actual");
						return;
					}				
					if(resta_a==1) {
						Ext.MessageBox.alert("Mensaje","La fecha debe ser menor o igual a la fecha actual");
						return;
					}
					
					if(resta_igual!=0 ) {
						Ext.MessageBox.alert("Mensaje","Se debe de capturar la misma fecha en el rango de fechas");
						return;
					}
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consNacionalData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'ConsNacional',
							ic_moneda:'1'
						})
					});
					
					consDolarData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'ConsDolar',
							ic_moneda:'54'
						})
					});
					
					Ext.getCmp('mensaje1').hide();
					Ext.getCmp('mensaje2').hide();
					Ext.getCmp('gridTotales').hide();
					Ext.getCmp('btnGrabar').hide();
					Ext.getCmp('gridConsulta').hide();					
				}
			},
			{
				text: 'Concentrado del d�a',
				id: 'btnConcentrado',						
				formBind: true,				
				handler: function(boton, evento) {	
					document.location.href  = 	'15constanciacadExt.jsp?pantalla=Concentrado';
				}
			},
			{
				text: 'Total del d�a',
				id: 'btnTotalDia',							
				formBind: true,				
				handler: function(boton, evento) {	
					document.location.href  = 	'15constanciacadExt.jsp?pantalla=Total_dia';
				}
			},
			{
				text: 'Consultar',
				id: 'btnConsultar',				
				formBind: true,
				handler: function(boton, evento) {	
				
					var ic_producto = Ext.getCmp("ic_producto1");
					if (Ext.isEmpty(ic_producto.getValue()) ){
						ic_producto.markInvalid('Debes seleccionar un Producto');
						return;
					}
					
					var ic_if = Ext.getCmp("ic_if1");
					if (Ext.isEmpty(ic_if.getValue()) ){
						ic_if.markInvalid('Debes seleccionar el Intermediario Financiero');
						return;
					}
					var ic_moneda = Ext.getCmp("ic_moneda1");
					if (Ext.isEmpty(ic_moneda.getValue()) ){
						ic_moneda.markInvalid('Debes de seleccionar una Moneda');
						return;
					}
					
					var fecha_de = Ext.getCmp("fecha_de");
					var fecha_a = Ext.getCmp("fecha_a");	
					
					if(Ext.isEmpty(fecha_de.getValue()) || Ext.isEmpty(fecha_a.getValue())){
						if(Ext.isEmpty(fecha_de.getValue())){
							fecha_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_a.getValue())){
							fecha_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_a.focus();
							return;
						}
					}
					
					var fecha_de_ = Ext.util.Format.date(fecha_de.getValue(),'d/m/Y');
					var fecha_a_ = Ext.util.Format.date(fecha_a.getValue(),'d/m/Y');					
					
					if(!Ext.isEmpty(fecha_de.getValue())){
						if(!isdate(fecha_de_)) { 
							fecha_de.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fecha_de.focus();
						return;
						}
					}
					
					if( !Ext.isEmpty(fecha_a.getValue())){
						if(!isdate(fecha_a_)) { 
							fecha_a.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fecha_a.focus();
							return;
						}
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Consultar',
							boton:'Consulta'
						})
					});
					
					Ext.getCmp('mensaje1').hide();
					Ext.getCmp('mensaje2').hide();
					Ext.getCmp('gridTotales').hide();
					Ext.getCmp('btnGrabar').hide();
					Ext.getCmp('gridNacional').hide();
					Ext.getCmp('gridDolar').hide();
					Ext.getCmp('fpBotones').hide();	
				}
			},
			{
				text: 'Captura de Remanente',
				id: 'btnCapturaRem',								
				formBind: true,		
				hidden: true,
				handler: function(boton, evento) {	
				
					var ic_producto = Ext.getCmp("ic_producto1");
					if (Ext.isEmpty(ic_producto.getValue()) ){
						ic_producto.markInvalid('Debe de seleccionar un Producto');
						return;
					}
					
					var ic_if = Ext.getCmp("ic_if1");
					if (Ext.isEmpty(ic_if.getValue()) ){
						ic_if.markInvalid('Debe de seleccionar el Intermediario Financiero');
						return;
					}
					var ic_moneda = Ext.getCmp("ic_moneda1");
					if (Ext.isEmpty(ic_moneda.getValue()) ){
						ic_moneda.markInvalid('Debe de seleccionar una Moneda');
						return;
					}
					
					var fecha_de = Ext.getCmp("fecha_de");
					var fecha_a = Ext.getCmp("fecha_a");	
					
					if(Ext.isEmpty(fecha_de.getValue()) || Ext.isEmpty(fecha_a.getValue())){
						if(Ext.isEmpty(fecha_de.getValue())){
							fecha_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_a.getValue())){
							fecha_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_a.focus();
							return;
						}
					}
					
					var fecha_de_ = Ext.util.Format.date(fecha_de.getValue(),'d/m/Y');
					var fecha_a_ = Ext.util.Format.date(fecha_a.getValue(),'d/m/Y');					
					
					if(!Ext.isEmpty(fecha_de.getValue())){
						if(!isdate(fecha_de_)) { 
							fecha_de.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fecha_de.focus();
						return;
						}
					}
					
					if( !Ext.isEmpty(fecha_a.getValue())){
						if(!isdate(fecha_a_)) { 
							fecha_a.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fecha_a.focus();
							return;
						}
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Consultar',
							boton:'CapturaR'
						})
					});
					
					Ext.getCmp('mensaje1').hide();
					Ext.getCmp('mensaje2').hide();
					Ext.getCmp('gridTotales').hide();
					Ext.getCmp('btnGrabar').hide();
					Ext.getCmp('gridNacional').hide();
					Ext.getCmp('gridDolar').hide();
					Ext.getCmp('fpBotones').hide();	
					
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',								
				formBind: true,				
				handler: function(boton, evento) {	
					document.location.href  = 	'15constanciacadExt.jsp?pantalla=Consulta';	
				}
			}
		]
	});	


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),				
			fp,
			NE.util.getEspaciador(20),		
			mensaje1, 
			gridNacional,
			NE.util.getEspaciador(20),		
			gridConsulta,
			gridTotales,
			gridDolar,
			NE.util.getEspaciador(20),
			mensaje2,
			fpBotones, 
			NE.util.getEspaciador(20)
		]
	});

	catalogoIF.load();
	catProducto.load();	
	catMoneda.load();
	
	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url : '15constanciacadExt.data.jsp',
		params: {
			informacion: "valoresIniciales"			
		},
		callback: procesaValoresIniciales
	});
	
	
});