<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.parametrosgrales.*,
		netropology.utilerias.*, 
		netropology.utilerias.usuarios.*, 
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar="", consulta="",consultaGrid="", mensaje ="";
	System.out.println("informacion *** "+informacion);
	String respuesta="";
	
	JSONObject jsonObj = new JSONObject();
	JSONArray registros1 = new JSONArray();
	HashMap datos = new HashMap();
	if (informacion.equals("CatalogoApis")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_api");
		cat.setCampoClave("cc_api");
		cat.setCampoDescripcion("cg_nombre");
		cat.setOrden("ig_orden");
		
		infoRegresar=cat.getJSONElementos(); 
		
	
	}else if(informacion.equals("Consulta_Epos")){	
		
		String combo_param = (request.getParameter("combo_param")!=null)?request.getParameter("combo_param"):"";
		String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
		String api = (request.getParameter("api")!=null)?request.getParameter("api"):"";
		ParamInterfDesc1erPiso paginador = new ParamInterfDesc1erPiso();
		
		Registros datosReg = paginador.catEpoPrimerPiso(combo_param, ic_epo, api);
		consulta	=	"{\"success\": true, \"total\": \""	+	datosReg.getNumeroRegistros() + "\", \"registros\": " + datosReg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
	}else if(informacion.equals("cat_moneda")){	
		String monedas  				="1";
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre");
		cat.setTabla("comcat_moneda");
		cat.setCondicionIn( monedas, "String.class");
		cat.setOrden("1");
		infoRegresar        = cat.getJSONElementos();
	
	}else if(informacion.equals("descargarArchivo")){	
		String api = (request.getParameter("api")!=null)?request.getParameter("api"):"";
		String tipo = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
		String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
		String moneda = (request.getParameter("moneda")!=null)?request.getParameter("moneda"):"";
		String epo = (request.getParameter("epo")!=null)?request.getParameter("epo"):"";
		HttpSession sessionAux = request.getSession();
		String strPais = (String)sessionAux.getAttribute("strPais");
		String sesExterno = (String)sessionAux.getAttribute("sesExterno");
		String sNombreProducto	  ="1C";
		String nombreArchivo = "";
		
		try {
			ParamInterfDesc1erPiso paginador = new ParamInterfDesc1erPiso();
			
			paginador.setsTipoInterfaz(api);
			paginador.setIc_epo(ic_epo);
			
			
			paginador.setsNombreProducto(sNombreProducto);
		
			nombreArchivo = paginador.getArchivo(tipo,api,epo,sNombreProducto,moneda,strDirectorioTemp,strPais,((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),strNombre,strNombreUsuario,strLogo,strDirectorioPublicacion,sesExterno);
		}catch(Exception e) {
				throw new AppException("Error en la paginación", e);
		}
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("tipo"	,  tipo); 
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		infoRegresar = jsonObj.toString();
		System.out.println("infoRegresar ** "+infoRegresar);
	}else if(informacion.equals("CONSULTA_INFORMACION")){	
		System.out.println("informacion "+informacion);
		String api = (request.getParameter("api")!=null)?request.getParameter("api"):"";
		String api_fin = (request.getParameter("api_fin")!=null)?request.getParameter("api_fin"):"";
		String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
		String rep_val = (request.getParameter("rep_val")!=null)?request.getParameter("rep_val"):"";
		
		String sNombreProducto	  ="1C";
		String listParam = "";
		ParamInterfDesc1erPiso paginador = new ParamInterfDesc1erPiso();
		String existe_param = (!ic_epo.equals(""))?paginador.getExisteParametrizacion("1C",api,ic_epo):"N";
		
		if(api.equals("PY")){
			listParam= "3";
		}else if(api.equals("CN")){
			listParam= "2,7";
		}else if(api.equals("PR")){
			listParam= "9,13,15,16,21,36,37,53,54";
		}else if(api.equals("DS")){
			listParam= "2,9";
		}
	
		paginador.setsTipoInterfaz(api);
		if(existe_param.equals("S")){
			paginador.setIc_epo(ic_epo);
		}else{
			paginador.setIc_epo("");
		}
		paginador.setsNombreProducto(sNombreProducto);
		paginador.setListParam(listParam);
	
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	 
		if (informacion.equals("CONSULTA_INFORMACION") ){ 
			try {
				Registros reg	=	queryHelper.doSearch();
				consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
				jsonObj.put("success", new Boolean(true));
				jsonObj = JSONObject.fromObject(consulta);
				if(api_fin.equals("T")){
					jsonObj.put("api",api_fin);
				}else{
					jsonObj.put("api",api);
				}
				jsonObj.put("rep_val",rep_val);
				infoRegresar = jsonObj.toString();
				System.out.println(" infoRegresar ** "+infoRegresar);
			}catch(Exception e) {
				throw new AppException("Error en la paginación", e);
			}
		}
		
	}else if(informacion.equals("GUARDAR_CAMBIOS")){	
		
		String api = (request.getParameter("api")!=null)?request.getParameter("api"):"";
		String api_fin = (request.getParameter("api_fin")!=null)?request.getParameter("api_fin"):"";
		String IC_EPO = (request.getParameter("IC_EPO")!=null)?request.getParameter("IC_EPO"):"";
		
		ParametrosGrales gral = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
				
		boolean resultado = false;
		if(api.equals("PY")&&!api_fin.equals("T")){
			String numRegistrosPY  =(request.getParameter("numRegistrosPY")!=null) ? request.getParameter("numRegistrosPY"):"";
			int i_numRegPY = Integer.parseInt(numRegistrosPY);
			for(int i = 0; i< i_numRegPY; i++){ 
				String LIST_IC_PARAMETRO_PY[] = request.getParameterValues("LIST_IC_PARAMETRO_PY");
				String LIST_NOMBRE_PARAM_PY[] = request.getParameterValues("LIST_NOMBRE_PARAM_PY");
				String LIST_CC_API_PY[] = request.getParameterValues("LIST_CC_API_PY");
				String LIST_IC_PRODUCTO_PY[] = request.getParameterValues("LIST_IC_PRODUCTO_PY");
				String LIST_IG_PARAM_CAMPO_PY[] = request.getParameterValues("LIST_IG_PARAM_CAMPO_PY");
				
				resultado = gral.guardaParamInterfazDescuento(IC_EPO,LIST_CC_API_PY[i],LIST_IC_PARAMETRO_PY[i],LIST_IC_PRODUCTO_PY[i],LIST_IG_PARAM_CAMPO_PY[i]);
			}
		}if(api.equals("CN")&&!api_fin.equals("T")){
			String numRegistrosCN  =(request.getParameter("numRegistrosCN")!=null) ? request.getParameter("numRegistrosCN"):"";
			int i_numRegCN = Integer.parseInt(numRegistrosCN);
			for(int i = 0; i< i_numRegCN; i++){ 
				String LIST_IC_PARAMETRO_CN[] = request.getParameterValues("LIST_IC_PARAMETRO_CN");
				String LIST_NOMBRE_PARAM_CN[] = request.getParameterValues("LIST_NOMBRE_PARAM_CN");
				String LIST_CC_API_CN[] = request.getParameterValues("LIST_CC_API_CN");
				String LIST_IC_PRODUCTO_CN[] = request.getParameterValues("LIST_IC_PRODUCTO_CN");
				String LIST_IG_PARAM_CAMPO_CN[] = request.getParameterValues("LIST_IG_PARAM_CAMPO_CN");
				
				resultado = gral.guardaParamInterfazDescuento(IC_EPO,LIST_CC_API_CN[i],LIST_IC_PARAMETRO_CN[i],LIST_IC_PRODUCTO_CN[i],LIST_IG_PARAM_CAMPO_CN[i]);
			}
		}else if(api.equals("PR")&&!api_fin.equals("T")){
			String numRegistrosPR  =(request.getParameter("numRegistrosPR")!=null) ? request.getParameter("numRegistrosPR"):"";
			int i_numRegPR = Integer.parseInt(numRegistrosPR);
			for(int i = 0; i< i_numRegPR; i++){ 
				String LIST_IC_PARAMETRO_PR[] = request.getParameterValues("LIST_IC_PARAMETRO_PR");
				String LIST_NOMBRE_PARAM_PR[] = request.getParameterValues("LIST_NOMBRE_PARAM_PR");
				String LIST_CC_API_PR[] = request.getParameterValues("LIST_CC_API_PR");
				String LIST_IC_PRODUCTO_PR[] = request.getParameterValues("LIST_IC_PRODUCTO_PR");
				String LIST_IG_PARAM_CAMPO_PR[] = request.getParameterValues("LIST_IG_PARAM_CAMPO_PR");
				resultado = gral.guardaParamInterfazDescuento(IC_EPO,LIST_CC_API_PR[i],LIST_IC_PARAMETRO_PR[i],LIST_IC_PRODUCTO_PR[i],LIST_IG_PARAM_CAMPO_PR[i]);
			}
		}else if(api.equals("DS")&&!api_fin.equals("T")){
			String numRegistrosDS  =(request.getParameter("numRegistrosDS")!=null) ? request.getParameter("numRegistrosDS"):"";
			int i_numRegDS = Integer.parseInt(numRegistrosDS);
			for(int i = 0; i< i_numRegDS; i++){ 
				String LIST_IC_PARAMETRO_DS[] = request.getParameterValues("LIST_IC_PARAMETRO_DS");
				String LIST_NOMBRE_PARAM_DS[] = request.getParameterValues("LIST_NOMBRE_PARAM_DS");
				String LIST_CC_API_DS[] = request.getParameterValues("LIST_CC_API_DS");
				String LIST_IC_PRODUCTO_DS[] = request.getParameterValues("LIST_IC_PRODUCTO_DS");
				String LIST_IG_PARAM_CAMPO_DS[] = request.getParameterValues("LIST_IG_PARAM_CAMPO_DS");
				
				resultado = gral.guardaParamInterfazDescuento(IC_EPO,LIST_CC_API_DS[i],LIST_IC_PARAMETRO_DS[i],LIST_IC_PRODUCTO_DS[i],LIST_IG_PARAM_CAMPO_DS[i]);
			}
		}else if(api_fin.equals("T")){
			String numRegistrosPY  =(request.getParameter("numRegistrosPY")!=null) ? request.getParameter("numRegistrosPY"):"";
			int i_numRegPY = Integer.parseInt(numRegistrosPY);
			String numRegistrosCN  =(request.getParameter("numRegistrosCN")!=null) ? request.getParameter("numRegistrosCN"):"";
			int i_numRegCN = Integer.parseInt(numRegistrosCN);
			String numRegistrosPR  =(request.getParameter("numRegistrosPR")!=null) ? request.getParameter("numRegistrosPR"):"";
			int i_numRegPR = Integer.parseInt(numRegistrosPR);
			String numRegistrosDS  =(request.getParameter("numRegistrosDS")!=null) ? request.getParameter("numRegistrosDS"):"";
			int i_numRegDS = Integer.parseInt(numRegistrosDS);
			boolean resultado_G1 = false;
			boolean resultado_G2 = false;
			boolean resultado_G3 = false;
			boolean resultado_G4 = false;
			for(int i = 0; i< i_numRegPY; i++){ 
				String LIST_IC_PARAMETRO_PY[] = request.getParameterValues("LIST_IC_PARAMETRO_PY");
				String LIST_NOMBRE_PARAM_PY[] = request.getParameterValues("LIST_NOMBRE_PARAM_PY");
				String LIST_CC_API_PY[] = request.getParameterValues("LIST_CC_API_PY");
				String LIST_IC_PRODUCTO_PY[] = request.getParameterValues("LIST_IC_PRODUCTO_PY");
				String LIST_IG_PARAM_CAMPO_PY[] = request.getParameterValues("LIST_IG_PARAM_CAMPO_PY");
				
				resultado_G1 = gral.guardaParamInterfazDescuento(IC_EPO,LIST_CC_API_PY[i],LIST_IC_PARAMETRO_PY[i],LIST_IC_PRODUCTO_PY[i],LIST_IG_PARAM_CAMPO_PY[i]);
			}
			for(int i = 0; i< i_numRegCN; i++){ 
				String LIST_IC_PARAMETRO_CN[] = request.getParameterValues("LIST_IC_PARAMETRO_CN");
				String LIST_NOMBRE_PARAM_CN[] = request.getParameterValues("LIST_NOMBRE_PARAM_CN");
				String LIST_CC_API_CN[] = request.getParameterValues("LIST_CC_API_CN");
				String LIST_IC_PRODUCTO_CN[] = request.getParameterValues("LIST_IC_PRODUCTO_CN");
				String LIST_IG_PARAM_CAMPO_CN[] = request.getParameterValues("LIST_IG_PARAM_CAMPO_CN");
				
				resultado_G2 = gral.guardaParamInterfazDescuento(IC_EPO,LIST_CC_API_CN[i],LIST_IC_PARAMETRO_CN[i],LIST_IC_PRODUCTO_CN[i],LIST_IG_PARAM_CAMPO_CN[i]);
			}
			for(int i = 0; i< i_numRegPR; i++){ 
				String LIST_IC_PARAMETRO_PR[] = request.getParameterValues("LIST_IC_PARAMETRO_PR");
				String LIST_NOMBRE_PARAM_PR[] = request.getParameterValues("LIST_NOMBRE_PARAM_PR");
				String LIST_CC_API_PR[] = request.getParameterValues("LIST_CC_API_PR");
				String LIST_IC_PRODUCTO_PR[] = request.getParameterValues("LIST_IC_PRODUCTO_PR");
				String LIST_IG_PARAM_CAMPO_PR[] = request.getParameterValues("LIST_IG_PARAM_CAMPO_PR");
				
				resultado_G3 = gral.guardaParamInterfazDescuento(IC_EPO,LIST_CC_API_PR[i],LIST_IC_PARAMETRO_PR[i],LIST_IC_PRODUCTO_PR[i],LIST_IG_PARAM_CAMPO_PR[i]);
			}
			for(int i = 0; i< i_numRegDS; i++){ 
				String LIST_IC_PARAMETRO_DS[] = request.getParameterValues("LIST_IC_PARAMETRO_DS");
				String LIST_NOMBRE_PARAM_DS[] = request.getParameterValues("LIST_NOMBRE_PARAM_DS");
				String LIST_CC_API_DS[] = request.getParameterValues("LIST_CC_API_DS");
				String LIST_IC_PRODUCTO_DS[] = request.getParameterValues("LIST_IC_PRODUCTO_DS");
				String LIST_IG_PARAM_CAMPO_DS[] = request.getParameterValues("LIST_IG_PARAM_CAMPO_DS");
				
				resultado_G3 = gral.guardaParamInterfazDescuento(IC_EPO,LIST_CC_API_DS[i],LIST_IC_PARAMETRO_DS[i],LIST_IC_PRODUCTO_DS[i],LIST_IG_PARAM_CAMPO_DS[i]);
			}
			if(resultado_G1==true && resultado_G2==true && resultado_G3==true && resultado_G4==true  ){
				resultado = true;
			}else{
				resultado = false;
			}
		}
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("resultado", new Boolean(resultado));
		infoRegresar =	jsonObj.toString();
	}else if(informacion.equals("PARAMETROS_DEFAULT")){
		String api = (request.getParameter("api")!=null)?request.getParameter("api"):"";
		ParamInterfDesc1erPiso paginador = new ParamInterfDesc1erPiso();
		List datosReg = paginador.getParamDefault(api,"1C");
		
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("datosValores",datosReg);
		infoRegresar =	jsonObj.toString();
		System.out.println("infoRegresar *** "+infoRegresar);
	}
	
	%>	

<%= infoRegresar %>


