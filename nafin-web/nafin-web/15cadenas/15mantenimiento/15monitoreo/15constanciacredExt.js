
	function selecCONFIR_DL(check, rowIndex, colIds){
		var  gridDolar = Ext.getCmp('gridDolar');	
		var store = gridDolar.getStore();
		var reg = gridDolar.getStore().getAt(rowIndex);
	 
		if(check.checked==true)  {
		reg.set('CONFIRMA', "S");				
		}else  {
			reg.set('CONFIRMA', "N"); 
		}	
		store.commitChanges();	
	}
	
	function selecCONFIR_MN(check, rowIndex, colIds){
		var  gridNacional = Ext.getCmp('gridNacional');	
		var store = gridNacional.getStore();
		var reg = gridNacional.getStore().getAt(rowIndex);
		if(check.checked==true)  {
			reg.set('CONFIRMA', "S");				
		}else  {
			reg.set('CONFIRMA', "N"); 
		}	
		store.commitChanges();	
	}
	

Ext.onReady(function(){

//********************DESCARGA ARCHIVO **********************************************
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}



//**************************ESTA PARTE CORRESPONDE AL BOTON DE Confirmaci�n de Operaciones del d�a  **********************************


	function repCofirmacion(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			if(info.mensaje !='')  {
				Ext.MessageBox.alert('Mensaje',info.mensaje,
					function(){
						fp.el.mask('Enviando...', 'x-mask-loading');			
						consNacionalData.load({
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ConsNacional',
								ic_moneda:'1'
							})
						});
						consDolarData.load({
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ConsDolar',
								ic_moneda:'54'
							})
						});
					
					}
				);				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var proConfirmacion = function() {
		
		var  gridNacional = Ext.getCmp('gridNacional');
		var storeMN = gridNacional.getStore();		
		
		var  gridDolar = Ext.getCmp('gridDolar');
		var storeDL = gridDolar.getStore();	
		
		var ic_ifMN = [];
		var ic_ifDL = [];
		var confirmaOperacionMN ="N";
		var confirmaOperacionDL ="N";
		var numRegMN =0;
		var numRegDL =0;
		
		storeMN.each(function(record) {	
			if(record.data['IC_IF']!=''){
				ic_ifMN.push(record.data['IC_IF']);
				numRegMN++;
			}
			confirmaOperacionMN= record.data['CONFIRMA'];								
		});
		
		storeDL.each(function(record) {	
			if(record.data['IC_IF']!=''){
				ic_ifDL.push(record.data['IC_IF']);
				numRegDL++;
			}
			confirmaOperacionDL= record.data['CONFIRMA'];								
		});
		
	
		Ext.Ajax.request({
			url: '15constanciacredExt.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'AceptarConfirmacion',	
				ic_ifMN:ic_ifMN,
				ic_ifDL:ic_ifDL,
				numRegMN:numRegMN,
				numRegDL:numRegDL, 
				confirmaOperacionMN:confirmaOperacionMN,
				confirmaOperacionDL:confirmaOperacionDL
			}),
			callback: repCofirmacion
		});					
	}
	



	var procConsMNacionalData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridNacional = Ext.getCmp('gridNacional');	
		var el = gridNacional.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridNacional.isVisible()) {
				gridNacional.show();
			}	
						
			var cm = gridNacional.getColumnModel();
			var el = gridNacional.getGridEl();	
			var info = store.reader.jsonData;		
			if(info.numRegistrosMN=='0') {
				gridNacional.hide();
			}
			Ext.getCmp('numRegistrosMN').setValue(info.numRegistrosMN);
						
			if(Ext.getCmp('numRegistrosMN').getValue()!='0' ||  Ext.getCmp('numRegistrosDL').getValue() !='0') {
				Ext.getCmp('fpBotones').show();	
			}
			if(Ext.getCmp('numRegistrosMN').getValue()=='0' && Ext.getCmp('numRegistrosDL').getValue() =='0') {
				Ext.getCmp('fpBotones').hide();	
			}
			
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consNacionalData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15constanciacredExt.data.jsp',
		baseParams: {
			informacion: 'ConsNacional'
		},		
		fields: [	
			{	name: 'IC_IF'},
			{	name: 'NOMBRE_IF'},
			{	name: 'NUM_OPERACIONES'},
			{	name: 'MONTO'},
			{	name: 'CHECKBOX'},
			{	name: 'CONFIRMA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procConsMNacionalData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procConsMNacionalData(null, null, null);					
				}
			}
		}		
	});
	
	var gridNacional = new Ext.grid.EditorGridPanel({	
		store: consNacionalData,
		id: 'gridNacional',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		hidden: true,
		title:'Moneda: Moneda Nacional',
		columns: [			
			{
				header: 'Nombre IF',				
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 250,			
				resizable: true,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'N�mero de Operaciones',				
				dataIndex: 'NUM_OPERACIONES',
				sortable: true,
				width: 240,			
				resizable: true,
				align: 'center'				
			},
			{
				header: 'Monto',				
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,
				align: 'right',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					var checked= '';
					if(record.data['CONFIRMA']=='S') {  checked='checked'; }
					if(record.data['CHECKBOX']=='SI'){
            		return '<input id="confirmaOperacionMN"'+ rowIndex + 'value="DL" type="checkbox" '+checked+' onclick="selecCONFIR_MN(this,'+rowIndex +','+colIndex+');"/>';				
            	}else{
            		return value;
            	}
            }				
			}		
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 200,
		width: 650,
		align: 'center',
		frame: false
	});

	var procConsDolarData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridDolar = Ext.getCmp('gridDolar');	
		var el = gridDolar.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridDolar.isVisible()) {
				gridDolar.show();
			}	
						
			var cm = gridDolar.getColumnModel();
			var el = gridDolar.getGridEl();	
			var info = store.reader.jsonData;		
			if(info.numRegistrosDL=='0') {
				gridDolar.hide();
			}
			
			Ext.getCmp('numRegistrosDL').setValue(info.numRegistrosDL);
						
			if(Ext.getCmp('numRegistrosMN').getValue()!='0' ||  Ext.getCmp('numRegistrosDL').getValue() !='0') {
				Ext.getCmp('fpBotones').show();	
			}
			if(Ext.getCmp('numRegistrosMN').getValue()=='0' && Ext.getCmp('numRegistrosDL').getValue() =='0') {
				Ext.getCmp('fpBotones').hide();	
			}
			
			
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consDolarData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15constanciacredExt.data.jsp',
		baseParams: {
			informacion: 'ConsDolar'
		},		
		fields: [
			{	name: 'IC_IF'},
			{	name: 'NOMBRE_IF'},
			{	name: 'NUM_OPERACIONES'},
			{	name: 'MONTO'},
			{	name: 'CHECKBOX'},
			{	name: 'CONFIRMA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procConsDolarData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procConsDolarData(null, null, null);					
				}
			}
		}		
	});
	
	var gridDolar = new Ext.grid.EditorGridPanel({	
		store: consDolarData,
		id: 'gridDolar',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		title:'Moneda: D�lares',
		hidden: true,		
		columns: [			
			{
				header: 'Nombre IF',				
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 250,			
				resizable: true,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'N�mero de Operaciones',				
				dataIndex: 'NUM_OPERACIONES',
				sortable: true,
				width: 240,			
				resizable: true,
				align: 'center'				
			},
			{
				header: 'Monto',				
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,
				align: 'right',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					var checked= '';
					if(record.data['CONFIRMA']=='S') {  checked='checked'; }
					if(record.data['CHECKBOX']=='SI'){
            		return '<input id="confirmaOperacionDL"'+ rowIndex + 'value="DL" type="checkbox" '+checked+' onclick="selecCONFIR_DL(this,'+rowIndex +','+colIndex+');"/>';
            	}else{
            		return value;
            	}
            }				
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 200,
		width: 650,
		align: 'center',
		frame: false		
	});



	

	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },	
		hidden: true,
		items: [			
			{
				xtype: 'button',				
				text: '<h1><b>Aceptar</h1></b>',	
				id: 'btnAceptar',
				handler:proConfirmacion
			},
			{ xtype: 'textfield',	id: 'numRegistrosMN' , hidden: true	},
			{ xtype: 'textfield',	id: 'numRegistrosDL' , hidden: true	}
		]
	};
	
//**************************ESTA PARTE CORRESPONDE AL BOTON DE Operaciones Confirmadas por IF  **********************************

	var mostrarArchivoOper = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var ic_moneda = registro.get('IC_MONEDA');
		var lsFechaOperacion = registro.get('FECHA_ENVIO');
		var lsNomMoneda = registro.get('MONEDA');
		var lsImporteTotalO =  registro.get('MONTO');
		var lsNombreIF = registro.get('NOMBRE_IF');
		Ext.Ajax.request({
			url: '15constanciacredExt.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Archivos',
				accionBoton:'Operaciones',
				tipoA:'PDF',			
				ic_moneda: ic_moneda,
				lsFechaOperacion:lsFechaOperacion,
				lsNomMoneda:lsNomMoneda,
				lsImporteTotalO:lsImporteTotalO,
				lsNombreIF:lsNombreIF
			}),
			callback: descargaArchivo
		});			
	}
	
	
	var procConsOperaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridOperaciones = Ext.getCmp('gridOperaciones');	
		var el = gridOperaciones.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridOperaciones.isVisible()) {
				gridOperaciones.show();
			}	
						
			var cm = gridOperaciones.getColumnModel();
			var el = gridOperaciones.getGridEl();	
			var info = store.reader.jsonData;		
	
			Ext.getCmp('btnOperaciones').hide();
			Ext.getCmp('btnConsultarOper').show();	

			if(store.getTotalCount() > 0) {								
				el.unmask();					
			} else {					
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
			
	var consOperaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15constanciacredExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaOperacion'
		},		
		fields: [	
			{	name: 'FECHA_ENVIO'},		
			{	name: 'NUM_OPERACIONES'},		
			{	name: 'MONTO'},		
			{	name: 'MONEDA'},		
			{	name: 'TIPO'},		
			{	name: 'USUARIO_REVISO'},		
			{	name: 'FECHA_REVISION'},		
			{	name: 'CONSTANCIA'},
			{	name: 'NOMBRE_IF'},	
			{	name: 'TIPO_PLAZO'},	
			{	name: 'IC_MONEDA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procConsOperaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procConsOperaData(null, null, null);					
				}
			}
		}		
	});
	
	
	
	var gridOperaciones = new Ext.grid.EditorGridPanel({	
		store: consOperaData,
		id: 'gridOperaciones',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consultar',
		clicksToEdit: 1,
		hidden: true,		
		columns: [			
			{
				header: 'Fecha de envio de operaciones',				
				dataIndex: 'FECHA_ENVIO',
				sortable: true,
				width: 150,			
				resizable: true,								
				align: 'center'				
			},
			{
				header: 'N�mero de Operaciones',				
				dataIndex: 'NUM_OPERACIONES',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'center'				
			},
			{
				header: 'Monto',				
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'Moneda',				
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'center'				
			},
			{
				header: 'Tipo',				
				dataIndex: 'TIPO',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'center'				
			},
			{
				header: 'Usuario Reviso',				
				dataIndex: 'USUARIO_REVISO',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'center'				
			},
			{
				header: 'Fecha Hora Revisi�n',				
				dataIndex: 'FECHA_REVISION',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'center'				
			},
			{
         	xtype: 'actioncolumn',
            header: 'Constancia',
            dataIndex: 'CONSTANCIA',
            sortable: false,            
            width: 150,
            align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					return 'Ver';
				},				
            items:[		      	
					{ //para enviar 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							this.items[0].tooltip = 'Descargar';
							return 'icoPdf';																					
						},
						handler: mostrarArchivoOper
					}
            ]
         }	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false
	});
	
	
	
	//**************************ESTA PARTE CORRESPONDE AL BOTON DE CONSULTAR **********************************
	
	var mostrarArchivo = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var txtfolio = registro.get('NUM_FOLIO');
		Ext.Ajax.request({
			url: '15constanciacredExt.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Archivos',
				tipoA:'PDF',
				accionBoton:'Consultar',
				txtfolio: txtfolio				
			}),
			callback: descargaArchivo
		});			
	}
	
	//********************CONSULTA **********************************************

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
						
			var cm = gridConsulta.getColumnModel();
			var el = gridConsulta.getGridEl();	
			var info = store.reader.jsonData;		
	
			if(store.getTotalCount() > 0) {	
				Ext.getCmp('btnArchivoXLS').enable();				
				el.unmask();					
			} else {	
				Ext.getCmp('btnArchivoXLS').disable();				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
			
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15constanciacredExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			
			{	name: 'NUM_FOLIO'},	
			{	name: 'NUM_PRESTAMO'},
			{	name: 'IMPRIMIR'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	

	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consultar',
		clicksToEdit: 1,
		hidden: true,		
		columns: [			
			{
				header: 'Folio',				
				dataIndex: 'NUM_FOLIO',
				sortable: true,
				width: 150,			
				resizable: true,								
				align: 'center'				
			},
			{
				header: 'N�mero Pr�stamo',				
				dataIndex: 'NUM_PRESTAMO',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'center'				
			},
			{
         	xtype: 'actioncolumn',
            header: '',
            dataIndex: 'IMPRIMIR',
            sortable: false,            
            width: 150,
            align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					return 'Imprimir';
				},				
            items:[		      	
					{ //para enviar 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							this.items[0].tooltip = 'Descargar';
							return 'icoPdf';																					
						},
						handler: mostrarArchivo					
					}
            ]
         }	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 460,
		align: 'center',
		frame: false,
		bbar: {
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo XLS ',					
					tooltip:	'Generar Archivo XLS ',
					iconCls: 'icoAceptar',
					id: 'btnArchivoXLS',					
					handler: function(boton, evento) {												
						Ext.Ajax.request({
							url: '15constanciacredExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'Archivos',
								tipoA:'XLS',
								accionBoton:'Consultar'
							}),							
							callback: descargaArchivo
						});						
					}
				}
			]
		}
	});
			
//**********************CRITERIOS DE BUSQUEDA **************************
	
		function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var info = Ext.util.JSON.decode(response.responseText);
			
			var fp= Ext.getCmp('forma');
			
			Ext.getCmp('txtfechaIni').setValue(info.fechaHoy);
			Ext.getCmp('txtfechaFin').setValue(info.fechaHoy);	
			Ext.getCmp('fecha_hoy').setValue(info.fechaHoy);	
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15constanciacredExt.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma = [
		{
			xtype: 'combo',
			fieldLabel: 'IF',
			name: 'ic_if',
			id: 'ic_if1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_if',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoIF,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120	
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Folio',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'textfield',
					fieldLabel: 'Folio',
					name: 'txtfolio',
					id: 'txtfolio',
					allowBlank: true,			
					maxLength: 100,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'			
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha',
			combineErrors: false,
			msgTarget: 'side',
			width: 600,
			items: [
				{
					xtype: 'displayfield',
					value: 'De',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txtfechaIni',
					id: 'txtfechaIni',
					allowBlank: true,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtfechaFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'Hasta',
					width: 40
				},
				{
					xtype: 'datefield',
					name: 'txtfechaFin',
					id: 'txtfechaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 					
					campoInicioFecha: 'txtfechaIni',
					margins: '0 20 0 0' 
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 20
				}
			]
		},
		{
			xtype: 'textfield',
			name: 'fecha_hoy',
			id: 'fecha_hoy',
			allowBlank: true,
			startDay: 1,
			width: 100,
			msgTarget: 'side',
			hidden: true,
			margins: '0 20 0 0'			
		}
	];
	
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 750,
		style: 'margin:0 auto;',
		title: 'Cr�dito Electr�nico',
		hidden: false,
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 70,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [	
			{
				text: 'Confirmaci�n de Operaciones del d�a',
				id: 'btnConfirma',									
				formBind: true,				
				handler: function(boton, evento) {	
					var ic_if = Ext.getCmp("ic_if1");
					var txtfolio = Ext.getCmp("txtfolio");
					
					if ( !Ext.isEmpty(ic_if.getValue()) || !Ext.isEmpty(txtfolio.getValue())  ){
						Ext.MessageBox.alert('Mensaje','Solo de debe capturar la Fecha');
						return;
					}										
					var txtfechaIni = Ext.getCmp("txtfechaIni");
					var txtfechaFin = Ext.getCmp("txtfechaFin");	
					var fecha_hoy = Ext.getCmp("fecha_hoy");
					
					if(Ext.isEmpty(txtfechaIni.getValue()) ||   Ext.isEmpty(txtfechaFin.getValue())){
						if(Ext.isEmpty(txtfechaIni.getValue())){
							txtfechaIni.markInvalid('Debe capturar ambas fechas');
							txtfechaIni.focus();
							return;
						}
						if(Ext.isEmpty(txtfechaFin.getValue())){
							txtfechaFin.markInvalid('Debe capturar ambas fechas');
							txtfechaFin.focus();
							return;
						}
					}
					
					var txtfechaIni_ = Ext.util.Format.date(txtfechaIni.getValue(),'d/m/Y');
					var txtfechaFin_ = Ext.util.Format.date(txtfechaFin.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(txtfechaIni.getValue())){
						if(!isdate(txtfechaIni_)) { 
							txtfechaIni.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txtfechaIni.focus();
							return;
						}
					}
					if( !Ext.isEmpty(txtfechaFin.getValue())){
						if(!isdate(txtfechaFin_)) { 
							txtfechaFin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txtfechaFin.focus();
							return;
						}
					}

					var resta_de = datecomp(Ext.util.Format.date(txtfechaIni.getValue(),'d/m/Y'),fecha_hoy.getValue());
					var resta_a = datecomp(Ext.util.Format.date(txtfechaFin.getValue(),'d/m/Y'),fecha_hoy.getValue());
					var resta_igual = datecomp( Ext.util.Format.date( txtfechaIni.getValue(),'d/m/Y'), Ext.util.Format.date(txtfechaFin.getValue(),'d/m/Y') );
					//  0 si son iguales.
					//  1 si la primera es mayor que la segunda
					//  2 si la segunda mayor que la primera
					// -1 si son fechas invalidas

					if(resta_de==1) {
						Ext.MessageBox.alert("Mensaje","La fecha debe ser menor o igual a la fecha actual");
						return;
					}				
					if(resta_a==1) {
						Ext.MessageBox.alert("Mensaje","La fecha debe ser menor o igual a la fecha actual");
						return;
					}
					
					if(resta_igual!=0 ) {
						Ext.MessageBox.alert("Mensaje","Se debe de capturar la misma fecha en el rango de fechas");
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consNacionalData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'ConsNacional',
							ic_moneda:'1'
						})
					});
					
					consDolarData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'ConsDolar',
							ic_moneda:'54'
						})
					});
					
					Ext.getCmp('gridConsulta').hide();
					
				}
			},
			{
				text: 'Concentrado del d�a',
				id: 'btnConcentrado',						
				formBind: true,				
				handler: function(boton, evento) {	
					document.location.href  = 	'15constanciacredExt.jsp?pantalla=Concentrado';	
				}
			},
			{
				text: 'Total del d�a',
				id: 'btnTotalDia',							
				formBind: true,				
				handler: function(boton, evento) {	
					document.location.href  = 	'15constanciacredExt.jsp?pantalla=Total_dia';	
				}
			},
			{
				text: 'Consultar',
				id: 'btnConsultar',
				formBind: true,				
				handler: function(boton, evento) {	
				
					var ic_if = Ext.getCmp("ic_if1");
					if (Ext.isEmpty(ic_if.getValue()) ){
						ic_if.markInvalid('Debes seleccionar el Intermediario Financiero');
						return;
					}
					
					var txtfechaIni = Ext.getCmp("txtfechaIni");
					var txtfechaFin = Ext.getCmp("txtfechaFin");	
					
					if(Ext.isEmpty(txtfechaIni.getValue()) || Ext.isEmpty(txtfechaFin.getValue())){
						if(Ext.isEmpty(txtfechaIni.getValue())){
							txtfechaIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtfechaIni.focus();
							return;
						}
						if(Ext.isEmpty(txtfechaFin.getValue())){
							txtfechaFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtfechaFin.focus();
							return;
						}
					}
					var txtfechaIni_ = Ext.util.Format.date(txtfechaIni.getValue(),'d/m/Y');
					var txtfechaFin_ = Ext.util.Format.date(txtfechaFin.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(txtfechaIni.getValue())){
						if(!isdate(txtfechaIni_)) { 
							txtfechaIni.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txtfechaIni.focus();
							return;
						}
					}
					if( !Ext.isEmpty(txtfechaFin.getValue())){
						if(!isdate(txtfechaFin_)) { 
							txtfechaFin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txtfechaFin.focus();
							return;
						}
					}
					
					Ext.getCmp('gridOperaciones').hide();
					Ext.getCmp('gridDolar').hide();
				   Ext.getCmp('gridNacional').hide();
					Ext.getCmp('fpBotones').hide();
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Consultar',
							accionBoton:'Consultar'
						})
					});
					
					
					
				}
			},
			{
				text: 'Consultar',
				id: 'btnConsultarOper',
				formBind: true,	
				hidden: true,
				handler: function(boton, evento) {	
					
					var ic_if = Ext.getCmp("ic_if1");
					if (Ext.isEmpty(ic_if.getValue()) ){
						ic_if.markInvalid('Debes seleccionar el Intermediario Financiero');
						return;
					}
					
					var txtfechaIni = Ext.getCmp("txtfechaIni");
					var txtfechaFin = Ext.getCmp("txtfechaFin");	
					
					if(Ext.isEmpty(txtfechaIni.getValue()) || Ext.isEmpty(txtfechaFin.getValue())){
						if(Ext.isEmpty(txtfechaIni.getValue())){
							txtfechaIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtfechaIni.focus();
							return;
						}
						if(Ext.isEmpty(txtfechaFin.getValue())){
							txtfechaFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtfechaFin.focus();
							return;
						}
					}
					
					var txtfechaIni_ = Ext.util.Format.date(txtfechaIni.getValue(),'d/m/Y');
					var txtfechaFin_ = Ext.util.Format.date(txtfechaFin.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(txtfechaIni.getValue())){
						if(!isdate(txtfechaIni_)) { 
							txtfechaIni.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txtfechaIni.focus();
							return;
						}
					}
					if( !Ext.isEmpty(txtfechaFin.getValue())){
						if(!isdate(txtfechaFin_)) { 
							txtfechaFin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txtfechaFin.focus();
							return;
						}
					}
					
					Ext.getCmp('gridConsulta').hide();	
					Ext.getCmp('btnConfirma').hide();	
					Ext.getCmp('btnTotalDia').hide();	
					Ext.getCmp('btnConsultar').hide();	
					Ext.getCmp('btnConcentrado').hide();	
					Ext.getCmp('gridDolar').hide();
				   Ext.getCmp('gridNacional').hide();
					Ext.getCmp('fpBotones').hide();
				  
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consOperaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'ConsultaOperacion',
							accionBoton:'Operaciones'
						})
					});	
				}
			},			
			{
				text: 'Limpiar',
				id: 'btnLimpiar',								
				formBind: true,				
				handler: function(boton, evento) {	
					document.location.href  = 	'15constanciacredExt.jsp?pantalla=Consulta';	
				}
			},
			{
				text: 'Operaciones Confirmadas por IF ',
				id: 'btnOperaciones',								
				formBind: true,				
				handler: function(boton, evento) {	
				
					var ic_if = Ext.getCmp("ic_if1");
					if (Ext.isEmpty(ic_if.getValue()) ){
						ic_if.markInvalid('Debes seleccionar el Intermediario Financiero');
						return;
					}
					
					var txtfechaIni = Ext.getCmp("txtfechaIni");
					var txtfechaFin = Ext.getCmp("txtfechaFin");	
					
					if(Ext.isEmpty(txtfechaIni.getValue()) || Ext.isEmpty(txtfechaFin.getValue())){
						if(Ext.isEmpty(txtfechaIni.getValue())){
							txtfechaIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtfechaIni.focus();
							return;
						}
						if(Ext.isEmpty(txtfechaFin.getValue())){
							txtfechaFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtfechaFin.focus();
							return;
						}
					}
					
					var txtfechaIni_ = Ext.util.Format.date(txtfechaIni.getValue(),'d/m/Y');
					var txtfechaFin_ = Ext.util.Format.date(txtfechaFin.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(txtfechaIni.getValue())){
						if(!isdate(txtfechaIni_)) { 
							txtfechaIni.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txtfechaIni.focus();
							return;
						}
					}
					if( !Ext.isEmpty(txtfechaFin.getValue())){
						if(!isdate(txtfechaFin_)) { 
							txtfechaFin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txtfechaFin.focus();
							return;
						}
					}
					
					Ext.getCmp('gridConsulta').hide();	
					Ext.getCmp('btnConfirma').hide();	
					Ext.getCmp('btnTotalDia').hide();	
					Ext.getCmp('btnConsultar').hide();	
					Ext.getCmp('btnConcentrado').hide();	
					Ext.getCmp('gridDolar').hide();
				   Ext.getCmp('gridNacional').hide();
					Ext.getCmp('fpBotones').hide();
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consOperaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'ConsultaOperacion',
							accionBoton:'Operaciones'
						})
					});	
					
				}
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),				
			fp,	
			gridNacional, 
			NE.util.getEspaciador(20),
			gridConsulta,
			gridOperaciones,						
			gridDolar,			
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20)
		]
	});

	catalogoIF.load();

	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url : '15constanciacredExt.data.jsp',
		params: {
			informacion: "valoresIniciales"			
		},
		callback: procesaValoresIniciales
	});
	
});