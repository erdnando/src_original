<%@ page contentType="application/json;charset=UTF-8" 
	import="  java.sql.ResultSet, 
	java.util.*,
	java.text.*,
	com.netro.procesos.*, 
	com.netro.cadenas.*,   
	net.sf.json.*,
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%  
String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
SimpleDateFormat formatoHora2 = new SimpleDateFormat("dd/MM/yyyy; hh:mm:ss a");
String infoRegresar = "";

//Permite cargar valores de usuario y fecha
if (informacion.equals("valoresIniciales")) {  
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("strNombreUsuario", strNombreUsuario);
	jsonObj.put("fechaHora",formatoHora2.format(new java.util.Date()));
	infoRegresar = jsonObj.toString();

//Tareas realizadas en operaciones en proceso
}else if (informacion.equals("consultaDatosProceso") || 
			informacion.equals("generarPdfProceso")) {
			
	CQueryHelperRegExtJS queryHelper;
	ConsOperacionesEstatus paginador = new ConsOperacionesEstatus();
	paginador.setOperacionEstatus("En Proceso");
	queryHelper = new CQueryHelperRegExtJS(paginador);	
	//Generar consulta Operaciones con estatus en proceso	
	if(informacion.equals("consultaDatosProceso")){
		try{
			Registros reg = queryHelper.doSearch();
			infoRegresar	=		"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
	//Generar PDF Operaciones con estatus en proceso
	}else if(informacion.equals("generarPdfProceso")){
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "pdf");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}

//Tareas realizadas en operaciones en proceso	
}else if (informacion.equals("consultaDatosOperadas") ||
			informacion.equals("generarPdfOperadas")) {
	
	CQueryHelperRegExtJS queryHelper;
	ConsOperacionesEstatus paginador = new ConsOperacionesEstatus();
	paginador.setOperacionEstatus("Operadas");
	queryHelper = new CQueryHelperRegExtJS(paginador);	
	//Generar consulta Operaciones con estatus operadas
	if (informacion.equals("consultaDatosOperadas")){
		try{
			Registros reg = queryHelper.doSearch();
			infoRegresar	=		"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
	//Generar PDF Operaciones con estatus operadas
	}else if (informacion.equals("generarPdfOperadas")) {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "pdf");
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		infoRegresar = jsonObj.toString();
	}
}
%>
<%=  infoRegresar%>
