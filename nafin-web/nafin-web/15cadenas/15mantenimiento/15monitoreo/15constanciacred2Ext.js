Ext.onReady(function() {

	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(infoR.mensaje !='')  {
				Ext.MessageBox.alert('Mensaje',infoR.mensaje);
			}else {
				var archivo = infoR.urlArchivo;				
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		
	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('fecha').setValue(info.fechaHoy);
						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var elementosForma =[
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha',			
			msgTarget: 'side',
			combineErrors: false,			
			width: 330,			
			items: [
				{	
					fieldLabel: 'Fecha',
					xtype: 'datefield',
					name: 'fecha',
					id: 'fecha',					
					width: 100,
					msgTarget: 'side',								
					margins: '0 20 0 0' 
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		}
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 330,
		style: 'margin:0 auto;',
		title: 'Concentrado del d�a',
		hidden: false,
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [	
			{
				text: 'Generar Archivo',
				id: 'btnGenArc',									
				formBind: true,				
				handler: function(boton, evento) {					
				
					var fecha = Ext.getCmp("fecha");
					if(Ext.isEmpty(fecha.getValue())){
						fecha.markInvalid('Debe capturar la fecha');
						fecha.focus();
						return;
					}			
					
					var fecha_ = Ext.util.Format.date(fecha.getValue(),'d/m/Y');
				
					if(!Ext.isEmpty(fecha.getValue())){
						if(!isdate(fecha_)) { 
							fecha.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fecha.focus();
							return;
						}
					}
				
					Ext.Ajax.request({
						url: '15constanciacredExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Concentrado_dia'
						}),							
						callback: descargaArchivo
					});	
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',									
				formBind: true,				
				handler: function(boton, evento) {	
					document.location.href  = 	'15constanciacredExt.jsp?pantalla=Concentrado';
				}
			}
		]
	});	


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),				
			fp,
			NE.util.getEspaciador(20),			
			NE.util.getEspaciador(20)
		]
	});

	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url : '15constanciacredExt.data.jsp',
		params: {
			informacion: "valoresIniciales"			
		},
		callback: procesaValoresIniciales
	});	

	
});