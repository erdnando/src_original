<%@ page contentType="application/json;charset=UTF-8"
	import="
		com.netro.model.catalogos.*,		
		com.netro.cadenas.*,
		com.netro.descuento.*,
		java.sql.*,	
		java.util.*,
		netropology.utilerias.*,	
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"				
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<jsp:useBean id="beanResOper" scope="page" class="com.netro.cadenas.interfasesMonitorResOperBean" />

<% 
String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String ic_producto = (request.getParameter("ic_producto") != null)?request.getParameter("ic_producto"):"T";
String ic_if = (request.getParameter("ic_if") != null)?request.getParameter("ic_if"):"";
String ic_nafin_electronico = (request.getParameter("ic_nafin_electronico") != null)?request.getParameter("ic_nafin_electronico"):"";
String cmb_estatus = (request.getParameter("cmb_estatus") != null)?request.getParameter("cmb_estatus"):"T";
String chckFueraFacultades = (request.getParameter("chckFueraFacultades") != null)?request.getParameter("chckFueraFacultades"):"N";
String claveSolicitud = (request.getParameter("claveSolicitud") != null)?request.getParameter("claveSolicitud"):"";
if(cmb_estatus.equals("T")) { cmb_estatus =""; }
if(ic_producto.equals("T")) { ic_producto =""; }
String pantalla = (request.getParameter("pantalla") != null)?request.getParameter("pantalla"):"";
String tipoProceso = (request.getParameter("tipoProceso") != null)?request.getParameter("tipoProceso"):"";
String logAdicional = (request.getParameter("logAdicional") != null)?request.getParameter("logAdicional"):"";
String monitor = (request.getParameter("monitor") != null)?request.getParameter("monitor"):"";
String tituloTipoProceso = (request.getParameter("tituloTipoProceso") != null)?request.getParameter("tituloTipoProceso"):"";
String directorioPlantilla = strDirectorioPublicacion+"/00archivos/15cadenas/15mantenimiento/interfaces.template.xlsx";
String ic_solicitudes = (request.getParameter("ic_solicitudes") != null)?request.getParameter("ic_solicitudes"):"";
if(!ic_solicitudes.equals("")){
	String valor =  ic_solicitudes.substring(ic_solicitudes.length()-1, ic_solicitudes.length());
	if(valor.equals(",")){
		ic_solicitudes =ic_solicitudes.substring(0,ic_solicitudes.length()-1); 
	}
}
String remitente = (request.getParameter("remitente") != null) ? request.getParameter("remitente") : "";
String destinatarios = (request.getParameter("destinatario") != null) ? request.getParameter("destinatario") : "";
String ccdestinatarios = (request.getParameter("ccdestinatario") != null) ? request.getParameter("ccdestinatario") : "";
String asunto = (request.getParameter("asunto") != null) ? request.getParameter("asunto") : "";
String cuerpoCorreo = (request.getParameter("cuerpoCorreo") != null) ? request.getParameter("cuerpoCorreo") : "";
String urlArchivo = (request.getParameter("urlArchivo") != null) ? request.getParameter("urlArchivo") : "";




String codigo_Aplicacion ="";
if(tipoProceso.equals("Diversos"))  {  codigo_Aplicacion  = "DV"; }
if(tipoProceso.equals("Proyectos")) {  codigo_Aplicacion  = "PY"; }
if(tipoProceso.equals("Contratos")) {  codigo_Aplicacion  = "CN"; }
if(tipoProceso.equals("Disposiciones")) {  codigo_Aplicacion  = "DS"; }
if(tipoProceso.equals("Prestamos")) {  codigo_Aplicacion  = "PR"; }

JSONObject jsonObj = new JSONObject();
String infoRegresar = "", consulta = "";
HashMap datos =  new HashMap();
JSONArray  registros  = new JSONArray();	
List registrosImp  = new ArrayList();

 System.out.println("<<<informacion: =================" + informacion + ">>>>");
 
/*	
System.out.println("<<<informacion: =================" + informacion + ">>>>");
System.out.println("<<<ic_producto: =================" + ic_producto + ">>>>");
System.out.println("<<<codigo_Aplicacion: =================" + codigo_Aplicacion + ">>>>");
System.out.println("<<<cmb_estatus: =================" + cmb_estatus + ">>>>");
System.out.println("<<<ic_if: =================" + ic_if + ">>>>");
System.out.println("<<<chckFueraFacultades: =================" + chckFueraFacultades + ">>>>");
System.out.println("<<<ic_nafin_electronico: =================" + ic_nafin_electronico + ">>>>");
System.out.println("<<<tipoProceso: =================" + tipoProceso + ">>>>"); 
 System.out.println("ic_solicitudes======>   "+ic_solicitudes);  
 */
 if(tipoProceso.equals("Préstamos") || tipoProceso.equals("Prestamos")  || tipoProceso.equals("Pr?stamos")  )    {  
 tipoProceso= "Prestamos";  
 tituloTipoProceso = "Préstamos";
 }else {  
	tituloTipoProceso = tipoProceso;
 }
  
//declaracion de Clases 
interfasesRechazadosBean paginador2 = new interfasesRechazadosBean ();
paginador2.setCodigoaplicacion(codigo_Aplicacion);
paginador2.setIcproducto(ic_producto);
paginador2.setCmbestatus(cmb_estatus);
paginador2.setIcif(ic_if);
paginador2.setChckFueraFacultades(chckFueraFacultades); 
paginador2.setTipoProceso(tipoProceso);
paginador2.setDirectorioPlantilla(directorioPlantilla); 
paginador2.setIc_solicitudes(ic_solicitudes); // para generar el archivo xls 	 

paginador2.setIcnafinelectronico(ic_nafin_electronico); // en este parametro se invoca el quey(no se poeque esta asi)
 
		
ConsMonitor paginador = new ConsMonitor ();


String stNombreProducto ="", modalidad="", producto  ="", nombreIf  ="", nombrePyme  ="", 
			numElecPyme ="", numCliSirac  ="", horaProceso ="", solicitud ="", 	motivo ="", 
			tablaOrigen  ="", agencia ="", subAplicacion ="", numLineaFondeo  ="", aprobadoPor ="",
			monto_descontar ="", numErrorProceso ="", igNumeroLineaCredito ="", igNumeroContrato ="", igNumeroPrestamo =  "";  
			
			
if(informacion.equals("valoresIniciales")) {
	
	jsonObj.put("success",  new Boolean(true));
	jsonObj.put("tipoProceso", tipoProceso);
	jsonObj.put("ic_producto", ic_producto);	
	jsonObj.put("monitor", monitor);	
	jsonObj.put("tituloTipoProceso", tituloTipoProceso);	
	
	infoRegresar = jsonObj.toString();


}else  if(informacion.equals("catalogoProducto")) {

	List catIF = paginador.getCatalogoProduto(pantalla);
	jsonObj.put("registros", catIF);
	jsonObj.put("success",  new Boolean(true));	
	jsonObj.put("ic_producto", ic_producto);	
   infoRegresar = jsonObj.toString();

}else if(informacion.equals("catalogoIF")) {
	
	String sprodGen = (ic_producto.equals("") || ic_producto == null)? "": ic_producto.substring(0,1);
	String ids = beanResOper.getIntermediarios(sprodGen);
	if(ids.equals("")) { 	ids = "0"; 	}

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setTabla("comcat_if");
	catalogo.setOrden("ic_if");
	catalogo.setValoresCondicionIn(ids, Integer.class);
	infoRegresar = catalogo.getJSONElementos();
	
}else if(informacion.equals("catalogoRechazo")) {

	CatCausaRechazo catalogo = new CatCausaRechazo(); 	
	
	if(tipoProceso.equals("Diversos"))  {
		
		catalogo.setIcproducto(ic_producto);
		catalogo.setDiversos("SI");	
		catalogo.setCmbestatus(cmb_estatus);	
		
	}else  if(tipoProceso.equals("Proyectos"))  {
		
		catalogo.setIcproducto(ic_producto);
		catalogo.setIgprocesonumero("1");
		catalogo.setCmbestatus(cmb_estatus);
		catalogo.setCgestatus("cg_estatus = 'E'");
		catalogo.setComcontrol("1");
		catalogo.setCccodigoaplicacion("PY");
		catalogo.setIn("6,8,9");
		
	}else  if(tipoProceso.equals("Contratos"))  {
	
		catalogo.setIcproducto(ic_producto);
		catalogo.setIgprocesonumero("2");
		catalogo.setCmbestatus(cmb_estatus);
		catalogo.setCgestatus("cs_estatus = 'N'");
		catalogo.setComcontrol("2");
		catalogo.setCccodigoaplicacion("CN");
		catalogo.setIn("6,8,9");

	}else  if(tipoProceso.equals("Disposiciones"))  {
	
		catalogo.setIcproducto(ic_producto);
		catalogo.setIgprocesonumero("4");
		catalogo.setCmbestatus(cmb_estatus);
		catalogo.setCgestatus("cs_estatus = 'N'");
		catalogo.setComcontrol("4");
		catalogo.setCccodigoaplicacion("DS");
		catalogo.setIn("6,8,9");
	}else  if(tipoProceso.equals("Prestamos") ||  tipoProceso.equals("Préstamos")  )  {
	
		catalogo.setIcproducto(ic_producto);
		catalogo.setIgprocesonumero("3");
		catalogo.setCmbestatus(cmb_estatus);
		catalogo.setCgestatus("cs_estatus = 'N'");
		catalogo.setComcontrol("3");
		catalogo.setCccodigoaplicacion("PR");
		catalogo.setIn("6,8,9");
		
	}
	
	infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("catalogoDescripcion")) {
	
	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("cd_descripcion");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_descripcion_esp");
	catalogo.setOrden("ic_descripcion_esp");
	infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("ConsultaMonitor")) {
	
	paginador.setIc_producto(ic_producto);
	paginador.setPantalla("ConsultaMonitor");
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	
	Hashtable htMonitor = new Hashtable();
	datos =  new HashMap();
	registros  = new JSONArray();
	
	try {
		Registros reg	=	queryHelper.doSearch();
		
		while (reg.next())
			htMonitor.put(reg.getString("CONTADOR"), reg.getString("VALOR"));
				
			String rproc = (htMonitor.get("RPROCE")==null)?"0":htMonitor.get("RPROCE").toString();
			String rpd   = (htMonitor.get("RPR-DI")==null)?"0":htMonitor.get("RPR-DI").toString();
			String rrd   = (htMonitor.get("RRE-DI")==null)?"0":htMonitor.get("RRE-DI").toString();
			String rcd   = (htMonitor.get("RCO-DI")==null)?"0":htMonitor.get("RCO-DI").toString();
			String rpp   = (htMonitor.get("RPR-PY")==null)?"0":htMonitor.get("RPR-PY").toString();
			String rrp   = (htMonitor.get("RRE-PY")==null)?"0":htMonitor.get("RRE-PY").toString();
			String rcp   = (htMonitor.get("RCO-PY")==null)?"0":htMonitor.get("RCO-PY").toString();
			String rpc   = (htMonitor.get("RPR-CN")==null)?"0":htMonitor.get("RPR-CN").toString();
			String rrc   = (htMonitor.get("RRE-CN")==null)?"0":htMonitor.get("RRE-CN").toString();
			String rcc   = (htMonitor.get("RCO-CN")==null)?"0":htMonitor.get("RCO-CN").toString();
			String rppr  = (htMonitor.get("RPR-PR")==null)?"0":htMonitor.get("RPR-PR").toString();
			String rrpr  = (htMonitor.get("RRE-PR")==null)?"0":htMonitor.get("RRE-PR").toString();
			String rcpr  = (htMonitor.get("RCO-PR")==null)?"0":htMonitor.get("RCO-PR").toString();
			String rpds  = (htMonitor.get("RPR-DS")==null)?"0":htMonitor.get("RPR-DS").toString();
			String rrds  = (htMonitor.get("RRE-DS")==null)?"0":htMonitor.get("RRE-DS").toString();
			String rcds  = (htMonitor.get("RCO-DS")==null)?"0":htMonitor.get("RCO-DS").toString();
		
			datos =  new HashMap();
			datos.put("PROCESOS", "Diversos");
			datos.put("REGISTROS_PROCESADOS", rpd);
			datos.put("REGISTROS_RECHAZADOS", rrd);
			datos.put("REGISTROS_CONCLUIDOS", rcd);
			registros.add(datos);	
			
			datos =  new HashMap();
			datos.put("PROCESOS", "Proyectos");
			datos.put("REGISTROS_PROCESADOS", rpp);
			datos.put("REGISTROS_RECHAZADOS", rrp);
			datos.put("REGISTROS_CONCLUIDOS", rcp);
			registros.add(datos);	
			
			datos =  new HashMap();
			datos.put("PROCESOS", "Contratos");
			datos.put("REGISTROS_PROCESADOS", rpc);
			datos.put("REGISTROS_RECHAZADOS", rrc);
			datos.put("REGISTROS_CONCLUIDOS", rcc);
			registros.add(datos);
			
			datos =  new HashMap();
			datos.put("PROCESOS", "Préstamos");
			datos.put("REGISTROS_PROCESADOS", rppr);
			datos.put("REGISTROS_RECHAZADOS", rrpr);
			datos.put("REGISTROS_CONCLUIDOS", rcpr);
			registros.add(datos);
			
			datos =  new HashMap();
			datos.put("PROCESOS", "Disposiciones");
			datos.put("REGISTROS_PROCESADOS", rpds);
			datos.put("REGISTROS_RECHAZADOS", rrds);
			datos.put("REGISTROS_CONCLUIDOS", rcds);
			registros.add(datos);
			
		jsonObj = new JSONObject();
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("rproc", rproc);
		jsonObj.put("tituloTipoProceso", tituloTipoProceso);
			
	}	catch(Exception e) {
		throw new AppException("Error en la consulta", e);
	}
	
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("ConsultaPausa")) {
	
        List estatusPausa = paginador.getPausaInterfaz(ic_producto);
        Iterator estatusPausaIterator = estatusPausa.iterator();
        while (estatusPausaIterator.hasNext()) {
            jsonObj.put("clave",estatusPausaIterator.next());
            jsonObj.put("descripcion",estatusPausaIterator.next());
        }
	jsonObj.put("success",  new Boolean(true));	
	jsonObj.put("ic_producto", ic_producto);
        infoRegresar = jsonObj.toString();
}else if(informacion.equals("PausaInterfaz")) {
	
        List estatusPausa = paginador.setPausaInterfaz(ic_producto);
        Iterator estatusPausaIterator = estatusPausa.iterator();
        while (estatusPausaIterator.hasNext()) {
            jsonObj.put("clave",estatusPausaIterator.next());
            jsonObj.put("descripcion",estatusPausaIterator.next());
        }
	jsonObj.put("success",  new Boolean(true));	
	jsonObj.put("ic_producto", ic_producto);
        infoRegresar = jsonObj.toString();
}else if(informacion.equals("ResetMonitor")) {
	
        paginador.borraMonitor(ic_producto);
	jsonObj.put("success",  new Boolean(true));	
	jsonObj.put("ic_producto", ic_producto);
        infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("ConsultaDetalle") ||   informacion.equals("ArchivoDetalle")  ) {
	
	AccesoDB con = new AccesoDB();	
	int numRegistros =0, errorCausa43 =0;
	
	datos =  new HashMap();
	registros  = new JSONArray();
	
			
	
	try	{
		con.conexionDB();
				
		String qrySentencia=paginador2.getQrysentencia();
		
		ResultSet rs = con.queryDB(qrySentencia);
		while (rs.next()) {
			numRegistros++;						
			producto 			= rs.getString("PRODUCTO")==null?"":rs.getString("PRODUCTO");
			nombreIf 			= rs.getString("NOMBREIF")==null?"":rs.getString("NOMBREIF");
			nombrePyme 			= rs.getString("NOMBREPYME")==null?"":rs.getString("NOMBREPYME");
			numElecPyme 		= rs.getString("IC_NAFIN_ELECTRONICO")==null?"":rs.getString("IC_NAFIN_ELECTRONICO");
			numCliSirac 		= rs.getString("numCliSirac")==null?"":rs.getString("numCliSirac");
			horaProceso 		= rs.getString("DF_HORA_PROCESO")==null?"":rs.getString("DF_HORA_PROCESO");
			solicitud 			= rs.getString("IC_FOLIO")==null?"":rs.getString("IC_FOLIO");
			motivo 				= (rs.getString("CG_DESCRIPCION")==null)?"":rs.getString("CG_DESCRIPCION");
			agencia 			= (rs.getString("IG_CODIGO_AGENCIA")==null)?"":rs.getString("IG_CODIGO_AGENCIA");
			subAplicacion 		= (rs.getString("IG_CODIGO_SUB_APLICACION")==null)?"":rs.getString("IG_CODIGO_SUB_APLICACION");
			numLineaFondeo 		= (rs.getString("IG_NUMERO_LINEA_FONDEO")==null)?"":rs.getString("IG_NUMERO_LINEA_FONDEO");
			aprobadoPor 		= (rs.getString("IG_APROBADO_POR")==null)?"":rs.getString("IG_APROBADO_POR");
			monto_descontar 	= (rs.getString("monto_descontar")==null)?"":rs.getString("monto_descontar");
			
			if(tipoProceso.equals("Diversos"))  { 
				tablaOrigen 		= rs.getString("TABLAORIGEN")==null?"":rs.getString("TABLAORIGEN");
			}
			if(tipoProceso.equals("Proyectos")) {  
				numErrorProceso = (rs.getString("IC_ERROR_PROCESO")==null)?"":rs.getString("IC_ERROR_PROCESO");
				if (numErrorProceso.equals("43")){
						errorCausa43++;
				} 
			}
			if(tipoProceso.equals("Contratos")  || tipoProceso.equals("Disposiciones") ) {
				igNumeroLineaCredito = rs.getString("IG_NUMERO_LINEA_CREDITO")==null?"":rs.getString("IG_NUMERO_LINEA_CREDITO");
			}
			if(tipoProceso.equals("Disposiciones")) {
				igNumeroContrato = rs.getString("IG_NUMERO_CONTRATO")==null?"":rs.getString("IG_NUMERO_CONTRATO");	
				igNumeroPrestamo = rs.getString("IG_NUMERO_PRESTAMO")==null?"":rs.getString("IG_NUMERO_PRESTAMO");		
			}
			if(tipoProceso.equals("Prestamos")  ||  tipoProceso.equals("Préstamos")  ) {			
				igNumeroLineaCredito = rs.getString("IG_NUMERO_LINEA_CREDITO")==null?"":rs.getString("IG_NUMERO_LINEA_CREDITO");
				igNumeroContrato = rs.getString("IG_NUMERO_CONTRATO")==null?"":rs.getString("IG_NUMERO_CONTRATO");
			}
			
			try {
					modalidad 		= (rs.getString("modalidad")==null)?"":rs.getString("modalidad");
				} catch(SQLException sql) {
					modalidad 		= "";
				}switch (Integer.parseInt(producto)) {
				case 0:
					stNombreProducto="Credito Electronico";
				   break;
				case 1:
					stNombreProducto="Descuento Electronico";
				   break;
				case 2:
					stNombreProducto = " Anticipo Electronico";
				   break;
				case 4:
					stNombreProducto="Financiamiento a Distribuidores";
				   break;
				case 5:
					stNombreProducto="Credicadenas";
				   break;
				default:
					stNombreProducto="Producto Desconocido";
				}
			if(!"".equals(modalidad)) {
				stNombreProducto = "Nafin Empresarial";
			}
				
			datos =  new HashMap();
			datos.put("PRODUCTO", stNombreProducto);
			datos.put("MODALIDAD", modalidad);
			datos.put("INTERMEDIARIO", nombreIf);
			datos.put("NOMBRE_PYME", nombrePyme);
			datos.put("NUM_PYME", numElecPyme);
			datos.put("MONTO_DESC", monto_descontar);
			datos.put("NUM_PYME_SIRAC", numCliSirac);
			datos.put("HORA", horaProceso);			
			datos.put("SOLICITUD", solicitud);
			datos.put("MOTIVO", motivo);
			datos.put("AGENCIA", agencia);
			datos.put("SUBAPLICACION", subAplicacion);
			datos.put("LINEA_FONDEO", numLineaFondeo);
			datos.put("APROBADOR", aprobadoPor);
			datos.put("SELECCION", "");
			datos.put("TABLA_ORIGEN", tablaOrigen);
			datos.put("IC_PRODUCTO", producto);
			datos.put("FACULTAD", chckFueraFacultades);	
			datos.put("IC_ERROR_PROCESO", numErrorProceso);	
			datos.put("NUM_PROYECTO", igNumeroLineaCredito);	
			datos.put("NUM_CONTRATO", igNumeroContrato);
			datos.put("NUM_PRESTAMO", igNumeroPrestamo);
			
			
			registros.add(datos);
			registrosImp.add(datos);
		}
		rs.close();
		
	}catch(Exception e) { out.println("Error: "+e); }
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); 
	}
	
	if(informacion.equals("ConsultaDetalle")) {
		jsonObj = new JSONObject();
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("chckFueraFacultades", chckFueraFacultades);
		jsonObj.put("ic_producto", ic_producto);	
		jsonObj.put("tipoProceso", tipoProceso);
		jsonObj.put("errorCausa43", String.valueOf(errorCausa43));	
		jsonObj.put("numRegistros", String.valueOf(numRegistros));
		jsonObj.put("tituloTipoProceso", tituloTipoProceso); 
		infoRegresar = jsonObj.toString();	
		
		System.out.println(informacion +"            <<<infoRegresar: =================" + infoRegresar + ">>>>");

	}else  if(informacion.equals("ArchivoDetalle")) {
		try {
				String nombreArchivo = paginador2.impDescargaArchivo(request, registrosImp, strDirectorioTemp, "PDF");				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}		
		infoRegresar = jsonObj.toString();
	}

}else  if ( informacion.equals("ArchivoVerOrigen") ) {
 
	AutorizacionSolicitud autorizacionSolicitud = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB",AutorizacionSolicitud.class);
	CreaArchivo creaArchivo = new CreaArchivo();

	String sContenidoArchivo = autorizacionSolicitud.getDatosSolicitudCredito(claveSolicitud);

	creaArchivo.make(sContenidoArchivo.toString(), strDirectorioTemp, ".csv");
	String nombreArchivo = creaArchivo.getNombre();
			
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();
	

}else  if ( informacion.equals("ArchivoLog") || informacion.equals("ArchivoNoProyecto")  
		|| informacion.equals("ArchivoNoContrato") || informacion.equals("ArchivoNoPrestamo")    ) {
 
	String 	nombreArchivo= paginador2.generarArchivoLog(ic_producto, claveSolicitud , tipoProceso, logAdicional,  strDirectorioTemp  );
				
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();
	
	
}else  if ( informacion.equals("procesar") )  {

	String mensaje ="";
	String noRegistros = (request.getParameter("noRegistros") != null)?request.getParameter("noRegistros"):"";
	String descripcion = (request.getParameter("descripcion") != null)?request.getParameter("descripcion"):"";
	String operacion = (request.getParameter("operacion") != null)?request.getParameter("operacion"):"";
		
	String noSolicitud[] = request.getParameterValues("noSolicitud");
	String producto_[] = request.getParameterValues("producto");
	String tabla_Origen[] = request.getParameterValues("tabla_Origen");
	String error_proceso[] = request.getParameterValues("error_proceso");
		
			
	if(operacion.equals("REINICIAR INTERFAZ")  && 
		(tipoProceso.equals("Diversos")   ||   tipoProceso.equals("Proyectos")   ||  tipoProceso.equals("Contratos") ||   tipoProceso.equals("Disposiciones")  || tipoProceso.equals("Prestamos") ||  tipoProceso.equals("Préstamos")  )  
		)  {
			try {
				mensaje=  paginador2.procRegREINICIAR_INTERFAZ(Integer.parseInt(noRegistros),   noSolicitud ,  producto_, error_proceso, operacion, tipoProceso );
			} catch(Exception e) {
				e.printStackTrace();
			}	
		}else  if(tipoProceso.equals("Diversos")  && !operacion.equals("REINICIAR INTERFAZ")  )  {			
		try {
		mensaje= paginador2.procRegDiversos(Integer.parseInt(noRegistros), noSolicitud , producto_ ,tabla_Origen , descripcion, operacion  );
		} catch(Exception e) {
			e.printStackTrace();
		}	
	
	}else if(tipoProceso.equals("Proyectos")  && !operacion.equals("REINICIAR INTERFAZ"))  {
		try {
			mensaje = paginador2.procRegProyectos(Integer.parseInt(noRegistros),   noSolicitud ,  producto_, error_proceso, operacion);
		} catch(Exception e) {
			 e.printStackTrace();			
		}		
	}else  if(tipoProceso.equals("Contratos")  && !operacion.equals("REINICIAR INTERFAZ") ) {
	
		try {
				mensaje = paginador2.procRegContratos(Integer.parseInt(noRegistros),   noSolicitud ,  producto_, error_proceso, operacion);
		} catch(Exception e) {
			e.printStackTrace();			
		}	
	}else  if(tipoProceso.equals("Disposiciones") && !operacion.equals("REINICIAR INTERFAZ") ) {
	
		try {
			mensaje = paginador2.procRegDisposiciones(Integer.parseInt(noRegistros),   noSolicitud ,  producto_, error_proceso, operacion);
			
		} catch(Exception e) {
			e.printStackTrace();			
		}		
	}else  if(tipoProceso.equals("Prestamos") ||  tipoProceso.equals("Préstamos")  && !operacion.equals("REINICIAR INTERFAZ") ) { 
	
		try {
			mensaje = paginador2.procRegPrestamos(Integer.parseInt(noRegistros),   noSolicitud ,  producto_, error_proceso, operacion);
				
		} catch(Exception e) {
			e.printStackTrace();			
		}		
	}
			
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", mensaje);
	jsonObj.put("tipoProceso", tipoProceso);	
	jsonObj.put("tituloTipoProceso", tituloTipoProceso);
	infoRegresar = jsonObj.toString();
	
}else  if ( informacion.equals("DatosCorreo") ) {
	
	UtilUsr usuario = new UtilUsr(); 
	
	Usuario user = new Usuario(); 
	user = usuario.getUsuario(strLogin);
	remitente = user.getEmail();
	
	HashMap alParametros = paginador2.getDatosCorreo("6");		 
	 
	 if(alParametros.size()>0) {
		 destinatarios  = alParametros.get("CG_DESTINATARIO").toString();
		 ccdestinatarios  = alParametros.get("CG_CCDESTINATARIO").toString();
		 asunto  = alParametros.get("CG_ASUNTO").toString();
		 cuerpoCorreo  = alParametros.get("CG_CUERPO_CORREO").toString();
	}
	AccesoDB con = new AccesoDB();	
	int numRegistros =0, errorCausa43 =0;
	
	datos =  new HashMap();
	registros  = new JSONArray();   
	
	String  nombreArchivo ="";    
			
	
	try	{
		con.conexionDB(); 
	
			
		String qrySentencia=paginador2.getQrysentencia(); 
		
		ResultSet rs = con.queryDB(qrySentencia);
		while (rs.next()) {
			numRegistros++;						
			producto 			= rs.getString("PRODUCTO")==null?"":rs.getString("PRODUCTO");
			nombreIf 			= rs.getString("NOMBREIF")==null?"":rs.getString("NOMBREIF");
			nombrePyme 			= rs.getString("NOMBREPYME")==null?"":rs.getString("NOMBREPYME");
			numElecPyme 		= rs.getString("IC_NAFIN_ELECTRONICO")==null?"":rs.getString("IC_NAFIN_ELECTRONICO");
			numCliSirac 		= rs.getString("numCliSirac")==null?"":rs.getString("numCliSirac");
			horaProceso 		= rs.getString("DF_HORA_PROCESO")==null?"":rs.getString("DF_HORA_PROCESO");
			solicitud 			= rs.getString("IC_FOLIO")==null?"":rs.getString("IC_FOLIO");
			motivo 				= (rs.getString("CG_DESCRIPCION")==null)?"":rs.getString("CG_DESCRIPCION");
			agencia 			= (rs.getString("IG_CODIGO_AGENCIA")==null)?"":rs.getString("IG_CODIGO_AGENCIA");
			subAplicacion 		= (rs.getString("IG_CODIGO_SUB_APLICACION")==null)?"":rs.getString("IG_CODIGO_SUB_APLICACION");
			numLineaFondeo 		= (rs.getString("IG_NUMERO_LINEA_FONDEO")==null)?"":rs.getString("IG_NUMERO_LINEA_FONDEO");
			aprobadoPor 		= (rs.getString("IG_APROBADO_POR")==null)?"":rs.getString("IG_APROBADO_POR");
			monto_descontar 	= (rs.getString("monto_descontar")==null)?"":rs.getString("monto_descontar");
			
			if(tipoProceso.equals("Diversos"))  { 
				tablaOrigen 		= rs.getString("TABLAORIGEN")==null?"":rs.getString("TABLAORIGEN");
			}
			if(tipoProceso.equals("Proyectos")) {  
				numErrorProceso = (rs.getString("IC_ERROR_PROCESO")==null)?"":rs.getString("IC_ERROR_PROCESO");
				if (numErrorProceso.equals("43")){
						errorCausa43++;
				} 
			}
			if(tipoProceso.equals("Contratos")  || tipoProceso.equals("Disposiciones") ) {
				igNumeroLineaCredito = rs.getString("IG_NUMERO_LINEA_CREDITO")==null?"":rs.getString("IG_NUMERO_LINEA_CREDITO");
			}
			if(tipoProceso.equals("Disposiciones")) {
				igNumeroContrato = rs.getString("IG_NUMERO_CONTRATO")==null?"":rs.getString("IG_NUMERO_CONTRATO");	
				igNumeroPrestamo = rs.getString("IG_NUMERO_PRESTAMO")==null?"":rs.getString("IG_NUMERO_PRESTAMO");		
			}
			if(tipoProceso.equals("Prestamos") ||  tipoProceso.equals("Préstamos") ) {			
				igNumeroLineaCredito = rs.getString("IG_NUMERO_LINEA_CREDITO")==null?"":rs.getString("IG_NUMERO_LINEA_CREDITO");
				igNumeroContrato = rs.getString("IG_NUMERO_CONTRATO")==null?"":rs.getString("IG_NUMERO_CONTRATO");
			}
			
			try {
					modalidad 		= (rs.getString("modalidad")==null)?"":rs.getString("modalidad");
				} catch(SQLException sql) {
					modalidad 		= "";
				}switch (Integer.parseInt(producto)) {
				case 0:
					stNombreProducto="Credito Electronico";
				   break;
				case 1:
					stNombreProducto="Descuento Electronico";
				   break;
				case 2:
					stNombreProducto = " Anticipo Electronico";
				   break;
				case 4:
					stNombreProducto="Financiamiento a Distribuidores";
				   break;
				case 5:
					stNombreProducto="Credicadenas";
				   break;
				default:
					stNombreProducto="Producto Desconocido";
				}
			if(!"".equals(modalidad)) {
				stNombreProducto = "Nafin Empresarial";
			}
				
			datos =  new HashMap();
			datos.put("PRODUCTO", stNombreProducto);
			datos.put("MODALIDAD", modalidad);
			datos.put("INTERMEDIARIO", nombreIf);
			datos.put("NOMBRE_PYME", nombrePyme);
			datos.put("NUM_PYME", numElecPyme);
			datos.put("MONTO_DESC", monto_descontar);
			datos.put("NUM_PYME_SIRAC", numCliSirac);
			datos.put("HORA", horaProceso);			
			datos.put("SOLICITUD", solicitud);
			datos.put("MOTIVO", motivo);
			datos.put("AGENCIA", agencia);
			datos.put("SUBAPLICACION", subAplicacion);
			datos.put("LINEA_FONDEO", numLineaFondeo);
			datos.put("APROBADOR", aprobadoPor);
			datos.put("SELECCION", "");
			datos.put("TABLA_ORIGEN", tablaOrigen);
			datos.put("IC_PRODUCTO", producto);
			datos.put("FACULTAD", chckFueraFacultades);	
			datos.put("IC_ERROR_PROCESO", numErrorProceso);	
			datos.put("NUM_PROYECTO", igNumeroLineaCredito);	
			datos.put("NUM_CONTRATO", igNumeroContrato);
			datos.put("NUM_PRESTAMO", igNumeroPrestamo);
			
			
			registros.add(datos);
			registrosImp.add(datos);
		}
		rs.close();
		
	}catch(Exception e) { out.println("Error: "+e); }
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); 
	}
	
	try {
		nombreArchivo = paginador2.impDescargaArchivo(request, registrosImp, strDirectorioTemp, "XLSX");				
								
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo PDF", e);
	}		
	JSONArray  vistaArchivosAdjuntos = new JSONArray();
	HashMap info = new HashMap();
	info.put("id", "12342");
	info.put("tituloArchivo", nombreArchivo);	
	vistaArchivosAdjuntos.add(info);
	
		
 	jsonObj.put("success", new Boolean(true));
	jsonObj.put("remitente", remitente);	
	jsonObj.put("destinatarios", destinatarios);	
	jsonObj.put("ccDestinatarios", ccdestinatarios);	
	jsonObj.put("asunto", asunto);	
	jsonObj.put("cuerpoCorreo", cuerpoCorreo);
	jsonObj.put("urlArchivo", strDirectorioTemp+nombreArchivo);	  	 
	jsonObj.put("vistaArchivosAdjuntos", vistaArchivosAdjuntos); 
	jsonObj.put("tituloTipoProceso", tituloTipoProceso);
	infoRegresar = jsonObj.toString();
	
	
}else  if(informacion.equals("CreaArchivoAdjunto") )  {

	String tituloArchivo = (request.getParameter("tituloArchivo") != null) ? request.getParameter("tituloArchivo") : "";
	
	HashMap datosArchivo = new HashMap();
	datosArchivo.put("tituloArchivo",	tituloArchivo);
	datosArchivo.put("directorio",	strDirecVirtualTemp);
	datosArchivo.put("nombreArchivo",	tituloArchivo);	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("vistaArchivoAdjunto", strDirecVirtualTemp+tituloArchivo);	
	jsonObj.put("datosArchivo", datosArchivo);	  
	infoRegresar = jsonObj.toString(); 
 
}else  if(informacion.equals("EnviodeCorreo") )  {

	List parametros = new ArrayList();	
	String respuesta ="Fallo";
	
	parametros.add(remitente);
	parametros.add(destinatarios);
	parametros.add(ccdestinatarios);
	parametros.add(asunto);
	parametros.add(cuerpoCorreo);
	parametros.add(urlArchivo);
	
	
	try {
		//envia el correo
		respuesta =  paginador2.procesoEnviodeCorreo (parametros)	;		
		
	} catch(Throwable e) {
		throw new AppException("Error enviar correo", e);
	} 
	
	String mensaje = "<center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+respuesta+"</center>"; 
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", mensaje);
	infoRegresar = jsonObj.toString(); 
	
}

%>

<%=infoRegresar%>


<%

%>
