	
Ext.onReady(function(){
	var varGlobPY = function  (){
		LIST_IC_PARAMETRO_PY = [];
		LIST_NOMBRE_PARAM_PY = [];
		LIST_CC_API_PY = [];
		LIST_IC_PRODUCTO_PY= [];
		LIST_IG_PARAM_CAMPO_PY= [];
	}
	var varGlobCN = function  (){
		LIST_IC_PARAMETRO_CN = [];
		LIST_NOMBRE_PARAM_CN = [];
		LIST_CC_API_CN = [];
		LIST_IC_PRODUCTO_CN= [];
		LIST_IG_PARAM_CAMPO_CN= [];
	}
	var varGlobPR = function  (){
		LIST_IC_PARAMETRO_PR = [];
		LIST_NOMBRE_PARAM_PR = [];
		LIST_CC_API_PR = [];
		LIST_IC_PRODUCTO_PR= [];
		LIST_IG_PARAM_CAMPO_PR= [];
	}
	var varGlobDS = function  (){
		LIST_IC_PARAMETRO_DS = [];
		LIST_NOMBRE_PARAM_DS = [];
		LIST_CC_API_DS = [];
		LIST_IC_PRODUCTO_DS= [];
		LIST_IG_PARAM_CAMPO_DS= [];
	}
	var procesarGuardaProyecto = function(opts, success, response) {
	
		
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('icoGuardar');
			Ext.ComponentQuery.query('#btnGuardar')[0].setDisabled(false);
			
		} else {
			NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
	}
	var procesarDefault = function(opts, success, response) {
		Ext.ComponentQuery.query('#btnParamdef')[0].setIconCls('icoAceptar');
		Ext.ComponentQuery.query('#btnParamdef')[0].setDisabled(false);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var listValores = infoR.datosValores;	
			var combo = Ext.ComponentQuery.query('#ic_interfaz')[0];
			var gridPY = Ext.ComponentQuery.query('#gridProyecto')[0];
			var gridCN = Ext.ComponentQuery.query('#gridContrato')[0];
			var gridPR = Ext.ComponentQuery.query('#gridPrestamo')[0];
			var gridDS = Ext.ComponentQuery.query('#gridDispersion')[0];
			for(var i = 0; i < listValores.length; i++){
					if(listValores[i].CC_API=='PY'){
							var storePY = gridPY.getStore();
							storePY.each(function(r){
								if(r.get('IC_PARAMETRO')==listValores[i].IC_PARAMETRO){
									r.set('IG_PARAM_CAMPO',listValores[i].IG_PARAM_CAMPO);
								}
							});
							storePY.commitChanges();
						
					}else if(listValores[i].CC_API=='CN'){
						var storeCN = gridCN.getStore();
						storeCN.each(function(r){
							if(r.get('IC_PARAMETRO')==listValores[i].IC_PARAMETRO){
									r.set('IG_PARAM_CAMPO',listValores[i].IG_PARAM_CAMPO);
								}
						});
						storeCN.commitChanges();
					
					}else if(listValores[i].CC_API=='PR'){
						var storePR = gridPR.getStore();
						storePR.each(function(r){
							if(r.get('IC_PARAMETRO')==listValores[i].IC_PARAMETRO){
								r.set('IG_PARAM_CAMPO',listValores[i].IG_PARAM_CAMPO);
							}
						});
						storePR.commitChanges();
					
					}else if(listValores[i].CC_API=='DS'){
						var storeDS = gridDS.getStore();
						storeDS.each(function(r){
							if(r.get('IC_PARAMETRO')==listValores[i].IC_PARAMETRO){
								r.set('IG_PARAM_CAMPO',listValores[i].IG_PARAM_CAMPO);
							}
						});
						storeDS.commitChanges();
					}
				
				
			}
			
		} else {
			NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
	}
	function 	obtenerValoresEPO(boton){
		
					var combo = Ext.ComponentQuery.query('#ic_interfaz')[0];
					var combo_epo = Ext.ComponentQuery.query('#ic_epo_param')[0];
					boton.setDisabled(true);
					boton.setIconCls('x-mask-msg-text');
					if(combo.getValue()=='PY'){
							consultaProyecto.load({
								params: Ext.apply({
									api:combo.getValue(),
									ic_epo:combo_epo.getValue(),
									rep_val:'S'
								})				 
							});	
						}else if(combo.getValue()=='CN'){
							consultaContrato.load({
								params: Ext.apply({
									api:combo.getValue(),
									ic_epo:combo_epo.getValue(),
									rep_val:'S'
								})				 
							});	
						}else if(combo.getValue()=='PR'){
							consultaPrestamo.load({
								params: Ext.apply({
									api:combo.getValue(),
									ic_epo:combo_epo.getValue(),
									rep_val:'S'
								})				 
							});	
						}else if(combo.getValue()=='DS'){
							consultaDisposicion.load({
								params: Ext.apply({
									api:combo.getValue(),
									ic_epo:combo_epo.getValue(),
									rep_val:'S'
								})				 
							});	
						}else if(combo.getValue()=='T'||Ext.isEmpty(combo.getValue())){
							consultaProyecto.load({
								params: Ext.apply({
									api:'PY',
									api_fin:'T',
									ic_epo:combo_epo.getValue(),
									rep_val:'S'
								})				 
							});
							consultaContrato.load({
								params: Ext.apply({
									api:'CN',
									api_fin:'T',
									ic_epo:combo_epo.getValue(),
									rep_val:'S'
								})				 
							});
							consultaPrestamo.load({
								params: Ext.apply({
									api:'PR',
									api_fin:'T',
									ic_epo:combo_epo.getValue(),
									rep_val:'S'
								})				 
							});
							consultaDisposicion.load({
								params: Ext.apply({
									api:'DS',
									api_fin:'T',
									ic_epo:combo_epo.getValue(),
									rep_val:'S'
								})				 
							});	
						}
				
		
		
	}

	function asignaValoresDefault(boton){
		var combo = Ext.ComponentQuery.query('#ic_interfaz')[0];
		boton.setDisabled(true);
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15ParamInterfaseDesc.data.jsp',
			params:Ext.apply(formaPrincipal.getForm().getValues(),{
				informacion: 'PARAMETROS_DEFAULT',
				api:combo.getValue()
			}),
			callback: procesarDefault
		});
	}
	var procesaGeneraArchivoPDF = function(opts, success, response) {
		Ext.ComponentQuery.query('#btnPDF')[0].enable();
		Ext.ComponentQuery.query('#btnPDF')[0].setIconCls('icoPdf');
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.JSON.decode(response.responseText);
			
			var archivo = jsonData.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();	
			
			
		} else {
			
			NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
	}
	var procesaGeneraArchivoXLS = function(opts, success, response) {
		Ext.ComponentQuery.query('#btnGA')[0].enable();
		Ext.ComponentQuery.query('#btnGA')[0].setIconCls('icoXls');
		
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.JSON.decode(response.responseText);
			var archivo = jsonData.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();	
			
		} else {
				NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
	}
	function descargaPDF(boton){
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15ParamInterfaseDesc.data.jsp',
			params: Ext.apply({
				informacion:  'descargarArchivo',
				tipo:         'PDF',
				api:Ext.ComponentQuery.query('#ic_interfaz')[0].getValue(),
				ic_epo:Ext.ComponentQuery.query('#ic_epo')[0].getValue(),
				epo:Ext.ComponentQuery.query('#ic_epo')[0].getRawValue(),
				moneda:Ext.ComponentQuery.query('#cmbMoneda')[0].getRawValue()
			}),
			callback: procesaGeneraArchivoPDF
		});
	}
	function descargaXLS(boton){
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15ParamInterfaseDesc.data.jsp',
			params: Ext.apply({
				informacion:  'descargarArchivo',
				tipo:         'XLS',
				api:Ext.ComponentQuery.query('#ic_interfaz')[0].getValue(),
				ic_epo:Ext.ComponentQuery.query('#ic_epo')[0].getValue( ),
				epo:Ext.ComponentQuery.query('#ic_epo')[0].getRawValue( ),
				moneda:Ext.ComponentQuery.query('#cmbMoneda')[0].getRawValue( )
			}),
			callback: procesaGeneraArchivoXLS
		});
	}
	function validaCampo(boton){
		var gridPY = Ext.ComponentQuery.query('#gridProyecto')[0];
		var gridCN = Ext.ComponentQuery.query('#gridContrato')[0];
		var gridPR = Ext.ComponentQuery.query('#gridPrestamo')[0];
		var gridDS = Ext.ComponentQuery.query('#gridDispersion')[0];
		var combo = Ext.ComponentQuery.query('#ic_interfaz')[0];
		var combo_epo = Ext.ComponentQuery.query('#ic_epo')[0];
		if(Ext.isEmpty(combo_epo.getValue())){
			combo_epo.markInvalid('Campo Obligatorio');
			combo_epo.focus();
			return;
		}
		var ban_g1 = 0;
		var ban_g2 = 0;
		var ban_g3 = 0;
		var ban_g4 = 0;
		if(combo.getValue()=='PY'){
			var storePY = gridPY.getStore();
			var selectedPY = storePY.getRange();
			var numRegistrosPY = 0;
			varGlobPY();
			Ext.each(selectedPY, function(item) {
				 LIST_IC_PARAMETRO_PY.push(item.data.IC_PARAMETRO);
				 LIST_NOMBRE_PARAM_PY.push(item.data.NOMBRE_PARAM);
				 LIST_CC_API_PY.push(item.data.CC_API);
				 LIST_IC_PRODUCTO_PY.push(item.data.IC_PRODUCTO);
				 LIST_IG_PARAM_CAMPO_PY.push(item.data.IG_PARAM_CAMPO);
				 numRegistrosPY++;
			}, this);
			
			Ext.each(selectedPY, function(item) {
				numReg = storePY.indexOf(item);
				if(item.data.IG_PARAM_CAMPO==''&&item.data.IC_PARAMETRO==3){
					ban_g1=1;
					var editor = gridPY.plugins[0];
					var fila = numReg;
					Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>El campo es obligatorio</center></td><td></td></table> </div>',
					function(){
						editor.startEditByPosition({row: fila, column:2});
					});
					valor= false
					return false;
				}
			
			});
			if(ban_g1==0 ){
				Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>La parametrizaci�n se realiz� exitosamente</center></td><td></td></table> </div>',function(){
						boton.setDisabled(true);
						boton.setIconCls('x-mask-msg-text');
						Ext.Ajax.request({
							url: '15ParamInterfaseDesc.data.jsp',
								params:Ext.apply(formaPrincipal.getForm().getValues(),{
								informacion: 'GUARDAR_CAMBIOS',
								IC_EPO:combo_epo.getValue(),
								LIST_IC_PARAMETRO_PY:LIST_IC_PARAMETRO_PY,
								LIST_NOMBRE_PARAM_PY:LIST_NOMBRE_PARAM_PY,
								LIST_CC_API_PY : LIST_CC_API_PY,
								LIST_IC_PRODUCTO_PY:LIST_IC_PRODUCTO_PY,
								LIST_IG_PARAM_CAMPO_PY:LIST_IG_PARAM_CAMPO_PY,
								numRegistrosPY : numRegistrosPY,
								api:combo.getValue(),
								api_fin:combo.getValue()
							}),
							callback: procesarGuardaProyecto
						});
						
				});
				
			}
		}else if(combo.getValue()=='CN'){
			var storeCN = gridCN.getStore();
			var selectedCN = storeCN.getRange();
			varGlobCN();
			var numRegistrosCN = 0;
			Ext.each(selectedCN, function(item) {
				 LIST_IC_PARAMETRO_CN.push(item.data.IC_PARAMETRO);
				 LIST_NOMBRE_PARAM_CN.push(item.data.NOMBRE_PARAM);
				 LIST_CC_API_CN.push(item.data.CC_API);
				 LIST_IC_PRODUCTO_CN.push(item.data.IC_PRODUCTO);
				 LIST_IG_PARAM_CAMPO_CN.push(item.data.IG_PARAM_CAMPO);
				 numRegistrosCN++;
			}, this);
			Ext.each(selectedCN, function(item) {
				numReg = storeCN.indexOf(item);
				if(item.data.IG_PARAM_CAMPO==''&&(item.data.IC_PARAMETRO==2 || item.data.IC_PARAMETRO==7 )){
					ban_g2=1;
					var editor = gridCN.plugins[0];
					var fila = numReg;
					Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>El campo es obligatorio</center></td><td></td></table> </div>',
					function(){
						editor.startEditByPosition({row: fila, column:2});
					});
					valor= false
					return false;
				}
			
			});
			if(ban_g2==0 ){
				Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>La parametrizaci�n se realiz� exitosamente</center></td><td></td></table> </div>',function(){
					
						boton.setDisabled(true);
						boton.setIconCls('x-mask-msg-text');
						Ext.Ajax.request({
							url: '15ParamInterfaseDesc.data.jsp',
								params:Ext.apply(formaPrincipal.getForm().getValues(),{
								informacion: 'GUARDAR_CAMBIOS',
								IC_EPO:combo_epo.getValue(),
								LIST_IC_PARAMETRO_CN:LIST_IC_PARAMETRO_CN,
								LIST_NOMBRE_PARAM_CN:LIST_NOMBRE_PARAM_CN,
								LIST_CC_API_CN : LIST_CC_API_CN,
								LIST_IC_PRODUCTO_CN:LIST_IC_PRODUCTO_CN,
								LIST_IG_PARAM_CAMPO_CN:LIST_IG_PARAM_CAMPO_CN,
								numRegistrosCN : numRegistrosCN,
								api:combo.getValue(),
								api_fin:combo.getValue()
							}),
							callback: procesarGuardaProyecto
						});	
						
				});
			}
							
		}else if(combo.getValue()=='PR'){
			var storePR = gridPR.getStore();
			var selectedPR = storePR.getRange();
			varGlobPR();
			var numRegistrosPR = 0;
			Ext.each(selectedPR, function(item) {
				 LIST_IC_PARAMETRO_PR.push(item.data.IC_PARAMETRO);
				 LIST_NOMBRE_PARAM_PR.push(item.data.NOMBRE_PARAM);
				 LIST_CC_API_PR.push(item.data.CC_API);
				 LIST_IC_PRODUCTO_PR.push(item.data.IC_PRODUCTO);
				 LIST_IG_PARAM_CAMPO_PR.push(item.data.IG_PARAM_CAMPO);
				 numRegistrosPR++;
			}, this);
			Ext.each(selectedPR, function(item) {
				numReg = storePR.indexOf(item);
				if(item.data.IG_PARAM_CAMPO==''&&(item.data.IC_PARAMETRO==9 || item.data.IC_PARAMETRO==13 || item.data.IC_PARAMETRO==15 || item.data.IC_PARAMETRO==37|| item.data.IC_PARAMETRO==53|| item.data.IC_PARAMETRO==54)){
					
					var editor = gridPR.plugins[0];
					var fila = numReg;
						ban_g3=1;
						Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>El campo es obligatorio</center></td><td></td></table> </div>',
						function(){
							editor.startEditByPosition({row: fila, column:2});
						});
						valor= false
						return false;
				}
			
			});
			
			if(ban_g3==0 ){
				Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>La parametrizaci�n se realiz� exitosamente</center></td><td></td></table> </div>',function(){
					
						boton.setDisabled(true);
						boton.setIconCls('x-mask-msg-text');
						Ext.Ajax.request({
							url: '15ParamInterfaseDesc.data.jsp',
								params:Ext.apply(formaPrincipal.getForm().getValues(),{
								informacion: 'GUARDAR_CAMBIOS',
								IC_EPO:combo_epo.getValue(),
								LIST_IC_PARAMETRO_PR:LIST_IC_PARAMETRO_PR,
								LIST_NOMBRE_PARAM_PR:LIST_NOMBRE_PARAM_PR,
								LIST_CC_API_PR : LIST_CC_API_PR,
								LIST_IC_PRODUCTO_PR:LIST_IC_PRODUCTO_PR,
								LIST_IG_PARAM_CAMPO_PR:LIST_IG_PARAM_CAMPO_PR,
								numRegistrosPR : numRegistrosPR,
								api:combo.getValue(),
								api_fin:combo.getValue()
							}),
							callback: procesarGuardaProyecto
						});	
						
				});
			}
						
		}else if(combo.getValue()=='DS'){
			var storeDS = gridDS.getStore();
			var selectedDS = storeDS.getRange();
			varGlobDS();
			var numRegistrosDS = 0;
			Ext.each(selectedDS, function(item) {
				 LIST_IC_PARAMETRO_DS.push(item.data.IC_PARAMETRO);
				 LIST_NOMBRE_PARAM_DS.push(item.data.NOMBRE_PARAM);
				 LIST_CC_API_DS.push(item.data.CC_API);
				 LIST_IC_PRODUCTO_DS.push(item.data.IC_PRODUCTO);
				 LIST_IG_PARAM_CAMPO_DS.push(item.data.IG_PARAM_CAMPO);
				 numRegistrosDS++;
			}, this);
			Ext.each(selectedDS, function(item) {
				numReg = storeDS.indexOf(item);
				if(item.data.IG_PARAM_CAMPO==''&&(item.data.IC_PARAMETRO==2 || item.data.IC_PARAMETRO==9 )){
					ban_g4=1;
					var editor = gridDS.plugins[0];
					var fila = numReg;
					Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>El campo es obligatorio</center></td><td></td></table> </div>',
					function(){
						editor.startEditByPosition({row: fila, column:2});
					});
					valor= false
					return false;
				}
			
			});	
			if(ban_g4==0 ){
				Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>La parametrizaci�n se realiz� exitosamente</center></td><td></td></table> </div>',function(){
					
						boton.setDisabled(true);
						boton.setIconCls('x-mask-msg-text');
						Ext.Ajax.request({
							url: '15ParamInterfaseDesc.data.jsp',
								params:Ext.apply(formaPrincipal.getForm().getValues(),{
								informacion: 'GUARDAR_CAMBIOS',
								IC_EPO:combo_epo.getValue(),
								LIST_IC_PARAMETRO_DS:LIST_IC_PARAMETRO_DS,
								LIST_NOMBRE_PARAM_DS:LIST_NOMBRE_PARAM_DS,
								LIST_CC_API_DS : LIST_CC_API_DS,
								LIST_IC_PRODUCTO_DS:LIST_IC_PRODUCTO_DS,
								LIST_IG_PARAM_CAMPO_DS:LIST_IG_PARAM_CAMPO_DS,
								numRegistrosDS : numRegistrosDS,
								api:combo.getValue(),
								api_fin:combo.getValue()
							}),
							callback: procesarGuardaProyecto
						});
						
				});
			}
		}else if(combo.getValue()=='T'||Ext.isEmpty(combo.getValue())){
			var storeCN = gridCN.getStore();
			var selectedCN = storeCN.getRange();
			varGlobCN();
			var numRegistrosCN = 0;
			Ext.each(selectedCN, function(item) {
				 LIST_IC_PARAMETRO_CN.push(item.data.IC_PARAMETRO);
				 LIST_NOMBRE_PARAM_CN.push(item.data.NOMBRE_PARAM);
				 LIST_CC_API_CN.push(item.data.CC_API);
				 LIST_IC_PRODUCTO_CN.push(item.data.IC_PRODUCTO);
				 LIST_IG_PARAM_CAMPO_CN.push(item.data.IG_PARAM_CAMPO);
				 numRegistrosCN++;
			}, this);
			Ext.each(selectedCN, function(item) {
				numReg = storeCN.indexOf(item);
				if(item.data.IG_PARAM_CAMPO==''&&(item.data.IC_PARAMETRO==2 || item.data.IC_PARAMETRO==7 )){
					ban_g1=1;
					var editor = gridCN.plugins[0];
					var fila = numReg;
					Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>El campo es obligatorio</center></td><td></td></table> </div>',
					function(){
						editor.startEditByPosition({row: fila, column:2});
					});
					valor= false
					return false;
				}
			
			});
			
			var storeDS = gridDS.getStore();
			var selectedDS = storeDS.getRange();
			varGlobDS();
			var numRegistrosDS = 0;
			Ext.each(selectedDS, function(item) {
				 LIST_IC_PARAMETRO_DS.push(item.data.IC_PARAMETRO);
				 LIST_NOMBRE_PARAM_DS.push(item.data.NOMBRE_PARAM);
				 LIST_CC_API_DS.push(item.data.CC_API);
				 LIST_IC_PRODUCTO_DS.push(item.data.IC_PRODUCTO);
				 LIST_IG_PARAM_CAMPO_DS.push(item.data.IG_PARAM_CAMPO);
				 numRegistrosDS++;
			}, this);
			Ext.each(selectedDS, function(item) {
				numReg = storeDS.indexOf(item);
				if(item.data.IG_PARAM_CAMPO==''&&(item.data.IC_PARAMETRO==2 || item.data.IC_PARAMETRO==9 )){
					ban_g2=1;
					var editor = gridDS.plugins[0];
					var fila = numReg;
					Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>El campo es obligatorio</center></td><td></td></table> </div>',
					function(){
						editor.startEditByPosition({row: fila, column:2});
					});
					valor= false
					return false;
				}
			
			});
			
			var storePR = gridPR.getStore();
			var selectedPR = storePR.getRange();
			varGlobPR();
			var numRegistrosPR = 0;
			Ext.each(selectedPR, function(item) {
				 LIST_IC_PARAMETRO_PR.push(item.data.IC_PARAMETRO);
				 LIST_NOMBRE_PARAM_PR.push(item.data.NOMBRE_PARAM);
				 LIST_CC_API_PR.push(item.data.CC_API);
				 LIST_IC_PRODUCTO_PR.push(item.data.IC_PRODUCTO);
				 LIST_IG_PARAM_CAMPO_PR.push(item.data.IG_PARAM_CAMPO);
				 numRegistrosPR++;
			}, this);
			Ext.each(selectedPR, function(item) {
				numReg = storePR.indexOf(item);
				if(item.data.IG_PARAM_CAMPO==''&&(item.data.IC_PARAMETRO==9 || item.data.IC_PARAMETRO==13 || item.data.IC_PARAMETRO==15 || item.data.IC_PARAMETRO==37|| item.data.IC_PARAMETRO==53|| item.data.IC_PARAMETRO==54)){
					
					var editor = gridPR.plugins[0];
					var fila = numReg;5
						ban_g3=1;
						Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>El campo es obligatorio</center></td><td></td></table> </div>',
						function(){
							editor.startEditByPosition({row: fila, column:2});
						});
						valor= false
						return false;
				}
			
			});
			
			var storePY = gridPY.getStore();
			var selectedPY = storePY.getRange();
			var numRegistrosPY = 0;
			varGlobPY();
			Ext.each(selectedPY, function(item) {
				 LIST_IC_PARAMETRO_PY.push(item.data.IC_PARAMETRO);
				 LIST_NOMBRE_PARAM_PY.push(item.data.NOMBRE_PARAM);
				 LIST_CC_API_PY.push(item.data.CC_API);
				 LIST_IC_PRODUCTO_PY.push(item.data.IC_PRODUCTO);
				 LIST_IG_PARAM_CAMPO_PY.push(item.data.IG_PARAM_CAMPO);
				 numRegistrosPY++;
			}, this);
			Ext.each(selectedPY, function(item) {
				numReg = storePY.indexOf(item);
				if(item.data.IG_PARAM_CAMPO==''&&item.data.IC_PARAMETRO==3){
					ban_g4=1;
					var editor = gridPY.plugins[0];
					var fila = numReg;
					Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>El campo es obligatorio</center></td><td></td></table> </div>',
					function(){
						editor.startEditByPosition({row: fila, column:2});
					});
					valor= false
					return false;
				}
			
			});
			
			if(ban_g1==0 && ban_g2==0 && ban_g3==0 && ban_g4==0){
				
				Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>La parametrizaci�n se realiz� exitosamente</center></td><td></td></table> </div>',
				function(){
						boton.setDisabled(true);
						boton.setIconCls('x-mask-msg-text');
						Ext.Ajax.request({
							url: '15ParamInterfaseDesc.data.jsp',
								params:Ext.apply(formaPrincipal.getForm().getValues(),{
								informacion: 'GUARDAR_CAMBIOS',
								IC_EPO:combo_epo.getValue(),
								
								LIST_IC_PARAMETRO_PY:LIST_IC_PARAMETRO_PY,
								LIST_NOMBRE_PARAM_PY:LIST_NOMBRE_PARAM_PY,
								LIST_CC_API_PY : LIST_CC_API_PY,
								LIST_IC_PRODUCTO_PY:LIST_IC_PRODUCTO_PY,
								LIST_IG_PARAM_CAMPO_PY:LIST_IG_PARAM_CAMPO_PY,
								numRegistrosPY : numRegistrosPY,
								
								LIST_IC_PARAMETRO_CN:LIST_IC_PARAMETRO_CN,
								LIST_NOMBRE_PARAM_CN:LIST_NOMBRE_PARAM_CN,
								LIST_CC_API_CN : LIST_CC_API_CN,
								LIST_IC_PRODUCTO_CN:LIST_IC_PRODUCTO_CN,
								LIST_IG_PARAM_CAMPO_CN:LIST_IG_PARAM_CAMPO_CN,
								numRegistrosCN : numRegistrosCN,
								
								LIST_IC_PARAMETRO_PR:LIST_IC_PARAMETRO_PR,
								LIST_NOMBRE_PARAM_PR:LIST_NOMBRE_PARAM_PR,
								LIST_CC_API_PR : LIST_CC_API_PR,
								LIST_IC_PRODUCTO_PR:LIST_IC_PRODUCTO_PR,
								LIST_IG_PARAM_CAMPO_PR:LIST_IG_PARAM_CAMPO_PR,
								numRegistrosPR : numRegistrosPR,
								
								LIST_IC_PARAMETRO_DS:LIST_IC_PARAMETRO_DS,
								LIST_NOMBRE_PARAM_DS:LIST_NOMBRE_PARAM_DS,
								LIST_CC_API_DS : LIST_CC_API_DS,
								LIST_IC_PRODUCTO_DS:LIST_IC_PRODUCTO_DS,
								LIST_IG_PARAM_CAMPO_DS:LIST_IG_PARAM_CAMPO_DS,
								numRegistrosDS : numRegistrosDS,
								
								api:combo.getValue(),
								api_fin:'T'
							}),
							callback: procesarGuardaProyecto
						});	
				});
			}
			
		}
	}
	
	var procesarProyecto = function(store, arrRegistros, success, opts){
		
		if (arrRegistros != null){
			var jsonData = store.proxy.reader.jsonData;
			var api = jsonData.api;
			var rep_val = jsonData.rep_val;
			if(rep_val=='S'){
				Ext.ComponentQuery.query('#btnCopVEPO')[0].setIconCls('icoAceptar');
				Ext.ComponentQuery.query('#btnCopVEPO')[0].setDisabled(false);
			}else{
				Ext.ComponentQuery.query('#btnAceptar')[0].setIconCls('icoAceptar');
				Ext.ComponentQuery.query('#btnAceptar')[0].setDisabled(false);
			}
			var gridPY = Ext.ComponentQuery.query('#gridProyecto')[0];
			var gridCN = Ext.ComponentQuery.query('#gridContrato')[0];
			var gridPR = Ext.ComponentQuery.query('#gridPrestamo')[0];
			var gridDS = Ext.ComponentQuery.query('#gridDispersion')[0];
			var ban_boton = 0;
			if(api=='PY'){
				if(!gridPY.isVisible()){
					gridPY.show();
				}
				if(gridCN.isVisible()){
					gridCN.hide();
				}
				if(gridPR.isVisible()){
					gridPR.hide();
				}
				if(gridDS.isVisible()){
					gridDS.hide();
				}
				
				if(store.getTotalCount() > 0){
					gridPY.getView().setAutoScroll(true);
				} else{
					gridPY.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
					gridPY.getView().setAutoScroll(false);
				}
				ban_boton = 1;
			}else if(api=='CN'){
				if(gridPY.isVisible()){
					gridPY.hide();
				}
				if(!gridCN.isVisible()){
					gridCN.show();
					
				}
				if(gridPR.isVisible()){
					gridPR.hide();
				}
				if(gridDS.isVisible()){
					gridDS.hide();
				}
				
				if(store.getTotalCount() > 0){
					gridCN.getView().setAutoScroll(true);
				} else{
					gridCN.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
					gridCN.getView().setAutoScroll(false);
				}
				ban_boton = 1;
			}else if(api=='PR'){
				if(gridPY.isVisible()){
					gridPY.hide();
				}
				if(gridCN.isVisible()){
					gridCN.hide();
				}
				if(!gridPR.isVisible()){
					gridPR.show();
				}
				if(gridDS.isVisible()){
					gridDS.hide();
				}
				
				if(store.getTotalCount() > 0){
					gridPR.getView().setAutoScroll(true);
				} else{
					gridPR.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
					gridPR.getView().setAutoScroll(false);
				}
				ban_boton = 1;
			}else if(api=='DS'){
				if(gridPY.isVisible()){
					gridPY.hide();
				}
				if(gridCN.isVisible()){
					gridCN.hide();
				}
				if(gridPR.isVisible()){
					gridPR.hide();
				}
				if(!gridDS.isVisible()){
					gridDS.show();
					
				}
				
				if(store.getTotalCount() > 0){
					gridDS.getView().setAutoScroll(true);
				} else{
					gridDS.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
					gridDS.getView().setAutoScroll(false);
				}
				ban_boton = 1;
			}else if(api=='T'){
				if(!gridPY.isVisible()){
					gridPY.show();
			
					if(store.getTotalCount() > 0){
						gridPY.getView().setAutoScroll(true);
					} else{
						gridPY.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
						gridPY.getView().setAutoScroll(false);
					};
				}
				if(!gridCN.isVisible()){
					gridCN.show();
					if(store.getTotalCount() > 0){
						gridCN.getView().setAutoScroll(true);
					} else{
						gridCN.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
						gridCN.getView().setAutoScroll(false);
					};
				}
				if(!gridPR.isVisible()){
					gridPR.show();
					if(store.getTotalCount() > 0){
						gridPR.getView().setAutoScroll(true);
					} else{
						gridPR.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
						gridPR.getView().setAutoScroll(false);
					};
				}
				if(!gridDS.isVisible()){
					gridDS.show();
					if(store.getTotalCount() > 0){
						gridDS.getView().setAutoScroll(true);
					} else{
						gridDS.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
						gridDS.getView().setAutoScroll(false);
					};
				}
				ban_boton = 1;
			}
			if(ban_boton == 1){
				Ext.ComponentQuery.query('#fpBotones')[0].show();
				Ext.ComponentQuery.query('#checkRVEPO')[0].show();
				storeEpoParam.load(
					{
						params: Ext.apply({ 
							combo_param:'S',
							ic_epo:Ext.ComponentQuery.query('#ic_epo')[0].getValue(),
							api:Ext.ComponentQuery.query('#ic_interfaz')[0].getValue()
						})
					}
				);	
				
			}else{
				Ext.ComponentQuery.query('#fpBotones')[0].hide();
				//Ext.ComponentQuery.query('#compoReplica')[0].hide();
				Ext.ComponentQuery.query('#checkRVEPO')[0].hide();
			}
			
			
		}
	}
	Ext.define('ListaProyecto',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'IC_PARAMETRO'     },
				{name: 'NOMBRE_PARAM'     },
				{name: 'CC_API'     },
				{name: 'IC_PRODUCTO'     },
				{name: 'IG_PARAM_CAMPO'     }
			]
	});
	Ext.define('ListaContrato',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'IC_PARAMETRO'     },
				{name: 'NOMBRE_PARAM'     },
				{name: 'CC_API'     },
				{name: 'IC_PRODUCTO'     },
				{name: 'IG_PARAM_CAMPO'     }
			]
	});
	Ext.define('ListaPrestamo',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'IC_PARAMETRO'     },
				{name: 'NOMBRE_PARAM'     },
				{name: 'CC_API'     },
				{name: 'IC_PRODUCTO'     },
				{name: 'IG_PARAM_CAMPO'     }
			]
	});
	Ext.define('ListaDisposicion',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'IC_PARAMETRO'     },
				{name: 'NOMBRE_PARAM'     },
				{name: 'CC_API'     },
				{name: 'IC_PRODUCTO'     },
				{name: 'IG_PARAM_CAMPO'     }
			]
	});
	var Todas = Ext.define('ModelEpo',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'CLAVE',       type: 'string'},
				{ name: 'DESCRIPCION', type: 'string'}
			]
		}
	);
	Ext.define('ModelEpoParam',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'CLAVE',       type: 'string'},
				{ name: 'DESCRIPCION', type: 'string'}
			]
		}
	);
	var storeEpo = Ext.create('Ext.data.Store',{
		model:'ModelEpo',
		autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '15ParamInterfaseDesc.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Epos'
				
			},
				totalProperty:  'total'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
	});
	var storeEpoParam = Ext.create('Ext.data.Store',{
		model:'ModelEpoParam',
		autoLoad: false,
		proxy: {
			type: 'ajax',
			url: '15ParamInterfaseDesc.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Epos'
			},
				totalProperty:  'total'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
	});
	var procesarTodos = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "T", 
		descripcion: "Todos", 
		loadMsg: ""})); 
		store.commitChanges(); 
	}
	var Todas = Ext.define('ModelApi',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	var CatalogoApis = Ext.create('Ext.data.Store',{
		model:'ModelApi',
		autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '15ParamInterfaseDesc.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CatalogoApis'
			},
				totalProperty:  'total'
			},
			listeners: {
				load: procesarTodos,
				exception: NE.util.mostrarProxyAjaxError
			}
	});
	var moneda = Ext.define('ModelMoneda',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	var catMoneda = Ext.create('Ext.data.Store',{
		model:'ModelMoneda',
		autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '15ParamInterfaseDesc.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'cat_moneda'
			},
				totalProperty:  'total'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
	});
	
	var consultaProyecto = Ext.create('Ext.data.Store',{
			model: 'ListaProyecto',
			storeId: 'consultaProyecto',
			proxy: {
				type: 'ajax',
				url:  '15ParamInterfaseDesc.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'CONSULTA_INFORMACION'
					
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			listeners: {
				load: procesarProyecto
			},
			autoLoad: false
		});
		var consultaContrato = Ext.create('Ext.data.Store',{
			model: 'ListaContrato',
			storeId: 'consultaContrato',
			proxy: {
				type: 'ajax',
				url:  '15ParamInterfaseDesc.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'CONSULTA_INFORMACION'
					
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			listeners: {
				load: procesarProyecto
			},
			autoLoad: false
		});
		var consultaPrestamo = Ext.create('Ext.data.Store',{
			model: 'ListaPrestamo',
			storeId: 'consultaPrestamo',
			proxy: {
				type: 'ajax',
				url:  '15ParamInterfaseDesc.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'CONSULTA_INFORMACION'
					
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			listeners: {
				load: procesarProyecto
			},
			autoLoad: false
		});
		var consultaDisposicion = Ext.create('Ext.data.Store',{
			model: 'ListaDisposicion',
			storeId: 'consultaDisposicion',
			proxy: {
				type: 'ajax',
				url:  '15ParamInterfaseDesc.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'CONSULTA_INFORMACION'
					
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			listeners: {
				load: procesarProyecto
			},
			autoLoad: false
		});
	
	

	
	
	var elementosForma = [
		{ 	
			xtype: 'displayfield',	
			width:450, 
			fieldLabel: 'Producto',
			labelWidth: 120,
			value:'Descuento Electr�nico 1er Piso Factoraje'
					
		},	 
		{
			xtype: 'combo',
			itemId:  'ic_epo',
			fieldLabel: 'Nombre de la EPO',
			name:  'ic_epo',
			hiddenName: 'ic_epo',
			allowBlank: false,
			forceSelection:true,
			emptyText: 'Seleccione...',
			queryMode:				'local',
			displayField:	'DESCRIPCION',
			valueField:		'CLAVE',
			typeAhead:		true,
			labelWidth: 120,
			triggerAction:	'all',
			width:  450,
			store: storeEpo,
			listeners: {
				select: function(combo, record, index) {
					if(combo.getValue()!=null){
						Ext.ComponentQuery.query('#cmbMoneda')[0].setValue("1");
					}else{
						Ext.ComponentQuery.query('#cmbMoneda')[0].setValue("");
					}
				}
			}
		},
		
		{
			xtype: 'combo',
			itemId:  'ic_interfaz',
			fieldLabel: 'Interfase',
			name:  'ic_interfaz',
			hiddenName: 'ic_interfaz',
			forceSelection:true,
			emptyText: 'Seleccione...',
			queryMode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			typeAhead:		true,
			triggerAction:	'all',
			width:  450,
			labelWidth: 120,
			store: CatalogoApis,
			listeners: {
				select: function(combo, record, index) {
					Ext.ComponentQuery.query('#ic_epo_param')[0].setValue('');
					var gridPY = Ext.ComponentQuery.query('#gridProyecto')[0];
					var gridCN = Ext.ComponentQuery.query('#gridContrato')[0];
					var gridPR = Ext.ComponentQuery.query('#gridPrestamo')[0];
					var gridDS = Ext.ComponentQuery.query('#gridDispersion')[0];
					var btones =Ext.ComponentQuery.query('#fpBotones')[0].hide();
					var cmbRep =Ext.ComponentQuery.query('#compoReplica')[0].hide();
					var check =Ext.ComponentQuery.query('#checkRVEPO')[0].hide();
					Ext.ComponentQuery.query('#rvepo')[0].setValue(false);
					Ext.ComponentQuery.query('#btnCopVEPO')[0].disable();
					if(gridPY.isVisible()){
						gridPY.hide();
					}
					if(gridCN.isVisible()){
						gridCN.hide();
					}
					if(gridPR.isVisible()){
						gridPR.hide();
					}
					if(gridDS.isVisible()){
						gridDS.hide();
					}
					if(btones.isVisible()){
						btones.hide();
					}
					if(cmbRep.isVisible()){
						cmbRep.hide();
					}
					if(check.isVisible()){
						check.hide();
					}
				}
			}
		},
		{
			xtype: 'combo',
			itemId:  'cmbMoneda',
			fieldLabel: 'Moneda',
			name:  'cmbMoneda',
			hiddenName: 'cmbMoneda',
			forceSelection:true,
			emptyText: 'Seleccione...',
			queryMode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			typeAhead:		true,
			triggerAction:	'all',
			width:  450,
			labelWidth: 120,
			store:catMoneda
		}
		
	]
	var formaPrincipal = new Ext.form.FormPanel({
		layout:            'form',
		width:530,
		frame: true,
		title:'Parametrizaci�n Interfase Descuento 1er Piso',
		bodyPadding: '0 0 10 0',
		fieldDefaults: {
			msgTarget: 'side',
         anchor: '100%',
			labelWidth: 120
		},
		autoHeight:        true,
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text:                   'Consultar',
				itemId:                 'btnAceptar',
				formBind: true,
				iconCls:                'icoAceptar',
				handler: function(boton, evento) {
					var combo = Ext.ComponentQuery.query('#ic_interfaz')[0];
					var combo_epo = Ext.ComponentQuery.query('#ic_epo')[0];
					boton.setDisabled(true);
					boton.setIconCls('x-mask-msg-text');
					if(combo.getValue()=='PY'){
							consultaProyecto.load({
								params: Ext.apply({
									api:combo.getValue(),
									ic_epo:combo_epo.getValue(),
									rep_val:'N'
								})				 
							});	
					}else if(combo.getValue()=='CN'){
							consultaContrato.load({
								params: Ext.apply({
									api:combo.getValue(),
									ic_epo:combo_epo.getValue(),
									rep_val:'N'
								})				 
							});	
					}else if(combo.getValue()=='PR'){
							consultaPrestamo.load({
								params: Ext.apply({
									api:combo.getValue(),
									ic_epo:combo_epo.getValue(),
									rep_val:'N'
								})				 
							});	
					}else if(combo.getValue()=='DS'){
							consultaDisposicion.load({
								params: Ext.apply({
									api:combo.getValue(),
									ic_epo:combo_epo.getValue(),
									rep_val:'N'
								})				 
							});	
					}else if(combo.getValue()=='T'||Ext.isEmpty(combo.getValue())){
							consultaProyecto.load({
								params: Ext.apply({
									api:'PY',
									api_fin:'T',
									ic_epo:combo_epo.getValue(),
									rep_val:'N'
								})				 
							});
							consultaContrato.load({
								params: Ext.apply({
									api:'CN',
									api_fin:'T',
									ic_epo:combo_epo.getValue(),
									rep_val:'N'
								})				 
							});
							consultaPrestamo.load({
								params: Ext.apply({
									api:'PR',
									api_fin:'T',
									ic_epo:combo_epo.getValue(),
									rep_val:'N'
								})				 
							});
							consultaDisposicion.load({
								params: Ext.apply({
									api:'DS',
									api_fin:'T',
									ic_epo:combo_epo.getValue(),
									rep_val:'N'
								})				 
							});	
						}
				}
			},
			{
				xtype: 'button',
				text:       'Limpiar',
				iconCls:    'icoLimpiar',
				handler: function(boton, evento) {
					this.up('form').getForm().reset();
					Ext.ComponentQuery.query('#ic_epo_param')[0].setValue('');
					var gridPY = Ext.ComponentQuery.query('#gridProyecto')[0];
					var gridCN = Ext.ComponentQuery.query('#gridContrato')[0];
					var gridPR = Ext.ComponentQuery.query('#gridPrestamo')[0];
					var gridDS = Ext.ComponentQuery.query('#gridDispersion')[0];
					var btones =Ext.ComponentQuery.query('#fpBotones')[0].hide();
					var cmbRep =Ext.ComponentQuery.query('#compoReplica')[0].hide();
					var check =Ext.ComponentQuery.query('#checkRVEPO')[0].hide();
					
					if(gridPY.isVisible()){
						gridPY.hide();
					}
					if(gridCN.isVisible()){
						gridCN.hide();
					}
					if(gridPR.isVisible()){
						gridPR.hide();
					}
					if(gridDS.isVisible()){
						gridDS.hide();
					}
					if(btones.isVisible()){
						btones.hide();
					}
					if(cmbRep.isVisible()){
						cmbRep.hide();
					}
					if(check.isVisible()){
						check.hide();
					}
				}
			}
		]
	});
	
	var gridProyecto = Ext.create('Ext.grid.Panel',{
			selType: 'cellmodel',
			plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
			itemId:         'gridProyecto',
			store: Ext.data.StoreManager.lookup('consultaProyecto'),
			width:          530,
			height:         200,
			hidden:true,
			style: 'margin:0 auto;',
			margins: '0 20 0 0',
			title:'<center>Proyectos</center>',
			frame: false,
			columns: [
				{
					header: 'N�mero de Par�metro', 
					tooltip: 'N�mero de<br>Par�metro',
					dataIndex: 'IC_PARAMETRO',
					width: 120,							
					resizable: true,				
					align: 'center'
				},
				{
					header: 'Nombre', 
					tooltip: 'Nombre',
					dataIndex: 'NOMBRE_PARAM',
					width: 200,							
					resizable: true,				
					align: 'center'
				},
				{
					header: 'Par�metros del Campo',
					tooltip: 'Par�metros<br>del Campo',
					dataIndex: 'IG_PARAM_CAMPO',
					sortable: true,
					width: 200,
					resizable: true,
					align: 'center',
					editor: 
					{
						
						xtype: 'textfield',
						allowBlank: true,
						width: '50',
						listeners:{
							change: function(obj, value) {
								var gridPY = Ext.ComponentQuery.query('#gridProyecto')[0];
								var filaSel = gridPY.getSelectionModel().getSelection();  
								var ic_param = "";
								Ext.each(filaSel, function (item) {
								  ic_param= item.data.IC_PARAMETRO;
								});
								obj.regex = new RegExp("");
								if(ic_param==3){
									obj.regex = new RegExp("^([0-9])*$");
									obj.regexText = '<div > <table width="120" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>Solo permite ingresar n�meros.</center></td></tr> </table> </div>';
									if(obj.validate()){
										obj.regex = new RegExp("(^((\\d){1,3})$)");
										obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 3 enteros.</center></td></tr> </table> </div>';
									}
								}else{
									obj.regex = new RegExp("(^((\\S){1,3})$)");
									obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 3 caracteres.</center></td></tr> </table> </div>';
							
								}
							}
                  } 
					},
					renderer:function(value,metadata,registro){
						metadata.tdAttr  = 'style="border: thin solid #3399CC;"';
						return value;
					}
				}

			]
		});
		
	var gridContrato = Ext.create('Ext.grid.Panel',{
			selType: 'cellmodel',
			plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
			itemId:         'gridContrato',
			store: Ext.data.StoreManager.lookup('consultaContrato'),
			width:          530,
			height:         280,
			hidden:true,
			style: 'margin: 10px auto 0px auto;',
			margins: '0 20 0 0',
			title:'<center>Contratos</center>',
			frame: false,
			columns: [
				{
					header: 'N�mero de Par�metro', 
					tooltip: 'N�mero de<br>Par�metro',
					dataIndex: 'IC_PARAMETRO',
					width: 120,							
					resizable: true,				
					align: 'center'
				},
				{
					header: 'Nombre', 
					tooltip: 'Nombre',
					dataIndex: 'NOMBRE_PARAM',
					width: 200,							
					resizable: true,				
					align: 'center'
				},
				{
					header: 'Par�metros del Campo',
					tooltip: 'Par�metros<br>del Campo',
					dataIndex: 'IG_PARAM_CAMPO',
					sortable: true,
					width: 200,
					resizable: true,
					align: 'center',
					editor: 
					{
						
						xtype: 'textfield',
						allowBlank: true,
						width: '50',
						listeners:{
                     change: function(obj, value){  
								var gridCN = Ext.ComponentQuery.query('#gridContrato')[0];
								var filaSel = gridCN.getSelectionModel().getSelection();  
								var ic_param = "";
								Ext.each(filaSel, function (item) {
								  ic_param= item.data.IC_PARAMETRO;
								});
								obj.regex = new RegExp("");
								if(ic_param==2){
									obj.regex = new RegExp("^([0-9])*$");
									obj.regexText = '<div > <table width="120" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>Solo permite ingresar n�meros.</center></td></tr> </table> </div>';
									if(obj.validate()){
										obj.regex = new RegExp("(^((\\d){1,3})$)");
										obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 3 enteros.</center></td></tr> </table> </div>';
									}
								}else if(ic_param==7){
									obj.regex = new RegExp("(^((\\S){1,3})$)");
									obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 3 caracteres.</center></td></tr> </table> </div>';
								}else{
									obj.regex = new RegExp("(^((\\S){1,3})$)");
									obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 3 caracteres.</center></td></tr> </table> </div>';
							
								}
								
							}
                  } 
					} ,
					renderer:function(value,metadata,registro){
						metadata.tdAttr  = 'style="border: thin solid #3399CC;"';
						return value;
					}		
				}

			]
		});
		var gridPrestamo = Ext.create('Ext.grid.Panel',{
			selType: 'cellmodel',
			plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
			itemId:         'gridPrestamo',
			store: Ext.data.StoreManager.lookup('consultaPrestamo'),
			width:          530,
			height:         280,
			style: 'margin: 10px auto 0px auto;',
			margins: '0 20 0 0',
			hidden:true,
			title:'<center>Prestamos</center>',
			frame: false,
			columns: [
				{
					header: 'N�mero de Par�metro', 
					tooltip: 'N�mero de<br>Par�metro',
					dataIndex: 'IC_PARAMETRO',
					width: 120,							
					resizable: true,				
					align: 'center'
				},
				{
					header: 'Nombre', 
					tooltip: 'Nombre',
					dataIndex: 'NOMBRE_PARAM',
					width: 200,							
					resizable: true,				
					align: 'center'
				},
				{
					header: 'Par�metros del Campo',
					tooltip: 'Par�metros<br>del Campo',
					dataIndex: 'IG_PARAM_CAMPO',
					sortable: true,
					width: 200,
					resizable: true,
					align: 'center',
					editor: 
					{
						
						xtype: 'textfield',
						allowBlank: true,
						width: '50',
						listeners:{
                     change: function(obj, value){  
								var gridPR = Ext.ComponentQuery.query('#gridPrestamo')[0];
								var filaSel = gridPR.getSelectionModel().getSelection();  
								var ic_param = "";
								Ext.each(filaSel, function (item) {
								  ic_param= item.data.IC_PARAMETRO;
								});
								obj.regex = new RegExp("");
								obj.regexText = '';
								if(ic_param==9 || ic_param==13 || ic_param==21 || ic_param==53){
									obj.regex = new RegExp("(^((\\S){1})$)");
									obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 1 car�cter.</center></td></tr> </table> </div>';
									obj.validate();
								}else if(ic_param==15 || ic_param==54 || ic_param==37 ){
									obj.regex = new RegExp("^([0-9])*$");
									obj.regexText = '<div > <table width="120" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>Solo permite ingresar n�meros.</center></td></tr> </table> </div>';
									if(obj.validate()){
										obj.regex = new RegExp("(^((\\d){1})$)");
										obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 1 entero.</center></td></tr> </table> </div>';
										obj.validate();
									}
								}else if(ic_param==16 ){
									obj.regex = new RegExp("(^(([0-9])*)(\\.(([0-9])*))$)|(^((([0-9])*))$)");
									obj.regexText = '<div > <table width="120" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>Solo permite ingresar n�meros.</center></td></tr> </table> </div>';
									if(obj.validate()){
										obj.regex = new RegExp("(^(((\\d){1}))(\\.\\d{1})$)|(^((\\d){1})$)");
										obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 1 entero y 1 decimal.</center></td></tr> </table> </div>';
										obj.validate();
									}
								}else if(ic_param==36){
									obj.regex = new RegExp("^([0-9])*$");
									obj.regexText = '<div > <table width="120" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>Solo permite ingresar n�meros.</center></td></tr> </table> </div>';
									if(obj.validate()){
										obj.regex = new RegExp("(^((\\d){1,3})$)");
										obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 3 enteros.</center></td></tr> </table> </div>';
										obj.validate();
									}
								}else{
									obj.regex = new RegExp("(^((\\S){1,3})$)");
									obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 3 caracteres.</center></td></tr> </table> </div>';
							
								}
								
							}
                  }
					},
					renderer:function(value,metadata,registro){
						metadata.tdAttr  = 'style="border: thin solid #3399CC;"';
						return value;
					} 			
				}

			]
		});
		var gridDispersion = Ext.create('Ext.grid.Panel',{
			selType: 'cellmodel',
			plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
			itemId:         'gridDispersion',
			store: Ext.data.StoreManager.lookup('consultaDisposicion'),
			width:          530,
			height:         280,
			style: 'margin: 10px auto 0px auto;',
			margins: '0 20 0 0',
			hidden:true,
			title:'<center>Disposiciones</center>',
			frame: false,
			columns: [
				{
					header: 'N�mero de Par�metro', 
					tooltip: 'N�mero de<br>Par�metro',
					dataIndex: 'IC_PARAMETRO',
					width: 120,							
					resizable: true,				
					align: 'center'
				},
				{
					header: 'Nombre', 
					tooltip: 'Nombre',
					dataIndex: 'NOMBRE_PARAM',
					width: 200,							
					resizable: true,				
					align: 'center'
				},
				{
					header: 'Par�metros del Campo',
					tooltip: 'Par�metros<br>del Campo',
					dataIndex: 'IG_PARAM_CAMPO',
					sortable: true,
					width: 200,
					resizable: true,
					align: 'center',
					editor: 
					{
						
						xtype: 'textfield',
						allowBlank: true,
						width: '50',
						listeners:{
                     change: function(obj, value){  
								var gridDS = Ext.ComponentQuery.query('#gridDispersion')[0];
								var filaSel = gridDS.getSelectionModel().getSelection();  
								var ic_param = "";
								Ext.each(filaSel, function (item) {
								  ic_param= item.data.IC_PARAMETRO;
								});
								obj.regex = new RegExp("");
								if(ic_param==2){
									obj.regex = new RegExp("^([0-9])*$");
									obj.regexText = '<div > <table width="120" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>Solo permite ingresar n�meros.</center></td></tr> </table> </div>';
									if(obj.validate()){
										obj.regex = new RegExp("(^((\\d){1,3})$)");
										obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 3 enteros.</center></td></tr> </table> </div>';
									}
								}else if(ic_param==9){
									obj.regex = new RegExp("^([0-9])*$");
									obj.regexText = '<div > <table width="120" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>Solo permite ingresar n�meros.</center></td></tr> </table> </div>';
									if(obj.validate()){
										obj.regex = new RegExp("(^((\\d){1,7})$)");
										obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 7 enteros.</center></td></tr> </table> </div>';
									}
								}else{
									obj.regex = new RegExp("(^((\\S){1,3})$)");
									obj.regexText = '<div > <table width="200" height:"15", border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>La longitud M�xima del campo es de 3 caracteres.</center></td></tr> </table> </div>';
							
								}
								
							}
                  } 
					},
					renderer:function(value,metadata,registro){
						metadata.tdAttr  = 'style="border: thin solid #3399CC;"';
						return value;
					} 			
				}

			]
		});
	var botonesTbr = new Ext.Container({
		layout: 'hbox',
		itemId: 'fpBotones',			
		width:          600,
		hidden:true,
		border : true,
		heigth:	'auto',
		align: 'center',	
		style: 'margin: 10px auto 0px 200px;',
		items: [	
			
			{
				xtype: 'button',
				text:       'Generar PDF',
				width:	100,
				itemId:     'btnPDF',
				iconCls:'icoPdf',
				handler: descargaPDF
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text:       'Generar Archivo',
				width:	120,
				itemId:     'btnGA',
				iconCls:'icoXls',
				handler:     descargaXLS
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text:       'Parametrizaci�n valores por Default',
				width:	200,
				itemId:     'btnParamdef',
				iconCls:'icoAceptar',
				handler:     asignaValoresDefault
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text:       'Guardar',
				width:	80,
				itemId:     'btnGuardar',
				iconCls:'icoGuardar',
				handler:     validaCampo
			},
			{
				xtype: 'box',
				width: 20
			}
		]
	});
	
	var compoReplica = new Ext.Container({
		layout: 'form',
		itemId: 'compoReplica',			
		width:	700,
		hidden:true,
		heigth:	'auto',
		border : true,
		frame :true,
		style: 'margin: 10px 160px; 0px auto;',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype:'panel',
				layout: 'table',
				frame: false,
				border	: false,
				items: [
					{
						xtype: 'combobox',
						fieldLabel: 'Seleccione EPO',
						itemId:  'ic_epo_param',
						name:  'ic_epo_param',
						hiddenName: 'ic_epo_param',
						store: storeEpoParam,
						forceSelection:true,
						width:	450,
						labelWidth: 100,
						emptyText: 'Seleccione...',
						queryMode:				'local',
						displayField:	'DESCRIPCION',
						valueField:		'CLAVE',
						allowBlank: true,
						triggerAction:	'all',
						blankText:'cc',
						typeAhead:		true,
						listeners:{
							select: function(combo, record, index) {
								 if(combo.getValue()){
									Ext.ComponentQuery.query('#btnCopVEPO')[0].enable();
								 }else{
									Ext.ComponentQuery.query('#btnCopVEPO')[0].disable();
								 }
									
								}
							}
						
					},
					{
						xtype: 'box',
						width: 20
					},
					{
						xtype: 'button',
						text:       'Copiar Valores de EPO',
						width:	150,
						itemId:     'btnCopVEPO',
						disabled: true,
						iconCls:'icoAceptar',
						handler:     obtenerValoresEPO
					}
				]
			},
			{
				xtype:'label',
				width:	500,
				style:    {'font-size' : '12px'},
				html:'<left><b>Nota:</b>  Solo se seleccionar� la EPO en caso de requerir parametrizar los valores de otra EPO.</left><br><br>'
			}
		]
	});
	var checkRVEPO = new Ext.Container({
		layout: 'form',
		width:	500,
		hidden:true,
		heigth:	'auto',
		itemId: 'checkRVEPO',
		border : true,
		frame :true,
		style: 'margin: 10px 400px; 0px auto;',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				items: [
					{
						xtype: 'checkboxfield',
						boxLabel  : 'Replicar valores de EPO parametrizada',
						name      : 'rvepo',
						itemId        : 'rvepo',
						listeners: {
							  change: function(cb, checked) {
								if(checked == true){
									Ext.ComponentQuery.query('#compoReplica')[0].show();
									Ext.ComponentQuery.query('#btnCopVEPO')[0].show();
								}else{
									Ext.ComponentQuery.query('#ic_epo_param')[0].setValue("");
									Ext.ComponentQuery.query('#compoReplica')[0].hide();
									Ext.ComponentQuery.query('#btnCopVEPO')[0].hide();
									Ext.ComponentQuery.query('#btnCopVEPO')[0].disable();
								}
									
						 }
						}
					}
				]
			}
		]
	});


	var main = Ext.create('Ext.container.Container',{
		itemId:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		width:      949,
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			NE.util.getEspaciador(10),
			gridProyecto,
			gridContrato,
			gridPrestamo,
			gridDispersion,
			checkRVEPO,
			compoReplica,
			botonesTbr
		]
	});
	
	CatalogoApis.load();
	storeEpo.load(
		{
			params: Ext.apply({ 
				combo_param:'N'
			})
		}
	);
	
});