Ext.onReady(function(){

//--------------------------------HANDLERS-----------------------------------
	
	//Funcion para validar la peticion Ajax y mostrar el usuario y fecha
	function procesaValoresIniciales(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if(jsonValoresIniciales != null){	
				var strNombreUsuario = Ext.getCmp('strNombreUsuario');
				var fechaHora = Ext.getCmp('fechaHora');
				strNombreUsuario.getEl().update('Usuario: '+jsonValoresIniciales.strNombreUsuario);
				fechaHora.getEl().update('Fecha: '+jsonValoresIniciales.fechaHora);	
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}
	}
	
	//Funcion que muestra el total de registros en Proceso encontrados
	var validacionProceso = function(){
		var el = gridProceso.getGridEl();
		if(consultaDataProceso.getTotalCount() > 0){
			el.unmask();		
			Ext.getCmp('btnGenerarPDFProceso').enable();
			Ext.getCmp('lblProceso').setText("Total en Proceso: " + consultaDataProceso.getTotalCount());
		}else{
			el.mask('No se encontr� ning�n registro', 'x-mask');
			Ext.getCmp('btnGenerarPDFProceso').disable();

		}
		
	}
	
	//Funcion que muestra el total de registros Operadas encontrados
	var validacionOperadas = function(opts, success, response){
		var el = gridOperadas.getGridEl();
		if(consultaDataOperadas.getTotalCount() > 0){
			el.unmask();		
			Ext.getCmp('btnGenerarPDFOperadas').enable();
			Ext.getCmp('lblOperadas').setText("Total Operadas: " + consultaDataOperadas.getTotalCount());
		}else{
			el.mask('No se encontr� ning�n registro', 'x-mask');
			Ext.getCmp('btnGenerarPDFOperadas').disable();
		}
	}
	
	//Funcion que valida Ajax de crear PDF y muestra el boton bajar PDF (En Proceso)
	var procesarSuccessFailurePdfProceso = function(opts, success, response){
		var btnGenerarPdfPro = Ext.getCmp('btnGenerarPDFProceso');
		btnGenerarPdfPro.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			btnGenerarPdfPro.enable();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Funcion que valida Ajax de crear PDF y muestra el boton bajar PDF (Operadas)
	var procesarSuccessFailurePdfOperadas = function(opts, success, response){
		var btnGenerarPdfOP = Ext.getCmp('btnGenerarPDFOperadas');
		btnGenerarPdfOP.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			btnGenerarPdfOP.enable();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------------STORES------------------------------------

	//Store para grid de Estatus en proceso
	var consultaDataProceso = new Ext.data.JsonStore({
		root: 'registros',
		url: '15credop01ext.data.jsp',
		baseParams: {
			informacion: 'consultaDatosProceso'
		},
		fields: [
					{name: 'NOMBREPYME'},
					{name: 'CLAVESIRAC'},
					{name: 'NUMDOCTO'},
					{name: 'FECHAETC'},
					{name: 'FECHADSCTO'},
					{name: 'NOMBREMONEDA'},
					{name: 'DESCTIPOCRED'},
					{name: 'IMPORTEDOCTO'},				
					{name: 'IMPORTEDSCTO'},
					{name: 'FOLIO'},
					{name: 'ESTATUSSOLIC'},
					{name: 'NUMPRESTAMO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: validacionProceso,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});
	
	//Store para grid de Estatus Operadas
	var consultaDataOperadas = new Ext.data.JsonStore({
		root: 'registros',
		url: '15credop01ext.data.jsp',
		baseParams: {
			informacion: 'consultaDatosOperadas'
		},
		fields: [
					{name: 'NOMBREPYME'},
					{name: 'CLAVESIRAC'},
					{name: 'NUMDOCTO'},
					{name: 'FECHAETC'},
					{name: 'FECHADSCTO'},
					{name: 'NOMBREMONEDA'},
					{name: 'DESCTIPOCRED'},
					{name: 'IMPORTEDOCTO'},				
					{name: 'IMPORTEDSCTO'},
					{name: 'FOLIO'},
					{name: 'ESTATUSSOLIC'},
					{name: 'NUMPRESTAMO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: validacionOperadas,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});

//------------------------------COMPONENTES----------------------------------
	
	//Elementos para Usuario y Fecha
	var elementosForma = [
		{
			xtype: 'label',
			id: 'strNombreUsuario',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldlabel: 'Usuario',
			text: '--'
		},
		{
			xtype: 'label',
			id: 'fechaHora',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldlabel: 'Fecha',
			text: '--'
		}
	];
	
	//Form para mostrar elementos Usuario y Fecha
	var fp = new Ext.form.FormPanel({
		id:				'forma',
		width:			310,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			false,
		bodyStyle:		'padding: 6px',
		labelWidth:		150,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosForma,
		monitorValid:	true
	});
	
	//Grid para registros con estatus en proceso
	var gridProceso = new Ext.grid.GridPanel({
		id: 'gridProceso',
		store: consultaDataProceso,
		hidden: false,
		margins: '20 0 0 0',
		align: 'center',
		frame: true,
		height: 300,
		width: 850,
		style: 'margin:0 auto',
		title: 'Estatus:	En Proceso',
		columns:[
			{
				header: 'Nombre del<br>Cliente',
				tooltip: 'Nombre del Cliente',
				width: 80,
				dataIndex: 'NOMBREPYME',
				sortable: true,
				resizable: false,
				align: 'left'
			},
			{
				header: 'N�mero de<br>Sirac',
				tooltip: 'N�mero de Sirac',
				width: 80,
				dataIndex: 'CLAVESIRAC',
				sortable: true,
				resizable: false,
				align: 'center'
			},
			{
				header: 'N�mero de<br>Documento',
				tooltip: 'N�mero de Documento',
				width: 80,
				dataIndex: 'NUMDOCTO',
				sortable: true,
				resizable: false,
				align: 'center'
			},
			{
				header: 'Fecha de<br>Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				width: 80,
				dataIndex: 'FECHAETC',
				sortable: true,
				resizable: false,
				align: 'center'
			},
			{
				header: 'Fecha de<br>Vencimiento<br>del Descuento',
				tooltip: 'Fecha de Vencimiento del Descuento',
				width: 90,
				dataIndex: 'FECHADSCTO',
				sortable: true,
				resizable: false,
				align: 'center'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				width: 120,
				dataIndex: 'NOMBREMONEDA',
				sortable: true,
				resizable: false,
				align: 'left'
			},
			{
				header: 'Tipo de Cr�dito',
				tooltip: 'Tipo de Cr�dito',
				width: 120,
				dataIndex: 'DESCTIPOCRED',
				sortable: true,
				resizable: false,
				align: 'left'
			},
			{
				header: 'Monto del<br>Documento',
				tooltip: 'Monto del Documento',
				width: 80,
				dataIndex: 'IMPORTEDOCTO',
				sortable: true,
				resizable: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')	
			},
			{
				header: 'Monto a<br>Descontar',
				tooltip: 'Monto a Descontar',
				width: 80,
				dataIndex: 'IMPORTEDSCTO',
				sortable: true,
				resizable: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')	
			},
			{
				header: 'Folio',
				tooltip: 'Folio',
				width: 80,
				dataIndex: 'FOLIO',
				sortable: true,
				resizable: false,
				align: 'center'
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				width: 80,
				dataIndex: 'ESTATUSSOLIC',
				sortable: true,
				resizable: false,
				align: 'left'
			},
			{
				header: 'N�mero de<br>Prestamo',
				tooltip: 'N�mero de Prestamo',
				width: 100,
				dataIndex: 'NUMPRESTAMO',
				sortable: true,
				resizable: false,
				align: 'center',
				renderer: function(val, meta, record, rowIndex, colIndex, store) {
					if(val == '0')
						return ''
					else
						return val
				}
			}			
		],
		bbar: {
			items: [
				'->',
				{
					xtype: 'label',
					id: 'lblProceso',
					name: 'lblProceso',
					fieldlabel: 'Proceso',
					text: ''
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFProceso',
					iconCls: 'icoPdf',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15credop01ext.data.jsp',
							params:{
								informacion: 'generarPdfProceso'
							},
							callback: procesarSuccessFailurePdfProceso
						});					
					}				
				}
			]
		}
	});
	
	
	//Grid para registros con estatus operadas
	var gridOperadas = new Ext.grid.GridPanel({
		id: 'gridOperadas',
		store: consultaDataOperadas,
		hidden: false,
		margins: '20 0 0 0',
		align: 'center',
		frame: true,
		height: 300,
		width: 850,
		style: 'margin:0 auto',
		title: 'Estatus:	Operadas',
		columns:[
			{
				header: 'Nombre del<br>Cliente',
				tooltip: 'Nombre del Cliente',
				width: 80,
				dataIndex: 'NOMBREPYME',
				sortable: true,
				resizable: false,
				align: 'left'
			},
			{
				header: 'N�mero de<br>Sirac',
				tooltip: 'N�mero de Sirac',
				width: 80,
				dataIndex: 'CLAVESIRAC',
				sortable: true,
				resizable: false,
				align: 'center'
			},
			{
				header: 'N�mero de<br>Documento',
				tooltip: 'N�mero de Documento',
				width: 80,
				dataIndex: 'NUMDOCTO',
				sortable: true,
				resizable: false,
				align: 'center'
			},
			{
				header: 'Fecha de<br>Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				width: 80,
				dataIndex: 'FECHAETC',
				sortable: true,
				resizable: false,
				align: 'center'
			},
			{
				header: 'Fecha de<br>Vencimiento<br>del Descuento',
				tooltip: 'Fecha de Vencimiento del Descuento',
				width: 90,
				dataIndex: 'FECHADSCTO',
				sortable: true,
				resizable: false,
				align: 'center'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				width: 120,
				dataIndex: 'NOMBREMONEDA',
				sortable: true,
				resizable: false,
				align: 'left'
			},
			{
				header: 'Tipo de Cr�dito',
				tooltip: 'Tipo de Cr�dito',
				width: 120,
				dataIndex: 'DESCTIPOCRED',
				sortable: true,
				resizable: false,
				align: 'left'
			},
			{
				header: 'Monto del<br>Documento',
				tooltip: 'Monto del Documento',
				width: 80,
				dataIndex: 'IMPORTEDOCTO',
				sortable: true,
				resizable: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')	
			},
			{
				header: 'Monto a<br>Descontar',
				tooltip: 'Monto a Descontar',
				width: 80,
				dataIndex: 'IMPORTEDSCTO',
				sortable: true,
				resizable: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')	
			},
			{
				header: 'Folio',
				tooltip: 'Folio',
				width: 80,
				dataIndex: 'FOLIO',
				sortable: true,
				resizable: false,
				align: 'center'
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				width: 80,
				dataIndex: 'ESTATUSSOLIC',
				sortable: true,
				resizable: false,
				align: 'left'
			},
			{
				header: 'N�mero de<br>Prestamo',
				tooltip: 'N�mero de Prestamo',
				width: 100,
				dataIndex: 'NUMPRESTAMO',
				sortable: true,
				resizable: false,
				align: 'center',
				renderer: function(val, meta, record, rowIndex, colIndex, store) {
					if(val == '0')
						return ''
					else
						return val
				}
			}			
		],
		bbar: {
			items: [
				'->',
				{
					xtype: 'label',
					id: 'lblOperadas',
					fieldlabel: 'Operadas',
					text: ''
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFOperadas',
					iconCls: 'icoPdf',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15credop01ext.data.jsp',
							params:{
								informacion: 'generarPdfOperadas'
							},
							callback: procesarSuccessFailurePdfOperadas
						});
					}
				}
			]
		}
	});

//-------------------------COMPONENTE PRINCIPAL------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipall',
		applyTo: 'areaContenido',
		width: 950,
		align: 'center',
		items: [
			fp,
			NE.util.getEspaciador(5),
			gridProceso,
			NE.util.getEspaciador(5),
			gridOperadas
		]
	});
	
	//Funcion para obtener usuario y fecha mediante peticion Ajax
	var valIniciales = function(){
		Ext.Ajax.request({
			url: '15credop01ext.data.jsp',
			params: {
						informacion: "valoresIniciales"
			},
			callback: procesaValoresIniciales
		});
	}
	
	valIniciales();
	consultaDataProceso.load();
	consultaDataOperadas.load();
});