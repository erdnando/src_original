<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.sql.*,		
		java.util.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*, 	
		com.netro.descuento.*, 
		netropology.utilerias.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"				
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String fechaHoy		= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String ic_if = (request.getParameter("ic_if") != null)?request.getParameter("ic_if"):"";
	String txtfolio = (request.getParameter("txtfolio") != null)?request.getParameter("txtfolio"):"";
	String txtfechaIni = (request.getParameter("txtfechaIni") != null)?request.getParameter("txtfechaIni"):fechaHoy;
	String txtfechaFin = (request.getParameter("txtfechaFin") != null)?request.getParameter("txtfechaFin"):fechaHoy;
	String tipoA = (request.getParameter("tipoA") != null)?request.getParameter("tipoA"):"";
	String accionBoton = (request.getParameter("accionBoton") != null)?request.getParameter("accionBoton"):"";
	String ic_moneda = (request.getParameter("ic_moneda") != null)?request.getParameter("ic_moneda"):"";
	String lsFechaOperacion = (request.getParameter("lsFechaOperacion") != null)?request.getParameter("lsFechaOperacion"):"";
	String lsNomMoneda = (request.getParameter("lsNomMoneda") != null)?request.getParameter("lsNomMoneda"):"";
	String lsImporteTotalO = (request.getParameter("lsImporteTotalO") != null)?request.getParameter("lsImporteTotalO"):"";
	String lsNombreIF = (request.getParameter("lsNombreIF") != null)?request.getParameter("lsNombreIF"):"";
	String fecha = (request.getParameter("fecha") != null)?request.getParameter("fecha"):"";


				
	String  infoRegresar ="", consulta ="", nombreArchivo ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	ConsDepositoCred paginador  =   new ConsDepositoCred();
	
if(informacion.equals("valoresIniciales")) {

	jsonObj.put("success",  new Boolean(true));
	jsonObj.put("fechaHoy", fechaHoy);			
	infoRegresar = jsonObj.toString();
	
}else  if(informacion.equals("catalogoIF")) {
	List catIF = paginador.getCatalogoIF();
	jsonObj.put("registros", catIF);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();

}else if(informacion.equals("Consultar")  ||  informacion.equals("Archivos")  )  {

	paginador.setIc_if(ic_if);
	paginador.setTxtfolio(txtfolio);
	paginador.setTxtfechaIni(txtfechaIni);
	paginador.setTxtfechaFin(txtfechaFin);
	paginador.setAccionBoton(accionBoton);
	paginador.setIc_moneda(ic_moneda);
	paginador.setLsFechaOperacion(lsFechaOperacion);
	paginador.setLsNomMoneda(lsNomMoneda);
	paginador.setLsImporteTotalO(lsImporteTotalO);
	paginador.setLsNombreIF(lsNombreIF);
	
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	
	if (informacion.equals("Consultar") ){ 
	
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}	catch(Exception e) {
			throw new AppException("Error en la consulta", e);
		}
		jsonObj.put("success", new Boolean(true));
		jsonObj = JSONObject.fromObject(consulta);	
	
	}else if (informacion.equals("Archivos") ){ 
	
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoA);		
		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		}
	}	
	infoRegresar = jsonObj.toString();	
}else if(informacion.equals("ConsultaOperacion")  )  {
	
	consulta = paginador.getConsOperaciones( ic_if, txtfolio, txtfechaIni,  txtfechaFin );
	
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);		
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("ConsNacional")  ||  informacion.equals("ConsDolar") ){ 

	List datos =  paginador.consConfirmacion(txtfechaIni , txtfechaFin, ic_moneda);
	consulta= datos.get(0).toString();	
	String numRegistrosMN= datos.get(1).toString();	
	String numRegistrosDL= datos.get(2).toString();	
		
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("numRegistrosMN",numRegistrosMN);
	jsonObj.put("numRegistrosDL",numRegistrosDL);
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("AceptarConfirmacion") ){ 

	String ic_ifMN[] = request.getParameterValues("ic_ifMN");
	String ic_ifDL[] = request.getParameterValues("ic_ifDL");
	String confirmaOperacionMN = (request.getParameter("confirmaOperacionMN") != null)?request.getParameter("confirmaOperacionMN"):"";
	String confirmaOperacionDL = (request.getParameter("confirmaOperacionDL") != null)?request.getParameter("confirmaOperacionDL"):"";
	String numRegMN = (request.getParameter("numRegMN") != null)?request.getParameter("numRegMN"):"";
	String numRegDL = (request.getParameter("numRegDL") != null)?request.getParameter("numRegDL"):"";
	
	mensaje = paginador.AceptaConfirmacion(txtfechaIni , ic_ifMN,  ic_ifDL ,numRegMN, numRegDL,  confirmaOperacionMN,  confirmaOperacionDL );
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje);
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("Concentrado_dia") ){

	try {
		nombreArchivo = paginador.generarArchivo(fecha, strDirectorioTemp);										
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo Concentrado del dia", e);
	}
	
	if(nombreArchivo.equals(""))  { mensaje="No se Encontró Ningún Registro";  }
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);			
	infoRegresar = jsonObj.toString();	
	

}else if (informacion.equals("Total_dia") ){

	try {
		nombreArchivo = paginador.generarArchivo(txtfechaIni, txtfechaFin , strDirectorioTemp);										
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo Total del día", e);
	}
	
	System.out.println("nombreArchivo "+nombreArchivo);
	
	if(nombreArchivo.equals(""))  { mensaje="No se Encontró Ningún Registro";  }
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", mensaje);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();	
}

%>
<%=infoRegresar%>