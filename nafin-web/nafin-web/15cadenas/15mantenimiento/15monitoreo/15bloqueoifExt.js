Ext.onReady(function(){
	var numFocus = 0;
//-------------Handlers-------------
	
	var banderaInicio=true;
	
	var procesarSelIF = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar IF", 
		loadMsg: ""})); 
		store.commitChanges(); 

		Ext.getCmp('idComboIF').setValue('');
	}
	
	
	var procesarSelProductos= function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar producto", 
		loadMsg: ""})); 
		store.commitChanges(); 

		Ext.getCmp('idComboProducto').setValue("0");
;}
	
	var procesarCausaVacia = function (){
		var grid = Ext.getCmp('grid');
		var columnModelGrid = grid.getColumnModel();
		var store = grid.getStore();
		var errorValidacion = false;
		var b = "";
		
		store.each(function(record) {	
			numRegistro = store.indexOf(record);
			//alert(record.data['estatusBloqueo']);
			//if(record.data['estatusBloqueo'] != 'B' && record.data['estatusBloqueoActual'] == 'B'){
			//if(record.data['estatusBloqueoActual'] == 'B' || (record.data['estatusBloqueo'] == 'B' && record.data['estatusBloqueoActual'] == '')){	
			//if(record.data['estatusBloqueoActual'] == 'B'){	
			if((record.data['estatusBloqueoActual'] == 'B' && record.data['estatusBloqueo'] != 'B') || (record.data['estatusBloqueo'] == 'B' && record.data['estatusBloqueoActual'] == '' )){
				if((record.data['estatusBloqueo'] == 'B' && record.data['estatusBloqueoActual'] == '' ))
					b = "desbloqueo";
				else
					b = "bloqueo";
				//alert("causa"+record.data['causaBloqueo']+"|"+numRegistro);
				if(record.data['causaBloqueo'] == ''){	
					errorValidacion = true;					
					Ext.MessageBox.alert('Error de validaci�n',' Favor de escribir la causa de '+b,
						function(){
							//Ext.getCmp('btnConfirmar').enable();
							grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('causaBloqueo'));
						}
					);
					return false;
				}
			}		   
			//numDatos++;
		});
		
		if (errorValidacion) {
			return;
		}
		
	}	
	var procesarConfirmar = function(){
		var grid = Ext.getCmp('grid');
		var columnModelGrid = grid.getColumnModel();
		var store = grid.getStore();
		//var errorValidacion = false;
		var arrDato1= new Array();
		var arrDato2= new Array();
		var arrDato3= new Array();
		var arrDato4= new Array();
		var arrDato5= new Array();
		var numRegistro = -1;
		var numDatos=0;
		/*
		store.each(function(record) {	
			numRegistro = store.indexOf(record);
			//if(record.data['estatusBloqueo'] != 'B' && record.data['estatusBloqueoActual'] == 'B'){
			//if(record.data['estatusBloqueoActual'] == 'B'){	
			if(record.data['estatusBloqueo'] != 'B' && record.data['estatusBloqueoActual'] == 'B'){
				if(record.data['causaBloqueo'] == ''){	
					errorValidacion = true;					
					Ext.MessageBox.alert('Error de validaci�n',' Favor de escribir la causa ',
						function(){
							Ext.getCmp('btnConfirmar').enable();
							grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('causaBloqueo'));
						}
					);
					return false;
				}else{
					arrDato1.push(record.data['claveIf']);
					arrDato2.push(record.data['claveProducto']);
					arrDato3.push(record.data['estatusBloqueo']);
					arrDato4.push(record.data['estatusBloqueoActual']);
					arrDato5.push(record.data['causaBloqueo']);
				}
			}
			else{*/
				store.each(function(record) {
					arrDato1.push(record.data['claveIf']);
					arrDato2.push(record.data['claveProducto']);
					arrDato3.push(record.data['estatusBloqueo']);
					arrDato4.push(record.data['estatusBloqueoActual']);
					arrDato5.push(record.data['causaBloqueo']);
			//}
		   
			numDatos++;
		});		
		

		//if (errorValidacion) {
		//	return;
		//}
		
		consultaConf.load({
		params: {clavesIF: arrDato1,
					clavesProductos: arrDato2,
					estatusBloqueos: arrDato3,
					estatusActuales: arrDato4,
					causasBloqueo: arrDato5,
					tam: numDatos	
					}
		});
		
		Ext.getCmp('grid').hide();
		Ext.getCmp('fpBotones').hide();
		Ext.getCmp('formaDetalle').hide();
		Ext.getCmp('grid2').show();
		
	}
	
	

	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');	
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				banderaInicio=false;
				el.unmask();
				Ext.getCmp('btnConfirmar').enable();
				Ext.getCmp('btnCancelar').enable();
				Ext.getCmp('btnBuscar').enable();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnConfirmar').disable();
				Ext.getCmp('btnCancelar').disable();
				Ext.getCmp('btnBuscar').enable();
			}
		}
	}
		

	var procesarResultados = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid2');	
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
				grid.setTitle('');
				el.mask('No hubo ning�n cambio a realizar', 'x-mask');
			}
		}
	}
	

//-------------Stores----------------

	var Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
	
	
	var CatalogoIF = new Ext.data.JsonStore
	  ({
			id: 'catIf',
			root : 'registros',
			fields : ['clave', 'descripcion', 'loadMsg'],
			url : '15bloqueoifExt.data.jsp',
			baseParams: 
			{
			 informacion: 'CatalogoIF'
			},
			autoLoad: true,
			listeners:
			{
			 load: procesarSelIF,
			 exception: NE.util.mostrarDataProxyError,
			 beforeload: NE.util.initMensajeCargaCombo
			}
	  });
	
	
	var CatalogoProducto= new Ext.data.JsonStore
	({
			id: 'catProducto',
			root : 'registros',
			fields : ['clave', 'descripcion', 'loadMsg'],
			url : '15bloqueoifExt.data.jsp',
			baseParams: 
			{
			 informacion: 'CatalogoProducto'
			},
			autoLoad: true,
			listeners:
			{
			 load: procesarSelProductos,
			 exception: NE.util.mostrarDataProxyError,
			 beforeload: NE.util.initMensajeCargaCombo
			}
	  });  
	
	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15bloqueoifExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'claveProducto'},
			{name: 'nombreProducto'},
			{name: 'claveIf'},
			{name: 'nombreIf'},
			{name: 'nafinElectronico'},
			{name: 'fechaModificacion'},
			{name: 'causaBloqueo'},
			{name: 'estatusBloqueo'},
			{name: 'estatusBloqueoActual'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	
	
	var consultaConf = new Ext.data.JsonStore({
		root : 'registros',
		url : '15bloqueoifExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaConfirma'
		},
		fields: [		
			{name: 'nombreProducto'},
			{name: 'nafinElectronico'},
			{name: 'nombreIf'},
			{name: 'estatusBloqueo'},
			{name: 'fechaModificacion'},
			{name: 'causaBloqueo'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarResultados,

			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	
	/**********CRITERIOS DE BUSQUEDA ***************/		

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: '<b>Captura</b>',			
				id: 'btnCaptura',					
				handler: function() {
					window.location = '15bloqueoifExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Administraci�n de Operaciones',			
				id: 'btnConsulta',					
				handler: function() {
					window.location = '15bloqueoifbext.jsp';
				}
			}	
		]
	});
	
//----------------COMPONENTES-------------------
	
	
	var sm = new Ext.grid.CheckboxSelectionModel({
		header: 'Bloq',
		checkOnly: true,
		id:'chk',
		tooltip: 'Bloqueada',
		width: 34,
		singleSelect:false,
		listeners:{
			rowselect: function(sm,rowIndex,record){
								record.data['estatusBloqueoActual']= 'B';
								//record.commit();
								var  gridConsulta = Ext.getCmp('grid');
								var store = gridConsulta.getStore();
								var columnModelGrid = gridConsulta.getColumnModel();
								
				
								if(record.data['estatusBloqueo'] != 'B'){
									record.data['causaBloqueo'] = '';
									Ext.MessageBox.alert("Alerta","Favor de escribir la causa de bloqueo", function(){
										gridConsulta.startEditing(rowIndex, columnModelGrid.findColumnIndex('causaBloqueo'));		
										} );
								}
							},
				
			rowdeselect: function(sm,rowIndex,record){
								record.data['estatusBloqueoActual']= '';
								//record.commit();
				
								var  gridConsulta = Ext.getCmp('grid');
								var store = gridConsulta.getStore();
								var columnModelGrid = gridConsulta.getColumnModel();	
								if(record.data['estatusBloqueo']== 'B')
								{
									record.data['causaBloqueo'] = '';								
									Ext.MessageBox.alert("Alerta","Favor de escribir la causa de desbloqueo", function(){
											gridConsulta.startEditing(rowIndex, columnModelGrid.findColumnIndex('causaBloqueo'));		
									});
								}
							}
		},
		renderer: function(value, metadata, record, rowindex, colindex, store) {
			if (record.data['estatusBloqueo']!= 'B'){
				return '<div class="x-grid3-row-checker" >&#160</div>';
			}else{
				if(banderaInicio)
					grid.getSelectionModel().selectRow(rowindex,true);
					return '<div class="x-grid3-row-checker" >&#160</div>';
				}  
			}
	});
	
		
	var elementosForma = 
	[	
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			forceSelection: true,
			autoSelect: true,
			name:'claveProducto',
			id:'idComboProducto',
			displayField: 'descripcion',
			allowBlank: true,
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			autoLoad: false,
			typeAhead: true,
			minChars: 1,
			store: CatalogoProducto,  
			width:120
		},{
			xtype: 'combo',
			fieldLabel: 'IF',
			forceSelection: true,
			autoSelect: true,
			name:'claveIF',
			id:'idComboIF',
			displayField: 'descripcion',
			allowBlank: true,
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: CatalogoIF,  
			width:235
		}
	]
	
	
	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
		buttons:[{
						xtype:	'button',
						id:		'btnBuscar',
						text:		'Buscar',
						iconCls: 'icoBuscar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
										if (Ext.getCmp('idComboProducto').getValue() == "") {
											//Ext.MessageBox.alert("Alerta","Por favor seleccione un producto");
											Ext.getCmp('idComboProducto').markInvalid('Por favor seleccione un producto');
										}else {
											boton.disable();
											banderaInicio=true;
											Ext.getCmp('grid2').hide();
											var params = (fp)?fp.getForm().getValues():{};
											consultaDataGrid.load({
											params: {ComboProducto:Ext.getCmp('idComboProducto').getValue(),
														ComboIF:Ext.getCmp('idComboIF').getValue()	}
											});
											grid.show();					
										}
									},
						style: 'margin:0 auto;',
						align:'rigth'
					},{
						xtype:	'button',
						id:		'btnCancel',
						text:		'Limpiar',
						iconCls: 'icoLimpiar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							window.location = '15bloqueoifExt.jsp';
						},
						style: 'margin:0 auto;'
					}],
		style: 'margin:0 auto;',	   
		height: 120,
		width: 570
	});
	
	
	var grid2 = new Ext.grid.GridPanel({
		store: consultaConf,
		sortable: true,
		title: '<center><b>Los cambios se realizaron con �xito</b><br>A continuaci�n se muestran los cambios realizados</center>', 
		id:'grid2',
		hidden: true,
		columns: [
			{
				header: '<center>Producto</center>',
				tooltip: 'Producto',
				dataIndex: 'nombreProducto',
				width: 150,
				align: 'center'				
			},{
				header: 'N@E',
				tooltip: 'N@E',
				dataIndex: 'nafinElectronico',
				align: 'center',
				width: 100
			}, {
				header: '<center>IF</center>',
				dataIndex: 'nombreIf',
								renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								return value;
							},
				align: 'left',
				width: 159
			}, {
				header: '<center>Bloqueo</center>',
				tooltip: 'Bloqueo',
				dataIndex: 'estatusBloqueo',
				align: 'center',
				width: 119
			}, {
				header: 'Fecha y Hora Bloq./ Desbloq.',
				tooltip: 'Fecha y Hora Bloq./ Desbloq.',
				dataIndex: 'fechaModificacion',
				align: 'center',
				width: 129
			}, {
				header: '<center>Causa</center>',
				tooltip: 'Causa',
				dataIndex: 'causaBloqueo',
				align: 'left',
				width: 240	
			}
		],
		loadMask: true,
				bbar: {
					autoScroll:true,
					id: 'barra2',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnRegresar',
						text:		'Regresar',
						iconCls: 'icoContinuar',
						width: 50,
						weight: 70,
						style: 'margin:0 auto;',
						handler: function (boton,evento){
							window.location = '15bloqueoifExt.jsp'
						},
						align:'rigth'
					}]
				},
		style: 'margin:0 auto;',
		frame: true,
		height: 240,
      width: 930	
	});
	
	
	var gridView = new Ext.grid.GridView({  
		getRowClass : function (row, index) { 
			var cls =''; 
			var data = row.data.estatusBloqueo; 
			if(data == 'B'){
				return cls = 'redrow';
			}
		}
	});
	
	
	var grid = new Ext.grid.EditorGridPanel({
		store: consultaDataGrid,
		sortable: true,
		clicksToEdit:1,
		view: gridView,
		hidden:true,
		id: 'grid',
		sm:sm,
		columns: [
			{
				header: '<center>Producto</center>',
				tooltip: 'Producto',
				dataIndex: 'nombreProducto',
				width: 150,
				align: 'center'				
			},{
				header: 'N@E',
				tooltip: 'N@E',
				dataIndex: 'nafinElectronico',
				align: 'center',
				width: 100
			}, {
				header: '<center>IF</center>',
				tooltip: 'IF',
				dataIndex: 'nombreIf',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								return value;
							},
				align: 'left',
				width: 189			
			},sm,{
				header: 'Fecha y Hora Bloq. / Desbloq',
				tooltip: 'Fecha y Hora Bloq./ Desbloq.',
				dataIndex: 'fechaModificacion',
				align: 'center',
				width: 139
			},{
				header: '<center>Causa</center>',
				tooltip: 'Causa',
				dataIndex: 'causaBloqueo',
				align: 'left',
				editor: { 
								xtype:'textfield',
								height: 10,
								id:'txtCausa',							
								listeners:{
									blur: function(field){
										//numFocus ++;
										if(field.getValue() == "" ){	
											procesarCausaVacia();
										}
									}
								}				
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
				metadata.attr='style="border: thin solid #3399CC;  color: black;"';
                   return value;
				},
				width: 260
			}
		],
		//listeners:{
		//	bodyscroll: function(){
		//	}
		//}, 

		loadMask: true,
		style: 'margin:0 auto;',
		height: 300,
			
		bbar: {
					autoScroll:true,
					id: 'barra',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnConfirmar',
						text:		'Confirmar',
						iconCls: 'icoAceptar',
						width: 50,
						weight: 70,
						style: 'margin:0 auto;',
						handler: function (boton,evento){
								
							Ext.Msg.confirm('Mensaje', 
											'�Est� seguro de querer enviar su informaci�n?',
											function(botonConf) {
												if (botonConf == 'ok' || botonConf == 'yes' || botonConf == 'si' ) {
																procesarConfirmar();
																boton.disable();
												}
											}
										);			
						},
							align:'rigth'
					},
					{
						xtype:	'button',
						id:		'btnCancelar',
						text:		'Cancelar',
						iconCls: 'icoRechazar',
						width: 50,
						handler: function(boton, evento) {
							window.location = '15bloqueoifExt.jsp';
							//grid.hide();
							//Ext.getCmp('idComboProducto').setValue(0);
							//Ext.getCmp('idComboIF').setValue('');
						},						
						weight: 70,
						style: 'margin:0 auto;',
						align:'rigth'
					}
					]
					},
					width: 910,
					frame: true
			});
	


//------ Componente Principal -------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [fpBotones,NE.util.getEspaciador(10),
				  fp,NE.util.getEspaciador(10),grid,
				  NE.util.getEspaciador(10),grid2 ]
	});

});