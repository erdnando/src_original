<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.procesos.*,
		com.netro.parametrosgrales.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	if (informacion.equals("CatalogoIF")){
		
		CatalogoIFBloqueo cat = new CatalogoIFBloqueo();
		cat.setClave("ic_if");
		cat.setDescripcion("cg_razon_social");
		infoRegresar=cat.getJSONElementos();
		
	}else if(informacion.equals("CatalogoProducto")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("COMCAT_PRODUCTO_NAFIN");
		cat.setCampoClave("ic_producto_nafin");
		cat.setCampoDescripcion("ic_nombre");
		
		List lis= new ArrayList ();
		lis.add("0");
		
		cat.setValoresCondicionIn(lis);
			
		infoRegresar=cat.getJSONElementos(); 
	
	}else if(informacion.equals("ConsultaGrid")){
		
		HashMap datos = new HashMap();
		JSONObject jsonObj = new JSONObject();	
		JSONArray registros = new JSONArray();
		
		String ic_producto_nafin		  = (request.getParameter("ComboProducto")==null)?"":request.getParameter("ComboProducto");
		String ic_if	  = (request.getParameter("ComboIF")==null)?"":request.getParameter("ComboIF");
		
		List regis = new ArrayList();

		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
		
		regis =(ArrayList)BeanParametrosGrales.getCamposBloqueoIF(ic_producto_nafin,ic_if); 
		
		 registros = JSONArray.fromObject(regis);

		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
			
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("ConsultaConfirma")){
		
		String DatoIF[]	= request.getParameterValues("clavesIF");
		String DatoProducto[]	= request.getParameterValues("clavesProductos");
		String DatoEstatusAnterior[]	= request.getParameterValues("estatusBloqueos");
		String DatoEstatusActual[]	= request.getParameterValues("estatusActuales");
		String DatoCausaBloqueo[]	= request.getParameterValues("causasBloqueo");
		String numVariables = (request.getParameter("tam") != null)?request.getParameter("tam"):"";
	
		int num= Integer.parseInt(numVariables);

		JSONObject jsonObj = new JSONObject();	
		JSONArray registros = new JSONArray();

		List regis = new ArrayList();
			
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
		
		regis =(ArrayList)BeanParametrosGrales.getCamposBloqueoIfModificaciones(DatoIF,DatoProducto,DatoEstatusAnterior,
		DatoEstatusActual,DatoCausaBloqueo,iNoUsuario, numVariables); 
		
		registros = JSONArray.fromObject(regis);
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
			
		infoRegresar = jsonObj.toString();
	}
%>
<%= infoRegresar %>