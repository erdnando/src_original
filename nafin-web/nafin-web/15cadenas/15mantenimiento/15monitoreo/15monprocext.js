//alert('entroMonitor');
Ext.onReady(function() {

	var ic_producto =  Ext.getDom('ic_producto').value;
	var tipoProceso =  Ext.getDom('tipoProceso').value;
	var monitor =  Ext.getDom('monitor').value; 
	var tituloTipoProceso =  Ext.getDom('tituloTipoProceso').value; 
	
	var noSolicitud = [];
	var producto = [];
	var tabla_Origen = [];
	var error_proceso = [];
	
	var cancelar =  function() { 
		noSolicitud = [];
		producto = [];
		tabla_Origen = [];
		error_proceso = [];
	}
    
	//Funci�n para deshabilitar o habilitar columnas 
    function findColumnByDataIndex(grid, dataIndex) {
        var selector = "gridcolumn[dataIndex=" + dataIndex + "]";
        return grid.down(selector);
    };
    
    //***************** Vista Previa Correo *************************++
	var procesarEnvioCorreo = function(options, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			
			var  jsonData = Ext.JSON.decode(response.responseText);
			
			var button = 	Ext.ComponentQuery.query('#botonEnviarCorreo')[0];
			button.enable();
			button.setIconCls('icoBotonEnviarCorreo'); 
					
			if(jsonData.respuesta!='Fallo') {
			
				Ext.MessageBox.alert('Mensaje',jsonData.respuesta,
					function(){ 					
						//window.location = '15monprocext.jsp'; 
						Ext.ComponentQuery.query('#VentanaCorreo')[0].destroy(); 
				}); 	  
				
			}									
						
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	
	var procesarDatosCorreo = function(options, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			
			var  jsonData = Ext.JSON.decode(response.responseText);
					
			Ext.ComponentQuery.query('#remitente')[0].setValue(jsonData.remitente);			
			Ext.ComponentQuery.query('#destinatarios')[0].setValue(jsonData.destinatarios);
			Ext.ComponentQuery.query('#ccDestinatarios')[0].setValue(jsonData.ccDestinatarios); 
			Ext.ComponentQuery.query('#asunto')[0].setValue(jsonData.asunto);
			Ext.ComponentQuery.query('#cuerpoCorreo')[0].setValue(jsonData.cuerpoCorreo);  
			Ext.ComponentQuery.query('#urlArchivo')[0].setValue(jsonData.urlArchivo);					
			Ext.ComponentQuery.query('#vistaArchivosAdjuntos')[0].setValue(jsonData.vistaArchivosAdjuntos);		
			Ext.ComponentQuery.query('#vistaArchivosAdjuntos')[0].show();	
									
						
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	 function FormaCorreo(readOnly ){
   	
   		this.xtype 									= 'mailform';
			this.title 									= '';
			this.hideCcDestinatarios 				= false;
			this.editMode 								= true;
			this.itemId 								= 'formaCorreo';
			this.hideArchivoAdjunto					= true;
			this.hideVistaArchivosAdjuntos   	= false;
			this.url 									= '15monproc.data.jsp';	
			this.height									= 500;
			this.destinatariosCfg					= {
				maxLength: 	1000
			};
			this.ccDestinatariosCfg 				= {
				allowBlank: true,
				maxLength: 	1000
			};
			this.asuntoCfg								= {
				allowBlank: false,
				maxLength: 	250
			};				
			this.cuerpoCorreoCfg						= {
				allowBlank: false,
				maxLength: 	4000
			};
			this.hideBotonEnviarCorreo 			=  readOnly;
			this.botonEnviarCorreoCfg 				= {
				handler:	function(button){				
				
					if (  Ext.isEmpty(Ext.ComponentQuery.query('#destinatarios')[0].getValue()) ) {  
						Ext.ComponentQuery.query('#destinatarios')[0].markInvalid('Este campo es obligatorio');
						return;
					}
			
					if (  Ext.isEmpty(Ext.ComponentQuery.query('#asunto')[0].getValue()) ) {  
						Ext.ComponentQuery.query('#asunto')[0].markInvalid('Este campo es obligatorio');
						return;
					}					
					
					if (  Ext.isEmpty(Ext.ComponentQuery.query('#cuerpoCorreo')[0].getValue()) ||  Ext.ComponentQuery.query('#cuerpoCorreo')[0].getValue()=='<p>&nbsp;</p>' ) {  
						Ext.ComponentQuery.query('#cuerpoCorreo')[0].markInvalid('Este campo es obligatorio'); 
						return;
					}
													
					var button = Ext.ComponentQuery.query('#botonEnviarCorreo')[0];					
					button.disable();
					button.setIconCls('x-mask-msg-text');		
										
					Ext.Ajax.request({
						url: '15monproc.data.jsp',
						params: {							
							informacion: 'EnviodeCorreo',
							remitente: Ext.ComponentQuery.query('#remitente')[0].getValue(),
							destinatario: Ext.ComponentQuery.query('#destinatarios')[0].getValue(),
							ccdestinatario: Ext.ComponentQuery.query('#ccDestinatarios')[0].getValue(),
							asunto: Ext.ComponentQuery.query('#asunto')[0].getValue(),
							cuerpoCorreo: Ext.ComponentQuery.query('#cuerpoCorreo')[0].getValue(),
							urlArchivo: Ext.ComponentQuery.query('#urlArchivo')[0].getValue()
							
						}
						,callback: procesarEnvioCorreo
					});
				}
			},
			this.items = [];			
		}
		
	var vistaPreviaCorreoResumen = function( ) {
		var forma= Ext.ComponentQuery.query('#forma')[0]; 
		
		var  tipoRadio01 =  Ext.ComponentQuery.query('#chckFueraFacultades01')[0].getValue();
		var chckFueraFacultades ="N";
		if(tipoRadio01==true){
			chckFueraFacultades= "S";
		}
		
		var ic_solicitudes='';
		var  gridDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
		var store = gridDetalle.getStore();		

		var selectionModel = gridDetalle.getSelectionModel();
        var collection = selectionModel.selected;
        for(var index =0 ;index<collection.length;index++){ 
            var record = collection.get(index); 
            ic_solicitudes +=record.data['SOLICITUD']+",";  
        }
        //IHJ
		//console.log("ic_solicitudes:"+ic_solicitudes);
        /*store.each(function(record) {				
			if(record.data['SELECCION']=='S' ){					
				ic_solicitudes +=record.data['SOLICITUD']+",";  
			}
		});	*/
			
		var titulo = "Vista Previa Correo - Rechazados  "+forma.query('#tituloTipoProceso')[0].getValue();	
		
		new Ext.Window({										
				modal: true,
				width: 610,
				//height: 630,	
				resizable: false,
				closable:true,
				id: 'VentanaCorreo',
				autoDestroy:true,
				closeAction: 'destroy',				
				items: [										
					new FormaCorreo(false)
				]				
			}).show().setTitle(titulo); 
			
			Ext.Ajax.request({
				url: '15monproc.data.jsp',
				params: {							
					informacion: 'DatosCorreo',
					chckFueraFacultades:chckFueraFacultades,
					pantalla: forma.query('#pantalla')[0].getValue(),
					tipoProceso:forma.query('#tipoProceso')[0].getValue(),
					monitor:forma.query('#monitor')[0].getValue(),
					ic_solicitudes:ic_solicitudes 
								
				},
				callback: procesarDatosCorreo
			});			
	}
		
						
	var procesaValoresIniciales = function(options, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			
			var  info = Ext.JSON.decode(response.responseText);
			
			var forma= Ext.ComponentQuery.query('#forma')[0];
			forma.query('#pantalla')[0].setValue('Detalle_Registos');
			forma.query('#tipoProceso')[0].setValue(info.tipoProceso);			
			forma.query('#monitor')[0].setValue(info.monitor);
			forma.query('#tituloTipoProceso')[0].setValue(info.tituloTipoProceso);
			
			
			Ext.ComponentQuery.query('#ic_producto1')[0].show();
			Ext.ComponentQuery.query('#ic_if1')[0].show();
			Ext.ComponentQuery.query('#ic_nafin_electronico1')[0].show();
		
			if(info.tipoProceso=='Diversos' && info.monitor=='SI')  {
				Ext.ComponentQuery.query('#cmb_estatus1')[0].show();
				Ext.ComponentQuery.query('#chckFueraFacultades1')[0].show();
			}
							
			Ext.ComponentQuery.query('#btnConsultar')[0].show();
			
			forma.setTitle('<div><div style="float:center">Rechazados  '+info.tituloTipoProceso+'</div>');
		
			if(info.monitor=='SI')  {
				forma.el.mask('Enviando...', 'x-mask-loading');				
				consDetalleData.load({
					params: Ext.apply(forma.getForm().getValues(),{
						informacion: 'ConsultaDetalle',
						tipoProceso:info.tipoProceso,
						pantalla: forma.query('#pantalla')[0].getValue(),
						monitor:info.monitor
					})
				});		
			}
			
			
				catalogoProducto.load({
					params :Ext.apply(forma.getForm().getValues(),{
						pantalla: 'Detalle_Registos',
						tipoProceso:info.tipoProceso,
						ic_producto:ic_producto	
					})
				});
			
			if(ic_producto!="") {
				catalogoIF.load({
					params : Ext.apply(forma.getForm().getValues(),{
						ic_producto: info.ic_producto,
						tipoProceso:info.tipoProceso
					})
				});
				
				catalogoRechazo.load({
					params : Ext.apply(forma.getForm().getValues(),{
						ic_producto: info.ic_producto,
						tipoProceso:info.tipoProceso
					})
				});
			
			}
		
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	
	var procesarArchivos = function(options, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {		
			var  infoR = Ext.JSON.decode(response.responseText);
			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};			
			
			var fp    		= Ext.getDom('formAux');
			fp.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.method 		= 'post';
			fp.target 		= '_self'; 	
			fp.submit();					
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var transmiteReProceso = function(options, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {		
			var  info = Ext.JSON.decode(response.responseText);		
			
			Ext.ComponentQuery.query('#forma')[0].hide();
			Ext.ComponentQuery.query('#gridDetalle')[0].hide();
			Ext.ComponentQuery.query('#comboDescripcion')[0].hide();
			Ext.ComponentQuery.query('#mensajeA')[0].show();			
			Ext.ComponentQuery.query('#mensaje')[0].setValue(info.mensaje);
			//Ext.ComponentQuery.query('#fpBotones')[0].hide();	
								
			var gridProcesados =  Ext.ComponentQuery.query('#gridProcesados')[0];						
			Ext.ComponentQuery.query('#gridProcesados')[0].show();			
				
			gridProcesados.setTitle('<div><div style="float:left">Rechazados '+info.tituloTipoProceso+'</div>');			
			
			if(info.tipoProceso=='Proyectos') {			
				findColumnByDataIndex(	gridProcesados,'MODALIDAD').show();
				findColumnByDataIndex(	gridProcesados,'NUM_PROYECTO').show();
				findColumnByDataIndex(	gridProcesados,'NUM_CONTRATO').show();
				findColumnByDataIndex(	gridProcesados,'NUM_PRESTAMO').show();			
			
			}
			if(info.tipoProceso=='Diversos') {
				findColumnByDataIndex(	gridProcesados,'NUM_PROYECTO').show();	
				findColumnByDataIndex(	gridProcesados,'NUM_CONTRATO').show();	
				findColumnByDataIndex(	gridProcesados,'NUM_PRESTAMO').show();					
			}
			if(info.tipoProceso=='Contratos') {
				findColumnByDataIndex(	gridProcesados,'NUM_PROYECTO').hide();	
				findColumnByDataIndex(	gridProcesados,'MODALIDAD').show();	
				findColumnByDataIndex(	gridProcesados,'NUM_CONTRATO').show();	
				findColumnByDataIndex(	gridProcesados,'NUM_PRESTAMO').show();	
	
			}
			if(info.tipoProceso=='Disposiciones' ) {
				findColumnByDataIndex(	gridProcesados,'NUM_CONTRATO').hide();	
				findColumnByDataIndex(	gridProcesados,'NUM_PRESTAMO').hide();	
				findColumnByDataIndex(	gridProcesados,'NUM_PROYECTO').hide();	
				findColumnByDataIndex(	gridProcesados,'MODALIDAD').hide();	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//** para PREPROCESAR 
	var Reproceso = function() {
	
		var  gridDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
		var store = gridDetalle.getStore();		
		var noRegistros = 0;
		var contRegError =0;
		var regSeleccionados = [];	
		var cg_descripcion=  Ext.ComponentQuery.query('#cg_descripcion1')[0].getValue();
		var tipoProceso=  Ext.ComponentQuery.query('#tipoProceso')[0].getValue();
		var ic_producto1 = Ext.ComponentQuery.query('#ic_producto1')[0].getValue();
		
		var forma= Ext.ComponentQuery.query('#forma')[0];	
		var  tipoRadio01 =  Ext.ComponentQuery.query('#chckFueraFacultades01')[0].getValue();
		var chckFueraFacultades ="N";
		if(tipoRadio01==true){ chckFueraFacultades= "S";  }	

		cancelar();
		
		var selectionModel = gridDetalle.getSelectionModel();
        var collection = selectionModel.selected;
        for(var index=0;index<collection.length;index++) {
			var record = collection.get(index);
			noRegistros++;
			regSeleccionados.push(record);
			noSolicitud.push(record.data['SOLICITUD']);
			producto.push(record.data['IC_PRODUCTO']);
			tabla_Origen.push(record.data['TABLA_ORIGEN']);
			error_proceso.push(record.data['IC_ERROR_PROCESO']);			
			if(record.data['IC_ERROR_PROCESO']=='43'){
				contRegError++;
			}
		}
		//console.log("noRegistros:"+noRegistros);
		/*IHJ
		store.each(function(record) {
			if(record.data['SELECCION']=='S' ){	
				noRegistros++;
				regSeleccionados.push(record); 
				noSolicitud.push(record.data['SOLICITUD']);
				producto.push(record.data['IC_PRODUCTO']);
				tabla_Origen.push(record.data['TABLA_ORIGEN']);
				error_proceso.push(record.data['IC_ERROR_PROCESO']);			
				if(record.data['IC_ERROR_PROCESO']=='43'){
					contRegError++;
				}
			}
		});
		*/
			
		if(noRegistros ==0) {
			Ext.MessageBox.alert('Error de validaci�n','Es necesario seleccionar un registro para reprocesar o cancelar');
			return;		
		}
		if(tipoProceso=='Proyectos' &&  ic_producto1 == '4') {
			if(contRegError>0 && Ext.isEmpty(cg_descripcion) ) {
				Ext.MessageBox.alert('Error de validaci�n','Hay registros seleccionados con "Causa 43", por lo que es necesario especificar una descripci�n de la lista');
				return;	
			}
		}
		
		/*Petici�n de Irving - REPROCESAR
		if(noRegistros >1000) {
			Ext.MessageBox.alert('Error de validaci�n','No se pueden reprocesar m�s de 1000 registros');
			return;	
		}
		*/
		
		Ext.Msg.show({
			title:	"Confirmaci�n",
			msg:		'�Desea continuar con la operaci�n "REPROCESAR" ?',
			buttons:	Ext.Msg.OKCANCEL,
			fn: function resultMsj(btn){
				if(btn =='ok'){
				
					consProcesadosData.add(regSeleccionados);
				
					Ext.Ajax.request({
						url: '15monproc.data.jsp',
						params: Ext.apply(forma.getForm().getValues(),{
							informacion: 'procesar',	
							operacion:'REPROCESAR',
							descripcion:cg_descripcion,
							noSolicitud:noSolicitud,
							producto:producto,
							tabla_Origen:tabla_Origen,
							noRegistros:noRegistros,
							error_proceso:error_proceso,
							chckFueraFacultades:chckFueraFacultades,
							pantalla: forma.query('#pantalla')[0].getValue(),
							tipoProceso:forma.query('#tipoProceso')[0].getValue(),
							monitor:forma.query('#monitor')[0].getValue()
						})
						,callback: transmiteReProceso
					});
				}
			},
			closable:false,
			icon: Ext.MessageBox.QUESTION
		});				
	}
	
	
			//**  CANCELAR 
	var ProcesCancelar = function() {
		var  gridDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
		var store = gridDetalle.getStore();		
		var noRegistros = 0;
		var regSeleccionados = [];	
		var cg_descripcion= Ext.ComponentQuery.query('#cg_descripcion1')[0].getValue();		
		cancelar();
		var forma= Ext.ComponentQuery.query('#forma')[0];	
		var  tipoRadio01 =  Ext.ComponentQuery.query('#chckFueraFacultades01')[0].getValue();
		var chckFueraFacultades ="N";
		if(tipoRadio01==true){
			chckFueraFacultades= "S";
		}
		
		var selectionModel = gridDetalle.getSelectionModel();
        var collection = selectionModel.selected;
        for(var index=0;index<collection.length;index++) {
			var record = collection.get(index);
			noRegistros++;
			regSeleccionados.push(record); 
			noSolicitud.push(record.data['SOLICITUD']);
			producto.push(record.data['IC_PRODUCTO']);
			tabla_Origen.push(record.data['TABLA_ORIGEN']);
			error_proceso.push(record.data['IC_ERROR_PROCESO']);
		}
		//console.log("noRegistros:"+noRegistros);
		/*IHJ
		store.each(function(record) {			
			if(record.data['SELECCION']=='S' ){	
				noRegistros++;
				regSeleccionados.push(record); 
				noSolicitud.push(record.data['SOLICITUD']);
				producto.push(record.data['IC_PRODUCTO']);
				tabla_Origen.push(record.data['TABLA_ORIGEN']);
				error_proceso.push(record.data['IC_ERROR_PROCESO']);
		
			}
		});
		*/
					
		if(noRegistros ==0) {
			Ext.MessageBox.alert('Error de validaci�n','Es necesario seleccionar un registro para reprocesar o cancelar');
			return;		
		}
		
		/*Petici�n de Irving - CANCELAR
		if(noRegistros >1000) {
			Ext.MessageBox.alert('Error de validaci�n','No se pueden reprocesar m�s de 1000 registros');
			return;	
		}
		*/
		
		Ext.Msg.show({
			title:	"Confirmaci�n",
			msg:		'�Desea continuar con la operaci�n "CANCELAR" ?',
			buttons:	Ext.Msg.OKCANCEL,
			fn: function resultMsj(btn){
				if(btn =='ok'){
					
					consProcesadosData.add(regSeleccionados);
					
					Ext.Ajax.request({
						url: '15monproc.data.jsp',
						params: Ext.apply(forma.getForm().getValues(),{
							informacion: 'procesar',	
							operacion:'CANCELAR',
							descripcion:cg_descripcion,
							noSolicitud:noSolicitud,
							producto:producto,
							tabla_Origen:tabla_Origen,
							noRegistros:noRegistros,
							error_proceso:error_proceso,						
							chckFueraFacultades:chckFueraFacultades,
							pantalla: forma.query('#pantalla')[0].getValue(),
							tipoProceso:forma.query('#tipoProceso')[0].getValue(),
							monitor:forma.query('#monitor')[0].getValue()

						})
						,callback: transmiteReProceso
					});
				}
			}
		});				
		
	}
	
	
		//"REINICIAR INTERFAZ	
	var ReInterfaz = function() {
	
		var  gridDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
		var store = gridDetalle.getStore();		
		var noRegistros = 0;
		var regSeleccionados = [];	
		var cg_descripcion=  Ext.ComponentQuery.query('#cg_descripcion1')[0].getValue();
		var forma= Ext.ComponentQuery.query('#forma')[0];
		var  tipoRadio01 =  Ext.ComponentQuery.query('#chckFueraFacultades01')[0].getValue();
		var chckFueraFacultades ="N";
		if(tipoRadio01==true){ chckFueraFacultades= "S";  }	

		cancelar();
		
		var selectionModel = gridDetalle.getSelectionModel();
        var collection = selectionModel.selected;
        for(var index=0;index<collection.length;index++) {
			var record = collection.get(index);
			noRegistros++;
			regSeleccionados.push(record); 
			noSolicitud.push(record.data['SOLICITUD']);
			producto.push(record.data['IC_PRODUCTO']);	
			error_proceso.push(record.data['IC_ERROR_PROCESO']);	
			tabla_Origen.push(record.data['TABLA_ORIGEN']);
		}
		//console.log("noRegistros:"+noRegistros);
		/*IHJ
		store.each(function(record) {
			if(record.data['SELECCION']=='S' ){	
				noRegistros++;
				regSeleccionados.push(record); 
				noSolicitud.push(record.data['SOLICITUD']);
				producto.push(record.data['IC_PRODUCTO']);	
				error_proceso.push(record.data['IC_ERROR_PROCESO']);	
				tabla_Origen.push(record.data['TABLA_ORIGEN']);
			}
		});
		*/
		
		if(noRegistros ==0) {
			Ext.MessageBox.alert('Error de validaci�n','Es necesario seleccionar un registro para reiniciar interfaz');
			return;		
		}
		
		/*Petici�n de Irving - REINICIAR INTERFAZ
		if(noRegistros >1000) {
			Ext.MessageBox.alert('Error de validaci�n','No se pueden reprocesar m�s de 1000 registros');
			return;	
		}
		*/
		
		Ext.Msg.show({
			title:	"Confirmaci�n",
			msg:		'�Desea continuar con la operaci�n "REINICIAR INTERFAZ" ?',
			buttons:	Ext.Msg.OKCANCEL,
			fn: function resultMsj(btn){
				if(btn =='ok'){
					consProcesadosData.add(regSeleccionados);
					Ext.Ajax.request({
						url: '15monproc.data.jsp',
						params: Ext.apply(forma.getForm().getValues(),{
							informacion: 'procesar',	
							operacion:'REINICIAR INTERFAZ',
							descripcion:cg_descripcion,
							noSolicitud:noSolicitud,
							producto:producto,							
							noRegistros:noRegistros,
							error_proceso:error_proceso,
							tabla_Origen:tabla_Origen,
							chckFueraFacultades:chckFueraFacultades,
							pantalla: forma.query('#pantalla')[0].getValue(),
							tipoProceso:forma.query('#tipoProceso')[0].getValue(),
							monitor:forma.query('#monitor')[0].getValue()							
						})
						,callback: transmiteReProceso
					});
				}
			}
		});				
	}
	
	
		//CANCEL_FONDEO	
	var ProcesCancelarFondeo = function() {
		var  gridDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
		var store = gridDetalle.getStore();		
		var noRegistros = 0;
		var regSeleccionados = [];	
		var cg_descripcion=  Ext.ComponentQuery.query('#cg_descripcion1')[0].getValue();
		var forma= Ext.ComponentQuery.query('#forma')[0];
		var  tipoRadio01 =  Ext.ComponentQuery.query('#chckFueraFacultades01')[0].getValue();
		var chckFueraFacultades ="N";
		if(tipoRadio01==true){ chckFueraFacultades= "S";  }	

		cancelar();
		
		var selectionModel = gridDetalle.getSelectionModel();
        var collection = selectionModel.selected;
        for(var index=0;index<collection.length;index++) {
			var record = collection.get(index);
			noRegistros++;
			regSeleccionados.push(record); 
			noSolicitud.push(record.data['SOLICITUD']);
			producto.push(record.data['IC_PRODUCTO']);
			tabla_Origen.push(record.data['TABLA_ORIGEN']);
			error_proceso.push(record.data['IC_ERROR_PROCESO']);
		}
		//console.log("noRegistros:"+noRegistros);		
		/*IHJ
		store.each(function(record) {				
			if(record.data['SELECCION']=='S' ){	
				noRegistros++;
				regSeleccionados.push(record); 
				noSolicitud.push(record.data['SOLICITUD']);
				producto.push(record.data['IC_PRODUCTO']);
				tabla_Origen.push(record.data['TABLA_ORIGEN']);
				error_proceso.push(record.data['IC_ERROR_PROCESO']);
		
			}
		});		
		*/
					
		if(noRegistros ==0) {
			Ext.MessageBox.alert('Error de validaci�n','Es necesario seleccionar un registro para reprocesar o cancelar');
			return;
		}
		
		/*Petici�n Irving - Operadas con Fondeo Propio
		if(noRegistros >1000) {
			Ext.MessageBox.alert('Error de validaci�n','No se pueden reprocesar m�s de 1000 registros');
			return;	
		}
		*/
		
		/*
		var chckFueraFacultades1 = Ext.getCmp('chckFueraFacultades1');	
		var cg_descripcion1 = Ext.getCmp('cg_descripcion1');	
		
		 duda  en que momento Aplica esta validaci�n
		if(chckFueraFacultades1.getValue=='N')  {			
			if (Ext.isEmpty(cg_descripcion1.getValue()) ){
				Ext.MessageBox.alert(('Error de validaci�n',"Hay registros con 'Descripci�n Especial', por lo que es necesario especificar una descripci�n de la lista");
			}
		}	
		*/
					
		Ext.Msg.show({
			title:	"Confirmaci�n",
			msg:		'�Desea continuar con la operaci�n "Operadas con Fondeo Propio" ?',
			buttons:	Ext.Msg.OKCANCEL,
			fn: function resultMsj(btn){
				if(btn =='ok'){
					
					consProcesadosData.add(regSeleccionados);
					
					Ext.Ajax.request({
						url: '15monproc.data.jsp',
						params: Ext.apply(forma.getForm().getValues(),{
							informacion: 'procesar',	
							operacion:'CANCEL_FONDEO',
							descripcion:cg_descripcion,
							noSolicitud:noSolicitud,
							producto:producto,
							tabla_Origen:tabla_Origen,
							noRegistros:noRegistros,
							error_proceso:error_proceso,
							chckFueraFacultades:chckFueraFacultades,
							pantalla: forma.query('#pantalla')[0].getValue(),
							tipoProceso:forma.query('#tipoProceso')[0].getValue(),
							monitor:forma.query('#monitor')[0].getValue()
						})
						,callback: transmiteReProceso
					});
				}
			}
		});			
	}
	
	
	
 var consProcesadosData = new Ext.data.ArrayStore({
		fields: [	
		{	name: 'PRODUCTO'},	
			{	name: 'MODALIDAD'},	
			{	name: 'INTERMEDIARIO'},	
			{	name: 'NOMBRE_PYME'},	
			{	name: 'NUM_PYME'},	
			{	name: 'MONTO_DESC'},	
			{	name: 'NUM_PYME_SIRAC'},
			{	name: 'HORA'},
			{	name: 'SOLICITUD'},	
			{	name: 'MOTIVO'},					
			{	name: 'AGENCIA'},	
			{	name: 'SUBAPLICACION'},	
			{	name: 'LINEA_FONDEO'},	
			{	name: 'APROBADOR'},	
			{	name: 'VER_ARCHIVO'},
			{  name: 'SELECCION' },
			{  name: 'TABLA_ORIGEN' },
			{  name: 'IC_PRODUCTO' },
			{  name: 'FACULTAD' },
			{	name: 'VER_ARCHIVO_LOG'},
			{	name: 'IC_ERROR_PROCESO'},
			{	name: 'NUM_PROYECTO'},
			{  name: 'NUM_CONTRATO'},
			{  name: 'NUM_PRESTAMO'}
		]
	});
	
	
	
	var  gridProcesados = Ext.create('Ext.grid.Panel',{	
	//	plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridProcesados',
		store: consProcesadosData,
		height: 150,
		width: 350,	
		//style: 'margin: 10px auto 0px auto;',		
		//xtype: 'cell-editing',
      title: 'Rechazados',
      //frame: false,	
		hidden: true,
		//plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		columns: [			
		{
				header: 'Producto',
				tooltip: 'Producto',
				dataIndex: 'PRODUCTO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Modalidad',
				tooltip: 'Modalidad',
				dataIndex: 'MODALIDAD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Intermediario',
				tooltip: 'Intermediario',
				dataIndex: 'INTERMEDIARIO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					var facultad = record.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			}, 
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					var facultad = record.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Num. PYME',
				tooltip: 'Num. PYME',
				dataIndex: 'NUM_PYME',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Monto a descontar',
				tooltip: 'Monto a descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + Ext.util.Format.number(value, '$0,0.00')  + '</span>';
					}else {
						return  Ext.util.Format.number(value, '$0,0.00');
					}
				}			
			},
			{
				header: 'Num. de PYME Sirac',
				tooltip: 'Num. de PYME Sirac',
				dataIndex: 'NUM_PYME_SIRAC',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Hora',
				tooltip: 'Hora',
				dataIndex: 'HORA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Solicitud',
				tooltip: 'Solicitud',
				dataIndex: 'SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Motivo',
				tooltip: 'Motivo',
				dataIndex: 'MOTIVO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					var facultad = record.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},			
			{						
				header:'Agencia',
				tooltip: 'Agencia',
				dataIndex : 'AGENCIA',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},				
			{						
				header:'Subaplicaci�n',
				tooltip: 'Subaplicaci�n',
				dataIndex : 'SUBAPLICACION',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},	
			{						
				header:'L�nea fondeo',
				tooltip: 'L�nea fondeo',
				dataIndex : 'LINEA_FONDEO',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},	
			{						
				header:'Aprobador por',
				tooltip: 'Aprobador por',
				dataIndex : 'APROBADOR',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{						
				header:'N�mero Proyecto',
				tooltip: 'N�mero Proyecto',
				dataIndex : 'NUM_PROYECTO',
				width : 150,
				align: 'center',
				sortable : false				
			},	
			{						
				header:'N�mero Contrato',
				tooltip: 'N�mero Contrato',
				dataIndex : 'NUM_CONTRATO',
				width : 150,
				align: 'center',
				sortable : false				
			},
			{						
				header:'N�mero Prestamo',
				tooltip: 'N�mero Prestamo',
				dataIndex : 'NUM_PRESTAMO',
				width : 150,
				align: 'center',
				sortable : false				
			}				
		
		],
		displayInfo: true,		
		loadMask: true,
		stripeRows: true,
		height: 250,
		width: 900,
		align: 'center',
		frame: false,
		bbar: [	
			'->',
			{
				text: 'Regresar',				
				xtype: 'button',
				id: 'btnRegresar',				
				iconCls: 'icoLimpiar',
				handler: function() {
					var forma= Ext.ComponentQuery.query('#forma')[0];
					var  tipoProceso=  forma.query('#tipoProceso')[0].getValue();	
					
					if(tipoProceso=='Proyectos') {	
						
						window.location = '15monproc2ext.jsp';
					
					}else  if(tipoProceso=='Diversos') {						
						
						window.location = '15monproc1ext.jsp';
					
					}else  if(tipoProceso=='Contratos') {
					
						window.location = '15monproc3ext.jsp';
					
					
					}else  if(tipoProceso=='Disposiciones' )  {
					
						window.location = '15monproc5ext.jsp';
					
					}else  if( tipoProceso=='Prestamos' || tipoProceso=='Pr�stamos' ) {
						
						window.location = '15monproc4ext.jsp';
					}
				}
			}
		]
	});
	//******************************************+++
	
	
	
	
	
		// Numero Prestamo
	var archNumPrestamo  = function (grid,rowIndex,colIndex,item){		
		var registro = grid.getStore().getAt(rowIndex);
		var claveSolicitud = registro.get('SOLICITUD');		
		var ic_producto = registro.get('IC_PRODUCTO');	
		var forma= Ext.ComponentQuery.query('#forma')[0];
		var  tipoProceso =  forma.query('#tipoProceso')[0].getValue();
		
		Ext.Ajax.request({
			url: '15monproc.data.jsp',
			params: Ext.apply(forma.getForm().getValues(),{
				informacion: 'ArchivoNoContrato',	
				claveSolicitud:claveSolicitud,
				ic_producto:ic_producto,
				tipoProceso:tipoProceso,
				logAdicional:'prestamos'
			}),
			callback: procesarArchivos
		});
	}
	
	//N�mero Proyecto
	var archNumProyecto  = function (grid,rowIndex,colIndex,item){			
		var registro = grid.getStore().getAt(rowIndex);
		var claveSolicitud = registro.get('SOLICITUD');		
		var ic_producto = registro.get('IC_PRODUCTO');			
		var forma= Ext.ComponentQuery.query('#forma')[0];
		var  tipoProceso =  forma.query('#tipoProceso')[0].getValue();
		
		Ext.Ajax.request({
			url: '15monproc.data.jsp',
			params: Ext.apply(forma.getForm().getValues(),{
				informacion: 'ArchivoNoProyecto',	
				claveSolicitud:claveSolicitud,
				ic_producto:ic_producto,
				tipoProceso:tipoProceso,
				logAdicional:'proyectos'
			}),
			callback: procesarArchivos
		});
	}
	
	
			//N�mero  Contrato
	var archNumContrato  = function (grid,rowIndex,colIndex,item){		
		var registro = grid.getStore().getAt(rowIndex);
		var claveSolicitud = registro.get('SOLICITUD');		
		var ic_producto = registro.get('IC_PRODUCTO');			
		var forma= Ext.ComponentQuery.query('#forma')[0];
		var  tipoProceso =  forma.query('#tipoProceso')[0].getValue();
		
		Ext.Ajax.request({
			url: '15monproc.data.jsp',
			params: Ext.apply(forma.getForm().getValues(),{
				informacion: 'ArchivoNoContrato',	
				claveSolicitud:claveSolicitud,
				ic_producto:ic_producto,
				tipoProceso:tipoProceso,
				logAdicional:'contratos'
			}),
			callback: procesarArchivos
		});
	}
	
		//Ver Archivo(Log)	
	var generarArchivoLog  = function (grid,rowIndex,colIndex,item){		
		var registro = grid.getStore().getAt(rowIndex);
		var claveSolicitud = registro.get('SOLICITUD');		
		var ic_producto = registro.get('IC_PRODUCTO');			
	 	var forma= Ext.ComponentQuery.query('#forma')[0];
	   var  tipoProceso =  forma.query('#tipoProceso')[0].getValue();
		if(tipoProceso=='Pr�stamos' || tipoProceso=='Prestamos')  { tipoProceso='Prestamos';  }
		
		
		Ext.Ajax.request({
			url: '15monproc.data.jsp',
			params: Ext.apply(forma.getForm().getValues(),{
				informacion: 'ArchivoLog',	
				claveSolicitud:claveSolicitud,
				ic_producto:ic_producto,
				tipoProceso:tipoProceso,
				logAdicional:''
			}),
			callback: procesarArchivos
		});
	}
	
	
		// Archivo Origen
	var generarArchivo  = function (grid,rowIndex,colIndex,item){		
		var registro = grid.getStore().getAt(rowIndex);
		var claveSolicitud = registro.get('SOLICITUD');	
		var forma= Ext.ComponentQuery.query('#forma')[0];		
		Ext.Ajax.request({
			url: '15monproc.data.jsp',
			params: Ext.apply(forma.getForm().getValues(),{
				informacion: 'ArchivoVerOrigen',	
				claveSolicitud:claveSolicitud
			}),
			callback: procesarArchivos
		});
	}
	
	
	var procesarConsDetalleData = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {			
			var jsonData =  store.proxy.reader.rawData;			
			
			var forma= Ext.ComponentQuery.query('#forma')[0];
			forma.el.unmask();
			
			var gridDetalle =  Ext.ComponentQuery.query('#gridDetalle')[0];
						
			Ext.ComponentQuery.query('#gridDetalle')[0].show();	
		
		
			gridDetalle.setTitle('<div><div style="float:left">Rechazados '+jsonData.tituloTipoProceso+'</div>');
												
			Ext.ComponentQuery.query('#barraPaginaD')[0].setText('Total Rechazadas : '+jsonData.numRegistros);
			Ext.ComponentQuery.query('#comboDescripcion')[0].show();	
			Ext.ComponentQuery.query('#totalSelec')[0].setValue('');	
			Ext.ComponentQuery.query('#totalSelec')[0].show();	
			
			 Ext.ComponentQuery.query('#btnEnviar')[0].hide();
			 
			if(jsonData.tipoProceso=='Diversos') {
									
				if( jsonData.ic_producto =='1' ||  jsonData.ic_producto=='4'  ) {
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO').hide(); 	//Origen
					
				}else if(  jsonData.ic_producto =='0'  )  {
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO').show(); 					
				
				}else if(  jsonData.ic_producto =='' )  {
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO').show(); 
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO_LOG').hide(); 
				}
				
				findColumnByDataIndex(	gridDetalle,'NUM_PROYECTO').hide();
				findColumnByDataIndex(	gridDetalle,'NUM_CONTRATO').hide();
				findColumnByDataIndex(	gridDetalle,'NUM_PRESTAMO').hide();
							
				
				if(jsonData.ic_producto=='1' ||  jsonData.ic_producto=='1A'  ||  jsonData.ic_producto=='1B' ||  jsonData.ic_producto=='1C' ||  jsonData.ic_producto=='1D'){
						Ext.ComponentQuery.query('#btnCancelarFondeo')[0].show();
				}else  {
					Ext.ComponentQuery.query('#btnCancelarFondeo')[0].hide();
				}
					
				if(jsonData.chckFueraFacultades=='S')  {
					Ext.ComponentQuery.query('#cg_descripcion1')[0].hide();
					Ext.ComponentQuery.query('#btnImprimir')[0].hide();
					Ext.ComponentQuery.query('#btnCancelar')[0].hide();
					Ext.ComponentQuery.query('#btnReproceso')[0].hide();					
					if(jsonData.ic_producto=='1'){
						Ext.ComponentQuery.query('#btnReproceso')[0].show();
						Ext.ComponentQuery.query('#btnCancelarRecS')[0].show();						
					}if(jsonData.ic_producto=='0'){
						Ext.ComponentQuery.query('#btnCancelarRecS')[0].show();
					}		
				}else if(jsonData.chckFueraFacultades=='N')  {					
					Ext.ComponentQuery.query('#cg_descripcion1')[0].show();
					Ext.ComponentQuery.query('#btnImprimir')[0].show();
					Ext.ComponentQuery.query('#btnCancelar')[0].show();
					Ext.ComponentQuery.query('#btnReproceso')[0].show();
					Ext.ComponentQuery.query('#btnCancelarRecS')[0].hide();										
				}
			}else  if(jsonData.tipoProceso=='Proyectos') {
			
				findColumnByDataIndex(	gridDetalle,'MODALIDAD').show();
				
				if(jsonData.ic_producto =='' ) {	
					findColumnByDataIndex(	gridDetalle,'NUM_PROYECTO').hide();
					findColumnByDataIndex(	gridDetalle,'NUM_CONTRATO').hide();
					findColumnByDataIndex(	gridDetalle,'NUM_PRESTAMO').hide();
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO').hide();
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO_LOG').show(); 
				
				}else if(jsonData.ic_producto =='0' ) {					
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO').hide();
					
				}else if(jsonData.ic_producto =='1' ||  jsonData.ic_producto=='4'  ) {							
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO').show();
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO_LOG').hide(); 
					findColumnByDataIndex(	gridDetalle,'NUM_CONTRATO').hide();
					findColumnByDataIndex(	gridDetalle,'NUM_PRESTAMO').hide();
				}				
			
				if(jsonData.ic_producto=='1' ||  jsonData.ic_producto=='1A'  ||  jsonData.ic_producto=='1B' ||  jsonData.ic_producto=='1C' ||  jsonData.ic_producto=='1D'){
			
					Ext.ComponentQuery.query('#btnCancelarFondeo')[0].show();				
				}else{
					Ext.ComponentQuery.query('#btnCancelarFondeo')[0].hide();
				}
				if(jsonData.errorCausa43!='0'){
					Ext.ComponentQuery.query('#cg_descripcion1')[0].show();				
				}else {
					Ext.ComponentQuery.query('#cg_descripcion1')[0].hide();
				}			
				Ext.ComponentQuery.query('#btnImprimir')[0].show();
				Ext.ComponentQuery.query('#btnReproceso')[0].show();
				Ext.ComponentQuery.query('#btnCancelar')[0].show();				
				
			}else  if(jsonData.tipoProceso=='Contratos') {
					
					findColumnByDataIndex(	gridDetalle,'MODALIDAD').show(); 
						
				if( jsonData.ic_producto ==''  ){					
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO').show();	
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO_LOG').show();
					findColumnByDataIndex(	gridDetalle,'NUM_PROYECTO').show();
					findColumnByDataIndex(	gridDetalle,'NUM_CONTRATO').hide();
					findColumnByDataIndex(	gridDetalle,'NUM_PRESTAMO').hide();
					
				}else if(jsonData.ic_producto=='0'    ){					
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO').show();	
					findColumnByDataIndex(	gridDetalle,'NUM_PROYECTO').show();
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO_LOG').show();
					findColumnByDataIndex(	gridDetalle,'NUM_CONTRATO').hide();
					findColumnByDataIndex(	gridDetalle,'NUM_PRESTAMO').hide();
				}else if(jsonData.ic_producto=='1'  ||  jsonData.ic_producto=='4' )  {					
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO').hide();	
					findColumnByDataIndex(	gridDetalle,'NUM_PROYECTO').show();
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO_LOG').show();
					findColumnByDataIndex(	gridDetalle,'NUM_CONTRATO').hide();
					findColumnByDataIndex(	gridDetalle,'NUM_PRESTAMO').hide();
				}
				
				if(jsonData.ic_producto=='1' ||  jsonData.ic_producto=='1A'  ||  jsonData.ic_producto=='1B' ||  jsonData.ic_producto=='1C' ||  jsonData.ic_producto=='1D'){
					Ext.ComponentQuery.query('#btnCancelarFondeo')[0].show();				
				}else{
					Ext.ComponentQuery.query('#btnCancelarFondeo')[0].hide();
				}
				Ext.ComponentQuery.query('#btnImprimir')[0].show();
				Ext.ComponentQuery.query('#btnReproceso')[0].show();
				Ext.ComponentQuery.query('#btnCancelar')[0].show();	
				Ext.ComponentQuery.query('#cg_descripcion1')[0].hide();
				
			}else  if(jsonData.tipoProceso=='Disposiciones') {
									  	
			 if(jsonData.ic_producto=='0'  ||  jsonData.ic_producto ==''  ||  jsonData.ic_producto =='1' ||  jsonData.ic_producto =='4' ){
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO').hide(); 
					findColumnByDataIndex(	gridDetalle,'NUM_CONTRATO').show();
					findColumnByDataIndex(	gridDetalle,'NUM_PRESTAMO').show();
					findColumnByDataIndex(	gridDetalle,'NUM_PROYECTO').show();
					findColumnByDataIndex(	gridDetalle,'VER_ARCHIVO_LOG').show();
				}	
				
				Ext.ComponentQuery.query('#btnImprimir')[0].show();
				Ext.ComponentQuery.query('#btnReproceso')[0].show();
				Ext.ComponentQuery.query('#btnCancelar')[0].show();	
				Ext.ComponentQuery.query('#cg_descripcion1')[0].hide();
				
				if(jsonData.ic_producto=='1' ||  jsonData.ic_producto=='1A'  ||  jsonData.ic_producto=='1B' ||  jsonData.ic_producto=='1C' ||  jsonData.ic_producto=='1D'){
					Ext.ComponentQuery.query('#btnCancelarFondeo')[0].show();				
				}else{
					Ext.ComponentQuery.query('#btnCancelarFondeo')[0].hide();
				}
			}else  if(jsonData.tipoProceso=='Prestamos') {
			
				Ext.ComponentQuery.query('#btnImprimir')[0].show();
				Ext.ComponentQuery.query('#btnReproceso')[0].show();
				Ext.ComponentQuery.query('#btnCancelar')[0].show();					 
				findColumnByDataIndex(	gridDetalle,'NUM_PRESTAMO').hide();
				
			}
		
			if(store.getTotalCount() > 0) {	
			
				gridDetalle.getView().getEl().unmask();	
				gridDetalle.getView().setAutoScroll(true);
				
			}else {
			
				Ext.ComponentQuery.query('#cg_descripcion1')[0].hide(); 
				Ext.ComponentQuery.query('#btnImprimir')[0].hide();
				Ext.ComponentQuery.query('#btnCancelar')[0].hide();
				Ext.ComponentQuery.query('#btnReproceso')[0].hide();
				Ext.ComponentQuery.query('#btnCancelarFondeo')[0].hide();
				Ext.ComponentQuery.query('#btnCancelarRecS')[0].hide();	
				Ext.ComponentQuery.query('#comboDescripcion')[0].hide();
				Ext.ComponentQuery.query('#btnReInterfaz')[0].hide();					
				gridDetalle.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridDetalle.getView().setAutoScroll(false);	
				
			}
			
		}
	};
	
	
	Ext.define('LisRegServicios', {
		extend: 'Ext.data.Model',
		fields: [
			{	name: 'PRODUCTO'},	
			{	name: 'MODALIDAD'},	
			{	name: 'INTERMEDIARIO'},	
			{	name: 'NOMBRE_PYME'},	
			{	name: 'NUM_PYME'},	
			{	name: 'MONTO_DESC'},	
			{	name: 'NUM_PYME_SIRAC'},
			{	name: 'HORA'},
			{	name: 'SOLICITUD'},	
			{	name: 'MOTIVO'},					
			{	name: 'AGENCIA'},	
			{	name: 'SUBAPLICACION'},	
			{	name: 'LINEA_FONDEO'},	
			{	name: 'APROBADOR'},	
			{	name: 'VER_ARCHIVO'},
			{  name: 'SELECCION' },
			{  name: 'TABLA_ORIGEN' },
			{  name: 'IC_PRODUCTO' },
			{  name: 'FACULTAD' },
			{	name: 'VER_ARCHIVO_LOG'},
			{	name: 'IC_ERROR_PROCESO'},
			{	name: 'NUM_PROYECTO'},
			{  name: 'NUM_CONTRATO'},
			{  name: 'NUM_PRESTAMO'}			
		 ]
	});

	var consDetalleData = Ext.create('Ext.data.Store', {
		model: 'LisRegServicios',	
		proxy: {
			type: 'ajax',
			url: '15monproc.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'ConsultaDetalle'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsDetalleData
			}
	});
	
	
		
	// *****************para realizar validaciones al seleccionar 			
	var selectModel = new Ext.create('Ext.selection.CheckboxModel',{	
		checkOnly: true,
		listeners: {
			//Para quitar lo selecccionado en el documentos ******************************************
			deselect: function(model, record, index) {
 				//console.log("entro-deselect")
				//record.data['SELECCION']='N';
				//record.commit();
				/*
				var gridDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
				var store = gridDetalle.getStore();	
				var total = 0;
				Ext.ComponentQuery.query('#totalSelec')[0].setValue('<h1><b>'+total+'</h1><b>');
				store.each(function(record2) {				
					if(record2.data['SELECCION']=='S'){
						total++;
					}						
				});
                */
                //var total = model.selected.length;
				//Ext.ComponentQuery.query('#totalSelec')[0].setValue(total);
				
				//if(total==0) {
				//	 Ext.ComponentQuery.query('#btnEnviar')[0].hide();
  				 //} 
				 
			},	
			//Para selecccionar los documentos ******************************************
			select: function(model, record, index) {
				//console.log("entro-select")
				//record.data['SELECCION']='S';
				//record.commit();
				
				//var gridDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
				//var store = gridDetalle.getStore();	
				//var total = 0;
				//Ext.ComponentQuery.query('#totalSelec')[0].setValue(total);
				/*store.each(function(record2) {				
                    console.log("entrootr1")
					if(record2.data['SELECCION']=='S'){
						total++;
					}						
				});		*/
                //var total = model.selected.length;
                //Ext.ComponentQuery.query('#totalSelec')[0].setValue(total);
                
                //if(total>0) {
                    //Ext.ComponentQuery.query('#btnEnviar')[0].show();
                //} 
                /*
                var forma= Ext.ComponentQuery.query('#forma')[0];
                var tipoProceso1= forma.query('#tipoProceso')[0].getValue();
				
				 if(tipoProceso1=='Disposiciones' ||  tipoProceso1=='Prestamos'  ||  tipoProceso1=='Pr�stamos'  || tipoProceso1=='Contratos' ) {
					 if(total>0) {
						 Ext.ComponentQuery.query('#btnEnviar')[0].show();
					 } 
				 }				 
                 */
				 
			},
            selectionchange: function(model, record, index) {
                //console.log("selectionchange")
                var total = model.selected.length;
                Ext.ComponentQuery.query('#totalSelec')[0].setValue(total);
                if(total==0) {
                    Ext.ComponentQuery.query('#btnEnviar')[0].hide();
                } else if(total>0) {
                    Ext.ComponentQuery.query('#btnEnviar')[0].show();
                }
            }
		}
	});
	
	
	var  gridDetalle = Ext.create('Ext.grid.Panel',{	
	//	plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridDetalle',
		store: consDetalleData,
		height: 150,
		width: 350,	
		//style: 'margin: 10px auto 0px auto;',		
		//xtype: 'cell-editing',
      title: 'Rechazados',
      //frame: false,	
		hidden: true,
		selModel:selectModel,
		//plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		columns: [			
			{
				header: 'Producto',
				tooltip: 'Producto',
				dataIndex: 'PRODUCTO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},			
			{
				header: 'Modalidad',
				tooltip: 'Modalidad',
				dataIndex: 'MODALIDAD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Intermediario',
				tooltip: 'Intermediario',
				dataIndex: 'INTERMEDIARIO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					var facultad = record.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			}, 
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					var facultad = record.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Num. PYME',
				tooltip: 'Num. PYME',
				dataIndex: 'NUM_PYME',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Monto a descontar',
				tooltip: 'Monto a descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + Ext.util.Format.number(value, '$0,0.00') + '</span>';
					}else {
						return  Ext.util.Format.number(value, '$0,0.00'); 
					}
				}			
			},
			{
				header: 'Num. de PYME Sirac',
				tooltip: 'Num. de PYME Sirac',
				dataIndex: 'NUM_PYME_SIRAC',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Hora',
				tooltip: 'Hora',
				dataIndex: 'HORA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Solicitud',
				tooltip: 'Solicitud',
				dataIndex: 'SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Motivo',
				tooltip: 'MOTIVO',
				dataIndex: 'MOTIVO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					var facultad = record.data['FACULTAD'];	
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';					
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}					
				}				
			},			
			{						
				header:'Agencia',
				tooltip: 'Agencia',
				dataIndex : 'AGENCIA',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},				
			{						
				header:'Subaplicaci�n',
				tooltip: 'Subaplicaci�n',
				dataIndex : 'SUBAPLICACION',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},	
			{						
				header:'L�nea fondeo',
				tooltip: 'L�nea fondeo',
				dataIndex : 'LINEA_FONDEO',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},	
			{						
				header:'Aprobador por',
				tooltip: 'Aprobador por',
				dataIndex : 'APROBADOR',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro){                                
					var facultad = registro.data['FACULTAD'];	
					if(facultad =='S') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},				
			{						
				xtype: 'actioncolumn',
				header:'N�mero Proyecto',
				tooltip: 'N�mero Proyecto',
				dataIndex : 'NUM_PROYECTO',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return value;
				},
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';							
						}
						,handler: archNumProyecto
					}	
				]
			},
			{						
				xtype: 'actioncolumn',
				header:'N�mero Contrato',
				tooltip: 'N�mero Contrato',
				dataIndex : 'NUM_CONTRATO',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return value;
				},
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(valor!='') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';	
							}
						}
						,handler: archNumContrato
					}	
				]
			},
			{						
				xtype: 'actioncolumn',
				header:'N�mero Prestamo',
				tooltip: 'N�mero Prestamo',
				dataIndex : 'NUM_PRESTAMO',
				width : 150,
				align: 'center',
				sortable : false,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return value;
				},
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(valor!='') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';							
							}
						}
						,handler: archNumPrestamo
					}	
				]
			},			
			{						
				xtype: 'actioncolumn',
				header:'Ver Archivo(Log)',
				tooltip: 'Ver Archivo (Log)',
				dataIndex : 'VER_ARCHIVO_LOG',
				width : 150,
				align: 'center',
				sortable : false,
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}
						,handler: generarArchivoLog
					}	
				]
			},
			{						
				xtype: 'actioncolumn',
				header:'Ver Archivo Origen',
				tooltip: 'Ver Archivo Origen',
				dataIndex : 'VER_ARCHIVO',
				width : 150,
				align: 'center',
				sortable : false,
				//hidden: true,
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							var ic_producto = registro.data['IC_PRODUCTO'];	
							if(ic_producto =='0') {						
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						}
						,handler: generarArchivo
					}	
				]
			}
		],
		//displayInfo: true,		
		emptyMsg: "No hay registros.",		
		//loadMask: true,
		//stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		//frame: false,
		bbar: [						
			{
				xtype: 'label',
				id:    'barraPaginaD',
				html:  'Total Rechazadas:',
				style: 'font-weight:bold;padding-bottom:10pt;text-align:center'
			},
			'->',
			'-',
			{
				text: 'Enviar',
				iconCls: 'icoBotonEnviarCorreo',
				xtype: 'button',
				id: 'btnEnviar',
				hidden: true,	
				handler:vistaPreviaCorreoResumen
			},					
			{
				text: 'Cancelar y Fondeo Propio',
				iconCls: 'cancelar',
				xtype: 'button',
				id: 'btnCancelarFondeo',
				hidden: true	
				,handler:ProcesCancelarFondeo
			},		
			{
				text: 'Cancelar y Rechazar Solicitudes',
				iconCls: 'cancelar',
				xtype: 'button',
				id: 'btnCancelarRecS',
				hidden: true	
				,handler:ProcesCancelar
			},				
			{
				text: 'Imprimir--',
				iconCls: 'icoPdf',
				xtype: 'button',
				id: 'btnImprimir',
				hidden: true	
				,handler: function(boton, evento) {
					var forma= Ext.ComponentQuery.query('#forma')[0];
					
					var  tipoRadio01 =  Ext.ComponentQuery.query('#chckFueraFacultades01')[0].getValue();
					var chckFueraFacultades ="N";
					if(tipoRadio01==true){ chckFueraFacultades= "S";  }
					
						Ext.Ajax.request({
							url: '15monproc.data.jsp',
							params: Ext.apply(forma.getForm().getValues(),{
								informacion: 'ArchivoDetalle',
								chckFueraFacultades:chckFueraFacultades,
								pantalla: forma.query('#pantalla')[0].getValue(),
								tipoProceso:forma.query('#tipoProceso')[0].getValue(),
								monitor:forma.query('#monitor')[0].getValue()
							}),
							callback: procesarArchivos
						});
					}
			},				
			'-',
			{
				text: 'Cancelar',
				iconCls: 'cancelar',
				xtype: 'button',
				id: 'btnCancelar',
				hidden: true						
				,handler:ProcesCancelar
			},
			'-',
			{
				text: 'Reproceso',
				iconCls: 'autorizar',
				xtype: 'button',
				id: 'btnReproceso',
				hidden: true
				,handler:Reproceso
			},
			{
				text: 'Reiniciar Interfaz',
				iconCls: 'autorizar',
				xtype: 'button',
				id: 'btnReInterfaz',				
				handler:ReInterfaz
			}
		]		
	});
		
	
		//***************************** Forma y Catalogos
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
		
	var catalogoProducto = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15monproc.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoProducto'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var catalogoIF = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15monproc.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoIF'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	
	var catalogoRechazo = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15monproc.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoRechazo'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var catalogoDescripcion = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15monproc.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoDescripcion'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	
	var mensajeA = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'mensajeA',
      width:	600,
		style: 'margin:0 auto;',		
		collapsible:	false,
		titleCollapse:	false,
		frame:			true,
		hidden: true,
		 fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 160
		},
		title:'',		
		items	: [
			{ 	xtype: 'displayfield',  id: 'mensaje', 	value: '' }	
		]
	});
	
	
	
	var comboDescripcion = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'comboDescripcion',
      width:	600,
		style: 'margin:0 auto;',		
		collapsible:	false,
		titleCollapse:	false,
		frame:			true,
		hidden: true,
		 fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 160
		},
		title:'Interfases',		
		items	: [
			{
				xtype: 'combo',
				fieldLabel: 'Producto',
				itemId: 'cg_descripcion1',
				name: 'cg_descripcion',
				hiddenName: 'cg_descripcion',
				forceSelection: true,
				hidden: false, 
				width: 450,
				store: catalogoDescripcion,
				emptyText: 'Seleccione...',
				queryMode: 'local',
				allowBlank: true,
				displayField: 'descripcion',
				valueField: 'clave'	
			},
			{ 	xtype: 'textfield',  style: 'font-weight:bold;text-align:center' ,  fieldLabel: 'Total de Registros Seleccionados', disabled:true,  id: 'totalSelec', 	value: '',  width: 80 }					
		]
	});
	
	
	var elementosForma =  [
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			itemId: 'ic_producto1',
			name: 'ic_producto',
			hiddenName: 'ic_producto',
			forceSelection: true,
			hidden: false, 
			width: 450,
			store: catalogoProducto,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',						
			listeners: {
				select: function(obj, newVal, oldVal){						
				
				var forma= Ext.ComponentQuery.query('#forma')[0];
				var  pantalla =  forma.query('#pantalla')[0].getValue();				
				var  tipoProceso =  forma.query('#tipoProceso')[0].getValue();
				var  ic_producto =  forma.query('#ic_producto1')[0].getValue();
				
				
				if(pantalla== 'Detalle_Registos' ) {
					Ext.ComponentQuery.query('#ic_if1')[0].show();	
					Ext.ComponentQuery.query('#ic_nafin_electronico1')[0].show();	
					Ext.ComponentQuery.query('#cmb_estatus1')[0].show();	
					
					if(tipoProceso=='Diversos') {	
						if( ic_producto=='1' || ic_producto =='1A' || ic_producto =='1B' || ic_producto =='1C' || ic_producto == '1D' 
							|| ic_producto=='0' ||ic_producto =='0F' || ic_producto =='0C' || ic_producto =='0E' ) {
							Ext.ComponentQuery.query('#chckFueraFacultades1')[0].show();	
							Ext.ComponentQuery.query('#chckFueraFacultades01')[0].setValue(false);		
							Ext.ComponentQuery.query('#chckFueraFacultades02')[0].setValue(true)	;							
						}else {
							Ext.ComponentQuery.query('#chckFueraFacultades1')[0].hide();
						}
					}
					
					if( ic_producto=='T'){
						Ext.ComponentQuery.query('#cmb_estatus1')[0].hide();
						Ext.ComponentQuery.query('#chckFueraFacultades1')[0].hide();
					}else {
						Ext.ComponentQuery.query('#cmb_estatus1')[0].show();
								
						if(tipoProceso=='Diversos') {	
							if( ic_producto=='1' || ic_producto =='1A' || ic_producto =='1B' || ic_producto =='1C' || ic_producto == '1D' 
								|| ic_producto=='0' || ic_producto =='0F' || ic_producto =='0C' || ic_producto =='0E' ) {
								Ext.ComponentQuery.query('#chckFueraFacultades1')[0].show();	
								Ext.ComponentQuery.query('#chckFueraFacultades01')[0].setValue(false);		
								Ext.ComponentQuery.query('#chckFueraFacultades02')[0].setValue(true)	;
							}else {
								Ext.ComponentQuery.query('#chckFueraFacultades1')[0].hide();
							}		
						}
					}		
					
				}
				
				
					if(pantalla== ''){							
						Ext.ComponentQuery.query('#ic_if1')[0].hide();
						Ext.ComponentQuery.query('#ic_nafin_electronico1')[0].hide();
						Ext.ComponentQuery.query('#cmb_estatus1')[0].hide();
						Ext.ComponentQuery.query('#chckFueraFacultades1')[0].hide();						
						
						forma.el.mask('Enviando...', 'x-mask-loading');			
							consultaData.load({
								params: Ext.apply(forma.getForm().getValues(),{
									informacion: 'ConsultaMonitor'							
								})
							});
					}	
						
				
				catalogoIF.load({
							params : Ext.apply(forma.getForm().getValues(),{
								ic_producto: ic_producto,
								tipoProceso:forma.query('#tipoProceso')[0].getValue()
							})
						});
						
						catalogoRechazo.load({
							params : Ext.apply(forma.getForm().getValues(),{	
								ic_producto: ic_producto,
								tipoProceso:forma.query('#tipoProceso')[0].getValue()
							})
						});
				}
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'Nombre del Intermediario',
			itemId: 'ic_if1',
			name: 'ic_if',
			hiddenName: 'ic_if',
			forceSelection: true,
			hidden: false, 
			width: 450,
			store: catalogoIF,
			emptyText: 'Seleccionar IF...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',						
			listeners: {
				select: function(obj, newVal, oldVal){						
				
				}
			}
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'Num. Elec. Pyme',		
			name: 'ic_nafin_electronico',
			id: 'ic_nafin_electronico1',
			allowBlank: true,
			maxLength: 11,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'combo',
			fieldLabel: 'Causas Rechazo',
			itemId: 'cmb_estatus1',
			name: 'cmb_estatus',
			hiddenName: 'cmb_estatus',
			forceSelection: true,
			hidden: false, 
			width: 450,
			store: catalogoRechazo,
			emptyText: 'Todas las Causas...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',	
			hidden:true			
		},
		{
			xtype:	'radiogroup',
			fieldLabel: "Operaciones fuera de facultades",
			name: 'chckFueraFacultades',   
			id: 'chckFueraFacultades1',
			hidden:true,
			columns:	[50, 50],
			items:[
				{	boxLabel: 'SI',	id:'chckFueraFacultades01',	name:'chckFueraFacultades',	inputValue:'S'},
				{	boxLabel: 'NO',	id:'chckFueraFacultades02',	name:'chckFueraFacultades',	inputValue:'N'}
			],
			listeners:{
				change: function(grupo, radio){
					
					var  tipoRadio01 =  Ext.ComponentQuery.query('#chckFueraFacultades01')[0].getValue();
					
					if(tipoRadio01==true) {						
						Ext.ComponentQuery.query('#cmb_estatus1')[0].hide();	
					}else  {						
						Ext.ComponentQuery.query('#cmb_estatus1')[0].show();	
					}
						
				}
			}
		},		
		
		{ 	xtype: 'textfield', hidden: true,  id: 'pantalla', 	value: '' },
		{ 	xtype: 'textfield', hidden: true,  id: 'tipoProceso', 	value: '' },
		{ 	xtype: 'textfield', hidden: true,  id: 'monitor', 	value: '' },
		{ 	xtype: 'textfield', hidden: true,  id: 'tituloTipoProceso', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'urlArchivo', 	value: '' }	
	];
	
	
	var forma = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'forma',
      width:	600,
		style: 'margin:0 auto;',		
		collapsible:	false,
		titleCollapse:	false,
		frame:			true,		
		 fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 160
		},
		title:'Interfases',		
		items	: elementosForma,
		
		buttons: [		
			{
				text: 'Consultar/Actualizar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
					
					var forma= Ext.ComponentQuery.query('#forma')[0];
					forma.el.mask('Enviando...', 'x-mask-loading');
					
					var  tipoRadio01 =  Ext.ComponentQuery.query('#chckFueraFacultades01')[0].getValue();
					var chckFueraFacultades ="N";
					if(tipoRadio01==true){ chckFueraFacultades= "S";  }
					
					consDetalleData.load({
						params: Ext.apply(forma.getForm().getValues(),{
							informacion: 'ConsultaDetalle',
							chckFueraFacultades:chckFueraFacultades,
							pantalla: forma.query('#pantalla')[0].getValue(),
							tipoProceso:forma.query('#tipoProceso')[0].getValue(),
							monitor:forma.query('#monitor')[0].getValue()
						})
					});
				}
			}
		]	
	});
	
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 920,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(20),	
			forma,			
			gridProcesados,
			mensajeA,
			NE.util.getEspaciador(20),
			gridDetalle,
			NE.util.getEspaciador(20),
			comboDescripcion
		]
	});
	
	catalogoProducto.load({
		params: {
			ic_producto:ic_producto		
		}	
	});
	
	catalogoDescripcion.load();
	
	
	Ext.Ajax.request({
		url : '15monproc.data.jsp',
		params: {
			informacion: "valoresIniciales",
			ic_producto:ic_producto,
			tipoProceso:tipoProceso,
			monitor:monitor,
			tituloTipoProceso:tituloTipoProceso
		},
		callback: procesaValoresIniciales
	});
});	
