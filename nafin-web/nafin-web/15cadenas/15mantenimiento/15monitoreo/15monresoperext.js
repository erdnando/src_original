Ext.onReady(function() {


function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
//******************CONTULTA********************************************
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
						
			var jsonData = store.reader.jsonData;		
			var cm = gridConsulta.getColumnModel();			
			var el = gridConsulta.getGridEl();
			
			if(jsonData.ic_producto =='8') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MODALIDAD'), false);	
			}else {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MODALIDAD'), true);		
			}
			
			if(store.getTotalCount() > 0) {	
				gridTotales.show();
				el.unmask();					
			} else {				
				gridTotales.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');		
				
			}
		}
	}	

	var consTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15monresoper.data.jsp',
		baseParams: {
			informacion: 'ConsultaTotales'
		},		
		fields: [	
			{  name: 'VACIO'},	
			{  name: 'MONEDA'},	
			{  name: 'MONTO_DOCTO'},	
			{  name: 'MONTO_OPERADO'},	
			{  name: 'TOTAL_SOLICITUDES'}		
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);									
				}
			}
		}		
	});	
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 230,			
				resizable: true,					
				align: 'left'				
			},	
			{
				header: '',
				tooltip: '',
				dataIndex: 'VACIO',
				sortable: true,
				width: 300,			
				resizable: true,					
				align: 'left'				
			},	
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 120,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{
				header: 'Monto Operado',
				tooltip: 'Monto Operado',
				dataIndex: 'MONTO_OPERADO',
				sortable: true,
				width: 120,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{
				header: 'Total de Solicitudes',
				tooltip: 'Total de Solicitudes',
				dataIndex: 'TOTAL_SOLICITUDES',
				sortable: true,
				width: 120,			
				resizable: true,					
				align: 'center'				
			}			
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 100,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			id: 'barraPaginaD',				
			items: [	
				'-',
				'->',
				{
					text: 'Imprimir',
					iconCls: 'icoPdf',
					xtype: 'button',
					id: 'btnImprimir',					
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '15monresoper.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Archivos'	,							
								tipoArchivo:'PDF'
							}),
							callback: procesarArchivos
						});
					}
				},
				{
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					xtype: 'button',
					id: 'btnGenerarArch',					
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '15monresoper.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Archivos',								
								tipoArchivo:'CSV'
							}),
							callback: procesarArchivos
						});
					}
				}			
			]
		}
	});
	
	//consulta General 
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15monresoper.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},		
		fields: [	
			{  name: 'MODALIDAD'},
			{  name: 'CLAVE_IF'},
			{  name: 'NOMBRE_IF'},	
			{  name: 'ESTATUS'},
			{  name: 'MONEDA'},	
			{  name: 'MONTO_DOCTO'},	
			{  name: 'MONTO_OPERADO'},	
			{  name: 'TOTAL_SOLICITUDES'}		
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});	

	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Modalidad',
				tooltip: 'Modalidad',
				dataIndex: 'MODALIDAD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},	
			{
				header: 'Clave del IF',
				tooltip: 'Clave del IF',
				dataIndex: 'CLAVE_IF',
				sortable: true,
				width: 80,			
				resizable: true,					
				align: 'center'				
			},	
			{
				header: 'Raz�n Social',
				tooltip: 'Raz�n Social',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},	
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},	
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 120,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{
				header: 'Monto Operado',
				tooltip: 'Monto Operado',
				dataIndex: 'MONTO_OPERADO',
				sortable: true,
				width: 120,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{
				header: 'Total de Solicitudes',
				tooltip: 'Total de Solicitudes',
				dataIndex: 'TOTAL_SOLICITUDES',
				sortable: true,
				width: 120,			
				resizable: true,					
				align: 'center'
			}			
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 220,
		width: 900,
		align: 'center',
		frame: false
	});
	

	//******************CRITERIOS DE BUSQUEDA********************************************
	var catalogoProducto = new Ext.data.JsonStore({
		id: 'catalogoProducto',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15monresoper.data.jsp',
		baseParams: {
			informacion: 'catalogoProducto'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15monresoper.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15monresoper.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var elementosForma = [
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			name: 'ic_producto',
			id: 'ic_producto1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_producto',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			forceSelection : true,
			store: catalogoProducto,
			listeners: {
				select: {
					fn: function(combo) {	
						if(combo.getValue()!='') {
							var cmbIF = Ext.getCmp('ic_if1');
							cmbIF.setValue('');
							cmbIF.store.load({
								params: {
									ic_producto:combo.getValue()
								}
							});	
							
							var cmbEstatus = Ext.getCmp('ic_estatus1');
							cmbEstatus.setValue('');
							cmbEstatus.store.load({
								params: {
									ic_producto:combo.getValue()
								}
							});	
							
						}							
					}
				}
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero:',
			name: 'ic_if',
			id: 'ic_if1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_if',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			forceSelection : true,
			store: catalogoIF			
		},
		{
			xtype: 'combo',
			fieldLabel: 'Estatus',
			name: 'ic_estatus',
			id: 'ic_estatus1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_estatus',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			forceSelection : true,
			store: catalogoEstatus			
		}	
	];
	
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Criterios de Busqueda ',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,							
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {					
		
					var ic_producto = Ext.getCmp('ic_producto1');
		
					if (Ext.isEmpty(ic_producto.getValue()) ){
						ic_producto.markInvalid('Seleccione un Producto');
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consulta'							
						})
					});	
					
					consTotalesData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ConsultaTotales'							
						})
					});	
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15monresoperext.jsp';					
				}
			}
		]
	});
	
	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: 'Concluidos por IF',			
				id: 'btnCons1',					
				handler: function() {
					window.location = '15monconcifext.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Monitor ',			
				id: 'btnCap',					
				handler: function() {
					window.location = '15monprocext.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',				
				text: '<h1><b>Resumen de Operaciones</h1></b>',
				id: 'btnCons2',					
				handler: function() {
					window.location = '15monresoperext.jsp';
				}
			}
		]
	};
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fpBotones,
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),
			gridConsulta, 
			gridTotales,
			NE.util.getEspaciador(20)			
		]
	});

	catalogoProducto.load();
	catalogoIF.load();
	catalogoEstatus.load();
	
});	
