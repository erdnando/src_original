
Ext.onReady(function() {

	//****************GRID DE REGISTROS PROCESADOS **********************************	
	var procesarSuccessFailureGenArchivo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('btnGenerar');
			boton.setIconClass('icoXls');
			boton.enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	//******************* CONSULTA  PRINCIPAL **************************************
	var procesarConsData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			var jsonData = store.reader.jsonData;		
			var cm = gridConsulta.getColumnModel();			
			var el = gridConsulta.getGridEl();
			var barraPagina = Ext.getCmp('barraPagina');								
			
			if(store.getTotalCount() > 0) {	
				Ext.getCmp('btnGenerar').enable();
				el.unmask();					
			} else {			
				Ext.getCmp('btnGenerar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15canccontbExt.data.jsp',
		baseParams: {
			informacion: 'Consultar',
			operacion: 'Generar',
			start: 0,
			limit: 15
		},		
		fields: [				
			{  name: 'CLAVEPRODUCTO'},
			{  name: 'NOMBREPRODUCTO'},
			{  name: 'INTERMEDIARIO'},
			{  name: 'EPO'},
			{  name: 'PYME'},
			{  name: 'NOSOLICITUD'},
			{  name: 'MONEDA'},
			{  name: 'TIPOFACTORAJE'},
			{  name: 'DFOPERACION'},
			{  name: 'NUMEROPRESTAMO'},
			{  name: 'MONTOOPERAR'},			
			{  name: 'ESTATUSACTUAL'},			
			{  name: 'FECHACANCELACION'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
										Ext.apply(options.params, {ic_producto: Ext.getCmp('ic_producto1').getValue()});
									}
							},
			load: procesarConsData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsData(null, null, null);					
				}
			}
		}		
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta de Cancelaciones X Contingencia',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: '<center>Producto</center>',
				tooltip: 'Producto',
				dataIndex: 'NOMBREPRODUCTO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},
			{
				header: '<center>Intermediario</center>',
				tooltip: 'Intermediario',
				dataIndex: 'INTERMEDIARIO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: '<center>EPO</center>',
				tooltip: 'EPO',
				dataIndex: 'EPO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: '<center>PYME</center>',
				tooltip: 'PYME',
				dataIndex: 'PYME',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'No. Solicitud',
				tooltip: 'No. Solicitud',
				dataIndex: 'NOSOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: '<center>Moneda</center>',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},
			{
				header: 'Tipo de Factoraje',
				tooltip: 'Tipo de Factoraje',
				dataIndex: 'TIPOFACTORAJE',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Fecha Operaci�n',
				tooltip: 'Fecha Operaci�n',
				dataIndex: 'DFOPERACION',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'No. de Pr�stamo',
				tooltip: 'No. de Pr�stamo',
				dataIndex: 'NUMEROPRESTAMO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTOOPERAR',
				sortable: true,
				width: 150,			
				resizable: true,					
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Estatus Actual',
				tooltip: 'Estatus Actual',
				dataIndex: 'ESTATUSACTUAL',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Fecha de Cancelaci�n',
				tooltip: 'Fecha de Cancelaci�n',
				dataIndex: 'FECHACANCELACION',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			}
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,	
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",		
			items: [	
				'->',
				'-',
				{
					text: 'Generar',
					iconCls: 'icoXls',
					xtype: 'button',
					id: 'btnGenerar',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15canccontbExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								tipo: 'CSV'
							}),
							callback: procesarSuccessFailureGenArchivo
						});
					}
				}
			]
		}
	});
	//***************Criterios de Busqueda*****************
	var catalogoProducto = new Ext.data.JsonStore({
		id: 'catalogoProducto',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15canccontbExt.data.jsp',
		baseParams: {
			informacion: 'catalogoProducto'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoIntermediario = new Ext.data.JsonStore({
		id: 'catalogoIntermediario',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15canccontbExt.data.jsp',
		baseParams: {
			informacion: 'catalogoIntermediario'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	

	var elementosForma =  [
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			name: 'ic_producto',
			id: 'ic_producto1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_producto',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			allowBlank		: true,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoProducto,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120
		},
		{
			xtype: 'combo',
			fieldLabel: 'Intermediario',
			name: 'ic_intermediario',
			id: 'cmbIntermadiario',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_intermediario',
			emptyText: 'Seleccionar...',
			allowBlank		: true,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoIntermediario,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120	
		},
		{
			xtype: 'compositefield',			
			fieldLabel: 'N�mero de Pr�stamo ',
			combineErrors: false,
			msgTarget: 'side',		
			items: [	
				{
				id : 'numeroPrestamo',	
				xtype : 'numberfield',
				name:'numPrestamo',
				width:       100,		
				labelWidth:   150,
				labelStyle: 'font-family: Arial, Helvetica, sans-serif; font-size: 11px;',
				defaultType:  'textfield',
				layout:       'form',
				border:       false,
				fieldLabel : 'N�mero de Pr�stamo'										
			}
			]
		},			
		{
			xtype: 'datefield',
			name: 'df_operacion',
			fieldLabel: 'Fecha de Operaci�n',
			id: 'df_operacion',
			allowBlank: true,
			editable: true,
			startDay: 0,
			width: 100,
			anchor: '47%',
			msgTarget: 'side',							
			margins: '0 20 0 0' //necesario para mostrar el icono de error
		},
		{ 	xtype: 'textfield', hidden: true,  id: 'solicitudes', 	value: '' },
		{
			xtype: 'datefield',
			name: 'df_cancelacion',
			fieldLabel: 'Fecha de Cancelaci�n',
			id: 'df_cancelacion',
			allowBlank: true,
			startDay: 0,
			width: 100,
			anchor: '47%',
			msgTarget: 'side',							
			margins: '0 20 0 0' //necesario para mostrar el icono de error
		},
		{ 	xtype: 'textfield', hidden: true,  id: 'solicitudes1', 	value: '' }
	];
	
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: 'margin:0 auto;',
		title: ' ',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 160,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
				var ic_producto = Ext.getCmp("ic_producto1");
					if (Ext.isEmpty(ic_producto.getValue()) ){
						ic_producto.markInvalid('Debe de seleccionar un Producto');
						return;
					}
					var df_operacion = Ext.getCmp("df_operacion");	
					var df_operacion_ = Ext.util.Format.date(df_operacion.getValue(),'d/m/Y');
					var df_cancelacion = Ext.getCmp("df_cancelacion");	
					var df_cancelacion_ = Ext.util.Format.date(df_cancelacion.getValue(),'d/m/Y');
					if(!Ext.isEmpty(df_operacion.getValue())){
						if(!isdate(df_operacion_)) { 
							df_operacion.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_operacion.focus();
							return;
						}
					}
					if(!Ext.isEmpty(df_cancelacion.getValue())){
						if(!isdate(df_cancelacion_)) { 
							df_cancelacion.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_cancelacion.focus();
							return;
						}
					}
					var ig_numero_prestamo = Ext.getCmp("numeroPrestamo").getValue();
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion : 'Consultar',
							start:0,
							limit:15		
						})
					});
					
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',				
				handler: function(boton, evento) {	
						document.location.href  = 	'15canccontbExt.jsp';
				}
			}
		]
	});	

	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [			
			{
				xtype: 'button',				
				text: 'Captura',	
				id: 'btnCap',					
				handler: function() {
					window.location = '15canccontExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<h1><b>Consulta</h1></b>',			
				id: 'btnCons2',					
				handler: function() {
					window.location = '15canccontbExt.jsp';
				}
			}
		]
	};
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fpBotones,
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,	
			NE.util.getEspaciador(20)
		]
	});
	catalogoProducto.load();
	catalogoIntermediario.load();

});	
