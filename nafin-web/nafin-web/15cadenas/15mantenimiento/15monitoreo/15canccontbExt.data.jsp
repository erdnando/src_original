<%@ page contentType="application/json;charset=UTF-8"
	import="
		com.netro.model.catalogos.*,
		java.util.*,  
		com.netro.cadenas.*,
		java.sql.*,
		com.netro.descuento.*,
		netropology.utilerias.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"				
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String ic_producto = (request.getParameter("ic_producto") != null)?request.getParameter("ic_producto"):"";
	String ic_intermediario = (request.getParameter("ic_intermediario") != null)?request.getParameter("ic_intermediario"):"";
	String df_operacion = (request.getParameter("df_operacion") != null)?request.getParameter("df_operacion"):"";
	String df_cancelacion = (request.getParameter("df_cancelacion") != null)?request.getParameter("df_cancelacion"):"";
	String numPrestamo = (request.getParameter("numPrestamo") != null)?request.getParameter("numPrestamo"):"";
	String solicitudes = (request.getParameter("solicitudes") != null)?request.getParameter("solicitudes"):"";
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 

		
	JSONObject jsonObj = new JSONObject();
	String infoRegresar = "", consulta = "",  nombreArchivo ="";
	int numRegistros=0;
	int  start= 0, limit =0;
	
	ConsultaCanceXConti2  paginador = new ConsultaCanceXConti2();	
	paginador.setIc_producto(ic_producto);
	paginador.setIc_if(ic_intermediario);
	paginador.setIg_numero_prestamo(numPrestamo);
	paginador.setDf_operacion(df_operacion);
	paginador.setDf_cancelacion(df_cancelacion);
	paginador.setSolicitudes(solicitudes);	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	
if(informacion.equals("catalogoProducto")) {
	
	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_producto_nafin");
	catalogo.setCampoDescripcion("ic_nombre");
	catalogo.setTabla("comcat_producto_nafin");
	catalogo.setValoresCondicionIn("0,1,4", Integer.class);
	catalogo.setOrden("ic_nombre");
	infoRegresar = catalogo.getJSONElementos();
	
} else  if(informacion.equals("catalogoIntermediario")) {

	List  registros =  this.getListIntermediario();
	jsonObj.put("registros", registros);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();

} else  if(informacion.equals("Consultar")  || informacion.equals("ArchivoCSV")  ) {

	if( informacion.equals("Consultar")    ) {
		try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
										
		} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
			throw new AppException("Error en la consulta", e);
		}
		
		infoRegresar = jsonObj.toString();
	
	} else if (informacion.equals("ArchivoCSV")) {
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
} 
	
%>
<%=infoRegresar%>

<%! 

	
	public List   getListIntermediario() throws AppException { 
		System.out.println("   getListIntermediario (S) ");		
		StringBuffer	qrySentencia 	= new StringBuffer();		
		List  lVarBind		= new ArrayList();
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		AccesoDB con = new AccesoDB(); 		
		boolean exito = true;
		HashMap	datos = new HashMap();	
		List  registros		= new ArrayList();
		try{
			con.conexionDB();
			qrySentencia 	= new StringBuffer();	
			qrySentencia.append(" SELECT ic_if AS CLAVE, cg_razon_social AS DESCRIPCION    "+
			" FROM comcat_if  "+
			" WHERE cs_habilitado = 'S'   "+
			" ORDER BY cg_razon_social ASC   "); 
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();			
			while(rs.next()){
			   datos = new HashMap();
				datos.put("clave", rs.getString("CLAVE")==null?"":rs.getString("CLAVE"));
				datos.put("descripcion", rs.getString("DESCRIPCION")==null?"":rs.getString("DESCRIPCION"));
				registros.add(datos); 
			}
			rs.close();
			ps.close();
		
		} catch (Exception e) {
			exito = false;
			e.printStackTrace();
			System.out.println(" getListIntermediario  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				System.out.println("   getListIntermediario (S) ");
			}
		} 
		return registros;
		
	}
	%>

	