Ext.onReady(function() {


//Funcion para validar la peticion Ajax y mostrar el usuario y fecha
	function procesaValoresIniciales(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if(jsonValoresIniciales != null){				
				Ext.getCmp('version').setValue(jsonValoresIniciales.version);
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}
	}
	
	function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var generarArchivo  = function (grid,rowIndex,colIndex,item){
		
		var registro = grid.getStore().getAt(rowIndex);
		var claveSolicitud = registro.get('FOLIO');
				
		Ext.Ajax.request({
			url: '15monconcif.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ArchivoVerOrigen',	
				claveSolicitud:claveSolicitud
			}),
			callback: procesarArchivos
		});
	}
	
	var verPrestamo = function (grid,rowIndex,colIndex,item){
	
		var registro = grid.getStore().getAt(rowIndex);
		var claveSolicitud = registro.get('FOLIO');
		var ic_producto = Ext.getCmp('ic_producto1').getValue();
				
		Ext.Ajax.request({
			url: '15monconcif.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ArchivoVerPrestamo',	
				ic_producto:ic_producto,
				claveSolicitud:claveSolicitud,
				interfase:'todos',
				logAdicional:''
			}),
			callback: procesarArchivos
		});			
	}
	

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;
			
			var barraPagina = Ext.getCmp('barraPagina');								
			Ext.getCmp('barraPagina').setText('Total Solicitudes Concluidas: '+jsonData.numRegistros);

			if(jsonData.ic_producto=='0'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FOLIO'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('INTERMEDIARIO'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_CLIENTE'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_CLIENTE_SIRAC'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESC'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_PRESTAMO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_PROYECTO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_CONTRATO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_OPERACION'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('ARCHIVO'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_EPO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_SOLICITUD'), true);		
			} 
			if(jsonData.ic_producto=='1' || jsonData.ic_producto=='2' || jsonData.ic_producto=='4' || jsonData.ic_producto=='5' || jsonData.ic_producto=='8' ){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('INTERMEDIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_CLIENTE'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_CLIENTE_SIRAC'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESC'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_PRESTAMO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_PROYECTO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_CONTRATO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_OPERACION'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_EPO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_SOLICITUD'), false);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FOLIO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('ARCHIVO'), true);
			}
			if( jsonData.ic_producto=='8' ){	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MODALIDAD'), false);
			}else {	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MODALIDAD'), true);
			}
			
			if(store.getTotalCount() > 0) {		
				el.unmask();
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
			} else {		
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15monconcif.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'FOLIO'},	
			{	name: 'INTERMEDIARIO'},	
			{	name: 'NOMBRE_CLIENTE'},	
			{	name: 'NUM_CLIENTE_SIRAC'},	
			{	name: 'MONTO_DESC'},	
			{	name: 'NUM_PRESTAMO'},	
			{	name: 'NUM_PROYECTO'},	
			{	name: 'NUM_CONTRATO'},	
			{	name: 'FECHA_OPERACION'},
			{  name: 'ARCHIVO' },
			{	name: 'NOMBRE_EPO'},
			{  name: 'NUM_SOLICITUD'},			
			{  name: 'MODALIDAD'}
			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	

	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Modalidad',
				tooltip: 'Modalidad',
				dataIndex: 'MODALIDAD',
				sortable: true,
				width: 150,			
				resizable: true,	
				hidden: true,
				align: 'left'				
			},			
			{
				header: 'Folio',
				tooltip: 'Folio',
				dataIndex: 'FOLIO',
				sortable: true,
				width: 150,			
				resizable: true,	
				hidden: true,
				align: 'center'				
			},
			{
				header: 'Intermediario',
				tooltip: 'Intermediario',
				dataIndex: 'INTERMEDIARIO',
				sortable: true,
				width: 150,			
				resizable: true,	
				hidden: true,
				align: 'left',	
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,	
				hidden: true,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Nombre Cliente',
				tooltip: 'Nombre Cliente',
				dataIndex: 'NOMBRE_CLIENTE',
				sortable: true,
				width: 150,			
				resizable: true,
				hidden: true,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'N�mero de solicitud',
				tooltip: 'N�mero de solicitud',
				dataIndex: 'NUM_SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,	
				hidden: true,
				align: 'center'	
			},
			{
				header: 'Num. de Cliente SIRAC',
				tooltip: 'Num. de Cliente SIRAC',
				dataIndex: 'NUM_CLIENTE_SIRAC',
				sortable: true,
				width: 150,			
				resizable: true,
				hidden: true,
				align: 'center'	
			},
			{
				header: 'Monto descuento',
				tooltip: 'Monto descuento',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				hidden: true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 'actioncolumn',
				header: 'N�mero de Pr�stamo',
				tooltip: 'N�mero de Pr�stamo',
				dataIndex: 'NUM_PRESTAMO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				hidden: true,
				renderer: function(value,metaData,registro,rowIndex,colIndex,store){
					return value;
				},
				items: [
					{
						getClass: function(value,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';						
						}
						,handler: verPrestamo
					}										
				]
			},
			{
				header: 'N�mero de Proyecto',
				tooltip: 'N�mero de Proyecto',
				dataIndex: 'NUM_PROYECTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				hidden: true,
				renderer: function(value,metaData,registro,rowIndex,colIndex,store){
					if(value==0){
						return '';
					}else  {
						return value;
					}
				}
			},
			{
				header: 'N�mero de Contrato',
				tooltip: 'N�mero de Contrato',
				dataIndex: 'NUM_CONTRATO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				hidden: true,
				renderer: function(value,metaData,registro,rowIndex,colIndex,store){
					if(value==0){
						return '';
					}else  {
						return value;
					}
				}
			},
			{
				header: 'Fecha de Operaci�n',
				tooltip: 'Fecha de Operaci�n',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				hidden: true
			},
			{
				xtype: 'actioncolumn',
				header: 'Ver Archivo Origen',
				tooltip: 'Ver Archivo Origen',
				sortable: true,
				dataIndex: 'ARCHIVO',
				align: 'center',
				width: 150,
				hidden: true,
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}
						,handler: generarArchivo
					}	
				]
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
			bbar: {
				xtype: 'toolbar',
				id: 'barraPagina',		
				items: [	
				{
					xtype: 'label',
					id:    'barraPagina',
					html:  'Total Solicitudes Concluidas:',
					style: 'font-weight:bold;padding-bottom:10pt;text-align:center'
				},
				'->','-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '15monconcif.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF'						
							}),
							callback: procesarArchivos
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',					
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '15monconcif.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'						
							}),
							callback: procesarArchivos
						});
					}
				}	
			]
		}
	});
	//**************************CRITERIOS DE BUSQUEDA*******************************+
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15monconcif.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoProducto = new Ext.data.JsonStore({
		id: 'catalogoProducto',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15monconcif.data.jsp',
		baseParams: {
			informacion: 'catalogoProducto'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma =  [
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			name: 'ic_producto',
			id: 'ic_producto1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_producto',
			emptyText: 'Seleccionar...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoProducto,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120
		},
		{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			name: 'ic_if',
			id: 'ic_if1',		
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_if',
			emptyText: 'Seleccionar IF...',
			autoLoad: false,
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoIF,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120
		},
		{
			xtype: 'compositefield',
			id: 'camposReportes',
			fieldLabel: 'Fecha de publicaci�n',
			combineErrors: false,			
			width: 500,
			items: [				
				{
					xtype: 'datefield',
					name: 'fechaOperacDe',
					id: 'fechaOperacDe',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecho: 'fechaOperacA',					
					margins: '0 20 0 0' //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 24
				},
				{
					xtype: 'datefield',
					name: 'fechaOperacA',
					id: 'fechaOperacA',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fechaOperacDe',					
					margins: '0 20 0 0' //necesario para mostrar el icono de error
				}
			]
		},
		{ 	xtype: 'textfield', hidden: true,  id: 'version', 	value: '' }
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Informaci�n de Solicitudes',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsulta',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function (boton, evento){
					var ic_producto = Ext.getCmp("ic_producto1");
					var ic_if = Ext.getCmp("ic_if1");	
					var fechaOperacDe = Ext.getCmp("fechaOperacDe");	
					var fechaOperacA = Ext.getCmp("fechaOperacA");	
					
					if (Ext.isEmpty(ic_producto.getValue()) ){
						ic_producto.markInvalid('Debe de seleccionar un Producto');
						return;
					}
									
					if (Ext.isEmpty(ic_if.getValue()) ){
						ic_if.markInvalid('Debe de seleccionar al Intermediario Financiero');
						return;
					}
					
					if(!Ext.isEmpty(fechaOperacDe.getValue()) || !Ext.isEmpty(fechaOperacA.getValue())){
						if(Ext.isEmpty(fechaOperacDe.getValue())){
							fechaOperacDe.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaOperacDe.focus();
							return;
						}
						if(Ext.isEmpty(fechaOperacA.getValue())){
							fechaOperacA.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaOperacA.focus();
							return;
						}
					}
					var fechaDe = Ext.util.Format.date(fechaOperacDe.getValue(),'d/m/Y');
					var fechaA = Ext.util.Format.date(fechaOperacA.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(fechaOperacDe.getValue())){
						if(!isdate(fechaDe)) { 
							fechaOperacDe.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fechaOperacDe.focus();
							return;
						}
					}
					if( !Ext.isEmpty(fechaOperacA.getValue())){						
						if(!isdate(fechaA)) { 
							fechaOperacA.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fechaOperacA.focus();
							return;
						}
					}
						
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'							
						})
					});
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				formBind: true,
				handler: function (boton, evento){
					window.location = '15monconcifext.jsp';
				}
			}
		]
	});
	
	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: '<h1><b>Concluidos por IF</h1></b>',			
				id: 'btnCons1',					
				handler: function() {
					window.location = '15monconcifext.jsp';					
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Monitor ',			
				id: 'btnCap',					
				handler: function() {
					window.location = '15monprocext.jsp';					
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Resumen de Operaciones',			
				id: 'btnCons2',					
				handler: function() {
					window.location = '15monresoperext.jsp';					
				}
			}
		]
	};

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fpBotones,
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
		//Funcion para obtener usuario y fecha mediante peticion Ajax
	var valIniciales = function(){
		Ext.Ajax.request({
			url: '15monconcif.data.jsp',
			params: {
						informacion: "valoresIniciales"
			},
			callback: procesaValoresIniciales
		});
	}
	valIniciales();
	catalogoIF.load();
	catalogoProducto.load();

});	