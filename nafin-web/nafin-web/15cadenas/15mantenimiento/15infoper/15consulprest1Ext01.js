
Ext.onReady(function() {
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		Ext.getCmp('btnArchivoPDF').enable();
		var btnImprimirPDF = Ext.getCmp('btnArchivoPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = store.reader.jsonData;
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
				
			}	
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnArchivoPDF').enable();
				el.unmask();
				storeMontoTotal.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsTotales'
					})
				});
			} else {	
				//Ext.getCmp('gridTotal').hide();
				Ext.getCmp('btnArchivoPDF').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}		
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15consulprest1Ext01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'CODIGO_EMPRESA'},
			{	name: 'CODIGO_AGENCIA'},	
			{	name: 'CODIGO_SUB_APLICACION'},
			{	name: 'CODIGO_LINEA_CREDITO'},	
			{	name: 'NUMERO_CONTRATO'},
			{	name: 'NUMERO_PRESTAMO'},
			{	name: 'CODIGO_FINANCIERA'},	
			{	name: 'NOMBRE'},
			{	name:	'CODIGO_BASE_OPERACION'},
			{	name:	'VALOR_INICIAL'},
			{	name: 'TIPO_SUBSIDIO'},
			{  name: 'CODIGO_VALOR_TASA_CARTERA'},
			{	name: 'CODIGO_TIPO_AMORTIZACION'},
			{	name: 'VALOR_PRESENTE'},
			{	name:	'TIPO_INTERES'},
			{	name: 'FECHA_APERTURA'},
			{	name: 'FECHA_ULTIMO_PAGO'},
			{	name: 'FECHA_PRIMER_PAGO_CAPITAL'},
			{	name: 'FECHA_PRIMER_PAGO_INTERES'},
			{	name: 'FECHA_PROXIMO_PAGO_CAPITAL'},
			{	name: 'FECHA_PROXIMO_PAGO_INTERES'},
			{ name: 'TOTAL_ADEUDO'},
			{ name: 'MONTO_TOTAL_ADEUDO'},
			{ name: 'NUMERO_REGISTROS'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotal');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
		
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}								
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Proyecto',
				tooltip: 'Proyecto',
				dataIndex: 'CODIGO_LINEA_CREDITO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Contrato',
				tooltip: 'Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Pr�stamo',
				tooltip: 'Pr�stamo',
				dataIndex: 'NUMERO_PRESTAMO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Intermediario',
				tooltip: 'Intermediario',
				dataIndex: 'NOMBRE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'left'
			},
			{
				header: 'Base de operaci�n',
				tooltip: 'Base de operaci�n',
				dataIndex: 'CODIGO_BASE_OPERACION',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Monto inicial',
				tooltip: 'Monto inicial',
				dataIndex: 'VALOR_INICIAL',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo subsidio',
				tooltip: 'Tipo subsidio',
				dataIndex: 'TIPO_SUBSIDIO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Grupo Tasa',
				tooltip: 'Grupo Tasa',
				dataIndex: 'CODIGO_VALOR_TASA_CARTERA',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},{
				header: 'Tipo Amortizaci&oacute;n',
				tooltip: 'Tipo Amortizaci&oacute;n',
				dataIndex: 'CODIGO_TIPO_AMORTIZACION',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Valor presente',
				tooltip: 'Valor presente',
				dataIndex: 'VALOR_PRESENTE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Inter&eacute;s Compuesto',
				tooltip: 'Inter&eacute;s Compuesto',
				dataIndex: 'TIPO_INTERES',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Fecha Operaci&oacute;n',
				tooltip: 'Fecha Operaci&oacute;n',
				dataIndex: 'FECHA_APERTURA',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Fecha &uacute;ltimo pago',
				tooltip: 'Fecha &uacute;ltimo pago',
				dataIndex: 'FECHA_ULTIMO_PAGO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: '1er. pago capital',
				tooltip: '1er. pago capital',
				dataIndex: 'FECHA_PRIMER_PAGO_CAPITAL',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
				
			},
			{
				header: '1er. pago inter&eacute;s',
				tooltip: '1er. pago inter&eacute;s',
				dataIndex: 'FECHA_PRIMER_PAGO_INTERES',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
				
			},
			{
				header: 'Prox. pago capital',
				tooltip: 'Prox. pago capital',
				dataIndex: 'FECHA_PROXIMO_PAGO_CAPITAL',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
				
			},
			{
				header: 'Prox. pago inter&eacute;s',
				tooltip: 'Prox. pago inter&eacute;s',
				dataIndex: 'FECHA_PROXIMO_PAGO_INTERES',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
				
			},
			{
				header: 'Total adeudo',
				tooltip: 'Total adeudo',
				dataIndex: 'TOTAL_ADEUDO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
				
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 430,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '15consulprest1Ext01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoPDF'
							}),
							callback: procesarDescargaArchivos
						});						
					}
				}
			]
		}
	});
	var storeMontoTotal = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15consulprest1Ext01.data.jsp',
		baseParams: {
			informacion: 'ConsTotales'
		},		
		fields: [			
			{	name: 'monto_total'},
			{	name: 'total_prestamo'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {	
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);									
					load: procesarConsultaTotalesData(null,null,null);
				}
			}
		}		
	});
		var gridTotal = new Ext.grid.EditorGridPanel({
		id: 'gridTotal',
		title: 'Totales',
		store: storeMontoTotal,
		margins: '20 0 0 0',				
		
		hidden: true,
		columns:[			
			
			{
				header: '<div align="center">Total Pr�stamos</div>',
				dataIndex: 'total_prestamo',   
				sortable: true,
				width: 100,
				align: 'center'
			},{
				header: '<div align="center">Monto total</div>',
				dataIndex: 'monto_total',
				sortable: true,
				width: 200,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 100,
		width: 350,
		style: 'margin:0 auto;',
		frame: true
	});
	var elementosForma=[
		{
			xtype: 'compositefield',
			fieldLabel: 'Cliente',
			combineErrors: false,
			msgTarget: 'side',
			width: 100,
			items: [				
				{
					xtype: 'textfield',
					fieldLabel: 'Cliente',
					name: 'codigoCliente',
					id: 'tfCodigoCliente',
					allowBlank: true,
					maxLength: 12,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
					
				}
			]	
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Pr�stamo',
			combineErrors: false,
			msgTarget: 'side',
			width: 100,
			items: [				
				{
					xtype: 'textfield',
					fieldLabel: 'Pr�stamo',
					name: 'numeroPrestamo',
					id: 'tfNumeroPrestamo',
					allowBlank: true,
					maxLength: 12,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'	
					
				}
			]	
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 300,
		title: 'Consulta Pr�stamo',
		frame: true,
		collapsible: true,
		aling: 'center',
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,	
				tabIndex			: 7,
				handler: function(boton, evento) {
					var valCliente = Ext.getCmp('tfCodigoCliente');
					var valPrestamo = Ext.getCmp('tfNumeroPrestamo');
					if(!Ext.isEmpty(valCliente.getValue())){
						if (!isdigit(valCliente.getValue()))
						{
							valCliente.markInvalid("El valor no es un n�mero v�lido.");
							return ;
						}
					}
					if(!Ext.isEmpty(valPrestamo.getValue())){
						if (!isdigit(valPrestamo.getValue()))
						{
							valPrestamo.markInvalid("El valor no es un n�mero v�lido.");
							return ;
						}
					}
					fp.el.mask('Procesando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						//	operacion : 'Generar',
							informacion : 'Consultar'
						})
						
					}); 
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				tabIndex			: 8,
				handler: function() {
					Ext.getCmp('forma').getForm().reset();
					gridConsulta.hide();
					gridTotal.hide();
					
				}
			}
		]
	});
	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 950,		
		height: 'auto',
		style: 'margin:0 auto;',
		aling: 'center',
		items: [					
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),
			gridTotal
					
		]
	});
});	
