<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.usuarios.*,
		javax.naming.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.descuento.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  			
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	PagosIFNB pagosIFNB = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	
	String intemediarioFinanciero=(request.getParameter("intemediarioFinanciero") == null) ? "" : request.getParameter("intemediarioFinanciero");
	String numIntermediario = (request.getParameter("no_intermediario") == null) ? "" : request.getParameter("no_intermediario");
	String ic_if = (request.getParameter("comboIntermediario") == null) ? "" : request.getParameter("comboIntermediario");
	String moneda = (request.getParameter("comboMoneda") == null) ? "" : request.getParameter("comboMoneda");
	String fechaCorte = (request.getParameter("catalogoFechaCorte") == null) ? "" : request.getParameter("catalogoFechaCorte");
	String tipoLinea = (request.getParameter("tipoLinea") == null) ? "" : request.getParameter("tipoLinea");
	
	String observaciones=(request.getParameter("observaciones") == null) ? "" : request.getParameter("observaciones");
	
	String tipo=(request.getParameter("tipo") == null) ? "" : request.getParameter("tipo");
	String icIf= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
	String numSirac = "";
	
	if("C".equals(tipoLinea)){
		if(!"".equals(ic_if)){
			if(!"CE".equals(tipo)){
				HashMap hmSirac = pagosIFNB.getSiracFinaciera(ic_if);
				numSirac = hmSirac.get("NUMERO_SIRAC")!=null?((String)hmSirac.get("NUMERO_SIRAC")):""; 
			}else{
				numSirac = pagosIFNB.getSiracLineaCredito(ic_if);
			}
		}else if(!"".equals(numIntermediario)){
			numSirac = numIntermediario;
		}
	}
	
	
	CedulaConciliacionObservaciones clase = new CedulaConciliacionObservaciones();
	int  start= 0, limit =0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	
if(informacion.equals("catalogoIntermediario")){
	
	clase.setTipo(tipo);
	clase.setTipoLinea(tipoLinea);
	Registros catInter= clase.getDataCatalogoIntermediario();
	jsonObj.put("total",new Integer( catInter.getNumeroRegistros()) );	
	jsonObj.put("registros", catInter.getJSONData() );	
   infoRegresar = jsonObj.toString();	

}else if (informacion.equals("catalogoFechaCorte")) {
	
	clase.setTipo(tipo);
	clase.setIcIf(ic_if);
	clase.setMoneda(moneda);
	clase.setTipoLinea(tipoLinea);
	
	if(!"C".equals(tipoLinea)){
	clase.setNumIntermediario(numIntermediario);
	}else{
		clase.setNumSirac(numSirac);
	}
	
	Registros catFechaCorte= clase.getDatosFechaCorte();
	jsonObj.put("total",new Integer( catFechaCorte.getNumeroRegistros()) );	
	jsonObj.put("registros", catFechaCorte.getJSONData() );	
   infoRegresar = jsonObj.toString();	

}else if(informacion.equals("catalogoMoneda")){
	Registros catMoneda= clase.catalogoMoneda();
	
	jsonObj.put("total",new Integer( catMoneda.getNumeroRegistros()) );	
	jsonObj.put("registros", catMoneda.getJSONData() );	
   infoRegresar = jsonObj.toString();	
}else if(informacion.equals("Consultar_")){
	
	
	PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB",PagosIFNB.class);
	
			Vector vecFilas = null;
		if(informacion.equals("Consultar_")  )  {
			try {
				
			vecFilas = bean.getEstadoCuenta(fechaCorte,moneda,ic_if,numIntermediario, tipoLinea, tipo, numSirac );
				jsonObj.put("total",new Integer( vecFilas.size()) );	
				jsonObj.put("registros", vecFilas.toArray() );	
				infoRegresar = jsonObj.toString();
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		infoRegresar = jsonObj.toString();
	}	
}else if(informacion.equals("Consultar_Observaciones")){
	String ic_if_1 = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");//Cuando no se selecciona del combo intermediario
	try{
	if(!"".equals(ic_if)){
		clase.setIcIf(ic_if);
		
	}else{
		clase.setIcIf(ic_if_1);
	}
	
		clase.setFechaCorte(fechaCorte);
		clase.setMoneda(moneda);
	clase.setNumSirac(numSirac);
	clase.setTipoLinea(tipoLinea);
	Registros dataObservaciones= clase.getObservaciones();
	jsonObj.put("observaciones", dataObservaciones );	
	}catch(Exception e){
		throw new AppException("Error en los parametros recibidos", e);
	}
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("Guardar")){
	String ic_if_1 = "";//(request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");//Cuando no se selecciona del combo intermediario
	
	PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB",PagosIFNB.class);

	observaciones = (observaciones.equals(""))?" ":observaciones;
	if(!"".equals(ic_if)){
		bean.guardaDatosOBS(fechaCorte,moneda,ic_if,observaciones, numSirac);
	}else{
		if(!"C".equals(tipoLinea)){
		//OBTENENOS EL IC_IF
		clase.setIcIf(ic_if);
		clase.setNumIntermediario(numIntermediario);
		Registros regClaveIF= clase.getClaveIF();
		while(regClaveIF.next()){
			ic_if_1 = (regClaveIF.getString("IC_IF")!=null)?regClaveIF.getString("IC_IF"):"";
		}
		
		}else{
			ic_if_1="12";
		}
		bean.guardaDatosOBS(fechaCorte,moneda,ic_if_1,observaciones, numSirac);
	}
		jsonObj.put("mensaje",new String( "Datos guardados satisfactoriamente") );
	
	infoRegresar = jsonObj.toString();
}

%>


<%=infoRegresar%>

