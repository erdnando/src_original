
Ext.onReady(function() {
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		Ext.getCmp('btnArchivoPDF').enable();
		var btnImprimirPDF = Ext.getCmp('btnArchivoPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = store.reader.jsonData;
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
				
			} 	
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnArchivoPDF').enable();
				el.unmask();
				consTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsTotales'
					})
				});
				
			} else {	
				Ext.getCmp('gridTotal').hide();
				Ext.getCmp('btnArchivoPDF').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotal');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
		
			if (!gridTotales.isVisible()) {
				gridTotales.show();
				
				
			}								
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15consulproy1Ext01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'NUMERO_LINEA_FONDEO'},//numero_linea_fondeo
			{	name: 'CODIGO_AGENCIA'},	
			{	name: 'CODIGO_SUB_APLICACION'},
			{	name: 'NUMERO_LINEA_CREDITO'},
			{	name: 'NOMBRE'},
			{	name: 'CODIGO_TIPO_LINEA'},
			{	name: 'FECHA_APERTURA'},
			{	name: 'FECHA_DE_VCMTO'},
			{	name: 'CODIGO_PRODUCTO_BANCO'},
			{	name: 'DESCRIPCION'},
			{	name:	'CODIGO_BASE_OPERACION'},
			{	name: 'MONTO_ASIGNADO'},
			{	name: 'MONTO_COMPROMETIDO'},
			{	name: 'MONTO_DISPONIBLE'},
			{	name: 'MONTO_UTILIZADO'},
			{	name: 'ESTADO_CONTRATO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'L&iacute;nea de Fondeo',
				tooltip: 'L&iacute;nea de Fondeo',
				dataIndex: 'NUMERO_LINEA_FONDEO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Proyecto',
				tooltip: 'Proyecto',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){                                
					return registro.data['CODIGO_AGENCIA']+'-'+registro.data['CODIGO_SUB_APLICACION']+'-'+registro.data['NUMERO_LINEA_CREDITO'];
				}
			},
			{
				header: 'Intermediario',
				tooltip: 'Intermediario',
				dataIndex: 'NOMBRE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'left'
			},
			{
				header: 'Tipo de Proyecto',
				tooltip: 'Tipo de Proyecto',
				dataIndex: 'CODIGO_TIPO_LINEA',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Fecha de Apertura',
				tooltip: 'Fecha de Apertura',
				dataIndex: 'FECHA_APERTURA',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_DE_VCMTO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Producto Banco',
				tooltip: 'Producto Banco',
				dataIndex: 'TIPO_SUBSIDIO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){                                
					return registro.data['CODIGO_PRODUCTO_BANCO']+' '+registro.data['DESCRIPCION'];
				}
			},
			{
				header: 'Base operaci&oacute;n',
				tooltip: 'Base operaci&oacute;n',
				dataIndex: 'CODIGO_BASE_OPERACION',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},{
				header: 'Monto Asignado',
				tooltip: 'Monto Asignado',
				dataIndex: 'MONTO_ASIGNADO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Comprometido',
				tooltip: 'Monto Comprometido',
				dataIndex: 'MONTO_COMPROMETIDO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Disponible',
				tooltip: 'Monto Disponible',
				dataIndex: 'MONTO_DISPONIBLE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Utilizado',
				tooltip: 'Monto Utilizado',
				dataIndex: 'MONTO_UTILIZADO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Estado',
				tooltip: 'Estado',
				dataIndex: 'ESTADO_CONTRATO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 430,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '15consulproy1Ext01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoPDF'
							}),
							callback: procesarDescargaArchivos
						});						
					}
				}
			]
		}
	});
	
	var consTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15consulproy1Ext01.data.jsp',
		baseParams: {
			informacion: 'ConsTotales'
		},		
		fields: [			
			{	name: 'monto_asignado'},
			{	name: 'monto_comprometido'},
			{	name: 'montoTotal'},
			{	name: 'totalProyecto'},
			{	name: 'monto_disponible'},
			{	name: 'monto_utilizado'},
			{	name: 'num_registros'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {	
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);									
					load: procesarConsultaTotalesData(null,null,null);
				}
			}
		}		
	});

	var gridTotal = new Ext.grid.EditorGridPanel({
		xtype: 'grid',
		id: 'gridTotal',
		store: consTotalesData,
		title: 'Totales',
		margins: '20 0 0 0',				
		align: 'center',
		hidden: true,
		columns:[	
			{
				header: 'Total Proyectos',
				dataIndex: 'totalProyecto',
				width: 100,
				align: 'center'
			},
			{
				header: 'Monto Asignado',
				dataIndex: 'monto_asignado',
				sortable: true,
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Comprometido',
				dataIndex: 'monto_comprometido',
				sortable: true,
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Disponible',
				dataIndex: 'monto_disponible',
				sortable: true,
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Utilizado',
				dataIndex: 'monto_utilizado',
				sortable: true,
				width: 100,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 150,
		width: 770,
		style: 'margin:0 auto;',
		frame: true
	});
	var elementosForma=[
		{
			xtype: 'compositefield',
			fieldLabel: 'Cliente',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [				
				{
					xtype: 'textfield',
					fieldLabel: 'Cliente',
					name: 'codigoCliente',
					id: 'tfCodigoCliente',
					allowBlank: true,
					maxLength: 12,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
					
				}
			]	
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Proyecto',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [				
				{
					xtype: 'textfield',
					fieldLabel: 'Proyecto',
					name: 'codigoProyecto',
					id: 'tfCodigoProyecto',
					allowBlank: true,
					maxLength: 15,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'	
					
				}
			]	
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 300,
		title: 'Consulta Proyectos',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,	
				tabIndex			: 7,
				handler: function(boton, evento) {
					var proyecto  = Ext.getCmp('tfCodigoProyecto');
					var valCliente = Ext.getCmp('tfCodigoCliente');
					if(!Ext.isEmpty(proyecto.getValue())){
						var cadena = proyecto.getValue();
						var splitstring = cadena.split("-");
						if (Ext.isEmpty(proyecto.getValue()) || splitstring.length!=3 || !isdigit(splitstring[0]) || !isdigit(splitstring[1]) || !isdigit(splitstring[2]))
						{
							proyecto.markInvalid("El formato del proyecto no es adecuado\nFormato (agencia-sub aplicacion-proyecto): xxxx-xxx-xxx");
							return;
						}
					}
					if(!Ext.isEmpty(valCliente.getValue())){
						if (!isdigit(valCliente.getValue()))
						{
							valCliente.markInvalid("El valor no es un n�mero v�lido.");
							return ;
						}
					}
					fp.el.mask('Procesando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion : 'Generar',
							informacion : 'Consultar'
						})
						
					}); 
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				tabIndex			: 8,
				handler: function() {
					Ext.getCmp('forma').getForm().reset();	
					gridConsulta.hide();
					gridTotal.hide();
				}
			}
		]
	});
	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 950,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),
			gridTotal
					
		]
	});
	
});	
