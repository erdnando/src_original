<%@ page 
	contentType=
		"application/json;charset=UTF-8" 
	import="
		net.sf.json.JSONArray, net.sf.json.JSONObject, netropology.utilerias.*, com.netro.dispersion.*,
		com.netro.descuento.*,	java.util.*,java.sql.*, java.io.*, javax.naming.*, com.netro.model.catalogos.*,
		com.netro.cadenas.*,  org.apache.commons.logging.Log  " 
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>
<%    
String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String operacion		= (request.getParameter("operacion")	!=null)	?	request.getParameter("operacion")	:	"";
String infoRegresar	= "";
int start = 0, limit = 15;
PagosIFNB  beanPagos = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class); 
if (informacion.equals("catalogoint") )	{

	JSONObject		jsonObj 	= new JSONObject();
	CatalogoIF		cat 		= new CatalogoIF();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion(" ic_financiera || ' ' || cg_razon_social");
	List lis = cat.getListaElementosGral();
	jsonObj.put("registros", lis);	
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString();	

} else if (informacion.equals("catalogomon") )	{

	JSONObject		jsonObj 	= new JSONObject();
	CatalogoMoneda cat 		= new CatalogoMoneda();
   cat.setCampoClave("ic_moneda");
   cat.setCampoDescripcion("cd_nombre"); 
   List lis = cat.getListaElementos();  
	jsonObj.put("registros", lis);	
   infoRegresar = jsonObj.toString();

} else if (informacion.equals("catalogoFC") )	{
try {
	String 			intermediario	= (request.getParameter("intermediario")	!=null)	?	request.getParameter("intermediario")	:	"";
	String cliente	 	= (request.getParameter("_cli") 			!=null)	?	request.getParameter("_cli")			:	"";
	String tipoLinea	 	= (request.getParameter("tipoLinea") 			!=null)	?	request.getParameter("tipoLinea")			:	"";

	JSONObject				jsonObj 	= new JSONObject();
	CatalogoFechaCorte	cfc 		= new CatalogoFechaCorte();
	cfc.setIntermediario(intermediario);
	cfc.setTipoLinea(tipoLinea);
	cfc.setCliente(cliente);
	
	cfc.setCampoClave("DISTINCT TO_CHAR (com_fechaprobablepago, 'dd/mm/yyyy')");
	cfc.setCampoDescripcion("com_fechaprobablepago"); 
	cfc.setOrden("com_fechaprobablepago DESC");
	
	List lis = cfc.getListaElementos();
	jsonObj.put("registros", lis);
	infoRegresar = jsonObj.toString();
	log.debug("lista "+ infoRegresar);
	} catch( Exception e){
		e.printStackTrace();
		log.info("CARGA DE CATALOGO FECHA CORTE ");
	}
		
} else if (informacion.equals("consultaGeneral")  )	{

	String interm	 	= (request.getParameter("_cmb_ti") 		!=null)	?	request.getParameter("_cmb_ti")		:	" ";
	String moneda	 	= (request.getParameter("_cmb_mon") 	!=null)	?	request.getParameter("_cmb_mon")		:	" ";
	String cliente	 	= (request.getParameter("_cli") 			!=null)	?	request.getParameter("_cli")			:	" ";
	String prestamo	= (request.getParameter("_pre") 			!=null)	?	request.getParameter("_pre")			:	" ";
	String fechaCorte	= (request.getParameter("_cmb_fc") 		!=null)	?	request.getParameter("_cmb_fc")		:	" ";
	String consulta 	=	"";
	
	start = Integer.parseInt((request.getParameter("start")==null)?"0":request.getParameter("start"));
	limit = Integer.parseInt((request.getParameter("limit")==null)?"0":request.getParameter("limit"));
	VencIfDE 				paginador 	= new VencIfDE();	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	JSONObject				jsonObj 		= new JSONObject();
	
	paginador.setIc_if(interm);
	paginador.setMoneda(moneda);
	paginador.setCliente(cliente);
	paginador.setPrestamo(prestamo);
	paginador.setFechaCorte(fechaCorte);
	
	queryHelper.executePKQuery(request);
	consulta = queryHelper.getJSONPageResultSet(request,start,limit);		
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();	
	log.debug("consulta general: "+infoRegresar);
	if(operacion.equals("Generar")){	
		String consulta2="";
		consulta2= queryHelper.getJSONResultCount(request);
		jsonObj = JSONObject.fromObject(consulta2);		
		infoRegresar = jsonObj.toString();
		log.debug("consula : operacion : "+ infoRegresar);
		}
	
} else if (informacion.equals("GeneraArchivoTxt")){

	VencIfDE 				paginador 	= new VencIfDE();	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	JSONObject				jsonObj 		= new JSONObject();
	
	String interm	 	= (request.getParameter("_cmb_ti") 		!=null)	?	request.getParameter("_cmb_ti")		:	" ";
	String moneda	 	= (request.getParameter("_cmb_mon") 	!=null)	?	request.getParameter("_cmb_mon")		:	" ";
	String cliente	 	= (request.getParameter("_cli") 			!=null)	?	request.getParameter("_cli")			:	" ";
	String prestamo	= (request.getParameter("_pre") 			!=null)	?	request.getParameter("_pre")			:	" ";
	String fechaCorte	= (request.getParameter("_cmb_fc") 		!=null)	?	request.getParameter("_cmb_fc")		:	" ";
	
	paginador.setIc_if(interm);
	paginador.setMoneda(moneda);
	paginador.setCliente(cliente);
	paginador.setPrestamo(prestamo);
	paginador.setFechaCorte(fechaCorte);
	
	try {
		String archivo= queryHelper.getCreateCustomFile(request, strDirectorioTemp, "TXT");
		log.debug(" nombreArchivo " +archivo);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+archivo);
		infoRegresar = jsonObj.toString();
		log.debug("infoRegresarArchivo txt " +infoRegresar);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo txt", e);
	}
	
} else if (informacion.equals("GeneraArchivoPDF")){

	VencIfDE 				paginador 	= new VencIfDE();	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	JSONObject				jsonObj 		= new JSONObject();
	String interm	 	= (request.getParameter("_cmb_ti") 		!=null)	?	request.getParameter("_cmb_ti")		:	" ";
	String moneda	 	= (request.getParameter("_cmb_mon") 	!=null)	?	request.getParameter("_cmb_mon")		:	" ";
	String cliente	 	= (request.getParameter("_cli") 			!=null)	?	request.getParameter("_cli")			:	" ";
	String prestamo	= (request.getParameter("_pre") 			!=null)	?	request.getParameter("_pre")			:	" ";
	String fechaCorte	= (request.getParameter("_cmb_fc") 		!=null)	?	request.getParameter("_cmb_fc")		:	" ";
	
	String s = (request.getParameter("s")	!=null)	?	request.getParameter("s")	:	"";
	String l = (request.getParameter("l")	!=null)	?	request.getParameter("l")	:	"";
	
	int st =Integer.parseInt(s);
	int lm =Integer.parseInt(l);
	
	paginador.setIc_if(interm);
	paginador.setMoneda(moneda);
	paginador.setCliente(cliente);
	paginador.setPrestamo(prestamo);
	paginador.setFechaCorte(fechaCorte);
	paginador.setParametro("S");
	paginador.setParamNumDocto(" ");
	
	try {
		String nombreArchivo = queryHelper.getCreatePageCustomFile(request,  st,  lm, strDirectorioTemp,"PDF");
		log.debug(" nombreArchivo " +nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo PDF", e);
	}
} else if (informacion.equals("DescargaZIP")){
	try {
  log.debug("Comenzando la generacion del ZIP");
	VencIfDE 				      paginador 	= new VencIfDE();	
	JSONObject				    jsonObj 		= new JSONObject();
	
	
	String interm	 	= (request.getParameter("_cmb_ti") 		!=null)	?	request.getParameter("_cmb_ti")		:	" ";
	String moneda	 	= (request.getParameter("_cmb_mon") 	!=null)	?	request.getParameter("_cmb_mon")		:	" ";
	String cliente	 	= (request.getParameter("_cli") 			!=null)	?	request.getParameter("_cli")			:	" ";
	String prestamo	= (request.getParameter("_pre") 			!=null)	?	request.getParameter("_pre")			:	" ";
	String fechaCorte	= (request.getParameter("_cmb_fc") 		!=null)	?	request.getParameter("_cmb_fc")		:	" ";
	
	paginador.setIc_if(interm);
	paginador.setMoneda(moneda);
	paginador.setCliente(cliente);
	paginador.setPrestamo(prestamo);
	paginador.setFechaCorte(fechaCorte);
	String consulta 	=	"";
	
	log.debug(interm);
	log.debug(fechaCorte);
	fechaCorte = fechaCorte.replace('/','_');		
	String file = "archvenc_"+fechaCorte+"_"+interm+"_S.txt";
  File fichero = new File(strDirectorioTemp+file);
  if (fichero.exists()){
    //Borrarlo
    boolean borrado = fichero.delete();
    System.err.println("SE BORRO EL ARCHIVO "+file);
  }
  CQueryHelperRegExtJS  queryHelper = new CQueryHelperRegExtJS( paginador);
  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "TXT");
  GenerarArchivoZip zip = new GenerarArchivoZip();
  String nombreZip = "archvenc_"+fechaCorte+"_"+interm+"_S.zip";
  //String directory = strDirectorioPublicacion + "16archivos/descuento/procesos/vencimiento/";
  //zip.generandoZip(strDirectorioTemp,directory,nombreArchivo.replaceAll(".txt",""));
  zip.generandoZip(strDirectorioTemp,strDirectorioTemp,nombreArchivo.replaceAll(".txt",""));
  beanPagos.guardarArchivosZip(nombreZip, strDirectorioTemp);
  
  jsonObj.put("success", new Boolean(true));
 	//jsonObj.put("urlArchivo","/nafin/16archivos/descuento/procesos/vencimiento/"+nombreZip);
  jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreZip);
  infoRegresar = jsonObj.toString();
  } catch(Throwable e) {
				throw new AppException("Error al generar el archivo ZIP", e);
	}
}
%>
<%=   infoRegresar %>