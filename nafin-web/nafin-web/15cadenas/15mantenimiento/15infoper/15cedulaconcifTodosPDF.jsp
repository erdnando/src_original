<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="netropology.utilerias.*"%>
<%@ page import="com.netro.descuento.*"%>
<%@ page import="com.netro.pdf.*"%>

<%@ page import="net.sf.json.JSONArray"%>
<%@ page import="net.sf.json.JSONObject"%>
<%@ page contentType="text/html; charset=Windows-1252" %>
<%@ include file="/13descuento/13secsession_extjs.jspf"%>
<%
String sFechaCorte = (request.getParameter("sFechaCorte")==null)?"":request.getParameter("sFechaCorte");
String sClaveMoneda = (request.getParameter("sClaveMoneda")==null)?"":request.getParameter("sClaveMoneda");
String sTipoBanco = (request.getParameter("cs_tipo")==null)?"":request.getParameter("cs_tipo");
String sChkIfMoneda = (request.getParameter("sChkIfMoneda")==null)?"":request.getParameter("sChkIfMoneda");
String tipoLinea = (request.getParameter("tipoLinea")==null)?"":request.getParameter("tipoLinea");

String query = null;

String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
JSONObject jsonObj = new JSONObject();
String infoRegresar ="";

CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer("");
String nombreArchivo = archivo.nombreArchivo()+".pdf";
AccesoDB con = new AccesoDB();
boolean flagDatosPdf = false;
	
try {
	con.conexionDB();

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

 	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual = fechaActual.substring(0,2);
	String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual = fechaActual.substring(6,10);
	String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	StringBuffer strSQL = new StringBuffer();
	List varBind = new ArrayList();

	if(!"C".equals(tipoLinea)){
	strSQL.append(" SELECT /*+use_nl(c i mon) index (c in_com_estado_cuenta_01_nuk) ordered*/");
	strSQL.append(" i.ic_if,");
	strSQL.append(" i.cg_razon_social,");
	strSQL.append(" cg_subapl_desc tipo_credito,");
	strSQL.append(" SUM(c.fg_montooperado) monto_operado,");
	strSQL.append(" SUM(c.fg_capitalvigente) capital_vigente, ");
	strSQL.append(" SUM(c.fg_capitalvencido) capital_vencido,");
	strSQL.append(" SUM(c.fg_interesprovi) interes_vigente, ");
	strSQL.append(" SUM(c.fg_interesvencido) interes_vencido,");
	strSQL.append(" SUM(c.fg_interesmorat + c.fg_sobretasamor) moras,");
	strSQL.append(" SUM(c.fg_adeudototal) total_adeudo,");
	strSQL.append(" COUNT(c.ig_prestamo) descuentos,");
	strSQL.append(" ig_subapl subaplic,");
	strSQL.append(" SUM(c.fg_saldoinsoluto) saldo_insoluto,");
	strSQL.append(" i.ic_financiera,");
	strSQL.append(" mon.ic_moneda clave_moneda,");
	strSQL.append(" mon.cd_nombre nombre_moneda");
	strSQL.append(" FROM com_estado_cuenta c");
	strSQL.append(", comcat_if i");
	strSQL.append(", comcat_moneda mon");
		strSQL.append(" WHERE c.ic_if = i.ic_if(+)");
	strSQL.append(" AND c.ic_moneda = mon.ic_moneda");
	strSQL.append(" AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy')");
	strSQL.append(" AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1");
	strSQL.append(" AND i.cs_tipo = ?");
	
	varBind.add(sFechaCorte);
	varBind.add(sFechaCorte);
	varBind.add(sTipoBanco);
	
		
	if (!sChkIfMoneda.equals("")) {
		strSQL.append(" AND (");
		StringTokenizer stringTokenizerChkIfMon = new StringTokenizer(sChkIfMoneda, ",");
		int countIf = 0;
		while (stringTokenizerChkIfMon.hasMoreTokens()) {
			String cadenaIfMoneda = stringTokenizerChkIfMon.nextToken();
			String relIfMon[] = cadenaIfMoneda.split("|");
			String claveIf = cadenaIfMoneda.substring(0, cadenaIfMoneda.indexOf("|"));
			String claveMon = cadenaIfMoneda.substring(cadenaIfMoneda.indexOf("|") + 1);
				String numSirac = relIfMon[2];
			if(countIf > 0){strSQL.append("  OR  ");}
			strSQL.append("(i.ic_if = ? AND c.ic_moneda = ?)");
				
			varBind.add(new Integer(claveIf));
			varBind.add(new Integer(claveMon));
			countIf++;
		}
		strSQL.append(" )");
	}
	
		strSQL.append(" GROUP BY i.ic_if, i.cg_razon_social, cg_subapl_desc, ig_subapl, i.ic_financiera, mon.ic_moneda, mon.cd_nombre");
	}else{
		if(!"CE".equals(sTipoBanco)){
			strSQL.append(" SELECT /*+use_nl(c i mon) index (c in_com_estado_cuenta_01_nuk) ordered*/");
			strSQL.append(" i.ic_if,");
			//strSQL.append(" c.cg_nombrecliente cg_razon_social,");
			strSQL.append(" i2.cg_razon_social cg_razon_social,");
			strSQL.append(" cg_subapl_desc tipo_credito,");
			strSQL.append(" SUM(c.fg_montooperado) monto_operado,");
			strSQL.append(" SUM(c.fg_capitalvigente) capital_vigente, ");
			strSQL.append(" SUM(c.fg_capitalvencido) capital_vencido,");
			strSQL.append(" SUM(c.fg_interesprovi) interes_vigente, ");
			strSQL.append(" SUM(c.fg_interesvencido) interes_vencido,");
			strSQL.append(" SUM(c.fg_interesmorat + c.fg_sobretasamor) moras,");
			strSQL.append(" SUM(c.fg_adeudototal) total_adeudo,");
			strSQL.append(" COUNT(c.ig_prestamo) descuentos,");
			strSQL.append(" ig_subapl subaplic,");
			strSQL.append(" SUM(c.fg_saldoinsoluto) saldo_insoluto,");
			strSQL.append(" c.ig_cliente as ic_financiera,");
			strSQL.append(" mon.ic_moneda clave_moneda,");
			strSQL.append(" mon.cd_nombre nombre_moneda");
			strSQL.append(" FROM com_estado_cuenta c");
			strSQL.append(", comcat_if i");
			strSQL.append(", comcat_if i2");
			strSQL.append(", comcat_moneda mon");
			strSQL.append(" WHERE c.ic_if = i.ic_if");
			strSQL.append(" AND c.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND c.ig_cliente = i2.in_numero_sirac ");
			strSQL.append(" AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy')");
			strSQL.append(" AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1");
			strSQL.append(" AND i2.cs_tipo = ?");
			strSQL.append(" AND c.ic_if = 12 ");
			strSQL.append(" AND c.ig_cliente is not null ");
			//strSQL.append(" AND c.ig_cliente = ?");
			
			varBind.add(sFechaCorte);
			varBind.add(sFechaCorte);
			varBind.add(sTipoBanco);
			
			
			if (!sChkIfMoneda.equals("")) {
				strSQL.append(" AND (");
				StringTokenizer stringTokenizerChkIfMon = new StringTokenizer(sChkIfMoneda, ",");
				int countIf = 0;
				while (stringTokenizerChkIfMon.hasMoreTokens()) {
					String cadenaIfMoneda = stringTokenizerChkIfMon.nextToken();
					String relIfMon[] = cadenaIfMoneda.split("|");
					String numSirac = cadenaIfMoneda.substring(0, cadenaIfMoneda.indexOf("|"));
					String claveMon = cadenaIfMoneda.substring(cadenaIfMoneda.indexOf("|") + 1);

					if(countIf > 0){strSQL.append("  OR  ");}
					strSQL.append("(c.ig_cliente = ? AND c.ic_moneda = ?)");
					
					varBind.add(new Integer(numSirac));
					varBind.add(new Integer(claveMon));
					countIf++;
				}
				strSQL.append(" )");
			}
		
			strSQL.append(" GROUP BY i.ic_if, i2.cg_razon_social, cg_subapl_desc, ig_subapl, c.ig_cliente, mon.ic_moneda, mon.cd_nombre");
		}else{
			strSQL.append(" SELECT /*+use_nl(c i mon) index (c in_com_estado_cuenta_01_nuk) ordered*/");
			strSQL.append(" cce.ic_nafin_electronico as ic_if,");
			strSQL.append(" cce.cg_razon_social,");
			strSQL.append(" cg_subapl_desc tipo_credito,");
			strSQL.append(" SUM(c.fg_montooperado) monto_operado,");
			strSQL.append(" SUM(c.fg_capitalvigente) capital_vigente, ");
			strSQL.append(" SUM(c.fg_capitalvencido) capital_vencido,");
			strSQL.append(" SUM(c.fg_interesprovi) interes_vigente, ");
			strSQL.append(" SUM(c.fg_interesvencido) interes_vencido,");
			strSQL.append(" SUM(c.fg_interesmorat + c.fg_sobretasamor) moras,");
			strSQL.append(" SUM(c.fg_adeudototal) total_adeudo,");
			strSQL.append(" COUNT(c.ig_prestamo) descuentos,");
			strSQL.append(" ig_subapl subaplic,");
			strSQL.append(" SUM(c.fg_saldoinsoluto) saldo_insoluto,");
			strSQL.append(" cce.in_numero_sirac ic_financiera,");
			strSQL.append(" mon.ic_moneda clave_moneda,");
			strSQL.append(" mon.cd_nombre nombre_moneda");
			strSQL.append(" FROM com_estado_cuenta c");
			//strSQL.append(", comcat_if i");
			strSQL.append(", comcat_cli_externo cce");
			strSQL.append(", comcat_moneda mon");
			strSQL.append(" WHERE c.ig_cliente = cce.in_numero_sirac ");
			strSQL.append(" AND c.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy')");
			strSQL.append(" AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1");
			strSQL.append(" AND c.ic_if = 12 ");
			strSQL.append(" AND c.ig_cliente is not null ");
			
			varBind.add(sFechaCorte);
			varBind.add(sFechaCorte);
			
			
			if (!sChkIfMoneda.equals("")) {
				strSQL.append(" AND (");
				StringTokenizer stringTokenizerChkIfMon = new StringTokenizer(sChkIfMoneda, ",");
				int countIf = 0;
				while (stringTokenizerChkIfMon.hasMoreTokens()) {
					String cadenaIfMoneda = stringTokenizerChkIfMon.nextToken();
					String relIfMon[] = cadenaIfMoneda.split("|");
					String numSirac = cadenaIfMoneda.substring(0, cadenaIfMoneda.indexOf("|"));
					String claveMon = cadenaIfMoneda.substring(cadenaIfMoneda.indexOf("|") + 1);
					if(countIf > 0){strSQL.append("  OR  ");}
					strSQL.append("(cce.in_numero_sirac = ? AND c.ic_moneda = ?)");

					
					varBind.add(new Integer(numSirac));
					varBind.add(new Integer(claveMon));
					countIf++;
				}
				strSQL.append(" )");
	}
	
			strSQL.append(" GROUP BY cce.ic_nafin_electronico, cce.cg_razon_social, cg_subapl_desc, ig_subapl, cce.in_numero_sirac, mon.ic_moneda, mon.cd_nombre");
		}
	}
	//System.out.println("..:: strSQL: "+strSQL.toString());
	//System.out.println("..:: varBind: "+varBind);
	
	PreparedStatement ps = con.queryPrecompilado(strSQL.toString(), varBind);
	ResultSet rs = ps.executeQuery();

	int registros = 0;
	double dTotal_MontoOperado = 0;
	double dTotal_CapitalVigente = 0;
	double dTotal_CapitalVencido = 0;
	double dTotal_InteresVigente = 0;
	double dTotal_InteresVencido = 0;
	double dTotal_Moras = 0;
	double dTotal_TotalAdeudo = 0;
	double dTotal_Descuentos = 0;
	double dSaldoInsoluto = 0;
	String sClaveIF = "";
	String sNombreIF = "";
	String sMontoOperado = "";
	String sCapitalVigente = "";
	String sCapitalVencido = "";
	String sInteresVencido = "";
	String sMoras = "";
	String sTotalAdeudo = "";
	String sDescuentos = "";
	String sFechaEnvio = "";
	String sTipoCredito = "";
	String sInteresVigente = "";	
	String sClaveSubAplic = "";
	String sSaldoInsoluto = "";
	String sNumeroSirac = "";
	String sNombreMoneda = "";
	
	ArrayList alAtributos = new ArrayList();
	ArrayList alRegistros= new ArrayList();

	while (rs.next()) {
		dTotal_MontoOperado = 0;
		dTotal_CapitalVigente = 0;
		dTotal_CapitalVencido = 0;
		dTotal_InteresVigente = 0;
		dTotal_InteresVencido = 0;
		dTotal_Moras = 0;
		dTotal_TotalAdeudo = 0;
		dTotal_Descuentos = 0;
		dSaldoInsoluto = 0;
		sClaveIF = "";
		sNombreIF = "";
		sMontoOperado = "";
		sCapitalVigente = "";
		sCapitalVencido = "";
		sInteresVencido = "";
		sMoras = "";
		sTotalAdeudo = "";
		sDescuentos = "";
		sFechaEnvio = "";
		sTipoCredito = "";
		sInteresVigente = "";	
		sClaveSubAplic = "";
		sSaldoInsoluto = "";
		sNumeroSirac = "";
		sClaveMoneda = "";
		sNombreMoneda = "";
		alAtributos = new ArrayList();
		alRegistros= new ArrayList();
		flagDatosPdf = true;

		sClaveIF = rs.getString("ic_if")==null?"":rs.getString("ic_if").trim();
		sNombreIF = rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social").trim();
		sTipoCredito = rs.getString("tipo_credito")==null?"":rs.getString("tipo_credito").trim();
		sMontoOperado = rs.getString("monto_operado")==null?"0":rs.getString("monto_operado").trim();
		sCapitalVigente = rs.getString("capital_vigente")==null?"0":rs.getString("capital_vigente").trim();
		sCapitalVencido = rs.getString("capital_vencido")==null?"0":rs.getString("capital_vencido").trim();
		sInteresVigente = rs.getString("interes_vigente")==null?"0":rs.getString("interes_vigente").trim();
		sInteresVencido = rs.getString("interes_vencido")==null?"0":rs.getString("interes_vencido").trim();
		sMoras = rs.getString("moras")==null?"0":rs.getString("moras").trim();
		sTotalAdeudo = rs.getString("total_adeudo")==null?"0":rs.getString("total_adeudo").trim();
		sDescuentos = rs.getString("descuentos")==null?"0":rs.getString("descuentos").trim();
		sClaveSubAplic = rs.getString("subaplic")==null?"":rs.getString("subaplic").trim();
		sSaldoInsoluto = rs.getString("saldo_insoluto")==null?"0":rs.getString("saldo_insoluto").trim();
		sNumeroSirac = rs.getString("ic_financiera")==null?"":rs.getString("ic_financiera").trim();
		sClaveMoneda = rs.getString("clave_moneda")==null?"":rs.getString("clave_moneda");
		sNombreMoneda = rs.getString("nombre_moneda")==null?"":rs.getString("nombre_moneda");
		
		dTotal_MontoOperado   += Double.parseDouble(sMontoOperado);
		dTotal_CapitalVigente += Double.parseDouble(sCapitalVigente);
		dTotal_CapitalVencido += Double.parseDouble(sCapitalVencido);
		dTotal_InteresVigente += Double.parseDouble(sInteresVigente);
		dTotal_InteresVencido += Double.parseDouble(sInteresVencido);
		dTotal_Moras          += Double.parseDouble(sMoras);
		dTotal_TotalAdeudo    += Double.parseDouble(sTotalAdeudo);
		dTotal_Descuentos     += Double.parseDouble(sDescuentos);
		dSaldoInsoluto	   	  += Double.parseDouble(sSaldoInsoluto);

		alAtributos.add(sTipoCredito.replace(',',' '));
		alAtributos.add(Comunes.formatoDecimal(sMontoOperado,2,false));
		alAtributos.add(Comunes.formatoDecimal(sCapitalVigente,2,false));
		alAtributos.add(Comunes.formatoDecimal(sCapitalVencido,2,false));
		alAtributos.add(Comunes.formatoDecimal(sInteresVencido,2,false));
		alAtributos.add(Comunes.formatoDecimal(sInteresVigente,2,false));
		alAtributos.add(Comunes.formatoDecimal(sMoras,2,false));
		alAtributos.add(Comunes.formatoDecimal(sTotalAdeudo,2,false));
		alAtributos.add(Comunes.formatoDecimal(sDescuentos,0,false));
		alAtributos.add(Comunes.formatoDecimal(sClaveSubAplic,2,false));
		alAtributos.add(Comunes.formatoDecimal(sSaldoInsoluto,0,false));
		alRegistros.add(alAtributos);

		strSQL = new StringBuffer();
		varBind = new ArrayList();
		
		strSQL.append(" SELECT ");
		strSQL.append(" IC_FIRMA_CEDULA FIRMA_IF " );
		strSQL.append("       ,CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO NOMBRE_IF " );
		strSQL.append(" 	  ,CG_PUESTO PUESTO_IF " );
		strSQL.append(" 	  ,(SELECT IC_FIRMA_CEDULA FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S'  AND IC_CLIENTE_EXTERNO IS NULL) FIRMA_NAFIN " );
		strSQL.append(" 	  ,(SELECT CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO RESP_IF  " );
		strSQL.append("        FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) RESP_NAFIN " );
		strSQL.append(" 	  ,(SELECT CG_PUESTO FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) PUESTO_NAFIN " );
		strSQL.append("	  ,TO_CHAR(sysdate,'DD/MM/YYYY') FECHA " );
		strSQL.append(" FROM comcat_firma_cedula");
		if(!"CE".equals(sTipoBanco)){
		strSQL.append(" WHERE ic_if = ?");
		}else{
			strSQL.append(" WHERE ic_cliente_externo = ?");
		}
			
		strSQL.append(" AND cs_activo = ?");
		
		varBind.add(new Integer(sClaveIF));
		varBind.add("S");
		
		//System.out.println("..:: strSQL: "+strSQL.toString());
		//System.out.println("..:: varBind: "+varBind);
		
		PreparedStatement pst = con.queryPrecompilado(strSQL.toString(), varBind);
		ResultSet rst = pst.executeQuery();
		
		String sClaveFirmaIF = "";
		String sFirmaIF = "";
		String sPuestoIF = "";
		String sClaveFirmaNafin = "";
		String sFirmaNafin = "";
		String sPuestoNafin = "";
		
		while (rst.next()) {
			sClaveFirmaIF = rst.getString("firma_if")==null?"":rst.getString("firma_if").trim();
			sFirmaIF = rst.getString("nombre_if")==null?"":rst.getString("nombre_if").trim();
			sPuestoIF = rst.getString("puesto_if")==null?"":rst.getString("puesto_if").trim();
			sClaveFirmaNafin = rst.getString("firma_nafin")==null?"":rst.getString("firma_nafin").trim();
			sFirmaNafin = rst.getString("resp_nafin")==null?"":rst.getString("resp_nafin").trim();
			sPuestoNafin = rst.getString("puesto_nafin")==null?"":rst.getString("puesto_nafin").trim();
			sFechaEnvio = rst.getString("fecha")==null?"":rst.getString("fecha").trim();
		}
		
		rst.close();
		pst.close();
		
		ArrayList alTotales = new ArrayList();
		alTotales.add(Comunes.formatoDecimal(dTotal_MontoOperado,2,false));
		alTotales.add(Comunes.formatoDecimal(dTotal_CapitalVigente,2,false));
		alTotales.add(Comunes.formatoDecimal(dTotal_CapitalVencido,2,false));
		alTotales.add(Comunes.formatoDecimal(dTotal_InteresVigente,2,false));
		alTotales.add(Comunes.formatoDecimal(dTotal_InteresVencido,2,false));
		alTotales.add(Comunes.formatoDecimal(dTotal_Moras,2,false));
		alTotales.add(Comunes.formatoDecimal(dTotal_TotalAdeudo,2,false));
		alTotales.add(Comunes.formatoDecimal(dTotal_Descuentos,0,false));
		alTotales.add(Comunes.formatoDecimal(dSaldoInsoluto,0,false));
		
		String sNota = new String("");
		strSQL = new StringBuffer();
		varBind = new ArrayList();
	
		strSQL.append(" SELECT cg_observaciones");
		strSQL.append(" FROM com_observaciones_cedula");
		strSQL.append(" WHERE df_fecha_corte >= TO_DATE(?, 'dd/mm/yyyy')");
		strSQL.append(" AND df_fecha_corte < TO_DATE(?, 'dd/mm/yyyy') + 1");
		strSQL.append(" AND ic_if = ?");
		strSQL.append(" AND ic_moneda = ?");
		if("C".equals(tipoLinea)){
			strSQL.append(" AND IG_CLIENTE = ? ");
		}
		
		varBind.add(sFechaCorte);
		varBind.add(sFechaCorte);
		varBind.add(new Integer(sClaveIF));
		varBind.add(new Integer(sClaveMoneda));
		if("C".equals(tipoLinea)){
			varBind.add(new Integer(sNumeroSirac));
		}
		
		//System.out.println("..:: strSQL: "+strSQL.toString());
		//System.out.println("..:: varBind: "+varBind);
		
		pst = con.queryPrecompilado(strSQL.toString(), varBind);
		rst = pst.executeQuery();
		
		if (rst.next()) {
			sNota = rst.getString("cg_observaciones");
		}

		rst.close();
		pst.close();
		
		alRegistros.add(sNota);

		String encabezado = "NACIONAL FINANCIERA, S.N.C.\n"
						+"SUBDIRECCION DE OPERACIONES DE CREDITO\n"
						+"RESUMEN DE LA CONCILIACI�N DE SALDOS AL "
						+sFechaCorte.substring(0,2) + " DE " + meses[Integer.parseInt(sFechaCorte.substring(3,5))-1].toUpperCase() + " DE " + sFechaCorte.substring(6,10) +"\n\n"
						+"("+sNumeroSirac+") "+sNombreIF+" - "+sNombreMoneda;

		float widths[] = {1, 2,1};
		pdfDoc.setTable(3, 100, widths);
		pdfDoc.setCellImage(strDirectorioPublicacion+"00archivos/15cadenas/15archcadenas/logos/nafinsa.gif", ComunesPDF.LEFT, 75);
		pdfDoc.setCell(encabezado, "formasrepB", ComunesPDF.CENTER,1,1,0);
		pdfDoc.setCell("", "formasrepB", ComunesPDF.CENTER,1,1,0);
		pdfDoc.addTable();
		
		int numCols = 9;
		float widths2[] = {.8f, 2, 1, 1, 1, 1, 1, 1, 1, .7f};
		float widths3[] = {2, 1, 1, 1, 1, 1, 1, 1, .7f};
	
		if ("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
			numCols++;
			pdfDoc.setTable(numCols, 100, widths2);
		} else {
			pdfDoc.setTable(numCols, 100, widths3);
		}
		
		if ("NB".equals(sTipoBanco)) {
			pdfDoc.setCell("Tipo de Cr�dito", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Operado", "formasmenB", ComunesPDF.CENTER);
		} else if ("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
			pdfDoc.setCell("Sub aplicaci�n", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Concepto Sub aplicaci�n", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Saldo insoluto Nafin", "formasmenB", ComunesPDF.CENTER);
		}
		pdfDoc.setCell("Capital Vigente", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Capital Vencido", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Inter�s Vigente", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Inter�s Vencido", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Moras", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Total Adeudo", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Descuentos", "formasmenB", ComunesPDF.CENTER);

		//for ( int i = 0; i < alRegistros.size(); i++) {
			ArrayList alTemp = new ArrayList();
			//alTemp = (ArrayList)alRegistros.get(i);
			alTemp = (ArrayList)alRegistros.get(0);
			if ("NB".equals(sTipoBanco)) {
				pdfDoc.setCell(alTemp.get(0).toString(), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(1).toString(), true), "formasmen", ComunesPDF.RIGHT);
			} else if ("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
				pdfDoc.setCell(Comunes.formatoDecimal(alTemp.get(9).toString(), 0), "formasmen", ComunesPDF.RIGHT);
				pdfDoc.setCell(alTemp.get(0).toString(), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(10).toString(), true), "formasmen", ComunesPDF.RIGHT);
			}
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(2).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(3).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(5).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(4).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(6).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(7).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoDecimal(alTemp.get(8).toString(), 0), "formasmen", ComunesPDF.RIGHT);
		//}
	
		int colSpan = 1;
		if ("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
			colSpan++;
		}
		pdfDoc.setCell("TOTAL", "formasmenB", ComunesPDF.CENTER, colSpan);
		if ("NB".equals(sTipoBanco)) {
			pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(0).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		} else if ("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
			pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(8).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		}
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(1).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(2).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(3).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(4).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(5).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(6).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoDecimal(alTotales.get(7).toString(), 0), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.addTable();
		
		String nota = alRegistros.get(1).toString();
		
		pdfDoc.addText("\nNOTA: "+nota, "formasB", ComunesPDF.LEFT);
		
		pdfDoc.addText("Observaciones:  " + sNota, "formasB", ComunesPDF.LEFT);
		
		pdfDoc.addText("\nConfirmo que los saldos operativos de Nacional Financiera, S.N.C. han sido conciliados con los registros contables de", "formas", ComunesPDF.CENTER);
		pdfDoc.addText(sNombreIF.substring(sNombreIF.indexOf(")")+1, sNombreIF.length()), "formasB", ComunesPDF.CENTER);
		
		pdfDoc.setTable(3, 85);
		
		pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCellImage(strDirectorioPublicacion + "00utils/gif/firma_pagos_cartera.gif", ComunesPDF.CENTER, 100);
	
		pdfDoc.setCell("_____________________________\n"+sFirmaIF, "formasB", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("_____________________________\n"+sFirmaNafin, "formasB", ComunesPDF.CENTER, 1, 1, 0);
		
		pdfDoc.setCell(sPuestoIF, "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell(sPuestoNafin, "formas", ComunesPDF.CENTER, 1, 1, 0);
		
		pdfDoc.setCell(sNombreIF.substring(sNombreIF.indexOf(")")+1, sNombreIF.length()), "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("NACIONAL FINANCIERA S.N.C.", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.addTable();
		
		float tWid[] = {1.5f, 1};
		
		pdfDoc.setTable(2, 100, tWid);
		pdfDoc.setCell("Nota: Si en un plazo de 10 d�as h�biles contados a partir de la recepci�n del presente Estado de Cuenta no se reciben las observaciones correspondientes se dar�n por aceptadas las cifras.", "formasrep", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Fecha de Env�o:  "+sFechaEnvio, "formasrep", ComunesPDF.RIGHT, 1, 1, 0);
		pdfDoc.addTable();

		pdfDoc.newPage();
	}
	rs.close();
	ps.close();

	if (flagDatosPdf) {
		pdfDoc.endDocument();
	if(operacion.equals("") ) {
%>
<html>
	<head>
		<title>Nafinet</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
	</head>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<table width="300" cellpadding="2" cellspacing="0" border="0" align="center">
			<tr>
				<td class="formas">&nbsp;</td>
			</tr>
			<tr>
				<td class="formas" align="center"><br>
					<table cellpadding="3" cellspacing="1">
						<tr>
							<td class="celda02" width="100" align="center" height="30">
								<a href="<%=strDirecVirtualTemp%><%=nombreArchivo%>">Bajar Archivo</a>
							</td>
							<td class="celda02" width="100" align="center">
								<a href="javascript:close();">Cerrar</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
<%
}
	} else {
if(operacion.equals("") ) { 
%>
<html>
	<head>
		<title>Nafinet</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
	</head>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<table width="300" cellpadding="2" cellspacing="0" border="0" align="center">
			<tr>
				<td class="formas">&nbsp;</td>
			</tr>
			<tr>
				<td class="celda01" align="center">NO SE ENCONTR&Oacute; NING&Uacute;N REGISTRO</td>
			</tr>
			<tr>
				<td class="formas">&nbsp;</td>
			</tr>
			<tr>
				<td>
					<table align="center" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="formas" width="120">&nbsp;</td>
							<td class="celda02" width="60" align="center"><a href="javascript:close();">Cerrar</a></td>
							<td class="formas" width="120">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
<%
}
	}
} catch(Exception e) {
	e.printStackTrace();
	if(operacion.equals("") ) { 
%>
			<tr>
				<td class="formas" align="center"><br>
					<table width="600" border="1" cellspacing="0" cellpadding="2" bordercolor="#A5B8BF">
						<tr>
							<td class="celda01" align="center" bgcolor="silver">Hubo problemas al generar el archivo.</td>
						</tr>
					</table>
				</td>
			</tr>
<%
}
} finally {
	if(con.hayConexionAbierta()) {
		con.cierraConexionDB();
	}
}
if(operacion.equals("") ) { 
%>
		</table>  
	</body>
</html>

<%
}
if(operacion.equals("VersionNueva") ) {
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();
	
	
}
%>
<%=infoRegresar%>