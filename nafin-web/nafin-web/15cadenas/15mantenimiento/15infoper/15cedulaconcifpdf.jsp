<%@ page import="java.sql.*, java.text.*, java.util.*, netropology.utilerias.*,net.sf.json.JSONObject,net.sf.json.JSONArray,com.netro.descuento.*, com.netro.pdf.*"%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String sTipoBanco = (request.getParameter("sTipoBanco")==null)?"":request.getParameter("sTipoBanco");
String sNombreMoneda = (request.getParameter("sNombreMoneda")==null)?"":request.getParameter("sNombreMoneda").replace('_',' '),
		sObservaciones = (request.getParameter("sObservaciones")==null)?"":request.getParameter("sObservaciones").replace('_',' '),
		sFirmaIF = (request.getParameter("sFirmaIF")==null)?"":request.getParameter("sFirmaIF").replace('_',' '),
		sFirmaNafin = (request.getParameter("sFirmaNafin")==null)?"":request.getParameter("sFirmaNafin").replace('_',' '),
		sPuestoIF = (request.getParameter("sPuestoIF")==null)?"":request.getParameter("sPuestoIF").replace('_',' '),
		sPuestoNafin = (request.getParameter("sPuestoNafin")==null)?"":request.getParameter("sPuestoNafin").replace('_',' '),
		sFechaEnvio = (request.getParameter("sFechaEnvio")==null)?"":request.getParameter("sFechaEnvio").replace('_',' '),
		sNombreIF = (request.getParameter("sNombreIF")==null)?"":request.getParameter("sNombreIF").replace('_',' '),
		sFechaCorte = (request.getParameter("sFechaCorte")==null)?"":request.getParameter("sFechaCorte").replace('_',' ');
String sNumeroSirac	  = (request.getParameter("sNumeroSirac")==null)?"":request.getParameter("sNumeroSirac");

String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
JSONObject jsonObj = new JSONObject();
String infoRegresar ="";

//ArrayList alRegistros = (ArrayList)session.getAttribute("Registros");
CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer("");
String nombreArchivo = archivo.nombreArchivo()+".pdf";
try {
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	if(operacion.equals("") ) {
%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="300" cellpadding="2" cellspacing="0" border="0" align="center">
<tr>
  <td class="formas">&nbsp;</td>
</tr>
<%
}
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	float widths[] = {1, 2,1};
	pdfDoc.setTable(3, 100, widths);
	pdfDoc.setCellImage(strDirectorioPublicacion+"00archivos/15cadenas/15archcadenas/logos/nafinsa.gif",ComunesPDF.LEFT, 75);
	String encabezado = "NACIONAL FINANCIERA, S.N.C.\n" 
					+"SUBDIRECCION DE OPERACIONES DE CREDITO\n"
					+"RESUMEN DE LA CONCILIACI�N DE SALDOS AL " + 
					sFechaCorte.substring(0,2) + " DE " + meses[Integer.parseInt(sFechaCorte.substring(3,5))-1].toUpperCase() + " DE " + sFechaCorte.substring(6,10) +"\n\n"
					+sNombreIF+" - "+sNombreMoneda;
	pdfDoc.setCell(encabezado, "formasrepB", ComunesPDF.CENTER,1,1,0);
	pdfDoc.setCell("", "formasrepB", ComunesPDF.CENTER,1,1,0);
	pdfDoc.addTable();

//pdfDoc.addText(sNombreIF+" - "+sNombreMoneda, "formasrepB", ComunesPDF.CENTER);
//	pdfDoc.addText(sNombreMoneda, "formasB", ComunesPDF.CENTER);
	
	int numCols = 9;
	float widths2[] = {.8f, 2, 1, 1, 1, 1, 1, 1, 1, .7f};
	float widths3[] = {2, 1, 1, 1, 1, 1, 1, 1, .7f};


	if("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
		numCols++;
		pdfDoc.setTable(numCols, 100, widths2);
	} else {
		pdfDoc.setTable(numCols, 100, widths3);
	}
	
	if("NB".equals(sTipoBanco)) {
		pdfDoc.setCell("Tipo de Cr�dito", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto Operado", "formasmenB", ComunesPDF.CENTER);
	} else if("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
		pdfDoc.setCell("Sub aplicaci�n", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Concepto Sub aplicaci�n", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Saldo insoluto Nafin", "formasmenB", ComunesPDF.CENTER);
	}
	pdfDoc.setCell("Capital Vigente", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Capital Vencido", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Inter�s Vigente", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Inter�s Vencido", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Moras", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Total Adeudo", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Descuentos", "formasmenB", ComunesPDF.CENTER);   
	
	ArrayList alRegistros = (ArrayList)session.getAttribute("Registros");
	//out.println(alRegistros.size());
	for(int i=0; i < alRegistros.size()-1; i++){
		ArrayList alTemp = new ArrayList();
		alTemp = (ArrayList)alRegistros.get(i);
		if("NB".equals(sTipoBanco)) {
			pdfDoc.setCell(alTemp.get(0).toString(), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(1).toString(), true), "formasmen", ComunesPDF.RIGHT);
		} else if("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
			pdfDoc.setCell(Comunes.formatoDecimal(alTemp.get(9).toString(), 0), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(alTemp.get(0).toString(), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(10).toString(), true), "formasmen", ComunesPDF.RIGHT);
		}// else if("B".equals(sTipoBanco)) {
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(2).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(3).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(5).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(4).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(6).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(7).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoDecimal(alTemp.get(8).toString(), 0), "formasmen", ComunesPDF.RIGHT);
	}
	ArrayList alTotales = (ArrayList)session.getAttribute("Totales");
	int colSpan = 1;
	if("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) 
		colSpan++;	
	pdfDoc.setCell("TOTAL", "formasmenB", ComunesPDF.CENTER, colSpan);
	if("NB".equals(sTipoBanco)) {
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(0).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	} else if("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(8).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	}
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(1).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(2).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(3).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(4).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(5).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(6).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoDecimal(alTotales.get(7).toString(), 0), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.addTable();
	
	String nota = alRegistros.get(alRegistros.size()-1).toString();
	
	pdfDoc.addText("\nNOTA: "+nota, "formasB", ComunesPDF.LEFT);
	
	pdfDoc.addText("Observaciones:  " + sObservaciones, "formasB", ComunesPDF.LEFT);
	
	pdfDoc.addText("\nConfirmo que los saldos operativos de Nacional Financiera, S.N.C. han sido conciliados con los registros contables de", "formas", ComunesPDF.CENTER);
	pdfDoc.addText(sNombreIF.substring(sNombreIF.indexOf(")")+1, sNombreIF.length()), "formasB", ComunesPDF.CENTER);
	
	pdfDoc.setTable(3, 85);
	
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCellImage(strDirectorioPublicacion + "00utils/gif/firma_pagos_cartera.gif", ComunesPDF.CENTER, 100);

	pdfDoc.setCell("_____________________________\n"+sFirmaIF, "formasB", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("_____________________________\n"+sFirmaNafin, "formasB", ComunesPDF.CENTER, 1, 1, 0);
	
	pdfDoc.setCell(sPuestoIF, "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell(sPuestoNafin, "formas", ComunesPDF.CENTER, 1, 1, 0);
	
	pdfDoc.setCell(sNombreIF.substring(sNombreIF.indexOf(")")+1, sNombreIF.length()), "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("NACIONAL FINANCIERA S.N.C.", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.addTable();
	
	float tWid[] = {1.5f, 1};
	
	pdfDoc.setTable(2, 100, tWid);
	pdfDoc.setCell("Nota: Si en un plazo de 10 d�as h�biles contados a partir de la recepci�n del presente Estado de Cuenta no se reciben las observaciones correspondientes se dar�n por aceptadas las cifras.", "formasrep", ComunesPDF.LEFT, 1, 1, 0);
	pdfDoc.setCell("Fecha de Env�o:  "+sFechaEnvio, "formasrep", ComunesPDF.RIGHT, 1, 1, 0);
	pdfDoc.addTable();
	
	pdfDoc.endDocument();
	if(operacion.equals("") ) {
%>
<tr>
	<td class="formas" align="center"><br>
		<table cellpadding="3" cellspacing="1">
			<tr>
				<td class="celda02" width="100" align="center" height="30">
					<a href="<%=strDirecVirtualTemp%><%=nombreArchivo%>">Bajar Archivo</a>
				</td>
				<td class="celda02" width="100" align="center">
					<a href="javascript:close();">Cerrar</a>
				</td>
			</tr>
		</table>
	</td>
</tr>
<%
}
} catch(Exception e) {
	e.printStackTrace();	
	if(operacion.equals("") ) {
%>
	<tr>
		<td class="formas" align="center"><br>
			<table width="600" border="1" cellspacing="0" cellpadding="2" bordercolor="#A5B8BF">
				<tr>
					<td class="celda01" align="center" bgcolor="silver">Hubo problemas al generar el archivo.</td>
				</tr>
			</table>
		</td>
	</tr>
<%
}
}
if(operacion.equals("") ) {
%>
</body>
</html>

<%
}
if(operacion.equals("VersionNueva") ) {
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();
	System.out.println("*****infoRegresar........."+infoRegresar);
	
}
%>
<%=infoRegresar%>