Ext.onReady(function(){

  Ext.override(Ext.form.Field, {
    setFieldLabel : function(text) {
      if (this.rendered) {
        this.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(text);
      }
      this.fieldLabel = text;
    }
  });

	var sClaveIF ="";
	var sNombreIF="";
	var sClaveMoneda="";
	var sNombreMoneda="";
	var sFechaCorte="";
	var sFechaEnvio="";
	var sNumeroSirac="";
	var countChecks= 0;
	
	var  sClaveIF_E =[]
	var  sClaveMoneda_E= [];
	var  sFechaCorte_E = []
	var sFechaEnvio_E = [];
	
	
	function cancelarPDF() {
		sClaveIF="";
		sNombreIF="";
		sClaveMoneda="";
		sNombreMoneda ="";
		sFechaCorte="";
		sFechaEnvio="";
		sNumeroSirac="";
		countChecks= 0;
	}
	
	function cancelarEliminar() {
		sClaveIF_E =[]
		sClaveMoneda_E= [];
		sFechaCorte_E = []
		sFechaEnvio_E = [];
	}

	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	
	var procesarTotalData = function(store,arrRegistros,opts){
	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros != null){			
			var griTotales = Ext.getCmp('griTotales');
			if (!griTotales.isVisible()) {
				griTotales.show();
			}	
			var el = Ext.getCmp('gridDetalle').getGridEl();			
			var info = store.reader.jsonData;	
			
			if(store.getTotalCount()>0){					
			}else{
				Ext.getCmp('griTotales').hide();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	
		
	var procesarDetalleData = function(store,arrRegistros,opts){
	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros != null){			
			var gridDetalle = Ext.getCmp('gridDetalle');
			if (!gridDetalle.isVisible()) {
				gridDetalle.show();
			}	
			var el = gridDetalle.getGridEl();			
			var info = store.reader.jsonData;	
							
			Ext.getCmp("gridDetalle").setTitle(info.leyendaEncabezado);			
			Ext.getCmp('lblFirmas').update(info.firmas);
			
			Ext.getCmp('sNombreMoneda').setValue(info.sNombreMoneda);
			Ext.getCmp('sObservaciones').setValue(info.sObservaciones);
			Ext.getCmp('sFirmaIF').setValue(info.sFirmaIF);
			Ext.getCmp('sFirmaNafin').setValue(info.sFirmaNafin);
			Ext.getCmp('sPuestoIF').setValue(info.sPuestoIF);
			Ext.getCmp('sPuestoNafin').setValue(info.sPuestoNafin);
			Ext.getCmp('sFechaEnvio').setValue(info.sFechaEnvio);
			Ext.getCmp('sFechaCorte').setValue(info.sFechaCorte);
			Ext.getCmp('sClaveIF').setValue(info.sClaveIF);
			Ext.getCmp('sClaveMoneda').setValue(info.sClaveMoneda);
			Ext.getCmp('sNombreIF').setValue(info.sNombreIF);
			Ext.getCmp('fcarga').setValue(info.fcarga);
			Ext.getCmp('icusuario').setValue(info.icusuario);
			Ext.getCmp('nusuario').setValue(info.nusuario);
			Ext.getCmp('cvecertificado').setValue(info.cvecertificado);
			Ext.getCmp('sNumeroSirac').setValue(info.sNumeroSirac);
						
			if(store.getTotalCount()>0){					
			}else{
				
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	
		
		
	
	//  Ver Detalel 
	
	
	var procesarVerDetalle= function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var sTipoLinea=registro.get('CG_TIPO_LINEA');
		var cs_tipo_B = Ext.getCmp('id_cs_tipo').getValue();
		
		var sClaveIF=registro.get('IC_IF');  
		var sNombreIF=(cs_tipo_B!='CE')?registro.get('NOMBRE_IF'):registro.get('NOMBRE_CE');  
		var sClaveMoneda=registro.get('IC_MONEDA');  
		var sNombreMoneda=registro.get('MONEDA');  
		var sFechaCorte=registro.get('FECHA_CORTE');  
		var sFechaEnvio=registro.get('FECHA_ENVIO');  
		var sNumeroSirac=(sTipoLinea!='c')?registro.get('CLAVE_SIRAC_IF'):registro.get('CLAVE_SIRAC_CE');  
		
			
		consDetalleData.load({
			params: {							
				informacion: 'ConsDetalle',
				sClaveIF:sClaveIF,
				sNombreIF:sNombreIF, 
				sClaveMoneda:sClaveMoneda,
				sNombreMoneda:sNombreMoneda,
				sFechaCorte:sFechaCorte,
				sFechaEnvio:sFechaEnvio,
				sNumeroSirac:sNumeroSirac,
				tipoLinea: sTipoLinea,
				cs_tipo_B: cs_tipo_B
			}
		});		
		
		consDetTotalesData.load({
			params: {							
				informacion: 'consDetTotalesData',
				sClaveIF:sClaveIF,
				sNombreIF:sNombreIF, 
				sClaveMoneda:sClaveMoneda,
				sNombreMoneda:sNombreMoneda,
				sFechaCorte:sFechaCorte,
				sFechaEnvio:sFechaEnvio,
				sNumeroSirac:sNumeroSirac,
				tipoLinea: sTipoLinea,
				cs_tipo_B: Ext.getCmp('id_cs_tipo').getValue()
			}
		});	
					
		new Ext.Window({					
			layout: 'fit',
			modal: true,
			width: 810,
			//height: '100%',
			modal: true,					
			closeAction: 'hide',
			resizable: false,
			//constrain: true,
			closable:false,
			id: 'VerDetalle',
			items: [					
				gridDetalle,
				griTotales,
				{
					width		: 800,	
					xtype: 'label',
					id: 'lblFirmas',	
					align: 'center',
					text: ''
				},
				{ 	xtype: 'textfield',  hidden: true, id: 'sNombreMoneda', 	value: '' },		
				{ 	xtype: 'textfield',  hidden: true, id: 'sObservaciones', 	value: '' },	
				{ 	xtype: 'textfield',  hidden: true, id: 'sFirmaIF', 	value: '' },	
				{ 	xtype: 'textfield',  hidden: true, id: 'sFirmaNafin', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'sPuestoIF', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'sPuestoNafin', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'sFechaEnvio', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'sFechaCorte', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'sClaveIF', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'sClaveMoneda', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'sNombreIF', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'fcarga', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'icusuario', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'nusuario', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'cvecertificado', 	value: '' },
				{ 	xtype: 'textfield',  hidden: true, id: 'sNumeroSirac', 	value: '' }
			],
			bbar: {
				xtype: 'toolbar',	buttonAlign:'center',	
				buttons: ['-',
					{	
						xtype: 'button',	text: 'Salir',	iconCls: 'icoLimpiar', 	id: 'btnCerrar',
						handler: function(){		
							Ext.getCmp('VerDetalle').destroy();		
						} 
					},
					'-',
					{
						xtype: 'button',
						text: 'Imprimir PDF',
						tooltip:	'Imprimir PDF',
						id: 'btnImprimirPDF',
						iconCls: 'icoPdf',
						handler: function(boton, evento) {
						
							Ext.Ajax.request({
								url: '15cedulaconcpdf.jsp',
								params: Ext.apply(fp.getForm().getValues(),{									
									operacion: 'VersionNueva',	
									sNombreMoneda:Ext.getCmp('sNombreMoneda').getValue(),
									sObservaciones:Ext.getCmp('sObservaciones').getValue(),
									sFirmaIF:Ext.getCmp('sFirmaIF').getValue(),
									sFirmaNafin:Ext.getCmp('sFirmaNafin').getValue(),
									sPuestoIF:Ext.getCmp('sPuestoIF').getValue(),
									sPuestoNafin:Ext.getCmp('sPuestoNafin').getValue(),
									sFechaEnvio:Ext.getCmp('sFechaEnvio').getValue(),
									sFechaCorte:Ext.getCmp('sFechaCorte').getValue(),
									sClaveIF:Ext.getCmp('sClaveIF').getValue(),
									sClaveMoneda:Ext.getCmp('sClaveMoneda').getValue(),
									sNombreIF:Ext.getCmp('sNombreIF').getValue(),
									fcarga:Ext.getCmp('fcarga').getValue(),
									icusuario:Ext.getCmp('icusuario').getValue(),
									nusuario:Ext.getCmp('nusuario').getValue(),
									cvecertificado:Ext.getCmp('cvecertificado').getValue(),
									sNumeroSirac:Ext.getCmp('sNumeroSirac').getValue(),
									tipoLinea: Ext.getCmp('cmbTipoLinea').getValue(),
									cs_tipo_B: Ext.getCmp('id_cs_tipo').getValue()
								}),							
								callback: descargaArchivo
							});								
						}
					}	
				]
			}
		}).show().setTitle('Ver Detalle ');				
	
	}
	

	
	var consDetalleData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15cedulaconc.data.jsp',
		baseParams: {
			informacion: 'ConsDetalle'			
		},		
		fields: [				
			{ name: 'SUB_APLICACION'},
			{ name: 'CONCEPTO_SUB_APLICACION'},
			{ name: 'SALDO_INSOLUTO'},
			{ name: 'CAPITAL_VIGENTE'},
			{ name: 'CAPITAL_VENCIDO'},
			{ name: 'INTERES_VIGENTE'},
			{ name: 'INTERES_VENCIDO'},
			{ name: 'MORAS'},
			{ name: 'TOTAL_ADEUDO'},
			{ name: 'DESCUENTOS'},
			{ name: 'MONTO_OPERADO'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDetalleData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDetalleData(null, null, null);					
				}
			}
		}					
	});
		
	var gridDetalle = {	
		xtype: 'grid',
		store: consDetalleData,
		id: 'gridDetalle',		
		hidden: false,
		title: 'Detalle',	
		columns: [			
			{
				header: 'Sub Aplicaci�n',
				tooltip: 'Sub Aplicaci�n',
				dataIndex: 'SUB_APLICACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){                                
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}			
			},
			{
				header: 'Concepto Sub Aplicaci�n',
				tooltip: 'Concepto Sub Aplicaci�n',
				dataIndex: 'CONCEPTO_SUB_APLICACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Saldo Insoluto Nafin',
				tooltip: 'Saldo Insoluto Nafin',
				dataIndex: 'SALDO_INSOLUTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Capital Vigente',
				tooltip: 'Capital Vigente',
				dataIndex: 'CAPITAL_VIGENTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Capital Vencido',
				tooltip: 'Capital Vencido',
				dataIndex: 'CAPITAL_VENCIDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{
				header: 'Inter�s Vigente',
				tooltip: 'Inter�s Vigente',
				dataIndex: 'INTERES_VIGENTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{
				header: 'Inter�s Vencido',
				tooltip: 'Inter�s Vencido',
				dataIndex: 'INTERES_VENCIDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{
				header: 'Moras',
				tooltip: 'Moras',
				dataIndex: 'MORAS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{
				header: 'Total Adeudo',
				tooltip: 'Total Adeudo',
				dataIndex: 'TOTAL_ADEUDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Descuentos',
				tooltip: 'Descuentos',
				dataIndex: 'DESCUENTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			}
		],
		stripeRows: true,
		loadMask: true,
		//height: 230,
		height: 'auto',		
		width: 800,		
		frame: true
	}
	

	var consDetTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15cedulaconc.data.jsp',
		baseParams: {
			informacion: 'consDetTotalesData'			
		},		
		fields: [							
			{ name: 'DESCRIPCION'},
			{ name: 'T_SALDO_INSOLUTO'},
			{ name: 'T_CAPITAL_VIGENTE'},
			{ name: 'T_CAPITAL_VENCIDO'},
			{ name: 'T_INTERES_VIGENTE'},
			{ name: 'T_INTERES_VENCIDO'},
			{ name: 'T_MORAS'},
			{ name: 'T_TOTAL_ADEUDO'},
			{ name: 'T_DESCUENTOS'}					
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarTotalData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarTotalData(null, null, null);					
				}
			}
		}					
	});
	
	var griTotales = {	
		xtype: 'grid',
		store: consDetTotalesData,
		id: 'griTotales',		
		hidden: false,
		title: 'Totales',	
		columns: [			
			{
				header: '',				
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Saldo Insoluto Nafin',				
				dataIndex: 'T_SALDO_INSOLUTO',
				sortable: true,
				width: 150,			
				resizable: true,								
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},			
			{
				header: 'Capital Vigente',
				dataIndex: 'T_CAPITAL_VIGENTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Capital Vencido',
				dataIndex: 'T_CAPITAL_VENCIDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{
				header: 'Inter�s Vigente',
				tooltip: '',
				dataIndex: 'T_INTERES_VIGENTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{
				header: 'Inter�s Vencido',
				tooltip: '',
				dataIndex: 'T_INTERES_VENCIDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{
				header: 'Moras',
				tooltip: '',
				dataIndex: 'T_MORAS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{
				header: 'Total Adeudo',				
				dataIndex: 'T_TOTAL_ADEUDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Descuentos',				
				dataIndex: 'T_DESCUENTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')					
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 100,
		width: 800,		
		frame: true
	}
	
	// Resuesta Para Eliminar los Registros 
	function ProcesarEliminacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
		
			window.location = '15cedulaconcExt.jsp';		
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		
	//***Para Eliminar los Registros 
	function procesoEliminarRegistros() {
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();		
		var countChecksE =0;
		cancelarEliminar();
		
		store.each(function(record) {
		
			if(record.data['SELECCIONAR']=='S')  {
				sClaveIF_E.push(record.data['IC_IF']);
				sClaveMoneda_E.push(record.data['IC_MONEDA']);
				sFechaCorte_E.push(record.data['FECHA_CORTE']);
				sFechaEnvio_E.push(record.data['FECHA_ENVIO']);
				countChecksE++;
			}
		});
		
		if(countChecksE==0) {
			Ext.MessageBox.alert('Aviso','Debe selecionar por lo menos un registro a eliminar ');		
		}else  {
			
			
			Ext.MessageBox.confirm('Confirmaci�n','Se van a eliminar las c�dulas de conciliaci�n seleccionadas: �Desea Continuar?', function(resp){
				if(resp =='yes'){	
				
					Ext.MessageBox.confirm('Confirmaci�n','Las c�dulas se eliminar�n y s�lo podr�n ser vistas hasta que hayan sido enviadas nuevamente por el IF', function(resp){
						if(resp =='yes'){	
							
							Ext.Ajax.request({
								url: '15cedulaconc.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Eliminar_Registros',										
									sClaveIF_E:sClaveIF_E,
									sClaveMoneda_E:sClaveMoneda_E,
									sFechaCorte_E:sFechaCorte_E,
									sFechaEnvio_E:sFechaEnvio_E,
									countChecks:countChecksE,
									tipoLinea: Ext.getCmp('cmbTipoLinea').getValue(),
									cs_tipo_B:  Ext.getCmp('id_cs_tipo').getValue()
								}),							
								callback: ProcesarEliminacion
							});
						}
					});
				}
			});

	
			
		}
	}
	
	
	//***Para Generar PDF	
	function procesoImprimirRegistros() {
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();		
		
		cancelarPDF();
		var tipoLinea = Ext.getCmp('cmbTipoLinea').getValue();
		var cs_tipo_B =  Ext.getCmp('id_cs_tipo').getValue();
		
		store.each(function(record) {
		
			if(record.data['SELECCIONAR']=='S')  {
			
				sClaveIF +=record.data['IC_IF']+",";
				if(cs_tipo_B!='CE'){
					sNombreIF +=record.data['NOMBRE_IF']+",";
				}else{
					sNombreIF +=record.data['NOMBRE_CE']+",";
				}
				sClaveMoneda +=record.data['IC_MONEDA']+",";
				sNombreMoneda +=record.data['MONEDA']+"," ;
				sFechaCorte +=record.data['FECHA_CORTE']+",";
				sFechaEnvio +=record.data['FECHA_ENVIO']+"," ;
				if(tipoLinea!='C'){
					sNumeroSirac += record.data['CLAVE_SIRAC_IF']+",";
				}else{
					sNumeroSirac += record.data['CLAVE_SIRAC_CE']+",";
				}
				countChecks++;
			}
		});
		
		if(countChecks==0) {
			Ext.MessageBox.alert('Aviso','Debe selecionar por lo menos un registro para generar el PDF');		
		}else  {
					
			Ext.Ajax.request({
				url: '15cedulaconcPdfAll.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Generar_PDF',
					operacion: 'VersionNueva',					
					sClaveIF :sClaveIF,
					sNombreIF :sNombreIF,
					sClaveMoneda :sClaveMoneda, 
					sNombreMoneda: sNombreMoneda, 
					sFechaCorte :sFechaCorte, 
					sFechaEnvio :sFechaEnvio,
					sNumeroSirac :sNumeroSirac,
					cs_tipo_B: cs_tipo_B,
					tipoLinea: tipoLinea,
					countChecks:countChecks
				}),							
				callback: descargaArchivo
			});
		}
	}
		
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {					
				el.unmask();
				Ext.getCmp('btnEliminar').enable();
				Ext.getCmp('btnImprimir').enable();				
			} else {		
				Ext.getCmp('btnEliminar').disable();
				Ext.getCmp('btnImprimir').disable();				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15cedulaconc.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [	
			{	name: 'IC_IF'},	
			{	name: 'IC_MONEDA'},	
			{	name: 'CLAVE_SIRAC_IF'},
			{	name: 'NOMBRE_IF'},	
			{	name: 'FECHA_CORTE'},	
			{	name: 'FECHA_ENVIO'},
			{	name: 'MONEDA'},
			{	name: 'CLAVE_SIRAC_CE'},
			{	name: 'NOMBRE_CE'},
			{	name: 'CG_TIPO_LINEA'},
			{	name: 'SELECCIONAR'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});

	var selectModel = new Ext.grid.CheckboxSelectionModel( {
		//checkOnly: true,		
	  	listeners: {
			rowdeselect: function(selectModel, rowIndex, record) {	// cuando se quita la selecci�n
		
				record.data['SELECCIONAR']='N';
				record.commit();				
					  
			},
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //Se activa cuando una fila est� seleccionada, return false para cancelar.		
		
				record.data['SELECCIONAR']='S';	
				record.commit();
								
			}		
		}
		
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Clave SIRAC del IF',
				tooltip: 'Clave SIRAC del IF',
				dataIndex: 'CLAVE_SIRAC_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{	header: 'Nombre del IF',
				tooltip: 'Nombre del IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 180,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){                                
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},{
				header: 'No. Cliente SIRAC',
				tooltip: 'No. Cliente SIRAC',
				dataIndex: 'CLAVE_SIRAC_CE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{	header: 'Cliente Externo',
				tooltip: 'Cliente Externo',
				dataIndex: 'NOMBRE_CE',
				sortable: true,
				width: 180,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){                                
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{	header: 'Fecha de Corte',
				tooltip: 'Fecha de Corte',
				dataIndex: 'FECHA_CORTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{	header: 'Fecha de Env�o',
				tooltip: 'Fecha de Env�o',
				dataIndex: 'FECHA_ENVIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
				{
				xtype		: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle ',				
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'icoBuscar';
						}
						,handler: procesarVerDetalle
					}
				]
			},
			selectModel
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 780,
		align: 'center',
		frame: false,
		sm:selectModel,
		bbar: {
			items: [	
			'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',					
					tooltip:	'Generar PDF ',
					iconCls: 'icoPdf',
					id: 'btnImprimir',
					handler: procesoImprimirRegistros
				},
				{
					xtype: 'button',
					text: 'Eliminar Registro',					
					tooltip:	'Eliminar Registro',
					iconCls: 'borrar',
					id: 'btnEliminar',
					handler: procesoEliminarRegistros
				}				
			]
		}
	});
	


	//*************************Criterios de Busqueda 
	var catAnio = new Ext.data.JsonStore({
		id: 'catAnio',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15cedulaconc.data.jsp',
		baseParams: {
			informacion: 'catAnio'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	

	var catalogoMes = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],
		data : [
			['01','Enero'],
			['02','Febrero'],
			['03','Marzo'],
			['04','Abril'],
			['05','Mayo'],
			['06','Junio'],
			['07','Julio'],
			['08','Agosto'],
			['09','Septiembre']	,
			['10','Octubre'],	
			['11','Noviembre'],	
			['12','Diciembre']			
		]
	});


	var catMoneda = new Ext.data.JsonStore({
		id: 'catMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15cedulaconc.data.jsp',
		baseParams: {
			informacion: 'catMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catIntermediario = new Ext.data.JsonStore({
		id: 'catIntermediario',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15cedulaconc.data.jsp',
		baseParams: {
			informacion: 'catIntermediario'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
  var catTipoLinea = new Ext.data.ArrayStore({
    id: 'catTipoLinea',
    fields : ['CLAVE', 'DESCRIPCION'],
    data:[['D','Linea Descuento'],['C', 'Linea Cr�dito']]
  });
	
  var catTipoAfiliado = new Ext.data.ArrayStore({
    id: 'catTipoAfiliado',
    fields : ['CLAVE', 'DESCRIPCION']	
  });
  //--------------------------------------------------------------------------
  
	var elementosForma  =[
		/*{  
			//width:100,	
			xtype	:'radiogroup',	
			align:'center' , 
			
			id:'id_cs_tipo',	
			name:'cs_tipo',	
			cls:'x-check-group-alt',	
			msgTarget: 'side',
			margins: '0 20 0 0',						
			items	: [
				{	boxLabel	: 'IF',	name : 'cs_tipo', inputValue: 'B'	}, 
				{	boxLabel	: 'IFNB',	name : 'cs_tipo', inputValue: 'NB' }
			],
			listeners	: {
				change:{
					fn:function(){
						var valor =	(Ext.getCmp('id_cs_tipo')).getValue();
						Ext.getCmp('sClaveIF1').reset();
						catIntermediario.load({
							params : Ext.apply({
								cs_tipo_B:valor.getGroupValue()								
							})
						});	
					}
				}
			}
		},*/
    {
			xtype				: 'combo',
			id					: 'cmbTipoLinea',
			name				: 'tipoLinea',
			hiddenName 		:'tipoLinea', 
			fieldLabel		: 'Tipo de Linea',
			mode				: 'local',
			autoLoad: false,
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			emptyText: 'Seleccionar',
			forceSelection	: true,
			triggerAction	: 'all',
			typeAhead: true,
			width			: 250,
			minChars : 1,
			allowBlank: true,
			store				: catTipoLinea,
			listeners: {
				select: {
					fn: function(combo) {
						var sClaveIF1 = Ext.getCmp('sClaveIF1');
						sClaveIF1.reset();
						sClaveIF1.setFieldLabel('Intermediario Financiero');
						Ext.getCmp('id_cs_tipo').reset();
						
						catIntermediario.removeAll();
            
						if(combo.getValue()=='D'){
						  catTipoAfiliado.loadData([['B','Intermediario Financiero'],['NB','Intermediario Financiero No Bancario']]);
						}else if(combo.getValue()=='C'){
						  catTipoAfiliado.loadData([['B','Intermediario Financiero'],['NB','Intermediario Financiero No Bancario'],['CE','Cliente Externo']]);
						}
						
					}
				}
			}
		},
    {
			xtype				: 'combo',
			id					: 'id_cs_tipo',
			name				: 'cs_tipo_B',
			hiddenName 		:'cs_tipo_B', 
			fieldLabel		: 'Afiliado',
			mode				: 'local',
			autoLoad: false,
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			emptyText: 'Seleccionar',
			forceSelection	: true,
			triggerAction	: 'all',
			typeAhead: true,
			width			: 250,
			minChars : 1,
			allowBlank: true,
			store				: catTipoAfiliado,
			listeners: {
				select: {
					fn: function(combo) {
						var valor =	(Ext.getCmp('id_cs_tipo')).getValue();
						var valorTipoLinea = Ext.getCmp('cmbTipoLinea').getValue();
						var sClaveIF1 = Ext.getCmp('sClaveIF1');
						if(valor=='CE'){
							sClaveIF1.setFieldLabel('Cliente Externo');
						}else if(valor=='B'){
							sClaveIF1.setFieldLabel('Intermediario Financiero');
						}else if(valor=='NB'){
							sClaveIF1.setFieldLabel('Intermediario Financiero No Bancario');
						}
						
						Ext.getCmp('sClaveIF1').reset();
						catIntermediario.load({
							params : Ext.apply({
							  cs_tipo_B: valor,
							  tipoLinea: valorTipoLinea
							})
						});	
          }
        }
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			name: 'sClaveIF',
			id: 'sClaveIF1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'sClaveIF',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,			
			store: catIntermediario,
			tpl:'<tpl for=".">' +
				 '<tpl if="!Ext.isEmpty(loadMsg)">'+
				 '<div class="loading-indicator">{loadMsg}</div>'+
				 '</tpl>'+
				 '<tpl if="Ext.isEmpty(loadMsg)">'+
				 '<div class="x-combo-list-item">' +
				 '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
				 '</div></tpl></tpl>'			
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Moneda',
					name: 'sClaveMoneda',
					id: 'sClaveMoneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'sClaveMoneda',					
					forceSelection : true,
					triggerAction : 'all',
					msgTarget: 'side',
					margins: '0 20 0 0',
					typeAhead: true,
					minChars : 1,
					forceSelection : true,
					store: catMoneda				
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Mes de Corte desde',
			width: 800,	
			combineErrors: false,
			//msgTarget: 'side',
			//margins: '0 20 0 0',
			items: [		
			{
				xtype				: 'combo',
				id					: 'sMes_ini1',
				name				: 'sMes_ini',
				hiddenName 		: 'sMes_ini',
				emptyText:     'Mes ...',
				fieldLabel		: '',
				width				: 80,
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	:'descripcion',					
				store				: catalogoMes,
				msgTarget: 'side',
				margins: '0 20 0 0'				
				},
				{
					xtype: 'combo',
					fieldLabel: '',
					name: 'sAnio_ini',
					id: 'sAnio_ini1',
					mode: 'local',
					width				: 70,
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'A�o ...',			
					valueField: 'clave',
					hiddenName : 'sAnio_ini',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,					
					forceSelection : true,
					store: catAnio,
					msgTarget: 'side',
					margins: '0 20 0 0'					
				},
				{
						xtype: 'displayfield',
						value: 'hasta:',
						width: 33
					},	
				{
					xtype				: 'combo',
					id					: 'sMes_fin1',
					name				: 'sMes_fin',
					hiddenName 		: 'sMes_fin', 
					fieldLabel		: '',
					emptyText:     'Mes ...',
					width				: 80,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'descripcion',					
					store				: catalogoMes,
					msgTarget: 'side',
					margins: '0 20 0 0'					
				},
				{
					xtype: 'combo',
					fieldLabel: '',
					name: 'sAnio_fin',
					id: 'sAnio_fin1',
					mode: 'local',
					width				: 70,
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'A�o ...',			
					valueField: 'clave',
					hiddenName : 'sAnio_fin',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					msgTarget: 'side',
					margins: '0 20 0 0',					
					forceSelection : true,
					store: catAnio			
				}
			]
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 120,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:  elementosForma,
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
					
					
					var id_cs_tipo =	Ext.getCmp('id_cs_tipo');	
						
					var sMes_ini1 = Ext.getCmp('sMes_ini1');					
					var sAnio_ini1 = Ext.getCmp('sAnio_ini1');
					
					var sMes_fin1 = Ext.getCmp('sMes_fin1');					
					var sAnio_fin1 = Ext.getCmp('sAnio_fin1');
					var cmbTipoLinea = Ext.getCmp('cmbTipoLinea');
					
					if (Ext.isEmpty(cmbTipoLinea.getValue()) ){
						cmbTipoLinea.markInvalid( 'Este campo es obigatorio');
						return;
					}
          
					if (Ext.isEmpty(id_cs_tipo.getValue()) ){
						id_cs_tipo.markInvalid( 'Este campo es obigatorio');
						return;
					}
					
					if (Ext.isEmpty(sMes_ini1.getValue()) ){
						sMes_ini1.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(sAnio_ini1.getValue()) ){
						sAnio_ini1.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(sMes_fin1.getValue()) ){
						sMes_fin1.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(sAnio_fin1.getValue()) ){
						sAnio_fin1.markInvalid('Este campo es obigatorio');
						return;
					}
		
					fp.el.mask('Enviando...', 'x-mask-loading');		
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Consultar'												
						})
					});										
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15cedulaconcExt.jsp';					
				}
			}
		]	
		
	});
	
	
		var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta
		]
	});
	
	catMoneda.load();
	catAnio.load();
	
});