<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		 java.sql.*,
		com.netro.exception.*, 		
		com.netro.pdf.*,		
		 java.math.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log "		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
String codigo_agencia = (request.getParameter("codigo_agencia")!=null)?request.getParameter("codigo_agencia"):""; 
String codigo_sub_aplicacion = (request.getParameter("codigo_sub_aplicacion")!=null)?request.getParameter("codigo_sub_aplicacion"):""; 
String numero_prestamo = (request.getParameter("numero_prestamo")!=null)?request.getParameter("numero_prestamo"):""; 
String fecha_proyeccion = (request.getParameter("fecha_proyeccion")!=null)?request.getParameter("fecha_proyeccion"):""; 

JSONObject jsonObj = new JSONObject();
String consulta = "",  infoRegresar ="";


if (informacion.equals("Consultar")) {

	List registros =   getRegistros(fecha_proyeccion, codigo_agencia  ,codigo_sub_aplicacion ,   numero_prestamo ) ;
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString(); 

}else  if (informacion.equals("Generar_PDF")) {

	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer("");
	String nombreArchivo = archivo.nombreArchivo()+".pdf";
	BigDecimal totalValorActual = new BigDecimal("0.00");
	BigDecimal totalValorProyectado = new BigDecimal("0.00");

	try {
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
		
		pdfDoc.setTable(3, 60);
		pdfDoc.setCell("Conceptos", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Valor Actual", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Valor Proyectado", "formasmenB", ComunesPDF.CENTER);
		
		List registros =   getRegistros(fecha_proyeccion, codigo_agencia  ,codigo_sub_aplicacion ,   numero_prestamo ) ;
			
		for(int i=0; i < registros.size(); i++){
			HashMap datos = (HashMap)registros.get(i);
			
			String  concepto  = (datos.get("CONCEPTOS").toString()) ;
			String valorActual  = (datos.get("VALOR_ACTUAL").toString()) ;
			String  valorProyectado  = (datos.get("VALOR_PROYECTADO").toString()) ;
			
			if (valorActual!=null) totalValorActual = totalValorActual.add(new BigDecimal(valorActual));
			if (valorProyectado!=null) totalValorProyectado = totalValorProyectado.add(new BigDecimal(valorProyectado));
				
				
			pdfDoc.setCell(concepto, "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(valorActual,2), "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(valorProyectado,2), "formas", ComunesPDF.RIGHT);
		}
		pdfDoc.setCell("Monto Total", "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalValorActual,2), "formas", ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(totalValorProyectado,2), "formas", ComunesPDF.RIGHT);
		
		pdfDoc.addTable();
		pdfDoc.endDocument();
		
		
	} catch(Exception e) {
		e.printStackTrace();	
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>

<%!

	/**
	 *Query para sacar  las Firmas de la cedula de conciliación
	 * @return 
	 * @param ic_if
	 */
	public List getRegistros(String fecha_proyeccion, String codigo_agencia  ,String codigo_sub_aplicacion ,  String  numero_prestamo )  {
		
		log.info ("getDatosFirma(E)  ");
		
		AccesoDB 	con = new AccesoDB();
      boolean		exito = true;	
		StringBuffer strSQL = new StringBuffer();
		List lVarBind		= new ArrayList();	
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		
		CallableStatement cs;
		
		HashMap	datos = new HashMap();
		String  error ="",  secuencia ="";
		List registros= new ArrayList();
		String consulta ="";
		
		try {
			con.conexionDB();  
			
			log.debug("fecha_proyeccion "+fecha_proyeccion);
			
			Vector _vectorFecha = Comunes.explode("/",fecha_proyeccion);
			String _dia = _vectorFecha.get(0).toString();
			String _mes = _vectorFecha.get(1).toString();
			String _anio = _vectorFecha.get(2).toString();	
			
			cs = con.ejecutaSP("PKG_PROYECCION.P_PROYECCION(?,?,?,?,?,?,?,?,?,?,?,?)");
			cs.setInt(1, 1);												//01.- CODEMPRESA
			cs.setInt(2, Integer.parseInt(codigo_agencia));					//02.- CODAGENCIA
			cs.setInt(3, Integer.parseInt(codigo_sub_aplicacion));			//03.- SUBAPLICACION
			cs.setInt(4, Integer.parseInt(numero_prestamo));				//04.- NUMPRESTAMO
			cs.setDate(5, java.sql.Date.valueOf(_anio+"-"+_mes+"-"+_dia), Calendar.getInstance());	//05.- FECHAFINPROY
			cs.setInt(6, 15);												//06.- W_OPERACION
			cs.setInt(7, 70);												//07.- W_TRANSACCION
			cs.setInt(8, 8);												//08.- W_TRN_INT_X_SUSP
			cs.setInt(9, 38);												//09.- W_TRN_INT_A_CAP
			cs.setInt(10, 1);												//10.- VERIFICATASA
			cs.setNull(11, Types.INTEGER);									//11.- SECUENCIAPROY (out)
			cs.registerOutParameter(11, Types.INTEGER);
			cs.setString(12, null);											//12.- IDENTIFICADORERROR (out)
			cs.registerOutParameter(12, Types.VARCHAR);
			
			cs.execute();
			secuencia = cs.getString(11); 
			error = cs.getString(12);
			cs.close();
					
			strSQL = new StringBuffer();
			strSQL.append( " select ts.descripcion as concepto , "+
									" ps.valor as valorActual , "+
									" ps.valor_proyectado as valorProyectado "+
							" from PR_PROYECCION_SALDOS ps, PR_TIPOS_SALDO ts"+
							" where ps.codigo_tipo_saldo = ts.codigo_tipo_saldo"+
							" and ps.secuencia =  ? ");									
									
				lVarBind		= new ArrayList();				
				lVarBind.add(secuencia);	
							
				log.debug("SQL.toString() "+strSQL.toString());
				log.debug("varBind "+lVarBind);
			
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				rs = ps.executeQuery();
		
         while( rs.next() ){ 
				datos = new HashMap();
				datos.put("CONCEPTOS", rs.getString("concepto")==null?"":rs.getString("concepto"));
				datos.put("VALOR_ACTUAL", rs.getString("valorActual")==null?"":rs.getString("valorActual"));	
				datos.put("VALOR_PROYECTADO", rs.getString("valorProyectado")==null?"":rs.getString("valorProyectado"));	
				registros.add(datos);
			}			
			rs.close();
			ps.close();					
			
			
			
		} catch(Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error ("getDatosFirma  " + e);
			throw new AppException("SIST0001");			
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getDatosFirma (S) ");
			}			
		}
		return registros;
	}



%>