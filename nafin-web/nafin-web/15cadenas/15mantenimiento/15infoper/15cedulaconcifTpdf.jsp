<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="netropology.utilerias.*"%>
<%@ page import="com.netro.descuento.*"%>
<%@ page import="com.netro.pdf.*"%>
<%@ page import="net.sf.json.JSONArray"%>
<%@ page import="net.sf.json.JSONObject"%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String sFechaCorte = (request.getParameter("sFechaCorte")==null)?"":request.getParameter("sFechaCorte");
String sClaveMoneda = (request.getParameter("sClaveMoneda")==null)?"":request.getParameter("sClaveMoneda");
String sClaveIF = (request.getParameter("sClaveIF")==null)?"":request.getParameter("sClaveIF");
String sTipoBanco = (request.getParameter("cs_tipo")==null)?"":request.getParameter("cs_tipo");
String sNombreMoneda  = (request.getParameter("sNombreMoneda")==null)?"":request.getParameter("sNombreMoneda");
String sNombreIF  = (request.getParameter("sNombreIF")==null)?"":request.getParameter("sNombreIF");
String tipoLinea  = (request.getParameter("tipoLinea")==null)?"":request.getParameter("tipoLinea");
String sClaveCliExt = (request.getParameter("sClaveCliExt")==null)?"":request.getParameter("sClaveCliExt");
String sNumSirac = (request.getParameter("sNumSirac")==null)?"":request.getParameter("sNumSirac");
String query = null;

String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
JSONObject jsonObj = new JSONObject();
String infoRegresar ="";

CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer("");
String nombreArchivo = archivo.nombreArchivo()+".pdf";
AccesoDB con = new AccesoDB();
	
try {
	con.conexionDB();

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
	StringBuffer strSQL = new StringBuffer();
	List varBind = new ArrayList();

	if(!"C".equals(tipoLinea)){
	strSQL.append(" SELECT /*+use_nl(c i mon) index (c in_com_estado_cuenta_01_nuk) ordered*/");
	strSQL.append(" i.cg_razon_social,");
	strSQL.append(" cg_subapl_desc tipo_credito,");
	strSQL.append(" SUM(c.fg_montooperado) monto_operado,");
	strSQL.append(" SUM(c.fg_capitalvigente) capital_vigente, ");
	strSQL.append(" SUM(c.fg_capitalvencido) capital_vencido,");
	strSQL.append(" SUM(c.fg_interesprovi) interes_vigente, ");
	strSQL.append(" SUM(c.fg_interesvencido) interes_vencido,");
	strSQL.append(" SUM(c.FG_INTERESMORAT + c.FG_SOBRETASAMOR) moras,");
	strSQL.append(" SUM(c.fg_adeudototal) total_adeudo,");
	strSQL.append(" COUNT(c.ig_prestamo) descuentos,");
	strSQL.append(" ig_subapl subaplic,");
	strSQL.append(" SUM(c.fg_saldoinsoluto) saldo_insoluto,");
	strSQL.append(" i.ic_financiera,");
	strSQL.append(" mon.cd_nombre nombre_moneda");
	strSQL.append(" FROM com_estado_cuenta c");
	strSQL.append(", comcat_if i");
	strSQL.append(", comcat_moneda mon");
	strSQL.append(" WHERE c.ic_if = i.ic_if");
	strSQL.append(" AND c.ic_moneda = mon.ic_moneda");
	strSQL.append(" AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy')");
	strSQL.append(" AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1");
	
	varBind.add(sFechaCorte);
	varBind.add(sFechaCorte);
	
	if (!sClaveMoneda.equals("")) {
		strSQL.append(" AND c.ic_moneda = ?");
		varBind.add(new Integer(sClaveMoneda));
	} else {
		strSQL.append(" AND c.ic_moneda IN (?, ?)");
		varBind.add(new Integer(1));
		varBind.add(new Integer(54));
	}

	if (!sClaveIF.equals("")) {
		strSQL.append(" AND c.ic_if = ?");
		varBind.add(new Integer(sClaveIF));
	}

	strSQL.append(" GROUP BY i.cg_razon_social, cg_subapl_desc, ig_subapl, i.ic_financiera, mon.cd_nombre");
	}else{
		if(!"CE".equals(sTipoBanco)){
			strSQL.append(" SELECT /*+use_nl(c i mon) index (c in_com_estado_cuenta_01_nuk) ordered*/");
			//strSQL.append(" c.cg_nombrecliente as cg_razon_social,");
			strSQL.append(" i2.cg_razon_social as cg_razon_social,");
			strSQL.append(" cg_subapl_desc tipo_credito,");
			strSQL.append(" SUM(c.fg_montooperado) monto_operado,");
			strSQL.append(" SUM(c.fg_capitalvigente) capital_vigente, ");
			strSQL.append(" SUM(c.fg_capitalvencido) capital_vencido,");
			strSQL.append(" SUM(c.fg_interesprovi) interes_vigente, ");
			strSQL.append(" SUM(c.fg_interesvencido) interes_vencido,");
			strSQL.append(" SUM(c.FG_INTERESMORAT + c.FG_SOBRETASAMOR) moras,");
			strSQL.append(" SUM(c.fg_adeudototal) total_adeudo,");
			strSQL.append(" COUNT(c.ig_prestamo) descuentos,");
			strSQL.append(" ig_subapl subaplic,");
			strSQL.append(" SUM(c.fg_saldoinsoluto) saldo_insoluto,");
			strSQL.append(" c.ig_cliente as ic_financiera,");
			strSQL.append(" mon.cd_nombre nombre_moneda");
			strSQL.append(" FROM com_estado_cuenta c");
			strSQL.append(", comcat_if i");
			strSQL.append(", comcat_if i2");
			strSQL.append(", comcat_moneda mon");
			strSQL.append(" WHERE c.ic_if = i.ic_if");
			strSQL.append(" AND c.ig_cliente = i2.in_numero_sirac ");
			strSQL.append(" AND c.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND c.ic_if = 12 ");
			strSQL.append(" AND c.ig_cliente is not null ");
			strSQL.append(" AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy')");
			strSQL.append(" AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1");
			
			varBind.add(sFechaCorte);
			varBind.add(sFechaCorte);
			
			if (!sClaveMoneda.equals("")) {
				strSQL.append(" AND c.ic_moneda = ?");
				varBind.add(new Integer(sClaveMoneda));
			} else {
				strSQL.append(" AND c.ic_moneda IN (?, ?)");
				varBind.add(new Integer(1));
				varBind.add(new Integer(54));
			}
		
			if (!sNumSirac.equals("")) {
				strSQL.append(" AND c.ig_cliente = ?");
				varBind.add(new Integer(sNumSirac));
			}
		
			strSQL.append(" GROUP BY i2.cg_razon_social, cg_subapl_desc, ig_subapl, c.ig_cliente, mon.cd_nombre");
		}else{
			strSQL.append(" SELECT /*+use_nl(c i mon) index (c in_com_estado_cuenta_01_nuk) ordered*/");
			strSQL.append(" cce.cg_razon_social,");
			strSQL.append(" cg_subapl_desc tipo_credito,");
			strSQL.append(" SUM(c.fg_montooperado) monto_operado,");
			strSQL.append(" SUM(c.fg_capitalvigente) capital_vigente, ");
			strSQL.append(" SUM(c.fg_capitalvencido) capital_vencido,");
			strSQL.append(" SUM(c.fg_interesprovi) interes_vigente, ");
			strSQL.append(" SUM(c.fg_interesvencido) interes_vencido,");
			strSQL.append(" SUM(c.FG_INTERESMORAT + c.FG_SOBRETASAMOR) moras,");
			strSQL.append(" SUM(c.fg_adeudototal) total_adeudo,");
			strSQL.append(" COUNT(c.ig_prestamo) descuentos,");
			strSQL.append(" ig_subapl subaplic,");
			strSQL.append(" SUM(c.fg_saldoinsoluto) saldo_insoluto,");
			strSQL.append(" c.ig_cliente as ic_financiera,");
			strSQL.append(" mon.cd_nombre nombre_moneda");
			strSQL.append(" FROM com_estado_cuenta c");
			strSQL.append(", comcat_cli_externo cce ");
			strSQL.append(", comcat_moneda mon");
			strSQL.append(" WHERE c.ig_cliente = cce.in_numero_sirac");
			strSQL.append(" AND c.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND c.ic_if = 12 ");
			strSQL.append(" AND c.ig_cliente is not null ");
			strSQL.append(" AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy')");
			strSQL.append(" AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1");
			
			varBind.add(sFechaCorte);
			varBind.add(sFechaCorte);
			
			if (!sClaveMoneda.equals("")) {
				strSQL.append(" AND c.ic_moneda = ?");
				varBind.add(new Integer(sClaveMoneda));
			} else {
				strSQL.append(" AND c.ic_moneda IN (?, ?)");
				varBind.add(new Integer(1));
				varBind.add(new Integer(54));
			}
		
			if (!sNumSirac.equals("")) {
				strSQL.append(" AND cce.in_numero_sirac = ?");
				varBind.add(new Integer(sNumSirac));
			}
		
			strSQL.append(" GROUP BY cce.cg_razon_social, cg_subapl_desc, ig_subapl, c.ig_cliente, mon.cd_nombre");
		}
	}
	
	//System.out.println("..:: strSQL: "+strSQL.toString());
	//System.out.println("..:: varBind: "+varBind);
	
	PreparedStatement ps = con.queryPrecompilado(strSQL.toString(), varBind);
	ResultSet rs = ps.executeQuery();
/*
	query = 
		" SELECT   /*+ USE_NL(C,I) INDEX (C CP_COM_ESTADO_CUENTA_PK) *\/"   +
		"        i.cg_razon_social, cg_subapl_desc tipo_credito,"   +
		"        SUM (c.fg_montooperado) monto_operado,"   +
		"        SUM (c.fg_capitalvigente) capital_vigente, "   +
		"        SUM (c.fg_capitalvencido) capital_vencido,"   +
		"        SUM (c.fg_interesprovi) interes_vigente, "   +
		"        SUM (c.fg_interesvencido) interes_vencido,"   +
		"        SUM (c.FG_INTERESMORAT + c.FG_SOBRETASAMOR) moras, "   +
		"        SUM (c.fg_adeudototal) total_adeudo, "   +
		"        COUNT (c.ig_prestamo) descuentos,"   +
		"        ig_subapl subaplic, "   +
		"        SUM (c.fg_saldoinsoluto) saldo_insoluto"   +
		"        , i.ic_financiera"   +
		"   FROM com_estado_cuenta c, comcat_if i"   +
		"  WHERE c.ic_if = i.ic_if"   +
		"    AND TRUNC (c.df_fechafinmes) = TO_DATE (?, 'DD/MM/YYYY')"   +
		"    AND c.ic_moneda = ?"   +
		"    AND c.ic_if = ?"   +
		"  GROUP BY i.cg_razon_social,  cg_subapl_desc, ig_subapl, i.ic_financiera"  ;
//out.println("query:"+query+"<br>");
	PreparedStatement ps = con.queryPrecompilado(query);
	ps.setString(1, sFechaCorte);
	ps.setString(2, sClaveMoneda);
	ps.setString(3, sClaveIF);
	ResultSet rs = ps.executeQuery();
*/
	int registros=0;
	double dTotal_MontoOperado=0, dTotal_CapitalVigente=0, dTotal_CapitalVencido=0;
	double dTotal_InteresVigente=0, dTotal_InteresVencido=0, dTotal_Moras=0 ,dTotal_TotalAdeudo=0, dTotal_Descuentos=0;
	double dSaldoInsoluto=0;
	String sMontoOperado="", sCapitalVigente="", sCapitalVencido="",sInteresVencido="";
	String sMoras="", sTotalAdeudo="", sDescuentos="", sFechaEnvio="", sTipoCredito="", sInteresVigente="";
	String sClaveSubAplic = "", sSaldoInsoluto = "", sNumeroSirac ="";
	
	ArrayList alAtributos = new ArrayList();
	ArrayList alRegistros= new ArrayList();

	while (rs.next()) {
		alAtributos = new ArrayList();
		sNombreIF        = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
		sTipoCredito     = (rs.getString(2) == null) ? "" : rs.getString(2).trim();
		sMontoOperado    = (rs.getString(3) == null) ? "" : rs.getString(3).trim();
		sCapitalVigente  = (rs.getString(4) == null) ? "" : rs.getString(4).trim();
		sCapitalVencido  = (rs.getString(5) == null) ? "" : rs.getString(5).trim();
		sInteresVigente  = (rs.getString(6) == null) ? "" : rs.getString(6).trim();
		sInteresVencido  = (rs.getString(7) == null) ? "" : rs.getString(7).trim();
		sMoras           = (rs.getString(8) == null) ? "0" : rs.getString(8).trim();
		sTotalAdeudo     = (rs.getString(9) == null) ? "" : rs.getString(9).trim();
		sDescuentos      = (rs.getString(10) == null) ? "" : rs.getString(10).trim();
		sClaveSubAplic	 = (rs.getString(11) == null) ? "" : rs.getString(11).trim();
		sSaldoInsoluto	 = (rs.getString(12) == null) ? "0" : rs.getString(12).trim();
		sNumeroSirac	 = (rs.getString("ic_financiera") == null) ? "" : rs.getString("ic_financiera").trim();
		sNombreMoneda = rs.getString("nombre_moneda")==null?"":rs.getString("nombre_moneda");//FODEA 047 - 2010 ACF
		
		dTotal_MontoOperado   += Double.parseDouble(sMontoOperado);
		dTotal_CapitalVigente += Double.parseDouble(sCapitalVigente);
		dTotal_CapitalVencido += Double.parseDouble(sCapitalVencido);
		dTotal_InteresVigente += Double.parseDouble(sInteresVigente);
		dTotal_InteresVencido += Double.parseDouble(sInteresVencido);
		dTotal_Moras          += Double.parseDouble(sMoras);
		dTotal_TotalAdeudo    += Double.parseDouble(sTotalAdeudo);
		dTotal_Descuentos     += Double.parseDouble(sDescuentos);
		dSaldoInsoluto	   	  += Double.parseDouble(sSaldoInsoluto);

		alAtributos.add(sTipoCredito.replace(',',' '));
		alAtributos.add(Comunes.formatoDecimal(sMontoOperado,2,false));
		alAtributos.add(Comunes.formatoDecimal(sCapitalVigente,2,false));
		alAtributos.add(Comunes.formatoDecimal(sCapitalVencido,2,false));
		alAtributos.add(Comunes.formatoDecimal(sInteresVencido,2,false));
		alAtributos.add(Comunes.formatoDecimal(sInteresVigente,2,false));
		alAtributos.add(Comunes.formatoDecimal(sMoras,2,false));
		alAtributos.add(Comunes.formatoDecimal(sTotalAdeudo,2,false));
		alAtributos.add(Comunes.formatoDecimal(sDescuentos,0,false));
		alAtributos.add(Comunes.formatoDecimal(sClaveSubAplic,2,false));
		alAtributos.add(Comunes.formatoDecimal(sSaldoInsoluto,0,false));
		alRegistros.add(alAtributos);
	}//while
	
	//query para obtener las firmas
	String queryFirmas = " SELECT"   +
	                     " IC_FIRMA_CEDULA FIRMA_IF " +
						"       ,CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO NOMBRE_IF " +
						" 	  ,CG_PUESTO PUESTO_IF " +
						" 	  ,(SELECT IC_FIRMA_CEDULA FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S'  AND IC_CLIENTE_EXTERNO IS NULL) FIRMA_NAFIN " +
						 " 	  ,(SELECT CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO RESP_IF "   +
						"        FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) RESP_NAFIN " +
						" 	  ,(SELECT CG_PUESTO FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) PUESTO_NAFIN " +
						"	  ,TO_CHAR(sysdate,'DD/MM/YYYY') FECHA " +
						 " FROM "   +
						 " 	  COMCAT_FIRMA_CEDULA"   +
						 " WHERE "   +
						 "       IC_IF=? "   +
						 " 	  AND CS_ACTIVO='S'"  ;
	//out.println("queryFirmas:"+queryFirmas+"<br>");
	ps = con.queryPrecompilado(queryFirmas);
	ps.setString(1, sClaveIF);
	rs = ps.executeQuery();
    String sClaveFirmaIF="", sFirmaIF="", sPuestoIF="", sClaveFirmaNafin="", sFirmaNafin="", sPuestoNafin="";
	while (rs.next()) {
       	sClaveFirmaIF    = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
       	sFirmaIF         = (rs.getString(2) == null) ? "" : rs.getString(2).trim();
       	sPuestoIF        = (rs.getString(3) == null) ? "" : rs.getString(3).trim();
       	sClaveFirmaNafin = (rs.getString(4) == null) ? "" : rs.getString(4).trim();
       	sFirmaNafin      = (rs.getString(5) == null) ? "" : rs.getString(5).trim();
       	sPuestoNafin     = (rs.getString(6) == null) ? "" : rs.getString(6).trim();
       	sFechaEnvio      = (rs.getString(7) == null) ? "" : rs.getString(7).trim();
	}//fin while
	//fin query para obtener las firmas
	rs.close();

	ArrayList alTotales = new ArrayList();
	alTotales.add(Comunes.formatoDecimal(dTotal_MontoOperado,2,false));
	alTotales.add(Comunes.formatoDecimal(dTotal_CapitalVigente,2,false));
	alTotales.add(Comunes.formatoDecimal(dTotal_CapitalVencido,2,false));
	alTotales.add(Comunes.formatoDecimal(dTotal_InteresVigente,2,false));
	alTotales.add(Comunes.formatoDecimal(dTotal_InteresVencido,2,false));
	alTotales.add(Comunes.formatoDecimal(dTotal_Moras,2,false));
	alTotales.add(Comunes.formatoDecimal(dTotal_TotalAdeudo,2,false));
	alTotales.add(Comunes.formatoDecimal(dTotal_Descuentos,0,false));
	alTotales.add(Comunes.formatoDecimal(dSaldoInsoluto,0,false));
	//session.setAttribute("Totales", alTotales);
	String sNota = new String("");
		query = "select cg_observaciones from com_observaciones_cedula "+
					"where trunc(df_fecha_corte) = to_date( ?, 'dd/mm/yyyy') "+
					"and ic_if = ? "+
					"and ic_moneda = ? ";
		if("C".equals(tipoLinea)){
			query += "and ig_cliente = ? "; 
		}
		
		ps.clearParameters();
		ps = con.queryPrecompilado(query);
		ps.setString(1,sFechaCorte);
		ps.setString(2,sClaveIF);
		ps.setString(3,sClaveMoneda);
		if("C".equals(tipoLinea)){
			ps.setString(4,sNumeroSirac);
		}
		
		rs = ps.executeQuery();
		if(rs.next())
			sNota = rs.getString("cg_observaciones");
		alRegistros.add(sNota);
		rs.close();
		if(ps!=null) 
			ps.close();
		if(operacion.equals("") ) {
%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="300" cellpadding="2" cellspacing="0" border="0" align="center">
<tr>
  <td class="formas">&nbsp;</td>
</tr>
<%
}
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	float widths[] = {1, 2,1};
	pdfDoc.setTable(3, 100, widths);
	pdfDoc.setCellImage(strDirectorioPublicacion+"00archivos/15cadenas/15archcadenas/logos/nafinsa.gif",ComunesPDF.LEFT, 75);
	String encabezado = "NACIONAL FINANCIERA, S.N.C.\n" 
					+"SUBDIRECCION DE OPERACIONES DE CREDITO\n"
					+"RESUMEN DE LA CONCILIACIÓN DE SALDOS AL " + 
					sFechaCorte.substring(0,2) + " DE " + meses[Integer.parseInt(sFechaCorte.substring(3,5))-1].toUpperCase() + " DE " + sFechaCorte.substring(6,10) +"\n\n"
					+"("+sNumeroSirac+") "+sNombreIF+" - "+sNombreMoneda;
					//+sNombreIF+" - "+sNombreMoneda;
	pdfDoc.setCell(encabezado, "formasrepB", ComunesPDF.CENTER,1,1,0);
	pdfDoc.setCell("", "formasrepB", ComunesPDF.CENTER,1,1,0);
	pdfDoc.addTable();

//pdfDoc.addText(sNombreIF+" - "+sNombreMoneda, "formasrepB", ComunesPDF.CENTER);
//	pdfDoc.addText(sNombreMoneda, "formasB", ComunesPDF.CENTER);
	
	int numCols = 9;
	float widths2[] = {.8f, 2, 1, 1, 1, 1, 1, 1, 1, .7f};
	float widths3[] = {2, 1, 1, 1, 1, 1, 1, 1, .7f};


	if("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
		numCols++;
		pdfDoc.setTable(numCols, 100, widths2);
	} else {
		pdfDoc.setTable(numCols, 100, widths3);
	}
	
	if("NB".equals(sTipoBanco)) {
		pdfDoc.setCell("Tipo de Crédito", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto Operado", "formasmenB", ComunesPDF.CENTER);
	} else if("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
		pdfDoc.setCell("Sub aplicación", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Concepto Sub aplicación", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Saldo insoluto Nafin", "formasmenB", ComunesPDF.CENTER);
	}
	pdfDoc.setCell("Capital Vigente", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Capital Vencido", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Interés Vigente", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Interés Vencido", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Moras", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Total Adeudo", "formasmenB", ComunesPDF.CENTER);
	pdfDoc.setCell("Descuentos", "formasmenB", ComunesPDF.CENTER);
	
	//ArrayList alRegistros = (ArrayList)session.getAttribute("Registros");
	//out.println(alRegistros.size());
	for(int i=0; i < alRegistros.size()-1; i++){
		ArrayList alTemp = new ArrayList();
		alTemp = (ArrayList)alRegistros.get(i);
		if("NB".equals(sTipoBanco)) {
			pdfDoc.setCell(alTemp.get(0).toString(), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(1).toString(), true), "formasmen", ComunesPDF.RIGHT);
		} else if("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
			pdfDoc.setCell(Comunes.formatoDecimal(alTemp.get(9).toString(), 0), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(alTemp.get(0).toString(), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(10).toString(), true), "formasmen", ComunesPDF.RIGHT);
		}// else if("B".equals(sTipoBanco)) {
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(2).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(3).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(5).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(4).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(6).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(7).toString(), true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoDecimal(alTemp.get(8).toString(), 0), "formasmen", ComunesPDF.RIGHT);
	}
	//ArrayList alTotales = (ArrayList)session.getAttribute("Totales");
	int colSpan = 1;
	if("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) 
		colSpan++;	
	pdfDoc.setCell("TOTAL", "formasmenB", ComunesPDF.CENTER, colSpan);
	if("NB".equals(sTipoBanco)) {
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(0).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	} else if("B".equals(sTipoBanco) || "CE".equals(sTipoBanco)) {
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(8).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	}
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(1).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(2).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(3).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(4).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(5).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(6).toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoDecimal(alTotales.get(7).toString(), 0), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.addTable();
	
	String nota = alRegistros.get(alRegistros.size()-1).toString();
	
	pdfDoc.addText("\nNOTA: "+nota, "formasB", ComunesPDF.LEFT);
	
	pdfDoc.addText("Observaciones:  " + sNota, "formasB", ComunesPDF.LEFT);
	
	pdfDoc.addText("\nConfirmo que los saldos operativos de Nacional Financiera, S.N.C. han sido conciliados con los registros contables de", "formas", ComunesPDF.CENTER);
	pdfDoc.addText(sNombreIF.substring(sNombreIF.indexOf(")")+1, sNombreIF.length()), "formasB", ComunesPDF.CENTER);
	
	pdfDoc.setTable(3, 85);
	
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCellImage(strDirectorioPublicacion + "00utils/gif/firma_pagos_cartera.gif", ComunesPDF.CENTER, 100);

	pdfDoc.setCell("_____________________________\n"+sFirmaIF, "formasB", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("_____________________________\n"+sFirmaNafin, "formasB", ComunesPDF.CENTER, 1, 1, 0);
	
	pdfDoc.setCell(sPuestoIF, "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell(sPuestoNafin, "formas", ComunesPDF.CENTER, 1, 1, 0);
	
	pdfDoc.setCell(sNombreIF.substring(sNombreIF.indexOf(")")+1, sNombreIF.length()), "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("NACIONAL FINANCIERA S.N.C.", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.addTable();
	
	float tWid[] = {1.5f, 1};
	
	pdfDoc.setTable(2, 100, tWid);
	pdfDoc.setCell("Nota: Si en un plazo de 10 días hábiles contados a partir de la recepción del presente Estado de Cuenta no se reciben las observaciones correspondientes se darán por aceptadas las cifras.", "formasrep", ComunesPDF.LEFT, 1, 1, 0);
	pdfDoc.setCell("Fecha de Envío:  "+sFechaEnvio, "formasrep", ComunesPDF.RIGHT, 1, 1, 0);
	pdfDoc.addTable();
	
	pdfDoc.endDocument();
	if(operacion.equals("") ) {
%>
<tr>
	<td class="formas" align="center"><br>
		<table cellpadding="3" cellspacing="1">
			<tr>
				<td class="celda02" width="100" align="center" height="30">
					<a href="<%=strDirecVirtualTemp%><%=nombreArchivo%>">Bajar Archivo</a>
				</td>
				<td class="celda02" width="100" align="center">
					<a href="javascript:close();">Cerrar</a>
				</td>
			</tr>
		</table>
	</td>
</tr>
<%
}
} catch(Exception e) {
	e.printStackTrace();
	if(operacion.equals("") ) {
	
%>
	<tr>
		<td class="formas" align="center"><br>
			<table width="600" border="1" cellspacing="0" cellpadding="2" bordercolor="#A5B8BF">
				<tr>
					<td class="celda01" align="center" bgcolor="silver">Hubo problemas al generar el archivo.</td>
				</tr>
			</table>
		</td>
	</tr>
<%
}
}finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
if(operacion.equals("") ) {
%>
</body>
</html>

<%
}
if(operacion.equals("VersionNueva") ) {
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();
	
}
%>
<%=infoRegresar%>