<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  			
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>  
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	String codigoCliente = (request.getParameter("codigoCliente") == null) ? "" : request.getParameter("codigoCliente");
	String numeroPrestamo = (request.getParameter("numeroPrestamo") == null) ? "" : request.getParameter("numeroPrestamo");
	String montoTotalInicial = "", montoTotalAdeudo= "";
	double  totalMonto = 0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	HashMap datos = new HashMap();
	int numRegistros=0;
	JSONArray registrost = new JSONArray();
	ConsulPrest1 paginado = new ConsulPrest1();
	CQueryHelperRegExtJS queryHelper1 = new CQueryHelperRegExtJS( paginado);
	paginado.setCodigoCliente(codigoCliente);
	paginado.setNumeroPrestamo(numeroPrestamo);
	if(informacion.equals("Consultar")  ||  informacion.equals("ArchivoPDF")||   informacion.equals("ConsTotales") )  {
		if(informacion.equals("Consultar")||  informacion.equals("ConsTotales")){
			try {
						
						Registros reg	=	queryHelper1.doSearch();
						StringBuffer Totales = new StringBuffer();
						while(reg.next()){
							numRegistros++;
							String numeroPrestamo1 = reg.getString("NUMERO_PRESTAMO");
							String codigoEmpresa = reg.getString("CODIGO_EMPRESA") ;
							String codigoAgencia = reg.getString("CODIGO_AGENCIA");
							String codigoSubAplicacion = reg.getString("CODIGO_SUB_APLICACION");
							HashMap registros = paginado.getValorTotal(numeroPrestamo1,codigoEmpresa,codigoAgencia,codigoSubAplicacion) ;
							if (registros!=null) montoTotalAdeudo = registros.get("TOTAL_ADEUDO").toString();
							reg.setObject("TOTAL_ADEUDO",registros.get("TOTAL_ADEUDO").toString());
							String totalAdeudoAux =registros.get("TOTAL_ADEUDO").toString();
							totalMonto +=  Double.parseDouble(totalAdeudoAux);
						}
					if (informacion.equals("Consultar") ) {
						consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
						jsonObj = JSONObject.fromObject(consulta);
						jsonObj.put("totalMonto",String.valueOf(totalMonto) );
						jsonObj.put("numeroRegistros",String.valueOf(numRegistros) );
						infoRegresar = jsonObj.toString();
					}
					if(informacion.equals("ConsTotales") ){
						datos = new HashMap();
						datos.put("monto_total",String.valueOf(totalMonto) );
						datos.put("total_prestamo",String.valueOf(numRegistros) );
						registrost.add(datos);
						consulta =  "{\"success\": true, \"total\": \"" + registrost.size() + "\", \"registros\": " + registrost.toString()+"}";
						jsonObj = JSONObject.fromObject(consulta);
					}
			} catch(Exception e) {
					throw new AppException("Error al obtener los datos", e);
			}
	   }else if(informacion.equals("ArchivoPDF") ){
			try {
				String nombreArchivo = queryHelper1.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error son demasiados registros", e);
			}
	}
	infoRegresar = jsonObj.toString();	
}
%>
<%=infoRegresar%>

