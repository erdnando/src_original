Ext.onReady(function(){


	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {	
				el.unmask();
				
				Ext.getCmp('btnImprimir').enable();				
			} else {		
				
				Ext.getCmp('btnImprimir').disable();				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15proyadeudoExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [	
			{	name: 'CONCEPTOS'},	
			{	name: 'VALOR_ACTUAL'},	
			{	name: 'VALOR_PROYECTADO'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});

	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Conceptos',
				tooltip: 'Conceptos',
				dataIndex: 'CONCEPTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{	header: 'Valor Actual',
				tooltip: 'Valor Actual',
				dataIndex: 'VALOR_ACTUAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
				
			},
			{	header: 'Valor Proyectado',
				tooltip: 'Valor Proyectado',
				dataIndex: 'VALOR_PROYECTADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			}
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 460,
		align: 'center',
		frame: false,		
		bbar: {
			items: [	
			'->','-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir'	,
					handler: function(boton, evento) {
					
						Ext.Ajax.request({
								url: '15proyadeudoExt.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{									
									informacion: 'Generar_PDF'
								}),							
								callback: descargaArchivo
							});
							
							
						}
				}			
			]
		}
	});
	
	
	
	var elementosForma  = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Agencia',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					fieldLabel: 'Agencia',
					name: 'codigo_agencia',
					id: 'codigo_agencia1',
					allowBlank: true,
					maxLength: 4,
					width: 50,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'Subaplicaci�n:',
					width: 80
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'Subaplicaci�n',
					name: 'codigo_sub_aplicacion',
					id: 'codigo_sub_aplicacion1',
					allowBlank: true,
					maxLength: 3,
					width: 50,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'N�mero Pr�stamo:',
					width: 70
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'N�mero Pr�stamo',
					name: 'numero_prestamo',
					id: 'numero_prestamo1',
					allowBlank: true,
					maxLength: 8,
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha proyectada',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_proyeccion',
					id: 'fecha_proyeccion',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',					
					margins: '0 20 0 0'  
				}
			]
		}
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 90,
		defaultType: 'textfield',		
		items:  elementosForma,
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
					
					var codigo_agencia = Ext.getCmp('codigo_agencia1');
					var codigo_sub_aplicacion = Ext.getCmp('codigo_sub_aplicacion1');
					var numero_prestamo = Ext.getCmp('numero_prestamo1');
					var fecha_proyeccion = Ext.getCmp('fecha_proyeccion');
					
					if (Ext.isEmpty(codigo_agencia.getValue()) ){
						codigo_agencia.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(codigo_sub_aplicacion.getValue()) ){
						codigo_sub_aplicacion.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(numero_prestamo.getValue()) ){
						numero_prestamo.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(fecha_proyeccion.getValue()) ){
						fecha_proyeccion.markInvalid('Este campo es obigatorio');
						return;
					}
					
					
					fp.el.mask('Enviando...', 'x-mask-loading');							
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Consultar'												
						})
					});
													
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15proyadeudoExt.jsp';					
				}
			}
		]	
		
	});
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta
		]
	});



})