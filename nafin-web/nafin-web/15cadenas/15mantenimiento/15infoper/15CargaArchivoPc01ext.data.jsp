<%@ page contentType="text/html;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.io.*,
		java.util.zip.*,
		com.jspsmart.upload.*,
		com.netro.cadenas.*,
		netropology.utilerias.*,
		com.netro.exception.*, 
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject"		
	errorPage="/00utils/error_extjs.jsp"
%>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload" />
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
//contentType="application/json;charset=UTF-8"
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	String 	PATH_FILE = strDirectorioTemp;
	
	String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
	String sregistro = "", linea = "";
	
	
	int numFiles = 0;
	response.setHeader("Cache-Control", "no-cache");
	
	
	int  start= 0, limit =0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	try{
	myUpload.initialize(pageContext);
	myUpload.upload();
	numFiles = myUpload.save(PATH_FILE);
	com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
	String nombreArch = myFile.getFileName();
	String tipoArchivo = myUpload.getRequest().getParameter("tipoArchivo");
	System.out.println("TIPO ARCHIVO:::: "+tipoArchivo);
	CargaArchivoPagosCartera carga = new CargaArchivoPagosCartera();
	carga.setNombreArchivo(nombreArch);
	carga.setTipoArchivo(tipoArchivo);
	carga.setFilePATH(PATH_FILE);
	
	if(carga.iniciarCarga()){
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("valida","ok");
		jsonObj.put("nombreArch",nombreArch);
		jsonObj.put("titulo","Archivo cargado:");
		jsonObj.put("err",carga.getErr());
		jsonObj.put("registros",new Integer(carga.getReg()));
		jsonObj.put("fechaHoy",fechaHoy);
		jsonObj.put("horaActual",horaActual);
		String user=strLogin+" "+strNombreUsuario;
		jsonObj.put("usuario",user);
		
		JSONArray 	cifrasControlDataArray 	= new JSONArray();
		JSONArray	registro						= null;
		
		registro = new JSONArray();
		registro.add("Archivo cargado:");
		registro.add(nombreArch);
		registro.add("TEXTO");
		cifrasControlDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Registros cargados:");
		registro.add(new Integer(carga.getReg()));
		registro.add("NUMERO_ENTERO");
		cifrasControlDataArray.add(registro);
			
		registro = new JSONArray();
		registro.add("Fecha de carga:");
		registro.add(fechaHoy);
		registro.add("FECHA");
		cifrasControlDataArray.add(registro);

		registro = new JSONArray();
		registro.add("Hora de carga:");
		registro.add(horaActual);
		registro.add("FECHA");
		cifrasControlDataArray.add(registro);

		registro = new JSONArray();
		registro.add("Usuario:");
		registro.add(strNombreUsuario);
		registro.add("TEXTO");
		cifrasControlDataArray.add(registro);

	

		jsonObj.put("cifrasControlDataArray",cifrasControlDataArray);
	}else{
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("valida","er");
		jsonObj.put("titulo","Errores durante la carga del archivo");
		nombreArch = nombreArch.substring(0,(nombreArch.length()-4));
					java.io.File ferr = new java.io.File(strDirectorioTemp+"logerr"+nombreArch+".txt");
					if(ferr.exists()){
						BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(ferr)));
						while ((sregistro=br.readLine())!=null){
							linea += sregistro+"\n";
						}
						br.close();
					}
		jsonObj.put("nombreArch",nombreArch);
		jsonObj.put("err",carga.getErr());
		
	}
}catch(Exception e) {
	System.err.println("El archivo no se pudo cargar");
	jsonObj.put("catch","El archivo no se pudo cargar "+e);
	e.printStackTrace();
}
	
   infoRegresar =jsonObj.toString();	

%>

<%=infoRegresar%>

