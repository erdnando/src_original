<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  			
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String proyecto = (request.getParameter("proyecto") == null) ? "" : request.getParameter("proyecto");
	String contrato = (request.getParameter("contrato") == null) ? "" : request.getParameter("contrato");
	
	
	
	
	//CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	
	int  start= 0, limit =0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
 if(informacion.equals("Consultar")  || informacion.equals("ConsultarTotales") ||  
			informacion.equals("ArchivoCSV")   ||   informacion.equals("ArchivoPDF")  ) {
		ConsultaContratos paginador = new ConsultaContratos();	
		paginador.setCodigoCliente(cliente);
		paginador.setProyectoCadena(proyecto);
		paginador.setNumeroContrato(contrato);
		
			
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
		if(informacion.equals("Consultar")  ||  informacion.equals("ArchivoPDF") )  {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
										
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			if(informacion.equals("Consultar") ){
				try {
					if (operacion.equals("Generar")) {	//Nueva consulta
						queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
					}
						consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
				} catch(Exception e) {
					throw new AppException("Error en la paginacion", e);
				}
					//Thread.sleep(5000);
					jsonObj = JSONObject.fromObject(consulta);
				
			}else if(informacion.equals("ConsultarTotales")) {
				consulta = queryHelper.getJSONResultCount(request);
				jsonObj = JSONObject.fromObject(consulta);
				//Thread.sleep(2000);
			}else if(informacion.equals("ArchivoPDF") ){
				try {
					String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					infoRegresar = jsonObj.toString();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
			}
			
		}else  if(informacion.equals("ArchivoCSV") )  {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("ConsultarTotales1")) {
		ConsultaContratos paginador = new ConsultaContratos();	
		paginador.setCodigoCliente(cliente);
		paginador.setProyectoCadena(proyecto);
		paginador.setNumeroContrato(contrato);
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
				consulta = queryHelper.getJSONResultCount(request);
				jsonObj = JSONObject.fromObject(consulta);
				//Thread.sleep(2000);
				infoRegresar = jsonObj.toString();	
				
}

%>


<%=infoRegresar%>

