<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.usuarios.*,
		javax.naming.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.descuento.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  			
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	PagosIFNB pagosIFNB = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String ic_if = (request.getParameter("comboIntermediario") == null) ? "" : request.getParameter("comboIntermediario");
	String moneda = (request.getParameter("comboMoneda") == null) ? "" : request.getParameter("comboMoneda");
	String fechaCorte = (request.getParameter("catalogoFechaCorte") == null) ? "" : request.getParameter("catalogoFechaCorte");
	String cs_tipo = (request.getParameter("cs_tipo")!=null)?request.getParameter("cs_tipo"):"";
	String nombreMoneda = (request.getParameter("nombreMoneda") == null) ? "" : request.getParameter("nombreMoneda");
	String nombreIF = (request.getParameter("nombreIF") == null) ? "" : request.getParameter("nombreIF");
	String tipoLinea = (request.getParameter("tipoLinea") == null) ? "" : request.getParameter("tipoLinea");
	String numSirac = "";
	StringBuffer  leyendaEncabezado 	= new StringBuffer();
   StringBuffer  firmas 	= new StringBuffer();
	
	if(ic_if!=null && !ic_if.equals("") && !ic_if.equals("T") ){
		if(!"CE".equals(cs_tipo)){
			HashMap hmSirac = pagosIFNB.getSiracFinaciera(ic_if);
			numSirac = hmSirac.get("NUMERO_SIRAC")!=null?((String)hmSirac.get("NUMERO_SIRAC")):""; 
		}else{
			numSirac = pagosIFNB.getSiracLineaCredito(ic_if);
		}
		
	}
	
	ConsCedulDeConciliacion paginador = new ConsCedulDeConciliacion();
	paginador.setSDateIni(fechaCorte);
	paginador.setCs_tipo(cs_tipo);
	paginador.setSClaveMoneda(moneda);
	paginador.setSClaveIF(ic_if);
	paginador.setTipoLinea(tipoLinea);
	paginador.setNumSirac(numSirac);
	int  start= 0, limit =0, cont = 0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	JSONArray registros1 = new JSONArray();
  
if(informacion.equals("catalogoIntermediario")){
	Registros catInter= paginador.getDataCatalogoIntermediario();
	jsonObj.put("total",new Integer( catInter.getNumeroRegistros()) );	
	jsonObj.put("registros", catInter.getJSONData() );	
   infoRegresar = jsonObj.toString();
   
}else if(informacion.equals("catalogoMoneda")){
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("COMCAT_MONEDA");
   cat.setCampoClave("IC_MONEDA");
   cat.setCampoDescripcion("CD_NOMBRE"); 
	cat.setOrden("2");
	cat.setValoresCondicionIn("1,54", Integer.class);
   infoRegresar = cat.getJSONElementos();
}else if (informacion.equals("catalogoFechaCorte")) {
	List alFechasCorte  = new ArrayList();
	HashMap datos = new HashMap();
	List registros  = new ArrayList();

	if(!"C".equals(tipoLinea)){
	if(!ic_if.equals("T")&&!ic_if.equals("")){
			alFechasCorte = pagosIFNB.getFechasCorte(6, "-", ic_if, tipoLinea, cs_tipo, "");	
		}else{
			alFechasCorte = pagosIFNB.getFechasCorte(6, "-", cs_tipo);
		}
	}else{
		if(!ic_if.equals("T")&&!ic_if.equals("")){
			alFechasCorte = pagosIFNB.getFechasCorte(6, "-", "12", tipoLinea, cs_tipo,  numSirac);
	}else{
			alFechasCorte = pagosIFNB.getFechasCorte(6, "-", "12", tipoLinea, cs_tipo, "");
		}
	}
  
  
	for(int i = 0; i<alFechasCorte.size();i++){
		datos = new HashMap();
		registros1.add(alFechasCorte.get(i));	
		datos.put("CLAVE",(String)alFechasCorte.get(i));
		datos.put("DESCRIPCION",(String)alFechasCorte.get(i));
		registros.add(datos);
	}
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("Consultar")){
	String 	observaciones_C  = "", sClaveFirmaIF  ="",   	sFirmaIF ="",   	sPuestoIF  ="", 	sClaveFirmaNafin ="",   	sFirmaNafin  ="",   	sPuestoNafin  ="";
	int registros=0;
	double dTotal_MontoOperado=0, dTotal_CapitalVigente=0, dTotal_CapitalVencido=0;
	double dTotal_InteresVigente=0, dTotal_InteresVencido=0, dTotal_Moras=0 ,dTotal_TotalAdeudo=0, dTotal_Descuentos=0;
	double dSaldoInsoluto=0;
   String sMontoOperado="", sCapitalVigente="", sCapitalVencido="",sInteresVencido="";
   String sMoras="", sTotalAdeudo="", sDescuentos="", sFechaEnvio="", sTipoCredito="", sInteresVigente="";
	String sClaveSubAplic = "", sSaldoInsoluto = "", sNumeroSirac ="";
	ArrayList alAtributos = new ArrayList();
	ArrayList alRegistros= new ArrayList();
	JSONObject 	resultado	= new JSONObject();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	Registros reg	=	queryHelper.doSearch();	
	if(ic_if.equals("T")){
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString();  
	}else{
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		   
		while (reg.next()) {
			alAtributos = new ArrayList();
       	sTipoCredito     = reg.getString("TIPO_CREDITO")==null?"":reg.getString("TIPO_CREDITO");
       	sMontoOperado    = reg.getString("MONTO_OPERADO")==null?"0":reg.getString("MONTO_OPERADO");
       	sCapitalVigente  = reg.getString("CAPITAL_VIGENTE")==null?"0":reg.getString("CAPITAL_VIGENTE");
       	sCapitalVencido  = reg.getString("CAPITAL_VENCIDO")==null?"0":reg.getString("CAPITAL_VENCIDO");
       	sInteresVigente  = reg.getString("INTERES_VIGENTE")==null?"0":reg.getString("INTERES_VIGENTE");
       	sInteresVencido  = reg.getString("INTERES_VENCIDO")==null?"0":reg.getString("INTERES_VENCIDO");
       	sMoras           = reg.getString("MORAS")==null?"0":reg.getString("MORAS");
       	sTotalAdeudo     = reg.getString("TOTAL_ADEUDO")==null?"0":reg.getString("TOTAL_ADEUDO");
       	sDescuentos      = reg.getString("DESCUENTOS")==null?"0":reg.getString("DESCUENTOS");
			sClaveSubAplic	 = reg.getString("SUBAPLIC")==null?"0":reg.getString("SUBAPLIC");
			sSaldoInsoluto	 = reg.getString("SALDO_INSOLUTO")==null?"0":reg.getString("SALDO_INSOLUTO");
			sNumeroSirac	 = reg.getString("IC_FINANCIERA")==null?"":reg.getString("IC_FINANCIERA");
			
			dTotal_MontoOperado   += Double.parseDouble(sMontoOperado);
			dTotal_CapitalVigente += Double.parseDouble(sCapitalVigente);
			dTotal_CapitalVencido += Double.parseDouble(sCapitalVencido);
			dTotal_InteresVigente += Double.parseDouble(sInteresVigente);
			dTotal_InteresVencido += Double.parseDouble(sInteresVencido);
			dTotal_Moras          += Double.parseDouble(sMoras);
			dTotal_TotalAdeudo    += Double.parseDouble(sTotalAdeudo);
			dTotal_Descuentos     += Double.parseDouble(sDescuentos);
			dSaldoInsoluto	   	 += Double.parseDouble(sSaldoInsoluto);
			
			alAtributos.add(sTipoCredito.replace(',',' '));
			alAtributos.add(Comunes.formatoDecimal(sMontoOperado,2,false));
			alAtributos.add(Comunes.formatoDecimal(sCapitalVigente,2,false));
			alAtributos.add(Comunes.formatoDecimal(sCapitalVencido,2,false));
			alAtributos.add(Comunes.formatoDecimal(sInteresVencido,2,false));
			alAtributos.add(Comunes.formatoDecimal(sInteresVigente,2,false));
			alAtributos.add(Comunes.formatoDecimal(sMoras,2,false));
			alAtributos.add(Comunes.formatoDecimal(sTotalAdeudo,2,false));
			alAtributos.add(Comunes.formatoDecimal(sDescuentos,0,false));
			alAtributos.add(Comunes.formatoDecimal(sClaveSubAplic,2,false));
			alAtributos.add(Comunes.formatoDecimal(sSaldoInsoluto,2,false));
			alRegistros.add(alAtributos);
			
			reg.setObject("stipo_credito",sTipoCredito);
			reg.setObject("stipo_credito1",sTipoCredito);
			reg.setObject("smonto_operado",sMontoOperado);
			reg.setObject("scapital_vigente",sCapitalVigente);
			reg.setObject("scapital_vencido",sCapitalVencido);
			reg.setObject("sinteres_vigente",sInteresVigente);
			reg.setObject("sinteres_vencido",sInteresVencido);
			reg.setObject("smoras",sMoras);
			reg.setObject("stotal_adeudo",sTotalAdeudo);
			reg.setObject("sdescuentos",sDescuentos);
			reg.setObject("sclave_sub_aplic",sClaveSubAplic);
			reg.setObject("ssaldo_insoluto",sSaldoInsoluto);
			cont++;
		}
		session.setAttribute("Registros", alRegistros);
		if(cont != 0){
			ArrayList alTotales = new ArrayList();
			alTotales.add(Comunes.formatoDecimal(dTotal_MontoOperado,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_CapitalVigente,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_CapitalVencido,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_InteresVigente,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_InteresVencido,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_Moras,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_TotalAdeudo,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_Descuentos,0,false));
			alTotales.add(Comunes.formatoDecimal(dSaldoInsoluto,2,false));
			session.setAttribute("Totales", alTotales);
		}
		leyendaEncabezado 	= new StringBuffer();
		leyendaEncabezado.append("<table cellpadding='2' cellspacing='2' border='0' align='center' ><tr><td class='titulos' align='center'>NACIONAL FINANCIERA, S.N.C.</td></tr>"+
						"<tr>	<td class='titulos' align='center'>	"+
						" RESUMEN DE LA CONCILIACIÓN DE SALDOS AL "+fechaCorte.substring(0,2) + " DE " + meses[Integer.parseInt(fechaCorte.substring(3,5))-1].toUpperCase() + " DE " + fechaCorte.substring(6,10)+"<br><br>"+
						nombreIF+"<br>"+nombreMoneda+"</td></tr></table>");
		HashMap regFirma = paginador.getDatosFirma(ic_if) ;
		sClaveFirmaIF  =regFirma.get("FIRMA_IF")==null?"":regFirma.get("FIRMA_IF").toString();
		sFirmaIF =regFirma.get("NOMBRE_IF")==null?"":regFirma.get("NOMBRE_IF").toString();
		sPuestoIF  =  regFirma.get("PUESTO_IF")==null?"":regFirma.get("PUESTO_IF").toString();
		sClaveFirmaNafin =regFirma.get("FIRMA_NAFIN")==null?"":regFirma.get("FIRMA_NAFIN").toString();
		sFirmaNafin  =regFirma.get("RESP_NAFIN")==null?"":regFirma.get("RESP_NAFIN").toString();
		sPuestoNafin  =regFirma.get("PUESTO_NAFIN")==null?"":regFirma.get("PUESTO_NAFIN").toString();
		String nota =  paginador.getNotas(fechaCorte, ic_if, moneda );
		firmas 	= new StringBuffer();	
		alRegistros.add(nota);
		firmas.append("<table cellpadding='1' cellspacing='2' border='0' width='780' > "+
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+		
				" <tr> <td align='justify' class='left'><b>NOTA:</b>&nbsp;"+nota+"&nbsp; </td></tr>"+
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+			
				" <tr> <td align='left' class='formas'><b>Observaciones:</b>&nbsp;"+observaciones_C+"&nbsp; </td></tr>"+	
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+					
				"<tr>"+
				"<td align='center'>"+
				"<table cellpadding='2' cellspacing='2' border='0'>"+
				"<tr>"+
				"<td class='formas' align='center'>	Confirmo que los saldos operativos de Nacional Financiera, S.N.C. han sido conciliados con los registros contables de<br> "+nombreIF.substring(nombreIF.indexOf(")")+1, nombreIF.length())+"	</td>"+
				"</tr>"+
				"</table>"+
				"</td>"+
				"</tr>"+
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+							
				"<tr>"+
				"	<td align='center'>"+
				"<table cellpadding='2' cellspacing='2' border='0' width='600'>"+
				"<tr>"+				
				" <td  class='formas' align='center' valign='top'>"+
				"</td>"+
				" <td class='formas' align='center' valign='top' >"+
				"	<img src='/nafin/00utils/gif/firma_pagos_cartera.gif' border='0' width='228' height='100'> "+
				"</td>"+
				" </tr> "+					
				"<tr> "+				
				" <td align='center' class='formas' valign='top'>_________________________________________<br> "+
				" <b>"+sFirmaIF+"</b><br>"+sPuestoIF+"<br>	"+nombreIF.substring(nombreIF.indexOf(")")+1, nombreIF.length())+"	</td>"+
				" <td align='center' class='formas' valign='top'>_________________________________________<br> "+
				"<b>"+sFirmaNafin+"</b><br>"+sPuestoNafin+"<br>	NACIONAL FINANCIERA S.N.C. </td>"+						
				"</tr>"+
				"</table>"+
				"</td>"+
				"</tr>"+				
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+								
				" <tr><td align='justify' class='formas'>Nota: Si en un plazo de 10 d&iacute;as h&aacute;biles contados a partir de la recepci&oacute;n del<br>presente Estado de Cuenta no se reciben las observaciones correspondientes se<br>dar&aacute;n por aceptadas las cifras. </td> </tr>"+	
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+		
				" <tr align='right' > <td align='right' class='formas'>Fecha de Env&iacute;o:&nbsp;"+sFechaEnvio+"</td></tr>	"+
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+		
				"</table>");
		String consulta2	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + reg.getJSONData()+"}";
		resultado = JSONObject.fromObject(consulta2);
		resultado.put("tipoBusqueda", cs_tipo);
		resultado.put("leyendaEncabezado", leyendaEncabezado.toString());
		resultado.put("firmas", firmas.toString());
		resultado.put("sNombreMoneda", nombreMoneda);
		resultado.put("sObservaciones", observaciones_C);
		resultado.put("sFirmaIF", sFirmaIF);
		resultado.put("sFirmaNafin", sFirmaNafin);
		resultado.put("sPuestoIF", sPuestoIF);
		resultado.put("sPuestoNafin", sPuestoNafin);
		resultado.put("sFechaEnvio", sFechaEnvio);
		resultado.put("sFechaCorte", fechaCorte);
		resultado.put("sClaveIF", ic_if);
		resultado.put("sClaveMoneda", moneda);
		resultado.put("sNombreIF", nombreIF);
		resultado.put("sNumeroSirac", sNumeroSirac);
	
		resultado.put("dTotal_MontoOperado",String.valueOf(dTotal_MontoOperado));
		resultado.put("dTotal_CapitalVigente",String.valueOf(dTotal_CapitalVigente));
		resultado.put("dTotal_CapitalVencido",String.valueOf(dTotal_CapitalVencido));
		resultado.put("dTotal_InteresVigente",String.valueOf(dTotal_InteresVigente));
		resultado.put("dTotal_InteresVencido",String.valueOf(dTotal_InteresVencido));
		resultado.put("dTotal_Moras",String.valueOf(dTotal_Moras));
		resultado.put("dTotal_TotalAdeudo",String.valueOf(dTotal_TotalAdeudo));
		resultado.put("dTotal_Descuentos",String.valueOf(dTotal_Descuentos));
		resultado.put("dSaldoInsoluto",String.valueOf(dSaldoInsoluto));
		
		
		infoRegresar = resultado.toString();  

	}
}
%>
<%=infoRegresar%>

