Ext.onReady(function(){
Ext.override(Ext.form.Field, {
  setFieldLabel : function(text) {
    if (this.rendered) {
      this.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(text);
    }
    this.fieldLabel = text;
  }
});
//---------------------------ocultaMuestraColumnas------------------------------
function ocultaMuestraColumnas(accion){
  var cmGConsulta = gridConsulta.getColumnModel();
  cmGConsulta.setHidden(cmGConsulta.findColumnIndex('FG_TOTDESCFOP'), accion);
  cmGConsulta.setHidden(cmGConsulta.findColumnIndex('FG_TOTDESCFINAPE'), accion);

	var cmGConsultaParcial = gridConsultaParcial.getColumnModel();
  cmGConsultaParcial.setHidden(cmGConsultaParcial.findColumnIndex('SUM(FG_TOTDESCFOP)'), accion);
  cmGConsultaParcial.setHidden(cmGConsultaParcial.findColumnIndex('SUM(FG_TOTDESCFINAPE)'), accion);

  var cmGConsultaTotal = gridConsultaTotal.getColumnModel();
  cmGConsultaTotal.setHidden(cmGConsultaTotal.findColumnIndex('SUM(FG_TOTDESCFOP)'), accion);
  cmGConsultaTotal.setHidden(cmGConsultaTotal.findColumnIndex('SUM(FG_TOTDESCFINAPE)'), accion);

}
//---------------------------descargaArchivoPDF------------------------------------
function descargaArchivoPDF(opts, success, response) {
	
if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			Ext.getCmp('btnGenerarPDF').enable();
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
			fp.el.unmask();
			Ext.getCmp('btnGenerarPDF').enable();
			Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
		}
	}
//---------------------------fin descargaArchivoPDF--------------------------------

//---------------------------descargaArchivo------------------------------------
 function descargaArchivo(opts, success, response) {
	
if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnTxt').setIconClass('icoTxt');			
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);//archivo;
			fp.getForm().getEl().dom.submit();
			Ext.getCmp('btnTxt').enable();//setDisabled(false);
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
			fp.el.unmask();
			Ext.getCmp('btnTxt').enable();
			Ext.getCmp('btnTxt').setIconClass('icoTxt');
		}
	}
//---------------------------fin descargaArchivo--------------------------------

//---------------------------descargaArchivoZip------------------------------------
	function procesaDescargaZIP(opts, success, response) {
	
if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

		var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
      archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);//archivo;
			fp.getForm().getEl().dom.submit();
		} else {
			NE.util.mostrarConnError(response,opts);
			fp.el.unmask();
		}	
	}
//---------------------------fin descargaArchivoZip------------------------------

//------------------------procesarConsultaDataTotal-----------------------------
var procesarConsultaDataTotal = function(store, arrRegistros, opts) 	{
	var 	fp = Ext.getCmp('formcon');
			fp.el.unmask();				
	
	var 	gridConsulta2 = Ext.getCmp('gridConsultaTotal');
	var 	gridConsulta3 = Ext.getCmp('gridConsultaParcial');

	var 	el = gridConsulta2.getGridEl();	
	if (arrRegistros != null) {
		if (!gridConsulta2.isVisible()) {
				gridConsulta2.show();
				gridConsulta3.show();
			}								
		if(store.getTotalCount() > 0) {					
				el.unmask();			
			}
	} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
	}
//----------------------Fin procesarConsultaDataTotal---------------------------

//------------------------procesarConsultaDataTotal-----------------------------
var procesarConsultaDataParcial = function(store, arrRegistros, opts) 	{
	var 	fp = Ext.getCmp('formcon');
			fp.el.unmask();				
	
	var 	gridConsulta2 = Ext.getCmp('gridConsultaParcial');

	var 	el = gridConsulta2.getGridEl();	
	if (arrRegistros != null) {
		if (!gridConsulta2.isVisible()) {
				gridConsulta2.show();			
			}								
		if(store.getTotalCount() > 0) {					
				el.unmask();		
			}
	} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
	}
//----------------------Fin procesarConsultaDataTotal---------------------------

//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
	var 	fp = Ext.getCmp('formcon');
	fp.el.unmask();				
	
	var 	gridConsulta = Ext.getCmp('gridConsulta');
	var 	el = gridConsulta.getGridEl();
  
  ocultaMuestraColumnas(true);
  
	if (arrRegistros != null) {
		if (!gridConsulta.isVisible()) {
				gridConsulta.show();	
			}								
		if(store.getTotalCount() > 0) {					
				el.unmask();

				consultaDataTotal.load({
						params: Ext.apply(fp.getForm().getValues(),{})
				})
				Ext.getCmp('btnGenerarPDF').enable();	
				Ext.getCmp('barraPaginacion').enable();
			} else {		
				Ext.getCmp('barraPaginacion').disable();	
				Ext.getCmp('btnGenerarPDF').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//--------------------------Fin procesarConsultaData----------------------------

//-----------------------------Consulta Data Total------------------------------
var consultaDataTotal = new Ext.data.JsonStore({
		root 			: 'registros',
		url 			: '15infoper2EXT.data.jsp',
		baseParams	: {
							informacion	: 'consultaGeneral',
							operacion : 'Generar',
							start :0,
							limit: 15
						},
		fields		: [	
							{	name: 'CD_NOMBRE'},	
							{	name: 'COUNT(1)'},	
							{	name: 'SUM(FG_SDOINSOLUTO)'},	
							{	name: 'SUM(FG_AMORTIZACION)'},	
							{	name: 'SUM(FG_INTERES)'},	
							{	name: 'SUM(FG_TOTDESCFOP)'},	
							{	name: 'SUM(FG_TOTDESCFINAPE)'},	
							{	name: 'SUM(FG_TOTALVENCIMIENTO)'},	
							{	name: 'SUM(FG_TOTALEXIGIBLE)'},
							{	name: 'SUM(FG_ADEUDOTOTAL)'},
							{	name: 'SUM(FG_SDOINSNVO)'}
							],		
		totalProperty 	: 'total',
		messageProperty: 'msg',
		autoLoad			: false,
		listeners		: {
			load			: procesarConsultaDataTotal,
				exception: {
						fn	: function(proxy, type, action, optionsRequest, response, args) {
								NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
								//LLama procesar consulta, para que desbloquee los componentes.
								procesarConsultaDataTotal(null, null, null);					
								}
								}
								}				
	})
//---------------------------Fin Consulta Data Total----------------------------

//-----------------------------Consulta Data Parcial----------------------------
var consultaDataParcial = new Ext.data.JsonStore({
		root 			: 'registros',
		url 			: '15infoper2EXT.data.jsp',
		baseParams	: {
							informacion	: 'consultaGeneral',
							operacion : 'Generar',
							start :0,
							limit: 15
						},
		fields		: [	
							{	name: 'CD_NOMBRE'},
							{	name: 'SUM(FG_SDOINSOLUTO)'},	
							{	name: 'SUM(FG_AMORTIZACION)'},	
							{	name: 'SUM(FG_INTERES)'},	
							{	name: 'SUM(FG_TOTDESCFOP)'},	
							{	name: 'SUM(FG_TOTDESCFINAPE)'},	
							{	name: 'SUM(FG_TOTALVENCIMIENTO)'},	
							{	name: 'SUM(FG_TOTALEXIGIBLE)'},
							{	name: 'SUM(FG_ADEUDOTOTAL)'},
							{	name: 'SUM(FG_SDOINSNVO)'}
							],		
		totalProperty 	: 'total',
		messageProperty: 'msg',
		autoLoad			: false,
		listeners		: {
			load			: procesarConsultaDataParcial,
				exception: {
						fn	: function(proxy, type, action, optionsRequest, response, args) {
								NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
								//LLama procesar consulta, para que desbloquee los componentes.
								procesarConsultaDataParcial(null, null, null);					
								}
								}
								}				
	})

//-----------------------------Consulta Data Parcial----------------------------

//-------------------------------ConsultaData-----------------------------------
var consultaData = new Ext.data.JsonStore({
		root 			: 'registros',
		url 			: '15infoper2EXT.data.jsp',
		baseParams	: {
							informacion: 'consultaGeneral',
							start       : 0,
							limit       : 15
							},
		fields		: [	
							{	name: 'IG_CLIENTE'},	
							{	name: 'CG_NOMBRECLIENTE'},	
							{	name: 'TO_CHAR(COM_FECHAPROBABLEPAGO,DD/MM/YYYY)'},	
							{	name: 'IG_PRESTAMO'},	
							{	name: 'IG_NUMELECTRONICO'},	
							{	name: 'CG_ESQUEMATASAS'},	
							{	name: 'FG_MARGEN'},	
							{	name: 'FG_SDOINSOLUTO'},	
							{	name: 'FG_AMORTIZACION'},
							{	name: 'FG_INTERES'},
							{	name: 'FG_TOTDESCFOP'},							
							{	name: 'FG_TOTDESCFINAPE'},
							{	name: 'FG_TOTALVENCIMIENTO'},
							{	name: 'FG_TOTALEXIGIBLE'},
							{	name: 'FG_ADEUDOTOTAL'},
							{	name: 'FG_SDOINSNVO'},
              {	name: 'CD_NOMBRE'}
							],		
		totalProperty 	: 'total',
		messageProperty: 'msg',
		autoLoad			: false,
		listeners		: {
			load			: procesarConsultaData,
			exception: {
				fn	: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			},
			beforeload: {
				fn: function(store, options){
					
					params		:Ext.apply(fp.getForm().getValues(),{
														informacion	:'consultaGeneral'
														})
					
					Ext.apply(options.params,fp.getForm().getValues())
				}
			}
		}					
	})
//----------------------------Fin ConsultaData----------------------------------

//-----------------------Catalogo Fecha de Corte--------------------------------
 var catalogoFC = new Ext.data.JsonStore({
		
		id					: 'catalogoFC',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '15infoper2EXT.data.jsp',
		baseParams		: {
								informacion		: 'catalogoFC'
								},
		totalProperty 	: 'total',
		autoLoad			: false,
		listeners		: {			
								beforeload: NE.util.initMensajeCargaCombo							
								}
	})
//------------------------Fin Catalogo Fecha de Corte---------------------------

//---------------------------Catalogo Moneda------------------------------------
 var catalogoMoneda = new Ext.data.JsonStore({
		id					: 'catalogoMoneda',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '15infoper2EXT.data.jsp',
		baseParams		: {
								informacion		: 'catalogomon'  
								},
		totalProperty 	: 'total',
		autoLoad			: false,
		listeners		: {			
								beforeload: NE.util.initMensajeCargaCombo,
								exception: NE.util.mostrarDataProxyError								
								}
	})
//----------------------------Fin Catalogo Moneda-------------------------------

//---------------------------Catalogo Intermediario-----------------------------
 var catalogoIntermediario = new Ext.data.JsonStore({
		id					: 'catalogoIntermediario',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '15infoper2EXT.data.jsp',
		baseParams		: {
								informacion		: 'catalogoint'  
								},
		totalProperty 	: 'total',
		autoLoad			: false,
		listeners		: {			
								beforeload: NE.util.initMensajeCargaCombo,
								exception: NE.util.mostrarDataProxyError								
								}
	})
//-----------------------Fin Catalogo Intermediario-----------------------------

//-------------------------------Elementos Forma--------------------------------
	var  elementosForma =  [
    {
      xtype: 'radiogroup',
      id:'tipoLinea',
      name: 'tipoLinea',
      fieldLabel: 'Tipo de Linea',
      items: [
         {boxLabel: 'LINEA CREDITO', name: 'tipoLinea', inputValue: 'C', value:'C' },
         {boxLabel: 'LINEA DESCUENTO', name: 'tipoLinea', inputValue: 'D', checked:true, value:'D' }
      ],
      listeners:{
        change: function(rgroup, radio){
          if(radio.value=='C'){
            Ext.getCmp('cmbti').setValue("12");
            Ext.getCmp('cmbti').hide();
            Ext.getCmp('cmbti').allowBlank = true;
			Ext.getCmp('dfcliente').allowBlank = false;
            Ext.getCmp('dfcliente').setFieldLabel('N�mero Cliente SIRAC');
			Ext.getCmp('dfcliente').setValue('');
            Ext.getCmp('cmbfc').setValue("");
            catalogoFC.removeAll();
            /*catalogoFC.load({
								params : Ext.apply({
									intermediario : "12"
								})
							});*/
          }else{
            Ext.getCmp('cmbti').setValue("");
            Ext.getCmp('cmbti').show();
            Ext.getCmp('cmbti').allowBlank = false;
			Ext.getCmp('dfcliente').allowBlank = true;
            Ext.getCmp('dfcliente').setFieldLabel('Cliente');
            Ext.getCmp('cmbfc').setValue("");
			Ext.getCmp('dfcliente').setValue('');
            //ocultaMuestraColumnas(false);
            catalogoFC.removeAll();
            
          }
        }
      }
    },
		{
		xtype				: 'combo',	
		name				: 'cmb_ti',	
		hiddenName		: '_cmb_ti',	
		id					: 'cmbti',	
		fieldLabel		: 'Intermediario', 
		allowBlank		: false,	
		mode				: 'local',
		displayField	: 'descripcion',
		valueField 		: 'clave',
		emptyText		: 'Seleccionar',
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,
		store 			: catalogoIntermediario,
		listeners		: {
			select		: {
				fn			: function(combo) {
								Ext.getCmp('gridConsulta').hide();
								Ext.getCmp('gridConsultaParcial').hide();
								Ext.getCmp('gridConsultaTotal').hide();
								Ext.getCmp('cmbmon').reset();
								Ext.getCmp('dfcliente').reset();
								Ext.getCmp('dfcprestamo').reset();
								Ext.getCmp('cmbfc').reset();
								Ext.getCmp('cmbmon').show();
								Ext.getCmp('dfcliente').show();
								Ext.getCmp('dfcprestamo').show();
								Ext.getCmp('cmbfc').show();
								catalogoFC.load({
								params 	: Ext.apply({
								intermediario		: combo.getValue()
									})
								});
								}
							}
						}
		},{
		xtype				: 'combo',	
		name				: 'cmb_mon',	
		hiddenName		: '_cmb_mon',	
		id					: 'cmbmon',	
		fieldLabel		: 'Moneda', 
		displayField	: 'descripcion',
		valueField 		: 'clave',
		emptyText		: 'Seleccionar',
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,	
		mode 				:'local',
		store 			: catalogoMoneda
		},{
		xtype			: 'numberfield',
		fieldLabel	: 'Cliente',
		name			: '_cli',
		id				: 'dfcliente',
		hiddenName	: 'df_cliente',
		allowBlank	: true,
		maxLength 	: 12,
		width			: 100,
		listeners: {
			change: function(obj, nVal, oVal){
				Ext.getCmp('cmbfc').setValue('');
				
				if(nVal!=''){
					catalogoFC.load({
						params : Ext.apply({
							intermediario : "12",
							_cli: nVal,
							tipoLinea: Ext.getCmp('tipoLinea').getValue().value
						})
					});
				}else{
					catalogoFC.removeAll();
				}
			}
		}
		},{
		xtype			: 'numberfield',
		fieldLabel	: 'Pr�stamo',
		name			: '_pre',
		id				: 'dfcprestamo',
		hiddenName	: 'df_prestamo',
		allowBlank	: true,
		maxLength 	: 12,
		width			: 100
		}, {
		xtype				: 'combo',	
		name				: 'cmb_ti',
		hiddenName		: '_cmb_fc',
		id					: 'cmbfc',
		fieldLabel		: 'Fecha de Corte',
		allowBlank		: false,
		editable			: false,	
		displayField	: 'clave',
		valueField 		: 'clave',		
		emptyText		: 'Seleccionar',
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,	
		store 			: catalogoFC,
		mode 				: 'local',
		autoload			: false
		}]
//-----------------------------Fin Elementos Forma------------------------------

//--------------------------------Grid Parcial--------------------------------
	var gridConsultaParcial = new Ext.grid.EditorGridPanel({
		store				: consultaDataTotal,
		id					:'gridConsultaParcial',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Parcial',	
		clicksToEdit	:1,
		hidden			:true,
		columns			:[{
							header		:'Moneda',
							tooltip		:'Moneda',
							dataIndex	:'CD_NOMBRE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'Saldo Insoluto',
							tooltip		:'Saldo Insoluto',
							dataIndex	:'SUM(FG_SDOINSOLUTO)',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Amortizaci�n',
							tooltip		:'Amortizaci�n',
							dataIndex	:'SUM(FG_AMORTIZACION)',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Intereses',
							tooltip		:'Intereses',
							dataIndex	:'SUM(FG_INTERES)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Total Desc. FOPYME',
							tooltip		:'Total Desc. FOPYME',
							dataIndex	:'SUM(FG_TOTDESCFOP)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Total Desc. FINAPE',
							tooltip		:'Total Desc. FINAPE',
							dataIndex	:'SUM(FG_TOTDESCFINAPE)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{     
							header		:'Total Vencimiento',
							tooltip		:'Total Vencimiento',
							dataIndex	:'SUM(FG_TOTALVENCIMIENTO)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Total Exigible',
							tooltip		:'Total Exigible',
							dataIndex	:'SUM(FG_TOTALEXIGIBLE)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		: 'Adeudo Total',
							tooltip		: 'Adeudo Total',
							dataIndex	: 'SUM(FG_ADEUDOTOTAL)',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		: 'Saldo Insoluto Nuevo',
							tooltip		: 'Saldo Insoluto Nuevo',
							dataIndex	: 'SUM(FG_SDOINSNVO)',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							}],	
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 150,
		width				: 800,
		align				: 'center',
		frame				: false
							
	})
//------------------------------Fin Grid Parcial------------------------------


//--------------------------------Grid Total--------------------------------
	var gridConsultaTotal = new Ext.grid.EditorGridPanel({
		store				: consultaDataTotal,
		id					:'gridConsultaTotal',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Total',	
		clicksToEdit	:1,
		hidden			:true,
		columns			:[{
							header		:'Moneda',
							tooltip		:'Moneda',
							dataIndex	:'CD_NOMBRE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'N�mero de Registros',
							tooltip		:'N�mero de Registros',
							dataIndex	:'COUNT(1)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'				
							},{
							header		:'Saldo Insoluto',
							tooltip		:'Saldo Insoluto',
							dataIndex	:'SUM(FG_SDOINSOLUTO)',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Amortizaci�n',
							tooltip		:'Amortizaci�n',
							dataIndex	:'SUM(FG_AMORTIZACION)',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Intereses',
							tooltip		:'Intereses',
							dataIndex	:'SUM(FG_INTERES)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Total Desc. FOPYME',
							tooltip		:'Total Desc. FOPYME',
							dataIndex	:'SUM(FG_TOTDESCFOP)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Total Desc. FINAPE',
							tooltip		:'Total Desc. FINAPE',
							dataIndex	:'SUM(FG_TOTDESCFINAPE)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{     
							header		:'Total Vencimiento',
							tooltip		:'Total Vencimiento',
							dataIndex	:'SUM(FG_TOTALVENCIMIENTO)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Total Exigible',
							tooltip		:'Total Exigible',
							dataIndex	:'SUM(FG_TOTALEXIGIBLE)',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		: 'Adeudo Total',
							tooltip		: 'Adeudo Total',
							dataIndex	: 'SUM(FG_ADEUDOTOTAL)',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		: 'Saldo Insoluto Nuevo',
							tooltip		: 'Saldo Insoluto Nuevo',
							dataIndex	: 'SUM(FG_SDOINSNVO)',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							}],	
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 150,
		width				: 800,
		align				: 'center',
		frame				: false
							
	})
//------------------------------Fin Grid Total------------------------------

//--------------------------------Grid Consulta---------------------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store				:consultaData,
		id					:'gridConsulta',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Consulta',	
		clicksToEdit	:1,
		hidden			:true,
		columns			:[{
							header		:'Cliente',
							tooltip		:'Cliente',
							dataIndex	:'IG_CLIENTE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Nombre del Cliente',
							tooltip		:'Nombre del Cliente',
							dataIndex	:'CG_NOMBRECLIENTE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'Pr�ximo Pago',
							tooltip		:'Pr�ximo Pago',
							dataIndex	:'TO_CHAR(COM_FECHAPROBABLEPAGO,DD/MM/YYYY)',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Pr�stamo',
							tooltip		:'Pr�stamo',
							dataIndex	:'IG_PRESTAMO',
							sortable		:true,
							width			:100,			
							resizable	:true,				
							align			:'center'				
							},{
							header		:'No. Electr�nico',
							tooltip		:'No. Electr�nico.',
							dataIndex	:'IG_NUMELECTRONICO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Esquema de Tasas',
							tooltip		:'Esquema de Tasas',
							dataIndex	:'CG_ESQUEMATASAS',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'				
							},{
							header		:'Moneda',
							tooltip		:'Moneda',
							dataIndex	:'CD_NOMBRE',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'				
							},{
							header		:'Margen',
							tooltip		:'Margen',
							dataIndex	:'FG_MARGEN',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center'
							},{
							header		:'Saldo Insoluto',
							tooltip		:'Saldo Insoluto',
							dataIndex	:'FG_SDOINSOLUTO',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		:'Amortizaci�n',
							tooltip		:'Amortizaci�n',
							dataIndex	:'FG_AMORTIZACION',
							sortable		:true,
							width			:'auto',			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		: 'Intereses',
							tooltip		: 'Intereses',
							dataIndex	: 'FG_INTERES',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		: 'Total Desc. FOPYME',
							tooltip		: 'Total Desc. FOPYME',
							dataIndex	: 'FG_TOTDESCFOP',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},{
							header		: 'Total Desc. FINAPE',
							tooltip		: 'Total Desc. FINAPE',
							dataIndex	: 'FG_TOTDESCFINAPE',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},	{
							header		: 'Total Vencimiento',
							tooltip		: 'Total Vencimiento',
							dataIndex	: 'FG_TOTALVENCIMIENTO',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},	{
							header		: 'Total Exigible',
							tooltip		: 'Total Exigible',
							dataIndex	: 'FG_TOTALEXIGIBLE',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},	{ 
							header		: 'Adeudo Total',
							tooltip		: 'Adeudo Total',
							dataIndex	: 'FG_ADEUDOTOTAL',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							},	{
							header		:'Saldo Insoluto Nuevo',
							tooltip		:'Saldo Insoluto Nuevo',
							dataIndex	:'FG_SDOINSNVO',
							sortable		: true,
							width			: 'auto',			
							resizable	: true,				
							align			: 'center',
							renderer:function(value){
								return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
								}
							}],	
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 400,
		width				: 800,
		align				: 'center',
		frame				: false,
		bbar				: {
							xtype			: 'paging',
							pageSize		: 15,
							buttonAlign	: 'left',
							id				: 'barraPaginacion',
							displayInfo	: true,							
							displayMsg	: '{0} - {1} de {2}',
							store: consultaData,
							emptyMsg		: "No hay registros.",
							items			: [
											'->','-',{
											xtype			: 'button',
											text			: 'Generar Archivo',					
											tooltip		: 'Generar Archivo ',
											iconCls		: 'icoTxt',
											id				: 'btnTxt',
											handler		: function(boton, evento) {
																fp.el.mask('Generando archivo...', 'x-mask-loading');
																var barraPaginacionA = Ext.getCmp("barraPaginacion");	
																Ext.getCmp('btnTxt').disable();
																Ext.Ajax.request({
																	url: '15infoper2EXT.data.jsp',
																	params: Ext.apply(fp.getForm().getValues(),{
																				informacion: 'GeneraArchivoTxt'			
																				}),
																callback: descargaArchivo
																});
															}
											},{
											xtype			: 'button',
											text			: 'Generar PDF',					
											tooltip		:	'Generar PDF ',
											iconCls		: 'icoPdf', 
											id				: 'btnGenerarPDF',
											handler		: function(boton, evento) {
																fp.el.mask('Generando archivo...', 'x-mask-loading');
																//boton.setIconClass('loading-indicator');
																var barraPaginacionA = Ext.getCmp("barraPaginacion");
																Ext.getCmp('btnGenerarPDF').disable();
																Ext.Ajax.request({
																	url: '15infoper2EXT.data.jsp',
																	params: Ext.apply(fp.getForm().getValues(),{
																				informacion: 'GeneraArchivoPDF',
																				s: barraPaginacionA.cursor,
																				l: barraPaginacionA.pageSize
																				}),		
																callback: descargaArchivoPDF
																});
															}
											}]
								}
	})
//-----------------------------Fin Grid Consulta--------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id					:'formcon',
		width				:500,
		heigth			:'auto',
		title				:'Consulta',
		frame				:true,
		collapsible		:true,
		titleCollapse	:false,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 6px',
		labelWidth		:150,
		monitorValid	:true,	
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosForma
							],
		buttons			:[{
							text				:' Bajar Archivo Zip',
							id					:'zip',
							iconCls			:'icoZip',
							formBind: true,
							handler			:function(boton, evento){
												var fechaCorte=Ext.getCmp('cmbfc').getValue();
												var ic_if=Ext.getCmp('cmbti').getValue();
												if(Ext.getCmp('cmbmon').getValue()!=''||(Ext.getCmp('dfcliente').getValue()!='' && Ext.getCmp('tipoLinea').getValue().value!='C' )||Ext.getCmp('dfcprestamo').getValue()!=''){
													if(Ext.getCmp('tipoLinea').getValue().value!='C'){
														 Ext.Msg.show({
															 title: 'Bajar Archivo Zip',
															 msg: 'Solamente son necesarios los criterios:<br> <br> Intermediario.<br> Fecha de Corte.<br><br>para esta consulta.',
															 modal: false,
															 icon: Ext.Msg.WARNING,
															 buttons: Ext.Msg.OK
															});
													}else{
														Ext.Msg.show({
															 title: 'Bajar Archivo Zip',
															 //msg: 'Solamente es necesaria <br><br>la Fecha de Corte <br><br>para esta consulta.',
															 msg: 'Solamente son necesarios los criterios:<br> <br> N�mero Cliente SIRAC.<br> Fecha de Corte.<br><br>para esta consulta.',
															 modal: false,
															 icon: Ext.Msg.WARNING,
															 buttons: Ext.Msg.OK
															});	
													}
													Ext.getCmp('cmbmon').setValue('');
													Ext.getCmp('dfcliente').setValue('');
													Ext.getCmp('dfcprestamo').setValue('');
													return;
												 } else{												 
													Ext.Ajax.request({
																url: '15infoper2EXT.data.jsp',
																params: Ext.apply(fp.getForm().getValues(),{
																	informacion: "DescargaZIP"
																	}),
																callback: procesaDescargaZIP
															})												
												}//FIN else					
											}//FIN handler
							},{
							text				:'Consultar',
							id					:'consultar',
							iconCls			:'icoBuscar',
							formBind			: true,
							handler			:function(boton, evento){
							Ext.getCmp('gridConsultaParcial').hide();
												Ext.getCmp('gridConsultaTotal').hide();
												Ext.getCmp('gridConsulta').hide();
												fp.el.mask('Enviando...', 'x-mask-loading');	
												consultaData.load({
														params		:Ext.apply(fp.getForm().getValues(),{
														informacion	:'consultaGeneral'
														})
													})	
							}
							},{
							text				:'Limpiar',
							id					:'btnLimpiar',
							iconCls			:'icoLimpiar',
							formBind			:false,
							handler			:function(boton, evento){
												Ext.getCmp('formcon').getForm().reset();
												Ext.getCmp('gridConsulta').hide();
												Ext.getCmp('gridConsultaParcial').hide();
												Ext.getCmp('gridConsultaTotal').hide();
												catalogoFC.load();
												}
							}]
	})
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		: 949,
		//height	: 600,
		style		:'margin:0 auto;',
		items		:[
					NE.util.getEspaciador(20),
					fp,
					NE.util.getEspaciador(20),
					gridConsulta,
					NE.util.getEspaciador(20),
					gridConsultaParcial,
					NE.util.getEspaciador(20),
					gridConsultaTotal,
					NE.util.getEspaciador(20)
					]
	})
//-----------------------------Fin Contenedor Principal-------------------------

catalogoIntermediario.load();
catalogoMoneda.load();
})