<!DOCTYPE html>
<% String version = (String)session.getAttribute("version"); %>

<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<html>
<head>
<title>Nafi@net - Carga Archivo Pagos Cartera</title>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /><!-- Evaluar el funcionamiento de este tag-->
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">


<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="15CargaArchivoPc01ext.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
                <div id="_menuApp"></div>
                <div id="Contcentral">
                <%if(version!=null){%>
                <%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
                <%}%>
                <div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
                </div>
                </div>
                <%if(version!=null){%>
                <%@ include file="/01principal/01nafin/pie.jspf"%>
                <%}%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>