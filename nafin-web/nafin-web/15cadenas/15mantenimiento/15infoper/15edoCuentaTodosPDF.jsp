<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="netropology.utilerias.*"%>
<%@ page import="com.netro.descuento.*"%>
<%@ page import="com.netro.pdf.*"%>
<%@ page import="netropology.utilerias.usuarios.*"%>
<%@ page import="net.sf.json.JSONArray"%>
<%@ page import="net.sf.json.JSONObject"%>
<%@ page contentType="text/html; charset=Windows-1252" %>
<%@ include file="/13descuento/13secsession_extjs.jspf"%>
<%
String sFechaCorte = (request.getParameter("sFechaCorte")==null)?"":request.getParameter("sFechaCorte");
String sClaveMoneda = (request.getParameter("sClaveMoneda")==null)?"":request.getParameter("sClaveMoneda");
String sTipoBanco = (request.getParameter("cs_tipo")==null)?"":request.getParameter("cs_tipo");
String sChkIfMoneda = (request.getParameter("sChkIfMoneda")==null)?"":request.getParameter("sChkIfMoneda");
String tipoLinea = (request.getParameter("tipoLinea")==null)?"":request.getParameter("tipoLinea");

String query = null;

String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
JSONObject jsonObj = new JSONObject();
String infoRegresar ="";

CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer("");
String nombreArchivo = archivo.nombreArchivo()+".pdf";
AccesoDB con = new AccesoDB();
boolean flagDatosPdf = false;
	
try {
	con.conexionDB();

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

 	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual = fechaActual.substring(0,2);
	String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual = fechaActual.substring(6,10);
	String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	StringBuffer strSQL = new StringBuffer();
	List varBind = new ArrayList();

	if(!"C".equals(tipoLinea)){
	strSQL.append(" SELECT /*+use_nl(c i mon) index (c in_com_estado_cuenta_01_nuk) ordered*/");
	strSQL.append(" i.ic_if,");
	strSQL.append(" i.cg_razon_social,");
	strSQL.append(" biedo.IC_USUARIO,");
	strSQL.append(" to_char(biedo.DF_FIN_MES,'dd/mm/yyyy') DF_FIN_MES,");
	strSQL.append(" to_char(hisedo.DF_FECHA_HORA,'dd/mm/yyyy') DF_FECHA_HORA,");
	strSQL.append(" to_char(biedo.DF_VISTO,'dd/mm/yyyy hh24:mi') DF_VISTO,");
	strSQL.append(" decode(biedo.CG_VISTO,'S','Enterado','No Enterado') CG_VISTO,");
    	strSQL.append(" i.ic_financiera,");
	strSQL.append(" mon.ic_moneda clave_moneda,");
	strSQL.append(" mon.cd_nombre nombre_moneda");
	strSQL.append(" FROM com_estado_cuenta c");
	strSQL.append(", comcat_if i");
	strSQL.append(", comcat_moneda mon");
	strSQL.append(", bi_notifica_edo_cta biedo");        
	strSQL.append(", comhis_proc_edo_cuenta hisedo");
        strSQL.append(" WHERE c.ic_if = i.ic_if(+)");
	strSQL.append(" AND c.ic_moneda = mon.ic_moneda");
	strSQL.append(" AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy')");
	strSQL.append(" AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1");
        strSQL.append(" AND hisedo.ic_proc_edo_cuenta = biedo.ic_proc_edo_cuenta");
        strSQL.append(" AND c.ic_if = biedo.ic_if");
        strSQL.append(" AND biedo.df_fin_mes >= TO_DATE (?, 'dd/mm/yyyy')");
        strSQL.append(" AND biedo.df_fin_mes  < TO_DATE (?, 'dd/mm/yyyy') + 1");
	strSQL.append(" AND i.cs_tipo = ?");
	
	varBind.add(sFechaCorte);
	varBind.add(sFechaCorte);
	varBind.add(sFechaCorte);
	varBind.add(sFechaCorte);
	varBind.add(sTipoBanco);
	
		
	if (!sChkIfMoneda.equals("")) {
		strSQL.append(" AND (");
		StringTokenizer stringTokenizerChkIfMon = new StringTokenizer(sChkIfMoneda, ",");
		int countIf = 0;
		while (stringTokenizerChkIfMon.hasMoreTokens()) {
			String cadenaIfMoneda = stringTokenizerChkIfMon.nextToken();
			String relIfMon[] = cadenaIfMoneda.split("|");
			String claveIf = cadenaIfMoneda.substring(0, cadenaIfMoneda.indexOf("|"));
			String claveMon = cadenaIfMoneda.substring(cadenaIfMoneda.indexOf("|") + 1);
				String numSirac = relIfMon[2];
			if(countIf > 0){strSQL.append("  OR  ");}
			strSQL.append("(i.ic_if = ? AND c.ic_moneda = ?)");
				
			varBind.add(new Integer(claveIf));
			varBind.add(new Integer(claveMon));
			countIf++;
		}
		strSQL.append(" )");
	}
	
		strSQL.append(" GROUP BY i.ic_if, i.cg_razon_social, i.ic_financiera, mon.ic_moneda, mon.cd_nombre, IC_USUARIO, DF_FIN_MES, DF_FECHA_HORA, DF_VISTO, CG_VISTO");
	}else{
		if(!"CE".equals(sTipoBanco)){
			strSQL.append(" SELECT /*+use_nl(c i mon) index (c in_com_estado_cuenta_01_nuk) ordered*/");
			strSQL.append(" i.ic_if,");
			//strSQL.append(" c.cg_nombrecliente cg_razon_social,");
			strSQL.append(" i2.cg_razon_social cg_razon_social,");
                        strSQL.append(" biedo.IC_USUARIO,");
                        strSQL.append(" to_char(biedo.DF_FIN_MES,'dd/mm/yyyy') DF_FIN_MES,");
                        strSQL.append(" to_char(hisedo.DF_FECHA_HORA,'dd/mm/yyyy') DF_FECHA_HORA,");
                        strSQL.append(" to_char(biedo.DF_VISTO,'dd/mm/yyyy hh24:mi') DF_VISTO,");
                        strSQL.append(" decode(biedo.CG_VISTO,'S','Enterado','No Enterado') CG_VISTO,");
			strSQL.append(" c.ig_cliente as ic_financiera,");
			strSQL.append(" mon.ic_moneda clave_moneda,");
			strSQL.append(" mon.cd_nombre nombre_moneda");
			strSQL.append(" FROM com_estado_cuenta c");
			strSQL.append(", comcat_if i");
			strSQL.append(", comcat_if i2");
			strSQL.append(", comcat_moneda mon");
                        strSQL.append(", bi_notifica_edo_cta biedo");        
                        strSQL.append(", comhis_proc_edo_cuenta hisedo");
			strSQL.append(" WHERE c.ic_if = i.ic_if");
			strSQL.append(" AND c.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND c.ig_cliente = i2.in_numero_sirac ");
			strSQL.append(" AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy')");
			strSQL.append(" AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1");
                        strSQL.append(" AND hisedo.ic_proc_edo_cuenta = biedo.ic_proc_edo_cuenta");
                        strSQL.append(" AND c.ic_if = biedo.ic_if");
                        strSQL.append(" AND biedo.df_fin_mes >= TO_DATE (?, 'dd/mm/yyyy')");
                        strSQL.append(" AND biedo.df_fin_mes  < TO_DATE (?, 'dd/mm/yyyy') + 1");
			strSQL.append(" AND i2.cs_tipo = ?");
			strSQL.append(" AND c.ic_if = 12 ");
			strSQL.append(" AND c.ig_cliente is not null ");
			//strSQL.append(" AND c.ig_cliente = ?");
			
			varBind.add(sFechaCorte);
			varBind.add(sFechaCorte);
			varBind.add(sFechaCorte);
			varBind.add(sFechaCorte);
			varBind.add(sTipoBanco);
			
			
			if (!sChkIfMoneda.equals("")) {
				strSQL.append(" AND (");
				StringTokenizer stringTokenizerChkIfMon = new StringTokenizer(sChkIfMoneda, ",");
				int countIf = 0;
				while (stringTokenizerChkIfMon.hasMoreTokens()) {
					String cadenaIfMoneda = stringTokenizerChkIfMon.nextToken();
					String relIfMon[] = cadenaIfMoneda.split("|");
					String numSirac = cadenaIfMoneda.substring(0, cadenaIfMoneda.indexOf("|"));
					String claveMon = cadenaIfMoneda.substring(cadenaIfMoneda.indexOf("|") + 1);

					if(countIf > 0){strSQL.append("  OR  ");}
					strSQL.append("(c.ig_cliente = ? AND c.ic_moneda = ?)");
					
					varBind.add(new Integer(numSirac));
					varBind.add(new Integer(claveMon));
					countIf++;
				}
				strSQL.append(" )");
			}
		
			strSQL.append(" GROUP BY i.ic_if, i.cg_razon_social, i.ic_financiera, mon.ic_moneda, mon.cd_nombre, IC_USUARIO, DF_FIN_MES, DF_FECHA_HORA, DF_VISTO, CG_VISTO");
		}else{
			strSQL.append(" SELECT /*+use_nl(c i mon) index (c in_com_estado_cuenta_01_nuk) ordered*/");
			strSQL.append(" cce.ic_nafin_electronico as ic_if,");
			strSQL.append(" cce.cg_razon_social,");
                        strSQL.append(" biedo.IC_USUARIO,");
                        strSQL.append(" to_char(biedo.DF_FIN_MES,'dd/mm/yyyy') DF_FIN_MES,");
                        strSQL.append(" to_char(hisedo.DF_FECHA_HORA,'dd/mm/yyyy') DF_FECHA_HORA,");
                        strSQL.append(" to_char(biedo.DF_VISTO,'dd/mm/yyyy hh24:mi') DF_VISTO,");
                        strSQL.append(" decode(biedo.CG_VISTO,'S','Enterado','No Enterado') CG_VISTO,");
			strSQL.append(" cce.in_numero_sirac ic_financiera,");
			strSQL.append(" mon.ic_moneda clave_moneda,");
			strSQL.append(" mon.cd_nombre nombre_moneda");
			strSQL.append(" FROM com_estado_cuenta c");
			//strSQL.append(", comcat_if i");
			strSQL.append(", comcat_cli_externo cce");
			strSQL.append(", comcat_moneda mon");
			strSQL.append(" WHERE c.ig_cliente = cce.in_numero_sirac ");
			strSQL.append(" AND c.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy')");
			strSQL.append(" AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1");
                        strSQL.append(" AND hisedo.ic_proc_edo_cuenta = biedo.ic_proc_edo_cuenta");
                        strSQL.append(" AND c.ic_if = biedo.ic_if");
                        strSQL.append(" AND biedo.df_fin_mes >= TO_DATE (?, 'dd/mm/yyyy')");
                        strSQL.append(" AND biedo.df_fin_mes  < TO_DATE (?, 'dd/mm/yyyy') + 1");
			strSQL.append(" AND c.ic_if = 12 ");
			strSQL.append(" AND c.ig_cliente is not null ");
			
			varBind.add(sFechaCorte);
			varBind.add(sFechaCorte);
			varBind.add(sFechaCorte);
			varBind.add(sFechaCorte);
			
			
			if (!sChkIfMoneda.equals("")) {
				strSQL.append(" AND (");
				StringTokenizer stringTokenizerChkIfMon = new StringTokenizer(sChkIfMoneda, ",");
				int countIf = 0;
				while (stringTokenizerChkIfMon.hasMoreTokens()) {
					String cadenaIfMoneda = stringTokenizerChkIfMon.nextToken();
					String relIfMon[] = cadenaIfMoneda.split("|");
					String numSirac = cadenaIfMoneda.substring(0, cadenaIfMoneda.indexOf("|"));
					String claveMon = cadenaIfMoneda.substring(cadenaIfMoneda.indexOf("|") + 1);
					if(countIf > 0){strSQL.append("  OR  ");}
					strSQL.append("(cce.in_numero_sirac = ? AND c.ic_moneda = ?)");

					
					varBind.add(new Integer(numSirac));
					varBind.add(new Integer(claveMon));
					countIf++;
				}
				strSQL.append(" )");
	}
	
			strSQL.append(" GROUP BY i.ic_if, i.cg_razon_social, i.ic_financiera, mon.ic_moneda, mon.cd_nombre, IC_USUARIO, DF_FIN_MES, DF_FECHA_HORA, DF_VISTO, CG_VISTO");
		}
	}
	System.out.println("..:: strSQL: "+strSQL.toString());
	System.out.println("..:: varBind: "+varBind);
	
	PreparedStatement ps = con.queryPrecompilado(strSQL.toString(), varBind);
	ResultSet rs = ps.executeQuery();

	int registros = 0;
	String sClaveIF = "";
	String sNombreIF = "";
	String sFechaEnvio = "";
	String sNumeroSirac = "";
	String sNombreMoneda = "";
        
        String icUsuario="";
        String dfFinmes = "";
        String dfFechaHora = "";
        String dfVisto = "";
        String cgVisto = "";
	
	ArrayList alAtributos = new ArrayList();
	ArrayList alRegistros= new ArrayList();
        UtilUsr utilUsr = new UtilUsr();


	while (rs.next()) {
		sClaveIF = "";
		sNombreIF = "";
                icUsuario="";
		dfFinmes = "";
		dfFechaHora = "";
		dfVisto = "";
		cgVisto = "";
		sNumeroSirac = "";
		sClaveMoneda = "";
		sNombreMoneda = "";
		alAtributos = new ArrayList();
		flagDatosPdf = true;

		sClaveIF = rs.getString("ic_if")==null?"":rs.getString("ic_if").trim();
		sNombreIF = rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social").trim();
                icUsuario= rs.getString("IC_USUARIO")==null?"":rs.getString("IC_USUARIO").trim();
                dfFinmes = rs.getString("DF_FIN_MES")==null?"":rs.getString("DF_FIN_MES").trim();
                dfFechaHora = rs.getString("DF_FECHA_HORA")==null?"":rs.getString("DF_FECHA_HORA").trim();
                dfVisto = rs.getString("DF_VISTO")==null?"":rs.getString("DF_VISTO").trim();
                cgVisto = rs.getString("CG_VISTO")==null?"":rs.getString("CG_VISTO").trim();
		sNumeroSirac = rs.getString("ic_financiera")==null?"":rs.getString("ic_financiera").trim();
		sClaveMoneda = rs.getString("clave_moneda")==null?"":rs.getString("clave_moneda");
		sNombreMoneda = rs.getString("nombre_moneda")==null?"":rs.getString("nombre_moneda");
		
                alAtributos.add(sClaveIF);
                alAtributos.add(sNombreIF);
                alAtributos.add(icUsuario);
                netropology.utilerias.usuarios.Usuario oUsuario;
                if(!icUsuario.equals("") ){
                    oUsuario = utilUsr.getUsuario(icUsuario);
                    alAtributos.add(oUsuario.getNombre()+" "+ oUsuario.getApellidoPaterno()+" "+ oUsuario.getApellidoMaterno() );
                }else{
                    alAtributos.add("");
                }

                alAtributos.add(dfFinmes);
                alAtributos.add(dfFechaHora);
                alAtributos.add(dfVisto);
                alAtributos.add(cgVisto);
                alAtributos.add(sNumeroSirac);
                alAtributos.add(sClaveMoneda);
                alAtributos.add(sNombreMoneda);
		alRegistros.add(alAtributos);
		//pdfDoc.newPage();
	}
	rs.close();
	ps.close();

        String encabezado = "NACIONAL FINANCIERA, S.N.C.\n"
                                        +"SUBDIRECCION DE OPERACIONES DE CREDITO\n"
                                        +"CONSULTA DE NOTIFICACIÓN DE LA CONCILIACIÓN DE SALDOS CON FECHA CORTE AL "
                                        +sFechaCorte.substring(0,2) + " DE " + meses[Integer.parseInt(sFechaCorte.substring(3,5))-1].toUpperCase() + " DE " + sFechaCorte.substring(6,10);

        float widths[] = {1, 2,1};
        pdfDoc.setTable(3, 100, widths);
        pdfDoc.setCellImage(strDirectorioPublicacion+"00archivos/15cadenas/15archcadenas/logos/nafinsa.gif", ComunesPDF.LEFT, 75);
        pdfDoc.setCell(encabezado, "formasrepB", ComunesPDF.CENTER,1,1,0);
        pdfDoc.setCell("", "formasrepB", ComunesPDF.CENTER,1,1,0);
        pdfDoc.addTable();
        
        int numCols = 11;
        float widths11[] = {.4f, 2.5f, .7f, 1, .7f, .7f, 1, .7f, .7f, .5f, 1};
        pdfDoc.setTable(numCols, 100, widths11);

        pdfDoc.setCell("Clave", "formasmenB", ComunesPDF.CENTER);
        pdfDoc.setCell("Intermediario Financiero", "formasmenB", ComunesPDF.CENTER);
        pdfDoc.setCell("Usuario", "formasmenB", ComunesPDF.CENTER);
        pdfDoc.setCell("Nombre", "formasmenB", ComunesPDF.CENTER);
        pdfDoc.setCell("Fecha Corte", "formasmenB", ComunesPDF.CENTER);
        pdfDoc.setCell("Fecha Publicación", "formasmenB", ComunesPDF.CENTER);
        pdfDoc.setCell("Fecha Aceptación", "formasmenB", ComunesPDF.CENTER);
        pdfDoc.setCell("Estatus", "formasmenB", ComunesPDF.CENTER);
        pdfDoc.setCell("Financiera", "formasmenB", ComunesPDF.CENTER);
        pdfDoc.setCell("Clave Moneda", "formasmenB", ComunesPDF.CENTER);
        pdfDoc.setCell("Moneda", "formasmenB", ComunesPDF.CENTER);
        //pdfDoc.addTable();


        Iterator iterator = alRegistros.iterator();
        while(iterator.hasNext()){
            ArrayList alTemp = new ArrayList();
            alTemp = (ArrayList)iterator.next();
            pdfDoc.setCell(alTemp.get(0).toString(), "formasmen", ComunesPDF.RIGHT);
            pdfDoc.setCell(alTemp.get(1).toString(), "formasmen", ComunesPDF.LEFT);
            pdfDoc.setCell(alTemp.get(2).toString(), "formasmen", ComunesPDF.RIGHT);
            pdfDoc.setCell(alTemp.get(3).toString(), "formasmen", ComunesPDF.LEFT);
            pdfDoc.setCell(alTemp.get(4).toString(), "formasmen", ComunesPDF.RIGHT);
            pdfDoc.setCell(alTemp.get(5).toString(), "formasmen", ComunesPDF.RIGHT);
            pdfDoc.setCell(alTemp.get(6).toString(), "formasmen", ComunesPDF.RIGHT);
            pdfDoc.setCell(alTemp.get(7).toString(), "formasmen", ComunesPDF.LEFT);
            pdfDoc.setCell(alTemp.get(8).toString(), "formasmen", ComunesPDF.RIGHT);
            pdfDoc.setCell(alTemp.get(9).toString(), "formasmen", ComunesPDF.RIGHT);
            pdfDoc.setCell(alTemp.get(10).toString(), "formasmen", ComunesPDF.LEFT);
        }
        pdfDoc.addTable();


	if (flagDatosPdf) {
		pdfDoc.endDocument();
	if(operacion.equals("") ) {
%>
<html>
	<head>
		<title>Nafinet</title>
		<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
		<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
	</head>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<table width="300" cellpadding="2" cellspacing="0" border="0" align="center">
			<tr>
				<td class="formas">&nbsp;</td>
			</tr>
			<tr>
				<td class="formas" align="center"><br>
					<table cellpadding="3" cellspacing="1">
						<tr>
							<td class="celda02" width="100" align="center" height="30">
								<a href="<%=strDirecVirtualTemp%><%=nombreArchivo%>">Bajar Archivo</a>
							</td>
							<td class="celda02" width="100" align="center">
								<a href="javascript:close();">Cerrar</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
<%
}
	} else {
if(operacion.equals("") ) { 
%>
<html>
	<head>
		<title>Nafinet</title>
		<meta http-equiv="Content-Type" content="text/html">
		<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
	</head>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<table width="300" cellpadding="2" cellspacing="0" border="0" align="center">
			<tr>
				<td class="formas">&nbsp;</td>
			</tr>
			<tr>
				<td class="celda01" align="center">NO SE ENCONTR&Oacute; NING&Uacute;N REGISTRO</td>
			</tr>
			<tr>
				<td class="formas">&nbsp;</td>
			</tr>
			<tr>
				<td>
					<table align="center" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="formas" width="120">&nbsp;</td>
							<td class="celda02" width="60" align="center"><a href="javascript:close();">Cerrar</a></td>
							<td class="formas" width="120">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
<%
}
	}
} catch(Exception e) {
	e.printStackTrace();
	if(operacion.equals("") ) { 
%>
			<tr>
				<td class="formas" align="center"><br>
					<table width="600" border="1" cellspacing="0" cellpadding="2" bordercolor="#A5B8BF">
						<tr>
							<td class="celda01" align="center" bgcolor="silver">Hubo problemas al generar el archivo.</td>
						</tr>
					</table>
				</td>
			</tr>
<%
}
} finally {
	if(con.hayConexionAbierta()) {
		con.cierraConexionDB();
	}
}
if(operacion.equals("") ) { 
%>
		</table>  
	</body>
</html>

<%
}
if(operacion.equals("VersionNueva") ) {
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();
	
	
}
%>
<%=infoRegresar%>