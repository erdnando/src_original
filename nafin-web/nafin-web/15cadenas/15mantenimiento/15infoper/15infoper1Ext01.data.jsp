<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.io.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.descuento.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		com.netro.zip.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  			
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	
	String ic_if = (request.getParameter("comboIntermediario") == null) ? "" : request.getParameter("comboIntermediario");
	String moneda = (request.getParameter("comboMoneda") == null) ? "" : request.getParameter("comboMoneda");
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String prestamo = (request.getParameter("prestamo") == null) ? "" : request.getParameter("prestamo");
	String fechaCorte = (request.getParameter("catalogoFechaCorte") == null) ? "" : request.getParameter("catalogoFechaCorte");
	
	PagosIFNB  beanPagos = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class); 
	
	//CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	
	int  start= 0, limit =0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
if(informacion.equals("catalogoIntermediario")){
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("comcat_if i");
   cat.setCampoClave("i.ic_if");
   cat.setCampoDescripcion("i.ic_financiera || ' ' || i.cg_razon_social"); 
	cat.setOrden("i.ic_financiera");
   infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("catalogoFechaCorte")) {
	//ESTADO DE CUENTA
	InformacionOperativa paginador = new InformacionOperativa();	
	String icIf= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
	paginador.setIcIf(icIf);
	paginador.setCliente(cliente);
	Registros catFechaCorte= paginador.getDatosFechaCorte();
	jsonObj.put("total",new Integer( catFechaCorte.getNumeroRegistros()) );	
	jsonObj.put("registros", catFechaCorte.getJSONData() );	
   infoRegresar = jsonObj.toString();	

}else if(informacion.equals("catalogoMoneda")){
//ESTADO DE CUENTA
		InformacionOperativa paginador = new InformacionOperativa();
	Registros catMoneda= paginador.catalogoMoneda();
	jsonObj.put("total",new Integer( catMoneda.getNumeroRegistros()) );	
	jsonObj.put("registros", catMoneda.getJSONData() );	
   infoRegresar = jsonObj.toString();	
}else if(informacion.equals("ConsultarTotales")) {
	//ESTADO DE CUENTA
		InformacionOperativa paginador = new InformacionOperativa();	
				paginador.setIcIf(ic_if);
				paginador.setMoneda(moneda);
				paginador.setCliente(cliente);
				paginador.setPrestamo(prestamo);
				paginador.setFechaCorte(fechaCorte);
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
				consulta = queryHelper.getJSONResultCount(request);
				jsonObj = JSONObject.fromObject(consulta);
				//Thread.sleep(2000);
				infoRegresar = jsonObj.toString();	
				
}else if(informacion.equals("Consultar")  || informacion.equals("ConsultarTotales") ||  
			informacion.equals("ArchivoCSV")   ||   informacion.equals("ArchivoPDF") ||   informacion.equals("ArchivoZIP")  ) {
	//ESTADO DE CUENTA
	InformacionOperativa paginador = new InformacionOperativa();	
		paginador.setIcIf(ic_if);
		paginador.setMoneda(moneda);
		paginador.setCliente(cliente);
		paginador.setPrestamo(prestamo);
		paginador.setFechaCorte(fechaCorte);
			
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
		if(informacion.equals("Consultar")  ||  informacion.equals("ArchivoPDF") )  {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
										
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			if(informacion.equals("Consultar") ){
				try {
					if (operacion.equals("Generar")) {	//Nueva consulta
						queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
					}
						consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
				} catch(Exception e) {
					throw new AppException("Error en la paginacion", e);
				}
					//Thread.sleep(5000);
					jsonObj = JSONObject.fromObject(consulta);
				
			}else if(informacion.equals("ArchivoPDF") ){
				try {
					String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					infoRegresar = jsonObj.toString();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
			}
			
		}else  if(informacion.equals("ArchivoCSV") )  {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}
	infoRegresar = jsonObj.toString();
	
}else  if(informacion.equals("DescargaZIP") )  {
		try {
			//ESTADO DE CUENTA
			InformacionOperativa paginador = new InformacionOperativa();	
			paginador.setIcIf(ic_if);
			paginador.setMoneda(moneda);
			paginador.setCliente(cliente);
			paginador.setPrestamo(prestamo);
			paginador.setFechaCorte(fechaCorte);
			
			fechaCorte = fechaCorte.replace('/','_');		
			String file = "archedocuenta_"+fechaCorte+"_"+ic_if+"_S.txt";
			File fichero = new File(strDirectorioTemp+file);
			if (fichero.exists()){
				//Borrarlo
				boolean borrado = fichero.delete();
				System.err.println("SE BORRO EL ARCHIVO "+file);
			}
			
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
				
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "ZIP");
					
					//archedocuenta_22_05_2013_427_S.txt
					GenerarArchivoZip zip = new GenerarArchivoZip();
					String nombreZip = "archedocuenta_"+fechaCorte+"_"+ic_if+"_S.zip";
      //String directory = strDirectorioPublicacion + "16archivos/descuento/procesos/edocuenta/";
      //zip.generandoZip(strDirectorioTemp,directory,nombreArchivo.replaceAll(".txt",""));
      zip.generandoZip(strDirectorioTemp,strDirectorioTemp,nombreArchivo.replaceAll(".txt",""));
      
      beanPagos.guardarArchivosZip(nombreZip, strDirectorioTemp);
      
					
					jsonObj.put("success", new Boolean(true));
      //jsonObj.put("urlArchivo","16archivos/descuento/procesos/edocuenta/"+nombreZip);
      jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreZip);
					infoRegresar = jsonObj.toString();
				
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ZIP", e);
			}
}

%>


<%=infoRegresar%>

