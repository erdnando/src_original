<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		java.math.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  			
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	String codigoCliente = (request.getParameter("codigoCliente") == null) ? "" : request.getParameter("codigoCliente");
	String codigoProyecto = (request.getParameter("codigoProyecto") == null) ? "" : request.getParameter("codigoProyecto");
	int numRegistros=0;
	String montoTotalInicial = "", montoTotalAdeudo= "";
	double  totalMonto = 0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	BigDecimal montoTotalAsignado = new BigDecimal("0.00");
	BigDecimal montoTotalComprometido = new BigDecimal("0.00");
	BigDecimal montoTotalDisponible = new BigDecimal("0.00");
	BigDecimal montoTotalUtilizado = new BigDecimal("0.00");
	
	ConsulProy1 paginado = new ConsulProy1();
	
	CQueryHelperRegExtJS queryHelper1 = new CQueryHelperRegExtJS( paginado);
	paginado.setCodigoCliente(codigoCliente);
	paginado.setCodigoProyecto(codigoProyecto);
	if(informacion.equals("Consultar")  ||  informacion.equals("ArchivoPDF") ||   informacion.equals("ConsTotales")||  informacion.equals("ArchivoPDF") )  {
		Registros reg= null;
		if(informacion.equals("Consultar") ||  informacion.equals("ConsTotales")){
			try {
						 reg	=	queryHelper1.doSearch();
					StringBuffer Totales = new StringBuffer();
					while(reg.next()){
						numRegistros++;
						String montoAsignado = reg.getString("MONTO_ASIGNADO");
						String montoComprometido = reg.getString("MONTO_COMPROMETIDO") ;
						String montoDisponible = reg.getString("MONTO_DISPONIBLE");
						String montoUtilizado = reg.getString("MONTO_UTILIZADO");
						if (montoAsignado!=null) montoTotalAsignado = montoTotalAsignado.add(new BigDecimal(montoAsignado));
						if (montoComprometido!=null) montoTotalComprometido = montoTotalComprometido.add(new BigDecimal(montoComprometido));
						if (montoDisponible!=null) montoTotalDisponible = montoTotalDisponible.add(new BigDecimal(montoDisponible));
						if (montoUtilizado!=null) montoTotalUtilizado = montoTotalUtilizado.add(new BigDecimal(montoUtilizado));

					}
				if (informacion.equals("Consultar") )  {
					consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
					jsonObj = JSONObject.fromObject(consulta);			
				}
			 if(informacion.equals("ConsTotales") ){
				datos = new HashMap();
				datos.put("montoTotal","Monto Total" );
				datos.put("monto_asignado",String.valueOf(montoTotalAsignado) );
				datos.put("monto_comprometido",String.valueOf(montoTotalComprometido) );
				datos.put("monto_disponible",String.valueOf(montoTotalDisponible) );
				datos.put("monto_utilizado",String.valueOf(montoTotalUtilizado) );
				datos.put("totalProyecto",String.valueOf(numRegistros)  );
				registros.add(datos);
				consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
				jsonObj = JSONObject.fromObject(consulta);
			}
						
			} catch(Exception e) {
					throw new AppException("Error al obtener los datos", e);
			}
	   }else if(informacion.equals("ArchivoPDF") ){
			try {
				String nombreArchivo = queryHelper1.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error son demasiados registros", e);
			}
	}
	infoRegresar = jsonObj.toString();	
}
%>
<%=infoRegresar%>

