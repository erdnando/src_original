
Ext.onReady(function() {
  Ext.override(Ext.form.Field, {
  setFieldLabel : function(text) {
    if (this.rendered) {
      this.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(text);
    }
    this.fieldLabel = text;
  }
});
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
			fp.el.unmask();
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = store.reader.jsonData;
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
				var totMontoOper='0.0';
            var totSaldoIns='0.0';
            var totCapitalVig ='0.0';
            var totCapitalVenc ='0.0';
            var totInteresVenc ='0.0';
            var totInteresMorat ='0.0';
            var totAdeudoTot ='0.0';
		
			var moneda=Ext.getCmp('cmbMoneda');
			
			// suma totales parciales en dolares o MN
			var tipoMoneda;
				if(moneda.getValue()==54){
					tipoMoneda=moneda.getRawValue();	
				}else{
					tipoMoneda='Moneda Nacional';
				}
				for(var i = 0 ; i < arrRegistros.length ; i++){
					totMontoOper =parseFloat(totMontoOper)+ parseFloat(arrRegistros[i].data.MONTO_OPERADO );
					totSaldoIns= parseFloat(totSaldoIns)+parseFloat(arrRegistros[i].data.SALDO_INSOLUTO );
					totCapitalVig=parseFloat(totCapitalVig)+ parseFloat(arrRegistros[i].data.CAPITAL_VIGENTE) ;
					totCapitalVenc=parseFloat(totCapitalVenc)+ parseFloat(arrRegistros[i].data.CAPITAL_VENCIDO) ;
					totInteresVenc=parseFloat(totInteresVenc)+ parseFloat(arrRegistros[i].data.INTERES_VENCIDO) ;
					totInteresMorat=parseFloat(totInteresMorat)+ parseFloat(arrRegistros[i].data.INTERES_MORATORIO) ;
					totAdeudoTot =parseFloat(totAdeudoTot)+parseFloat(arrRegistros[i].data.ADEUDO_TOTAL) ;
				}
				
	
				var resutlados =[
					[	tipoMoneda,
						Ext.util.Format.usMoney(Ext.util.Format.round( totMontoOper, 2 )) ,
						Ext.util.Format.usMoney(Ext.util.Format.round( totSaldoIns, 2 )),
						Ext.util.Format.usMoney(Ext.util.Format.round( totCapitalVig, 2 )),
						Ext.util.Format.usMoney(Ext.util.Format.round( totCapitalVenc, 2 )),
						Ext.util.Format.usMoney(Ext.util.Format.round( totInteresVenc, 2 )),
						Ext.util.Format.usMoney(Ext.util.Format.round( totInteresMorat, 2 )),
						Ext.util.Format.usMoney(Ext.util.Format.round( totAdeudoTot, 2 ))
					]
				];
					
	
				consultaTotalesParcialesData.loadData(resutlados);
				
			
				
			
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
				
			}	
			
				
				
			if(store.getTotalCount() > 0) {//////////Verifica que existan registros
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
				
				consultaTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarTotales'
					})
				});
				
				el.unmask();					
			} else {	
				gridTotales.hide();
				gridTotalesParciales.hide();
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
		
			if (!gridTotales.isVisible()) {
				gridTotales.show();
				
				
			}								
			if(store.getTotalCount() > 0) {	
				/*consultaTotalesParcialesData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarTotalesParciales'
					})
				});*/
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var procesarConsultaTotalesParcialesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotalesParciales');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}								
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
			
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------
	var catalogoIntermediario = new Ext.data.JsonStore({
		id: 'catalogoIntermediario',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15infoper1Ext01.data.jsp',
		baseParams: {
			informacion: 'catalogoIntermediario'  ///hacer el metodo en el data
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['CLAVE', 'DESCRIPCION','loadMsg'],
		url : '15infoper1Ext01.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'  ///hacer el metodo en el data
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoFechaCorte = new Ext.data.JsonStore({
		id: 'catalogoFechaCorte',
		root : 'registros',
		fields : ['CLAVE', 'DESCRIPCION','loadMsg'],
		url : '15infoper1Ext01.data.jsp',
		baseParams: {
			informacion: 'catalogoFechaCorte'  ///hacer el metodo en el data
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var consultaTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15infoper1Ext01.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},		
		fields: [	
			{	name: 'NUMERO_REGISTROS'},
			{	name: 'TIPO_MONEDA'},
			{	name: 'MONTO_OPERADO'},
			{	name:	'SALDO_INSOLUTO'},
			{	name:	'CAPITAL_VIGENTE'},
			{	name: 'CAPITAL_VENCIDO'},
			{  name: 'INTERES_VENCIDO'},
			{	name: 'INTERES_MORATORIO'},
			{	name: 'ADEUDO_TOTAL'},
			{	name:	'IC_MODEDA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesData(null, null, null);					
				}
			}
		}		
	});
	var rt = Ext.data.Record.create([
		{	name: 'TIPO_MONEDA'},
		{	name: 'MONTO_OPERADO'},
		{	name:	'SALDO_INSOLUTO'},
		{	name:	'CAPITAL_VIGENTE'},
		{	name: 'CAPITAL_VENCIDO'},
		{  name: 'INTERES_VENCIDO'},
		{	name: 'INTERES_MORATORIO'},
		{	name: 'ADEUDO_TOTAL'}//,
		//{	name:	'IC_MODEDA'}
	]);
	var consultaTotalesParcialesData=  new Ext.data.Store({
		 reader: new Ext.data.ArrayReader(
			  {
					idIndex: 1  // id for each record will be the first element
			  },
			  rt // recordType
		 ),
		 listeners: {
				load: procesarConsultaTotalesParcialesData,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaTotalesData(null, null, null);					
					}
				}
			}		
	});
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15infoper1Ext01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'CLIENTE'},
			{	name: 'NOMBRE_CLIENTE'},	
			{	name: 'FECHA_OPERACION'},
			{	name: 'PRESTAMO'},	
			{	name: 'TASA_REFERENCIAL'},
			{	name: 'SPREAD'},
			{	name: 'MARGEN'},	
			{	name: 'MONTO_OPERADO'},
			{	name:	'SALDO_INSOLUTO'},
			{	name:	'CAPITAL_VIGENTE'},
			{	name: 'CAPITAL_VENCIDO'},
			{  name: 'INTERES_VENCIDO'},
			{	name: 'INTERES_MORATORIO'},
			{	name: 'ADEUDO_TOTAL'},
			{	name:	'IC_MODEDA'},
			{	name: 'CUENTA'},
			{	name:	'CD_NOMBRE'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	

//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
var elementosForma=[
		{
      xtype: 'radiogroup',
      id:'tipoLinea',
      name: 'tipoLinea',
      fieldLabel: 'Tipo de Linea',
      items: [
         {boxLabel: 'LINEA CREDITO', name: 'tipoLinea', inputValue: 'C', value:'C' },
         {boxLabel: 'LINEA DESCUENTO', name: 'tipoLinea', inputValue: 'D', checked:true, value:'D' }
      ],
      listeners:{
        change: function(rgroup, radio){
          if(radio.value=='C'){
            Ext.getCmp('cmbIntermediario').setValue("12");
            Ext.getCmp('cmbIntermediario').hide();
            Ext.getCmp('cmbIntermediario').allowBlank = true;
			Ext.getCmp('txtCliente').allowBlank = false;
            Ext.getCmp('txtCliente').setFieldLabel('N�mero Cliente SIRAC');
			Ext.getCmp('txtCliente').setValue('');
            Ext.getCmp('cmbFechaCorte').setValue("");
            catalogoFechaCorte.removeAll();
			/*catalogoFechaCorte.load({
								params : Ext.apply({
									ic_if : "12"
								})
							});*/
          }else{
            Ext.getCmp('cmbIntermediario').setValue("");
            Ext.getCmp('cmbIntermediario').show();
            Ext.getCmp('cmbIntermediario').allowBlank = false;
			Ext.getCmp('txtCliente').allowBlank = true;
            Ext.getCmp('txtCliente').setFieldLabel('Cliente');
			Ext.getCmp('txtCliente').setValue('');
            Ext.getCmp('cmbFechaCorte').setValue("");
            catalogoFechaCorte.removeAll();
            
          }
        }
      }
    },
    {
			xtype				: 'combo',
			id					: 'cmbIntermediario',
			name				: 'comboIntermediario',
			hiddenName 		:'comboIntermediario', 
			fieldLabel		: 'Intermediario',
			width				: 250,
			forceSelection	: true,
			allowBlank		: true,
			emptyText		:'Seleccionar...',
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'clave',
			displayField	:'descripcion',
			store				: catalogoIntermediario,
			tabIndex			: 1,
			listeners: {
				select: {
					fn: function(combo) {
              Ext.getCmp('cmbFechaCorte').setValue('');
							catalogoFechaCorte.load({
								params : Ext.apply({
									ic_if : combo.getValue()
								})
							});
					}
				}
			}
		},
		{
			xtype				: 'combo',
			id					: 'cmbMoneda',
			name				: 'comboMoneda',
			hiddenName 		:'comboMoneda', 
			fieldLabel		: 'Moneda',
			width				: 250,
			forceSelection	: true,
			emptyText		:'Seleccionar...',
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			tabIndex			: 2,
			store				: catalogoMoneda
			
		},
		{
			xtype			:	'numberfield',
			fieldLabel 	: 'Cliente',
			name 			: 'cliente',
			id 			: 'txtCliente',
			allowBlank	: true,
			maxLength 	: 12,
			width 		: 100,
			msgTarget 	: 'side',
			tabIndex		: 3,
			margins		: '0 20 0 0',
			listeners: {
			change: function(obj, nVal, oVal){
				if(Ext.getCmp('tipoLinea').getValue().value=='C'){
					Ext.getCmp('cmbFechaCorte').setValue('');
					if(nVal!=''){
						catalogoFechaCorte.load({
							params : Ext.apply({
								ic_if : "12",
								cliente: nVal,
								tipoLinea: Ext.getCmp('tipoLinea').getValue().value
							})
						});
					}else{
						catalogoFechaCorte.removeAll();
					}
				}
			}
		}
		},
		{
			xtype			:	'numberfield',
			fieldLabel 	: 'Pr�stamo',
			name 			: 'prestamo',
			id 			: 'txtPrestamo',
			allowBlank	: true,
			maxLength 	: 12,
			width 		: 100,
			msgTarget 	: 'side',
			tabIndex		: 4,
			margins		: '0 20 0 0'
		},{
			xtype				: 'combo',
			fieldLabel		: 'Fecha de corte',
			name				: 'catalogoFechaCorte',
			id					: 'cmbFechaCorte',
			mode				: 'local',
			autoLoad			: false,
			allowBlank		: false,
			displayField	: 'DESCRIPCION',
			emptyText		: 'Seleccionar ...',			
			valueField		: 'CLAVE',
			hiddenName 		: 'catalogoFechaCorte',					
			forceSelection : true,
			triggerAction 	: 'all',
			typeAhead		: true,
			minChars 		: 1,
			tabIndex			: 5,
			forceSelection : true,
			store				: catalogoFechaCorte
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Consulta -Estado de Cuenta',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Bajar Archivo Zip',
				id:	'btnArchivoZIp',
				iconCls: 'icoZip',
				formBind: true,	
				tabIndex: 6,
				handler:function(boton){
				var fechaCorte=Ext.getCmp('cmbFechaCorte').getValue();
				var ic_if=Ext.getCmp('cmbIntermediario').getValue();
				if(Ext.getCmp('cmbMoneda').getValue()!=''|| (Ext.getCmp('txtCliente').getValue()!='' && Ext.getCmp('tipoLinea').getValue().value!='C') ||Ext.getCmp('txtPrestamo').getValue()!=''){
						if(Ext.getCmp('tipoLinea').getValue().value!='C'){
							Ext.Msg.alert('','Solamente es necesario los criterios de Intermediario y Fecha de Corte para esta consulta');
						}else{
							Ext.Msg.alert('','Solo es necesario seleccionar la Fecha de Corte para esta consulta');
						}
						Ext.getCmp('cmbMoneda').setValue('');
						Ext.getCmp('txtCliente').setValue('');
						Ext.getCmp('txtPrestamo').setValue('');
						return;
					}else{
					fp.el.mask('Procesando...', 'x-mask-loading');
						var fechaCorteNew=fechaCorte.replace('/','_');
						fechaCorteNew=fechaCorteNew.replace('/','_');
						Ext.Ajax.request({
							url: '15infoper1Ext01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: "DescargaZIP"
								}),
							callback: procesarDescargaArchivos
						})		
						
					}
					
				}
			},
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,	
				tabIndex			: 7,
				handler: function(boton, evento) {					
					fp.el.mask('Procesando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion : 'Generar',
							informacion : 'Consultar',
							start:0,
							limit:15					
						})
						
					}); 
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				tabIndex			: 8,
				handler: function() {
				gridConsulta.hide();gridTotales.hide();
				gridTotalesParciales.hide();
				Ext.getCmp('cmbFechaCorte').getStore().removeAll(true);//
					fp.getForm().reset();
					
					//window.location = '15infoper1Ext01.jsp';					
				}
			}
		]
	});
	//grid de resultados de la consulta
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Cliente',
				tooltip: 'Cliente',
				dataIndex: 'CLIENTE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Nombre del Cliente',
				tooltip: 'Nombre del Cliente',
				dataIndex: 'NOMBRE_CLIENTE',
				sortable: true,
				width: 300,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='left'>"+value+"</div>";
				}
			},
			{
				header: 'Fecha<br>Operaci�n',
				tooltip: 'Fecha Operaci�n',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				width: 80,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Pr�stamo',
				tooltip: 'Pr�stamo',
				dataIndex: 'PRESTAMO',
				sortable: true,
				width: 80,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Tasa ref.',
				tooltip: 'Tasa ref.',
				dataIndex: 'TASA_REFERENCIAL',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'CD_NOMBRE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Spread',
				tooltip: 'Spread',
				dataIndex: 'SPREAD',
				sortable: true,
				width: 60,			
				resizable: true,				
				align: 'center',
				renderer: function(v){
					return Ext.util.Format.number(v, '0.0000');
				}
			},
			{
				header: 'Margen',
				tooltip: 'Margen',
				dataIndex: 'MARGEN',
				sortable: true,
				width: 50,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
				return Ext.util.Format.number(v, '0.00');
				}
			},
			{
				header: 'Monto Operado',
				tooltip: 'Monto Operado',
				dataIndex: 'MONTO_OPERADO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}/*,
				renderer: Ext.util.Format.usMoney*/
			},{
				header: 'Saldo Insoluto',
				tooltip: 'Saldo Insoluto',
				dataIndex: 'SALDO_INSOLUTO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}/*,
				renderer: Ext.util.Format.usMoney*/
			},
			{
				header: 'Capital vigente',
				tooltip: 'Capital vigente',
				dataIndex: 'CAPITAL_VIGENTE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}/*,
				renderer: Ext.util.Format.usMoney*/
			},
			{
				header: 'Capital vencido',
				tooltip: 'Capital vencido',
				dataIndex: 'CAPITAL_VENCIDO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}/*,
				renderer: Ext.util.Format.usMoney*/
			},
			{
				header: 'Inter�s<br>vencido',
				tooltip: 'Inter�s vencido',
				dataIndex: 'INTERES_VENCIDO',
				sortable: true,
				width: 60,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}/*,
				renderer: Ext.util.Format.usMoney*/
			},
			{
				header: 'Inter�s<br>moratorio',
				tooltip: 'Inter�s moratorio',
				dataIndex: 'INTERES_MORATORIO',
				sortable: true,
				width: 60,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}/*,
				renderer: Ext.util.Format.usMoney*/
			},
			{
				header: 'Adeudo Total',
				tooltip: 'Adeudo Total',
				dataIndex: 'ADEUDO_TOTAL',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}/*,
				renderer: Ext.util.Format.usMoney*/
				
			}												
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 430,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
					{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',
					iconCls: 'icoTxt',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {
					boton.setIconClass('loading-indicator');
						var barraPaginacionA = Ext.getCmp("barraPaginacion");						
							Ext.Ajax.request({
							url: '15infoper1Ext01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoCSV'
							}),
							success : function(response) {
								boton.setIconClass('icoTxt');     
								boton.setDisabled(false);

							},	
							callback: procesarDescargaArchivos
						});						
					}
				},
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						var barraPaginacionA = Ext.getCmp("barraPaginacion");						
							Ext.Ajax.request({
							url: '15infoper1Ext01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoPDF',
								start: barraPaginacionA.cursor,
								limit: barraPaginacionA.pageSize
							}),
							callback: procesarDescargaArchivos
						});						
					}
				}
			]
		}
	});
	//grid de Totales
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Totales',	
		align: 'center',
		hidden: true,//true,
		columns: [	
			{
				header: 'Moneda',
				tooltip: 'Tipo de Moneda ',
				dataIndex: 'TIPO_MONEDA',
				sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
							return '<div align="left">'+ value+'</div>';
				 }
			},
			{	header:'N�mero de registros',
				dataIndex:'NUMERO_REGISTROS',sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Monto Operado',
				tooltip: 'Monto Operado',
				dataIndex: 'MONTO_OPERADO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}
			},{
				header: 'Saldo Insoluto',
				tooltip: 'Saldo Insoluto',
				dataIndex: 'SALDO_INSOLUTO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}
			},
			{
				header: 'Capital vigente',
				tooltip: 'Capital vigente',
				dataIndex: 'CAPITAL_VIGENTE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}
			},
			{
				header: 'Capital vencido',
				tooltip: 'Capital vencido',
				dataIndex: 'CAPITAL_VENCIDO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}
			},
			{
				header: 'Inter�s<br>vencido',
				tooltip: 'Inter�s vencido',
				dataIndex: 'INTERES_VENCIDO',
				sortable: true,
				width: 60,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}
			},
			{
				header: 'Inter�s<br>moratorio',
				tooltip: 'Inter�s moratorio',
				dataIndex: 'INTERES_MORATORIO',
				sortable: true,
				width: 60,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}
			},
			{
				header: 'Adeudo Total',
				tooltip: 'Adeudo Total',
				dataIndex: 'ADEUDO_TOTAL',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				}
				
			}				
		],	
		stripeRows: true,
		loadMask: true,
		height: 150,
		width: 900,		
		frame: true	
	});
	//GRID TOTALES PARCIALES

	var gridTotalesParciales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesParcialesData,
		id: 'gridTotalesParciales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Totales Parciales',	
		align: 'center',
		hidden: true,//true,
		columns: [	
			{
				header: 'Moneda',
				tooltip: 'Tipo de Moneda ',
				dataIndex: 'TIPO_MONEDA',
				sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
							
							return '<div align="left">'+ value+'</div>';
				 }
			},
			{
				header: 'Monto Operado',
				tooltip: 'Monto Operado',
				dataIndex: 'MONTO_OPERADO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+(value)+"</div>";
				}
			},{
				header: 'Saldo Insoluto',
				tooltip: 'Saldo Insoluto',
				dataIndex: 'SALDO_INSOLUTO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+(value)+"</div>";
				}
			},
			{
				header: 'Capital vigente',
				tooltip: 'Capital vigente',
				dataIndex: 'CAPITAL_VIGENTE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+(value)+"</div>";
				}
			},
			{
				header: 'Capital vencido',
				tooltip: 'Capital vencido',
				dataIndex: 'CAPITAL_VENCIDO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+(value)+"</div>";
				}
			},
			{
				header: 'Inter�s<br>vencido',
				tooltip: 'Inter�s vencido',
				dataIndex: 'INTERES_VENCIDO',
				sortable: true,
				width: 60,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+(value)+"</div>";
				}
			},
			{
				header: 'Inter�s<br>moratorio',
				tooltip: 'Inter�s moratorio',
				dataIndex: 'INTERES_MORATORIO',
				sortable: true,
				width: 60,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+(value)+"</div>";
				}
			},
			{
				header: 'Adeudo Total',
				tooltip: 'Adeudo Total',
				dataIndex: 'ADEUDO_TOTAL',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return "<div align='right'>"+(value)+"</div>";
				}
			}				
		],	
		stripeRows: true,
		loadMask: true,
		height: 120,
		width: 900,		
		frame: true	
	});
	
	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 900,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),			
			gridTotalesParciales,
			gridTotales,
			NE.util.getEspaciador(20)			
		]
	});

	///Cargar el catalogo intemediario
	catalogoIntermediario.load();
	catalogoMoneda.load();
});	
