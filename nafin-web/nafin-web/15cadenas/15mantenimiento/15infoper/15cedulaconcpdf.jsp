<%@ page import="java.sql.*,
java.text.*, 
java.util.*, 
java.math.*,	
netropology.utilerias.*,
com.netro.descuento.*, 
net.sf.json.JSONArray,  
net.sf.json.JSONObject, 
com.netro.pdf.*"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String sNombreMoneda = (request.getParameter("sNombreMoneda")==null)?"":request.getParameter("sNombreMoneda").replace('_',' '),
		sObservaciones = (request.getParameter("sObservaciones")==null)?"":request.getParameter("sObservaciones").replace('_',' '),
		sFirmaIF = (request.getParameter("sFirmaIF")==null)?"":request.getParameter("sFirmaIF").replace('_',' '),
		sFirmaNafin = (request.getParameter("sFirmaNafin")==null)?"":request.getParameter("sFirmaNafin").replace('_',' '),
		sPuestoIF = (request.getParameter("sPuestoIF")==null)?"":request.getParameter("sPuestoIF").replace('_',' '),
		sPuestoNafin = (request.getParameter("sPuestoNafin")==null)?"":request.getParameter("sPuestoNafin").replace('_',' '),
		sFechaEnvio = (request.getParameter("sFechaEnvio")==null)?"":request.getParameter("sFechaEnvio").replace('_',' '),
		sFechaCorte = (request.getParameter("sFechaCorte")==null)?"":request.getParameter("sFechaCorte").replace('_',' '),
		sClaveIF = (request.getParameter("sClaveIF")==null)?"":request.getParameter("sClaveIF").replace('_',' '),
		sClaveMoneda = (request.getParameter("sClaveMoneda")==null)?"":request.getParameter("sClaveMoneda").replace('_',' '),
		sNombreIF = (request.getParameter("sNombreIF")==null)?"":request.getParameter("sNombreIF"),
		sNumeroSirac = (request.getParameter("sNumeroSirac")==null)?"":request.getParameter("sNumeroSirac"),
		tipoLinea = (request.getParameter("tipoLinea")==null)?"":request.getParameter("tipoLinea"),
		cs_tipo_B = (request.getParameter("cs_tipo_B")==null)?"":request.getParameter("cs_tipo_B");
		
String fcarga = (request.getParameter("fcarga")==null)?"":request.getParameter("fcarga").replace('_',' ');
String icusuario = (request.getParameter("icusuario")==null)?"":request.getParameter("icusuario").replace('_',' ');
String nusuario = (request.getParameter("nusuario")==null)?"":request.getParameter("nusuario").replace('_',' ');
String cvecertificado = (request.getParameter("sNumeroSirac")==null)?"":request.getParameter("cvecertificado").replace('_',' ');

String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
JSONObject jsonObj = new JSONObject();
String infoRegresar ="";
	
CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer("");
String nombreArchivo = archivo.nombreArchivo()+".pdf";
int registros=0;
AccesoDB con = new AccesoDB();
try {
	con.conexionDB();
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	sNombreIF = sNombreIF.replace('_',' ');
	
	

if(operacion.equals("") ) { 
%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=strDirecVirtualCSS%>/css/<%=strClase%>">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="300" cellpadding="2" cellspacing="0" border="0" align="center">
<tr>
  <td class="formas">&nbsp;</td>
</tr>
<%
}
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	String ig_subaplicacion, fg_saldo_insoluto, cs_tipo= "";//FODA029 16/05/2005 SMJ
	float widths[] = {1, 2,1};
	pdfDoc.setTable(3, 100, widths);
	pdfDoc.setCellImage(strDirectorioPublicacion+"00archivos/15cadenas/15archcadenas/logos/nafinsa.gif",ComunesPDF.LEFT, 75);
	String encabezado = "NACIONAL FINANCIERA, S.N.C.\n" 
					+"SUBDIRECCI�N DE OPERACIONES DE CR�DITO\n"
					+"RESUMEN DE LA CONCILIACI�N DE SALDOS AL "
					+ sFechaCorte.substring(0,2) + " DE " + meses[Integer.parseInt(sFechaCorte.substring(3,5))-1].toUpperCase() + " DE " + sFechaCorte.substring(6,10)+"\n\n" + 
					"(" + sNumeroSirac + ") " +sNombreIF +" - " + sNombreMoneda;
	pdfDoc.setCell(encabezado, "formasrepB", ComunesPDF.CENTER,1,1,0);
	pdfDoc.setCell("", "formasrepB", ComunesPDF.CENTER,1,1,0);
	pdfDoc.addTable();
	
	String query = " select /*+ USE_NL(C) INDEX (C CP_COM_CEDULA_CONCILIA_PK) */ "   +
				   " C.CG_TIPO_CREDITO"   +
				   " ,C.FG_MONTO_OPERADO"   +
				   " ,C.FG_CAPITAL_VIGENTE"   +
				   " ,C.FG_CAPITAL_VENCIDO"   +
				   " ,C.FG_INTERES_VIGENTE"   +
				   " ,C.FG_INTERES_VENCIDO"   +
				   " ,C.FG_INTERES_MORA"   +
				   " ,C.FG_TOTAL_ADEUDO"   +
				   " ,C.IG_DESCUENTOS"   +
				   " ,C.CG_OBSERVACIONES"   +
				   " ,C.IC_FIRMA_CEDULA_NAF"   +
				   " ,C.IC_FIRMA_CEDULA"   +
				   " ,C.ig_subaplicacion"   +//FODA029 16/05/2005 SMJ
				   " ,C.fg_saldo_insoluto"   +
				   ((!"CE".equals(cs_tipo_B))?" ,IF.cs_tipo":" ,'B' as cs_tipo"); //FODA029 16/05/2005 SMJ
		if(!"CE".equals(cs_tipo_B)){
			query += " from com_cedula_concilia C, comcat_if IF"+
					" where C.IC_IF=IF.IC_IF " +
					" AND C.IC_IF=?" ;
		}else{
			query += " from com_cedula_concilia C, comcat_cli_externo IF" +
					" where C.IC_CLIENTE_EXTERNO=IF.IC_NAFIN_ELECTRONICO "+
					" AND C.IC_CLIENTE_EXTERNO= ? " ;
		}
		query +=   " AND C.IC_MONEDA=?"   +
				   " AND TRUNC(C.DF_FECHA_CORTE) = TO_DATE(?,'DD/MM/YYYY')	  "   +
				   " AND TRUNC(C.DF_CARGA) = TO_DATE(?,'DD/MM/YYYY')	" +
				   " AND CG_TIPO_LINEA = ? ";
	//System.out.println("query:"+query+"<br>");
	PreparedStatement ps = con.queryPrecompilado(query);
	ps.setString(1, sClaveIF);
	ps.setString(2, sClaveMoneda);
	ps.setString(3, sFechaCorte);
	ps.setString(4, sFechaEnvio);
	ps.setString(5, tipoLinea);
	ResultSet rs = ps.executeQuery();

    String sTipoCredito_C="", sMontoOperado_C="", sCapitalVigente_C="", sCapitalVencido_C="", sInteresVigente_C="", sInteresVencido_C="", sMoras_C="", sTotalAdeudo_C="", sDescuentos_C="", sObservaciones_C="", sFirmaNafin_C="", sFirmaIF_C="";

	BigDecimal dTotal_MontoOperado = new BigDecimal(0), dTotal_CapitalVigente = new BigDecimal(0), dTotal_CapitalVencido = new BigDecimal(0);
	BigDecimal dTotal_InteresVigente = new BigDecimal(0), dTotal_InteresVencido = new BigDecimal(0), 
				dTotal_Moras = new BigDecimal(0), dTotal_TotalAdeudo = new BigDecimal(0), dTotal_Descuentos = new BigDecimal(0);
	BigDecimal dTotal_saldoInsolto = new BigDecimal(0);			
	while (rs.next()) {
		
	
        sTipoCredito_C     = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
        sMontoOperado_C    = (rs.getString(2) == null) ? "" : rs.getString(2).trim();
        sCapitalVigente_C  = (rs.getString(3) == null) ? "" : rs.getString(3).trim();
        sCapitalVencido_C  = (rs.getString(4) == null) ? "" : rs.getString(4).trim();
        sInteresVigente_C  = (rs.getString(5) == null) ? "" : rs.getString(5).trim();
        sInteresVencido_C  = (rs.getString(6) == null) ? "" : rs.getString(6).trim();
        sMoras_C           = (rs.getString(7) == null) ? "" : rs.getString(7).trim();
        sTotalAdeudo_C     = (rs.getString(8) == null) ? "" : rs.getString(8).trim();
        sDescuentos_C      = (rs.getString(9) == null) ? "" : rs.getString(9).trim();
        sObservaciones_C   = (rs.getString(10) == null) ? "" : rs.getString(10).trim();
        sFirmaNafin_C      = (rs.getString(11) == null) ? "" : rs.getString(11).trim();
        sFirmaIF_C         = (rs.getString(12) == null) ? "" : rs.getString(12).trim();
		ig_subaplicacion   = (rs.getString(13) == null) ? "" : rs.getString(13).trim();
		fg_saldo_insoluto  = (rs.getString(14) == null) ? "" : rs.getString(14).trim();
		cs_tipo			   = (rs.getString(15) == null) ? "" : rs.getString(15).trim();
		
		//Encabezados
		
		if(registros==0){
			int numCols = 9;
			float widths2[] = {.8f, 2, 1, 1, 1, 1, 1, 1, 1, .7f};
			float widths3[] = {2, 1, 1, 1, 1, 1, 1, 1, .7f};
		
			if("B".equals(cs_tipo)){
				numCols++;
				pdfDoc.setTable(numCols, 100, widths2);
			} else {
				pdfDoc.setTable(numCols, 100, widths3);
			}

			if(cs_tipo.equals("B")){
				pdfDoc.setCell("Sub aplicaci�n", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Concepto Sub aplicaci�n", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Saldo insoluto Nafin", "formasmenB", ComunesPDF.CENTER);
			}else{
				pdfDoc.setCell("Tipo de Cr�dito", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Operado", "formasmenB", ComunesPDF.CENTER);
			}
			pdfDoc.setCell("Capital Vigente", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Capital Vencido", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Inter�s Vigente", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Inter�s Vencido", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Moras", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Total Adeudo", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Descuentos", "formasmenB", ComunesPDF.CENTER);
		}
		
		
		//Desplegados	
		//pdfDoc.setCell(sTipoCredito_C, "formasmen", ComunesPDF.CENTER);
		if(cs_tipo.equals("B")){
			pdfDoc.setCell(ig_subaplicacion, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(sTipoCredito_C, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMoneda2(fg_saldo_insoluto, true), "formasmen", ComunesPDF.RIGHT);
		}else{
			pdfDoc.setCell(sTipoCredito_C, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMoneda2(sMontoOperado_C, true), "formasmen", ComunesPDF.RIGHT);
		}
		pdfDoc.setCell(Comunes.formatoMoneda2(sCapitalVigente_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(sCapitalVencido_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(sInteresVigente_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(sInteresVencido_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(sMoras_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(sTotalAdeudo_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoDecimal(sDescuentos_C, 0), "formasmen", ComunesPDF.RIGHT);
		
		dTotal_MontoOperado   = dTotal_MontoOperado.add(new BigDecimal(sMontoOperado_C));
		dTotal_CapitalVigente = dTotal_CapitalVigente.add(new BigDecimal(sCapitalVigente_C));
		dTotal_CapitalVencido = dTotal_CapitalVencido.add(new BigDecimal(sCapitalVencido_C));
		dTotal_InteresVigente = dTotal_InteresVigente.add(new BigDecimal(sInteresVigente_C));
		dTotal_InteresVencido = dTotal_InteresVencido.add(new BigDecimal(sInteresVencido_C));
		dTotal_Moras          = dTotal_Moras.add(new BigDecimal(sMoras_C));
		dTotal_TotalAdeudo    = dTotal_TotalAdeudo.add(new BigDecimal(sTotalAdeudo_C));
		dTotal_Descuentos     = dTotal_Descuentos.add(new BigDecimal(sDescuentos_C));
		dTotal_saldoInsolto   = dTotal_saldoInsolto.add(new BigDecimal(fg_saldo_insoluto));
		registros++;
	}
	ps.close();

	ArrayList alTotales = (ArrayList)session.getAttribute("Totales");
	pdfDoc.setCell("TOTAL", "formasmenB", ComunesPDF.CENTER);
	if(cs_tipo.equals("B")){
		pdfDoc.setCell("", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_saldoInsolto.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	}else{
		pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_MontoOperado.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	}
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_CapitalVigente.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_CapitalVencido.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_InteresVigente.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_InteresVencido.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_Moras.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_TotalAdeudo.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoDecimal(dTotal_Descuentos.toString(), 0), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.addTable();
	
	
	String sNota = new String("");
	query = "select cg_observaciones from com_observaciones_cedula "+
				"where trunc(df_fecha_corte) = to_date( ?, 'dd/mm/yyyy') "+
				"and ic_if = ? "+
				"and ic_moneda = ?";
	ps = con.queryPrecompilado(query);
	ps.setString(1,sFechaCorte);
	ps.setString(2,sClaveIF);
	ps.setString(3,sClaveMoneda);
	rs = ps.executeQuery();
	if(rs.next())
		sNota = rs.getString("cg_observaciones");
	rs.close();
	if(ps!=null) ps.close();
	
	pdfDoc.addText("\nNOTA: "+ sNota, "formasB", ComunesPDF.LEFT);	
	
	pdfDoc.addText("Observaciones:  " + sObservaciones, "formasB", ComunesPDF.LEFT);
	
	pdfDoc.addText("\nConfirmo que los saldos operativos de Nacional Financiera, S.N.C. han sido conciliados con los registros contables de", "formas", ComunesPDF.CENTER);
	pdfDoc.addText(sNombreIF, "formasB", ComunesPDF.CENTER);
	
	pdfDoc.setTable(3, 85);
	
	pdfDoc.setCell("\nUsuario: "+icusuario+"\nNombre: "+nusuario+"\nNo. serie certificado: "+cvecertificado+"\nFecha/Hora: "+fcarga, "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCellImage(strDirectorioPublicacion + "00utils/gif/firma_pagos_cartera.gif", ComunesPDF.CENTER,100);

	pdfDoc.setCell("_____________________________\n"+sFirmaIF, "formasB", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("_____________________________\n"+sFirmaNafin, "formasB", ComunesPDF.CENTER, 1, 1, 0);
	
	pdfDoc.setCell(sPuestoIF, "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell(sPuestoNafin, "formas", ComunesPDF.CENTER, 1, 1, 0);
	
	pdfDoc.setCell(sNombreIF, "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("NACIONAL FINANCIERA S.N.C.", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.addTable();

	float tWid[] = {1.5f, 1};
	
	pdfDoc.setTable(2, 100, tWid);

	pdfDoc.setCell("Nota: Si en un plazo de 10 d�as h�biles contados a partir de la recepci�n del presente Estado de Cuenta no se reciben las observaciones correspondientes se dar�n por aceptadas las cifras.", "formas", ComunesPDF.LEFT, 1, 1, 0);
	pdfDoc.setCell("Fecha de Env�o:  "+sFechaEnvio, "formas", ComunesPDF.RIGHT, 1, 1, 0);
	pdfDoc.addTable();
	
	pdfDoc.endDocument();
	
if(operacion.equals("") ) { 
%>
<tr>
	<td class="formas" align="center"><br>
		<table cellpadding="3" cellspacing="1">
			<tr>
				<td class="celda02" width="100" align="center" height="30">
					<a href="<%=strDirecVirtualTemp%><%=nombreArchivo%>">Bajar Archivo</a>
				</td>
				<td class="celda02" width="100" align="center">
					<a href="javascript:close();">Cerrar</a>
				</td>
			</tr>
		</table>
	</td>
</tr>
<%
}

} catch(Exception e) {
	e.printStackTrace();	
	if(operacion.equals("") ) { 
	%>
	<tr>
		<td class="formas" align="center"><br>
			<table width="600" border="1" cellspacing="0" cellpadding="2" bordercolor="#A5B8BF">
				<tr>
					<td class="celda01" align="center" bgcolor="silver">Hubo problemas al generar el archivo.</td>
				</tr>
			</table>
		</td>
	</tr>
	
	
	</body>
</html>

<%
	}
} finally { 
	if(con.hayConexionAbierta()) con.cierraConexionDB();
}
%>


<%

if(operacion.equals("VersionNueva") ) {
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();
	
	
}
%>
<%=infoRegresar%>