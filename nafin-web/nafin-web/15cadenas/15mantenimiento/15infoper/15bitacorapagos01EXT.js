Ext.onReady(function(){

Ext.override(Ext.form.Field, {
  setFieldLabel : function(text) {
    if (this.rendered) {
      this.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(text);
    }
    this.fieldLabel = text;
  }
});

//----------------------------Procesara Descarga Archivos-----------------------
function procesarDescargaArchivos(opts, success, response) {
  if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
    var infoR = Ext.util.JSON.decode(response.responseText);
    var archivo = infoR.urlArchivo;				
    archivo = archivo.replace('/nafin','');
    var params = {nombreArchivo: archivo};				
    fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
    fp.getForm().getEl().dom.submit();
    fp.el.unmask();
    var el = gridConsulta.getGridEl();
    el.unmask();
    if (Ext.getCmp('gridTotales').isVisible()){
          var e2 = gridTotales.getGridEl();	
          e2.unmask();
        }
  }else {
    NE.util.mostrarConnError(response,opts);
    fp.el.unmask();
  }
};
//-----------------------------------Fin Descarga Archivos-----------------------------

//----------------------------procesarConsultaTotalesData ----------------------------------
var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
  var fp = Ext.getCmp('formcon');
  //fp.el.unmask();
            
  var gridTotales = Ext.getCmp('gridTotales');	
  var el = gridTotales.getGridEl();	

  if (arrRegistros != null) {  
    if (!gridTotales.isVisible()) {
      gridTotales.show();    
    }								
    if(store.getTotalCount() > 0) {
      el.unmask();					
    } else {				
      gridTotales.hide();
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
};
//--------------------------fin procesarConsultaTotalesData ---------------------------------
  
//----------------------------Procesar ConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  var fp = Ext.getCmp('formcon');
  fp.el.unmask();
  var jsonData = store.reader.jsonData;
  var gridConsulta = Ext.getCmp('gridConsulta');	
  var el = gridConsulta.getGridEl();	
  
  var cmGConsulta = gridConsulta.getColumnModel();
  if(Ext.getCmp('cmbTipoAfiliado').getValue()=='CE'){
    cmGConsulta.setColumnHeader(cmGConsulta.findColumnIndex('CLIENTE'), 'No. Cliente SIRAC');
    cmGConsulta.setHidden(cmGConsulta.findColumnIndex('NOMBRECLIEXTERNO'), false);
  }else{
    cmGConsulta.setColumnHeader(cmGConsulta.findColumnIndex('CLIENTE'), 'Cliente');
    cmGConsulta.setHidden(cmGConsulta.findColumnIndex('NOMBRECLIEXTERNO'), true);
  }
  
  if (arrRegistros != null) {
    if (!gridConsulta.isVisible()) {
      gridConsulta.show();
    }	
    if(store.getTotalCount() > 0) {//////////Verifica que existan registros
      Ext.getCmp('btnArchivoPDF').enable();
      Ext.getCmp('btnArchivoCSV').enable();
      el.unmask();					
      
      consultaTotalesData.load({
        params: Ext.apply(fp.getForm().getValues(),{
          informacion: 'ConsultarTotales'
        })
      });
      
    } else {	
      Ext.getCmp('btnArchivoPDF').disable();
      Ext.getCmp('btnArchivoCSV').disable();
      Ext.getCmp('gridTotales').hide();
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
}//-------------------------------Fin Procesar ConsultaData--------------


//--------------------------Consulta TotalesData------------------------------
var consultaTotalesData   = new Ext.data.JsonStore({ 
  root : 'registros',
  url : '15bitacorapagos01EXT.data.jsp',
  baseParams: {
    informacion: 'ConsultarTotales'
  },		
  fields: [	
    {	name: 'CD_NOMBRE'},
    {	name:	'TOTAL'}
  ],		
  totalProperty : 'total',
  messageProperty: 'msg',
  autoLoad: false,
  listeners: {
    load: procesarConsultaTotalesData,
    exception: {
      fn: function(proxy, type, action, optionsRequest, response, args) {
        NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
        //LLama procesar consulta, para que desbloquee los componentes.
        procesarConsultaTotalesData(null, null, null);					
      }
    }
  }		
});
//---------------------------Fin Consulta TotalesData-------------------------

//-----------------------Consulta Data----------------------------------
var consultaData   = new Ext.data.JsonStore({ 
  root : 'registros',
  url : '15bitacorapagos01EXT.data.jsp',
  baseParams: {
    informacion: 'Consultar'
  },		
  fields: [	
    {	name: 'TIPO'},
    {	name: 'INTERMEDIARIO'},	
    {	name: 'NOMBREINTERMEDIARIO'},
    {	name: 'CLIENTE'},	
    {	name: 'USUARIO'},
    {	name: 'FECHAACCESO'},
    {	name: 'MONEDA'},	
    {	name: 'FECHACORTE'},
    {	name:	'PRESTAMO'},
    {	name:	'FECHAOPERACION'},
    {	name: 'FECHAVENCIMIENTO'},
    {	name: 'NOMBRECLIEXTERNO'}
  ],		
  totalProperty : 'total',
  messageProperty: 'msg',
  autoLoad: false,
  listeners: {
    load: procesarConsultaData,
	beforeLoad:	{
		fn: function(store, options){   
			
			Ext.apply(options.params, {
				comboTipoLinea: Ext.getCmp('cmbTipoLinea').getValue(),
				cmb_ti: Ext.getCmp('cmbti').getValue(),
				numInter: Ext.getCmp('numInt').getValue(),
				_fechaReg: Ext.getCmp('fechaRegistro').getValue(),
				_fechaFinReg: Ext.getCmp('fechaFinRegistro').getValue(),
				cmb_tipo : Ext.getCmp('cmbTipo').getValue(),
				cmb_mon : Ext.getCmp('cmbmon').getValue(),
				comboTipoAfiliado: Ext.getCmp('cmbTipoAfiliado').getValue(),
				numSirac: Ext.getCmp('numSiracId').getValue()
		   });
		}		
	 },
    exception: {
      fn: function(proxy, type, action, optionsRequest, response, args) {
        NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
        //LLama procesar consulta, para que desbloquee los componentes.
        procesarConsultaData(null, null, null);					
      }
    }
  }		
});//---------------------------------Fin ConsultaData-------------------------
  

//-------------------------Store tipo---------------------------------
var tipo    = new Ext.data.ArrayStore({
   fields		: ['clave', 'tipo'],
   data 		: [
            ['S','Seleccione'],	
            ['E','Estado de cuenta'],
            ['V','Vencimiento'],
            ['O','Operados'],
            ['C','Cedula de conciliaci�n'],
            ['A','Archivos de pago']
          ]
});//--------------------------Fin Store tipo----------------------------

var catTipoLinea = new Ext.data.ArrayStore({
  id: 'catTipoLinea',
  fields : ['CLAVE', 'DESCRIPCION'],
  data:[['D','Linea Descuento'],['C', 'Linea Cr�dito']]
  /*listeners: {
    load: procesarCatIntermediarioData,
    exception: NE.util.mostrarDataProxyError,
    beforeload: NE.util.initMensajeCargaCombo
  }	*/	
});

var catTipoAfiliado = new Ext.data.ArrayStore({
  id: 'catTipoAfiliado',
  fields : ['CLAVE', 'DESCRIPCION']
  /*listeners: {
    load: procesarCatIntermediarioData,
    exception: NE.util.mostrarDataProxyError,
    beforeload: NE.util.initMensajeCargaCombo
  }	*/	
});

//---------------------------Catalogo Intermediario-----------------------------
var catalogoIntermediario = new Ext.data.JsonStore({
  id					: 'catalogoIntermediario',
  //autoDestroy     : true,
  //autoSave			: false,
  root 				: 'registros',
  fields 			: ['CLAVE', 'DESCRIPCION', 'loadMsg'],
  url 				: '15bitacorapagos01EXT.data.jsp',
  baseParams	: {
              informacion		: 'catalogoint'  
              },
  totalProperty 	: 'total',
  autoLoad			: false,
  listeners		: {			
              beforeload: NE.util.initMensajeCargaCombo,
              exception: NE.util.mostrarDataProxyError								
              }
});//-----------------------Fin Catalogo Intermediario-----------------------------

//---------------------------Catalogo Moneda------------------------------------
var catalogoMoneda = new Ext.data.JsonStore({
  id					: 'catalogoMoneda',
  root 				: 'registros',
  fields 			: ['clave', 'descripcion', 'loadMsg'],
  url 				: '15bitacorapagos01EXT.data.jsp',
  baseParams		: {
              informacion		: 'catalogomon'  
              },
  totalProperty 	: 'total',
  autoLoad			: false,
  listeners		: {			
              beforeload: NE.util.initMensajeCargaCombo,
              exception: NE.util.mostrarDataProxyError								
              }
});//----------------------------Fin Catalogo Moneda-------------------------------

//---------------------------------Grid Totales ------------------------------------
var gridTotales = new Ext.grid.EditorGridPanel({	
  store: consultaTotalesData,
  id: 'gridTotales',
  margins: '20 0 0 0',		
  style: 'margin:0 auto;',
  title: 'Totales',	
  align: 'center',
  hidden: true,
  columns: [	
    {
      header: 'Moneda',
      tooltip: 'Tipo de Moneda ',
      dataIndex: 'CD_NOMBRE',
      sortable: true,
      width: 220,			
      resizable: true,				
      align: 'center',
      renderer:function(value){
            return '<div align="left">'+ value+'</div>';
       }
    },
    {	header:'N�mero de registros',
      dataIndex:'TOTAL',
      sortable: true,
      width: 220,			
      resizable: true,				
      align: 'center'
    }			
  ],	
  stripeRows: true,
  loadMask: true,
  height: 150,
  width: 800,		
  frame: true	
});
//---------------------------Fin Grid Totales-----------------------------------

//--------------------------Grid Consulta-----------------------

var gridConsulta = new Ext.grid.EditorGridPanel({	
  store: consultaData,
  id: 'gridConsulta',
  margins: '20 0 0 0',		
  style: 'margin:0 auto;',
  title: 'Consulta',
  clicksToEdit: 1,
  hidden: true,
  columns: [	
    {
      header: 'Tipo',
      tooltip: 'Tipo',
      dataIndex: 'TIPO',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
		renderer:function(value){
		  if (Ext.getCmp('cmbTipo').getValue()=="E"){
				return "Estado de Cuenta";
		  } else if(Ext.getCmp('cmbTipo').getValue()=="V"){
				return "Vencimiento";
		  } else if(Ext.getCmp('cmbTipo').getValue()=="O"){
				return "Operados";
		  } else if(Ext.getCmp('cmbTipo').getValue()=="C"){
				return "C�dula de Conciliaci�n";
		  } else if(Ext.getCmp('cmbTipo').getValue()=="A"){
				return "Archivos de P�go";
		  }
      }
    },
    {
      header: 'Intermediario',
      tooltip: 'Intermediario',
      dataIndex: 'INTERMEDIARIO',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center'
    },
    {
      header: 'Nombre del Intermediario',
      tooltip: 'Nombre del Intermediario',
      dataIndex: 'NOMBREINTERMEDIARIO',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
      renderer:function(value){
        return "<div align='left'>"+value+"</div>";
      }
    },
    {
      header: 'Cliente',
      tooltip: 'Cliente',
      dataIndex: 'CLIENTE',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
		renderer			:function(value,params,record){
			if(record.data.CLIENTE=='0'){ 
				return "";
			} else { 
				return "<div align='left'>"+value+"</div>";
			}
		}
    },
    {
      header: 'Cliente Externo',
      tooltip: 'Cliente Externo',
      dataIndex: 'NOMBRECLIEXTERNO',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
      renderer:function(value){
        return "<div align='left'>"+value+"</div>";
      }
    },
    {
      header: 'Usuario',
      tooltip: 'Usuario',
      dataIndex: 'USUARIO',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
      renderer:function(value){
        return "<div align='left'>"+value+"</div>";
      }
    },
    {
      header: 'Fecha de Acceso',
      tooltip: 'Fecha de Acceso',
      dataIndex: 'FECHAACCESO',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center'
    },
    {
      header: 'Moneda',
      tooltip: 'Moneda',
      dataIndex: 'MONEDA',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center'
    },
    {
      header: 'Fecha de Corte',
      tooltip: 'Fecha de Corte',
      dataIndex: 'FECHACORTE',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
		renderer			:function(value,params,record){  
			if(Ext.getCmp('cmbTipo').getValue()=="V"){ 
				return "";
			} else if(Ext.getCmp('cmbTipo').getValue()=="O"){ 
				return "";
			} else { 
				return value;
			}
		}
    },{
      header: 'Pr�stamo',
      tooltip: 'Pr�stamo',
      dataIndex: 'PRESTAMO',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
      renderer:function(value){
			if(value=="0"){
				return "";
			} else {
				//return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
				return "<div align='left'>"+value+"</div>";
			}
      }
    }, {
      header: 'Fecha de Operaci�n',
      tooltip: 'Fecha de Operaci�n',
      dataIndex: 'FECHAOPERACION',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
		renderer			:function(value,params,record){  
			if(Ext.getCmp('cmbTipo').getValue()=="O"){ 
				return record.data.FECHACORTE;
			} else { 
				return value;
			}
		}
    },		{
      header: 'Fecha de Vencimiento',
      tooltip: 'Fecha de Vencimiento',
      dataIndex: 'FECHAVENCIMIENTO',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
		renderer			:function(value,params,record){  
			if(Ext.getCmp('cmbTipo').getValue()=="V"){ 
				return record.data.FECHACORTE;
			} else if(Ext.getCmp('cmbTipo').getValue()=="O"){ 
				return "";
			} else { 
				return value;
			}
		}
    }						
  ],			
  displayInfo: true,		
  emptyMsg: "No hay registros.",		
  loadMask: true,
  stripeRows: true,
  height: 430,
  width: 800,
  align: 'center',
  //frame: false,
  frame: true,
  bbar: {
    xtype: 'paging',
    pageSize: 15,
    buttonAlign: 'left',
    id: 'barraPaginacion',
    displayInfo: true,
    store: consultaData,
    displayMsg: '{0} - {1} de {2}',
    emptyMsg: "No hay registros.",
    items: [
      '->','-',
        {
        xtype: 'button',
        text: 'Generar Archivo',					
        tooltip:	'Generar Archivo',
        iconCls: 'icoXls',
        id: 'btnArchivoCSV',
        handler: function(boton, evento) {
        fp.el.mask('Generando Archivo...', 'x-mask-loading');
        var el = gridConsulta.getGridEl();	
        el.mask('Generando Archivo...', 'x-mask-loading');
        if (Ext.getCmp('gridTotales').isVisible()){
          var e2 = gridTotales.getGridEl();	
          e2.mask('Generando Archivo...', 'x-mask-loading');
        }
        boton.setIconClass('loading-indicator');
        boton.setDisabled(true);
          var barraPaginacionA = Ext.getCmp("barraPaginacion");						
            Ext.Ajax.request({
            url: '15bitacorapagos01EXT.data.jsp',
            params: Ext.apply(fp.getForm().getValues(),{							
              informacion: 'ArchivoCSV'
            }),
            success : function(response) {
              boton.setIconClass('icoXls');     
              boton.setDisabled(false);
            },	
            callback: procesarDescargaArchivos
          });						
        }
      },
      {
        xtype: 'button',
        text: 'Imprimir',					
        tooltip:	'Imprimir',
        iconCls: 'icoPdf',
        id: 'btnArchivoPDF',
        handler: function(boton, evento) {
        fp.el.mask('Generando Archivo...', 'x-mask-loading');
        var el = gridConsulta.getGridEl();	
        el.mask('Generando Archivo...', 'x-mask-loading');
        if (Ext.getCmp('gridTotales').isVisible()){
          var e2 = gridTotales.getGridEl();	
          e2.mask('Generando Archivo...', 'x-mask-loading');
        }
        boton.setIconClass('loading-indicator');
        boton.setDisabled(true);
          var barraPaginacionA = Ext.getCmp("barraPaginacion");						
            Ext.Ajax.request({
            url: '15bitacorapagos01EXT.data.jsp',
            params: Ext.apply(fp.getForm().getValues(),{							
              informacion: 'ArchivoPDF',
              start: barraPaginacionA.cursor,
              limit: barraPaginacionA.pageSize
            }),
            success : function(response) {
              boton.setIconClass('icoPdf');     
              boton.setDisabled(false);
            },
            callback: procesarDescargaArchivos
          });						
        }
      }
    ]
  }
});//----------------------------------Fin GridConsulta

//----------------------------------Radios--------------------------------------
var radios = [
  {
  xtype      : 'radio', 
  id			   : 'radioIFB',
  boxLabel   : 'IFB',
  name       : 'rbGroup',
  inputValue : 'B',
  handler 	 : function(rb,value) {
              Ext.getCmp('cmbTipo').clearValue();
              Ext.getCmp('cmbti').clearValue();
              Ext.getCmp('cmbmon').clearValue();
              Ext.getCmp('numInt').reset();
              Ext.getCmp('fechaRegistro').reset();
              Ext.getCmp('fechaFinRegistro').reset();
              Ext.getCmp('gridConsulta').hide();
              Ext.getCmp('gridTotales').hide();
				  if(value){
				  Ext.getCmp('cmbti').setValue('');
              catalogoIntermediario.load({
               params 	: Ext.apply({
               informacion:'catalogoint',
               rbGroup		:rb.getRawValue()
                })
              });
             }
      }// fin handler
  },  {
  xtype          : 'radio', 
  id					   : 'radioIFNB',
  boxLabel       : 'IFNB',
  name           : 'rbGroup',
  inputValue     : 'NB',
  handler 	 : function(rb,value) {
              Ext.getCmp('cmbTipo').clearValue();
              Ext.getCmp('cmbti').clearValue();
              Ext.getCmp('cmbmon').clearValue();
              Ext.getCmp('numInt').reset();
              Ext.getCmp('fechaRegistro').reset();
              Ext.getCmp('fechaFinRegistro').reset();                    
              Ext.getCmp('gridConsulta').hide();
              Ext.getCmp('gridTotales').hide();
				  if(value){
				  Ext.getCmp('cmbti').setValue('');
              catalogoIntermediario.load({
               params 	: Ext.apply({
               informacion:'catalogoint',
               rbGroup		:rb.getRawValue()
                })                
              });
             }
     }//fin handler
  }
];//--------------------------------Fin Radios-------------------------------

//-------------------------------Elementos Forma--------------------------------
var  elementosForma =  [
  /*{ 
  xtype		:'radiogroup',
  id			:'radioGroup',
  items		:radios,
  columns	:2
  }, */
  {
			xtype				: 'combo',
			id					: 'cmbTipoLinea',
			name				: 'comboTipoLinea',
			hiddenName 		:'comboTipoLinea', 
			fieldLabel		: 'Tipo de Linea',
			mode				: 'local',
			autoLoad: false,
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			emptyText: 'Seleccione Tipo de Linea',
			forceSelection	: true,
			triggerAction	: 'all',
			typeAhead: true,
			width			: 250,
			minChars : 1,
			allowBlank: true,
			store				: catTipoLinea,
			listeners: {
				select: {
					fn: function(combo) {
            Ext.getCmp('cmbTipoAfiliado').clearValue();
            Ext.getCmp('cmbTipo').clearValue();
            Ext.getCmp('cmbti').clearValue();
            Ext.getCmp('cmbmon').clearValue();
            Ext.getCmp('numInt').reset();
            Ext.getCmp('fechaRegistro').reset();
            Ext.getCmp('fechaFinRegistro').reset();
            Ext.getCmp('numInt').show();
            Ext.getCmp('numSiracId').hide();
            Ext.getCmp('gridConsulta').hide();
            Ext.getCmp('gridTotales').hide();
            
            if(combo.getValue()=='D'){
              catTipoAfiliado.loadData([['B','Intermediario Financiero'],['NB','Intermediario Financiero No Bancario']]);
            }else if(combo.getValue()=='C'){
              catTipoAfiliado.loadData([['B','Intermediario Financiero'],['NB','Intermediario Financiero No Bancario'],['CE','Cliente Externo']]);
            }
						
					}
				}
			}
		},
    {
			xtype				: 'combo',
			id					: 'cmbTipoAfiliado',
			name				: 'comboTipoAfiliado',
			hiddenName 		:'comboTipoAfiliado', 
			fieldLabel		: 'Afiliado',
			mode				: 'local',
			autoLoad: false,
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			emptyText: 'Seleccione Tipo de Afiliado',
			forceSelection	: true,
			triggerAction	: 'all',
			typeAhead: true,
			width			: 250,
			minChars : 1,
			allowBlank: true,
			store				: catTipoAfiliado,
			listeners: {
				select: {
					fn: function(combo) {
            Ext.getCmp('cmbTipo').clearValue();
            Ext.getCmp('cmbti').clearValue();
            Ext.getCmp('cmbmon').clearValue();
            Ext.getCmp('numInt').reset();
            Ext.getCmp('fechaRegistro').reset();
            Ext.getCmp('fechaFinRegistro').reset();
            Ext.getCmp('gridConsulta').hide();
            Ext.getCmp('gridTotales').hide();
            
            if(combo.getValue()=='B'){
              if(Ext.getCmp('cmbTipoLinea').getValue()=='C'){
                Ext.getCmp('numInt').setValue('');
                Ext.getCmp('numInt').hide();
              }else{
                Ext.getCmp('numInt').show();
              }
              
              Ext.getCmp('numSiracId').setValue('');
              Ext.getCmp('numSiracId').hide();
              
              Ext.getCmp('cmbti').setValue('');
              Ext.getCmp('cmbti').setFieldLabel('Intermediario Financiero');
              
              catalogoIntermediario.load({
                params 	: Ext.apply({
                informacion:'catalogoint',
                comboTipoAfiliado		:combo.getValue(),
                comboTipoLinea: Ext.getCmp('cmbTipoLinea').getValue()
                })
              });
            }else if(combo.getValue()=='NB'){
              
              if(Ext.getCmp('cmbTipoLinea').getValue()=='C'){
                Ext.getCmp('numInt').setValue('');
                Ext.getCmp('numInt').hide();
              }else{
                Ext.getCmp('numInt').show();
              }
              
              Ext.getCmp('numSiracId').setValue('');
              Ext.getCmp('numSiracId').hide();
              
              Ext.getCmp('cmbti').setValue('');
              Ext.getCmp('cmbti').setFieldLabel('Intermediario Financiero');
              
              catalogoIntermediario.load({
                params 	: Ext.apply({
                  informacion:'catalogoint',
                  comboTipoAfiliado		:combo.getValue(),
                  comboTipoLinea: Ext.getCmp('cmbTipoLinea').getValue()
                })                
              });
            }else if(combo.getValue()=='CE'){
              Ext.getCmp('numInt').hide();
              Ext.getCmp('numSiracId').show();
              Ext.getCmp('cmbti').setValue('');
              Ext.getCmp('cmbti').setFieldLabel('Cliente Externo');
              
              catalogoIntermediario.load({
                params 	: Ext.apply({
                  informacion:'catalogoint',
                  comboTipoAfiliado		:combo.getValue(),
                  comboTipoLinea: Ext.getCmp('cmbTipoLinea').getValue()
                })                
              });
            }
					}
				}
			}
		},
  {
  xtype				: 'combo',
  id          : 'cmbTipo',
  name			  : 'cmb_tipo',
  hiddenName	: 'cmb_tipo',
  fieldLabel	: 'Tipo', 
  mode				: 'local',	
  forceSelection : true,	
  triggerAction 	: 'all',	
  minChars 		: 1,	
  editable		: false,
  store 			: tipo,		
  valueField 	: 'clave',
  displayField: 'tipo',	
  typeAhead		: true,
  editable    : true,
  allowBlank	: false,
  listeners		: {
    select		: {
      fn			: function(combo) {
              var valor  = combo.getValue();
                 if (valor == 'S'){
                 }
                }// FIN fn
              }//FIN select
            }//FIN listeners
  },{
  xtype			  : 'numberfield',
  fieldLabel	: 'No. Intermediario Financiero',
  id				  : 'numInt',
  name			  : 'numInter',  
  hiddenName	: 'numInter',
  allowBlank	: true,
  maxLength 	: 10,
  anchor      : '55%'
  },{
  xtype			  : 'numberfield',
  fieldLabel	: 'N�mero Cliente SIRAC',
  id				  : 'numSiracId',
  name			  : 'numSirac',  
  hiddenName	: 'numSirac',
  allowBlank	: true,
  hidden      : true,
  maxLength 	: 12,
  anchor      : '55%'
  },{
  xtype				: 'combo',	
  name				: 'cmb_ti',	
  hiddenName	: 'cmb_ti',	
  id					: 'cmbti',	
  fieldLabel	: 'Intermediario Financiero', 
  mode				: 'local',
  autoLoad: false,
  displayField: 'DESCRIPCION',
  valueField 	: 'CLAVE',
  emptyText		: 'Seleccionar',
  forceSelection : true,	
  triggerAction  : 'all',	
  editable    	: true,
  minChars 		: 1,
  typeAhead		: true,
  store 			: catalogoIntermediario
  },{
  xtype				: 'combo',	
  name				: 'cmb_mon',	
  hiddenName	: 'cmb_mon',	
  id					: 'cmbmon',	
  fieldLabel	: 'Moneda', 
  displayField : 'descripcion',
  valueField 	: 'clave',
  emptyText		: 'Seleccionar',
  forceSelection : true,	
  triggerAction  : 'all',	
  typeAhead		: true,
  minChars 		: 1,	
  mode 			:'local',
  anchor       : '55%',
  store 			: catalogoMoneda
  },{
  xtype			: 'compositefield',
  id				:'fr',
  combineErrors: false,
  msgTarget		: 'side',
  items			:[{
    xtype		: 'datefield',
    fieldLabel : 'Fecha de Acceso',
    name			: '_fechaReg',
    id			: 'fechaRegistro',
    hiddenName : 'fecha_registro',
    allowBlank : true,
    startDay	: 0,
    width		: 140,
	 minValue	: '01/01/1901',
    msgTarget	: 'side',
    margins		: '0 20 0 0'
    },	{
    xtype		: 'displayfield',
    value		: 'al:',
    width		: 30
    },	{
    xtype		: 'datefield',
    name			: '_fechaFinReg',
    id			: 'fechaFinRegistro',
    hiddenName : 'fecha_fin_reg',
    allowBlank : true,
    startDay	: 0,
    width		: 140,
	 minValue	: '01/01/1901',
    msgTarget	: 'side',
    margins		: '0 20 0 0'
    }]
  }
];//-----------------------------Fin Elementos Forma------------------------------


//-------------------------------Panel Consulta---------------------------------
var fp = new Ext.form.FormPanel({
  id					:'formcon',
  width				:650,
  heigth			:'auto',
  title				:'<center>Bit�cora Pagos de Cartera</center>',
  frame				:true,
  collapsible		:true,
  titleCollapse	:false,
  style				:'margin:0 auto;',
  bodyStyle		:'padding: 6px',
  labelWidth		:200,
  //monitorValid	:true,	
  defaults			:{
            msgTarget: 'side',
            anchor: '-20'
            },
  items				:[ 
            elementosForma
            ],
  buttons		:[{
            text				:'Consultar',
            id					:'consultar',
            iconCls			:'icoBuscar',
            handler			:function(boton, evento){     
            Ext.getCmp('gridConsulta').hide;
            Ext.getCmp('gridTotales').hide;
            //if ( Ext.getCmp('radioIFB').getValue() | Ext.getCmp('radioIFNB').getValue()  ){
            if ( Ext.getCmp('cmbTipoAfiliado').getValue()=='B' | Ext.getCmp('cmbTipoAfiliado').getValue()=='NB' | Ext.getCmp('cmbTipoAfiliado').getValue()=='CE' ){
              if ( Ext.getCmp('cmbTipo').getValue() != "" & Ext.getCmp('cmbTipo').getValue() != "S"  ){     
                if(Ext.getCmp('fechaRegistro').isValid() & Ext.getCmp('fechaFinRegistro').isValid()){
                  if ( (Ext.getCmp('fechaRegistro').getValue() != "" & Ext.getCmp('fechaFinRegistro').getValue() != "") | (Ext.getCmp('fechaRegistro').getValue() == "" & Ext.getCmp('fechaFinRegistro').getValue() == "") ){ 
                    if( (Ext.getCmp('fechaRegistro').getValue() <= Ext.getCmp('fechaFinRegistro').getValue()) | (Ext.getCmp('fechaRegistro').getValue() == "" & Ext.getCmp('fechaFinRegistro').getValue() == "") ){								
												Ext.getCmp('fechaRegistro').clearInvalid();
												Ext.getCmp('fechaFinRegistro').clearInvalid();
												fp.el.mask('Cargando...', 'x-mask-loading');
                        consultaData.load({
                        params: Ext.apply(fp.getForm().getValues(),{
                          operacion : 'Generar',
                          informacion : 'Consultar',
                          start:0,
                          limit:15										
                        })
                        });
                    } else {
                      Ext.getCmp('fechaRegistro').markInvalid('La fecha inicial debe ser menor que la fecha final.');
                    }
                  } else if( (Ext.getCmp('fechaRegistro').getValue() != "" & Ext.getCmp('fechaFinRegistro').getValue() == "") | (Ext.getCmp('fechaRegistro').getValue() == "" & Ext.getCmp('fechaFinRegistro').getValue() != "") ){
                    if( Ext.getCmp('fechaRegistro').getValue() == "" ){
                      Ext.getCmp('fechaRegistro').markInvalid('Debe de poner ambas fechas para usar este criterio de b�squeda.');
                  } else if( Ext.getCmp('fechaFinRegistro').getValue() == "" ){
                  	Ext.getCmp('fechaFinRegistro').markInvalid('Debe de poner ambas fechas para usar este criterio de b�squeda.');
                  }
                }
              } else {
              }
            } else { 
              Ext.getCmp('cmbTipo').markInvalid('Debe seleccionar el tipo que desea consultar');
            }                               
          } else {
            Ext.getCmp('cmbTipoAfiliado').markInvalid('Debe seleccionar el tipo de Intermediario Financiero (IFB/IFNB)');
          }
        }//fin handler
      },{
        text				:'Limpiar',
        id					:'btnLimpiar',
        iconCls			:'icoLimpiar',
        handler			:function(boton, evento){
                catalogoIntermediario.load();
                Ext.getCmp('formcon').getForm().reset();                     
                Ext.getCmp('gridConsulta').hide();
                Ext.getCmp('gridTotales').hide();
        }
      }]
});//------------------------------Fin Panel Consulta------------------------------


//----------------------------Contenedor Principal------------------------------
var pnl = new Ext.Container({
  id			:'contenedorPrincipal',
  applyTo	:'areaContenido',
  width		: 940,
  style		:'margin:0 auto;',	
  items		:[
        NE.util.getEspaciador(20),
        fp,
        NE.util.getEspaciador(20),
        gridConsulta,
        NE.util.getEspaciador(20),
        gridTotales
        ]
});//-----------------------------Fin Contenedor Principal-------------------------
catalogoMoneda.load();
});