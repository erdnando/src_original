
Ext.onReady(function() {
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = store.reader.jsonData;
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			if(store.getTotalCount() > 0) {//////////Verifica que existan registros
				Ext.getCmp('btnArchivoPDF').enable();
				//Ext.getCmp('btnArchivoCSV').enable();
				consultaTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarTotales1'
					})
				});
				el.unmask();					
			} else {	
				gridTotales.hide();
				Ext.getCmp('btnArchivoPDF').disable();
				//Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
		
			if (!gridTotales.isVisible()) {
				gridTotales.show();
				
				
			}								
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
			
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------

	var consultaTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15consultacontratos01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales1'
		},		
		fields: [	
			{	name: 'TOTAL_CONTRATOS'},
			{	name: 'MONTO_APROBADO'},
			{	name: 'MONTO_DISPONIBLE'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesData(null, null, null);					
				}
			}
		}		
	});

	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15consultacontratos01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'PROYECTO'},
			{	name: 'NUMERO_CONTRATO'},	
			{	name: 'NOMBRE'},
			{	name: 'FECHA_APERTURA'},	
			{	name: 'CODIGO_LINEA_FINANCIERA'},
			{	name: 'MONTO_APROBADO'},
			{	name: 'MONTO_DISPONIBLE'},	
			{	name: 'ESTADO_CONTRATO'},
			//OTROS
			{	name:	'CODIGO_AGENCIA'},
			{	name:	'CODIGO_SUB_APLICACION'},
			{	name: 'CODIGO_LINEA_CREDITO'},
			{  name: 'CODIGO_FINANCIERA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	

//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
var elementosForma=[
		{
			xtype			:	'numberfield',
			fieldLabel 	: 'Cliente',
			name 			: 'cliente',
			id 			: 'txtCliente',
			allowBlank	: true,
			maxLength 	: 10,
			width 		: 100,
			msgTarget 	: 'side',
			tabIndex		: 1,
			margins		: '0 20 0 0'
		},
		{
			xtype			:	'textfield',
			fieldLabel 	: 'Proyecto',
			name 			: 'proyecto',
			id 			: 'txtProyecto',
			allowBlank	: true,
			maxLength 	: 15,
			width 		: 100,
			msgTarget 	: 'side',
			tabIndex		: 2,
			regex:/^\d{2}-\d{3}-\d{8}/,
			regexText:'El formato del proyecto no es adecuado<br>Formato (agencia-sub aplicacion-proyecto): xx-xxx-xxxxxxxx',
			margins		: '0 20 0 0'
		},
		{
			xtype			:	'numberfield',
			fieldLabel 	: 'Contrato',
			name 			: 'contrato',
			id 			: 'txtContrato',
			allowBlank	: true,
			maxLength 	: 8,
			width 		: 100,
			msgTarget 	: 'side',
			tabIndex		: 3,
			margins		: '0 20 0 0'
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Consulta Contratos',
		frame: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		//defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		collapsible: true,
		titleCollapse: false,
		items: elementosForma,			
		monitorValid: true,
		buttons: [
		{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,	
				tabIndex			: 4,
				handler: function(boton, evento) {
					var val = Ext.getCmp('txtProyecto').getValue();
					if(val!=''){
					if(!/^\d{2}-\d{3}-\d{8}/.test(val)){
						Ext.Msg.alert('Mensaje','El formato del proyecto no es adecuado<br>Formato (agencia-sub aplicacion-proyecto): xx-xxx-xxxxxxxx');
						Ext.getCmp('txtProyecto').setValue('');
						return;
					}
					}
					fp.el.mask('Procesando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion : 'Generar',
							informacion : 'Consultar',
							start:0,
							limit:15					
						})
						
					}); 
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				tabIndex			: 8,
				handler: function() {
					//window.location = '15consultacontratos01Ext.jsp';					
					gridConsulta.hide();
					gridTotales.hide();
					fp.getForm().reset();
					
				}
			}
		]
	});
	//grid de resultados de la consulta
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Proyecto',
				tooltip: 'Proyecto',
				dataIndex: 'PROYECTO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'N�m.<br>Contrato',
				tooltip: 'N�m. Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Intermediario',
				tooltip: 'Intermediario',
				dataIndex: 'NOMBRE',
				sortable: true,
				width: 250,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=left>'+value+'</div>';
				}
			},
			{
				header: 'Fecha de<BR>Apertura',
				tooltip: 'Fecha de Apertura',
				dataIndex: 'FECHA_APERTURA',
				sortable: true,
				width: 80,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'L�nea <BR>Financiera',
				tooltip: 'L�nea Financiera',
				dataIndex: 'CODIGO_LINEA_FINANCIERA',
				sortable: true,
				width: 80,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Monto Aprobado',
				tooltip: 'Monto Aprobado',
				dataIndex: 'MONTO_APROBADO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer: function(value){ 
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>'
				}
			},
			{
				header: 'Monto Disponible',
				tooltip: 'Monto Disponible',
				dataIndex: 'MONTO_DISPONIBLE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer: function(value){ 
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>'
				}
			},
			{
				header: 'Estado',
				tooltip: 'Estado',
				dataIndex: 'ESTADO_CONTRATO',
				sortable: true,
				width: 80,			
				resizable: true,				
				align: 'center'
			}									
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 430,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
					/*{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',
					hidden: true,
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						var barraPaginacionA = Ext.getCmp("barraPaginacion");						
							Ext.Ajax.request({
							url: '15consultacontratos01Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoCSV'
							}),
							success : function(response) {
								boton.setIconClass('icoXls');     
								boton.setDisabled(false);

							},	
							callback: procesarDescargaArchivos
						});						
					}
				},*/
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						var barraPaginacionA = Ext.getCmp("barraPaginacion");	
						boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '15consultacontratos01Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoPDF',
								start: barraPaginacionA.cursor,
								limit: barraPaginacionA.pageSize
							}),
							success : function(response) {
								boton.setIconClass('icoPdf');     
								boton.setDisabled(false);

							},
							callback: procesarDescargaArchivos
						});						
					}
				}
			]
		}
	});
	//grid de Totales
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Totales',	
		align: 'center',
		hidden: true,//true,
		columns: [	
			
			{	header:'Total Contratos',
				dataIndex:'TOTAL_CONTRATOS',sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Monto Aprobado',
				tooltip: 'Monto Aprobado',
				dataIndex: 'MONTO_APROBADO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer: function(value){ 
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>'
				}
			},{
				header: 'Monto Disponible',
				tooltip: 'Monto Disponible',
				dataIndex: 'MONTO_DISPONIBLE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer: function(value){ 
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>'
				}
			}	
		],	
		stripeRows: true,
		loadMask: true,
		height: 150,
		width: 900,		
		frame: true	
	});

	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 947,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),			
			gridTotales,
			NE.util.getEspaciador(20)			
		]
	});


});	
