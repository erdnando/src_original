Ext.onReady(function() {
	Ext.override(Ext.form.Field, {
		setFieldLabel : function(text) {
			if (this.rendered) {
				this.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(text);
			}
			this.fieldLabel = text;
		}
	});

	var pantallaTitulo='Mensaje';
	var ic_if='';
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarGuardar = function (datos, opts) {
		var fp= Ext.getCmp('formaG');
		fp.el.unmask();
		var infoR = Ext.util.JSON.decode(datos.responseText);
		Ext.Msg.alert('',infoR.mensaje);
		limpiarFrm();
	}
	function limpiarFrm(){
			gridConsulta.hide();
			gridTotales.hide();
			fpGuardar.hide();
			botonesG.hide();
	}
	var procesarConsultaData_ = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridTotales = Ext.getCmp('gridTotales');	
		gridTotales.hide();
		fpGuardar.hide();
		botonesG.hide()
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {	
				el.unmask();	
				//MOSTRAR TOTALES
				var montoOperado='0.0';
				var capitalVigente='0.0';
				var capitalVencido='0.0';
				var interesVigente='0.0';
				var interesVencido='0.0';
				var moras='0.0';
				var totalAdeudo='0.0';
				store.each(function(rec){
					ic_if = rec.get('IC_IF');
					montoOperado =parseFloat(montoOperado)+ parseFloat(rec.get('MONTO_OPERADO') );
					capitalVigente =parseFloat(capitalVigente)+ parseFloat(rec.get('CAPITAL_VIGENTE') );
					capitalVencido =parseFloat(capitalVencido)+ parseFloat(rec.get('CAPITAL_VENCIDO') );
					interesVigente =parseFloat(interesVigente)+ parseFloat(rec.get('INTERES_VIGENTE') );
					interesVencido =parseFloat(interesVencido)+ parseFloat(rec.get('INTERES_VENCIDO') );
					moras =parseFloat(moras)+ parseFloat(rec.get('MORAS') );
					totalAdeudo =parseFloat(totalAdeudo)+ parseFloat(rec.get('TOTAL_ADEUDO') );
				},this);
				var resultados=[
					[montoOperado,capitalVigente,capitalVencido,interesVigente,interesVencido,moras,totalAdeudo]
				];
				consultaTotalesData.loadData(resultados);
				//Consultar_Observaciones
				Ext.Ajax.request({
					url: '15cedulaconciliacionobservacion01Ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{							
						informacion: 'Consultar_Observaciones',
						ic_if:ic_if
					}),	
					callback: procesarConsultaObsercacionData
				});
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var procesarTotalesData_  = function(store, arrRegistros, opts) 	{
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}								
			if(store.getTotalCount() > 0) {	
				el.unmask();	
				
				fpGuardar.show();
				botonesG.show()
				//gridTotales.show();
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
				gridTotales.hide();
			}
		}
	}
	var procesarConsultaObsercacionData = function(store, arrRegistros, opts) 	{
		var resultados =Ext.util.JSON.decode(opts.responseText);
		if (arrRegistros != null) {
				Ext.getCmp('txtObservaciones').setValue(resultados.observaciones.data[0]);
		}
	}
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------
	var consultaData_   = new Ext.data.ArrayStore({ 
		root : 'registros',
		url : '15cedulaconciliacionobservacion01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar_'
		},		
		fields: [	
			{	name: 'TIPO_CREDITO'},
		{	name: 'MONTO_OPERADO'},
		{	name: 'CAPITAL_VIGENTE'},
		{	name: 'CAPITAL_VENCIDO'},
		{	name: 'INTERES_VIGENTE'},
		{	name: 'INTERES_VENCIDO'},
		{	name: 'MORAS'},
		{	name: 'TOTAL_ADEUDO'},
		{	name: 'DESCUENTOS'},
		{	name: 'IC_IF'},
		{	name:	'CG_RAZON_SOCIAL'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData_,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData_(null, null, null);					
				}
			}
		}		
	});
	var consultaTotalesData   = new Ext.data.ArrayStore({ 
		fields: [	
		{	name: 'MONTO_OPERADO'},
		{	name: 'CAPITAL_VIGENTE'},
		{	name: 'CAPITAL_VENCIDO'},
		{	name: 'INTERES_VIGENTE'},
		{	name: 'INTERES_VENCIDO'},
		{	name: 'MORAS'},
		{	name: 'TOTAL_ADEUDO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarTotalesData_,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData_(null, null, null);					
				}
			}
		}		
	});

	var catalogoIntermediario = new Ext.data.JsonStore({
		id: 'catalogoIntermediario',
		root : 'registros',
		fields : ['CLAVE', 'DESCRIPCION','loadMsg'],
		url : '15cedulaconciliacionobservacion01Ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIntermediario'  ///hacer el metodo en el data
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['CLAVE', 'DESCRIPCION','loadMsg'],
		url : '15cedulaconciliacionobservacion01Ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'  ///hacer el metodo en el data
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoFechaCorte = new Ext.data.JsonStore({
		id: 'catalogoFechaCorte',
		root : 'registros',
		fields : ['CLAVE', 'DESCRIPCION','loadMsg'],
		url : '15cedulaconciliacionobservacion01Ext.data.jsp',
		baseParams: {
			informacion: 'catalogoFechaCorte'  ///hacer el metodo en el data
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catTipoLinea = new Ext.data.ArrayStore({
		id: 'catTipoLinea',
		fields : ['CLAVE', 'DESCRIPCION'],
		data:[['D','Linea Descuento'],['C', 'Linea Cr�dito']]
	});
	
	var catTipoAfiliado = new Ext.data.ArrayStore({
		id: 'catTipoAfiliado',
		fields : ['CLAVE', 'DESCRIPCION']	
	});
//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
 var radios = [
      {
        xtype      : 'radio',
        fieldLabel : "",
        boxLabel   : 'IF',
        name       : 'intemediarioFinanciero',
        inputValue : 'B',
			handler		:function(rb,value){
				var tipo ='';
				if(value){
					Ext.getCmp('txtInternediario').setValue('');
					Ext.getCmp('cmbIntermediario').setValue('');
					Ext.getCmp('cmbMoneda').setValue('');
					Ext.getCmp('cmbFechaCorte').setValue('');
					Ext.getCmp('cmbFechaCorte').getStore().removeAll(true);//
					limpiarFrm();
					/*gridConsulta.hide();
					gridTotales.hide();
					fpGuardar.hide();
					botonesG.hide();*/
					
					tipo= rb.getRawValue();
					catalogoIntermediario.load({
						params : Ext.apply({
							tipo : rb.getRawValue()
						})
					});	
					
				}
			}
      },
      {
        xtype          : 'radio',
        fieldLabel     : "",
        //labelSeparator : ' ',
        boxLabel       : 'IFNB',
        name           : 'intemediarioFinanciero',
        inputValue     : 'NB',
		  handler		:function(rb,value){
				var tipo ='';
				if(value){
					Ext.getCmp('txtInternediario').setValue('');
					Ext.getCmp('cmbIntermediario').setValue('');
					Ext.getCmp('cmbMoneda').setValue('');
					Ext.getCmp('cmbFechaCorte').setValue('');
					Ext.getCmp('cmbFechaCorte').getStore().removeAll(true);//
					limpiarFrm();
					tipo= rb.getRawValue();
					catalogoIntermediario.load({
						params : Ext.apply({
							tipo : rb.getRawValue()
						})
					});
				}
			}
      }
    ]
var elementosForma=[
		/*{
			xtype		:'radiogroup',
			id			:'radioGroup',
			items		:radios,
			columns	:2,
			allowBlank	: false
			
		},*/
		{
			xtype: 'combo',
			id: 'cmbTipoLinea',
			name: 'tipoLinea',
			hiddenName:'tipoLinea', 
			fieldLabel: 'Tipo de Linea',
			mode: 'local',
			autoLoad: false,
			valueField: 'CLAVE',
			displayField:'DESCRIPCION',
			emptyText: 'Seleccionar',
			forceSelection: true,
			triggerAction: 'all',
			typeAhead: true,
			width: 250,
			minChars: 1,
			allowBlank: true,
			store: catTipoLinea,
			listeners: {
				select: {
					fn: function(combo) {
						var sClaveIF1 = Ext.getCmp('cmbIntermediario');
						sClaveIF1.reset();
						sClaveIF1.setFieldLabel('Intermediario Financiero');
						Ext.getCmp('id_cs_tipo').reset();
						
						catalogoIntermediario.removeAll();
            
						if(combo.getValue()=='D'){
							Ext.getCmp('txtInternediario').setFieldLabel('No. Intermediario:');
						  catTipoAfiliado.loadData([['B','Intermediario Financiero'],['NB','Intermediario Financiero No Bancario']]);
						}else if(combo.getValue()=='C'){
							Ext.getCmp('txtInternediario').setFieldLabel('No. Cliente SIRAC:');
						  catTipoAfiliado.loadData([['B','Intermediario Financiero'],['NB','Intermediario Financiero No Bancario'],['CE','Cliente Externo']]);
						}
						
					}
				}
			}
		},
		{
			xtype: 'combo',
			id: 'id_cs_tipo',
			name: 'tipo',
			hiddenName:'tipo', 
			fieldLabel: 'Afiliado',
			mode: 'local',
			autoLoad: false,
			valueField: 'CLAVE',
			displayField:'DESCRIPCION',
			emptyText: 'Seleccionar',
			forceSelection: true,
			triggerAction: 'all',
			typeAhead: true,
			width: 250,
			minChars: 1,
			allowBlank: true,
			store: catTipoAfiliado,
			listeners: {
				select: {
					fn: function(combo) {
						var valor =	(Ext.getCmp('id_cs_tipo')).getValue();
						var valorTipoLinea = Ext.getCmp('cmbTipoLinea').getValue();
						var sClaveIF1 = Ext.getCmp('cmbIntermediario');
						
						if(valor=='CE'){
							sClaveIF1.setFieldLabel('Cliente Externo:');
						}else if(valor=='B'){
							sClaveIF1.setFieldLabel('Intermediario Financiero:');
						}else if(valor=='NB'){
							sClaveIF1.setFieldLabel('Intermediario Financiero No Bancario:');
						}
						
						Ext.getCmp('txtInternediario').setValue('');
						Ext.getCmp('cmbIntermediario').setValue('');
						Ext.getCmp('cmbMoneda').setValue('');
						Ext.getCmp('cmbFechaCorte').setValue('');
						Ext.getCmp('cmbFechaCorte').getStore().removeAll(true);//
						limpiarFrm();
						
						sClaveIF1.reset();
						catalogoIntermediario.load({
							params : Ext.apply({
							  tipo: valor,
							  tipoLinea: valorTipoLinea
							})
						});	
					}
				}
			}
		},
		{
			xtype			:	'numberfield',
			fieldLabel 	: 'No. Intermediario',
			name 			: 'no_intermediario',
			id 			: 'txtInternediario',
			//allowBlank	: true,
			maxLength 	: 10,
			width 		: 100,
			msgTarget 	: 'side',
			//tabIndex		: 1,
			margins		: '0 20 0 0',
			listeners: {
				blur: {
					fn: function(txt) {
						
						Ext.getCmp('cmbIntermediario').reset();
						Ext.getCmp('cmbFechaCorte').reset();
						Ext.getCmp('cmbFechaCorte').getStore().removeAll(true);//
						Ext.getCmp('cmbMoneda').reset();
						//catalogoFechaCorte.recmbFechaCorte
						/*var valorRadio= Ext.getCmp('radioGroup').getValue();
						if(valorRadio){
							if(!Ext.isEmpty(txt.value) || !Ext.isEmpty(Ext.getCmp('cmbIntermediario').getValue())){
								catalogoFechaCorte.load({
									params : Ext.apply(fp.getForm().getValues())
								});
							}
						}*/
					}
				}
			}
		},
		{
			xtype				: 'combo',
			id					: 'cmbIntermediario',
			name				: 'comboIntermediario',
			hiddenName 		:'comboIntermediario', 
			fieldLabel		: 'Intermediario',
			width				: 250,
			forceSelection	: true,
			emptyText		:'Seleccionar...',
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			store				: catalogoIntermediario,
			//tabIndex			: 1,
			listeners: {
				select: {
					fn: function(combo) {
							Ext.getCmp('txtInternediario').setValue();
							/*catalogoFechaCorte.load({
								params : Ext.apply({
									ic_if : combo.getValue()
								})
							});
							Ext.getCmp('cmbFechaCorte').setValue();*/
							Ext.getCmp('cmbMoneda').reset();
							Ext.getCmp('cmbFechaCorte').reset();
							Ext.getCmp('cmbFechaCorte').getStore().removeAll(true);//
					}
				}
			}
		},
		{
			xtype				: 'combo',
			id					: 'cmbMoneda',
			name				: 'comboMoneda',
			hiddenName 		:'comboMoneda', 
			fieldLabel		: 'Moneda',
			width				: 250,
			forceSelection	: true,
			//allowBlank		: false,
			emptyText		:'Seleccionar...',
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			//tabIndex			: 2,
			store				: catalogoMoneda,
			listeners:{
				select:{
					fn:function(combo){
						var valorRadio= Ext.getCmp('id_cs_tipo').getValue();
						if(valorRadio!=''){
							if(!Ext.isEmpty(Ext.getCmp('txtInternediario').getValue()) || !Ext.isEmpty(Ext.getCmp('cmbIntermediario').getValue())){
								catalogoFechaCorte.load({
									params : Ext.apply(fp.getForm().getValues())
								});
							}
						}
					
					}
				}
			}
			
		},
		{
			xtype				: 'combo',
			fieldLabel		: 'Fecha de corte',
			name				: 'catalogoFechaCorte',
			id					: 'cmbFechaCorte',
			mode				: 'local',
			autoLoad			: false,
			displayField	: 'DESCRIPCION',
			emptyText		: 'Seleccionar ...',			
			valueField		: 'CLAVE',
			hiddenName 		: 'catalogoFechaCorte',					
			forceSelection : true,
			triggerAction 	: 'all',
			typeAhead		: true,
			minChars 		: 1,
			//tabIndex			: 3,
			forceSelection : true,
			store				: catalogoFechaCorte
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'C�dula de Conciliaci�n Observaciones',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'consultar',
				iconCls: 'icoBuscar',
				formBind: true,	
				//tabIndex			: 7,
				handler: function(boton, evento) {	
					var valido = false;
					var numero= Ext.getCmp('txtInternediario');
					var intermediario= Ext.getCmp('cmbIntermediario');
					var moneda= Ext.getCmp('cmbMoneda');
					var fecha= Ext.getCmp('cmbFechaCorte');
					
					if(Ext.isEmpty(numero.getValue()) && Ext.isEmpty(intermediario.getValue())){
						Ext.Msg.alert(pantallaTitulo,'Debe capturar el N�m. de Intermediario Financiero o Seleccionarlo');
						numero.focus(this);
						valido= false;
					}else if(moneda.getValue()==''){
						Ext.Msg.alert(pantallaTitulo,'Debe seleccionar el tipo de moneda');
						moneda.focus(this);
						valido= false;
					}else if(fecha.getValue()==''){
						Ext.Msg.alert(pantallaTitulo,'Debe seleccionar la fecha de corte');
						fecha.focus(this);
						valido= false;
					}else{
						valido= true;
					}
					
					if(valido){
						fp.el.mask('Procesando...', 'x-mask-loading');
						consultaData_.load({
							params: Ext.apply(fp.getForm().getValues(),{
								informacion : 'Consultar_'
							})
						}); 
					}else{
						return;
					}
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('cmbIntermediario').getStore().removeAll(true);//
					Ext.getCmp('cmbFechaCorte').getStore().removeAll(true);//
					fp.getForm().reset();
					limpiarFrm();
				}
			}
		]
	});
	//grid de resultados de la consulta
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData_,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		title: 'Consulta',
		collapsible : true,
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Tipo de Cr�dito',
				tooltip: 'Tipo de Cr�dito',
				dataIndex: 'TIPO_CREDITO',
				sortable: true,
				width: 250,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=left>'+(value)+'</div>';
				} 
			},
			{
				header: 'Monto Operado',
				tooltip: 'Monto Operado',
				dataIndex: 'MONTO_OPERADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Capital Vigente',
				tooltip: 'Capital Vigente',
				dataIndex: 'CAPITAL_VIGENTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Capital Vencido',
				tooltip: 'Capital Vencido',
				dataIndex: 'CAPITAL_VENCIDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Inter�s Vigente',
				tooltip: 'Inter�s Vigente',
				dataIndex: 'INTERES_VIGENTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Inter�s Vencido',
				tooltip: 'Inter�s Vencido',
				dataIndex: 'INTERES_VENCIDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Moras',
				tooltip: 'Moras',
				dataIndex: 'MORAS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Total Adeudo',
				tooltip: 'Total Adeudo',
				dataIndex: 'TOTAL_ADEUDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Descuentos',
				tooltip: 'Descuentos',
				dataIndex: 'DESCUENTOS',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			}					
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 430,
		width: 900,
		align: 'center',
		frame: false
	});
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Totales',
		collapsible : true,
		clicksToEdit: 1,
		hidden: true,
		viewConfig :{
			forceFit:true
		},
		columns: [	
			{
				header: 'Monto Operado',
				tooltip: 'Monto Operado',
				dataIndex: 'MONTO_OPERADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Capital Vigente',
				tooltip: 'Capital Vigente',
				dataIndex: 'CAPITAL_VIGENTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Capital Vencido',
				tooltip: 'Capital Vencido',
				dataIndex: 'CAPITAL_VENCIDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Inter�s Vigente',
				tooltip: 'Inter�s Vigente',
				dataIndex: 'INTERES_VIGENTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Inter�s Vencido',
				tooltip: 'Inter�s Vencido',
				dataIndex: 'INTERES_VENCIDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Moras',
				tooltip: 'Moras',
				dataIndex: 'MORAS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Total Adeudo',
				tooltip: 'Total Adeudo',
				dataIndex: 'TOTAL_ADEUDO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			}				
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 100,
		width: 900,
		align: 'center',
		frame: false
	});
	var elementosFormaGuardar=[
		{
			xtype:'textarea',
			id:'txtObservaciones',
			name:'observaciones',
			fieldLabel: 'Observaciones'
			
		}
	];
	var fpGuardar = new Ext.form.FormPanel({
		id: 'formaG',
		width: 500,
		hidden: true,
		//title: 'Cedula de Conciliaci�n Observaciones',
		frame: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:elementosFormaGuardar ,			
		monitorValid: true
	});
	var botonesG =new Ext.Container ({
		layout: 'table',
		//id: 'fpBotones',
		hidden: true,
		width: '100',
		heigth: 'auto',
		style: 'margin: 0 auto',
		items: [	
					{
						xtype: 'button',
						text: 'Limpiar',
						id: 'limpiarG',
						iconCls: 'icoLimpiar',
						handler: function() {
							fpGuardar.getForm().reset();
						}
					},
					{
						xtype: 'button',
						text: 'Guardar',
						id: 'btnAceptar',
						iconCls: 'icoGuardar',
						formBind: true,				
						handler: function(boton, evento) {
								fpGuardar.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '15cedulaconciliacionobservacion01Ext.data.jsp',
									params:Ext.apply(fp.getForm().getValues(),fpGuardar.getForm().getValues(),
										{							
											informacion: 'Guardar'
										}
									),	success: procesarGuardar
								});
						}
					}
				]
  });
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 947,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,
			//NE.util.getEspaciador(20),			
			gridTotales,
			NE.util.getEspaciador(20),
			fpGuardar,
			NE.util.getEspaciador(5),
			botonesG
		]
	});
	///Cargar el catalogo intemediario
	catalogoMoneda.load();
});