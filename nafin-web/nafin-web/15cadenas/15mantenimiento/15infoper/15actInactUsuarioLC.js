Ext.onReady(function(){
	function procesarSuccessFailureModifica(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  info = Ext.JSON.decode(response.responseText); 
			Ext.MessageBox.alert("Mensaje",info.mensaje);
			consultaData.load({	
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15						
						})
					});
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
	Ext.define('ModelCatalogos1', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'CLAVE', 					type: 'srocesarConsultartring' },
			{ name: 'DESCRIPCION', 			type: 'string' }				
		]
	});
		var catalogoEstatus = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['S','Activar'],
			['N','Inactivar']
		 ]
	});
	Ext.define('LisRegistros', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'NUM_ELECTRONICO'},	
			{ name: 'NOMBRE_RAZON_SOCIAL' },
			{ name: 'RFC' },
			{ name: 'NUM_SIRAC' },
			{ name: 'DOMICILIO' },
			{ name: 'ESTADO' },
			{ name: 'TELEFONO' },
			{ name: 'CS_HABILITADO' },
			{ name: 'CS_OBSERVACIONES' }		
		 ]
	});
	var comboEstatus = new Ext.form.ComboBox({  
		mode: 'local',
		id: 'cmbEstatus',	 
		displayField : 'descripcion',
		forceSelection : true,
		triggerAction : 'all',
		allowBlank: true,
		store: catalogoEstatus,
		valueField : 'clave',
		typeAhead: true,
		minChars : 1
	});
	Ext.util.Format.comboRenderer = function(comboEstatus){
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			if(value!=''){
				var record = comboEstatus.findRecord(comboEstatus.valueField, value);
				return record ? record.get(comboEstatus.displayField) : comboEstatus.valueNotFoundText;
			}				
		}
	}
	var procesarConsultar = function(store,  arrRegistros, success, opts) {
		var fp = Ext.ComponentQuery.query('#forma')[0];
		fp.unmask();
		
		if (arrRegistros != null) {
			var gridConsulta =  Ext.ComponentQuery.query('#gridConsulta')[0];
			var json =  store.proxy.reader.rawData;
			var Linea = Ext.ComponentQuery.query('#cmbLineaCredito')[0];
			var num = Ext.ComponentQuery.query('#cmbLineaCredito')[0].getValue();
			var cliente = Ext.ComponentQuery.query('#cmbLineaCredito')[0].getRawValue();
			if(!Ext.isEmpty(Linea.getValue())){
				Ext.ComponentQuery.query('#gridConsulta')[0].setTitle('Cliente  : '+cliente+'<br>'+'N�mero N@E : '+num);
			}else{
				Ext.ComponentQuery.query('#gridConsulta')[0].setTitle('');
			}
			Ext.ComponentQuery.query('#gridConsulta')[0].show();	
			if(store.getTotalCount() > 0) {		
				//Ext.ComponentQuery.query('#gridConsulta')[0].show();	
			}else {
				Ext.ComponentQuery.query('#gridConsulta')[0].show();
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().getEl().mask('No se han registrados.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(false);		
			}
		}
	};
	var catalogoLineaCredito = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos1',
		proxy: {
			type: 'ajax',
			url: '15actInactUsuarioLC.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoLineaCredito'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
       clicksToMoveEditor: 1,
       autoCancel: false
   });
	var modificarEstatus = function (grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		var cveCliente =registro.get('NUM_ELECTRONICO');
		var estatus =registro.get('CS_HABILITADO');
		var observaciones =registro.get('CS_OBSERVACIONES');
		var gridConsulta =  Ext.ComponentQuery.query('#gridConsulta')[0];
		var sel_model  = gridConsulta.getView().getSelectionModel();
		if(observaciones==''){
			Ext.MessageBox.alert('Error de validaci�n','Para Activa/Inactivar el usuario se deben de capturar las observaciones.');
			return false;
		}
		Ext.Ajax.request({
			url: '15actInactUsuarioLC.data.jsp',
			params:{
				informacion: 'modificarEstatus',
				cveCliente: cveCliente,
				estatus: estatus,
				observaciones: observaciones
			},
			callback: procesarSuccessFailureModifica
		});
	}

	var elementosForma=[
		{
			xtype				: 'combo',
			itemId			: 'cmbLineaCredito',
			name				: 'comboLineaCredito',
			hiddenName 		:'comboLineaCredito', 
			fieldLabel		: 'L�nea Cr�dito',
			width				: 350,
			forceSelection	: false,
			allowBlank		: true,
			emptyText		:'Seleccionar...',
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			store				: catalogoLineaCredito
		}
	];
	
	var consultaData = Ext.create('Ext.data.Store', {
		model: 'LisRegistros',			
		proxy: {
			type: 'ajax',
			url: '15actInactUsuarioLC.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consultar'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsultar
			}
	});
	

	var gridConsulta = Ext.create('Ext.grid.Panel', {
		 itemId: 'gridConsulta',
		 title: '   ',
		 store:consultaData,
		 height: 300,
		 width: 940,
		 border: true,
		 hidden: true,
		 dyPadding: '12 6 12 6',
		 columns: [
			  {
					header: 'N�mero de Nafin Electr�nico', 
					tooltip: 'N�mero de Nafin Electr�nico',
					dataIndex: 'NUM_ELECTRONICO',
					field: 'textfield',
					sortable: true,
					width: 100,			
					resizable: true,				
					align: 'center'	
				},
			  {
					header: 'Nombre o R�zon Social', 
					tooltip: 'Nombre o R�zon Social',
					dataIndex: 'NOMBRE_RAZON_SOCIAL', 
					sortable: true,
					width: 150,			
					resizable: true,				
					align: 'left',
					renderer:function(value){
								return '<div align="LEFT">'+ (value)+'</div>';
					 }
			  },
			  {
					header: 'R.F.C', 
					tooltip: 'R.F.C',
					dataIndex: 'RFC',
					sortable: true,
					width: 100,			
					resizable: true,				
					align: 'left'	
				},
			  {
					header: 'N�m. SIRAC', 
					tooltip: 'N�m. SIRAC',
					dataIndex: 'NUM_SIRAC',
					sortable: true,
					width: 100,			
					resizable: true,				
					align: 'center'	
				},
			  {
					header: 'Domicilio',
					tooltip: 'Domicilio',
					dataIndex: 'DOMICILIO',
					sortable: true,
					width: 100,			
					resizable: true,				
					align: 'left'	
				},
			   {
					header: 'Estado', 
					tooltip: 'Estado',
					dataIndex: 'ESTADO',
					sortable: true,
					width: 100,			
					resizable: true,				
					align: 'left'	
				},
			   {
					header: 'Tel�fono',
					tooltip: 'Tel�fono',
					dataIndex: 'TELEFONO',
					sortable: true,
					width: 100,			
					resizable: true,				
					align: 'center'	
				},
			   {
					header: 'Estatus',
					dataIndex: 'CS_HABILITADO',
					width: 		100,
					editor: comboEstatus,
					align: 'center',
					renderer: Ext.util.Format.comboRenderer(comboEstatus)
					
				},
				{
					header: 'Observaciones',
					tooltip: 'Observaciones',
					dataIndex: 'CS_OBSERVACIONES',
					fixed:		true,
					sortable: true,
					hideable: true,
					width: 130,
					align: 'left',
					editor: {
						xtype: 'textarea',
						maxLength: 250
					},
					renderer: function(value, metadata, record, rowIndex, colIndex, store){            	
						return NE.util.colorCampoEdit(value,metadata,record);					
					}
				},
				{
         	xtype:  'actioncolumn',
            header: 'Acciones',
				tooltip:'Acciones',
            sortable: false,
            hideable: false,
            width: 70,
            align: 'center',           
            items:  [
					{
						getClass:   function(valor, metadata, registro, rowIndex, colIndex, store) {	
							return 'icoGuardar'
						},
						tooltip:  'Guardar',
						handler:  modificarEstatus
					}
            ] 
         }
				
		 ],
		 selType: 'cellmodel',
		 plugins: [
			  Ext.create('Ext.grid.plugin.CellEditing', {
					clicksToEdit: 1
			  })
		 ]
	});		

	var fp = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'forma',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: 'Consulta Clientes',
      width: 400,
      style: 'margin: 0px auto 0px auto;',
      hidden: false,
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 80
		},
		layout: 'anchor',
		items: elementosForma,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {	
					var  fp =   Ext.ComponentQuery.query('#forma')[0];
					fp.el.mask('Consultando...', 'x-mask-loading');
					consultaData.load({	
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15						
						})
					});						
				}		
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function(){	
					Ext.ComponentQuery.query('#gridConsulta')[0].hide();
					Ext.ComponentQuery.query('#cmbLineaCredito')[0].setValue();
				}
			}	
		]
	});
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 920,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [	
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta
		]
	});
	catalogoLineaCredito.load(); 
});