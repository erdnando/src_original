<%@ page import="java.sql.*, 
java.text.*, 
java.util.*, 
java.math.*, 
net.sf.json.JSONObject, 	
netropology.utilerias.*,
com.netro.descuento.*, 
com.netro.pdf.*, 
net.sf.json.JSONArray,  
net.sf.json.JSONObject,  			
com.netro.model.catalogos.*"		
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String sNombreMoneda = (request.getParameter("sNombreMoneda")==null)?"":request.getParameter("sNombreMoneda");
String sObservaciones = (request.getParameter("sObservaciones")==null)?"":request.getParameter("sObservaciones");
String sFirmaIF = (request.getParameter("sFirmaIF")==null)?"":request.getParameter("sFirmaIF");
String sFirmaNafin = (request.getParameter("sFirmaNafin")==null)?"":request.getParameter("sFirmaNafin");
String sPuestoIF = (request.getParameter("sPuestoIF")==null)?"":request.getParameter("sPuestoIF");
String sPuestoNafin = (request.getParameter("sPuestoNafin")==null)?"":request.getParameter("sPuestoNafin");
String sFechaEnvio = (request.getParameter("sFechaEnvio")==null)?"":request.getParameter("sFechaEnvio");
String sFechaCorte = (request.getParameter("sFechaCorte")==null)?"":request.getParameter("sFechaCorte");
String sClaveIF = (request.getParameter("sClaveIF")==null)?"":request.getParameter("sClaveIF");
String sClaveMoneda = (request.getParameter("sClaveMoneda")==null)?"":request.getParameter("sClaveMoneda");
String sNombreIF = (request.getParameter("sNombreIF")==null)?"":request.getParameter("sNombreIF");
String sNumeroSirac = (request.getParameter("sNumeroSirac")==null)?"":request.getParameter("sNumeroSirac");
//String countChecks = (request.getParameter("countChecks")==null)?"":request.getParameter("countChecks");		
String fcarga = (request.getParameter("fcarga")==null)?"":request.getParameter("fcarga");
String icusuario = (request.getParameter("icusuario")==null)?"":request.getParameter("icusuario");
String nusuario = (request.getParameter("nusuario")==null)?"":request.getParameter("nusuario");
String cvecertificado = "";
//String cvecertificado = (request.getParameter("sNumeroSirac")==null)?"":request.getParameter("cvecertificado").replace('_',' ');
String countChecks = (request.getParameter("countChecks")==null)?"":request.getParameter("countChecks");
String tipoLinea = (request.getParameter("tipoLinea")==null)?"":request.getParameter("tipoLinea");
String cs_tipo_B = (request.getParameter("cs_tipo_B")==null)?"":request.getParameter("cs_tipo_B");

String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");

JSONObject jsonObj = new JSONObject();

String query = "", infoRegresar ="";
CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer("");
String nombreArchivo = archivo.nombreArchivo()+".pdf";
int registros=0;
AccesoDB con = new AccesoDB();


try {
	con.conexionDB();
  ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);


if(operacion.equals("") ) { 
%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=strDirecVirtualCSS%>/css/<%=strClase%>">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="300" cellpadding="2" cellspacing="0" border="0" align="center">
<tr>
  <td class="formas">&nbsp;</td>
</tr>
<%
}
   ///////////////////////////////////////////////////////////////////////////////////////// FOR countChecks
	
    for(int x=0; x < Integer.parseInt(countChecks); x++){
    String[] claveIF      = sClaveIF.split(",");
        String clave_if = claveIF[x];
    String[] nombreIF     = sNombreIF.split(",");
        String nombre_if = nombreIF[x];
    String[] claveMoneda  = sClaveMoneda.split(",");
        String clave_moneda = claveMoneda[x];  
    String[] nombreMoneda = sNombreMoneda.split(",");
        String nombre_moneda = nombreMoneda[x];        
    String[] fechaCorte   = sFechaCorte.split(",");
        String fecha_corte = fechaCorte[x];   
    String[] fechaEnvio   = sFechaEnvio.split(",");
        String fecha_envio = fechaEnvio[x];    		  
    String[] numeroSirac  = sNumeroSirac.split(",");
        String numero_sirac = numeroSirac[x];        
	
	nombre_if = nombre_if.replace('_',' ');
  
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	String ig_subaplicacion, fg_saldo_insoluto, cs_tipo= "";
	float widths[] = {1, 2,1};
	pdfDoc.setTable(3, 100, widths);
	pdfDoc.setCellImage(strDirectorioPublicacion+"00archivos/15cadenas/15archcadenas/logos/nafinsa.gif",ComunesPDF.LEFT, 75);
	StringBuffer encabezado = new StringBuffer(); 
 
  encabezado.append("NACIONAL FINANCIERA, S.N.C.\n"); 
	encabezado.append("SUBDIRECCION DE OPERACIONES DE CREDITO\n");
	encabezado.append("RESUMEN DE LA CONCILIACI�N DE SALDOS AL ");
	
	encabezado.append( fecha_corte.substring(0,2) + " DE " );
  encabezado.append( meses[Integer.parseInt(fecha_corte.substring(3,5))-1].toUpperCase() + " DE " );
  encabezado.append( fecha_corte.substring(6,10)+"\n\n" ); 
	encabezado.append( "(" + numero_sirac + ") " +nombre_if +" - " + nombre_moneda);
	pdfDoc.setCell(encabezado.toString(), "formasrepB", ComunesPDF.CENTER,1,1,0);
	pdfDoc.setCell("", "formasrepB", ComunesPDF.CENTER,1,1,0);
	pdfDoc.addTable();
	
  //////////////////////////////////////////////////////////////////////////////
  
    
	 query = " select /*+ USE_NL(C) INDEX (C CP_COM_CEDULA_CONCILIA_PK) */ "   +
				   "  C.CG_TIPO_CREDITO AS tipo_credito, "   +
				   "  C.FG_MONTO_OPERADO AS monto_operado, "   +
				   "  C.FG_CAPITAL_VIGENTE AS capital_vigente, "   +
				   "  C.FG_CAPITAL_VENCIDO AS capital_vencido, "   +
				   "  C.FG_INTERES_VIGENTE AS interes_vigente, "   +
				   "  C.FG_INTERES_VENCIDO AS interes_vencido, "   +
				   "  C.FG_INTERES_MORA AS interes_mora, "   +
				   "  C.FG_TOTAL_ADEUDO AS total_adeudo, "   +
				   "  C.IG_DESCUENTOS AS descuentos, "   +
				   "  C.CG_OBSERVACIONES AS observaciones, "   +
				   "  C.IC_FIRMA_CEDULA_NAF AS firma_cedula_naf, "   +
				   "  C.IC_FIRMA_CEDULA AS firma_cedula, "   +
				   "  C.IG_SUBAPLICACION AS subaplicacion, "   +
				   "  C.FG_SALDO_INSOLUTO AS saldo_insoluto, "   +
           "  to_char(df_carga,'dd/mm/yyyy hh24:mi:ss') AS fecha_carga, "+ 
           "  IC_USUARIO AS usuario, "+  
				   "  CG_NOMBRE_USUARIO AS nombre_usuario, "+  
				   "  IG_CLAVE_CERTIFICADO AS clave_certificado, ";
			if(!"CE".equals(cs_tipo_B)){
			   query +="  IF.CS_TIPO AS tipo "+ 
				   "  FROM com_cedula_concilia C, comcat_if IF"  +
				   "  WHERE C.IC_IF=IF.IC_IF"+ 
				   "  AND C.IC_IF=?"   ;
			}else{
				query +="  'B' AS tipo "+ 
				   "  FROM com_cedula_concilia C, comcat_cli_externo IF"  +
				   "  WHERE C.IC_CLIENTE_EXTERNO=IF.IC_NAFIN_ELECTRONICO"+ 
				   "  AND C.IC_CLIENTE_EXTERNO=?"   ;
			}
			   query +="  AND C.IC_MONEDA=?"   +
				   "  AND TRUNC(C.DF_FECHA_CORTE) = TO_DATE(?,'DD/MM/YYYY')	  "   +
				   "  AND TRUNC(C.DF_CARGA) = TO_DATE(?,'DD/MM/YYYY')	" +
				   "  AND C.CG_TIPO_LINEA = ?"  ;
	
	PreparedStatement ps = con.queryPrecompilado(query);
	ps.setInt(1, Integer.parseInt(clave_if));
	ps.setInt(2, Integer.parseInt(clave_moneda));
	ps.setString(3, fecha_corte);
	ps.setString(4, fecha_envio);
	ps.setString(5, tipoLinea);
  
  System.out.println("query:"+query+"<br>");
  System.out.println("Condiciones: "+clave_if+","+clave_moneda+","+fecha_corte+","+fecha_envio);
	ResultSet rs = ps.executeQuery();

    String sTipoCredito_C="", sMontoOperado_C="", sCapitalVigente_C="", sCapitalVencido_C="", sInteresVigente_C="", sInteresVencido_C="", sMoras_C="", sTotalAdeudo_C="", sDescuentos_C="", sObservaciones_C="", sFirmaNafin_C="", sFirmaIF_C="";

	BigDecimal dTotal_MontoOperado = new BigDecimal(0), dTotal_CapitalVigente = new BigDecimal(0), dTotal_CapitalVencido = new BigDecimal(0);
	BigDecimal dTotal_InteresVigente = new BigDecimal(0), dTotal_InteresVencido = new BigDecimal(0), 
				dTotal_Moras = new BigDecimal(0), dTotal_TotalAdeudo = new BigDecimal(0), dTotal_Descuentos = new BigDecimal(0);
	BigDecimal dTotal_saldoInsolto = new BigDecimal(0);			
	while (rs.next()) {
		
        sTipoCredito_C     = (rs.getString("tipo_credito") == null) ? "" : rs.getString("tipo_credito").trim();
        sMontoOperado_C    = (rs.getString("monto_operado") == null) ? "" : rs.getString("monto_operado").trim();
        sCapitalVigente_C  = (rs.getString("capital_vigente") == null) ? "" : rs.getString("capital_vigente").trim();
        sCapitalVencido_C  = (rs.getString("capital_vencido") == null) ? "" : rs.getString("capital_vencido").trim();
        sInteresVigente_C  = (rs.getString("interes_vigente") == null) ? "" : rs.getString("interes_vigente").trim();
        sInteresVencido_C  = (rs.getString("interes_vencido") == null) ? "" : rs.getString("interes_vencido").trim();
        sMoras_C           = (rs.getString("interes_mora") == null) ? "" : rs.getString("interes_mora").trim();
        sTotalAdeudo_C     = (rs.getString("total_adeudo") == null) ? "" : rs.getString("total_adeudo").trim();
        sDescuentos_C      = (rs.getString("descuentos") == null) ? "" : rs.getString("descuentos").trim();
        sObservaciones_C   = (rs.getString("observaciones") == null) ? "" : rs.getString("observaciones").trim();
        sFirmaNafin_C      = (rs.getString("firma_cedula_naf") == null) ? "" : rs.getString("firma_cedula_naf").trim();
        sFirmaIF_C         = (rs.getString("firma_cedula") == null) ? "" : rs.getString("firma_cedula").trim();
        ig_subaplicacion   = (rs.getString("subaplicacion") == null) ? "" : rs.getString("subaplicacion").trim();
        fg_saldo_insoluto  = (rs.getString("saldo_insoluto") == null) ? "" : rs.getString("saldo_insoluto").trim();
        cs_tipo			       = (rs.getString("tipo") == null) ? "" : rs.getString("tipo").trim();
        fcarga             = (rs.getString("fecha_carga") == null) ? "" : rs.getString("fecha_carga").trim();
        icusuario          = (rs.getString("usuario") == null) ? "" : rs.getString("usuario").trim();
        nusuario           = (rs.getString("nombre_usuario") == null) ? "" : rs.getString("nombre_usuario").trim();
        cvecertificado     = (rs.getString("clave_certificado") == null) ? "" : rs.getString("clave_certificado").trim();
		
		//Encabezados
		
			int numCols = 9;
			float widths2[] = {.8f, 2, 1, 1, 1, 1, 1, 1, 1, .7f};
			float widths3[] = {2, 1, 1, 1, 1, 1, 1, 1, .7f};
		
			if("B".equals(cs_tipo)){
				numCols++;
				pdfDoc.setTable(numCols, 100, widths2);
			} 
      if("NB".equals(cs_tipo)){
				pdfDoc.setTable(numCols, 100, widths3);
			}

			if(cs_tipo.equals("B")){
				pdfDoc.setCell("Sub aplicaci�n", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Concepto Sub aplicaci�n", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Saldo insoluto Nafin", "formasmenB", ComunesPDF.CENTER);
			}
      if(cs_tipo.equals("NB")){
				pdfDoc.setCell("Tipo de Cr�dito", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Operado", "formasmenB", ComunesPDF.CENTER);
			}
			pdfDoc.setCell("Capital Vigente", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Capital Vencido", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Inter�s Vigente", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Inter�s Vencido", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Moras", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Total Adeudo", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Descuentos", "formasmenB", ComunesPDF.CENTER);
		
		
		
		//Desplegados	
		//pdfDoc.setCell(sTipoCredito_C, "formasmen", ComunesPDF.CENTER);
		if(cs_tipo.equals("B")){
			pdfDoc.setCell(ig_subaplicacion, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(sTipoCredito_C, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMoneda2(fg_saldo_insoluto, true), "formasmen", ComunesPDF.RIGHT);
		}
    if(cs_tipo.equals("NB")){
			pdfDoc.setCell(sTipoCredito_C, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMoneda2(sMontoOperado_C, true), "formasmen", ComunesPDF.RIGHT);
		}
		pdfDoc.setCell(Comunes.formatoMoneda2(sCapitalVigente_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(sCapitalVencido_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(sInteresVigente_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(sInteresVencido_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(sMoras_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(sTotalAdeudo_C, true), "formasmen", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoDecimal(sDescuentos_C, 0), "formasmen", ComunesPDF.RIGHT);
		
		dTotal_MontoOperado   = dTotal_MontoOperado.add(new BigDecimal(sMontoOperado_C));
		dTotal_CapitalVigente = dTotal_CapitalVigente.add(new BigDecimal(sCapitalVigente_C));
		dTotal_CapitalVencido = dTotal_CapitalVencido.add(new BigDecimal(sCapitalVencido_C));
		dTotal_InteresVigente = dTotal_InteresVigente.add(new BigDecimal(sInteresVigente_C));
		dTotal_InteresVencido = dTotal_InteresVencido.add(new BigDecimal(sInteresVencido_C));
		dTotal_Moras          = dTotal_Moras.add(new BigDecimal(sMoras_C));
		dTotal_TotalAdeudo    = dTotal_TotalAdeudo.add(new BigDecimal(sTotalAdeudo_C));
		dTotal_Descuentos     = dTotal_Descuentos.add(new BigDecimal(sDescuentos_C));
		dTotal_saldoInsolto   = dTotal_saldoInsolto.add(new BigDecimal(fg_saldo_insoluto));
		registros++;
	}
	ps.close();

	ArrayList alTotales = (ArrayList)session.getAttribute("Totales");
	pdfDoc.setCell("TOTAL", "formasmenB", ComunesPDF.CENTER);
	if(cs_tipo.equals("B")){
		pdfDoc.setCell("", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_saldoInsolto.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	}
  if(cs_tipo.equals("NB")){
		pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_MontoOperado.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	}
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_CapitalVigente.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_CapitalVencido.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_InteresVigente.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_InteresVencido.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_Moras.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMoneda2(dTotal_TotalAdeudo.toString(), true), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoDecimal(dTotal_Descuentos.toString(), 0), "formasmenB", ComunesPDF.RIGHT);
	pdfDoc.addTable();

 
   
	
	String sNota = new String("");
	query = "select cg_observaciones from com_observaciones_cedula "+
				"where trunc(df_fecha_corte) = to_date( ?, 'dd/mm/yyyy') "+
				"and ic_if = ? "+
				"and ic_moneda = ?";
	ps = con.queryPrecompilado(query);
	ps.setString(1,fecha_corte);
	ps.setInt(2, Integer.parseInt(clave_if));
	ps.setInt(3, Integer.parseInt(clave_moneda));
	rs = ps.executeQuery();
	if(rs.next())
		sNota = rs.getString("cg_observaciones");
	rs.close();
	if(ps!=null) ps.close();
	
	pdfDoc.addText("\nNOTA: "+ sNota, "formasB", ComunesPDF.LEFT);	
	
	pdfDoc.addText("Observaciones:  " + sObservaciones, "formasB", ComunesPDF.LEFT);
	
	pdfDoc.addText("\nConfirmo que los saldos operativos de Nacional Financiera, S.N.C. han sido conciliados con los registros contables de", "formas", ComunesPDF.CENTER);
	pdfDoc.addText(nombre_if, "formasB", ComunesPDF.CENTER);
	
	pdfDoc.setTable(3, 85);
	
	pdfDoc.setCell("\nUsuario: "+icusuario+"\nNombre: "+nusuario+"\nNo. serie certificado: "+cvecertificado+"\nFecha/Hora: "+fcarga, "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCellImage(strDirectorioPublicacion + "00utils/gif/firma_pagos_cartera.gif", ComunesPDF.CENTER,100);

	pdfDoc.setCell("_____________________________\n"+sFirmaIF, "formasB", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("_____________________________\n"+sFirmaNafin, "formasB", ComunesPDF.CENTER, 1, 1, 0);
	
	pdfDoc.setCell(sPuestoIF, "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell(sPuestoNafin, "formas", ComunesPDF.CENTER, 1, 1, 0);
	
	pdfDoc.setCell(nombre_if, "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.setCell("NACIONAL FINANCIERA S.N.C.", "formas", ComunesPDF.CENTER, 1, 1, 0);
	pdfDoc.addTable();

	float tWid[] = {1.5f, 1};
	
	pdfDoc.setTable(2, 100, tWid);

	pdfDoc.setCell("Nota: Si en un plazo de 10 d�as h�biles contados a partir de la recepci�n del presente Estado de Cuenta no se reciben las observaciones correspondientes se dar�n por aceptadas las cifras.", "formas", ComunesPDF.LEFT, 1, 1, 0);
	pdfDoc.setCell("Fecha de Env�o:  "+fecha_envio, "formas", ComunesPDF.RIGHT, 1, 1, 0);
	pdfDoc.addTable();
  pdfDoc.newPage();
	} /////////////////////////////////////////////////////////////////////////////// FOR countChecks END
	pdfDoc.endDocument();
if(operacion.equals("") ) {
%> 

<tr>
	<td class="formas" align="center"><br>
		<table cellpadding="3" cellspacing="1">
			<tr>
				<td class="celda02" width="100" align="center" height="30">
					<a href="<%=strDirecVirtualTemp%><%=nombreArchivo%>">Bajar Archivo</a>
				</td>
				<td class="celda02" width="100" align="center">
					<a href="javascript:close();">Cerrar</a>
				</td>
			</tr>
		</table>
	</td>
</tr>

<% 
}
} catch(Exception e) {
	e.printStackTrace();	%>
	<tr>
		<td class="formas" align="center"><br>
			<table width="600" border="1" cellspacing="0" cellpadding="2" bordercolor="#A5B8BF">
				<tr>
					<td class="celda01" align="center" bgcolor="silver">Hubo problemas al generar el archivo.</td>
				</tr>
			</table>
		</td>
	</tr>
</body>
</html>
<%
} finally { 
	if(con.hayConexionAbierta()) con.cierraConexionDB();
}

%>

<%
if(operacion.equals("VersionNueva") ) {
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString(); 
	
}
%>
<%=infoRegresar%>

