Ext.onReady(function() {
	Ext.override(Ext.form.Field, {
		setFieldLabel : function(text) {
		if (this.rendered) {
		  this.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(text);
		}
		this.fieldLabel = text;
		}
	});
//------------------------------------------------------------------------------
	var pantallaTitulo='C�dula de Consiliaci�n';
	var resultados = null;
	var sClaveIFMONEDA = "";
	var countChecks= 0;
	var recordType = Ext.data.Record.create([
		{name:'CLAVE'},
		{name:'DESCRIPCION'}
	]);
	
	var numSirac = '';
	function variablePDF() {
		sClaveIFMONEDA="";
		countChecks= 0;
	}
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	var procesarSuccessDatosIniciales = function(opts, success, response) {
		var fpDato = Ext.getCmp('formaDatos');
		fpDato.hide();
	}
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		var fpDato = Ext.getCmp('formaDatos');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				if (fpDato.isVisible()) {
					fpDato.hide();
				}	
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {					
				el.unmask();
				Ext.getCmp('btnArchivoPDF').enable();
			} else {		
				Ext.getCmp('btnArchivoPDF').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var procesarConsultaTotales = function(store, arrRegistros, opts) 	{
		
		//var tipoBanB = Ext.getCmp('cs_tipo_B').getValue();
		//var tipoBanNB = Ext.getCmp('cs_tipo_NB').getValue();
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();	
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				
				gridTotales.show();
			}								
			if(store.getTotalCount() > 0) {	
				var cm = gridTotales.getColumnModel();
				if(Ext.getCmp('cmbTipoAfiliado').getValue()=='B' || Ext.getCmp('cmbTipoAfiliado').getValue()=='CE'){
					gridTotales.getColumnModel().setHidden(1,false);
				}else if(Ext.getCmp('cmbTipoAfiliado').getValue()=='NB'){
					gridTotales.getColumnModel().setHidden(0,false);
				
				}
				
			}
		}
	}
	function descargaArchivo(opts, success, response) {   
		Ext.getCmp('btnArchivoPDF').enable();
		var btnImprimirPDF = Ext.getCmp('btnArchivoPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		Ext.getCmp('btnImprimirPantalla').enable();
		var btnImprimirPDF = Ext.getCmp('btnImprimirPantalla');
		btnImprimirPDF.setIconClass('icoPdf');
		Ext.getCmp('btnImprimirPDF').enable();
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaDataG = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaG = Ext.getCmp('gridConsultaG');	
		var el = gridConsultaG.getGridEl();
	  var gridTotales = Ext.getCmp('gridTotales');
		var el = gridTotales.getGridEl();
		var gridConsulta = Ext.getCmp('gridConsulta');
		var fpDato = Ext.getCmp('formaDatos');
		if (arrRegistros != null) {
			if (!fpDato.isVisible() ) {
					if (gridConsulta.isVisible() ) {
						gridConsulta.hide();
					}
					fpDato.show();
					fp.hide();
		}
		if(store.getTotalCount() > 0) {		
			var jsonData = store.reader.jsonData;
			var  tipoBusqueda = jsonData.tipoBusqueda;
			var  dTotal_MontoOperado = jsonData.dTotal_MontoOperado;
			var  dTotal_CapitalVigente = jsonData.dTotal_CapitalVigente;
			var  dTotal_CapitalVencido = jsonData.dTotal_CapitalVencido;
			var  dTotal_InteresVigente = jsonData.dTotal_InteresVigente;
			var  dTotal_InteresVencido = jsonData.dTotal_InteresVencido;
			var  dTotal_Moras = jsonData.dTotal_Moras;
			var  dTotal_TotalAdeudo = jsonData.dTotal_TotalAdeudo;
			var  dTotal_Descuentos = jsonData.dTotal_Descuentos;
			var  dSaldoInsoluto = jsonData.dSaldoInsoluto;
			var  leyendaEncabezado = jsonData.leyendaEncabezado;
			var  firmas = jsonData.firmas;
			Ext.getCmp("gridConsultaG").setTitle(jsonData.leyendaEncabezado);			
		  Ext.getCmp('lblFirmas').update(jsonData.firmas);
			Ext.getCmp('sNombreMoneda').setValue(jsonData.sNombreMoneda);
			Ext.getCmp('sObservaciones').setValue(jsonData.sObservaciones);
			Ext.getCmp('sFirmaIF').setValue(jsonData.sFirmaIF);
			Ext.getCmp('sFirmaNafin').setValue(jsonData.sFirmaNafin);
			Ext.getCmp('sPuestoIF').setValue(jsonData.sPuestoIF);
			Ext.getCmp('sPuestoNafin').setValue(jsonData.sPuestoNafin);
			Ext.getCmp('sFechaEnvio').setValue(jsonData.sFechaEnvio);
			Ext.getCmp('sFechaCorte').setValue(jsonData.sFechaCorte);
			Ext.getCmp('sClaveIF').setValue(jsonData.sClaveIF);
			Ext.getCmp('sClaveMoneda').setValue(jsonData.sClaveMoneda);
			Ext.getCmp('sNombreIF').setValue(jsonData.sNombreIF);
			Ext.getCmp('sNumeroSirac').setValue(jsonData.sNumeroSirac);
			var resultados = null;
			var cm = gridConsultaG.getColumnModel();
			if(tipoBusqueda=='NB'){
				gridConsultaG.getColumnModel().setHidden(cm.findColumnIndex('STIPO_CREDITO'), jsonData.STIPO_CREDITO);
				gridConsultaG.getColumnModel().setHidden(cm.findColumnIndex('SMONTO_OPERADO'), jsonData.SMONTO_OPERADO);
				 resultados=[
					[dTotal_MontoOperado,'',dTotal_CapitalVigente,dTotal_CapitalVencido,dTotal_InteresVigente,dTotal_InteresVencido,dTotal_Moras,dTotal_TotalAdeudo,dTotal_Descuentos]
				];
			}else{
				gridConsultaG.getColumnModel().setHidden(cm.findColumnIndex('STIPO_CREDITO1'), jsonData.STIPO_CREDITO1);
				gridConsultaG.getColumnModel().setHidden(cm.findColumnIndex('SCLAVE_SUB_APLIC'), jsonData.SCLAVE_SUB_APLIC);
				gridConsultaG.getColumnModel().setHidden(cm.findColumnIndex('SSALDO_INSOLUTO'), jsonData.SSALDO_INSOLUTO);
				 resultados=[
					['',dSaldoInsoluto,dTotal_CapitalVigente,dTotal_CapitalVencido,dTotal_InteresVigente,dTotal_InteresVencido,dTotal_Moras,dTotal_TotalAdeudo,dTotal_Descuentos]
				];
			}	
				consultaTotalesData.loadData(resultados);
				gridTotales.show();
		} else {
				var btnRegresar = Ext.getCmp('btnCerra');
				btnRegresar.show();
				Ext.getCmp('numeroDatos').show();
				 Ext.getCmp('lblFirmas').hide();
				Ext.getCmp('btnImprimirPantalla').hide();
				Ext.getCmp('btnImprimirPDF').hide();	
				Ext.getCmp('btnCerrar').hide();
				Ext.getCmp('gridConsultaG').hide();
				Ext.getCmp('gridTotales').hide();
				Ext.getCmp('sObservaciones').reset();
				Ext.getCmp('sFirmaIF').reset();
				Ext.getCmp('sFirmaIF').reset();
				Ext.getCmp('sFirmaNafin').reset();
				Ext.getCmp('sPuestoIF').reset();
				Ext.getCmp('sPuestoNafin').reset();
				Ext.getCmp('sFechaEnvio').reset();
				Ext.getCmp('sFechaCorte').reset();;
				Ext.getCmp('sClaveIF').reset();
				Ext.getCmp('sClaveMoneda').reset();;
				Ext.getCmp('sNombreIF').reset();
				Ext.getCmp('sNumeroSirac').reset();
			}
		}
	}
	var procesarCatIntermediarioData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cveIntermediario = Ext.getCmp('cmbIntermediario');
			if(cveIntermediario.getValue()==''){
				var newRecord = new recordType({
					CLAVE:'T',
					DESCRIPCION:'TODOS'
				});
				store.insert(0,newRecord);
				store.commitChanges();
				cveIntermediario.setValue('');
			}
		}
  }
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------
	var rt = Ext.data.Record.create([
		{	name: 'dTotal_MontoOperado'},
		{	name: 'dSaldoInsoluto'},
		{	name: 'dTotal_CapitalVigente'},
		{	name: 'dTotal_CapitalVencido'},
		{	name: 'dTotal_InteresVigente'},
		{	name: 'dTotal_InteresVencido'},
		{	name: 'dTotal_Moras'},
		{	name: 'dTotal_TotalAdeudo'},
		{	name: 'dTotal_Descuentos'}
	]);
	var consultaTotalesData= new Ext.data.Store({
		reader: new Ext.data.ArrayReader(
			{
				idIndex: 1  // id for each record will be the first element
			},
			  rt // recordType				
		 ),
		listeners: {
			load: procesarConsultaTotales,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				procesarConsultaTotales(null, null, null);
				}
			}
		}		
	});
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15cedulaconcifExt01.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		fields: [	
			{	name: 'CG_RAZON_SOCIAL'},
			{	name: 'NOMBRECLIENTE'},
			{	name: 'CD_NOMBRE'},	
			{	name: 'IC_IF'},	
			{	name: 'IC_MONEDA'},
			{	name: 'IG_CLIENTE'},
			{	name: 'IC_NAFIN_ELECTRONICO'},
			{	name: 'SELECCIONAR'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});
	var consultaDataG = new Ext.data.JsonStore({
		root : 'registros',
		url : '15cedulaconcifExt01.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		fields: [	
			{	name: 'CG_RAZON_SOCIAL'},	
			{	name: 'TIPO_CREDITO'},	
			{	name: 'MONTO_OPERADO'},	
			{	name: 'CAPITAL_VIGENTE'},	
			{	name: 'CAPITAL_VENCIDO'},	
			{	name: 'INTERES_VIGENTE'},	
			{	name: 'INTERES_VENCIDO'},	
			{	name: 'MORAS'},	
			{	name: 'TOTAL_ADEUDO'},	
			{	name: 'DESCUENTOS'},	
			{	name: 'SUBAPLIC'},	
			{	name: 'SALDO_INSOLUTO'},	
			{	name: 'IC_FINANCIERA'},	
			{	name: 'STIPO_CREDITO'},
			{	name: 'STIPO_CREDITO1'},
			{	name: 'SMONTO_OPERADO'},
			{	name: 'SCAPITAL_VIGENTE'},	
			{	name: 'SCAPITAL_VENCIDO'},	
			{	name: 'SINTERES_VIGENTE'},	
			{	name: 'SINTERES_VENCIDO'},	
			{	name: 'SMORAS'},	
			{	name: 'STOTAL_ADEUDO'},	
			{	name: 'SDESCUENTOS'},	
			{	name: 'SCLAVE_SUB_APLIC'},	
			{	name: 'SSALDO_INSOLUTO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataG,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataG(null, null, null);					
				}
			}
		}					
	});
  
  var catTipoLinea = new Ext.data.ArrayStore({
		id: 'catTipoLinea',
		fields : ['CLAVE', 'DESCRIPCION'],
    data:[['D','Linea Descuento'],['C', 'Linea Cr�dito']]
	});
  
  var catTipoAfiliado = new Ext.data.ArrayStore({
		id: 'catTipoAfiliado',
		fields : ['CLAVE', 'DESCRIPCION']
	});
  
	var catIntermediario = new Ext.data.JsonStore({
		id: 'catIntermediario',
		root : 'registros',
		fields : ['CLAVE', 'DESCRIPCION', 'loadMsg'],
		url : '15cedulaconcifExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoIntermediario'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatIntermediarioData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15cedulaconcifExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'  ///hacer el metodo en el data
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoFechaCorte = new Ext.data.JsonStore({
		id: 'catalogoFechaCorte',
		root : 'registros',
		fields : ['CLAVE', 'DESCRIPCION','loadMsg'],
		url : '15cedulaconcifExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoFechaCorte'  ///hacer el metodo en el data
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	function procesoImprimirRegistros() {
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();		
		variablePDF();
		store.each(function(record) {
			if(record.data['SELECCIONAR']=='S')  {
				if(Ext.getCmp('cmbTipoLinea').getValue()!='C'){
					sClaveIFMONEDA +=record.data['IC_IF']+"|"+record.data['IC_MONEDA']+",";
				}else{
					if(Ext.getCmp('cmbTipoAfiliado').getValue()!='CE'){
						sClaveIFMONEDA +=record.data['IG_CLIENTE']+"|"+record.data['IC_MONEDA']+",";
					}else{
						sClaveIFMONEDA +=record.data['IG_CLIENTE']+"|"+record.data['IC_MONEDA']+",";
					}
				}
				countChecks++;
			}
		});
		if(countChecks==0) {
			Ext.MessageBox.alert('Aviso','Debe selecionar por lo menos un registro para generar el PDF');		
		}else  {
			Ext.getCmp("btnArchivoPDF").disable();
			Ext.getCmp("btnArchivoPDF").setIconClass('loading-indicator');
			Ext.Ajax.request({
				url: '15cedulaconcifTodosPDF.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					sFechaCorte : Ext.getCmp('cmbFechaCorte').getValue(),
					sClaveMoneda : Ext.getCmp('cmbMoneda').getValue(),
					sChkIfMoneda : sClaveIFMONEDA,
					operacion: 'VersionNueva'					
				}),							
				callback: descargaArchivo
			});
		}
	}
	var procesarVerCuentas= function(grid, rowIndex, colIndex, item, event) {
		var  registro = grid.getStore().getAt(rowIndex);
		var sClaveIF=registro.get('IC_IF');  
		var sNombreIF=registro.get('CG_RAZON_SOCIAL');  
		var sClaveMoneda=registro.get('IC_MONEDA');  
		var sNombreMoneda=registro.get('CD_NOMBRE');
		var sClaveCliExt=registro.get('IC_NAFIN_ELECTRONICO');
		var sNumSirac=registro.get('IG_CLIENTE');
		
		Ext.Ajax.request({
			url: '15cedulaconcifTpdf.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				sFechaCorte :Ext.getCmp('cmbFechaCorte').getValue(),
				sClaveMoneda : sClaveMoneda,
				sClaveIF : sClaveIF,
				sNombreMoneda : sNombreMoneda,
				sNombreIF : sNombreIF,
				sClaveCliExt : sClaveCliExt,
				sNumSirac : sNumSirac,
				operacion: 'VersionNueva'					
			}),							
			callback: descargaArchivo
		});
	}
//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
	var selectModel = new Ext.grid.CheckboxSelectionModel( {
		//checkOnly: true,		
	  listeners: {
		rowdeselect: function(selectModel, rowIndex, record) {	// cuando se quita la selecci�n
			record.data['SELECCIONAR']='N';
			record.commit();				
		},
		beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //Se activa cuando una fila est� seleccionada, return false para cancelar.		
			record.data['SELECCIONAR']='S';	
			record.commit();
			}		
		}
	});
	var gridConsulta = new Ext.grid.GridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',	
		style: 'margin:0 auto;',
		title: 'consulta',
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,
				width: 300,			
				resizable: true,				
				align: 'center'		
			},
			{
				header: 'Cliente Externo',
				tooltip: 'Cliente Externo',
				dataIndex: 'NOMBRECLIENTE',
				sortable: true,
				width: 300,			
				resizable: true,
				align: 'center',
				hidden: true
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'CD_NOMBRE',
				sortable: true,
				width: 300,			
				resizable: true,				
				align: 'center'
			},
			{
				xtype		: 'actioncolumn',
				header: 'Generar PDF',
				dataIndex:	'',
				tooltip: 'Generar PDF ',				
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}
						,handler: procesarVerCuentas
					}
				]
			},
			selectModel
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 780,
		align: 'center',
		frame: false,
		sm:selectModel,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: procesoImprimirRegistros
				}
			]
		}
	});
	var gridConsultaG = new Ext.grid.GridPanel({	
		title: '',
		id: 'gridConsultaG',
		store: consultaDataG,
		style: 'margin:0 auto;',
		stripeRows : true,
		loadMask: true,
		hidden: false,
		frame: true,
		header	  : true,
		columns: [
			{
				header: 'Tipo de cr�dito',
				tooltip: 'Tipo de cr�dito',
				dataIndex: 'STIPO_CREDITO',
				sortable: true,
				width: 100,			
				resizable: true,
				hidden: true,
				align: 'center'		
			},
			{
				header: 'Monto Operado',
				tooltip: 'Monto Operado',
				dataIndex: 'SMONTO_OPERADO',
				sortable: true,
				width: 100,	
				hidden: true,
				resizable: true,				
				align: 'left'
			},
			{
				header: 'Sub aplicaci�n',
				tooltip: 'Sub aplicaci�n',
				dataIndex: 'SCLAVE_SUB_APLIC',
				sortable: true,
				width: 100,	
				hidden: true,
				resizable: true,				
				align: 'center'		
			},
			{
				header: 'Concepto Sub aplicaci�n',
				tooltip: 'Concepto Sub aplicaci�n',
				dataIndex: 'STIPO_CREDITO1',
				sortable: true,
				width: 100,		
				hidden: true,
				resizable: true,				
				align: 'left'
			},
			{
				header: 'Saldo insoluto Nafin',
				tooltip: 'Saldo insoluto Nafin',
				dataIndex: 'SSALDO_INSOLUTO',
				sortable: true,
				width: 100,	
				hidden: true,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Capital Vigente',
				tooltip: 'Capital Vigente',
				dataIndex: 'CAPITAL_VIGENTE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Capital Vencido',
				tooltip: 'Capital Vencido',
				dataIndex: 'CAPITAL_VENCIDO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Inter�s Vigente',
				tooltip: 'Inter�s Vigente',
				dataIndex: 'INTERES_VIGENTE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Inter�s Vencido',
				tooltip: 'Inter�s Vencido',
				dataIndex: 'INTERES_VENCIDO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Moras',
				tooltip: 'Moras',
				dataIndex: 'MORAS',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Adeudo',
				tooltip: 'Total Adeudo',
				dataIndex: 'TOTAL_ADEUDO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Descuentos',
				tooltip: 'Descuentos',
				dataIndex: 'DESCUENTOS',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			}
		]
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'TOTAL',
		clicksToEdit: 1,
		width: 767,
		height: 100,
		stripeRows : true,
		loadMask: true,
		hidden: false,
		frame: true,
		columns: [
			{
				header: 'Monto Operado',
				tooltip: 'Monto Operado',
				dataIndex: 'dTotal_MontoOperado',
				sortable: true,
				width: 100,	
				resizable: true,
				hidden: true,
				align: 'left',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Saldo insoluto Nafin',
				tooltip: 'Saldo insoluto Nafin',
				dataIndex: 'dSaldoInsoluto',
				sortable: true,
				width: 150,			
				resizable: true,
				hidden: true,
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Capital Vigente',
				tooltip: 'Capital Vigente',
				dataIndex: 'dTotal_CapitalVigente',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Capital Vencido',
				tooltip: 'Capital Vencido',
				dataIndex: 'dTotal_CapitalVencido',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Inter�s Vigente',
				tooltip: 'Inter�s Vigente',
				dataIndex: 'dTotal_InteresVigente',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Inter�s Vencido',
				tooltip: 'Inter�s Vencido',
				dataIndex: 'dTotal_InteresVencido',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Moras',
				tooltip: 'Moras',
				dataIndex: 'dTotal_Moras',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Total Adeudo',
				tooltip: 'Total Adeudo',
				dataIndex: 'dTotal_TotalAdeudo',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return '<div align=right>'+Ext.util.Format.usMoney(value)+'</div>';
				} 
			},
			{
				header: 'Descuento',
				tooltip: 'Descuento',
				dataIndex: 'dTotal_Descuentos',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			}		
		]
	});
	var elementosForma=[
		
    {
			xtype				: 'combo',
			id					: 'cmbTipoLinea',
			name				: 'tipoLinea',
			hiddenName 		:'tipoLinea', 
			fieldLabel		: 'Tipo de Linea',
			mode				: 'local',
			autoLoad: false,
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			emptyText: 'Seleccione Tipo de Linea',
			forceSelection	: true,
			triggerAction	: 'all',
			typeAhead: true,
			width			: 250,
			minChars : 1,
			allowBlank: true,
			store				: catTipoLinea,
			listeners: {
				select: {
					fn: function(combo) {
						var fpDato = Ext.getCmp('formaDatos');
						fpDato.hide();
						Ext.getCmp('gridConsulta').hide();
						Ext.getCmp('cmbMoneda').reset();
						Ext.getCmp('cmbFechaCorte').reset();
            Ext.getCmp('cmbIntermediario').reset();
            Ext.getCmp('cmbTipoAfiliado').reset();
            
						if (fpDato.isVisible()) {
							fpDato.hide();
							Ext.getCmp('cmbMoneda').reset();
							Ext.getCmp('cmbFechaCorte').reset();
						}
            
            if(combo.getValue()=='D'){
              catTipoAfiliado.loadData([['B','Intermediario Financiero'],['NB','Intermediario Financiero No Bancario']]);
            }else if(combo.getValue()=='C'){
              catTipoAfiliado.loadData([['B','Intermediario Financiero'],['NB','Intermediario Financiero No Bancario'],['CE','Cliente Externo']]);
            }
						
					}
				}
			}
		},
    {
			xtype				: 'combo',
			id					: 'cmbTipoAfiliado',
			name				: 'cs_tipo',
			hiddenName 		:'cs_tipo', 
			fieldLabel		: 'Afiliado',
			mode				: 'local',
			autoLoad: false,
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			emptyText: 'Seleccione Tipo de Afiliado',
			forceSelection	: true,
			triggerAction	: 'all',
			typeAhead: true,
			width			: 250,
			minChars : 1,
			allowBlank: true,
			store				: catTipoAfiliado,
			listeners: {
				select: {
					fn: function(combo) {
						Ext.getCmp('cmbIntermediario').setValue('');
						if(combo.getValue()=='CE'){
							Ext.getCmp('cmbIntermediario').setFieldLabel('Cliente Externo');
							catIntermediario.load({
								params : Ext.apply({
									cs_tipo : combo.getValue()
								})
						  });	
						}else{
							Ext.getCmp('cmbIntermediario').setFieldLabel('Intermediario');
							catIntermediario.load({
								params : Ext.apply({
									cs_tipo : combo.getValue(),
									tipoLinea: Ext.getCmp('cmbTipoLinea').getValue()
								})
							});	
						}
					}
				}
			}
		},
		{
			xtype				: 'combo',
			id					: 'cmbIntermediario',
			name				: 'comboIntermediario',
			hiddenName 		:'comboIntermediario', 
			fieldLabel		: 'Intermediario',
			mode				: 'local',
			autoLoad: false,
			valueField		: 'CLAVE',
			displayField	:'DESCRIPCION',
			emptyText: 'Seleccione Tipo de Intermediario',
			forceSelection	: true,
			triggerAction	: 'all',
			typeAhead: true,
			width			: 250,
			minChars : 1,
			allowBlank: true,
			store				: catIntermediario,
		/*	tpl:  '<tpl for=".">' +
                '<tpl if="!Ext.isEmpty(loadMsg)">'+
                '<div class="loading-indicator">{loadMsg}</div>'+
                '</tpl>'+
                '<tpl if="Ext.isEmpty(loadMsg)">'+
                '<div class="x-combo-list-item">' +
                '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                '</div></tpl></tpl>',*/
			listeners: {
				select: {
					fn: function(combo) {
						var fpDato = Ext.getCmp('formaDatos');
						fpDato.hide();
						Ext.getCmp('gridConsulta').hide();
						Ext.getCmp('cmbMoneda').reset();
						Ext.getCmp('cmbFechaCorte').reset();
						if (fpDato.isVisible()) {
							fpDato.hide();
							Ext.getCmp('cmbMoneda').reset();
							Ext.getCmp('cmbFechaCorte').reset();
						}
						
						if(combo.getValue()=='T'){
							catalogoFechaCorte.load({
								params : Ext.apply(fp.getForm().getValues(),{
									ic_if : combo.getValue(),
									tipoLinea: Ext.getCmp('cmbTipoLinea').getValue()
								})
							});
						}else{
							catalogoFechaCorte.load({
								params : Ext.apply(fp.getForm().getValues(),{
									//comboIntermediario : ((Ext.getCmp('cmbTipoLinea').getValue()=='C')?'12':combo.getValue()),
									comboIntermediario : combo.getValue(),
									tipoLinea: Ext.getCmp('cmbTipoLinea').getValue()
									//cliente : ((Ext.getCmp('cmbTipoLinea').getValue()=='C')?combo.getValue():'')
								})
							});
						}
						catalogoMoneda.load({
							params : Ext.apply(fp.getForm().getValues(),{
								ic_if : combo.getValue()
							})
						});
						Ext.getCmp("cmbMoneda").show();
						Ext.getCmp("cmbFechaCorte").show();
					}
				}
			}
		},
		{
			xtype				: 'combo',
			name				: 'comboMoneda',
			id					: 'cmbMoneda',
			fieldLabel		: 'Moneda',
			mode: 'local', 
			hiddenName		: 'comboMoneda',
			emptyText: 'Seleccionar',	
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoMoneda,
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			width			: 330,
			hidden:true,
			allowBlank: true
		},
		{
			xtype				: 'combo',
			fieldLabel		: 'Fecha de corte',
			name				: 'catalogoFechaCorte',
			id					: 'cmbFechaCorte',
			mode: 'local', 
			autoLoad: false,	
			displayField : 'DESCRIPCION',	
			emptyText: 'Seleccionar...',
			valueField : 'CLAVE',
			hiddenName 		: 'catalogoFechaCorte',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			width			: 330,
			minChars : 1,	
			tabIndex			: 5,
			hidden:true,
			store : catalogoFechaCorte

		}
	];
	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaDatos',
		layout			: 'form',
		width				: 840,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		height: 'auto',
		bodyStyle		: 'padding: 20px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			{
			
				layout: 'fit',
				modal: true,
				width: 840,
				id : 'fitDatos',
				modal: true,					
				closeAction: 'hide',
				resizable: false,
				closable:false,
				items		: [ 
					gridConsultaG,
					gridTotales,
					{
						width		: 600,	
						xtype: 'label',
						id: 'lblFirmas',	
						align: 'center',
						text: ''
					},
					{ 	xtype: 'textfield',
						hidden: true, 
						id: 'numeroDatos',
						width:250,
						align : 'center',
						value: 'NO SE ENCONTR� NING�N REGISTRO'
					},
					{ 	xtype: 'textfield',  hidden: true, id: 'sNombreMoneda', 	value: '' },		
					{ 	xtype: 'textfield',  hidden: true, id: 'sObservaciones', value: '' },	
					{ 	xtype: 'textfield',  hidden: true, id: 'sFirmaIF', 	value: '' },	
					{ 	xtype: 'textfield',  hidden: true, id: 'sFirmaNafin', 	value: '' },
					{ 	xtype: 'textfield',  hidden: true, id: 'sPuestoIF', 	value: '' },
					{ 	xtype: 'textfield',  hidden: true, id: 'sPuestoNafin', 	value: '' },
					{ 	xtype: 'textfield',  hidden: true, id: 'sFechaEnvio', 	value: '' },
					{ 	xtype: 'textfield',  hidden: true, id: 'sFechaCorte', 	value: '' },
					{ 	xtype: 'textfield',  hidden: true, id: 'sClaveIF', 	value: '' },
					{ 	xtype: 'textfield',  hidden: true, id: 'sClaveMoneda', 	value: '' },
					{ 	xtype: 'textfield',  hidden: true, id: 'sNombreIF', 	value: '' },
					{ 	xtype: 'textfield',  hidden: true, id: 'sNumeroSirac', 	value: '' }
				]
			}
		],
		buttons			: [
			{
				xtype: 'button',
				text: 'Imprimir Pantalla',
				tooltip:	'Imprimir Pantalla',
				id: 'btnImprimirPantalla',
				iconCls: 'icoPdf',
				handler: function(boton, evento) {
					var banco = "";
          
					/*if(Ext.getCmp('cs_tipo_NB').getValue()==true){
						banco = "NB";
					}else if(Ext.getCmp('cs_tipo_B').getValue()==true){
						banco = "B";
					}*/
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '15cedulaconcifpdf.jsp',
						params: Ext.apply(fp.getForm().getValues(),{									
							operacion: 'VersionNueva',	
							sTipoBanco : Ext.getCmp('cmbTipoAfiliado').getValue(),
							sNombreMoneda:Ext.getDom('cmbMoneda').value,
							sObservaciones:Ext.getCmp('sObservaciones').getValue(),
							sFirmaIF:Ext.getCmp('sFirmaIF').getValue(),
							sFirmaNafin:Ext.getCmp('sFirmaNafin').getValue(),
							sPuestoIF:Ext.getCmp('sPuestoIF').getValue(),
							sPuestoNafin:Ext.getCmp('sPuestoNafin').getValue(),
							sFechaEnvio:Ext.getCmp('sFechaEnvio').getValue(),
							sFechaCorte:Ext.getCmp('sFechaCorte').getValue(),
							sNombreIF:Ext.getCmp('sNombreIF').getValue(),
							sNumeroSirac:Ext.getCmp('sNumeroSirac').getValue()
						}),							
						callback: descargaArchivo
					});								
				}
			},
			{
				xtype: 'button',
				text: 'Imprimir PDF',
				tooltip:	'Imprimir PDF',
				id: 'btnImprimirPDF',
				iconCls: 'icoPdf',
				handler: function(boton, evento) {
					/*var banco = "";
					if(Ext.getCmp('cs_tipo_NB').getValue()==true){
						banco = "NB";
					}else if(Ext.getCmp('cs_tipo_B').getValue()==true){
						banco = "B";
					}*/
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '15cedulaconcifpdf.jsp',
						params: Ext.apply(fp.getForm().getValues(),{									
							operacion: 'VersionNueva',	
							sTipoBanco : Ext.getCmp('cmbTipoAfiliado').getValue(),
							sNombreMoneda:Ext.getDom('cmbMoneda').value,
							sObservaciones:Ext.getCmp('sObservaciones').getValue(),
							sFirmaIF:Ext.getCmp('sFirmaIF').getValue(),
							sFirmaNafin:Ext.getCmp('sFirmaNafin').getValue(),
							sPuestoIF:Ext.getCmp('sPuestoIF').getValue(),
							sPuestoNafin:Ext.getCmp('sPuestoNafin').getValue(),
							sFechaEnvio:Ext.getCmp('sFechaEnvio').getValue(),
							sFechaCorte:Ext.getCmp('sFechaCorte').getValue(),
							sNombreIF:Ext.getCmp('sNombreIF').getValue(),
							sNumeroSirac:Ext.getCmp('sNumeroSirac').getValue()
						}),							
						callback: descargaArchivo
					});								
				}
			},
			{	
				xtype: 'button',	text: 'Salir',	iconCls: 'icoLimpiar', 	id: 'btnCerrar',
					handler: function(){		
						Ext.getCmp('formaDatos').hide();
						Ext.getCmp('forma').getForm().reset();
						Ext.getCmp('forma').show();
						Ext.getCmp('cmbMoneda').hide();
						Ext.getCmp('cmbFechaCorte').hide();
            catTipoAfiliado.removeAll();
            catIntermediario.removeAll();
						
					} 
			},
			{	
				xtype: 'button',	text: 'Regresar',	iconCls: 'icoCerrar', 	id: 'btnCerra',hidden : true,
					tabIndex			: 8,
					handler: function() {
						window.location = '15cedulaconcifExt01.jsp';					
					}
			}
		]
	});
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame: true,
		width: 500,
		//autoHeight	: true,
		title: 'C�dula de Conciliaci�n',
		layout		: 'form',
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 120,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		
	//	collapsible: true,
		//titleCollapse: false,
		
		
		
	//	defaultType: 'textfield',
		
		items: elementosForma,			
	//	monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'consultar',
				iconCls: 'icoBuscar',
				formBind: true,	
				tabIndex			: 1,
				handler: function(boton, record) {	
					var valido = false;
					var intermediario= Ext.getCmp('cmbIntermediario');
					var moneda= Ext.getCmp('cmbMoneda');
					var fecha= Ext.getCmp('cmbFechaCorte');
					var nombreIF = Ext.getDom('cmbIntermediario').value;
					var nombreMoneda = Ext.getDom('cmbMoneda').value;
					if(intermediario.getValue()==''){
						intermediario.markInvalid("Debes seleccionar Tipo de Intermediario");
						return;
					}
					if(intermediario.getValue()=="T"){
						if(fecha.getValue()==''){
							fecha.markInvalid("Debes seleccionar la fecha de corte");
							return;
						}
						fp.el.mask('Procesando...', 'x-mask-loading');
						var cmGConsulta = gridConsulta.getColumnModel();
						if(Ext.getCmp('cmbTipoLinea').getValue()=='C'){
							cmGConsulta.setHidden(cmGConsulta.findColumnIndex('NOMBRECLIENTE'), false);
							cmGConsulta.setHidden(cmGConsulta.findColumnIndex('CG_RAZON_SOCIAL'), true);
						}else{
							cmGConsulta.setHidden(cmGConsulta.findColumnIndex('NOMBRECLIENTE'), true);
							cmGConsulta.setHidden(cmGConsulta.findColumnIndex('CG_RAZON_SOCIAL'), false);
						}
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'Consultar'
							})
						});
					}else{
						if(Ext.isEmpty(moneda.getValue())){
							moneda.markInvalid("Debes seleccionar el tipo de moneda");
							return;
						}
						if(Ext.isEmpty(fecha.getValue())){
							fecha.markInvalid("Debes seleccionar la fecha de corte");
							return;
						}
						fp.el.mask('Procesando...', 'x-mask-loading');
						consultaDataG.load({
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'Consultar',
								nombreIF : nombreIF,
								nombreMoneda : nombreMoneda
							})
						});
					}
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				tabIndex			: 8,
				handler: function() {
					window.location = '15cedulaconcifExt01.jsp';					
				}
			}
		]
	});
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			fp,	
			fpDatos,
			NE.util.getEspaciador(20),
			gridConsulta
		]
	});
	procesarSuccessDatosIniciales();
});	
