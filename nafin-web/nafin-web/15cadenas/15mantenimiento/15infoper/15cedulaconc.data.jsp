<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.descuento.*,
		com.netro.seguridad.*,		
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 	
	String sClaveIF = (request.getParameter("sClaveIF")!=null)?request.getParameter("sClaveIF"):""; 
	String sClaveMoneda = (request.getParameter("sClaveMoneda")!=null)?request.getParameter("sClaveMoneda"):""; 
	String sMes_ini = (request.getParameter("sMes_ini")!=null)?request.getParameter("sMes_ini"):""; 
	String sAnio_ini = (request.getParameter("sAnio_ini")!=null)?request.getParameter("sAnio_ini"):""; 
	String sMes_fin = (request.getParameter("sMes_fin")!=null)?request.getParameter("sMes_fin"):""; 
	String sAnio_fin = (request.getParameter("sAnio_fin")!=null)?request.getParameter("sAnio_fin"):""; 
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

	String sNombreIF       = (request.getParameter("sNombreIF")==null)?"":request.getParameter("sNombreIF");
	String sNombreMoneda  = (request.getParameter("sNombreMoneda")==null)?"":request.getParameter("sNombreMoneda");
	String sFechaCorte    = (request.getParameter("sFechaCorte")==null)?"":request.getParameter("sFechaCorte");
	String sFechaEnvio    = (request.getParameter("sFechaEnvio")==null)?"":request.getParameter("sFechaEnvio");
	String sNumeroSirac	  = (request.getParameter("sNumeroSirac")==null)?"":request.getParameter("sNumeroSirac");
	String cs_tipo = (request.getParameter("cs_tipo_B")!=null)?request.getParameter("cs_tipo_B"):""; 
	String tipoLinea = (request.getParameter("tipoLinea")!=null)?request.getParameter("tipoLinea"):""; 
	
		
	System.out.println("cs_tipo  "+cs_tipo);
		
	System.out.println("informacion -------------------- "+informacion); 	
	
	String sDateIni = "01/"+sMes_ini+"/"+sAnio_ini;
   String sDateFin = "01/"+sMes_fin+"/"+sAnio_fin;
  	
	StringBuffer  leyendaEncabezado 	= new StringBuffer();
   StringBuffer  firmas 	= new StringBuffer();
  
	String infoRegresar="", consulta ="";
	JSONObject jsonObj = new JSONObject();
	
	ConCedulaConciliacion paginador = new ConCedulaConciliacion();
	paginador.setCs_tipo(cs_tipo);
	paginador.setSClaveIF(sClaveIF);
	paginador.setSClaveMoneda(sClaveMoneda);
	paginador.setSDateIni(sDateIni);
	paginador.setSDateFin(sDateFin);
	paginador.setFechaCorte(sFechaCorte);
	paginador.setFechaCarga(sFechaEnvio);
	paginador.setTipoConsulta(informacion);	
	paginador.setTipoLinea(tipoLinea);	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

if (informacion.equals("catIntermediario")) {

	
	CatalogoIFOperNafin cat = new CatalogoIFOperNafin();
	if(!"C".equals(tipoLinea)){
   cat.setClave("i.ic_if");
   cat.setDescripcion("'('||ic_financiera||') '||i.cg_razon_social" ); 
	cat.setFinanciera("i.ic_if");
	cat.setCs_tipo(cs_tipo);	 
	cat.setOrden("ic_financiera");
	}else{
		if(!"CE".equals(cs_tipo)){
			cat.setTipoLinea(tipoLinea);
			cat.setClave("i.ic_if");
			cat.setDescripcion("'('||cc.IN_NUMERO_SIRAC||') '||i.cg_razon_social" ); 
			cat.setCs_tipo(cs_tipo);	 
			cat.setOrden("cc.IN_NUMERO_SIRAC");
		}else{
			cat.setTipoLinea(tipoLinea);
			cat.setClave("cc.IC_CLIENTE_EXTERNO");
			cat.setDescripcion("'('||cc.IN_NUMERO_SIRAC||') '||i.cg_razon_social" ); 
			cat.setCs_tipo(cs_tipo);	 
			cat.setOrden("cc.IN_NUMERO_SIRAC");
		}
	}
   infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("catMoneda")) {

	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("COMCAT_MONEDA");
   cat.setCampoClave("IC_MONEDA");
   cat.setCampoDescripcion("CD_NOMBRE"); 
	cat.setOrden("CD_NOMBRE");
	cat.setValoresCondicionIn("1,54", Integer.class);
   infoRegresar = cat.getJSONElementos();


	
}else if (informacion.equals("catAnio")) {

	Calendar calendario = Calendar.getInstance();			
	int anio = calendario.get(Calendar.YEAR);
	List registros  = new ArrayList();
	HashMap datos = new HashMap();	

	for(int x=2005; x<=anio;x++){
		datos = new HashMap();			
		datos.put("clave", String.valueOf(x) );
		datos.put("descripcion",String.valueOf(x));
		registros.add(datos);	
	}
	
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();	

}else  if (informacion.equals("Consultar")){
	
	paginador.setTipoConsulta(informacion);
	
	queryHelper = new CQueryHelperRegExtJS( paginador);
	Registros reg	=	queryHelper.doSearch();	
	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();  
	

}else  if (informacion.equals("Eliminar_Registros") ){
   
	String sClaveIF_E[] = request.getParameterValues("sClaveIF_E");
	String sClaveMoneda_E[] = request.getParameterValues("sClaveMoneda_E");
	String sFechaCorte_E[] = request.getParameterValues("sFechaCorte_E");
	String sFechaEnvio_E[] = request.getParameterValues("sFechaEnvio_E");
 
	PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB",PagosIFNB.class);

	try{  
  
	  for(int x=0; x< sClaveIF_E.length; x++){
	
		 bean.eliminaCedula(sClaveIF_E[x], sClaveMoneda_E[x], sFechaCorte_E[x], sFechaEnvio_E[x], tipoLinea, cs_tipo);
		 
		} 
	
	} catch (NafinException ne){
		jsonObj.put("success", new Boolean(false));
		log.error(" Exception "+ne);
	System.out.println(ne.getMsgError());
	}catch(Exception e){ 
		out.println(e);
		log.error(" Exception "+e);
		jsonObj.put("success", new Boolean(false));
		}finally {
	jsonObj.put("success", new Boolean(true));
	
	}
	
	infoRegresar = jsonObj.toString();  		

}else  if (informacion.equals("ConsDetalle") ){
	
	Registros reg	=	queryHelper.doSearch();	
	
	String 	observaciones_C  = "", ic_usuario ="", nombreUsuario = "", cvecertificado ="", fcarga="",
	sClaveFirmaIF  ="",   	sFirmaIF ="",   	sPuestoIF  ="", 	sClaveFirmaNafin ="",   	sFirmaNafin  ="",   	sPuestoNafin  ="";
	
	while (reg.next()) {
		observaciones_C = reg.getString("OBSERVACIONES")==null?"":reg.getString("OBSERVACIONES");
		ic_usuario = reg.getString("ic_usuario")==null?"":reg.getString("ic_usuario");
		nombreUsuario  	= reg.getString("nombreUsuario")==null?"":reg.getString("nombreUsuario");
		cvecertificado	   = reg.getString("cvecertificado") == null?"":reg.getString("cvecertificado");
		fcarga	   = reg.getString("fcarga") == null?"" : reg.getString("fcarga");
	}

   leyendaEncabezado 	= new StringBuffer();
	leyendaEncabezado.append("<table cellpadding='2' cellspacing='2' border='0' align='center' ><tr><td class='titulos' align='center'>NACIONAL FINANCIERA, S.N.C.</td></tr>"+
						"<tr>	<td class='titulos' align='center'>	"+
						" RESUMEN DE LA CONCILIACIÓN DE SALDOS AL "+sFechaCorte.substring(0,2) + " DE " + meses[Integer.parseInt(sFechaCorte.substring(3,5))-1].toUpperCase() + " DE " + sFechaCorte.substring(6,10)+"<br><br>"+
						"(" + sNumeroSirac + ") " +sNombreIF+"<br>"+sNombreMoneda+"</td></tr></table>");			
	 
	 
	HashMap regFirma = paginador.getDatosFirma(sClaveIF) ;
	if(regFirma.size()>0)  {
		sClaveFirmaIF  =regFirma.get("FIRMA_IF").toString();
		sFirmaIF =regFirma.get("NOMBRE_IF").toString();
		sPuestoIF  =  regFirma.get("PUESTO_IF").toString();
		sClaveFirmaNafin =regFirma.get("FIRMA_NAFIN").toString();
		sFirmaNafin  =regFirma.get("RESP_NAFIN").toString();
		sPuestoNafin  =regFirma.get("PUESTO_NAFIN").toString();
	}
	 
				
	 String nota =  paginador.getNotas(sFechaCorte, sClaveIF, sClaveMoneda );
	
	firmas 	= new StringBuffer();	
	firmas.append("<table cellpadding='1' cellspacing='2' border='0' width='780' > "+
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+		
				
				" <tr> <td align='justify' class='left'><b>NOTA:</b>&nbsp;"+nota+"&nbsp; </td></tr>"+
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+			
				" <tr> <td align='left' class='formas'><b>Observaciones:</b>&nbsp;"+observaciones_C+"&nbsp; </td></tr>"+	
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+					
				"<tr>"+
				"<td align='center'>"+
				"<table cellpadding='2' cellspacing='2' border='0'>"+
				"<tr>"+
				"<td class='formas' align='center'>	Confirmo que los saldos operativos de Nacional Financiera, S.N.C. han sido conciliados con los registros contables de<br> "+sNombreIF+"	</td>"+
				"</tr>"+
				"</table>"+
				"</td>"+
				"</tr>"+
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+							
				"<tr>"+
				"	<td align='center'>"+
				"<table cellpadding='2' cellspacing='2' border='0' width='600'>"+
				"<tr>"+				
				" <td  class='formas' align='center' valign='top'>"+
					" <table class='formas' > "+
					" <tr> <td align='right' class='formas'> <b>Usuario:               </b> </td> <td class='formas'>"+ic_usuario+		 "</td> </tr>"+
					" <tr> <td align='right' class='formas'> <b>Nombre: 							 </b> </td> <td class='formas'>"+nombreUsuario+ "</td> </tr>"+
					" <tr> <td align='right' class='formas'> <b>No. serie certificado: </b> </td> <td class='formas'>"+cvecertificado+"</td> </tr>"+
					" <tr> <td align='right' class='formas'> <b>Fecha/Hora:            </b> </td> <td class='formas'>"+fcarga+				 "</td> </tr>"+
					" </table >"+
					"</td>"+
					" <td class='formas' align='center' valign='top' >"+
					"	<img src='/nafin/00utils/gif/firma_pagos_cartera.gif' border='0' width='228' height='100'> "+
					"</td>"+
					" </tr> "+					
					"<tr> "+				
					" <td align='center' class='formas' valign='top'>_________________________________________<br> "+
					" <b>"+sFirmaIF+"</b><br>"+sPuestoIF+"<br>"+sNombreIF+"</td>"+
					
					" <td align='center' class='formas' valign='top'>_________________________________________<br> "+
					"<b>"+sFirmaNafin+"</b><br>"+sPuestoNafin+"<br>	NACIONAL FINANCIERA S.N.C.	</td>"+						
				"</tr>"+
				"</table>"+
				"</td>"+
				"</tr>"+				
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+								
				" <tr><td align='justify' class='formas'>Nota: Si en un plazo de 10 d&iacute;as h&aacute;biles contados a partir de la recepci&oacute;n del<br>presente Estado de Cuenta no se reciben las observaciones correspondientes se<br>dar&aacute;n por aceptadas las cifras. </td> </tr>"+	
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+		
				" <tr align='right' > <td align='right' class='formas'>Fecha de Env&iacute;o:&nbsp;"+sFechaEnvio+"</td></tr>	"+
				" <tr> <td align='center' class='formas'> &nbsp; </td></tr>"+		
	
				"</table>");
	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);	
	jsonObj.put("leyendaEncabezado", leyendaEncabezado.toString());
	jsonObj.put("firmas", firmas.toString());
	
	jsonObj.put("sNombreMoneda", sNombreMoneda);
	jsonObj.put("sObservaciones", observaciones_C);
	jsonObj.put("sFirmaIF", sFirmaIF);
	jsonObj.put("sFirmaNafin", sFirmaNafin);
	jsonObj.put("sPuestoIF", sPuestoIF);
	jsonObj.put("sPuestoNafin", sPuestoNafin);
	jsonObj.put("sFechaEnvio", sFechaEnvio);
	jsonObj.put("sFechaCorte", sFechaCorte);
	jsonObj.put("sClaveIF", sClaveIF);
	jsonObj.put("sClaveMoneda", sClaveMoneda);
	jsonObj.put("sNombreIF", sNombreIF);
	jsonObj.put("fcarga", fcarga);
	jsonObj.put("icusuario", ic_usuario);
	jsonObj.put("nusuario", nombreUsuario);
	jsonObj.put("cvecertificado", cvecertificado);
	jsonObj.put("sNumeroSirac", sNumeroSirac);
			
				
	infoRegresar = jsonObj.toString(); 

}else  if (informacion.equals("consDetTotalesData") ){

	Registros reg	=	queryHelper.doSearch();	
	
	double 	dTotal_saldoInsolto=0, dTotal_MontoOperado =0, 	dTotal_CapitalVigente =0, 
		dTotal_CapitalVencido =0,  dTotal_InteresVigente = 0, dTotal_InteresVencido = 0,
		dTotal_Moras = 0,  dTotal_TotalAdeudo = 0, dTotal_Descuentos  =  0;
	HashMap	registrosTot = new HashMap();
	JSONArray registros = new JSONArray();	
	
	while (reg.next()) {
		dTotal_saldoInsolto  += Double.parseDouble(reg.getString("SALDO_INSOLUTO")==null?"0":reg.getString("SALDO_INSOLUTO"));
		dTotal_MontoOperado  += Double.parseDouble(reg.getString("CAPITAL_VIGENTE")==null?"0":reg.getString("CAPITAL_VIGENTE"));
		dTotal_CapitalVigente  += Double.parseDouble(reg.getString("INTERES_VIGENTE")==null?"0":reg.getString("INTERES_VIGENTE"));
		dTotal_CapitalVencido  += Double.parseDouble(reg.getString("CAPITAL_VENCIDO")==null?"0":reg.getString("CAPITAL_VENCIDO"));
		dTotal_InteresVigente  += Double.parseDouble(reg.getString("INTERES_VIGENTE")==null?"0":reg.getString("INTERES_VIGENTE"));
		dTotal_InteresVencido  += Double.parseDouble(reg.getString("INTERES_VENCIDO")==null?"0":reg.getString("INTERES_VENCIDO"));
		dTotal_Moras  += Double.parseDouble(reg.getString("MORAS")==null?"0":reg.getString("MORAS"));
		dTotal_TotalAdeudo  += Double.parseDouble(reg.getString("TOTAL_ADEUDO")==null?"0":reg.getString("TOTAL_ADEUDO"));
		dTotal_Descuentos  += Double.parseDouble(reg.getString("DESCUENTOS")==null?"0":reg.getString("DESCUENTOS"));			
	}
	registrosTot = new HashMap();
	registrosTot.put("DESCRIPCION", "TOTAL");
	registrosTot.put("T_SALDO_INSOLUTO", Double.toString(dTotal_saldoInsolto) );	
	registrosTot.put("T_CAPITAL_VIGENTE", Double.toString(dTotal_CapitalVigente) );	
	registrosTot.put("T_CAPITAL_VENCIDO", Double.toString(dTotal_CapitalVencido) );	
	registrosTot.put("T_INTERES_VIGENTE", Double.toString(dTotal_InteresVigente) );	
	registrosTot.put("T_INTERES_VENCIDO", Double.toString(dTotal_InteresVencido) );	
	registrosTot.put("T_MORAS", Double.toString(dTotal_Moras) );
	registrosTot.put("T_TOTAL_ADEUDO", Double.toString(dTotal_TotalAdeudo) );
	registrosTot.put("T_DESCUENTOS", Double.toString(dTotal_Descuentos) );
	
	registros.add(registrosTot);	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();

	System.out.println("infoRegresar -------------------- "+infoRegresar); 
}





	

%>


<%=infoRegresar%>

