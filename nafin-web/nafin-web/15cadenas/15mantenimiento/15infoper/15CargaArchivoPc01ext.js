
Ext.onReady(function() {
var myValidFn = function(v) {
		var myRegex = /^.+\.([zZ][iI][pP])$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivozip 		: myValidFn,
		archivozipText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): ZIP.'
	});

//-----------------------------HANDLER's----------------------------------------

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var resultados =Ext.util.JSON.decode(opts.responseText);
		if (arrRegistros != null) {
				//consultaData.loadData(resultados.registros);
		}
	}
	var procesarConsultaCifrasControl = function(store, registros, opts){

		var gridCifrasControl 			= Ext.getCmp('gridCifrasControl');

		if (registros != null) {

			var el 							= gridCifrasControl.getGridEl();

			if(store.getTotalCount() > 0) {

				el.unmask();

			} else {

				el.mask('Se present� un error al leer los par�metros', 'x-mask');

			}

		}

	}
	var renderContenido = function(value, metaData, record, rowIndex, colIndex, store) {

		if(        record.get('TIPO_CONTENIDO') == 'NUMERO_ENTERO' ){
			metaData.style += 'text-align: left !important';
		} else if( record.get('TIPO_CONTENIDO') == 'FECHA' 		  ){
			metaData.style += 'text-align: left  !important';
		} else if( record.get('TIPO_CONTENIDO') == 'TEXTO' 		  ){
			metaData.style += 'text-align: left  !important';
		}
		return value;

	}
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------
	var cifrasControlData = new Ext.data.ArrayStore({

		storeId: 'cifrasControlDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  	mapping: 0 },
			{ name: 'CONTENIDO', 		mapping: 1 },
			{ name: 'TIPO_CONTENIDO', 	mapping: 2 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaCifrasControl,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCifrasControl(null, null, null);
				}
			}
		}

	});



//------------------------------------------------------------------------------
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
var elementosForma=[
	{
		xtype:'displayfield',
		value:'<div align=center width=400 style="font-size:12px">Tipo de Archivo:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>'
	},
	{
		xtype: 'radiogroup',
		id:'rbTipoArchivo',
		name:'tipo',
		allowBlank	: false,
		style: 'margin:0 auto;padding: 6px;',
		items: [
			{xtype:'displayfield',value:'<div align=center width=400>&nbsp;&nbsp;&nbsp;&nbsp;</div>'},
			{boxLabel: 'Vencimiento', name: 'tipoArchivo', allowBlank	: false, inputValue:'V'},
			{boxLabel: 'Operados', name: 'tipoArchivo', allowBlank	: false, inputValue: 'O'},
			{xtype:'displayfield',value:'<div align=center width=400>&nbsp;</div>'}
		]
	},//Ruta del Archivo Origen:
	{
		xtype:'displayfield',
		value:'<div align=center width=400 style="font-size:12px">Ruta del Archivo Origen:&nbsp;&nbsp;</div><br>'
	},
	{
		xtype: 'fileuploadfield',
		id: 'archivoCarga',
		emptyText: 'Ruta del Archivo',
		name: 'archivoPC',
		buttonText: null,
		buttonCfg: {
			iconCls: 'upload-icon'
		},
		anchor: '98%',
		vtype: 'archivozip'
    }
];
	var fp = new Ext.form.FormPanel({
      id: 'forma',
		layout :'anchor',
		width: 600,
		title: 'Carga de Archivo Pagos Cartera',
		frame: true,
		collapsible: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		fileUpload: true,
		labelWidth: 30,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:[
			{
				xtype:'button',
				text:'Subir',
				iconCls: 'upload-icon',
				formBind: true,
				handler:function(){
					var todoCorrecto=true;

					var labelTitulo = Ext.getCmp('titulo');
					var labelTitulo2 = Ext.getCmp('titulo2');
					var panel = Ext.getCmp('display');
					var textArea = Ext.getCmp('textArea');
					var gridCarga = Ext.getCmp('gridCifrasControl');
					labelTitulo2.show();

					var value = Ext.getCmp('forma').getForm().findField("rbTipoArchivo").getValue();
					if(!value){
						Ext.Msg.alert('Validaci�n','Debe seleccionar alg�n tipo de archivo');
						todoCorrecto=false;
						return ;
					}
					var txtArchivo=Ext.getCmp('archivoCarga').getValue();
					if(!formatoValido(txtArchivo, 7)) {///7
						Ext.Msg.alert("","El formato del archivo de origen no es el correcto para el tipo de archivo seleccionado");
						todoCorrecto=false;
						return;
					}
					if(todoCorrecto){
						var form = fp.getForm();
						//if(form.isValid()){

							 form.submit({
								  url:'15CargaArchivoPc01ext.data.jsp',
								  //waitMsg: 'Esta operaci&oacute;n puede tardar varios minutos, dependiendo del tama�o de su archivo.<br><br>Espere por favor...',
								  success: function(form, action, resp) {

										var jsonData=Ext.decode(action.response.responseText);

										if(jsonData.valida=='er'){
											labelTitulo2.hide();
											labelTitulo.hide();
											gridCarga.hide();
											var panel = Ext.getCmp('panel');
											panel.setTitle('<div  align=center>'+jsonData.titulo+'</div>');
											//plainOldPanel.show();
											panel.doLayout();
											textArea.setValue(jsonData.err);
											textArea.show();
										}else if(jsonData.valida=='ok'){
											labelTitulo.hide();
											labelTitulo2.hide();
											textArea.hide();
											gridCarga.show();
											var panel = Ext.getCmp('panel');
											panel.setTitle('<div  align=center>'+jsonData.titulo+'</div>');
											//plainOldPanel.show();
											panel.doLayout();
											var cifrasControlData = Ext.StoreMgr.key('cifrasControlDataStore');
											cifrasControlData.loadData(jsonData.cifrasControlDataArray);
											labelTitulo.update('<div  align=center>'+jsonData.titulo+'</div><br>');
											
										}
								  },
									failure: NE.util.mostrarSubmitError
							 });
							 plainOldPanel.show();
							 fp.hide();
					}
				}
			}
		]
    });


//------------------------------------------------------------------------------
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
 var plainOldPanel = new Ext.Panel({
	id:'panel',
	bodyBorder : true,
	style: 'margin:0 auto;border:0;',
	padding:5,
	hidden : true,
	frame: true,
	width: 600,
	autoHeight: true,
	title: ' ',
	buttonAlign: 'center',
	listeners:{
		beforeshow:function( panel ){
			panel.doLayout();
		}
	},
	items: [{
				xtype:'label',
				id:'titulo',
				html:'<div  align=center>Cargando Archivo...<div>'//class="titulos"

			},{
				xtype:'label',
				id:'titulo2',
				html:'<div  align=center>Esta operaci&oacute;n puede tardar varios minutos, dependiendo del tama�o de su archivo.<br><br>Espere por favor...<div>'
				//class="titulos"
			}
			,
					{
						xtype:'textarea',
						id:'textArea',
						//style: 'margin:10 auto;',
						region:'center',
						width:'99%',// 500,
						height:300,
						region: 'center',
						value:'',
						hidden: true
					},
					{
						xtype: 	'box',
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '5%'
						},
						region:	'west'
					},
					{
						store: 			cifrasControlData,
						xtype: 			'grid',
						id:				'gridCifrasControl',
						stripeRows: 	true,
						loadMask: 		true,
						 width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '90%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		200,
								hidden: 		false,
								hideable:	false,
								fixed:		true,
								align: 		'right'
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								align:		'right',
								renderer: 	renderContenido
							}
						]
					},
					{
						xtype: 	'box',
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '5%'
						},
						region:	'east'
					}
			],
		fbar: [
			{
				  text: 'Regresar',
				  iconCls:	'icoRegresar',
				  handler:function(){
							Ext.getCmp('gridCifrasControl').hide();;
							plainOldPanel.hide();
							var textArea = Ext.getCmp('textArea').hide();
							fp.getForm().reset();
							fp.show();
						}
			 }
		 ]
    });
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 900,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,//panelInfo,
			NE.util.getEspaciador(20),
			plainOldPanel
		]
	});
});
