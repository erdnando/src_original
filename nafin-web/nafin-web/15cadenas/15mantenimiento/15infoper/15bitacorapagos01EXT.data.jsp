<%@ page 
	contentType=
		"application/json;charset=UTF-8" 
	import="
		net.sf.json.JSONArray, net.sf.json.JSONObject, netropology.utilerias.*,
		java.util.*, java.sql.*, javax.naming.*,
    com.netro.model.catalogos.CatalogoMoneda, com.netro.cadenas.BitacoraPagos,
		org.apache.commons.logging.Log  " 
	errorPage=
		"/00utils/error_extjs.jsp"
%>

<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%    
String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String operacion		= (request.getParameter("operacion")	!=null)	?	request.getParameter("operacion")	:	"";
//String tipoRadio    = (request.getParameter("rbGroup")	!=null)	?	request.getParameter("rbGroup")	:	"";
String intermediario= (request.getParameter("cmb_ti")	!=null)	?	request.getParameter("cmb_ti")	:	"";
String numInter		  = (request.getParameter("numInter")	!=null)	?	request.getParameter("numInter")	:	"";
String fechaInicial	= (request.getParameter("_fechaReg")	!=null)	?	request.getParameter("_fechaReg")	:	"";
String fechaFinal		= (request.getParameter("_fechaFinReg")	!=null)	?	request.getParameter("_fechaFinReg")	:	"";
String tipo		      = (request.getParameter("cmb_tipo")	!=null)	?	request.getParameter("cmb_tipo")	:	"";
String moneda   		= (request.getParameter("cmb_mon")	!=null)	?	request.getParameter("cmb_mon")	:	"";
String comboTipoAfiliado   		= (request.getParameter("comboTipoAfiliado")	!=null)	?	request.getParameter("comboTipoAfiliado")	:	"";
String numSirac   		= (request.getParameter("numSirac")	!=null)	?	request.getParameter("numSirac")	:	"";
String tipoLinea   		= (request.getParameter("comboTipoLinea")	!=null)	?	request.getParameter("comboTipoLinea")	:	"";

String infoRegresar	= "";
String consulta   	= "";
int  start= 0, limit =0;

if (informacion.equals("catalogomon") )	{

	JSONObject		jsonObj 	= new JSONObject();
	CatalogoMoneda cat 		= new CatalogoMoneda();
  cat.setCampoClave("ic_moneda");
  cat.setCampoDescripcion("cd_nombre"); 
  List lis = cat.getListaElementos();  
	jsonObj.put("registros", lis);	
  infoRegresar = jsonObj.toString();

} else if (informacion.equals("catalogoint") )	{

	JSONObject		jsonObj 	= new JSONObject();
  BitacoraPagos bitacora  = new BitacoraPagos();
  Registros     registros = new Registros();
  
  if(!"CE".equals(comboTipoAfiliado)){
    bitacora.setIntermediario(comboTipoAfiliado);
    bitacora.setTipoLinea(tipoLinea);
	registros = bitacora.catalogoIF();
  }else{
    registros = bitacora.catalogoClienteExterno();
  }
  
  jsonObj.put("total",new Integer( registros.getNumeroRegistros()) );	
	jsonObj.put("registros", registros.getJSONData() );	
  infoRegresar = jsonObj.toString();	
  
} else if (informacion.equals("Consultar") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")  ){
  JSONObject		jsonObj 	= new JSONObject();
  BitacoraPagos paginador = new BitacoraPagos();
  
  paginador.setRadio(comboTipoAfiliado);
  paginador.setTipo(tipo);
  paginador.setNumInter(numInter);
  paginador.setMoneda(moneda);
  paginador.setFechaInicial(fechaInicial);
  paginador.setFechaFinal(fechaFinal);
  paginador.setTipoLinea(tipoLinea);
  
  if("C".equals(tipoLinea)){
    
    if("CE".equals(comboTipoAfiliado)){
      paginador.setIntermediario("");
      paginador.setNumSirac(numSirac);
      paginador.setCliExterno(intermediario);
    }else{
      paginador.setIntermediario("");
      paginador.setNumSirac(intermediario);
      //paginador.setIntermediario(intermediario);
      paginador.setCliExterno("");
    }
  }else{
    paginador.setIntermediario(intermediario);
    paginador.setNumSirac("");
    paginador.setCliExterno("");
  }
    
  
  CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
  if(informacion.equals("Consultar") ||  informacion.equals("ArchivoPDF")){
    try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));										
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
    if(informacion.equals("Consultar") ){
      try {
					if (operacion.equals("Generar")) {	//Nueva consulta
						queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
					}
					consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
				} catch(Exception e) {
					throw new AppException("Error en la paginacion", e);
				}
        jsonObj = JSONObject.fromObject(consulta);        
    
    } else if(informacion.equals("ArchivoPDF") ){
				try {
					String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					//infoRegresar = jsonObj.toString();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
			}//fin pdf
  // fin consulta | pdf  
  } else  if(informacion.equals("ArchivoCSV") )  {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}
    
infoRegresar = jsonObj.toString();
log.debug("jsonObj: "+jsonObj);

} else if (informacion.equals("ConsultarTotales")){
  JSONObject		jsonObj 	= new JSONObject();
  BitacoraPagos paginador = new BitacoraPagos();
  
  paginador.setRadio(comboTipoAfiliado);
  paginador.setTipo(tipo);
  paginador.setIntermediario(intermediario);
  paginador.setNumInter(numInter);
  paginador.setMoneda(moneda);
  paginador.setFechaInicial(fechaInicial);
  paginador.setFechaFinal(fechaFinal);
  
  CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	consulta = queryHelper.getJSONResultCount(request);
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();
  log.debug("jsonObj: "+jsonObj);
}

%>
<%=  infoRegresar %>