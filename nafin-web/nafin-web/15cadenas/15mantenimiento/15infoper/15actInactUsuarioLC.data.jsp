<%@ page contentType="application/json;charset=UTF-8" import="   java.util.*,   java.io.*,   java.text.*,   com.netro.exception.*,    com.netro.cadenas.*,   java.sql.*,   com.netro.afiliacion.*,   netropology.utilerias.*,   net.sf.json.JSONObject" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<% 
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	String lineaCredito = (request.getParameter("comboLineaCredito") == null) ? "" : request.getParameter("comboLineaCredito");
	
	ConsActClLineaCred paginador = new ConsActClLineaCred();	
	int  start= 0, limit =0;
	String infoRegresar="", consulta="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	
if(informacion.equals("catalogoLineaCredito")){

	List  registros =  getClientesExternos();
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("registros", registros); 
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("Consultar")   ) {

	paginador.setLineaCredito(lineaCredito);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	if(informacion.equals("Consultar")   )  {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		if(informacion.equals("Consultar") ){
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
					consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
				jsonObj = JSONObject.fromObject(consulta);
		}
	}
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("modificarEstatus")){

	String cveCliente= (request.getParameter("cveCliente")==null) ? "" : request.getParameter("cveCliente");
	String estatus= (request.getParameter("estatus")==null) ? "" : request.getParameter("estatus");
	String observaciones= (request.getParameter("observaciones").equals("")) ? "null" : request.getParameter("observaciones");
	try{
		String mens = paginador.modificaEstatus(cveCliente,estatus, observaciones);
		jsonObj = new JSONObject();
		jsonObj.put("success",  new Boolean(true)); 	
		jsonObj.put("mensaje", mens);
	}catch(Exception e){
		throw new AppException("Error al guardar los datos", e);
	}
	infoRegresar = jsonObj.toString();

}

%>
<%= infoRegresar%>
<%! 

	
	public List   getClientesExternos() throws AppException { 
		System.out.println("   getBusquedaAvanzada (S) ");		
		StringBuffer	qrySentencia 	= new StringBuffer();		
		List  lVarBind		= new ArrayList();
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		AccesoDB con = new AccesoDB(); 		
		boolean exito = true;
		HashMap	datos = new HashMap();	
		List  registros		= new ArrayList();
		try{
			con.conexionDB();
			qrySentencia 	= new StringBuffer();	
			qrySentencia.append(" SELECT c.ic_nafin_electronico AS clave,   "+
			" DECODE (c.cs_tipo_persona,'F', c.cg_nombre || ' ' || c.cg_appat || ' ' || c.cg_apmat,c.cg_razon_social) AS descripcion   "+
			" FROM comcat_cli_externo c"+
			"  where  1 = 1  "); 
				lVarBind		= new ArrayList(); 
			System.out.println(" qrySentencia " + qrySentencia);
			System.out.println(" lVarBind " + lVarBind); 
			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();			
			while(rs.next()){
			   datos = new HashMap();
				datos.put("CLAVE", rs.getString("CLAVE")==null?"":rs.getString("CLAVE"));
				datos.put("DESCRIPCION", rs.getString("DESCRIPCION")==null?"":rs.getString("DESCRIPCION"));
				registros.add(datos); 
			}
			rs.close();
			ps.close();
		
		} catch (Exception e) {
			exito = false;
			e.printStackTrace();
			System.out.println(" getBusquedaAvanzada  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				System.out.println("   getBusquedaAvanzada (S) ");
			}
		} 
		return registros;
		
	}
	%>
