Ext.onReady(function(){
	var _OPERACION_="I";
	var _mess = "";
	var evento = "";
	var procesaAceptar = function(options, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  jsonData = Ext.JSON.decode(response.responseText);
			var mns = jsonData.msg;
			var evento = jsonData.evento;
			var cadena = Ext.ComponentQuery.query('#id_cadenas')[0].getValue();
			
			Ext.ComponentQuery.query('#CveClasifEpo')[0].setValue('');
			Ext.ComponentQuery.query('#CveEpo')[0].setValue('');
			Ext.ComponentQuery.query('#ic_area')[0].setValue('');
			Ext.ComponentQuery.query('#id_responsable')[0].setValue('');
			
			if(evento=='GUARDAR'){
				
				Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('icoGuardar');
				Ext.ComponentQuery.query('#btnGuardar')[0].enable();
				main.el.mask('Actializando...', 'x-mask-loading');
				Ext.ComponentQuery.query('#grid')[0].hide();
				consultaData.load({
					params: Ext.apply(
					{
							informacion: 'consultar_grid',
							id_cadenas: cadena
					})
				});
			}else if(evento=='ACTUALIZA'){
				
				
				Ext.ComponentQuery.query('#btnActualizar')[0].setIconCls('icoGuardar');
				Ext.ComponentQuery.query('#btnActualizar')[0].enable();
				Ext.Ajax.request({
					url: '15mancatalogoClasificacionEpo01Ext.data.jsp',
						params: Ext.apply({
							informacion:  'consultar_clasificacion',
							id_cadenas:cadena
						}),
						callback: procesarDatosIniciales
				});
			}else if(evento=='ELIMINAR'){
				
				main.el.mask('Actializando...', 'x-mask-loading');
				Ext.ComponentQuery.query('#grid')[0].hide();
				consultaData.load({
					params: Ext.apply(
					{
							informacion: 'consultar_grid',
							id_cadenas: cadena
					})
				});
			}
			_OPERACION_="I";
			_mess = "";
			evento = "";
			Ext.Msg.alert('Aviso','<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>'+mns+'</center></td></tr> </table> </div>'//,
			);
		
		}else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	function limpiar(){
		var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
		Ext.ComponentQuery.query('#CveClasifEpo')[0].setValue('');
		Ext.ComponentQuery.query('#CveEpo')[0].setValue('');
		Ext.ComponentQuery.query('#ic_area')[0].setValue('');
		Ext.ComponentQuery.query('#ic_class_epo')[0].setValue('');
		Ext.ComponentQuery.query('#id_responsable')[0].setValue('');
		Ext.ComponentQuery.query('#id_cadenas')[0].setValue('');
		gridConsulta.hide();
	}
	var _URL_ORIGEN_= '15mancatalogoClasificacionEpo01Ext.jsp';
	function catagoloValor(valor) {
		if (valor == 2){					//Tasa
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
		} else if (valor == 11){		//Estatus Documento
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
		} else if (valor == 12){		//Cambio Estatus
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
		} else if (valor == 13){		//Clasificacion ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
		} else if (valor == 21){		//Subsector
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
		} else if (valor == 33){		//D�as Inh�biles
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
		} else if (valor == 34){		//Productos
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
		} else if(valor == 36){			//Personas Facultadas por Banco
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
		} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
		} else if(valor == 39){			//Version Convenio
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
		} else if(valor == 40){			//Bancos TEF
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
		} else if (valor == 47){		//Firma Cedulas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
		} else if (valor == 49){		//Plazos por Producto
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
		} else if (valor == 60){		//Area Promocion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
		} else if (valor == 62){		//Subdireccion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
		} else if (valor == 63){		//Lider Promotor
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
		} else if (valor == 65){		//Subtipo EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
		} else if (valor == 66){		//Sector EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
		} else if (valor == 68){		//Ventanillas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
		} else if (valor == 70){		//D�as Inh�biles por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
		} else if (valor == 71){		//D�as Inh�biles por EPO por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
		}else if (valor == 70){		//D�as Inh�biles por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
		}else if (valor == 103){		//D�as Inh�biles por EPO por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
		} else {
			window.location  = _URL_ORIGEN_;
		}
	}
	function guardar(boton){
	
		var CveClasifEpo = Ext.ComponentQuery.query('#CveClasifEpo')[0];
		var CveEpo = Ext.ComponentQuery.query('#CveEpo')[0];
		var cboCatalogo = Ext.ComponentQuery.query('#cboCatalogo')[0];
		var id_cadenas = Ext.ComponentQuery.query('#id_cadenas')[0];
		var ic_area = Ext.ComponentQuery.query('#ic_area')[0];
		var classEpo = Ext.ComponentQuery.query('#ic_class_epo')[0];
		var respo = Ext.ComponentQuery.query('#id_responsable')[0];
		if(Ext.isEmpty(ic_area.getValue())) {
			ic_area.markInvalid('Faltan datos');
			ic_area.focus();
			return false;
		}
		if(Ext.isEmpty(respo.getValue())) {
			respo.markInvalid('Por favor selecciona a un Responsable');
			respo.focus();
			return false;
		}
		if(Ext.isEmpty(CveClasifEpo.getValue()) && Ext.isEmpty(CveEpo.getValue())){
			 Ext.ComponentQuery.query('#CveEpo')[0].setValue(id_cadenas.getValue());
			 _OPERACION_="I";
			  _mess="G";
			
		}else{
			 Ext.ComponentQuery.query('#CveEpo')[0].setValue(id_cadenas.getValue());
			 _OPERACION_="M";
			 _mess="";
		}
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15mancatalogoClasificacionEpo01Ext.data.jsp',
			params: Ext.apply({
				informacion:  'Clasificacion.Aceptar',
				operacion	: _OPERACION_,
				_mess	: _mess,
				CveClasifEpo: forma.query('#CveClasifEpo')[0].getValue(),
				CveEpo:    forma.query('#CveEpo')[0].getValue(),
				cboCatalogo:      forma.query('#cboCatalogo')[0].getValue(),
				id_cadenas:    forma.query('#id_cadenas')[0].getValue(),
				ic_class_epo:    forma.query('#ic_class_epo')[0].getValue(),
				ic_area:    forma.query('#ic_area')[0].getValue(),
				id_responsable:      forma.query('#id_responsable')[0].getValue(),
				evento: 'GUARDAR'
			}),
			callback: procesaAceptar
		});
	}
	function modificaInfo(CveClasifEpo, CveEpo, txtArea, txtResponsable){
		_OPERACION_="M";
		_mess ="";
		Ext.ComponentQuery.query('#CveClasifEpo')[0].setValue(CveClasifEpo);
		Ext.ComponentQuery.query('#CveEpo')[0].setValue(CveEpo);
		Ext.ComponentQuery.query('#ic_area')[0].setValue(txtArea);
		Ext.ComponentQuery.query('#id_responsable')[0].setValue('');
	}
	function elimina(CveClasifEpo, CveEpo){
		_OPERACION_="E";
		_mess ="";
		Ext.ComponentQuery.query('#CveClasifEpo')[0].setValue(CveClasifEpo);
		Ext.ComponentQuery.query('#CveEpo')[0].setValue(CveEpo);
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		Ext.Ajax.request({
			url: '15mancatalogoClasificacionEpo01Ext.data.jsp',
			params: Ext.apply({
				informacion:  'Clasificacion.Aceptar',
				operacion	: _OPERACION_,
				_mess	: _mess,
				CveClasifEpo: forma.query('#CveClasifEpo')[0].getValue(),
				CveEpo:    forma.query('#CveEpo')[0].getValue(),
				cboCatalogo:      forma.query('#cboCatalogo')[0].getValue(),
				id_cadenas:    forma.query('#id_cadenas')[0].getValue(),
				ic_class_epo:    forma.query('#ic_class_epo')[0].getValue(),
				ic_area:    forma.query('#ic_area')[0].getValue(),
				id_responsable:      forma.query('#id_responsable')[0].getValue(),
				evento: 'ELIMINAR'
			}),
			callback: procesaAceptar
		});
	}
	function actualizar(boton){
		var classEpo = Ext.ComponentQuery.query('#ic_class_epo')[0];
		var id_cadenas = Ext.ComponentQuery.query('#id_cadenas')[0];
		if(Ext.isEmpty(classEpo.getValue())) {
			classEpo.markInvalid('Faltan datos');
			classEpo.focus();
			return false;
		}
		Ext.ComponentQuery.query('#CveEpo')[0].setValue(id_cadenas.getValue());
		_OPERACION_="I";
		_mess="";
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15mancatalogoClasificacionEpo01Ext.data.jsp',
			params: Ext.apply({
				informacion:  'Clasificacion.Aceptar',
				operacion	: _OPERACION_,
				_mess	: _mess,
				CveClasifEpo: forma.query('#CveClasifEpo')[0].getValue(),
				CveEpo:    forma.query('#CveEpo')[0].getValue(),
				cboCatalogo:      forma.query('#cboCatalogo')[0].getValue(),
				id_cadenas:    forma.query('#id_cadenas')[0].getValue(),
				ic_class_epo:    forma.query('#ic_class_epo')[0].getValue(),
				ic_area:    forma.query('#ic_area')[0].getValue(),
				id_responsable:      forma.query('#id_responsable')[0].getValue(),
				evento: 'ACTUALIZA'
			}),
			callback: procesaAceptar
		});
	}
	var procesarConsultaData = function(store, arrRegistros, success, opts){
		main.el.unmask();
		var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
		if (arrRegistros != null){
			if(store.getTotalCount() > 0){
				gridConsulta.show();
			} else{
				gridConsulta.hide();
				
			}
		}
	}
	var procesarDatosIniciales = function(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var json=Ext.JSON.decode(response.responseText);
			var resul_clas = json.resul_clasificacion;
			Ext.ComponentQuery.query('#ic_class_epo')[0].setValue(resul_clas);
			
		}else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	// Se crea el MODEL para el cat�logo 'Raz�n Social'
	Ext.define('ModelCadenas',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'CLAVE',       type: 'string'},
				{ name: 'DESCRIPCION', type: 'string'}
			]
		}
	);
	Ext.define('ModelCatalogo',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	var catalogoCadenas = Ext.create('Ext.data.Store',{
		model: 'ModelCadenas',
		proxy: {
			type: 'ajax',
			url: '15mancatalogoClasificacionEpo01Ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Catalogo_Cadenas'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: true
	});
	var catalogoResp = Ext.create('Ext.data.Store',{
		model: 'ModelCadenas',
		proxy: {
			type: 'ajax',
			url: '15mancatalogoClasificacionEpo01Ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Catalogo_Responsable'
				
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});
	Ext.define('ListaRegistros',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'IC_CLASIFICACION_EPO'     },
				{name: 'IC_EPO'     },
				{name: 'CG_AREA'     },
				{name: 'CG_RESPONSABLE'     }
			]
		});
	var procesarCatalogo = function(store, arrRegistros, opts) {
		Ext.each(records, function(record, index){
          if(record.get('clave')!=60 && record.get('clave')!=70  && record.get('clave')!=34 && record.get('clave')!=68 && 
				record.get('clave')!=40 && record.get('clave')!=37 && record.get('clave')!=103 && record.get('clave')!=39 && 
				record.get('clave')!=12 && record.get('clave')!=11 && record.get('clave')!=66 && 
				record.get('clave')!=13 && record.get('clave')!=47 && record.get('clave')!=62  &&
				record.get('clave')!=677 && record.get('clave')!=63 && record.get('clave')!=21 && 
				record.get('clave')!=33  && record.get('clave')!=36 && record.get('clave')!=65 && 
				record.get('clave')!=71 && record.get('clave')!=49 && record.get('clave')!=2){
				store.remove(record);
				
			}
      });
		
		store.commitChanges(); 
	}	
	var consultaData = Ext.create('Ext.data.Store',{
		storeId: 'consultaData',
		model: 'ListaRegistros',
		pageSize: 15,
		proxy: {
			type: 'ajax',
			url:  '15mancatalogoClasificacionEpo01Ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'consultar_grid'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		listeners: {
			load: procesarConsultaData
		}
	});
	var procesarCatalogo = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#cboCatalogo')[0].setValue('677');
		}
	}
	var catalogo = Ext.create('Ext.data.Store',{
		model: 'ModelCatalogo',
		storeId:'consultaInfoFinan',
		proxy: {
			type: 'ajax',
			url: '15mancatalogoClasificacionEpo01Ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Catalogo'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		 autoLoad: true
	});
	var formaPrincipal = new Ext.form.FormPanel({
		width:                     600,
		itemId:                    'formaPrincipal',
		title:                     'Clasificaci�n Epo',
		bodyPadding:               '0 0 0 0',
		style:                     'margin: 0px auto 0px auto;',
		frame:                     true,
		border:                    true,
		fieldDefaults: {
			msgTarget:              'side',
			labelWidth:             90
		},
		items:[
			{
				xtype:'hidden',
				itemId: 'CveClasifEpo',
				name:'CveClasifEpo',
				width:                     200
			},
			{
				xtype:'hidden',
				itemId: 'CveEpo',
				name:'CveEpo',
				width:                     200
			},
			{
				xtype: 'combo',
				itemId:  'cboCatalogo',
				fieldLabel: 'Cat�logos',
				name:  'cboCatalogo',
				hiddenName: 'cboCatalogo',
				forceSelection:true,
				allowBlank: false,
				labelWidth: 100,
				emptyText: 'Seleccione...',
				queryMode:				'local',
				displayField:	'descripcion',
				valueField:		'clave',
				typeAhead:		true,
				triggerAction:	'all',
				anchor:			'95%',
				store: Ext.create('Ext.data.Store', {
					model:'ModelCatalogo',
					autoLoad: true,
					proxy: {
						type: 'ajax',
						url: '15mancatalogoClasificacionEpo01Ext.data.jsp',
						reader: {
							type: 'json',
							root: 'registros'
						},
						extraParams: {
							informacion: 'Consulta_Catalogo'
						},
							totalProperty:  'total'
						},
						listeners: {
							load : procesarCatalogo,
							exception: NE.util.mostrarProxyAjaxError
						}
					}),
					listeners: {
						select: {
							fn: function(combo) {
								var valor  = combo.getValue();
								catagoloValor(valor);		
							}
						}
					}
			},
			{
				anchor:                 '95%',
				xtype:                  'combobox',
				itemId:                 'id_cadenas',
				name:                   'id_cadenas',
				hiddenName:             'id_cadenas',
				fieldLabel:             'Cadenas',
				labelWidth: 100,
				allowBlank: false,
				emptyText:              'Seleccione ...',
				displayField:           'DESCRIPCION',
				valueField:             'CLAVE',
				queryMode:              'local',
				triggerAction:          'all',
				listClass:              'x-combo-list-small',
				typeAhead:              true,
				forceSelection:         true,
				hidden:                 false,
				store:                  catalogoCadenas,
				listeners: {
					select: function(combo, record, index) {
						if(combo.getValue()!=''){
							Ext.ComponentQuery.query('#CveClasifEpo')[0].setValue('');
							Ext.ComponentQuery.query('#CveEpo')[0].setValue('');
							Ext.ComponentQuery.query('#ic_area')[0].setValue('');
							Ext.ComponentQuery.query('#id_responsable')[0].setValue('');
							var cadena = combo.getValue();
							Ext.Ajax.request({
								url: '15mancatalogoClasificacionEpo01Ext.data.jsp',
								params: Ext.apply({
									informacion:  'consultar_clasificacion',
									id_cadenas:cadena
								}),
								callback: procesarDatosIniciales
							});
												
							catalogoResp.load({
								params: Ext.apply(
								{
									informacion: 'Catalogo_Responsable',
									id_cadenas: cadena
								})
							});	
							
							consultaData.load({
								params: Ext.apply(
								{
									informacion: 'consultar_grid',
									id_cadenas: cadena
								})
							});	
						}
											
					}
				}
			},
			{
			  xtype: 'fieldcontainer',
			  fieldLabel: 'Clasificaci�n EPO',
			  labelWidth: 100,
			  layout: 'hbox',
			  items: [
					{
						xtype:'textfield',
						itemId: 'ic_class_epo',
						maxLength:100,
						maxLengthText:'<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>El tama�o maximo para este campo es de 100</center></td></tr> </table> </div>',
						name:'ic_class_epo',
						width:                     200,
						allowBlank: true
					},
					{
						xtype:'displayfield',
						width: 10
					},
					{
						xtype:'button',
						text: 'Actualizar',
						iconCls: 'icoActualizar',
						itemId: 'btnActualizar',
						handler:                actualizar
					}
				]
			},
			{
			  xtype: 'fieldcontainer',
			  fieldLabel: '�rea',
			  labelWidth: 100,
			  layout: 'hbox',
			  items: [
					{
						xtype:'textfield',
						itemId: 'ic_area',
						maxLength:150,
						maxLengthText:'<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>El tama�o maximo para este campo es de 150</center></td></tr> </table> </div>',
						name:'ic_area',
						width:                     200,
						allowBlank: true
					}
				]
			},
			{
				anchor:                 '95%',
				xtype:                  'combobox',
				itemId:                 'id_responsable',
				name:                   'id_responsable',
				hiddenName:             'id_responsable',
				fieldLabel:             'Responsable',
				 labelWidth: 100,
				emptyText:              'Seleccione ...',
				displayField:           'DESCRIPCION',
				valueField:             'CLAVE',
				queryMode:              'local',
				triggerAction:          'all',
				listClass:              'x-combo-list-small',
				typeAhead:              true,
				forceSelection:         true,
				hidden:                 false,
				store:                  catalogoResp
			}
		],
		buttons: [{
			text:                   'Guardar',
			itemId:                 'btnGuardar',
			iconCls:                'icoGuardar',
			formBind: true,
			handler:                guardar
		},{
			text:                   'Limpiar',
			itemId:                 'btnLimpiar',
			iconCls:                'icoLimpiar',
			handler:                limpiar
		}]
	});
	var grid = Ext.create('Ext.grid.Panel',{
		selType: 'cellmodel',
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId:           'grid',
		store:            Ext.data.StoreManager.lookup('consultaData'),
		height:           200,
		width:            600,
		margins: '0 20 0 0',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            'Consulta',
		hidden:           true,
		columns: [
			
			{
				dataIndex:     'IC_CLASIFICACION_EPO',
				align:         'left',
				sortable:      true,
				resizable:     true,
				hidden: true
			},
			{
				dataIndex:     'IC_EPO',
				align:         'left',
				sortable:      true,
				resizable:     true,
				hidden: true
			},
			{
				width:         140,
				dataIndex:     'CG_AREA',
				header:        '�rea',
				align:         'left',
				sortable:      true,
				resizable:     true
			},{
				width:         340,
				dataIndex:     'CG_RESPONSABLE',
				header:        'Responsable',
				align:         'left',
				sortable:      true,
				resizable:     true
			},
			{
				width:         100,
				xtype:         'actioncolumn',
				header:        'Acci�n',
				align:         'center',
				sortable:      false,
				resizable:     false,
				menuDisabled:  true,
				items: [
					{
						getClass:   function(value,metadata,record,rowIndex,colIndex,store){
								return 'modificar';
						},
						handler:    function(grid, rowIndex, colIndex){
							var rec = consultaData.getAt(rowIndex);
							var IC_CLASIFICACION_EPO = rec.get('IC_CLASIFICACION_EPO');
							var IC_EPO = rec.get('IC_EPO');
							var CG_AREA = rec.get('CG_AREA');
							var CG_RESPONSABLE = rec.get('CG_RESPONSABLE');
							modificaInfo(IC_CLASIFICACION_EPO,IC_EPO,CG_AREA,CG_RESPONSABLE);
						}
					},
					{
						getClass:   function(value,metadata,record,rowIndex,colIndex,store){
								return 'borrar';
						},
						handler:    function(grid, rowIndex, colIndex){
							var rec = consultaData.getAt(rowIndex);
							var IC_CLASIFICACION_EPO = rec.get('IC_CLASIFICACION_EPO');
							var IC_EPO = rec.get('IC_EPO');
							elimina(IC_CLASIFICACION_EPO,IC_EPO);
						}
					}
				]
			}
		]
	});
	var main = Ext.create('Ext.container.Container',{
		itemId:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		width:      949,
		items: [
			formaPrincipal,
			NE.util.getEspaciador(10),
			grid
		]
	});
});