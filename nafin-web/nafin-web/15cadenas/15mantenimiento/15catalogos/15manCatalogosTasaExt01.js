function cambioCreditoElectronico(check,rowIndex,colIds){
	var gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	if(reg.get('HABILITADOCE') == 'checked'){
		check.checked = true
	}else{
		if(check.checked == true)  {
			reg.set('HABILITADOCE','S');
		}else{
			reg.set('HABILITADOCE','N');
		}	
	}
}
Ext.onReady(function(){



var procesarDatos = function (store,arrRegistros,opts){
	var grid = Ext.getCmp('grid');
	var el = grid.getGridEl();	
	el.unmask();
	if(arrRegistros!=null){
		if (!grid.isVisible()) {
				grid.show();
		}
		if(store.getTotalCount()>0){
			el.unmask();
		}else{
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
	}else{
	
	}
	
}



//-------------------------Store tipoDisponible---------------------------------	
var tipoDisponible = new Ext.data.ArrayStore({
	fields: ['clave', 'descripcion'],
	data : [
		['','Seleccionar'],
		['S','S'],
		['N','N']
	 ]
});	
//-------------------------FIN Store tipoDisponible-----------------------------

//------------------------------------Moneda------------------------------------
var Moneda = Ext.data.Record.create([ 
 {name: "clave", type: "string"}, 
 {name: "descripcion", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//---------------------------------Fin Moneda-----------------------------------

//---------------------------- procesarMoneda-----------------------------------
var procesarMoneda = function(store, arrRegistros, opts) {
	store.insert(0,new Moneda({ 
	clave: "", 
	descripcion: "Seleccionar", 
	loadMsg: ""
 })); 
 
 Ext.getCmp('cmbMoneda').setValue("");
 store.commitChanges(); 
};	
//----------------------------Fin procesarMoneda------------------------------------

//-------------------------StoreComboMoneda---------------------------------
var StoreComboMoneda = new Ext.data.JsonStore({
	id: 'StoreComboMoneda',
	root : 'registros',
	fields : ['clave', 'descripcion'],
	url : '15manCatalogosTasaExt01.data.jsp',
	baseParams: {
		informacion: 'catalogoMoneda'
	},
	totalProperty : 'total',
	autoLoad: true,		
	listeners: {
		load 			: procesarMoneda,
		exception	: NE.util.mostrarDataProxyError,
		beforeload	: NE.util.initMensajeCargaCombo
	}		
});
//------------------------FIN StoreComboMoneda---------------------------------

//----------------------------Fin procesarCatalogo-------------------------------
var procesarCatalogo = function(store, arrRegistros, opts) {
	store.each(function(record){
		if(record.get('clave')==45 || record.get('clave')==6  || record.get('clave')==25 || record.get('clave')==101 || record.get('clave')==5 || record.get('clave')==102 || record.get('clave')==19 || record.get('clave')==7 ||
			record.get('clave')==29 ||  record.get('clave')==38 || record.get('clave')==43  || record.get('clave')==18 || record.get('clave')==8 ||
			record.get('clave')==24 || record.get('clave')==15 || record.get('clave')==41 || record.get('clave')==42 || record.get('clave')==17 || record.get('clave')==100 || record.get('clave')==50 || record.get('clave')==1 ||
			record.get('clave')==35 || record.get('clave')==26 || record.get('clave')==3  || record.get('clave')==52 || record.get('clave')==14 || record.get('clave')==270 ||  record.get('clave')==104 ||
			record.get('clave')==20 || record.get('clave')==61 || record.get('clave')==53 || record.get('clave')==10 || record.get('clave')==67 || record.get('clave')==208 || record.get('clave')==23 || record.get('clave')==22  ||
			record.get('clave')==9  || record.get('clave')==16 || record.get('clave')==64 || record.get('clave')==30 || record.get('clave')==48 || record.get('clave')==31  || record.get('clave')==32 || record.get('clave')==46  ||
			record.get('clave')==51 || record.get('clave')==27|| record.get('clave')==6769){
			store.remove(record);
		}
		});
	Ext.getCmp('cmbCAT').setValue('2');
	store.commitChanges(); 
}	
//----------------------------Fin procesarCatalogo-------------------------------

//--------------------------Store catalogo--------------------------------------
var catalogo = new Ext.data.JsonStore({
	id		: 'catalogos',
	root: 'registros',
	fields: ['clave','descripcion','loadMsg'], 
	url: '15mancatalogoClasificacion01Ext.data.jsp' ,
	baseParams: {
		informacion: 'Clasificacion.Carga.Catalogo'
	},
	totalProperty: 'total',
	autoLoad: true,
	listeners: {
		load : procesarCatalogo,
		exception: NE.util.mostrarDataProxyError,
		beforeload: NE.util.initMensajeCargaCombo
	}		
});
//--------------------------Fin Store catalogo-----------------------------------

//---------------------------- consultaDataGrid----------------------------------
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15manCatalogosTasaExt01.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'TIPOTASA'},
			{name: 'INTERCLAVE'},
			{name: 'CLAVE'},
			{name: 'DESCRIPT'},
			{name: 'DESCRIPT_AUX'},
			{name: 'MONEDA'},
			{name: 'DISPONIBLE'},
			{name: 'DISPONIBLE_AUX'},
			{name: 'IC_MONEDA'},
			{name: 'IC_MONEDA_AUX'},
			{name: 'SOBRETASA'},
			{name: 'SOBRETASA_AUX'},
			{name: 'HABILITADOCE'},
			{name: 'HABILITADOCE_AUX'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarDatos(null, null, null);
				}
			}
		}
	});
//----------------------------Fin consultaDataGrid----------------------------------

//-------------------------------- gridConsulta---------------------------------
var grid = new Ext.grid.GridPanel({//EditorGridPanel
	store: consultaDataGrid,
	style: 'margin:0 auto;',
	id: 'grid',
	title:'Contenido del Cat�logo',
	margins: '20 0 0 0',
	hidden:false,
	//clicksToEdit: 1,			
	style: 'margin:0 auto;',
	enableColumnMove: false,
	enableColumnHide: false,
	height: 450,
	width: 843,
	frame: true,
	loadMask: true,
	tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
					{
						iconCls	: 'icon-register-edit',
						text		: 'Editar Registro',
						disabled	: false,
						handler	: function(){
									var grid	  				= Ext.getCmp("grid");
									var seleccionados  	= grid.getSelectionModel().getSelections();
									if(       seleccionados.length == 0){
										Ext.MessageBox.alert('Aviso','Debe seleccionar un registro.');
										return;
									} else if( seleccionados.length >  1){
										Ext.MessageBox.alert('Aviso','S�lo se puede editar un registro a la vez.');
										return;
									}
									var formTit	  				= Ext.getCmp("forma");
									formTit.setTitle('Editar Cat�logo: Modificar Registro');
									var respuesta 			= new Object();
									respuesta['record']	= seleccionados[0];
									Ext.getCmp('grid').hide();
									Ext.getCmp('forma').show();
									Ext.getCmp('txtClave').setValue(respuesta.record.data.CLAVE);	
									Ext.getCmp('cmbMoneda').setValue(respuesta.record.data.IC_MONEDA);
									Ext.getCmp('tfDescripcion').setValue(respuesta.record.data.DESCRIPT);
									Ext.getCmp('cmbFinan').setValue(respuesta.record.data.DISPONIBLE);
									Ext.getCmp('tfSobretasa').setValue(respuesta.record.data.SOBRETASA);
						}
					},{
						iconCls	: 'icon-register-add',
						text		: 'Agregar Registro',
						disabled	: true,
						handler	: function(){
						}
					},{
						iconCls	: 'icon-register-delete',
						text		: 'Eliminar Registro',
						disabled	: true,
						handler	: function(){}
					},{
						iconCls	: 'icoActualizar',
						text		: 'Actualizar Registros',
						disabled	: false,
						handler	: function(){
									var creditoElectronico = "";
									consultaDataGrid.each(function(record) {
										if (record.get('HABILITADOCE') =='S'){
											creditoElectronico = creditoElectronico + record.get('CLAVE') + ",";
										}
									});
									Ext.Ajax.request({
										url: '15manCatalogosTasaExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Actualizar',
										  cE: creditoElectronico
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var actualizar = info.actualizar;
											if (actualizar){
												  Ext.Msg.show({
												  title: 'Actualizar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('2');
														Ext.getCmp('txtClave').setValue('').hide();
														Ext.getCmp('cmbMoneda').setValue('');
														Ext.getCmp('cmbFinan').setValue('');
														Ext.getCmp('btnAceptar').hide();	
														consultaDataGrid.load();
													 }
												  });//fin Msg
											} else {
												  Ext.Msg.show({
												  title: 'Actualizar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('2');
														Ext.getCmp('txtClave').setValue('').hide();
														Ext.getCmp('cmbMoneda').setValue('');
														Ext.getCmp('cmbFinan').setValue('');		
														Ext.getCmp('btnAceptar').hide();	
														consultaDataGrid.load();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
						}
					}
					
			]
	},
	columns: [
		{
			header: 'Clave',
			tooltip: 'Clave',
			dataIndex: 'CLAVE',
			sortable: true,
			width: 50,			
			resizable: true,				
			align: 'center'				
		},
		{
			header: 'Moneda',
			tooltip: 'Moneda',
			dataIndex: 'MONEDA',
			sortable: true,
			resizable: true,
			width: 180,
			hidden: false,		
			minChars : 1,
			align: 'left'
		},
		{
			header: 'Descripci�n',
			tooltip: 'Descripci�n',
			dataIndex: 'DESCRIPT',
			fixed:		true,
			sortable: true,
			width: 330,
			align: 'left'				
		},
		{
			header: 'Disponible',
			tooltip: 'Descripci�n',
			dataIndex: 'DISPONIBLE',
			sortable: true,
			resizable: true,
			width: 70,
			hidden: false,		
			minChars : 1,
			align: 'center'				
		},
		{
			header: 'Sobretasa',
			tooltip: 'Descripci�n',
			dataIndex: 'SOBRETASA',
			sortable: true,
			resizable: true,
			width: 70,
			align: 'center'				
		},{
    xtype			: 'actioncolumn',
    header			: 'Seleccionar',
    tooltip			: 'Seleccionar',	
	 hidden			: true,
    width			: 70,							
    align			: 'center',
    items			: [
     {
      getClass		: function(valor, metadata, registro, rowIndex, colIndex, store) {
							  this.items[0].tooltip = 'Modificar';
							  return 'icoModificar';										
							},
      handler		: function(grid, rowIndex, colIndex, item, event){var registro = grid.getStore().getAt(rowIndex);
							Ext.getCmp('btnAceptar').show();	
							Ext.getCmp('txtClave').setValue(registro.get('CLAVE')).show();	
							Ext.getCmp('cmbMoneda').setValue(registro.get('IC_MONEDA'));
							Ext.getCmp('tfDescripcion').setValue(registro.get('DESCRIPT'));
							Ext.getCmp('cmbFinan').setValue(registro.get('DISPONIBLE'));
							Ext.getCmp('tfSobretasa').setValue(registro.get('SOBRETASA'));
					}//fin handler
			} ]
		},	{
			header:'Cr�dito Electr�nico',
			dataIndex : 'HABILITADOCE',
			width : 100,
			align: 'center',
			sortable: false,
			hideable: false,
			renderer: function(value, metadata, record, rowIndex, colIndex, store){				;
				if(record.data['HABILITADOCE'] == ''){
						if(record.data['HABILITADOCE']=='checked' ){
							return '<input  id="chkCredito" type="checkbox" checked  onclick="cambioCreditoElectronico(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkCredito" type="checkbox"  onclick="cambioCreditoElectronico(this, '+rowIndex +','+colIndex+');" />';
						}
				
				}else{
					if(record.data['HABILITADOCE'] == 'S'){
						return '<input  id="chkCredito"  type="checkbox" checked  onclick="cambioCreditoElectronico(this, '+rowIndex +','+colIndex+');" />';
					}else{
						return '<input  id="chkCredito" type="checkbox"  onclick="cambioCreditoElectronico(this, '+rowIndex +','+colIndex+');" />';
					}
				}
			}
		}
	],
	bbar: {
		autoScroll:true,
		id: 'barra',
		hidden:true,
		displayInfo: true,
		items: ['->','-',
			{	
				xtype:	'button',
				id:		'btnGrabar',
				text:		'Actualizar',
				iconCls	: 'icoLimpiar',
				width		: 50,
				weight	: 70,
				//handler	: procesarActualizar
				handler	: function (boton, evento){
					var creditoElectronico = "";
					consultaDataGrid.each(function(record) {
						if (record.get('HABILITADOCE') =='S'){
							creditoElectronico = creditoElectronico + record.get('CLAVE') + ",";
						}
					});
					Ext.Ajax.request({
						url: '15manCatalogosTasaExt01.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						  informacion: 'Actualizar',
						  cE: creditoElectronico
						}),
						success : function(response) {
							var info = Ext.util.JSON.decode(response.responseText);
							var actualizar = info.actualizar;
							if (actualizar){
								  Ext.Msg.show({
								  title: 'Actualizar',
								  msg: info.msg,
								  modal: true,
								  icon: Ext.Msg.INFO,
								  buttons: Ext.Msg.OK,
								  fn: function (){
										fp.getForm().reset();
										Ext.getCmp('cmbCAT').setValue('2');
										Ext.getCmp('txtClave').setValue('').hide();
										Ext.getCmp('cmbMoneda').setValue('');
										Ext.getCmp('cmbFinan').setValue('');
										Ext.getCmp('btnAceptar').hide();	
										consultaDataGrid.load();
									 }
								  });//fin Msg
							} else {
								  Ext.Msg.show({
								  title: 'Actualizar',
								  msg: info.msg,
								  modal: true,
								  icon: Ext.Msg.ERROR,
								  buttons: Ext.Msg.OK,
								  fn: function (){
										fp.getForm().reset();
										Ext.getCmp('cmbCAT').setValue('2');
										Ext.getCmp('txtClave').setValue('').hide();
										Ext.getCmp('cmbMoneda').setValue('');
										Ext.getCmp('cmbFinan').setValue('');		
										Ext.getCmp('btnAceptar').hide();	
										consultaDataGrid.load();
									 }
								  });//fin Msg
							}
						 }
						});//fin AJAX
				}//FIN handler
			}]
		}
});
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------elementosForma---------------------------------	
var elementosForma =  [	
  {
  xtype			: 'compositefield',
  id				:'cmpCBHIDEcat',
  combineErrors: false,
  msgTarget		: 'side',
  hidden 		: true,
  items: [
    {
    xtype		: 'combo',
    name			: 'cmbCatalogos',
    hiddenName	: '_cmbCat',
    id			: 'cmbCATHIDEN',	
    fieldLabel	: 'Cat�logos', 
    mode			: 'local',	
    forceSelection : true,	
    triggerAction  : 'all',	
    typeAhead		: true,
    minChars 		: 1,	
    store 			: catalogo,		
    valueField 	: 'clave',
    displayField	: 'descripcion',	
    editable		: true,
    typeAhead		: true,
    width			:  337,
	 listeners		: {
		select		: {
			fn			: function(combo) {
							var valor  = combo.getValue();
							}// FIN fn
							}//FIN select
						}//FIN listeners
    },{
    xtype :'displayfield',
    frame :true,
    border: false,
    value :'',
    width	: 10
    },{
    xtype   : 'button',
    text    : 'Buscar',					
    tooltip :	'Buscar',
    iconCls : 'icoBuscar',
    id      : 'btnBuscar', 
    handler : function(boton, evento) { 
	 var valor = Ext.getCmp('cmbCAT').getValue();
		if (valor == 2){					//Tasa
			consultaDataGrid.load();
			//window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
		} else if (valor == 11){		//Estatus Documento
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
		} else if (valor == 12){		//Cambio Estatus
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
		} else if (valor == 13){		//Clasificacion ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
		} else if (valor == 21){		//Subsector
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
		} else if (valor == 33){		//D�as Inh�biles
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
		} else if (valor == 34){		//Productos
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
		} else if(valor == 36){			//Personas Facultadas por Banco
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
		} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
		} else if(valor == 39){			//Version Convenio
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
		} else if(valor == 40){			//Bancos TEF
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
		} else if (valor == 47){		//Firma Cedulas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
		} else if (valor == 49){		//Plazos por Producto
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
		} else if (valor == 60){		//Area Promocion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
		} else if (valor == 62){		//Subdireccion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
		} else if (valor == 63){		//Lider Promotor
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
		} else if (valor == 65){		//Subtipo EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
		} else if (valor == 66){		//Sector EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
		} else if (valor == 68){		//Ventanillas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
		} else if (valor == 70){		//D�as Inh�biles por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
		}else if (valor == 71){		//D�as Inh�biles EPO por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
		} else if (valor == 103){		//Programa a Fondo JR
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
		}else if (valor == 677){		//Clasificaci�n Epo
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
		}
	 }
    }]			
	},{
	xtype				: 'displayfield',
	id					: 'txtClave',
	fieldLabel		: 'Clave',
	value				: 'Test',
	hidden			: true
	},	{
	xtype				: 'combo',
	id					: 'cmbMoneda',
	name				: 'moneda',
	hiddenName		: 'Hmoneda',
	mode				: 'local',
	fieldLabel		: 'Moneda', 
	displayField 	: 'descripcion',
	valueField 		: 'clave',
	store				: StoreComboMoneda,
	forceSelection : true,
	msgTarget		: 'side',
	margins			: '0 20 0 0',
	triggerAction 	: 'all',   
	allowBlank		: true,
	typeAhead		: true,
	minChars 		: 1,
	anchor			: '95%'	
	},{
	xtype				: 'textfield',
	id        		: 'tfDescripcion',
	name				: 'descricpion',
	fieldLabel 		: 'Descripci�n',
	anchor			: '95%',
	maxLength		: 50,
	margins			: '0 20 0 0',
	listeners		:{
		change		:function(field, newValue){
							field.setValue(newValue.toUpperCase()); 	
							}
						}
  },{
	xtype				: 'combo',
	id					: 'cmbFinan',
	name				: 'disponible',
	mode				: 'local',
	fieldLabel		: 'Disponible', 
	displayField	: 'descripcion',
	store				: tipoDisponible,
	forceSelection : true,
	triggerAction 	: 'all',
	allowBlank		: true,
	valueField		: 'clave',
	typeAhead		: true,
	minChars 		: 1,
	anchor			: '95%'
  },{
	xtype				: 'numberfield',
	id        		: 'tfSobretasa',
	name				: 'sobretasa',
	fieldLabel 		: 'Sobretasa',
	anchor			: '95%',
	maxValue			:	999.9999,
	decimalPrecision: 4,
	margins			: '0 20 0 0',
	msgTarget		: 'side'
  }
];
//-----------------------------Fin elementosForma-------------------------------

//-------------------------------Panel Consulta---------------------------------
var fp = new Ext.form.FormPanel({
	id					: 'forma',
	width				: 843,
	title				: 'Criterios de B�squeda del Cat�logo',
	frame				: true,		
	collapsible		: true,
	titleCollapse	: true,
	hidden			: false,
	style				: 'margin:0 auto;',
	bodyStyle		: 'padding: 6px',
	defaults			: {
		msgTarget	: 'side',
		anchor		: '-20'
							},
	labelWidth		: 130,
	items				: elementosForma,
	buttons			: [
      {
		text			: 'Guardar',
		id				: 'btnGuardar',
		iconCls		: 'icoGuardar',
		hidden		: false,
		handler		: function (boton, evento){
						if (Ext.getCmp('cmbMoneda').getValue() !=""){
								if (Ext.getCmp('tfDescripcion').getValue() !=""){
								 if (Ext.getCmp('tfSobretasa').isValid()){ 
									Ext.Ajax.request({
									url: '15manCatalogosTasaExt01.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									  informacion: 'Modificar',
									  clave: Ext.getCmp('txtClave').getValue(),
									  moneda:Ext.getCmp('cmbMoneda').getValue()
									}),
									success : function(response) {
										var info = Ext.util.JSON.decode(response.responseText);
										var modificar = info.modificar;
										if (modificar){
											  Ext.Msg.show({
											  title: 'Modificar',
											  msg: info.msg,
											  modal: true,
											  icon: Ext.Msg.INFO,
											  buttons: Ext.Msg.OK,
											  fn: function (){
													fp.getForm().reset();
													Ext.getCmp('cmbCAT').setValue('2');
													Ext.getCmp('txtClave').setValue('').hide();
													Ext.getCmp('cmbMoneda').setValue('');
													Ext.getCmp('cmbFinan').setValue('');
													Ext.getCmp('btnAceptar').hide();	
													Ext.getCmp('forma').hide();	
													consultaDataGrid.load();
												 }
											  });//fin Msg
										} else {
											  Ext.Msg.show({
											  title: 'Modificar',
											  msg: info.msg,
											  modal: true,
											  icon: Ext.Msg.ERROR,
											  buttons: Ext.Msg.OK,
											  fn: function (){
													fp.getForm().reset();
													Ext.getCmp('cmbCAT').setValue('2');
													Ext.getCmp('txtClave').setValue('').hide();
													Ext.getCmp('cmbMoneda').setValue('');
													Ext.getCmp('cmbFinan').setValue('');		
													Ext.getCmp('btnAceptar').hide();	
													Ext.getCmp('forma').hide();	
													consultaDataGrid.load();
												 }
											  });//fin Msg
										}
									 }
									});//fin AJAX
								 } else {
									Ext.getCmp('tfSobretasa').focus();
								 }
								} else {
									Ext.getCmp('tfDescripcion').focus();
									Ext.getCmp('tfDescripcion').markInvalid('Ingrese una descripcion.');
								}	
							} else {
								Ext.getCmp('cmbMoneda').focus();
								//Ext.getCmp('cmbMoneda').clearInvalid();
								Ext.getCmp('cmbMoneda').markInvalid('Tiene que seleccionar la moneda.');
							}
			}
		},{
		text			: 'Cancelar',
		id				: 'btnCancelar',
		iconCls		: 'icoCancelar',
		hidden		: false,
		handler		: function (boton, evento){
							fp.getForm().reset();
							Ext.getCmp('cmbCAT').setValue('2');
							Ext.getCmp('forma').hide();	
							Ext.getCmp('grid').show();							
			}
		},{
		text			: 'Aceptar',
		id				: 'btnAceptar',
		iconCls		: 'icoAceptar',
		hidden		: true,
		handler		: function (boton, evento){
							if (Ext.getCmp('cmbMoneda').getValue() !=""){
								if (Ext.getCmp('tfDescripcion').getValue() !=""){
								 if (Ext.getCmp('tfSobretasa').isValid()){ 
									Ext.Ajax.request({
									url: '15manCatalogosTasaExt01.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									  informacion: 'Modificar',
									  clave: Ext.getCmp('txtClave').getValue(),
									  moneda:Ext.getCmp('cmbMoneda').getValue()
									}),
									success : function(response) {
										var info = Ext.util.JSON.decode(response.responseText);
										var modificar = info.modificar;
										if (modificar){
											  Ext.Msg.show({
											  title: 'Modificar',
											  msg: info.msg,
											  modal: true,
											  icon: Ext.Msg.INFO,
											  buttons: Ext.Msg.OK,
											  fn: function (){
													fp.getForm().reset();
													Ext.getCmp('cmbCAT').setValue('2');
													Ext.getCmp('txtClave').setValue('').hide();
													Ext.getCmp('cmbMoneda').setValue('');
													Ext.getCmp('cmbFinan').setValue('');
													Ext.getCmp('btnAceptar').hide();	
													consultaDataGrid.load();
												 }
											  });//fin Msg
										} else {
											  Ext.Msg.show({
											  title: 'Modificar',
											  msg: info.msg,
											  modal: true,
											  icon: Ext.Msg.ERROR,
											  buttons: Ext.Msg.OK,
											  fn: function (){
													fp.getForm().reset();
													Ext.getCmp('cmbCAT').setValue('2');
													Ext.getCmp('txtClave').setValue('').hide();
													Ext.getCmp('cmbMoneda').setValue('');
													Ext.getCmp('cmbFinan').setValue('');		
													Ext.getCmp('btnAceptar').hide();	
													consultaDataGrid.load();
												 }
											  });//fin Msg
										}
									 }
									});//fin AJAX
								 } else {
									Ext.getCmp('tfSobretasa').focus();
								 }
								} else {
									Ext.Msg.show({
									  title: 'Descripcion',
									  msg: 'Ingrese una descripcion.',
									  modal: true,
									  icon: Ext.Msg.INFO,
									  buttons: Ext.Msg.OK,
									  fn: function (){
										Ext.getCmp('tfDescripcion').focus();
										 }
									  });//fin Msg
								}	
							} else {
								Ext.Msg.show({
								  title: 'Moneda',
								  msg: 'Tiene que seleccionar la moneda.',
								  modal: true,
								  icon: Ext.Msg.INFO,
								  buttons: Ext.Msg.OK,
								  fn: function (){
									Ext.getCmp('cmbMoneda').focus();
									 }
								  });//fin Msg
							}
						}//FIN handler Aceptar
		},{
		text			: 'Salir',
		id				: 'btnSalir',
		iconCls		: 'icoContinuar',
		hidden		: true,
		handler		: function (boton, evento){
							window.location = "./15mancatalogo.jsp";
						}//FIN handler Limpiar
		}
 ]
});
//------------------------------Fin Panel Consulta------------------------------

	var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		autoHeight		:true,//height:			80,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		'padding:6px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		items: 			[
			 {
				 xtype		: 'combo',
				 name			: 'cmbCatalogos',
				 hiddenName	: '_cmbCat',
				 id			: 'cmbCAT',	
				 fieldLabel	: 'Cat�logo', 
				 mode			: 'local',	
				 forceSelection : true,	
				 triggerAction  : 'all',	
				 typeAhead		: true,
				 minChars 		: 1,	
				 store 			: catalogo,		
				 valueField 	: 'clave',
				 displayField	: 'descripcion',	
				 editable		: true,
				 typeAhead		: true,
				 anchor			: '95%',
				 listeners		: {
					select		: {
						fn			: function(combo) {
										var valor = Ext.getCmp('cmbCAT').getValue();
										if (valor == 2){					//Tasa
											Ext.getCmp('forma').hide();
											consultaDataGrid.load();
											//window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
										} else if (valor == 11){		//Estatus Documento
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
										} else if (valor == 12){		//Cambio Estatus
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
										} else if (valor == 13){		//Clasificacion ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
										} else if (valor == 21){		//Subsector
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
										} else if (valor == 33){		//D�as Inh�biles
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
										} else if (valor == 34){		//Productos
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
										} else if(valor == 36){			//Personas Facultadas por Banco
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
										} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
										} else if(valor == 39){			//Version Convenio
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
										} else if(valor == 40){			//Bancos TEF
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
										} else if (valor == 47){		//Firma Cedulas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
										} else if (valor == 49){		//Plazos por Producto
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
										} else if (valor == 60){		//Area Promocion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
										} else if (valor == 62){		//Subdireccion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
										} else if (valor == 63){		//Lider Promotor
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
										} else if (valor == 65){		//Subtipo EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
										} else if (valor == 66){		//Sector EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
										} else if (valor == 68){		//Ventanillas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
										} else if (valor == 70){		//D�as Inh�biles por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
										}else if (valor == 71){		//D�as Inh�biles EPO por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
										} else if (valor == 677){		//Clasificaci�n Epo
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
										}else if (valor == 103){		//Programa a Fondo JR
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
										}
										}// FIN fn
										}//FIN select
									}//FIN listeners
				 }],
		monitorValid: 	false
	});

//----------------------------Contenedor Principal------------------------------	
var pnl = new Ext.Container({
	id			: 'contenedorPrincipal',
	applyTo	: 'areaContenido',
	width		: 949,	
	//style		: 'margin:0 auto;',
	items		: [
					NE.util.getEspaciador(20),	
					panelFormaCatalogos,
					NE.util.getEspaciador(20),	
					fp,
					NE.util.getEspaciador(20),
					grid,
					NE.util.getEspaciador(20)
					]
});	
//-----------------------------Fin Contenedor Principal-------------------------
Ext.getCmp('cmbFinan').setValue('');
Ext.getCmp('forma').setVisible(false);
});//-----------------------------------------------Fin Ext.onReady(function(){}