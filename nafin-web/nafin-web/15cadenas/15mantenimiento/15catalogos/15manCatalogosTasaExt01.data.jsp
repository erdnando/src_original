<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.catalogos.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String infoRegresar="", consulta="",consultaGrid="", mensaje ="";
	ConsCatalogoTasas paginado = new ConsCatalogoTasas();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginado);
	JSONObject jsonObj = new JSONObject();
	HashMap datos = new HashMap();
	JSONArray registros1 = new JSONArray();
	JSONObject jsonObjG = new JSONObject();
	
	if (informacion.equals("ConsultaGrid")){
		infoRegresar ="";
		try {
				Registros reg	=	queryHelper.doSearch();
				while(reg.next()){
							if(!(reg.getString("MONEDA").equals("MULTIMONEDA"))&&reg.getString("IC_MONEDA").equals("0")){
								reg.setObject("IC_MONEDA","");
								reg.setObject("IC_MONEDA_AUX","");
							}
							if(reg.getString("MONEDA").equals("MULTIMONEDA")&&reg.getString("IC_MONEDA").equals("0")){
								reg.setObject("IC_MONEDA","-1");
								reg.setObject("IC_MONEDA_AUX","-1");
							}
			}
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString();
			} catch(Exception e) {
				throw new AppException("Error al obtener los datos", e);
			}
	
} else if(informacion.equals("catalogoMoneda")){
		infoRegresar ="";
		List registros = paginado.catMoneda() ;
		for(int i =0; i<registros.size();i++){
			HashMap aux =(HashMap)registros.get(i);
			datos = new HashMap();
			String clave = aux.get("clave").toString();
			String descripcion = aux.get("descripcion").toString();
			if((descripcion.equals("MULTIMONEDA"))&&clave.equals("0")){
				datos.put("clave", "-1");
				datos.put("descripcion", aux.get("descripcion").toString());
			}else{
				datos.put("clave", aux.get("clave").toString());
				datos.put("descripcion", aux.get("descripcion").toString());
			}
			registros1.add(datos);
		}
		consulta =  "{\"success\": true, \"total\": \"" + registros1.size() + "\", \"registros\": " + registros1.toString()+"}";
		jsonObjG = JSONObject.fromObject(consulta);
		infoRegresar = jsonObjG.toString();
		System.out.println("moneda ++++ "+infoRegresar);

} else if(informacion.equals("Actualizar") || informacion.equals("Modificar") ){

	String claves  	= (request.getParameter("cE")!=null) ? request.getParameter("cE"):"";
	String clave  		= (request.getParameter("clave")			!=null) ? request.getParameter("clave"):"";
	String moneda  	= (request.getParameter("moneda")		!=null) ? request.getParameter("moneda"):"";
	String descricpion= (request.getParameter("descricpion")	!=null) ? request.getParameter("descricpion"):"";
	String disponible = (request.getParameter("disponible")	!=null) ? request.getParameter("disponible"):"";
	String sobretasa  = (request.getParameter("sobretasa")	!=null) ? request.getParameter("sobretasa"):"";

	ActualizacionCatalogosBean ac = new ActualizacionCatalogosBean();
	MantenimientoCatalogo mC = new MantenimientoCatalogo ();
	boolean	actu	= true;
	boolean	modi	= true;
	String[] credEle	= null;
	String	clv="";
	int 		elementos=0;
	
	try{
		if(!claves.equals("")){
			clv= claves.substring(0, claves.length()-1);
			credEle = claves.split(",");
		}
		if(informacion.equals("Modificar")){
			if (moneda.equals("-1")){
				moneda ="0";
			}
			if (disponible.equals("Seleccionar")){
				disponible ="";
			}
			if (sobretasa.equals("")){
				sobretasa=null;
			}
		}		
		if(credEle != null) {
			StringBuffer sbNoCredElec = new StringBuffer();
			for(int i=0; i < credEle.length; i++) {
				sbNoCredElec.append(credEle[i].concat(credEle.length-1==i?"":","));
				if(informacion.equals("Actualizar")){
					mC.setTabla(" COMCAT_TASA ");
					mC.setClaveVal(" CS_CREDITOELEC = 'S'");
					mC.setCondicion(" IC_TASA = ?");
					mC.setBind(credEle[i]);		
					ac.actualizarCatalogo(mC);
				}
			}
			if(informacion.equals("Actualizar")){
				mC.setTabla(" COMCAT_TASA ");
				mC.setClaveVal(" CS_CREDITOELEC = 'N'");
				StringBuffer condicion = new StringBuffer("");
				String inicio="";
				for(int i=0; i < credEle.length; i++) {
					if (i<1){
						inicio=credEle[i];
						condicion.append(credEle[i]);
					} else {
						condicion.append(","+credEle[i]);
					}
					
				}
				if (!condicion.equals("")){
					mC.setCondicion(" IC_TASA NOT IN ( "+condicion+" ) AND 1=?");
				} else {
					mC.setCondicion(" IC_TASA NOT IN ( "+inicio+" ) AND 1=?");
				}
				mC.setBind("1");			
				ac.actualizarCatalogo(mC);
				mensaje = "Los registros fueron Actualizados.";
			}
		} else {
			if(informacion.equals("Actualizar")){
				mC.setTabla(" COMCAT_TASA ");
				mC.setClaveVal(" CS_CREDITOELEC = 'N'");
				mC.setCondicion(" 1= ? ");
				mC.setBind("1");		
				ac.actualizarCatalogo(mC);
				mensaje = "Los registros fueron Actualizados.";
			}
		}
		if(informacion.equals("Modificar")){
			mC.setTabla(" COMCAT_TASA ");
			mC.setClaveVal(" CD_NOMBRE='"+descricpion+
							"',IC_MONEDA="+moneda+",CS_DISPONIBLE='"+disponible+
							"', FG_SOBRETASA="+sobretasa);
			mC.setCondicion(" IC_TASA= ? ");
			mC.setBind(clave);		
			ac.actualizarCatalogo(mC);
			mensaje ="el registro fue Actualizado.";
		}
		
	} catch (Exception e) {
		actu	= false;
		modi	= false;
		mensaje = "Ocurrio un error al intentar actualizar los registros.";
		System.err.println("Consultar  Error: " + e);
	} finally {

	}
	JSONObject jsonObj1 = new JSONObject();
	jsonObj1.put("success", new Boolean(true));
	jsonObj1.put("actualizar", new Boolean(actu));
	jsonObj1.put("modificar", new Boolean(modi));
	jsonObj1.put("msg", mensaje);
	infoRegresar = jsonObj1.toString();
	
}

%>	

<%= infoRegresar %>