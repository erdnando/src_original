<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.cadenas.PaginadorGenerico"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion 	= (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String infoRegresar 	= "";
	String consulta		= "";

if(informacion.equals("Consultar")){
	
	PaginadorGenerico pG = new PaginadorGenerico();
	pG.setCampos("t1.ic_subsector AS interclave, t1.ic_subsector AS clave,t1.cd_nombre AS descript, t2.cd_nombre AS sectoreconomico");
	pG.setTabla("comcat_subsector t1, comcat_sector_econ t2");
	pG.setCondicion("t1.ic_sector_econ = t2.ic_sector_econ");
	pG.setOrden("t2.cd_nombre, t1.cd_nombre");
	
	JSONObject jsonObj   = new JSONObject();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pG);
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	infoRegresar = jsonObj.toString();
} 

%>
<%=infoRegresar%>