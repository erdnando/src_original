Ext.onReady(function() {


//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  
var fp = Ext.getCmp('forma');
  fp.el.unmask();							
  var gridConsulta = Ext.getCmp('grid');	
  var el = gridConsulta.getGridEl();		
  if (arrRegistros != null) {
    if (!gridConsulta.isVisible()) {
      gridConsulta.show();
      }				
    if(store.getTotalCount() > 0) {		
      el.unmask();
    } else {		
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
};
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
var consultaData   = new Ext.data.JsonStore({ 
	root 	: 'registros',
	url 	: '15mancatalogoSubsector01Ext.data.jsp',
	baseParams: {
		informacion: 'Consultar'
	},		
	fields: [	
		{	name: 'INTERCLAVE'},
		{	name: 'CLAVE'},
		{	name: 'DESCRIPT'},
		{	name: 'SECTORECONOMICO'}
	],		
	totalProperty : 'total',
	messageProperty: 'msg',
	autoLoad: true,
	listeners: {
		load: procesarConsultaData,
		exception: {
			fn: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				//LLama procesar consulta, para que desbloquee los componentes.
				procesarConsultaData(null, null, null);					
			}
		}
	}		
});
//----------------------------Fin ConsultaData----------------------------------

//----------------------------Fin procesarCatalogo--------------------------------
var procesarCatalogo = function(store, arrRegistros, opts) {
	store.each(function(record){
		if(record.get('clave')==45 || record.get('clave')==6  || record.get('clave')==25 || record.get('clave')==101 || record.get('clave')==5 || record.get('clave')==102 || record.get('clave')==19 || record.get('clave')==7 ||
			record.get('clave')==29 ||  record.get('clave')==38 || record.get('clave')==43  || record.get('clave')==18 || record.get('clave')==8 ||
			record.get('clave')==24 || record.get('clave')==15 || record.get('clave')==41 || record.get('clave')==42 || record.get('clave')==17 || record.get('clave')==100 || record.get('clave')==50 || record.get('clave')==1 ||
			record.get('clave')==35 || record.get('clave')==26 || record.get('clave')==3  || record.get('clave')==52 || record.get('clave')==14 || record.get('clave')==270 ||  record.get('clave')==104 ||
			record.get('clave')==20 || record.get('clave')==61 || record.get('clave')==53 || record.get('clave')==10 || record.get('clave')==67 || record.get('clave')==208 || record.get('clave')==23 || record.get('clave')==22  ||
			record.get('clave')==9  || record.get('clave')==16 || record.get('clave')==64 || record.get('clave')==30 || record.get('clave')==48 || record.get('clave')==31  || record.get('clave')==32 || record.get('clave')==46  ||
			record.get('clave')==51 || record.get('clave')==27|| record.get('clave')==6769){
			store.remove(record);
		}
		});
	Ext.getCmp('cmbCAT').setValue('21')
	store.commitChanges(); 
}	
//----------------------------Fin procesarCatalogo--------------------------------

//--------------------------Store catalogo----------------------------
var catalogo = new Ext.data.JsonStore({
	id					: 'catalogos',
	root				: 'registros',
	fields			: ['clave','descripcion','loadMsg'], 
	url				: '15mancatalogoClasificacion01Ext.data.jsp' ,
	baseParams		: {
		informacion	: 'Clasificacion.Carga.Catalogo'
	},
	totalProperty	: 'total',
	autoLoad			: true,
	listeners		: {
		load 			: procesarCatalogo,
		exception	: 	NE.util.mostrarDataProxyError,
		beforeload	: NE.util.initMensajeCargaCombo
	}		
});
//--------------------------Fin Store catalogo----------------------------

//--------------------------------gridConsulta----------------------------------	
var gridConsulta = new Ext.grid.EditorGridPanel({	
	store				:consultaData,
	id					:'grid',
	margins			:'20 0 0 0',		
	style				:'margin:0 auto;',
	title				:'Contenido del Cat�logo',	
	hidden			:false,			
	displayInfo		: true,		
	emptyMsg			: "No hay registros.",		
	loadMask			: true,
	stripeRows		: true,
	height			: 350,
	width				: 843,
	align				: 'center',
	frame				: true,
	columns			:[
    {
    header			:'Sector Econ�mico',
    tooltip			:'Sector Econ�mico',
    dataIndex		:'SECTORECONOMICO',
    sortable		:true,
    width			:400,			
    resizable		:true,
    align			:'center',
    renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    },{
    header			:'Descripci�n',
    tooltip			:'Descripci�n',
    dataIndex		:'DESCRIPT',
    sortable		:true,
    width			:400,			
    resizable		:true,
    align			:'center',
	 renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    }]   
});	
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------elementosForma---------------------------------	
var elementosForma =  [	
  {
				 xtype		: 'combo',
				 name			: 'cmbCatalogos',
				 hiddenName	: '_cmbCat',
				 id			: 'cmbCAT',	
				 fieldLabel	: 'Cat�logo', 
				 mode			: 'local',	
				 forceSelection : true,	
				 triggerAction  : 'all',	
				 typeAhead		: true,
				 minChars 		: 1,	
				 store 			: catalogo,		
				 valueField 	: 'clave',
				 displayField	: 'descripcion',	
				 editable		: true,
				 typeAhead		: true,
				 anchor			: '95%',
				 listeners		: {
					select		: {
						fn			: function(combo) {
										var valor = Ext.getCmp('cmbCAT').getValue();
										if (valor == 2){					//Tasa
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
										} else if (valor == 11){		//Estatus Documento
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
										} else if (valor == 12){		//Cambio Estatus
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
										} else if (valor == 13){		//Clasificacion ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
										} else if (valor == 21){		//Subsector
											consultaData.load();
											//window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
										} else if (valor == 33){		//D�as Inh�biles
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
										} else if (valor == 34){		//Productos
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
										} else if(valor == 36){			//Personas Facultadas por Banco
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
										} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
										} else if(valor == 39){			//Version Convenio
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
										} else if(valor == 40){			//Bancos TEF
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
										} else if (valor == 47){		//Firma Cedulas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
										} else if (valor == 49){		//Plazos por Producto
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
										} else if (valor == 60){		//Area Promocion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
										} else if (valor == 62){		//Subdireccion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
										} else if (valor == 63){		//Lider Promotor
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
										} else if (valor == 65){		//Subtipo EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
										} else if (valor == 66){		//Sector EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
										} else if (valor == 68){		//Ventanillas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
										} else if (valor == 70){		//D�as Inh�biles por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
										}else if (valor == 71){		//D�as Inh�biles EPO por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
										} else if (valor == 677){		//Clasificaci�n Epo
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
										}else if (valor == 103){		//Programa a Fondo JR
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
										}
										}// FIN fn
										}//FIN select
									}//FIN listeners
				 }
  /*{
  xtype					: 'compositefield',
  id						: 'cmpCB',
  combineErrors		: false,
  msgTarget				: 'side',
  hidden 				: false,
  items: [
    {
    xtype				: 'combo',
    name					: 'cmbCatalogos',
    hiddenName			: '_cmbCat',
    id					: 'cmbCAT',	
    fieldLabel			: 'Cat�logos', 
    mode					: 'local',	
    forceSelection 	: true,	
    triggerAction  	: 'all',	
    typeAhead		: true,
    minChars 		: 1,	
    store 			: catalogo,		
    valueField 	: 'clave',
    displayField: 'descripcion',	
    editable		: true,
    typeAhead		: true,
    width			: 337,
	 listeners		: {
		select		: {
			fn			: function(combo) {
							var valor  = combo.getValue();
							}// FIN fn
							}//FIN select
						}//FIN listeners
    },{
    xtype :'displayfield',
    frame :true,
    border: false,
    value :'',
    width	: 10
    },{
    xtype   : 'button',
    text    : 'Buscar',					
    tooltip :	'Buscar',
    iconCls : 'icoBuscar',
    id      : 'btnBuscar', 
    handler : function(boton, evento) { 
	 var valor = Ext.getCmp('cmbCAT').getValue();
		if (valor == 13){
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
		} else if(valor == 36){
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
		} else if(valor == 21){
			consultaData.load();
		} else if(valor == 37){
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
		} else if(valor == 40){
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
		} else if (valor == 47){
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
		} else if (valor == 49){
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
		} else {
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo.jsp";
		}
	 }
    }]			
  }*/
];
//-----------------------------Fin elementosForma-------------------------------

//-------------------------------Panel Consulta---------------------------------
var fp = new Ext.form.FormPanel({
	id					: 'forma',
	width				: 843,
	title				: 'Cat�logos',
	frame				: true,		
	collapsible		: true,
	titleCollapse	: true,
	style				: 'margin:0 auto;',
	bodyStyle		: 'padding: 6px',
	defaults			: {
							msgTarget: 'side',
							anchor: '-20'
						},
	items				: elementosForma
});
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------	
var pnl = new Ext.Container({
	id			: 'contenedorPrincipal',
	applyTo	: 'areaContenido',
	width		: 949,		
	//style		: 'margin:0 auto;',
	items		: [
					NE.util.getEspaciador(20),	
					fp,
					NE.util.getEspaciador(20),
					gridConsulta,
					NE.util.getEspaciador(20)
					]
});	
//-----------------------------Fin Contenedor Principal-------------------------

});//-----------------------------------------------Fin Ext.onReady(function(){}