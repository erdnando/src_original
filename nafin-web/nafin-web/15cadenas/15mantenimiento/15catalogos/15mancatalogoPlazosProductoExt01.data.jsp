<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*, 
		netropology.utilerias.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.catalogos.*,
		com.netro.cadenas.Consultas,
		com.netro.cadenas.PaginadorGenerico,
		com.netro.cadenas.MantenimientoCatalogo,
		com.netro.model.catalogos.CatalogoSimple"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion 	= (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String infoRegresar  = "";
	String consulta		= "";

 if(informacion.equals("productos")) {
   List conditions =  new ArrayList ();
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_producto_nafin");
	cat.setDistinc(false);
	cat.setCampoClave("ic_producto_nafin");
	cat.setCampoDescripcion("ic_nombre");
	conditions.add(new Integer(1));
	cat.setValoresCondicionIn(conditions);
	List lista = cat.getListaElementos();
	JSONObject	jsonObj  = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("total",	  new Integer( lista.size()) );	
	jsonObj.put("registros", lista);	
   infoRegresar = jsonObj.toString();

} else if(informacion.equals("Consultar")){
	PaginadorGenerico pG = new PaginadorGenerico();
	pG.setCampos("NVL (pn.ic_nombre, ' ') AS producto, p.cg_descripcion, p.in_plazo_dias, p.in_plazo_meses");
	pG.setTabla("comcat_plazo p, comcat_producto_nafin pn");
	pG.setCondicion("p.ic_producto_nafin = pn.ic_producto_nafin(+) AND p.ic_producto_nafin = 1 ");
	pG.setOrden("3, 2, 1");	
	JSONObject jsonObj   = new JSONObject();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pG);
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("Nuevo")){
	
	String producto 	= (request.getParameter("comboProductoHidden") != null) ? request.getParameter("comboProductoHidden") :"";
	String diasDesde 	= (request.getParameter("comboDiasDesdeHidden") != null) ? request.getParameter("comboDiasDesdeHidden") :"";
	String diasHasta 	= (request.getParameter("comboDiasHastaHidden") != null) ? request.getParameter("comboDiasHastaHidden") :"";
	String mensaje		= "";
	boolean bandera	= true;
	JSONObject jsonObj   = new JSONObject();
	Consultas consultas  = new Consultas();
	
	try{
		int meses = (int)(Integer.parseInt(diasHasta)/30);
		String descripcion = diasDesde+"-"+diasHasta;
		consultas.setCampos("NVL (MAX (ic_plazo) + 1, 1)");
		consultas.setTabla("comcat_plazo");
		String str = consultas.getInfoByQuery();
		String clave="";
		if (!str.equals("")){
			String [] newCadena = str.split(",");
			for(int a=0; a<newCadena.length; a++){
				if (a==3){
					clave = newCadena[a];
					break;
				}
			}
		}
		int registro = 0;
		consultas.setCampos("COUNT (1)");
		consultas.setTabla("comcat_plazo");
		consultas.setCondicion("in_plazo_dias ="+diasHasta+" AND in_plazo_inicio ="+diasDesde+" AND ic_producto_nafin = "+producto);
		str=consultas.getInfoByQuery();
		if (!str.equals("")){
			String [] newCadena = str.split(",");
			for(int a=0; a<newCadena.length; a++){
				if (a==2){
					registro = Integer.parseInt(newCadena[a]);
					break;
				}
			}
		}
		if(registro>0) {
			bandera = false;
			mensaje = "No puede registrar nuevamente ese Plazo  "+diasDesde+"-"+diasHasta+" para ese producto.";
		}
		if(bandera){
			MantenimientoCatalogo 		mC = new MantenimientoCatalogo ();
			ActualizacionCatalogosBean ac = new ActualizacionCatalogosBean();
			String values =  "?," + diasHasta +",'"+descripcion+"',"+meses+","+producto+","+diasDesde;
			mC.setTabla("comcat_plazo");
			mC.setCampos("IC_PLAZO,IN_PLAZO_DIAS,CG_DESCRIPCION,IN_PLAZO_MESES,IC_PRODUCTO_NAFIN,IN_PLAZO_INICIO");
			mC.setValues(values);
			mC.setBind(clave);
			ac.insertarEnCatalogo(mC);
			mensaje = "El registro fue agregado.";
		}
	} catch (Exception e) {
		System.err.println("Nuevo  Error: " + e);
	} 
	jsonObj.put("success",  new Boolean(true));
	jsonObj.put("nuevo",    new Boolean(bandera));
	jsonObj.put("msg",    	mensaje);
	infoRegresar = jsonObj.toString();
  
}

%>
<%=infoRegresar%>