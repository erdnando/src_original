<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
	String infoRegresar="", consulta="",consultaGrid="", mensaje ="";
	
	String respuesta="";
	
	JSONObject jsonObj = new JSONObject();
	HashMap datos = new HashMap();
	JSONArray registros1 = new JSONArray();
	JSONObject jsonObjG = new JSONObject();
	if (informacion.equals("DiasInahbilesEpoAnio.Consultar")){
		String claveCadena  = (request.getParameter("catalogoCadenas") != null)?request.getParameter("catalogoCadenas"):"";	
		try {
				ConsCatalogoDiasInhabilesEpoAnio clase = new ConsCatalogoDiasInhabilesEpoAnio();
				clase.setClaveCadena(claveCadena);
				Registros reg	=	clase.getConsultaData();
				
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString();
			} catch(Exception e) {
				throw new AppException("Error al obtener los datos", e);
			}
	}else if (informacion.equals("DiasInahbilesEpoAnio.Aceptar")){//procesa las peticiones de insertar, modificar y eliminar
		String interClave = (request.getParameter("hdnInterClave")!=null)?request.getParameter("hdnInterClave"):""; 
		String claveCadena  = (request.getParameter("catalogoCadenas") != null)?request.getParameter("catalogoCadenas"):"";	
		String descripcion = (request.getParameter("descripcion")!=null)?request.getParameter("descripcion"):""; 
		String dia = (request.getParameter("dia")!=null)?request.getParameter("dia"):""; 
		
		String interClaves[] = request.getParameterValues("interClave");
		String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):""; 
		ConsCatalogoDiasInhabilesEpoAnio clase = new ConsCatalogoDiasInhabilesEpoAnio();
			clase.setInterclave(interClave);
			clase.setClaveCadena(claveCadena);
			clase.setDescripcion(descripcion.toUpperCase() );
			clase.setOperacion(operacion);
			clase.setDia(dia);
			clase.setModifica("T");
			clase.setInterClaves(interClaves);
			clase.setAnio(anio);
		if(operacion.equals("MODIFICAR")){
			boolean exito = clase.insertModificarEliminarData();
			if(exito){
				respuesta="El registro fue Actualizado";
			}else{
				respuesta = "EL DIA INHABIL YA EXISTE";
			}
			JSONObject resultado = new JSONObject();
			resultado.put("success", new Boolean(true));
			resultado.put("msg", respuesta);
			resultado.put("exito", new Boolean(exito));
			infoRegresar = resultado.toString();		
			System.out.println("Aceptar "+infoRegresar);
		} else if(operacion.equals("INSERTAR")){
			boolean exito = clase.insertModificarEliminarData();
			if(exito){
				respuesta = "El registro fue Agregado";
			}else{
				respuesta = "EL DIA INHABIL YA EXISTE";
			}
			JSONObject resultado = new JSONObject();
			resultado.put("success", new Boolean(true));
			resultado.put("msg", respuesta);
			resultado.put("exito", new Boolean(exito));
			infoRegresar = resultado.toString();		
			System.out.println("Aceptar "+infoRegresar);
		}else if(operacion.equals("ELIMINAR")){
			boolean exito = clase.insertModificarEliminarData();
			if(exito){
				respuesta="El registro fue Eliminado";
			}
			JSONObject resultado = new JSONObject();
			resultado.put("success", new Boolean(true));
			resultado.put("msg", respuesta);
			resultado.put("exito", new Boolean(exito));
			infoRegresar = resultado.toString();		
			System.out.println("Aceptar "+infoRegresar);
		}else if (operacion.equals("MODIFICAR_ANIO")){
			boolean exito = clase.insertModificarEliminarData();
			if(exito){
				respuesta="Los Registros fue Actualizado";
			}
			JSONObject resultado = new JSONObject();
			resultado.put("success", new Boolean(true));
			resultado.put("msg", respuesta);
			resultado.put("exito", new Boolean(exito));
			infoRegresar = resultado.toString();
			
		}
	}
	
	log.debug("infoRegresar = <" + infoRegresar + ">"); 
	%>	

<%= infoRegresar %>