<%@ page contentType="application/json;charset=UTF-8"
	import="
	 java.util.*,java.sql.*,
	 net.sf.json.JSONArray,
	 net.sf.json.JSONObject,
	 netropology.utilerias.*,
	 com.netro.catalogos.*,
    com.netro.model.catalogos.*,
	 com.netro.cadenas.Consultas,
    com.netro.cadenas.PaginadorGenerico,
	 com.netro.cadenas.MantenimientoCatalogo,
    com.netro.cadenas.ParametrizacionCatalogos"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
	String informacion 	= (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String infoRegresar  = "";
	String consulta		= "";

 if(informacion.equals("institucionFinanciera")) {
  
	JSONObject jsonObj  = new JSONObject();
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("int_institucion_bancaria");
	cat.setCampoClave("clave_dcat_s");
	cat.setCampoDescripcion("descripcion_dcat_s");
	cat.setOrden("clave_dcat_s");
	List lista = cat.getListaElementos();
	jsonObj.put("registros", lista);
	jsonObj.put("success",  new Boolean(true)); 
	infoRegresar = jsonObj.toString();  
	
 } else if(informacion.equals("bancoServicio")) {
	
	CatalogoFinanciera cat = new CatalogoFinanciera();
	cat.setClave("ic_financiera");
	cat.setDescripcion("cd_nombre");
	cat.setTipo("1");
	List l = cat.getListaElementos();
	JSONObject jsonObj   = new JSONObject();
	jsonObj.put("registros", l);
	jsonObj.put("success",  new Boolean(true)); 
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("carga")){

	PaginadorGenerico pG = new PaginadorGenerico();
	pG.setCampos("bt.ic_bancos_tef AS interclave, bt.ic_bancos_tef AS clave, bt.cd_descripcion AS descript, bt.cc_clave_dcat_s, ib.descripcion_dcat_s, bt.cs_equiv_plaza, fin.ic_financiera, fin.cd_nombre");
	pG.setTabla("comcat_bancos_tef bt, int_institucion_bancaria ib, comcat_financiera fin "); 
	pG.setCondicion(" ib.clave_dcat_s(+) = bt.cc_clave_dcat_s AND bt.ic_bancos_tef = fin.ic_bancos_tef(+) ");
	pG.setOrden("bt.cd_descripcion");		
	JSONObject jsonObj   = new JSONObject();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pG);
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("nuevo") || informacion.equals("modificar") || informacion.equals("Eliminar") ){
   
	String clave 		 = (request.getParameter("id") !=null)		?	request.getParameter("id")	:	"";	
	String plaza       = (request.getParameter("plaza") !=null)	?	request.getParameter("plaza")	:	"";	
	String descr		 = (request.getParameter("descr") != null) ? request.getParameter("descr") :"";
	String clave2 		 = (request.getParameter("clave") != null) ? request.getParameter("clave") :"";
	String financiera  = (request.getParameter("financiera") !=null)	?	request.getParameter("financiera")	:	"";	
	String descripcion = (request.getParameter("descripcion") !=null)	?	request.getParameter("descripcion")	:	"";
	String claveDCAT_S = (request.getParameter("dcats") !=null)	?	request.getParameter("dcats")	:	"";
	
	ActualizacionCatalogosBean ac = new ActualizacionCatalogosBean();
	MantenimientoCatalogo mC = new MantenimientoCatalogo ();
	Consultas  consultas	= new Consultas();
	JSONObject jsonObj = new JSONObject();
	try {
		if (informacion.equals("nuevo")){
			boolean    nuevo   = false;
			consultas.setCampos("nvl(count(*),0) as ClaveCatalogo");
			consultas.setTabla("comcat_bancos_tef");
			consultas.setCondicion("ic_bancos_tef ="+clave2);
			String str = consultas.getInfoByQuery();	
			if (!str.equals("")){
				String [] newCadena = str.split(",");
				int valida=1;
				for(int a=0; a<newCadena.length; a++){
					if(a==2){
						valida = Integer.parseInt(newCadena[a]);
						break;
					}
				}
				if (valida > 0){
					jsonObj.put("existe",  new Boolean(true));
				} else{
					jsonObj.put("existe",  new Boolean(false));
					nuevo=true;
				}
			}
			if(nuevo){				
				mC.setTabla("comcat_bancos_tef");
				mC.setCampos("cd_descripcion,ic_bancos_tef,cc_clave_dcat_s,cs_equiv_plaza");
				mC.setValues("'"+descr+"',?,'"+claveDCAT_S+"','"+plaza+"'");
				mC.setBind(clave2);		
				ac.insertarEnCatalogo(mC);
				mC.setTabla(" comcat_financiera ");
				mC.setCampos("");
				mC.setClaveVal("ic_bancos_tef = "+clave2);
				mC.setCondicion(" ic_financiera= ? ");
				mC.setBind(financiera);		
				ac.actualizarCatalogo(mC);
				jsonObj.put("nuevo",  new Boolean(true));
			} else{
				jsonObj.put("nuevo",  new Boolean(false));
			}
			
		} else if (informacion.equals("modificar")){
		
			mC.setTabla(" comcat_financiera ");
			mC.setClaveVal("ic_bancos_tef = NULL");
			mC.setCondicion(" ic_bancos_tef= ? ");
			mC.setBind(clave);		
			ac.actualizarCatalogo(mC);
			if(!financiera.equals("")){
				mC.setTabla(" comcat_bancos_tef ");
				mC.setClaveVal("cd_descripcion='"+descripcion+"', cc_clave_dcat_s = '"+claveDCAT_S+"', cs_equiv_plaza= '"+plaza+"'");
				mC.setCondicion(" ic_bancos_tef= ? ");
				mC.setBind(clave);		
				ac.actualizarCatalogo(mC);			
				mC.setTabla(" comcat_financiera ");
				mC.setClaveVal("ic_bancos_tef ="+clave);
				mC.setCondicion(" ic_financiera= ? ");
				mC.setBind(financiera);		
				ac.actualizarCatalogo(mC);
				jsonObj.put("modificar",  new Boolean(true));
			}
			
		} else if(informacion.equals("Eliminar")){ 
			mC.setTabla(" comcat_financiera ");
			mC.setClaveVal("ic_bancos_tef = NULL");
			mC.setCondicion(" ic_bancos_tef= ? ");
			mC.setBind(clave);		
			ac.actualizarCatalogo(mC); 
			mC.setTabla(" comcat_bancos_tef ");
			mC.setCondicion(" ic_bancos_tef= ?");
			mC.setBind(clave);
			int regAfectados=ac.eliminarDelCatalogo(mC);
			if(regAfectados==1){
				jsonObj.put("eliminar",  new Boolean(true));
			} else {
				jsonObj.put("eliminar",  new Boolean(false));
			}		
		}
		
	}catch (Exception e){
		jsonObj.put("modificar",  new Boolean(false));		
	}
	jsonObj.put("success",  new Boolean(true));		
	infoRegresar = jsonObj.toString();

} 

%>
<%=infoRegresar%>




