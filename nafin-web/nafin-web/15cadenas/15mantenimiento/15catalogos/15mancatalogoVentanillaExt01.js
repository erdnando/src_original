Ext.onReady(function(){
		
//------------------------------------procesarDatos-----------------------------
var procesarDatos = function (store,arrRegistros,opts){
	var grid = Ext.getCmp('grid');
	var el = grid.getGridEl();
	fp.el.unmask();
	el.unmask();
	if(arrRegistros!=null){
		if (!grid.isVisible()) {
				grid.show();
		}
		if(store.getTotalCount()>0){
			el.unmask();
		}else{
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
	
	} else{
	
	}
	
};
//------------------------------------FIN procesarDatos-------------------------

//------------------------------------Epo------------------------------------
var Epo = Ext.data.Record.create([ 
 {name: "clave", type: "string"}, 
 {name: "descripcion", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//---------------------------------Fin Epo-----------------------------------

//---------------------------- procesarEpo-----------------------------------
var procesarEpo = function(store, arrRegistros, opts) {
	store.insert(0,new Epo({ 
	clave: "", 
	descripcion: "Seleccionar", 
	loadMsg: ""
 })); 
 
 Ext.getCmp('comboEpo').setValue("");
 store.commitChanges(); 
};	
//----------------------------Fin procesarEpo------------------------------------

//-------------------------storeEpo---------------------------------
var storeEpo = new Ext.data.JsonStore({
	id			: 'storeEpo',
	root 		: 'registros',
	fields 	: ['clave', 'descripcion','loadMsg'],
	url 		: '15mancatalogoVentanillaExt01.data.jsp',
	baseParams: {
		informacion: 'comboEpo'
	},
	totalProperty  : 'total',
	autoLoad			: true,		
	listeners		: {
		load 			: procesarEpo,
		exception	: NE.util.mostrarDataProxyError,
		beforeload	: NE.util.initMensajeCargaCombo
	}		
});
//------------------------FIN storeEpo---------------------------------

//----------------------------Fin procesarCatalogo-------------------------------
var procesarCatalogo = function(store, arrRegistros, opts) {
	store.each(function(record){
		if(record.get('clave')==45 || record.get('clave')==6  || record.get('clave')==25 || record.get('clave')==101 || record.get('clave')==5 || record.get('clave')==102 || record.get('clave')==19 || record.get('clave')==7 ||
			record.get('clave')==29 ||  record.get('clave')==38 || record.get('clave')==43  || record.get('clave')==18 || record.get('clave')==8 ||
			record.get('clave')==24 || record.get('clave')==15 || record.get('clave')==41 || record.get('clave')==42 || record.get('clave')==17 || record.get('clave')==100 || record.get('clave')==50 || record.get('clave')==1 ||
			record.get('clave')==35 || record.get('clave')==26 || record.get('clave')==3  || record.get('clave')==52 || record.get('clave')==14 || record.get('clave')==270 ||  record.get('clave')==104 ||
			record.get('clave')==20 || record.get('clave')==61 || record.get('clave')==53 || record.get('clave')==10 || record.get('clave')==67 || record.get('clave')==208 || record.get('clave')==23 || record.get('clave')==22  ||
			record.get('clave')==9  || record.get('clave')==16 || record.get('clave')==64 || record.get('clave')==30 || record.get('clave')==48 || record.get('clave')==31  || record.get('clave')==32 || record.get('clave')==46  ||
			record.get('clave')==51 || record.get('clave')==27|| record.get('clave')==6769){
			store.remove(record);
		}
		});
	Ext.getCmp('cmbCAT').setValue('68');
	store.commitChanges(); 
}	
//----------------------------Fin procesarCatalogo-------------------------------

//--------------------------Store catalogo--------------------------------------
var catalogo = new Ext.data.JsonStore({
	id		: 'catalogos',
	root	: 'registros',
	fields: ['clave','descripcion','loadMsg'], 
	url	: '15mancatalogoClasificacion01Ext.data.jsp' ,
	baseParams: {
		informacion: 'Clasificacion.Carga.Catalogo'
	},
	totalProperty: 'total',
	autoLoad: true,
	listeners: {
		load : procesarCatalogo,
		exception: NE.util.mostrarDataProxyError,
		beforeload: NE.util.initMensajeCargaCombo
	}		
});
//--------------------------Fin Store catalogo-----------------------------------

//---------------------------- consultaDataGrid----------------------------------
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15mancatalogoVentanillaExt01.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'IC_VENTANILLA'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'CG_DESCRIPCION'},
			{name: 'CG_USUARIO'}
		],
		totalProperty 	: 'total',
		messageProperty: 'msg',
		autoLoad			: false,
		listeners		: {
			load			: procesarDatos,
			exception	: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarDatos(null, null, null);
				}
			}
		}
	});
//----------------------------Fin consultaDataGrid----------------------------------

//-------------------------------- gridConsulta---------------------------------
var grid = new Ext.grid.GridPanel({//EditorGridPanel
	store			: consultaDataGrid,
	style			: 'margin:0 auto;',
	id				: 'grid',
	title			:'Contenido del Cat�logo',
	margins		: '20 0 0 0',
	hidden		:true,
	//clicksToEdit: 1,		
	enableColumnMove: false,
	enableColumnHide: false,
	style			: 'margin:0 auto;',
	height		: 450,
	width			: 843,
	frame			: true,
	loadMask		: true,
	tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
				{
					iconCls: 		'icon-register-edit',
					text: 			'Editar Registro',
					handler: function(){
										
										var grid	  				= Ext.getCmp("grid");
										var seleccionados  	= grid.getSelectionModel().getSelections();
										if(       seleccionados.length == 0){
											Ext.MessageBox.alert('Aviso','Debe seleccionar un registro.');
											return;
										} else if( seleccionados.length >  1){
											Ext.MessageBox.alert('Aviso','S�lo se puede editar un registro a la vez.');
											return;
										}
										var formTit	  				= Ext.getCmp("forma");
										formTit.setTitle('Editar Cat�logo: Modificar Registro');
										var respuesta 			= new Object();
										respuesta['record']	= seleccionados[0];										
										Ext.getCmp('tfVentanilla').setValue(respuesta.record.data.CG_DESCRIPCION);
										Ext.getCmp('tfLogin').setValue(respuesta.record.data.CG_USUARIO);
										Ext.getCmp('tfClaveVentanilla').setValue(respuesta.record.data.IC_VENTANILLA);
										Ext.getCmp('tfAccion').setValue(1);
										Ext.getCmp('grid').hide();
										//Ext.getCmp('comboEpo').hide();
										Ext.getCmp('btnGuardar').show();
										Ext.getCmp('btnCancelar').show();
									//	Ext.getCmp('btnLimpiar').hide();
										//Ext.getCmp('btnAceptar').hide();
										//Ext.getCmp('comboEpoHIDE').show();
										Ext.getCmp('tfVentanilla').show();
										Ext.getCmp('tfLogin').show();
										 
										}
				},
				{
					iconCls	: 		'icon-register-add',
					text		: 			'Agregar Registro',
					handler	: function (){
									var formTit	  				= Ext.getCmp("forma");
									formTit.setTitle('Editar Cat�logo: Agregar Registro');
										
									Ext.getCmp('grid').hide();
									Ext.getCmp('btnGuardar').show();
									Ext.getCmp('btnCancelar').show();
									Ext.getCmp('tfVentanilla').show();
									Ext.getCmp('tfLogin').show();
									
					}
				},
				{
					//ref: 				'../removeBtn',
					iconCls: 		'icon-register-delete',
					text: 			'Eliminar Registro',
					handler:	function(){
							var grid	  	 = Ext.getCmp("grid");
							var seleccionados   = grid.getSelectionModel().getSelections(); 
							var numeroRegistros = seleccionados.length;
							var aviso			  = "";
							if(numeroRegistros == 0){
								Ext.MessageBox.alert('Aviso','Debe seleccionar al menos un registro.');
								return;
							} else if(numeroRegistros > 1000){
								Ext.MessageBox.alert('Aviso','S�lo se permiten como m�ximo 1000 registros seleccionados.');
								return;
							} else if(numeroRegistros == 1  ){
								aviso = 'El registro seleccionado ser� eliminado, �Desea usted continuar?';
							} else if(numeroRegistros >  1  ){
								Ext.MessageBox.alert('Aviso','Solo es permitido eliminar un registro.');
								return; 
							}
							var respuesta 			= new Object();
							respuesta['record']	= seleccionados[0];			
							Ext.getCmp('tfClaveVentanilla').setValue(respuesta.record.data.IC_VENTANILLA);
							Ext.Msg.show({
							  title: 'Eliminar',
							  msg: 'Esta seguro de querer eliminar el registro ?',
							  modal: true,
							  icon: Ext.Msg.QUESTION,
							  buttons: Ext.Msg.OKCANCEL,
							  fn: function (btn,text){
									if (btn == 'ok'){
										Ext.Ajax.request({
										url: '15mancatalogoVentanillaExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Eliminar',
										  ventana: Ext.getCmp('tfVentanilla').getValue(),
										  login	: Ext.getCmp('tfLogin').getValue(),
										  clvVen : Ext.getCmp('tfClaveVentanilla').getValue()
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var eliminar = info.eliminar;
											if (eliminar){
												  Ext.Msg.show({
												  title: 'Eliminar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('68');
														Ext.getCmp('comboEpo').setValue("");
														//Ext.getCmp('btnLimpiar').hide();	
														//Ext.getCmp('btnAceptar').hide();
														Ext.getCmp('grid').hide();
													 }
												  });//fin Msg
											} else {
												  Ext.Msg.show({
												  title: 'Eliminar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('68');
														Ext.getCmp('comboEpo').setValue("");
														//Ext.getCmp('btnLimpiar').hide();	
														//Ext.getCmp('btnAceptar').hide();
														Ext.getCmp('grid').hide();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
									} else {
										Ext.getCmp('tfClaveVentanilla').reset();
									}
								 }
							  });//fin Msg
					}
				}
			]
		},
	columns: [
	{
		header	: 'EPO',
		tooltip	: 'EPO',
		dataIndex: 'CG_RAZON_SOCIAL',
		sortable	: true,
		width		: 400,			
		resizable: true,				
		align		: 'center',
		renderer : function(value){
						return "<div align='left'>"+value+"</div>"									
					}
	}, {
		header	: 'Nombre Ventanilla',
		tooltip	: 'Nombre Ventanilla',
		dataIndex: 'CG_DESCRIPCION',
		sortable	: true,
		resizable: true,
		width		: 200,
		hidden	: false,	
		align		: 'center'
	},	{
		header	: 'Usuario',
		tooltip	: 'Usuario',
		dataIndex: 'CG_USUARIO',
		fixed		:	false,
		sortable	: true,
		width		: 100,
		align		: 'center'				
	}, {
    xtype			: 'actioncolumn',
    header			: 'Seleccionar',
    tooltip			: 'Seleccionar',	
    width			: 100,							
    align			: 'center',
	 hidden			:true,
    items			: [
     {
      getClass		: function(valor, metadata, registro, rowIndex, colIndex, store) {
							  this.items[0].tooltip = 'Modificar';
							  return 'icoModificar';										
							},
      handler		: function(grid, rowIndex, colIndex, item, event){var registro = grid.getStore().getAt(rowIndex);
							Ext.getCmp('tfVentanilla').setValue(registro.get('CG_DESCRIPCION'));
							Ext.getCmp('tfLogin').setValue(registro.get('CG_USUARIO'));
							Ext.getCmp('tfClaveVentanilla').setValue(registro.get('IC_VENTANILLA'));
							Ext.getCmp('tfAccion').setValue(1);
					}//fin handler
			},{
      getClass		: function(valor, metadata, registro, rowIndex, colIndex, store) {
							  this.items[0].tooltip = 'Eliminar';
							  return 'icoCancelar';										
							},
      handler		: function(grid, rowIndex, colIndex, item, event){var registro = grid.getStore().getAt(rowIndex);
							Ext.getCmp('tfClaveVentanilla').setValue(registro.get('IC_VENTANILLA'));
							Ext.Msg.show({
							  title: 'Eliminar',
							  msg: 'Esta seguro de querer eliminar el registro ?',
							  modal: true,
							  icon: Ext.Msg.QUESTION,
							  buttons: Ext.Msg.OKCANCEL,
							  fn: function (btn,text){
									if (btn == 'ok'){
										Ext.Ajax.request({
										url: '15mancatalogoVentanillaExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Eliminar',
										  ventana: Ext.getCmp('tfVentanilla').getValue(),
										  login	: Ext.getCmp('tfLogin').getValue(),
										  clvVen : Ext.getCmp('tfClaveVentanilla').getValue()
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var eliminar = info.eliminar;
											if (eliminar){
												  Ext.Msg.show({
												  title: 'Eliminar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('68');
														Ext.getCmp('comboEpo').setValue("");
														//Ext.getCmp('btnLimpiar').hide();	
														//Ext.getCmp('btnAceptar').hide();
														Ext.getCmp('grid').hide();
													 }
												  });//fin Msg
											} else {
												  Ext.Msg.show({
												  title: 'Eliminar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('68');
														Ext.getCmp('comboEpo').setValue("");
														//Ext.getCmp('btnLimpiar').hide();	
														//Ext.getCmp('btnAceptar').hide();
														Ext.getCmp('grid').hide();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
									} else {
										Ext.getCmp('tfClaveVentanilla').reset();
									}
								 }
							  });//fin Msg
					}//fin handler
			} ]
		}
	],
	bbar: {
		autoScroll:true,
		id: 'barra',
		displayInfo: true
	}
});
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------elementosForma---------------------------------	
var elementosForma =  [	
  {
  xtype			: 'compositefield',
  id				:'cmpCBHIDE',
  combineErrors: false,
  msgTarget		: 'side',
  hidden 		: true,
  items: [
    {
    xtype		: 'combo',
    name			: 'cmbCatalogos',
    hiddenName	: '_cmbCat',
    id			: 'cmbCATHIDE',	
    fieldLabel	: 'Cat�logos', 
    mode			: 'local',	
    forceSelection : true,	
    triggerAction  : 'all',	
    typeAhead		: true,
    minChars 		: 1,	
    store 			: catalogo,		
    valueField 	: 'clave',
    displayField	: 'descripcion',	
    editable		: true,
    typeAhead		: true,
    width			:  337,
	 listeners		: {
		select		: {
			fn			: function(combo) {
							var valor  = combo.getValue();
							}// FIN fn
							}//FIN select
						}//FIN listeners
    },{
    xtype :'displayfield',
    frame :true,
    border: false,
    value :'',
    width	: 10
    },{
    xtype   : 'button',
    text    : 'Buscar',					
    tooltip :	'Buscar',
    iconCls : 'icoBuscar',
    id      : 'btnBuscar', 
    handler : function(boton, evento) { 
	 var valor = Ext.getCmp('cmbCAT').getValue();
		if (valor == 2){					//Tasa
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
		} else if (valor == 11){		//Estatus Documento
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
		} else if (valor == 12){		//Cambio Estatus
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
		} else if (valor == 13){		//Clasificacion ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
		} else if (valor == 21){		//Subsector
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
		} else if (valor == 33){		//D�as Inh�biles
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
		} else if (valor == 34){		//Productos
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
		} else if(valor == 36){			//Personas Facultadas por Banco
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
		} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
		} else if(valor == 39){			//Version Convenio
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
		} else if(valor == 40){			//Bancos TEF
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
		} else if (valor == 47){		//Firma Cedulas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
		} else if (valor == 49){		//Plazos por Producto
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
		} else if (valor == 60){		//Area Promocion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
		} else if (valor == 62){		//Subdireccion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
		} else if (valor == 63){		//Lider Promotor
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
		} else if (valor == 65){		//Subtipo EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
		} else if (valor == 66){		//Sector EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
		} else if (valor == 68){		//Ventanillas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
		} else if (valor == 70){		//D�as Inh�biles por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
		}else if (valor == 71){		//D�as Inh�biles EPO por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
		} else if (valor == 103){		//Programa a Fondo JR
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
		}else if (valor == 677){		//Clasificaci�n Epo
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
		}
	 }
    }]			
	},	{
	xtype				: 'combo',
	id					: 'comboEpoHIDE',
	name				: 'epo',
	hiddenName		: 'hEpo',
	mode				: 'local',
	fieldLabel		: 'EPO HIDEN', 
	displayField 	: 'descripcion',
	valueField 		: 'clave',
	store				: storeEpo,
	hidden			: true,
	forceSelection : true,
	triggerAction 	: 'all',   
	allowBlank		: true,
	typeAhead		: true,
	anchor			: '95%',
	tpl				: NE.util.templateMensajeCargaComboConDescripcionCompleta,
	minChars 		: 1,
	listeners		: {
		select		: {
			fn			: function(combo) {
							/*Ext.getCmp('tfVentanilla').reset();
							Ext.getCmp('tfLogin').reset();
							Ext.getCmp('tfClaveVentanilla').reset();
							Ext.getCmp('tfAccion').reset();
							Ext.getCmp('btnLimpiar').show();
							Ext.getCmp('btnAceptar').show();
							var valor  = combo.getValue();
							if(valor==""){return;}
							fp.el.mask('Cargando...', 'x-mask-loading');	
							consultaDataGrid.load({
							params		:Ext.apply(fp.getForm().getValues(),{
							informacion	:'ConsultaGrid'
											})
									});*/									
							}// FIN fn
							}//FIN select
						}//FIN listeners
   },{
	xtype				: 'combo',
	id					: 'comboEpo',
	name				: 'epo',
	hiddenName		: 'hEpo',
	mode				: 'local',
	fieldLabel		: 'EPO', 
	displayField 	: 'descripcion',
	valueField 		: 'clave',
	store				: storeEpo,
	forceSelection : true,
	triggerAction 	: 'all',   
	allowBlank		: true,
	typeAhead		: true,
	anchor			: '95%',
	tpl				: NE.util.templateMensajeCargaComboConDescripcionCompleta,
	minChars 		: 1,
	listeners		: {
		select		: {
			fn			: function(combo) {
							var valor  = combo.getValue();
							if(valor==""){return;}
							Ext.getCmp('btnGuardar').hide();
							Ext.getCmp('btnCancelar').hide();
							Ext.getCmp('tfVentanilla').hide();
							Ext.getCmp('tfLogin').hide();
							Ext.getCmp('tfVentanilla').reset();
							Ext.getCmp('tfLogin').reset();
							Ext.getCmp('tfClaveVentanilla').reset();
							Ext.getCmp('tfAccion').reset();
							//Ext.getCmp('btnLimpiar').show();
							//Ext.getCmp('btnAceptar').show();
							fp.el.mask('Cargando...', 'x-mask-loading');	
							consultaDataGrid.load({
							params		:Ext.apply(fp.getForm().getValues(),{
							informacion	:'ConsultaGrid'
											})
									});									
							}// FIN fn
							}//FIN select
						}//FIN listeners
  },{
	xtype				: 'textfield',
	id        		: 'tfVentanilla', 
	name				: 'ventanilla',
	fieldLabel 		: 'Nombre Ventanilla',
	anchor			: '95%',
	maxLength		: 50,
	margins			: '0 20 0 0',
	hidden			: true
  },{
	xtype				: 'textfield',
	id        		: 'tfLogin',
	name				: 'login',
	fieldLabel 		: 'Login',
	anchor			: '95%',
	maxLength		: 10,
	margins			: '0 20 0 0',
	hidden			: true
  },{
	xtype				: 'textfield',
	id        		: 'tfClaveVentanilla',
	name				: 'claveVentanilla',
	fieldLabel 		: 'claveVentanilla',
	width				: 150,
	maxLength		: 10,
	margins			: '0 20 0 0',
	hidden			: true
  },{
	xtype				: 'textfield',
	id        		: 'tfAccion',
	name				: 'accion',
	fieldLabel 		: 'Acci�n',
	width				: 150,
	maxLength		: 10,
	margins			: '0 20 0 0',
	hidden			: true
  }
];
//-----------------------------Fin elementosForma-------------------------------

//-------------------------------Panel Consulta---------------------------------
var fp = new Ext.form.FormPanel({
	id					: 'forma',
	width				: 843,
	title				: 'Criterios de B�squeda del Cat�logo',
	frame				: true,
	collapsible		: true,
	titleCollapse	: true,
	style				: 'margin:0 auto;',
	bodyStyle		: 'padding: 6px',
	defaults			: {
		msgTarget	: 'side',
		anchor		: '-20'
							},
	labelWidth		: 130,
	items				: elementosForma,
	buttons			: [
      {
		text			: 'Guardar',
		id				: 'btnGuardar',
		iconCls		: 'icoGuardar',
		hidden		: true,
		handler		: function (boton, evento){
							if (Ext.getCmp('tfVentanilla').getValue() !=""){
								if (Ext.getCmp('tfLogin').getValue() !=""){
									if(Ext.getCmp('tfAccion').getValue() !=1){
										if (Ext.getCmp('tfVentanilla').isValid()){
											if (Ext.getCmp('tfLogin').isValid()) {
												Ext.Ajax.request({
												url: '15mancatalogoVentanillaExt01.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
												  informacion: 'Agregar',
												  ventana: Ext.getCmp('tfVentanilla').getValue(),
												  login	: Ext.getCmp('tfLogin').getValue(),
												  clvVen : Ext.getCmp('tfClaveVentanilla').getValue()
												}),
												success : function(response) {
													var info = Ext.util.JSON.decode(response.responseText);
													var agregar = info.agregar;
													if (agregar){
														  Ext.Msg.show({
														  title: 'Agregar',
														  msg: info.msg,
														  modal: true,
														  icon: Ext.Msg.INFO,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fp.getForm().reset();
																Ext.getCmp('cmbCAT').setValue('68');
																Ext.getCmp('comboEpo').setValue("");
																//Ext.getCmp('btnLimpiar').hide();	
																//Ext.getCmp('btnAceptar').hide();
																Ext.getCmp('grid').hide();
																Ext.getCmp('tfVentanilla').hide();
																Ext.getCmp('tfLogin').hide();
																Ext.getCmp('btnGuardar').hide();
																Ext.getCmp('btnCancelar').hide();
//																Ext.getCmp('comboEpo').show();
																var formTit	  				= Ext.getCmp("forma");
																formTit.setTitle('Criterios de B�squeda del Cat�logo');
															 }
														  });//fin Msg
													} else {
														  Ext.Msg.show({
														  title: 'Agregar',
														  msg: info.msg,
														  modal: true,
														  icon: Ext.Msg.ERROR,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fp.getForm().reset();
																Ext.getCmp('cmbCAT').setValue('68');
																Ext.getCmp('comboEpo').setValue("");
																//Ext.getCmp('btnLimpiar').hide();	
																//Ext.getCmp('btnAceptar').hide();
																Ext.getCmp('grid').hide();
																Ext.getCmp('tfVentanilla').hide();
																Ext.getCmp('tfLogin').hide();
																Ext.getCmp('btnGuardar').hide();
																Ext.getCmp('btnCancelar').hide();
																//Ext.getCmp('comboEpo').show();
																var formTit	  				= Ext.getCmp("forma");
																formTit.setTitle('Criterios de B�squeda del Cat�logo');
															 }
														  });//fin Msg
													}
												 }
												});//fin AJAX
											} else {
												Ext.getCmp('tfLogin').focus();
											}
										} else {
											Ext.getCmp('tfVentanilla').focus();
										}
										
									} else {
										if (Ext.getCmp('tfVentanilla').isValid()){
											if (Ext.getCmp('tfLogin').isValid()) {
												Ext.Ajax.request({
												url: '15mancatalogoVentanillaExt01.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
												  informacion: 'Modificar',
												  ventana: Ext.getCmp('tfVentanilla').getValue(),
												  login	: Ext.getCmp('tfLogin').getValue(),
												  clvVen : Ext.getCmp('tfClaveVentanilla').getValue()
												}),
												success : function(response) {
													var info = Ext.util.JSON.decode(response.responseText);
													var agregar = info.agregar;
													if (agregar){
														  Ext.Msg.show({
														  title: 'Actualizar',
														  msg: info.msg,
														  modal: true,
														  icon: Ext.Msg.INFO,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fp.getForm().reset();
																Ext.getCmp('cmbCAT').setValue('68');
																Ext.getCmp('comboEpo').setValue("");
																//Ext.getCmp('btnLimpiar').hide();	
																//Ext.getCmp('btnAceptar').hide();
																Ext.getCmp('grid').hide();
																Ext.getCmp('tfVentanilla').hide();
																Ext.getCmp('tfLogin').hide();
																Ext.getCmp('btnGuardar').hide();
																Ext.getCmp('btnCancelar').hide();
																Ext.getCmp('comboEpo').show();
																var formTit	  				= Ext.getCmp("forma");
																formTit.setTitle('Criterios de B�squeda del Cat�logo');
															 }
														  });//fin Msg
													} else {
														  Ext.Msg.show({
														  title: 'Actualizar',
														  msg: info.msg,
														  modal: true,
														  icon: Ext.Msg.ERROR,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fp.getForm().reset();
																Ext.getCmp('cmbCAT').setValue('68');
																Ext.getCmp('comboEpo').setValue("");
																//Ext.getCmp('btnLimpiar').hide();	
																//Ext.getCmp('btnAceptar').hide();
																Ext.getCmp('grid').hide();
																Ext.getCmp('grid').hide();
																Ext.getCmp('tfVentanilla').hide();
																Ext.getCmp('tfLogin').hide();
																Ext.getCmp('btnGuardar').hide();
																Ext.getCmp('btnCancelar').hide();
																Ext.getCmp('comboEpo').show();
																var formTit	  				= Ext.getCmp("forma");
																formTit.setTitle('Criterios de B�squeda del Cat�logo');
															 }
														  });//fin Msg
													}
												 }
												});//fin AJAX
											} else {
												Ext.getCmp('tfLogin').focus();
											}
										} else {
											Ext.getCmp('tfVentanilla').focus();
										}
										
									}
									
								} else {
									Ext.getCmp('tfLogin').focus();
									Ext.getCmp('tfLogin').markInvalid('Debe introducir el Login del usuario.');
									/*Ext.Msg.show({
									  title	: 'Login',
									  msg		: 'Debe introducir el Login del usuario.',
									  modal	: true,
									  icon	: Ext.Msg.INFO,
									  buttons: Ext.Msg.OK,
									  fn		: function (){
													Ext.getCmp('tfLogin').focus();
										 }
									  });//fin Msg
									  */
								}	
							} else {
								Ext.getCmp('tfVentanilla').focus();
								Ext.getCmp('tfVentanilla').markInvalid('Debe introducir una Descripci�n.');
								/*Ext.Msg.show({
								  title	: 'Ventanilla',
								  msg		: 'Debe introducir una Descripci�n.',
								  modal	: true,
								  icon	: Ext.Msg.INFO,
								  buttons: Ext.Msg.OK,
								  fn		: function (){
												Ext.getCmp('tfVentanilla').focus();
									 }
								  });//fin Msg
								  */
							}
						
						}//FIN handler Guardar
		},{
		text			: 'Cancelar',
		id				: 'btnCancelar',
		iconCls		: 'icoCancelar',
		hidden		: true,
		handler		: function (boton, evento){
							var formTit	  				= Ext.getCmp("forma");
								 formTit.setTitle('Criterios de B�squeda del Cat�logo');
										
							Ext.getCmp('tfVentanilla').reset();
							Ext.getCmp('tfLogin').reset();
							Ext.getCmp('tfClaveVentanilla').reset();
							Ext.getCmp('tfAccion').reset();
							
							Ext.getCmp('grid').show();
							Ext.getCmp('comboEpo').show();
							Ext.getCmp('btnGuardar').hide();
							Ext.getCmp('btnCancelar').hide();
							//Ext.getCmp('btnLimpiar').show();
							//Ext.getCmp('btnAceptar').show();
							//Ext.getCmp('comboEpoHIDE').show();
							Ext.getCmp('tfVentanilla').hide();
							Ext.getCmp('tfLogin').hide();
						}//FIN handler Cancelar
		},{
		text			: 'Limpiar',
		id				: 'btnLimpiar',
		iconCls		: 'icoLimpiar',
		hidden		: true,
		handler		: function (boton, evento){
							Ext.getCmp('tfVentanilla').reset();
							Ext.getCmp('tfLogin').reset();
							Ext.getCmp('tfClaveVentanilla').reset();
							Ext.getCmp('tfAccion').reset();
						}//FIN handler Limpiar
		},{
		text			: 'Aceptar',
		id				: 'btnAceptar',
		iconCls		: 'icoAceptar',
		hidden		: true,
		handler		: function (boton, evento){
							if (Ext.getCmp('tfVentanilla').getValue() !=""){
								if (Ext.getCmp('tfLogin').getValue() !=""){
									if(Ext.getCmp('tfAccion').getValue() !=1){
										if (Ext.getCmp('tfVentanilla').isValid()){
											if (Ext.getCmp('tfLogin').isValid()) {
												Ext.Ajax.request({
												url: '15mancatalogoVentanillaExt01.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
												  informacion: 'Agregar',
												  ventana: Ext.getCmp('tfVentanilla').getValue(),
												  login	: Ext.getCmp('tfLogin').getValue(),
												  clvVen : Ext.getCmp('tfClaveVentanilla').getValue()
												}),
												success : function(response) {
													var info = Ext.util.JSON.decode(response.responseText);
													var agregar = info.agregar;
													if (agregar){
														  Ext.Msg.show({
														  title: 'Agregar',
														  msg: info.msg,
														  modal: true,
														  icon: Ext.Msg.INFO,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fp.getForm().reset();
																Ext.getCmp('cmbCAT').setValue('68');
																Ext.getCmp('comboEpo').setValue("");
																Ext.getCmp('btnLimpiar').hide();	
																//Ext.getCmp('btnAceptar').hide();
																Ext.getCmp('grid').hide();
															 }
														  });//fin Msg
													} else {
														  Ext.Msg.show({
														  title: 'Agregar',
														  msg: info.msg,
														  modal: true,
														  icon: Ext.Msg.ERROR,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fp.getForm().reset();
																Ext.getCmp('cmbCAT').setValue('68');
																Ext.getCmp('comboEpo').setValue("");
																Ext.getCmp('btnLimpiar').hide();	
																//Ext.getCmp('btnAceptar').hide();
																Ext.getCmp('grid').hide();
															 }
														  });//fin Msg
													}
												 }
												});//fin AJAX
											} else {
												Ext.getCmp('tfLogin').focus();
											}
										} else {
											Ext.getCmp('tfVentanilla').focus();
										}
										
									} else {
										if (Ext.getCmp('tfVentanilla').isValid()){
											if (Ext.getCmp('tfLogin').isValid()) {
												Ext.Ajax.request({
												url: '15mancatalogoVentanillaExt01.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
												  informacion: 'Modificar',
												  ventana: Ext.getCmp('tfVentanilla').getValue(),
												  login	: Ext.getCmp('tfLogin').getValue(),
												  clvVen : Ext.getCmp('tfClaveVentanilla').getValue()
												}),
												success : function(response) {
													var info = Ext.util.JSON.decode(response.responseText);
													var agregar = info.agregar;
													if (agregar){
														  Ext.Msg.show({
														  title: 'Actualizar',
														  msg: info.msg,
														  modal: true,
														  icon: Ext.Msg.INFO,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fp.getForm().reset();
																Ext.getCmp('cmbCAT').setValue('68');
																Ext.getCmp('comboEpo').setValue("");
																Ext.getCmp('btnLimpiar').hide();	
																//Ext.getCmp('btnAceptar').hide();
																Ext.getCmp('grid').hide();
															 }
														  });//fin Msg
													} else {
														  Ext.Msg.show({
														  title: 'Actualizar',
														  msg: info.msg,
														  modal: true,
														  icon: Ext.Msg.ERROR,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fp.getForm().reset();
																Ext.getCmp('cmbCAT').setValue('68');
																Ext.getCmp('comboEpo').setValue("");
																Ext.getCmp('btnLimpiar').hide();	
																//Ext.getCmp('btnAceptar').hide();
																Ext.getCmp('grid').hide();
															 }
														  });//fin Msg
													}
												 }
												});//fin AJAX
											} else {
												Ext.getCmp('tfLogin').focus();
											}
										} else {
											Ext.getCmp('tfVentanilla').focus();
										}
										
									}
									
								} else {
									Ext.Msg.show({
									  title	: 'Login',
									  msg		: 'Debe introducir el Login del usuario.',
									  modal	: true,
									  icon	: Ext.Msg.INFO,
									  buttons: Ext.Msg.OK,
									  fn		: function (){
													Ext.getCmp('tfLogin').focus();
										 }
									  });//fin Msg
								}	
							} else {
								Ext.Msg.show({
								  title	: 'Ventanilla',
								  msg		: 'Debe introducir una Descripci�n.',
								  modal	: true,
								  icon	: Ext.Msg.INFO,
								  buttons: Ext.Msg.OK,
								  fn		: function (){
												Ext.getCmp('tfVentanilla').focus();
									 }
								  });//fin Msg
							}
						}//FIN handler Aceptar
		}
 ]
});
//------------------------------Fin Panel Consulta------------------------------

	var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		autoHeight:true,//height:			80,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		'padding:6px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		items: 			[
			 {
				 xtype		: 'combo',
				 name			: 'cmbCatalogos',
				 hiddenName	: '_cmbCat',
				 id			: 'cmbCAT',	
				 fieldLabel	: 'Cat�logo', 
				 mode			: 'local',	
				 forceSelection : true,	
				 triggerAction  : 'all',	
				 typeAhead		: true,
				 minChars 		: 1,	
				 store 			: catalogo,		
				 valueField 	: 'clave',
				 displayField	: 'descripcion',	
				 editable		: true,
				 typeAhead		: true,
				 anchor			: '95%',
				 listeners		: {
					select		: {
						fn			: function(combo) {
										var valor = Ext.getCmp('cmbCAT').getValue();
										if (valor == 2){					//Tasa
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
										} else if (valor == 11){		//Estatus Documento
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
										} else if (valor == 12){		//Cambio Estatus
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
										} else if (valor == 13){		//Clasificacion ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
										} else if (valor == 21){		//Subsector
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
										} else if (valor == 33){		//D�as Inh�biles
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
										} else if (valor == 34){		//Productos
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
										} else if(valor == 36){			//Personas Facultadas por Banco
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
										} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
										} else if(valor == 39){			//Version Convenio
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
										} else if(valor == 40){			//Bancos TEF
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
										} else if (valor == 47){		//Firma Cedulas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
										} else if (valor == 49){		//Plazos por Producto
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
										} else if (valor == 60){		//Area Promocion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
										} else if (valor == 62){		//Subdireccion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
										} else if (valor == 63){		//Lider Promotor
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
										} else if (valor == 65){		//Subtipo EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
										} else if (valor == 66){		//Sector EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
										} else if (valor == 68){		//Ventanillas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
										} else if (valor == 70){		//D�as Inh�biles por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
										}else if (valor == 71){		//D�as Inh�biles EPO por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
										} else if (valor == 103){		//Programa a Fondo JR
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
										}else if (valor == 677){		//Clasificaci�n Epo
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
										}
										}// FIN fn
										}//FIN select
									}//FIN listeners
				 }],
		monitorValid: 	false
	});	
	
//----------------------------Contenedor Principal------------------------------	
var pnl = new Ext.Container({
	id			: 'contenedorPrincipal',
	applyTo	: 'areaContenido',
	width		: 949,
	autoHeight:true,
	//style		: 'margin:0 auto;',
	items		: [
					NE.util.getEspaciador(20),	
					panelFormaCatalogos,
					NE.util.getEspaciador(20),	
					fp,
					NE.util.getEspaciador(20),
					grid,
					NE.util.getEspaciador(20)
					]
});	
//-----------------------------Fin Contenedor Principal-------------------------

});//-----------------------------------------------Fin Ext.onReady(function(){}

