function cambioCreditoElectronico(check,rowIndex,colIds){
	var gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	if(reg.get('HABILITADOCE') == 'checked'){
		check.checked = true
	}else{
		if(check.checked == true)  {
			reg.set('HABILITADOCE','S');
		}else{
			reg.set('HABILITADOCE','N');
		}	
	}
}
Ext.onReady(function(){
	var	chkCE =[];
	var   listMoneda = [];
	var 	listDescrip = [];
	var 	listDispo = [];
	var 	listTasa =[];
	var 	clave = [];
	var 	chkCEAnt =[];
	var	listMonedaAnt = [];
	var	listDescripAnt = [];
	var  	listDispoAnt = [];
	var  	listTasaAnt =[];
	var varGlob = function  (){
		chkCEAnt =[];
		listMonedaAnt = [];
		listDescripAnt = [];
	   listDispoAnt = [];
	   listTasaAnt =[];
		chkCE =[];
		listMoneda = [];
		listDescrip = [];
	   listDispo = [];
	   listTasa =[];
		clave = [];
	}
//-------------Handlers--------------------------------------------------------------	
	var procesaResultado= function(opts, success, response){
		var fpDatos = Ext.getCmp('grid');
		fpDatos.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			if (info != null){	
				Ext.MessageBox.alert('Mensaje',info.mensaje);
				consultaDataGrid.load();
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');
		var el = grid.getGridEl();	
		el.unmask();
		if(arrRegistros!=null){
			if (!grid.isVisible()) {
					grid.show();
			}
			if(store.getTotalCount()>0){
				Ext.getCmp('btnGrabar').enable();
				el.unmask();
			}else{
				Ext.getCmp('btnGrabar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}else{
			Ext.getCmp('btnGrabar').disable();
		}
		
	}
	var procesarActualizar = function(store, arrRegistros, opts){
		var estatusAnterior = new Array();	
		var  gridConsulta = Ext.getCmp('grid');
		var store = gridConsulta.getStore();
		var columnModelGrid = gridConsulta.getColumnModel();
		var jsonData = store.data.items;
		var numRegistros=0;
		var errorValidacion = false;
		varGlob();
		Ext.each(jsonData, function(item,inx,arrItem){
			chkCE.push(item.data.HABILITADOCE);	
			listMoneda.push(item.data.IC_MONEDA);
			listDescrip.push(item.data.DESCRIPT);
			listDispo.push(item.data.DISPONIBLE);
			listTasa.push(item.data.SOBRETASA);
			clave.push(item.data.CLAVE);
			chkCEAnt.push(item.data.HABILITADOCE_AUX);	
			listMonedaAnt.push(item.data.IC_MONEDA_AUX);
			listDescripAnt.push(item.data.DESCRIPT_AUX);
			listDispoAnt.push(item.data.DISPONIBLE_AUX);
			listTasaAnt.push(item.data.SOBRETASA_AUX);
			numRegistros++;
		});
		var valor = true ;	
		store.each(function(record) {
			numReg = store.indexOf(record);
			if((record.data['HABILITADOCE'] != record.data['HABILITADOCE_AUX']) &&(record.data['IC_MONEDA_AUX'] != record.data['IC_MONEDA'] || record.data['SOBRETASA_AUX'] != record.data['SOBRETASA'] || record.data['DISPONIBLE_AUX'] != record.data['DISPONIBLE'] || record.data['DESCRIPT_AUX'] != record.data['DESCRIPT'] )){	
				if(record.data['DESCRIPT']==''){
					Ext.MessageBox.alert('Error de validaci�n','Faltan datos',
					function(){
						gridConsulta.startEditing(numReg, columnModelGrid.findColumnIndex('DESCRIPT'));
					});
					valor= false ;
					return false;
				}
				if(record.data['IC_MONEDA']==''){
					Ext.MessageBox.alert('','Tiene que seleccionar la moneda...',
					function(){
						gridConsulta.startEditing(numReg, columnModelGrid.findColumnIndex('IC_MONEDA'));
					});
					valor= false ;
					return false;
				}
			}else if((record.data['HABILITADOCE'] == record.data['HABILITADOCE_AUX']) &&(record.data['IC_MONEDA_AUX'] != record.data['IC_MONEDA'] || record.data['SOBRETASA_AUX'] != record.data['SOBRETASA'] || record.data['DISPONIBLE_AUX'] != record.data['DISPONIBLE'] || record.data['DESCRIPT_AUX'] != record.data['DESCRIPT'] )){	
				if(record.data['DESCRIPT']==''){
					Ext.MessageBox.alert('Error de validaci�n','Faltan datos',
					function(){
						gridConsulta.startEditing(numReg, columnModelGrid.findColumnIndex('DESCRIPT'));
					});
					valor = false ;
					return false;
				}
				if(record.data['IC_MONEDA']==''){
					Ext.MessageBox.alert('','Tiene que seleccionar la moneda...',
					function(){
						gridConsulta.startEditing(numReg, columnModelGrid.findColumnIndex('IC_MONEDA'));
					});
					valor = false ;
					return false;
				}
			}
		});
		if(valor != false){
			grid.el.mask('Guardando....', 'x-mask-loading')
			Ext.Ajax.request({
				url: '15manCatalogoTasaExt01.data.jsp',
				params: {
					informacion: 'Guardar_Datos',
					chkCE : chkCE,
					listMoneda : listMoneda, 
					listDescrip : listDescrip,
					listDispo : listDispo,
					listTasa : listTasa, 
					chkCEAnt : chkCEAnt, 
					listMonedaAnt : listMonedaAnt, 
					listDescripAnt : listDescripAnt, 
					listDispoAnt : listDispoAnt,
					listTasaAnt : listTasaAnt,
					numeroRegistros : numRegistros,
					clave : clave
				},
				callback: procesaResultado
			});	
		}
	}
	var tipoDisponible = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['S','S'],
			['N','N ']
		 ]
	});
	var StoreComboMoneda = new Ext.data.JsonStore({
		id: 'StoreComboMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '15manCatalogoTasaExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var comboLineas = new Ext.form.ComboBox({  
		id: 'cmbFinan',
		mode: 'local',
		displayField: 'descripcion',
		store:tipoDisponible,
		forceSelection : true,
		triggerAction : 'all',
		allowBlank: true,
		valueField: 'clave',
		typeAhead: true,
		minChars : 1
	});
	Ext.util.Format.comboRenderer = function(comboLineas){
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			if(value!=''){
				var record = comboLineas.findRecord(comboLineas.valueField, value);
				return record ? record.get(comboLineas.displayField) : comboLineas.valueNotFoundText;
			}				
		}
	}
	var comboMoneda = new Ext.form.ComboBox({  
		id: 'cmbMoneda',
		mode: 'local',
		displayField : 'descripcion',
		valueField : 'clave',
		store:StoreComboMoneda,
		forceSelection : true,
		triggerAction : 'all',   
		allowBlank: true,
		typeAhead: true,
		minChars : 1
	});
	Ext.util.Format.comboRendererM = function(comboMoneda){
		return function(value,metadata,registro,rowIndex,colIndex,store){	
			if(value!=""){
				var record = comboMoneda.findRecord(comboMoneda.valueField, value);
				return record ? record.get(comboMoneda.displayField) : comboMoneda.valueNotFoundText;
			}				
		}
	}
//-------------Stores-----------------------------------------------------------	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15manCatalogoTasaExt01.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'TIPOTASA'},
			{name: 'INTERCLAVE'},
			{name: 'CLAVE'},
			{name: 'DESCRIPT'},
			{name: 'DESCRIPT_AUX'},
			{name: 'MONEDA'},
			{name: 'DISPONIBLE'},
			{name: 'DISPONIBLE_AUX'},
			{name: 'IC_MONEDA'},
			{name: 'IC_MONEDA_AUX'},
			{name: 'SOBRETASA'},
			{name: 'SOBRETASA_AUX'},
			{name: 'HABILITADOCE'},
			{name: 'HABILITADOCE_AUX'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarDatos(null, null, null);
				}
			}
		}
	});
//--------------Componentes-----------------------------------------------------	
	var grid = new Ext.grid.EditorGridPanel({
		store: consultaDataGrid,
		style: 'margin:0 auto;',
		id: 'grid',
		margins: '20 0 0 0',
		hidden:false,
		clicksToEdit: 1,
		columns: [
			{
				header: 'Clave',
				tooltip: 'Clave',
				dataIndex: 'CLAVE',
				sortable: true,
				width: 50,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
			   dataIndex: 'IC_MONEDA',
				sortable: true,
				resizable: true,
				width: 150,
				hidden: false,		
				minChars : 1,
				align: 'left',
				editor:comboMoneda,
				renderer: Ext.util.Format.comboRendererM(comboMoneda)
			},
			{
				header: 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex: 'DESCRIPT',
				fixed:		true,
				sortable: true,
				width: 200,
				align: 'left',
				editor: {
					xtype: 'textarea',
					maxLength : 50
				},
				renderer:function(value,metadata,registro){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}				
			},
			{
				header: 'Disponible',
				tooltip: 'Descripci�n',
				dataIndex: 'DISPONIBLE',
				sortable: true,
				resizable: true,
				width: 100,
				hidden: false,		
				minChars : 1,
				align: 'center',
				editor:comboLineas,
				renderer: Ext.util.Format.comboRenderer(comboLineas)					
			},
			{
				header: 'Sobretasa',
				tooltip: 'Descripci�n',
				dataIndex: 'SOBRETASA',
				sortable: true,
				resizable: true,
				width: 100,
				align: 'center',
				editor: {
					xtype: 'numberfield'
				},
				renderer:function(value,metadata,registro){
					if(registro.get('SOBRETASA')!=0){
						return NE.util.colorCampoEdit(value,metadata,registro);
					}
				}				
			},
			{
				header:'Cr�dito Electr�nico',
				dataIndex : 'HABILITADOCE',
				width : 50,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				;
					if(record.data['HABILITADOCE'] == ''){
							if(record.data['HABILITADOCE']=='checked' ){
								return '<input  id="chkCredito" type="checkbox" checked  onclick="cambioCreditoElectronico(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkCredito" type="checkbox"  onclick="cambioCreditoElectronico(this, '+rowIndex +','+colIndex+');" />';
							}
					
					}else{
						if(record.data['HABILITADOCE'] == 'S'){
							return '<input  id="chkCredito"  type="checkbox" checked  onclick="cambioCreditoElectronico(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkCredito" type="checkbox"  onclick="cambioCreditoElectronico(this, '+rowIndex +','+colIndex+');" />';
						}
					}
				}
			}
		],
		loadMask: true,
		bbar: {
			autoScroll:true,
			id: 'barra',
			displayInfo: true,
			items: ['->','-',
				{	
					xtype:	'button',
					id:		'btnGrabar',
					text:		'Actualizar',
					iconCls: 'icoGuardar ',
					width: 50,
					weight: 70,
					handler: procesarActualizar
				},{	
					xtype:	'button',
					id:		'btnSalir',
					text:		'Salir',
					iconCls: 'icoLimpiar',
					width: 50,
					weight: 70,
					style: 'margin:0 auto;',
					handler: function (boton,evento){
						window.location = '15manCatalogoTasaExt01.jsp'
					},
					align:'rigth'
				}]
			},			
			style: 'margin:0 auto;',
			height: 450,
			width: 690,
			frame: true
	});
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		align: 'center',
		items: grid
		
	});
	StoreComboMoneda.load();
});