<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, 
    com.netro.model.catalogos.*,
	 com.netro.catalogos.*,
    com.netro.cadenas.*,
    netropology.utilerias.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject "
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
	String informacion 	= (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String infoRegresar = "";

 if(informacion.equals("catalogoif")) {
  String rbGroup 	= (request.getParameter("rbGroup") != null) ? request.getParameter("rbGroup") :"";
  
  if(!"C".equals(rbGroup)){
  JSONObject jsonObj  = new JSONObject();
	CatalogoIF	catIf 	= new CatalogoIF();
	catIf.setClave("ic_if");
	catIf.setDescripcion("cg_razon_social");
	catIf.setG_orden("2");
	List lis = catIf.getListaElementosGral();
	jsonObj.put("registros", lis);
	jsonObj.put("success",  new Boolean(true)); 
  infoRegresar = jsonObj.toString();  
  }else{
    CatalogoSimple catCliExterno = new CatalogoSimple();
    catCliExterno.setTabla("comcat_cli_externo i");
    catCliExterno.setCampoClave("i.ic_nafin_electronico");
    catCliExterno.setCampoDescripcion("i.cg_razon_social"); 
    catCliExterno.setOrden("i.ic_nafin_electronico");
    infoRegresar = catCliExterno.getJSONElementos();
  }
  
  
	  
  
} else if(informacion.equals("Consultar")){

  ParametrizacionCatalogos param = new ParametrizacionCatalogos();
  JSONObject jsonObj  = new JSONObject();
  Registros   layout = param.catalogo();
  jsonObj.put("registros", layout.getJSONData() );
  infoRegresar = jsonObj.toString();
  
} else if (informacion.equals("Persona")){
  
  String apaterno 	= (request.getParameter("apaterno") != null) ? request.getParameter("apaterno") :"";
  String amaterno 	= (request.getParameter("amaterno") != null) ? request.getParameter("amaterno") :"";
  String nombre 	= (request.getParameter("nombre") != null) ? request.getParameter("nombre") :"";
  String puesto 	= (request.getParameter("puesto") != null) ? request.getParameter("puesto") :"";
  String cliExt	= (request.getParameter("cliExt") != null) ? request.getParameter("cliExt") :"";
  
  ParametrizacionCatalogos param = new ParametrizacionCatalogos();
  JSONObject jsonObj = new JSONObject();
  String miRespuesta ="";
  
  param.setAPaterno(apaterno);
  param.setAMaterno(amaterno);
  param.setNombre(nombre);
  param.setPuesto(puesto);
  param.setTipoPersona((!cliExt.equals(""))?"C":"");
  String persona = param.persona();
  miRespuesta="{\"success\": true,\"persona\": \""+persona+"\" "+"}";
  jsonObj = JSONObject.fromObject(miRespuesta); 
  infoRegresar = jsonObj.toString(); 

} else if (informacion.equals("Actualiza")){
  String cedula 	= (request.getParameter("cedula") != null) ? request.getParameter("cedula") :"";
  String interm 	= (request.getParameter("interm") != null) ? request.getParameter("interm") :"";
  String intermcombo 	= (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") :"";
  String titulo 	= (request.getParameter("cmbTitulo") != null) ? request.getParameter("cmbTitulo") :"";
  String apaterno = (request.getParameter("tfPat") != null) ? request.getParameter("tfPat") :"";
  String amaterno = (request.getParameter("tfMat") != null) ? request.getParameter("tfMat") :"";
  String nombre 	= (request.getParameter("tfNom") != null) ? request.getParameter("tfNom") :"";
  String puesto 	= (request.getParameter("tfPues") != null) ? request.getParameter("tfPues") :"";
  String habilita	= (request.getParameter("rbGroupHab") != null) ? request.getParameter("rbGroupHab") :"";
  String razon	= (request.getParameter("razon") != null) ? request.getParameter("razon") :"";
  String rbGroup	= (request.getParameter("rbGroup") != null) ? request.getParameter("rbGroup") :"";
  
  ParametrizacionCatalogos param = new ParametrizacionCatalogos();
  JSONObject jsonObj = new JSONObject();
  String miRespuesta ="";
  
  param.setTitulo(titulo);
  param.setCedula(cedula);
  param.setHabilitado(habilita);
  param.setIntermediario(intermcombo);
  param.setAPaterno(apaterno);
  param.setAMaterno(amaterno);
  param.setNombre(nombre);
  param.setPuesto(puesto);
  param.setTipoPersona(rbGroup);
  
  String persona = param.actualiza();
  miRespuesta="{\"success\": true,\"persona\": \""+persona+"\" "+"}";
  jsonObj = JSONObject.fromObject(miRespuesta); 
  infoRegresar = jsonObj.toString(); 

}else if (informacion.equals("Nuevo")){
  String cedula 	= (request.getParameter("cedula") != null) ? request.getParameter("cedula") :"";
  String interm 	= (request.getParameter("interm") != null) ? request.getParameter("interm") :"";
  String intermcombo 	= (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") :"";
  String titulo 	= (request.getParameter("cmbTitulo") != null) ? request.getParameter("cmbTitulo") :"";
  String apaterno = (request.getParameter("tfPat") != null) ? request.getParameter("tfPat") :"";
  String amaterno = (request.getParameter("tfMat") != null) ? request.getParameter("tfMat") :"";
  String nombre 	= (request.getParameter("tfNom") != null) ? request.getParameter("tfNom") :"";
  String puesto 	= (request.getParameter("tfPues") != null) ? request.getParameter("tfPues") :"";
  String habilita	= (request.getParameter("rbGroupHab") != null) ? request.getParameter("rbGroupHab") :"";
  String razon	= (request.getParameter("razon") != null) ? request.getParameter("razon") :"";
  String rbGroup	= (request.getParameter("rbGroup") != null) ? request.getParameter("rbGroup") :"";
  
  ParametrizacionCatalogos param = new ParametrizacionCatalogos();
  JSONObject jsonObj = new JSONObject();
  String miRespuesta ="";
  
  param.setTitulo(titulo);
  param.setCedula(cedula);
  param.setHabilitado(habilita);
  param.setIntermediario(intermcombo);
  param.setAPaterno(apaterno);
  param.setAMaterno(amaterno);
  param.setNombre(nombre);
  param.setPuesto(puesto);
  param.setTipoPersona(rbGroup);
  
  String persona = param.nuevo();
  miRespuesta="{\"success\": true,\"persona\": \""+persona+"\" "+"}";
  jsonObj = JSONObject.fromObject(miRespuesta); 
  infoRegresar = jsonObj.toString(); 

} else if (informacion.equals("Eliminar")){
	String idCedula 	= "0";
	String id = (request.getParameter("id") != null) ? request.getParameter("id") :"";
	ActualizacionCatalogosBean ac = new ActualizacionCatalogosBean();
	MantenimientoCatalogo mC = new MantenimientoCatalogo ();
	JSONObject jsonObj = new JSONObject();
	try {
		mC.setTabla(" comcat_firma_cedula ");
		mC.setCondicion(" ic_firma_cedula= ?");
		mC.setBind(id);
		int regAfectados=ac.eliminarDelCatalogo(mC);
		if(regAfectados==1){
			jsonObj.put("eliminar",  new Boolean(true));
		} else {
			jsonObj.put("eliminar",  new Boolean(false));
		}	
	}catch (Exception e){
		jsonObj.put("eliminar",  new Boolean(false));		
	}
	jsonObj.put("success",  new Boolean(true));		
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>
