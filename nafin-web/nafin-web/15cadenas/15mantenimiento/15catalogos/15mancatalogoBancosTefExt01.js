Ext.onReady(function(){

//----------------------------Fin procesarCatalogo--------------------------------
	var procesarCatalogo = function(store, arrRegistros, opts) {
		store.each(function(record){
		if(record.get('clave')==1 || record.get('clave')==10  || record.get('clave')==20 || record.get('clave')==29 || record.get('clave')==41 || record.get('clave')==48|| record.get('clave')==64 || record.get('clave')==270 ||
			record.get('clave')==3 || record.get('clave')==14 || record.get('clave')==22 || record.get('clave')==30 || record.get('clave')==42 || record.get('clave')==50  || record.get('clave')==67 || record.get('clave')==6769 ||
			record.get('clave')==5 || record.get('clave')==15 || record.get('clave')==23 || record.get('clave')==31 || record.get('clave')==43 || record.get('clave')==51 || record.get('clave')==100 ||  record.get('clave')==208 ||
			record.get('clave')==6 || record.get('clave')==16 || record.get('clave')==24  || record.get('clave')==32 || record.get('clave')==45 || record.get('clave')==52 || record.get('clave')==101 || 
			record.get('clave')==7 || record.get('clave')==17 || record.get('clave')==25 || record.get('clave')==35 || record.get('clave')==45 || record.get('clave')==53 || record.get('clave')==102 || 
			record.get('clave')==8  || record.get('clave')==18 || record.get('clave')==26 || record.get('clave')==38 || record.get('clave')==46 || record.get('clave')==61  || record.get('clave')==104 || 
			record.get('clave')==9 || record.get('clave')==19 || record.get('clave')==27){
			store.remove(record);
		}
		});
		Ext.getCmp('cmbCAT').setValue('40')
		store.commitChanges(); 
	}	
//----------------------------Fin procesarCatalogo--------------------------------
//--------------------------Store catalogo----------------------------
var catalogo = new Ext.data.JsonStore({  
		id		: 'catalogos',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: '15mancatalogoClasificacion01Ext.data.jsp' ,
		baseParams: {
			informacion: 'Clasificacion.Carga.Catalogo'
		},
		totalProperty: 'total',
		autoLoad: true,
		listeners: {
			load : procesarCatalogo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
//--------------------------Fin Store catalogo----------------------------

//------------------------------------Financiera-------------------------------------
var Financiera = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin Financiera------------------------------------

//---------------------------- procesarInstitucionFinanciera--------------------------------
	var procesarInstitucionFinanciera = function(store, arrRegistros, opts) {
		store.insert(0,new Financiera({ 
		clave: "-1", 
		descripcion: "Seleccionar Instituci�n Financiera", 
		loadMsg: ""}));
		Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
		store.commitChanges(); 
	}	
//----------------------------Fin procesarInstitucionFinanciera--------------------------------

//------------------------------institucionFinanciera-------------------------------------
	var institucionFinanciera = new Ext.data.JsonStore({  
		id: 'institucionFinancieraStore',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15mancatalogoBancosTefExt01.data.jsp',
		baseParams: {
			informacion: 'institucionFinanciera'
		},
		totalProperty: 'total',
		autoLoad: true,
		listeners: {
			load: procesarInstitucionFinanciera,
			exception: NE.util.mostrarDataProxyError//,
			//beforeload: NE.util.initMensajeCargaCombo
		}		
	});
//------------------------------Fin institucionFinanciera---------------------------------

//------------------------------------Financiera-------------------------------------
var Servicio = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin Financiera------------------------------------

//---------------------------- procesarBancoServicio--------------------------------
	var procesarBancoServicio = function(store, arrRegistros, opts) {
		store.insert(0,new Servicio({ 
		clave: "-1", 
		descripcion: "Seleccionar Banco de Servicio", 
		loadMsg: ""}));
		Ext.getCmp('combobancoServicio').setValue('-1');
		store.commitChanges(); 
	}	
//----------------------------Fin procesarBancoServicio--------------------------------

//------------------------------bancoServicio-------------------------------------
	var bancoServicio = new Ext.data.JsonStore({  
		id: 'bancoServicioStore',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15mancatalogoBancosTefExt01.data.jsp',
		baseParams: {
			informacion: 'bancoServicio'
		},
		totalProperty: 'total',
		autoLoad: true,
		listeners: {
			load: procesarBancoServicio,
			exception: NE.util.mostrarDataProxyError//,
			//beforeload: NE.util.initMensajeCargaCombo
		}		
	});
//------------------------------Fin bancoServicio---------------------------------

//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  
var fp = Ext.getCmp('forma');
  //fp.el.unmask();							
  var gridConsulta = Ext.getCmp('grid');	
  var el = gridConsulta.getGridEl();		
  if (arrRegistros != null) {
    if (!gridConsulta.isVisible()) {
      gridConsulta.show();
      }				
    if(store.getTotalCount() > 0) {		
      el.unmask();
    } else {		
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
};
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15mancatalogoBancosTefExt01.data.jsp',
		baseParams: {
			informacion: 'carga'
		},		
		fields: [	
			{	name: 'INTERCLAVE'},
			{	name: 'CLAVE'},
			{	name: 'DESCRIPT'},
			{	name: 'CC_CLAVE_DCAT_S'},
			{	name: 'DESCRIPCION_DCAT_S'},
			{	name: 'CS_EQUIV_PLAZA'},
			{	name: 'IC_FINANCIERA'},
			{	name: 'CD_NOMBRE'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
//----------------------------Fin ConsultaData----------------------------------

//--------------------------------gridConsulta----------------------------------	
var gridConsulta = new Ext.grid.GridPanel({	
		store				:consultaData,
		id					:'grid',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Contenido del Cat�logo',
		hidden			:false,			
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 450,
		width				: 843,
		align				: 'center',
		enableColumnMove: false,
		enableColumnHide: false,
		frame				: true,
		listeners 		: {
			cellclick	: function(grid, rowIndex, columnIndex, e) {
								Ext.getCmp('textfieldFila').setValue(rowIndex)
								}
		},
		tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
					{
						iconCls	: 'icon-register-edit',
						text		: 'Editar Registro',
						disabled	: false,
						handler	: function(){
									var grid	  				= Ext.getCmp("grid");
									var seleccionados  	= grid.getSelectionModel().getSelections();
									if(       seleccionados.length == 0){
										Ext.MessageBox.alert('Aviso','Debe seleccionar un registro.');
										return;
									} else if( seleccionados.length >  1){
										Ext.MessageBox.alert('Aviso','S�lo se puede editar un registro a la vez.');
										return;
									}
									var formTit	  			= Ext.getCmp("forma");
										 formTit.setTitle('Editar Cat�logo: Modificar Registro');
									var respuesta 			= new Object();
									respuesta['record']	= seleccionados[0];
									
									Ext.getCmp('textfieldAccion').setValue('modificar');
									var banco = respuesta.record.data.IC_FINANCIERA;
									var finan = respuesta.record.data.CC_CLAVE_DCAT_S;
									var desc  = respuesta.record.data.DESCRIPT;
									var clave  = respuesta.record.data.INTERCLAVE;
									if (banco !="")
										Ext.getCmp('combobancoServicio').setValue(banco);
									else
										Ext.getCmp('combobancoServicio').setValue('-1');
									if (finan !="")
										Ext.getCmp('comboInstitucionFinanciera').setValue(finan);
									else
										Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
										
									Ext.getCmp('textfieldDescripcion').setValue(desc);	
									
									Ext.getCmp('textfieldClave').setValue(clave);
									Ext.getCmp('textfieldClave').enable();						
																	
									Ext.getCmp('grid').hide();
									Ext.getCmp('forma').show();
									
						}
					},{
						iconCls	: 'icon-register-add',
						text		: 'Agregar Registro',
						disabled	: false,
						handler	: function(){
									var formTit	 = Ext.getCmp("forma");
									formTit.setTitle('Editar Cat�logo: Agregar Registro');
									Ext.getCmp('grid').hide();
									Ext.getCmp('forma').show();
						}
					},{
						iconCls	: 'icon-register-delete',
						text		: 'Eliminar Registro',
						disabled	: false,
						handler	: function(){
										var grid	  	 = Ext.getCmp("grid");
										var seleccionados   = grid.getSelectionModel().getSelections(); 
										var numeroRegistros = seleccionados.length;
										var aviso			  = "";
										if(numeroRegistros == 0){
											Ext.MessageBox.alert('Aviso','Debe seleccionar al menos un registro.');
											return;
										} else if(numeroRegistros == 1  ){
											aviso = 'El registro seleccionado ser� eliminado, �Desea usted continuar?';
										} else if(numeroRegistros >  1  ){
											Ext.MessageBox.alert('Aviso','Solo es permitido eliminar un registro.');
											return; 
										}
										var respuesta 			= new Object();
										respuesta['record']	= seleccionados[0];
										
										Ext.Msg.show({
										title: 'Eliminar',
										msg: 'Esta seguro de querer eliminar el registro ?',
										modal: true,
										icon: Ext.Msg.QUESTION,
										buttons: Ext.Msg.OKCANCEL,
										fn: function (btn, text){
											if (btn == 'ok'){
													Ext.Ajax.request({
													url: '15mancatalogoBancosTefExt01.data.jsp',
													params: Ext.apply(fpDatos.getForm().getValues(),{
													  informacion: 'Eliminar',
													  id: respuesta.record.data.CLAVE
													}),
													success : function(response) {
														var info = Ext.util.JSON.decode(response.responseText);
														var eliminar = info.eliminar;
														if (eliminar){
															  Ext.Msg.show({
															  title: 'Eliminar',
															  msg: 'Se elimino el registro.',
															  modal: true,
															  icon: Ext.Msg.INFO,
															  buttons: Ext.Msg.OK,
															  fn: function (){
																	fpDatos.getForm().reset();
																	Ext.getCmp('forma').hide();
																	Ext.getCmp('cmbCAT').setValue('40');
																	Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
																	Ext.getCmp('combobancoServicio').setValue('-1');										
																	consultaData.load();
																 }
															  });//fin Msg
														} else {
															  Ext.Msg.show({
															  title: 'Eliminar',
															  msg: 'No se puede eliminar el registro porque esta relacionado.',
															  modal: true,
															  icon: Ext.Msg.ERROR,
															  buttons: Ext.Msg.OK,
															  fn: function (){
																	fpDatos.getForm().reset();
																	Ext.getCmp('forma').hide();
																	Ext.getCmp('cmbCAT').setValue('40');
																	Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
																	Ext.getCmp('combobancoServicio').setValue('-1');
																	consultaData.load();
																 }
															  });//fin Msg
														}
													 }
													});//fin AJAX
											} else {		}
										 }//FIN fn
										});
						}
					}					
			]
	 },
		columns			:[
    {
    header		:'Clave',
    tooltip		:'Clave',
    dataIndex	:'INTERCLAVE',
    sortable	:true,
    width		:50,			
    resizable	:true,
    align		:'center'
    },{
    header		:'Descripci�n',
    tooltip		:'Descripci�n',
    dataIndex	:'DESCRIPT',
    sortable	:true,
    width		:200,			
    resizable	:true,
    align		:'center',
	 renderer:function(value){
		return "<div align='left'>"+value+"</div>";
		}
    },{
    header		:'Instituci�n Financiera',
    tooltip		:'Instituci�n Financiera',
    dataIndex	:'DESCRIPT',
    sortable	:true,
    width		:350,			
    resizable	:true,
    align		:'center',
	 renderer: function(v,params,record){
		if(record.data.CC_CLAVE_DCAT_S !=" "){
			return "<div align='left'>"+record.data.CC_CLAVE_DCAT_S +"-"+record.data.DESCRIPCION_DCAT_S+"</div>";
		} else {
			return "";
		}
	  }
    },{
    header		:'Equivalencia de Plaza',
    tooltip		:'Equivalencia de Plaza',
    dataIndex	:'CS_EQUIV_PLAZA',
    sortable	:true,
    width		:100,			
    resizable	:true,
    align		:'center',
	 renderer: function(v,params,record){
		if(record.data.CS_EQUIV_PLAZA =='S' )
			return "S�";
		else 
			return "No";
	  }// FIN renderer
    },{
    header		:'Banco de Servicio',
    tooltip		:'Banco de Servicio',
    dataIndex	:'CD_NOMBRE',
    sortable	:true,
    width		:110,			
    resizable	:true,
    align		:'center',
	 renderer:function(value){
		return "<div align='left'>"+value+"</div>";
		}
    },{
    xtype		: 'actioncolumn',
    header		: 'Seleccionar',
    tooltip		: 'Seleccionar',				
    align		: 'center',
	 hidden		: true,
    items		: [
     {
      getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
        this.items[0].tooltip = 'Modificar';
        return 'icoModificar';										
      },
      handler: function(grid, rowIndex, colIndex, item, event){
		Ext.getCmp('textfieldAccion').setValue('modificar');
		Ext.getCmp('textfieldFila').setValue(rowIndex);
		var registro = grid.getStore().getAt(rowIndex);
		var banco = registro.get('IC_FINANCIERA');
		var finan = registro.get('CC_CLAVE_DCAT_S');
		var desc  = registro.get('DESCRIPT');
		if (banco !="")
			Ext.getCmp('combobancoServicio').setValue(banco);
		else
			Ext.getCmp('combobancoServicio').setValue('-1');
		if (finan !="")
			Ext.getCmp('comboInstitucionFinanciera').setValue(finan);
		else
			Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
			
			Ext.getCmp('textfieldDescripcion').setValue(desc);
			
		//Ext.getCmp('compositefieldDos').hide(true);
		
	   }//fin handler
    },
    {
      getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
        this.items[1].tooltip = 'Eliminar';
        return 'icoCancelar';										
      },
      handler: function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
			Ext.Msg.show({
			title: 'Eliminar',
			msg: 'Esta seguro de querer eliminar el registro ?',
			modal: true,
			icon: Ext.Msg.QUESTION,
			buttons: Ext.Msg.OKCANCEL,
			fn: function (btn, text){
				if (btn == 'ok'){
					   Ext.Ajax.request({
						url: '15mancatalogoBancosTefExt01.data.jsp',
						params: Ext.apply(fpDatos.getForm().getValues(),{
						  informacion: 'Eliminar',
						  id: registro.get('CLAVE')
						}),
						success : function(response) {
							var info = Ext.util.JSON.decode(response.responseText);
							var eliminar = info.eliminar;
							if (eliminar){
								  Ext.Msg.show({
								  title: 'Eliminar',
								  msg: 'Se elimino el registro.',
								  modal: true,
								  icon: Ext.Msg.INFO,
								  buttons: Ext.Msg.OK,
								  fn: function (){
										fpDatos.getForm().reset();
										Ext.getCmp('cmbCAT').setValue('40');
										Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
										Ext.getCmp('combobancoServicio').setValue('-1');										
										consultaData.load();
									 }
								  });//fin Msg
							} else {
								  Ext.Msg.show({
								  title: 'Eliminar',
								  msg: 'No se puede eliminar el registro porque esta relacionado.',
								  modal: true,
								  icon: Ext.Msg.ERROR,
								  buttons: Ext.Msg.OK,
								  fn: function (){
										fpDatos.getForm().reset();
										Ext.getCmp('cmbCAT').setValue('40');
										Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
										Ext.getCmp('combobancoServicio').setValue('-1');
										consultaData.load();
									 }
								  });//fin Msg
							}
						 }
						});//fin AJAX
				} else {		}
			 }//FIN fn
			});
		
		
		 
      
      }//fin handler
    } 
  ]
	}   
]
	});	
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------elementosBancoTEF--------------------------------
var  elementosBancoTEF =  [
{
	xtype: 'compositefield',
	id:'compositefieldUno',
	combineErrors: false,
	msgTarget: 'side',
	hidden : true,
	items: [
		{
    xtype		: 'combo',
    name			: 'cmbCatalogos',
    hiddenName	: '_cmbCat',
    id			: 'cmbCATHIDEN',	
    fieldLabel	: 'Cat�logos', 
    mode			: 'local',	
    forceSelection : true,	
    triggerAction  : 'all',	
    typeAhead		: true,
    minChars 		: 1,	
    store 			: catalogo,		
    valueField 	: 'clave',
    displayField	: 'descripcion',	
    editable		: true,
    typeAhead		: true,
    width			:  365,
	 listeners		: {
		select		: {
			fn			: function(combo) {
							var valor  = combo.getValue();
							}// FIN fn
							}//FIN select
						}//FIN listeners
    },{
		 xtype :'displayfield',
		 frame :true,
		 border: false,
		 value :'',
		 width : 10
	},{
		xtype     :'button',
		text		 :'Buscar',
		id			 :'btnBuscarCombo',
		iconCls	 :'icoBuscar',
		disabled	 : false,
		handler	 :function(boton, evento){
		var valor = Ext.getCmp('cmbCAT').getValue();
		if (valor == 2){					//Tasa
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
		} else if (valor == 11){		//Estatus Documento
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
		} else if (valor == 12){		//Cambio Estatus
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
		} else if (valor == 13){		//Clasificacion ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
		} else if (valor == 21){		//Subsector
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
		} else if (valor == 33){		//D�as Inh�biles
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
		} else if (valor == 34){		//Productos
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
		} else if(valor == 36){			//Personas Facultadas por Banco
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
		} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
		} else if(valor == 39){			//Version Convenio
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
		} else if(valor == 40){			//Bancos TEF
			fpDatos.getForm().reset();
			Ext.getCmp('cmbCAT').setValue('40');
			catalogo.load();
			institucionFinanciera.load();
			bancoServicio.load();
			consultaData.load();
			//window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
		} else if (valor == 47){		//Firma Cedulas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
		} else if (valor == 49){		//Plazos por Producto
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
		} else if (valor == 60){		//Area Promocion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
		} else if (valor == 62){		//Subdireccion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
		} else if (valor == 63){		//Lider Promotor
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
		} else if (valor == 65){		//Subtipo EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
		} else if (valor == 66){		//Sector EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
		} else if (valor == 68){		//Ventanillas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
		} else if (valor == 70){		//D�as Inh�biles por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
		}
		}//fin handler 
		}]
		}, {
		xtype     : 'textfield',
		fieldLabel: 'Descripci�n',
		name      :	'descripcion',
		id        :	'textfieldDescripcion',
		hiddenName:	'descripcionHidden',
		maxLength : 99,
		anchor	 : '95%',
		allowBlank:	false,
		listeners :{
			change :function(field, newValue){
						field.setValue(newValue.toUpperCase());
						}
					}//FIN listeners
		},{
		xtype				: 'combo',
		name				: 'institucionFinanciera',
		hiddenName		: 'institucionFinancieraHidden',
		id					: 'comboInstitucionFinanciera',	
		fieldLabel		: 'Instituci�n<br>Financiera', 
		mode				: 'local',	
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,	
		store 			: institucionFinanciera,		
		valueField 		: 'clave',
		displayField 	: 'descripcion',	
		tpl				: NE.util.templateMensajeCargaComboConDescripcionCompleta,
		anchor	 		: '95%',
		editable			: true,
		typeAhead		: true,
		allowBlank		: false
		},{
		xtype     : 'checkbox',
		boxLabel	 : 'Equivalencia de plaza.',
		checked	 : false,
		name      :	'equivalencia',
		id        :	'checkboxEquivalencia'
		},{
		xtype				: 'combo',
		name				: 'bancoServicio',
		hiddenName		: 'bancoServicioHidden',
		id					: 'combobancoServicio',	
		fieldLabel		: 'Banco de Servicio', 
		mode				: 'local',	
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,	
		store 			: bancoServicio,	
		tpl				: NE.util.templateMensajeCargaComboConDescripcionCompleta,
		anchor	 		: '95%',
		valueField 		: 'clave',
		displayField 	: 'descripcion',	
		editable			: true,
		typeAhead		: true,
		allowBlank		: false
		},	
		{
			xtype     : 'numberfield',
			fieldLabel: 'Clave',
			name      :	'clave1',
			id        :	'textfieldClave',
			hiddenName:	'claveHidden',
			decimalPrecision: 0,
			maxLength : 3,
			//width		 : 30,
			msgTarget	: 'side',
			anchor	 : '30%',
			bodyStyle:'padding: 10px', 
			margins		: '0 20 0 0'
			},
		
		
		/*
		{
		xtype: 'compositefield',
		id:'compositefieldDos',
		combineErrors: false,
		msgTarget: 'side',
		hidden : false,
		items: [
		{
		xtype     : 'numberfield',
		fieldLabel: 'Clave',
		name      :	'clave',
		id        :	'textfieldClave',
		hiddenName:	'claveHidden',
		decimalPrecision: 0,
		maxLength : 10,
		width		 : 100,
		msgTarget	: 'side',
		margins		: '0 20 0 0'
		},
		
		{
		 xtype :'displayfield',
		 frame :true,
		 border: false,
		 value :'',
		 width	: 10
		},{
		xtype     :'button',
		text		 :'Buscar',
		id			 :'buscarClave',
		iconCls	 :'icoBuscar',
		disabled	 :false,
		hidden	 :true,
		handler	 :function(boton, evento){	
					if(Ext.getCmp('textfieldClave').getValue()==""){
						Ext.getCmp('textfieldClave').markInvalid('Especifique la clave.');
					} else if( Ext.getCmp('textfieldClave').isValid() ){
						Ext.getCmp('textfieldClave').reset();	
						consultaData.load();
					} else  {
						Ext.getCmp('textfieldClave').focus();	
					}												
			}//fin handler 
		}
		]			
	},
	*/
	{
		xtype     : 'textfield',
		name      :	'accion',
		id        :	'textfieldAccion',
		hiddenName:	'accionHidden',
		width		 : 60,
		hidden	 :true
		
		},{
		xtype     : 'textfield',
		name      :	'fila',
		id        :	'textfieldFila',
		hiddenName:	'filaHidden',
		width		 : 60,
		hidden	 :true
		
		}
];
//-----------------------------Fin elementosBancoTEF------------------------------


//-------------------------------Panel Datos---------------------------------
	var fpDatos = new Ext.form.FormPanel({
		id					:'forma',
		width				:843,
		title				:'Criterios de B�squeda del Cat�logo',
		//layout			:'form',
		frame				:true,
		collapsible		:true,
		titleCollapse	:true,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 6px',
		labelWidth		: 130,
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosBancoTEF
							],
		buttons			:[{
							text				:'Guardar',
							id					:'btnAceptar',
							iconCls			:'icoGuardar',
							disabled			: false,
							handler			:function(boton, evento){
							
								if(Ext.getCmp('textfieldDescripcion').isValid()){									
									if(Ext.getCmp('comboInstitucionFinanciera').getValue() != '-1'){
										if(Ext.getCmp('combobancoServicio').getValue() != '-1'){
											if(Ext.getCmp('textfieldAccion').getValue() =='modificar'){											
												var grid = Ext.getCmp('grid');
												var store = grid.getStore();
												var fila= Ext.getCmp('textfieldFila').getValue()
												var registro = grid.getStore().getAt(fila);
												var plaza=Ext.getCmp('checkboxEquivalencia').getValue();
												var equiv="";
												if(plaza)
													equiv="S";
												else
													equiv="N";
												if(Ext.getCmp('textfieldDescripcion').isValid() & 
													Ext.getCmp('comboInstitucionFinanciera').isValid() &
													Ext.getCmp('combobancoServicio').isValid() ){}else {return;}
											   if (Ext.getCmp('combobancoServicio').getValue()== '-1'){return;}
												Ext.Ajax.request({
													url: '15mancatalogoBancosTefExt01.data.jsp',
													params: Ext.apply(fpDatos.getForm().getValues(),{
													  informacion: 'modificar',
													  id: registro.get('CLAVE'),
													  dcats: Ext.getCmp('comboInstitucionFinanciera').getValue(),
													  plaza:equiv,
													  financiera: Ext.getCmp('combobancoServicio').getValue()
													}),
													success : function(response) {
														var info = Ext.util.JSON.decode(response.responseText);
														var modificar = info.modificar;
														if (modificar){
														  Ext.Msg.show({
														  title: 'Modificar',
														  msg: 'El registro fue Actualizado.',
														  modal: true,
														  icon: Ext.Msg.INFO,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fpDatos.getForm().reset();
																Ext.getCmp('cmbCAT').setValue('40');
																Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
																Ext.getCmp('combobancoServicio').setValue('-1');										
																consultaData.load();
																//Ext.getCmp('compositefieldDos').show();
																Ext.getCmp('forma').hide();
															 }
														  });//fin Msg
													} else {
														  Ext.Msg.show({
														  title: 'Modificar',
														  msg: 'Ocurrio un error al intentar actualizar el registro.',
														  modal: true,
														  icon: Ext.Msg.ERROR,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fpDatos.getForm().reset();
																Ext.getCmp('forma').hide();
																Ext.getCmp('cmbCAT').setValue('40');
																Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
																Ext.getCmp('combobancoServicio').setValue('-1');
																consultaData.load();
															 }
														  });//fin Msg
													}
													}//fin success
												});//fin AJAX
												
											} else {
												if(Ext.getCmp('textfieldClave').getValue() != '' && Ext.getCmp('textfieldClave').isValid()){
													var plaza=Ext.getCmp('checkboxEquivalencia').getValue();
													var equiv="";
													if(plaza)
														equiv="S";
													else
														equiv="N";
													Ext.Ajax.request({
													url: '15mancatalogoBancosTefExt01.data.jsp',
													params: Ext.apply(fpDatos.getForm().getValues(),{
													  informacion: 'nuevo',
													  clave: Ext.getCmp('textfieldClave').getValue(),
													  descr: Ext.getCmp('textfieldDescripcion').getValue(),
													  dcats: Ext.getCmp('comboInstitucionFinanciera').getValue(),
													  plaza:equiv,
													  financiera: Ext.getCmp('combobancoServicio').getValue()
													}),
													success : function(response) {
														var info = Ext.util.JSON.decode(response.responseText);
														var nuevo = info.nuevo;
														var existe= info.existe;
														if (nuevo){
														  Ext.Msg.show({
														  title: 'Nuevo',
														  msg: 'El registro fue agregado.',
														  modal: true,
														  icon: Ext.Msg.INFO,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fpDatos.getForm().reset();
																Ext.getCmp('forma').hide();
																Ext.getCmp('cmbCAT').setValue('40');
																Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
																Ext.getCmp('combobancoServicio').setValue('-1');										
																consultaData.load();
															 }
														  });//fin Msg
													} else {
														  Ext.Msg.show({
														  title: 'Nuevo',
														  msg: 'La clave ya existe.',
														  modal: true,
														  icon: Ext.Msg.WARNING,
														  buttons: Ext.Msg.OK,
														  fn: function (){
																fpDatos.getForm().reset();
																Ext.getCmp('forma').hide();
																Ext.getCmp('cmbCAT').setValue('40');
																Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
																Ext.getCmp('combobancoServicio').setValue('-1');
																consultaData.load();
															 }
														  });//fin Msg
													    }
														}//fin success
													});//fin AJAX
												} else {
													/*Ext.Msg.show({ title: 'Clave', msg: 'Ingrese la Clave.', modal: true,
															icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
															fn: function(){ Ext.getCmp('textfieldClave').focus(); }
													});*/Ext.getCmp('textfieldClave').markInvalid('Ingrese la Clave.');
												}
												
											}
										} else {
												/*Ext.Msg.show({ title: 'Banco de Servicio', msg: 'Seleccione un Banco de Servicio.', modal: true,
														icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
														fn: function(){ Ext.getCmp('combobancoServicio').focus(); }
												});*/Ext.getCmp('combobancoServicio').markInvalid('Seleccione un Banco de Servicio.');
										}	
									} else {
										/*Ext.Msg.show({ title: 'Instituci�n Financiera', msg: 'Seleccione una Instituci�n Financiera.', modal: true,
												icon: Ext.Msg.INFO, buttons: Ext.Msg.OK,
												fn: function(){ Ext.getCmp('comboInstitucionFinanciera').focus(); }
										});*/Ext.getCmp('comboInstitucionFinanciera').markInvalid('Seleccione una Instituci�n Financiera.');
									}			
								} else{
									Ext.getCmp('textfieldDescripcion').markInvalid('Ingrese una descripci�n');
								} 								
							}//fin handler 
						},{
							text				:'Cancelar',
							id					:'btnLimpiar',
							iconCls			:'icoCancelar',
							disabled			: false,
							handler			:function(boton, evento){
								var grid = Ext.getCmp('grid');
								grid.show();
								Ext.getCmp('forma').hide();
								fpDatos.getForm().reset();
								Ext.getCmp('cmbCAT').setValue('40');
									Ext.getCmp('comboInstitucionFinanciera').setValue('-1');
									Ext.getCmp('combobancoServicio').setValue('-1');
									//Ext.getCmp('compositefieldDos').show();
								}//fin handler 
							}]
	})
//------------------------------Fin Panel Datos------------------------------

	var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		autoHeight		:true,//height:			80,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		'padding:6px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		items: 			[
			 {
				 xtype		: 'combo',
				 name			: 'cmbCatalogos',
				 hiddenName	: '_cmbCat',
				 id			: 'cmbCAT',	
				 fieldLabel	: 'Cat�logo', 
				 mode			: 'local',	
				 forceSelection : true,	
				 triggerAction  : 'all',	
				 typeAhead		: true,
				 minChars 		: 1,	
				 store 			: catalogo,		
				 valueField 	: 'clave',
				 displayField	: 'descripcion',	
				 editable		: true,
				 typeAhead		: true,
				 anchor			: '95%',
				 listeners		: {
					select		: {
						fn			: function(combo) {
										var valor = Ext.getCmp('cmbCAT').getValue();
										if (valor == 2){					//Tasa
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
										} else if (valor == 11){		//Estatus Documento
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
										} else if (valor == 12){		//Cambio Estatus
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
										} else if (valor == 13){		//Clasificacion ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
										} else if (valor == 21){		//Subsector
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
										} else if (valor == 33){		//D�as Inh�biles
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
										} else if (valor == 34){		//Productos
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
										} else if(valor == 36){			//Personas Facultadas por Banco
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
										} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
										} else if(valor == 39){			//Version Convenio
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
										} else if(valor == 40){			//Bancos TEF
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
										} else if (valor == 47){		//Firma Cedulas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
										} else if (valor == 49){		//Plazos por Producto
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
										} else if (valor == 60){		//Area Promocion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
										} else if (valor == 62){		//Subdireccion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
										} else if (valor == 63){		//Lider Promotor
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
										} else if (valor == 65){		//Subtipo EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
										} else if (valor == 66){		//Sector EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
										} else if (valor == 68){		//Ventanillas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
										} else if (valor == 70){		//D�as Inh�biles por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
										} else if (valor == 71){		//D�as Inh�biles EPO por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
										} else if (valor == 677){		//Clasificaci�n Epo
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
										}else if (valor == 103){		//Programa a Fondo JR
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
										}
										}// FIN fn
										}//FIN select
									}//FIN listeners
				 }],
		monitorValid: 	false
	});

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		: 949,
		items		:[
					NE.util.getEspaciador(20),	
					panelFormaCatalogos,
					NE.util.getEspaciador(20),
					fpDatos,
					NE.util.getEspaciador(20),
					gridConsulta
					]
	})
//-----------------------------Fin Contenedor Principal-------------------------
Ext.getCmp('forma').setVisible(false);
})//-----------------------------------------------Fin Ext.onReady(function(){}