Ext.onReady(function() {

var claves=[];

//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  
var fp = Ext.getCmp('forma');
  fp.el.unmask();							
  var gridConsulta = Ext.getCmp('grid');	
  var el = gridConsulta.getGridEl();		
  if (arrRegistros != null) {
    if (!gridConsulta.isVisible()) {
      gridConsulta.show();
      }				
    if(store.getTotalCount() > 0) {		
      el.unmask();
    } else {		
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
};
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
var consultaData   = new Ext.data.JsonStore({ 
	root 	: 'registros',
	url 	: '15mancatalogoDiaInhabilExt01.data.jsp',
	baseParams: {
		informacion: 'Consultar'
	},		
	fields: [	
		{	name: 'CLAVE'},
		{	name: 'FECHA'},		
		{	name: 'DESCRIPCION'}
	],		
	totalProperty : 'total',
	messageProperty: 'msg',
	autoLoad: true,
	listeners: {
		load: procesarConsultaData,
		exception: {
			fn: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				//LLama procesar consulta, para que desbloquee los componentes.
				procesarConsultaData(null, null, null);					
			}
		}
	}		
});
//----------------------------Fin ConsultaData----------------------------------

//----------------------------Fin procesarCatalogo--------------------------------
	
var procesarCatalogo = function(store, arrRegistros, opts) {
		store.each(function(record){
		if(record.get('clave')==45 || record.get('clave')==6  || record.get('clave')==25 || record.get('clave')==101 || record.get('clave')==5 || record.get('clave')==102 || record.get('clave')==19 || record.get('clave')==7 ||
			record.get('clave')==29 ||  record.get('clave')==38 || record.get('clave')==43  || record.get('clave')==18 || record.get('clave')==8 ||
			record.get('clave')==24 || record.get('clave')==15 || record.get('clave')==41 || record.get('clave')==42 || record.get('clave')==17 || record.get('clave')==100 || record.get('clave')==50 || record.get('clave')==1 ||
			record.get('clave')==35 || record.get('clave')==26 || record.get('clave')==3  || record.get('clave')==52 || record.get('clave')==14 || record.get('clave')==270 ||  record.get('clave')==104 ||
			record.get('clave')==20 || record.get('clave')==61 || record.get('clave')==53 || record.get('clave')==10 || record.get('clave')==67 || record.get('clave')==208 || record.get('clave')==23 || record.get('clave')==22  ||
			record.get('clave')==9  || record.get('clave')==16 || record.get('clave')==64 || record.get('clave')==30 || record.get('clave')==48 || record.get('clave')==31  || record.get('clave')==32 || record.get('clave')==46  ||
			record.get('clave')==51 || record.get('clave')==27|| record.get('clave')==6769){
			store.remove(record);
		}
		});
		Ext.getCmp('cmbCAT').setValue('33')
	store.commitChanges(); 
	}	
//----------------------------Fin procesarCatalogo--------------------------------

//--------------------------Store catalogo----------------------------
var catalogo = new Ext.data.JsonStore({
	id					: 'catalogos',
	root				: 'registros',
	fields			: ['clave','descripcion','loadMsg'], 
	url				: '15mancatalogoClasificacion01Ext.data.jsp' ,
	baseParams		: {
		informacion	: 'Clasificacion.Carga.Catalogo'
	},
	totalProperty	: 'total',
	autoLoad			: true,
	listeners		: {
		load 			: procesarCatalogo,
		exception	: 	NE.util.mostrarDataProxyError,
		beforeload	: NE.util.initMensajeCargaCombo
	}		
});
//--------------------------Fin Store catalogo----------------------------

//--------------------------------gridConsulta----------------------------------	
var gridConsulta = new Ext.grid.GridPanel({	
	store				:consultaData,
	id					:'grid',
	margins			:'20 0 0 0',		
	style				:'margin:0 auto;',
	title				:'Contenido del Cat�logo',
	hidden			: false,			
	displayInfo		: true,		
	emptyMsg			: "No hay registros.",		
	loadMask			: true,
	stripeRows		: true,
	height			: 450,
	width				: 843,
	enableColumnMove: false,
	enableColumnHide: false,
	align				: 'center',
	frame				: true,
	listeners 		: {
			cellclick	: function(grid, rowIndex, columnIndex, e) {
								Ext.getCmp('idRow').setValue(rowIndex)
								}
		},
		tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
					{
						iconCls	: 'icon-register-edit',
						text		: 'Editar Registro',
						disabled	: false,
						handler	: function(){
									var grid	  				= Ext.getCmp("grid");
									var seleccionados  	= grid.getSelectionModel().getSelections();
									if(       seleccionados.length == 0){
										Ext.MessageBox.alert('Aviso','Debe seleccionar un registro.');
										return;
									} else if( seleccionados.length >  1){
										Ext.MessageBox.alert('Aviso','S�lo se puede editar un registro a la vez.');
										return;
									}
									var formTit	  			= Ext.getCmp("forma");
										 formTit.setTitle('Editar Cat�logo: Modificar Registro');
									var respuesta 			= new Object();
									respuesta['record']	= seleccionados[0];
									
								//	var fila= Ext.getCmp('idRow').getValue();									
								//	var rec = gridConsulta.getStore().getAt(fila);
									Ext.getCmp('idFecha').setValue(respuesta.record.data.FECHA);
									Ext.getCmp('idDescripcion').setValue(respuesta.record.data.DESCRIPCION);
									Ext.getCmp('idAccion').setValue('1');
									//Ext.getCmp('idRow').setValue(rowIndex);
									
									Ext.getCmp('grid').hide();
									Ext.getCmp('forma').show();
									
						}
					},{
						iconCls	: 'icon-register-add',
						text		: 'Agregar Registro',
						disabled	: false,
						handler	: function(){
									var formTit	 = Ext.getCmp("forma");
									formTit.setTitle('Editar Cat�logo: Agregar Registro');
									Ext.getCmp('grid').hide();
									Ext.getCmp('forma').show();
						}
					},{
						iconCls	: 'icon-register-delete',
						text		: 'Eliminar Registro',
						disabled	: false,
						handler	: function(){
										var grid	  	 = Ext.getCmp("grid");
										var seleccionados   = grid.getSelectionModel().getSelections(); 
										var numeroRegistros = seleccionados.length;
										var aviso			  = "";
										if(numeroRegistros == 0){
											Ext.MessageBox.alert('Aviso','Debe seleccionar al menos un registro.');
											return;
										} else if(numeroRegistros == 1  ){
											aviso = 'El registro seleccionado ser� eliminado, �Desea usted continuar?';
										} else if(numeroRegistros >  1  ){
											Ext.MessageBox.alert('Aviso','Solo es permitido eliminar un registro.');
											return; 
										}
										var respuesta 			= new Object();
										respuesta['record']	= seleccionados[0];
										Ext.Msg.show({
										  title: 'Eliminar',
										  msg: '�Esta seguro de querer eliminar el registro?',
										  modal: true,
										  icon: Ext.Msg.QUESTION,
										  buttons: Ext.Msg.OKCANCEL,
										  fn: function (btn,text){
												if (btn == 'ok'){
													Ext.Ajax.request({
													url: '15mancatalogoDiaInhabilExt01.data.jsp',
													params: Ext.apply(fp.getForm().getValues(),{
													  informacion: 'Eliminar', 
													  clave : respuesta.record.data.CLAVE
													}),
													success : function(response) {
														var info = Ext.util.JSON.decode(response.responseText);
														var eliminar = info.eliminar;
														if (eliminar){
															  Ext.getCmp('forma').hide();	
															  Ext.Msg.show({
															  title: 'Eliminar',
															  msg: info.msg,
															  modal: true,
															  icon: Ext.Msg.INFO,
															  buttons: Ext.Msg.OK,
															  fn: function (){
																	fp.getForm().reset();
																	Ext.getCmp('cmbCAT').setValue('33');
																	consultaData.load();
																 }
															  });//fin Msg
														} else {
																Ext.getCmp('forma').hide();	
															  Ext.Msg.show({
															  title: 'Eliminar',
															  msg: info.msg,
															  modal: true,
															  icon: Ext.Msg.ERROR,
															  buttons: Ext.Msg.OK,
															  fn: function (){
																	fp.getForm().reset();
																	Ext.getCmp('cmbCAT').setValue('33');
																	consultaData.load();
																 }
															  });//fin Msg
														}
													 }
													});//fin AJAX
												} 
											 }
										  });//fin Msg
						}
					}					
			]
	 },
	columns			:[
	 {
    header			:'D�a Inh�bil',
    tooltip			:'D�a Inh�bil',
    dataIndex		:'FECHA',
    sortable		:true,
    width			:70,			
    resizable		:true,
    align			:'center',
    renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    },{
    header			:'Descripci�n',
    tooltip			:'Descripci�n',
    dataIndex		:'DESCRIPCION',
    sortable		:true,
    width			:400,			
    resizable		:true,
    align			:'center',
	 renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    },{
	xtype		: 'actioncolumn',
	header	: '',
	tooltip	: '',	
	width		: 80,							
	align		: 'center',
	hidden	:true,
	items		: [
					{
					getClass: function(value,metadata,record,rowIndex,colIndex,store){
						this.items[0].tooltip = 'Modificar';	
						return 'modificar';
						},
					handler: function(grid, rowIndex, colIndex) {
								var store = grid.getStore();
								var rec = store.getAt(rowIndex);
								Ext.getCmp('idFecha').setValue(rec.get('FECHA'));
								Ext.getCmp('idDescripcion').setValue(rec.get('DESCRIPCION'));
								Ext.getCmp('idAccion').setValue('1');
								Ext.getCmp('idRow').setValue(rowIndex);
						
					}//FIN HANDLER
					},					{
					getClass: function(value,metadata,record,rowIndex,colIndex,store){
						this.items[1].tooltip = 'Eliminar';	
						return 'icoCancelar';
						},
						handler		: function(grid, rowIndex, colIndex, item, event){
							var registro = grid.getStore().getAt(rowIndex);
							Ext.Msg.show({
							  title: 'Eliminar',
							  msg: '�Esta seguro de querer eliminar el registro?',
							  modal: true,
							  icon: Ext.Msg.QUESTION,
							  buttons: Ext.Msg.OKCANCEL,
							  fn: function (btn,text){
									if (btn == 'ok'){
										Ext.Ajax.request({
										url: '15mancatalogoDiaInhabilExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Eliminar', 
										  clave : registro.get('CLAVE')
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var eliminar = info.eliminar;
											if (eliminar){
												  Ext.Msg.show({
												  title: 'Eliminar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('33');
														consultaData.load();
													 }
												  });//fin Msg
											} else {
												  Ext.Msg.show({
												  title: 'Eliminar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('33');
														consultaData.load();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
									} 
								 }
							  });//fin Msg
						}
					}
				  ]
	 }]   
});	
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------elementosForma---------------------------------	
var elementosForma =  [	
  {
  xtype					: 'compositefield',
  id						: 'cmpCB',
  combineErrors		: false,
  msgTarget				: 'side',
  hidden 				: true,
  items: [
    {
    xtype				: 'combo',
    name					: 'cmbCatalogos',
    hiddenName			: '_cmbCat',
    id					: 'cmbCATHIDEN',	
    fieldLabel			: 'Cat�logos', 
    mode					: 'local',	
    forceSelection 	: true,	
    triggerAction  	: 'all',	
    typeAhead		: true,
    minChars 		: 1,	
    store 			: catalogo,		
    valueField 	: 'clave',
    displayField: 'descripcion',	
    editable		: true,
    typeAhead		: true,
    width			: 330,
	 listeners		: {
		select		: {
			fn			: function(combo) {
							var valor  = combo.getValue();
							}// FIN fn
							}//FIN select
						}//FIN listeners
    },{
    xtype :'displayfield',
    frame :true,
    border: false,
    value :'',
    width	: 10
    },{
    xtype   : 'button',
    text    : 'Buscar',					
    tooltip :	'Buscar',
    iconCls : 'icoBuscar',
    id      : 'btnBuscar', 
    handler : function(boton, evento) { 
	 var valor = Ext.getCmp('cmbCAT').getValue();
		if (valor == 11){ 			// Estatus Documento
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
		} else if (valor == 12){	// Cambio Estatus
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
		} else if (valor == 13){	// Clasificacion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
		} else if(valor == 21){		// Subsector 
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
		} else if(valor == 33){		// Dias Inhabiles
			consultaData.load();
		} else if(valor == 34){		// Productos
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
		} else if(valor == 36){		// Personas Facultadas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
		}  else if(valor == 37){	// Dias Inhabiles EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
		} else if (valor == 39){   // Version Convenio
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
		} else if (valor == 40){	// Bancos TEF
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
		} else if (valor == 47){	// Firma Cedulas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
		} else if (valor == 49){	// Plazos por Producto
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
		} else if (valor == 60){ 	// Area de Promocion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
		} else if (valor == 62){ 	// subdireccion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
		} else if (valor == 63){ 	// Lider Promotor
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
		} else if (valor == 65){ 	// Subtipo EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
		} else if (valor == 66){ 	// Sector Epo
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
		} else if (valor == 68){ 	// Ventanilla
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
		} else if (valor == 70){ 	// Dias Inhabiles por A�o 
			 window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilesExt01.jsp";
		} else if (valor == 71){		//D�as Inh�biles EPO por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
		} else if (valor == 677){		//Clasificaci�n Epo
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
		}else if (valor == 103){		//Programa a Fondo JR
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
		}
	 }
    }]			
  },{
	xtype			: 'datefield',
	fieldLabel	: 'D�a inh�bil (dd/mm)',
	name			: 'fechaName',
	id				: 'idFecha',
	hiddenName	: 'fecha_registro',
	allowBlank	: true,
	anchor		: '95%',
	msgTarget	: 'side',
	margins		: '0 20 0 0',
	format		: 'd/m'
	},{
  xtype			:'textfield',
  id				:'idDescripcion',
  name			:'descripcionName',
  fieldLabel	:'Descricpi�n',
  maxLength		:100,
  anchor			:'95%',
	listeners	:{
		change	:function(field, newValue){
						field.setValue(newValue.toUpperCase());
						}
					 }
  },{
  xtype			:'textfield',
  hidden			: true,
  id				:'idAccion',
  name			:'accionName',
  fieldLabel	:'',
  anchor			:'75%'
  },{
  xtype			:'textfield',
  hidden			: true,
  id				:'idRow',
  name			:'rowName',
  fieldLabel	:'',
  anchor			:'75%'
  }
];
//-----------------------------Fin elementosForma-------------------------------



//-------------------------------Panel Consulta---------------------------------
var fp = new Ext.form.FormPanel({
	id					: 'forma',
	width				: 843,
	title				: 'Criterios de B�squeda del Cat�logo',
	frame				: true,		
	collapsible		:true,
	titleCollapse	:true,
	style				: 'margin:0 auto;',
	bodyStyle		: 'padding: 6px',
	defaults			: {
							msgTarget: 'side',
							anchor: '-20'
						},
	labelWidth		: 130,
	items				: elementosForma,
	buttons			:[
	{
	text				:'Guardar',
	id					:'idAceptar',
	iconCls			:'icoGuardar',
	handler			:function(boton, evento){
						var accion = Ext.getCmp('idAccion').getValue();
						if (accion!="1"){
						
						  if( Ext.getCmp('idFecha').getValue() != "" && Ext.getCmp('idDescripcion').getValue() !=""){
							fp.el.mask('Cargando...', 'x-mask-loading');	
								Ext.Ajax.request({
										url: '15mancatalogoDiaInhabilExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Agregar'
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var agregar = info.agregar;
											if (agregar){
													Ext.getCmp('forma').hide();
												  Ext.Msg.show({
												  title: 'Agregar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('33');
														consultaData.load();
													 }
												  });//fin Msg
											} else {
												Ext.getCmp('forma').hide();
												  Ext.Msg.show({
												  title: 'Agregar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('33');
														consultaData.load();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
							} else if( Ext.getCmp('idFecha').getValue() == ""){
								//Ext.getCmp('idFecha').focus();
								Ext.getCmp('idFecha').markInvalid('Ingrese la fecha.');
							} else if( Ext.getCmp('idDescripcion').getValue() == ""){
								//Ext.getCmp('idDescripcion').focus();
								Ext.getCmp('idDescripcion').markInvalid('Ingrese una descripcion.');
							}
						
						} else { // Modificar
						  if( Ext.getCmp('idFecha').getValue() != "" && Ext.getCmp('idDescripcion').getValue() !=""){
							if(Ext.getCmp('idDescripcion').isValid()){}else{return;}
							fp.el.mask('Cargando...', 'x-mask-loading');
							var store = gridConsulta.getStore();
							var fila = Ext.getCmp('idRow').getValue();
							var rec = gridConsulta.getStore().getAt(fila);
							Ext.Ajax.request({
										url: '15mancatalogoDiaInhabilExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Modificar',
										  clave: rec.get('CLAVE')
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var agregar = info.agregar;
											if (agregar){
												Ext.getCmp('forma').hide();
												  Ext.Msg.show({
												  title: 'Actualizar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('33');
														consultaData.load();
													 }
												  });//fin Msg
											} else {
												Ext.getCmp('forma').hide();
												  Ext.Msg.show({
												  title: 'Actualizar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('33');
														consultaData.load();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
							} else if( Ext.getCmp('idFecha').getValue() == ""){
								Ext.getCmp('idFecha').markInvalid('Ingrese la fecha.');
							} else if( Ext.getCmp('idDescripcion').getValue() == ""){
								Ext.getCmp('idDescripcion').markInvalid('Ingrese una descripcion.');
							}
						}
						
				}//fin handler 
	},{
	text				:'Cancelar',
	id					:'idLimpiar',
	iconCls			:'icoCancelar',
	handler			:function(boton, evento){
							//fp.getForm().reset(); 
							Ext.getCmp('idFecha').reset();
							Ext.getCmp('idDescripcion').reset();
							Ext.getCmp('cmbCAT').setValue('33');
							Ext.getCmp('forma').hide();
							Ext.getCmp('grid').show();
				}//fin handler 
	}]
});
//------------------------------Fin Panel Consulta------------------------------


	var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		autoHeight		:true,//height:			80,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		'padding:6px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		items: 			[
			 {
				 xtype		: 'combo',
				 name			: 'cmbCatalogos',
				 hiddenName	: '_cmbCat',
				 id			: 'cmbCAT',	
				 fieldLabel	: 'Cat�logo', 
				 mode			: 'local',	
				 forceSelection : true,	
				 triggerAction  : 'all',	
				 typeAhead		: true,
				 minChars 		: 1,	
				 store 			: catalogo,		
				 valueField 	: 'clave',
				 displayField	: 'descripcion',	
				 editable		: true,
				 typeAhead		: true,
				 anchor			: '95%',
				 listeners		: {
					select		: {
						fn			: function(combo) {
										var valor = Ext.getCmp('cmbCAT').getValue();
										if (valor == 2){					//Tasa
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
										} else if (valor == 11){		//Estatus Documento
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
										} else if (valor == 12){		//Cambio Estatus
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
										} else if (valor == 13){		//Clasificacion ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
										} else if (valor == 21){		//Subsector
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
										} else if (valor == 33){		//D�as Inh�biles
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
										} else if (valor == 34){		//Productos
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
										} else if(valor == 36){			//Personas Facultadas por Banco
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
										} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
										} else if(valor == 39){			//Version Convenio
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
										} else if(valor == 40){			//Bancos TEF
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
										} else if (valor == 47){		//Firma Cedulas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
										} else if (valor == 49){		//Plazos por Producto
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
										} else if (valor == 60){		//Area Promocion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
										} else if (valor == 62){		//Subdireccion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
										} else if (valor == 63){		//Lider Promotor
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
										} else if (valor == 65){		//Subtipo EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
										} else if (valor == 66){		//Sector EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
										} else if (valor == 68){		//Ventanillas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
										} else if (valor == 70){		//D�as Inh�biles por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
										} else if (valor == 71){		//D�as Inh�biles EPO por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
										} else if (valor == 677){		//Clasificaci�n Epo
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
										}else if (valor == 103){		//Programa a Fondo JR
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
										}
										}// FIN fn
										}//FIN select
									}//FIN listeners
				 }],
		monitorValid: 	false
	});

//----------------------------Contenedor Principal------------------------------	
var pnl = new Ext.Container({
	id			: 'contenedorPrincipal',
	applyTo	: 'areaContenido',
	width		: 949,		
	items		: [
					NE.util.getEspaciador(20),	
					panelFormaCatalogos,
					NE.util.getEspaciador(20),	
					fp,
					NE.util.getEspaciador(20),
					gridConsulta,
					NE.util.getEspaciador(20)
					]
});	
//-----------------------------Fin Contenedor Principal-------------------------
Ext.getCmp('forma').setVisible(false);
});//-----------------------------------------------Fin Ext.onReady(function(){}