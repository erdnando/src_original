Ext.onReady(function(){
//------------------------------------Tipo------------------------------------
var Tipo = Ext.data.Record.create([ 
 {name: "clave", type: "string"}, 
 {name: "descripcion", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//---------------------------------Fin Tipo-----------------------------------

//---------------------------- procesarTipo-----------------------------------
var procesarTipo = function(store, arrRegistros, opts) {
	store.insert(0,new Tipo({ 
	clave: "", 
	descripcion: "Seleccionar", 
	loadMsg: ""
 })); 
 
 Ext.getCmp('comboTipo').setValue("");
 store.commitChanges(); 
};	
//----------------------------Fin procesarTipo------------------------------------

//------------------------------- storeTipo ------------------------------------
var storeTipo = new Ext.data.JsonStore({
	id			: 'storeTipo',
	root 		: 'registros',
	fields 	: ['clave', 'descripcion'],
	url 		: '15mancatalogoSubtipoEPOExt01.data.jsp',
	baseParams: {
		informacion: 'comboTipo'
	},
	totalProperty  : 'total',
	autoLoad			: true,		
	listeners		: {
		load 			: procesarTipo,
		exception	: NE.util.mostrarDataProxyError,
		beforeload	: NE.util.initMensajeCargaCombo
	}		
});
//---------------------------- FIN storeTipo ------------------------------------

//----------------------------Fin procesarCatalogo-------------------------------
var procesarCatalogo = function(store, arrRegistros, opts) {
	store.each(function(record){
		if(record.get('clave')==45 || record.get('clave')==6  || record.get('clave')==25 || record.get('clave')==101 || record.get('clave')==5 || record.get('clave')==102 || record.get('clave')==19 || record.get('clave')==7 ||
			record.get('clave')==29 ||  record.get('clave')==38 || record.get('clave')==43  || record.get('clave')==18 || record.get('clave')==8 ||
			record.get('clave')==24 || record.get('clave')==15 || record.get('clave')==41 || record.get('clave')==42 || record.get('clave')==17 || record.get('clave')==100 || record.get('clave')==50 || record.get('clave')==1 ||
			record.get('clave')==35 || record.get('clave')==26 || record.get('clave')==3  || record.get('clave')==52 || record.get('clave')==14 || record.get('clave')==270 ||  record.get('clave')==104 ||
			record.get('clave')==20 || record.get('clave')==61 || record.get('clave')==53 || record.get('clave')==10 || record.get('clave')==67 || record.get('clave')==208 || record.get('clave')==23 || record.get('clave')==22  ||
			record.get('clave')==9  || record.get('clave')==16 || record.get('clave')==64 || record.get('clave')==30 || record.get('clave')==48 || record.get('clave')==31  || record.get('clave')==32 || record.get('clave')==46  ||
			record.get('clave')==51 || record.get('clave')==27|| record.get('clave')==6769){
			store.remove(record);
		}
		});
	Ext.getCmp('cmbCAT').setValue('65')
	store.commitChanges(); 
}	
//----------------------------Fin procesarCatalogo-------------------------------

//--------------------------Store catalogo--------------------------------------
var catalogo = new Ext.data.JsonStore({
	id		: 'catalogos',
	root	: 'registros',
	fields: ['clave','descripcion','loadMsg'], 
	url	: '15mancatalogoClasificacion01Ext.data.jsp' ,
	baseParams: {
		informacion: 'Clasificacion.Carga.Catalogo'
	},
	totalProperty: 'total',
	autoLoad: true,
	listeners: {
		load : procesarCatalogo,
		exception: NE.util.mostrarDataProxyError,
		beforeload: NE.util.initMensajeCargaCombo
	}		
});
//--------------------------Fin Store catalogo-----------------------------------

//------------------------------------procesarDatos-----------------------------
var procesarDatos = function (store,arrRegistros,opts){
	var grid = Ext.getCmp('grid');
	var el = grid.getGridEl();
	fp.el.unmask();
	el.unmask();
	if(arrRegistros!=null){
		if (!grid.isVisible()) {
				grid.show();
		}
		if(store.getTotalCount()>0){
			el.unmask();
		}else{
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
	
	} else{
	
	}
	
};
//------------------------------------FIN procesarDatos-------------------------

//---------------------------- consultaDataGrid----------------------------------
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15mancatalogoSubtipoEPOExt01.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'CLAVE'},
			{name: 'DESCRIPCION'},
			{name: 'CLAVETIPO'},
			{name: 'TIPO'}
		],
		totalProperty 	: 'total',
		messageProperty: 'msg',
		autoLoad			: true,
		listeners		: {
			load			: procesarDatos,
			exception	: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarDatos(null, null, null);
				}
			}
		}
	});
//----------------------------Fin consultaDataGrid----------------------------------

//-------------------------------- gridConsulta---------------------------------
var grid = new Ext.grid.GridPanel({
	store			: consultaDataGrid,
	id				: 'grid',
	title			:'Contenido del Cat�logo',
	margins		: '20 0 0 0',
	hidden		:false,
	style			: 'margin:0 auto;',
	height		: 450,
	width			: 843,
	frame			: true,
	loadMask		: true,
	enableColumnMove: false,
	enableColumnHide: false,
	tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
					{
						iconCls	: 'icon-register-edit',
						text		: 'Editar Registro',
						disabled	: false,
						handler	: function(){
										var grid	  				= Ext.getCmp("grid");
										var seleccionados  	= grid.getSelectionModel().getSelections();
										if(       seleccionados.length == 0){
											Ext.MessageBox.alert('Aviso','Debe seleccionar un registro.');
											return;
										} else if( seleccionados.length >  1){
											Ext.MessageBox.alert('Aviso','S�lo se puede editar un registro a la vez.');
											return;
										}
										var formTit	  				= Ext.getCmp("forma");
										formTit.setTitle('Editar Cat�logo: Modificar Registro');
										var respuesta 			= new Object();
										respuesta['record']	= seleccionados[0];										
										Ext.getCmp('comboTipo').setValue(respuesta.record.data.CLAVETIPO);
										Ext.getCmp('tfSubtipo').setValue(respuesta.record.data.DESCRIPCION);
										Ext.getCmp('tfAccion').setValue(1);
										Ext.getCmp('tfAyuda').setValue(respuesta.record.data.CLAVE);
										Ext.getCmp("grid").hide();	
										Ext.getCmp('forma').show();
						}		
					},{
						iconCls	: 'icon-register-add',
						text		: 'Agregar Registro',
						disabled	: false,
						handler	: function(){
										var formTit	  				= Ext.getCmp("forma");
										formTit.setTitle('Editar Cat�logo: Agregar Registro');
										Ext.getCmp('forma').show();	
										Ext.getCmp('grid').hide();
						}
					},{
						iconCls	: 'icon-register-delete',
						text		: 'Eliminar Registro',
						disabled	: false,
						handler	: function(){
							var grid	  	 = Ext.getCmp("grid");
							var seleccionados   = grid.getSelectionModel().getSelections(); 
							var numeroRegistros = seleccionados.length;
							var aviso			  = "";
							if(numeroRegistros == 0){
								Ext.MessageBox.alert('Aviso','Debe seleccionar al menos un registro.');
								return;
							} else if(numeroRegistros > 1000){
								Ext.MessageBox.alert('Aviso','S�lo se permiten como m�ximo 1000 registros seleccionados.');
								return;
							} else if(numeroRegistros == 1  ){
								aviso = 'El registro seleccionado ser� eliminado, �Desea usted continuar?';
							} else if(numeroRegistros >  1  ){
								Ext.MessageBox.alert('Aviso','Solo es permitido eliminar un registro.');
								return; 
							}
							var respuesta 			= new Object();
							respuesta['record']	= seleccionados[0];			
							Ext.getCmp('tfAyuda').setValue(respuesta.record.data.CLAVE);
							Ext.Msg.show({
							  title: 'Eliminar',
							  msg: 'Esta seguro de querer eliminar el registro ?',
							  modal: true,
							  icon: Ext.Msg.QUESTION,
							  buttons: Ext.Msg.OKCANCEL,
							  fn: function (btn,text){
									if (btn == 'ok'){
										Ext.Ajax.request({
										url: '15mancatalogoSubtipoEPOExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Eliminar', 
										  clave : Ext.getCmp('tfAyuda').getValue()
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var eliminar = info.eliminar;
											if (eliminar){
												  Ext.Msg.show({
												  title: 'Eliminar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
													 }
												  });//fin Msg
											} else {
												  Ext.Msg.show({
												  title: 'Eliminar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
									} else {
										Ext.getCmp('tfAyuda').reset();
									}
								 }
							  });//fin Msg
						}
					}
				]
	},
	columns: [
	{
		header	: 'Clave Tipo',
		tooltip	: 'Clave Tipo',
		dataIndex: 'CLAVETIPO',
		sortable	: true,
		width		: 100,			
		resizable: true,				
		align		: 'center'
	}, {
		header	: 'Descripci�n Tipo',
		tooltip	: 'Descripci�n Tipo',
		dataIndex: 'TIPO',
		sortable	: true,
		resizable: true,
		width		: 300,
		hidden	: false,	
		align		: 'center',
		renderer : function(value){
						return "<div align='left'>"+value+"</div>"									
					}
	},	{
		header	: 'Clave Subtipo',
		tooltip	: 'Clave Subtipo',
		dataIndex: 'CLAVE',
		fixed		:	false,
		sortable	: true,
		width		: 100,
		align		: 'center'				
	},{
		header	: 'Descripci�n Subtipo',
		tooltip	: 'Descripci�n Subtipo',
		dataIndex: 'DESCRIPCION',
		sortable	: true,
		resizable: true,
		width		: 300,
		hidden	: false,	
		align		: 'center',
		renderer : function(value){
						return "<div align='left'>"+value+"</div>"									
					}
	}, {
    xtype			: 'actioncolumn',
    header			: 'Seleccionar',
    tooltip			: 'Seleccionar',
	 hidden			: true,
    width			: 80,							
    align			: 'center',
    items			: [
     {
      getClass		: function(valor, metadata, registro, rowIndex, colIndex, store) {
							  this.items[0].tooltip = 'Modificar';
							  return 'icoModificar';										
							},
      handler		: function(grid, rowIndex, colIndex, item, event){
							var registro = grid.getStore().getAt(rowIndex);
							Ext.getCmp('comboTipo').setValue(registro.get('CLAVETIPO'));
							Ext.getCmp('tfSubtipo').setValue(registro.get('DESCRIPCION'));
							Ext.getCmp('tfAccion').setValue(1);
							Ext.getCmp('tfAyuda').setValue(registro.get('CLAVE'));
					}//fin handler
			},{
      getClass		: function(valor, metadata, registro, rowIndex, colIndex, store) {
							  this.items[0].tooltip = 'Eliminar';
							  return 'icoCancelar';										
							},
      handler		: function(grid, rowIndex, colIndex, item, event){
							var registro = grid.getStore().getAt(rowIndex);
							Ext.getCmp('tfAyuda').setValue(registro.get('CLAVE'));
							Ext.Msg.show({
							  title: 'Eliminar',
							  msg: 'Esta seguro de querer eliminar el registro ?',
							  modal: true,
							  icon: Ext.Msg.QUESTION,
							  buttons: Ext.Msg.OKCANCEL,
							  fn: function (btn,text){
									if (btn == 'ok'){
										Ext.Ajax.request({
										url: '15mancatalogoSubtipoEPOExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Eliminar', 
										  clave : Ext.getCmp('tfAyuda').getValue()
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var eliminar = info.eliminar;
											if (eliminar){
												  Ext.Msg.show({
												  title: 'Eliminar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
													 }
												  });//fin Msg
											} else {
												  Ext.Msg.show({
												  title: 'Eliminar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
									} else {
										Ext.getCmp('tfAyuda').reset();
									}
								 }
							  });//fin Msg
					}//fin handler
			} ]
		}
	],
	bbar: {
		autoScroll:true,
		id: 'barra',
		displayInfo: true
		}
});
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------elementosForma---------------------------------	
var elementosForma =  [	
  {
  xtype			: 'compositefield',
  id				:'cmpCB',
  combineErrors: false,
  msgTarget		: 'side',
  hidden 		: true,
  items: [
    {
    xtype		: 'combo',
    name			: 'cmbCatalogos',
    hiddenName	: '_cmbCat',
    id			: 'cmbCATHIDEN',	
    fieldLabel	: 'Cat�logos', 
    mode			: 'local',	
    forceSelection : true,	
    triggerAction  : 'all',	
    typeAhead		: true,
    minChars 		: 1,	
    store 			: catalogo,		
    valueField 	: 'clave',
    displayField	: 'descripcion',	
    editable		: true,
    typeAhead		: true,
    width			:  330,
	 listeners		: {
		select		: {
			fn			: function(combo) {
							var valor  = combo.getValue();
							}// FIN fn
							}//FIN select
						}//FIN listeners
    },{
    xtype :'displayfield',
    frame :true,
    border: false,
    value :'',
    width	: 10
    },{
    xtype   : 'button',
    text    : 'Buscar',					
    tooltip :	'Buscar',
    iconCls : 'icoBuscar',
    id      : 'btnBuscar', 
    handler : function(boton, evento) { 
	 var valor = Ext.getCmp('cmbCAT').getValue();
		if (valor == 2){					//Tasa
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
		} else if (valor == 11){		//Estatus Documento
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
		} else if (valor == 12){		//Cambio Estatus
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
		} else if (valor == 13){		//Clasificacion ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
		} else if (valor == 21){		//Subsector
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
		} else if (valor == 33){		//D�as Inh�biles
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
		} else if (valor == 34){		//Productos
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
		} else if(valor == 36){			//Personas Facultadas por Banco
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
		} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
		} else if(valor == 39){			//Version Convenio
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
		} else if(valor == 40){			//Bancos TEF
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
		} else if (valor == 47){		//Firma Cedulas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
		} else if (valor == 49){		//Plazos por Producto
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
		} else if (valor == 60){		//Area Promocion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
		} else if (valor == 62){		//Subdireccion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
		} else if (valor == 63){		//Lider Promotor
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
		} else if (valor == 65){		//Subtipo EPO
			fp.getForm().reset();
			Ext.getCmp('cmbCAT').setValue('65');
			Ext.getCmp('comboTipo').setValue("");
			consultaDataGrid.load();
			//window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
		} else if (valor == 68){		//Ventanillas	
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
		} else if (valor == 70){		//D�as Inh�biles por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
		} else if (valor == 71){		//D�as Inh�biles EPO por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
		} else if (valor == 103){		//Programa a Fondo JR
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
		}else if (valor == 677){		//Clasificaci�n Epo
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
		}else {
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo.jsp";
		}
	 }
    }]			
	},{
	xtype				: 'combo',
	id					: 'comboTipo',
	name				: 'tipo',
	hiddenName		: 'tipoHidden',
	mode				: 'local',
	fieldLabel		: 'Tipo', 
	displayField 	: 'descripcion',
	valueField 		: 'clave',
	store				: storeTipo,
	forceSelection : true,
	triggerAction 	: 'all',   
	allowBlank		: true,
	typeAhead		: true,
	minChars 		: 1
  },{
	xtype				: 'textfield',
	id        		: 'tfSubtipo',
	name				: 'subtipo',
	fieldLabel 		: 'Descripci�n Subtipo',
	width				: 150,
	maxLength		: 50,
	margins			: '0 20 0 0',
	listeners		: {
		change		: function(text, newText){
								text.setValue(newText.toUpperCase());
							}// fin change
						}// fin listeners
  },{
	xtype				: 'textfield',
	id        		: 'tfAyuda',
	name				: 'ayuda',
	fieldLabel 		: 'Ayuda',
	width				: 150,
	maxLength		: 10,
	margins			: '0 20 0 0',
	hidden			: true
  },{
	xtype				: 'textfield',
	id        		: 'tfAccion',
	name				: 'accion',
	fieldLabel 		: 'Acci�n',
	width				: 150,
	maxLength		: 10,
	margins			: '0 20 0 0',
	hidden			: true
  }
];
//-----------------------------Fin elementosForma-------------------------------

//-------------------------------Panel Consulta---------------------------------
var fp = new Ext.form.FormPanel({
	id					: 'forma',
	width				: 843,
	title				: 'Criterios de B�squeda del Cat�logo',
	frame				: true,		
	collapsible		: true,
	titleCollapse	: true,
	hidden			: false,
	style				: 'margin:0 auto;',
	bodyStyle		: 'padding: 6px',
	defaults			: {
		msgTarget	: 'side',
		anchor		: '-20'
							},
	items				: elementosForma,
	buttons			: [
      {
		text			: 'Guardar',
		id				: 'btnGuardar',
		iconCls		: 'icoGuardar',
		hidden		: false,
		handler		: function (boton, evento){
							if (Ext.getCmp('comboTipo').getValue() !=""){
								if (Ext.getCmp('tfSubtipo').getValue() !=""){
								 if(Ext.getCmp('comboTipo').isValid() & Ext.getCmp('tfSubtipo').isValid()){
								 } else{return;}
									if(Ext.getCmp('tfAccion').getValue() !=1){//agregar
										Ext.Ajax.request({
										url: '15mancatalogoSubtipoEPOExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Agregar',
										  tipo: Ext.getCmp('comboTipo').getValue(),
										  subtipo: Ext.getCmp('tfSubtipo').getValue()
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var agregar = info.agregar;
											if (agregar){
												  Ext.Msg.show({
												  title: 'Agregar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
														Ext.getCmp('forma').hide();
													 }
												  });//fin Msg
											} else {
												  Ext.Msg.show({
												  title: 'Agregar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
														Ext.getCmp('forma').hide();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
									} else { // Modificar
										Ext.Ajax.request({
										url: '15mancatalogoSubtipoEPOExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Modificar',
										  tipo: Ext.getCmp('comboTipo').getValue(),
										  subtipo: Ext.getCmp('tfSubtipo').getValue(),
										  clave : Ext.getCmp('tfAyuda').getValue()										  
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var agregar = info.agregar;
											if (agregar){
												  Ext.Msg.show({
												  title: 'Actualizar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
														Ext.getCmp('forma').hide();
													 }
												  });//fin Msg
											} else {
												  Ext.Msg.show({
												  title: 'Actualizar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
														Ext.getCmp('forma').hide();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
									}
									
								} else {
									Ext.getCmp('tfSubtipo').markInvalid('Introduzca la descripci�n del Subtipo EPO.');
								}	
							} else {
								Ext.getCmp('comboTipo').markInvalid('Seleccione el tipo de EPO.');
							}
							}
		},{
		text			: 'Cancelar',
		id				: 'btnCancelar',
		iconCls		: 'icoCancelar',
		hidden		: false,
		handler		: function (boton, evento){
							var formTit	  				= Ext.getCmp("forma");
								 formTit.setTitle('Criterios de B�squeda del Cat�logo');
							fp.getForm().reset();
							Ext.getCmp('cmbCAT').setValue('65');
							Ext.getCmp('comboTipo').setValue("");
							Ext.getCmp('grid').show();
							Ext.getCmp('forma').hide();
							}
		},{
		text			: 'Limpiar',
		id				: 'btnLimpiar',
		iconCls		: 'icoLimpiar',
		hidden		: true,
		handler		: function (boton, evento){
							fp.getForm().reset();
							Ext.getCmp('cmbCAT').setValue('65');
							Ext.getCmp('comboTipo').setValue("");
							consultaDataGrid.load();
						}//FIN handler Limpiar
		},{
		text			: 'Aceptar',
		id				: 'btnAceptar',
		iconCls		: 'icoAceptar',
		hidden		: true,
		handler		: function (boton, evento){
							if (Ext.getCmp('comboTipo').getValue() !=""){
								if (Ext.getCmp('tfSubtipo').getValue() !=""){
								 if(Ext.getCmp('comboTipo').isValid() & Ext.getCmp('tfSubtipo').isValid()){
								 } else{return;}
									if(Ext.getCmp('tfAccion').getValue() !=1){//agregar
										Ext.Ajax.request({
										url: '15mancatalogoSubtipoEPOExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Agregar',
										  tipo: Ext.getCmp('comboTipo').getValue(),
										  subtipo: Ext.getCmp('tfSubtipo').getValue()
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var agregar = info.agregar;
											if (agregar){
												  Ext.Msg.show({
												  title: 'Agregar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
														Ext.getCmp('forma').hide();
													 }
												  });//fin Msg
											} else {
												  Ext.Msg.show({
												  title: 'Agregar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
														Ext.getCmp('forma').hide();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
									} else { // Modificar
										Ext.Ajax.request({
										url: '15mancatalogoSubtipoEPOExt01.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
										  informacion: 'Modificar',
										  tipo: Ext.getCmp('comboTipo').getValue(),
										  subtipo: Ext.getCmp('tfSubtipo').getValue(),
										  clave : Ext.getCmp('tfAyuda').getValue()										  
										}),
										success : function(response) {
											var info = Ext.util.JSON.decode(response.responseText);
											var agregar = info.agregar;
											if (agregar){
												  Ext.Msg.show({
												  title: 'Actualizar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.INFO,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
														Ext.getCmp('forma').hide();
													 }
												  });//fin Msg
											} else {
												  Ext.Msg.show({
												  title: 'Actualizar',
												  msg: info.msg,
												  modal: true,
												  icon: Ext.Msg.ERROR,
												  buttons: Ext.Msg.OK,
												  fn: function (){
														fp.getForm().reset();
														Ext.getCmp('cmbCAT').setValue('65');
														Ext.getCmp('comboTipo').setValue("");
														consultaDataGrid.load();
														Ext.getCmp('forma').hide();
													 }
												  });//fin Msg
											}
										 }
										});//fin AJAX
									}
									
								} else {
									Ext.getCmp('tfSubtipo').markInvalid('Introduzca la descripci�n del Subtipo EPO.');
								}	
							} else {
								Ext.getCmp('comboTipo').markInvalid('Seleccione el tipo de EPO.');
							}
						}//FIN handler Aceptar
		}
 ]
});
//------------------------------Fin Panel Consulta------------------------------


	var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		autoHeight:true,//height:			80,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		'padding:6px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		items: 			[
			 {
				 xtype		: 'combo',
				 name			: 'cmbCatalogos',
				 hiddenName	: '_cmbCat',
				 id			: 'cmbCAT',	
				 fieldLabel	: 'Cat�logo', 
				 mode			: 'local',	
				 forceSelection : true,	
				 triggerAction  : 'all',	
				 typeAhead		: true,
				 minChars 		: 1,	
				 store 			: catalogo,		
				 valueField 	: 'clave',
				 displayField	: 'descripcion',	
				 editable		: true,
				 typeAhead		: true,
				 anchor			: '95%',
				 listeners		: {
					select		: {
						fn			: function(combo) {
										var valor = Ext.getCmp('cmbCAT').getValue();
										if (valor == 2){					//Tasa
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
										} else if (valor == 11){		//Estatus Documento
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
										} else if (valor == 12){		//Cambio Estatus
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
										} else if (valor == 13){		//Clasificacion ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
										} else if (valor == 21){		//Subsector
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
										} else if (valor == 33){		//D�as Inh�biles
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
										} else if (valor == 34){		//Productos
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
										} else if(valor == 36){			//Personas Facultadas por Banco
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
										} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
										} else if(valor == 39){			//Version Convenio
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
										} else if(valor == 40){			//Bancos TEF
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
										} else if (valor == 47){		//Firma Cedulas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
										} else if (valor == 49){		//Plazos por Producto
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
										} else if (valor == 60){		//Area Promocion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
										} else if (valor == 62){		//Subdireccion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
										} else if (valor == 63){		//Lider Promotor
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
										} else if (valor == 65){		//Subtipo EPO
											fp.getForm().reset();
											Ext.getCmp('cmbCAT').setValue('65');
											Ext.getCmp('forma').hide();
											Ext.getCmp('comboTipo').setValue("");
											consultaDataGrid.load();
											//window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
										} else if (valor == 66){		//Sector EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
										} else if (valor == 68){		//Ventanillas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
										} else if (valor == 70){		//D�as Inh�biles por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
										}else if (valor == 71){		//D�as Inh�biles EPO por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
										}else if (valor == 677){		//Clasificaci�n Epo
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
										} else if (valor == 103){		//Programa a Fondo JR
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
										}
										}// FIN fn
										}//FIN select
									}//FIN listeners
				 }],
		monitorValid: 	false
	});	

//----------------------------Contenedor Principal------------------------------	
var pnl = new Ext.Container({
	id			: 'contenedorPrincipal',
	applyTo	: 'areaContenido',
	width		: 949,
	//style		: 'margin:0 auto;',
	items		: [
					NE.util.getEspaciador(20),	
					panelFormaCatalogos,
					NE.util.getEspaciador(20),	
					fp,
					NE.util.getEspaciador(20),
					grid,
					NE.util.getEspaciador(20)
					]
});	
//-----------------------------Fin Contenedor Principal-------------------------
Ext.getCmp('forma').setVisible(false);
});//-----------------------------------------------Fin Ext.onReady(function(){}