Ext.onReady(function() {

	// Ext.QuickTips.init();
		
	//----------------------------------- FUNCIONES --------------------------------------
	var doBuscar = function(boton, evento){
 
		var grid	  				  		= Ext.getCmp("gridPanelCatalogo");
			
		var panelFormaCatalogo 		= Ext.getCmp('panelFormaCatalogo');
		panelFormaCatalogo.el.mask('Realizando B�squeda...', 'x-mask-loading');
		
		var gridPanelCatalogoData = Ext.StoreMgr.key('gridPanelCatalogoDataStore');
		gridPanelCatalogoData.load({
				params: 				Ext.apply(panelFormaCatalogo.getForm().getValues(),{
				informacion: 		'Buscar', //Generar datos para la consulta
				claveCatalogo: 	grid.claveCatalogo
			})
		});
 
	}
	
	var doLimpiarForma 	= function(){
		
		var panelFormaCatalogo = Ext.getCmp('panelFormaCatalogo');
		for(var elemento=panelFormaCatalogo.items.length-1;elemento>=0;elemento--){
			panelFormaCatalogo.getComponent(elemento).reset();
		}
		
	}
	
	var doGuardarEdicion = function(){
		
		var panelFormaEdicionCatalogo = Ext.getCmp("panelFormaEdicionCatalogo");
		var grid	  							= Ext.getCmp("gridPanelCatalogo");
		var columnModel 					= grid.getColumnModel(); // Obtener modelo de columna
		
		// Si la forma no es invalida, suspender operaci�n
		if( !panelFormaEdicionCatalogo.getForm().isValid() ){
			return;
		}
		
		// Copiar contenido actual de la forma
		var record_data 		= new Object();
		var record_modified 	= new Object();
		
		if( panelFormaEdicionCatalogo.editingMode        == "AGREGAR_REGISTRO" ){
   
			// Copiar contenido del registro a la forma de edicion
			panelFormaEdicionCatalogo.items.each(function(item){
				if( item.primaryKey && item.readOnly ){
					record_data[item.gridColumnId] = item.defaultValue;
				}else{
					var itemValue = item.getValue();
					if(Ext.isDate(itemValue)){
						itemValue = item.formatDate(itemValue); //item.getRawValue();
					}
					record_data[item.gridColumnId] = itemValue;
				}
    		});
 
    		panelFormaEdicionCatalogo.el.mask('Insertando Registro...','x-mask-loading');
    		
			Ext.Ajax.request({
				url: '15mancatalogo01ext.data.jsp',
				params: {
				  informacion: 		"GuardarAlta",
				  pageId:				pageId,
				  claveCatalogo: 		grid.claveCatalogo,
				  data: 					Ext.encode(record_data),
				  modified:				Ext.encode(record_modified),
				  id:						panelFormaEdicionCatalogo.recordId
				  //, row_index:		rowIndex
				 },
				 callback: procesarSuccessFailureGuardarEdicion
			});
			
		} else if( panelFormaEdicionCatalogo.editingMode == "EDITAR_REGISTRO"  ){
			
			var gridPanelCatalogoData 	= Ext.StoreMgr.key('gridPanelCatalogoDataStore');
			var record						= gridPanelCatalogoData.getById(panelFormaEdicionCatalogo.recordId);

			// Copiar contenido original de los registros
			panelFormaEdicionCatalogo.items.each(function(item){
				var recordValue = record.json[item.gridColumnId];
				if( Ext.isEmpty(recordValue) ){
					record_data[item.gridColumnId] = "";
				} else {
					record_data[item.gridColumnId] = recordValue;
				}
    		});
 
			// Copiar registros que fueron modificados
			var hayRegistosModificados = false;
			panelFormaEdicionCatalogo.items.each(function(item){
				var itemValue = item.getValue();
				if(Ext.isDate(itemValue)){
					itemValue = item.formatDate(itemValue); //item.getRawValue();
				}
				if( itemValue != record_data[item.gridColumnId] ){
					record_modified[item.gridColumnId] = record_data[item.gridColumnId];
					record_data[item.gridColumnId]     = itemValue;
					hayRegistosModificados = true;
				}
    		});
			
    		if(!hayRegistosModificados){
    			Ext.Msg.alert("Aviso","No se ha modificado ning�n campo.");
    		} else {
    			
    			panelFormaEdicionCatalogo.el.mask('Guardando Modificaciones...','x-mask-loading');
    			
				Ext.Ajax.request({
					url: '15mancatalogo01ext.data.jsp',
					params: {
						informacion: 		"GuardarModificacion",
						pageId:				pageId,
						claveCatalogo: 	grid.claveCatalogo,
						data: 				Ext.encode(record_data),
						modified:			Ext.encode(record_modified),
						id:					panelFormaEdicionCatalogo.recordId
						// row_index:		rowIndex
					},
					callback: procesarSuccessFailureGuardarEdicion
				});
			}
			
		}
		
	}
	
	var doCancelarEdicion 	= function(){
		var respuesta = new Object();
		respuesta.updateCatalogo = false;
		consultaCatalogo("REGRESAR_A_CATALOGO", respuesta);
	}
	
	var comboRenderer = function(value, metadata, record, rowIndex, colIndex, store){
 
	 	var grid	  					= Ext.getCmp("gridPanelCatalogo");
	 	
	 	if( Ext.isEmpty( grid.columnStore ) || colIndex >= grid.columnStore.length ){
	 		return value;
	 	}
	 	
	 	var columnStore 			= grid.columnStore[colIndex];
	 	for(var i=0;i<columnStore.getTotalCount();i++){
	 		var item = columnStore.data.items[i];
	 		if( Ext.isEmpty(item) ){
	 			continue;
	 		} else if(value == item.json[0]){
	 			return item.json[1];
	 		}
	 	}
	 	
		return value;
		
	}
	
	var getComponenteFormaCatalogo = function(elemento){
	
		if("checkbox" == elemento.type){
			
			var myCheckb = {  
				xtype: 		'checkbox', 	  
            fieldLabel: elemento.fieldLabel,		  
            name: 		elemento.name, 	  
            id: 			elemento.name 		  
			};
			if(!Ext.isEmpty(elemento.label)){
				myCheckb.boxLabel 	= elemento.label;
			}
         myCheckb.inputValue 	= elemento.value;
���������return myCheckb;	

		} else if("checkboxgroup" == elemento.type) {

			var myCheckBoxGroup = {
				xtype: 		'checkboxgroup',
            fieldLabel: elemento.fieldLabel,	
            name: 		elemento.name,
				id:			elemento.name,
				items:		[]
			};
			if(!Ext.isEmpty(elemento.columns)){
				myCheckBoxGroup.columns = parseInt(elemento.columns,10);
			}
			for(var itemIndex=0;itemIndex<elemento.items.length;itemIndex++){
				var myCheckBox = {
					boxLabel:    elemento.items[itemIndex].label, 
					name: 		 elemento.items[itemIndex].name
				};
				if(!Ext.isEmpty(elemento.items[itemIndex].checked)){
					myCheckBox.checked = elemento.items[itemIndex].checked;
				}
				myCheckBoxGroup.items[itemIndex] = myCheckBox;
			}
			return myCheckBoxGroup;
			
		}else if("textfield" == elemento.type){
			
			var myTextBox = {
				xtype: 		'textfield',
            fieldLabel: elemento.fieldLabel,	
            name: 		elemento.name,
				id:			elemento.name
			};
			if(elemento.toUpperCase){
				myTextBox["plugins"] = "uppercasetf"; 
			}
			
			return myTextBox;
			
		}else if("radiogroup" == elemento.type){
			
			var myRadioGroup = {
			  xtype: 		'radiogroup',
			  fieldLabel: 	elemento.fieldLabel,	
			  name: 			elemento.name,
			  id: 			elemento.name,
			  items: 		[]
			};
			for(var itemIndex=0;itemIndex<elemento.items.length;itemIndex++){
				var myRadio = {
					boxLabel:    elemento.items[itemIndex].label, 
					name: 		 elemento.name, 
					inputValue:  elemento.items[itemIndex].value
				};
				if(!Ext.isEmpty(elemento.items[itemIndex].checked)){
					myRadio.checked = elemento.items[itemIndex].checked;
				}
				myRadioGroup.items[itemIndex] = myRadio;
			}
			if(!Ext.isEmpty(elemento.columns)){
				myRadioGroup.columns = parseInt(elemento.columns,10);
			}
			return myRadioGroup;
			
		}else if("combo" == elemento.type){
			
			var myComboStore = new Ext.data.ArrayStore({
					autoDestroy: 	true,
					fields: 			['clave', 'descripcion'],
					data: 			elemento.data
			});
			var myCombo = {
				xtype: 				'combo',
				name:					elemento.name + "_combo",
				id:					elemento.name + "_comboid",
				fieldLabel: 		elemento.fieldLabel,
				hiddenName: 		elemento.name,
				displayField: 		'descripcion',
				valueField: 		'clave',
				forceSelection: 	true, //antes false
				emptyText: 			'Seleccione...',
				triggerAction: 	'all',
				typeAhead: 			true,
				minChars: 			1,
				editable: 			true,
				mode: 				'local',
				store:				myComboStore
			};
			return myCombo;
			
		}else if("label" == elemento.type){
			
			var myPadding;
			if(Ext.isEmpty(elemento.padding)){
				myPadding = 'padding:10px;';
			}else{
				myPadding = 'padding:'+elemento.padding+'px;';
			}
			var myLabel  = {
				xtype: 	'label',
				name:		elemento.name,
				id: 	 	elemento.name,
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:center;'+myPadding,
				html:  	elemento.fieldLabel
			};		
			return myLabel;
			
		}else if("datefield" == elemento.type){
 
			var myDateField = {
				xtype: 			'compositefield',
				fieldLabel: 	elemento.fieldLabel,
				combineErrors: false,
				msgTarget: 		'side',
				items: [
					{
						xtype: 'datefield',
						name: elemento.name,
						id: 	elemento.name,
						allowBlank: true,
						startDay: 	0,
						width: 		100,
						msgTarget: 'side', 
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			};
			return myDateField;
			
		}else if("daterange" == elemento.type){
 
			var myDateRange = {
				xtype: 			'compositefield',
				fieldLabel: 	elemento.fieldLabel,
				combineErrors: false,
				msgTarget: 		'side',
				items: 			[]
			};
			var myStartDate = {
				xtype: 					'datefield',
				name: 					elemento.startdatename,
				id: 						elemento.startdatename,
				allowBlank: 			true,
				startDay: 				0,
				width: 					100,
				msgTarget: 				'side',
				vtype: 					'rangofecha', 
				campoFinFecha: 		elemento.enddatename,
				margins: 				'0 20 0 0'  //necesario para mostrar el icono de error
			};
			var joinText = {
				xtype: 					'displayfield',
				value: 					elemento.jointext
			};
			if(Ext.isEmpty(elemento.jointextwidth)){
				joinText.width = 20;
			}else{
				joinText.width = parseInt(elemento.jointextwidth,10);
			}
			var myEndDate = {
				xtype: 					'datefield',
				name: 					elemento.enddatename,
				id: 						elemento.enddatename,
				allowBlank: 			true,
				startDay: 				1,
				width: 					100,
				msgTarget: 				'side',
				vtype: 					'rangofecha', 
				campoInicioFecha: 	elemento.startdatename,
				margins: 				'0 20 0 0'  //necesario para mostrar el icono de error
			};
			myDateRange.items[0] = myStartDate;
			myDateRange.items[1] = joinText;
			myDateRange.items[2] = myEndDate;
			return myDateRange;
			
		}else if("timefield" == elemento.type){
 
			var myTimeField = {
				xtype: 			'compositefield',
				fieldLabel: 	elemento.fieldLabel,
				combineErrors: false,
				msgTarget: 		'side',
				items: 			[]
			};
			var myTimeItem = {
				xtype: 		'timefield',
				name: 		elemento.name,
				id: 			elemento.name,
				editable:	true,
				width: 		100,
				format:		"H:i:s",
				msgTarget: 	'side', 
				margins: 	'0 20 0 0'  //necesario para mostrar el icono de error
			};
			myTimeField.items[0] = myTimeItem;
			return myTimeField;
			
		}else if("timerange" == elemento.type){
 
			var myTimeRange = {
				xtype: 			'compositefield',
				fieldLabel: 	elemento.fieldLabel,
				combineErrors: false,
				msgTarget: 		'side',
				items: 			[]
			};
			var myStartTime = {
				xtype: 		'timefield',
				name: 		elemento.starttimename,
				id: 			elemento.starttimename,
				editable:	true,
				width: 		100,
				format:		"H:i:s",
				msgTarget: 	'side', 
				margins: 	'0 20 0 0'  //necesario para mostrar el icono de error
			};
			var joinText = {
				xtype: 					'displayfield',
				value: 					elemento.jointext
			};
			if(Ext.isEmpty(elemento.jointextwidth)){
				joinText.width = 20;
			}else{
				joinText.width = parseInt(elemento.jointextwidth,10);
			}
			var myEndTime = {
				xtype: 		'timefield',
				name: 		elemento.endtimename,
				id: 			elemento.endtimename,
				editable:	true,
				width: 		100,
				format:		"H:i:s",
				msgTarget: 	'side', 
				margins: 	'0 20 0 0'  //necesario para mostrar el icono de error
			};
			myTimeRange.items[0] = myStartTime;
			myTimeRange.items[1] = joinText;
			myTimeRange.items[2] = myEndTime;
			return myTimeRange;
			
		}else if("numberfield" == elemento.type){
			
			var myNumberField = {
				xtype: 			'compositefield',
				fieldLabel: 	elemento.fieldLabel,
				combineErrors: false,
				msgTarget: 		'side',
				items: 			[]
			};
			var myNumberItem = {
				xtype: 		'spinnerfield',
				name: 		elemento.name,
				id:			elemento.name,
				msgTarget: 	'side', 
				margins: 	'0 20 0 0'
			};
			if(Ext.isEmpty(elemento.width)){
				myNumberItem.width = 100;
			}else{
				myNumberItem.width = parseInt(elemento.width,10);
			}
			myNumberField.items[0] = myNumberItem;
			/*
			var myIntegerField = {
				xtype: 							'spinnerfield',
				fieldLabel: 					'Test',
				name: 							'test',
				minValue: 						0,
				maxValue: 						100,
				allowDecimals: 				true,
				decimalPrecision: 			1,
				incrementValue: 				0.4,
				alternateIncrementValue: 	2.1,
				accelerate: true
			};*/
			return myNumberField;
			
		}else if("numberrange" == elemento.type){
			
			var myNumberRange = {
				xtype: 			'compositefield',
				fieldLabel: 	elemento.fieldLabel,
				combineErrors: false,
				msgTarget: 		'side',
				items: 			[]
			};
			var myStartNumber = {
				xtype: 		'spinnerfield',
				name: 		elemento.startnumbername,
				id:			elemento.startnumbername,
				msgTarget: 	'side', 
				margins: 	'0 20 0 0'
			};
			if(Ext.isEmpty(elemento.width)){
				myStartNumber.width = 100;
			}else{
				myStartNumber.width = parseInt(elemento.width,10);
			}
			var joinText = {
				xtype: 					'displayfield',
				value: 					elemento.jointext
			};
			if(Ext.isEmpty(elemento.jointextwidth)){
				joinText.width = 20;
			}else{
				joinText.width = parseInt(elemento.jointextwidth,10);
			}
			var myEndNumber = {
				xtype: 		'spinnerfield',
				name: 		elemento.endnumbername,
				id:			elemento.endnumbername,
				msgTarget: 	'side', 
				margins: 	'0 20 0 0'
			};
			if(Ext.isEmpty(elemento.width)){
				myEndNumber.width = 100;
			}else{
				myEndNumber.width = parseInt(elemento.width,10);
			}
			myNumberRange.items[0] = myStartNumber;
			myNumberRange.items[1] = joinText;
			myNumberRange.items[2] = myEndNumber;
			
			return myNumberRange;
			
		}else if( "textarea" == elemento.type){
			
			var myTextArea = {
				 xtype: 			'textarea',
				 fieldLabel: 	elemento.fieldLabel,
				 name:			elemento.name,
				 id:				elemento.name,
				 maxLength: 	parseInt(elemento.maxlength,10),
				 msgTarget: 	'side',
				 allowBlank: 	true
			};
			
			return myTextArea;
			
		}
	
		return null;
			
	}
	
	var getComponenteFormaEdicionCatalogo = function(elemento){
 
		if(        "combo"       == elemento.type ){
			
			var myCombo = {
				xtype: 					'combo',
				id:						"edicion." + elemento.id + "_comboid",
				gridColumnId:			elemento.id,
				fieldLabel: 			elemento.name,
				displayField: 			'descripcion',
				valueField: 			'clave',
				forceSelection: 		true,
				readOnly:				elemento.readOnly,
				allowBlank: 			elemento.allowBlank,
				name:						"edicion." + elemento.id + "_combo",
				triggerAction: 		'all',
				typeAhead: 				true,
				minChars: 				1,
				editable: 				true,
				mode: 					'local',
				hiddenName: 			elemento.id,
				defaultValue:			elemento.defaultValue,
				emptyText:				elemento.emptyText,
				primaryKey:				elemento.primaryKey?true:false,
				defaultEmptyText:		elemento.emptyText,
				defaultAllowBlank: 	elemento.allowBlank
			};
			return myCombo;
			
		} else if( "textcolumn"   == elemento.type ){
			
			var myTextColumn = {
				xtype:					"textfield",
				id:						"edicion." + elemento.id,
				gridColumnId:			elemento.id,
				fieldLabel:				elemento.name,
				readOnly:				elemento.readOnly,
				allowBlank: 			elemento.allowBlank,
				maxLength:				elemento.maxLength,
				defaultValue:			elemento.defaultValue,
				primaryKey:				elemento.primaryKey?true:false,
				defaultEmptyText:		null,
				defaultAllowBlank: 	elemento.allowBlank
			}
			if(!Ext.isEmpty(elemento.vtype)){
				myTextColumn["vtype"] 	= elemento.vtype;
			}		
			if( elemento.toUpperCase ){
				myTextColumn["plugins"] = "uppercasetf";  
			}
			return myTextColumn;
			
		} else if( "datecolumn"   == elemento.type ){
			
			var myDateColumn  = {
				xtype:					"datefield",
				id:						"edicion." + elemento.id,
				gridColumnId:			elemento.id,
				fieldLabel:				elemento.name,
				readOnly:				elemento.readOnly,
				allowBlank: 			elemento.allowBlank,
				defaultValue:			elemento.defaultValue,
				primaryKey:				elemento.primaryKey?true:false,
				defaultEmptyText:		null,
				defaultAllowBlank: 	elemento.allowBlank
			}     
			return myDateColumn;
			
		} else if( "numbercolumn" == elemento.type ){
			
			var myNumberColumn = {
				xtype:					"numberfield",
				id:						"edicion." + elemento.id,
				gridColumnId:			elemento.id,
				fieldLabel:				elemento.name,
				readOnly:				elemento.readOnly,
				allowBlank: 			elemento.allowBlank,
				defaultValue:			elemento.defaultValue,
				primaryKey:				elemento.primaryKey?true:false,
				defaultEmptyText:		null,
				defaultAllowBlank: 	elemento.allowBlank
			}
			if(!Ext.isEmpty(elemento.minValue)){
				myNumberColumn["minValue"] = elemento.minValue;
			}
			if(!Ext.isEmpty(elemento.maxValue)){
				myNumberColumn["maxValue"] = elemento.maxValue;
			}
			return myNumberColumn;
			
		} else if("primarykeycolumn" == elemento.type ){
			
			var myPrimaryKey = {
				 xtype:					"hidden",				
				 id:						"edicion." + elemento.id,
				 gridColumnId:			elemento.id,
				 value:					"0",
				 defaultValue:			elemento.defaultValue,
				 primaryKey:			elemento.primaryKey?true:false,
				 defaultEmptyText:	null,
				 defaultAllowBlank: 	elemento.allowBlank
			};
			return myPrimaryKey;
			
		}
		
		return null;
		
	}
	
	var getFieldStoreFormaEdicionCatalogo = function(elemento){
		
		if( !Ext.isEmpty( elemento.storeData ) ){
			
			store = new Ext.data.ArrayStore({
				autoDestroy: 	true,
				storeId: 		'edicionStore' + elemento.id,
				fields: [ 
					'clave', 
					'descripcion' 
				],
				data: elemento.storeData
			});
		}
		return store;
		
	}
	
	var getComponenteGridPanelCatalogo = function(elemento){
	
		if("textcolumn" == elemento.type){
			
			var myTextColumn = {
				 id: 				elemento.id,
				 header:			elemento.header,
				 dataIndex:		elemento.id,
				 width:			elemento.width,
				 sortable:		true
			};
			return myTextColumn;
			
		} else if("foreigncolumn" == elemento.type){
 	
			var myForeignColumn = {
				 id: 				elemento.id,
				 header:			elemento.header,
				 dataIndex:		elemento.id,
				 width:			elemento.width,
				 sortable:		true,
				 renderer: 		comboRenderer 
			};
			return myForeignColumn;
			
		} else if("numbercolumn"  == elemento.type){
			
			var myNumberColumn = {
				 id: 				elemento.id,
				 header:			elemento.header,
				 dataIndex:		elemento.id,
				 width:			elemento.width,
				 sortable:		true,
				 format:			elemento.format
			};
			return myNumberColumn;
			
		} else if("datecolumn"    == elemento.type){
			
			var myDateColumn = {
				 id: 				elemento.id,
				 header:			elemento.header,
				 dataIndex:		elemento.id,
				 width:			elemento.width,
				 sortable:		true,
				 renderer:		Ext.util.Format.dateRenderer(elemento.format)
			};
			return myDateColumn;
			
		} else if("primarykeycolumn" == elemento.type ){
			
			var myPrimaryKey = {
				 id:				elemento.id,
				 name:			elemento.id,
				 header:			"Llave " + elemento.id,	
				 //tipo:			"llave",
				 dataIndex: 	elemento.id,
				 hidden:			true,
				 hideable:		false
			};
			return myPrimaryKey;
			
		}
		
		return null;
		
	}
	
	var getStoreFieldGridPanelCatalogo = function(elemento){

		if("textcolumn" == elemento.type){
			
			var myTextColumn = {
				 name: 			elemento.id,
				 type:			"string"
			};
			return myTextColumn;
			
		} else if("foreigncolumn" == elemento.type){
 	
			var myForeignColumn = {
				 name: 			elemento.id,
				 type:			"string"
			};
			return myForeignColumn;
			
		} else if("numbercolumn"  == elemento.type){
			
			var myNumberColumn = {
				 name: 			elemento.id,
				 type:			"float"
			};
			return myNumberColumn;
			
		} else if("datecolumn"    == elemento.type){
			
			var myDateColumn = {
				 name: 			elemento.id,
				 type:			"date",
				 dateFormat:	elemento.format
			};
			return myDateColumn;
			
		} else if("primarykeycolumn" == elemento.type ){
			
			var myPrimaryKey = {
				 name: 			elemento.id,
				 type:			"string"
			};
			return myPrimaryKey;
			
		}
		
		return null;
		
	}
	
	var getColumnStoreGridPanelCatalogo   = function(elemento){
		
		var store = null;
		if( !Ext.isEmpty( elemento.storeData) ){
			
			store = new Ext.data.ArrayStore({
				autoDestroy: 	true,
				storeId: 		'columnStore' + elemento.id,
				fields: [ 
					'clave', 
					'descripcion' 
				],
				data: elemento.storeData
			});
		}
		return store;
		
	}
	
	var procesarConsultaGridPanelCatalogo = function(store, arrRegistros, opts){
 
		var panelFormaCatalogo	= Ext.getCmp('panelFormaCatalogo');
		panelFormaCatalogo.el.unmask();
		
		var gridPanelCatalogo 	= Ext.getCmp('gridPanelCatalogo');
		
		if (arrRegistros != null) {
 
			var el 					= gridPanelCatalogo.getGridEl();
			
			if( store.getTotalCount() > 0 ){
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
 
			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			/*
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
			*/
			
		}
			
	}
 
	var doEditRegister 	= function(){
    	
		var grid	  				= Ext.getCmp("gridPanelCatalogo");
 
		var seleccionados  	= grid.getSelectionModel().getSelections();
		if(       seleccionados.length == 0){
			Ext.MessageBox.alert('Aviso','Debe seleccionar un registro.');
			return;
		}else if( seleccionados.length >  1){
			Ext.MessageBox.alert('Aviso','S�lo se puede editar un registro a la vez.');
			return;
		}
		
		var respuesta 			= new Object();
		respuesta['record']	= seleccionados[0];
		consultaCatalogo("EDITAR_REGISTRO", respuesta );
		
	}
	
	var doAddRegister = function(){

		var respuesta 	= new Object();
		consultaCatalogo("AGREGAR_REGISTRO", respuesta );
		
	}
	
	var doRemoveRegister = function(){
 
		var grid	  = Ext.getCmp("gridPanelCatalogo");
 
		var seleccionados   = grid.getSelectionModel().getSelections();
 
		var numeroRegistros = seleccionados.length;
		var aviso			  = "";
		if(numeroRegistros == 0){
			Ext.MessageBox.alert('Aviso','Debe seleccionar al menos un registro.');
			return;
		} else if(numeroRegistros > 1000){
			Ext.MessageBox.alert('Aviso','S�lo se permiten como m�ximo 1000 registros seleccionados.');
			return;
		} else if(numeroRegistros == 1  ){
			aviso = 'El registro seleccionado ser� eliminado, �Desea usted continuar?';
		} else if(numeroRegistros >  1  ){
			aviso = 'Los registros seleccionados ser�n eliminados, �Desea usted continuar?';
		}
 
		Ext.Msg.show({
			title: 		'Confirmaci�n',
			msg: 			aviso,
			width: 		300,
			buttons: 	Ext.MessageBox.YESNO,
			fn: 			function(buttonId,text,opt){
				
				var confirmacion = buttonId == "yes"?true:false;
				if(!confirmacion){
					return;
				}
					
				// Recolectar los registros que ser�n borrados
				var registros 	= new Array();
				var ids			= new Array();
				for(var i=0;i<numeroRegistros;i++){
					registros.push(seleccionados[i].data);	
					ids.push(seleccionados[i].id);
				}
 
				// Poner mascara con los registros que ser�n borrados
				if(numeroRegistros == 1){
					grid.el.mask("Eliminando Registro...","x-mask-loading");
				} else if( numeroRegistros >  1){
					grid.el.mask("Eliminando Registros...","x-mask-loading");
				}
				
				// Borrar registros
				Ext.Ajax.request({
					url: '15mancatalogo01ext.data.jsp',
					params: {
					  informacion: 	"RemoverRegistro",
					  pageId:			pageId,
					  claveCatalogo: 	grid.claveCatalogo,
					  dataarray:		true,
					  data:     		Ext.encode(registros),
					  ids:				Ext.encode(ids)
					},
					callback: procesarSuccessFailureRemoverRegistro
				});
 
			},
			icon: 		Ext.MessageBox.WARNING
		});
 
	}
 
	//------------------------------------- STORES ---------------------------------------
	// Hurray!! no stores here :)
	
	//------------------------------------ HANDLERS --------------------------------------
	
	function procesarSuccessFailureGuardarEdicion(opts, success, response){
		
		/*
		var grid	  = Ext.getCmp("gridPanelCatalogo");
		grid.el.unmask();
		*/
		var panelFormaEdicionCatalogo = Ext.getCmp("panelFormaEdicionCatalogo");
		panelFormaEdicionCatalogo.el.unmask();
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var respuesta 					= Ext.util.JSON.decode(response.responseText);
			if( !respuesta.guardarSuccess ){
				
				Ext.MessageBox.alert(
					'Error',
					respuesta.msg
				);
				
			} else { 
			
				var msg 		= Ext.isDefined(respuesta.msg) && !Ext.isEmpty(respuesta.msg)?respuesta.msg:"La operaci�n ha sido realizada con �xito.";
				respuesta 	= new Object();
				respuesta['updateCatalogo'] = true;

				Ext.Msg.alert(
						'Mensaje',
						msg,
						function(btn, text){
							consultaCatalogo("REGRESAR_A_CATALOGO",respuesta);
						}
					);
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
		
	}
 
	function procesarSuccessFailureRemoverRegistro(opts, success, response){
		
		var grid	  = Ext.getCmp("gridPanelCatalogo");
		grid.el.unmask();
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			var respuesta 					= Ext.util.JSON.decode(response.responseText);
			var gridPanelCatalogoData 	= Ext.StoreMgr.key('gridPanelCatalogoDataStore');
 
			/*
			var hayRegistrosBorrados = false;
			for(var i=0;i<respuesta.ids.length;i++){
				var r = gridPanelCatalogoData.getById(respuesta.ids[i]);
				gridPanelCatalogoData.remove(r);
				hayRegistrosBorrados  = true;	
			}
			if(hayRegistrosBorrados){
				gridPanelCatalogoData.commitChanges();
			}
			
			if( gridPanelCatalogoData.getCount() == 0 ){
				var gridPanelCatalogo = Ext.getCmp("gridPanelCatalogo");
				gridPanelCatalogo.el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			*/
			
			if( !respuesta.removerSuccess ){
				Ext.MessageBox.alert(
					'Error',
					respuesta.msg
				);
			} else if(respuesta.ids.length > 1){
				Ext.MessageBox.alert(
					'Aviso',
					'Los registros fueron eliminados.',
					function(){
						var gridPanelCatalogoData 	= Ext.StoreMgr.key('gridPanelCatalogoDataStore');
						// Actualizar catalogo
						gridPanelCatalogoData.load({ 
							params: 				Ext.apply(Ext.getCmp("panelFormaCatalogo").getForm().getValues(),{
								informacion: 		'Buscar', //Generar datos para la consulta
								claveCatalogo: 	Ext.getCmp("gridPanelCatalogo").claveCatalogo
						  })
						});
					}
				);
			} else {
				Ext.MessageBox.alert(
					'Aviso',
					'El registro fue eliminado.',
					function(){
						var gridPanelCatalogoData 	= Ext.StoreMgr.key('gridPanelCatalogoDataStore');
						// Actualizar catalogo
						gridPanelCatalogoData.load({ 
							params: 				Ext.apply(Ext.getCmp("panelFormaCatalogo").getForm().getValues(),{
								informacion: 		'Buscar', //Generar datos para la consulta
								claveCatalogo: 	Ext.getCmp("gridPanelCatalogo").claveCatalogo
						  })
						});
					}
				);
			}
 
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
		
	}
	
	var procesaConsultaCatalogo = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						consultaCatalogo(resp.estadoSiguiente,resp);
					}
				);
			} else {
				consultaCatalogo(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Resetear componentes segun se requiera
			var panelFormaCatalogos = Ext.getCmp("panelFormaCatalogos");
			panelFormaCatalogos.el.unmask();
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	//------------------------------- MAQUINA DE ESTADO ----------------------------------
	
	var consultaCatalogo = function(estadoSiguiente, respuesta){
		
		if( estadoSiguiente         == "CONSULTAR_CATALOGO" ){
			
			var panelFormaCatalogos							= Ext.getCmp("panelFormaCatalogos");
			var comboCatalogo									= panelFormaCatalogos.getComponent("comboCatalogo");
			
			Ext.Ajax.request({
				url: 		'15mancatalogo01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaCatalogo.consultarCatalogo',
					claveCatalogo: 	comboCatalogo.getValue(),
					pageId:				pageId
				},
				callback: 				procesaConsultaCatalogo
			});
			
		} else if(	estadoSiguiente == "CARGAR_CATALOGO"	){
		
			var claveCatalogo = respuesta.claveCatalogo;
 
			var panelFormaCatalogos							= Ext.getCmp("panelFormaCatalogos");
			panelFormaCatalogos.el.mask('Cargando componentes del cat�logo...','x-mask-loading');
			
			// 2. Cargar componentes del nuevo cat�logo
			Ext.Ajax.request({
				url: 		'15mancatalogo01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaCatalogo.cargarCatalogo',
					claveCatalogo: 	claveCatalogo,
					pageId:				pageId
				},
				callback: 				procesaConsultaCatalogo
			});
			
		} else if(  estadoSiguiente == "CARGAR_FORMA_CATALOGO"   ){
		
			var claveCatalogo = respuesta.claveCatalogo;
			
			// 3. Cargar componentes de la forma asociada al catalogo
			Ext.Ajax.request({
				url: 		'15mancatalogo01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaCatalogo.cargarFormaCatalogo',
					claveCatalogo: 	claveCatalogo,
					pageId:				pageId
				},
				callback: 				procesaConsultaCatalogo
			});
			
		} else if(	estadoSiguiente == "MOSTRAR_FORMA_CATALOGO"	){
			
			var claveCatalogo = respuesta.claveCatalogo;
			
			// 1. AGREGAR A LA FORMA ASOCIADA AL CATALOGO, LOS ELEMENTOS DE ESTA
 
			// 1.1. Borrar los elementos de una forma anterior que pudieran haber quedado
			var contenedorPrincipal = Ext.getCmp("contenedorPrincipal");
			for(var elemento=contenedorPrincipal.items.length-1;elemento>1;elemento--){
				contenedorPrincipal.remove(contenedorPrincipal.get(elemento), true);
			}
			contenedorPrincipal.doLayout();
			
			// 1.2 Crear arreglo con los elementos de la forma
			var elementosFormaCatalogo = [];
			var botonesFormaCatalogo	= [];
			// 1.2.1 Si no hay ning�n campo de busqueda, mostrar el siguiente mensaje de error
			if(respuesta.elementosFormaCatalogo.length == 0){
				
				elementosFormaCatalogo.push({
					xtype: 	'label',
					id:	 	'labelFormaCatalogoSinCampos',
					cls:		'x-form-item',
					style: 	'font-weight:bold;text-align:center;color:red;',
					html:  	'El cat�logo no tiene definido ning�n campo de b�squeda.'
				});
		
			// 1.2.2 Insertar elementos de la forma	
			} else {
				
				for(var i=0;i<respuesta.elementosFormaCatalogo.length;i++){
					
					// Obtener descripcion del elemento a agregar
					var elemento 			= respuesta.elementosFormaCatalogo[i];
					
					// Crear componente de extjs
					var componenteForma	= getComponenteFormaCatalogo(elemento);
					
					// Agregar componente a la lista de elementos
					if( componenteForma == null ){
						continue;
					} else {
						elementosFormaCatalogo.push(componenteForma);
					}
				
				}
				
				// Agregar los botones asociados a la forma
				botonesFormaCatalogo.push({
					name:			'botonBuscar',
					id:			'botonBuscar',
					text: 		'Buscar',
					iconCls: 	'icoBuscar',
					formBind: 	true,
					handler: 	doBuscar,
					hidden:		false
				});
				
				botonesFormaCatalogo.push({
					name:			'botonLimpiar',
					id:			'botonLimpiar',
					text: 		'Limpiar',
					iconCls: 	'icoLimpiar',
					handler: 	doLimpiarForma,
					hidden:		false
				});
 
			}
			
			// 1.3 Crear forma nueva
			var panelFormaCatalogo			= new Ext.form.FormPanel({
				id: 				'panelFormaCatalogo',
				title:			'Criterios de B�squeda del Cat�logo',
				width: 			843,
				frame: 			true,
				collapsible: 	true,
				titleCollapse: true,
				style: 			'margin: 0 auto',
				hidden:			true,
				bodyStyle:		(respuesta.elementosFormaCatalogo.length > 0?'padding:10px':null),
				defaults: {
					msgTarget: 	'side',
					anchor: 		'-20'
				},
				labelWidth: 	130,
				defaultType: 	'textfield',
				items: 			elementosFormaCatalogo,
				monitorValid: 	false,
				buttons:			botonesFormaCatalogo
			});
 
			// 1.4 Insertar forma en el contenedor principal
			contenedorPrincipal.insert(contenedorPrincipal.items.length,NE.util.getEspaciador(10));
			contenedorPrincipal.insert(contenedorPrincipal.items.length,panelFormaCatalogo);
			contenedorPrincipal.doLayout();
			
			// 1.5 Mostrar paneles agregados
			panelFormaCatalogo.show();
			
			// 2.0 Determinar el estado siguiente
			Ext.Ajax.request({
				url: 		'15mancatalogo01ext.data.jsp',
				params: 	{
					informacion:						'ConsultaCatalogo.mostrarFormaCatalogo',
					claveCatalogo: 					claveCatalogo,
					numeroElementosFormaCatalogo: respuesta.elementosFormaCatalogo.length,
					pageId:								pageId
				},
				callback: 				procesaConsultaCatalogo
			});
		
		} else if(  estadoSiguiente == "CARGAR_FORMA_EDICION_CATALOGOS"   ){
		
			var claveCatalogo = respuesta.claveCatalogo;
			
			// 1. Cargar componentes de la forma asociada al catalogo
			Ext.Ajax.request({
				url: 		'15mancatalogo01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaCatalogo.cargarFormaEdicionCatalogos',
					claveCatalogo: 	claveCatalogo,
					pageId:				pageId
				},
				callback: 				procesaConsultaCatalogo
			});
			
		} else if(  estadoSiguiente == "REGISTRAR_FORMA_EDICION_CATALOGOS"   ){
			
			var claveCatalogo = respuesta.claveCatalogo;
			
			// 1. AGREGAR A LA FORMA DE EDICION DEL CATALOGO, LOS ELEMENTOS DE ESTA
			
			// 1.1 Crear arreglo con los elementos de la forma
			var elementosFormaEdicionCatalogo	= [];
			var botonesFormaEdicionCatalogo		= [];
			// 1.2.1 Si no hay ning�n campo de busqueda, mostrar el siguiente mensaje de error
			if(respuesta.elementosFormaEdicionCatalogo.length == 0){
				
				elementosFormaEdicionCatalogo.push({
					xtype: 	'label',
					id:	 	'labelFormaEdicionCatalogoSinCampos',
					cls:		'x-form-item',
					style: 	'font-weight:bold;text-align:center;color:red;',
					html:  	'El cat�logo no tiene definido ning�n campo de edici�n.'
				});
		
			// 1.2.2 Insertar elementos de la forma	
			} else {
				
				for(var i=0;i<respuesta.elementosFormaEdicionCatalogo.length;i++){
					
					// Obtener descripcion del elemento a agregar
					var elemento 			= respuesta.elementosFormaEdicionCatalogo[i];
					
					// Crear componente de extjs
					var componenteForma	= getComponenteFormaEdicionCatalogo(elemento);
					
					// Agregar componente a la lista de elementos
					if( componenteForma == null ){
						continue;
					} else {
						
						// Si el componente trae asociado un store, registrar el store
						if( !Ext.isEmpty(elemento.storeData) ){
							var myFieldStore = getFieldStoreFormaEdicionCatalogo(elemento);
							componenteForma['store'] = myFieldStore;
						}
						// Agregar componente a la lista :)
						elementosFormaEdicionCatalogo.push(componenteForma);
						
					}
				
				}
				
				// Agregar los botones asociados a la forma
				botonesFormaEdicionCatalogo.push({
					name:			'botonGuardar',
					id:			'botonGuardar',
					text: 		'Guardar',
					iconCls: 	'icoGuardar',
					formBind: 	true,
					handler: 	doGuardarEdicion
				});
				
				botonesFormaEdicionCatalogo.push({
					name:			'botonCancelar',
					id:			'botonCancelar',
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					handler: 	doCancelarEdicion
				})
				
			}
				
			// 1.3 Crear forma nueva
			var panelFormaEdicionCatalogo	= new Ext.form.FormPanel({
				id: 				'panelFormaEdicionCatalogo',
				title:			'Editar Cat�logo',
				width: 			843,
				frame: 			true,
				//collapsible: 	true,
				//titleCollapse: true,
				style: 			'margin: 0 auto',
				hidden:			true,
				bodyStyle:		(respuesta.elementosFormaEdicionCatalogo.length > 0?'padding:10px':null),
				defaults: {
					msgTarget: 	'side',
					anchor: 		'-20'
				},
				labelWidth: 	130,
				defaultType: 	'textfield',
				items: 			elementosFormaEdicionCatalogo,
				monitorValid: 	false,
				buttons:			botonesFormaEdicionCatalogo,
				editingMode:	null
			});
 
			// 1.3 Insertar forma en el contenedor principal
			var contenedorPrincipal = Ext.getCmp("contenedorPrincipal");
			// No es necesario agregar un elemento de espacio ya que cuando se muestre esta forma 
			// se ocultaran el grid panel y la forma de busqueda
			//contenedorPrincipal.insert(contenedorPrincipal.items.length,NE.util.getEspaciador(10));
			contenedorPrincipal.insert(contenedorPrincipal.items.length,panelFormaEdicionCatalogo);
			contenedorPrincipal.doLayout();
			
			// 1.4 Mostrar paneles agregados
			// Por default, esta forma debe permanecer oculta
			// panelFormaEdicionCatalogo.show();
			
			// 2. Cargar componentes de la forma asociada al catalogo
			Ext.Ajax.request({
				url: 		'15mancatalogo01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaCatalogo.registrarFormaEdicionCatalogos',
					claveCatalogo: 	claveCatalogo,
					pageId:				pageId
				},
				callback: 				procesaConsultaCatalogo
			});
			
		} else if(  estadoSiguiente == "CARGAR_GRID_PANEL_CATALOGOS"    ){
			
			var claveCatalogo = respuesta.claveCatalogo;
			
			// 1. Mostrar los componentes del grid panel asociado al catalogo
			Ext.Ajax.request({
				url: 		'15mancatalogo01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaCatalogo.cargarGridPanelCatalogos',
					claveCatalogo: 	claveCatalogo,
					pageId:				pageId
				},
				callback: 				procesaConsultaCatalogo
			});
			
		} else if(  estadoSiguiente ==  "MOSTRAR_GRID_PANEL_CATALOGOS" ){
			 
			var claveCatalogo = respuesta.claveCatalogo;
			
			// 1. Crear arreglo con los elementos del Panel que contendra el Grid Panel asociado al catalogo
			var elementosPanelGridPanelCatalogo 		= [];
			// 2. Crear arreglo con los elementos del Grid Panel asociado al catalogo
			var elementosGridPanelCatalogo 				= [];
			//    Crear arreglo con los stores asociados a las columnas
			var columnStoreGridPanelCatalogo				= [];
			//    Crear arreglo con la definicion de los campos asociado al  JSONStore del Grid Panel
			var elementosJSONStoreGridPanelCatalogo 	= [];
			// 3. Si no hay ning�n campo definido para el grid, mostrar el siguiente mensaje de error
			if(respuesta.elementosGridPanelCatalogo.length == 0){
				
				elementosPanelGridPanelCatalogo.push({
					xtype: 	'label',
					id:	 	'labelGridCatalogoSinCampos',
					cls:		'x-form-item',
					style: 	'font-weight:bold;text-align:center;color:red;',
					html:  	'El cat�logo no tiene definido ning�n campo de que pueda ser mostrado en este panel.'
				});
 
			// 4. Insertar elementos de la forma	
			} else {

				// 4.1 Construir arreglo de los campos asociados al JSONStore
				for(var indice=0; indice < respuesta.elementosGridPanelCatalogo.length; indice++ ){
					
					// Obtener descripcion del elemento a agregar
					var elemento 				= respuesta.elementosGridPanelCatalogo[indice];
						
					// Crear componente de extjs
					var componenteJSONStore	= getStoreFieldGridPanelCatalogo(elemento);
					
					// Agregar componente a la lista de elementos
					if( componenteJSONStore == null ){
						continue;
					} else {
						elementosJSONStoreGridPanelCatalogo.push(componenteJSONStore);
					}
					
				}
 
				// 4.2 Agregar columna de conteo de linea
				elementosGridPanelCatalogo.push(new Ext.grid.RowNumberer());
				columnStoreGridPanelCatalogo.push(null);
				
				// 4.3 Construir lista con los componentes del grid panel
				for(var indice=0; indice < respuesta.elementosGridPanelCatalogo.length; indice++ ){
					
					// Obtener descripcion del elemento a agregar
					var elemento 				= respuesta.elementosGridPanelCatalogo[indice];
						
					// Crear componente de extjs
					var componenteGridPanel	= getComponenteGridPanelCatalogo(elemento);
						
					// Agregar componente a la lista de elementos
					if( componenteGridPanel == null ){
						continue;
					} else {
						elementosGridPanelCatalogo.push(componenteGridPanel);
						// Si el componente trae asociado un store, registrar el store
						var myColumnStore = getColumnStoreGridPanelCatalogo(elemento);
						columnStoreGridPanelCatalogo.push(myColumnStore);
					}
				}
				
				// 4.4 Crear JSONStore
				var gridPanelCatalogoData = new Ext.data.JsonStore({
					id:					'gridPanelCatalogoDataStore',
					name:					'gridPanelCatalogoDataStore',
					autoDestroy:		true,
					root: 				'registros',
					totalProperty: 	'total',
					messageProperty: 	'msg',
					url: 					'15mancatalogo01ext.data.jsp',
					baseParams: {
						pageId:  pageId
					},
					fields: 		elementosJSONStoreGridPanelCatalogo,
					autoLoad: 	false,
					listeners: { 
						load: 	procesarConsultaGridPanelCatalogo,			
						exception: {
							fn: function(proxy, type, action, optionsRequest, response, args) {
								NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
								//Llama procesar consulta, para que desbloquee los componentes.
								procesarConsultaGridPanelCatalogo(null, null, null);						
							}
						}
					}
					
				});
 
				// 4.5 Agregar Grid Panel
				elementosPanelGridPanelCatalogo.push( 
					new Ext.grid.GridPanel({
						store: 					gridPanelCatalogoData,
						id:						'gridPanelCatalogo',
						autoExpandColumn: 	(!Ext.isEmpty(respuesta.gridPanelAutoExpandColumnId)?respuesta.gridPanelAutoExpandColumnId:null),
						columns: 				elementosGridPanelCatalogo,
						columnStore:			columnStoreGridPanelCatalogo,
						stripeRows: 			true,
						loadMask: 				true,
						height: 					400,
						claveCatalogo:			claveCatalogo,
						style: {
								  borderWidth: 1
						},
						tbar:{ 
							xtype: 'toolbar',
							style: {
								borderWidth: 0
							},
							items:	[
								{
									iconCls: 		'icon-register-edit',
									text: 			'Editar Registro',
									disabled: 		!respuesta.hayPermisoModificarRegistro,
									handler: 		doEditRegister
								},
								{
									iconCls: 		'icon-register-add',
									text: 			'Agregar Registro',
									disabled: 		!respuesta.hayPermisoAgregarRegistro,
									handler: 		doAddRegister
								},
								{
									ref: 				'../removeBtn',
									iconCls: 		'icon-register-delete',
									text: 			'Eliminar Registro',
									hidden: 		!respuesta.hayPermisoBorrarRegistro,//disabled
									handler: 		doRemoveRegister
								}
							]
						},
						permisoModificarRegistro:	respuesta.hayPermisoModificarRegistro,
						permisoAgregarRegistro:		respuesta.hayPermisoAgregarRegistro,
						permisoBorrarRegistro:		respuesta.hayPermisoBorrarRegistro,
						stateful: false
					})
				);
			}
 
			// 5. Crear panel que alojar� el grid
			var panelGridPanelCatalogo			= new Ext.Panel({
				title:			'Contenido del Cat�logo',
				hidden:			true,
				id: 				'panelGridPanelCatalogo',
				width: 			843,
				frame: 			true,
				style: 			'margin: 0 auto',
				items: 			elementosPanelGridPanelCatalogo
			});
 
			// 6. Insertar forma en el contenedor principal
			var contenedorPrincipal = Ext.getCmp("contenedorPrincipal");
			contenedorPrincipal.insert(contenedorPrincipal.items.length,NE.util.getEspaciador(10));
			contenedorPrincipal.insert(contenedorPrincipal.items.length,panelGridPanelCatalogo);
			contenedorPrincipal.doLayout();
 
			// 7. Mostrar paneles agregados
			panelGridPanelCatalogo.show();
 
			// 8. Cargar catalogo con valores default
			if(respuesta.elementosGridPanelCatalogo.length > 0){
				var gridPanelCatalogoData = Ext.StoreMgr.key('gridPanelCatalogoDataStore');
				gridPanelCatalogoData.load({
					params: 				Ext.apply(Ext.getCmp("panelFormaCatalogo").getForm().getValues(),{
						informacion: 		'Buscar', //Generar datos para la consulta
						claveCatalogo: 	Ext.getCmp("gridPanelCatalogo").claveCatalogo
					})
				});
			}
			
			// 9. Cargar componentes del grid panel asociado al catalogo
			Ext.Ajax.request({
				url: 		'15mancatalogo01ext.data.jsp',
				params: 	{
					informacion:		'ConsultaCatalogo.mostrarGridPanelCatalogos',
					claveCatalogo: 	claveCatalogo,
					pageId:				pageId
				},
				callback: 				procesaConsultaCatalogo
			});
 
		} else if(	estadoSiguiente == "ESPERAR_DECISION"	){
			
			// Resetear componentes segun se requiera
			var panelFormaCatalogos = Ext.getCmp("panelFormaCatalogos");
			panelFormaCatalogos.el.unmask();
			
			// Esperar la decision del usuario
			
		} else if(	estadoSiguiente == "FIN"					){

			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "15mancatalogo01ext.data.jsp";
			forma.target	= "_self";
			
			Ext.DomHelper.insertFirst(forma, { 
					tag: 	'input', 
					type: 'hidden', 
					id: 	'informacion', 
					name: 'informacion', 
					value: "ConsultaCatalogo.fin"
			}); 
			
			Ext.DomHelper.insertFirst(forma, { 
					tag: 	'input', 
					type: 'hidden', 
					id: 	'pageId', 
					name: 'pageId', 
					value: pageId
			}); 
			
			if(!Ext.isEmpty(respuesta.redirigir)){
				Ext.DomHelper.insertFirst(forma, { 
					tag: 	'input', 
					type: 'hidden', 
					id: 	'redirigir', 
					name: 'redirigir', 
					value: String(respuesta.redirigir)
				}); 
			}
			
			if(!Ext.isEmpty(respuesta.claveCatalogo)){
				Ext.DomHelper.insertFirst(forma, { 
					tag: 	'input', 
					type: 'hidden', 
					id: 	'claveCatalogo', 
					name: 'claveCatalogo', 
					value: respuesta.claveCatalogo 
				}); 
			}
			
			forma.submit();
			
		} else if(	estadoSiguiente == "EDITAR_REGISTRO"  ){
			
			// Ocultar FormaCatalogo y GridPanelCatalogo
			var panelFormaCatalogo 						= Ext.getCmp("panelFormaCatalogo");
			var panelGridPanelCatalogo 				= Ext.getCmp("panelGridPanelCatalogo");
			panelFormaCatalogo.hide();
			panelGridPanelCatalogo.hide();
			
			// Cargar registros
			var record 											= respuesta.record; //Ext.StoreMgr.key('gridPanelCatalogoDataStore').getAt(respuesta.rowIndex);
			var panelFormaEdicionCatalogo 				= Ext.getCmp("panelFormaEdicionCatalogo");
			panelFormaEdicionCatalogo.editingMode 		= "EDITAR_REGISTRO";
			panelFormaEdicionCatalogo.setTitle("Editar Cat�logo: Modificar Registro");
			panelFormaEdicionCatalogo.recordId			= record.id;
			
			// Copiar contenido del registro a la forma de edicion
			panelFormaEdicionCatalogo.items.each(function(item){
				item.emptyText 	= item.defaultEmptyText;
				item.allowBlank 	= item.defaultAllowBlank;
				item.setValue(record.json[item.gridColumnId]);
				item.show();
    		});
 
			// Mostrar Forma Edicion Catalogo
			panelFormaEdicionCatalogo.show();
			
			// Limpiar los campos que hayan quedado invalidos por la operacion
			// de inicializado
			panelFormaEdicionCatalogo.items.each(function(item){
				item.clearInvalid();
    		});
			
			
		} else if(  estadoSiguiente == "AGREGAR_REGISTRO" ){
 
			// Ocultar FormaCatalogo y GridPanelCatalogo
			var panelFormaCatalogo 						= Ext.getCmp("panelFormaCatalogo");
			var panelGridPanelCatalogo 				= Ext.getCmp("panelGridPanelCatalogo");
			panelFormaCatalogo.hide();
			panelGridPanelCatalogo.hide();
				
			// Cargar registros
			var panelFormaEdicionCatalogo 			= Ext.getCmp("panelFormaEdicionCatalogo");
			panelFormaEdicionCatalogo.recordId		= null;
			panelFormaEdicionCatalogo.editingMode 	= "AGREGAR_REGISTRO";
			panelFormaEdicionCatalogo.setTitle("Editar Cat�logo: Agregar Registro");
			
			// Copiar contenido del registro a la forma de edicion
			panelFormaEdicionCatalogo.items.each(function(item){
				if( item.primaryKey && !item.readOnly && "0" == item.defaultValue ){
					item.emptyText 	= "Especifique una Clave";
					item.allowBlank 	= item.defaultAllowBlank;
					item.setValue(null);
					item.show();
				} else if( item.primaryKey && item.readOnly ){
					item.emptyText 	= "Generado por el Sistema";
					item.allowBlank	= true;
					item.setValue(null);
					item.hide();
				} else {
					item.emptyText 	= item.defaultEmptyText;
					item.allowBlank 	= item.defaultAllowBlank;
					item.setValue(item.defaultValue);
				}
    		});
 
			// Mostrar Forma Edicion Catalogo
			panelFormaEdicionCatalogo.show();
			
			// Limpiar los campos que hayan quedado invalidos por la operacion
			// de inicializado
			panelFormaEdicionCatalogo.items.each(function(item){
				item.clearInvalid();
    		});
			
			
		} else if(  estadoSiguiente == "REGRESAR_A_CATALOGO" ){
			
			// Ocultar Forma Edicion Catalogo
			var panelFormaEdicionCatalogo = Ext.getCmp("panelFormaEdicionCatalogo");
			panelFormaEdicionCatalogo.getForm().reset();
			panelFormaEdicionCatalogo.hide();
			
			// Mostrar Forma Catalogo y Grid Panel Catalogo
			var panelFormaCatalogo 		= Ext.getCmp("panelFormaCatalogo");
			var panelGridPanelCatalogo = Ext.getCmp("panelGridPanelCatalogo");
			panelFormaCatalogo.show();
			panelGridPanelCatalogo.show();
			
			// Actualizar catalogo
			if( respuesta.updateCatalogo ){
				var gridPanelCatalogoData = Ext.StoreMgr.key('gridPanelCatalogoDataStore');
				gridPanelCatalogoData.load({ 
						params: 				Ext.apply(Ext.getCmp("panelFormaCatalogo").getForm().getValues(),{
						informacion: 		'Buscar', //Generar datos para la consulta
						claveCatalogo: 	Ext.getCmp("gridPanelCatalogo").claveCatalogo
					})
				});
			}
			
		}
		
		return;
		
	}
	
	//-------------------------------- FORMA CATALOGOS -----------------------------------

	var catalogoCatalogosParametrizacionData = new Ext.data.JsonStore({
		id: 			'catalogoCatalogosParametrizacionDataStore',
		root: 		'registros',
		autoDestroy: true,
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15mancatalogo01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogosParametrizacion',
			pageId:			pageId
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("comboCatalogo").setValue(defaultValue);
						consultaCatalogo("CONSULTAR_CATALOGO",null);
					}
					// Ext.getCmp("comboCatalogo").setReadOnly(true);
									
				} 
								
			}
		}
	});
 
	var elementosPanelFormaCatalogos = [
		{
			xtype: 				'combo',
			name: 				'comboCatalogo',
			id: 					'comboCatalogo',
			fieldLabel: 		'Cat�logo',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveCatalogo',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoCatalogosParametrizacionData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			listeners: {
				collapse: function(comboCatalogosParametrizacion){
					consultaCatalogo("CONSULTAR_CATALOGO",null);
				}
			}
		}
	];
 
	var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		(elementosPanelFormaCatalogos.length > 0?'padding:10px':null),
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		defaultType: 	'textfield',
		items: 			elementosPanelFormaCatalogos,
		monitorValid: 	false
	});
	
	//----------------------------------- CONTENEDOR -------------------------------------	
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			panelFormaCatalogos
		]
	});
	
	//-------------------------------- INICIALIZACION -----------------------------------	
	catalogoCatalogosParametrizacionData.load({
			params: { defaultValue: !Ext.isEmpty(defaultClaveCatalogo)?defaultClaveCatalogo:undefined }
   });
	
});
