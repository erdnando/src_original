<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
    <%if(esEsquemaExtJS) {%>
			<%@ include file="/01principal/menu.jspf"%>		
		<%}%>
<%

	// Determinar el tipo de usuario
	String	tipoUsuario    = (String) session.getAttribute("strTipoUsuario");
 
	// Determinar el sistema de menus que se est� utilizando para acceder a la pantalla.
	String pageId 	 		= null;
	String sistemaMenus	= null;
	
	pageId  = request.getParameter("idMenu");
	pageId  = pageId  == null?"":pageId;
 
	if( !"".equals(pageId)){
		sistemaMenus = "idMenu.";
	} else {
		pageId = request.getParameter("parametros");
		pageId = pageId == null?"":pageId;
		sistemaMenus = "parametros.";
	}
	
	if( pageId.indexOf("idMenu.") == 0 ) {
		sistemaMenus = "";
	} else if( pageId.indexOf("parametros.") == 0 ){
		sistemaMenus = "";
	}
 
%>
<script type="text/javascript" src="15mancatalogoExt01.js?<%=session.getId()%>"></script>
<script language="JavaScript1.2" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script language="JavaScript">
	var pageId 						= '<%= sistemaMenus + pageId %>';
	var defaultClaveCatalogo	= '<%= request.getParameter("claveCatalogo") %>';
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<% if(        "EPO".equals(tipoUsuario)    && esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<% } else if( "NAFIN".equals(tipoUsuario)  && esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<% } %>
	<%if(esEsquemaExtJS) {%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%} else {%> 
		 <div id="areaContenido"></div>
		 <%}%> 
	<% if(        "EPO".equals(tipoUsuario)   && esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<% } else if( "NAFIN".equals(tipoUsuario) && esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
	<% } %>
	<%if(esEsquemaExtJS) {%>
		<div id="areaContenido"><div style="height:230px"></div></div>						
		<%} %> 
	</div>
	</div>
	<% if(        "EPO".equals(tipoUsuario)   && esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01epo/pie.jspf"%>
	<% } else if( "NAFIN".equals(tipoUsuario) && esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
	<% } %>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>