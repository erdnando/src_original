<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.cadenas.catalogos.*,
		com.netro.fondojr.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>  
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String infoRegresar="", consulta="", mensaje ="";
	String claveIF = request.getParameter("claveIF") == null?"":(String)request.getParameter("claveIF");
	String descripcion = request.getParameter("descripcion") == null?"":(String)request.getParameter("descripcion");
	String IC_PROGRAMA_FONDOJR[] = request.getParameterValues("IC_PROGRAMA_FONDOJR");
	//FondoJuniorHome fondoJuniorHome = (FondoJuniorHome)ServiceLocator.getInstance().getEJBHome("FondoJuniorEJB", FondoJuniorHome.class);
	//FondoJunior fondoJunior = fondoJuniorHome.create();
	
	FondoJunior fondoJunior  = ServiceLocator.getInstance().lookup("FondoJuniorEJB",FondoJunior.class);
	
	ConsManCatProgramaFondeoJR paginado = new ConsManCatProgramaFondeoJR();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginado);
	
	JSONObject jsonObj = new JSONObject();
	if (informacion.equals("ConsultaGrid")){
		infoRegresar ="";
		try {  
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString();
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	}else if(informacion.equals("catIntermediario")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setTabla("comcat_if");
		catalogo.setCampoClave("ic_if");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setOrden("cg_razon_social");
		infoRegresar = catalogo.getJSONElementos();
	}else if(informacion.equals("Agregar")){
					  
		boolean resultado = fondoJunior.agregarPrograma(claveIF , descripcion);
		if(resultado!=true){
			mensaje = "Error al agregar el registro ";
		}else{
			mensaje = "El registro fue Agregado ";
		}
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("mensaje",mensaje );
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("Eliminar")){
		String listId="";
		for(int i = 0; i<IC_PROGRAMA_FONDOJR.length; i++){
			listId += IC_PROGRAMA_FONDOJR[i]+",";
		}
		mensaje =  fondoJunior.eliminarPrograma(listId.substring(0,(listId.length())-1));
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("mensaje",mensaje );
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("Clasificacion.Carga.Catalogo")){
		String	tipoUsuario    = (String) session.getAttribute("strTipoUsuario");
		JSONObject registros = new JSONObject();
		JSONArray resultado = new JSONArray();
		
		if(strPerfil.equals("ADMIN NAFIN PAG")){
			registros = new JSONObject();registros.put("clave",new Integer(47));registros.put("descripcion","Firmas Cedulas");resultado.add(registros);
		} else {
		
		if(tipoUsuario.equals("NAFIN")){
			registros = new JSONObject();registros.put("clave",new Integer(45));registros.put("descripcion","Agencias SIRAC");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(6));registros.put("descripcion","Amortizaci\u00F3n");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(60));registros.put("descripcion","Area de Promoción");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(25));registros.put("descripcion","Aval");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(101));registros.put("descripcion","Bancos Fondeo");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(40));registros.put("descripcion","Bancos TEF");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(5));registros.put("descripcion","Bloqueo");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(12));registros.put("descripcion","Cambio Estatus");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(102));registros.put("descripcion","Ciudades");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(19));registros.put("descripcion","Clase");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(7));registros.put("descripcion","Clase Documento");resultado.add(registros);
		}
			registros = new JSONObject();registros.put("clave",new Integer(13));registros.put("descripcion","Clasificación");resultado.add(registros);//falta idioma
			
		if(strPerfil.equals("ADMIN EPO") || strPerfil.equals("ADMIN NAFIN")){
			registros = new JSONObject();registros.put("clave",new Integer(677));registros.put("descripcion","Clasificación EPO");resultado.add(registros);//falta idioma
		}
		if (tipoUsuario.equals("NAFIN")) {
			registros = new JSONObject();registros.put("clave",new Integer(45));registros.put("descripcion","Descripciones Especiales API's");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(33));registros.put("descripcion","Días Inhábiles");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(37));registros.put("descripcion","Días Inhábiles por EPO");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(70));registros.put("descripcion","Días Inhábiles por Año");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(71));registros.put("descripcion","Días Inhábiles EPO por Año");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(29));registros.put("descripcion","Domicilio Correspondencia");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(38));registros.put("descripcion","Emisores");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(43));registros.put("descripcion","Equivalencias Plazas FFON");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(18));registros.put("descripcion","Escolaridad");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(8));registros.put("descripcion","Esquema Amortizaci&oacute;n");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(24));registros.put("descripcion","Estado");resultado.add(registros);
			
			registros = new JSONObject();registros.put("clave",new Integer(15));registros.put("descripcion","Estado Civil");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(11));registros.put("descripcion","Estatus Documento");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(41));registros.put("descripcion","Estatus TEF");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(42));registros.put("descripcion","Estatus CECOBAN");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(47));registros.put("descripcion","Firmas Cedulas");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(17));registros.put("descripcion","Identificaci&oacute;n");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(100));registros.put("descripcion","Intermediario SUCRE");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(50));registros.put("descripcion","IVA");resultado.add(registros);
				registros = new JSONObject();registros.put("clave",new Integer(63));registros.put("descripcion","Lider Promotor");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(1));registros.put("descripcion","Moneda");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(35));registros.put("descripcion","Motivos");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(26));registros.put("descripcion","Oficina");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(3));registros.put("descripcion","Oficina Estatal");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(52));registros.put("descripcion","Organismos");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(14));registros.put("descripcion","País");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(49));registros.put("descripcion","Plazos por Producto");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(36));registros.put("descripcion","Personas facultadas por Banco");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(34));registros.put("descripcion","Productos");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(270));registros.put("descripcion","Productos CAT");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(103));registros.put("descripcion","Programa a Fondo JR");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(104));registros.put("descripcion","Programa por Base de Operación");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(20));registros.put("descripcion","Rama");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(61));registros.put("descripcion","Región");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(53));registros.put("descripcion","Secretar&iacute;as");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(10));registros.put("descripcion","Sector Económico");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(66));registros.put("descripcion","Sector EPO");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(67));registros.put("descripcion","SIAFF Ramos");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(208));registros.put("descripcion","SIAFF Unidades");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(62));registros.put("descripcion","Subdirección");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(21));registros.put("descripcion","Subsector");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(65));registros.put("descripcion","Subtipo EPO");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(2));registros.put("descripcion","Tasa");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(23));registros.put("descripcion","Tipo Categor&iacute;a");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(22));registros.put("descripcion","Tipo Afiliado");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(9));registros.put("descripcion","Tipo Crédito");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(16));registros.put("descripcion","Tipo Empresa");resultado.add(registros);   
			registros = new JSONObject();registros.put("clave",new Integer(64));registros.put("descripcion","Tipo Epo");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(30));registros.put("descripcion","Tipo Mensaje");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(48));registros.put("descripcion","Tipo Pago 1er. Piso (SIRAC)");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(31));registros.put("descripcion","Tipo Publicación");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(32));registros.put("descripcion","Tipo Respuesta");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(46));registros.put("descripcion","Tipo Riesgo");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(51));registros.put("descripcion","Tipo de Sector");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(68));registros.put("descripcion","Ventanillas");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(39));registros.put("descripcion","Version Convenio");resultado.add(registros);
			registros = new JSONObject();registros.put("clave",new Integer(27));registros.put("descripcion","Viabilidad");resultado.add(registros);
		}
	}	
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("registros",resultado);
			infoRegresar = jsonObj.toString();
			System.out.println("datos "+infoRegresar);
	}
	
	%>	

<%= infoRegresar %>