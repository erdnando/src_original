<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*,
		com.netro.seguridad.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*,
		com.netro.catalogos.*,
		com.netro.cadenas.PaginadorGenerico,
		com.netro.cadenas.MantenimientoCatalogo"		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String infoRegresar="", consulta="",consultaGrid="", mensaje ="";
		
if (informacion.equals("ConsultaGrid")){
	String		 epo  	= (request.getParameter("hEpo") != null)?request.getParameter("hEpo"):"";
	PaginadorGenerico pG = new PaginadorGenerico();
	pG.setCampos("vent.ic_ventanilla, epo.cg_razon_social, cg_descripcion, vent.cg_usuario");
	pG.setTabla("comcat_ventanilla_epo vent, comcat_epo epo");
	pG.setCondicion("vent.ic_epo = epo.ic_epo AND vent.ic_epo = "+epo);
	
	JSONObject jsonObj   = new JSONObject();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pG);
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("comboEpo")){
	
	JSONObject jsonObj   = new JSONObject();
	CatalogoEPO catalogo = new CatalogoEPO();	
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setCs_cesion_derechos("S");
	List lista = catalogo.getListaElementos();
	jsonObj.put("registros", lista);
	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("Agregar") || informacion.equals("Modificar") || informacion.equals("Eliminar")){
	String	epo  	= (request.getParameter("hEpo")   	!= null)? request.getParameter("hEpo"):"";
	String ventana = (request.getParameter("ventana")	!=null) ? request.getParameter("ventana"):"";
	String login   = (request.getParameter("login")		!=null) ? request.getParameter("login"):"";
	String clvVen  = (request.getParameter("clvVen")	!=null) ? request.getParameter("clvVen"):"";
		
	MantenimientoCatalogo 		mC = new MantenimientoCatalogo ();
	ActualizacionCatalogosBean ac = new ActualizacionCatalogosBean();
	
	boolean	agreg	= true;
	boolean	elim	= false;
	boolean	bandera	= true;
	String[] credEle	= null;
	String	clv="";
	int 		elementos=0;
	
	try{
		UtilUsr utilUsr = new UtilUsr();
		boolean usuarioEncontrado = false;
		boolean perfilIndicado = false;
		List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(epo, "E");
		for (int i = 0; i < usuariosPorPerfil.size(); i++) {
			String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
			if (login.equals(loginUsuarioEpo)) {
				usuarioEncontrado = true;
				Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
				if ("EPO VENTANILLA".equals(usuarioEpo.getPerfil())) {
					perfilIndicado = true;
				}
			}
		}
		if (!usuarioEncontrado) {
				agreg	= false;
				bandera = false;
				mensaje = "El usuario introducido no pertenece a la EPO seleccionada.";
		} else {
			if (!perfilIndicado) {
				agreg	= false;
				bandera = false;
				mensaje = "El usuario introducido no tiene el perfil requerido.";
			}
		}
		if (bandera){
			if(informacion.equals("Agregar")){
				mC.setTabla("COMCAT_VENTANILLA_EPO");
				mC.setCampos("IC_VENTANILLA, IC_EPO, CG_USUARIO, CG_DESCRIPCION");
				mC.setValues("(SELECT NVL(MAX(ic_ventanilla), 0) + 1 clave_ventanilla FROM comcat_ventanilla_epo), ?, '"+login+"','"+ventana+"'");
				mC.setBind(epo);
				ac.insertarEnCatalogo(mC);
				mensaje = "El registro fue agregado.";
			} else if(informacion.equals("Modificar")){
				mC.setTabla(" COMCAT_VENTANILLA_EPO ");
				mC.setClaveVal("CG_DESCRIPCION='"+ventana+"', CG_USUARIO = '"+login+"' ");
				mC.setCondicion(" IC_EPO = "+epo+" AND IC_VENTANILLA = ?");
				mC.setBind(clvVen);		
				ac.actualizarCatalogo(mC);
				mensaje = "El registro fue actualizado.";
			}
		} 
		if(informacion.equals("Eliminar")){
			mC.setTabla(" COMCAT_VENTANILLA_EPO ");
			mC.setCondicion(" IC_EPO = "+epo+" AND IC_VENTANILLA = ?");
			mC.setBind(clvVen);
			int regAfectados=ac.eliminarDelCatalogo(mC);
			if(regAfectados==1){
				mensaje = "El registro fue eliminado.";
				elim	= true;
			} else {
				mensaje = "No se puede eliminar el registro porque esta relacionado.";
			}
		}
	} catch (Exception e) {
		agreg	= false;
		mensaje = "Ocurrio un error al intentar "+informacion+" el registros.";
		System.err.println(informacion +" Error: " + e);
	}

	JSONObject jsonObj1 = new JSONObject();
	jsonObj1.put("success", new Boolean(true));
	jsonObj1.put("agregar", new Boolean(agreg));
	jsonObj1.put("eliminar", new Boolean(elim));
	jsonObj1.put("msg", mensaje);
	infoRegresar = jsonObj1.toString();	
}
%>	

<%= infoRegresar %>