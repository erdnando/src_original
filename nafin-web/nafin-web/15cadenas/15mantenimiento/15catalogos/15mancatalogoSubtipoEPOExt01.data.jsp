<%@ page contentType="application/json;charset=UTF-8"

	import="
		java.util.List,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  		
		com.netro.cadenas.SubtipoEPO,
		org.apache.commons.logging.Log,		
		netropology.utilerias.Registros,
		netropology.utilerias.AppException,
		netropology.utilerias.ServiceLocator,
		com.netro.cadenas.MantenimientoCatalogo,
		com.netro.model.catalogos.CatalogoSimple,
		netropology.utilerias.CQueryHelperRegExtJS,
		com.netro.catalogos.*"		
		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>
<% 
	String informacion 	= (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	String consulta		= "";
	String mensaje			= "";
	
if (informacion.equals("ConsultaGrid")){
	
	JSONObject jsonObj = new JSONObject();
	SubtipoEPO paginador = new SubtipoEPO();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	infoRegresar = jsonObj.toString();
	
} else  if (informacion.equals("comboTipo")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_tipo_epo");
	cat.setDistinc(false);
	cat.setCampoClave("ic_tipo_epo");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setOrden("cg_descripcion");
	List lista = cat.getListaElementos();
	JSONObject	jsonObj  = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("total",	  new Integer(lista.size()) );	
	jsonObj.put("registros",lista);	
   infoRegresar = jsonObj.toString();

} else if(informacion.equals("Agregar") || informacion.equals("Modificar") || informacion.equals("Eliminar")){

	String 	tipo		= (request.getParameter("tipo")   	!=null) ? request.getParameter("tipo"):"";
	String 	subtipo 	= (request.getParameter("subtipo")	!=null) ? request.getParameter("subtipo"):"";
	String 	clave 	= (request.getParameter("clave")		!=null) ? request.getParameter("clave"):"";
	boolean	agreg		= true;
	boolean	elim		= false;
	boolean	bandera	= true;
	ActualizacionCatalogosBean ac = new ActualizacionCatalogosBean();
	MantenimientoCatalogo mC = new MantenimientoCatalogo ();
	try {
		if(informacion.equals("Agregar")){
			mC.setTabla("comcat_subtipo_epo");
			mC.setCampos("ic_subtipo_epo,ic_tipo_epo,cg_descripcion");
			mC.setValues("(SELECT NVL (MAX (ic_subtipo_epo), 0) + 1 FROM comcat_subtipo_epo), ? ,'"+subtipo+"'");
			mC.setBind(tipo);		
			ac.insertarEnCatalogo(mC);
			mensaje = "El registro fue agregado.";
			
		} else if(informacion.equals("Modificar")){
			mC.setTabla(" comcat_subtipo_epo ");
			mC.setClaveVal("cg_descripcion='"+subtipo+"'");
			mC.setCondicion(" ic_subtipo_epo= ?");
			mC.setBind(clave);		
			ac.actualizarCatalogo(mC);
			mensaje = "El registro fue Actualizado.";
			
		} else if(informacion.equals("Eliminar")){
			mC.setTabla(" comcat_subtipo_epo ");
			mC.setCondicion(" ic_subtipo_epo= ?");
			mC.setBind(clave);
			int regAfectados=ac.eliminarDelCatalogo(mC);
			if(regAfectados==1){
				mensaje = "El registro fue eliminado.";
				elim	= true;
			} else {
				mensaje = "No se puede eliminar el registro porque esta relacionado.";
			}
		}
	} catch (Exception e){
		agreg=false;
		mensaje = "Ocurrio un error al intentar "+informacion+" el registros.";
	}
	
	JSONObject jsonObj1 = new JSONObject();
	jsonObj1.put("success", new Boolean(true));
	jsonObj1.put("agregar", new Boolean(agreg));
	jsonObj1.put("eliminar", new Boolean(elim));
	jsonObj1.put("msg", mensaje);
	infoRegresar = jsonObj1.toString();
}

%>	
<%= infoRegresar %>