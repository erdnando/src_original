Ext.onReady(function() {
Ext.override(Ext.form.Field, {
  setFieldLabel : function(text) {
    if (this.rendered) {
      this.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(text);
    }
    this.fieldLabel = text;
  }
});

//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  
var fp = Ext.getCmp('forma');
  fp.el.unmask();							
  var gridConsulta = Ext.getCmp('grid');	
  var el = gridConsulta.getGridEl();		
  if (arrRegistros != null) {
    if (!gridConsulta.isVisible()) {
      gridConsulta.show();
      }				
    if(store.getTotalCount() > 0) {		
      el.unmask();
    } else {		
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
};
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15mancatalogoExt01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'IC_FIRMA_CEDULA'},
			{	name: 'CG_RAZON_SOCIAL'},
      {	name: 'CG_RAZON_SOCIAL_CE'},
			{	name: 'CG_TITULO'},
			{	name: 'CG_APPATERNO'},
      {	name: 'CG_APMATERNO'},
      {	name: 'CG_NOMBRE'},
			{	name: 'CG_PUESTO'},
      {	name: 'CS_ACTIVO'},
      {	name: 'IC_IF'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
//----------------------------Fin ConsultaData----------------------------------

//------------------------------------If----------------------------------------
var If = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin If---------------------------------------

//---------------------------- procesarIf---------------------------------------
	var procesarIf = function(store, arrRegistros, opts) {
		store.insert(0,new If({ 
      clave: "", 
      descripcion: "Seleccionar", 
      loadMsg: ""
    })); 
    
    Ext.getCmp('ic_if1').setValue("");
    if(Ext.getCmp('radioGroup').getValue()!='F' && Ext.getCmp('radioGroup').getValue()!='C'){
      Ext.getCmp('ic_if1').setVisible(false);
    }
    //Ext.getCmp('ic_if1').setValue(256);
		store.commitChanges(); 
	}	
//----------------------------Fin procesarIf------------------------------------

//---------------------------------radiosHabilitado-------------------------------
 var radiosHabilitado = [
  {
  xtype      : 'radio', 
  id			   : 's',
  boxLabel   : 'Si',
  name       : 'rbGroupHab',
  hidden		 : false,
  inputValue : 'S',
  handler 	 : function() {
   }//fin handler
  }, {
  xtype     : 'radio', 
  id				: 'n',
  boxLabel  : 'No',
  name      : 'rbGroupHab',
  inputValue: 'N',
  hidden		:false,
  handler 	: function() {  }//fin handler
  }
];
//---------------------------------radiosHabilitado-------------------------------

//---------------------------------radios-------------------------------
 var radios = [
  {
  xtype      : 'radio', 
  id			   : 'f',
  boxLabel   : 'Empleado IF',
  name       : 'rbGroup',
  //checked    :true,
  hidden		 :false,
  inputValue : 'F',
  handler 	 : function() {
   var m=Ext.getCmp('m');
   if(!m.getValue()){
      Ext.getCmp('ic_if1').setVisible(true);
      Ext.getCmp('s').setValue(false);
      Ext.getCmp('n').setValue(false);
    }
   }//fin handler
  }, {
  xtype     : 'radio', 
  id				: 'm',
  boxLabel  : 'Empleado Nafin',
  name      : 'rbGroup',
  hidden		:false,
  inputValue: 'M',
  handler 	: function() {  
    var f=Ext.getCmp('f');
    if(!f.getValue()){
      Ext.getCmp('ic_if1').setVisible(false);
      Ext.getCmp('s').setValue(false);
      Ext.getCmp('n').setValue(false);
      }
    }//fin handler
  }
];
//---------------------------------radios------------------------------- 

//-------------------------Store tipoTitulo---------------------------------
	var tipoTitulo = new Ext.data.ArrayStore({
		 fields		: ['clave', 'descripcion'],
		 data 		: [
							['C.','C.'],
              ['C.P.','C.P.'],
              ['Ing.','Ing.'],
              ['Lic.','Lic.'],
              ['Sr.','Sr.'],
              ['Sra.','Sra.'],
              ['Dr.','Dr.'],
              ['Dra.','Dra.'],
              ['Mvz.','Mvz.'],
              ['Biol.','Biol.']
						]
		})
//--------------------------Fin Store tipoTitulo----------------------------

//-------------------------Store tipoCatalogo---------------------------------
	var tipoCatalogo = new Ext.data.ArrayStore({
		 fields		: ['clave', 'descripcion'],
		 data 		: [
							['F','Firmas Cedulas']
						]
		})
//--------------------------Fin Store tipoCatalogo----------------------------

//----------------------------Fin procesarCatalogo--------------------------------
	var procesarCatalogo = function(store, arrRegistros, opts) {
		store.each(function(record){
		if(record.get('clave')==45 || record.get('clave')==6  || record.get('clave')==25 || record.get('clave')==101 || record.get('clave')==5 || record.get('clave')==102 || record.get('clave')==19 || record.get('clave')==7 ||
			record.get('clave')==29 ||  record.get('clave')==38 || record.get('clave')==43  || record.get('clave')==18 || record.get('clave')==8 ||
			record.get('clave')==24 || record.get('clave')==15 || record.get('clave')==41 || record.get('clave')==42 || record.get('clave')==17 || record.get('clave')==100 || record.get('clave')==50 || record.get('clave')==1 ||
			record.get('clave')==35 || record.get('clave')==26 || record.get('clave')==3  || record.get('clave')==52 || record.get('clave')==14 || record.get('clave')==270 ||  record.get('clave')==104 ||
			record.get('clave')==20 || record.get('clave')==61 || record.get('clave')==53 || record.get('clave')==10 || record.get('clave')==67 || record.get('clave')==208 || record.get('clave')==23 || record.get('clave')==22  ||
			record.get('clave')==9  || record.get('clave')==16 || record.get('clave')==64 || record.get('clave')==30 || record.get('clave')==48 || record.get('clave')==31  || record.get('clave')==32 || record.get('clave')==46  ||
			record.get('clave')==51 || record.get('clave')==27|| record.get('clave')==6769){
			store.remove(record);
		}
		});
		Ext.getCmp('cmbCAT').setValue('47')
		store.commitChanges(); 
	}	
//----------------------------Fin procesarCatalogo--------------------------------

//--------------------------Store catalogo----------------------------
var catalogo = new Ext.data.JsonStore({
		id		: 'catalogos',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: '15mancatalogoClasificacion01Ext.data.jsp' ,
		baseParams: {
			informacion: 'Clasificacion.Carga.Catalogo'
		},
		totalProperty: 'total',
		autoLoad: true,
		listeners: {
			load : procesarCatalogo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
//--------------------------Fin Store catalogo----------------------------

//------------------------------Catalogo IF-------------------------------------
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15mancatalogoExt01.data.jsp',
		baseParams: {
			informacion: 'catalogoif'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {	
			load: procesarIf,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
  
  var catalogoTipoPersona = new Ext.data.ArrayStore({
    fields		: ['clave', 'descripcion'],
    data 		: [
							['F','Empleado IF'],
              ['M','Empleado Nafin'],
              ['C','Cliente Externo']
						]
		})
//------------------------------Fin Catalogo IF---------------------------------

//-------------------------------elementosForma---------------------------------	
	var elementosForma = [{
		xtype:                  'compositefield',
		id:                     'cmpCB',
		combineErrors:          false,
		msgTarget:              'side',
		hidden:                 true,
		items: [{
			xtype:              'combo',
			name:               'cmbCatalogos',
			hiddenName:         '_cmbCat',
			id:                 'cmbCATHIDEN',
			fieldLabel:         'Cat�logos',
			mode:               'local',
			forceSelection:     true,
			triggerAction:      'all',
			typeAhead:          true,
			minChars:           1,
			store:              catalogo,
			valueField:         'clave',
			displayField:       'descripcion',
			editable:           true,
			typeAhead:          true,
			width:              340,
			listeners: {
				select: {
					fn: function(combo) {
						var valor = combo.getValue();
					}// FIN fn
				}//FIN select
			}//FIN listeners
		},{
			xtype:              'displayfield',
			frame:              true,
			border:             false,
			value:              '',
			width:              10
		},{
			xtype:              'button',
			text:               'Buscar',
			tooltip:            'Buscar',
			iconCls:            'icoBuscar',
			id:                 'btnBuscar',
			handler: function(boton, evento) { 
				var valor = Ext.getCmp('cmbCAT').getValue();
				if (valor == 2){ //Tasa
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
				} else if (valor == 11){ //Estatus Documento
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
				} else if (valor == 12){ //Cambio Estatus
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
				} else if (valor == 13){ //Clasificacion ****No disponible**
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
				} else if (valor == 21){ //Subsector
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
				} else if (valor == 33){ //D�as Inh�biles
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
				} else if (valor == 34){ //Productos
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
				} else if(valor == 36){ //Personas Facultadas por Banco
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
				} else if(valor == 37){ //D�as Inh�biles EPO ****No disponible**
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
				} else if(valor == 39){ //Version Convenio
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
				} else if(valor == 40){ //Bancos TEF
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
				} else if (valor == 47){ //Firma Cedulas
					fp.getForm().reset();
					Ext.getCmp('cmbCAT').setValue('47');
					Ext.getCmp('cmbTIT').setValue("C.");
					Ext.getCmp('f').setValue(false);
					Ext.getCmp('m').setValue(false);
					Ext.getCmp('s').setValue(false);
					Ext.getCmp('n').setValue(false);
					Ext.getCmp('m').setDisabled(false);
					Ext.getCmp('ic_if1').setValue('');
					Ext.getCmp('tfAyuda').setValue(1);
					Ext.getCmp('filagrid').reset();
					Ext.getCmp('ic_if1').setVisible(false);
					catalogoIF.load();
					consultaData.load();
					//window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
				} else if (valor == 49){ //Plazos por Producto
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
				} else if (valor == 60){ //Area Promocion
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
				} else if (valor == 62){ //Subdireccion
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
				} else if (valor == 63){ //Lider Promotor
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
				} else if (valor == 65){ //Subtipo EPO
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
				} else if (valor == 68){ //Ventanillas
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
				} else if (valor == 70){ //D�as Inh�biles por A�o
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
				} else if (valor == 71){		//D�as Inh�biles EPO por A�o
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
				} else if (valor == 677){		//Clasificaci�n Epo
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
				}else if (valor == 103){		//Programa a Fondo JR
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
				}
			}
		}]
		},/*{
			xtype:              'compositefield',
			id:                 'cmpTP',
			combineErrors:      false,
			hidden:             false,
			items:[{
				xtype:          'displayfield',
				id:             'tpersona',
				name:           '_tpersona',
				fieldLabel:     'Tipo de Persona'
			},{
				xtype:          'radiogroup',
				id:             'radioGroup',
				items:          radios,
				anchor:         '55%',
				style:          { width: '50%'},
				hidden:         false
			}]
		}*/{
      xtype: 'combo',
      fieldLabel: 'Tipo de Persona',
      name: 'rbGroup',
      id: 'radioGroup',		
      mode: 'local',			
      displayField: 'descripcion',
      valueField: 'clave',
      hiddenName: 'rbGroup',
      autoLoad: false,
      forceSelection: true,
      triggerAction: 'all',
      typeAhead: true,
      minChars: 1,
      store: catalogoTipoPersona,
      //tpl: NE.util.templateMensajeCargaCombo,
      anchor: '44%',
      listeners:{
          select:function(combo){
          
            if(combo.getValue()=="F"){
              Ext.getCmp('ic_if1').setVisible(true);
              Ext.getCmp('ic_if1').setFieldLabel('IF:');
              Ext.getCmp('s').setValue(false);
              Ext.getCmp('n').setValue(false);
              catalogoIF.load({
                params:{
                  rbGroup: Ext.getCmp('radioGroup').getValue()
                }
              })
            }
            
            if(combo.getValue()=="M"){
              Ext.getCmp('ic_if1').setVisible(false);
              Ext.getCmp('s').setValue(false);
              Ext.getCmp('n').setValue(false);
              catalogoIF.load({
                params:{
                  rbGroup: Ext.getCmp('radioGroup').getValue()
                }
              })
            }
            
            if(combo.getValue()=="C"){
              Ext.getCmp('ic_if1').setVisible(true);
              Ext.getCmp('ic_if1').setFieldLabel('Cliente Externo:');
              Ext.getCmp('s').setValue(false);
              Ext.getCmp('n').setValue(false);
              catalogoIF.load({
                params:{
                  rbGroup: Ext.getCmp('radioGroup').getValue()
                }
              })
            }
            
          }
        }
      },{
			xtype:              'combo',
			fieldLabel:         'IF',
			name:               'ic_if',
			id:                 'ic_if1',
			mode:               'local',
			displayField:       'descripcion',
			valueField:         'clave',
			hiddenName:         'ic_if',
			autoLoad:           false,
			forceSelection:     true,
			triggerAction:      'all',
			typeAhead:          true,
			minChars:           1,
			store:              catalogoIF,
			tpl:                NE.util.templateMensajeCargaCombo,
			anchor:             '95%'
		},{
			xtype:              'combo',
			name:               'cmbTitulo',
			id:                 'cmbTIT',
			hiddenName:         'cmbTitulo',
			fieldLabel:         'T�tulo',
			mode:               'local',
			forceSelection:     true,
			triggerAction:      'all',
			typeAhead:          true,
			minChars:           1,
			store:              tipoTitulo,
			valueField:         'clave',
			displayField:       'descripcion',
			editable:           true,
			typeAhead:          true,
			anchor:             '44%'
		},{
			layout:             'column',
			items:[{
				columnWidth:    .5,
				width:          '50%',
				layout:         'form',
				items: [{
					xtype:      'textfield',
					fieldLabel: 'Apellido Paterno',
					id:         'tfApaterno',
					name:       'tfPat',
					msgTarget:  'side',
					anchor:     '95%',
					maxLength:  30
				}]
			},{
				columnWidth:    .5,
				width:          '50%',
				layout:         'form',
				items: [{
					xtype:      'textfield',
					fieldLabel: '&nbsp;&nbsp; Apellido Materno',
					labelAlign: 'right',
					id:         'tfAmaterno',
					name:       'tfMat',
					msgTarget:  'side',
					anchor:     '95%',
					maxLength:  30
				}]
			}]
		},{
			layout:             'column',
			items:[{
				columnWidth:    .5,
				width:          '50%',
				layout:         'form',
				items: [{
					xtype:      'textfield',
					fieldLabel: 'Nombre',
					id:         'tfNombre',
					name:       'tfNom',
					msgTarget:  'side',
					anchor:     '95%',
					maxLength:  30
				}]
			},{
				columnWidth:    .5,
				width:          '50%',
				layout:         'form',
				items: [{
					xtype:      'textfield',
					fieldLabel: '&nbsp;&nbsp; Puesto',
					id:         'tfPuesto',
					name:       'tfPues',
					msgTarget:  'side',
					anchor:     '95%',
					maxLength:  50
				}]
			}]
		},{
			xtype:              'compositefield',
			id:                 'cmpHab',
			combineErrors:      false,
			hidden:             false,
			items:[{
				xtype:          'displayfield',
				id:             'tHabilitado',
				name:           '_tHabilitado',
				fieldLabel:     'Habilitado'
			},{
				xtype:          'radiogroup',
				id:             'radioGroupHabilitado',
				items:          radiosHabilitado,
				hidden:         false,
				anchor:         '55%',
				style:          { width: '50%'}
			}]
		},{
			xtype:              'compositefield',
			id:                 'ayuda',
			combineErrors:      false,
			msgTarget:          'side',
			hidden:             true,
			items: [{
				xtype:          'textfield',
				id:             'tfAyuda',
				name:           'accion',
				fieldLabel:     'accion',
				value:          1,
				width:          150,
				margins:        '0 20 0 0'
			},{
				xtype:          'displayfield',
				frame:          true,
				border:         false,
				value:          'filagrid:',
				width:          95
			},{
				xtype:          'textfield',
				id:             'filagrid',
				name:           '_fila',
				width:          150,
				margins:        '0 20 0 0'
			}]
		}
	];
//-----------------------------Fin elementosForma-------------------------------

//--------------------------------gridConsulta----------------------------------	
var gridConsulta = new Ext.grid.GridPanel({	
		store				:consultaData,
		id					:'grid',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'Contenido del Cat�logo',	
		enableColumnMove: false,
		enableColumnHide: false,
		listeners 		: {
		cellclick	: function(grid, rowIndex, columnIndex, e) {
							Ext.getCmp('filagrid').setValue(rowIndex)
			}
		},
		tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
						{
						iconCls	: 'icon-register-edit',
						text		: 'Editar Registro',
						disabled	: false,
						handler	: function(){
									var grid	  				= Ext.getCmp("grid");
									var seleccionados  	= grid.getSelectionModel().getSelections();
									if(       seleccionados.length == 0){
										Ext.MessageBox.alert('Aviso','Debe seleccionar un registro.');
										return;
									} else if( seleccionados.length >  1){
										Ext.MessageBox.alert('Aviso','S�lo se puede editar un registro a la vez.');
										return;
									}
									var formTit	  				= Ext.getCmp("forma");
									formTit.setTitle('Editar Cat�logo: Modificar Registro');
									var respuesta 			= new Object();
									respuesta['record']	= seleccionados[0];
									Ext.getCmp('grid').hide();
									Ext.getCmp('forma').show();
									//Ext.getCmp('cmpTP').setVisible(true);
									Ext.getCmp('radioGroup').setVisible(true);
									Ext.getCmp('cmpHab').setVisible(true);
									Ext.getCmp('radioGroupHabilitado').setVisible(true);
									Ext.getCmp('s').setVisible(true);
									Ext.getCmp('n').setVisible(true);
									//Ext.getCmp('filagrid').setValue(rowIndex);
									Ext.getCmp('tfAyuda').setValue(2);
									//var registro = grid.getStore().getAt(rowIndex);
									var tipoPersona=0;
									
									Ext.Ajax.request({
									  url: '15mancatalogoExt01.data.jsp',
									  params: Ext.apply(fp.getForm().getValues(),{
										 informacion: 'Persona',
										 apaterno: respuesta.record.data.CG_APPATERNO,
										 amaterno: respuesta.record.data.CG_APMATERNO,
										 nombre:   respuesta.record.data.CG_NOMBRE,
										 puesto:   respuesta.record.data.CG_PUESTO,
                     cliExt:   respuesta.record.data.CG_RAZON_SOCIAL_CE
									  }),
									  success : function(a,b) {
										var infoa = Ext.util.JSON.decode(a.responseText);
										//var infob = Ext.util.JSON.decode(b.responseText);
										tipoPersona=infoa.persona; 
										if (tipoPersona == 1){
											Ext.getCmp('radioGroup').setValue('F');
                      //Ext.getCmp('f').setValue(true);
											Ext.getCmp('ic_if1').setValue(respuesta.record.data.IC_IF);
											//Ext.getCmp('m').setDisabled(true);
                      Ext.getCmp('radioGroup').setReadOnly(true);
                    }else if (tipoPersona == 2){
                        Ext.getCmp('radioGroup').setValue('C');
                        //Ext.getCmp('f').setValue(true);
                        Ext.getCmp('ic_if1').setValue(respuesta.record.data.IC_IF);
                        Ext.getCmp('radioGroup').setReadOnly(true);
										 } else {
											//Ext.getCmp('f').setValue(false);
											//Ext.getCmp('f').setDisabled(true);
											//Ext.getCmp('m').setDisabled(false);
											//Ext.getCmp('m').setValue(true);
                      Ext.getCmp('radioGroup').setValue('M');
                      Ext.getCmp('radioGroup').setReadOnly(true);
										 }
										 Ext.getCmp('cmbTIT').setValue(respuesta.record.data.CG_TITULO);
										 Ext.getCmp('tfApaterno').setValue(respuesta.record.data.CG_APPATERNO);
										 Ext.getCmp('tfAmaterno').setValue(respuesta.record.data.CG_APMATERNO);
										 Ext.getCmp('tfNombre').setValue(respuesta.record.data.CG_NOMBRE);
										 Ext.getCmp('tfPuesto').setValue(respuesta.record.data.CG_PUESTO);      
										 var habilit =respuesta.record.data.CS_ACTIVO;
										 if (habilit === "S"){
											 Ext.getCmp('s').setValue(true);
										 } else {
											 Ext.getCmp('n').setValue(true);
										 } 
										 
									  }//fin success
									});//fin Ajax
									
							}
						},{
							iconCls	: 'icon-register-add',
							text		: 'Agregar Registro',
							disabled	: false,
							handler	: function(){
											var formTit	  				= Ext.getCmp("forma");
											formTit.setTitle('Editar Cat�logo: Agregar Registro');
											Ext.getCmp('grid').hide();
											Ext.getCmp('forma').show();
											//Ext.getCmp('cmpTP').setVisible(true);
											Ext.getCmp('radioGroup').setVisible(true);
                      Ext.getCmp('radioGroup').setReadOnly(false);
											Ext.getCmp('cmpHab').setVisible(true);
											Ext.getCmp('radioGroupHabilitado').setVisible(true);
											Ext.getCmp('s').setVisible(true);
											Ext.getCmp('n').setVisible(true);
							}
						},{
							iconCls	: 'icon-register-delete',
							text		: 'Eliminar Registro',
							disabled	: false,
							handler	: function(){
								var grid	  	 = Ext.getCmp("grid");
								var seleccionados   = grid.getSelectionModel().getSelections(); 
								var numeroRegistros = seleccionados.length;
								var aviso			  = "";
								if(numeroRegistros == 0){
									Ext.MessageBox.alert('Aviso','Debe seleccionar al menos un registro.');
									return;
								}/* else if(numeroRegistros > 1000){
									Ext.MessageBox.alert('Aviso','S�lo se permiten como m�ximo 1000 registros seleccionados.');
									return;
								}*/ else if(numeroRegistros == 1  ){
									aviso = 'El registro seleccionado ser� eliminado, �Desea usted continuar?';
								} else if(numeroRegistros >  1  ){
									Ext.MessageBox.alert('Aviso','Solo es permitido eliminar un registro.');
									return; 
								}
								var respuesta 			= new Object();
								respuesta['record']	= seleccionados[0];
								
								//var registro = grid.getStore().getAt(rowIndex);
								var idCed =respuesta.record.data.IC_FIRMA_CEDULA;
								Ext.Msg.show({
								  title: 'Eliminar',
								  msg: 'Esta seguro de querer eliminar el registro?',
								  modal: true,
								  icon: Ext.Msg.QUESTION,
								  buttons: Ext.Msg.OKCANCEL,
								  fn: function (btn, text){
										if (btn == 'ok' | btn == 'ok' ){
											Ext.Ajax.request({
												url: '15mancatalogoExt01.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
												  informacion: 'Eliminar',
												  id: idCed
												}),
												success : function(response) {
												var info = Ext.util.JSON.decode(response.responseText);
												var eliminar = info.eliminar;
												if(!eliminar){
													Ext.Msg.show({
													  title: 'Eliminar',
													  msg: 'No se puede eliminar el registro porque esta relacionado',
													  modal: true,
													  icon: Ext.Msg.WARNING,
													  buttons: Ext.Msg.OK,
													  fn: function (){
													  fp.el.mask('Cargando...', 'x-mask-loading');			
													  consultaData.load();
														 }
													  });//fin Msg
												  }else{
													//Ext.Msg.alert('Aviso','El registro fue eliminado');
													Ext.Msg.show({
													  title: 'Aviso',
													  msg: 'El registro fue eliminado',
													  modal: true,
													  icon: Ext.Msg.INFO,
													  buttons: Ext.Msg.OK,
													  fn: function (){
														fp.el.mask('Cargando...', 'x-mask-loading');			
														consultaData.load();
														}
													});
												  }
												}//FIN success
											});//fin AJAX
										}
									}
								  });//fin Msg
							}
						}
					]},						
						
		columns			:[
    {
    header		:'IF',
    tooltip		:'IF',
    dataIndex	:'CG_RAZON_SOCIAL',
    sortable	:true,
    width		:265,			
    resizable	:true,
    align		:'center',
    renderer	:function(value){
						return "<div align='left'>"+value+"</div>";
					}
    },{
    header		:'Cliente Externo',
    tooltip		:'Cliente Externo',
    dataIndex	:'CG_RAZON_SOCIAL_CE',
    sortable	:true,
    width		:265,			
    resizable	:true,
    align		:'center',
    renderer	:function(value){
						return "<div align='left'>"+value+"</div>";
					}
    },{
    header		:'T�tulo',
    tooltip		:'T�tulo',
    dataIndex	:'CG_TITULO',
    sortable	:true,
    resizable	:true,
    align		:'center'
    },{
    header		:'Apellido Paterno',
    tooltip		:'Apellido Paterno',
    dataIndex	:'CG_APPATERNO',
    sortable	:true,
    resizable	:true,
    align		:'center',
	 renderer	:function(value){
						return "<div align='left'>"+value+"</div>";
					}
    },{
    header		:'Apellido Materno',
    tooltip		:'Apellido Materno',
    dataIndex	:'CG_APMATERNO',
    sortable	:true,		
    resizable	:true,
    align		:'center',
	 renderer	:function(value){
						return "<div align='left'>"+value+"</div>";
					}
    },{
    header		:'Nombre',
    tooltip		:'Nombre',
    dataIndex	:'CG_NOMBRE',
    sortable	:true,		
    resizable	:true,
    align		:'center',
	 renderer	:function(value){
						return "<div align='left'>"+value+"</div>";
					}
    },{
    header		:'Puesto',
    tooltip		:'Puesto',
    dataIndex	:'CG_PUESTO',
    sortable	:true,
    width		:265,			
    resizable	:true,
    align		:'center',
	 renderer	:function(value){
						return "<div align='left'>"+value+"</div>";
					}
    },{
    header		:'Habilitado ',
    tooltip		:'Habilitado ',
    dataIndex	:'CS_ACTIVO',
    sortable	:true,
    resizable	:true,
    align		:'center',
    renderer	:function(value){
						if (value == "S"){
						  return "Si";
						} else {
						  return "No";
						}
					}//fin renderer
    },{
    xtype		: 'actioncolumn',
    header	: 'Seleccionar',
    tooltip	: 'Seleccionar',	
    //width		: 80,			
	 hidden:true,
    align		: 'center',
    items		: [
     {
      getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
        this.items[0].tooltip = 'Modificar';
        return 'icoModificar';										
      },
      handler: function(grid, rowIndex, colIndex, item, event){
      Ext.getCmp('filagrid').setValue(rowIndex);
      Ext.getCmp('tfAyuda').setValue(2);
      var registro = grid.getStore().getAt(rowIndex);
      var tipoPersona=0;
      
      Ext.Ajax.request({
        url: '15mancatalogoExt01.data.jsp',
        params: Ext.apply(fp.getForm().getValues(),{
          informacion: 'Persona',
          apaterno: registro.get('CG_APPATERNO'),
          amaterno: registro.get('CG_APMATERNO'),
          nombre:   registro.get('CG_NOMBRE'),
          puesto:   registro.get('CG_PUESTO'),
          cliExt:   registro.get('CG_RAZON_SOCIAL_CE')
        }),
        success : function(a,b) {
         var infoa = Ext.util.JSON.decode(a.responseText);
         //var infob = Ext.util.JSON.decode(b.responseText);
          tipoPersona=infoa.persona; 
          if (tipoPersona == 1){
            Ext.getCmp('radioGroup').setValue('F');
            //Ext.getCmp('f').setValue(true);
            Ext.getCmp('ic_if1').setValue(registro.get('IC_IF'));
            //Ext.getCmp('m').setDisabled(true);
            Ext.getCmp('radioGroup').setReadOnly(true);
          }else if (tipoPersona == 2){
            Ext.getCmp('radioGroup').setValue('C');
            //Ext.getCmp('f').setValue(true);
            Ext.getCmp('ic_if1').setValue(respuesta.record.data.IC_IF);
            Ext.getCmp('radioGroup').setReadOnly(true);
          } else {
            //Ext.getCmp('f').setValue(false);
            //Ext.getCmp('f').setDisabled(true);
            //Ext.getCmp('m').setDisabled(false);
            //Ext.getCmp('m').setValue(true);
            Ext.getCmp('radioGroup').setValue('M');
            Ext.getCmp('radioGroup').setReadOnly(true);
          }
          Ext.getCmp('cmbTIT').setValue(registro.get('CG_TITULO'));
          Ext.getCmp('tfApaterno').setValue(registro.get('CG_APPATERNO'));
          Ext.getCmp('tfAmaterno').setValue(registro.get('CG_APMATERNO'));
          Ext.getCmp('tfNombre').setValue(registro.get('CG_NOMBRE'));
          Ext.getCmp('tfPuesto').setValue(registro.get('CG_PUESTO'));      
          var habilit =registro.get('CS_ACTIVO');
          if (habilit === "S"){
             Ext.getCmp('s').setValue(true);
          } else {
             Ext.getCmp('n').setValue(true);
          } 
        }//fin success
      });//fin Ajax
	    }//fin handler
    },
    {
      getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
        this.items[1].tooltip = 'Eliminar';
        return 'cancelar';										
      },
      handler: function(grid, rowIndex, colIndex, item, event){
				var registro = grid.getStore().getAt(rowIndex);
				var idCed =registro.get('IC_FIRMA_CEDULA');
				Ext.Msg.show({
				  title: 'Eliminar',
				  msg: 'Esta seguro de querer eliminar el registro?',
				  modal: true,
				  icon: Ext.Msg.QUESTION,
				  buttons: Ext.Msg.OKCANCEL,
				  fn: function (btn, text){
						if (btn == 'ok' | btn == 'ok' ){
							Ext.Ajax.request({
								url: '15mancatalogoExt01.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
								  informacion: 'Eliminar',
								  id: idCed
								}),
								success : function(response) {
								var info = Ext.util.JSON.decode(response.responseText);
								var eliminar = info.eliminar;
								 Ext.Msg.show({
								  title: 'Eliminar',
								  msg: 'No se puede eliminar el registro porque esta relacionado',
								  modal: true,
								  icon: Ext.Msg.WARNING,
								  buttons: Ext.Msg.OK,
								  fn: function (){
								  fp.el.mask('Cargando...', 'x-mask-loading');			
								  consultaData.load();
									 }
								  });//fin Msg
								}//FIN success
							});//fin AJAX
						}
					}
				  });//fin Msg
      }//fin handler
    } 
  ]
	}   
],			
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 450,
    width				: 843,
		align				: 'center',
		frame				: true
	});	
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 843,
		title: 'Criterios de B�squeda del Cat�logo',
		frame: true,		
		collapsible		: true,
		titleCollapse	: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		labelWidth		: 130,
		items: elementosForma,
		buttons: [
      {
				text: 'Guardar',
				id: 'btnConsulta',
				iconCls: 'icoGuardar',
				handler: function (boton, evento){
          var help = Ext.getCmp('tfAyuda').getValue();
          if(Ext.getCmp('tfApaterno').isValid() & Ext.getCmp('tfAmaterno').isValid() & Ext.getCmp('tfNombre').isValid() & Ext.getCmp('tfPuesto').isValid() ){
			 } else {
				return;
			 }
			 
          if ( help == 1 ){//nuevo
          //if (( Ext.getCmp('f').getValue() || Ext.getCmp('m').getValue()) && ( Ext.getCmp('s').getValue()  || Ext.getCmp('n').getValue()) && Ext.getCmp('tfPuesto').getValue() != ""  ){
          if (( Ext.getCmp('radioGroup').getValue()=='F' || Ext.getCmp('radioGroup').getValue()=='M' || Ext.getCmp('radioGroup').getValue()=='C') && ( Ext.getCmp('s').getValue()  || Ext.getCmp('n').getValue()) && Ext.getCmp('tfPuesto').getValue() != ""  ){
          
            Ext.Ajax.request({
            url: '15mancatalogoExt01.data.jsp',
            params: Ext.apply(fp.getForm().getValues(),{
              informacion: 'Nuevo'
            }),
            success : function(a,b) {
             var infoa = Ext.util.JSON.decode(a.responseText);
             fp.getForm().reset();
             Ext.getCmp('cmbCAT').setValue(47);
             Ext.getCmp('cmbTIT').setValue("C.");
             Ext.getCmp('ic_if1').setVisible(false);
             Ext.Msg.show({
              title: 'Nuevo',
              msg: 'El registro fue agregado',
              modal: true,
              icon: Ext.Msg.INFO,
              buttons: Ext.Msg.OK,
              fn: function (){
              fp.el.mask('Cargando...', 'x-mask-loading');			
              consultaData.load();
                }
              });
            }//fin success
          });//fin Ajax
				 //Ext.getCmp('grid').show();
					  Ext.getCmp('forma').hide();
          } else {
           Ext.Msg.show({
            title: 'Faltan Datos',
            msg: 'Faltan Datos',
            modal: true,
            icon: Ext.Msg.WARNING,
            buttons: Ext.Msg.OK,
            fn: function (){//Ext.getCmp('tfPuesto').focus();
              }
            }); 
          }//fin else
          }//fin 1
          if (help == 2 ){//actualiza
			 var fila =Ext.getCmp('filagrid').getValue();
           var registro = gridConsulta.getStore().getAt(fila);
           Ext.Ajax.request({
            url: '15mancatalogoExt01.data.jsp',
            params: Ext.apply(fp.getForm().getValues(),{
              informacion: 'Actualiza',
              cedula: registro.get('IC_FIRMA_CEDULA'),
              interm: registro.get('IC_IF'),
              razon: registro.get('CG_RAZON_SOCIAL')
            }),
            success : function(a,b) {
             var infoa = Ext.util.JSON.decode(a.responseText);
             fp.getForm().reset();
             Ext.getCmp('cmbCAT').setValue(47);
             Ext.getCmp('cmbTIT').setValue("C.");
             //Ext.getCmp('f').setDisabled(false); 
             //Ext.getCmp('m').setDisabled(false);
             Ext.getCmp('radioGroup').setReadOnly(false);
             Ext.getCmp('ic_if1').setVisible(false);
             Ext.Msg.show({
              title: 'Modificaci�n',
              msg: 'El registro fue actualizado',
              modal: true,
              icon: Ext.Msg.INFO,
              buttons: Ext.Msg.OK,
              fn: function (){
                fp.el.mask('Cargando...', 'x-mask-loading');			
                consultaData.load();
                }
            }); //fin Msg
            }//fin success
          });//fin Ajax
			  //Ext.getCmp('grid').show();
					  Ext.getCmp('forma').hide();
          }
				}
			},{
      text: 'Cancelar',
      id: 'btnLimpiar',
      iconCls: 'icoCancelar',
      handler: function (boton, evento){
        //Ext.getCmp('f').setValue(false);
        //Ext.getCmp('m').setValue(false);
        Ext.getCmp('radioGroup').setValue('');
        Ext.getCmp('ic_if1').setValue('');
        Ext.getCmp('cmbTIT').setValue('C.');
        Ext.getCmp('tfApaterno').setValue('');
        Ext.getCmp('tfAmaterno').setValue('');
        Ext.getCmp('tfNombre').setValue('');
        Ext.getCmp('tfPuesto').setValue('');      
        Ext.getCmp('s').setValue(false);
        Ext.getCmp('n').setValue(false);        
        //Ext.getCmp('m').setDisabled(false); 
        Ext.getCmp('radioGroup').setReadOnly(false); 
        Ext.getCmp('ic_if1').setVisible(false);
        Ext.getCmp('tfAyuda').setValue(1);
        Ext.getCmp('filagrid').reset(); 
        Ext.getCmp('grid').show();
		  Ext.getCmp('forma').hide();
				}//FIN handler Limpiar
			}
		]
	});
//------------------------------Fin Panel Consulta------------------------------

	var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		autoHeight		:true,//height:			80,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		'padding:6px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		items: 			[
			 {
				 xtype		: 'combo',
				 name			: 'cmbCatalogos',
				 hiddenName	: '_cmbCat',
				 id			: 'cmbCAT',	
				 fieldLabel	: 'Cat�logo', 
				 mode			: 'local',	
				 forceSelection : true,	
				 triggerAction  : 'all',	
				 typeAhead		: true,
				 minChars 		: 1,	
				 store 			: catalogo,		
				 valueField 	: 'clave',
				 displayField	: 'descripcion',	
				 editable		: true,
				 typeAhead		: true,
				 anchor			: '95%',
				 listeners		: {
					select		: {
						fn			: function(combo) {
										var valor = Ext.getCmp('cmbCAT').getValue();
										if (valor == 2){					//Tasa
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
										} else if (valor == 11){		//Estatus Documento
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
										} else if (valor == 12){		//Cambio Estatus
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
										} else if (valor == 13){		//Clasificacion ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
										} else if (valor == 21){		//Subsector
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
										} else if (valor == 33){		//D�as Inh�biles
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
										} else if (valor == 34){		//Productos
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
										} else if(valor == 36){			//Personas Facultadas por Banco
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
										} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
										} else if(valor == 39){			//Version Convenio
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
										} else if(valor == 40){			//Bancos TEF
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
										} else if (valor == 47){		//Firma Cedulas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
										} else if (valor == 49){		//Plazos por Producto
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
										} else if (valor == 60){		//Area Promocion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
										} else if (valor == 62){		//Subdireccion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
										} else if (valor == 63){		//Lider Promotor
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
										} else if (valor == 65){		//Subtipo EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
										} else if (valor == 66){		//Sector EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
										} else if (valor == 68){		//Ventanillas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
										} else if (valor == 70){		//D�as Inh�biles por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
										}else if (valor == 71){		//D�as Inh�biles EPO por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
										} else if (valor == 677){		//Clasificaci�n Epo
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
										}else if (valor == 103){		//Programa a Fondo JR
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
										}
										}// FIN fn
										}//FIN select
									}//FIN listeners
				 }],
		monitorValid: 	false
	});


//----------------------------Contenedor Principal------------------------------	
var pnl = new Ext.Container({
	id			: 'contenedorPrincipal',
	applyTo	: 'areaContenido',
	width		: 949,		
	//style		: 'margin:0 auto;',
	items		: [
					NE.util.getEspaciador(20),	
					panelFormaCatalogos,
					NE.util.getEspaciador(20),	
					fp,
					NE.util.getEspaciador(20),
					gridConsulta,
					NE.util.getEspaciador(20)
					]
});	
//-----------------------------Fin Contenedor Principal-------------------------
Ext.getCmp('cmbTIT').setValue("C.");
catalogoIF.load();
consultaData.load();
Ext.getCmp('forma').setVisible(false);

});//-----------------------------------------------Fin Ext.onReady(function(){}