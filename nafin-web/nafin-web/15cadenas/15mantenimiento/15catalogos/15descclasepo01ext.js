Ext.onReady(function() {
		
	//----------------------------------- HANDLERS ---------------------------------------
	
	var procesaActualizarNombreClasificacionEPO = function(opts, success, response) { 
	
		var panelFormaDefinicion = Ext.getCmp("panelFormaDefinicion");
		panelFormaDefinicion.el.unmask();
			
		if ( success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var respuesta = Ext.util.JSON.decode(response.responseText);
			
			if( respuesta.actualizacionSuccess ){
				Ext.Msg.alert("Aviso","El registro fue actualizado. Favor de capturar �rea y Responsable en el Cat�logo: \"Clasificaci�n EPO\".");
			} else {
				Ext.Msg.alert("Aviso","No se pudo actualizar la Clasificaci�n EPO debido a que ocurri� un error.");
			}
			
		}else{
 
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesaConsultaNombreClasificacionEPO = function(opts, success, response) {
		
		// Suprimir m�scara
		var panelFormaDefinicion = Ext.getCmp("panelFormaDefinicion");
		panelFormaDefinicion.el.unmask();
 
		if ( success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var respuesta	= Ext.util.JSON.decode(response.responseText);
			var nombreClasificacionEPO = Ext.getCmp("nombreClasificacionEPO");
			nombreClasificacionEPO.setValue(respuesta.nombreClasificacionEPO);
			nombreClasificacionEPO.clearInvalid();
			
		}else{
 
			var botonActualizar = Ext.getCmp("botonActualizar");
			botonActualizar.setDisabled(true);
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesaConsultaCatalogo = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						consultaCatalogo(resp.estadoSiguiente,resp);
					}
				);
			} else {
				consultaCatalogo(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Resetear componentes segun se requiera
			var panelFormaCatalogos = Ext.getCmp("panelFormaCatalogos");
			panelFormaCatalogos.el.unmask();
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	//------------------------------- MAQUINA DE ESTADO ----------------------------------
	
	var consultaCatalogo = function(estadoSiguiente, respuesta){
		
		if( estadoSiguiente         == "CONSULTAR_CATALOGO" ){
			
			var panelFormaCatalogos							= Ext.getCmp("panelFormaCatalogos");
			var comboCatalogo									= panelFormaCatalogos.getComponent("comboCatalogo");
			
			Ext.Ajax.request({
				url: 		'15descclasepo01ext.data.jsp',
				params: 	{
					informacion:										'ConsultaCatalogo.consultarCatalogo',
					claveCatalogoDefinicionClasificacionEPO: 	claveCatalogoDefinicionClasificacionEPO,
					claveCatalogo: 									comboCatalogo.getValue(),
					pageId:												pageId
				},
				callback: 												procesaConsultaCatalogo
			});
			
		} else if(	estadoSiguiente == "ESPERAR_DECISION"	){
			
			// Resetear componentes segun se requiera
			var panelFormaCatalogos = Ext.getCmp("panelFormaCatalogos");
			panelFormaCatalogos.el.unmask();
			
			// Esperar la decision del usuario
			
		} else if(	estadoSiguiente == "FIN"					){

			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "15descclasepo01ext.data.jsp";
			forma.target	= "_self";
			
			Ext.DomHelper.insertFirst(forma, { 
					tag: 	'input', 
					type: 'hidden', 
					id: 	'informacion', 
					name: 'informacion', 
					value: "ConsultaCatalogo.fin"
			}); 
			
			Ext.DomHelper.insertFirst(forma, { 
					tag: 	'input', 
					type: 'hidden', 
					id: 	'pageId', 
					name: 'pageId', 
					value: pageId
			}); 
			
			if(!Ext.isEmpty(respuesta.redirigir)){
				Ext.DomHelper.insertFirst(forma, { 
					tag: 	'input', 
					type: 'hidden', 
					id: 	'redirigir', 
					name: 'redirigir', 
					value: String(respuesta.redirigir)
				}); 
			}
			
			if(!Ext.isEmpty(respuesta.claveCatalogo)){
				Ext.DomHelper.insertFirst(forma, { 
					tag: 	'input', 
					type: 'hidden', 
					id: 	'claveCatalogo', 
					name: 'claveCatalogo', 
					value: respuesta.claveCatalogo 
				}); 
			}
			
			forma.submit();
			
		} 
		
	}
		
	//--------------------------- FORMA CLASIFICACION EPO --------------------------------
	
	function doActualizarClasificacion(boton, evento){
		
		var panelFormaDefinicion = Ext.getCmp("panelFormaDefinicion");
		
		// Si la forma no es valida, cancelar la operaci�n
		if( !panelFormaDefinicion.getForm().isValid() ){
			return;
		}
		
		// Actualizar nombre
		panelFormaDefinicion.el.mask('Actualizando...','x-mask-loading');
		
		// Realizar actualizaci�n
		Ext.Ajax.request({
				url: 		'15descclasepo01ext.data.jsp',
				params: Ext.apply(
					panelFormaDefinicion.getForm().getValues(), {
						informacion: 	'ActualizarNombreClasificacionEPO',
						pageId:			pageId
					}
				),
				callback: 				procesaActualizarNombreClasificacionEPO
			}
		);
		
	}
	
	var elementosPanelFormaDefinicion = [
		{
			xtype:					"textfield",
			id:						"nombreClasificacionEPO",
			name:						"nombreClasificacionEPO",
			fieldLabel:				"Clasificaci&oacute;n EPO",
			readOnly:				false,
			allowBlank: 			false,
			maxLength:				100
		}
	];
	
	var panelFormaDefinicion =  new Ext.form.FormPanel({
		id: 				'panelFormaDefinicion',
		title: 			'Definici�n',
		width: 			843,
		frame: 			true,
		//collapsible: 	true,
		//titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		(elementosPanelFormaDefinicion.length > 0?'padding:10px':null),
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		defaultType: 	'textfield',
		items: 			elementosPanelFormaDefinicion,
		monitorValid: 	false,
		buttons:[	
			{
				name:			'botonActualizar',
				id:			'botonActualizar',
				text: 		'Actualizar',
				iconCls: 	'icoGuardar',
				formBind: 	true,
				handler: 	doActualizarClasificacion,
				hidden:		false
			}
		]
	});
	
	//-------------------------------- FORMA CATALOGOS -----------------------------------
	var catalogoCatalogosParametrizacionData = new Ext.data.JsonStore({
		id: 			'catalogoCatalogosParametrizacionDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15descclasepo01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogosParametrizacion',
			pageId:			pageId
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("comboCatalogo").setValue(defaultValue);
					}
					// Ext.getCmp("comboCatalogo").setReadOnly(true);
									
				} 
								
			}
		}
	});
 
	var elementosPanelFormaCatalogos = [
		{
			xtype: 				'combo',
			name: 				'comboCatalogo',
			id: 					'comboCatalogo',
			fieldLabel: 		'Cat�logo',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveCatalogo',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoCatalogosParametrizacionData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			listeners: {
				collapse: function(comboCatalogosParametrizacion){
					consultaCatalogo("CONSULTAR_CATALOGO",null);
				}
			}
		}
	];
 
	var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		(elementosPanelFormaCatalogos.length > 0?'padding:10px':null),
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		defaultType: 	'textfield',
		items: 			elementosPanelFormaCatalogos,
		monitorValid: 	false
	});
	
	//----------------------------------- CONTENEDOR -------------------------------------	
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			panelFormaCatalogos,
			NE.util.getEspaciador(10),
			panelFormaDefinicion
		]
	});
	
	//-------------------------------- INICIALIZACION -----------------------------------
	
	// Volver a llenar combo	
	catalogoCatalogosParametrizacionData.load({
     	params: { defaultValue: claveCatalogoDefinicionClasificacionEPO }
   });
   
   // Cargar nombre de la clasificacion de la EPO
   panelFormaDefinicion.el.mask('Cargando...','x-mask-loading');
   Ext.Ajax.request({
		url: 		'15descclasepo01ext.data.jsp',
		params: 	{
			informacion:		'NombreClasificacionEPO',
			pageId:				pageId
		},
		callback: 				procesaConsultaNombreClasificacionEPO
	});
 
});