<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.catalogos.*,
		com.netro.cadenas.*; "
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion 	= (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String clave		 	= (request.getParameter("clave") != null) ? request.getParameter("clave") :"";
	String fecha		 	= (request.getParameter("fechaName") != null) ? request.getParameter("fechaName") :"";
	String descripcion 	= (request.getParameter("descripcionName") != null) ? request.getParameter("descripcionName") :"";
	String infoRegresar 	= "";
	String consulta		= "";
	String mensaje			= "";

if(informacion.equals("Consultar")){
	
	PaginadorGenerico pG = new PaginadorGenerico();
	pG.setCampos("IC_DIA_INHABIL AS CLAVE, CG_DIA_INHABIL AS FECHA, CD_DIA_INHABIL AS DESCRIPCION");
	pG.setTabla("COMCAT_DIA_INHABIL");
	pG.setCondicion("DF_DIA_INHABIL IS NULL");
	pG.setOrden("IC_DIA_INHABIL");
	
	JSONObject jsonObj   = new JSONObject();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pG);
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Agregar") || informacion.equals("Modificar") || informacion.equals("Eliminar") ){

	boolean	agreg		= true;
	boolean	elim		= false;
	boolean	bandera	= true;	
	ActualizacionCatalogosBean ac = new ActualizacionCatalogosBean();
	MantenimientoCatalogo mC = new MantenimientoCatalogo ();
	try {
		if(informacion.equals("Agregar")){
			PaginadorGenerico pGA = new PaginadorGenerico();
			pGA.setCampos("IC_DIA_INHABIL");
			pGA.setTabla("COMCAT_DIA_INHABIL");
			pGA.setCondicion("DF_DIA_INHABIL IS NULL AND CG_DIA_INHABIL='"+fecha+"'");
			CQueryHelperRegExtJS queryHelperA = new CQueryHelperRegExtJS(pGA);
			Registros regA	=	queryHelperA.doSearch(); 
			if(!regA.next()){
				mC.setTabla("COMCAT_DIA_INHABIL");
				mC.setCampos("CD_DIA_INHABIL,IC_DIA_INHABIL,CG_DIA_INHABIL");
				mC.setValues("'"+descripcion+"',?,'"+fecha+"'");
				pGA.setCampos("NVL (MAX (IC_DIA_INHABIL), 0) + 1");
				pGA.setTabla("COMCAT_DIA_INHABIL");
				pGA.setCondicion("");
				CQueryHelperRegExtJS queryHelperAi = new CQueryHelperRegExtJS(pGA);
				regA	=	queryHelperAi.doSearch(); 
				String id = "";
				if(regA.next()){
					id = regA.getString(1);
				}
				mC.setBind(id);		
				ac.insertarEnCatalogo(mC);
				mensaje = "El registro fue agregado.";
			}else{
				mensaje = "El día inhábil ya existe.";
			}
		} else if(informacion.equals("Modificar")){
			PaginadorGenerico pGM = new PaginadorGenerico();
			pGM.setCampos("CG_DIA_INHABIL");
			pGM.setTabla("COMCAT_DIA_INHABIL");
			pGM.setCondicion("IC_DIA_INHABIL = "+clave);
			CQueryHelperRegExtJS queryHelperM = new CQueryHelperRegExtJS(pGM);
			Registros regM	=	queryHelperM.doSearch();
			String date="";
			while(regM.next()){
				date = (String)regM.getString(1);
				break;
			}
			pGM.setCampos("IC_DIA_INHABIL");
			pGM.setTabla("COMCAT_DIA_INHABIL");
			pGM.setCondicion("DF_DIA_INHABIL IS NULL AND CG_DIA_INHABIL='"+fecha+"'");
			regM	=	queryHelperM.doSearch();
			if(regM.next() && !date.equals(fecha)){
				bandera=false;
				mensaje = "El día inhábil ya existe";
			} else { 
				mC.setTabla(" COMCAT_DIA_INHABIL ");
				mC.setClaveVal("CG_DIA_INHABIL = '"+fecha+"',  CD_DIA_INHABIL = '"+descripcion+"'");
				mC.setCondicion(" IC_DIA_INHABIL= ?");
				mC.setBind(clave);		
				ac.actualizarCatalogo(mC);
				mensaje = "El registro fue Actualizado.";	
			}
		} else if(informacion.equals("Eliminar")){
			mC.setTabla(" COMCAT_DIA_INHABIL ");
			mC.setCondicion(" IC_DIA_INHABIL= ?");
			mC.setBind(clave);
			int regAfectados=ac.eliminarDelCatalogo(mC);
			if(regAfectados==1){
				mensaje = "El registro fue eliminado.";
				elim	= true;
			} else {
				mensaje = "No se puede eliminar el registro porque esta relacionado.";
			}
		}
	} catch (Exception e){
		agreg=false;
		mensaje = "Ocurrio un error al intentar "+informacion+" el registros.";
	}
	JSONObject jsonObj1 = new JSONObject();
	jsonObj1.put("success", new Boolean(true));
	jsonObj1.put("agregar", new Boolean(agreg));
	jsonObj1.put("eliminar", new Boolean(elim));
	jsonObj1.put("msg", mensaje);
	infoRegresar = jsonObj1.toString();

} 

%>
<%=infoRegresar%>

