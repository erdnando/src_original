Ext.onReady(function() {

//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
	var fp = Ext.getCmp('forma');
		 fp.el.unmask();							
	var gridConsulta = Ext.getCmp('grid');	
	var el = gridConsulta.getGridEl();		
	if (arrRegistros != null) {
		if (!gridConsulta.isVisible()) {
			gridConsulta.show();
			}				
		if(store.getTotalCount() > 0) {		
			el.unmask();
		} else {		
			el.mask('No se encontr� ning�n registro', 'x-mask');				
		}
	}
};
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
var consultaData   = new Ext.data.JsonStore({ 
	root : 'registros',
	url : '15mancatalogoPlazosProductoExt01.data.jsp',
	baseParams: {
		informacion: 'Consultar'
	},		
	fields: [	
	{	name: 'PRODUCTO'},
	{	name: 'CG_DESCRIPCION'},
	{	name: 'IN_PLAZO_DIAS'},
	{	name: 'IN_PLAZO_MESES'}
	],		
	totalProperty 	: 'total',
	messageProperty: 'msg',
	autoLoad			: true,
	listeners		: {
		load			: procesarConsultaData,
		exception	: {
			fn			: function(proxy, type, action, optionsRequest, response, args) {
							NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
							procesarConsultaData(null, null, null);					
							}
					}
		}		
});
//----------------------------Fin ConsultaData----------------------------------

//-------------------------Store diasDesde---------------------------------
var diasDesde = new Ext.data.ArrayStore({
 fields	: ['clave', 'descripcion'],
 data 	: [
  ['1','1'],
  ['31','31'],
  ['61','61'],
  ['91','91'],
  ['121','121'],
  ['151','151'],
  ['181','181'],
  ['211','211'],
  ['241','241'],
  ['271','271'],
  ['301','301'],
  ['331','331']
 ]
});
//--------------------------Fin Store diasDesde----------------------------

//-------------------------Store diasHasta---------------------------------
var diasHasta = new Ext.data.ArrayStore({
 fields		: ['clave', 'descripcion'],
 data 		: [
  ['30','30'],
  ['60','60'],
  ['90','90'],
  ['120','120'],
  ['150','150'],
  ['180','180'],
  ['210','210'],
  ['240','240'],
  ['270','270'],
  ['300','300'],
  ['330','330'],
  ['360','360'],
  ['366','366']
  ]
});
//--------------------------Fin Store diasHasta----------------------------

//----------------------------Fin procesarCatalogo-------------------------------
var procesarCatalogo = function(store, arrRegistros, opts) {
	store.each(function(record){
		if(record.get('clave')==45 || record.get('clave')==6  || record.get('clave')==25 || record.get('clave')==101 || record.get('clave')==5 || record.get('clave')==102 || record.get('clave')==19 || record.get('clave')==7 ||
			record.get('clave')==29 ||  record.get('clave')==38 || record.get('clave')==43  || record.get('clave')==18 || record.get('clave')==8 ||
			record.get('clave')==24 || record.get('clave')==15 || record.get('clave')==41 || record.get('clave')==42 || record.get('clave')==17 || record.get('clave')==100 || record.get('clave')==50 || record.get('clave')==1 ||
			record.get('clave')==35 || record.get('clave')==26 || record.get('clave')==3  || record.get('clave')==52 || record.get('clave')==14 || record.get('clave')==270 ||  record.get('clave')==104 ||
			record.get('clave')==20 || record.get('clave')==61 || record.get('clave')==53 || record.get('clave')==10 || record.get('clave')==67 || record.get('clave')==208 || record.get('clave')==23 || record.get('clave')==22  ||
			record.get('clave')==9  || record.get('clave')==16 || record.get('clave')==64 || record.get('clave')==30 || record.get('clave')==48 || record.get('clave')==31  || record.get('clave')==32 || record.get('clave')==46  ||
			record.get('clave')==51 || record.get('clave')==27|| record.get('clave')==6769){
			store.remove(record);
		}
	});
	Ext.getCmp('cmbCAT').setValue('49');
	store.commitChanges(); 
}	
//----------------------------Fin procesarCatalogo-------------------------------

//--------------------------Store catalogo--------------------------------------
var catalogo = new Ext.data.JsonStore({
		id		: 'catalogos',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: '15mancatalogoClasificacion01Ext.data.jsp' ,
		baseParams: {
			informacion: 'Clasificacion.Carga.Catalogo'
		},
		totalProperty: 'total',
		autoLoad: true,
		listeners: {
			load : procesarCatalogo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
//--------------------------Fin Store catalogo-----------------------------------

//----------------------------Fin procesarProductos-------------------------------
var procesarProductos = function(store, arrRegistros, opts) {
	Ext.getCmp('comboProducto').setValue('1')
	store.commitChanges(); 
}	
//----------------------------Fin procesarProductos-------------------------------

//------------------------------ store productos --------------------------------
var productos = new Ext.data.JsonStore({
	id		: 'storeProductos',
	root	: 'registros',
	fields: ['clave','descripcion','loadMsg'],
	url	: '15mancatalogoPlazosProductoExt01.data.jsp',
	baseParams: {
		informacion: 'productos'
	},
	totalProperty	: 'total',
	autoLoad			: true,
	listeners		: {	
		load			: procesarProductos,
		exception	: NE.util.mostrarDataProxyError,
		beforeload	: NE.util.initMensajeCargaCombo
	}		
});
//------------------------------Fin store productos -----------------------------

//-------------------------------elementosForma---------------------------------	
var elementosForma =  [	
  {
  xtype: 'compositefield',
  id:'cmpCBHIDE',
  combineErrors: false,
  msgTarget: 'side',
  hidden : true,
  items: [
    {
    xtype		: 'combo',
    name			: 'cmbCatalogos',
    hiddenName	: '_cmbCat',
    id			: 'cmbCATHIDEN',	
    fieldLabel	: 'Cat�logos', 
    mode			: 'local',	
    forceSelection : true,	
    triggerAction  : 'all',	
    typeAhead		: true,
    minChars 		: 1,	
    store 			: catalogo,		
    valueField 	: 'clave',
    displayField	: 'descripcion',	
    editable		: true,
    typeAhead		: true,
    width			:  330,
	 listeners		: {
		select		: {
			fn			: function(combo) {
							var valor  = combo.getValue();
							}// FIN fn
							}//FIN select
						}//FIN listeners
    },{
    xtype :'displayfield',
    frame :true,
    border: false,
    value :'',
    width	: 10
    },{
    xtype   : 'button',
    text    : 'Buscar',					
    tooltip :	'Buscar',
    iconCls : 'icoBuscar',
    id      : 'btnBuscar', 
    handler : function(boton, evento) { 
	 var valor = Ext.getCmp('cmbCAT').getValue();
		if (valor == 2){					//Tasa
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
		} else if (valor == 11){		//Estatus Documento
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
		} else if (valor == 12){		//Cambio Estatus
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
		} else if (valor == 13){		//Clasificacion ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
		} else if (valor == 21){		//Subsector
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
		} else if (valor == 33){		//D�as Inh�biles
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
		} else if (valor == 34){		//Productos
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
		} else if(valor == 36){			//Personas Facultadas por Banco
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
		} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
		} else if(valor == 39){			//Version Convenio
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
		} else if(valor == 40){			//Bancos TEF
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
		} else if (valor == 47){		//Firma Cedulas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
		} else if (valor == 49){		//Plazos por Producto
			consultaData.load();	
			//window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
		} else if (valor == 60){		//Area Promocion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
		} else if (valor == 62){		//Subdireccion
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
		} else if (valor == 63){		//Lider Promotor
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
		} else if (valor == 65){		//Subtipo EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
		} else if (valor == 66){		//Sector EPO
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
		} else if (valor == 68){		//Ventanillas
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
		} else if (valor == 677){		//Clasificaci�n Epo
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
		}else if (valor == 70){		//D�as Inh�biles por A�o
			window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
		} 
	 }
    }]			
  } , {
  xtype				: 'combo',
  fieldLabel		: 'Producto',
  name				: 'producto',
  id					: 'comboProducto',		
  //mode				: 'local',			
  displayField 	: 'descripcion',
  valueField		: 'clave',
  hiddenName		: 'comboProductoHidden',
  autoLoad			: true,
  forceSelection	: true,
  triggerAction	: 'all',
  typeAhead			: true,
  minChars			: 1,
  store				: productos,
  anchor				: '95%'
  },{
  xtype				: 'combo',
  name				: 'diasDesde',
  id					: 'comboDiasDesde',	
  hiddenName		: 'comboDiasDesdeHidden',  
  fieldLabel		: 'D�as desde', 
  mode				: 'local',	
  forceSelection  : true,	
  triggerAction   : 'all',	
  typeAhead			: true,
  minChars 			: 1,	
  store 				: diasDesde,		
  valueField 		: 'clave',
  displayField		: 'descripcion',	
  editable			: true,
  typeAhead			: true,
  anchor				: '95%'
  },{
  xtype				: 'combo',
  name				: 'diasHasta',
  id					: 'comboDiasHasta',	
  hiddenName		: 'comboDiasHastaHidden',  
  fieldLabel		: 'D�as hasta', 
  mode				: 'local',	
  forceSelection  : true,	
  triggerAction   : 'all',	
  typeAhead			: true,
  minChars 			: 1,	
  store 				: diasHasta,		
  valueField 		: 'clave',
  displayField		: 'descripcion',	
  editable			: true,
  typeAhead			: true,
  anchor				: '95%'
  }
];
//-----------------------------Fin elementosForma-------------------------------

//--------------------------------gridConsulta----------------------------------	
var gridConsulta = new Ext.grid.EditorGridPanel({	
	store				:consultaData,
	id					:'grid',
	margins			:'20 0 0 0',		
	style				:'margin:0 auto;',
	title				:'Contenido del Cat�logo',
	displayInfo		: true,		
	emptyMsg			: "No hay registros.",		
	loadMask			: true,
	stripeRows		: true,
	height			: 450,
	width				: 843,
	align				: 'center',
	frame				: true,
	hidden			: false,
	enableColumnMove: false,
	enableColumnHide: false,
	tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
					{
					iconCls	: 'icon-register-edit',
					text		: 'Editar Registro',
					disabled	: true,
					handler	: function(){}
					},{
					iconCls	: 'icon-register-add',
					text		: 'Agregar Registro',
					disabled	: false,
					handler	: function (){
								Ext.getCmp('grid').hide();
								Ext.getCmp('forma').show();
					}
					},{
					iconCls	: 'icon-register-delete',
					text		: 'Eliminar Registro',
					disabled	: true,
					handler	: function(){}
					}					
			]
	},
	columns			:[
		{
		header		:'Producto',
		tooltip		:'Producto',
		dataIndex	:'PRODUCTO',
		sortable	:true,
		width		:250,			
		resizable	:true,
		align		:'center',
		renderer	:function(value){
		return	"<div align='left'>"+value+"</div>";
		}
		},{
		header		:'Descripci�n',
		tooltip		:'Descripci�n',
		dataIndex	:'CG_DESCRIPCION',
		sortable	:true,
		width		:200,			
		resizable	:true,
		align		:'center'
		},{
		header		:'Plazo en D�as',
		tooltip		:'Plazo en D�as',
		dataIndex	:'IN_PLAZO_DIAS',
		sortable	:true,
		width		:200,			
		resizable	:true,
		align		:'center'
		},{
		header		:'Plazo en Meses',
		tooltip		:'Plazo en Meses',
		dataIndex	:'IN_PLAZO_MESES',
		sortable	:true,
		width		:150,			
		resizable	:true,
		align		:'center'
		}
	]
});	
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------Panel Consulta---------------------------------
var fp = new Ext.form.FormPanel({
	id		: 'forma',
	width	: 843,
	title	: 'Editar Cat�logo: Agregar Registro',
	frame	: true,		
	collapsible		: true,
	titleCollapse	: true,
	labelWidth		: 130,
	style	: 'margin:0 auto;',
	bodyStyle: 'padding: 6px',
	defaults	: {
		msgTarget: 'side',
		anchor: '-20'
	},
	items: elementosForma,
		buttons: [
      {
		text		: 'Guardar',
		id			: 'btnConsulta',
		iconCls	: 'icoGuardar',
		handler	: function (boton, evento){
		var desde= Ext.getCmp('comboDiasDesde').getValue();
		var hasta= Ext.getCmp('comboDiasHasta').getValue();
		var intDesde = parseInt(desde);
		var intHasta = parseInt(hasta);
		
		if (intDesde < intHasta){//if(Ext.getCmp('comboDiasDesde').getValue() < Ext.getCmp('comboDiasHasta').getValue()){
         Ext.Ajax.request({
			url: '15mancatalogoPlazosProductoExt01.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
			  informacion: 'Nuevo'
			}),
			success : function(response) {
				var info = Ext.util.JSON.decode(response.responseText);
				var nuevo = info.nuevo;
				if (nuevo){
					Ext.getCmp('forma').hide();
				  Ext.Msg.show({
				  title: 'Registro.',
				  msg: info.msg,
				  modal: true,
				  icon: Ext.Msg.INFO,
				  buttons: Ext.Msg.OK,
				  fn: function (){
						window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
					 }
				  });//fin Msg
			} else {
				  /*Ext.getCmp('forma').hide();
				  Ext.getCmp('grid').show();	*/
				  Ext.Msg.show({
				  title: 'Registro.',
				  msg: info.msg,
				  modal: true,
				  icon: Ext.Msg.WARNING,
				  buttons: Ext.Msg.OK,
				  fn: function (){
						//window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
					 }
				  });//fin Msg
			}
			}//fin success
		});//fin AJAX
	} else {
		Ext.Msg.show({
		title: 'Plazo',
		msg: 'El plazo debe capturarse en orden ascendente.',
		modal: true,
		icon: Ext.Msg.INFO,
		buttons: Ext.Msg.OK,
		fn: function (){
			Ext.getCmp('comboDiasDesde').focus();
			}
		});//fin Msg
	}
	}//FIN handler Aceptar
  },{
      text: 'Cancelar',
      id: 'btnLimpiar',
      iconCls: 'icoCancelar',
      handler: function (boton, evento){
			Ext.getCmp('forma').hide();
			Ext.getCmp('grid').show();
      //  window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
				}//FIN handler Limpiar
		} ]
});
//------------------------------Fin Panel Consulta------------------------------

	var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		autoHeight:true,//height:			80,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		'padding:6px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		items: 			[
			 {
				 xtype		: 'combo',
				 name			: 'cmbCatalogos',
				 hiddenName	: '_cmbCat',
				 id			: 'cmbCAT',	
				 fieldLabel	: 'Cat�logo', 
				 mode			: 'local',	
				 forceSelection : true,	
				 triggerAction  : 'all',	
				 typeAhead		: true,
				 minChars 		: 1,	
				 store 			: catalogo,		
				 valueField 	: 'clave',
				 displayField	: 'descripcion',	
				 editable		: true,
				 typeAhead		: true,
				 anchor			: '95%',
				 listeners		: {
					select		: {
						fn			: function(combo) {
										var valor = Ext.getCmp('cmbCAT').getValue();
										if (valor == 2){					//Tasa
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
										} else if (valor == 11){		//Estatus Documento
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
										} else if (valor == 12){		//Cambio Estatus
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
										} else if (valor == 13){		//Clasificacion ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
										} else if (valor == 21){		//Subsector
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
										} else if (valor == 33){		//D�as Inh�biles
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
										} else if (valor == 34){		//Productos
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
										} else if(valor == 36){			//Personas Facultadas por Banco
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
										} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
										} else if(valor == 39){			//Version Convenio
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
										} else if(valor == 40){			//Bancos TEF
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
										} else if (valor == 47){		//Firma Cedulas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
										} else if (valor == 49){		//Plazos por Producto
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
										} else if (valor == 60){		//Area Promocion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
										} else if (valor == 62){		//Subdireccion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
										} else if (valor == 63){		//Lider Promotor
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
										} else if (valor == 65){		//Subtipo EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
										} else if (valor == 66){		//Sector EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
										} else if (valor == 68){		//Ventanillas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
										} else if (valor == 677){		//Clasificaci�n Epo
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
										}else if (valor == 70){		//D�as Inh�biles por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
										}
										}// FIN fn
										}//FIN select
									}//FIN listeners
				 }],
		monitorValid: 	false
	});	

//----------------------------Contenedor Principal------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		//style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			panelFormaCatalogos,
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});	
//-----------------------------Fin Contenedor Principal-------------------------
Ext.getCmp('comboDiasDesde').setValue(1);
Ext.getCmp('comboDiasHasta').setValue(30);
Ext.getCmp('forma').setVisible(false);
});//-----------------------------------------------Fin Ext.onReady(function(){}