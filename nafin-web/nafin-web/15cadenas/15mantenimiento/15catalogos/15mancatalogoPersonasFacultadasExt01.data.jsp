<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.catalogos.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.Consultas,
		com.netro.cadenas.PaginadorGenerico,
		com.netro.cadenas.MantenimientoCatalogo"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion  = (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String infoRegresar = "";
	String consulta	  = "";

if(informacion.equals("catalogoif")) {
   
	JSONObject jsonObj = new JSONObject();
	CatalogoIF	catIf  = new CatalogoIF();
	catIf.setClave("ic_if");
	catIf.setDescripcion("cg_razon_social");
	catIf.setG_orden("2");
	List lis = catIf.getListaElementosGral();
	jsonObj.put("registros", lis);
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString(); 
  
} else if(informacion.equals("productos")) {
   
	List conditions =  new ArrayList ();
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_producto_nafin");
	cat.setDistinc(false);
	cat.setCampoClave("ic_producto_nafin");
	cat.setCampoDescripcion("ic_nombre");
	conditions.add(new Integer(1));
	conditions.add(new Integer(2));
	conditions.add(new Integer(0));
	cat.setValoresCondicionIn(conditions);
	List lista = cat.getListaElementos();
	JSONObject	jsonObj  = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("total",	  new Integer( lista.size()) );	
	jsonObj.put("registros", lista);	
   infoRegresar = jsonObj.toString();
  
} else if(informacion.equals("Consultar")){
	
	PaginadorGenerico pG = new PaginadorGenerico();
	pG.setCampos("a.cg_razon_social, b.cg_ap_paterno, b.cg_ap_materno, b.cg_nombre,b.cg_puesto, b.cs_habilitado, b.ic_if, b.ic_personal_facultado,cpn.ic_nombre, b.ic_producto_nafin");
	pG.setTabla("com_personal_facultado b, comcat_if a, comcat_producto_nafin cpn ");
	pG.setCondicion(" a.ic_if = b.ic_if AND a.cs_habilitado = 'S' AND cpn.ic_producto_nafin = b.ic_producto_nafin ");
		
	JSONObject jsonObj   = new JSONObject();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pG);
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("Nuevo")){

	String accion     = (request.getParameter("accion") != null) ? request.getParameter("accion") :"";
	String intermed	= (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") :"";
	String producto 	= (request.getParameter("comboProductoHidden") != null) ? request.getParameter("comboProductoHidden") :"";
	String paterno		= (request.getParameter("tfPat") != null) ? request.getParameter("tfPat") :"";
	String materno	 	= (request.getParameter("tfMat") != null) ? request.getParameter("tfMat") :"";
	String nombre	 	= (request.getParameter("tfNom") != null) ? request.getParameter("tfNom") :"";
	String puesto		= (request.getParameter("tfPues") != null) ? request.getParameter("tfPues") :"";
	String habilitado	= (request.getParameter("rbGroupHab") != null) ? request.getParameter("rbGroupHab") :"";
	String icFacultado= (request.getParameter("_icFacultado") != null) ? request.getParameter("_icFacultado") :"";
	String mensaje		= "";
	String claveCat	= "";
	boolean bandera	= true;
	int veri =0;
	JSONObject jsonObj   = new JSONObject();
	Consultas  cons		= new Consultas();
	List 	info	= new ArrayList();	
	try {
		cons.setCampos("*");
		cons.setTabla("com_personal_facultado");
		String str = cons.getInfoByQuery();	
		if (!str.equals("")){
			cons.setCampos("nvl(max(ic_personal_facultado),0)+1 as ClaveCatalogo");
			cons.setTabla("com_personal_facultado");
			cons.setCondicion("");
			str = cons.getInfoByQuery();	
			if(!str.equals("")){
				String [] newCadena = str.split(",");
				for(int a=0; a<newCadena.length; a++){
					if(a==2){
						claveCat = newCadena[a];
					}
				}
			}
		} else {
			claveCat ="0";
		}		
		if(accion.equals("agregar")){
			int registro = 0;
			cons.setCampos("*");
			cons.setTabla("com_personal_facultado");
			cons.setCondicion("ic_if= "+intermed+" And cs_habilitado='S' And ic_producto_nafin ="+producto);
			str = cons.getInfoByQuery();	
			if(!str.equals("")){  
				if(habilitado.equals("S")){
						bandera=false;
						mensaje="No puede crear porque ya existe un registro habilitado para ese intermediario";
					}
			}
			
		} else if(accion.equals("modificar")){
			veri		= Integer.parseInt(icFacultado);
			cons.setCampos("*");
			cons.setTabla("com_personal_facultado");
			cons.setCondicion("ic_if= "+intermed+" And cs_habilitado='S' And ic_producto_nafin ="+producto);
			str = cons.getInfoByQuery();		
			if(!str.equals("")){
					int personalFacultado=1;
					String [] newCadena = str.split(",");
					for(int a=0; a<newCadena.length; a++){
						if(a==5){
							personalFacultado =Integer.parseInt(newCadena[a]);
							break;
						} 
					}				
				if(personalFacultado == veri){
					
				}else{
					if(habilitado.equals("S")){
						bandera=false;
						mensaje="No puede habilitar porque ya existe un registro habilitado para ese intermediario";
					}
				}
			}
			cons.setCampos("*");
			cons.setTabla("com_personal_facultado");
			cons.setCondicion("ic_if= "+intermed+" And cs_habilitado='S' And ic_personal_facultado = "+veri+" And ic_producto_nafin ="+producto);
			str = cons.getInfoByQuery();	
			if(!str.equals("")){
				if(habilitado.equals("N")){
					bandera=true;
				} else{
					int personalFacultado=1;
					String [] newCadena = str.split(",");
					for(int a=0; a<newCadena.length; a++){
						if(a==5){
							personalFacultado = Integer.parseInt(newCadena[a]);
							break;
						} 
					}
					if(personalFacultado==veri){
						
					}
					else
						bandera=false;
				}
			}
		}//fin modificar
		
		if(bandera){	
			ActualizacionCatalogosBean ac = new ActualizacionCatalogosBean();
			MantenimientoCatalogo mC = new MantenimientoCatalogo ();
			String values	 = "'"+paterno+"','" + materno +"','"+nombre+"','"+puesto+"','"+habilitado+"',0,"+intermed+","+producto;
			String condicion= " ic_if=" + intermed +" And ic_personal_facultado =0  And ic_producto_nafin = " + producto;
			if(accion.equals("agregar")){
				mC.setTabla("com_personal_facultado");
				mC.setCampos("cg_ap_paterno, cg_ap_materno, cg_nombre, cg_puesto,cs_habilitado, ic_personal_facultado, ic_if, ic_producto_nafin");
				mC.setValues("'"+paterno+"','" + materno +"','"+nombre+"','"+puesto+"','"+habilitado+"',0,"+intermed+", ?");
				mC.setBind(producto);		
				ac.insertarEnCatalogo(mC);
				mensaje = "El registro fue agregado.";
			} else if(accion.equals("modificar")){
				mC.setTabla(" com_personal_facultado ");
				mC.setClaveVal("cg_ap_paterno ='"+paterno+"',cg_ap_materno = '"+materno+"',cg_nombre = '"+nombre+"',cg_puesto = '"+puesto+"',cs_habilitado = '"+habilitado+"'");
				mC.setCondicion(" ic_if=" + intermed +" And ic_personal_facultado ="+veri+"  And ic_producto_nafin = ?");
				mC.setBind(producto);		
				ac.actualizarCatalogo(mC);
				mensaje = "El registro fue Actualizado.";
			}
		}
		
	}catch (Exception e){
		System.err.print("Error: "+e);
		if(accion.equals("agregar")){
			mensaje="No puede crear porque ya existe un registro habilitado para ese intermediario";
		} else if(accion.equals("modificar")){
			mensaje="No se logro Actualizar el registro.";
		}
	}
	jsonObj.put("success",  new Boolean(true));
	jsonObj.put("nuevo",    new Boolean(bandera));
	jsonObj.put("modif",    new Boolean(bandera));
	jsonObj.put("msg",    	mensaje);
	infoRegresar = jsonObj.toString();
  
}

%>
<%=infoRegresar%>