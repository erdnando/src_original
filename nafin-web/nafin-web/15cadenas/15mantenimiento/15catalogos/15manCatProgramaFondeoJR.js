Ext.onReady(function() {
//------------------------------------------------------------------------------
//----------------------------HANDLERS------------------------------------------	
//------------------------------------------------------------------------------
	function procesaAgregaPrograma(opts, success, response) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var mensaje = jsonData.mensaje;
				if(mensaje != ""){
					Ext.getCmp('forma').hide();
					Ext.MessageBox.alert('Mensaje',mensaje);
					consultaDataGrid.load();
				}else{
					Ext.getCmp('forma').hide();
					Ext.MessageBox.alert('Mensaje',mensaje);	
				}
				Ext.getCmp('forma').getForm().reset();
				Ext.getCmp('cmbCatalogos').setValue('103');
			}
		} else {
			Ext.getCmp('forma').getForm().reset();
			Ext.getCmp('forma').hide();
			NE.util.mostrarConnError(response,opts);
		}
	}
	function TransmiteBorrar(opts, success, response) {
		var grid = Ext.getCmp('grid');
		grid.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var mensaje = jsonData.mensaje;
				Ext.MessageBox.alert('Mensaje',mensaje);
				consultaDataGrid.load();
			}
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	var procesarDatos = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');	
		var el = grid.getGridEl();
		el.unmask();
		if (arrRegistros != null) {
			var grid = Ext.getCmp('grid');
			var el = grid.getGridEl();	
			if(!grid.isVisible()){
				grid.show();
			}
			if(store.getTotalCount() > 0) {
				Ext.getCmp('dtnEliminar').enable();
				el.unmask();
			} else {	
				Ext.getCmp('dtnEliminar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}else{
			Ext.getCmp('dtnEliminar').disable();
		}
	}
	var procesarEliminar = function() {
		var grid = Ext.getCmp('grid');
		var seleccionados   = grid.getSelectionModel().getSelections();
 
		var numeroRegistros = seleccionados.length;
		var aviso			  = "";
		
		if(numeroRegistros == 0){
			Ext.MessageBox.alert('Aviso','Debe seleccionar al menos un registro.');
			return;
		} else if(numeroRegistros == 1  ){
			aviso = 'Est� seguro de querer eliminar el registro?';
		} else if(numeroRegistros >  1  ){
			Ext.MessageBox.alert('Aviso','Solo es permitido eliminar un registro.');
			return;
		}
		Ext.Msg.show({
		title: 'Confirmar',
		msg: aviso,
		buttons: Ext.Msg.OKCANCEL,
		fn: function peticionAjax(btn,text,opt){
			if (btn == 'ok'){
				var ids			= [];
				for(var i=0;i<numeroRegistros;i++){
					ids.push(seleccionados[i].get('IC_PROGRAMA_FONDOJR'));
				}
				if(numeroRegistros == 1){
					grid.el.mask("Eliminando Registro...","x-mask-loading");
				} else if( numeroRegistros >  1){
					grid.el.mask("Eliminando Registros...","x-mask-loading");
				}
				Ext.Ajax.request({
					url: '15manCatProgramaFondeoJR.data.jsp',
					params: Ext.apply({
						informacion: 'Eliminar',
						IC_PROGRAMA_FONDOJR:ids,
						numeroRegistros:numeroRegistros
					}),
					callback: TransmiteBorrar
				});
				}
			}
		});
	}
//------------------------------------------------------------------------------
//----------------------------STORES--------------------------------------------	
//------------------------------------------------------------------------------
	var catIntermediario = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15manCatProgramaFondeoJR.data.jsp',
		baseParams: {
			informacion: 'catIntermediario'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
var procesarCatalogo = function(store, arrRegistros, opts) {
	store.each(function(record){
		if(record.get('clave')==45 || record.get('clave')==6  || record.get('clave')==25 || record.get('clave')==101 || record.get('clave')==5 || record.get('clave')==102 || record.get('clave')==19 || record.get('clave')==7 ||
			record.get('clave')==29 ||  record.get('clave')==38 || record.get('clave')==43  || record.get('clave')==18 || record.get('clave')==8 ||
			record.get('clave')==24 || record.get('clave')==15 || record.get('clave')==41 || record.get('clave')==42 || record.get('clave')==17 || record.get('clave')==100 || record.get('clave')==50 || record.get('clave')==1 ||
			record.get('clave')==35 || record.get('clave')==26 || record.get('clave')==3  || record.get('clave')==52 || record.get('clave')==14 || record.get('clave')==270 ||  record.get('clave')==104 ||
			record.get('clave')==20 || record.get('clave')==61 || record.get('clave')==53 || record.get('clave')==10 || record.get('clave')==67 || record.get('clave')==208 || record.get('clave')==23 || record.get('clave')==22  ||
			record.get('clave')==9  || record.get('clave')==16 || record.get('clave')==64 || record.get('clave')==30 || record.get('clave')==48 || record.get('clave')==31  || record.get('clave')==32 || record.get('clave')==46  ||
			record.get('clave')==51 || record.get('clave')==27|| record.get('clave')==6769){
			store.remove(record);
		}
		});
	
	Ext.getCmp('cmbCatalogos').setValue('');	
	if (arrRegistros != null) {
		Ext.getCmp('cmbCatalogos').setValue('103');
		Ext.getCmp('cmbCAT').setValue('103');
	}
}
var tipoCatalogo = new Ext.data.JsonStore({
	id					: 'catalogos',
	root				: 'registros',
	fields			: ['clave','descripcion','loadMsg'], 
	url				: '15manCatProgramaFondeoJR.data.jsp' ,
	baseParams		: {
		informacion	: 'Clasificacion.Carga.Catalogo'
	},
	totalProperty	: 'total',
	autoLoad			: false,
	listeners		: {
		load 			: procesarCatalogo,
		exception	: 	NE.util.mostrarDataProxyError,
		beforeload	: NE.util.initMensajeCargaCombo
	}		
});
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15manCatProgramaFondeoJR.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'IC_PROGRAMA_FONDOJR'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'CG_DESCRIPCION'},
			{name: 'IC_IF'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				  procesarDatos(null, null, null);
				}
			}
		}
	});
//------------------------------------------------------------------------------
//---------------------------ELEMENTOS------------------------------------------	
//------------------------------------------------------------------------------
	var grid = new Ext.grid.GridPanel({
		store: consultaDataGrid,
		sortable: true,
		title				:'Contenido del Cat�logo',
		style: 'margin:0 auto;',
		id: 'grid',
		margins: '20 0 0 0',
		hidden:false,
		columns: [
			{
				header: 'Clave',
				tooltip: 'Clave',
				dataIndex: 'IC_PROGRAMA_FONDOJR',
				sortable: true,
				width: 50,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: '<center>Intermediario Financiero</center>',
				tooltip: 'Intermediario Financiero',
			   dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,
				resizable: true,
				width: 380,
				hidden: false,		
				minChars : 1,
				align: 'left'
			},
			{
				header: '<center>Descripci�n</center>',
				tooltip: 'Descripci�n',
				dataIndex: 'CG_DESCRIPCION',
				fixed:		true,
				sortable: true,
				width: 390,
				align: 'left'
			}
		],
		height			: 450,
		width				: 843,
		enableColumnMove: false,
		enableColumnHide: false,
		frame: true,
		tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
				{
						iconCls	: 'icon-register-edit',
						text		: 'Editar Registro',
						disabled	: true,
						handler	: function(){}
				},{
						iconCls	: 'icon-register-add',
						text		: 'Agregar Registro',
						disabled	: false,
						handler	: function(){
									var formTit	 = Ext.getCmp("forma");
									formTit.setTitle('Editar Cat�logo: Agregar Registro');
									Ext.getCmp('grid').hide();
									Ext.getCmp('forma').show();
						}
			},	{
					ref: 				'../removeBtn',
					iconCls: 		'icon-register-delete',
					text: 			'Eliminar Registro',
					id			: 'dtnEliminar',
					disabled:true,
					handler: 		procesarEliminar
				}
			]
		}
	});
	var elementosForma =  [
		 {
			xtype: 'compositefield',
			fieldLabel: 'Cat�logos',
			combineErrors: false,
			msgTarget: 'side',
			hidden:true,
			items: [
				{
					xtype				: 'combo',
					name				: 'catalogos',
					hiddenName	: 'catalogos',
					id					: 'cmbCatalogos',	
					fieldLabel	: 'Cat�logos', 
					mode				: 'local',	
					forceSelection : true,	
					triggerAction  : 'all',	
					typeAhead		: true,
					minChars 		: 1,	
					store 			: tipoCatalogo,		
					valueField 	: 'clave',
					displayField: 'descripcion',	
					editable		: true,
					typeAhead		: true,
					width:  235,
					listeners		: {
					select		: {
					fn			: function(combo) {
								var valor  = combo.getValue();
							}// FIN fn
					}//FIN select
				}//FIN listeners
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 5
				},
				{
					xtype   : 'button',
					text    : 'Buscar',					
					tooltip :	'Buscar',
					iconCls : 'icoBuscar',
					id      : 'btnBuscar', 
					width:  100,
					handler : function(boton, evento) { 
						var valor = Ext.getCmp('cmbCatalogos').getValue();
						if (valor == 13){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
						} else if(valor == 36){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
						} else if(valor == 21){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
						} else if(valor == 37){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
						} else if(valor == 40){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
						} else if (valor == 47){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
						} else if (valor == 49){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
						}else if (valor == 2){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogoTasaExt01.jsp";
						}else if (valor == 103){
							consultaDataGrid.load();
							Ext.getCmp('forma').hide();
						}else if (valor == 71){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
						}else if (valor == 65){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
						}else if (valor == 68){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
						}else if (valor == 60){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
						}else if (valor == 12){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
						}else if (valor == 33){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
						}else if (valor == 70){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
						}else if (valor == 11){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
						}else if (valor == 63){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
						}else if (valor == 34){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
						}else if (valor == 66){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
						}else if (valor == 62){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
						}else if (valor == 39){
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
						} else if (valor == 677){		//Clasificaci�n Epo
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
						}else {
							window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo.jsp";
						}
					}
				}
			]
		},
		{
			xtype: 'combo',
			fieldLabel: 'IF',
			name: 'claveIF',
			id: 'cmbClaveIF1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'claveIF',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,			
			anchor:'95%',
			store: catIntermediario,
			tpl:'<tpl for=".">' +
				 '<tpl if="!Ext.isEmpty(loadMsg)">'+
				 '<div class="loading-indicator">{loadMsg}</div>'+
				 '</tpl>'+
				 '<tpl if="Ext.isEmpty(loadMsg)">'+
				 '<div class="x-combo-list-item">' +
				 '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
				 '</div></tpl></tpl>'			
		},
		{
			xtype	    : 'textfield',
			id        : 'txfDesc',
			name		 : 'descripcion',
			hiddenName: 'descripcion',
			fieldLabel: 'Descripci�n',
			anchor    : '95%',
			msg       : 'side',
			//width:  235,
			hidden    : false,
			maxLength: 100,
			margins	 : '0 20 0 0'
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 843,
		style: 'margin:0 auto;',
		title				: 'Criterios de B�squeda del Cat�logo',
		frame				: true,		
		collapsible		:true,
		titleCollapse	:true,
		height: 'auto',
		labelWidth: 130,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid:	true,
		buttons: [
			{
				text : 'Guardar',
				id   : 'btnAceptar',
				iconCls :'icoGuardar',
				formBind: true,	
				handler: function (boton, evento){
					var IF = Ext.getCmp('cmbClaveIF1');
					var descrip = Ext.getCmp('txfDesc');
					if(Ext.isEmpty(IF.getValue())){
						IF.markInvalid('Seleccione una EPO por favor');
						return;
					}
					if(Ext.isEmpty(descrip.getValue())){
						descrip.markInvalid('Debe de capturar la Descripci�n');
						return;
					} 
					fp.el.mask('Procesando...', 'x-mask-loading');	
					Ext.Ajax.request({
						url: '15manCatProgramaFondeoJR.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{  
							informacion: "Agregar"
						}),
						callback: procesaAgregaPrograma
					});
				}
			},{
				text : 'Cancelar',
				id   : 'btnLimpiar',
				iconCls :'icoCancelar',
				handler: function (){
					var panel =Ext.getCmp('forma');
					panel.getForm().reset();
					tipoCatalogo.load();
					var formTit	 = Ext.getCmp("forma");
						 formTit.setTitle('Editar Cat�logo: Agregar Registro');
									Ext.getCmp('grid').show();
									Ext.getCmp('forma').hide();

				}
			}
		]
	});
	
		var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		autoHeight		:true,//height:			80,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		'padding:6px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		items: 			[
			 {
				 xtype		: 'combo',
				 name			: 'cmbCatalogos',
				 hiddenName	: '_cmbCat',
				 id			: 'cmbCAT',	
				 fieldLabel	: 'Cat�logo', 
				 mode			: 'local',	
				 forceSelection : true,	
				 triggerAction  : 'all',	
				 typeAhead		: true,
				 minChars 		: 1,	
				 store 			: tipoCatalogo,//catalogo,		
				 valueField 	: 'clave',
				 displayField	: 'descripcion',	
				 editable		: true,
				 typeAhead		: true,
				 anchor			: '95%',
				 listeners		: {
					select		: {
						fn			: function(combo) {
										var valor = Ext.getCmp('cmbCAT').getValue();
										if (valor == 2){					//Tasa
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
										} else if (valor == 11){		//Estatus Documento
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
										} else if (valor == 12){		//Cambio Estatus
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
										} else if (valor == 13){		//Clasificacion ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
										} else if (valor == 21){		//Subsector
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
										} else if (valor == 33){		//D�as Inh�biles
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
										} else if (valor == 34){		//Productos
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
										} else if(valor == 36){			//Personas Facultadas por Banco
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
										} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
										} else if(valor == 39){			//Version Convenio
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
										} else if(valor == 40){			//Bancos TEF
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
										} else if (valor == 47){		//Firma Cedulas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
										} else if (valor == 49){		//Plazos por Producto
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
										} else if (valor == 60){		//Area Promocion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
										} else if (valor == 62){		//Subdireccion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
										} else if (valor == 63){		//Lider Promotor
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
										} else if (valor == 65){		//Subtipo EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
										} else if (valor == 66){		//Sector EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
										} else if (valor == 68){		//Ventanillas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
										} else if (valor == 70){		//D�as Inh�biles por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
										} else if (valor == 71){		//D�as Inh�biles EPO por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
										} else if (valor == 103){
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
										} else if (valor == 677){		//Clasificaci�n Epo
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
										}else {
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo.jsp";
										}
										}// FIN fn
										}//FIN select
									}//FIN listeners
				 }],
		monitorValid: 	false
	});
	
//------------------------------------------------------------------------------
//----------------------------CONTENEDOR PRINCIPAL------------------------------	
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		height: 'auto',
		items: [
			NE.util.getEspaciador(20),	
			panelFormaCatalogos,
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),	
			grid
		]
	});	
//-----------------------------FIN CONTENEDOR PRINCIPAL-------------------------
	
	tipoCatalogo.load();
	catIntermediario.load();
	var grid = Ext.getCmp('grid');	
	var el = grid.getGridEl();
	el.mask('Cargando...', 'x-mask-loading');
	consultaDataGrid.load();
	Ext.getCmp('forma').setVisible(false);
});