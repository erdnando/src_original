<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String infoRegresar="", consulta="",consultaGrid="", mensaje ="";
	ConsCatalogoTasas paginado = new ConsCatalogoTasas();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginado);
	JSONObject jsonObj = new JSONObject();
	HashMap datos = new HashMap();
	JSONArray registros1 = new JSONArray();
	JSONObject jsonObjG = new JSONObject();
	if (informacion.equals("ConsultaGrid")){
		infoRegresar ="";
		try {
				Registros reg	=	queryHelper.doSearch();
				while(reg.next()){
							if(!(reg.getString("MONEDA").equals("MULTIMONEDA"))&&reg.getString("IC_MONEDA").equals("0")){
								reg.setObject("IC_MONEDA","");
								reg.setObject("IC_MONEDA_AUX","");
							}
							if(reg.getString("MONEDA").equals("MULTIMONEDA")&&reg.getString("IC_MONEDA").equals("0")){
								reg.setObject("IC_MONEDA","-1");
								reg.setObject("IC_MONEDA_AUX","-1");
							}
			}
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString();
			} catch(Exception e) {
				throw new AppException("Error al obtener los datos", e);
			}
	}else if(informacion.equals("catalogoMoneda")){
		infoRegresar ="";
		List registros = paginado.catMoneda() ;
		for(int i =0; i<registros.size();i++){
			HashMap aux =(HashMap)registros.get(i);
			datos = new HashMap();
			String clave = aux.get("clave").toString();
			String descripcion = aux.get("descripcion").toString();
			if((descripcion.equals("MULTIMONEDA"))&&clave.equals("0")){
				datos.put("clave", "-1");
				datos.put("descripcion", aux.get("descripcion").toString());
			}else{
				datos.put("clave", aux.get("clave").toString());
				datos.put("descripcion", aux.get("descripcion").toString());
			}
			registros1.add(datos);
		}
		consulta =  "{\"success\": true, \"total\": \"" + registros1.size() + "\", \"registros\": " + registros1.toString()+"}";
		jsonObjG = JSONObject.fromObject(consulta);
		infoRegresar = jsonObjG.toString();
		System.out.println("moneda ++++ "+infoRegresar);
	}else if(informacion.equals("Guardar_Datos")){
		infoRegresar ="";
		JSONObject jsonObj1 = new JSONObject();
		String check = "";
		boolean resultado = false;
		String numeroRegistros  =(request.getParameter("numeroRegistros")!=null) ? request.getParameter("numeroRegistros"):"";
		int i_numReg = Integer.parseInt(numeroRegistros);
		for(int i = 0; i< i_numReg; i++){  
			String chkCEAnt[] = request.getParameterValues("chkCEAnt");
			String listMonedaAnt[] = request.getParameterValues("listMonedaAnt");
			String listDescripAnt[] = request.getParameterValues("listDescripAnt");
			String listDispoAnt[] = request.getParameterValues("listDispoAnt");
			String listTasaAnt[] = request.getParameterValues("listTasaAnt");
			String clave[] = request.getParameterValues("clave");
			String chkCE[] = request.getParameterValues("chkCE");
			String listMoneda[] = request.getParameterValues("listMoneda");
			String listDescrip[] = request.getParameterValues("listDescrip");
			String listDispo[] = request.getParameterValues("listDispo");
			String listTasa[] = request.getParameterValues("listTasa");
			if((!chkCEAnt[i].equals(chkCE[i]))&& listMonedaAnt[i].equals(listMoneda[i])&&listDispoAnt[i].equals(listDispo[i])&&listTasaAnt[i].equals(listTasa[i])&&listDescripAnt[i].equals(listDescrip[i]) ){
				check = "N";
				resultado =	 paginado.actualizarTasas(listMoneda[i], listDescrip[i], listDispo[i], listTasa[i], clave[i], chkCE[i], check);
			}else if( ((!listMonedaAnt[i].equals(listMoneda[i]))||(!listDispoAnt[i].equals(listDispo[i]))||(!listTasaAnt[i].equals(listTasa[i]))||(!listDescripAnt[i].equals(listDescrip[i]))) ){
				resultado =	 paginado.actualizarTasas(listMoneda[i], listDescrip[i], listDispo[i], listTasa[i], clave[i], chkCE[i], check);
			}else{
				mensaje = "No hay cambios";
			}
		}
		if(resultado== true){
			mensaje = "Los registros fueron actualizados.";
		}
		jsonObj1.put("success", new Boolean(true));
		jsonObj1.put("mensaje", mensaje);
		infoRegresar = jsonObj1.toString();
	}
	
	%>	

<%= infoRegresar %>