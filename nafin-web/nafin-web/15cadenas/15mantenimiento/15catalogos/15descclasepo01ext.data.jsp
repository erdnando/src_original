<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		javax.naming.*,
		com.netro.model.catalogos.CatalogosParametrizacion,
		com.netro.descuento.*,
		com.netro.cesion.*,
		com.netro.catalogos.container.CatalogSettingsContainer,
		com.netro.catalogos.vo.Catalogos,
		com.netro.catalogos.dao.util.CatalogFkInfo,
		com.netro.catalogos.dao.QueryCatalog,
		com.netro.catalogos.dao.CatalogosDML,
		com.netro.catalogos.web.ComboItem,
		com.netro.catalogos.vo.Catalogo,
		com.netro.catalogos.vo.Campo,
		com.netro.catalogos.vo.DataSet,
		com.netro.catalogos.vo.DataRow,
		com.netro.catalogos.vo.HardCodedCatalog,
		com.netro.catalogos.parametrizacion.*,
		java.math.BigDecimal"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String perfilCatalogo 	= null;
String informacion 		= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion			= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");
String pageId				= request.getParameter("pageId");

String infoRegresar		= "";

log.debug("informacion = <"+informacion+">");

// DETERMINAR EL PERFIL DEL CATALOGO
String perfilUsuario 	= (String) session.getAttribute("sesPerfil");

// PANTALLA: ADMIN EPO - ADMINISTRACION - PARAMETRIZACION - CATALOGOS
if( "ADMIN EPO".equals(perfilUsuario) && ( "parametros.PEPOPARAM|PARAM".equals(pageId)  || "idMenu.15MANCATALOGO".equals(pageId) ) ){
	perfilCatalogo 		= "PARAMCATADMINEPO";
} else {
	System.err.println("perfilUsuario = <" + perfilUsuario+ ">");
	System.err.println("pageId        = <" + pageId+ ">");
	System.err.println("informacion   = <" + informacion+ ">");
	throw new AppException("Acceso denegado");	
}

if (informacion.equals("CatalogosParametrizacion")) {
	
	//------------------------------------------------------------------	
	// I. CONSULTAR LISTA DE CATALOGOS PARAMETRIZABLES 
	//    EN BASE AL PERFIL DEL USUARIO LOGEADO
	//------------------------------------------------------------------
	
	com.netro.model.catalogos.CatalogosParametrizacion cat = new com.netro.model.catalogos.CatalogosParametrizacion();
	cat.setPerfil(perfilCatalogo);
	
	infoRegresar = cat.getJSONElementos();
	
} else if ( informacion.equals("NombreClasificacionEPO") ){

	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String claveEPO 					= iNoCliente;
	String nombreClasificacionEPO	= null;
	
	// Obtener instancia del EJB de Cesion
	com.netro.cesion.CesionEJB cesion = null;
	try {
		cesion = ServiceLocator.getInstance().lookup("CesionEJB",CesionEJB.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("NombreClasificacionEPO(Exception): Al obtener instancia del EJB de Cesión.");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Cesión.");
	 
	}
	
	// Consultar nombre de la clasificación de la EPO
	nombreClasificacionEPO = cesion.clasificacionEpo(claveEPO);
	
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)		);
	resultado.put("nombreClasificacionEPO", 	nombreClasificacionEPO	);
 
	infoRegresar = resultado.toString();

} else if ( informacion.equals("ActualizarNombreClasificacionEPO")   ) {

	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String claveEPO 					= iNoCliente;
	String nombreClasificacionEPO	= request.getParameter("nombreClasificacionEPO");
	
	nombreClasificacionEPO			= nombreClasificacionEPO == null?"":nombreClasificacionEPO;
	
	// Obtener instancia del EJB de Cesion
	com.netro.cesion.CesionEJB cesion = null;
	try {
		cesion = ServiceLocator.getInstance().lookup("CesionEJB",CesionEJB.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("NombreClasificacionEPO(Exception): Al obtener instancia del EJB de Cesión.");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Cesión.");
	 
	}
	
	// Actualizar nombre de la clasificación EPO
	boolean actualizacionSuccess = cesion.actualizarNombreClasificacionEpo(claveEPO,nombreClasificacionEPO); 
	
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)						);
	resultado.put("actualizacionSuccess", 		new Boolean(actualizacionSuccess)	);
 
	infoRegresar = resultado.toString();
	
} else if ( informacion.equals("ConsultaCatalogo.consultarCatalogo") ) {
	
	//--------------------------------------------------------------------------	
	// II. CONSULTAR EL TIPO DE CATALOGO PARA DETERMINAR LA ACCION QUE PROCEDE
	//--------------------------------------------------------------------------	
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String 		claveCatalogo 										= request.getParameter("claveCatalogo");
	String   	claveCatalogoDefinicionClasificacionEPO	= request.getParameter("claveCatalogoDefinicionClasificacionEPO");
	
   claveCatalogo 										= ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
	if( claveCatalogoDefinicionClasificacionEPO == null || claveCatalogoDefinicionClasificacionEPO.trim().equals("")){
		throw new AppException("La clave del catalogo de la pantalla actual es requerida.");
	}
	
   boolean		sinClaveCatalogo 	= "0".equals(claveCatalogo)?true:false;
   
	// Obtener instancia del EJB de Catalogos Parametrizacion
	com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
	try {
				
		//CatalogosParametrizacionHome 	catalogosParametrizacionHome 	= null;
		//Context 								context 								= ContextoJNDI.getInitialContext();
		
		//catalogosParametrizacionHome 	= (CatalogosParametrizacionHome) context.lookup("CatalogosParametrizacionEJB");
		//catalogosParametrizacion 		= catalogosParametrizacionHome.create();
		
		catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("ConsultaCatalogo.consultarCatalogo(Exception): Obtener instancia del EJB de Catálogos Parametrización");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
	 
	}

	// Determinar el estado siguiente
	boolean redirigir 			= false;
	String  estadoSiguiente   	= null;
	
	// No se especifico la clave del catalogo	
	if( "0".equals(claveCatalogo) ) {
		
		redirigir 			= false;
		estadoSiguiente   = "ESPERAR_DECISION"; //"FIN";
		
	// Se trata del catalogo de la pantalla actual, se procederá a mostrar su contenido
	} else if ( claveCatalogoDefinicionClasificacionEPO.equals(claveCatalogo)  ){
		
		redirigir 			= false;
		estadoSiguiente   = "CARGAR_CATALOGO";
		
	// Se trata de un catalogo externo
	} else if( catalogosParametrizacion.esCatalogoExterno(claveCatalogo, perfilCatalogo) ) {
	
		redirigir 			= true;
		estadoSiguiente   = "FIN";
		
	// Se trata de un catalogo parametrizado por lo que se procederá a mostrar su contenido
	} else if( !claveCatalogoDefinicionClasificacionEPO.equals(claveCatalogo) ){ 
		
		redirigir 			= true;
		estadoSiguiente   = "FIN";
 
	}
 
	// Enviar la clave del catalogo
	resultado.put("claveCatalogo", 		claveCatalogo				);
	// Especificar si se redirige a otra pagina
	resultado.put("redirigir", 			new Boolean(redirigir)	);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 	estadoSiguiente			);
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)		);
 
	infoRegresar = resultado.toString();
	
} else if ( informacion.equals("ConsultaCatalogo.fin") ) {
	
	//------------------------------------------------------------------	
	// V. DETERMINAR EL TIPO DE CATALOGO
	//------------------------------------------------------------------	
 
	String 		claveCatalogo 					= request.getParameter("claveCatalogo");
	String 		redirigir 						= (request.getParameter("redirigir") == null)?"false":request.getParameter("redirigir");
	
   claveCatalogo = ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
 
	boolean		redirigirAOtraPagina			= "true".equals(redirigir)?true:false;
	boolean  	hayClaveCatalogo 				= claveCatalogo != null && !claveCatalogo.trim().equals("")?true:false;
	boolean 		enviarClaveCatalogo			= hayClaveCatalogo;
	
	String 		paginaDestino 					= "15descclasepo01ext.jsp"; 
	String		extensionArchivo				= null;
	
	// Se redirigen a todos los catalogos diferentes del definido en esta página: Catálogo Descripción de la Clasificación EPO
	if( redirigirAOtraPagina && hayClaveCatalogo ){
 
		try {
					
			// Obtener instancia del EJB de Catalogos Parametrizacion
			com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
		
			//CatalogosParametrizacionHome 	catalogosParametrizacionHome 	= null;
			//Context 								context 								= ContextoJNDI.getInitialContext();
			
			//catalogosParametrizacionHome 	= (CatalogosParametrizacionHome) context.lookup("CatalogosParametrizacionEJB");
			//catalogosParametrizacion 		= catalogosParametrizacionHome.create();
			
			catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
			
			// El catalogo corresponde a una implementación especifica, que no tiene nada que ver con la implementacion
			// local de los catalogos, como se encuentra definida en: 15mancatalogo01ext 
			if( catalogosParametrizacion.esCatalogoExterno(claveCatalogo, perfilCatalogo) ){
 
				paginaDestino 	  = catalogosParametrizacion.getNombreArchivoCatalogo(claveCatalogo, perfilCatalogo);
				// Obtener extension del archivo
				extensionArchivo = paginaDestino != null && paginaDestino.length() > 3 ?paginaDestino.substring(paginaDestino.length()-3):"";
				
				if("xml".equalsIgnoreCase(extensionArchivo)){
					throw new AppException("El archivo de redirección no puede ser de tipo xml");
				}
				
			// Si bien el catalogo no es externo, es decir se comforma a las reglas del framework local del catalogo
			// es necesario redirigir a la pantalla: 15mancatalogo01ext para su despliege; esto se hace con 
			// el proposito de evitar duplicar la funcionalidad en este jsp
			} else {
				
				paginaDestino 		= "15mancatalogo01ext.jsp";
				
			} 
			
		} catch(Exception e) {
		 
			log.error("ConsultaCatalogo.fin(Exception): Obtener instancia del EJB de Catálogos Parametrización");
			log.error("ConsultaCatalogo.fin::claveCatalogo = <" + claveCatalogo + ">");
			log.error("ConsultaCatalogo.fin::redirigir     = <" + redirigir     + ">");
			e.printStackTrace();
			
			paginaDestino 						= "15mancatalogo01ext.jsp";
			// En el caso del reenvío se suprime el lanzamiento de excepciones
			//throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
			// Nota: En la versión actual no se tiene soporte para manejar errores en un submit
		   enviarClaveCatalogo				= false;

		}
 
	}

	response.sendRedirect(paginaDestino+"?parametros="+pageId+(enviarClaveCatalogo?"&claveCatalogo="+claveCatalogo:""));
	
} else {
	throw new AppException("La acción: " + informacion + " no se encuentra registrada");
}

log.debug("infoRegresar = <" + infoRegresar + ">"); 
%>
<%=infoRegresar%>