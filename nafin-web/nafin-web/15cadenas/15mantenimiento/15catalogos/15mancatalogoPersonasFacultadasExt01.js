Ext.onReady(function() {


//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  
var fp = Ext.getCmp('forma');
  fp.el.unmask();							
  var gridConsulta = Ext.getCmp('grid');	
  var el = gridConsulta.getGridEl();		
  if (arrRegistros != null) {
    if (!gridConsulta.isVisible()) {
      gridConsulta.show();
      }				
    if(store.getTotalCount() > 0) {		
      el.unmask();
    } else {		
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
};
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
var consultaData   = new Ext.data.JsonStore({ 
	root 	: 'registros',
	url 	: '15mancatalogoPersonasFacultadasExt01.data.jsp',
	baseParams: {
		informacion: 'Consultar'
	},		
	fields: [	
		{	name: 'CG_RAZON_SOCIAL'},
		{	name: 'CG_AP_PATERNO'},
		{	name: 'CG_AP_MATERNO'},
		{	name: 'CG_NOMBRE'},
		{	name: 'CG_PUESTO'},
		{	name: 'CS_HABILITADO'},
		{	name: 'IC_IF'},
		{	name: 'IC_PERSONAL_FACULTADO'},
		{	name: 'IC_NOMBRE'},
		{	name: 'IC_PRODUCTO_NAFIN'}
	],		
	totalProperty : 'total',
	messageProperty: 'msg',
	autoLoad: true,
	listeners: {
		load: procesarConsultaData,
		exception: {
			fn: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				//LLama procesar consulta, para que desbloquee los componentes.
				procesarConsultaData(null, null, null);					
			}
		}
	}		
});
//----------------------------Fin ConsultaData----------------------------------

//---------------------------------radiosHabilitado-------------------------------
	var radiosHabilitado = [{
		xtype:      'radio', 
		id:         's',
		boxLabel:   'Si',
		name:       'rbGroupHab',
		inputValue: 'S'
	},{
		xtype:      'radio', 
		id:         'n',
		boxLabel:   'No',
		name:       'rbGroupHab',
		inputValue: 'N'
	}];
//---------------------------------radiosHabilitado------------------------------

//----------------------------Fin procesarProductos-------------------------------
var procesarProductos = function(store, arrRegistros, opts) {
	Ext.getCmp('comboProducto').setValue('1')
	store.commitChanges(); 
}	
//----------------------------Fin procesarProductos-------------------------------

//------------------------------------If----------------------------------------
var If = Ext.data.Record.create([ 
 {name: "clave", type: "string"}, 
 {name: "descripcion", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//---------------------------------Fin If---------------------------------------

//---------------------------- procesarIf---------------------------------------
var procesarIf = function(store, arrRegistros, opts) {
	store.insert(0,new If({ 
	clave: "", 
	descripcion: "Seleccionar", 
	loadMsg: ""
 })); 
 
 Ext.getCmp('ic_if1').setValue("");
 store.commitChanges(); 
};	
//----------------------------Fin procesarIf------------------------------------

//----------------------------Fin procesarCatalogo--------------------------------
var procesarCatalogo = function(store, arrRegistros, opts) {
	store.each(function(record){
		if(record.get('clave')==45 || record.get('clave')==6  || record.get('clave')==25 || record.get('clave')==101 || record.get('clave')==5 || record.get('clave')==102 || record.get('clave')==19 || record.get('clave')==7 ||
			record.get('clave')==29 ||  record.get('clave')==38 || record.get('clave')==43  || record.get('clave')==18 || record.get('clave')==8 ||
			record.get('clave')==24 || record.get('clave')==15 || record.get('clave')==41 || record.get('clave')==42 || record.get('clave')==17 || record.get('clave')==100 || record.get('clave')==50 || record.get('clave')==1 ||
			record.get('clave')==35 || record.get('clave')==26 || record.get('clave')==3  || record.get('clave')==52 || record.get('clave')==14 || record.get('clave')==270 ||  record.get('clave')==104 ||
			record.get('clave')==20 || record.get('clave')==61 || record.get('clave')==53 || record.get('clave')==10 || record.get('clave')==67 || record.get('clave')==208 || record.get('clave')==23 || record.get('clave')==22  ||
			record.get('clave')==9  || record.get('clave')==16 || record.get('clave')==64 || record.get('clave')==30 || record.get('clave')==48 || record.get('clave')==31  || record.get('clave')==32 || record.get('clave')==46  ||
			record.get('clave')==51 || record.get('clave')==27|| record.get('clave')==6769){
			store.remove(record);
		}
		});
	Ext.getCmp('cmbCAT').setValue('36');
	store.commitChanges(); 
}	
//----------------------------Fin procesarCatalogo--------------------------------

//--------------------------Store catalogo----------------------------
var catalogo = new Ext.data.JsonStore({
	id					: 'catalogos',
	root				: 'registros',
	fields			: ['clave','descripcion','loadMsg'], 
	url				: '15mancatalogoClasificacion01Ext.data.jsp' ,
	baseParams		: {
		informacion	: 'Clasificacion.Carga.Catalogo'
	},
	totalProperty	: 'total',
	autoLoad			: true,
	listeners		: {
		load 			: procesarCatalogo,
		exception	: 	NE.util.mostrarDataProxyError,
		beforeload	: NE.util.initMensajeCargaCombo
	}		
});
//--------------------------Fin Store catalogo----------------------------

//------------------------------Catalogo IF-------------------------------------
var catalogoIF 	= new Ext.data.JsonStore({
	id					: 'catalogoIF',
	root				: 'registros',
	fields			: ['clave','descripcion','loadMsg'],
	url				: '15mancatalogoPersonasFacultadasExt01.data.jsp',
	baseParams		: {
		informacion	: 'catalogoif'
	},
	totalProperty	: 'total',
	autoLoad			: true,
	listeners		: {	
		load			: procesarIf,
		exception	: NE.util.mostrarDataProxyError,
		beforeload	: NE.util.initMensajeCargaCombo
	}		
});
//------------------------------Fin Catalogo IF---------------------------------

//------------------------------ store productos --------------------------------
var productos = new Ext.data.JsonStore({
	id		: 'storeProductos',
	root	: 'registros',
	fields: ['clave','descripcion','loadMsg'],
	url	: '15mancatalogoPersonasFacultadasExt01.data.jsp',
	baseParams: {
		informacion: 'productos'
	},
	totalProperty	: 'total',
	autoLoad			: true,
	listeners		: {	
		load			: procesarProductos,
		exception	: NE.util.mostrarDataProxyError,
		beforeload	: NE.util.initMensajeCargaCombo
	}		
});
//------------------------------Fin store productos -----------------------------

//-------------------------------elementosForma---------------------------------	
	var elementosForma = [{
		xtype:            'compositefield',
		id:               'cmpCB',
		combineErrors:    false,
		msgTarget:        'side',
		hidden:           true,
		items: [{
			xtype:         'combo',
			name:          'cmbCatalogos',
			hiddenName:    '_cmbCat',
			id:            'cmbCATHIDEN',
			fieldLabel:    'Cat�logos',
			mode:          'local',
			forceSelection:true,
			triggerAction: 'all',
			typeAhead:     true,
			minChars:      1,
			store:         catalogo,
			valueField:    'clave',
			displayField:  'descripcion',
			editable:      true,
			typeAhead:     true,
			width:         337,
			listeners: {
				select: {
					fn: function(combo) {
						var valor = combo.getValue();
					}// FIN fn
				}//FIN select
			}//FIN listeners
		},{
			xtype:         'displayfield',
			frame:         true,
			border:        false,
			value:         '',
			width:         10
		},{
			xtype:         'button',
			text:          'Buscar',
			tooltip:       'Buscar',
			iconCls:       'icoBuscar',
			id:            'btnBuscar', 
			handler: function(boton, evento) {
				var valor = Ext.getCmp('cmbCAT').getValue();
				if (valor == 2){ //Tasa
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
				} else if (valor == 11){ //Estatus Documento
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
				} else if (valor == 12){ //Cambio Estatus
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
				} else if (valor == 13){ //Clasificacion ****No disponible**
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
				} else if (valor == 21){ //Subsector
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
				} else if (valor == 33){ //D�as Inh�biles
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
				} else if (valor == 34){ //Productos
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
				} else if(valor == 36){ //Personas Facultadas por Banco
					consultaData.load();
					//window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
				} else if(valor == 37){ //D�as Inh�biles EPO ****No disponible**
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
				} else if(valor == 39){ //Version Convenio
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
				} else if(valor == 40){ //Bancos TEF
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
				} else if (valor == 47){ //Firma Cedulas
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
				} else if (valor == 49){ //Plazos por Producto
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
				} else if (valor == 60){ //Area Promocion
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
				} else if (valor == 62){ //Subdireccion
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
				} else if (valor == 63){ //Lider Promotor
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
				} else if (valor == 65){ //Subtipo EPO
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
				} else if (valor == 66){ //Sector EPO
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
				} else if (valor == 68){ //Ventanillas
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
				} else if (valor == 70){ //D�as Inh�biles por A�o
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
				}else if (valor == 71){		//D�as Inh�biles EPO por A�o
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
				} else if (valor == 677){		//Clasificaci�n Epo
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
				}else if (valor == 103){		//Programa a Fondo JR
					window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
				}
			}
		}]
	},{
		xtype:            'combo',
		fieldLabel:       'IF',
		name:             'ic_if',
		id:               'ic_if1',
		mode:             'local',
		displayField:     'descripcion',
		valueField:       'clave',
		hiddenName:       'ic_if',
		autoLoad:         false,
		forceSelection:   true,
		triggerAction:    'all',
		typeAhead:        true,
		minChars:         1,
		store:            catalogoIF,
		tpl:              NE.util.templateMensajeCargaComboConDescripcionCompleta,
		anchor:           '95%'
	},{
		xtype:            'combo',
		fieldLabel:       'Producto',
		name:             'producto',
		id:               'comboProducto',
		mode:             'local',
		displayField:     'descripcion',
		valueField:       'clave',
		hiddenName:       'comboProductoHidden',
		autoLoad:         true,
		forceSelection:   true,
		triggerAction:    'all',
		typeAhead:        true,
		minChars:         1,
		store:            productos,
		anchor:           '95%'
	},{
		layout:           'column',
		id:               'cmpAPE',
		items:[{
			columnWidth:   .5,
			width:         '50%',
			layout:        'form',
			items: [{
				xtype:      'textfield',
				fieldLabel: 'Apellido Paterno',
				id:         'tfApaterno',
				name:       'tfPat',
				msgTarget:  'side',
				anchor:     '95%',
				maxLength:  25
			}]
		},{
			columnWidth:   .5,
			width:         '50%',
			layout:        'form',
			items: [{
				xtype:      'textfield',
				fieldLabel: '&nbsp;&nbsp; Apellido Materno',
				id:         'tfAmaterno',
				name:       'tfMat',
				msgTarget:  'side',
				anchor:     '95%',
				maxLength:  25
			}]
		}]
	},{
		layout:           'column',
		id:               'cmpNP',
		items:[{
			columnWidth:   .5,
			width:         '50%',
			layout:        'form',
			items: [{
				xtype:      'textfield',
				fieldLabel: 'Nombre',
				id:         'tfNombre',
				name:       'tfNom',
				msgTarget:  'side',
				anchor:     '95%',
				maxLength:  80
			}]
		},{
			columnWidth:   .5,
			width:         '50%',
			layout:        'form',
			items: [{
				xtype:      'textfield',
				fieldLabel: '&nbsp;&nbsp; Puesto',
				id:         'tfPuesto',
				name:       'tfPues',
				msgTarget:  'side',
				anchor:     '95%',
				maxLength:  50
			}]
		}]
	},{
		xtype:            'radiogroup',
		id:               'radioGroupHabilitado',
		fieldLabel:       'Habilitado',
		msgTarget:        'side',
		anchor:           '55%',
		align:            'center',
		cls:              'x-check-group-alt',
		style:            { width: '50%'},
		allowBlank:       true,
		items:            radiosHabilitado
	},{
		xtype:            'compositefield',
		id:               'ayuda',
		combineErrors:    false,
		msgTarget:        'side',
		hidden:           true,
		items: [{
			xtype:         'textfield',
			id:            'tfAyuda',
			name:          'accion',
			fieldLabel:    'accion',
			value:         1,
			width:         50,
			margins:       '0 20 0 0'
		},{
			xtype:         'displayfield',
			frame:         true,
			border:        false,
			value:         'filagrid:',
			width:         50
		},{
			xtype:         'textfield',
			id:            'filagrid',
			name:          '_fila',
			width:         50,
			margins:       '0 20 0 0'
		},{
			xtype:         'displayfield',
			frame:         true,
			border:        false,
			value:         'facultado:',
			width:         50
		},{
			xtype:         'textfield',
			id:            'icFacultado',
			name:          '_icFacultado',
			width:         50,
			margins:       '0 20 0 0'
		}]
	}];
//-----------------------------Fin elementosForma-------------------------------

//--------------------------------gridConsulta----------------------------------	
var gridConsulta = new Ext.grid.GridPanel({	
	store				:consultaData,
	id					:'grid',
	margins			:'20 0 0 0',		
	style				:'margin:0 auto;',
	title				:'Contenido del Cat�logo',	
	hidden			:false,			
	displayInfo		: true,		
	emptyMsg			: "No hay registros.",		
	loadMask			: true,
	stripeRows		: true,
	height			: 450,
	width				: 843,
	enableColumnMove: false,
	enableColumnHide: false,
	align				: 'center',
	frame				: true,
	listeners 		: {
		cellclick	: function(grid, rowIndex, columnIndex, e) {
							Ext.getCmp('filagrid').setValue(rowIndex)
		}
   },
	tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
					{
					iconCls	: 'icon-register-edit',
					text		: 'Editar Registro',
					disabled	: false,
					handler	: function(){
										var grid	  				= Ext.getCmp("grid");
										var seleccionados  	= grid.getSelectionModel().getSelections();
										
										if(       seleccionados.length == 0){
											Ext.MessageBox.alert('Aviso','Debe seleccionar un registro.');
											return;
										} else if( seleccionados.length >  1){
											Ext.MessageBox.alert('Aviso','S�lo se puede editar un registro a la vez.');
											return;
										}
										var formTit	  				= Ext.getCmp("forma");
										formTit.setTitle('Editar Cat�logo: Modificar Registro');
										var respuesta 			= new Object();
										respuesta['record']	= seleccionados[0];										
										//Ext.getCmp('tfVentanilla').setValue(respuesta.record.data.CG_DESCRIPCION);
										//Ext.getCmp('filagrid').setValue(rowIndex);
										Ext.getCmp('tfAyuda').setValue(2);							
										
										Ext.getCmp('icFacultado').setValue(respuesta.record.data.IC_PERSONAL_FACULTADO); 
										Ext.getCmp('ic_if1').setValue(respuesta.record.data.IC_IF);
										Ext.getCmp('comboProducto').setValue(respuesta.record.data.IC_PRODUCTO_NAFIN);
										Ext.getCmp('tfApaterno').setValue(respuesta.record.data.CG_AP_PATERNO);
										Ext.getCmp('tfAmaterno').setValue(respuesta.record.data.CG_AP_MATERNO);
										Ext.getCmp('tfNombre').setValue(respuesta.record.data.CG_NOMBRE);
										Ext.getCmp('tfPuesto').setValue(respuesta.record.data.CG_PUESTO);
										if(respuesta.record.data.CS_HABILITADO =='S' ){
											Ext.getCmp('s').setValue(true);
										} else if(respuesta.record.data.CS_HABILITADO =='N' ){	
											Ext.getCmp('n').setValue(true);
										} else {
											Ext.getCmp('n').setValue(false);
											Ext.getCmp('s').setValue(false);
										}
										Ext.getCmp('grid').hide();
										Ext.getCmp('forma').show();
					}
					},{
					iconCls	: 'icon-register-add',
					text		: 'Agregar Registro',
					disabled	: false,
					handler	: function (){
									var formTit	  				= Ext.getCmp("forma");
									formTit.setTitle('Editar Cat�logo: Agregar Registro');
									Ext.getCmp('forma').show();	
									Ext.getCmp('grid').hide();
									
						}
					},{
					iconCls	: 'icon-register-delete',
					text		: 'Eliminar Registro',
					disabled	: true,
					handler	: function(){}
					}					
			]
	},
	columns			:[
    {
    header			:'IF',
    tooltip			:'IF',
    dataIndex		:'CG_RAZON_SOCIAL',
    sortable		:true,
    width			:265,			
    resizable		:true,
    align			:'center',
    renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    },{
    header			:'Producto',
    tooltip			:'Producto',
    dataIndex		:'IC_NOMBRE',
    sortable		:true,
    width			:130,			
    resizable		:true,
    align			:'center',
	 renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    },{
    header			:'Apellido Paterno',
    tooltip			:'Apellido Paterno',
    dataIndex		:'CG_AP_PATERNO',
    sortable		:true,
    width			:100,			
    resizable		:true,
    align			:'center',
	 renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    },{
    header			:'Apellido Materno',
    tooltip			:'Apellido Materno',
    dataIndex		:'CG_AP_MATERNO',
    sortable		:true,
    width			:100,			
    resizable		:true,
    align			:'center',
	 renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    },{
    header			:'Nombre',
    tooltip			:'Nombre',
    dataIndex		:'CG_NOMBRE',
    sortable		:true,
    width			:100,			
    resizable		:true,
    align			:'center',
	 renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    },{
    header			:'Puesto',
    tooltip			:'Puesto',
    dataIndex		:'CG_PUESTO',
    sortable		:true,
    width			:200,			
    resizable		:true,
    align			:'center',
	 renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    },{
    header			:'Habilitado ',
    tooltip			:'Habilitado ',
    dataIndex		:'CS_HABILITADO',
    sortable		:true,
    width			:70,			
    resizable		:true,
    align			:'center',
    renderer		:function(value){
      return 		"<div align='left'>"+value+"</div>";
      }
    },{
    xtype			: 'actioncolumn',
    header			: 'Seleccionar',
    tooltip			: 'Seleccionar',	
	 hidden			: true,
    width			: 70,							
    align			: 'center',
    items			: [
     {
      getClass		: function(valor, metadata, registro, rowIndex, colIndex, store) {
							  this.items[0].tooltip = 'Modificar';
							  return 'icoModificar';										
							},
      handler		: function(grid, rowIndex, colIndex, item, event){
							Ext.getCmp('filagrid').setValue(rowIndex);
							Ext.getCmp('tfAyuda').setValue(2);							
							var registro = grid.getStore().getAt(rowIndex);
							Ext.getCmp('icFacultado').setValue(registro.get('IC_PERSONAL_FACULTADO')); 
							Ext.getCmp('ic_if1').setValue(registro.get('IC_IF'));
							Ext.getCmp('comboProducto').setValue(registro.get('IC_PRODUCTO_NAFIN'));
							Ext.getCmp('tfApaterno').setValue(registro.get('CG_AP_PATERNO'));
							Ext.getCmp('tfAmaterno').setValue(registro.get('CG_AP_MATERNO'));
							Ext.getCmp('tfNombre').setValue(registro.get('CG_NOMBRE'));
							Ext.getCmp('tfPuesto').setValue(registro.get('CG_PUESTO'));
							if(registro.get('CS_HABILITADO') =='S' ){
								Ext.getCmp('s').setValue(true);
							}else if(registro.get('CS_HABILITADO') =='N' ){	
								Ext.getCmp('n').setValue(true);
							} else {
								Ext.getCmp('n').setValue(false);
								Ext.getCmp('s').setValue(false);
							}
					}//fin handler
    } ]
	}   
]
});	
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------Panel Consulta---------------------------------
var fp = new Ext.form.FormPanel({
	id					: 'forma',
	width				: 843,
	title				: 'Criterios de B�squeda del Cat�logo',
	frame				: true,		
	collapsible		: true,
	titleCollapse	: true,
	style				: 'margin:0 auto;',
	bodyStyle		: 'padding: 6px',
	labelWidth		: 	130,
	defaults			: {
							msgTarget: 'side',
							anchor: '-20'
						},
	items				: elementosForma,
	buttons			: [
		{
		text			: 'Guardar',
		id				: 'btnAceptar',
		iconCls		: 'icoGuardar',
		handler		: function (boton, evento){
		var help = Ext.getCmp('tfAyuda').getValue();
		 if(Ext.getCmp('tfApaterno').isValid() & Ext.getCmp('tfAmaterno').isValid() & Ext.getCmp('tfNombre').isValid() & Ext.getCmp('tfPuesto').isValid() ){				
		 if ( help == 1 ){//nuevo
		 if (  Ext.getCmp('ic_if1').getValue() !="" && Ext.getCmp('comboProducto').getValue() !="" &&  Ext.getCmp('tfApaterno').getValue()!="" && Ext.getCmp('tfAmaterno').getValue()!="" && Ext.getCmp('tfNombre').getValue()!="" && Ext.getCmp('tfPuesto').getValue() != ""  ){
				Ext.Ajax.request({
				url: '15mancatalogoPersonasFacultadasExt01.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
				  informacion	: 'Nuevo',
				  accion			: 'agregar'
				}),
				success : function(response,b) {
				 var info = Ext.util.JSON.decode(response.responseText);
				 fp.getForm().reset();
				 Ext.getCmp('cmbCAT').setValue(36);
				 Ext.getCmp('ic_if1').setValue('');
				 Ext.getCmp('comboProducto').setValue(1);
				 if (info.nuevo){
					 Ext.getCmp('forma').hide();
					 Ext.Msg.show({
					  title: 'Nuevo',
					  msg: info.msg,
					  modal: true,
					  icon: Ext.Msg.INFO,
					  buttons: Ext.Msg.OK,
					  fn: function (){
					  fp.el.mask('Cargando...', 'x-mask-loading');			
					  consultaData.load();
						 }
					  });//fin Msg
				  } else {
					  Ext.getCmp('forma').hide();
					  Ext.Msg.show({
						  title: 'Nuevo',
						  msg: info.msg,
						  modal: true,
						  icon: Ext.Msg.ERROR,
						  buttons: Ext.Msg.OK,
						  fn: function (){
						  fp.el.mask('Cargando...', 'x-mask-loading');			
						  consultaData.load();
							 }
						  });//fin Msg
				  }
				 }//fin success
				});//fin Ajax 
		 
		 } else {
		  Ext.Msg.show({
			title: 'Faltan Datos',
			msg: 'Faltan Datos',
			modal: true,
			icon: Ext.Msg.WARNING,
			buttons: Ext.Msg.OK,
			fn: function (){
			  }
			}); 
		 }//fin else
		 }//fin agregar
		 
		 if (help == 2 ){//actualiza
		 if (  Ext.getCmp('ic_if1').getValue() !=""  &&  Ext.getCmp('tfApaterno').getValue()!="" && Ext.getCmp('tfAmaterno').getValue()!="" && Ext.getCmp('tfNombre').getValue()!="" && Ext.getCmp('tfPuesto').getValue() != ""  ){
				if ( Ext.getCmp('comboProducto').getValue() ==""){
					Ext.getCmp('comboProducto').focus(false,true);
				} else {
		  var registro = gridConsulta.getStore().getAt(Ext.getCmp('filagrid').getValue());
		  Ext.Ajax.request({
			url: '15mancatalogoPersonasFacultadasExt01.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
			  informacion	: 'Nuevo',
			  accion			: 'modificar'
			}),
			success : function(response,b) {
			 var info = Ext.util.JSON.decode(response.responseText);
			 fp.getForm().reset();
			 Ext.getCmp('cmbCAT').setValue(36);
			 Ext.getCmp('ic_if1').setValue('');
			 Ext.getCmp('comboProducto').setValue(1);
			 if (info.modif){
				  Ext.getCmp('forma').hide();
  				  Ext.Msg.show({
				  title: 'Actualiza.',
				  msg: info.msg,
				  modal: true,
				  icon: Ext.Msg.INFO,
				  buttons: Ext.Msg.OK,
				  fn: function (){
				  fp.el.mask('Cargando...', 'x-mask-loading');			
				  consultaData.load();
					 }
				  });//fin Msg
			  } else {
				  Ext.getCmp('forma').hide();
				  Ext.Msg.show({
					  title: 'Actualiza.',
					  msg: info.msg,
					  modal: true,
					  icon: Ext.Msg.ERROR,
					  buttons: Ext.Msg.OK,
					  fn: function (){
					  fp.el.mask('Cargando...', 'x-mask-loading');			
					  consultaData.load();
						 }
					  });//fin Msg
					}
				}//fin success
			 });//fin Ajax
			 }
		 } else {
			 Ext.Msg.show({
				title: 'Faltan Datos',
				msg: 'Faltan Datos',
				modal: true,
				icon: Ext.Msg.WARNING,
				buttons: Ext.Msg.OK,
				fn: function (){
				  }
				});
		   }
		  }//fin actualizar
		 } else {
			if(!Ext.getCmp('tfApaterno').isValid()){
				Ext.getCmp('tfApaterno').focus();
			}	else if (!Ext.getCmp('tfAmaterno').isValid()){
				Ext.getCmp('tfAmaterno').focus();
			} else if (!Ext.getCmp('tfNombre').isValid()){
				Ext.getCmp('tfNombre').focus();
			} else if (!Ext.getCmp('tfPuesto').isValid() ){
				Ext.getCmp('tfPuesto').focus();
			}
		 }
		 } // FIN handler
		},{
		text			: 'Cancelar',
		id				: 'btnLimpiar',
		iconCls		: 'icoCancelar',
		handler		: function (boton, evento){
			 fp.getForm().reset();
			 Ext.getCmp('cmbCAT').setValue(36);
			 Ext.getCmp('ic_if1').setValue('');
			 Ext.getCmp('comboProducto').setValue(1);
			 Ext.getCmp('forma').hide();
			 Ext.getCmp('grid').show();
		  }//FIN handler Limpiar
		} ]
});
//------------------------------Fin Panel Consulta------------------------------

	var panelFormaCatalogos =  new Ext.form.FormPanel({
		id: 				'panelFormaCatalogos',
		title: 			'Cat�logos',
		width: 			843,
		autoHeight:true,//height:			80,
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		hidden:			false,
		bodyStyle:		'padding:6px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		items: 			[
			 {
				 xtype		: 'combo',
				 name			: 'cmbCatalogos',
				 hiddenName	: '_cmbCat',
				 id			: 'cmbCAT',	
				 fieldLabel	: 'Cat�logo', 
				 mode			: 'local',	
				 forceSelection : true,	
				 triggerAction  : 'all',	
				 typeAhead		: true,
				 minChars 		: 1,	
				 store 			: catalogo,		
				 valueField 	: 'clave',
				 displayField	: 'descripcion',	
				 editable		: true,
				 typeAhead		: true,
				 anchor			: '95%',
				 listeners		: {
					select		: {
						fn			: function(combo) {
										var valor = Ext.getCmp('cmbCAT').getValue();
										if (valor == 2){					//Tasa
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatalogosTasaExt01.jsp";
										} else if (valor == 11){		//Estatus Documento
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=11";
										} else if (valor == 12){		//Cambio Estatus
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=12";
										} else if (valor == 13){		//Clasificacion ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacion01Ext.jsp";
										} else if (valor == 21){		//Subsector
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubsector01Ext.jsp";
										} else if (valor == 33){		//D�as Inh�biles
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=33";
										} else if (valor == 34){		//Productos
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=34";
										} else if(valor == 36){			//Personas Facultadas por Banco
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPersonasFacultadasExt01.jsp";
										} else if(valor == 37){			//D�as Inh�biles EPO ****No disponible**
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOExt01.jsp";
										} else if(valor == 39){			//Version Convenio
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=39";
										} else if(valor == 40){			//Bancos TEF
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoBancosTefExt01.jsp";
										} else if (valor == 47){		//Firma Cedulas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoExt01.jsp";
										} else if (valor == 49){		//Plazos por Producto
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoPlazosProductoExt01.jsp";
										} else if (valor == 60){		//Area Promocion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=60";
										} else if (valor == 62){		//Subdireccion
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=62";
										} else if (valor == 63){		//Lider Promotor
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=63";
										} else if (valor == 65){		//Subtipo EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoSubtipoEPOExt01.jsp";
										} else if (valor == 66){		//Sector EPO
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=66";
										} else if (valor == 68){		//Ventanillas
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoVentanillaExt01.jsp";
										} else if (valor == 70){		//D�as Inh�biles por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogo01ext.jsp?idMenu=15MANCATALOGO&claveCatalogo=70";
										}else if (valor == 71){		//D�as Inh�biles EPO por A�o
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoDiasInhabilEPOAnioExt01.jsp";
										}else if (valor == 677){		//Clasificaci�n Epo
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15mancatalogoClasificacionEpo01Ext.jsp";
										} else if (valor == 103){		//Programa a Fondo JR
											window.location = "/nafin/15cadenas/15mantenimiento/15catalogos/15manCatProgramaFondeoJR.jsp";
										}
										}// FIN fn
										}//FIN select
									}//FIN listeners
				 }],
		monitorValid: 	false
	});


//----------------------------Contenedor Principal------------------------------	
var pnl = new Ext.Container({
	id			: 'contenedorPrincipal',
	applyTo	: 'areaContenido',
	width		: 949,		
	//style		: 'margin:0 auto;',
	items		: [
					NE.util.getEspaciador(20),	
					panelFormaCatalogos,
					NE.util.getEspaciador(20),	
					fp,
					NE.util.getEspaciador(20),
					gridConsulta,
					NE.util.getEspaciador(20)
					]
});	
//-----------------------------Fin Contenedor Principal-------------------------
Ext.getCmp('forma').setVisible(false);
});//-----------------------------------------------Fin Ext.onReady(function(){}