<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		javax.naming.*,
		com.netro.model.catalogos.CatalogosParametrizacion,
		com.netro.descuento.*,
		com.netro.catalogos.container.CatalogSettingsContainer,
		com.netro.catalogos.vo.Catalogos,
		com.netro.catalogos.dao.util.CatalogFkInfo,
		com.netro.catalogos.dao.QueryCatalog,
		com.netro.catalogos.dao.CatalogosDML,
		com.netro.catalogos.web.ComboItem,
		com.netro.catalogos.vo.Catalogo,
		com.netro.catalogos.vo.Campo,
		com.netro.catalogos.vo.DataSet,
		com.netro.catalogos.vo.DataRow,
		com.netro.catalogos.vo.HardCodedCatalog,
		com.netro.catalogos.parametrizacion.*,
		java.math.BigDecimal"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String perfilCatalogo 	= null;
String informacion 		= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion			= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");
String pageId				= request.getParameter("pageId");

String infoRegresar		= "";

log.debug("informacion = <"+informacion+">");

// DETERMINAR EL PERFIL DEL CATALOGO
String perfilUsuario 	= (String) session.getAttribute("sesPerfil");
String tipoUsuario		= (String) session.getAttribute("strTipoUsuario"); 
// PANTALLA: ADMIN NAFIN - ADMINISTRACION - PARAMETRIZACION - CATALOGOS 
if( "ADMIN NAFIN".equals(perfilUsuario)         &&   "parametros.PPPARAMETR|PARAM".equals(pageId) ){
	perfilCatalogo 		= "PARAMCATADMINNAFIN";
// PANTALLA: ADMIN NAFIN - ADMINISTRACION – PARAMETRIZACION – CATALOGOS II
} else if( "ADMIN NAFIN".equals(perfilUsuario)  &&   "parametros.PPPARAMETR|PARAMII".equals(pageId) ){ // Nota: Agregar aquí idMenu para la versión nueva
	perfilCatalogo 		= "PARAMCATADMINNAFINII";

}// PANTALLA: ADMIN NAFIN - ADMINISTRACION – PARAMETRIZACION – CATALOGOS ->
	else if("ADMIN NAFIN".equals(perfilUsuario)  &&  "idMenu.15MANCATALOGO".equals(pageId)){
	perfilCatalogo 		= "PARAMCATADMINNAFIN";
	
// PANTALLA: ADMIN EPO - ADMINISTRACION - PARAMETRIZACION - CATALOGOS
} else if( "ADMIN EPO".equals(perfilUsuario)    && ( "parametros.PEPOPARAM|PARAM".equals(pageId)  || "idMenu.15MANCATALOGO".equals(pageId) )     ){
	perfilCatalogo 		= "PARAMCATADMINEPO";
// PANTALLA: ADMIN EPO - DESCUENTO ELECTRONICO - CESION DE DERECHOS - CAPTURAS - CATALOGOS 
} else if( "ADMIN EPO".equals(perfilUsuario)    && ( "parametros.CDERCAPEPO|CDCAP".equals(pageId) || "idMenu.34EPOCATALOGO".equals(pageId) )     ){
	perfilCatalogo 		= "PARAMCATADMINEPO_CESION";
// PANTALLA: ADMIN GARANT - CONCILIACION AUTOMATICA – PARAMETRIZACION – CATALOGOS
} else if( "ADMIN GARANT".equals(perfilUsuario) && ( "parametros.GPROTPAG|GPAG".equals(pageId)    || "idMenu.29ADMCONCPARAMCATAL".equals(pageId) )){ 
	perfilCatalogo 		= "PARAMCATADMINGARANT";
} else {
	log.error("perfilUsuario = <" + perfilUsuario+ ">");
	log.error("pageId        = <" + pageId+ ">");
	log.error("informacion   = <" + informacion+ ">");
	throw new AppException("Acceso denegado.");	
}

if (informacion.equals("CatalogosParametrizacion")) {
	
	//------------------------------------------------------------------	
	// I. CONSULTAR LISTA DE CATALOGOS PARAMETRIZABLES 
	//    EN BASE AL PERFIL DEL USUARIO LOGEADO
	//------------------------------------------------------------------
	
	com.netro.model.catalogos.CatalogosParametrizacion cat = new com.netro.model.catalogos.CatalogosParametrizacion();
	cat.setPerfil(perfilCatalogo);
	
	infoRegresar = cat.getJSONElementos();
 
} else if ( informacion.equals("ConsultaCatalogo.consultarCatalogo") ) {
	
	//--------------------------------------------------------------------------	
	// II. CONSULTAR EL TIPO DE CATALOGO PARA DETERMINAR LA ACCION QUE PROCEDE
	//--------------------------------------------------------------------------	
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String 		claveCatalogo 		= request.getParameter("claveCatalogo");
   claveCatalogo = ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
	
   boolean		sinClaveCatalogo 	= "0".equals(claveCatalogo)?true:false;
   
	// Obtener instancia del EJB de Catalogos Parametrizacion
	com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
	try {
				
		//CatalogosParametrizacionHome 	catalogosParametrizacionHome 	= null;
		//Context 								context 								= ContextoJNDI.getInitialContext();
		
		//catalogosParametrizacionHome 	= (CatalogosParametrizacionHome) context.lookup("CatalogosParametrizacionEJB");
		//catalogosParametrizacion 		= catalogosParametrizacionHome.create();
		
		catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("ConsultaCatalogo.consultarCatalogo(Exception): Obtener instancia del EJB de Catálogos Parametrización.");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
	 
	}

	// Determinar el estado siguiente
	boolean esCatalogoExterno = false;
	String  estadoSiguiente   = null;
	
	// No se especifico la clave del catalogo	
	if( "0".equals(claveCatalogo) ) {
		
		esCatalogoExterno = false;
		estadoSiguiente   = "ESPERAR_DECISION"; //"FIN";
		
	// Se trata de un catalogo externo
	} else if( catalogosParametrizacion.esCatalogoExterno(claveCatalogo, perfilCatalogo) ) {
	
		esCatalogoExterno = true;
		estadoSiguiente   = "FIN";
		
	// Se tratar de un catalogo parametrizado por lo que se procederá a mostrar su contenido
	} else {
		
		esCatalogoExterno = false;
		estadoSiguiente   = "CARGAR_CATALOGO";
		
	}
 
	// Enviar la clave del catalogo
	resultado.put("claveCatalogo", 		claveCatalogo							);
	// Especificar si se redirige a otra pagina
	resultado.put("redirigir", 			new Boolean(esCatalogoExterno)	);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 	estadoSiguiente						);
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)					);
 
	infoRegresar = resultado.toString();
	
} else if ( informacion.equals("ConsultaCatalogo.cargarCatalogo") ) {

	//------------------------------------------------------------------
	// III. CARGAR CONTENIDO DEL CATALOGO EN SESION
	//------------------------------------------------------------------
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String		estadoSiguiente	= null;
	
	String claveCatalogo = request.getParameter("claveCatalogo");
   claveCatalogo 			= ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
 	
	// Obtener instancia del EJB de Catalogos Parametrizacion
	com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
	try {
				
		//CatalogosParametrizacionHome 	catalogosParametrizacionHome 	= null;
		//Context 								context 								= ContextoJNDI.getInitialContext();
		
		//catalogosParametrizacionHome 	= (CatalogosParametrizacionHome) context.lookup("CatalogosParametrizacionEJB");
		//catalogosParametrizacion 		= catalogosParametrizacionHome.create();
		
		catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("ConsultaCatalogo.consultarCatalogo(Exception): Obtener instancia del EJB de Catálogos Parametrización.");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
	 
	}
	   	
  	// Leer configuracion del Catalogo
  	String 							nombreArchivoCatalogo 	= catalogosParametrizacion.getNombreArchivoCatalogo(claveCatalogo, perfilCatalogo);
   CatalogSettingsContainer 	_settings 					= new CatalogSettingsContainer( pageContext.getServletContext().getRealPath("/") );
   Catalogo 						catalogo 					= _settings.getCatalogo(claveCatalogo,nombreArchivoCatalogo);
        
   // Cargar Elementos del Catalogo
   if( catalogo != null ){
   	
   	catalogo.setPerfilCatalogo(perfilCatalogo);
		session.setAttribute("15MANCATALOGO01EXT.DATA.JSP."+perfilCatalogo+".CATALOGO", catalogo);
		estadoSiguiente = "CARGAR_FORMA_CATALOGO";
		
	} else {
		
		// Si no se pudo leer el catalogo ir a la pantalla de inicio   	
      resultado.put("msg","Ocurrió un error al leer el archivo de configuración del catálogo.");
      estadoSiguiente = "FIN";
      
	}	
 
	// Enviar la clave del catalogo
	resultado.put("claveCatalogo", 		claveCatalogo			);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 	estadoSiguiente		);
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (informacion.equals("ConsultaCatalogo.cargarFormaCatalogo") ){
 
	//------------------------------------------------------------------
	// III. CARGAR COMPONENTES DE LA FORMA ASOCIADA AL CATALOGO
	//------------------------------------------------------------------
	
	// 1. Obtener variables iniciales
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String		estadoSiguiente	= null;
	
	String claveCatalogo 			= request.getParameter("claveCatalogo");
   claveCatalogo 						= ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
   
    // Obtener instancia del EJB de Catalogos Parametrizacion
	com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
	try {
				
		//CatalogosParametrizacionHome 	catalogosParametrizacionHome 	= null;
		//Context 								context 								= ContextoJNDI.getInitialContext();
		
		//catalogosParametrizacionHome 	= (CatalogosParametrizacionHome) context.lookup("CatalogosParametrizacionEJB");
		//catalogosParametrizacion 		= catalogosParametrizacionHome.create();
		
		catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("ConsultaCatalogo.consultarCatalogo(Exception): Obtener instancia del EJB de Catálogos Parametrización");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
	 
	}
	
   // 2. Obtener configuracion del catalogo
   Catalogo 		catalogo 		= (Catalogo) session.getAttribute("15MANCATALOGO01EXT.DATA.JSP."+perfilCatalogo+".CATALOGO");
 
	// 3. Cargar elementos correspondientes a la forma asociada al catalogo
   JSONArray  elementosFormaCatalogo 	= new JSONArray();
   JSONObject elementoForma  				= null;
	for(int i = 0; i < catalogo.getFieldCount(); i++) {
				
		Campo campo = catalogo.getField(i);
			
		if(campo.canSearch) { // Si es un campo de busqueda
				
			// 3.0 Crear objeto que guardara las propiedades del campo de busqueda
			elementoForma  = new JSONObject();
				
			// 3.1 Obtener nombre del campo
			elementoForma.put("fieldLabel", 	campo.getFriendlyName());
				
			// 3.2 Nota: Agregar logica aquí para el hardcoded catalog...
			// En un futuro, volver mas generica esta funcionalidad
			if( campo.isHardCodedCatalog ){
				
				// 3.2.1 Definir tipo de campo: combo
				elementoForma.put("type",    		"combo");
				elementoForma.put("name",    		campo.getFieldName());
				
				// 3.2.2 Consultar los valores posibles del combo
				HardCodedCatalog 	hardCodedCatalog 	= campo.getHardCodedCatalog();
				String 				action 				= hardCodedCatalog.getAction();
				List					lista					= null;
						
				if( "GET_LISTA_USUARIOS_OID".equalsIgnoreCase(action) ){
							
					// La siguiente implementacion es temporal y en una mejora esta debe ser migrada a una propiedad del tag: hard-coded-catalog
					String perfilActual 	= (String) session.getAttribute("sesPerfil");
					if( !"ADMIN EPO".equals(perfilActual)){
						throw new AppException("No tiene permiso para hacer uso de esta funcionalidad.");
					}
					String claveEpoCesion 	= (String) request.getSession().getAttribute("iNoCliente");
							
					// Leer lista de responsables
					lista 						= catalogosParametrizacion.getListaResponsables(claveEpoCesion);
							
				} else {
						
					throw new AppException("La acción especificada en la propiedad hard-coded-catalog.action dentro del XML asociado al catalogo, no es valida.");
					
				}
				
				// 3.2.3 Agregar datos del Array Store
				JSONArray registros 		= new JSONArray();
				JSONArray registro  		= null;
				for(int k = 0; k < lista.size(); k++) {
					
					HashMap elemento = (HashMap) lista.get(k);
							
					registro = new JSONArray();
					registro.add((String) elemento.get("CLAVE")			); 
					registro.add((String) elemento.get("DESCRIPCION")	);
					
					registros.add(registro);
							
				} 
				elementoForma.put("data", registros);
				
				// 3.2.4 Agregar campo a la forma
				elementosFormaCatalogo.add(elementoForma);
				
			// Si el campo corresponde a una llave foranea se empleara un combobox
			} else if(campo.isForeignKey) {
					
				// 3.3.1 Definir tipo de campo: combo
				elementoForma.put("type",    		"combo");
				elementoForma.put("name",    		campo.getFieldName());
					
				// 3.3.2 Consultar los valores posibles del combo
				ArrayList arFk 		= CatalogFkInfo.getCatalogFkInfo(campo.getForeignKey().getTable(), campo.getForeignKey().getFk(), campo.getForeignKey().getShowField(), campo.getForeignKey().getIn() );
				JSONArray registros 	= new JSONArray();
				JSONArray registro  	= null;
				for(int j=0;j<arFk.size();j++){
						
					ComboItem comboItem = (ComboItem) arFk.get(j);
						
					registro = new JSONArray();
					registro.add(comboItem.getPK());
					registro.add((String)comboItem.getValue());
						
					registros.add(registro);
				}
				elementoForma.put("data", registros);
					
				// 3.3.3 Agregar campo a la forma
				elementosFormaCatalogo.add(elementoForma);
	 
			// 3.4 Si el campo solo permite una lista de valores, mostrar un combobox
			} else if(campo.getAllowedValues().size() > 0) {
					
				// 3.4.1 Definir tipo de campo: combo
				elementoForma.put("type",    		"combo");
				elementoForma.put("name",    		campo.getFieldName());
									
				// 3.4.2 Obtener los valores posibles del combo
				JSONArray registros 	= new JSONArray();
				JSONArray registro  	= null;
				for(int j = 0; j < campo.getAllowedValues().size(); j++) {
					
					registro = new JSONArray();
					registro.add(campo.getAllowedValue(j).getValue()); 
					registro.add(campo.getAllowedValue(j).getName());
						
					registros.add(registro);
						
				} 
				elementoForma.put("data", registros); 
					
				// 3.4.3 Agregar campo a la forma
				elementosFormaCatalogo.add(elementoForma);
					
			// 3.5 Para todos los otros casos mostrar campos de texto
			} else {
					
				// 3.6.1 Definir tipo de campo:
				elementoForma.put("name", 			campo.getFieldName()	          );
				elementoForma.put("type", 			"textfield"				          );
				if( campo.toUpperCase ){
					elementoForma.put("toUpperCase", new Boolean(campo.toUpperCase) );
				}
				// 3.6.2 Agregar campo a la forma
				elementosFormaCatalogo.add(elementoForma);
								 
			}
	 
		} else {
			elementoForma  = new JSONObject();
			elementosFormaCatalogo.add(elementoForma);
		}
			
	}
		
	// 4. Determinar el estado siguiente
	estadoSiguiente = "MOSTRAR_FORMA_CATALOGO";
	
	// Enviar elementos de la Forma del Catálogo
	resultado.put("elementosFormaCatalogo", elementosFormaCatalogo );	
	// Enviar la clave del catalogo
	resultado.put("claveCatalogo", 		claveCatalogo			);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 	estadoSiguiente		);
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if ( informacion.equals("ConsultaCatalogo.mostrarFormaCatalogo") ){
 
	//------------------------------------------------------------------
	// III. CARGAR COMPONENTES DE LA FORMA ASOCIADA AL CATALOGO
	//------------------------------------------------------------------
	
	// 1. Obtener variables iniciales
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String		estadoSiguiente	= null;
	
	String claveCatalogo 						= request.getParameter("claveCatalogo");
   claveCatalogo 									= ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
   String numeroElementosFormaCatalogo  	= request.getParameter("numeroElementosFormaCatalogo");
   numeroElementosFormaCatalogo 			   = ( numeroElementosFormaCatalogo == null || numeroElementosFormaCatalogo.trim().equals("") )?"0":numeroElementosFormaCatalogo;
   
   if( Integer.parseInt(numeroElementosFormaCatalogo) > 0){
   	estadoSiguiente = "CARGAR_FORMA_EDICION_CATALOGOS";
   } else {
   	estadoSiguiente = "ESPERAR_DECISION";
   }

   // Enviar la clave del catalogo
	resultado.put("claveCatalogo", 		claveCatalogo			);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 	estadoSiguiente		);
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if ( informacion.equals("ConsultaCatalogo.cargarFormaEdicionCatalogos") ){
	
	//------------------------------------------------------------------
	// III. CARGAR COMPONENTES DE LA FORMA DE EDICION DEL CATALOGO
	//------------------------------------------------------------------
	
	// 1. Obtener variables iniciales
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String		estadoSiguiente	= null;
 
   String claveCatalogo 			= request.getParameter("claveCatalogo");
   claveCatalogo 						= ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
   
   // Obtener instancia del EJB de Catalogos Parametrizacion
	com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
	try {
				
		//CatalogosParametrizacionHome 	catalogosParametrizacionHome 	= null;
		//Context 								context 								= ContextoJNDI.getInitialContext();
		
		//catalogosParametrizacionHome 	= (CatalogosParametrizacionHome) context.lookup("CatalogosParametrizacionEJB");
		//catalogosParametrizacion 		= catalogosParametrizacionHome.create();
		
		catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("ConsultaCatalogo.consultarCatalogo(Exception): Obtener instancia del EJB de Catálogos Parametrización");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
	 
	}
	
   // 2. Obtener configuracion del catalogo
   Catalogo catalogo 		= (Catalogo) 	session.getAttribute("15MANCATALOGO01EXT.DATA.JSP."+perfilCatalogo+".CATALOGO");
   String 	nombreUsuario	= (String) 		session.getAttribute("sesCveUsuario"); // Nota: esta propiedad está considerada a volverse mas
   																										// genérica en el futuro.
   
   // 3. Crear objeto asociado al editor del grid panel asociado al catalogo
   JSONArray  elementosFormaEdicionCatalogo 	= new JSONArray();
   JSONObject elementoFormaEdicion				= null;
	for(int i = 0; i < catalogo.getFieldCount(); i++){
		
		Campo campo = catalogo.getField(i);
			
		if(campo.isVisible && campo.showEdit){
		
			// 3.1 Determinar el tipo de columna
			String strTipo			= "";
			if(campo.getAllowedValues().size() > 0){
				strTipo = "textcolumn";
			}else if( campo.isForeignKey ){
				strTipo = "foreigncolumn";
			}else if( campo.getType() instanceof java.math.BigDecimal){
				strTipo = "numbercolumn";
			}else if( campo.getType() instanceof java.lang.String){
				strTipo = "textcolumn";
			}else if( campo.getType() instanceof java.sql.Date){
				strTipo = "datecolumn";
			}else{ // No es un tipo de dato registrado
				continue;
			}
			
			// 3.2 Crear objeto que guardará las propiedades del elemento de la forma de Edicion
			elementoFormaEdicion	= new JSONObject();

			// 3.2.1 Nota: Agregar logica aquí para el hardcoded catalog...
			// En un futuro, volver mas generica esta funcionalidad
			if( campo.isHardCodedCatalog ){
						
				HardCodedCatalog 	hardCodedCatalog 	= campo.getHardCodedCatalog();
				String 				action 				= hardCodedCatalog.getAction();
				List					lista					= null;
						
				if( "GET_LISTA_USUARIOS_OID".equalsIgnoreCase(action) ){
							
					// La siguiente implementacion es temporal y en una mejora esta debe ser migrada a una propiedad del tag: hard-coded-catalog
					String perfilActual 	= (String) session.getAttribute("sesPerfil");
					if( !"ADMIN EPO".equals(perfilActual)){
						throw new AppException("No tiene permiso para hacer uso de esta funcionalidad.");
					}
					String claveEpoCesion 	= (String) request.getSession().getAttribute("iNoCliente");
							
					// Leer lista de responsables
					lista 						= catalogosParametrizacion.getListaResponsables(claveEpoCesion);
							
				} else {
						
					throw new AppException("La acción especificada en la propiedad hard-coded-catalog.action dentro del XML asociado al catalogo, no es valida.");
					
				}
						
				// Se utilizar un combo
				elementoFormaEdicion.put("type",				"combo");
				elementoFormaEdicion.put("id",				campo.getFieldName());
				elementoFormaEdicion.put("name",				campo.getFriendlyName());
				//* elementoFormaEdicion.put("displayField",		"descripcion");
				//* elementoFormaEdicion.put("valueField",		"clave");
				//* elementoFormaEdicion.put("forceSelection",	new Boolean("true"));
				elementoFormaEdicion.put("emptyText",		"Seleccione..." );
				elementoFormaEdicion.put("readOnly",		campo.isReadOnly?new Boolean("true"):new Boolean("false"));
				elementoFormaEdicion.put("allowBlank",	   campo.isNullable?new Boolean("true"):new Boolean("false"));
				//* elementoFormaEdicion.put("triggerAction",	"all");
				//* elementoFormaEdicion.put("typeAhead",			new Boolean("true"));
				//* elementoFormaEdicion.put("minChars",			new Integer("1"));
				//* elementoFormaEdicion.put("editable",			new Boolean("true"));
				//* elementoFormaEdicion.put("mode",				"local");
 
				// Agregar datos del Array Store
				JSONArray registros 		= new JSONArray();
				JSONArray registro  		= null;
				for(int k = 0; k < lista.size(); k++) {
					
					HashMap elemento = (HashMap) lista.get(k);
							
					registro = new JSONArray();
					registro.add((String) elemento.get("CLAVE")			); 
					registro.add((String) elemento.get("DESCRIPCION")	);
					
					registros.add(registro);
							
				} 
				elementoFormaEdicion.put("storeData",  registros);
 
			// 3.2.2 Si el campo corresponde a una llave foranea
			} else if(campo.isForeignKey) {
	 
				// Se utilizara un combo
				elementoFormaEdicion.put("type",						"combo");
				elementoFormaEdicion.put("id",						campo.getFieldName());
				elementoFormaEdicion.put("name",						campo.getFriendlyName());
				//* elementoFormaEdicion.put("displayField",		"descripcion");
				//* elementoFormaEdicion.put("valueField",		"clave");
				//* elementoFormaEdicion.put("forceSelection",	new Boolean( "true" ));
				elementoFormaEdicion.put("emptyText",				"Seleccione..." );
				elementoFormaEdicion.put("readOnly",				campo.isReadOnly?new Boolean("true"):new Boolean("false"));
				elementoFormaEdicion.put("allowBlank",	   		campo.isNullable?new Boolean("true"):new Boolean("false"));
				//* elementoFormaEdicion.put("triggerAction",	"all");
				//* elementoFormaEdicion.put("typeAhead",			new Boolean("true"));
				//* elementoFormaEdicion.put("minChars",			new Integer("1"));
				//* elementoFormaEdicion.put("editable",			new Boolean("true"));
				//* elementoFormaEdicion.put("mode",				"local");
						
				// Consultar contenido del simple store
				ArrayList arFk 			= CatalogFkInfo.getCatalogFkInfo(campo.getForeignKey().getTable(), campo.getForeignKey().getFk(), campo.getForeignKey().getShowField(), campo.getForeignKey().getIn() );
				JSONArray registros 		= new JSONArray();
				JSONArray registro  		= null;
				for(int j=0;j<arFk.size();j++){
							
					ComboItem comboItem = (ComboItem) arFk.get(j);
							
					registro = new JSONArray();
					registro.add(comboItem.getPK());
					registro.add((String)comboItem.getValue());
							
					registros.add(registro);
				}
				elementoFormaEdicion.put("storeData",  registros);
	
			// 3.2.3 Si el campo solo puede tener valor de una lista restringida
			} else if(campo.getAllowedValues().size() > 0){
						
				// Se utilizar un combo
				elementoFormaEdicion.put("type",						"combo");
				elementoFormaEdicion.put("id",						campo.getFieldName());
				elementoFormaEdicion.put("name",						campo.getFriendlyName());
				//* elementoFormaEdicion.put("displayField",		"descripcion");
				//* elementoFormaEdicion.put("valueField",		"clave");
				//* elementoFormaEdicion.put("forceSelection",	new Boolean("true"));
				elementoFormaEdicion.put("emptyText",				"Seleccione..." );				
				elementoFormaEdicion.put("readOnly",				campo.isReadOnly?new Boolean("true"):new Boolean("false"));
				elementoFormaEdicion.put("allowBlank",	   		campo.isNullable?new Boolean("true"):new Boolean("false"));
				//* elementoFormaEdicion.put("triggerAction",	"all");
				//* elementoFormaEdicion.put("typeAhead",			new Boolean("true"));
				//* elementoFormaEdicion.put("minChars",			new Integer("1"));
				//* elementoFormaEdicion.put("editable",			new Boolean("true"));
				//* elementoFormaEdicion.put("mode",				"local");
						
				// Agregar datos del Simple Store
				JSONArray registros 		= new JSONArray();
				JSONArray registro  		= null;
				for(int k = 0; k < campo.getAllowedValues().size(); k++) {
					
					registro = new JSONArray();
					registro.add(campo.getAllowedValue(k).getValue()); 
					registro.add(campo.getAllowedValue(k).getName());
						
					registros.add(registro);
				} 
				elementoFormaEdicion.put("storeData",  registros);
 
			// 3.2.4 Si el campo es de tipo texto
			}else if("textcolumn".equals(strTipo)){
						
				elementoFormaEdicion.put("type",				"textcolumn");
				elementoFormaEdicion.put("id",				campo.getFieldName());
				elementoFormaEdicion.put("name",				campo.getFriendlyName());
				elementoFormaEdicion.put("readOnly",		new Boolean(campo.isReadOnly));
				elementoFormaEdicion.put("allowBlank",		campo.isNullable?new Boolean("true"):new Boolean("false"));
 
				String vtype = campo.getValitadionType();
				if(!vtype.equals("")){
					elementoFormaEdicion.put("vtype",	vtype);
				}
				if(campo.toUpperCase){
					elementoFormaEdicion.put("toUpperCase", new Boolean(campo.toUpperCase) );
				}
				elementoFormaEdicion.put("maxLength", 	campo.getMaxLength());
					
			// 3.2.5 Si el campo es de tipo datecolumn
			}else if("datecolumn".equals(strTipo)){
						
				elementoFormaEdicion.put("type",				"datecolumn");
				elementoFormaEdicion.put("id",				campo.getFieldName());
				elementoFormaEdicion.put("name",				campo.getFriendlyName());
				elementoFormaEdicion.put("readOnly",		new Boolean(campo.isReadOnly));
				elementoFormaEdicion.put("allowBlank",		campo.isNullable?new Boolean("true"):new Boolean("false"));
				//* elementoFormaEdicion.put("format",		"d/m/Y");	
				
				//elementoFormaEdicion.put("width",			campo.getGridWidth());
				//elementoFormaEdicion.put("minValue",		"01/01/2006"); 														// Debug info: propiedad pendiente de agregar
				//elementoFormaEdicion.put("minText",		"Can't have a start date before the company existed!"); 	// Debug info: propiedad pendiente de agregar
				//elementoFormaEdicion.put("maxValue",		"(new Date()).format('d/m/Y')"); 								// Debug info: propiedad pendiente de agregar
					
			// 3.2.6 Si el campo es de tipo numberField
			} else if("numbercolumn".equals(strTipo)){
				
				elementoFormaEdicion.put("type",				"numbercolumn");
				elementoFormaEdicion.put("id",				campo.getFieldName());
				elementoFormaEdicion.put("name",				campo.getFriendlyName());
				elementoFormaEdicion.put("readOnly",		new Boolean(campo.isReadOnly));
				elementoFormaEdicion.put("allowBlank",		campo.isNullable?new Boolean("true"):new Boolean("false"));

				if(campo.getNumberMinValue() != null){
					elementoFormaEdicion.put("minValue",	campo.getNumberMinValue()); 										
				}
				if( campo.getNumberMaxValue() != null){
					elementoFormaEdicion.put("maxValue",	campo.getNumberMaxValue()); 										
				}
 	
			}
				
			// 3.3 Determinar el valor default
			String defaultValue = null;
			if( "VUSUARIO_ULT_MODIF".equalsIgnoreCase( campo.getFieldName() ) ){ 
				// Nota: en un desarrollo futuro, considerar leer esta propiedad de otra manera, a lo mejor en CatalogSettings Container, etc.
				defaultValue = nombreUsuario == null || nombreUsuario.trim().equals("")?campo.getDefaulValue().toString():nombreUsuario;
			}else{
				defaultValue = campo.getDefaulValue() != null?campo.getDefaulValue().toString():"";
			}	
			if( "datecolumn".equals(strTipo) && "SYSDATE".equalsIgnoreCase(defaultValue)){
				defaultValue = Fecha.getFechaActual();
			}else if( "numbercolumn".equals(strTipo) && "".equals(defaultValue)){
				defaultValue = "0";
			}
			elementoFormaEdicion.put("defaultValue",defaultValue);
				
			// 3.4 Agregar campo a la forma
			elementosFormaEdicionCatalogo.add(elementoFormaEdicion);
					
		}
		
	}	
	
	// 4. Revisar si se incluira la llave primaria como columna hidden en el Grid Panel Asociado al Catalogo
	boolean 	foundPrimaryKey 	= false;
	String 	primaryKeyName 	= catalogo.getPrimaryKeyName().trim();
	for(int k=0;k<elementosFormaEdicionCatalogo.size();k++){
		JSONObject registro = (JSONObject) elementosFormaEdicionCatalogo.get(k);
		// Se ha encontrado la llave primairia en los campos del catalogo
		if( primaryKeyName.equalsIgnoreCase( (String)registro.get("id") ) ){
			foundPrimaryKey  = true;
			registro.put("primaryKey", new Boolean("true"));
			break;
		}
	}
	// Si la llave primaria no ha sido especificada, agregarla al catalogo
	if(!foundPrimaryKey){
		// Crear objeto que guardará las propiedades del "grid field"
		elementoFormaEdicion    		  = new JSONObject();
		// Agregar propiedades adicionales a la columna 
		elementoFormaEdicion.put("type",					"primarykeycolumn"	);
		elementoFormaEdicion.put("id",					primaryKeyName			);
		elementoFormaEdicion.put("name",					primaryKeyName			);
		elementoFormaEdicion.put("primaryKey",		new Boolean("true")	);
		elementoFormaEdicion.put("defaultValue",		""							);
		// Agregar campo a la lista de grid fields
		elementosFormaEdicionCatalogo.add(elementoFormaEdicion);
	}
	
	// 5. Determinar estado siguiente
   estadoSiguiente = "REGISTRAR_FORMA_EDICION_CATALOGOS";
   
   resultado.put("elementosFormaEdicionCatalogo",	elementosFormaEdicionCatalogo	);
   // Enviar la clave del catalogo
	resultado.put("claveCatalogo", 						claveCatalogo						);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 					estadoSiguiente					);
	// Enviar resultado de la operacion
	resultado.put("success", 								new Boolean(success)				);
	
	infoRegresar = resultado.toString();
	
} else if ( informacion.equals("ConsultaCatalogo.registrarFormaEdicionCatalogos") ){
	
	//------------------------------------------------------------------
	// III. MOSTRAR COMPONENTES DE LA FORMA DE EDICION DEL CATALOGO
	//------------------------------------------------------------------
	
	// 1. Obtener variables iniciales
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String		estadoSiguiente	= null;
	
	String claveCatalogo 							= request.getParameter("claveCatalogo");
   claveCatalogo 										= ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
 
   estadoSiguiente = "CARGAR_GRID_PANEL_CATALOGOS";
   
   // Enviar la clave del catalogo
	resultado.put("claveCatalogo", 		claveCatalogo			);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 	estadoSiguiente		);
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if ( informacion.equals("ConsultaCatalogo.cargarGridPanelCatalogos") ){
	
	//------------------------------------------------------------------
	// III. CARGAR COMPONENTES DE LA FORMA ASOCIADA AL CATALOGO
	//------------------------------------------------------------------
	
	// 1. Obtener variables iniciales
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String		estadoSiguiente	= null;
	
	String claveCatalogo 			= request.getParameter("claveCatalogo");
   claveCatalogo 						= ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
   
   // Obtener instancia del EJB de Catalogos Parametrizacion
	com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
	try {
	
		catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("ConsultaCatalogo.consultarCatalogo(Exception): Obtener instancia del EJB de Catálogos Parametrización");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
	 
	}
	
   // 2. Obtener configuracion del catalogo
   Catalogo 		catalogo 		= (Catalogo) session.getAttribute("15MANCATALOGO01EXT.DATA.JSP."+perfilCatalogo+".CATALOGO");
 
	// 3. Cargar elementos correspondientes al grid panel asociado al catalogo
	JSONArray  elementosGridPanelCatalogo 			= new JSONArray();
	JSONObject elementoGridPanelCatalogo    			= null; 
 
	for(int i = 0; i < catalogo.getFieldCount(); i++){
			
		// 3.1 Leer campo asociado al Grid Panel
		Campo  campo         = catalogo.getField(i);
		String strFieldName 	= campo.getFieldName();
 
		// 3.2 Si el campo es visible agregarlo a la lista de elementos del grid panel
		if(catalogo.getField(i).isVisible){
	 
			// 3.2.1 Crear objeto que guardará las propiedades del "grid field"
			elementoGridPanelCatalogo    		= new JSONObject(); 
				
			// 3.2.2 Determinar el tipo de columna
			String strTipo			= "";
			if(campo.getAllowedValues().size() > 0){
				strTipo = "textcolumn";
			}else if( campo.isForeignKey ){
				strTipo = "foreigncolumn";
			}else if( campo.getType() instanceof java.math.BigDecimal){
				strTipo = "numbercolumn";
			}else if( campo.getType() instanceof java.lang.String){
				strTipo = "textcolumn";
			}else if( campo.getType() instanceof java.sql.Date){
				strTipo = "datecolumn";
			}else{ // No es un tipo de dato registrado
				continue;
			}
				
			// 3.2.3 Agregar propiedades adicionales a la columna
			elementoGridPanelCatalogo.put("type",		strTipo);
			elementoGridPanelCatalogo.put("id",			strFieldName);
			elementoGridPanelCatalogo.put("header",	campo.getFriendlyName());
			elementoGridPanelCatalogo.put("width",		campo.getGridWidth()); 
 
			// 3.2.4 Dependiendo del tipo de columna, agregar propiedades especificas
			if("datecolumn".equals(strTipo)){
				elementoGridPanelCatalogo.put("format",				campo.getDateFormat()   );
			}else if("numbercolumn".equals(strTipo)){
				elementoGridPanelCatalogo.put("format",				campo.getNumberFormat() ); 
			}
 
			// 3.2.5 Aquellos que utilicen una representación con colmna "foránea"
			// Nota: Agregar logica aquí para el hardcoded catalog...
			// En un futuro, volver mas generica esta funcionalidad
			if( campo.isHardCodedCatalog ){
						
				HardCodedCatalog 	hardCodedCatalog 	= campo.getHardCodedCatalog();
				String 				action 				= hardCodedCatalog.getAction();
				List					lista					= null;
						
				if( "GET_LISTA_USUARIOS_OID".equalsIgnoreCase(action) ){
							
					// La siguiente implementacion es temporal y en una mejora esta debe ser migrada a una propiedad del tag: hard-coded-catalog
					String perfilActual 	= (String) session.getAttribute("sesPerfil");
					if( !"ADMIN EPO".equals(perfilActual)){
						throw new AppException("No tiene permiso para hacer uso de esta funcionalidad.");
					}
					String claveEpoCesion 	= (String) request.getSession().getAttribute("iNoCliente");
							
					// Leer lista de responsables
					lista 						= catalogosParametrizacion.getListaResponsables(claveEpoCesion);
						
				} else {
						
					throw new AppException("La acción especificada en la propiedad hard-coded-catalog.action dentro del XML asociado al catalogo, no es valida.");
							
				}
 	
				// Agregar datos del Simple Store
				JSONArray registros 		= new JSONArray();
				JSONArray registro  		= null;
				for(int k = 0; k < lista.size(); k++) {
					
					HashMap elemento = (HashMap) lista.get(k);
							
					registro = new JSONArray();
					registro.add((String) elemento.get("CLAVE")			); 
					registro.add((String) elemento.get("DESCRIPCION")	);
						
					registros.add(registro);
							
				} 
				// Agregar datos del store
				elementoGridPanelCatalogo.put("storeData",  registros);
 
			// Si el campo corresponde a una llave foranea
			} else if(campo.isForeignKey) {
	 
				// Consultar contenido del simple store
				ArrayList arFk 			= CatalogFkInfo.getCatalogFkInfo(campo.getForeignKey().getTable(), campo.getForeignKey().getFk(), campo.getForeignKey().getShowField(), campo.getForeignKey().getIn() );
				JSONArray registros 		= new JSONArray();
				JSONArray registro  		= null;
				for(int j=0;j<arFk.size();j++){
							
					ComboItem comboItem = (ComboItem) arFk.get(j);
							
					registro = new JSONArray();
					registro.add(comboItem.getPK());
					registro.add((String)comboItem.getValue());
							
					registros.add(registro);
				}
				// Agregar datos del store
				elementoGridPanelCatalogo.put("storeData",  registros);		
						
			// Si el campo solo puede tener valor de una lista restringida
			} else if(campo.getAllowedValues().size() > 0){
 
				// Agregar datos del Simple Store
				JSONArray registros 		= new JSONArray();
				JSONArray registro  		= null;
				for(int k = 0; k < campo.getAllowedValues().size(); k++) {
				
					registro = new JSONArray();
					registro.add(campo.getAllowedValue(k).getValue()); 
					registro.add(campo.getAllowedValue(k).getName());
						
					registros.add(registro);
				} 
				// Agregar datos del store
				elementoGridPanelCatalogo.put("storeData",  registros);		
 
			}
					
			// 3.2.6 Agregar campo a la lista de grid fields
			elementosGridPanelCatalogo.add(elementoGridPanelCatalogo);
 
			// 3.2.7 Agregar la unica columna que se autoexpandir
			if(campo.isAutoExpand){
				resultado.put("gridPanelAutoExpandColumnId", strFieldName );
			}
			
		}
					  
	}
 
	// 3.3 Revisar si se incluira la llave primaria como columna hidden en el Grid Panel Asociado al Catalogo
	boolean 	foundPrimaryKey 	= false;
	String 	primaryKeyName 	= catalogo.getPrimaryKeyName().trim();
	for(int k=0;k<elementosGridPanelCatalogo.size();k++){
		JSONObject registro = (JSONObject) elementosGridPanelCatalogo.get(k);
		// Se ha encontrado la llave primairia en los campos del catalogo
		if( primaryKeyName.equalsIgnoreCase( (String)registro.get("id") ) ){
			foundPrimaryKey  = true;
			registro.put("primaryKey",	new Boolean("true"));
			break;
		}
	}
	// 3.4 Si la llave primaria no ha sido especificada, agregarla al catalogo
	if(!foundPrimaryKey){
		// 3.4.1 Crear objeto que guardará las propiedades del "grid field"
		elementoGridPanelCatalogo    		= new JSONObject();
		// 3.4.2 Agregar propiedades adicionales a la columna 
		elementoGridPanelCatalogo.put("type",				"primarykeycolumn"	);
		elementoGridPanelCatalogo.put("id",					primaryKeyName			);
		elementoGridPanelCatalogo.put("primaryKey",	new Boolean("true")	);
		// 3.4.3 Agregar campo a la lista de grid fields
		elementosGridPanelCatalogo.add(elementoGridPanelCatalogo);
	}
 
	// 4. Determinar los permisos que el usuario tiene sobre el grid panel del catalogo
	boolean hayPermisoAgregarRegistro 	= catalogosParametrizacion.hayPermisoAgregarRegistro(claveCatalogo,   perfilCatalogo);
	boolean hayPermisoModificarRegistro = catalogosParametrizacion.hayPermisoModificarRegistro(claveCatalogo, perfilCatalogo);
	boolean hayPermisoBorrarRegistro 	= catalogosParametrizacion.hayPermisoBorrarRegistro(claveCatalogo,    perfilCatalogo);
			
	resultado.put("hayPermisoAgregarRegistro", 	new Boolean(hayPermisoAgregarRegistro)		);
	resultado.put("hayPermisoModificarRegistro", new Boolean(hayPermisoModificarRegistro)	);
	resultado.put("hayPermisoBorrarRegistro", 	new Boolean(hayPermisoBorrarRegistro)		);
	
   // 5. Determinar el estado siguiente
	estadoSiguiente = "MOSTRAR_GRID_PANEL_CATALOGOS";
 
	// Enviar los componentes del catalogo
	resultado.put("elementosGridPanelCatalogo",    elementosGridPanelCatalogo 	);
	// Enviar la clave del catalogo
	resultado.put("claveCatalogo", 						claveCatalogo					);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 					estadoSiguiente				);
	// Enviar resultado de la operacion
	resultado.put("success", 								new Boolean(success)			);
	
	infoRegresar = resultado.toString();
 
} else if ( informacion.equals("ConsultaCatalogo.mostrarGridPanelCatalogos") ) {
   
	//------------------------------------------------------------------
	// III. CARGAR COMPONENTES DE LA FORMA ASOCIADA AL CATALOGO
	//------------------------------------------------------------------
	
	// 1. Obtener variables iniciales
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String		estadoSiguiente	= null;
	
	String claveCatalogo 							= request.getParameter("claveCatalogo");
   claveCatalogo 										= ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
 
   estadoSiguiente = "ESPERAR_DECISION";
   
   // Enviar la clave del catalogo
	resultado.put("claveCatalogo", 		claveCatalogo			);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 	estadoSiguiente		);
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
 
	infoRegresar = resultado.toString();
 	
} else if ( informacion.equals("ConsultaCatalogo.fin") ) {
	
	//------------------------------------------------------------------	
	// V. DETERMINAR EL TIPO DE CATALOGO
	//------------------------------------------------------------------	
 
	String 		claveCatalogo 	= request.getParameter("claveCatalogo");
	String 		redirigir 		= (request.getParameter("redirigir")	  == null)?"false":request.getParameter("redirigir");
	
   claveCatalogo = ( claveCatalogo == null || claveCatalogo.trim().equals("") )?"0":claveCatalogo;
   
	boolean		redirigeACatalogoExterno	= "true".equals(redirigir)?true:false;
	boolean  	hayClaveCatalogo 				= claveCatalogo != null && !claveCatalogo.trim().equals("")?true:false;
	boolean		enviarClaveCatalogo			= false;
	
	String 		paginaDestino 					= "15mancatalogo01ext.jsp";
	String		extensionArchivo				= null;
	
	if( redirigeACatalogoExterno && hayClaveCatalogo ){
 
		try {
					
			// Obtener instancia del EJB de Catalogos Parametrizacion
			com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
		
			//CatalogosParametrizacionHome 	catalogosParametrizacionHome 	= null;
			//Context 								context 								= ContextoJNDI.getInitialContext();
			
			//catalogosParametrizacionHome 	= (CatalogosParametrizacionHome) context.lookup("CatalogosParametrizacionEJB");
			//catalogosParametrizacion 		= catalogosParametrizacionHome.create();
			
			catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
			
			// Determinar el tipo de catalogo
			paginaDestino 						= catalogosParametrizacion.getNombreArchivoCatalogo(claveCatalogo, perfilCatalogo);
			
			// Obtener extension del archivo
			extensionArchivo = paginaDestino != null && paginaDestino.length() > 3 ?paginaDestino.substring(paginaDestino.length()-3):"";
			
			if("xml".equalsIgnoreCase(extensionArchivo)){
				throw new AppException("El archivo de redirección no puede ser de tipo xml.");
			}
 
			enviarClaveCatalogo =  true;
			
		} catch(Exception e) {
		 
			log.error("ConsultaCatalogo.fin(Exception): Obtener instancia del EJB de Catálogos Parametrización");
			e.printStackTrace();
			
			paginaDestino 						= "15mancatalogo01ext.jsp";
			// En el caso del reenvío se suprime el lanzamiento de excepciones
			//throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
		 
		}
 
	}
 	
	response.sendRedirect(paginaDestino+"?parametros="+pageId+(enviarClaveCatalogo?"&claveCatalogo="+claveCatalogo:""));
	
} else if (informacion.equals("Buscar")) {
 
	// 1. OBTENER CLAVE DEL CATALOGO 
	String claveCatalogo 	= (request.getParameter("claveCatalogo")  !=null)?request.getParameter("claveCatalogo"):"-1";
	String orderBy 			= (request.getParameter("orderby")      	!=null)?request.getParameter("orderby")      :"";
 
	// 2. CONSULTAR CATALOGO
	QueryCatalog 					query 		= new QueryCatalog(request);

   Catalogo 						catalogo 	= (Catalogo) session.getAttribute("15MANCATALOGO01EXT.DATA.JSP."+perfilCatalogo+".CATALOGO");
   DataSet      					data  		= query.getCatalogData(catalogo, orderBy);
 
	// 3. CONSTRUIR RESPUESTA A LA OPERACION
	JSONObject respuesta 		= new JSONObject();
   respuesta.put("success", 	new Boolean(true));
	respuesta.put("total", 		String.valueOf(data.getRowCount()));
	respuesta.put("registros", data.toJSON());
 
	infoRegresar = respuesta.toString();

}else if (informacion.equals("GuardarModificacion")){

	//------------------------------------------------------------------			
   // IV. GUARDAR REGISTRO MODIFICADO
   //------------------------------------------------------------------	
   
   Boolean success = new Boolean("true");
   
   // Obtener instancia del EJB de Catalogos Parametrizacion
	com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
	try {
				
		//CatalogosParametrizacionHome 	catalogosParametrizacionHome 	= null;
		//Context 								context 								= ContextoJNDI.getInitialContext();
		
		//catalogosParametrizacionHome 	= (CatalogosParametrizacionHome) context.lookup("CatalogosParametrizacionEJB");
		//catalogosParametrizacion 		= catalogosParametrizacionHome.create();
		
		catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
					
	}catch(Exception e){
	 
      success = new Boolean("false");
		log.error("ConsultaCatalogo.consultarCatalogo(Exception): Obtener instancia del EJB de Catálogos Parametrización");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
	 
	}
	
	// 1. OBTENER CLAVE DEL CATALOGO 
	String claveCatalogo 	= (request.getParameter("claveCatalogo")  !=null)?request.getParameter("claveCatalogo"):"-1";
	String registroId			= (request.getParameter("id")           	!=null)?request.getParameter("id")         	:"-1";
	/*
   String rowIndex			= (request.getParameter("row_index")    	!=null)?request.getParameter("row_index")  	:"-1";
   */
	String nombreUsuario		= (String) session.getAttribute("sesCveUsuario"); // Debug info: Revisar si este es el que se va a usar.
	
	if(!catalogosParametrizacion.hayPermisoModificarRegistro(claveCatalogo,   perfilCatalogo)){
		throw new AppException("No tiene permiso para editar los registros de este catálogo.");
	}
		
	// 2. OBTENER CONFIGURACION DEL CATALOGO
   Catalogo 		catalogo 	= (Catalogo) session.getAttribute("15MANCATALOGO01EXT.DATA.JSP."+perfilCatalogo+".CATALOGO");
 
	CatalogosDML 	catDML 		= new CatalogosDML(request);
	catDML.setUsuario(nombreUsuario);
	
	// 3. OBTENER DESCRIPCION DE LA LLAVE PRIMARIA
	String strPkName 				= catalogo.getPrimaryKeyName();
   String strPkCompuesto 		= "";
          
   if(strPkName.indexOf(",") != -1){
    	strPkName 		= catalogo.getPrimaryKeyName().substring(0, catalogo.getPrimaryKeyName().indexOf(",")).trim();
      strPkCompuesto = catalogo.getPrimaryKeyName().substring(catalogo.getPrimaryKeyName().indexOf(",") + 1).trim();
   }
	
	// 4. OBTENER LOS VALORES ASOCIADOS A LA LLAVE PRIMARIA
	String catPk = null;
	if(catDML.existsOnModifiedParameterList(strPkName)){ // La llave fue modificada
		catPk = catDML.obtenerValorOriginal(strPkName,	"-1");
	} else { // La llave no fue modificada
		catPk = catDML.obtenerValor(strPkName, 			"-1");
	}
	// 5. ACTUALIZAR REGISTRO
	HashMap resultado 		= catDML.updateCatalogo(catalogo, new BigDecimal(catPk),"", true);
	// Procesar respuesta
	Boolean guardarSuccess 	= (Boolean) resultado.get("success");
	String  msg 				= null;
	if( guardarSuccess.booleanValue() == false ){
		int	  sqlErrorCode		= resultado.get("SQL_ERROR_CODE") != null?((Integer)resultado.get("SQL_ERROR_CODE")).intValue():-1;
		if( sqlErrorCode        == 1    ){ // ORA-00001
			msg = "Se cancela la operación. La clave proporcionada ya se encuentra registrada.";
		} else if( sqlErrorCode == 1438 ){ // ORA-01438
			msg = "Se cancela la operación. La clave proporcionada es más grande que el valor máximo permitido.";
		} else if( sqlErrorCode == 2292 ){ // ORA-02292
			msg = "Se cancela la operación. La clave proporcionada no se puede modificar debido a que se encuentra en uso.";
		} else {
			msg = "Se cancela la operación." + (String) resultado.get("ERROR_MESSAGE");
		}
	} else if( guardarSuccess.booleanValue() == true && "NAFIN".equals(tipoUsuario) ){
		//msg = "La información se modificó exitosamente, este movimiento fue realizado por el usuario " + nombreUsuario + " y será guardado para fines de auditoría.";
		msg = "El registro fue Actualizado.";
	}
 
	// 6. CONSULTAR LOS NUEVOS VALORES DEL REGISTRO
   DataRow row = catDML.getDatos(catalogo, new BigDecimal(catPk));
      
   // 7. PREPARAR RESPUESTA
   JSONObject 		respuesta 	= new JSONObject();
	respuesta.put("success",         success 			);
	respuesta.put("guardarSuccess",	guardarSuccess );
   respuesta.put("id",              registroId		);
	respuesta.put("updated_record",  row.toJSON()	);
	if( msg != null ){
		respuesta.put("msg",          msg				);
	}
	/*
   respuesta.put("row_index",      	rowIndex);
	*/
	
	// 8. ENVIAR RESPUESTA DE LA OPERACION
	infoRegresar = respuesta.toString();
 
}else if (informacion.equals("RemoverRegistro")){

	//------------------------------------------------------------------	
   // V. BORRAR REGISTRO
   //------------------------------------------------------------------	
   Boolean success = new Boolean("true");
   
	// Obtener instancia del EJB de Catalogos Parametrizacion
	com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
	try {
				
		//CatalogosParametrizacionHome 	catalogosParametrizacionHome 	= null;
		//Context 								context 								= ContextoJNDI.getInitialContext();
		
		//catalogosParametrizacionHome 	= (CatalogosParametrizacionHome) context.lookup("CatalogosParametrizacionEJB");
		//catalogosParametrizacion 		= catalogosParametrizacionHome.create();
		
		catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
					
	}catch(Exception e){
	 
		success		= new Boolean("false");
		log.error("ConsultaCatalogo.consultarCatalogo(Exception): Obtener instancia del EJB de Catálogos Parametrización");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
	 
	}

	// 1. OBTENER CLAVE DEL CATALOGO 
	String claveCatalogo 	= (request.getParameter("claveCatalogo")     !=null)?request.getParameter("claveCatalogo"):"-1";
	String ids					= (request.getParameter("ids")               !=null)?request.getParameter("ids")        :"-1";
	String nombreUsuario		= (String) session.getAttribute("sesCveUsuario"); // Debug info: Revisar si este es el que se va a usar.
		
   if(!catalogosParametrizacion.hayPermisoBorrarRegistro(claveCatalogo,   perfilCatalogo)){
		throw new AppException("No tiene permiso para borrar registros de este catálogo.");
	}
   
	// 2. OBTENER CONFIGURACION DEL CATALOGO
   Catalogo 						catalogo 	= (Catalogo) session.getAttribute("15MANCATALOGO01EXT.DATA.JSP."+perfilCatalogo+".CATALOGO");
   
	CatalogosDML 	catDML 		= new CatalogosDML(request);
	catDML.setUsuario(nombreUsuario);
 
	// 3. OBTENER DESCRIPCION DE LA LLAVE PRIMARIA
	String strPkName 				= catalogo.getPrimaryKeyName();
   String strPkCompuesto 		= "";
          
   if(strPkName.indexOf(",") != -1){
   	strPkName 		= catalogo.getPrimaryKeyName().substring(0, catalogo.getPrimaryKeyName().indexOf(",")).trim();
      strPkCompuesto = catalogo.getPrimaryKeyName().substring(catalogo.getPrimaryKeyName().indexOf(",") + 1).trim();
   }
	
	// 4. OBTENER LOS VALORES ASOCIADOS A LA LLAVE PRIMARIA
	BigDecimal[] catPks = catDML.obtenerValores(strPkName, "-1");
	
	// 5. ACTUALIZAR REGISTRO
	HashMap resultado 		= catDML.removeRecords(catalogo,catPks);
	// Procesar respuesta
	Boolean removerSuccess 	= (Boolean) resultado.get("success");
	String  msg 				= null;
	if( removerSuccess.booleanValue() == false ){
		int	  sqlErrorCode		= resultado.get("SQL_ERROR_CODE") != null?((Integer)resultado.get("SQL_ERROR_CODE")).intValue():-1;
		if( sqlErrorCode        == 1    ){ // ORA-00001
			msg = "Se cancela la operación. La clave proporcionada ya se encuentra registrada.";
		} else if( sqlErrorCode == 1438 ){ // ORA-01438
			msg = "Se cancela la operación. La clave proporcionada es más grande que el valor máximo permitido.";
		} else if( sqlErrorCode == 2292 ){ // ORA-02292
			msg = "Se cancela la operación. La clave proporcionada no se puede eliminar debido a que se encuentra en uso.";
		} else {
			msg = "Se cancela la operación." + (String) resultado.get("ERROR_MESSAGE");
		}
	}
   
	// 6. PREPARAR RESPUESTA
	JSONObject 		respuesta 	= new JSONObject();
	respuesta.put("success",   		success);
	respuesta.put("removerSuccess",	removerSuccess);
	respuesta.put("ids",       		ids);
	if( msg != null ){
		respuesta.put("msg",    		msg);
	}
	
	// 7. ENVIAR RESPUESTA DE LA OPERACION
	infoRegresar = respuesta.toString();
		
	
}else if (informacion.equals("GuardarAlta")){
	
	//------------------------------------------------------------------	
   // VI. GUARDAR REGISTRO
   //------------------------------------------------------------------

   Boolean success = new Boolean("true");
   
	// Obtener instancia del EJB de Catalogos Parametrizacion
	com.netro.catalogos.parametrizacion.CatalogosParametrizacion catalogosParametrizacion = null;
	try {
				
		catalogosParametrizacion = ServiceLocator.getInstance().lookup("CatalogosParametrizacionEJB",com.netro.catalogos.parametrizacion.CatalogosParametrizacion.class);
					
	}catch(Exception e){
	 
		success		= new Boolean("false");
		log.error("ConsultaCatalogo.consultarCatalogo(Exception): Obtener instancia del EJB de Catálogos Parametrización");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Catálogos Parametrización.");
	 
	}
	
	// 1. OBTENER CLAVE DEL CATALOGO 
	String claveCatalogo 	= (request.getParameter("claveCatalogo")  != null)?request.getParameter("claveCatalogo")	:"-1";
	String registroId			= (request.getParameter("id")           	!= null)?request.getParameter("id")         		:"-1";
	/*
	String rowIndex			= (request.getParameter("row_index")    	!= null)?request.getParameter("row_index")  		:"-1";
	*/
	String nombreUsuario		= (String) session.getAttribute("sesCveUsuario"); // Debug info: Revisar si este es el que se va a usar.
	
	
	if(!catalogosParametrizacion.hayPermisoAgregarRegistro(claveCatalogo,   perfilCatalogo)){
		throw new AppException("No tiene permiso para agregar registros a este catálogo.");
	}
	
	// 2. OBTENER CONFIGURACION DEL CATALOGO
   Catalogo 		catalogo 	= (Catalogo) session.getAttribute("15MANCATALOGO01EXT.DATA.JSP."+perfilCatalogo+".CATALOGO");
   
	CatalogosDML 	catDML 		= new CatalogosDML(request);
	catDML.setUsuario(nombreUsuario);
	
	// 3. INSERTAR CATALOGO
	HashMap 	resultado    	= catDML.insertCatalogo(catalogo,false);
	// Procesar respuesta
	Boolean	guardarSuccess = (Boolean) resultado.get("success");
	String  	msg 				= null;
	if( guardarSuccess.booleanValue() == false ){
		int	  sqlErrorCode		= resultado.get("SQL_ERROR_CODE") != null?((Integer)resultado.get("SQL_ERROR_CODE")).intValue():-1;
		if( sqlErrorCode        == 1    ){ // ORA-00001
			msg = "Se cancela la operación. La clave proporcionada ya se encuentra registrada.";
		} else if( sqlErrorCode == 1438 ){ // ORA-01438
			msg = "Se cancela la operación. La clave proporcionada es más grande que el valor máximo permitido.";
		} else if( sqlErrorCode == 2292 ){ // ORA-02292
			msg = "Se cancela la operación. La clave proporcionada no se puede modificar debido a que se encuentra en uso.";
		} else {
			msg = "Se cancela la operación." + (String) resultado.get("ERROR_MESSAGE");
		}
	}
	
	// 4. OBTENER VALORES DE LA LLAVE PRIMARIA
	DataRow row = catDML.getDatos(catalogo, new BigDecimal( (String) resultado.get("PK_VALUE") ));
 
	// 5. PREPARAR RESPUESTA
	JSONObject 		respuesta 	= new JSONObject();
	respuesta.put("success",         success      	);
	respuesta.put("guardarSuccess", 	guardarSuccess	);
	respuesta.put("id",              registroId   	);
   respuesta.put("new_record",      row.toJSON() 	);
   if( msg != null ){
   	respuesta.put("msg",				msg);
   }
   /*
	respuesta.put("row_index",      	rowIndex);
	*/
	
	// 6. ENVIAR RESPUESTA DE LA OPERACION
	infoRegresar = respuesta.toString();
 
} else {
	throw new AppException("La acción: " + informacion + " no se encuentra registrada.");
}

log.debug("infoRegresar = <" + infoRegresar + ">"); 
%>
<%=infoRegresar%>
