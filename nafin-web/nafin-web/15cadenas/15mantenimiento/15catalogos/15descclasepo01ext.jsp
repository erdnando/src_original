<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%

	// Determinar el sistema de menus que se est� utilizando para acceder a la pantalla.
	String pageId 	 		= null;
	String sistemaMenus	= null;
	
	pageId = request.getParameter("idMenu");
	pageId = pageId == null?"":pageId;
	
	if( !"".equals(pageId)){
		sistemaMenus = "idMenu.";
	} else {
		pageId = request.getParameter("parametros");
		pageId = pageId == null?"":pageId;
		sistemaMenus = "parametros.";
	}
	
	if( pageId.indexOf("idMenu.") == 0 ) {
		sistemaMenus = "";
	} else if( pageId.indexOf("parametros.") == 0 ){
		sistemaMenus = "";
	}
	
%>
<script type="text/javascript" src="15descclasepo01ext.js?<%=session.getId()%>"></script>
<script language="JavaScript1.2" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script language="JavaScript">
	var pageId 												= '<%= sistemaMenus + pageId %>';
	var claveCatalogoDefinicionClasificacionEPO 	= '<%= request.getParameter("claveCatalogo") %>';
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>