Ext.onReady(function() {
var bandera=true;
var stores;
var totalDocumentos=0;
var totalMontoDocumentos=0;
var dataT;
var conTotal=0;
var ivaTotal=0;
var contraTotal=0;
var banderaAjustesFijo=false;
var banderaAjustesRango=false;

var miBandera = Ext.get('bandera').getValue();
	if(miBandera=='true'){
		bandera=true;
	}else{
		bandera=false;
	}
	
var textoGrid;

//--------------------Handlers---------------------------------

var botonesIniciales = function (){
	
	Ext.getCmp('btnGenerarPDF').enable();
	Ext.getCmp('btnBajarPDF').hide();
	Ext.getCmp('btnTotales').enable();
	Ext.getCmp('btnAjuste').enable();
	Ext.getCmp('btnGenerarPDF').enable();
	Ext.getCmp('btnGenerarPDF2').enable();
	Ext.getCmp('btnGenerarPDF3').enable();
	Ext.getCmp('btnDetalle').enable();
	Ext.getCmp('btnVerDetalle').enable();

}
var botonesIniciales2 = function (){
	Ext.getCmp('btnGenerarArchivo2').enable();
	Ext.getCmp('btnBajarArchivo2').hide();
	Ext.getCmp('btnGenerarPDF2').enable();
	Ext.getCmp('btnBajarPDF2').hide();
	Ext.getCmp('btnTotales2').enable();
}
var botonesIniciales3 = function (){
	
	
	Ext.getCmp('btnGenerarArchivo3').enable();
	Ext.getCmp('btnBajarArchivo3').hide();
	Ext.getCmp('btnGenerarPDF3').enable();
	Ext.getCmp('btnBajarPDF3').hide();

}
var procesarBanco= function(){
	Ext.getCmp('Banco').setValue('1');
}

var botonesError= function(){
	Ext.getCmp('btnTotales').disable();
	Ext.getCmp('btnAjuste').disable();
	Ext.getCmp('btnGenerarPDF').disable();
	Ext.getCmp('btnGenerarPDF2').disable();
	Ext.getCmp('btnGenerarPDF3').disable();
	Ext.getCmp('btnDetalle').disable();
	Ext.getCmp('btnVerDetalle').disable();
}
var procesarSuccessFailureGenerarArchivo2 = function(opts, success, response){
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo2');
		btnGenerarArchivo.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo2');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

var procesarSuccessFailureGenerarPDF2 = function(opts,success,response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF2');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarPDF2');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

var procesarSucnocessFailureGenerarArchivo = function(opts, success, response){
		verDetalles.show();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnDetalleR = Ext.getCmp('detalleR');
			var btnDetalleF= Ext.getCmp('detalleF');
			Ext.getCmp('btnVerDetalle').setIconClass('');
			
			if(Ext.util.JSON.decode(response.responseText).urlArchivo2==''){
				btnDetalleR.hide();
			}else{
				btnDetalleR.setHandler(function(boton,evento){
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo2;
					forma.submit();
				});
			}
			if(Ext.util.JSON.decode(response.responseText).urlArchivo1==''){
				btnDetalleF.hide();
			}else{
				btnDetalleF.setHandler(function(boton,evento){
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo1;
					forma.submit();
				});
			}
			if(Ext.util.JSON.decode(response.responseText).urlArchivo1==''&&Ext.util.JSON.decode(response.responseText).urlArchivo2==''){
							NE.util.mostrarConnError(response,opts);

				}
			
		}
	}

var procesarSuccessFailureGenerarArchivo3 = function(opts, success, response){
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo3');
		btnGenerarArchivo.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo3');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

var procesarSuccessFailureGenerarPDF3 = function(opts,success,response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF3');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarPDF3');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPDF = function(opts,success,response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPDF = function(opts,success,response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

var procesarConsultaData = function(store,arrRegistros,opts){
	pnl.el.unmask();
	gridTotalesA.hide();
	if(arrRegistros!=null){
		var el = grid.getGridEl();
		botonesIniciales();
		if (!grid.isVisible()){
			grid.show();
		}
			if(store.getTotalCount()>0){
				//Botones
				
				//Fin botones
				el.unmask();
				Ext.getCmp('btnVerDetalle').setVisible(store.reader.jsonData.detalle);
				grid.setTitle('<center>IF: '+store.getAt(0).get('NOMBREIF')+'</center>');
				var cm = grid.getColumnModel();
				totalDocumentos=0;
				totalMontoDocumentos=0;
				for(var i=0;i<arrRegistros.length;i++){
					totalDocumentos+=parseInt(store.getAt(i).get('DOCTOSVIGENTES'));
					totalMontoDocumentos+=parseFloat(store.getAt(i).get('MONTODOCUMENTOS'));
				}
				if(bandera){
					cm.setHidden(cm.findColumnIndex('CG_ESTATUS_PAGADO'), true);
				}else{
					cm.setHidden(cm.findColumnIndex('CG_ESTATUS_PAGADO'), false);
				}
			}else{
				
					
					el.mask('No se encontr� ning�n registro', 'x-mask');
					botonesError();
			}
			
	}

	
	gridTotalesA.setTitle('<center>SUMA DE TOTALES '+Ext.getCmp('cbMoneda').lastSelectionText+'</center>');
	
	if(dataTotal.getAt(0)==undefined){
	var regi = Ext.data.Record.create(['NUMDOCUMENTOS', 'TOTAL_REGISTROS','TOTAL_MONTO_DOCUMENTOS']);
  dataTotal.add(	new regi({NUMDOCUMENTOS:'',TOTAL_REGISTROS: '',TOTAL_MONTO_DOCUMENTOS: ''}) );
	}
	var reg = dataTotal.getAt(0);
	reg.set('NUMDOCUMENTOS',totalDocumentos);
	reg.set('TOTAL_REGISTROS',totalMontoDocumentos);
	reg.set('TOTAL_MONTO_DOCUMENTOS',store.reader.jsonData.GRANTOTALMONTOCOMISION);
	reg.commit();
}

	var procesarConsultaDetalle = function(store,arrRegistros,opts){
		gridTotalesDetalles.setVisible(false);
		panel.setVisible(false);
		botonesIniciales3();
		if(arrRegistros!=null){
		var el = gridDetalle.getGridEl();
		ventanaDetalle.setTitle('<center>RESUMEN PARA COBRO</center>');
		
			if(store.getTotalCount()>0){
				el.unmask();
				
				gridDetalle.setTitle('<center>Intermediario Financiero '+store.getAt(0).get('NOMBREIF')+'</center>');
				conTotal=0;
				ivaTotal=0;
				contraTotal=0;
				for(var i=0;i<arrRegistros.length;i++){
					conTotal+=parseFloat(store.getAt(i).get('CONTRAPRESTACION'));
					ivaTotal+=parseFloat(store.getAt(i).get('IVA'));
					contraTotal+=parseFloat(store.getAt(i).get('CONTRAPRESTACIONTOTAL'));
				}
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					botonesError();
			}
	
	}
	gridTotalesDetalles.setTitle('GRAN TOTAL');
	if(dataDetalles.getAt(0)==undefined){
	var regi = Ext.data.Record.create(['CONTRAPRESTACION', 'IVA','CONTRAPRESTACION_TOTAL']);
  dataDetalles.add(	new regi({CONTRAPRESTACION:'',IVA: '',CONTRAPRESTACION_TOTAL: ''}) );
	}
	var reg = dataDetalles.getAt(0);
	reg.set('CONTRAPRESTACION',conTotal);
	reg.set('IVA',ivaTotal);
	reg.set('CONTRAPRESTACION_TOTAL',contraTotal);
	reg.commit();
	}

	var procesarTotalesAnual = function(store,arrRegistros,opts){
		var gridTot=Ext.getCmp('gridTotalesAnual');
		
		gridTot.setTitle('<center>TOTALES '+Ext.getCmp('cbMoneda').lastSelectionText+
		'</center><br/>SALDO PROMEDIO DE REFERENCIA: '+Ext.util.Format.number(store.getAt(0).get('SALDO_REFERENCIA'),'$0,0.00'));
										
	}
	
	var procesarConsultaAjuste = function(store,arrRegistros,opts){
		var el = gridAjuste.getGridEl();
		gridTotalesAnual.setVisible(false);
		botonesIniciales2();
		panel.setVisible(true);
		if(arrRegistros.length!=0){
			ventanaDetalle.setTitle('<center>AJUSTE ANUAL ('+Ext.getCmp('anio_2').lastSelectionText+')- '+store.getAt(0).get('NOMBREIF')+'</center>');
				if(store.getTotalCount()>0){
					//Calcula Totales
					
					el.unmask();
					
				}else{
						el.mask('No se encontr� ning�n registro', 'x-mask');
						Ext.getCmp('btnGenerarArchivo2').disable();
						Ext.getCmp('btnGenerarPDF2').disable();
						Ext.getCmp('btnTotales2').disable();
	
				}
		
		}else{
						el.mask('No se encontr� ning�n registro', 'x-mask');
						Ext.getCmp('btnGenerarArchivo2').disable();
						Ext.getCmp('btnGenerarPDF2').disable();
						Ext.getCmp('btnTotales2').disable();
				}
	}

//--------------------STORES--------------------------------




var catalogoAnio = new Ext.data.ArrayStore({
        fields: [
            'anio'
        ],
				
        data: [[2002],[2003],[2004],[2005],[2006],[2007],[2008],[2009],[2010],[2011],[2012],[2013],[2014],[2015]]
				
    });

var catalogoMes = new Ext.data.ArrayStore({
        fields: [
            'myId',
            'displayText'
        ],
        data: [[1, 'Enero'], [2, 'Febrero'],[3, 'Marzo'],
				[4, 'Abril'],[5, 'Mayo'],[6, 'Junio'],[7, 'Julio'],
				[8, 'Agosto'],[9, 'Septiembre'],[10, 'Octubre'],
				[11, 'Noviembre'],[12, 'Diciembre']]
    });


var consulta = new Ext.data.GroupingStore({
						root: 'registros',
						url : '15comision2ext.data.jsp',
						baseParams: {
							informacion: 'Consulta'
						},
						reader: new Ext.data.JsonReader({
								root : 'registros',	totalProperty: 'total',
								fields: [
										{name: 'IC_NOMBRE'},
										{name: 'NOMBREEPO'},
										{name: 'IC_MES_CALCULO'},
										{name: 'DOCTOSVIGENTES'},
										{name: 'MONEDA'},
										{name: 'MONTODOCUMENTOS'},
										{name: 'FG_PORCENTAJE_APLICADO'},
										{name: 'FG_CONTRAPRESTACION'},
										{name: 'CG_ESTATUS_PAGADO'},
										{name: 'NOMBREIF'},
										{name: 'GRANTOTALMONTOCOMISION'}
										
								]
						}),
						groupField: 'IC_NOMBRE',	sortInfo:{field: 'NOMBREEPO', direction: "ASC"},
						autoLoad: false,
						listeners: {
							load: procesarConsultaData,
							exception: {
								fn: function(proxy, type, action, optionsRequest, response, args) {
									NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
									procesarConsultaData(null, null, null);
								}
							}
						}
					});
					
var consultaDetalle = new Ext.data.GroupingStore({
	root: 'registros',
	url: '15comision2ext.data.jsp',
	baseParams: {
		informacion: 'Detalle'
	},
	reader: new Ext.data.JsonReader({
	root : 'registros',	totalProperty: 'total',
	fields: [
				{name: 'NOMBREIF'},
				{name: 'IC_NOMBRE'},
				{name: 'IC_MES_CALCULO'},
				{name: 'CONTRAPRESTACION'},
				{name: 'IVA_POR'},
				{name: 'IVA'},
				{name: 'CONTRAPRESTACIONTOTAL'},
				{name: 'CG_ESTATUS_PAGADO'}
	]
	}),
	groupField: 'IC_NOMBRE',	sortInfo:{field: 'IC_NOMBRE', direction: "ASC"},
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaDetalle,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaDetalle(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});


var consultaAjuste = new Ext.data.GroupingStore({
	root: 'registros',
	url: '15comision2ext.data.jsp',
	baseParams: {
		informacion: 'Ajuste'
	},
	reader: new Ext.data.JsonReader({
	root : 'registros',	totalProperty: 'total',
	fields: [
				{name: 'NOMBREIF'},
				{name: 'IC_NOMBRE'},
				{name: 'IC_MES_CALCULO'},
				{name: 'FG_SALDO_PROMEDIO'},
				{name: 'FG_PORCENTAJE_APLICADO'},
				{name: 'FG_PORCENTAJE_AJUSTADO'},
				{name: 'FG_CONTRAPRESTACION'},
				{name: 'IVA'},
				{name: 'FG_CONTRAPRESTACION'},
				{name: 'FG_CONTRAPRESTACION_AJUSTADA'},
				{name: 'CG_ESTATUS_PAGADO'},
				{name: 'CG_TIPO_COMISION'}
	]
	}),
	groupField: 'IC_NOMBRE',	sortInfo:{field: 'IC_NOMBRE', direction: "ASC"},
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaAjuste,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaAjuste(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

	var consultaAjusteTotales = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields: [
			{name: 'TOTAL_SALDO'},
			{name: 'TOTAL_COBRADA'},
			{name: 'TOTAL_TOT_COBRADA'},
			{name: 'TOTAL_AJUSTADA'},
			{name: 'TOTAL_TOT_AJUSTADA'},
			{name: 'TOTAL_AJUSTE'},
			{name: 'SALDO_REFERENCIA'}
		],
		url : '15comision2ext.data.jsp',
		baseParams: {
			informacion: 'AjusteTotales'
		},
		autoLoad: false,
		listeners: {
		load: procesarTotalesAnual,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							}
					}
		}
	});


var catalogoBanco = new Ext.data.JsonStore
  ({
	   id: 'catologoBancoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15comision2ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoBancoDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo,
		 load: procesarBanco
		}
  });
	
var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catologoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15comision2ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });



	var dataTotal=new Ext.data.ArrayStore({
				autoLoad: false,
        fields: [
            'NUMDOCUMENTOS',
						'TOTAL_REGISTROS',
						'TOTAL_MONTO_DOCUMENTOS'
        ]
				
        //data: [[totalDocumentos,totalMontoDocumentos,store.reader.jsonData.GRANTOTALMONTOCOMISION]]
				
    });
		
		var dataDetalles=new Ext.data.ArrayStore({
				autoLoad: false,
        fields: [
            'CONTRAPRESTACION',
						'IVA',
						'CONTRAPRESTACION_TOTAL'
        ]
				
        //data: [[totalDocumentos,totalMontoDocumentos,store.reader.jsonData.GRANTOTALMONTOCOMISION]]
				
    });
		
var gridTotalesA = new Ext.grid.EditorGridPanel({
		store: dataTotal,
		id: 'gridTotalesA',
		hidden:	true,
		style: 'margin: 0 auto;',
		columns: [
			{
				header: 'Num. Documentos',
				dataIndex: 'NUMDOCUMENTOS',
				align: 'left',	width: 240
			},{
				header: 'Monto de los documentos',
				dataIndex: 'TOTAL_REGISTROS',
				width: 240,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto de la comisi�n',
				dataIndex: 'TOTAL_MONTO_DOCUMENTOS',
				width: 240,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			}
		],
		width: 800,
		height: 100,
		title: '',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	});

var gridTotalesDetalles = new Ext.grid.EditorGridPanel({
		store: dataDetalles,
		id: 'gridTotalesDetalles',
		hidden:	true,
		title: 'GRAN TOTAL',
		style: 'margin: 0 auto;',
		columns: [
			{
				header: 'Contraprestaci�n',
				dataIndex: 'CONTRAPRESTACION',
				align: 'right',	width: 240,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')

			},{
				header: 'IVA',
				dataIndex: 'IVA',
				width: 240,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Contraprestaci�n Total',
				dataIndex: 'CONTRAPRESTACION_TOTAL',
				width: 240,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			}
		],
		width: 800,
		height: 100,
		title: '',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	});

var gridTotalesAnual = new Ext.grid.EditorGridPanel({
		store: consultaAjusteTotales,
		id: 'gridTotalesAnual',
		title: '',
		hidden:	true,
		style: 'margin: 0 auto;',
		columns: [
			{
				header: 'Saldo <br/>Prom.',
				dataIndex: 'TOTAL_SALDO',
				align: 'right',	width: 145,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Contraprestaci�n<br/>Cobrada',
				dataIndex: 'TOTAL_COBRADA',
				width: 145,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Contraprestaci�n <br/>Total Cobrada',
				dataIndex: 'TOTAL_TOT_COBRADA',
				width: 145,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},
			{
				header: 'Contraprestaci�n<br/> Ajustada',
				dataIndex: 'TOTAL_AJUSTADA',
				width: 145,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},
			{
				header: 'Contraprestaci�n<br/> Total Ajustada',
				dataIndex: 'TOTAL_TOT_AJUSTADA',
				width: 145,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},
			{
				header: 'Ajuste',
				dataIndex: 'TOTAL_AJUSTE',
				width: 145,	align: 'right',
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {
													return '<font color="#FF0000"><strong>'+Ext.util.Format.number(value,'$ 0,0.00')+'</strong></font>';
						
											}
				

			}
		],
		width: 900,
		height: 100,
		title: '',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	});
//--------------------Componentes-------------------------------



var dt= Date();
var anio =Ext.util.Format.date(dt,'Y');
anio--;
var mes =Ext.util.Format.date(dt,'m');
mes--;
if(mes<1){
mes=12;
anio--;
}

var elementosForma = [
	{
				//Banco
				xtype: 'combo',
				fieldLabel: 'Banco de Fodeo',
				emptyText: 'Seleccionar',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				allowBlank: false,
				minChars: 1,
				store: catalogoBanco,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'HBanco',
				id: 'Banco',
				mode: 'local',
				hiddenName: 'HBanco',
				forceSelection: true
    },{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			name:'HcbMoneda',
			id: 'cbMoneda',
			mode: 'local',
			forceSelection : true,
			allowBlank: false,
			hiddenName: 'HcbMoneda',
			hidden: false,
			emptyText: 'Seleccionar Moneda',
			store: catalogoMoneda,
			tpl: NE.util.templateMensajeCargaCombo
		},{
			xtype: 'compositefield',
			id:'mesAnio',
			
			items:[
				{
					xtype: 'combo',
					value: mes,
					editable:false,
					fieldLabel: 'Mes de c�lculo',
					displayField: 'displayText',
					valueField: 'myId',
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					name:'Hmes',
					store: catalogoMes,
					id: 'mes',
					mode: 'local',
					hiddenName: 'Hmes',
					hidden: false,
					emptyText: 'Seleccionar Mes'
		},{	
					xtype: 'combo',
					displayField: 'anio',
					value: anio+1,
					//value: Ext.util.Format.date(dt,'Y'),
					editable:false,
					valueField: 'anio',
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					name:'Hanio',
					id: 'anio',
					store: catalogoAnio,
					mode: 'local',
					hiddenName: 'Hanio',
					hidden: false,
					emptyText: 'Seleccionar A�o'
				}
			
			]
			 		
		},
		{	
					xtype: 'combo',
					displayField: 'anio',
					height: 40,
					editable:false,
					value: anio,
					//value: Ext.util.Format.date(dt,'Y'),
					fieldLabel: 'A�o de c�lculo',
					valueField: 'anio',
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					name:'Hanio_2',
					id: 'anio_2',
					store: catalogoAnio,
					mode: 'local',
					hiddenName: 'Hanio_2',
					hidden: true,
					emptyText: 'Seleccionar A�o'
				}

];





var grid = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,height: 300,	width: 800,frame: true,style: 'margin: 0 auto;',
							title: ' ',
							hidden: true,
							store: consulta,
							columns: [
								{header: '',tooltip: '',hidden: true,	dataIndex: 'NOMBREEPO',	width: 150,	align: 'center', hideable:false,
								renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
													return value+'PRODUCTO NAFIN: '+registro.get('IC_NOMBRE');
											}
								},
								{header: '',tooltip: '',hidden: true,dataIndex: 'IC_NOMBRE',	width: 150,	align: 'center', hideable:false,
								renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
													if(registro.get('NOMBREEPO')=='-'){
													//	return value;
													return value;
													}else
													
													return registro.get('NOMBREEPO')+'<br/>PRODUCTO NAFIN: '+value;
													//return registro.get('NOMBREEPO')+'<br/>PRODUCTO NAFIN: '+value;
											}
								},
								{header: 'Mes',tooltip: '',	dataIndex: 'IC_MES_CALCULO',	width: 110,	align: 'center',
								renderer: function(value, metadata, record, rowindex, colindex, store) {
									if(bandera){
										return Date.getShortMonthName(parseFloat(value)-1)+' '+Ext.getCmp('anio').lastSelectionText;
									}else{
										return Date.getShortMonthName(parseFloat(value)-1)+' '+Ext.getCmp('anio_2').lastSelectionText;

									}
								}},
								{header: 'Num. Doctos',tooltip: '',	dataIndex: 'DOCTOSVIGENTES',	width: 110,	align: 'center'},
								{header: 'Moneda',tooltip: '',id: 'colMoneda',width: 150,	align: 'center', renderer:
										function(value, metadata, record, rowindex, colindex, store) { 
											return Ext.getCmp('cbMoneda').lastSelectionText;
											}},
								{header: 'Monto de Documentos',tooltip: '',	dataIndex: 'MONTODOCUMENTOS',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00') },
								{header: 'Porcentaje comisi�n',tooltip: '',	dataIndex: 'FG_PORCENTAJE_APLICADO',	width: 110,	align: 'center',renderer: Ext.util.Format.numberRenderer('0.00%')},
								{header: 'Monto Comisi�n',tooltip: '',	dataIndex: 'FG_CONTRAPRESTACION',	width: 110,	align: 'right',renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
													return '$'+value;
						
											}},
								{header: 'Pagado',tooltip: '', id:'pago',name:'pago',hiddenName: 'pago',	dataIndex: 'CG_ESTATUS_PAGADO',	width: 110,	align: 'center'}
							],
							view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{[values.rs[0].data["NOMBREEPO"] != "-" ? "EPO" : "PRODUCTO NAFIN"]} {text}'}),
							bbar: {
								items: [
									'->',
									{
										xtype: 'button',
										text: 'Totales',
										id: 'btnTotales',
										hidden: false,
										handler: function(boton, evento) {									
												Ext.getCmp('gridTotalesA').show();
										}
									},{
										xtype: 'button',
										text: 'Ver Detalle',
										id: 'btnVerDetalle',
										handler: function(boton, evento) {
	
				
											boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
												url: '15detalleComisionIfext.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'archivo'
												}),
												callback: procesarSucnocessFailureGenerarArchivo
											});
										}
									},{
										xtype: 'button',
										text: 'Resumen para cobro',
										id: 'btnDetalle',
										hidden: false,
										handler: function(boton, evento) {
										consultaDetalle.load({ params: Ext.apply(fp.getForm().getValues(),{ bandera: bandera})});
										ventanaDetalle.show();
										gridDetalle.show();											
										}
									},
									{
										xtype: 'button',
										text: 'Ajuste Anual',
										id: 'btnAjuste',
										hidden: false,
										handler: function(boton, evento) {
										
										consultaAjuste.load({ params: Ext.apply(fp.getForm().getValues(),{ bandera: bandera})});
										ventanaDetalle.show();
										consultaAjusteTotales.load({ params: Ext.apply(fp.getForm().getValues(),{ bandera: bandera})});
										
										gridAjuste.show();
										
											/*var totalesACmp = Ext.getCmp('gridTotalesA');
											if (!totalesACmp.isVisible()) {
												totalesACmp.show();
												totalesACmp.el.dom.scrollIntoView();
											}*/
											
										}
									},{
										xtype: 'button',
										text: 'Generar PDF',
										//iconCls: 'icoPdf',
										id: 'btnGenerarPDF',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											var reg = dataTotal.getAt(0);
											
											Ext.Ajax.request({
												url: '15comision2extPDF.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'ConsultaPDF',
													bandera: bandera,
													numDoc: reg.get('NUMDOCUMENTOS'),
													totReg: Ext.util.Format.number(reg.get('TOTAL_REGISTROS'),'0,0.00'),
													totMont: Ext.util.Format.number(reg.get('TOTAL_MONTO_DOCUMENTOS'),'0,0.00'),
													moneda:Ext.getCmp('cbMoneda').lastSelectionText
												}),
												callback: procesarSuccessFailureGenerarPDF
											});
										}
									},{
										xtype: 'button',
										text: 'Bajar PDF',
										id: 'btnBajarPDF',
										hidden: true
									}]
								}
						});
						

var gridAjuste = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,		
							height: 400,	width: 900,
							frame: true,style: 'margin: 0 auto;',
							title: ' ',
							hidden: true,
							store: consultaAjuste,
							columns: [
								{header: 'Ajuste correspondiente al tipo de Comisi�n',tooltip: '',hidden: true,dataIndex: 'IC_NOMBRE',	width: 150,	align: 'center', hideable:false,
								renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
													if(registro.get('CG_TIPO_COMISION')=='F'){								
														return 'Fija<br/>'+'PRODUCTO NAFIN: '+registro.get('IC_NOMBRE');
													}else{	
															return 'Por Rangos<br/>'+'PRODUCTO NAFIN: '+registro.get('IC_NOMBRE');
													}
												}
								},
								{header: 'Mes',tooltip: '',	dataIndex: 'IC_MES_CALCULO',	width: 120,	align: 'center',
								renderer: function(value, metadata, record, rowindex, colindex, store) {
															return Date.getShortMonthName(parseFloat(value)-1)+' '+Ext.getCmp('anio_2').lastSelectionText;

								}},
								{header: 'Saldo <br/>Prom.',tooltip: '',	dataIndex: 'FG_SALDO_PROMEDIO',	width: 110,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
								{header: 'Tasa <br/>Aplicada',tooltip: '',	dataIndex: 'FG_PORCENTAJE_APLICADO',	width: 110,	align: 'center',renderer: Ext.util.Format.numberRenderer('0.00%')},
								{header: 'Tasa <br/>Ajustada',tooltip: '',	dataIndex: 'FG_PORCENTAJE_AJUSTADO',	width: 110,	align: 'center',renderer: Ext.util.Format.numberRenderer('0.00%')},
								{header: 'Contraprestaci�n <br/>Cobrada',tooltip: '',	dataIndex: 'FG_CONTRAPRESTACION',	width: 170,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
								{header: 'IVA',tooltip: '',dataIndex: 'IVA',width: 150,	align: 'center',renderer: Ext.util.Format.numberRenderer('0%')},
								{header: 'Contraprestaci�n <br/>Total Cobrada',tooltip: '',	dataIndex: 'FG_CONTRAPRESTACION',	width: 160,	align: 'right',renderer: function(value, metadata, record, rowindex, colindex, store) {
															return Ext.util.Format.number(value*1.15,'$0,0.00');
								}},
								{header: 'Contraprestaci�n <br/>Ajustada',tooltip: '',	dataIndex: 'FG_CONTRAPRESTACION_AJUSTADA',	width: 160,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
								{header: 'IVA',tooltip: '',dataIndex: 'IVA',width: 150,	align: 'center',renderer: Ext.util.Format.numberRenderer('0%')},
								{header: 'Contraprestaci�n <br/>Total Ajustada',tooltip: '',	dataIndex: 'FG_CONTRAPRESTACION_AJUSTADA',	width: 160,	align: 'right',renderer: function(value, metadata, record, rowindex, colindex, store) {
															return Ext.util.Format.number(value*1.15,'$0,0.00');
								}},
								{header: 'Ajuste',tooltip: '',	dataIndex: 'FG_CONTRAPRESTACION',	width: 160,	align: 'right', renderer: function(value, metadata, record, rowindex, colindex, store) {
															var ajuste = (record.get('FG_CONTRAPRESTACION_AJUSTADA')-record.get('FG_CONTRAPRESTACION'))*1.15;
															 return Ext.util.Format.number(ajuste,'$0,0.00');
								}},
								{header: 'Pagado',tooltip: '', id:'pago',name:'pago',hiddenName: 'pago',	dataIndex: 'CG_ESTATUS_PAGADO',	width: 160,	align: 'center'}
							],
							view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),

							bbar: {
								id: 'barra1',
								items: [
									'->','-',
									
									{
										xtype: 'button',
										text: 'Generar Archivo',
										id: 'btnGenerarArchivo2',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											var reg =consultaAjusteTotales.getAt(0);
											Ext.Ajax.request({
												url: '15comision2extPDF.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'AjusteCSV',
													mesText: Ext.getCmp('mes').lastSelectionText,
													bandera: bandera,
													SalRef: Ext.util.Format.number(reg.get('SALDO_REFERENCIA'),'0,0.00')
												}),
												callback: procesarSuccessFailureGenerarArchivo2
											});
										}
									},
									{
										xtype: 'button',
										text: 'Bajar Archivo',
										id: 'btnBajarArchivo2',
										hidden: true
									}
									,{
										xtype: 'button',
										text: 'Generar PDF',
										//iconCls: 'icoPdf',
										id: 'btnGenerarPDF2',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											var reg =consultaAjusteTotales.getAt(0);
											Ext.Ajax.request({
												url: '15comision2extPDF.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'AjustePDF',
													mesText: Ext.getCmp('mes').lastSelectionText,
													bandera: bandera,
													totSal: Ext.util.Format.number(reg.get('TOTAL_SALDO'),'0,0.00'),
													totCob: Ext.util.Format.number(reg.get('TOTAL_COBRADA'),'0,0.00'),
													totTotCob: Ext.util.Format.number(reg.get('TOTAL_TOT_COBRADA'),'0,0.00'),
													totAjus: Ext.util.Format.number(reg.get('TOTAL_AJUSTADA'),'0,0.00'),
													totTotAjus: Ext.util.Format.number(reg.get('TOTAL_TOT_AJUSTADA'),'0,0.00'),
													totA: Ext.util.Format.number(reg.get('TOTAL_AJUSTE'),'0,0.00'),
													SalRef: Ext.util.Format.number(reg.get('SALDO_REFERENCIA'),'0,0.00'),
													moneda:Ext.getCmp('cbMoneda').lastSelectionText
												}),
												callback: procesarSuccessFailureGenerarPDF2
											});
										}
									},{
										xtype: 'button',
										text: 'Bajar PDF',
										id: 'btnBajarPDF2',
										hidden: true
									}
									,{
										xtype: 'button',
										text: 'Totales',
										id: 'btnTotales2',
										hidden: false,
										handler: function(boton, evento) {
										//consultaAjusteTotales.load({ params: Ext.apply(fp.getForm().getValues(),{ bandera: bandera})});
										var gridTot=Ext.getCmp('gridTotalesAnual');
		
										
										gridTot.show();
											
										}
									},
									 {
									  //Bot�n Limpiar
									  xtype: 'button',
									  text: 'Salir',
									  name: 'btnLimpiar',
									  hidden: false,
									
									  handler: function(boton, evento) 
									  {
										ventanaDetalle.hide();
										
										//window.location = '15IFPublicacionProveedoresext.jsp'
									  }
									}
								]
							}
						});

var gridDetalle = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,		
							height: 300,	width: 800,
							frame: true,style: 'margin: 0 auto;',
							title: ' ',
							hidden: true,
							store: consultaDetalle,
							columns: [
								{header: 'PRODUCTO NAFIN',tooltip: 'PRODUCTO NAFIN',hidden: true,dataIndex: 'IC_NOMBRE',	width: 150,	align: 'center', hideable:false
								},
								{header: 'Mes C�lculo',tooltip: '',	dataIndex: 'IC_MES_CALCULO',	width: 110,	align: 'center',
								renderer: function(value, metadata, record, rowindex, colindex, store) {
															return Ext.getCmp('mes').lastSelectionText+' '+Ext.getCmp('anio').lastSelectionText;

								}},
								{header: 'Contraprestaci�n',tooltip: '',	dataIndex: 'CONTRAPRESTACION',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
								{header: '% IVA',tooltip: '',dataIndex: 'IVA_POR',width: 150,	align: 'right',
								renderer: function(value, metadata, record, rowindex, colindex, store) {
															return value*100+'%';
								}},
								{header: 'IVA',tooltip: '',	dataIndex: 'IVA',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00') },
								{header: 'Contraprestacion total',tooltip: '',	dataIndex: 'CONTRAPRESTACIONTOTAL',	width: 110,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
								{header: 'Pagado',tooltip: '',	dataIndex: 'CG_ESTATUS_PAGADO',	width: 110,	align: 'center'}
							],
							view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),

							bbar: {
								id:'barra2',
								items: [
									'-','->',
									{
										xtype: 'button',
										text: 'Totales',
										id: 'btnTotales3',
										hidden: false,
										handler: function(boton, evento) {
			
										Ext.getCmp('gridTotalesDetalles').show();
										}
									},
									{
										xtype: 'button',
										text: 'Generar Archivo',
										id: 'btnGenerarArchivo3',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
												url: '15comision2extPDF.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'DetalleCSV',
													mesText: Ext.getCmp('mes').lastSelectionText,
													bandera: bandera
												}),
												callback: procesarSuccessFailureGenerarArchivo3
											});
										}
									},
									{
										xtype: 'button',
										text: 'Bajar Archivo',
										id: 'btnBajarArchivo3',
										hidden: true
									},
									{
										xtype: 'button',
										text: 'Generar PDF',
										//iconCls: 'icoPdf',
										id: 'btnGenerarPDF3',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											var reg = dataDetalles.getAt(0);
											Ext.Ajax.request({
												url: '15comision2extPDF.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'DetallePDF',
													mesText: Ext.getCmp('mes').lastSelectionText,
													bandera: bandera,
													totalContra: Ext.util.Format.number(reg.get('CONTRAPRESTACION'),'0,0.00'),
													totalIva:Ext.util.Format.number(reg.get('IVA'),'0,0.00'),
													totalContaTot:Ext.util.Format.number(reg.get('CONTRAPRESTACION_TOTAL'),'0,0.00')
												}),
												callback: procesarSuccessFailureGenerarPDF3
											});
										}
									},{
										xtype: 'button',
										text: 'Bajar PDF',
										id: 'btnBajarPDF3',
										hidden: true
									},{
									  //Bot�n Limpiar
									  xtype: 'button',
									  text: 'Salir',
									  name: 'btnLimpiar',
									  hidden: false,
									
									  handler: function(boton, evento) 
									  {
										ventanaDetalle.hide();
										
										//window.location = '15IFPublicacionProveedoresext.jsp'
									  }
									}]
								}
						});
						
	var panel= new Ext.FormPanel({  
    //renderTo: 'frame',  
    hidden: true,
		defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px', 
    html: 'Importante<br/>Los saldos positivos son a favor de Nacional Fianaciera, S.N.C.'+
		'<br/>Los saldo negativos son a favor del Intermediario Financiero !' 
});  


var ventanaDetalle = new Ext.Window({
			title: '',
			width: 920,
			height: 700,
			maximizable: false,
			closable: false,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			items: [gridDetalle,gridAjuste,gridTotalesAnual,gridTotalesDetalles,panel]
});




var panelDetalles = new Ext.Panel
  ({
	
		hidden: false,
		height: 'auto',
		width: 'auto',
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 160,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		
		monitorValid: true,
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  align: 'center',
			  text: 'Archivo Comisi�n Fijo',
			  id:'detalleF',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{
							/*Ext.Ajax.request({
								url: '15comision2ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{informacion:'Consulta',bandera: bandera })
								//callback: procesaConsulta
							});*/
					}
			 },
			 {
			  //Bot�n 
			  xtype: 'button',
			  align: 'center',
			  text: 'Detalle Validaci�n por Rangos',
			  name: 'detalleR',
				id:'detalleR',
			  hidden: false,
			  handler: function(boton, evento) 
			  {
			   
			   
			
			  }
			 }
		  ]
  });

var verDetalles = new Ext.Window({
			title: 'Ver Detalle',
			width: 300,
			height: 100,
			maximizable: false,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			maximized: false,
			constrain: true,
			items: [panelDetalles]
});
var fp = new Ext.form.FormPanel
  ({
	
		hidden: false,
		height: 'auto',
		
		width: 560,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 160,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		
		monitorValid: true,
		items: [elementosForma],
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{					
							grid.show();
							if(bandera){
								consulta.load({ params: Ext.apply(fp.getForm().getValues(),{ bandera: bandera})});
							}else{
			
								if(Ext.getCmp('anio_2').getValue()>anio){
									Ext.MessageBox.alert('Mensaje de p�gina web','La fecha para realizar el c�lculo debe ser el ultimo d�a del a�o o posterior');
								}
								else{
									consulta.load({ params: Ext.apply(fp.getForm().getValues(),{ bandera: bandera})});
							
								}
							}
					}
			 },
			 {
			  //Bot�n Limpiar
			  xtype: 'button',
			  text: 'Limpiar',
			  name: 'btnLimpiar',
			  hidden: false,
			  iconCls: 'icoLimpiar',
			  handler: function(boton, evento) 
			  {
			   location.reload();
			   
			   //window.location = '15IFPublicacionProveedoresext.jsp'
			  }
			 }
		  ]
  });
	
//--------------Inicio--------------------	
	
var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid,
			gridTotalesA,
			ventanaDetalle
		]
	});
	var miBandera = Ext.get('bandera').getValue();
	if(miBandera=='true'){
		Ext.getCmp('anio_2').hide();
		Ext.getCmp('mesAnio').show();
		Ext.getCmp('btnAjuste').hide();
		bandera=true;
	}else{
		Ext.getCmp('mesAnio').hide();
		Ext.getCmp('anio_2').show();
		Ext.getCmp('btnVerDetalle').hide();
		Ext.getCmp('btnDetalle').hide();
		bandera=false;
	}
catalogoBanco.load();
catalogoMoneda.load();

});