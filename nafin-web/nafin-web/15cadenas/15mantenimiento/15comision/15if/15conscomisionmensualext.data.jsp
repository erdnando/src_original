<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	netropology.utilerias.usuarios.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject,
	java.math.BigDecimal"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/15cadenas/015secsession.jspf"%>
<% 

/*** OBJETOS ***/
com.netro.cadenas.ConsComision paginador;
CQueryHelperRegExtJS queryHelperRegExtJS;
CQueryHelper queryHelper;
CatalogoSimple cat;
JSONObject jsonObj;
Afiliacion afiliacion;
//Registros registros;
JSONArray jsonArr;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
Calendar calFechaActual = new GregorianCalendar();
int anioActual = calFechaActual.get(Calendar.YEAR);
int mesActual = calFechaActual.get(Calendar.MONTH) + 1;  //Dado que 0 es Enero le sumamos 1
int diaActual = calFechaActual.get(Calendar.DAY_OF_MONTH);
int mesInicial = mesActual; //(0 es Enero... por lo tanto el mesInicial es el anterior al mes actual)
int anioInicial = anioActual;

String informacion 			= (request.getParameter("informacion") == null) ? "" : request.getParameter("informacion");
String informacion2 = (request.getParameter("informacion2")==null)?"":request.getParameter("informacion2");
String periodo		 			= (request.getParameter("periodo") == null) ? "Mensual" : request.getParameter("periodo");
String strAccion 				= (request.getParameter("strAccion") == null) ? "" : request.getParameter("strAccion");
String ic_producto_nafin 	= (request.getParameter("ic_producto_nafin") == null) ? "" : request.getParameter("ic_producto_nafin");
String ic_mes_calculo_de 	= (request.getParameter("ic_mes_calculo_de") == null) ? String.valueOf(mesInicial) : request.getParameter("ic_mes_calculo_de");
String ic_mes_calculo_a 	= (request.getParameter("ic_mes_calculo_a") == null) ? String.valueOf(mesInicial) : request.getParameter("ic_mes_calculo_a");
String ic_anio_calculo 		= (request.getParameter("ic_anio_calculo") == null) ? String.valueOf(anioInicial) : request.getParameter("ic_anio_calculo");
String cg_estatus_pagado 	= (request.getParameter("cg_estatus_pagado") == null) ? "1" : request.getParameter("cg_estatus_pagado");
String ic_moneda 				= (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
String ic_banco_fondeo 		= (request.getParameter("ic_banco_fondeo") == null) ? "" : request.getParameter("ic_banco_fondeo");
StringBuffer strCondiciones= new StringBuffer();
String ic_if					=  iNoCliente;
String resultado				= null;
String epoDefault 			= "";
String strSQLConsulta		= "";
int start						= 0;
int limit 						= 2;
AccesoDB con 					= null;
PreparedStatement ps 		= null;
ResultSet rs 					= null;
/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/

/*** INICIO CATALOGO DE BANCO FONDEO ***/
if(informacion.equals("catalogoBancoFondeo")){
	cat = new CatalogoSimple();
	cat.setTabla("comcat_banco_fondeo");
	cat.setCampoClave("ic_banco_fondeo");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setOrden("1");
	resultado = cat.getJSONElementos();
} /*** FIN DEL CATALOGO DE BANCO FONDEO***/

/*** INICIO CATALOGO MONEDA ***/
else if(informacion.equals("catalogoMoneda")){
	CatalogoMoneda catalogo = new CatalogoMoneda();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	resultado = catalogo.getJSONElementos();		
} /*** FIN CATALOGO MONEDA ***/

/*** INICIO CATALOGO PRODUCTO ***/
else if(informacion.equals("catalogoProducto")){
   List inCond = new ArrayList();
   inCond.add("1");
   inCond.add("2");
   inCond.add("4");
   inCond.add("5");
	cat = new CatalogoSimple();
	cat.setTabla("comcat_producto_nafin");
	cat.setCampoClave("ic_producto_nafin");
	cat.setCampoDescripcion("ic_nombre");
	cat.setOrden("1");
	cat.setCondicionIn(inCond);
	resultado = cat.getJSONElementos();	
} /*** FIN CATALOGO PRODUCTO ***/

/*** INICIO CONSULTA ***/
else if(informacion.equals("Consulta") || informacion.equals("ConsultaTotales")) { 
	paginador = new com.netro.cadenas.ConsComision();
	String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
	/* SE PASAN LOS VALORES AL OBJETO PAGINADOR PARA REALIZAR LA CONSULTA */
	paginador.setIc_producto_nafin(ic_producto_nafin);
	paginador.setPeriodo(periodo);
	paginador.setIc_mes_calculo_de(ic_mes_calculo_de);
	paginador.setIc_mes_calculo_a(ic_mes_calculo_a);
	paginador.setIc_anio_calculo(ic_anio_calculo);
	paginador.setCg_estatus_pagado(cg_estatus_pagado);
	paginador.setIc_moneda(ic_moneda);
	paginador.setIc_if(ic_if);
	paginador.setIc_banco_fondeo(ic_banco_fondeo);
	queryHelperRegExtJS	= new CQueryHelperRegExtJS( paginador ); 
	Registros registros = queryHelperRegExtJS.doSearch();
	HashMap	datos;
	List reg=new ArrayList();
	String fecha   = "01/"+ ic_mes_calculo_a + "/" + ic_anio_calculo; // formato fecha: DD/MM/YYYY
   String porcentajeIva   = Iva.getPorcentaje(fecha);
	
	float saldopromedio = 0;
	float contraprestacion = 0;
	float total = 0;
	float ajuste = 0;
	
	if(periodo.equals("Mensual")){		 
		while(registros.next()){
			datos=new HashMap();
			saldopromedio += Float.valueOf(registros.getString("saldopromedio").replaceAll(" ", "")).floatValue();
			contraprestacion += Float.valueOf(registros.getString("contraprestacion").replaceAll(" ","")).floatValue();
			total += Float.valueOf(registros.getString("contraprestaciontotal").replaceAll(" ","")).floatValue();
			
			datos.put("NOMBREIF",registros.getString("nombreif"));
			datos.put("IC_MES_CALCULO",registros.getString("ic_mes_calculo"));
			datos.put("SALDOPROMEDIO",registros.getString("saldopromedio"));
			//datos.put("SALDOPROMEDIOTOTAL",registros.getString("saldopromediototal"));
			datos.put("CONTRAPRESTACION",registros.getString("contraprestacion"));
			datos.put("MONTOIVA",registros.getString("montoiva"));
			datos.put("CONTRAPRESTACIONTOTAL",registros.getString("contraprestaciontotal"));
			datos.put("CG_ESTATUS_PAGADO",registros.getString("cg_estatus_pagado"));
			datos.put("IC_PRODUCTO_NAFIN",registros.getString("ic_producto_nafin"));
			datos.put("IC_NOMBRE",registros.getString("ic_nombre"));
			datos.put("IC_PORCENTAJE_IVA",porcentajeIva);
			reg.add(datos);
		}
	}else if(periodo.equals("Anual")){
		while(registros.next()){
			datos=new HashMap();
			saldopromedio += Float.valueOf(registros.getString("saldopromedio").replaceAll(" ", "")).floatValue();
			contraprestacion += Float.valueOf(registros.getString("contraprestacionajustada").replaceAll(" ","")).floatValue();
			total += Float.valueOf(registros.getString("contraprestaciontotalajustada").replaceAll(" ","")).floatValue();
			ajuste += Float.valueOf(registros.getString("ajuste").replaceAll(" ","")).floatValue();
			
			datos.put("NOMBREIF",registros.getString("nombreif"));
			datos.put("IC_PRODUCTO_NAFIN",registros.getString("ic_producto_nafin"));
			datos.put("IC_NOMBRE",registros.getString("ic_nombre"));
			datos.put("FG_SALDO_REFERENCIA",registros.getString("fg_saldo_referencia"));
			datos.put("FG_PROCENTAJE_AJUSTADO",registros.getString("fg_porcentaje_ajustado"));
			datos.put("CG_ESTATUS_PAGADO",registros.getString("cg_estatus_pagado"));
			datos.put("SALDOPROMEDIO",registros.getString("saldopromedio"));
			datos.put("CONTRAPRESTACIONAJUSTADA",registros.getString("contraprestacionajustada"));
			datos.put("CONTRAPRESTACIONTOTALAJUSTADA",registros.getString("contraprestaciontotalajustada"));
			datos.put("AJUSTE",registros.getString("ajuste"));
			datos.put("IC_PORCENTAJE_IVA",porcentajeIva);
			reg.add(datos);
		}
	}
	//resultado=  "{\"success\": true, \"total\": \"" + registros.getNumeroRegistros() + "\", \"registros\": " + registros.getJSONData()+"}";
	jsonObj = new JSONObject();
	if(informacion.equals("Consulta")){		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		resultado=jsonObj.toString();
		//String per = periodo;
	}
	//System.out.println(per);
	
	if(operacion.equals("XLS")||operacion.equals("PDF")){
		paginador.setPeriodo(periodo);
		jsonObj = new JSONObject();
		String nombreArchivo = queryHelperRegExtJS.getCreateCustomFile(request, strDirectorioTemp,operacion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
	}else if(informacion.equals("ConsultaTotales") && periodo.equals("Mensual")){
		List reg1=new ArrayList();
		HashMap	datos1;
		datos1=new HashMap();
		
		datos1.put("SALDOPROMEDIO_TOTAL",Float.toString(saldopromedio));
		datos1.put("CONTRAPRESTACION_TOTAL",Float.toString(contraprestacion));
		datos1.put("CONTRAPRESTACIONTOTAL_TOTAL",Float.toString(total));
		reg1.add(datos1);
		
		String resultado2 = "{\"success\": true, \"total\": \"" + reg1.size() + "\", \"registros\": " + reg1.toString()+"}";
		jsonObj = JSONObject.fromObject(resultado2);	
		resultado = jsonObj.toString();	
	}else if(informacion.equals("ConsultaTotales") && periodo.equals("Anual")){
		List reg1=new ArrayList();
		HashMap	datos1;
		datos1=new HashMap();
		
		datos1.put("SALDOPROMEDIO_TOTAL",Float.toString(saldopromedio));
		datos1.put("CONTRAPRESTACIONAJUSTADA_TOTAL",Float.toString(contraprestacion));
		datos1.put("CONTRAPRESTACIONTOTAL_TOTAL",Float.toString(total));
		datos1.put("AJUSTE_TOTAL",Float.toString(ajuste));
		reg1.add(datos1);
		
		String resultado2 = "{\"success\": true, \"total\": \"" + reg1.size() + "\", \"registros\": " + reg1.toString()+"}";
		jsonObj = JSONObject.fromObject(resultado2);	
		resultado = jsonObj.toString();	
	}
} /*** FIN DE CONSULTA ***/

%>

<%= resultado%>
