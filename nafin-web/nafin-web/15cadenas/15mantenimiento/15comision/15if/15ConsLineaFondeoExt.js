Ext.onReady(function() {
var parametrizacion = Ext.get('parametrizacion').getValue();
var intermediario = Ext.get('intermediario').getValue();

//--------------------Handlers---------------------------------




var procesarConsultaData = function(store,arrRegistros,opts){
	pnl.el.unmask();
	
	if(arrRegistros!=null){
		var el = grid.getGridEl();
		
		
			if(store.getTotalCount()>0){
				//Botones
				
				
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					Ext.getCmp('barra').disable();
			}
	}
	
}

	function procesarArchivoSuccess(opts, success, response) {
	grid.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//--------------------STORES--------------------------------



	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		fields: [
			{name: 'NUMERO_LINEA_FONDEO'},
			{name: 'MONTO_ASIGNADO'},
			{name: 'MONTO_UTILIZADO'},
			{name: 'MONTO_COMPROMETIDO'},
			{name: 'DISPONIBLE'},
			{name: 'ADICIONADO_POR'},
			{name: 'FECHA_ADICION'},
			{name: 'MODIFICADO_POR'},
			{name: 'FECHA_MODIFICACION'}
		],
		url : '15ConsLineaFondeoExt.data.jsp',
		baseParams: {
			informacion: 'Consulta',
			parametrizacion:parametrizacion
		},
		autoLoad: false,
		listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							}
					}
		}
	});

//--------------------Componentes-------------------------------

var grid = new Ext.grid.GridPanel({
		store: consultaData,
		viewConfig:{markDirty:false},
		width: 750,
		height: 350,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		stripeRows: true,
		loadMask: true,
		title: '<center>'+intermediario+'</center>',
		frame: true,
		hidden: false,
		columns: [
			{
				header : 'N�mero L�nea Fondeo', tooltip: 'N�mero L�nea Fondeo',dataIndex : 'NUMERO_LINEA_FONDEO',
				sortable : true, width : 180, align: 'center',  hidden: false
			},
			{
				header : 'Monto Asigando', tooltip: 'Monto Asigando',	dataIndex : 'MONTO_ASIGNADO',
				sortable : true, width : 200, align: 'center',  hidden: false,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return Ext.util.Format.number(value,'$0,0.00');
						}
				
			},{
				header : ' Monto Utilizado', tooltip: 'Monto Utilizado',dataIndex : 'MONTO_UTILIZADO',
				sortable : true, width : 200, align: 'center',  hidden: false,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return Ext.util.Format.number(value,'$0,0.00');
							}
			},{
				header : 'Monto Comprometido', tooltip: 'Monto Comprometido',dataIndex : 'MONTO_COMPROMETIDO',
				sortable : true, width : 200, align: 'center',  hidden: false,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return Ext.util.Format.number(value,'$0,0.00');
							}
			},
			{
				header : 'Disponible', tooltip: 'Disponible',	dataIndex : 'DISPONIBLE',
				sortable : true, width : 200, align: 'center',  hidden: false,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return Ext.util.Format.number(value,'$0,0.00');
							}

				
			},{
				header : ' Adicionado por', tooltip: 'Adicionado por',dataIndex : 'ADICIONADO_POR',
				sortable : true, width : 200, align: 'center',  hidden: false
			},{
				header : 'Fecha Adici�n', tooltip: 'Fecha Adici�n',dataIndex : 'FECHA_ADICION',
				sortable : true, width : 150, align: 'center',  hidden: false
			},
			{
				header : 'Modificado por', tooltip: 'Modificado por',	dataIndex : 'MODIFICADO_POR',
				sortable : true, width : 200, align: 'center',  hidden: false

				
			},{
				header : ' Fecha Modificaci�n', tooltip: 'Fecha Modificaci�n',dataIndex : 'FECHA_MODIFICACION',
				sortable : true, width : 150, align: 'center',  hidden: false
			}
			],
	bbar: {
			buttonAlign: 'right',
			id: 'barra',
			hidden:false,
			displayInfo: true,
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
					grid.el.mask('Generando Archivo','x-mask-loading');
						Ext.Ajax.request({
											url: '15ConsLineaFondeoExt.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV',
												parametrizacion:parametrizacion}),
											callback: procesarArchivoSuccess
										});
						
					}
				},'-',{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
					grid.el.mask('Generando PDF','x-mask-loading');
						Ext.Ajax.request({
											url: '15ConsLineaFondeoExt.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoPDF',
												parametrizacion:parametrizacion}),
											callback: procesarArchivoSuccess
										});
					}
				}
			]
		}
		
	}
);					




var fp = new Ext.form.FormPanel
  ({
	
		hidden: false,
		height: 'auto',
		
		width: 560,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 160,
		defaultType: 'textfield',
		frame: false,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			 {
				xtype: 	'label',
				id:	 	'leyenda',
				hidden: 	false,
				cls:		'x-form-item',
				style: 	'font size:1; text-align:center;margin:0 auto;',
				html:  	'<center>Consulta de L�neas de Fondeo en SIRAC<center/>'
			}
		]
  });
	
//--------------Inicio--------------------	
	
var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});
	consultaData.load();
	if(parametrizacion=='G'){
		var cm = grid.getColumnModel();
		cm.setHidden(cm.findColumnIndex('MONTO_COMPROMETIDO'), true);	
		cm.setHidden(cm.findColumnIndex('DISPONIBLE'), true);	
		cm.setHidden(cm.findColumnIndex('ADICIONADO_POR'), true);	
		cm.setHidden(cm.findColumnIndex('FECHA_ADICION'), true);	
		cm.setHidden(cm.findColumnIndex('MODIFICADO_POR'), true);	
		cm.setHidden(cm.findColumnIndex('FECHA_MODIFICACION'), true);	
		
	}else {
		
	}

});