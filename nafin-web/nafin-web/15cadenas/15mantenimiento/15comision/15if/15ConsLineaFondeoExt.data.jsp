	<%@ page contentType="application/json;charset=UTF-8" import="   
  com.netro.anticipos.*,
	java.util.*,
	com.netro.exception.*,  
	com.netro.model.catalogos.*,
	netropology.utilerias.usuarios.*,   
	net.sf.json.JSONObject,
	
	com.netro.fondeopropio.*,
	com.netro.cadenas.*,
	net.sf.json.JSONArray,
	java.math.*,
	com.netro.afiliacion.*"
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/15cadenas/015secsession.jspf" %>

<%
	String informacion            =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar           = "";

	

	
 if(informacion.equals("Consulta")|| informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")){

		ConsuLineaFondeoSIAC paginador =new ConsuLineaFondeoSIAC();
		String parametrizacion            =(request.getParameter("parametrizacion")    != null) ?   request.getParameter("parametrizacion") :"";

		paginador.setNoIntermediario(iNoCliente);
		paginador.setParametrizacion(parametrizacion);
		paginador.setNombreIntermediario(strNombre);
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
		
		if(informacion.equals("Consulta")){
		Registros reg = cqhelper.doSearch();	
		
			//jsonObj.put(registros.getNombreColumnas().get(0).toString().toUpperCase(),registros.getString(registros.getNombreColumnas().get(0).toString()));
			 try {
			  infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
			 }
			 catch(Exception e) {
			  throw new AppException("Error en la paginacion", e);
			 }
		} else if(informacion.equals("ArchivoCSV")){
			String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("ArchivoPDF")){
			String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
	} 


%>

<%=infoRegresar%>