<%@ page language="java" 
import="java.util.*,
java.text.*,
java.math.*,
net.sf.json.JSONArray,
net.sf.json.JSONObject,
com.netro.exception.*,
com.netro.seguridadbean.*" 
contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="../../../015secsession.jspf" %>
<%
// Par�metros provenienen de la p�gina actual
String informacion            =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
JSONObject jsonObj 	      = new JSONObject();
String ic_if			= iNoCliente;
String ic_mes_calculo	= (request.getParameter("Hmes") == null) ? "" : request.getParameter("Hmes");
ic_mes_calculo = ic_mes_calculo.length()==1?"0"+ic_mes_calculo:ic_mes_calculo;
String ic_anio_calculo	= (request.getParameter("Hanio") == null) ? "" : request.getParameter("Hanio").trim();

String ic_moneda = (request.getParameter("HcbMoneda") == null) ? "" : request.getParameter("HcbMoneda");
String infoRegresar           = "";
String ic_banco_fondeo = (request.getParameter("HBanco") == null) ? "" : request.getParameter("HBanco");

CreaArchivo archivo 	= new CreaArchivo();
String nombreArchivoR 	= "";
String nombreArchivoF 	= "";
String contenidoArchivoR = "";
String contenidoArchivoF = "";
AccesoDB			con				= null;
PreparedStatement	ps				= null;
ResultSet			rs				= null;
String				condicion		= "";
String				condicion2		= "";
String				qrySentencia	= "";

//_________________________ INICIO DEL PROGRAMA _________________________
try{
 	con = new AccesoDB();
   	con.conexionDB();
	qrySentencia = 
		" SELECT tt_contenido_detalle_comision"   +
		" FROM com_comision_fondeo_det "   +
		" WHERE ic_mes_calculo = ? "   +
		" AND ic_anio_calculo = ? "   +
		" AND ic_if = ? "   +
		" AND cg_tipo_comision = 'R'" +
		" AND ic_moneda = ? " + 
		" AND ic_banco_fondeo = ? ";
		System.out.println(".::qrySentencia 1::." + qrySentencia);
		
	ps = con.queryPrecompilado(qrySentencia);
	ps.setInt(1,Integer.parseInt(ic_mes_calculo));
	ps.setInt(2,Integer.parseInt(ic_anio_calculo));
	ps.setInt(3,Integer.parseInt(ic_if));
	ps.setInt(4,Integer.parseInt(ic_moneda));
	ps.setInt(5,Integer.parseInt(ic_banco_fondeo));
	rs = ps.executeQuery();
	while(rs.next()) {
		Clob loClob = rs.getClob(1);		
		contenidoArchivoR += loClob.getSubString(1, (int)loClob.length())+"\n";
	}
	rs.close();if(ps!=null) ps.close();
	qrySentencia = 
		" SELECT tt_contenido_detalle_comision"   +
		" FROM com_comision_fondeo_det "   +
		" WHERE ic_mes_calculo = ? "   +
		" AND ic_anio_calculo = ? "   +
		" AND ic_if = ? "   +
		" AND cg_tipo_comision = 'F'" +
		" AND ic_moneda = ? " + 
		" AND ic_banco_fondeo = ? ";
		
		System.out.println(".::qrySentencia 2::." + qrySentencia);
		
	ps = con.queryPrecompilado(qrySentencia);
	ps.setInt(1,Integer.parseInt(ic_mes_calculo));
	ps.setInt(2,Integer.parseInt(ic_anio_calculo));
	ps.setInt(3,Integer.parseInt(ic_if));
	ps.setInt(4,Integer.parseInt(ic_moneda));
	ps.setInt(5,Integer.parseInt(ic_banco_fondeo));
	
	rs = ps.executeQuery();
	while(rs.next()) {
		Clob loClob = rs.getClob(1);		
		contenidoArchivoF += loClob.getSubString(1, (int)loClob.length())+"\n";
	}
	rs.close();if(ps!=null) ps.close();
	
	
%>

<%	if(contenidoArchivoR.length()>1 || contenidoArchivoF.length()>1) {%>				
				
<%		if(contenidoArchivoR.length()>1) {
			if (!archivo.make(contenidoArchivoR, strDirectorioTemp , ".csv"))
				out.println("Error en la generacion del archivo");
			else
				nombreArchivoR = archivo.nombre;%>			
								
<%		}
		if(contenidoArchivoF.length()>1) {			
			if (!archivo.make(contenidoArchivoF, strDirectorioTemp , ".csv"))
				out.println("Error en la generacion del archivo");
			else
				nombreArchivoF = archivo.nombre;%>						
								
<%		}%>								
								
<%	}//if(contenidoArchivoR.length()>1 || contenidoArchivoF.length()>1)%>		
			

<%
		
			jsonObj.put("success", new Boolean(true));
			System.out.println(nombreArchivoF+nombreArchivoR);
			if(nombreArchivoF.equals("")){
				jsonObj.put("urlArchivo1","");
			}else{
				jsonObj.put("urlArchivo1", strDirecVirtualTemp+nombreArchivoF);
			}
			
			if(nombreArchivoR.equals("")){
				jsonObj.put("urlArchivo2", "");
			}else{
				jsonObj.put("urlArchivo2", strDirecVirtualTemp+nombreArchivoR);
			}			infoRegresar=jsonObj.toString();
			
			
}catch(Exception e){
	out.print("Error inesperado: "+e.toString());
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
}%>

<%=infoRegresar%>