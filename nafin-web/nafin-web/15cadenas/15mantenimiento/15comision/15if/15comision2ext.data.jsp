	<%@ page contentType="application/json;charset=UTF-8" import="   
  com.netro.anticipos.*,
	java.util.*,
	com.netro.exception.*,  
	com.netro.model.catalogos.*,
	netropology.utilerias.usuarios.*,   
	net.sf.json.JSONObject,
	
	com.netro.fondeopropio.*,
	com.netro.cadenas.*,
	net.sf.json.JSONArray,
	java.math.*,
	com.netro.afiliacion.*"
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/15cadenas/015secsession.jspf" %>

<%
	String informacion            =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar           = "";
	String ic_if = iNoCliente;
	String mes_calculo="";
	String bandera =(request.getParameter("bandera")    != null) ?   request.getParameter("bandera") :"";
	String anio_calculo;
	if(bandera.equals("true")){
		anio_calculo  =(request.getParameter("Hanio")    != null) ?   request.getParameter("Hanio") :"";
		mes_calculo =(request.getParameter("Hmes")    != null) ?   request.getParameter("Hmes") :"";
	}else
	{
			mes_calculo="13";
			anio_calculo  =(request.getParameter("Hanio_2")    != null) ?   request.getParameter("Hanio_2") :"";
	}
	String perfilRequerido ="";
	String ic_moneda  =(request.getParameter("HcbMoneda")    != null) ?   request.getParameter("HcbMoneda") :"";
	String ic_banco_fondeo =(request.getParameter("HBanco")    != null) ?   request.getParameter("HBanco") :"";
	
	if(ic_banco_fondeo.equals("1"))
		perfilRequerido = "ADMIN NAFIN";
	if(ic_banco_fondeo.equals("2"))
		perfilRequerido = "ADMIN BANCOMEXT";
	
	if(informacion.equals("catologoBancoDist")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_banco_fondeo ");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("comcat_banco_fondeo");		
	catalogo.setOrden("ic_banco_fondeo ");	
	infoRegresar = catalogo.getJSONElementos();	
	
}else if (informacion.equals("catologoMonedaDist")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
		
	}else if(informacion.equals("Consulta")){
		
		ComisionFondeoPropio comisionBean = ServiceLocator.getInstance().lookup("ComisionFondeoPropioEJB",ComisionFondeoPropio.class);
		
		ComisionRecursosPropios paginador =new ComisionRecursosPropios();
		
		
		boolean variosTiposComision = comisionBean.manejaVariosTiposComision(ic_if, mes_calculo, anio_calculo, perfilRequerido, ic_moneda, false); //Al enviar el booleano en true tomara en cuenta Pesos y Dolares sin importar que se le mande otro tipo de moneda como parametro
		
		paginador.setIc_if(ic_if);
		paginador.setVariosTiposComision(variosTiposComision);
		paginador.setMes_calculo(mes_calculo);
		paginador.setAnio_calculo(anio_calculo);
		paginador.setIc_moneda(ic_moneda);
		paginador.setIc_banco_fondeo(ic_banco_fondeo);
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
		Registros registros = cqhelper.doSearch();	
		HashMap	datos;
		List reg=new ArrayList();

		
		double dblContraPrestacion=0.0d;
		registros.next();
		
		
		BigDecimal granTotalMontoComisionMN = new BigDecimal("0.00");
		if(registros.getNumeroRegistros()>0){
					AccesoDB con  = new AccesoDB();
					con.conexionDB();
					try {
					System.out.println(paginador.getAggregateCalculationQuery());
					Registros re = con.consultarDB(paginador.getAggregateCalculationQuery());
					if(re.next())
					jsonObj.put(re.getNombreColumnas().get(0).toString().toUpperCase(),re.getString(re.getNombreColumnas().get(0).toString()));
					granTotalMontoComisionMN = new BigDecimal(re.getString(re.getNombreColumnas().get(0).toString()));
					} catch(Exception e) {
					throw new AppException("Error al obtener los Totales", e);
				} finally {
					if(con.hayConexionAbierta()) {
						con.terminaTransaccion(true); //Para consultas a tablas remotas a traves de dblink
						con.cierraConexionDB();
					}
				}
		}
		registros.rewind();
		BigDecimal montoTotalDocumentos = new BigDecimal("0.00");
		while(registros.next()){
			montoTotalDocumentos=montoTotalDocumentos.add(new BigDecimal(registros.getString("MONTODOCUMENTOS")==null?"":registros.getString("MONTODOCUMENTOS")));
		}
		BigDecimal montoDocumentos = new BigDecimal("0.00");
		BigDecimal porcentajeComision = new BigDecimal("0.00");
		BigDecimal montoComision = new BigDecimal("0.00");
		
		registros.rewind();
		while(registros.next()){
						datos=new HashMap();
						for(int i=0;i<registros.getNombreColumnas().size();i++){
						
								if(registros.getNombreColumnas().get(i).toString().toUpperCase().equals("FG_CONTRAPRESTACION")){
										
										if(bandera.equals("true")){
										montoDocumentos = new BigDecimal(registros.getString("MONTODOCUMENTOS")==null?"":registros.getString("MONTODOCUMENTOS")); 
										porcentajeComision = (montoDocumentos.multiply(new BigDecimal("100"))).divide(montoTotalDocumentos, 20, BigDecimal.ROUND_HALF_UP);
										montoComision = (granTotalMontoComisionMN.multiply(porcentajeComision)).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP);
										datos.put(registros.getNombreColumnas().get(i).toString().toUpperCase(),Comunes.formatoDecimal(montoComision, 2));									

										}else{
										dblContraPrestacion=Double.parseDouble(registros.getString(registros.getNombreColumnas().get(i).toString()))*1.15d;
										datos.put(registros.getNombreColumnas().get(i).toString().toUpperCase(),Comunes.formatoDecimal(dblContraPrestacion, 2));									

										}
								}else{
										datos.put(registros.getNombreColumnas().get(i).toString().toUpperCase(),registros.getString(registros.getNombreColumnas().get(i).toString()));
								}
							}
							reg.add(datos);
						}					
			
			jsonObj.put("detalle",new Boolean(comisionBean.existeDetalleComision(ic_if, mes_calculo, anio_calculo, perfilRequerido, ic_moneda, false)));
			
			//jsonObj.put(registros.getNombreColumnas().get(0).toString().toUpperCase(),registros.getString(registros.getNombreColumnas().get(0).toString()));
			
			JSONArray regB = new JSONArray();
			regB = JSONArray.fromObject(reg);
			jsonObj.put("registros", regB);
			jsonObj.put("total", registros.getNumeroRegistros()+"");
			jsonObj.put("success", new Boolean(true));
			infoRegresar=jsonObj.toString();
	} else if(informacion.equals("Detalle")){
			
			ComisionFondeoPropio comisionBean = ServiceLocator.getInstance().lookup("ComisionFondeoPropioEJB",ComisionFondeoPropio.class);
			Registros registros = comisionBean.getComisionResumen(ic_if,mes_calculo,anio_calculo,ic_moneda,ic_banco_fondeo);
			String fecha 				= "01/"+ mes_calculo + "/" + anio_calculo; 
			double iva 					= ((BigDecimal)Iva.getValor(fecha)).doubleValue();
			HashMap	datos;
			List reg=new ArrayList();
	
					while(registros.next()){
								datos=new HashMap();
							
								
								for(int i=0;i<registros.getNombreColumnas().size();i++){
												datos.put(registros.getNombreColumnas().get(i).toString().toUpperCase(),registros.getString(registros.getNombreColumnas().get(i).toString()));	
									}
									datos.put("IVA_POR",iva+"");
									reg.add(datos);
							}
				
					JSONArray regB = new JSONArray();
					regB = JSONArray.fromObject(reg);
					jsonObj.put("registros", regB);
					jsonObj.put("total", registros.getNumeroRegistros()+"");
					jsonObj.put("success", new Boolean(true));
					infoRegresar=jsonObj.toString();
		
	}else if(informacion.equals("Ajuste")||informacion.equals("AjusteTotales")){
			
			ComisionFondeoPropio comisionBean = ServiceLocator.getInstance().lookup("ComisionFondeoPropioEJB",ComisionFondeoPropio.class);
			Registros registros = comisionBean.getAjusteAnual(ic_if,mes_calculo,anio_calculo,ic_moneda,ic_banco_fondeo);
			HashMap	datos;
			double granTotalSaldoPromedioMN=0, dblSaldoPromedio=0, granTotalContraprestacionCobradaMN=0,
			dblContraprestacion=0,granTotalContraprestacionTotalCobradaMN=0,dblContraprestacionTotal=0,
			granTotalContraprestacionAjustadaMN=0, dblContraprestacionAjustada=0, granTotalContraprestacionTotalAjustadaMN=0, 
			dblContraprestacionTotalAjustada=0, granTotalAjuste=0, dblAjuste=0;
			List reg=new ArrayList();
			double dblSaldoPromedioDeReferencia=0;
			while(registros.next()){
						datos=new HashMap();
						
						dblSaldoPromedio= Double.parseDouble(registros.getString("FG_SALDO_PROMEDIO"));
						dblContraprestacion= Double.parseDouble(registros.getString("FG_CONTRAPRESTACION"));
						dblContraprestacionAjustada = Double.parseDouble(registros.getString("FG_CONTRAPRESTACION_AJUSTADA"));
						dblAjuste=(dblContraprestacionAjustada*1.15-dblContraprestacion*1.15);
						dblSaldoPromedioDeReferencia = Double.parseDouble(registros.getString("FG_SALDO_REFERENCIA"));
						
						granTotalSaldoPromedioMN += dblSaldoPromedio;
						granTotalContraprestacionCobradaMN += dblContraprestacion;
						granTotalContraprestacionTotalCobradaMN += dblContraprestacion*1.15;
						granTotalContraprestacionAjustadaMN += dblContraprestacionAjustada;
						granTotalContraprestacionTotalAjustadaMN += dblContraprestacionAjustada*1.15;
						granTotalAjuste += dblAjuste;
						for(int i=0;i<registros.getNombreColumnas().size();i++){
										datos.put(registros.getNombreColumnas().get(i).toString().toUpperCase(),registros.getString(registros.getNombreColumnas().get(i).toString()));	
							}
						datos.put("IVA","15");	
						reg.add(datos);
					}
			if(informacion.equals("Ajuste")){
					JSONArray regB = new JSONArray();
					regB = JSONArray.fromObject(reg);
					jsonObj.put("registros", regB);
					jsonObj.put("total", registros.getNumeroRegistros()+"");
					jsonObj.put("success", new Boolean(true));
					infoRegresar=jsonObj.toString();
			}else{
				datos=new HashMap();
				reg=new ArrayList();
				datos.put("SALDO_REFERENCIA",dblSaldoPromedioDeReferencia+"");
				datos.put("TOTAL_SALDO",granTotalSaldoPromedioMN+"");								datos.put("TOTAL_SALDO",granTotalSaldoPromedioMN+"");
				datos.put("TOTAL_COBRADA",granTotalContraprestacionCobradaMN+"");
				datos.put("TOTAL_TOT_COBRADA",granTotalContraprestacionTotalCobradaMN+"");
				datos.put("TOTAL_AJUSTADA",granTotalContraprestacionAjustadaMN+"");
				datos.put("TOTAL_TOT_AJUSTADA",granTotalContraprestacionTotalAjustadaMN+"");
				datos.put("TOTAL_AJUSTE",granTotalAjuste+"");
				reg.add(datos);
				JSONArray regB = new JSONArray();
				regB = JSONArray.fromObject(reg);
				jsonObj.put("registros", regB);
				jsonObj.put("total", registros.getNumeroRegistros()+"");
				jsonObj.put("success", new Boolean(true));
				infoRegresar=jsonObj.toString();
				
			}
	
	}


%>

<%=infoRegresar%>