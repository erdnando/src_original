Ext.onReady(function() {

var ic_if =  Ext.getDom('ic_if').value;
	var busqAvanzadaSelect=0;
	Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);
	//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
	var procesarCatalogoAnio = function(store, arrRegistros, opts) {
		Ext.getCmp("idIc_anio_calculo").setValue('2012');	
	}
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		var grid = Ext.getCmp('grid');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
	
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
			
				grid.setTitle(store.getAt(0).get('NOMBREIF'));
				gridTotales.show();
					btnGenerarPDF.enable();
					btnBajarPDF.hide();
				if(!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}
				el.unmask();
			}else {
				if(gridTotales.isVisible()){
					gridTotales.hide();
				}
				Ext.getCmp('grid').setTitle('');
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
		  }
		}
	}
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);			
		}
	}
	var bajarContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();			
		} else {
			alert("error");
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
			
    	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	
	var consultaDataGrid = new Ext.data.GroupingStore({
		root : 'registros',
		url : '15conscomisionmensualext.data.jsp',
		baseParams: {
			informacion: 'Consulta',
			periodo: 'Anual'
		},
    reader: new Ext.data.JsonReader({
    	root : 'registros', totalProperty: 'total',
      fields: [
			{name: 'IC_NOMBRE'},
			{name: 'NOMBREIF'},
			{name: 'IC_PRODUCTO_NAFIN'},
			{name: 'FG_SALDO_REFERENCIA'},
			{name: 'FG_PROCENTAJE_AJUSTADO'},
			{name: 'CG_ESTATUS_PAGADO'},
			{name: 'SALDOPROMEDIO'},
			{name: 'CONTRAPRESTACIONAJUSTADA'},
			{name: 'CONTRAPRESTACIONTOTALAJUSTADA'},
			{name: 'AJUSTE'},
			{name: 'IC_PORCENTAJE_IVA'}
		]
    }),
    groupField: 'IC_NOMBRE',	sortInfo:{field: 'IC_NOMBRE', direction: "ASC"},
	 totalProperty : 'total', messageProperty: 'msg', autoLoad: false, 
    listeners: {
    load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	}); 
	var resumenTotalesData = new Ext.data.JsonStore({		
		root : 'registros',
		url : '15conscomisionmensualext.data.jsp',
		baseParams: {
			informacion: 'ConsultaTotales',
			operacion: 'ResumenTotales',
			periodo: 'Anual'
		},
		fields: [
			{name: 'SALDOPROMEDIO_TOTAL',  type: 'float',mapping: 'SALDOPROMEDIO_TOTAL'},
			{name: 'CONTRAPRESTACIONAJUSTADA_TOTAL',  type: 'float', mapping: 'CONTRAPRESTACIONAJUSTADA_TOTAL'},
			{name: 'CONTRAPRESTACIONTOTAL_TOTAL',type: 'float',  mapping: 'CONTRAPRESTACIONTOTAL_TOTAL'},
			{name: 'AJUSTE_TOTAL',type: 'float',  mapping: 'AJUSTE_TOTAL'}			
		],
		totalProperty : 'total', 
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
  //fin de store grid
 
	var catalogoBancoFondeo = new Ext.data.JsonStore({
		id: 'catalogoBancoFondeo',
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15conscomisionmensualext.data.jsp',
		baseParams: {
			informacion: 'catalogoBancoFondeo'
		},
		totalProperty : 'total',
		autoLoad: true
	});
	var catalogoProducto = new Ext.data.JsonStore({
		id: 'catalogoProducto',
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15conscomisionmensualext.data.jsp',
		listeners: {
			//load: procesarProducto,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						procesarConsultaData(null, null, null); // < ------------------------------
				}
			}
		},
		baseParams: {
			informacion: 'catalogoProducto'
		},
		totalProperty : 'total',
		autoLoad: true
	});
	var catalogoMesDe = new Ext.data.ArrayStore({
		fields : ['clave', 'descripcion'],  
		data:[
			[1,'Enero'],
			[2,'Febrero'],
			[3,'Marzo'],
			[4,'Abril'],
			[5,'Mayo'],
			[6,'Junio'],
			[7,'Julio'],
			[8,'Agosto'],
			[9,'Septiembre'],
			[10,'Octubre'],
			[11,'Noviembre'],
			[12,'Diciembre']
		]
	});
	
	var catalogoAnio = new Ext.data.SimpleStore({
		fields : ['clave', 'descripcion'],  
		 data:[
			[2002,'2002'],
			[2003,'2003'],
			[2004,'2004'],
			[2005,'2005'],
			[2006,'2006'],
			[2007,'2007'],
			[2008,'2008'],
			[2009,'2009'],
			[2010,'2010'],
			[2011,'2011'],
			[2012,'2012'],
			[2013,'2013'],
			[2014,'2014'],
			[2015,'2015'],
			[2016,'2016'],
			[2017,'2017'],
			[2018,'2018'],
			[2019,'2019'],
			[2020,'2020']
		 ]
	});
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoBancoFondeo',
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15conscomisionmensualext.data.jsp',
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: true
	});
	var catalogoEstatus = new Ext.data.ArrayStore({
		fields : ['clave', 'descripcion'],  
		 data:[
			['S','Pagado'],
			['N','No Pagado']
		 ]
	});
//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
	var grid = new Ext.grid.GridPanel({
		store: consultaDataGrid,
		id: 'grid',
		hidden: true,
		columns: [
			{
				header: 'Producto', tooltip: 'Producto', dataIndex: 'IC_NOMBRE',
				sortable: true,width: 120, resizable: true,align: 'left',hidden:true
			},{
				header: 'A�o', tooltip: 'A�o',dataIndex: '',
				sortable: true,	align: 'center',width: 50, resizable: true,align: 'center',
				renderer: function() {
                return Ext.getCmp('idIc_anio_calculo').getValue();
            }
			},{
				header: 'Saldo Promedio', tooltip: 'Saldo Promedio',dataIndex: 'SALDOPROMEDIO',
				sortable: true,	align: 'center',width: 90, resizable: true,align: 'right',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.usMoney(record.get('SALDOPROMEDIO'));
            }
			},{
				header: 'Saldo Referencia', tooltip: 'Saldo de Referencia',
				dataIndex: 'FG_SALDO_REFERENCIA',
				sortable: true,	align: 'center',
				width: 100, resizable: true,
				align: 'right',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.usMoney(record.get('FG_SALDO_REFERENCIA'));
            }
			},{
				header: 'Tasa Ajustada', tooltip: 'Tasa Ajustada',
				dataIndex: 'FG_PROCENTAJE_AJUSTADO',
				sortable: true,	align: 'center',
				width: 70, resizable: true,
				align: 'center',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.usMoney(record.get('FG_PROCENTAJE_AJUSTADO'));
            }
			},{
				header: 'Contraprestacion Ajustada', tooltip: 'Contraprestacion Ajustada',
				dataIndex: 'CONTRAPRESTACIONAJUSTADA',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'right',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.usMoney(record.get('CONTRAPRESTACIONAJUSTADA'));
            }
			},{
				header: '% IVA', tooltip: '% IVA',
				dataIndex: 'IC_PORCENTAJE_IVA',
				sortable: true,	align: 'center',
				width: 60, resizable: true,
				align: 'right',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return value + ' %';
            }
			},{
				header: 'Contraprestacion Total Ajustada', tooltip: 'Contraprestacion Total Ajustada',
				dataIndex: 'CONTRAPRESTACIONTOTALAJUSTADA',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.usMoney(record.get('CONTRAPRESTACIONTOTALAJUSTADA'));
            }
			},{
				header: 'Ajuste', tooltip: 'Ajuste',
				dataIndex: 'AJUSTE',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.usMoney(record.get('AJUSTE'));
            }
			},{
				header: 'Pagado', tooltip: 'Pagado',
				dataIndex: 'CG_ESTATUS_PAGADO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center'
			}
		],
		view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),
		autoScroll: true,
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 400,
		width: 940,
		title: '...',
		frame: true,
		bbar: {
			items: [
				'->','-',
			{
				xtype: 'button',
				text: 'Generar Archivo',
				iconCls: 'icoXls',
				id: 'btnGenerarArchivo',
				disabled : true,
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					Ext.Ajax.request({
						url: '15conscomisionmensualext.data.jsp',
						params: Ext.apply(paramSubmit,{
							operacion:'XLS',
							informacion: 'Consulta',
							periodo: 'Anual'
						}),
						callback: procesarSuccessFailureGenerarArchivo
					});
				}
			},{
				xtype: 'button',
				text: 'Bajar Archivo',
				id: 'btnBajarArchivo',
				hidden: true
			},'-',{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnGenerarPDF',
				iconCls: 'icoPdf',
				disabled : true,
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					Ext.Ajax.request({
						url: '15conscomisionmensualext.data.jsp',
						params: Ext.apply(paramSubmit,{
							operacion:'PDF',
							informacion: 'Consulta',
							periodo: 'Anual'
						}),
						callback: procesarSuccessFailureGenerarPDF
					});
				}
			},{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		},
		height: 200,
		width: 729,
		style: 'margin:0 auto'
	});
	var gridTotales = new Ext.grid.EditorGridPanel({
		id: 'gridTotales',
		title: 'Gran Total M.N.',
		hidden: true,
		align: 'center',
		store: resumenTotalesData,
			columns: [	
				{
					header: 'Saldo Promedio',
					dataIndex: 'SALDOPROMEDIO_TOTAL',
					width: 100,
					align: 'center',
					renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
						 return Ext.util.Format.usMoney(record.get('SALDOPROMEDIO_TOTAL'));
					}			
				},
				{
					header: 'Contraprestacion Ajustada',
					dataIndex: 'CONTRAPRESTACIONAJUSTADA_TOTAL',
					width: 180,
					align: 'center',
					renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
						 return Ext.util.Format.usMoney(record.get('CONTRAPRESTACIONAJUSTADA_TOTAL'));
					}							
				},
				{
					header: 'Contraprestacion Total Ajustada',
					dataIndex: 'CONTRAPRESTACIONTOTAL_TOTAL',
					width: 210,
					align: 'center',
					renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
						 return Ext.util.Format.usMoney(record.get('CONTRAPRESTACIONTOTAL_TOTAL'));
					}	
				},
				{
					header: 'Ajuste',
					dataIndex: 'AJUSTE_TOTAL',
					width: 100,
					align: 'center',
					renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
						 return Ext.util.Format.usMoney(record.get('AJUSTE_TOTAL'));
					}	
				}			
			],
			height: 100,		
			width: 600,
			style: 'margin:0 auto',
			frame: false
		});
	/**** MANEJO DE FECHAS *****/
	date = new Date();  
	month = date.getMonth()+1; // NUMERO DEL MES TOMANDO EL PRIMER MES COMO CERO
	year = date.getFullYear();
	/**** FIN MANEJO DE FECHAS *****/
	
	var elementosForma = [
		{
			width: 800,
			fieldLabel: 'Banco fondeo',
			labelWidth: 200,
			xtype: 'combo',
			name: 'ic_banco_fondeo',
			id: 'idIc_banco_fondeo',
			hiddenName: 'ic_banco_fondeo',
			emptyText : 'Todos',
			store: catalogoBancoFondeo,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection: false,
			triggerAction: 'all',
			typeAhead: true,
			width:300	
		},{
			xtype: 'combo',
			name: 'ic_producto_nafin',
			id: 'idIc_producto_nafin',
			hiddenName : 'ic_producto_nafin',
			fieldLabel: 'Producto',
			emptyText: 'Seleccione Producto',
			store: catalogoProducto,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			width:300
		},{
         xtype: 'combo',
         name: 'ic_anio_calculo',
         id: 'idIc_anio_calculo',
         hiddenName: 'ic_anio_calculo',
         fieldLabel: 'A�o de Calculo',
         emptyText: '- Seleccione -',
			store: catalogoAnio,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			forceSelection : true,
			typeAhead: true,
			allowBlank : false,
			minChars : 1,
			anchor: '40%',
         value:   parseInt(year)-1
		},{
			xtype: 'combo',
			name: 'ic_moneda',
			id: 'idIc_moneda',
			hiddenName : 'ic_moneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccione Moneda',
			store: catalogoMoneda,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			allowBlank : false,
			minChars : 1,
			width:300
		},{
			xtype: 'combo',
			name: 'cg_estatus_pagado',
			id: 'idCg_estatus_pagado',
			hiddenName : 'cg_estatus_pagado',
			fieldLabel: 'Estatus',
			emptyText: 'Seleccione Estatus',
			store: catalogoEstatus,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			width:300
		}
	];
	
	//Forma para hacer la busqueda filtrada
	var fp = new Ext.form.FormPanel({
		xtype: 'form',
		id: 'forma',
		style: ' margin:0 auto; text-align:left',
		title:'Comision de Recursos Propios : Consulta Anual',
		collapsible: true,
		titleCollapse: false,
		frame:true,
		width: 600,
		bodyStyle: 'padding: 6px',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					var cmpForma = Ext.getCmp('forma');					
					var _ic_moneda = Ext.getCmp("idIc_moneda");	
					var _ic_anio_calculo = Ext.getCmp("idIc_anio_calculo");
               
					if (Ext.isEmpty(_ic_anio_calculo.getValue()) ) {
							_ic_anio_calculo.markInvalid('Por favor, seleccione el a�o de Calculo.');
							return;
					}
					if (Ext.isEmpty(_ic_moneda.getValue()) ) {
							_ic_moneda.markInvalid('Por favor, seleccione una Moneda...');
							return;
					}
               
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():"";	
					consultaDataGrid.load({params: Ext.apply(paramSubmit,{})});
					fp.el.mask('Enviando...', 'x-mask-loading');
					grid.hide();
					resumenTotalesData.load({params: Ext.apply(fp.getForm().getValues(),{})})
					Ext.getCmp('btnGenerarArchivo').disable();
					Ext.getCmp('btnBajarArchivo').hide();
					Ext.getCmp('btnGenerarPDF').disable();
					Ext.getCmp('btnBajarPDF').hide();
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15conscomisionanualext.jsp';
				}
			}
		]
	});//FIN DE LA FORMA
	
	/**** TABS ****/		
/**** FIN DE TRABS ****/

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10), 
			grid,
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});
});
