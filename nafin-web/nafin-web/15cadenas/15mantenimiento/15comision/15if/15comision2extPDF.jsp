	<%@ page contentType="application/json;charset=UTF-8" import="   
  com.netro.anticipos.*,
	java.util.*,
	com.netro.exception.*,  
	com.netro.model.catalogos.*,
	netropology.utilerias.usuarios.*,   
	net.sf.json.JSONObject,
	com.netro.pdf.*,
	com.netro.descuento.*,
	com.netro.fondeopropio.*,
	com.netro.cadenas.*,
	java.text.SimpleDateFormat,
	java.math.*,
	net.sf.json.JSONArray,
	com.netro.afiliacion.*
	 
"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/15cadenas/015secsession.jspf" %>

<%
	String informacion            =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String[] meses= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar           = "";
	String ic_if = iNoCliente;
	String mes_calculo="";
	String bandera =(request.getParameter("bandera")    != null) ?   request.getParameter("bandera") :"";
	String anio_calculo;
	if(bandera.equals("true")){
		anio_calculo  =(request.getParameter("Hanio")    != null) ?   request.getParameter("Hanio") :"";
		mes_calculo =(request.getParameter("Hmes")    != null) ?   request.getParameter("Hmes") :"";
	}else
	{
			mes_calculo="13";
			anio_calculo  =(request.getParameter("Hanio_2")    != null) ?   request.getParameter("Hanio_2") :"";
	}
	String perfilRequerido ="";
	String ic_moneda  =(request.getParameter("HcbMoneda")    != null) ?   request.getParameter("HcbMoneda") :"";
	String ic_banco_fondeo =(request.getParameter("HBanco")    != null) ?   request.getParameter("HBanco") :"";
	String txtMoneda  =(request.getParameter("moneda")    != null) ?   request.getParameter("moneda") :"";
	if(ic_banco_fondeo.equals("1"))
		perfilRequerido = "ADMIN NAFIN";
	if(ic_banco_fondeo.equals("2"))
		perfilRequerido = "ADMIN BANCOMEXT";
	
	if(informacion.equals("catologoBancoDist")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_banco_fondeo ");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("comcat_banco_fondeo");		
	catalogo.setOrden("ic_banco_fondeo ");	
	infoRegresar = catalogo.getJSONElementos();	
	
}else if (informacion.equals("catologoMonedaDist")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
		
	}else if(informacion.equals("Consulta")||informacion.equals("ConsultaPDF")){
	
		ComisionFondeoPropio comisionBean = ServiceLocator.getInstance().lookup("ComisionFondeoPropioEJB",ComisionFondeoPropio.class);
		List montosComision = new ArrayList();
		ComisionRecursosPropios paginador =new ComisionRecursosPropios();
		
		
		boolean variosTiposComision = comisionBean.manejaVariosTiposComision(ic_if, mes_calculo, anio_calculo, perfilRequerido, ic_moneda, false); //Al enviar el booleano en true tomara en cuenta Pesos y Dolares sin importar que se le mande otro tipo de moneda como parametro
		
		paginador.setIc_if(ic_if);
		paginador.setVariosTiposComision(variosTiposComision);
		paginador.setMes_calculo(mes_calculo);
		paginador.setAnio_calculo(anio_calculo);
		paginador.setIc_moneda(ic_moneda);
		paginador.setIc_banco_fondeo(ic_banco_fondeo);
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
		Registros registros = cqhelper.doSearch();	
		HashMap	datos;
		List reg=new ArrayList();

		
		double dblContraPrestacion=0.0d;
		registros.next();
		BigDecimal granTotalMontoComisionMN = new BigDecimal("0.00");
		if(registros.getNumeroRegistros()>0){
					AccesoDB con  = new AccesoDB();
					con.conexionDB();
					try {
					System.out.println(paginador.getAggregateCalculationQuery());
					Registros re = con.consultarDB(paginador.getAggregateCalculationQuery());
					if(re.next())
					jsonObj.put(re.getNombreColumnas().get(0).toString().toUpperCase(),re.getString(re.getNombreColumnas().get(0).toString()));
					granTotalMontoComisionMN = new BigDecimal(re.getString(re.getNombreColumnas().get(0).toString()));
					} catch(Exception e) {
					throw new AppException("Error al obtener los Totales", e);
				} finally {
					if(con.hayConexionAbierta()) {
						con.terminaTransaccion(true); //Para consultas a tablas remotas a traves de dblink
						con.cierraConexionDB();
					}
				}
		}
		registros.rewind();
		BigDecimal montoTotalDocumentos = new BigDecimal("0.00");
		
		while(registros.next()){
			montoTotalDocumentos=montoTotalDocumentos.add(new BigDecimal(registros.getString("MONTODOCUMENTOS")==null?"":registros.getString("MONTODOCUMENTOS")));
		}
		BigDecimal montoDocumentos = new BigDecimal("0.00");
		BigDecimal porcentajeComision = new BigDecimal("0.00");
		BigDecimal montoComision = new BigDecimal("0.00");
		
		registros.rewind();
		while(registros.next()){
						datos=new HashMap();
						for(int i=0;i<registros.getNombreColumnas().size();i++){
								if(registros.getNombreColumnas().get(i).toString().toUpperCase().equals("FG_CONTRAPRESTACION")){
										
										if(bandera.equals("true")){
										montoDocumentos = new BigDecimal(registros.getString("MONTODOCUMENTOS")==null?"":registros.getString("MONTODOCUMENTOS")); 
										porcentajeComision = (montoDocumentos.multiply(new BigDecimal("100"))).divide(montoTotalDocumentos, 20, BigDecimal.ROUND_HALF_UP);
										montoComision = (granTotalMontoComisionMN.multiply(porcentajeComision)).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP);
										datos.put(registros.getNombreColumnas().get(i).toString().toUpperCase(),Comunes.formatoDecimal(montoComision, 2));									
										montosComision.add(Comunes.formatoDecimal(montoComision,2));
										}else{
										
										dblContraPrestacion=Double.parseDouble(registros.getString(registros.getNombreColumnas().get(i).toString()))*1.15d;
										datos.put(registros.getNombreColumnas().get(i).toString().toUpperCase(),Comunes.formatoDecimal(dblContraPrestacion, 2));									
										montosComision.add(Comunes.formatoDecimal(dblContraPrestacion,2));
										}
								}else{
										datos.put(registros.getNombreColumnas().get(i).toString().toUpperCase(),registros.getString(registros.getNombreColumnas().get(i).toString()));
								}
							}
							reg.add(datos);
						}					
			if(informacion.equals("Consulta")){
			jsonObj.put("detalle",comisionBean.existeDetalleComision(ic_if, mes_calculo, anio_calculo, perfilRequerido, ic_moneda, false)+"");
			
			//jsonObj.put(registros.getNombreColumnas().get(0).toString().toUpperCase(),registros.getString(registros.getNombreColumnas().get(0).toString()));
			JSONArray regB = new JSONArray();
			regB = JSONArray.fromObject(reg);
			jsonObj.put("registros", regB);
			jsonObj.put("total", registros.getNumeroRegistros()+"");
			jsonObj.put("success", new Boolean(true));
			infoRegresar=jsonObj.toString();
			}else{
				String numDoc =(request.getParameter("numDoc")    != null) ?   request.getParameter("numDoc") :"";
				String totReg =(request.getParameter("totReg")    != null) ?   request.getParameter("totReg") :"";
				String totMont =(request.getParameter("totMont")    != null) ?   request.getParameter("totMont") :"";

			
				String linea = "";
				String nombreArchivo = "";
				String espacio = "";
					//HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo);
						
					//String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual = fechaActual.substring(0,2);
					String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual = fechaActual.substring(6,10);
					String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String)session.getAttribute("strNombre"),
					(String)session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),
					(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
					boolean band=registros.next();
					pdfDoc.addText("México, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
					
					String producto="";
					String var="";
					int col=0;
					String producto2="";
					if(bandera.equals("false")){
							var="IC_EPO";
							col=7;
							if(registros.getString("NOMBREEPO").equals("-"))
								producto2="PRODUCTO NAFIN: "+registros.getString("IC_NOMBRE");	
							else
								producto2="EPO: "+registros.getString("NOMBREEPO")+"\nPRODUCTO NAFIN: "+registros.getString("IC_NOMBRE");
						}else{
							var="IC_EPO";
							col=6;
							producto2=registros.getString("NOMBREEPO");
							if(producto2.equals("-"))
								producto2="PRODUCTO NAFIN: "+registros.getString("IC_NOMBRE");
							else
								producto2="EPO: "+registros.getString("NOMBREEPO")+"\nPRODUCTO NAFIN: "+registros.getString("IC_NOMBRE");
							
						}
					producto=registros.getString(var);
					pdfDoc.addText("IF: "+registros.getString("NOMBREIF")+"\n","formasB",ComunesPDF.CENTER);
					pdfDoc.setTable(1,100);
					pdfDoc.setCell(producto2, "celda01", ComunesPDF.CENTER);
					pdfDoc.addTable();
					
					
					pdfDoc.setTable(col,100);
					pdfDoc.setCell("Mes", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Num. Doctos.", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de Documentos", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje comisión", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Comisión", "celda01",ComunesPDF.CENTER);
					if(bandera.equals("false"))
					pdfDoc.setCell("Pagado", "celda01",ComunesPDF.CENTER);
					
					while (band) {
						producto=registros.getString(var);
						String mes = (registros.getString("IC_MES_CALCULO"));
						String numDoctos = (registros.getString("DOCTOSVIGENTES"));
						String moneda = txtMoneda;
						String montoDoctos = (registros.getString("MONTODOCUMENTOS"));
						String porcentajeCom = (registros.getString("FG_PORCENTAJE_APLICADO"));
						String montoCom = montosComision.remove(0).toString();
					
						
						pdfDoc.setCell(meses[Integer.parseInt(mes)-1]+" "+anio_calculo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numDoctos,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+montoDoctos,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(porcentajeCom+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+montoCom,"formas",ComunesPDF.CENTER);
						if(bandera.equals("false")){
							String pagado = (registros.getString("CG_ESTATUS_PAGADO"));
							pdfDoc.setCell(pagado,"formas",ComunesPDF.CENTER);
						}
						band=registros.next();
						if(band)
							if(!producto.equals(registros.getString(var))){
								
								if(bandera.equals("false")){
									if(registros.getString("NOMBREEPO").equals("-"))
										producto2="PRODUCTO NAFIN: "+registros.getString("IC_NOMBRE");	
							else
								producto2="EPO: "+registros.getString("NOMBREEPO")+"\nPRODUCTO NAFIN: "+registros.getString("IC_NOMBRE");
								}else{
									//producto2=registros.getString(var);
									producto2=registros.getString("NOMBREEPO");
								if(producto2.equals("-"))
								producto2="PRODUCTO NAFIN: "+registros.getString("IC_NOMBRE");
							else
								producto2="EPO: "+registros.getString("NOMBREEPO")+"\nPRODUCTO NAFIN: "+registros.getString("IC_NOMBRE");
								}
								pdfDoc.addTable();
								pdfDoc.setTable(1,100);
								pdfDoc.setCell(producto2, "celda01", ComunesPDF.CENTER);
								pdfDoc.addTable();
								pdfDoc.setTable(col, 100);
								pdfDoc.setCell("Mes", "celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Num. Doctos.", "celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda", "celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto de Documentos", "celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Porcentaje comisión", "celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto Comisión", "celda01",ComunesPDF.CENTER);
								if(bandera.equals("false"))
								pdfDoc.setCell("Pagado", "celda01",ComunesPDF.CENTER);
							}
					}
					pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,col);
					pdfDoc.setCell("SUMA DE TOTALES "+txtMoneda, "celda01", ComunesPDF.CENTER,1);
					pdfDoc.setCell(numDoc, "formas", ComunesPDF.CENTER,1);
					pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
					pdfDoc.setCell("$"+totReg, "formas", ComunesPDF.CENTER,1);
					pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
					pdfDoc.setCell("$"+totMont, "formas", ComunesPDF.CENTER,1);
					if(bandera.equals("false"))
					pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
	
					pdfDoc.addTable();
					pdfDoc.endDocument();
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					infoRegresar=jsonObj.toString();
			
			}
	} else if(informacion.equals("DetalleCSV")||informacion.equals("DetallePDF")){
			String totalContra =(request.getParameter("totalContra")    != null) ?   request.getParameter("totalContra") :"";
			String totalIva =(request.getParameter("totalIva")    != null) ?   request.getParameter("totalIva") :"";
			String totalContraTot =(request.getParameter("totalContaTot")    != null) ?   request.getParameter("totalContaTot") :"";
			//System.out.println("\n\n\n\n\n\n\n\n\n\n\n"+totalContra+"\n"+totalIva+"\n"+totalContraTot);
			
			ComisionFondeoPropio comisionBean = ServiceLocator.getInstance().lookup("ComisionFondeoPropioEJB",ComisionFondeoPropio.class);
			
			Registros registros = comisionBean.getComisionResumen(ic_if,mes_calculo,anio_calculo,ic_moneda,ic_banco_fondeo);
			HashMap	datos;
			List reg=new ArrayList();
			String fecha = "01/"+ mes_calculo + "/" + anio_calculo; 
			double iva = ((BigDecimal)Iva.getValor(fecha)).doubleValue();
			iva*=100;
			 if(informacion.equals("DetalleCSV")){
			 boolean band=registros.next();
						String archivo ="RESUMEN PARA COBRO\nIntermediario Financiero,"+registros.getString("NOMBREIF");
						archivo+="\n"+registros.getString("IC_NOMBRE")+"\n"+
									"Mes Cálculo,Año Cálculo,Contraprestación,% IVA,IVA,Contraprestacion Total,Pagado\n";
						String icAnterior=registros.getString("IC_NOMBRE");
						while(band){
								icAnterior=registros.getString("IC_NOMBRE");			
								archivo+=request.getParameter("mesText")+","+anio_calculo+",";
								archivo+=registros.getString("CONTRAPRESTACION").replaceAll("[,]","")+",";
								double temp=iva;
								archivo+=temp+"";
								archivo+=","+registros.getString("IVA").replaceAll("[,]","")+",";
								archivo+=registros.getString("CONTRAPRESTACIONTOTAL").replaceAll("[,]","")+",";
								archivo+=registros.getString("CG_ESTATUS_PAGADO")+"\n";
								
								band=registros.next();
								if(band)
								if(!icAnterior.equals(registros.getString("IC_NOMBRE"))){
								
								archivo+="\n"+registros.getString("IC_NOMBRE")+"\n"+
									"Mes Cálculo,Año Cálculo,Contraprestación,% IVA,IVA,Contraprestacion Total,Pagado\n";

								}
							}
				CreaArchivo creaArchivo = new CreaArchivo();
				String nombreArchivo;
				if(!creaArchivo.make(archivo, strDirectorioTemp, ".csv")) {
					jsonObj.put("success", new Boolean(false));
					jsonObj.put("msg", "Error al generar el archivo");
				} else {
					nombreArchivo = creaArchivo.nombre;
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			}
				
				
				infoRegresar=jsonObj.toString();
				
			
			}else{
			//Genera PDF
			
			String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			//String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
			boolean band=registros.next();
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("RESUMEN PARA COBRO\n"+registros.getString("NOMBREIF")+"\n","formasB",ComunesPDF.CENTER);
			List lEncabezados = new ArrayList();
			String producto = registros.getString("IC_NOMBRE");
			pdfDoc.setTable(1,100);
			pdfDoc.setCell(producto, "formasmenB", ComunesPDF.CENTER);
			pdfDoc.addTable();
			lEncabezados.add("Mes Cálculo");
			lEncabezados.add("Año Cálculo");
			lEncabezados.add("Contraprestación");
			lEncabezados.add("% IVA");
			lEncabezados.add("IVA");
			lEncabezados.add("Contraprestacion Total");
			lEncabezados.add("Pagado");
			pdfDoc.setTable(lEncabezados, "formasmenB", 100);
			
			
			
			
			
			while(band){
				producto = registros.getString("IC_NOMBRE");
				
				pdfDoc.setCell(request.getParameter("mesText"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(anio_calculo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(registros.getString("CONTRAPRESTACION"),2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(iva+"%"+"", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(registros.getString("IVA"),2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(registros.getString("CONTRAPRESTACIONTOTAL"),2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(registros.getString("CG_ESTATUS_PAGADO"), "formasmen", ComunesPDF.CENTER);
				band=registros.next();
				if(band)
				if(!producto.equals(registros.getString("IC_NOMBRE"))){
					

					pdfDoc.addTable();
					pdfDoc.setTable(1,100);
					pdfDoc.setCell(registros.getString("IC_NOMBRE"), "formasmenB", ComunesPDF.CENTER);
					pdfDoc.addTable();
					pdfDoc.setTable(lEncabezados, "formasmenB", 100);
				}
			}
			
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,7);
			pdfDoc.setCell("GRAN TOTAL:", "formasmenB", ComunesPDF.CENTER,1);
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+totalContra, "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+totalIva, "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+totalContraTot, "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.addTable();

			pdfDoc.endDocument();
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar=jsonObj.toString();
			}
	}else if(informacion.equals("AjusteCSV")||informacion.equals("AjustePDF")){
			String SalRef =(request.getParameter("SalRef")    != null) ?   request.getParameter("SalRef") :"";
			ComisionFondeoPropio comisionBean = ServiceLocator.getInstance().lookup("ComisionFondeoPropioEJB",ComisionFondeoPropio.class);
			
			Registros registros = comisionBean.getAjusteAnual(ic_if,mes_calculo,anio_calculo,ic_moneda,ic_banco_fondeo);
			if(informacion.equals("AjusteCSV")){
			String archivoCSV="";
			boolean band=registros.next();
			archivoCSV+="AJUSTE ANUAL "+anio_calculo+","+registros.getString("NOMBREIF")+"\n";//NOMBREIF
			
			String comision=registros.getString("CG_TIPO_COMISION");
			String producto = registros.getString("IC_NOMBRE");
			
			if(comision.equals("F")){
				archivoCSV+="Ajuste correspondiente al tipo de Comisión Fija";
			}else{
				archivoCSV+="Ajuste correspondiente al tipo de Comisión por Rangos";
			}
			archivoCSV+="\nPRODUCTO NAFIN:,"+registros.getString("IC_NOMBRE")+"\n";//IC_NOMBRE
			archivoCSV+="Mes,Saldo Promedio,Tasa Aplicada,Tasa ajustada,Contraprestación cobrada,IVA,Contraprestación Total Cobrada,Contraprestacion Ajustada,IVA,Contraprestación Total Ajustada,Ajuste\n";

			while(band){
						producto = registros.getString("IC_NOMBRE");
						comision=registros.getString("CG_TIPO_COMISION");
						
						archivoCSV+=meses[Integer.parseInt(registros.getString("IC_MES_CALCULO"))-1]+" "+anio_calculo+",";
						archivoCSV+=registros.getString("FG_SALDO_PROMEDIO")+",";
						archivoCSV+=registros.getString("FG_PORCENTAJE_APLICADO")+",";
						archivoCSV+=registros.getString("FG_PORCENTAJE_AJUSTADO")+",";
						archivoCSV+=registros.getString("FG_CONTRAPRESTACION")+",";
						archivoCSV+=15+",";
						archivoCSV+=Comunes.formatoDecimal((Float.parseFloat(registros.getString("FG_CONTRAPRESTACION"))*1.15),2).replace(',',' ')+",";
						archivoCSV+=registros.getString("FG_CONTRAPRESTACION_AJUSTADA")+",";
						archivoCSV+=15+",";
						archivoCSV+=Comunes.formatoDecimal((Float.parseFloat(registros.getString("FG_CONTRAPRESTACION_AJUSTADA"))*1.15),2).replace(',',' ')+",";
						archivoCSV+=Comunes.formatoDecimal(((Float.parseFloat(registros.getString("FG_CONTRAPRESTACION_AJUSTADA"))*1.15)-(Float.parseFloat(registros.getString("FG_CONTRAPRESTACION"))*1.15)),2).replace(',',' ')+"\n";
						//archivoCSV+=registros.getString("CG_ESTATUS_PAGADO")+"\n";
						
						band=registros.next();
						if(band)
						if(!producto.equals(registros.getString("IC_NOMBRE"))||!comision.equals(registros.getString("CG_TIPO_COMISION"))){
							if(registros.getString("CG_TIPO_COMISION").equals("F")){
								archivoCSV+="Ajuste correspondiente al tipo de Comisión Fija";
								}else{
								archivoCSV+="Ajuste correspondiente al tipo de Comisión por Rangos";
							}
							archivoCSV+="\nPRODUCTO NAFIN:,"+registros.getString("IC_NOMBRE")+"\n";//IC_NOMBRE
							archivoCSV+="Mes,Saldo Promedio,Tasa Aplicada,Tasa ajustada,Contraprestación cobrada,IVA,Contraprestación Total Cobrada,Contraprestacion Ajustada,IVA,Contraprestación Total Ajustada,Ajuste\n";

						}
					
			}
			
			archivoCSV+="SALDO PROMEDIO DE REFERENCIA, $"+SalRef.replace(',',' ');
			
				CreaArchivo creaArchivo = new CreaArchivo();
				String nombreArchivo;
				if(!creaArchivo.make(archivoCSV, strDirectorioTemp, ".csv")) {
					jsonObj.put("success", new Boolean(false));
					jsonObj.put("msg", "Error al generar el archivo");
				} else {
					nombreArchivo = creaArchivo.nombre;
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			}
				infoRegresar=jsonObj.toString();
		}else{
			
			String totSal =(request.getParameter("totSal")    != null) ?   request.getParameter("totSal") :"";
			String totCob =(request.getParameter("totCob")    != null) ?   request.getParameter("totCob") :"";
			String totTotCob =(request.getParameter("totTotCob")    != null) ?   request.getParameter("totTotCob") :"";
			String totAjus =(request.getParameter("totAjus")    != null) ?   request.getParameter("totAjus") :"";
			String totA =(request.getParameter("totA")    != null) ?   request.getParameter("totA") :"";
			String totTotAjus =(request.getParameter("totTotAjus")    != null) ?   request.getParameter("totTotAjus") :"";

			
			
			String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			//String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
			boolean band=registros.next();
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Ajuste Anual ("+anio_calculo+") "+registros.getString("NOMBREIF"),"formasB",ComunesPDF.CENTER);
			List lEncabezados = new ArrayList();
			
			lEncabezados.add("Mes");
			lEncabezados.add("Saldo Promedio");
			lEncabezados.add("Tasa Aplicada");
			lEncabezados.add("Tasa ajustada");
			lEncabezados.add("Contraprestación cobrada");
			lEncabezados.add("IVA");
			lEncabezados.add("Contraprestación Total Cobrada");
			lEncabezados.add("Contraprestacion Ajustada");
			lEncabezados.add("IVA");
			lEncabezados.add("Contraprestación Total Ajustada");
			lEncabezados.add("Ajuste");
			lEncabezados.add("Pagado");
			String comision=registros.getString("CG_TIPO_COMISION");
			String producto = registros.getString("IC_NOMBRE");
			pdfDoc.setTable(1,100);
			if(comision.equals("F")){
				pdfDoc.setCell("Ajuste correspondiente al tipo de Comisión Fija", "formasmenB", ComunesPDF.CENTER);
			}else{
				pdfDoc.setCell("Ajuste correspondiente al tipo de Comisión Por Rangos", "formasmenB", ComunesPDF.CENTER);
			}
			pdfDoc.setCell(registros.getString("IC_NOMBRE"), "formasmenB", ComunesPDF.LEFT);
			pdfDoc.addTable();
			pdfDoc.setTable(lEncabezados, "formasmenB", 100);
			
			while(band){
				producto = registros.getString("IC_NOMBRE");
				comision=registros.getString("CG_TIPO_COMISION");
				
				pdfDoc.setCell(meses[Integer.parseInt(registros.getString("IC_MES_CALCULO"))-1], "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(registros.getString("FG_SALDO_PROMEDIO"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(registros.getString("FG_PORCENTAJE_APLICADO"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(registros.getString("FG_PORCENTAJE_AJUSTADO")+"", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(registros.getString("FG_CONTRAPRESTACION"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(15+"%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal((Float.parseFloat(registros.getString("FG_CONTRAPRESTACION"))*1.15),2)+"", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(registros.getString("FG_CONTRAPRESTACION_AJUSTADA")+"", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(15+"%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal((Float.parseFloat(registros.getString("FG_CONTRAPRESTACION_AJUSTADA"))*1.15),2)+"", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(((Float.parseFloat(registros.getString("FG_CONTRAPRESTACION_AJUSTADA"))*1.15)-(Float.parseFloat(registros.getString("FG_CONTRAPRESTACION"))*1.15)),2)+""+"", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(registros.getString("CG_ESTATUS_PAGADO"), "formasmen", ComunesPDF.CENTER);
				band=registros.next();
				if(band)
				if(!producto.equals(registros.getString("IC_NOMBRE"))||!comision.equals(registros.getString("CG_TIPO_COMISION"))){
					pdfDoc.addTable();
					pdfDoc.setTable(1,100);
					if(registros.getString("CG_TIPO_COMISION").equals("F")){
						pdfDoc.setCell("Ajuste correspondiente al tipo de Comisión Fija", "formasmenB", ComunesPDF.CENTER);
					}else{
						pdfDoc.setCell("Ajuste correspondiente al tipo de Comisión Por Rangos", "formasmenB", ComunesPDF.CENTER);
					}
					pdfDoc.setCell(registros.getString("IC_NOMBRE"), "formasmenB", ComunesPDF.LEFT);
					
					pdfDoc.addTable();
					pdfDoc.setTable(lEncabezados, "formasmenB", 100);
				}
			}
			
			
			
			pdfDoc.setCell("TOTALES "+txtMoneda, "formasB", ComunesPDF.CENTER,12);
			pdfDoc.setCell("SALDO PROMEDIO DE REFERENCIA $"+SalRef, "formasmenB", ComunesPDF.LEFT,12);
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+totSal, "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+totCob, "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+totTotCob, "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+totAjus, "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+totTotAjus, "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+totA, "formasmen", ComunesPDF.CENTER,1);
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,1);

			pdfDoc.addTable();
			pdfDoc.endDocument();
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar=jsonObj.toString();
			}
				
		
		}
	
	


%>

<%=infoRegresar%>