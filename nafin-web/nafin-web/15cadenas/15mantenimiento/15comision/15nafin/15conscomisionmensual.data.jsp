<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		 java.sql.*,
		com.netro.exception.*, 		
		com.netro.pdf.*,		
		 java.math.*,
		com.netro.model.catalogos.*,
		com.netro.fondeopropio.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		com.netro.exception.NafinException,
		org.apache.commons.logging.Log "		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
String ic_producto_nafin = (request.getParameter("ic_producto_nafin")!=null)?request.getParameter("ic_producto_nafin"):"";
String ic_mes_calculo_de = (request.getParameter("ic_mes_calculo_de")!=null)?request.getParameter("ic_mes_calculo_de"):"";
String ic_mes_calculo_a = (request.getParameter("ic_mes_calculo_a")!=null)?request.getParameter("ic_mes_calculo_a"):"";
String anio_calculo = (request.getParameter("anio_calculo")!=null)?request.getParameter("anio_calculo"):"";
String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
String cg_estatus_pagado = (request.getParameter("cg_estatus_pagado")!=null)?request.getParameter("cg_estatus_pagado"):"";



String infoRegresar ="", fecha ="", porcentajeIva ="", consulta ="";

HashMap datos = new HashMap();		
List registros  = new ArrayList();
JSONArray registrosT  = new JSONArray();
JSONObject jsonObj = new JSONObject();
boolean variosTiposComision = false;
int  start= 0, limit =0, totales =0;

Registros reg = new Registros();

List meses = new ArrayList();
meses.add("Enero"); meses.add("Febrero"); meses.add("Marzo");
meses.add("Abril"); meses.add("Mayo"); meses.add("Junio");
meses.add("Julio"); meses.add("Agosto"); meses.add("Septiembre");
meses.add("Octubre"); meses.add("Noviembre"); meses.add("Diciembre");
double valorIva =0;
if(!ic_mes_calculo_a.equals("") &&  !anio_calculo.equals(""))   {
	fecha   = "01/"+ ic_mes_calculo_a + "/" + anio_calculo; // formato fecha: DD/MM/YYYY
	porcentajeIva   = Iva.getPorcentaje(fecha);
	valorIva = ((BigDecimal)Iva.getValor(fecha)).doubleValue();
}
	  
String despMoneda =  ic_moneda.equals("1")?"MONEDA NACIONAL":"DOLARES";

BigDecimal montoTotalDocumentos = new BigDecimal("0.00");
BigDecimal numeroTotalDocumentos = new BigDecimal("0.00");
BigDecimal montoTotalComision = new BigDecimal("0.00");

double dblTotalSaldoPromedioMN = 0, dblTotalContraprestacionMN = 0,  dblTotalContraprestacionTotalMN = 0;
		

ConsComisionCalculo paginador = new ConsComisionCalculo();
paginador.setIc_if(ic_if);
paginador.setIc_producto_nafin(ic_producto_nafin);
paginador.setIc_mes_calculo_de(ic_mes_calculo_de) ;
paginador.setIc_mes_calculo_a(ic_mes_calculo_a) ;
paginador.setAnio_calculo(anio_calculo) ;
paginador.setIc_moneda(ic_moneda);
paginador.setCg_estatus_pagado(cg_estatus_pagado);
paginador.setPorcentajeIva(porcentajeIva);

 	
CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
			
if (informacion.equals("Inicializa_Pantalla")) {

	Calendar calFechaActual = new GregorianCalendar();
	int ultimoDiaDelMesActual = calFechaActual.getActualMaximum(Calendar.DAY_OF_MONTH);
	int anioActual = calFechaActual.get(Calendar.YEAR);
	int mesActual = calFechaActual.get(Calendar.MONTH) +1;  //Dado que 0 es Enero le sumamos 1

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mes_calculo", String.valueOf(mesActual));
	jsonObj.put("anio_calculo", String.valueOf(anioActual));
	infoRegresar = jsonObj.toString();  		

}else if (informacion.equals("catIntermediario")) {

	CatalogoIFRecPropios cat = new CatalogoIFRecPropios();
   cat.setClave("ci.ic_if ");
   cat.setDescripcion(" ci.cg_razon_social " ); 
	cat.setOrden("ci.cg_razon_social");
   infoRegresar = cat.getJSONElementos();

										
}else if (informacion.equals("catAnio")) {

	for(int anio = 2002; anio <= 2015; anio++) {
		datos = new HashMap();			
		datos.put("clave", String.valueOf(anio));
		datos.put("descripcion",String.valueOf(anio));
		registros.add(datos);	
	}
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();		

	
}else if (informacion.equals("catMoneda")) {

	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("COMCAT_MONEDA");
   cat.setCampoClave("IC_MONEDA");
   cat.setCampoDescripcion("CD_NOMBRE"); 
	cat.setOrden("CD_NOMBRE");
	cat.setValoresCondicionIn("1,54", Integer.class);
   infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("catProducto")) {

	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("comcat_producto_nafin");
   cat.setCampoClave("ic_producto_nafin");
   cat.setCampoDescripcion("ic_nombre"); 
	cat.setOrden("ic_producto_nafin");
	cat.setValoresCondicionIn("1,2,4,5", Integer.class);
   infoRegresar = cat.getJSONElementos();
	
}else if ( informacion.equals("Consultar")   ||   informacion.equals("Consulta_Totales")

|| informacion.equals("ArchivoCSV")   || informacion.equals("ArchivoImprimir")    ) {

	if( informacion.equals("Consultar")  )  {
		boolean oculta=false;
		boolean rompe=false;
		boolean muestraAnterior=false;
		reg	=	queryHelper.doSearch();	
		List condIF = new ArrayList();
		String auxIF = "";			
		while(reg.next()){
			rompe=true;
			String miIf = reg.getString("IC_IF")==null?"":reg.getString("IC_IF");	
			if(auxIF.equals("")){
				condIF.add(miIf);
			} else {
				if(auxIF.equals(miIf)){
				} else {
					condIF.add(miIf);
				}
			}
			auxIF = miIf;
		}	
			ConsComisionCalculo paginador2 = new ConsComisionCalculo();
			paginador2.setIc_if(ic_if);
			paginador2.setIc_producto_nafin(ic_producto_nafin);
			paginador2.setIc_mes_calculo_de(ic_mes_calculo_de) ;
			paginador2.setIc_mes_calculo_a(ic_mes_calculo_a) ;
			paginador2.setAnio_calculo(anio_calculo) ;
			paginador2.setIc_moneda(ic_moneda);
			paginador2.setCg_estatus_pagado(cg_estatus_pagado);
			paginador2.setPorcentajeIva(porcentajeIva);
			paginador2.setCondIF("1");//Para acceder a la nueva Consulta
			StringBuffer bIF= new StringBuffer("");
			for(int i=0; i<condIF.size(); i++){
				bIF.append(condIF.get(i)+",");
			}
			String condNueva = "";
			if(rompe){	
				condNueva = bIF.toString().substring(0,bIF.toString().length()-1);
			} else {
				condNueva = "";
			}
			paginador2.setBindIF(condNueva);
			CQueryHelperRegExtJS queryHelper2 = new CQueryHelperRegExtJS( paginador2);
			
		Registros	reg2	= null;
		if(rompe){
			reg2=queryHelper2.doSearch();	
			while(reg2.next()){
				String mes  	=  reg2.getString("DESCRIPCION_MES")==null?"":reg2.getString("DESCRIPCION_MES");			
				String mesDesc =  meses.get(Integer.parseInt(mes) - 1)+" ";
				String saldoEpo= reg2.getString("SALDO_PROMEDIO")==null?"":reg2.getString("SALDO_PROMEDIO");	
				reg2.setObject("PORCENTAJE_IVA",porcentajeIva);
				reg2.setObject("ANIO",anio_calculo);
				reg2.setObject("DESCRIPCION_MES",mesDesc);
				if(saldoEpo.equals("") | saldoEpo.equals("0")){
					 muestraAnterior=true;
					 oculta=true;
				} 
			}
		} else{
			muestraAnterior=true;
		}
		if(muestraAnterior){
			reg	=	queryHelper.doSearch();	
			while(reg.next()){
				String  strMes  =  reg.getString("DESCRIPCION_MES")==null?"":reg.getString("DESCRIPCION_MES");			
				String mes =  meses.get(Integer.parseInt(strMes) - 1)+" ";//+" -"+anio_calculo;		
				reg.setObject("PORCENTAJE_IVA",porcentajeIva);
				reg.setObject("ANIO",anio_calculo);
				reg.setObject("DESCRIPCION_MES",mes);
				oculta=true;
			}
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ ",\"oculta\":"+oculta+" }";
		} else{
			oculta=false;
			consulta	=	"{\"success\": true, \"total\": \""	+	reg2.getNumeroRegistros() + "\", \"registros\": " + reg2.getJSONData()+ ",\"oculta\":"+oculta+" }";
		} 
		
		jsonObj.put("success",new Boolean(true));
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString();
		
		
	}else  if(informacion.equals("Consulta_Totales")  )  {
		
		boolean oculta=false;
		boolean rompe=false;
		boolean muestraAnterior=false;
		reg	=	queryHelper.doSearch();	
		List condIF = new ArrayList();
		String auxIF = "";			
		while(reg.next()){
			rompe=true;
			String miIf = reg.getString("IC_IF")==null?"":reg.getString("IC_IF");	
			if(auxIF.equals("")){
				condIF.add(miIf);
			} else {
				if(auxIF.equals(miIf)){
					log.debug("...");
				} else {
					condIF.add(miIf);
				}
			}
			auxIF = miIf;
		}	
			ConsComisionCalculo paginador2 = new ConsComisionCalculo();
			paginador2.setIc_if(ic_if);
			paginador2.setIc_producto_nafin(ic_producto_nafin);
			paginador2.setIc_mes_calculo_de(ic_mes_calculo_de) ;
			paginador2.setIc_mes_calculo_a(ic_mes_calculo_a) ;
			paginador2.setAnio_calculo(anio_calculo) ;
			paginador2.setIc_moneda(ic_moneda);
			paginador2.setCg_estatus_pagado(cg_estatus_pagado);
			paginador2.setPorcentajeIva(porcentajeIva);
			paginador2.setCondIF("1");//Para acceder a la nueva Consulta
			StringBuffer bIF= new StringBuffer("");
			for(int i=0; i<condIF.size(); i++){
				bIF.append(condIF.get(i)+",");
			}
			String condNueva = "";
			if(rompe){	
				condNueva = bIF.toString().substring(0,bIF.toString().length()-1);
				log.debug("...:"+condNueva);
			} else {
				condNueva = "";
			}
			paginador2.setBindIF(condNueva);
			CQueryHelperRegExtJS queryHelper2 = new CQueryHelperRegExtJS( paginador2);
			
		Registros	reg2	= null;
		if(rompe){
			reg2=queryHelper2.doSearch();	
			dblTotalSaldoPromedioMN = 0; dblTotalContraprestacionMN = 0;  dblTotalContraprestacionTotalMN = 0;
			while(reg2.next()){
				String mes  	=  reg2.getString("DESCRIPCION_MES")==null?"":reg2.getString("DESCRIPCION_MES");			
				String mesDesc =  meses.get(Integer.parseInt(mes) - 1)+" ";
				String saldoEpo= reg2.getString("SALDO_PROMEDIO")==null?"":reg2.getString("SALDO_PROMEDIO");	
				reg2.setObject("PORCENTAJE_IVA",porcentajeIva);
				reg2.setObject("ANIO",anio_calculo);
				reg2.setObject("DESCRIPCION_MES",mesDesc);
				if(saldoEpo.equals("") | saldoEpo.equals("0")){
					 muestraAnterior=true;
					 oculta=true;
				} else {
					String  saldo_promedio  =  reg2.getString("SALDO_PROMEDIO")==null?"0":reg2.getString("SALDO_PROMEDIO");			
					String  contraprestacion  =  reg2.getString("CONTRAPRESTACION")==null?"0":reg2.getString("CONTRAPRESTACION");		
					String  montoTotal  =  reg2.getString("MONTO_TOTAL")==null?"0":reg2.getString("MONTO_TOTAL");		
					
					dblTotalSaldoPromedioMN += Double.parseDouble((String)saldo_promedio);
					dblTotalContraprestacionMN += Double.parseDouble((String)contraprestacion);
					dblTotalContraprestacionTotalMN += Double.parseDouble((String)montoTotal);	
				} 
			}
		} else{
			muestraAnterior=true;
		}
		if(muestraAnterior){
			reg	=	queryHelper.doSearch();	
			dblTotalSaldoPromedioMN = 0; dblTotalContraprestacionMN = 0;  dblTotalContraprestacionTotalMN = 0;
			while(reg.next()){
				String  saldo_promedio  =  reg.getString("SALDO_PROMEDIO")==null?"0":reg.getString("SALDO_PROMEDIO");			
				String  contraprestacion  =  reg.getString("CONTRAPRESTACION")==null?"0":reg.getString("CONTRAPRESTACION");		
				String  montoTotal  =  reg.getString("MONTO_TOTAL")==null?"0":reg.getString("MONTO_TOTAL");		
				
				dblTotalSaldoPromedioMN += Double.parseDouble((String)saldo_promedio);
				dblTotalContraprestacionMN += Double.parseDouble((String)contraprestacion);
				dblTotalContraprestacionTotalMN += Double.parseDouble((String)montoTotal);			
			}
			
		}
			
		
		despMoneda =  ic_moneda.equals("1")?"M.N.":"DOLARES";
		datos = new HashMap();
		datos.put("DESCRIPCION", " Gran Total  "+despMoneda);		
	   datos.put("SALDO_PROMEDIO_TOTAL", Double.toString(dblTotalSaldoPromedioMN)  );
		datos.put("CONTRAPRESTACION_TOTAL", Double.toString (dblTotalContraprestacionMN) );	
		datos.put("MONTO_TOTAL",Double.toString (dblTotalContraprestacionTotalMN) );			
		registrosT.add(datos);		
		
		consulta =  "{\"success\": true, \"total\": \"" + registrosT.size() + "\", \"registros\": " + registrosT.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);		
		infoRegresar = jsonObj.toString();	
		
		
		
			infoRegresar = jsonObj.toString();
	
	
	}else  if(informacion.equals("ArchivoCSV")  )  {
		
		String querySCV = (request.getParameter("csv")!=null)?request.getParameter("csv"):"";
		String idIF = (request.getParameter("idIF")!=null)?request.getParameter("idIF"):"";
		String nombreArchivo="";
		if(querySCV.equals("true")){ //columna epo oculta
			nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
		} else {
			ConsComisionCalculo paginador2 = new ConsComisionCalculo();
			paginador2.setIc_if(ic_if);
			paginador2.setIc_producto_nafin(ic_producto_nafin);
			paginador2.setIc_mes_calculo_de(ic_mes_calculo_de) ;
			paginador2.setIc_mes_calculo_a(ic_mes_calculo_a) ;
			paginador2.setAnio_calculo(anio_calculo) ;
			paginador2.setIc_moneda(ic_moneda);
			paginador2.setCg_estatus_pagado(cg_estatus_pagado);
			paginador2.setPorcentajeIva(porcentajeIva);
			paginador2.setCondIF("1");//Para acceder a la nueva Consulta
			paginador2.setBindIF(idIF);
			CQueryHelperRegExtJS queryHelper2 = new CQueryHelperRegExtJS( paginador2);
			nombreArchivo = queryHelper2.getCreateCustomFile(request,strDirectorioTemp,"CSV");
		}
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();	
	
	}else  if( informacion.equals("ArchivoImprimir")  )  {
	
		String nombreArchivo = "";
		String querySCV = (request.getParameter("csv")!=null)?request.getParameter("csv"):"";
		String idIF = (request.getParameter("idIF")!=null)?request.getParameter("idIF"):"";
		if(querySCV.equals("true")){
			nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
		} else {
			ConsComisionCalculo paginador2 = new ConsComisionCalculo();
			paginador2.setIc_if(ic_if);
			paginador2.setIc_producto_nafin(ic_producto_nafin);
			paginador2.setIc_mes_calculo_de(ic_mes_calculo_de) ;
			paginador2.setIc_mes_calculo_a(ic_mes_calculo_a) ;
			paginador2.setAnio_calculo(anio_calculo) ;
			paginador2.setIc_moneda(ic_moneda);
			paginador2.setCg_estatus_pagado(cg_estatus_pagado);
			paginador2.setPorcentajeIva(porcentajeIva);
			paginador2.setCondIF("1");//Para acceder a la nueva Consulta
			paginador2.setBindIF(idIF);
			CQueryHelperRegExtJS queryHelper2 = new CQueryHelperRegExtJS( paginador2);
			nombreArchivo = queryHelper2.getCreateCustomFile(request,strDirectorioTemp,"PDF");
		}
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();	

	}	 

}
%>
<%=infoRegresar%>