Ext.onReady(function(){

var banderaInicio = true;
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};						
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	//***********************************OPCION DE VER DETALLE  ***************************************
	function transmiteVerDetalle(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			if(info.archivoF!=''){
				Ext.getCmp('archivoF').setValue(info.archivoF);
				Ext.getCmp('btnArchivoFijo').show();				
			}
		
		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	
	
	var ventanaVerDetalle = function() {
	
		Ext.Ajax.request({
			url: '15comision2Ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ver_Detalle'
			}),
			callback: transmiteVerDetalle
		});	
								
		
		var venVerDetalle = Ext.getCmp('venVerDetalle');
		if(venVerDetalle){
			venVerDetalle.show();
		}else{
				
			new Ext.Window({				
				id: 'venVerDetalle',
				modal: true,
				width: 200,
				height: 100,												
				resizable: true,
				constrain: true,
				closable:false,				
				closeAction: 'hide',
				items: [	
					NE.util.getEspaciador(20),
					fpVerDetalle,
					NE.util.getEspaciador(20)
				],
				buttons: [
					'-',
					'->',
					{
						xtype: 'button', 	
						buttonAlign:'right',
						text: 'Cerrar',
						iconCls: 'icoLimpiar',
						id: 'btnCerrar', 
						handler: function(){							
							Ext.getCmp('btnArchivoFijo').hide();																				
							Ext.getCmp('venVerDetalle').destroy();
						} 
					}
				]
			}).show().setTitle('Ver Detalle ');
		}	
	}
	
	
	var fpVerDetalle = {
		xtype: 'container',
		id: 'fpVerDetalle',
		layout: 'table',		
		align: 'center',
		style: 'margin:0 auto;',			
		layoutConfig: {
			columns: 100,
			align: 'center'
		},
		width:	300,
		heigth:	100,
		items:[	
			{
				xtype: 'button',
				//text: 'Archivo Comisi�n Fijo',					
				text :'Saldos Vigentes',
				//tooltip:	'Archivo Comisi�n Fijo',	
				tooltip :'Saldos Vigentes',
				hidden: true,
				id: 'btnArchivoFijo',
				iconCls: 'icoXls',
				handler: function(boton, evento){		
					Ext.Ajax.request({
						url: '15comision2Ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ArchivoFijo',
						archivoF:Ext.getCmp('archivoF').getValue()
						}),
						callback: descargaArchivo
					});
				}			
			},
			{ 	xtype: 'textfield',  hidden: true, id: 'archivoR', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true , id: 'archivoF', 	value: '' }			
		],
		monitorValid: true
	};
	
	
	
	//***********************************PANTALLA DE RESUMEN DE COBRO ***************************************
	
		function transmiteActualizaEstatus(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.MessageBox.alert('Mensaje',info.mensaje, function(){ 
				consResumenCobroData.rejectChanges( );
				consResumenCobroData.load({
					params: Ext.apply(fp.getForm().getValues(),{							
						informacion:  'Cons_Resumen_Cobro',
						tipoConsulta: 'Consulta_Resumen'
					})
				});					
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	
	
	var ventanaResumenparaCobro = function() {
			banderaInicio = true;				
		consResumenCobroData.load({
			params: Ext.apply(fp.getForm().getValues(),{							
				informacion:  'Cons_Resumen_Cobro',
				tipoConsulta: 'Consulta_Resumen'
			})
		});	
					
		var resumenparaCobro = Ext.getCmp('resumenparaCobro');
		if(resumenparaCobro){
			resumenparaCobro.show();
		}else{
			new Ext.Window({				
				id: 'resumenparaCobro',
				modal: true,
				width: 710,
				height: 400,												
				resizable: true,
				constrain: true,
				closable:false,
				closeAction: 'hide',
				items: [					
					gridResumenCobro,
					gridTotalCobro
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{
							xtype: 'button',
							text: 'Actualizaci�n de Estatus',					
							tooltip:	'Actualizaci�n de Estatus',
							hidden: true,
							iconCls: 'icoAutorizar', 
							id: 'btnActualizarEstatus',
							handler: function(boton, evento){
								var arrProductosPagados = [];
								var arrFechasPago = [];
								var  gridResumenCobro = Ext.getCmp('gridResumenCobro');
								var store = gridResumenCobro.getStore();		
								var modificados = store.getModifiedRecords( ) ;
								if((modificados.length > 0)){
									Ext.each(modificados,function(record,index,arrItem){
											arrProductosPagados.push(record.data['IC_PRODUCTO']);
											arrFechasPago.push(Ext.util.Format.date(record.data['FECHA_PAGO'],'d/m/Y'));
									});
									
									Ext.Ajax.request({
										url: '15comision2Ext.data.jsp',
										params: Ext.apply(fp.getForm().getValues(),{
											informacion: 'Actualizar_Resumen',									
											arrProductosPagados	:	arrProductosPagados,
											arrFechasPago			:	arrFechasPago
										}),
										callback: transmiteActualizaEstatus
									});
								}else{
									Ext.Msg.alert('','Favor de capturar la fecha de Pago');
								}
								/*store.each(function(record) {							
									if(record.data['ESTATUS']==true) {
										arrProductosPagados.push(record.data['IC_PRODUCTO']);
										arrProductosPagados.push(record.data['FECHA_PAGO']);
									}
								});*/
								
									
							}
						},
						'-',
						{
							xtype: 'button',
							text: 'Generar Archivo',					
							tooltip:	'Generar Archivo',	
							hidden: true,
							id: 'btnArchivoResumen',
							iconCls: 'icoXls',
							handler: function(boton, evento){							
								Ext.Ajax.request({
									url: '15comision2Ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Generar_Resumen_Cobro',
									tipoConsulta: 'Consulta_Resumen'
									}),
									callback: descargaArchivo
								});
							}
						},
						'-',
						{
							xtype: 'button',
							text: 'Imprimir',					
							tooltip:	'Imprimir',	
							hidden: true,
							id: 'btnImprimir',
							iconCls: 'icoPdf',
							handler: function(boton, evento){							
								Ext.Ajax.request({
									url: '15comision2Ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Imprimir_Resumen_Cobro',
									tipoConsulta: 'Consulta_Resumen'
									}),
									callback: descargaArchivo
								});
							}
						},
						'-',
						{	xtype: 'button',	
							text: 'Salir',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerraP', 
							handler: function(){
								Ext.getCmp('resumenparaCobro').hide();
							} 
						}													
					]
				}
			}).show().setTitle('');
		}	
	}
	
	
	
	var procesarResumenCobroData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
			
		var gridResumenCobro = Ext.getCmp('gridResumenCobro');	
		var el = gridResumenCobro.getGridEl();	
		var jsonData = store.reader.jsonData;	
	
		if (arrRegistros != null) {
			if (!gridResumenCobro.isVisible()) {
				gridResumenCobro.show();
			}				
					
			if(store.getTotalCount() > 0) {	
					banderaInicio = false;				
				//Totales de Resumen de Cobro 
				consTotalCobroData.load({
					params: Ext.apply(fp.getForm().getValues(),{							
						informacion:  'Cons_Total_Cobro',
						tipoConsulta: 'Consulta_Resumen'
					})
				});	
				
				el.unmask();
				Ext.getCmp('gridTotalCobro').show();
				Ext.getCmp('btnActualizarEstatus').show();
				Ext.getCmp('btnArchivoResumen').show();
				Ext.getCmp('btnImprimir').show();
				Ext.getCmp("gridResumenCobro").setTitle(jsonData.titulo);		
				
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');	
				Ext.getCmp('btnActualizarEstatus').hide();
				Ext.getCmp('btnArchivoResumen').hide();
				Ext.getCmp('btnImprimir').hide();
				Ext.getCmp('gridTotalCobro').hide();
			}
		}
	}
	
	var consTotalCobroData = new Ext.data.JsonStore({	
		root : 'registros',
		url : '15comision2Ext.data.jsp',
		baseParams: {
			informacion: 'Cons_Total_Cobro'			
		},		
		fields: [
			{	name: 'DESCRIPCION'},	
			{	name: 'TOTAL_CONTRAPRESTACION'},	
			{	name: 'TOTAL_MONTO_IVA'},	
			{	name: 'CONTRAPRESTACION_TOTAL'}				
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);									
				}
			}
		}					
	});
	
	var gridTotalCobro = {	
		xtype: 'grid',
		store: consTotalCobroData,
		id: 'gridTotalCobro',		
		hidden: false,
		title: '',	
		columns: [	
			{
				header: '',
				tooltip: '',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 110,										
				align: 'center'						
			},			
			{
				header: '',
				tooltip: '',
				dataIndex: 'TOTAL_CONTRAPRESTACION',
				sortable: true,
				width: 110,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: '',
				tooltip: '',
				dataIndex: '',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: '',
				tooltip: '',
				dataIndex: 'TOTAL_MONTO_IVA',
				sortable: true,
				width: 110,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: ' ',
				tooltip: ' ',
				dataIndex: 'CONTRAPRESTACION_TOTAL',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{
				header: '',
				tooltip: '',
				dataIndex: '',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'							
			}
		],
		
		stripeRows: true,
		loadMask: true,
		height: 60,
		width: 700,		
		frame: true	
	}
	
	
	
	var consResumenCobroData = new Ext.data.GroupingStore  ({
		root : 'registros',
		url : '15comision2Ext.data.jsp',
		baseParams: {
			informacion: 'Cons_Resumen_Cobro'			
		},
		hidden: true,
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
				{	name: 'IC_PRODUCTO'},	
				{	name: 'PRODUCTO'},	
				{	name: 'DESCRIPCION_MES'},	
				{	name: 'CONTRAPRESTACION'},	
				{	name: 'POR_IVA'},	
				{	name: 'MONTO_IVA'},	
				{	name: 'CONTRAPRESTACION_TOTAL'},					
				{ name: 'ESTATUS', convert: NE.util.string2boolean},
				{	name: 'FECHA_PAGO',type: 'date', dateFormat: 'd/m/Y'},
				{ name :'CHECK_ESTATUS'},
				{name : 'EDITABLE'},
				{name : 'FECHACALCULO'}				
			]
		}),
		groupField:'PRODUCTO',
		sortInfo:{field: 'PRODUCTO',direction:"ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarResumenCobroData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarResumenCobroData(null, null, null);					
				}
			}
		}					
	});
	var selectModel = new Ext.grid.CheckboxSelectionModel({
		header 		:	'Pagado',
		checkOnly :	true,
		fixed : true,
		id: 'chk',
      tooltip: 'Pagado',
      width: 35,
		singleSelect: false,
			listeners	: {
				beforerowselect: function( sm, rowIndex, keepExisting, record ){
					var pagado = record.get('CHECK_ESTATUS');
					if (pagado == 'S'){
						return false;
					}
				},
				rowdeselect:function ( sm, rowIndex, record ){
					//INhabilitar fecha de pago
						record.data['EDITABLE'] = 'N'; 
						record.reject();
						var grid = Ext.getCmp('gridResumenCobro');
						var columnModelGrid = grid.getColumnModel();
						//grid.startEditing(rowIndex, columnModelGrid.findColumnIndex('FECHA_PAGO'));
				},
				rowselect:function ( sm, rowIndex, record ){
					//habilitar fecha de pago
						record.data['EDITABLE'] = 'S'; 
						var grid = Ext.getCmp('gridResumenCobro');
						var columnModelGrid = grid.getColumnModel();
						grid.startEditing(rowIndex, columnModelGrid.findColumnIndex('FECHA_PAGO'));
				}
			},
			renderer: function(value, metadata, record, rowindex, colindex, store){
				if (record.data['CHECK_ESTATUS']!='S'){
					return '<div class="x-grid3-row-checker">&#160;</div>';
				}else{
					return '<div  >  <input type="checkbox" name="vehicle" value="Car" disabled="disabled" checked="checked"></div>';
				}
		}
	});
	
	var campoFechas = new Ext.form.DateField({
		startDay: 0,
		minValue :'01/01/2001',
		allowBlank : false,
		altFormats : 'd/m/Y'//,
		,format: 'd/m/Y'
	});
	
	var gridResumenCobro = {	
		xtype: 'editorgrid',
		store: consResumenCobroData,
		id: 'gridResumenCobro',		
		hidden: false,
		title: 'Detalle',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		sm : selectModel,
		columns: [	
			{
				header: '',
				tooltip: '',
				dataIndex: 'PRODUCTO',
				sortable: true,
				width: 110,			
				hidden: true,				
				align: 'center'						
			},
			{
				header: 'Mes C�lculo',
				tooltip: 'Mes C�lculo',
				dataIndex: 'DESCRIPCION_MES',
				sortable: true,
				width: 110,			
				resizable: true,				
				align: 'center'						
			},
			{
				header: 'Contraprestaci�n',
				tooltip: 'Contraprestaci�n',
				dataIndex: 'CONTRAPRESTACION',
				sortable: true,
				width: 110,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: '% IVA',
				tooltip: '% IVA',
				dataIndex: 'POR_IVA',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0%')	
			},
			{
				header: 'IVA',
				tooltip: 'IVA',
				dataIndex: 'MONTO_IVA',
				sortable: true,
				width: 110,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Contraprestacion total',
				tooltip: 'Contraprestacion total',
				dataIndex: 'CONTRAPRESTACION_TOTAL',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},	selectModel,		
			{
				header: 'Fecha de Pago',
				tooltip: 'Fecha de Pago',
				dataIndex : 'FECHA_PAGO',
				format: 		'd/m/Y', // 'd-F-Y 
				width : 150,
				align: 'center',
				sortable : false,
				editor:campoFechas,
				renderer:function(value,metadata,registro, rowIndex, colIndex, store){
					value = Ext.util.Format.date(value, 'd/m/Y');
					if(registro.get('CHECK_ESTATUS')== 'S'){
						return 	value;//Ext.util.Format.date(v,'d/m/Y');
					}else {
						return 	NE.util.colorCampoEdit(value,metadata,registro);//NE.util.colorCampoEdit(Ext.util.Format.date(v,'d/m/Y'),metadata,registro);
					}
				}
			}
		],
		view: new Ext.grid.GroupingView({ forceFit: true, groupTextTpl: '{text}'}),
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 280,
		width: 700,
		align: 'center',
		frame: false,
		listeners:{
			beforeedit:function(e){
				if( e.record.data['EDITABLE'] =='N'){
					e.cancel=true;
				}else {
					e.cancel=false;		
				}
			},			
			afteredit : function(e){
			
				var record = e.record;
				var gridResumenCobro = e.gridResumenCobro; 
				var campo= e.field;
				
				if( e.record.data['FECHA_PAGO'] !=''){
					var resultado = datecomp(e.record.data['FECHACALCULO'],Ext.util.Format.date(e.record.data['FECHA_PAGO'] ,'d/m/Y')	);					 
					if (resultado == 1 ||  resultado == 0 ){
						Ext.MessageBox.alert('Mensaje','La fecha de pago no puede ser menor o igual al Mes de cobro. Favor de Verificarlo.');	
						record.data['FECHA_PAGO'] ='';
						record.commit();		
						return;
					}					
				}
				
				
			}
			
		}
	}
	
//****************************PANTALLA DE CONSULTA *****************************	
	
	var  procesoConsultarTotales =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var json = Ext.decode(response.responseText);
			
			var gridResumen = Ext.getCmp('gridResumen');	
			
				var resumen = [
					['IF ', json.nombreIF],
					['Num Documentos ', json.num_Documentos],
					['Monto de los Documentos ', json.monto_Documentos],
					['Monto de la Comisi�n ', json.monto_Comision]					
				];
				
				storeResumenData.loadData(resumen);	
				
				Ext.getCmp("gridResumen").setTitle('RESUMEN SUMA DE TOTALES  ' +json.despMoneda);	
				
				gridResumen.show();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	

	//**************grid de Resumen********************++
	var storeResumenData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridResumen = new Ext.grid.GridPanel({
		id: 'gridResumen',
		store: storeResumenData,
		margins: '20 0 0 0',				
		align: 'center',
		hidden: true,
		title   : 'RESUMEN SUMA DE TOTALES',		
		columns: [
			{
				header : '',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true,
				align: 'left'	
			},
			{
				header : '',			
				dataIndex : 'informacion',
				width : 200,
				align: 'left',
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 430,
		height: 150,
		style: 'margin:0 auto;',		
		frame: true
	});
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		var jsonData = store.reader.jsonData;	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarArchivo').enable();	
				Ext.getCmp('btnResumenparaCobro').enable();
				Ext.getCmp('fpMensaje_1').show();
				Ext.getCmp('fpMensaje_2').show();
				Ext.getCmp('gridResumen').show();	
				Ext.getCmp('btnVerDetalle').enable();					
				Ext.getCmp('btnGenerarDetalle').enable();
				Ext.getCmp('btnGenerarDetalle01').enable();
			
			if(store.getTotalCount() > 0) {					
				el.unmask();	
				
				Ext.getCmp('fpMensaje_1').show();
				Ext.getCmp('mensajeTitulo_1').update("<center><b>Comisi�n Recursos Propios</b></center>");
				
				Ext.getCmp('fpMensaje_2').show();
				Ext.getCmp('mensajeTitulo_2').update("NOTA: IVA DEL "+jsonData.porcentajeIva+"%");
				
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarArchivo').enable();	
				Ext.getCmp('btnResumenparaCobro').enable();
						
				if(jsonData.verDetalle=='S') {
					Ext.getCmp('btnVerDetalle').show();
					Ext.getCmp('btnGenerarDetalle').hide();
					Ext.getCmp('btnGenerarDetalle01').show();
				}else  {
					Ext.getCmp('btnVerDetalle').hide();					
					Ext.getCmp('btnGenerarDetalle').show();
					Ext.getCmp('btnGenerarDetalle01').show();
				}
			
				Ext.Ajax.request({
					url:'15comision2Ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{					
						informacion:'ConsultarTotales'	
					}),
					callback: procesoConsultarTotales
				});
								
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');	
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarArchivo').disable();	
				Ext.getCmp('btnResumenparaCobro').disable();
				Ext.getCmp('fpMensaje_1').hide();
				Ext.getCmp('fpMensaje_2').hide();
				Ext.getCmp('gridResumen').hide();	
				Ext.getCmp('btnVerDetalle').disable();					
				Ext.getCmp('btnGenerarDetalle').disable();
				Ext.getCmp('btnGenerarDetalle01').disable();
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15comision2Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [	
			{	name: 'DESCRIPCION_MES'},	
			{	name: 'NOMBRE_EPO'},	
			{	name: 'NUM_DOCUMENTOS'},	
			{	name: 'MONEDA'},	
			{	name: 'MONTO_DOCUMENTOS'},	
			{	name: 'PORCE_CONTRIBUCION'},	
			{	name: 'MONTO_CONTRIBUCION'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {			
			beforeLoad:	{
				fn: function(store, options){
					var ic_if = Ext.getCmp('ic_if1').getValue();
					var mes_calculo = Ext.getCmp('mes_calculo1').getValue();
					var anio_calculo = Ext.getCmp('anio_calculo1').getValue();
					var ic_moneda = Ext.getCmp('ic_moneda1').getValue();
					Ext.apply(options.params, {
						ic_if:ic_if,
						mes_calculo:mes_calculo,
						anio_calculo:anio_calculo,
						ic_moneda:ic_moneda
					});
				}
			},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});
	
	//******************GRID DE DETALLE
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'DETALLE POR EPO',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Mes',
				tooltip: 'DESCRIPCION_MES',
				dataIndex: 'DESCRIPCION_MES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{	header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 180,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){                                
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{	header: 'Num Documentos',
				tooltip: 'Num Documentos',
				dataIndex: 'NUM_DOCUMENTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{	header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{	header: 'Monto Documentos',
				tooltip: 'Monto Documentos',
				dataIndex: 'MONTO_DOCUMENTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{	header: '% de contribuci�n a la comisi�n',
				tooltip: '% de contribuci�n a la comisi�n',
				dataIndex: 'PORCE_CONTRIBUCION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000000000000000000%')
			},
			{	header: 'Monto de contribuci�n a la comisi�n',
				tooltip: 'Monto de contribuci�n a la comisi�n',
				dataIndex: 'MONTO_CONTRIBUCION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			}
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 780,
		align: 'center',
		frame: false,		
		bbar: {
			xtype: 'paging',
			autoScroll : true,
			height: 45,
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [	
			'->','-',
				{
					xtype: 'button',
					text: 'Ver Detalle',					
					tooltip:	'Ver Detalle',	
					hidden: true,
					id: 'btnVerDetalle',
					iconCls: 'icoBuscar',
					handler:ventanaVerDetalle
				},
				'-',
				{
					xtype: 'button',
					text: 'Saldos Vigentes',///FODEA-039-2014 Cambio de nombre					
					tooltip:	'Saldos Vigentes',	
					hidden: true,
					id: 'btnGenerarDetalle',
					iconCls: 'icoXls',
					handler: function(boton, evento){							
						Ext.Ajax.request({
							url: '15comision2Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Saldos_Vigentes'
							}),
							callback: descargaArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Generar Detalle',///FODEA-039-2014 Nuevo Boton					
					tooltip:	'Generar Detalle',	
					//hidden: true,
					id: 'btnGenerarDetalle01',
					iconCls: 'icoXls',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15comision2Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Generar_Detalle'
							}),success:function(){
								boton.enable();
								boton.setIconClass('icoXls');
							},
							callback: descargaArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Resumen para Cobro',					
					tooltip:	'Resumen para Cobro',
					id: 'btnResumenparaCobro',	
					iconCls: 'icoBuscar',
					handler:ventanaResumenparaCobro
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',					
					id: 'btnGenerarArchivo',
					iconCls: 'icoXls',
					handler: function(boton, evento){							
						Ext.Ajax.request({
							url: '15comision2Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoXLS'
							}),
							callback: descargaArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',					
					id: 'btnImprimir',
					iconCls: 'icoPdf',
					handler: function(boton, evento){							
						Ext.Ajax.request({
							url: '15comision2Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoImprimir'
							}),
							callback: descargaArchivo
						});
					}						
				}	
			]
		}
	});
	
	
		
	var fpMensaje_1 = new Ext.Container({
		layout: 'table',
		id: 'fpMensaje_1',
		hidden: true,	
		width:	200,		
		style: 'margin:0 auto;',
		align: 'center',
		items: [			
			{
				xtype: 'label',
				width		: 200,	
				id: 'mensajeTitulo_1',
				align: 'center',
				value: ''
			}
		]
	});
	
	var fpMensaje_2 = new Ext.Container({
		layout: 'table',
		id: 'fpMensaje_2',
		hidden: true,		
		layoutConfig: {
			columns: 20
		},
		width:	'430',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{
				xtype: 'label',
				width		: 800,	
				id: 'mensajeTitulo_2',
				align: 'center',
				value: ''
			}
		]
	});
	
			
//**********************Criterios de Busqueda **************************

	function procesoInicializa(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('mes_calculo1').setValue(info.mes_calculo);
			Ext.getCmp('anio_calculo1').setValue(info.anio_calculo);
			
		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	
	
	
	var catMoneda = new Ext.data.JsonStore({
		id: 'catMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15comision2Ext.data.jsp',
		baseParams: {
			informacion: 'catMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var catAnio = new Ext.data.JsonStore({
		id: 'catAnio',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15comision2Ext.data.jsp',
		baseParams: {
			informacion: 'catAnio'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMes = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],
		data : [
			['1','Enero'],
			['2','Febrero'],
			['3','Marzo'],
			['4','Abril'],
			['5','Mayo'],
			['6','Junio'],
			['7','Julio'],
			['8','Agosto'],
			['9','Septiembre']	,
			['10','Octubre'],	
			['11','Noviembre'],	
			['12','Diciembre']			
		]
	});
	
	var catIntermediario = new Ext.data.JsonStore({
		id: 'catIntermediario',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15comision2Ext.data.jsp',
		baseParams: {
			informacion: 'catIntermediario'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var elementosForma  = [
		{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			name: 'ic_if',
			id: 'ic_if1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_if',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,	
			width: 300,
			store: catIntermediario,
			tpl:'<tpl for=".">' +
				 '<tpl if="!Ext.isEmpty(loadMsg)">'+
				 '<div class="loading-indicator">{loadMsg}</div>'+
				 '</tpl>'+
				 '<tpl if="Ext.isEmpty(loadMsg)">'+
				 '<div class="x-combo-list-item">' +
				 '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
				 '</div></tpl></tpl>'			
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Mes de C�lculo',			
			combineErrors: false,		
			items: [		
				{
					xtype				: 'combo',
					id					: 'mes_calculo1',
					name				: 'mes_calculo',
					hiddenName 		: 'mes_calculo',
					emptyText:     'Mes ...',
					fieldLabel		: '',
					width				: 80,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'descripcion',					
					store				: catalogoMes,
					msgTarget: 'side',
					margins: '0 20 0 0'				
				},
				{
					xtype: 'combo',
					fieldLabel: '',
					name: 'anio_calculo',
					id: 'anio_calculo1',
					mode: 'local',
					width				: 70,
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'A�o ...',			
					valueField: 'clave',
					hiddenName : 'anio_calculo',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,					
					forceSelection : true,
					store: catAnio,
					msgTarget: 'side',
					margins: '0 20 0 0'					
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Moneda',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'ic_moneda',					
					forceSelection : true,
					triggerAction : 'all',
					msgTarget: 'side',
					margins: '0 20 0 0',
					typeAhead: true,
					minChars : 1,
					forceSelection : true,
					store: catMoneda				
				}
			]
		}

	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 450,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',		
		items:  elementosForma,
		monitorValid: true,
		buttons: [		
			{
				text: 'Generar',
				id: 'btnGenerar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
				
					var ic_if = Ext.getCmp('ic_if1');	
					var mes_calculo = Ext.getCmp('mes_calculo1');	
					var anio_calculo = Ext.getCmp('anio_calculo1');	
					var ic_moneda = Ext.getCmp('ic_moneda1');	
				
				
					if (Ext.isEmpty(ic_if.getValue()) ){
						ic_if.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(mes_calculo.getValue()) ){
						mes_calculo.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(anio_calculo.getValue()) ){
						anio_calculo.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(ic_moneda.getValue()) ){
						ic_moneda.markInvalid('Este campo es obigatorio');
						return;
					}			
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Consultar',
							operacion: 'Generar',					
							start:0,
							limit:15
						})
					});	
					
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',				
				formBind: true,				
				handler: function(boton, evento) {	
					window.location = '15comision2Ext.jsp';
				}
			}
		]
	});
	
				

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			fpMensaje_1,
			NE.util.getEspaciador(20),
			gridResumen,
			NE.util.getEspaciador(20),
			fpMensaje_2,
			NE.util.getEspaciador(20),
			gridConsulta
		]
	});


	catIntermediario.load();	
	catMoneda.load();
	catAnio.load();   

	
	Ext.Ajax.request({
		url: '15comision2Ext.data.jsp',
		params: {
			informacion: "Inicializa_Pantalla"			
		},
		callback: procesoInicializa
	});
	
						
});