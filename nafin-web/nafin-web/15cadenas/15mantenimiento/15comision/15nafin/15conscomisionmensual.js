Ext.onReady(function(){

	//*************************************
	
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};						
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
			
	var consTotalesData = new Ext.data.JsonStore({	
		root : 'registros',
		url : '15conscomisionmensual.data.jsp',
		baseParams: {
			informacion: 'Consulta_Totales'			
		},		
		fields: [
			{	name: 'DESCRIPCION'},
			{	name: 'SALDO_PROMEDIO_TOTAL'},	
			{	name: 'CONTRAPRESTACION_TOTAL'},	
			{	name: 'MONTO_TOTAL'}				
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);									
				}
			}
		}					
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: '',
				tooltip: '',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 110,										
				align: 'center'						
			},			
			{
				header: '',
				tooltip: '',
				dataIndex: 'SALDO_PROMEDIO_TOTAL',
				sortable: true,
				width: 110,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},			
			{
				header: '',
				tooltip: '',
				dataIndex: 'CONTRAPRESTACION_TOTAL',
				sortable: true,
				width: 110,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: ' ',
				tooltip: ' ',
				dataIndex: 'MONTO_TOTAL',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			}
		],
		
		stripeRows: true,
		loadMask: true,
		height: 60,
		width: 780,		
		frame: true	
	});
	
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
					
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		var jsonData = store.reader.jsonData;	
		var oculta =jsonData.oculta;
		var columnModelGrid = gridConsulta.getColumnModel();
		if(oculta){
			columnModelGrid.setHidden( 0, true );
		} else {
			columnModelGrid.setHidden( 0, false );
		}
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}			
			
			if(store.getTotalCount() > 0) {					
				el.unmask();	
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarArchivo').enable();	
				
				consTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{							
						informacion: 'Consulta_Totales'										
					})
				});		
				Ext.getCmp('gridTotales').show();		
					
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');	
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarArchivo').disable();	
				Ext.getCmp('gridTotales').hide();	
			}
		}		
	}
	
	var consultaData = new Ext.data.GroupingStore  ({
		root : 'registros',
		url : '15conscomisionmensual.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
				{	name: 'IC_IF'},
				{	name: 'EPO'},
				{	name: 'IC_PRODUCTO'},	
				{	name: 'PRODUCTO'},	
				{	name: 'NOMBRE_IF'},	
				{	name: 'DESCRIPCION_MES'},
				{	name: 'ANIO'},
				{	name: 'SALDO_PROMEDIO'},
				{	name: 'CONTRAPRESTACION'},
				{	name: 'PORCENTAJE_IVA'},
				{	name: 'MONTO_IVA'},
				{	name: 'MONTO_TOTAL'},
				{	name: 'ESTATUS'}
			]
		}),
		groupField:'PRODUCTO',
		sortInfo:{field: 'PRODUCTO',direction:"ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});
	
	var textoCompleto = function (value, metadata, record, rowIndex, colIndex, store){
		metadata.attr = 'style="white-space: normal; word-wrap: break-word; "';
		return value;
	};
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'DETALLE POR EPO',	
		clicksToEdit: 1,
		hidden: true,
		enableColumnMove: false,
		enableColumnHide: false,
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'EPO',
				sortable: true,
				width : 270,			
				hidden: false,				
				align: 'left',
				renderer	: textoCompleto
			},
			{
				header: 'PRODUCTO',
				tooltip: 'PRODUCTO',
				dataIndex: 'PRODUCTO',
				sortable: true,
				width: 110,			
				hidden: true,				
				align: 'center'						
			},
			{
				header: 'Intermediario Financiero',
				tooltip: 'NOMBRE_IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 270,			
				resizable: true,				
				align: 'left',renderer	: textoCompleto
				/*renderer:function(value,metadata,registro){                                
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}*/			
			},
			{
				header : 'Mes',
				tooltip: 'DESCRIPCION_MES',
				dataIndex: 'DESCRIPCION_MES',
				sortable: true,
				width: 70,			
				resizable: true,				
				align: 'left'			
			},
			{
				header: 'A�o',
				tooltip: 'ANIO',
				dataIndex: 'ANIO',
				sortable: true,
				width: 50,			
				resizable: true,				
				align: 'center'			
			},
			{	
				header: 'Saldo Promedio',
				tooltip: 'Saldo Promedio',
				dataIndex: 'SALDO_PROMEDIO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{	header: 'Contraprestaci�n',
				tooltip: 'Contraprestaci�n',
				dataIndex: 'CONTRAPRESTACION',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{	header: '% IVA',
				tooltip: '% IVA',
				dataIndex: 'PORCENTAJE_IVA',
				sortable: true,
				width: 50,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0%')
			},
			{	header: 'Monto de IVA',
				tooltip: 'MONTO_IVA',
				dataIndex: 'MONTO_IVA',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{	header: 'Total',
				tooltip: 'Total',
				dataIndex: 'MONTO_TOTAL',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{	header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer: function(value){
					if(value =='N')
						return "No Pagado";
					else 
						return "Pagado";
				}
			}	
		],
		view: new Ext.grid.GroupingView({ forceFit: false, groupTextTpl: '{text}'}),
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 780,
		align: 'center',
		frame: false,		
		bbar: {		
			items: [	
			'->','-',	
			{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',					
					id: 'btnGenerarArchivo',
					iconCls: 'icoXls',
					handler: function(boton, evento){
						var columnModelGrid = gridConsulta.getColumnModel();
						var csv2=false;
						var store = gridConsulta.getStore();
						var reg = store.getAt(0);
						if(columnModelGrid.isHidden(0)){
							csv2=true;						
						}
						Ext.Ajax.request({
							url: '15conscomisionmensual.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV',
							csv: csv2,
							idIF:reg.get('IC_IF')
							}),
							callback: descargaArchivo
						});
					}
				},				
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',					
					id: 'btnImprimir',
					iconCls: 'icoPdf',
					handler: function(boton, evento){
						var columnModelGrid = gridConsulta.getColumnModel();
						var csv2=false;
						var store = gridConsulta.getStore();
						var reg = store.getAt(0);
						if(columnModelGrid.isHidden(0)){
							csv2=true;						
						}
						Ext.Ajax.request({
							url: '15conscomisionmensual.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoImprimir',
							csv: csv2,
							idIF:reg.get('IC_IF')
							}),
							callback: descargaArchivo
						});
					}						
				}
			]
		}
	});
	
	

	//**********************Criterios de Busqueda **************************

	function procesoInicializa(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('ic_mes_calculo_de1').setValue(info.mes_calculo);
			Ext.getCmp('ic_mes_calculo_a1').setValue(info.mes_calculo);			
			Ext.getCmp('anio_calculo1').setValue(info.anio_calculo);
			
		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	
	var catMoneda = new Ext.data.JsonStore({
		id: 'catMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15conscomisionmensual.data.jsp',
		baseParams: {
			informacion: 'catMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var catAnio = new Ext.data.JsonStore({
		id: 'catAnio',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15conscomisionmensual.data.jsp',
		baseParams: {
			informacion: 'catAnio'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMes = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],
		data : [
			['1','Enero'],
			['2','Febrero'],
			['3','Marzo'],
			['4','Abril'],
			['5','Mayo'],
			['6','Junio'],
			['7','Julio'],
			['8','Agosto'],
			['9','Septiembre']	,
			['10','Octubre'],	
			['11','Noviembre'],	
			['12','Diciembre']			
		]
	});
	
	var catalogoEstatus = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],
		data : [
			['S','Pagado'],
			['N','No Pagado']			
		]
	});
	
	
	
	
	var catProducto = new Ext.data.JsonStore({
		id: 'catProducto',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15conscomisionmensual.data.jsp',
		baseParams: {
			informacion: 'catProducto'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catIntermediario = new Ext.data.JsonStore({
		id: 'catIntermediario',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15conscomisionmensual.data.jsp',
		baseParams: {
			informacion: 'catIntermediario'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	
	
	var elementosForma  = [
		{
			xtype: 'combo',
			fieldLabel: 'IF',
			name: 'ic_if',
			id: 'ic_if1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_if',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,	
			width: 300,
			store: catIntermediario,
			tpl:'<tpl for=".">' +
				 '<tpl if="!Ext.isEmpty(loadMsg)">'+
				 '<div class="loading-indicator">{loadMsg}</div>'+
				 '</tpl>'+
				 '<tpl if="Ext.isEmpty(loadMsg)">'+
				 '<div class="x-combo-list-item">' +
				 '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
				 '</div></tpl></tpl>'			
		},
		{
			xtype: 'combo',
			fieldLabel: 'Producto',
			name: 'ic_producto_nafin',
			id: 'ic_producto_nafin1',
			mode: 'local',
			width	: 240,
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione un Producto  ...',			
			valueField: 'clave',
			hiddenName : 'ic_producto_nafin',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,					
			forceSelection : true,
			store: catProducto,
			msgTarget: 'side',
			margins: '0 20 0 0'					
		},				
		{
			xtype: 'compositefield',
			fieldLabel: 'Mes de C�lculo',			
			combineErrors: false,		
			items: [		
				{
					xtype				: 'combo',
					id					: 'ic_mes_calculo_de1',
					name				: 'ic_mes_calculo_de',
					hiddenName 		: 'ic_mes_calculo_de',
					emptyText:     'Mes ...',
					fieldLabel		: '',
					width				: 80,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'descripcion',					
					store				: catalogoMes,
					msgTarget: 'side',
					margins: '0 20 0 0'				
				},
				{
					xtype: 'displayfield',
					value: ' A '						
				}	,
				{
					xtype				: 'combo',
					id					: 'ic_mes_calculo_a1',
					name				: 'ic_mes_calculo_a',
					hiddenName 		: 'ic_mes_calculo_a',
					emptyText:     'Mes ...',
					fieldLabel		: '',
					width				: 80,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'descripcion',					
					store				: catalogoMes,
					msgTarget: 'side',
					margins: '0 20 0 0'				
				},
				
				{
					xtype: 'combo',
					fieldLabel: '',
					name: 'anio_calculo',
					id: 'anio_calculo1',
					mode: 'local',
					width				: 70,
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'A�o ...',			
					valueField: 'clave',
					hiddenName : 'anio_calculo',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,					
					forceSelection : true,
					store: catAnio,
					msgTarget: 'side',
					margins: '0 20 0 0'					
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Moneda',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'ic_moneda',					
					forceSelection : true,
					triggerAction : 'all',
					msgTarget: 'side',
					margins: '0 20 0 0',
					typeAhead: true,
					minChars : 1,
					forceSelection : true,
					store: catMoneda				
				}
			]
		},
		{
			xtype				: 'combo',
			id					: 'cg_estatus_pagado1',
			name				: 'cg_estatus_pagado',
			hiddenName 		: 'cg_estatus_pagado',
			emptyText:     '',
			fieldLabel		: 'Estatus',
			width				: 80,
			forceSelection	: true,
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'clave',
			displayField	:'descripcion',					
			store				: catalogoEstatus,
			msgTarget: 'side',
			margins: '0 20 0 0'				
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 450,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',		
		items:  elementosForma,
		monitorValid: true,
		buttons: [		
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
									
					var ic_mes_calculo_de1 = Ext.getCmp('ic_mes_calculo_de1');	
					var ic_mes_calculo_a1 = Ext.getCmp('ic_mes_calculo_a1');	
					
					var anio_calculo = Ext.getCmp('anio_calculo1');	
					var ic_moneda = Ext.getCmp('ic_moneda1');	
								
					
					if (Ext.isEmpty(ic_mes_calculo_de1.getValue()) ){
						ic_mes_calculo_de1.markInvalid('Este campo es obigatorio');
						return;
					}
						if (Ext.isEmpty(ic_mes_calculo_a1.getValue()) ){
						ic_mes_calculo_a1.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(anio_calculo.getValue()) ){
						anio_calculo.markInvalid('Este campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(ic_moneda.getValue()) ){
						ic_moneda.markInvalid('Este campo es obigatorio');
						return;
					}			
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
							
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Consultar'										
							
						})
					});					
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',				
				formBind: true,				
				handler: function(boton, evento) {	
					window.location = '15conscomisionmensualExt.jsp';
				}
			}
		]
	});
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales
		]
	});


	catIntermediario.load();	
	catMoneda.load();
	catAnio.load();  
	catProducto.load();

	
	Ext.Ajax.request({
		url: '15conscomisionmensual.data.jsp',
		params: {
			informacion: "Inicializa_Pantalla"			
		},
		callback: procesoInicializa
	});
	
	
						
});