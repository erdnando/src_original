<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		 java.sql.*,
		com.netro.exception.*, 		
		com.netro.pdf.*,		
		 java.math.*,
		com.netro.model.catalogos.*,
		com.netro.fondeopropio.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		com.netro.exception.NafinException,
		org.apache.commons.logging.Log "		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
String mes_calculo = (request.getParameter("mes_calculo")!=null)?request.getParameter("mes_calculo"):"1";
String anio_calculo = (request.getParameter("anio_calculo")!=null)?request.getParameter("anio_calculo"):"";
String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String tipoConsulta = (request.getParameter("tipoConsulta")!=null)?request.getParameter("tipoConsulta"):"Consulta";
String archivoF = (request.getParameter("archivoF")!=null)?request.getParameter("archivoF"):"";
String archivoR = (request.getParameter("archivoR")!=null)?request.getParameter("archivoR"):"";
 
//Se crea la instancia del EJB para su utilización.
ComisionFondeoPropio comisionBean = ServiceLocator.getInstance().lookup("ComisionFondeoPropioEJB",ComisionFondeoPropio.class);

String infoRegresar ="", consulta ="",  verDetalle = "N", fecha ="", porcentajeIva ="", mensaje ="";

HashMap datos = new HashMap();		
List registros  = new ArrayList();
JSONObject jsonObj = new JSONObject();
boolean variosTiposComision = false;
int  start= 0, limit =0, totales =0;

Registros reg = new Registros();

List meses = new ArrayList();
meses.add("Enero"); meses.add("Febrero"); meses.add("Marzo");
meses.add("Abril"); meses.add("Mayo"); meses.add("Junio");
meses.add("Julio"); meses.add("Agosto"); meses.add("Septiembre");
meses.add("Octubre"); meses.add("Noviembre"); meses.add("Diciembre");
String descripcionMes = meses.get(Integer.parseInt(mes_calculo)-1)+" - "+anio_calculo;
String fechaFinal  ="", mes_calculo_ ="";

if(!mes_calculo.equals("11") && !mes_calculo.equals("12") )  {
	mes_calculo_ = "0"+mes_calculo;
}else  {
	mes_calculo_ = mes_calculo;
}

if(!mes_calculo.equals("2"))  {
		fechaFinal = "30/"+ mes_calculo_ + "/" + anio_calculo; 
}else  {
		fechaFinal = "28/"+ mes_calculo_ + "/" + anio_calculo; 
}
	

double iva  = 0;


if(!mes_calculo.equals("") &&  !anio_calculo.equals(""))   {
	// Obtener el valor del IVA
	fecha = "01/"+ mes_calculo + "/" + anio_calculo; 
	iva = ((BigDecimal)Iva.getValor(fecha)).doubleValue();
	porcentajeIva = Iva.getPorcentaje(fecha);
}
String despMoneda =  ic_moneda.equals("1")?"MONEDA NACIONAL":"DOLARES";

BigDecimal montoTotalDocumentos = new BigDecimal("1");
BigDecimal numeroTotalDocumentos = new BigDecimal("0");
BigDecimal montoTotalComision = new BigDecimal("1");

ComisionCalculo paginador = new ComisionCalculo();
paginador.setIc_if(ic_if);
paginador.setMes_calculo(mes_calculo);
paginador.setAnio_calculo(anio_calculo);
paginador.setIc_moneda(ic_moneda);
paginador.setDescripcionMes(descripcionMes);
paginador.setIva(Double.toString (iva));
paginador.setPorcentajeIva(porcentajeIva);
paginador.setDespMoneda(despMoneda);    
paginador.setTipoConsulta(tipoConsulta); 	 
paginador.setFechaFinal(fechaFinal); 	   

	
CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
			
if (informacion.equals("Inicializa_Pantalla")) {

	Calendar calFechaActual = new GregorianCalendar();
	int ultimoDiaDelMesActual = calFechaActual.getActualMaximum(Calendar.DAY_OF_MONTH);
	int anioActual = calFechaActual.get(Calendar.YEAR);
	int mesActual = calFechaActual.get(Calendar.MONTH) +1;  //Dado que 0 es Enero le sumamos 1
	int anioInicial = anioActual;
	int mesInicial = mesActual - 1;  //Mes de calculo anterior al actual
	if (mesActual == 1) {	//Si el mes actual es  Enero
		mesInicial = 12; //diciembre
		anioInicial-- ; //año anterior
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mes_calculo", String.valueOf(mesInicial));
	jsonObj.put("anio_calculo", String.valueOf(anioInicial));
	infoRegresar = jsonObj.toString();  		

}else if (informacion.equals("catIntermediario")) {

	CatalogoIFRecPropios cat = new CatalogoIFRecPropios();
   cat.setClave("ci.ic_if ");
   cat.setDescripcion(" ci.cg_razon_social " ); 
	cat.setOrden("ci.cg_razon_social");
   infoRegresar = cat.getJSONElementos();

										
}else if (informacion.equals("catAnio")) {

	for(int anio = 2002; anio <= 2015; anio++) {
		datos = new HashMap();			
		datos.put("clave", String.valueOf(anio));
		datos.put("descripcion",String.valueOf(anio));
		registros.add(datos);	
	}
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();		

	
}else if (informacion.equals("catMoneda")) {

	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("COMCAT_MONEDA");
   cat.setCampoClave("IC_MONEDA");
   cat.setCampoDescripcion("CD_NOMBRE"); 
	cat.setOrden("CD_NOMBRE");
	cat.setValoresCondicionIn("1,54", Integer.class);
   infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consultar")  || informacion.equals("ConsultarTotales") 
	|| informacion.equals("ArchivoImprimir")  ||   informacion.equals("ArchivoXLS")     ){
	
	String  montoTotalComi=   paginador.getMontoTotalComision (ic_if , anio_calculo , mes_calculo  , ic_moneda , iva   );
	montoTotalComision = new BigDecimal(montoTotalComi);
		
	HashMap  regTotales=   paginador.getMontoTotales (ic_if , anio_calculo , mes_calculo  , ic_moneda   );
	
	if(regTotales.size()>0)  {
		String  montoTotal =  regTotales.get("TOTAL_MONTO_DOCUMENTOS").toString();
		montoTotalDocumentos = new BigDecimal(montoTotal);
	}
	
	if (informacion.equals("Consultar") ){	
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
					
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
			 
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				if(strPerfil.equalsIgnoreCase("ADMIN NAFIN") ){
					comisionBean.calcularComisionMensual(ic_if, mes_calculo, anio_calculo, "ADMIN NAFIN", ic_moneda);//Este método realiza el cálculo de la comision mensual, si ya hay un claculo previo, no hace nada :P
					variosTiposComision = comisionBean.manejaVariosTiposComision(ic_if, mes_calculo, anio_calculo, "ADMIN NAFIN", ic_moneda, false);//Este método verifica si el if maneja varios tipos de comisión, de ser así despliega el resultado por cada EPO		
				}						
					
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
				
			// Edito los registros 
			reg = queryHelper.getPageResultSet(request,start,limit);		
			// Editos los campos que se requien 
			while(reg.next()){
				String  montoDoc  =  reg.getString("MONTO_DOCUMENTOS")==null?"0":reg.getString("MONTO_DOCUMENTOS");			
				BigDecimal montoDocumentos = new BigDecimal(montoDoc);
				BigDecimal porcentajeComision = (montoDocumentos.multiply(new BigDecimal("100"))).divide(montoTotalDocumentos, 20, BigDecimal.ROUND_HALF_UP);
				BigDecimal montoComision = (montoTotalComision.multiply(porcentajeComision)).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP);
				String  contriComision  =  reg.getString("FG_CONTRI_COMISION")==null?"0":reg.getString("FG_CONTRI_COMISION");					
						
				if(contriComision.toString().equals("0"))  {
					reg.setObject("MONTO_CONTRIBUCION",montoComision);		
				}else  {
					reg.setObject("MONTO_CONTRIBUCION",contriComision);	
				}
				
				reg.setObject("PORCE_CONTRIBUCION",porcentajeComision);
					
				totales++;
			}	
			if(totales>0 ){
				if(strPerfil.equalsIgnoreCase("ADMIN NAFIN") ){	
					if (comisionBean.existeDetalleComision(ic_if, mes_calculo, anio_calculo, "ADMIN NAFIN", ic_moneda, false)) {
						verDetalle = "S";
					} 
				}
			}
				
			
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		
		consulta =  "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + reg.getJSONData()+"}";
			
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("verDetalle", verDetalle);		
		jsonObj.put("despMoneda", despMoneda);	
		jsonObj.put("porcentajeIva", porcentajeIva);	
		infoRegresar = jsonObj.toString();  	
		
	
	}else if (informacion.equals("ConsultarTotales")) {	
	
		queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
		reg =  queryHelper.getResultCount(request);  
		
		String  nombreIF  ="", num_Documentos ="", monto_Documentos ="", monto_Comision  = "0";
		
	   if(reg.next()){
			nombreIF =  reg.getString(1);			
			num_Documentos =  reg.getString(2);
			monto_Documentos =  reg.getString(3);				
		}
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("nombreIF", nombreIF);
		jsonObj.put("num_Documentos", num_Documentos);
		jsonObj.put("monto_Documentos","$" + Comunes.formatoDecimal(monto_Documentos,2)  );
		jsonObj.put("monto_Comision", "$" + Comunes.formatoDecimal(montoTotalComi,2) );		
		jsonObj.put("despMoneda", despMoneda);		
		infoRegresar = jsonObj.toString();  
		
	}else if(informacion.equals("ArchivoImprimir")){
	
		String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
			
	}else if(informacion.equals("ArchivoXLS")){
	
		String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"XLS");
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
		
	}	
}else if(  informacion.equals("Cons_Resumen_Cobro")   ||    informacion.equals("Cons_Total_Cobro")  
	|| informacion.equals("Imprimir_Resumen_Cobro") || informacion.equals("Generar_Resumen_Cobro")    )  {
		
	 if(informacion.equals("Cons_Resumen_Cobro")){
	
		String nombreIF ="", titulo ="";
		reg	=	queryHelper.doSearch();	
		while (reg.next()) {
			nombreIF = reg.getString("NOMBRE_IF")==null?"":reg.getString("NOMBRE_IF");
		}
		titulo = "<center> RESUMEN PARA COBRO <br>  Intermediario Financiero  "+nombreIF +" <br> Descuento Electrónico </center>";


		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("titulo",titulo);
		infoRegresar = jsonObj.toString();	
	
	}else if(informacion.equals("Cons_Total_Cobro")){
	 
		reg	=	queryHelper.doSearch();	
					
		JSONArray registrosT  = new JSONArray();
	
		double dblGranTotalContraprestacion =  0,  dblGranTotalIva =  0,  dblGranTotalContraprestacionTotal =  0;		
	
		while (reg.next()) {
			String contrapestatacion = reg.getString("CONTRAPRESTACION")==null?"":reg.getString("CONTRAPRESTACION");
			String montoIva = reg.getString("MONTO_IVA")==null?"":reg.getString("MONTO_IVA");
			String contraprestacio_total =   reg.getString("CONTRAPRESTACION_TOTAL")==null?"":reg.getString("CONTRAPRESTACION_TOTAL");		
				
			dblGranTotalContraprestacion += Double.parseDouble((String)contrapestatacion);
			dblGranTotalIva += Double.parseDouble((String)montoIva);
			dblGranTotalContraprestacionTotal += Double.parseDouble((String)contraprestacio_total);
						
		}
		datos = new HashMap();
		datos.put("DESCRIPCION", "GRAN TOTAL");		
	   datos.put("TOTAL_CONTRAPRESTACION", Double.toString(dblGranTotalContraprestacion)  );
		datos.put("TOTAL_MONTO_IVA", Double.toString (dblGranTotalIva) );	
		datos.put("CONTRAPRESTACION_TOTAL",Double.toString (dblGranTotalContraprestacionTotal) );			
		registrosT.add(datos);		
		
		consulta =  "{\"success\": true, \"total\": \"" + registrosT.size() + "\", \"registros\": " + registrosT.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);		
		infoRegresar = jsonObj.toString();					
		
	}else if(informacion.equals("Imprimir_Resumen_Cobro")){
	
		String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
			
	}else if(informacion.equals("Generar_Resumen_Cobro")){
	
		String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	}
	
}else if(informacion.equals("Actualizar_Resumen")){
	try{
		String[] arrProductosPagados 	= request.getParameterValues("arrProductosPagados");
		String[] arrFechasPago 			= request.getParameterValues("arrFechasPago");
		for(int i=0; i<arrProductosPagados.length; i++){
			System.err.println(arrProductosPagados[i]);
			System.err.println(arrFechasPago[i]);
		}			
		comisionBean.actualizarEstatusPago(ic_if, anio_calculo, mes_calculo, arrProductosPagados, arrFechasPago, "ADMIN NAFIN", ic_moneda);
		mensaje = "La información ha sido actualizada satisfactoriamente";
	
	}catch(Exception e){
		out.println(e.getMessage());
	}

	jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("mensaje",mensaje);
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("ver_Detalle")){

	HashMap archivos=  paginador.getArchivosVerDetalle(ic_if , anio_calculo , mes_calculo  ,  ic_moneda ,  strDirectorioTemp  );
	
	archivoF=  archivos.get("ARCHIVO_FIJO").toString();  
	
	jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("archivoF",archivoF);
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("ArchivoFijo")){

	jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("urlArchivo",strDirecVirtualTemp+archivoF);
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("Saldos_Vigentes")){//FODEA-039-2014 Cambio de Nombre

	String nombreArchivo =   paginador.getSaldos_Vigentes   ( ic_if ,  anio_calculo ,  mes_calculo  ,  ic_moneda ,  strDirectorioTemp  );

	jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("Generar_Detalle")){//FODEA-039-2014 Nuevo
	String nombreArchivo =   paginador.generarArchivoDetalles  ( ic_if ,  anio_calculo ,  mes_calculo  ,  ic_moneda ,  strDirectorioTemp  );
	jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();
}


%>
<%=infoRegresar%>

