<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.descuento.*,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.model.catalogos.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");
String infoRegresar	= "";
if(informacion.equals("CatalogoProducto")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_producto_nafin");
		cat.setCampoDescripcion("ic_nombre");
		cat.setTabla("comcat_producto_nafin");
		cat.setOrden("ic_nombre");
		cat.setCondicionIn("1,4","Integer");
		infoRegresar = cat.getJSONElementos();
	
	}else if(informacion.equals("CatalogoEpo")){
		String producto 	= (request.getParameter("producto")	== null)?"":request.getParameter("producto");
		String tipoAfiliado 	= (request.getParameter("tipoAfiliado")	== null)?"":request.getParameter("tipoAfiliado");
		String claveIf 	= (request.getParameter("claveIf")	== null)?"":request.getParameter("claveIf");
		/*
		CatalogoEPOProd cat = new CatalogoEPOProd();
		cat.setClave("e.ic_epo");
		cat.setProducto(producto);
		cat.setDescripcion("e.cg_razon_social");
		infoRegresar = cat.getJSONElementos();
		*/
		
		CatalogoEPOSwift cat = new CatalogoEPOSwift();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setTipoAfiliado(tipoAfiliado);
		cat.setCveProducto(producto);
		if(!"".equals(claveIf)){
			cat.setCveIf(claveIf);
		}
		infoRegresar = cat.getJSONElementos();
	
	}else if (	informacion.equals("CatalogoBuscaAvanzada") ) {

	String num_pyme	= (request.getParameter("num_pyme")==null)?"":request.getParameter("num_pyme");
	String rfc_pyme	= (request.getParameter("rfc_pyme")==null)?"":request.getParameter("rfc_pyme");
	String nombre_pyme= (request.getParameter("nombre_pyme")==null)?"":request.getParameter("nombre_pyme");
	String ic_epo		= (request.getParameter("HicEpo")==null)?"":request.getParameter("HicEpo");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

	Registros registros = afiliacion.getProveedores("",ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pyme,"","");

	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (registros.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(registros.getString("IC_PYME"));
		elementoCatalogo.setDescripcion(registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("CG_RAZON_SOCIAL"));
		coleccionElementos.add(elementoCatalogo);
	}	
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenNombrePyme")) {

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
	
	Afiliacion afiliacion= ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

	JSONObject jsonObj			= new JSONObject();
	String numeroDeProveedor	= (request.getParameter("numeroDeProveedor")!=null)?request.getParameter("numeroDeProveedor"):"";

	//Registros registros			= new Registros();

	String ic_pyme					= "";
	String txtCadenasPymes		= "";
	String rfc						= "";

	//registros					= BeanParamDscto.getParametrosPymeNafin(numeroDeProveedor);
	Map registros =afiliacion.buscaAfiliado(numeroDeProveedor);

		if(registros != null && registros.get("clave") != null){
			//ic_pyme					= (registros.getString("PYME")	== null)?"":registros.getString("PYME");
			//txtCadenasPymes		= (registros.getString("NOMBRE")	== null)?"":registros.getString("NOMBRE");			
			//rfc 						= (registros.getString("RFC")	== null)?"":registros.getString("RFC");
			
			ic_pyme					= (registros.get("clave")	== null)?"":(String)registros.get("clave");
			txtCadenasPymes		= (registros.get("nombre")	== null)?"":(String)registros.get("nombre");
			rfc 						= (registros.get("rfc")	== null)?"":(String)registros.get("rfc");
		}else{
			jsonObj.put("muestraMensaje", new Boolean(true));
			jsonObj.put("textoMensaje", 	"El nafin electrónico no corresponde a una<br>PyME afiliada a la EPO o no existe.");
		
	}

	jsonObj.put("ic_pyme",					ic_pyme									);
	jsonObj.put("txt_nafelec",				numeroDeProveedor						);
	jsonObj.put("txtCadenasPymes",		txtCadenasPymes						);	
	jsonObj.put("rfc",		rfc						);	
	jsonObj.put("estadoSiguiente",		"RESPUESTA_OBTENER_NOMBRE_PYME"	);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Consultar")||informacion.equals("GenerarPDF")||informacion.equals("GenerarArchivo") ||  informacion.equals("GenerarArchivosCSV") ) {
	
	
	String afiliado 	= (request.getParameter("Afiliado")	== null)?"":request.getParameter("Afiliado");
	String producto		= (request.getParameter("Producto") 	== null)?"":request.getParameter("Producto");
	String epo 	= (request.getParameter("HicEpo")	== null)?"":request.getParameter("HicEpo");
	String neE		= (request.getParameter("txt_nafelec") 	== null)?"":request.getParameter("txt_nafelec");
	String rfc 	= (request.getParameter("R_F_C")	== null)?"":request.getParameter("R_F_C");
	String fecha_emi_ini		= (request.getParameter("fechaEm1") 	== null)?"":request.getParameter("fechaEm1");
	String fecha_emi_fin 	= (request.getParameter("fechaEm2")	== null)?"":request.getParameter("fechaEm2");
	String cuentas_sin_validar		= (request.getParameter("Cuentas") 	== null)?"":request.getParameter("Cuentas");

	String moneda		= (request.getParameter("cboMoneda01") 	== null)?"":request.getParameter("cboMoneda01");//FODEA-033-2014

	ConsCLABESwift paginador = new  ConsCLABESwift();
	paginador.setCmb_tipo_afiliado(afiliado);
	paginador.setCuentas_sin_validar(cuentas_sin_validar);
	paginador.setFecha_registro_de(fecha_emi_ini);
	paginador.setFecha_registro_a(fecha_emi_fin);
	paginador.setNo_nafin(neE);
	paginador.setNo_epo(epo);
	paginador.setNo_producto(producto);
	paginador.setRfc(rfc);
	paginador.setMoneda(moneda);//FODEA-033-2014
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";
	JSONObject jsonObj			= new JSONObject();
	if(informacion.equals("Consultar")){
		 try {
		  Registros reg = queryHelper.doSearch();
		  infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
		 } 
		 catch(Exception e) {
		  throw new AppException("Error en la paginacion", e);
		 }
	 } else if(informacion.equals("GenerarArchivo")){
			String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("GenerarPDF")){
			String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		
		}else if(informacion.equals("GenerarArchivosCSV")){
			String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSVGRAL"); 
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("descarga",new Boolean(true)); 
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
	}else if(informacion.equals("procesoEliminaRegistro")){
		String mensaje = "";
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		
		String afiliadoE 	= (request.getParameter("afiliadoE")	== null)?"":request.getParameter("afiliadoE");
		String cuenta		= (request.getParameter("cuenta") 	== null)?"":request.getParameter("cuenta");
		String estatus_cecoban 	= (request.getParameter("estatus_cecoban")	== null)?"":request.getParameter("estatus_cecoban");
		String cuenta_dolares		= (request.getParameter("cuenta_dolares") 	== null)?"":request.getParameter("cuenta_dolares");
		int existe = dispersion.getExisteCuenta(cuenta);
		boolean banderaElimina=true;
		boolean banderaValida = false;
		
		if( cuenta_dolares.equals( "true")){
		if(existe>0) {
			banderaElimina=false;
			 mensaje="Esta cuenta no se puede eliminar ya que tiene operaciones asociadas.";
			//alert("Esta cuenta no se puede eliminar ya que tiene operaciones asociadas.");
			//return;	
		} else {
			if(estatus_cecoban.equals("99")) {
				
					mensaje="Ya existe la validación de esta Cuenta Swift, realmente desea eliminarla?";
					banderaValida=true;
					banderaElimina=true;
					//ruta.accion.value = "Valida";
					
				
			} else {
				banderaValida = false;
				banderaElimina=true;
				mensaje="¿Esta seguro de querer eliminar el registro?";
			}
		}
	}else{
		if(existe>0) {
			banderaElimina=false;
			mensaje="Esta cuenta no se puede eliminar ya que tiene operaciones asociadas.";
			//alert("Esta cuenta no se puede eliminar ya que tiene operaciones asociadas.");
			
		} else {
			if(estatus_cecoban.equals("99")) {
				mensaje="Ya existe la validación de esta cuenta CLABE, realmente desea eliminarla?";
				banderaElimina=true;
					banderaValida=true;
					//ruta.accion.value = "Valida";
					
				
			} else {
				banderaElimina=true;
				banderaValida = false;
				mensaje="¿Esta seguro de querer eliminar el registro?";
				
			}
		}
	}
		
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("banderaElimina",new Boolean(banderaElimina));
			jsonObj.put("banderaValida",new Boolean(banderaValida));
			jsonObj.put("afiliadoE",afiliadoE);
			jsonObj.put("cuenta",cuenta);
			jsonObj.put("estatus_cecoban",estatus_cecoban);
			jsonObj.put("cuenta_dolares",cuenta_dolares);
			jsonObj.put("mensaje",mensaje);
			
			infoRegresar = jsonObj.toString();
			System.out.println(" jsonObj  ** "+jsonObj.toString());

	}else if (informacion.equals("eliminaRegistro")){
		String mensaje ="";
		
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		String afiliadoE 	= (request.getParameter("afiliadoE")	== null)?"":request.getParameter("afiliadoE");
		String cuenta		= (request.getParameter("cuenta") 	== null)?"":request.getParameter("cuenta");
		String estatus_cecoban 	= (request.getParameter("estatus_cecoban")	== null)?"":request.getParameter("estatus_cecoban");
		String cuenta_dolares		= (request.getParameter("cuenta_dolares") 	== null)?"":request.getParameter("cuenta_dolares");
		boolean banderaConfirma=false;
		try {
			HashMap parametrosAdicionales = new HashMap();
			if("ADMIN NAFIN".equals(strPerfil)){
				//parametrosAdicionales.put("DESCRIPCION_ELIMINAR",	"Registro Eliminado"					);
			}else{ // ADMIN EPO, ADMIN IF CLABE
				//parametrosAdicionales.put("DESCRIPCION_ELIMINAR",	"Registro Eliminado\n"+strNombre	);
			}
		//	dispersion.eliminarCuenta(cuenta, iNoUsuario, parametrosAdicionales);
			mensaje = "Registro eliminado satisfactoriamente";
			banderaConfirma=true;
		} catch (Exception e) {
			e.printStackTrace();
			banderaConfirma=false;
			mensaje = "Error al eliminar al cuenta";
		}
		JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("banderaConfirma",new Boolean(banderaConfirma));
			jsonObj.put("mensaje",mensaje);
			infoRegresar = jsonObj.toString();
	
	}else if (informacion.equals("invalidaCuenta")){
		String mensaje ="Ocurrio un error al invalidar la cuenta";
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		HashMap parametrosAdicionales = new HashMap();
			parametrosAdicionales.put("DESCRIPCION_SIN_VALIDACION",	"SIN VALIDAR"		);// + strNombre );
			parametrosAdicionales.put("PANTALLA_ORIGEN", 				"CONSCTAS" 			);
			parametrosAdicionales.put("IC_USUARIO", 						iNoUsuario 			);
			parametrosAdicionales.put("STR_LOGIN",strLogin);
			String cuenta		= (request.getParameter("cuenta") 	== null)?"":request.getParameter("cuenta");
			boolean banderaConfirma=false;
			try{
			dispersion.invalidarCuenta(cuenta, parametrosAdicionales);
			banderaConfirma=true;
			}catch(Exception e){
				e.printStackTrace();
			}
		JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("banderaConfirma",new Boolean(banderaConfirma));
			jsonObj.put("mensaje",mensaje);
			infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("validaCuenta")){
	
		String mensaje ="Ocurrio un error al invalidar la cuenta";

		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		HashMap parametrosAdicionales = new HashMap();
			parametrosAdicionales.put("DESCRIPCION_VALIDACION",	"CUENTA CORRECTA"	);// + strNombre );
			parametrosAdicionales.put("PANTALLA_ORIGEN", 			"CONSCTAS" 			);
			parametrosAdicionales.put("IC_USUARIO", 					iNoUsuario 			);
			parametrosAdicionales.put("STR_LOGIN",strLogin);
			
			String cuenta		= (request.getParameter("cuenta") 	== null)?"":request.getParameter("cuenta");
			boolean banderaConfirma=false;
			try{
			dispersion.validarCuenta(cuenta, parametrosAdicionales);
			banderaConfirma=true;
			}catch(Exception e){
				e.printStackTrace();
			}
		JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("banderaConfirma",new Boolean(banderaConfirma));
			jsonObj.put("mensaje",mensaje);
			infoRegresar = jsonObj.toString();
	}else if(informacion.equals("obtenerCuenta")){
		String cuenta		= (request.getParameter("cuenta") 	== null)?"":request.getParameter("cuenta");

		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		HashMap parametosCuenta = dispersion.getCuenta(cuenta);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("cuenta",parametosCuenta);
		infoRegresar = jsonObj.toString();
	
	}else if (informacion.equals("catalogoMoneda")) {
		String moneda		= (request.getParameter("moneda") 	== null)?"":request.getParameter("moneda");
		CatalogoMoneda catalogo = new CatalogoMoneda();
		catalogo.setCampoClave("IC_MONEDA");
		catalogo.setCampoDescripcion("CD_NOMBRE");
		catalogo.setValoresCondicionIn(moneda,Integer.class);
		infoRegresar = catalogo.getJSONElementos();	
	
}	else  if(informacion.equals("CatalogoEpo2")){
		String producto 	= (request.getParameter("producto")	== null)?"":request.getParameter("producto");
		String clave 	= (request.getParameter("clave")	== null)?"":request.getParameter("clave");
		String tipo_afiliado 	= (request.getParameter("tipo_afiliado")	== null)?"":request.getParameter("tipo_afiliado");
		System.out.println("clave ===== "+ clave);
		/*CatalogoEPOProdReasignar cat = new CatalogoEPOProdReasignar();
		cat.setClave("e.ic_epo");
		cat.setProducto(producto);
		cat.setClaveParam(clave);
		cat.setTipo_afiliado(tipo_afiliado);
		cat.setDescripcion("e.cg_razon_social");
		infoRegresar = cat.getJSONElementos();
		*/
		
		CatalogoEPOSwift cat = new CatalogoEPOSwift();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setTipoAfiliado(tipo_afiliado);
		cat.setCveProducto(producto);
   // cat.setCsModifica("S");
		if("P".equals(tipo_afiliado)){
			cat.setCvePyme(clave);
		}else if("I".equals(tipo_afiliado) || "B".equals(tipo_afiliado)){
			cat.setCveIf(clave);
		}
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("catalogoBanco")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_bancos_tef");
		String banco 	= (request.getParameter("banco")	== null)?"":request.getParameter("banco");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_bancos_tef");
		cat.setOrden("cd_descripcion");
		if(!banco.equals("")){
			cat.setCondicionIn(banco,"Integer");
		}
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("CatalogoProducto2")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_producto_nafin");
		cat.setCampoDescripcion("ic_nombre");
		cat.setTabla("comcat_producto_nafin");
		cat.setOrden("ic_nombre");
		//cat.setCondicionIn("1,4","Integer");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("ModificaReasigna")){
		
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		String accion 	= (request.getParameter("accion")	== null)?"":request.getParameter("accion");
		String no_moneda 	= (request.getParameter("HcbMoneda")	== null)?"":request.getParameter("HcbMoneda");
		String tipo_cuenta 	= (request.getParameter("tipo_cuenta")	== null)?"":request.getParameter("tipo_cuenta");
		String mensaje ="";
		String plaza_swift 	= (request.getParameter("plaza_swift")	== null)?"":request.getParameter("plaza_swift");
		String sucursal_swift 	= (request.getParameter("sucursal_swift")	== null)?"":request.getParameter("sucursal_swift");
		String no_cuenta 	= (request.getParameter("no_cuenta")	== null)?"":request.getParameter("no_cuenta");
		String no_nafin 	= (request.getParameter("numElectronico")	== null)?"":request.getParameter("numElectronico");
		String no_producto 	= (request.getParameter("Producto")	== null)?"":request.getParameter("Producto");
		String no_banco 	= (request.getParameter("Hbanco")	== null)?"":request.getParameter("Hbanco");
		String cuenta 	= (request.getParameter("cuenta")	== null)?"":request.getParameter("cuenta");
		String cuentaDL 	= (request.getParameter("cuentaDL")	== null)?"":request.getParameter("cuentaDL");
    String ic_epo 	= (request.getParameter("HicEpo")	== null)?"":request.getParameter("HicEpo");
    
    String cgSwift 	= (request.getParameter("cgswift")	== null)?"":request.getParameter("cgswift");
    String cgAba 	= (request.getParameter("cgaba")	== null)?"":request.getParameter("cgaba");
		 
		boolean banderaConfirma =false;
		
		if("M".equals(accion) ){  /*--Agregar--*/
		try {
			
			// Si es cuenta swift agregar los campos plaza_swift y sucursal_swift para ser actualizados
			boolean esCuentaSwift 			= ("54".equals(no_moneda) && "50".equals(tipo_cuenta))?true:false;
			HashMap parametrosAdicionales = new HashMap();
			
			if("ADMIN NAFIN".equals(strPerfil)){
				parametrosAdicionales.put("DESCRIPCION_MODIFICACION",		"REGISTRO MODIFICADO");
			}else{ // ADMIN EPO, ADMIN IF CLABE
				parametrosAdicionales.put("DESCRIPCION_MODIFICACION",		"REGISTRO MODIFICADO\n"+strNombre);
			}
			parametrosAdicionales.put("PANTALLA_ORIGEN",						"CONSCTAS");
			if(esCuentaSwift){
        cuenta = cuentaDL;
				parametrosAdicionales.put("PLAZA_SWIFT",		plaza_swift		);
				parametrosAdicionales.put("SUCURSAL_SWIFT",	sucursal_swift	);
        parametrosAdicionales.put("CG_SWIFT",	cgSwift	);
        parametrosAdicionales.put("CG_ABA",	cgAba	);
        
			}
			
			// Actualizar cuenta
			dispersion.ovactualizarCuenta(no_cuenta, no_nafin, no_producto, no_moneda, no_banco, cuenta, iNoUsuario, tipo_cuenta, parametrosAdicionales);
			mensaje = "La cuenta se modifico satisfactoriamente";
			} catch(Exception neError) {
				neError.printStackTrace();
				mensaje="Error al modificar la Cuenta<br>"+neError.getMessage() ;
				banderaConfirma =false;
		}
		
		banderaConfirma =true;
		
	}else if("R".equals(accion)) {
		try {
			
			HashMap parametrosAdicionales = new HashMap();
			if("ADMIN NAFIN".equals(strPerfil)){
				parametrosAdicionales.put("DESCRIPCION_REASIGNACION",		"REASIGNACION");
			}else{ // ADMIN IF CLABE
				parametrosAdicionales.put("DESCRIPCION_REASIGNACION",		"REASIGNACION\n"+strNombre);
			}
			
			dispersion.reasignarCuenta(no_cuenta, ic_epo, iNoUsuario, parametrosAdicionales);
			mensaje = "La cuenta se reasigno correctamente";
		} catch(Exception ne){
			ne.printStackTrace();
			mensaje="La cuenta no se puede reasignar por que ya\nexiste una cuenta asociada a esa EPO";
			banderaConfirma =false;
			
		} /*catch(Exception e) {
			e.printStackTrace();
			mensaje = "Error al reasignar la Cuenta";		
			banderaConfirma =false;
		}*/
			
			banderaConfirma =true;
		}
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("banderaConfirma",new Boolean(banderaConfirma));
			jsonObj.put("mensaje",mensaje);
			infoRegresar = jsonObj.toString();
	
	
	}else if (informacion.equals("Catagolo.Moneda.01")){//FODEA-033-2014
		CatalogoMoneda catalogo = new CatalogoMoneda();
			catalogo.setCampoClave("IC_MONEDA");
			catalogo.setCampoDescripcion("CD_NOMBRE");
			catalogo.setValoresCondicionIn("1,54",Integer.class);
			infoRegresar = catalogo.getJSONElementos();	
	}else if (informacion.equals("obtener_info_ventana_modificar")){
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
		Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		//Consulta para obtener los datos que se mostraran el la ventana
		//->Producto
		//->No. Nafin Electronico
		String numeroNE 	= (request.getParameter("txt_nafelec")==null)?"":request.getParameter("txt_nafelec");
		String producto 	= (request.getParameter("Producto")==null)?"":request.getParameter("Producto");
		String moneda 	= (request.getParameter("cboMoneda01")==null)?"":request.getParameter("cboMoneda01");
		//->Nombre
		//->RFC
		//->Moneda
		String ic_pyme					= "";
		String txtCadenasPymes		= "";
		String rfc						= "";
		Map registros =afiliacion.buscaAfiliado(numeroNE);
		if(registros != null && registros.get("clave") != null){
			//ic_pyme					= (registros.getString("PYME")	== null)?"":registros.getString("PYME");
			//txtCadenasPymes		= (registros.getString("NOMBRE")	== null)?"":registros.getString("NOMBRE");			
			//rfc 						= (registros.getString("RFC")	== null)?"":registros.getString("RFC");
			
			ic_pyme					= (registros.get("clave")	== null)?"":(String)registros.get("clave");
			txtCadenasPymes		= (registros.get("nombre")	== null)?"":(String)registros.get("nombre");
			rfc 						= (registros.get("rfc")	== null)?"":(String)registros.get("rfc");
		}
		String ic_cuentas[] = request.getParameterValues("ic_cuentas");
		System.err.println(ic_cuentas);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("Producto",producto);
		jsonObj.put("NoNafinElect",numeroNE);
		jsonObj.put("Nombre",txtCadenasPymes);
		jsonObj.put("RFC",rfc);
		jsonObj.put("Moneda",moneda);
		jsonObj.put("estadoSiguiente",		"MOSTRAR_VENTANA_MODIFICAR"	);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("guargar_info_ventana_modificar")){
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
		
		String ic_cuentas[] = request.getParameterValues("ic_cuentas");
		String cuentaClabeConfirma  	= (request.getParameter("cuentaClabeConfirma")	== null)?"":request.getParameter("cuentaClabeConfirma");
		
		String no_moneda 	= (request.getParameter("hdnMoneda")	== null)?"":request.getParameter("hdnMoneda");
		String no_nafin 	= (request.getParameter("numElectronico")	== null)?"":request.getParameter("numElectronico");
		String no_producto 	= (request.getParameter("Producto")	== null)?"":request.getParameter("Producto");
		String no_banco 	= (request.getParameter("banco01")	== null)?"":request.getParameter("banco01");
		
		String mensaje ="";
		String plaza_swift 	= (request.getParameter("plaza_swift")	== null)?"":request.getParameter("plaza_swift");
		String sucursal_swift 	= (request.getParameter("sucursal_swift")	== null)?"":request.getParameter("sucursal_swift");
		
		String cuenta 	= (request.getParameter("cuentaClabe")	== null)?"":request.getParameter("cuentaClabe");
		String no_cuenta_confirmacion  	= (request.getParameter("cuentaClabeConfirma")	== null)?"":request.getParameter("cuentaClabeConfirma");
		//String cuenta 	= (request.getParameter("cuenta")	== null)?"":request.getParameter("cuenta");
		String ic_epo 	= (request.getParameter("HicEpo")	== null)?"":request.getParameter("HicEpo");
		
		boolean banderaConfirma =false;
		try{
			
			// Actualizar cuenta
			//dispersion.ovactualizarCuenta(no_cuenta, no_nafin, no_producto, no_moneda, no_banco, cuenta, iNoUsuario, tipo_cuenta, parametrosAdicionales);
			for(int i = 0;i<ic_cuentas.length;i++){
			
				String no_cuenta 	=ic_cuentas[i] ;
				HashMap parametosCuenta = dispersion.getCuenta(no_cuenta);
				String tipo_cuenta =(String) parametosCuenta.get("tipo_cuenta").toString();
				// Si es cuenta swift agregar los campos plaza_swift y sucursal_swift para ser actualizados
				boolean esCuentaSwift 			= ("54".equals(no_moneda) && "50".equals(tipo_cuenta))?true:false;
				HashMap parametrosAdicionales = new HashMap();
				
				if("ADMIN NAFIN".equals(strPerfil)){
					parametrosAdicionales.put("DESCRIPCION_MODIFICACION",		"REGISTRO MODIFICADO");
				}else{ // ADMIN EPO, ADMIN IF CLABE
					parametrosAdicionales.put("DESCRIPCION_MODIFICACION",		"REGISTRO MODIFICADO\n"+strNombre);
				}
				parametrosAdicionales.put("PANTALLA_ORIGEN",						"CONSCTAS");
				if(esCuentaSwift){
					parametrosAdicionales.put("PLAZA_SWIFT",		plaza_swift		);
					parametrosAdicionales.put("SUCURSAL_SWIFT",	sucursal_swift	);
				}
				dispersion.ovactualizarCuenta(no_cuenta, no_nafin, no_producto, no_moneda, no_banco, cuenta, iNoUsuario, tipo_cuenta, parametrosAdicionales);
				banderaConfirma =true;
			}
			if(banderaConfirma){
				mensaje = "Se realizo con éxito la modificación de las Cadenas Productivas";
			}
		}catch(Exception e ){
			e.printStackTrace();
			banderaConfirma =false;
			mensaje = e.getMessage().toString();
		}
		
		JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("banderaConfirma",new Boolean(banderaConfirma));
			jsonObj.put("mensaje",mensaje);
			infoRegresar = jsonObj.toString();
	}else if(informacion.equals("catalogoBanco01")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_bancos_tef");
		String banco 	= (request.getParameter("banco")	== null)?"":request.getParameter("banco");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_bancos_tef");
		cat.setOrden("cd_descripcion");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("Validar_Invalidar")){
		String ic_cuentas[] = request.getParameterValues("ic_cuentas");
		
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		HashMap parametrosAdicionales = new HashMap();
			parametrosAdicionales.put("DESCRIPCION_SIN_VALIDACION",	"SIN VALIDAR"		);// + strNombre );
			parametrosAdicionales.put("PANTALLA_ORIGEN", 				"CONSCTAS" 			);
			parametrosAdicionales.put("IC_USUARIO", 						iNoUsuario 			);
			parametrosAdicionales.put("STR_LOGIN",strLogin);
			String cuenta		= (request.getParameter("cuenta") 	== null)?"":request.getParameter("cuenta");
			boolean banderaConfirma=false;
			try{
				String accion = "";
				for(int i = 0; i<ic_cuentas.length;i++){
					accion = ic_cuentas[i].substring(0,1);
					cuenta = ic_cuentas[i].substring(1,ic_cuentas[i].length());
					System.err.println("ACCION ="+accion+" --->"+cuenta);
					if("V".equals(accion)){
						dispersion.validarCuenta(cuenta, parametrosAdicionales);
					}else{
						dispersion.invalidarCuenta(cuenta, parametrosAdicionales);
					}
					banderaConfirma=true;		
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("banderaConfirma",new Boolean(banderaConfirma));
			//jsonObj.put("mensaje",mensaje);
			infoRegresar = jsonObj.toString();
			
	}else if(informacion.equals("GenerarArchivos")){
		String tipoArchivo 	= (request.getParameter("tipo")	== null)?"":request.getParameter("tipo");
		String nombreArchivo = "DetalleCuentasCLABE";
		
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		Calendar calendar = Calendar.getInstance();
		
		int HH = calendar.get(Calendar.HOUR_OF_DAY);
		int hh = 13;
		int MM = calendar.get(Calendar.MINUTE);
		int mm = 29;
		int SS = calendar.get(Calendar.SECOND);
		int ss = 59;
		boolean generarArchivo = false;
		Date now = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("HH:mm:ss"); 
		
			String horaLimite = "13:29:59";
			String horaActual = formato.format(now);
			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			Date horaLim;
         Date horaAct;
                horaLim = dateFormat.parse(horaLimite);
                horaAct = dateFormat.parse(horaActual);
			
			int resultado = horaAct.compareTo(horaLim);
			//0 = igual
			//-1 si hora actual < hora limite
			//1 si hora actual > hora limite
		
		//if((HH<=hh) && (MM<=mm) && (SS<=ss)){
		//if(resultado < 1){
			System.err.println(HH+" : "+MM+" : "+SS+" <---Descarga por pantalla  es posible");
			boolean exito =dispersion.generarArchivosCuenta(iNoUsuario, strNombreUsuario, strDirectorioTemp,  nombreArchivo,  tipoArchivo);
			generarArchivo = true;
		/*}else{
			System.err.println(HH+" : "+MM+" : "+SS+" <---Descarga por pantalla no es posible");
			//boolean exito =dispersion.generarArchivosCuenta("", "", strDirectorioTemp,  "DetalleCuentasCLABE",  null);//Para descargar ambos archivos el que falta
			generarArchivo = false;
		}*/		
		
		JSONObject jsonObj			= new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("descarga",new Boolean(generarArchivo));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo+((tipoArchivo.equals("TXT"))?".txt":".xls"));
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("ValidaUsuario")){
		String claveUsuario 	= (request.getParameter("claveUsuario")	== null)?"":request.getParameter("claveUsuario");
		String contrasenia 	= (request.getParameter("contrasenia")	== null)?"":request.getParameter("contrasenia");
		String cuenta		= (request.getParameter("cuentaConfirma") 	== null)?"":request.getParameter("cuentaConfirma");
		String accion = "";
		String mensaje ="";
	
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		if(dispersion.esUsuarioCorrecto(strLogin, claveUsuario, contrasenia)) {
			accion = "E";
		} else {
			accion = "";
			mensaje = "El login y/o password del usuario es incorrecto, operación cancelada.";
		}
		JSONObject jsonObj			= new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("accion",accion);
		jsonObj.put("mensaje",mensaje);
		jsonObj.put("cuenta",cuenta);
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("eliminaRegistroValida")){
		String mensaje ="";
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		String cuenta		= (request.getParameter("cuenta") 	== null)?"":request.getParameter("cuenta");
		boolean banderaConfirma=false;
		try {
			HashMap parametrosAdicionales = new HashMap();
			if("ADMIN NAFIN".equals(strPerfil)){
				parametrosAdicionales.put("DESCRIPCION_ELIMINAR",	"Registro Eliminado"					);
			}else{ // ADMIN EPO, ADMIN IF CLABE
				parametrosAdicionales.put("DESCRIPCION_ELIMINAR",	"Registro Eliminado\n"+strNombre	);
			}
			dispersion.eliminarCuenta(cuenta, iNoUsuario, parametrosAdicionales);
			mensaje = "Registro eliminado satisfactoriamente";
			banderaConfirma=true;
		} catch (Exception e) {
			e.printStackTrace();
			banderaConfirma=false;
			mensaje = "Error al eliminar al cuenta";
		}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("banderaConfirma",new Boolean(banderaConfirma));
		jsonObj.put("mensaje",mensaje);
		infoRegresar = jsonObj.toString();
	
	}
log.info("InfoRegresar "+infoRegresar);	
%>
<%=infoRegresar%>
