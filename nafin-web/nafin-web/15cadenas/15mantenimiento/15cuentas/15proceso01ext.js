Ext.onReady(function() {


	var cargaArchivo = function(estado, respuesta ){
 
		if(estado == "INICIAR_CARGA_VALIDACION_ARCH"){
			// Determinar el estado siguiente
			
			Ext.getCmp("panelFormaCargaArchivo").getForm().submit({
				clientValidation: 	true,
				url: 						'15proceso01ext.data.jsp?informacion=CargaArchivo.Iniciar',
				waitMsg:   				'Subiendo archivo...',
				waitTitle: 				'Por favor espere',
				success: function(form, action) {
					 procesaCargaArchivo(null,  true,  action.response );
				},
				failure: function(form, action) {
				 	 action.response.status = 200;
					 procesaCargaArchivo(null,  false, action.response );
				}
			});
			
		} 
	};
	
	var procesaCargaArchivo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			//alert(resp.lsEstCorrecto);
			Ext.getCmp('lsProcSirac1').setValue(resp.lsProcSirac);
			Ext.getCmp('panelResumenValidArch1').show();
			Ext.getCmp('txtAregCorrectos1').setValue(resp.lsEstCorrecto);
			Ext.getCmp('txtAregErroneos1').setValue(resp.lsEstError);
			
			if(Number(resp.numRegCorrectos)>0){
				Ext.getCmp('btnConfirmar1').show();
			}else{
				Ext.getCmp('btnConfirmar1').hide();
			}
			if(Number(resp.numRegErroneos)>0){
				Ext.getCmp('btnArchivoErrores1').show();
				Ext.getCmp('btnArchivoErrores1').setHandler(function(){
					var archivo = resp.urlArchivo;
					archivo = archivo.replace('/nafin','');
					var params = {nombreArchivo: archivo};				
					panelFormaCargaArchivo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					panelFormaCargaArchivo.getForm().getEl().dom.submit();
				});
				
			}else{
				Ext.getCmp('btnArchivoErrores1').hide();
			}
			
		}
	}
	
	var succesProcesaInfo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			storeSolicitudes.loadData(resp.lsNumeroCuenta);
			Ext.getCmp('panelFormaCargaArchivo').hide();
			Ext.getCmp('panelResumenValidArch1').hide();
			Ext.getCmp('panelInfoProcesada1').show();
			//Ext.getCmp('txtAreaSolicitudes1').setValue(resp.lsNumeroCuenta);
			Ext.getCmp('labelPreacuseCargaArchivo').update('Total: '+resp.totalContador);
			
		}
	}

//------------------------STORES------------------------------------------------
	var catalogoProducto = new Ext.data.JsonStore({
	   id: 'catalogoProducto',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15proceso01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoProducto'	
		},
		autoLoad: false,
		listeners:{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
  });
  var storeSolicitudes = new Ext.data.ArrayStore({
    // store configs
    autoDestroy: true,
    storeId: 'myStore',
	 autoLoad: false,
    // reader configs
    //idIndex: 0,  
    fields: [
       'SOLICITUD'
    ]
});

//------------------------------------------------------------------------------
	var elementosFormaCargaArchivo = [
		{
			xtype: 'combo',
			name: 'Producto',
			id:	'idProducto',
			fieldLabel: 'Producto',
			emptyText: 'Seleccionar',
			mode: 'local',
			//anchor: '-150',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'Producto',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoProducto,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'radiogroup',
			fieldLabel: 'Cuenta',
			columns: 2,
			id:'rgCuenta',
			name:'frgCuenta',
			items: [
				 {boxLabel: 'TEF', name: 'optCuenta', inputValue: "T", checked: true},
				 {boxLabel: 'CECOBAN', name: 'optCuenta', inputValue: "C"}
			]
		},
		{
			xtype: 'radiogroup',
			fieldLabel: 'Tipo Archivo',
			id:'rgTipoArch',
			name:'frgTipoArch',
			columns: 2,
			items: [
				 {boxLabel: 'TXT', name: 'tipoArchivo', inputValue: "archTxt", checked: true},
				 {boxLabel: 'EXCEL', name: 'tipoArchivo', inputValue: "archExcel"}
			]
		},
		{
		  xtype: 		'fileuploadfield',
		  id: 			'_archivo',
		  name: 			'archivo',
		  emptyText: 	'Seleccione...',
		  fieldLabel: 	"Ruta del Archivo de Registros", 
		  buttonText: 	null,
		  buttonCfg: {
			  iconCls: 	'upload-icon'
		  },
		  anchor: 		'-20'
		},
		{
			xtype: 		'panel',
			anchor: 		'100%',
			style: {
				marginTop: 		'10px'
			},
			layout: {
				type: 'table'
				//pack: 'end',
				//align: 'middle'
			},
			layoutConfig: {columns:2},
			items: [
				{
					xtype:'displayfield',
					html:	'&nbsp;',
					width:'570'
				},
				{
					xtype: 			'button',
					text: 			'Continuar',
					iconCls: 		'icoContinuar',
					id: 				'botonContinuarCargaArchivo',
					handler:    function(boton, evento) {

						// Revisar si la forma es invalida
						var forma = Ext.getCmp("panelFormaCargaArchivo").getForm();
						if(!forma.isValid()){
							return;
						}

						
						// Validar que se haya especificado un archivo
						var cmbProd = Ext.getCmp("idProducto");
						if( Ext.isEmpty( cmbProd.getValue() ) ){
							cmbProd.markInvalid("Debe especificar un producto");
							return;
						}
						
						// Validar que se haya especificado un archivo
						var archivo = Ext.getCmp("_archivo");
						if( Ext.isEmpty( archivo.getValue() ) ){
							archivo.markInvalid("Debe especificar un archivo");
							return;
						}

						// Revisar el tipo de extension del archivo
						var myRegexTXT = /^.+\.([tT][xX][tT])$/;
						var myRegexXLS = /^.+\.([xX][lL][sS])$/;

						var nombreArchivo 	= archivo.getValue();
						var numeroRegistros 	= Ext.getCmp("numeroRegistros1").getValue();
						if( Ext.getCmp('rgTipoArch').getValue().getGroupValue()=='archTxt' && !myRegexTXT.test(nombreArchivo) ){
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensión txt.");
							return;
						} else if( Ext.getCmp('rgTipoArch').getValue().getGroupValue()=='archExcel' && !myRegexXLS.test(nombreArchivo) ){
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensión xls.");
							return;
						}

						// Cargar archivo
						cargaArchivo("INICIAR_CARGA_VALIDACION_ARCH",null);

					}
				}
			
			]
		},
		{
         xtype: 	'hidden',
			name:		'lsProcSirac',
			id:		'lsProcSirac1'
		},
		{
			xtype: 	'hidden',
			name:		'numeroRegistros1',
			id:		'numeroRegistros1'
		},
		{
			xtype: 	'hidden',
			name:		'sumatoriaSaldosFinMes1',
			id:		'sumatoriaSaldosFinMes1'
		}
	];

	var panelFormaCargaArchivo = new Ext.form.FormPanel({
		id: 				'panelFormaCargaArchivo',
		width: 			710,
		title: 			'Verificación CLABE y Swift',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		fileUpload: 	true,
		hidden:			false,
		renderHidden:	true,
		style:			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth:		190,
		defaultType:	'textfield',
		/*
		layoutConfig: {
			fieldTpl: new Ext.XTemplate(
				'<tpl if="id==\'_archivo\'">',
           		 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">',
						  		'<a class="x-tool x-tool-ayudaLayout" onfocus="blur()" style="float:left;" onclick="javascript:showPanelLayoutDeCarga();"></a>&nbsp;',
						  		'{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
						  '</div>',
					 '</div>',
			  '</tpl>',
			  '<tpl if="id!=\'_archivo\'">',
					 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
					 '</div>',
			  '</tpl>'
		   )
		},*/
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementosFormaCargaArchivo,
		monitorValid: 	false
	});
	
	var panelResumenValidArch = new Ext.Panel({
		name: 'panelResumenValidArch',
		id: 'panelResumenValidArch1',
		style: 'margin: 0 auto',
		width: 710,
		frame: true,
		hidden: true,
		layout: 'hbox',
		items:[
			{
				xtype: 'panel',
				frame: true,
				title: 'Solicitudes Correctas',
				width: 350,
				items:[
					{
						xtype:'textarea',
						name: 'txtAregCorrectos',
						id: 'txtAregCorrectos1',
						readOnly: true,
						height: 300,
						width: 340
					}
				]
				
			},
			{
				xtype: 'panel',
				frame: true,
				title: 'Solicitudes con Errores',
				width: 350,
				items:[
					{
						xtype:'textarea',
						name: 'txtAregErroneos',
						id: 'txtAregErroneos1',
						readOnly: true,
						height: 300,
						width:340
					}
				]
			}
		],
		buttons: [
			{
				name:'btnConfirmar',
				id:'btnConfirmar1',
				text: 'Confirmar',
				handler: function(btn){
					Ext.Ajax.request({
						url: '15proceso01ext.data.jsp',
						params: 	Ext.apply(
							panelFormaCargaArchivo.getForm().getValues(),{
								informacion: 'ProcesarInformacion'
							}
						),
						callback: succesProcesaInfo
					});
				}
			},
			{
				name:'btnRegresar1',
				text: 'Regresar',
				handler: function(){
					window.location = '15proceso01ext.jsp';
				}
			},
			{
				name:'btnArchivoErrores',
				id:'btnArchivoErrores1',
				text: 'Archivo Errores'
			}
		]
	});
	
	var panelInfoProcesada = new Ext.Panel({
		name: 'panelInfoProcesada',
		id: 'panelInfoProcesada1',
		style: 'margin: 0 auto',
		width: 320,
		frame: true,
		hidden: true,
		title: '<p align="center">Solicitudes</p>',
		items:[
			/*{
				xtype:'textarea',
				name: 'txtAreaSolicitudes',
				id: 'txtAreaSolicitudes1',
				readOnly: true,
				height: 300,
				width:450
			},*/
			{
				xtype: 'grid',
				store: 	storeSolicitudes,
				id:		'gridCuentasAgregadas',
				style: 'margin: 0 auto',
				//hidden: 	true,
				margins: '20 0 0 0',
				title: 	'',
				columns: [
					{
						header: 		'',
						dataIndex: 	'SOLICITUD',
						align: 'center',
						sortable: 	true,
						resizable: 	true,
						width: 		280,
						hidden: 		false
					}
				],
				stripeRows: true,
				loadMask: 	true,
				height: 		250,
				width: 		300,		
				frame: 		true,
				bbar: [{
					xtype: 	'label',
					id:	 	'labelPreacuseCargaArchivo'
					//cls:		'x-form-item'
				}]
			}
		],
		buttons: [
			{
				name:'btnRegresar',
				text: 'Regresar',
				handler: function(btn){
					window.location = '15proceso01ext.jsp';
				}
			}
		]
	});

	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelFormaCargaArchivo,
			panelResumenValidArch,
			panelInfoProcesada
		]
	});
	
	catalogoProducto.load();

 
});